﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CabRideEngineDapper {
    public partial class AffiliateAreaCodes {

        #region Related Tables

        private Affiliate mAffiliate;
        public Affiliate Affiliate {
            get {
                if (mAffiliate == null) {
                    mAffiliate = Affiliate.GetAffiliate(this.AffiliateID);
                }
                return mAffiliate;
            }
        }

        #endregion
    }
}
