﻿using PayPal.Payments.Common.Utility;
using PayPal.Payments.DataObjects;
using PayPal.Payments.Transactions;
using System;
using System.Collections.Generic;
using System.Net;
using TaxiPassCommon.Banking.Models;

namespace TaxiPassCommon.Banking {
	public class Verisign : IProcessor {

		private UserInfo mUserInfo;
		private VerisignHost mHost;
		private VerisignProxy mProxy;

		internal static VerisignCredentials mCredentials;

		public Verisign(VerisignCredentials pCredentials) {
			ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls;

			if (pCredentials.Timeout == 0) {
				pCredentials.Timeout = 30;
			}

			mCredentials = pCredentials;

			mUserInfo = new UserInfo(pCredentials.UserName, pCredentials.Vendor, pCredentials.Partner, pCredentials.Password);
			mHost = new VerisignHost();
			mHost.HostAddress = pCredentials.ProdURL;
			mHost.TimeOut = pCredentials.Timeout;
			mHost.VerisignPort = pCredentials.Port;

			mProxy = new VerisignProxy();
			//mProxy.ProxyAddress = pCredentials.ProxyAddress;
			//mProxy.ProxyLogon = pCredentials.ProxyLogon;
			//mProxy.ProxyPassword = pCredentials.ProxyPassword;
			//mProxy.ProxyPort = pCredentials.ProxyPort.ToString();
		}


		/// <summary>
		/// Pre authorizes the card.
		/// </summary>
		/// <param name="pRequest.pCardData">The p card data.</param>
		/// <param name="pAuthAmount">The p auth amount.</param>
		/// <param name="pComments">The p comments.</param>
		/// <param name="pVoucherNo">The p voucher no.</param>
		/// <param name="pService">The p service.</param>
		/// <returns></returns>
		public ProcessorResponse PreAuthCard(CCRequest pRequest) {
			if (pRequest.CardData.TestCard) {
				mHost.HostAddress = mCredentials.TestURL;
			}

			if (mHost.HostAddress.IsNullOrEmpty()) {
				return null;
			}

			PayflowConnectionData connection = new PayflowConnectionData(mHost.HostAddress, mHost.VerisignPort, mHost.TimeOut);

			CardTender card = FillCardInfo(pRequest.CardData);
			Invoice oInvoice = CreatePaymentInvoice(pRequest); // pRequest.Amount, pRequest.Comments, pRequest.VoucherNo, pRequest.Service, "");

			// Create a new Auth Transaction.
			AuthorizationTransaction trans = new AuthorizationTransaction(mUserInfo, connection, oInvoice, card, PayflowUtility.RequestId);
			// Submit the Transaction
			Response response = trans.SubmitTransaction();


			return ProcessResponse(response, pRequest.Amount, TrxTypes.Authorization, pRequest.CardData.TestCard);

		}


		/// <summary>
		/// Fills the card info.
		/// </summary>
		/// <param name="pRequest.pCardData">The credit card data.</param>
		/// <returns></returns>
		private static CardTender FillCardInfo(CreditCard pCardData) {
			CardTender card = null;
			if (pCardData.Track2.IsNullOrEmpty()) {
				PayPal.Payments.DataObjects.CreditCard CC = new PayPal.Payments.DataObjects.CreditCard(pCardData.CardNo, pCardData.CardExpiryAsMMYY);
				CC.Cvv2 = pCardData.CVV;

				// Create a new Tender - Card Tender data object.
				card = new CardTender(CC);
			} else {
				SwipeCard oSwipe = new SwipeCard(pCardData.Track2);
				card = new CardTender(oSwipe);
			}
			return card;
		}


		/// <summary>
		/// Processes the response.
		/// </summary>
		/// <param name="pResponse">The response.</param>
		/// <param name="pAuthAmount">The auth amount.</param>
		/// <param name="pTrxType">Type of the Transaction.</param>
		/// <param name="pTestCard">if set to <c>true</c> [test card].</param>
		/// <returns></returns>
		private ProcessorResponse ProcessResponse(Response pResponse, decimal pAmount, string pTrxType, bool pTestCard) {
			ProcessorResponse oResult = new ProcessorResponse();


			oResult.TestCard = pTestCard;
			oResult.VerisignAccountID = mCredentials.VerisignAccountID;
			oResult.MerchantID = mCredentials.MerchantID;
			oResult.CreditCardProcessor = mCredentials.MyGateway();

			oResult.Result = pResponse.TransactionResponse.Result.ToString();
			oResult.Reference = pResponse.TransactionResponse.Pnref;
			oResult.Message = pResponse.TransactionResponse.RespMsg;
			oResult.Amount = pAmount;

			oResult.AuthCode = pResponse.TransactionResponse.AuthCode;
			oResult.AVS = pResponse.TransactionResponse.IAVS;
			oResult.AVSZip = pResponse.TransactionResponse.AVSZip;
			oResult.CVV2Match = pResponse.TransactionResponse.CVV2Match;

			oResult.TransType = pTrxType;
			oResult.TransDate = DateTime.Now;

			// 12/5/11 Added for Carmel
			oResult.AVSResultCode = pResponse.TransactionResponse.AVSAddr;

			//placed as USAePay is now sending us failed trans with a result code of 0
			if (oResult.Result.Equals("0")) {
				if (!oResult.Message.IsNullOrEmpty() && !oResult.Message.Equals("Approved", StringComparison.CurrentCultureIgnoreCase)) {
					if (!oResult.Message.Equals("Unable to get tran details (p)", StringComparison.CurrentCultureIgnoreCase)) {
						oResult.Result = "1";
					}
				}
			}

			return oResult;
		}


		//private ProcessorResponse ProcessResponse(PayPal.Api.Refund pResponse, decimal pAmount, string pTrxType, bool pTestCard) {
		//    ProcessorResponse oResult = new ProcessorResponse();


		//    oResult.TestCard = pTestCard;
		//    oResult.VerisignAccountID = mCredentials.VerisignAccountID;
		//    oResult.MerchantID = mCredentials.MerchantID;
		//    oResult.CreditCardProcessor = mCredentials.MyGateway();

		//    oResult.Result = pResponse.id; //.TransactionResponse.Result.ToString();
		//    oResult.Reference = pResponse.capture_id; // .TransactionResponse.Pnref;
		//    oResult.Message = pResponse.reason; //.TransactionResponse.RespMsg;
		//    oResult.Amount = pAmount;

		//    //oResult.AuthCode = pResponse.TransactionResponse.AuthCode;
		//    //oResult.AVS = pResponse.TransactionResponse.IAVS;
		//    //oResult.AVSZip = pResponse.TransactionResponse.AVSZip;
		//    //oResult.CVV2Match = pResponse.TransactionResponse.CVV2Match;

		//    oResult.TransType = pTrxType;
		//    oResult.TransDate = DateTime.Now;

		//    // 12/5/11 Added for Carmel
		//    //oResult.AVSResultCode = pResponse.TransactionResponse.AVSAddr;

		//    //placed as USAePay is now sending us failed trans with a result code of 0
		//    if (oResult.Result.Equals("0")) {
		//        if (!oResult.Message.IsNullOrEmpty() && !oResult.Message.Equals("Approved", StringComparison.CurrentCultureIgnoreCase)) {
		//            if (!oResult.Message.Equals("Unable to get tran details (p)", StringComparison.CurrentCultureIgnoreCase)) {
		//                oResult.Result = "1";
		//            }
		//        }
		//    }

		//    return oResult;
		//}

		/// <summary>
		/// Creates the payment invoice.
		/// </summary>
		/// <param name="pTransAmount">Amount.</param>
		/// <param name="pComments">Comments.</param>
		/// <param name="pVoucherNo">Voucher no.</param>
		/// <param name="pService">The Service Type.</param>		/// <returns></returns>
		private Invoice CreatePaymentInvoice(CCRequest pRequest) { // decimal pTransAmount, string pComments, string pVoucherNo, string pService, string pDriverID) {

			//decimal amount = pTransAmount;
			//Invoice oInvoice = new Invoice();
			//oInvoice.TaxAmt = new Currency(0);
			//oInvoice.Amt = new Currency(amount);
			//oInvoice.TaxExempt = "Y";
			//oInvoice.Comment1 = pComments;
			//oInvoice.Comment2 = pVoucherNo;
			//oInvoice.OrderDate = DateTime.Today.ToString("MMddyy");
			//oInvoice.InvNum = pDriverID;

			//decimal amount = pTransAmount;
			Invoice oInvoice = new Invoice();
			oInvoice.TaxAmt = new Currency(0);
			oInvoice.Amt = new Currency(pRequest.Amount);
			oInvoice.TaxExempt = "Y";
			oInvoice.Comment1 = pRequest.Comments;
			oInvoice.Comment2 = pRequest.CardData.SalesPerson;
			oInvoice.PoNum = pRequest.CardData.SalesPerson;
			oInvoice.OrderDate = DateTime.Today.ToString("MMddyy");
			oInvoice.InvNum = pRequest.VoucherNo;

			return oInvoice;
		}

		/// <summary>
		/// Charges the card.
		/// </summary>
		/// <param name="pRequest">The request.</param>
		/// <returns></returns>
		public ProcessorResponse ChargeCard(CCRequest pRequest) {
			if (pRequest.CardData.TestCard) {
				mHost.HostAddress = mCredentials.TestURL;
			}

			if (mHost.HostAddress.IsNullOrEmpty()) {
				return null;
			}

			PayflowConnectionData connection = new PayflowConnectionData(mHost.HostAddress, mHost.VerisignPort, mHost.TimeOut);

			CardTender card = FillCardInfo(pRequest.CardData);
			Invoice oInvoice = CreatePaymentInvoice(pRequest); // pRequest.Amount, pRequest.Comments, pRequest.VoucherNo, pRequest.Service, pRequest.DriverID);

			// Create a new Auth Transaction.
			SaleTransaction trans = new SaleTransaction(mUserInfo, connection, oInvoice, card, PayflowUtility.RequestId);
			// Submit the Transaction
			Response response = trans.SubmitTransaction();


			return ProcessResponse(response, pRequest.Amount, TrxTypes.Sale, pRequest.CardData.TestCard);

		}

		/// <summary>
		/// Charges the pre authorization.
		/// </summary>
		/// <param name="pRequest">The request.</param>
		/// <returns></returns>
		public ProcessorResponse ChargeAuth(CCRequestRefNo pRequest) {
			if (pRequest.TestCard) {
				mHost.HostAddress = mCredentials.TestURL;
			}

			if (mHost.HostAddress.IsNullOrEmpty()) {
				return null;
			}

			PayflowConnectionData connection = new PayflowConnectionData(mHost.HostAddress, mHost.VerisignPort, mHost.TimeOut);

			Invoice oInvoice = CreatePaymentInvoice(new CCRequest(new CreditCard(), pRequest.Amount, "", pRequest.VoucherNo, "", "", pRequest.DriverID, pRequest.GetRideTrans, 0, pRequest.TaxiPassFee, pRequest.DriverFee));
			Response response = new CaptureTransaction(pRequest.RefNum, mUserInfo, connection, oInvoice, PayflowUtility.RequestId).SubmitTransaction();

			return ProcessResponse(response, pRequest.Amount, TrxTypes.DelayedCapture, pRequest.TestCard);

		}

		/// <summary>
		/// Refunds the card.
		/// </summary>
		/// <param name="pRequest">The request.</param>
		/// <returns></returns>
		public ProcessorResponse Refund(CCRequestRefNo pRequest) {
			if (pRequest.TestCard) {
				mHost.HostAddress = mCredentials.TestURL;
			}

			if (mHost.HostAddress.IsNullOrEmpty()) {
				return null;
			}

			PayflowConnectionData connection = new PayflowConnectionData(mHost.HostAddress, mHost.VerisignPort, mHost.TimeOut);

			////Invoice oInvoice = CreatePaymentInvoice(pRequest.Amount, "", "", "", "");
			Invoice oInvoice = CreatePaymentInvoice(new CCRequest(new CreditCard(), Math.Abs(pRequest.Amount), "", pRequest.VoucherNo, "", "", pRequest.DriverID, pRequest.GetRideTrans, 0, pRequest.TaxiPassFee, pRequest.DriverFee));
			CreditTransaction trans = new CreditTransaction(pRequest.RefNum, mUserInfo, connection, oInvoice, PayflowUtility.RequestId);
			Response response = trans.SubmitTransaction();

			//PayPal.Api.OAuthTokenCredential tokenCredential =   new PayPal.Api.OAuthTokenCredential(mCredentials.UserName, mCredentials.Password);

			//string accessToken = tokenCredential.GetAccessToken();

			//var apiContext = Configuration.GetAPIContext();
			//PayPal.Api.Sale sale = PayPal.Api.Sale.Get(apiContext, pRequest.RefNum);


			//PayPal.Api.Refund refund = new PayPal.Api.Refund() {
			//    amount = new PayPal.Api.Amount() {
			//        currency = "USD",
			//        total = pRequest.Amount.ToString("F2")
			//    }
			//};

			//// Create a Sale object with the given sale transaction id.
			////var sale = new PayPal.Api.Sale() {
			////    id = pRequest.RefNum
			////};

			////var apiContext = Configuration.GetAPIContext();
			//var response = sale.Refund(apiContext, refund);

			return ProcessResponse(response, pRequest.Amount, TrxTypes.Credit, pRequest.TestCard);
		}



		/// <summary>
		/// Voids the specified transaction.
		/// </summary>
		/// <param name="pRequest">The request.</param>
		/// <returns></returns>
		/// <exception cref="System.Exception">Verisign URL Missing</exception>
		public bool Void(CCRequestRefNo pRequest) {
			if (pRequest.TestCard) {
				mHost.HostAddress = mCredentials.TestURL;
			}

			if (mHost.HostAddress.IsNullOrEmpty()) {
				throw new Exception("Verisign URL Missing");
			}

			PayflowConnectionData connection = new PayflowConnectionData(mHost.HostAddress, mHost.VerisignPort, mHost.TimeOut);
			VoidTransaction trans = new VoidTransaction(pRequest.RefNum, mUserInfo, connection, PayflowUtility.RequestId);
			Response response = trans.SubmitTransaction();
			return response.TransactionResponse.Result == 0;
		}


		public ProcessorResponse OpenCredit(CCRequest pRequest) {
			if (pRequest.CardData.TestCard) {
				mHost.HostAddress = mCredentials.TestURL;
			}

			if (mHost.HostAddress.IsNullOrEmpty()) {
				return null;
			}

			PayflowConnectionData connection = new PayflowConnectionData(mHost.HostAddress, mHost.VerisignPort, mHost.TimeOut);

			CardTender card = FillCardInfo(pRequest.CardData);
			Invoice oInvoice = CreatePaymentInvoice(pRequest); // pRequest.Amount, pRequest.Comments, pRequest.VoucherNo, pRequest.Service, pRequest.DriverID);

			// Create a new Auth Transaction.
			CreditTransaction trans = new CreditTransaction(mUserInfo, connection, oInvoice, card, PayflowUtility.RequestId);
			// Submit the Transaction
			Response response = trans.SubmitTransaction();


			return ProcessResponse(response, pRequest.Amount, TrxTypes.Credit, pRequest.CardData.TestCard);

		}

		public string GetTransactionHistory(DateTime pStart, DateTime pEnd) {
			throw new NotImplementedException();
		}

		public ProcessorResponse AvsCheck(CCRequest pRequest) {
			throw new NotImplementedException();
		}

		public ProcessorResponse ProcessCheck(CheckRequest pRequest) {
			throw new NotImplementedException();
		}

		public ProcessorResponse RefundCheck(CheckRequest pRequest) {
			throw new NotImplementedException();
		}

		public List<CheckStatus> GetCheckTransactionHistory(DateTime pStart, DateTime pEnd) {
			throw new NotImplementedException();
		}
	}

	public class VerisignLogin {
		public string User { get; set; }
		public string Vendor { get; set; }
		public string Partner { get; set; }
		public string Password { get; set; }
	}

	public class VerisignProxy {
		public string ProxyAddress { get; set; }
		public string ProxyLogon { get; set; }
		public string ProxyPassword { get; set; }
		public string ProxyPort { get; set; }
	}

	public class VerisignHost {
		public string HostAddress { get; set; }
		public int VerisignPort { get; set; }
		public int TimeOut { get; set; }
	}


	//public static class Configuration {
	//    public static string ClientId;
	//    public static string ClientSecret;

	//    // Static constructor for setting the readonly static members.
	//    static Configuration() {
	//        //var config = GetConfig();
	//        ClientId = Verisign.mCredentials.UserName; // pClientID;
	//        ClientSecret = Verisign.mCredentials.Password; // pPassword;
	//    }

	//    // Create the configuration map that contains mode and other optional configuration details.
	//    public static Dictionary<string, string> GetConfig() {
	//        return PayPal.Api.ConfigManager.Instance.GetProperties();
	//    }

	//    // Create accessToken
	//    private static string GetAccessToken() {
	//        // ###AccessToken
	//        // Retrieve the access token from
	//        // OAuthTokenCredential by passing in
	//        // ClientID and ClientSecret
	//        // It is not mandatory to generate Access Token on a per call basis.
	//        // Typically the access token can be generated once and
	//        // reused within the expiry window                
	//        string accessToken = new PayPal.Api.OAuthTokenCredential(ClientId, ClientSecret, GetConfig()).GetAccessToken();
	//        return accessToken;
	//    }

	//    // Returns APIContext object
	//    public static PayPal.Api.APIContext GetAPIContext(string accessToken = "") {
	//        // ### Api Context
	//        // Pass in a `APIContext` object to authenticate 
	//        // the call and to send a unique request id 
	//        // (that ensures idempotency). The SDK generates
	//        // a request id if you do not pass one explicitly. 
	//        var apiContext = new PayPal.Api.APIContext(string.IsNullOrEmpty(accessToken) ? GetAccessToken() : accessToken);
	//        apiContext.Config = GetConfig();

	//        // Use this variant if you want to pass in a request id  
	//        // that is meaningful in your application, ideally 
	//        // a order id.
	//        // String requestId = Long.toString(System.nanoTime();
	//        // APIContext apiContext = new APIContext(GetAccessToken(), requestId ));

	//        return apiContext;
	//    }

	//}
}
