﻿using CabRideEngineDapper;
using CabRideEngineDapper.Utils;
using Dapper;
using System.Collections.Generic;
using System.Linq;

namespace CabRideEngineDapperAudit {
    public partial class DriversAudit {

        public static List<DriversAudit> GetDriverHistory(Drivers pDriver) {
            string sql = @"SELECT  *
                            FROM    DriversAudit
                            WHERE   DriverID = @DriverID
                            ORDER BY RowVersion";

            using (var conn = SqlHelper.OpenSqlAuditConnection()) {
                return conn.Query<DriversAudit>(sql, new { DriverID = pDriver.DriverID }).ToList();
            }
        }

    }
}
