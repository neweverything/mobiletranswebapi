﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Collections.Specialized;

using System.ComponentModel;

using IdeaBlade.Persistence;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;
using System.Text;
using TaxiPassCommon;


namespace CabRideEngine {
	[Serializable]
	public sealed class Countries : CountriesDataRow {

		#region Constructors -- DO NOT MODIFY
		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new'
		private Countries()
			: this(null) {
		}

		// Typed DataSet constructor (needed by framework) - DO NOT REMOVE
		public Countries(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static Countries Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      Countries aCountries = (Countries) pManager.CreateEntity(typeof(Countries));
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aCountries, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aCountries.AddToManager();
		//      return aCountries;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		// Add additional logic to your business object here...
		public static Countries Create(PersistenceManager pPM) {
			Countries oCountry = null;

			try {
				// Creates the Country but it is not yet accessible to the application
				oCountry = (Countries)pPM.CreateEntity(typeof(Countries));

				//pPM.GenerateId(emp, Countries.IdEntityColumn);
				oCountry.AddToManager();
			} catch (Exception ex) {
				throw ex;
			}
			return oCountry;
		}

		protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
			if (IsDeserializing)
				return;
			base.OnColumnChanging(pArgs);
			DoAudit(pArgs);
		}

		private void DoAudit(DataColumnChangeEventArgs pArgs) {
			if (!this.PersistenceManager.IsClient) {
				return;
			}
			if (this.RowState == DataRowState.Added) {
				return;
			}
			if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
				AuditRecord = CountriesAudit.Create(this.PersistenceManager, this);
			}
			//AuditRecord[pArgs.Column.ColumnName] = pArgs.ProposedValue;
		}

		// ******************************************************************************
		// return the country (USA) by looking up the Name (United States)
		// ******************************************************************************
		public string FindCountryCode(string pCountryName) {
			EntityList<Countries> oCountries = GetData(this.PersistenceManager, pCountryName, false);
			return oCountries[0].Country;
		}


		// ******************************************************************************
		// Get the data, then sort
		// ******************************************************************************
		public static EntityList<Countries> GetAllCountries(PersistenceManager pPM, string pSortColumnName) {
			EntityList<Countries> oCountries = GetData(pPM, "", false);
			if (pSortColumnName.Length > 0) {
				oCountries.ApplySort(pSortColumnName, ListSortDirection.Ascending, false);
			}
			return oCountries;
		}

		// ******************************************************************************
		// Get the default country
		// ******************************************************************************
		public static Countries GetDefaultCountry(PersistenceManager pPM) {
			return GetData(pPM, "", true)[0];
		}

		// ******************************************************************************
		// Call the proc
		// ******************************************************************************
		private static EntityList<Countries> GetData(PersistenceManager pPM, string pName, bool pDefault) {
			string sDelimiter = "";
			int iArgCount = 0;
			StringBuilder sql = new StringBuilder();
			sql.Append("usp_CountriesGet ");
			if (!pName.IsNullOrEmpty()) {
				sql.AppendFormat(" @Name = '{0}'", pName);
				iArgCount++;
			}
			if (pDefault) {
				if (iArgCount > 0) {
					sDelimiter = ", ";
				}
				sql.AppendFormat("{0}@Default = 1", sDelimiter);
				iArgCount++;
			}
			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(Countries), sql.ToString());
			EntityList<Countries> oCountries = pPM.GetEntities<Countries>(qry);
			if (oCountries.Count == 0) {
				oCountries.Add(new Countries());
			}
			return oCountries;
		}



		public static StringDictionary GetStaticCountries() {

			StringDictionary statCountries = new StringDictionary();
			statCountries.Add("AR", "Argentina");
			statCountries.Add("AT", "Austria");
			statCountries.Add("AU", "Australia");
			statCountries.Add("BE", "Belgium");
			statCountries.Add("BR", "Brazil");
			statCountries.Add("BG", "Bulgaria");
			statCountries.Add("CA", "Canada");
			statCountries.Add("CL", "Chile");
			statCountries.Add("CN", "China");
			statCountries.Add("CO", "Colombia");
			statCountries.Add("CR", "Costa Rica");
			statCountries.Add("HR", "Croatia");
			statCountries.Add("CS", "Czech Republic");
			statCountries.Add("DK", "Denmark");
			statCountries.Add("EC", "Ecuador");
			statCountries.Add("FI", "Finland");
			statCountries.Add("FR", "France");
			statCountries.Add("DE", "Germany");
			statCountries.Add("GR", "Greece");
			statCountries.Add("HK", "Hong Kong");
			statCountries.Add("HU", "Hungary");
			statCountries.Add("IN", "India");
			statCountries.Add("ID", "Indonesia");
			statCountries.Add("IE", "Ireland");
			statCountries.Add("IL", "Israel");
			statCountries.Add("IT", "Italy");
			statCountries.Add("JP", "Japan");
			statCountries.Add("KE", "Kenya");
			statCountries.Add("KR", "Korea");
			statCountries.Add("LU", "Luxembourg");
			statCountries.Add("MY", "Malaysia");
			statCountries.Add("MX", "Mexico");
			statCountries.Add("MA", "Morocco");
			statCountries.Add("NL", "Netherlands");
			statCountries.Add("NZ", "New Zealand");
			statCountries.Add("NG", "Nigeria");
			statCountries.Add("NO", "Norway");
			statCountries.Add("PE", "Peru");
			statCountries.Add("PH", "Philippines");
			statCountries.Add("PL", "Poland");
			statCountries.Add("PT", "Portugal");
			statCountries.Add("PR", "Puerto Rico");
			statCountries.Add("RO", "Romania");
			statCountries.Add("SU", "Russia");
			statCountries.Add("SG", "Singapore");
			statCountries.Add("SK", "Slovak Republic");
			statCountries.Add("ZA", "South Africa");
			statCountries.Add("ES", "Spain");
			statCountries.Add("SA", "Saudi Arabia");
			statCountries.Add("SE", "Sweden");
			statCountries.Add("CH", "Switzerland");
			statCountries.Add("TW", "Taiwan");
			statCountries.Add("TH", "Thailand");
			statCountries.Add("TR", "Turkey");
			statCountries.Add("UA", "Ukraine");
			statCountries.Add("AE", "United Arab Emirates");
			statCountries.Add("GB", "United Kingdom");
			statCountries.Add("USA", "United States");
			statCountries.Add("VE", "Venezuela");
			statCountries.Add("VN", "Vietnam");
			statCountries.Add("YU", "Yugoslavia");

			return statCountries;
		}

		public static DataView GetStaticCountriesView() {
			StringDictionary statCountries = GetStaticCountries();
			DataTable oTable = new DataTable("StaticCountries");

			oTable.Columns.Add("CountryCode", typeof(string));
			oTable.Columns.Add("Name", typeof(string));

			foreach (string key in statCountries.Keys) {
				DataRow oRow = oTable.NewRow();
				oRow["CountryCode"] = key;
				oRow["Name"] = statCountries[key];
				oTable.Rows.Add(oRow);
			}

			DataView oView = oTable.DefaultView;
			oView.Sort = "Name";
			return oView;
		}
	}

}
