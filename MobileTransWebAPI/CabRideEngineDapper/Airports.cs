﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Dapper;
using TaxiPassCommon;

using CabRideEngineDapper.Utils;
using System.Data.SqlClient;

namespace CabRideEngineDapper {
	public partial class Airports {

		public static Airports GetAirport(string pAirportCode) {
			using (var conn = SqlHelper.OpenSqlConnection()) {
                return GetAirport(conn, pAirportCode);
			}
		}

        public static Airports GetAirport(SqlConnection pConn, string pAirportCode) {
				return pConn.Query<Airports>("Select * FROM Airports WHERE Airport = @Airport", new { Airport = pAirportCode }).DefaultIfEmpty(Airports.GetNullEntity()).FirstOrDefault();
        }
    }
}
