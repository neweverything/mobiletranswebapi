﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileTransWebAPI.Models {
	public class DriverSignUpResult {
		public string ErrorMsg { get; set; }

		public string DriverName { get; set; }

	}
}