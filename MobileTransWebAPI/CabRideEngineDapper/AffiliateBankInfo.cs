﻿
using CabRideEngineDapper.Utils;
using Dapper;
using System;
using TaxiPassCommon.Banking;
using TaxiPassCommon.Banking.Enums;
using TaxiPassCommon.Banking.Interfaces;

namespace CabRideEngineDapper {
    public partial class AffiliateBankInfo {


        public override int Save(string pModifiedBy) {
            ModifiedDate = DateTime.UtcNow;
            ModifiedBy = string.Format("{0} | {1}", pModifiedBy, CabRideDapperSettings.GetAppName());
            if (IsNewRecord) {
                this.AffiliateBankID = NextID.GetNextKey(AffiliateBankInfo.TableDef.TABLE_NAME);
            } else {
                RowVersion++;
            }

            return base.Save(pModifiedBy);
        }

        private Affiliate mAffiliate;
        public Affiliate Affiliate {
            get {
                if (mAffiliate == null) {
                    mAffiliate = Affiliate.GetAffiliate(this.AffiliateID);
                }
                return mAffiliate;
            }
        }

        private MerchantInfo mMerchantInfo;
        public MerchantInfo MerchantInfo {
            get {
                if (mMerchantInfo == null) {
                    mMerchantInfo = MerchantInfo.GetMerchant(this.MerchantID);
                }
                return mMerchantInfo;
            }
        }


        public PayInfo GetPayInfo() {
            PayInfo oPay = new PayInfo();
            oPay.NachaFile = this.Affiliate.SendNacha;
            oPay.BankAccountHolder = this.AccountHolder;
            oPay.BankAccountNumber = this.AccountNumber;
            oPay.BankAccountPersonal = this.AccountPersonal;
            oPay.BankAddress = this.Address;
            oPay.BankCity = this.City;
            oPay.BankCode = this.Code;
            oPay.BankState = this.State;
            oPay.BankSuite = this.Suite;
            oPay.BankZIPCode = this.ZIPCode;
            if (oPay.MyMerchant == null) {
                oPay.MyMerchant = new Merchant();
            }
            oPay.MyMerchant.MerchantNo = this.MerchantInfo.MerchantNo;
            oPay.PaidTo = "Affiliate: " + this.Affiliate.Name;

            oPay.WireTransfer = this.Affiliate.ACHWireTransfer;
            return oPay;
        }

        public static AffiliateBankInfo GetByAffiliateID(long pAffiliateID) {
            string sql = @"SELECT * FROM dbo.AffiliateBankInfo
							WHERE AffiliateID = @AffiliateID";
            using (var conn = SqlHelper.OpenSqlConnection()) {
                return conn.Get<AffiliateBankInfo>(sql, new { AffiliateID = pAffiliateID });
            }
        }
    }
}
