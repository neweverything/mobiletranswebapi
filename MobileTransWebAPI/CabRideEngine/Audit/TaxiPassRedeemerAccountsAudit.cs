﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Util;
using IdeaBlade.Persistence.Rdb;

namespace CabRideEngine {
    /// <summary>
    /// The developer class representing the TaxiPassRedeemerAccountsAudit business object class.
    /// </summary>
    /// <remarks>
    /// <para>Place all application-specific business logic in this class.</para>
    /// <para>
    /// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
    /// and will not overwrite changes.
    /// </para>
    /// <para>
    /// This class inherits from the generated class, its companion in the pair of classes 
    /// defining a business object. Properties in the generated class may be overridden in 
    /// order to change the behavior or add business logic.
    /// </para>
    /// <para>
    /// This class is the final class in the inheritance chain for this business object. 
    /// It should not be inherited.
    /// </para>
    /// </remarks>
    [Serializable]
    public sealed partial class TaxiPassRedeemerAccountsAudit : TaxiPassRedeemerAccountsAuditDataRow {

        #region Constructors -- DO NOT MODIFY

        // Do not create constructors for this class
        // Developers cannot create instances of this class with the "new" keyword
        // You should write a static Create method instead; see the "Suggested Customization" region
        // Please review the documentation to learn about entity creation and inheritance

        // This private constructor prevents accidental instance creation via 'new';
        // it is also required if the class is visible from a web service.
        // DO NOT REMOVE **
        private TaxiPassRedeemerAccountsAudit()
            : this(null) {
        }

        /// <summary>
        /// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
        /// </summary>
        /// <param name="pRowBuilder"></param>
        public TaxiPassRedeemerAccountsAudit(DataRowBuilder pRowBuilder)
            : base(pRowBuilder) {
        }

        #endregion

        #region Suggested Customizations

        //    // Use this factory method template to create new instances of this class
        //    public static TaxiPassRedeemerAccountsAudit Create(PersistenceManager pManager,
        //      ... your creation parameters here ...) { 
        //
        //      TaxiPassRedeemerAccountsAudit aTaxiPassRedeemerAccountsAudit = pManager.CreateEntity<TaxiPassRedeemerAccountsAudit>();
        //
        //      // if this object type requires a unique id and you have implemented
        //      // the IIdGenerator interface implement the following line
        //      pManager.GenerateId(aTaxiPassRedeemerAccountsAudit, // add id column here //);
        //
        //      // Add custom code here
        //
        //      aTaxiPassRedeemerAccountsAudit.AddToManager();
        //      return aTaxiPassRedeemerAccountsAudit;
        //    }

        //    // Implement this method if you want your object to sort in a specific order
        //    public override int CompareTo(Object pObject) {
        //    }

        //    // Implement this method to customize the null object version of this class
        //    protected override void UpdateNullEntity() {
        //    }

        #endregion

        // Add additional logic to your business object here...
        public static TaxiPassRedeemerAccountsAudit Create(PersistenceManager pManager, TaxiPassRedeemerAccounts pTaxiPassRedeemersAccounts) {
            TaxiPassRedeemerAccountsAudit oAudit = pManager.CreateEntity<TaxiPassRedeemerAccountsAudit>();
            pManager.GenerateId(oAudit, TaxiPassRedeemerAccountsAudit.TaxiPassRedeemerAccountHistoryIDEntityColumn);

            for (int i = 0; i < pTaxiPassRedeemersAccounts.Table.Columns.Count; i++) {
                if (oAudit.Table.Columns.Contains(pTaxiPassRedeemersAccounts.Table.Columns[i].ColumnName)) {
                    oAudit[pTaxiPassRedeemersAccounts.Table.Columns[i].ColumnName] = pTaxiPassRedeemersAccounts[i];
                }
            }
            oAudit.AddToManager();

            return oAudit;
        }


        public static EntityList<TaxiPassRedeemerAccountsAudit> GetRedeemer(TaxiPassRedeemerAccounts pRedeemerAcct) {
            RdbQuery qry = new RdbQuery(typeof(TaxiPassRedeemerAccountsAudit), TaxiPassRedeemerAccountsAudit.TaxiPassRedeemerAccountIDEntityColumn, EntityQueryOp.EQ, pRedeemerAcct.TaxiPassRedeemerAccountID);
            qry.AddOrderBy(RowVersionEntityColumn);
            return pRedeemerAcct.PersistenceManager.GetEntities<TaxiPassRedeemerAccountsAudit>(qry);
        }

        public static EntityList<TaxiPassRedeemerAccountsAudit> GetRedeemer(PersistenceManager pPM, string pRedeemerName) {
            RdbQuery qry = new RdbQuery(typeof(TaxiPassRedeemerAccountsAudit), TaxiPassRedeemerAccountsAudit.CompanyNameEntityColumn, EntityQueryOp.EQ, pRedeemerName);
            qry.AddOrderBy(RowVersionEntityColumn);
            return pPM.GetEntities<TaxiPassRedeemerAccountsAudit>(qry);
        }

        public static TaxiPassRedeemerAccountsAudit GetByPayCardAdminNo(PersistenceManager pPM, string pPayCard) {
            if (string.IsNullOrEmpty(pPayCard)) {
                return pPM.GetNullEntity<TaxiPassRedeemerAccountsAudit>();
            }

            RdbQuery qry = new RdbQuery(typeof(TaxiPassRedeemerAccountsAudit), TaxiPassRedeemerAccountsAudit.TransCardAdminNoEntityColumn, EntityQueryOp.EQ, pPayCard);
            qry.AddClause(TaxiPassRedeemerAccounts.ActiveEntityColumn, EntityQueryOp.EQ, true);
            return pPM.GetEntity<TaxiPassRedeemerAccountsAudit>(qry, QueryStrategy.DataSourceThenCache);
        }

        public static TaxiPassRedeemerAccountsAudit GetByBankInfo(PersistenceManager pPM, string pRouting, string pAccountNo) {
            RdbQuery qry = new RdbQuery(typeof(TaxiPassRedeemerAccountsAudit), TaxiPassRedeemerAccountsAudit.BankCodeEntityColumn, EntityQueryOp.EQ, pRouting);
            qry.AddClause(TaxiPassRedeemerAccountsAudit.BankAccountNumberEntityColumn, EntityQueryOp.EQ, pAccountNo);
            qry.AddOrderBy(TaxiPassRedeemerAccountsAudit.RowVersionEntityColumn, System.ComponentModel.ListSortDirection.Descending);
            return pPM.GetEntity<TaxiPassRedeemerAccountsAudit>(qry);
        }
    }

    #region EntityPropertyDescriptors.TaxiPassRedeemerAccountsAuditPropertyDescriptor
    ////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
    ////    For example: 
    ////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
    //public static partial class EntityPropertyDescriptors {
    //	public partial class TaxiPassRedeemerAccountsAuditPropertyDescriptor : AdaptedPropertyDescriptor {
    //    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
    //    public AdaptedPropertyDescriptor Age {
    //      get { return this.Get("Age"); }
    //    }
    //  }
    //} 
    #endregion

}
