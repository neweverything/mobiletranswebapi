﻿using IdeaBlade.Persistence;
using System;
using System.Data;
using System.Linq;

namespace CabRideEngine {
	/// <summary>
	/// The developer class representing the NachaDefaults business object class.
	/// </summary>
	/// <remarks>
	/// <para>Place all application-specific business logic in this class.</para>
	/// <para>
	/// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
	/// and will not overwrite changes.
	/// </para>
	/// <para>
	/// This class inherits from the generated class, its companion in the pair of classes 
	/// defining a business object. Properties in the generated class may be overridden in 
	/// order to change the behavior or add business logic.
	/// </para>
	/// <para>
	/// This class is the final class in the inheritance chain for this business object. 
	/// It should not be inherited.
	/// </para>
	/// </remarks>
	[Serializable]
	public sealed partial class NachaDefaults : NachaDefaultsDataRow {

		#region Constructors -- DO NOT MODIFY

		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new';
		// it is also required if the class is visible from a web service.
		// DO NOT REMOVE **
		private NachaDefaults() : this(null) { }

		/// <summary>
		/// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
		/// </summary>
		/// <param name="pRowBuilder"></param>
		public NachaDefaults(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static NachaDefaults Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      NachaDefaults aNachaDefaults = pManager.CreateEntity<NachaDefaults>();
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aNachaDefaults, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aNachaDefaults.AddToManager();
		//      return aNachaDefaults;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		// Add additional logic to your business object here...
		private static NachaDefaults Create(PersistenceManager pManager) {

			NachaDefaults rec = pManager.CreateEntity<NachaDefaults>();

			//pManager.GenerateId(rec, NachaDefaults.NachaDefaultIDEntityColumn);
			rec.AddToManager();
			return rec;
		}

		public static NachaDefaults GetDefaults(PersistenceManager pPM) {
			EntityList<NachaDefaults> list = pPM.GetEntities<NachaDefaults>();
			if (list.Count == 0) {
				NachaDefaults rec = Create(pPM);
				return rec;
			}
			return list[0];
		}

		public static NachaDefaults GetDefault(PersistenceManager pPM, bool pNewFundingAccount) {
			EntityList<NachaDefaults> list = pPM.GetEntities<NachaDefaults>();
			if (list.Count == 0) {
				NachaDefaults rec = Create(pPM);
				return rec;
			}

			if (list.Count == 1) {
				return list[0];
			}

			bool test = false;
			if (test) {
				return list[2];
			}

			return pNewFundingAccount ? list.FirstOrDefault(p => p.NachaDefaultID != 1 && p.StartDate < DateTime.Today && (!p.EndDate.HasValue || p.EndDate >= DateTime.Today)) : list.FirstOrDefault(p => p.NachaDefaultID == 1);
		}

		protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
			if (IsDeserializing)
				return;
			base.OnColumnChanging(pArgs);
			//DoAudit(pArgs);
		}

		private void DoAudit(DataColumnChangeEventArgs pArgs) {
			if (!this.PersistenceManager.IsClient) {
				return;
			}
			if (this.RowState == DataRowState.Added) {
				return;
			}
			if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
				AuditRecord = NachaDefaultsAudit.Create(this.PersistenceManager, this);
			}
		}

		public static NachaDefaults GetDefault() {
			throw new NotImplementedException();
		}
	}

	#region EntityPropertyDescriptors.NachaDefaultsPropertyDescriptor
	////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
	////    For example: 
	////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
	//public static partial class EntityPropertyDescriptors {
	//	public partial class NachaDefaultsPropertyDescriptor : BaseEntityPropertyDescriptor {
	//    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
	//    public AdaptedPropertyDescriptor Age {
	//      get { return this.Get("Age"); }
	//    }
	//  }
	//} 
	#endregion

}