﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

using Dapper;
using TaxiPassCommon;


namespace CabRideEngineDapper {
    public partial class ReservationDrivers {

        public static List<ReservationDrivers> GetReservationDriver(SqlConnection pConn, long pReservationID) {
            string sql = @"select * from ReservationDrivers with (nolock) where  ReservationID = @ReservationID";
            return pConn.Query<ReservationDrivers>(sql, new { ReservationID = pReservationID }).ToList();
        }

        public static ReservationDrivers GetReservationDriver(SqlConnection pConn, long pReservationID, long pDriverID) {
            string sql = @"SELECT  *
                            FROM    ReservationDrivers WITH (NOLOCK)
                            WHERE   ReservationID = @ReservationID AND DriverID = @DriverID";

            return pConn.Query<ReservationDrivers>(sql, new { ReservationID = pReservationID, DriverID = pDriverID }).DefaultIfEmpty(ReservationDrivers.GetNullEntity()).FirstOrDefault();
        }
    }
}
