using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace MobileTransWebAPI.Models {
	
	public partial class ValidateVoucherRequest {
		public string VoucherNo { get; set; }
		public string CardNo { get; set; }
		public string RequestKey { get; set; }
	}
}