﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Dapper;
using TaxiPassCommon;

using CabRideEngineDapper.Utils;

namespace CabRideEngineDapper {
	public partial class RedeemerShift {


		#region Related Tables

		private TaxiPassRedeemers mShiftManager;
		public TaxiPassRedeemers ShiftManager {
			get {
				if (!this.ShiftManagerID.HasValue) {
					return TaxiPassRedeemers.GetNullEntity();
				}
				if (mShiftManager == null) {
					mShiftManager = TaxiPassRedeemers.GetRedeemer(this.ShiftManagerID);
				}
				return mShiftManager;
			}
		}

		private ShiftName mShiftName;
		public ShiftName ShiftName {
			get {
				if (!this.ShiftTypeID.HasValue) {
					return ShiftName.GetNullEntity();
				}
				if (mShiftName == null) {
					mShiftName = ShiftName.GetShift(this.ShiftTypeID.Value);
				}
				return mShiftName;
			}
		}
			


		#endregion

		public static RedeemerShift Create(TaxiPassRedeemers pRedeemer) {
			RedeemerShift rec = null;

			try {
				rec = GetLastShiftByRedeemer(pRedeemer);
				if (rec.StartTime.Date != DateTime.Now.Date) {
					TimeZoneConverter tzConvert = new TimeZoneConverter();
					DateTime now = tzConvert.ConvertTime(DateTime.Now, TimeZone.CurrentTimeZone.StandardName, pRedeemer.TaxiPassRedeemerAccounts.Markets.TimeZone);

					rec = RedeemerShift.Create();
					rec.TaxiPassRedeemerID = pRedeemer.TaxiPassRedeemerID;
					rec.StartTime = DateTime.Now;
				}

			} catch (Exception ex) {
				throw ex;
			}
			return rec;
		}


		public static RedeemerShift GetCurrrentShiftManager(TaxiPassRedeemers pRedeemer) {
			string sql = @"SELECT * FROM RedeemerShift
							WHERE TaxiPassRedeemerID IN (
									SELECT TaxiPassRedeemerID FROM TaxiPassRedeemers
									WHERE ShiftManager = 1
										AND TaxiPassRedeemerAccountID = @TaxiPassRedeemerAccountID
									)
								AND Closed = 0";

			using (var conn = SqlHelper.OpenSqlConnection()) {
				return conn.Query<RedeemerShift>(sql, new { TaxiPassRedeemerAccountID = pRedeemer.TaxiPassRedeemerAccountID }).DefaultIfEmpty(RedeemerShift.GetNullEntity()).FirstOrDefault();
			}
		}

		public static RedeemerShift GetShift(long pShiftID) {
			using (var conn = SqlHelper.OpenSqlConnection()) {
				return conn.Get<RedeemerShift>(pShiftID);
			}
		}

		public static RedeemerShift GetLastShiftByRedeemer(TaxiPassRedeemers pRedeemer) {
			string sql = @"SELECT * FROM RedeemerShift 
							WHERE TaxiPassRedeemerID =  @TaxiPassRedeemerID
									AND Closed = 0
									AND StartTime >= @Start 
							ORDER BY StartTime Desc";
			using (var conn = SqlHelper.OpenSqlConnection()) {
				return conn.Query<RedeemerShift>(sql, new { TaxiPassRedeemerID = pRedeemer.TaxiPassRedeemerID, Start = DateTime.Today.AddDays(-1) }).DefaultIfEmpty(RedeemerShift.GetNullEntity()).FirstOrDefault();
			}
		}


		public static List<RedeemerShift> GetShiftsForRedeemerByDate(TaxiPassRedeemers pRedeemer, DateTime pDate) {

			string sql = @"SELECT TOP 1 redeemershift.* FROM ATMAccess
							LEFT JOIN redeemershift ON ATMAccess.taxipassredeemerid = redeemershift.taxipassredeemerid
								AND ATMAccess.TransactionDate BETWEEN redeemershift.StartTime
									AND redeemershift.EndTime
							LEFT JOIN shiftname ON shiftname.shiftnameid = redeemershift.shifttypeid
							WHERE ATMAccess.taxipassredeemerid = @TaxiPassRedeemerID
								AND ATMAccess.TransactionDate BETWEEN @Start
									AND @End

							UNION

							SELECT redeemershift.* FROM redeemershift
							WHERE redeemershift.TaxiPassRedeemerID = @TaxiPassRedeemerID
								AND redeemershift.StartTime BETWEEN @Start
									AND @End";

			DynamicParameters param = new DynamicParameters();
			param.Add("TaxiPassRedeemerID", pRedeemer.TaxiPassRedeemerID);
			param.Add("Start", pDate.Date);
			param.Add("End", pDate.Date.AddDays(1));
			using (var conn = SqlHelper.OpenSqlConnection()) {
				return conn.Query<RedeemerShift>(sql, param).ToList();
			}
		}



	}
}
