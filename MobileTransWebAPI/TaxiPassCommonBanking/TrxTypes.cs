﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TaxiPassCommon.Banking {
	public static class TrxTypes {
		public const string Authorization = "A";
		public const string Sale = "S";
		public const string Credit = "C";
		public const string Void = "V";
		public const string VoiceAuthorization = "F";
		public const string Inquiry = "I";
		public const string DelayedCapture = "D";
        public const string OpenCredit = "O";

        public static string GetTrxDescription(string pTrxType) {
			string sText = "";
			switch (pTrxType) {
				case TrxTypes.Authorization:
					sText = "Authorization";
					break;
				case TrxTypes.Sale:
					sText = "Sale";
					break;
				case TrxTypes.Credit:
					sText = "Credit";
					break;
				case TrxTypes.Void:
					sText = "Void";
					break;
				case TrxTypes.VoiceAuthorization:
					sText = "Voice Authorization";
					break;
				case TrxTypes.Inquiry:
					sText = "Inquiry";
					break;
				case TrxTypes.DelayedCapture:
					sText = "Sale";
					break;
			}
			return sText;
		}
	}
}
