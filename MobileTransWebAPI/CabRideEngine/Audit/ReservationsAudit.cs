﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

namespace CabRideEngine {
    [Serializable]
    public sealed class ReservationsAudit : ReservationsAuditDataRow {

        #region Constructors -- DO NOT MODIFY
        // Do not create constructors for this class
        // Developers cannot create instances of this class with the "new" keyword
        // You should write a static Create method instead; see the "Suggested Customization" region
        // Please review the documentation to learn about entity creation and inheritance

        // This private constructor prevents accidental instance creation via 'new'
        private ReservationsAudit()
            : this(null) {
        }

        // Typed DataSet constructor (needed by framework) - DO NOT REMOVE
        public ReservationsAudit(DataRowBuilder pRowBuilder)
            : base(pRowBuilder) {
        }

        #endregion

        #region Suggested Customizations

        //    // Use this factory method template to create new instances of this class
        //    public static ReservationsAudit Create(PersistenceManager pManager,
        //      ... your creation parameters here ...) { 
        //
        //      ReservationsAudit aReservationsAudit = pManager.CreateEntity<ReservationsAudit>();
        //
        //      // if this object type requires a unique id and you have implemented
        //      // the IIdGenerator interface implement the following line
        //      pManager.GenerateId(aReservationsAudit, // add id column here //);
        //
        //      // Add custom code here
        //
        //      aReservationsAudit.AddToManager();
        //      return aReservationsAudit;
        //    }

        //    // Implement this method if you want your object to sort in a specific order
        //    public override int CompareTo(Object pObject) {
        //    }

        //    // Implement this method to customize the null object version of this class
        //    protected override void UpdateNullEntity() {
        //    }

        #endregion

        // Add additional logic to your business object here...
        public static ReservationsAudit Create(PersistenceManager pManager, Reservations pReservation) {
            ReservationsAudit oAudit = (ReservationsAudit)pManager.CreateEntity(typeof(ReservationsAudit));
            //pManager.GenerateId(oAudit, ReservationsAudit.ReserveHistoryIDEntityColumn);

            for (int i = 0; i < pReservation.Table.Columns.Count; i++) {
                if (oAudit.Table.Columns.Contains(pReservation.Table.Columns[i].ColumnName)) {
                    oAudit[pReservation.Table.Columns[i].ColumnName] = pReservation[i];
                }
            }
            oAudit.AddToManager();

            return oAudit;
        }

        public static ReservationsAudit GetFirstReserveRecord(PersistenceManager pPM, long pReserveID) {
            RdbQuery oQry = new RdbQuery(typeof(ReservationsAudit), ReservationsAudit.ReservationIDEntityColumn, EntityQueryOp.EQ, pReserveID);
            oQry.AddClause(ReservationsAudit.RowVersionEntityColumn, EntityQueryOp.EQ, 0);
            //oQry.AddOrderBy(ReservationsAudit.ReserveHistoryIDEntityColumn, System.ComponentModel.ListSortDirection.Descending);
            return pPM.GetEntity<ReservationsAudit>(oQry);
        }

        public string PickUpInfoShort {
            get {
                RoutingAudit oRoute = RoutingAudit.GetLastPickUpRouting(this.PersistenceManager, this.ReservationID);
                return oRoute.FormattedAddressShort;
            }
        }

        public string PickUpNote {
            get {
                RoutingAudit oRoute = RoutingAudit.GetLastPickUpRouting(this.PersistenceManager, this.ReservationID);
                return oRoute.Notes;
            }
        }

        public string PickUpDateString {
            get {
                if (PickUpDate == null) {
                    return "";
                }
                return PickUpDate.Value.ToShortDateString();
            }
        }

        public string PickUpTimeString {
            get {
                if (PickUpDate == null) {
                    return "";
                }
                return PickUpDate.Value.ToShortTimeString();
            }
        }
    }

}
