﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

using Dapper;
using TaxiPassCommon;

using CabRideEngineDapper.Utils;

namespace CabRideEngineDapper {
	public partial class DriverStatus {

		public static DriverStatus GetDriverStatus(long pDriverStatusID) {
			string sql = "SELECT * FROM DriverStatus WITH (NOLOCK) WHERE DriverStatusID = @DriverStatusID";
			using (var conn = SqlHelper.OpenSqlConnection()) {
				return conn.Query<DriverStatus>(sql, new { DriverStatusID = pDriverStatusID }).DefaultIfEmpty(DriverStatus.GetNullEntity()).FirstOrDefault();
			}
		}

		public static List<DriverStatus> GetAllForDropDowns(SqlConnection pConn) {
			string sql = @"SELECT DriverStatusID, STATUS FROM DriverStatus WITH (NOLOCK)
							ORDER BY Status";

			return pConn.Query<DriverStatus>(sql).ToList();
		}
	}
}
