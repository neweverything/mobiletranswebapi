﻿using CabRideEngineDapper;
using CabRideEngineDapper.Utils;
using MobileTransWebAPI.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Http;
using TaxiPassCommon;

namespace MobileTransWebAPI.Controllers {

	public class PreAuthTipDapperController : ApiController {

		private int mExpiryYear = 0;
		private int mExpiryMonth = 0;
		private string mCardNo = "";


		public ChargeResult Get() {
			string temp = "399C8D7B93C96AA99741B4A8C7A7DD0190184AC6E5E73A0FBBF2DF24CB35791A8C44FF834F32649761484D9C3F7C686A87E1A8AE47B870968B70A94BAF79FCED8D9029A8796C3A33425C459CFC9F518210A33153B23522F4C9021960B01DE480C5A606401DCA8425392E3779DB035B10C2B7691CBBB47BA07214539C90DB4D7A8CDA125E2AA2AD241C6E367501BF5105E9C64C8328499D19CB05608C85E1B60CB090B94C69CFDBC3166524AE7F47158E58549CC6F2D2773613DAA8CE553354FF40BD54252249797336339B7FA635C0FE79A29498B260EEA46000BB7E2D742743A85FE9A6D26FBE117E737033842E5EA9B82F1C95FDF776EA6B02E1162BB9EDEB1CD4A3A6CD066B19E2750BD45613A9E08A8C7288A55A5C1E1EB426D2B1E6F3B14B81F1006F7C36862DA927AF01BA0D33FBD68C144AA8B20B39822445238747D39C89462F405E88F996108B0DD0B3918071018932985D7E7C33E493EF226F9A226802B62C42DA3550D72A17BCC5F91890857BEBE1D028051269135456D9427E9C805C2E7E6AE4C1E7087EF0CEA2AD9D07D1000BF9B23870CC69135456D9427E9C5EEE3E78F5921D56587EDB7BD237DFF8DDA33201FFFD9B3CA4C6C9DDA7A9B4760CC599EC17EDFED18E71B49DB32F5DA22369F76E72248F47E1F901C2A676A322FA4913B0D01C0BFB32F541E2497B3B672653B9E689843446189482BFB922547324E650A96715BE883C5CA249931F295615F5882363FB3A6D980E3D9E81C7493A69135456D9427E9C8EE4E497CE5527788577A47FC98B8BD4589FC7B3BDCC11AE37335E70168B280ECB05608C85E1B60CD526D3624467B7D91DF2ED3924206495F8DAB0EEF1E5AE24C11DBBC23CD794FF7A3B8BB3586C76E1589FC7B3BDCC11AE9F2A762801AE288CCB05608C85E1B60CBB4E9364E73F9B7828F11FEA7AB9CCF93376973FEBEFC497E1B1AD5E73D0C8387C514FAD16AA1A525344F424F70C98880A6977DAF9CBBDB2096A15B9F4E450E035D1F6FB3D13B4E09BF674AB85D1B4D5BA83F076028D91CAF15A629526A4631B12F28CF23096D839E95787C57C6BB767A3B207E0E9DFE0D8112EF94526B54EDC8F697F088B92DB21C40E68F76BBB238D241D9FF846A0E7651D2EE445D284743ACB05608C85E1B60C519152D1C1EF7691BD387336B9B5FF974E5FB3E751AE6217E7495DCFC0ADADBEDF7825B4C96C6272309DE15FE4787B9A00DC59A78E8690B3C92D69B9161D5854E6054DEE3266A78BF54C36904D386AD5B353E215C80BAD6DCD289637632A7780047DACFFBCC3D66DBA6EABAA828C85806B52A46243A0784D297E1F83ABA13B1F6B4951CA32CD8F32519152D1C1EF7691AC8468530A779F6E2959EBB60351D017951330D8D362E83666E0DFAF1174F78D4F119F76E873D26FCB05608C85E1B60C1DD09B48C8A218F4DB0C4CCC110532E6933828E7864B9F4FB2247CF19D5CA1321D543F9178F48589667DE621583D501CB738FC925AB305F73652611AF3DF23777852936480F1EEDB71018932985D7E7CCA01B95CCEEF1429F4A0B24EA02CCFEF1CE454F7554F8D7F204A3435A22E254D46BDDA2C78510781271E7FEE2325BF56E06DC12440BD5ED1D9E21B11683C1BA2F38237D7735C68BA425C459CFC9F5182FDD70DD15558035518B22D27E67645018B70A94BAF79FCED5E1FB80ADE88C7A29C932984".DecryptIceKey();

			temp = File.ReadAllText(@"d:\temp\t.json");
			ChargeInfo info = JsonConvert.DeserializeObject<ChargeInfo>(temp);
			//info.RequestTime = DateTime.Now;
			return PreAuthCard(info);
		}

		public ChargeResult PreAuthCard(ChargeInfo pChargeInfo) {
			ChargeResult oChargeResult = new ChargeResult();

			//WebApiApplication.LogIt("PreAuthTipDapper", JsonConvert.SerializeObject(pChargeInfo).EncryptIceKey());
			WebApiApplication.LogIt(pChargeInfo.Platform, JsonConvert.SerializeObject(pChargeInfo), pChargeInfo.Platform + "PreAuthTip_{0}.txt");
			using (var conn = SqlHelper.OpenSqlConnection()) {
				SystemDefaults defaults = SystemDefaults.GetDefaults(conn);

				if (pChargeInfo.Platform != Platforms.EZRide.ToString()) {
					DateTime requestDate = Convert.ToDateTime(pChargeInfo.RequestTime);
					if (WebApiApplication.GetUTCServerTime().Subtract(requestDate).TotalMinutes > 1) {
						oChargeResult.ErrorMsg = "Invalid Access";
						return oChargeResult;
					}
				}

				if (pChargeInfo.CardNo.IsNullOrEmpty()) {
					if (!pChargeInfo.CardToken.IsNullOrEmpty()) {
						var cardInfo = CustomerCards.GetCardByToken(conn, pChargeInfo.CardToken, -1);
						if (!cardInfo.IsNullEntity) {
							pChargeInfo.CardNo = cardInfo.DecryptCard();
							pChargeInfo.CardNumberDisplay = cardInfo.CardNumberDisplay;
							pChargeInfo.CardHolder = cardInfo.NameDecrypted;
							//pChargeInfo.CardType = cardInfo.CardType;
							if (pChargeInfo.CVV.IsNullOrEmpty()) {
								pChargeInfo.CVV = cardInfo.Verification.DecryptIceKey();
							}
							pChargeInfo.ExpMonth = cardInfo.Expiration.Value.Month;
							pChargeInfo.ExpYear = cardInfo.Expiration.Value.Year;
						}
					}
				}

				List<string> testCards = defaults.TreatAsTestCards.DecryptIceKey().Split('|').ToList();

				if (!pChargeInfo.SwipeData.IsNullOrEmpty()) {
					string swipeData = pChargeInfo.SwipeData;
					if (pChargeInfo.homeATMEncrypted) {
						swipeData = pChargeInfo.DecryptHomeATMSwipe();
					}
					if (swipeData.IsNullOrEmpty()) {
						pChargeInfo.ErrorMsg = "Could not decode HomeATM swipe data";

					} else {

						if (testCards.FirstOrDefault(p => swipeData.Contains(p)) != null) {
							swipeData = string.Format(";4111111111111111={0}121010000047520001?", (DateTime.Today.Year + 2).ToString().Right(2));
						}
						if (swipeData.StartsWith("%B") && swipeData.Contains("?")) {
							swipeData = swipeData.Left(swipeData.IndexOf("?") + 1);
						}
						if (setSwipedCard(swipeData)) {
							pChargeInfo.CardNo = mCardNo; // swipeData.Split('=')[0].Replace(";", "");
							pChargeInfo.ExpMonth = mExpiryMonth;
							pChargeInfo.ExpYear = mExpiryYear;
						}
						pChargeInfo.SwipeData = swipeData;
					}


					if (pChargeInfo.SwipeData.StartsWith(";%")) {
						pChargeInfo.SwipeData = pChargeInfo.SwipeData.Substring(1);
					}
				}

				// Driver/Device lookup
				Drivers oDriver;
				if (pChargeInfo.DriverID > 0) {
					oDriver = Drivers.GetDriver(conn, pChargeInfo.DriverID);
				} else if (pChargeInfo.DeviceID.IsNullOrEmpty()) {
					Affiliate oAff = Affiliate.GetAffiliate(conn, pChargeInfo.AffiliateID);
					oDriver = Drivers.GetChargeCard(oAff);
				} else {
					oDriver = Drivers.GetDriverPhoneSubNoOrSerial(conn, pChargeInfo.AffiliateID, pChargeInfo.DeviceID);
				}
				if (oDriver.IsNullEntity) {
					oChargeResult.ErrorMsg = "Unknown Device ID '" + pChargeInfo.DeviceID + "'";
					return oChargeResult;
				}


				AffiliateDrivers actualDriver = AffiliateDrivers.GetNullEntity();
				if (pChargeInfo.AffDriverID > 0) {
					actualDriver = AffiliateDrivers.GetDriver(conn, pChargeInfo.AffDriverID);
					if (actualDriver.IsNullEntity) {
						oChargeResult.ErrorMsg = "Driver does not exist";
						return oChargeResult;
					} else {
						if (actualDriver.AffiliateID != oDriver.AffiliateID) {
							oChargeResult.ErrorMsg = "Error Driver and Device are not configured correctly";
							return oChargeResult;
						}
					}
				}


				decimal tipAmt = 0;
				if (oDriver.MyAffiliate.AffiliateDriverDefaults.AutoTipPercent > 0) {
					tipAmt = Math.Round(pChargeInfo.Fare * (oDriver.MyAffiliate.AffiliateDriverDefaults.AutoTipPercent / 100), 2);
				} else {
					tipAmt = Math.Round(pChargeInfo.Fare * 0.35M, 2);
				}


				// Create the DriverPayment record
				DriverPayments oPayment = DriverPayments.Create(oDriver);

				oPayment.GetSetCardNumber = pChargeInfo.CardNo;
				oPayment.GetSetExpiration = pChargeInfo.CardExpires;
				oPayment.ReferenceNo = pChargeInfo.ReferenceNo;

				if (oPayment.Affiliate.AffiliateDriverDefaults.DebitFeeProcessing.GetValueOrDefault(false)) {
					// check if card is Debit
					string binCardNo = oPayment.DecryptCardNumber();
					CardBin bin = CardBin.GetCardBinInfo(conn, binCardNo);
					oPayment.IsDebitCard = ("" + bin.DebitCredit).Equals("DEBIT", StringComparison.CurrentCultureIgnoreCase);

				}

				oPayment.SetFare = pChargeInfo.Fare;
				oPayment.AuthAmount = pChargeInfo.Fare + tipAmt + oPayment.TaxiPassFee;

				if (!pChargeInfo.CVV.IsNullOrEmpty()) {
					oPayment.CVV = pChargeInfo.CVV;
				}
				if (!pChargeInfo.BillingZipCode.IsNullOrEmpty()) {
					oPayment.CardZIPCode = pChargeInfo.BillingZipCode;
				} else if (!pChargeInfo.ZipCode.IsNullOrEmpty()) {
					oPayment.CardZIPCode = pChargeInfo.ZipCode;
				}

				oPayment.AffiliateID = oDriver.AffiliateID;


				if (!actualDriver.IsNullEntity) {
					oPayment.AffiliateDriverID = actualDriver.AffiliateDriverID;
				}
				if (!pChargeInfo.VehicleNo.IsNullOrEmpty()) {
					Vehicles oVehicle = Vehicles.GetVehicle(conn, long.Parse(pChargeInfo.VehicleNo));
					if (!oVehicle.IsNullEntity) {
						oPayment.VehicleID = oVehicle.VehicleID;
					}
				}

				if (oPayment.Affiliate.AffiliateDriverDefaults.TPFeeOnFareOnly) {
					oPayment.TaxiPassFee = DriverPayments.CalcFee(oPayment.Affiliate, pChargeInfo.Fare);
				}

				oPayment.Platform = pChargeInfo.Platform;
				if (oPayment.Platform.IsNullOrEmpty()) {
					oPayment.Platform = oPayment.Affiliate.DefaultPlatform;
				}
				oPayment.Save();

				oPayment.CardSwiped = false;
				if (!pChargeInfo.SwipeData.IsNullOrEmpty()) {
					oPayment.CardSwiped = (pChargeInfo.SwipeData.Trim().Length > 0);
				}
				DriverPaymentResults oResult = null;

				// Pre-auth the credit card
				try {
					//lock (thisLock) {
					if (oPayment.CardSwiped) {
						oResult = oPayment.PreAuthCard(pChargeInfo.SwipeData);
					} else {
						oResult = oPayment.PreAuthCard();
					}
					//}
					oChargeResult.ErrorCode = oResult.Result;
					if (oResult.Result != "0") {
						oChargeResult.AVSResultCode = oResult.AVSResultCode;
						oChargeResult.CVVResultCode = oResult.CVV2Match;
						oChargeResult.ErrorMsg = "Auth Failed: " + oResult.Message;
						return oChargeResult;
					} else {
						if (!pChargeInfo.CardToken.IsNullOrEmpty()) {
							var cardInfo = CustomerCards.GetCardByToken(conn, pChargeInfo.CardToken, -1);
							cardInfo.CardExpiryAsMMYY = string.Format("{0}{1}", pChargeInfo.ExpMonth.ToString().PadLeft(2, '0'), pChargeInfo.ExpYear);
							cardInfo.Verification = pChargeInfo.CVV.Encrypt();
							cardInfo.Save();
						}
					}
					oChargeResult.Fare = oPayment.Fare;
					oChargeResult.TPFee = oPayment.TaxiPassFee;
					oChargeResult.AuthCode = oResult.AuthCode;
					oChargeResult.VoucherNo = oResult.DriverPayments.TransNo;
					oChargeResult.AuthDate = oResult.DriverPayments.AuthDate.Value.ToString("MM/dd/yyyy HH:mm");
					oChargeResult.IsTestCard = oResult.DriverPayments.Test;

					oChargeResult.AVSResultCode = oResult.AVSResultCode;
					oChargeResult.CVVResultCode = oResult.CVV2Match;
					oChargeResult.CardNo = oPayment.CardNumberDisplay;
					oChargeResult.TransType = oResult.TransType;
					oChargeResult.DriverPaymentID = oPayment.DriverPaymentID;
					oChargeResult.CardHolder = oPayment.CardHolder.Decrypt();
					//if (oResult.DriverPayments.Test) {
					//	oChargeResult.ErrorMsg = "TEST TRANSACTION";
					//}

				} catch {
					if (pChargeInfo.Platform.Equals(Platforms.EZRide.ToString())) {
						oChargeResult.ErrorMsg = oPayment.DriverPaymentNotes.Reason;
					} else {
						oChargeResult.ErrorMsg = SystemDefaults.GetDefaults(conn).DriverFraudMessage;
					}
					return oChargeResult;
				}
			}

			return oChargeResult;
		}


		private bool setSwipedCard(string pValue) {
			try {
				pValue = pValue.Trim();


				if (pValue.Equals("exit", StringComparison.CurrentCultureIgnoreCase)) {
					return true;
				}

				//fix swipe data 
				if ((pValue.StartsWith(";") || pValue.StartsWith("%B")) && pValue.Contains("?")) {
					pValue = pValue.Left(pValue.IndexOf("?") + 1);
				}

				if ((pValue.StartsWith(";%") || pValue.StartsWith("%B")) && pValue.EndsWith("?") && pValue.IndexOf("=") > 0) {
					String workData = pValue;
					int startPos = pValue.IndexOf("?") + 1;
					pValue = pValue.Substring(startPos);
					int endPos = pValue.IndexOf("=");
					mCardNo = pValue.Left(endPos);
					if (mCardNo.StartsWith(";")) {
						mCardNo = mCardNo.Substring(1);
					}
					String dateData = pValue.Substring(endPos + 1);
					setExpiryYear(Int32.Parse(dateData.Substring(0, 2)));
					setExpiryMonth(Int32.Parse(dateData.Substring(2, 2)));

					pValue = workData;
					startPos = pValue.IndexOf("^") + 1;
					if (startPos >= 0) {
						workData = pValue.Substring(startPos);
						endPos = workData.IndexOf("^");
						if (endPos > 0) {
							//CardName = workData.Substring(0, endPos).Trim();
						}
					}

					//setTrack1(value.Substring(0, value.IndexOf(";")));

				} else if ((pValue.StartsWith(";%") || pValue.StartsWith("%B")) && pValue.EndsWith("?") && pValue.IndexOf("^") > 0 && pValue.IndexOf('=') < 0) {
					string[] workData = pValue.Split('^');

					//int startPos = value.IndexOf("?") + 1;
					//int endPos = value.IndexOf("=");
					if (workData.Length != 3) {
						try {

						} catch (Exception) {
						}
					} else {
						mCardNo = workData[0].Replace("%B", "").Replace(";%", "").Replace(" ", "");

						//CardName = workData[1].Trim();

						String dateData = workData[2];
						setExpiryYear(Int32.Parse(dateData.Substring(0, 2)));
						setExpiryMonth(Int32.Parse(dateData.Substring(2, 2)));

						//setTrack1(value);
					}
				} else if ((pValue.StartsWith(";") || pValue.StartsWith("';")) && pValue.EndsWith("?") && pValue.IndexOf("=") > 0) {
					//if (value.IndexOf(Globals.TEST_CARD) > 0) {
					//    value = Globals.USE_TEST_CARD;
					//}
					//mSwipeInfo = value;
					String sText = pValue.Substring(1);
					sText = sText.Substring(0, sText.Length - 1);
					mCardNo = sText.Split('=')[0];
					if (mCardNo.Length < 14 || !mCardNo.IsNumeric()) {
						//mErrorMsg = "Bad Swipe";
						return false;
					}
					sText = sText.Split('=')[1];
					mExpiryYear = Int32.Parse(sText.Substring(0, 2));
					sText = sText.Substring(2);
					mExpiryMonth = Int32.Parse(sText.Substring(0, 2));
					sText = sText.Substring(2);
					//ServiceCode = sText.Substring(0, 3);
					sText = sText.Substring(3);

				} else if (pValue.StartsWith(";0") && pValue.EndsWith("?")) {   // TaxiPass Cards
																				//if (value.IndexOf(Globals.TEST_CARD) > 0) {
																				//    value = Globals.USE_TEST_CARD;
																				//}
																				//mSwipeInfo = value;
																				//sText = sText.Substring(1, sText.Length - 1);
					mCardNo = pValue.Substring(1, pValue.Length - 2);
					mExpiryYear = 9999;
					mExpiryMonth = 12;
					//ServiceCode = "101";

				} else {
					//mErrorMsg = "Card Swipe Read Error";
					return false;
				}
				if (mCardNo.Length < 14 || !mCardNo.IsNumeric()) {
					//mErrorMsg = "Bad Swipe";
					return false;
				}
			} catch (Exception) {
				return false;
				//throw new Exception("Error Validating card, Please try again " + ex.Message);
			}
			return true;
		}

		private String getExpiration() {
			String month = mExpiryMonth.ToString();
			String year = mExpiryYear.ToString();
			return StringUtils.PadLeft(month, 2, "0") + StringUtils.PadLeft(year, 2, "0");
		}

		private int getExpiryMonth() {
			return mExpiryMonth;
		}

		private void setExpiryMonth(int value) {
			mExpiryMonth = value;
		}

		private int getExpiryYear() {

			return mExpiryYear;
		}

		private void setExpiryYear(int value) {
			if (value < 1000) {
				value += 2000;
			}
			mExpiryYear = value;
		}
	}
}
