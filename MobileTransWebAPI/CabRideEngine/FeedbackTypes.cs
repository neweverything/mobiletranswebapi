﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

using Csla.Validation;

namespace CabRideEngine {
    [Serializable]
    public sealed class FeedbackTypes : FeedbackTypesDataRow {

        #region Constructors -- DO NOT MODIFY
        // Do not create constructors for this class
        // Developers cannot create instances of this class with the "new" keyword
        // You should write a static Create method instead; see the "Suggested Customization" region
        // Please review the documentation to learn about entity creation and inheritance

        // This private constructor prevents accidental instance creation via 'new'
        private FeedbackTypes() : this(null) {
        }

        // Typed DataSet constructor (needed by framework) - DO NOT REMOVE
        public FeedbackTypes(DataRowBuilder pRowBuilder)
            : base(pRowBuilder) {
        }

        #endregion

        #region Suggested Customizations

        //    // Use this factory method template to create new instances of this class
        //    public static FeedbackTypes Create(PersistenceManager pManager,
        //      ... your creation parameters here ...) { 
        //
        //      FeedbackTypes aFeedbackTypes = pManager.CreateEntity<FeedbackTypes>();
        //
        //      // if this object type requires a unique id and you have implemented
        //      // the IIdGenerator interface implement the following line
        //      pManager.GenerateId(aFeedbackTypes, // add id column here //);
        //
        //      // Add custom code here
        //
        //      aFeedbackTypes.AddToManager();
        //      return aFeedbackTypes;
        //    }

        //    // Implement this method if you want your object to sort in a specific order
        //    public override int CompareTo(Object pObject) {
        //    }

        //    // Implement this method to customize the null object version of this class
        //    protected override void UpdateNullEntity() {
        //    }

        #endregion

        // Add additional logic to your business object here...
        public static FeedbackTypes Create(PersistenceManager pManager) {

            FeedbackTypes oType = (FeedbackTypes)pManager.CreateEntity(typeof(FeedbackTypes));

            //pManager.GenerateId(oType, FeedbackTypes.FeedBackTypeIDEntityColumn);
            oType.AddToManager();
            return oType;
        }


        protected override void AddRules(RuleList pList) {
            PropertyRequiredRule.AddToList(pList, FeedbackTypes.FeedBackTypeEntityColumn.ColumnName);
            pList.Add(IsFeedbackTypeUnique, FeedbackTypes.FeedBackTypeEntityColumn.ColumnName);
        }

        private bool IsFeedbackTypeUnique(object pTarget, RuleArgs e) {
            FeedbackTypes oType = (FeedbackTypes)pTarget;

            RdbQuery oQry = new RdbQuery(typeof(FeedbackTypes), FeedbackTypes.FeedBackTypeEntityColumn, EntityQueryOp.EQ, oType.FeedBackType);
            oQry.AddClause(FeedbackTypes.FeedBackTypeIDEntityColumn, EntityQueryOp.NE, oType.FeedBackTypeID);
            EntityList<FeedbackTypes> oFeedbackTypes = this.PersistenceManager.GetEntities<FeedbackTypes>(oQry);
            if (oFeedbackTypes.Count > 0) {
                e.Description = "Feedback Type must be unique!";
                return false;
            }
            return true;
        }

        [BindingBrowsable(false)]
        public override string EntityInstanceDisplayName {
            get {
                return FeedBackType;
            }
        }

        protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
            if (IsDeserializing)
                return;
            base.OnColumnChanging(pArgs);
            //DoAudit(pArgs);
        }

        private void DoAudit(DataColumnChangeEventArgs pArgs) {
            if (!this.PersistenceManager.IsClient) {
                return;
            }
            if (this.RowState == DataRowState.Added) {
                return;
            }
            if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
                AuditRecord = FeedbackTypesAudit.Create(this.PersistenceManager, this);
            }
            //AuditRecord[pArgs.Column.ColumnName] = pArgs.ProposedValue;
        }


    }

}
