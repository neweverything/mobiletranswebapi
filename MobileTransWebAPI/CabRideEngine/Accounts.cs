﻿using Csla.Validation;
using IdeaBlade.Persistence;
using IdeaBlade.Persistence.Rdb;
using System;
using System.Data;
using System.Text;
using TaxiPassCommon;

namespace CabRideEngine {
	[Serializable]
	public sealed class Accounts : AccountsDataRow {

		#region Constructors -- DO NOT MODIFY
		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new'
		private Accounts()
			: this(null) {
		}

		// Typed DataSet constructor (needed by framework) - DO NOT REMOVE
		public Accounts(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static Accounts Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      Accounts aAccounts = pManager.CreateEntity<Accounts>();
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aAccounts, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aAccounts.AddToManager();
		//      return aAccounts;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		// Add additional logic to your business object here...
		public static Accounts Create(PersistenceManager pManager) {

			Accounts oAccount = (Accounts)pManager.CreateEntity(typeof(Accounts));

			pManager.GenerateId(oAccount, Accounts.AccountIDEntityColumn);
			oAccount.AddToManager();
			return oAccount;
		}

		protected override void AddRules(RuleList pList) {
			PropertyRequiredRule.AddToList(pList, AccountNoEntityColumn.ColumnName);
			PropertyRequiredRule.AddToList(pList, Accounts.NameEntityColumn.ColumnName);

			pList.Add(IsAccountUnique, Accounts.AccountNoEntityColumn.ColumnName);
			pList.Add(IsSubDomainUnique, Accounts.SubDomainEntityColumn.ColumnName);
		}

		private bool IsAccountUnique(object pTarget, RuleArgs e) {
			Accounts oAccount = (Accounts)pTarget;

			RdbQuery qry = new RdbQuery(typeof(Accounts), Accounts.AccountNoEntityColumn, EntityQueryOp.EQ, oAccount.AccountNo);
			qry.AddClause(Accounts.AccountIDEntityColumn, EntityQueryOp.NE, oAccount.AccountID);
			EntityList<Accounts> oAccounts = this.PersistenceManager.GetEntities<Accounts>(qry);
			if (oAccounts.Count > 0) {
				e.Description = "Account No must be unique!";
				return false;
			}
			return true;
		}

		private bool IsSubDomainUnique(object pTarget, RuleArgs e) {
			Accounts rec = pTarget as Accounts;
			if (string.IsNullOrEmpty(rec.SubDomain)) {
				return true;
			}

			RdbQuery qry = new RdbQuery(typeof(Accounts), SubDomainEntityColumn, EntityQueryOp.EQ, rec.SubDomain);
			qry.AddClause(AccountIDEntityColumn, EntityQueryOp.NE, rec.AccountID);
			EntityList<Accounts> oList = this.PersistenceManager.GetEntities<Accounts>(qry);
			if (oList.Count > 0) {
				e.Description = "An Account already contains this Sub Domain: " + rec.SubDomain;
				return false;
			}

			//check if redeemer has this subdomain
			TaxiPassRedeemerAccounts acct = TaxiPassRedeemerAccounts.GetBySubDomain(this.PersistenceManager, rec.SubDomain);
			if (!acct.IsNullEntity) {
				e.Description = "A Redeemer already contains this Sub Domain: " + rec.SubDomain;
				return false;
			}
			return true;
		}

		// ****************************************************************************
		// Replaced IdeaBlade with a proc for speed
		// ****************************************************************************
		public static Accounts GetAccount(PersistenceManager pPM, string pAccountNo) {
			StringBuilder sql = new StringBuilder();
			sql.Append("usp_AccountsGet ");
			if (!pAccountNo.IsNullOrEmpty()) {
				sql.AppendFormat(" @AccountNo = '{0}'", pAccountNo);
			}
			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(Accounts), sql.ToString());
			EntityList<Accounts> oAccounts = pPM.GetEntities<Accounts>(qry);
			if (oAccounts.Count == 0) {
				return pPM.GetNullEntity<Accounts>();
			} else {
				return oAccounts[0];
			}
		}

		public static Accounts GetAccountBySubDomain(PersistenceManager pPM, string pSubDomain) {
			RdbQuery qry = new RdbQuery(typeof(Accounts), Accounts.SubDomainEntityColumn, EntityQueryOp.EQ, pSubDomain);
			return pPM.GetEntity<Accounts>(qry);
		}


		private AccountRef mISOLogo;
		public string ISOLogo {
			get {
				CreateISOLogo();
				return mISOLogo.ValueString;
			}
			set {
				CreateISOLogo();
				mISOLogo.ValueString = value;
			}
		}

		private void CreateISOLogo() {
			if (mISOLogo == null) {
				mISOLogo = AccountRef.GetCreate(this, "Account", "ISOLogo", "");
			}
		}
	}

}
