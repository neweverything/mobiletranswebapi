﻿using System;

namespace MobileTransWebAPI.Models {
    public class GPSInfo {
        public String Key;

        public long DriverID { get; set; }
        public long AffiliateDriverID { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }
        public float Speed { get; set; }
        public string GpsTime { get; set; }
        public int LatLonAccuracy { get; set; }
        public String Method { get; set; }
        public int SatsUsed { get; set; }
        public String Address { get; set; }
    }
}