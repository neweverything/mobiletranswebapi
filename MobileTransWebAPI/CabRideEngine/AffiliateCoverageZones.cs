﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

using Csla.Validation;

namespace CabRideEngine {
	[Serializable]
	public sealed class AffiliateCoverageZones : AffiliateCoverageZonesDataRow {

		#region Constructors -- DO NOT MODIFY
		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new'
		private AffiliateCoverageZones()
			: this(null) {
		}

		// Typed DataSet constructor (needed by framework) - DO NOT REMOVE
		public AffiliateCoverageZones(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static AffiliateCoverageZones Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      AffiliateCoverageZones aAffiliateCoverageZones = (AffiliateCoverageZones) pManager.CreateEntity(typeof(AffiliateCoverageZones));
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aAffiliateCoverageZones, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aAffiliateCoverageZones.AddToManager();
		//      return aAffiliateCoverageZones;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		// Add additional logic to your business object here...
		public static AffiliateCoverageZones Create(PersistenceManager pPM, long pAffiliateID, long pCoverageZoneID) {
			AffiliateCoverageZones oZone = null;

			try {
				oZone = (AffiliateCoverageZones)pPM.CreateEntity(typeof(AffiliateCoverageZones));

				// Using the IdeaBlade Id Generation technique
				pPM.GenerateId(oZone, AffiliateCoverageZones.AffiliateCoverageZoneIDEntityColumn);

				oZone.AffiliateID = pAffiliateID;
				oZone.CoverageZoneID = pCoverageZoneID;
				oZone.RideDistribution = 100;

				oZone.AddToManager();

			} catch (Exception ex) {
				throw ex;
			}
			return oZone;
		}

		public override long CoverageZoneID {
			get {
				return base.CoverageZoneID;
			}
			set {

				RdbQuery oQry = new RdbQuery(typeof(CoverageZones), CoverageZones.CoverageZoneIDEntityColumn, EntityQueryOp.EQ, value);
				if (this.PersistenceManager.GetEntities<CoverageZones>(oQry).Count < 1) {
					throw new Exception("Coverage Zone: Invalid value");
				}

				oQry = new RdbQuery(typeof(AffiliateCoverageZones), AffiliateCoverageZones.CoverageZoneIDEntityColumn, EntityQueryOp.EQ, value);
				oQry.AddClause(AffiliateCoverageZones.AffiliateIDEntityColumn, EntityQueryOp.EQ, this.AffiliateID);
				oQry.AddClause(AffiliateCoverageZones.AffiliateCoverageZoneIDEntityColumn, EntityQueryOp.NE, this.AffiliateCoverageZoneID);
				EntityList<AffiliateCoverageZones> oZones = this.PersistenceManager.GetEntities<AffiliateCoverageZones>(oQry);
				if (oZones.Count > 0) {
					throw new Exception("Coverage Zone: value must be unique!");
				}

				base.CoverageZoneID = value;
				OnPropertyChanged("CoverageZoneID");
				
			}
		}

		public override short RideDistribution {
			get {
				return base.RideDistribution;
			}
			set {
				if (value > 100) {
					throw new Exception("Ride Distribution: value must be less than or equal to 100");
				}
				base.RideDistribution = value;
			}
		}

		protected override void AddRules(RuleList pList) {
			//PropertyRequiredRule.AddToList(pList, AffiliateCoverageZones.AffiliateIDEntityColumn.ColumnName, null);
			PropertyRequiredRule.AddToList(pList, AffiliateCoverageZones.CoverageZoneIDEntityColumn.ColumnName);

			base.AddRules(pList);
		}

		protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
			if (IsDeserializing)
				return;
			base.OnColumnChanging(pArgs);
			DoAudit(pArgs);
		}

		private void DoAudit(DataColumnChangeEventArgs pArgs) {
			if (!this.PersistenceManager.IsClient) {
				return;
			}
            if (this.RowState == DataRowState.Added) {
                return;
            }
			if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
				AuditRecord = AffiliateCoverageZonesAudit.Create(this.PersistenceManager, this);
			}
			//AuditRecord[pArgs.Column.ColumnName] = pArgs.ProposedValue;
		}

        public static EntityList<AffiliateCoverageZones> GetAffiliatesZones(PersistenceManager pPM, long pPUZone, long pDOZone) {
            RdbQuery oQry = new RdbQuery(typeof(AffiliateCoverageZones), AffiliateCoverageZones.CoverageZoneIDEntityColumn, EntityQueryOp.EQ, pPUZone);
            EntityList<AffiliateCoverageZones> oZones = pPM.GetEntities<AffiliateCoverageZones>(oQry);

            if (oZones.Count == 0) {
                oQry = new RdbQuery(typeof(AffiliateCoverageZones), AffiliateCoverageZones.CoverageZoneIDEntityColumn, EntityQueryOp.EQ, pDOZone);
                oZones = pPM.GetEntities<AffiliateCoverageZones>(oQry);
            }

            return oZones;
        }


	}

}
