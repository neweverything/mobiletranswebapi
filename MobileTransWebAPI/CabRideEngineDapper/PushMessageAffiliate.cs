﻿using CabRideEngineDapper.Utils;
using Dapper;

namespace CabRideEngineDapper {
	public class PushMessageAffiliate {
		private enum FieldNames {
			AffiliateID
		}

		private const string PUSH_MESSAGE_AFFILIATE = "PushMessageAffiliate";
		public static CabDriverRef CreatePushMessageAffiliate(PushMessage pPushMessage) {
			CabDriverRef rec = CabDriverRef.Create();
			rec.SourceID = pPushMessage.PushMessageID;
			rec.SourceTable = PUSH_MESSAGE_AFFILIATE;

			return rec;
		}

		public static CabDriverRef GetAffiliate(PushMessage pPushMessage, long pAffiliateID) {
			string sql = @"SELECT *
							FROM CabDriverRef
							WHERE SourceTable = @SourceTable
								  AND SourceID = @SourceID
								  AND ReferenceKey = @ReferenceKey
								  AND ValueString = @ValueString";
			using (var conn = SqlHelper.OpenSqlConnection()) {
				return conn.Get<CabDriverRef>(sql, new { SourceTable = PUSH_MESSAGE_AFFILIATE, SourceID = pPushMessage.PushMessageID, ReferenceKey = FieldNames.AffiliateID.ToString(), ValueString = pAffiliateID.ToString() });
			}
		}


		public static CabDriverRef GetOrCreateAffiliateRec(PushMessage pPushMessage, long pAffiliateID) {
			CabDriverRef rec = GetAffiliate(pPushMessage, pAffiliateID);

			if (rec.IsNullEntity) {
				rec = CabDriverRef.Create();
				rec.SourceTable = PUSH_MESSAGE_AFFILIATE;
				rec.SourceID = pPushMessage.PushMessageID;
				rec.ReferenceKey = FieldNames.AffiliateID.ToString();
				rec.ValueString = pAffiliateID.ToString();
				rec.Save();
			}

			return rec;
		}
	}
}
