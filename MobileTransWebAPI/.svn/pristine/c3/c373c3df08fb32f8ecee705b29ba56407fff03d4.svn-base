﻿using CabRideEngine;
using CabRideEngineDapper.Utils;
using Dapper;
using IdeaBlade.Persistence;
using IdeaBlade.Persistence.Rdb;
using MobileTransWebAPI.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Http;
using TaxiPassCommon;
using TaxiPassCommon.Banking;

namespace MobileTransWebAPI.Controllers {

    public class FundPayCardController : ApiController {

        public FundsTransferResponse Get() {
            try {
                List<string> lines = System.IO.File.ReadAllLines(@"d:\temp\FundPayCard_20170912").ToList();
                foreach (string data in lines) {
                    AffDriverLoginInfo info = JsonConvert.DeserializeObject<AffDriverLoginInfo>(data.Trim().Decrypt());
                    if (info.Phone == "9086598167" || info.DriverID == 70496) {
                        string js = JsonConvert.SerializeObject(info);
                        Console.WriteLine(js);
                        FundPayCard(data.Trim());
                    }
                }
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }

            return FundPayCard("E730AD852C20A211B400CAD88BFC4F1D31876102AAE7DA24DCBA27110198B6089B8488DA1F8A112322AD065C5520072B59D0BE5500A343DDD521FE1EAA864977EE3D8A7C2DB70AAF3A70B7692291D712644633DC786EA490B2504CB668D656CA01CC3942209081BAADBD1666C7AE9FD562506A406D6370E06B73F0EFFEA3F4968E709A57B3E6E5A9402ADC020F08DC48FB2F9D2B628A36DEE47670CAE498A20AA2C862A93927233A6745034C657309172D4A2B9A4476A4ADCD8F429CE0DD81266B83B3AEABFC769D00EC86E74B0E16AB2E67701A9A51DB06C08AC306EEFD3DCE8CAEE86EFC5D551F353235C6ED9F53EC372E1F7FF50AB937F6DAE53A183FBD9BBAF315479028CBBA746150BFC868DDDDFDDAE379BD68675CC0B7BDB3D60A6C5BBB81AD351A581CD0DBD91CC05530603DCDB36EDDE36DD784BFA2AD6EAC528EDA74545DCD4C2595349D02ED96DC13869E40E13536830FB6D2762DAEB60940B7327BD739EB9C6D1FD9861FCAB1CE36FD8D705F047B33939FFB1199F37EAFDB35941D647238370E38CDD85EA9A86694CA86EDF35CE2CCB5562F51393936C28E9A803FDCBE9A8943C688561939534701EDEC0924049D71B08928CE7D9254FAFAD752464EBFA98A268C4AF48BF472");
        }

        [HttpGet]
        public FundsTransferResponse FundPayCard(string pRequest) {
            FundsTransferResponse fundResults = new FundsTransferResponse();

            if (!Utils.ActionValidator.IsValid(Utils.ActionValidator.ActionTypeEnum.None)) {
                fundResults.ReturnCode = "-10";
                fundResults.ReturnMessage = "Pay card funding in progress";
                return fundResults;
            }

            WebApiApplication.LogIt("FundPayCard:", pRequest, "FundPayCard_{0}");

            PersistenceManager mPM = WebApiApplication.UserPM;
            if (mPM == null) {
                fundResults.ReturnMessage = "Could not connect to database";
                return fundResults;
            }



            AffDriverLoginInfo info = new AffDriverLoginInfo();
            try {
                string request = pRequest.DecryptIceKey();
                info = JsonConvert.DeserializeObject<AffDriverLoginInfo>(request);

                DateTime requestDate = Convert.ToDateTime(info.RequestTime);

                if (WebApiApplication.GetUTCServerTime().Subtract(requestDate).TotalMinutes > 1) {
                    fundResults.ReturnMessage = "Invalid Access";
                    return fundResults;
                }

                if (!info.Phone.IsNullOrEmpty()) {
                    using (var conn = SqlHelper.OpenSqlConnection()) {
                        long taxipassRedeemerID = CabRideEngineDapper.SystemDefaultsDict.GetByKey(conn, "SelfRedemption", "RedeemerID", false).ValueString.AsLong();

                        CabRideEngineDapper.AffiliateDrivers pPayeeDriver = CabRideEngineDapper.AffiliateDrivers.GetDriverByCallInCell(conn, info.Phone);

                        if (pPayeeDriver.IsNullEntity || pPayeeDriver.RedeemerStopPayment || pPayeeDriver.TransCardNumber.IsNullOrEmpty()) {
                            fundResults.ReturnCode = "-1";
                            fundResults.ReturnMessage = "You cannot get paid at this time, please contact TaxiPass to resolve";
                            return fundResults;
                        }

                        EntityList<DriverPayments> payList = GetPayList(mPM, pPayeeDriver.AffiliateDriverID);
                        if (payList.Count == 0) {
                            fundResults.ReturnCode = "-1";
                            fundResults.ReturnMessage = "No vouchers available for funding";
                            return fundResults;
                        }

                        bool passAppTest = true;
                        bool checkAppVersion = SystemDefaultsDict.GetByKey(mPM, "PayCard", "CheckAppVersion", false).ValueString.AsBool();
                        if (checkAppVersion) {
                            if (info.AppName.Equals("TaxiPass EWR")) {
                                if (info.VersionNo.StartsWith("1.")) {
                                    decimal vers = info.VersionNo.Substring(2).AsDecimal();
                                    if (vers < 3.60M) {
                                        passAppTest = false;
                                    }
                                }
                            } else if (info.AppName.Equals("GetRidePayments")) {
                                if (info.VersionNo.StartsWith("3.")) {
                                    decimal vers = info.VersionNo.Substring(2).AsDecimal();
                                    if (info.HardwareOS == "Android") {
                                        if (vers < 10.66M) {
                                            passAppTest = false;
                                        }
                                    } else {
                                        if (vers < 10.60M) {
                                            passAppTest = false;
                                        }
                                    }
                                } else {
                                    int verStart = info.VersionNo.Split('.').First().AsInt();
                                    if (verStart < 3) {
                                        passAppTest = false;
                                    }
                                }
                            }
                            if (!passAppTest) {
                                fundResults.ReturnCode = "99";
                                fundResults.ReturnMessage = "Please update app in order to fund your pay card";
                                return fundResults;
                            }
                        }


                        // Ensure only 1 funding request goes through 
                        CabRideEngineDapper.PayCardProcessing payCardProcessing = CabRideEngineDapper.PayCardProcessing.GetNullEntity();
                        try {
                            payCardProcessing = CabRideEngineDapper.PayCardProcessing.AddTransCardAdminNo(conn, pPayeeDriver.TransCardAdminNo);
                            if (payCardProcessing.PayCardProcessingID <= 0) {
                                fundResults.ReturnCode = "99";
                                fundResults.ReturnMessage = "Error Adding Card Info";
                                return fundResults;
                            }
                        } catch (Exception) {
                            fundResults.ReturnCode = "99";
                            //fundResults.ReturnMessage = ex.Message;
                            fundResults.ReturnMessage = "Too soon to fund your pay card, try again in an hour";
                            return fundResults;
                        }

                        var lastProcessed = CabRideEngineDapper.PayCardProcessing.GetLastFunding(conn, pPayeeDriver.TransCardAdminNo, payCardProcessing.PayCardProcessingID);
                        if (!lastProcessed.IsNullEntity && payCardProcessing.CreatedDate.Subtract(lastProcessed.CreatedDate).TotalMinutes < 30) {
                            fundResults.ReturnCode = "99";
                            fundResults.ReturnMessage = "Too soon to fund your pay card, try again in an hour";
                            return fundResults;
                        }


                        decimal decAmount = 0;

                        FSV.Credentials cred = new FSV.Credentials();
                        CabRideEngineDapper.AffiliateDriverRef rec;
                        cred = conn.Query<FSV.Credentials>("usp_SystemDefaultsDict_Flattened", new { ReferenceGroup = "FSV" }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                        rec = CabRideEngineDapper.AffiliateDriverRef.GetOrCreate(conn, pPayeeDriver.AffiliateDriverID, "FSV", "Registration");

                        FSV fsv = new FSV(cred, cred.UseProductionAsBoolen);
                        FSV.VerifyConnectionResponse verifyResponse = new FSV.VerifyConnectionResponse();
                        verifyResponse = fsv.VerifyConnection();
                        if (verifyResponse.ReturnCode != "1") {
                            fundResults.ReturnCode = "-1";
                            fundResults.ReturnMessage = "Could not verify connection to USBank: " + verifyResponse.ReturnMessage;
                            return fundResults;
                        }

                        FSV.CardRegistrationResponse resp = JsonConvert.DeserializeObject<FSV.CardRegistrationResponse>(rec.ValueString);
                        FSV.FundsTransfer transfer = new FSV.FundsTransfer();
                        transfer.FromCardID = cred.AdjustmentCard.DecryptIceKey();
                        transfer.FromCardPassCode = cred.AdjustmentCode.DecryptIceKey();
                        transfer.ToCardID = resp.CardID;

                        List<CabRideEngineDapper.ACHDetail> achRecList = new List<CabRideEngineDapper.ACHDetail>();

                        string transNo = "";
                        foreach (DriverPayments pay in payList) {
                            if (pay.Test || pay.DoNotPay || pay.Failed) {
                                continue;
                            }
                            //ACHDetail achRec = ACHDetail.GetTransactionByDriverPaymentID(mPM, pay.DriverPaymentID, QueryStrategy.DataSourceThenCache);
                            CabRideEngineDapper.ACHDetail achRec = CabRideEngineDapper.ACHDetail.GetByDriverPaymentID(pay.DriverPaymentID);
                            if (!achRec.ACHPaidDate.HasValue) {
                                if (achRec.IsNullEntity) {
                                    //achRec = ACHDetail.Create(pay);
                                    achRec = CabRideEngineDapper.ACHDetail.Create(pay.DriverPaymentID);
                                }
                                //if (pPayeeDriver.PayCardRealTimePay || pPayeeDriver.Affiliate.PayCardRealTimePay) {
                                if (pay.RedemptionFee == 0) {
                                    pay.RedemptionFee = pay.Affiliate.AffiliateDriverDefaults.DriverInitiatedPayCardRedemptionFee;
                                }
                                decAmount = pay.DriverTotalPaid; // - pay.Affiliate.AffiliateDriverDefaults.DriverInitiatedPayCardRedemptionFee.GetValueOrDefault(0);

                                if (decAmount == 0) {
                                    decAmount = pay.DriverPaymentKioskAmount.Amount;
                                }

                                if (decAmount == 0) {
                                    achRec.ACHErrorReason = "No Fare amount found";
                                } else {
                                    achRec.AmountPaid = decAmount;
                                    transfer.TransferAmount += decAmount;
                                }

                                achRec.ACHErrorReason = "Paid: " + pay.DriverTotalPaid.ToString("C");
                                //}
                                achRec.ACHPaidDate = DateTime.Now;
                                achRec.ACHPaidTo = pPayeeDriver.Name;
                                achRec.ACHBankAccountNumber = "PayCard: " + pPayeeDriver.TransCardNumber.DecryptIceKey().Right(5);

                                achRec.TransCardAdminNo = pPayeeDriver.TransCardAdminNo;
                                //achRec.Save(SessionHelper.EMail(Session));
                                if (transNo.IsNullOrEmpty()) {
                                    transNo = pay.TransNo;
                                } else {
                                    transNo = string.Format("{0}-{1}", transNo, pay.TransNo);
                                }
                                achRecList.Add(achRec);

                                if (!pay.DriverPaid && achRec.AmountPaid.GetValueOrDefault(0) != 0) {
                                    pay.DriverPaid = true;
                                    pay.DriverPaidDate = achRec.ACHPaidDate;

                                    if (pay.TaxiPassRedeemerID.GetValueOrDefault(0) == 0) {
                                        pay.TaxiPassRedeemerID = taxipassRedeemerID;
                                    }
                                    pay.Save(info.UserID);

                                    CabRideEngineDapper.DriverPaymentRef dpRef = CabRideEngineDapper.DriverPaymentRefTable.Create();
                                    dpRef.DriverPaymentID = pay.DriverPaymentID;
                                    dpRef.ReferenceGroup = "ACHToPayCard";
                                    dpRef.ReferenceKey = "AdminNo";
                                    dpRef.ValueString = pPayeeDriver.TransCardAdminNo;
                                    dpRef.Save(info.UserID);
                                }
                            }
                        }

                        transfer.TransferAmount = achRecList.Distinct().Sum(p => p.AmountPaid.GetValueOrDefault(0));
                        transfer.ReferenceFromCard = string.Format("ADriverID: {0}|{1}", pPayeeDriver.AffiliateDriverID, transNo);
                        if (transfer.ReferenceFromCard.Length > 100) {
                            long masterVoucherID = payList[0].DriverPaymentID;
                            foreach (var achRec in achRecList) {
                                CabRideEngineDapper.DriverPaymentAux aux = CabRideEngineDapper.DriverPaymentAux.GetOrCreate(achRec.DriverPaymentID);
                                aux.RowState = DataRowState.Modified;
                                aux.MasterVoucherID = masterVoucherID;
                                aux.Save(info.UserID);
                            }
                            transfer.ReferenceFromCard = string.Format("ADriverID: {0}|^{1}", pPayeeDriver.AffiliateDriverID, masterVoucherID);
                        }
                        transfer.ReferenceToCard = "Driver Payment";

                        var funded = fsv.TransferToPayCard(transfer);
                        if (funded.ReturnCode == "1") {
                            fundResults.ReturnCode = funded.ReturnCode;
                            fundResults.ReturnMessage = funded.ReturnMessage;
                            CabRideEngine.Utils.Misc.SendTransCardPaymentSMS(payList.ToList(), pPayeeDriver, "");
                            //mPM.SaveChanges(achRecList);
                            foreach (var achRec in achRecList) {
                                achRec.Save();
                            }
                            CabRideEngineDapper.SystemDefaultsDict fundBal = CabRideEngineDapper.SystemDefaultsDict.GetOrCreate(conn, "FSV", "FundingAccountBalance");
                            fundBal.ValueString = funded.FromCardBalance;
                            fundBal.Save();

                            CabRideEngineDapper.USBankPayCardPaid usBank = CabRideEngineDapper.USBankPayCardPaid.Create();
                            usBank.AdminNo = transfer.ToCardID;
                            usBank.AffiliateDriverID = pPayeeDriver.AffiliateDriverID;
                            usBank.CardEnding = pPayeeDriver.TransCardCardNumber.Right(5);
                            usBank.DebitTransaction = transfer.TransferAmount;
                            usBank.FundingTransDate = DateTime.Now;
                            usBank.LastName = pPayeeDriver.LastName.IsNullOrEmpty() ? pPayeeDriver.Name : pPayeeDriver.LastName;
                            //usBank.TaxiPassRedeemerID = info.tax
                            usBank.TransactionDetail = transfer.ReferenceFromCard;
                            usBank.Save();



                        } else {
                            fundResults.ReturnCode = funded.ReturnCode;
                            //fundResults.ReturnMessage = string.Format("Payment of {0} to be posted later to your card\n{1}", transfer.TransferAmount.ToString("C"), funded.ReturnMessage);
                            fundResults.ReturnMessage = string.Format("Payment of {0} to be posted later to your card\n", transfer.TransferAmount.ToString("C"));
                            pPayeeDriver.SendDriverMsg(string.Format("Payment of {0} to be posted later to your card", transfer.TransferAmount.ToString("C")), false);
                        }
                    }
                }

            } catch (Exception ex) {
                fundResults.ReturnCode = "-1";
                fundResults.ReturnMessage = ex.Message;
            }

            return fundResults;
        }

        public EntityList<DriverPayments> GetPayList(PersistenceManager pPM, long pAffiliateDriverID) {
            string sql = @"DECLARE @LookBackDays INT = (SELECT CONVERT(INT, ValueString)
														 FROM   SystemDefaultsDict
														 WHERE  ReferenceGroup = 'PayCard'
																AND ReferenceKey = 'PayDriverChargeDateMinusDays'
														)

                          DECLARE @MaxAge INT = (SELECT CONVERT(INT, ValueString)
                                                         FROM   SystemDefaultsDict
                                                         WHERE  ReferenceGroup = 'PayCard'
                                                                AND ReferenceKey = 'MaxAge'
                                                        )
							SELECT  DISTINCT
									DriverPayments.*
							FROM    dbo.DriverPayments
							LEFT JOIN ACHDetail ON ACHDetail.DriverPaymentID = DriverPayments.DriverPaymentID
							LEFT JOIN DriverPaymentAux ON DriverPaymentAux.DriverPaymentID = DriverPayments.DriverPaymentID
							WHERE   AffiliateDriverID = {0}
									AND ACHPaidDate IS NULL
									AND Failed = 0
									AND Test = 0
									AND DATEADD(DAY, @LookBackDays, ChargeDate) < GETUTCDATE()
									AND ChargeDate >= '1/16/2016'
                                    AND ChargeDate > DATEADD(DAY, -@MaxAge, GETUTCDATE())
									AND ISNULL(DriverPayments.DoNotPay, 0) = 0 
									AND ISNULL(DriverPaymentAux.DoNotPay, 0) = 0
									AND TaxiPassRedeemerID IS NULL";

            PassthruRdbQuery qry = new PassthruRdbQuery(typeof(DriverPayments), string.Format(sql, pAffiliateDriverID));
            return pPM.GetEntities<DriverPayments>(qry);
        }
    }
}