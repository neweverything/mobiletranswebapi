﻿using Dapper;
using System.Data.SqlClient;
using System.Linq;

namespace CabRideEngineDapper.Utils {
    public class XSocketsHelper {

        public string GetRideSocketsController { get; set; }
        public string GetRideSocketsControllerOrigin { get; set; }

        public static XSocketsHelper GetData(SqlConnection pConn) {


            string sql = @"DECLARE @GetRideSocketsController VARCHAR(50) = (SELECT ValueString
                                                                            FROM   dbo.SystemDefaultsDict
                                                                            WHERE  ReferenceGroup = 'XSockets'
                                                                                AND ReferenceKey = 'GetRideSocketsController'
                                                                        )

                        DECLARE @GetRideSocketsControllerOrigin VARCHAR(50) = (SELECT ValueString
                                                                            FROM   dbo.SystemDefaultsDict
                                                                            WHERE  ReferenceGroup = 'XSockets'
                                                                                AND ReferenceKey = 'GetRideSocketsControllerOrigin'
                                                                        )

                        SELECT  @GetRideSocketsController GetRideSocketsController, @GetRideSocketsControllerOrigin GetRideSocketsControllerOrigin";

            //using (var conn = SqlHelper.OpenSqlConnection()) {
            return pConn.Query<XSocketsHelper>(sql).FirstOrDefault();
            //}

        }
    }
}
