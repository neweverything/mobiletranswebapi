﻿using CabRideEngineDapper.Utils;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using TaxiPassCommon;

namespace CabRideEngineDapper {
    public partial class DriversGPSHistory {


        public static List<DriversGPSHistory> GetRecords(SqlConnection pConn, long pDriverID, DateTime pStart, DateTime pEnd, string pTimeZone) {
            TimeZoneConverter tzConvert = new TimeZoneConverter();
            DateTime dGMTStart = tzConvert.FromUniversalTime(pTimeZone, pStart);
            DateTime dGMTEnd = tzConvert.FromUniversalTime(pTimeZone, pEnd);

            DynamicParameters parms = new DynamicParameters();
            parms.Add("DriverID", pDriverID);
            parms.Add("GPSStart", pStart);
            parms.Add("GPSEnd", pEnd);
            parms.Add("GPSGMTStart", dGMTStart);
            parms.Add("GPSGMTEnd", dGMTEnd);

            return pConn.Query<DriversGPSHistory>("usp_DriversGPSHistory_GetDriver", parms, commandType: System.Data.CommandType.StoredProcedure).ToList();
        }


        public static List<DriversGPSHistory> GetRecords(Drivers pDriver, DateTime pStart, DateTime pEnd) {
            string sql = @"SELECT  *
                            FROM    DriversGPSHistory
                            WHERE   DriverID = @DriverID
                                    AND GPSTime >= @Start
                                    AND GPSTime <= @End
                            ORDER BY GPSTime";

            using (var conn = SqlHelper.OpenSqlConnection()) {
                return conn.Query<DriversGPSHistory>(sql, new { DriverID = pDriver.DriverID, Start = pStart, End = pEnd }).ToList();
            }
        }


        public static DriversGPSHistory GetDriverRecordForDateTime(Drivers pDriver, DateTime pDate) {
            string sql = @"SELECT TOP 1 *
                            FROM    DriversGPSHistory
                            WHERE   DriverID = @DriverID
                                    AND GPSTime <= @Date
                            ORDER BY GPSTime DESC";

            DynamicParameters parms = new DynamicParameters();
            parms.Add("DriverID", pDriver.DriverID);
            parms.Add("Date", pDate);
            using (var conn = SqlHelper.OpenSqlConnection()) {
                return conn.Get<DriversGPSHistory>(sql, parms);
            }
        }

    }
}
