﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Util;
using IdeaBlade.Persistence.Rdb;

using TaxiPassCommon;

namespace CabRideEngine {
    [Serializable]
    public sealed partial class CardResults : CardResultsDataRow {

        #region Constructors -- DO NOT MODIFY
        // Do not create constructors for this class
        // Developers cannot create instances of this class with the "new" keyword
        // You should write a static Create method instead; see the "Suggested Customization" region
        // Please review the documentation to learn about entity creation and inheritance

        // This private constructor prevents accidental instance creation via 'new'
        private CardResults()
            : this(null) {
        }

        // Typed DataSet constructor (needed by framework) - DO NOT REMOVE
        public CardResults(DataRowBuilder pRowBuilder)
            : base(pRowBuilder) {
        }

        #endregion

        #region Suggested Customizations

        //    // Use this factory method template to create new instances of this class
        //    public static CardResults Create(PersistenceManager pManager,
        //      ... your creation parameters here ...) { 
        //
        //      CardResults aCardResults = pManager.CreateEntity<CardResults>();
        //
        //      // if this object type requires a unique id and you have implemented
        //      // the IIdGenerator interface implement the following line
        //      pManager.GenerateId(aCardResults, // add id column here //);
        //
        //      // Add custom code here
        //
        //      aCardResults.AddToManager();
        //      return aCardResults;
        //    }

        //    // Implement this method if you want your object to sort in a specific order
        //    public override int CompareTo(Object pObject) {
        //    }

        //    // Implement this method to customize the null object version of this class
        //    protected override void UpdateNullEntity() {
        //    }

        #endregion

        // Add additional logic to your business object here...
        public static CardResults Create(PersistenceManager pManager, long pReserveID) {
            CardResults oResults = null;
            try {
                oResults = (CardResults)pManager.CreateEntity(typeof(CardResults));
                oResults.ReservationID = pReserveID;

                pManager.GenerateId(oResults, CardResults.CardResultIDEntityColumn);
                oResults.AddToManager();

            } catch (Exception ex) {
                throw ex;
            }
            return oResults;
        }

        public override string Number {
            get {
                if (base.Number.IsNumeric()) {
                    return base.Number;
                }
                return Utils.CreditCardUtils.DecryptCardNo(this.Reservations.Customer, base.Number);
            }
            set {
                base.Number = value;
            }
        }

        public override SaveResult Save() {
            SaveResult oResult = base.Save();
            Reservations oRes = this.Reservations;

            //set the reservation billing status
            switch (this.Result.Trim()) {
                case "0": // approved
                    switch (this.TransType) {
                        case VerisignTrxTypes.Authorization:
                            oRes.BillingStatus = "Authorized";
                            break;
                        case VerisignTrxTypes.Inquiry:
                            oRes.BillingStatus = "Inquiry";
                            break;
                        default:
                            oRes.BillingStatus = "Charged";
                            if (string.IsNullOrEmpty(oRes.EMail)) {
                                oRes.BillingStatus += " (No Email)";
                            }
                            break;
                    }
                    break;
                default:
                    oRes.BillingStatus = "Failed " + VerisignTrxTypes.GetTrxDescription(this.TransType);
                    break;
            }
            oRes.Save();
            return oResult;
        }


        public static CardResults GetCardResults(PersistenceManager pPM, long pCardResultID) {
            RdbQuery qry = new RdbQuery(typeof(CardResults), CardResults.CardResultIDEntityColumn, EntityQueryOp.EQ, pCardResultID);
            return pPM.GetEntity<CardResults>(qry);
        }
    }


    
}
