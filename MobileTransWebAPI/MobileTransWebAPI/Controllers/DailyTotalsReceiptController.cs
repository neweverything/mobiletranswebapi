﻿using CabRideEngineDapper.Utils;
using Dapper;
using MobileTransWebAPI.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace MobileTransWebAPI.Controllers {
    public class DailyTotalsReceiptController : ApiController {
        // GET api/dailytotalsreceipt
        public IEnumerable<string> Get() {
            return new string[] { "value1", "value2" };
        }


        // POST api/dailytotalsreceipt
        public DailyTotalsResult Post([FromBody]DailyTotals pTotal) {

            string sql = @"SELECT  Affiliate.Name AffiliateName
                                  , @StartDate StartDate
                                  , @EndDate EndDate
                                  , AffiliateDrivers.Name + ' ' + LastName LoginName
                                  , AffiliateDrivers.Cell LoginNo
                                  , COUNT(*) Transactions
                                  , SUM(Fare + Gratuity) Total
                            FROM    dbo.DriverPayments
                            LEFT JOIN Affiliate ON Affiliate.AffiliateID = DriverPayments.AffiliateID
                            LEFT JOIN AffiliateDrivers ON AffiliateDrivers.AffiliateDriverID = DriverPayments.AffiliateDriverID
                            WHERE   DriverPayments.AffiliateDriverID = @AffiliateDriverID
                                    AND DriverPayments.AffiliateID = @AffiliateID
                                    AND ChargeDate >= @StartDate
                                    AND ChargeDate <= @EndDate
                                    AND Test = 0
                                    AND Failed = 0
                            GROUP BY Affiliate.Name
                                  , AffiliateDrivers.Name + ' ' + LastName
                                  , AffiliateDrivers.Cell";

            using (var conn = SqlHelper.OpenSqlConnection()) {
                var response = conn.Query<DailyTotalsResult>(sql, new { AffiliateDriverID = pTotal.AffDriverID, AffiliateID = pTotal.AffiliateID, StartDate = pTotal.StartDate, EndDate = pTotal.EndDate }).FirstOrDefault();
                return response;
            }

        }
    }
}
