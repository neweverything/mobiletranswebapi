﻿using CabRideEngineDapper.Utils;
using Dapper;
using System;
using TaxiPassCommon;

namespace CabRideEngineDapper {
    public partial class TaxiPassCardTransactions {


        public static TaxiPassCardTransactions AddTransaction(TaxiPassCards pTaxiPassCard, decimal pAmount, Int64? pDriverPaymentID) {
            TaxiPassCardTransactions rec = null;

            //PersistenceManager oPM = pTaxiPassCard.PersistenceManager;
            try {
                rec = TaxiPassCardTransactions.Create();
                rec.TaxiPassCardID = pTaxiPassCard.TaxiPassCardID;

                //oPM.GenerateId(rec, TaxiPassCardTransactions.TaxiPassCardTransIDEntityColumn);
                //rec.AddToManager();

                //get last trans balance
                TaxiPassCardTransactions tran = GetLastTrans(pTaxiPassCard);
                rec.Amount = pAmount;
                rec.Balance = tran.Balance + pAmount;
                rec.Date = DateTime.Now.ToUniversalTime();
                if (pDriverPaymentID.HasValue) {
                    rec.DriverPaymentID = pDriverPaymentID.Value;

                    TimeZoneConverter convert = new TimeZoneConverter();
                    rec.Date = convert.FromUniversalTime(rec.DriverPayments.Affiliate.TimeZone, rec.Date);
                }
                rec.Save();

            } catch (Exception ex) {
                throw ex;
            }
            return rec;
        }


        [Editable(false)]
        public DateTime MyLocalTime {
            get {
                if (this.TaxiPassCards.TimeZone.IsNullOrEmpty()) {
                    return this.Date;
                }
                TimeZoneConverter oTimeZone = new TimeZoneConverter();
                return oTimeZone.FromUniversalTime(this.TaxiPassCards.TimeZone, this.Date);
            }
        }

        [Editable(false)]
        public decimal Credit {
            get {
                if (Amount > 0) {
                    return Amount;
                }
                return 0;
            }
        }

        [Editable(false)]
        public decimal Debit {
            get {
                if (Amount < 0) {
                    return Math.Abs(Amount);
                }
                return 0;
            }
        }


        public static TaxiPassCardTransactions GetLastTrans(TaxiPassCards pTaxiPassCard) {
            string sql = @"SELECT TOP 1 * FROM TaxiPassCardTransactions
                            WHERE TaxiPassCardID = @TaxiPassCardID
                            ORDER BY TaxiPassCardTransID DESC";

            using (var conn = SqlHelper.OpenSqlConnection()) {
                return conn.Get<TaxiPassCardTransactions>(sql, new { TaxiPassCardID = pTaxiPassCard.TaxiPassCardID });
            }
        }
    }
}