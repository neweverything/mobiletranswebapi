using IdeaBlade.Persistence;
using IdeaBlade.Persistence.Rdb;
using System;
using System.Data;
using System.Text;
using TaxiPassCommon;

namespace CabRideEngine {
	/// <summary>
	/// The developer class representing the MasterVoucher business object class.
	/// </summary>
	/// <remarks>
	/// <para>Place all application-specific business logic in this class.</para>
	/// <para>
	/// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
	/// and will not overwrite changes.
	/// </para>
	/// <para>
	/// This class inherits from the generated class, its companion in the pair of classes 
	/// defining a business object. Properties in the generated class may be overridden in 
	/// order to change the behavior or add business logic.
	/// </para>
	/// <para>
	/// This class is the final class in the inheritance chain for this business object. 
	/// It should not be inherited.
	/// </para>
	/// </remarks>
	[Serializable]
	public sealed partial class MasterVoucher : MasterVoucherDataRow {

		#region Constructors -- DO NOT MODIFY

		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new';
		// it is also required if the class is visible from a web service.
		// DO NOT REMOVE **
		private MasterVoucher() : this(null) { }

		/// <summary>
		/// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
		/// </summary>
		/// <param name="pRowBuilder"></param>
		public MasterVoucher(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static MasterVoucher Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      MasterVoucher aMasterVoucher = pManager.CreateEntity<MasterVoucher>();
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aMasterVoucher, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aMasterVoucher.AddToManager();
		//      return aMasterVoucher;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		// Add additional logic to your business object here...
		#region Standard Entity logic to create/audit/validate the entity

		private const int MAX_TRANSNO = 999999999;

		// Add additional logic to your business object here...
		public static MasterVoucher Create(Drivers pDriver) {
			MasterVoucher rec = null;
			try {
				rec = pDriver.PersistenceManager.CreateEntity<MasterVoucher>();
				rec.DriverID = pDriver.DriverID;
				rec.AddToManager();

				rec.TransNo = CreateTransNo();
			} catch (Exception ex) {
				throw ex;
			}
			return rec;
		}

		// *************************************************************
		// Create a transNo for the master voucher
		// *************************************************************
		public static string CreateTransNo() {
			Random rnd = new Random(unchecked((int)DateTime.Now.Ticks));
			return rnd.Next(MAX_TRANSNO).ToString().PadLeft(9, '0');
		}

		protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
			if (IsDeserializing) {
				return;
			}
			base.OnColumnChanging(pArgs);
			DoAudit(pArgs);
		}

		private void DoAudit(DataColumnChangeEventArgs pArgs) {
			if (!this.PersistenceManager.IsClient) {
				return;
			}
			if (this.RowState == DataRowState.Added) {
				return;
			}
			if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
				AuditRecord = MasterVoucherAudit.Create(this);
			}
		}



		public override SaveResult Save() {
			return Save(LoginManager.PrincipleUser.Identity.Name);
		}


		// ********************************************************************************
		// 
		// ********************************************************************************
		public override SaveResult Save(string sModifiedBy) {

			//ensure that the TransNo is unique
			if (this.RowState == DataRowState.Added) {

				Random rnd = new Random(unchecked((int)DateTime.Now.Ticks));
				while (true) {
					RdbQuery qry = new RdbQuery(typeof(MasterVoucher), TransNoEntityColumn, EntityQueryOp.EQ, this.TransNo);
					qry.AddClause(MasterVoucher.MasterVoucherIDEntityColumn, EntityQueryOp.NE, this.MasterVoucherID);
					EntityList<MasterVoucher> oList = this.PersistenceManager.GetEntities<MasterVoucher>(qry, QueryStrategy.DataSourceThenCache);
					if (oList.Count < 1) {
						break;
					}
					this.TransNo = rnd.Next(MAX_TRANSNO).ToString().PadLeft(9, '0');
				}
			}

			PersistenceManager oPM = this.PersistenceManager;
			EntityList<Entity> saveList = new EntityList<Entity>();
			saveList.ShouldRemoveDeletedEntities = false;

			saveList.Add(this);

			//update modified by and date
			this.ModifiedDate = DateTime.Now.ToUniversalTime();

			//try to save 3 times incase of deadlocks or timeouts
			int saveCnt = 0;
			SaveResult result = null;
			while (saveCnt < 3) {
				try {
					result = this.PersistenceManager.SaveChanges(saveList);
					break;
				} catch (PersistenceManagerSaveException ex) {
					if (saveCnt >= 2) {
						SaveError(ex);
						//throw ex;
						break;
					}
					saveCnt++;
				}
			}

			return result;
		}

		public override DateTime CreatedDate {
			get {
				return base.CreatedDate;
			}
			set {
				base.CreatedDate = value;
				if (this.Drivers.MyAffiliate != null && this.Drivers.MyAffiliate.TimeZone != null) {
					TimeZoneConverter oTimeZone = new TimeZoneConverter();
					base.CreatedDate = oTimeZone.ConvertTime(value, TimeZone.CurrentTimeZone.StandardName, this.Drivers.MyAffiliate.TimeZone);
				}

			}
		}

		public override DateTime ModifiedDate {
			get {
				return base.ModifiedDate;
			}
			set {
				base.ModifiedDate = value;
				if (this.Drivers.MyAffiliate.TimeZone != null) {
					TimeZoneConverter oTimeZone = new TimeZoneConverter();
					base.ModifiedDate = oTimeZone.ConvertTime(value, TimeZone.CurrentTimeZone.StandardName, this.Drivers.MyAffiliate.TimeZone);
				}
			}
		}

		public void SaveError(PersistenceManagerSaveException ex) {
			StringBuilder sHtml = new StringBuilder();
			sHtml.AppendLine("<hr noshade color=#000000 size=1>");
			sHtml.AppendLine(String.Format("{0} MasterVoucher Save Error ({1})<br>", DateTime.Now.ToString(), this.TransNo));
			sHtml.AppendLine("<br>** Error Info **<br>");
			sHtml.AppendLine(String.Format("<br>TransNo: {0}", this.TransNo));
			sHtml.AppendLine("Message: " + ex.Message + "<br>");
			if (ex.InnerException != null) {
				sHtml.AppendLine("Inner Message: " + ex.InnerException.Message + "<br>");
			}
			sHtml.AppendLine("<br>Stack Trace: " + ex.StackTrace + "<br>");
			sHtml.AppendLine("Entities with Errors<br><br>");
			if (ex.SaveResult != null && ex.SaveResult.EntitiesWithErrors != null) {
				foreach (Entity x in ex.SaveResult.EntitiesWithErrors) {
					sHtml.AppendLine(ObjectDisplayer.ObjectToXMLString(x, "").EncryptIceKey());
					sHtml.AppendLine("<br><br><br>");
				}
			}

			try {
				CabRideEngineDapper.SystemDefaultsDict dict = CabRideEngineDapper.SystemDefaultsDict.GetByKey("EMail", "Receipt", false);
				string[] cred = dict.ValueString.Decrypt().Split('|');
				string from = cred[0];
				string password = cred[1];

				TaxiPassCommon.Mail.EMailMessage oMail = new TaxiPassCommon.Mail.EMailMessage(from, String.Format("MasterVoucher Save Error ({0})", this.TransNo), sHtml.ToString(), password);
				oMail.SendMail(Properties.Settings.Default.EMailErrorsTo);
			} catch (Exception) {
			}
		}

		#endregion


		public static MasterVoucher GetVoucher(PersistenceManager pPM, string pVoucherNo) {
			RdbQuery qry = new RdbQuery(typeof(MasterVoucher), MasterVoucher.TransNoEntityColumn, EntityQueryOp.EQ, pVoucherNo);
			return pPM.GetEntity<MasterVoucher>(qry);

		}

	}

	#region EntityPropertyDescriptors.MasterVoucherPropertyDescriptor
	////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
	////    For example: 
	////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
	//public static partial class EntityPropertyDescriptors {
	//	public partial class MasterVoucherPropertyDescriptor : BaseEntityPropertyDescriptor {
	//    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
	//    public AdaptedPropertyDescriptor Age {
	//      get { return this.Get("Age"); }
	//    }
	//  }
	//} 
	#endregion


}
