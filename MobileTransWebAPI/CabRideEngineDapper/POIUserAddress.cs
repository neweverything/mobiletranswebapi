﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Dapper;

using TaxiPassCommon;

using CabRideEngineDapper.Utils;
using System.Data.SqlClient;

namespace CabRideEngineDapper {
	public partial class POIUserAddress {

		public static POIUserAddress Create(POIUsers pUser) {
			POIUserAddress rec = POIUserAddress.Create();
			rec.POIUserID = pUser.POIUserID;
			return rec;
		}

		#region Save
		public new void Save(SqlConnection pConn, string pModifiedBy) {
			if (IsNewRecord) {
				CreatedBy = pModifiedBy;
				CreatedDate = DateTime.UtcNow;
				ModifiedDate = CreatedDate;
				ModifiedBy = CreatedBy;

				int? id = pConn.Insert(this);
				this.POIUserAddressID = id.Value;
			} else {
				ModifiedBy = pModifiedBy;
				ModifiedDate = DateTime.UtcNow;
				pConn.Update(this);
			}
		}
		#endregion


		public static POIUserAddress GetByStreet(int pUserID, string pStreetName) {
			//where Name = @Name", new {Name = new DbString { Value = "abcde", IsFixedLength = true, Length = 10, IsAnsi = true });
			//string sql = string.Format( "SELECT * FROM {0} WHERE {1} = @{1} AND {2} = @{2}", POIUserAddressTableDef.TABLE_NAME, POIUserAddressTableDef.POIUserID, POIUserAddressTableDef.Street);
			//DynamicParameters param = new DynamicParameters();
			//param.Add(POIUserAddressTableDef.POIUserID, pUserID);
			//param.Add(POIUserAddressTableDef.Street, pStreetName);

			using (var conn = SqlHelper.OpenSqlConnection()) {
				//POIUserAddress rec = conn.Query<POIUserAddress>(sql, param).DefaultIfEmpty(POIUserAddress.GetNullEntity()).FirstOrDefault();
				POIUserAddress rec = conn.GetList<POIUserAddress>(new { POIUserID = pUserID, Street = pStreetName }).DefaultIfEmpty(POIUserAddress.GetNullEntity()).FirstOrDefault();
				return rec;
			}
		}

	}
}
