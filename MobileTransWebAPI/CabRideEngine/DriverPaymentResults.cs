﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Util;
using IdeaBlade.Persistence.Rdb;

using CabRideEngine.Utils;
using TaxiPassCommon;
using TaxiPassCommon.Banking;

namespace CabRideEngine {
	/// <summary>
	/// The developer class representing the DriverPaymentResults business object class.
	/// </summary>
	/// <remarks>
	/// <para>Place all application-specific business logic in this class.</para>
	/// <para>
	/// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
	/// and will not overwrite changes.
	/// </para>
	/// <para>
	/// This class inherits from the generated class, its companion in the pair of classes 
	/// defining a business object. Properties in the generated class may be overridden in 
	/// order to change the behavior or add business logic.
	/// </para>
	/// <para>
	/// This class is the final class in the inheritance chain for this business object. 
	/// It should not be inherited.
	/// </para>
	/// </remarks>
	[Serializable]
	public sealed partial class DriverPaymentResults : DriverPaymentResultsDataRow {

		#region Constructors -- DO NOT MODIFY

		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new';
		// it is also required if the class is visible from a web service.
		// DO NOT REMOVE **
		private DriverPaymentResults()
			: this(null) {
		}

		/// <summary>
		/// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
		/// </summary>
		/// <param name="pRowBuilder"></param>
		public DriverPaymentResults(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static DriverPaymentResults Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      DriverPaymentResults aDriverPaymentResults = pManager.CreateEntity<DriverPaymentResults>();
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aDriverPaymentResults, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aDriverPaymentResults.AddToManager();
		//      return aDriverPaymentResults;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		#region Standard Entity logic to create/audit/validate the entity
		// Add additional logic to your business object here...
		public static DriverPaymentResults Create(DriverPayments pDriverPayment) {
			DriverPaymentResults oResult = null;
			try {
				oResult = pDriverPayment.PersistenceManager.CreateEntity<DriverPaymentResults>();

				// Using the IdeaBlade Id Generation technique
				pDriverPayment.PersistenceManager.GenerateId(oResult, DriverPaymentResults.DriverPaymentResultIDEntityColumn);
				oResult.DriverPaymentID = pDriverPayment.DriverPaymentID;
				oResult.AddToManager();

			} catch (Exception ex) {
				throw ex;
			}
			return oResult;
		}

		protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
			if (IsDeserializing) {
				return;
			}
			base.OnColumnChanging(pArgs);
			DoAudit(pArgs);
		}

		private void DoAudit(DataColumnChangeEventArgs pArgs) {
			if (!this.PersistenceManager.IsClient) {
				return;
			}
			if (this.RowState == DataRowState.Added) {
				return;
			}
			if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
				AuditRecord = DriverPaymentResultsAudit.Create(this.PersistenceManager, this);
			}
		}
		#endregion

		#region Override properties
		public override DateTime? TransDate {
			get {
				return base.TransDate;
			}
			set {
				base.TransDate = value;
				if (value.HasValue && !this.DriverPayments.Affiliate.TimeZone.IsNullOrEmpty()) {
					TimeZoneConverter convert = new TimeZoneConverter();
					base.TransDate = convert.ConvertTime(value.Value, TimeZone.CurrentTimeZone.StandardName, this.DriverPayments.Affiliate.TimeZone);
				}
			}
		}
		#endregion

		#region Override properties
		public DateTime? TransDateLocalTime {
			get {
				if (!TransDate.HasValue) {
					return TransDate;
				}
				if (this.DriverPayments.Drivers.MyAffiliate == null && string.IsNullOrEmpty(this.DriverPayments.Drivers.MyAffiliate.TimeZone)) {
					return TransDate;
				}
				TimeZoneConverter oTime = new TimeZoneConverter();
				return oTime.FromUniversalTime(this.DriverPayments.Drivers.MyAffiliate.TimeZone, TransDate.Value);
			}
		}
		#endregion

		#region Retrieve Records

		public static DriverPaymentResults GetPaymentResult(PersistenceManager pPM, long pDriverPaymentResultID) {
			RdbQuery qry = new RdbQuery(typeof(DriverPaymentResults), DriverPaymentResultIDEntityColumn, EntityQueryOp.EQ, pDriverPaymentResultID);
			return pPM.GetEntity<DriverPaymentResults>(qry);
		}

		public static EntityList<DriverPaymentResults> GetPaymentResults(PersistenceManager pPM, long pDriverPaymentID) {
			RdbQuery qry = new RdbQuery(typeof(DriverPaymentResults), DriverPaymentIDEntityColumn, EntityQueryOp.EQ, pDriverPaymentID);
			return pPM.GetEntities<DriverPaymentResults>(qry);
		}

		public static DriverPaymentResults GetSaledPaymentResult(PersistenceManager pPM, long pDriverPaymentID) {
			RdbQuery qry = new RdbQuery(typeof(DriverPaymentResults), DriverPaymentIDEntityColumn, EntityQueryOp.EQ, pDriverPaymentID);
			EntityList<DriverPaymentResults> list = pPM.GetEntities<DriverPaymentResults>(qry, QueryStrategy.DataSourceThenCache);
			DriverPaymentResults result = pPM.GetNullEntity<DriverPaymentResults>();
			foreach (DriverPaymentResults rec in list) {
				if ("DS".Contains(rec.TransType)) {
					result = rec;
					if (result.Result.Trim() == "0") {
						break;
					}
				}
			}
			return result;
		}

		public static DriverPaymentResults GetAuthApprovedResult(DriverPayments pPayment) {
			RdbQuery qry = new RdbQuery(typeof(DriverPaymentResults), DriverPaymentResults.DriverPaymentIDEntityColumn, EntityQueryOp.EQ, pPayment.DriverPaymentID);
			qry.AddClause(DriverPaymentResults.TransTypeEntityColumn, EntityQueryOp.EQ, "A");
			qry.AddClause(ResultEntityColumn, EntityQueryOp.EQ, "0");
			qry.AddOrderBy(DriverPaymentResults.DriverPaymentResultIDEntityColumn, System.ComponentModel.ListSortDirection.Descending);
			qry.Top = 1;
			return pPayment.PersistenceManager.GetEntity<DriverPaymentResults>(qry);
		}

		public static DriverPaymentResults GetSaledApprovedResult(DriverPayments pPayment) {
			RdbQuery qry = new RdbQuery(typeof(DriverPaymentResults), DriverPaymentResults.DriverPaymentIDEntityColumn, EntityQueryOp.EQ, pPayment.DriverPaymentID);
			qry.AddClause(DriverPaymentResults.TransTypeEntityColumn, EntityQueryOp.In, new string[] { "D", "S" });
			qry.AddClause(ResultEntityColumn, EntityQueryOp.EQ, "0");
			qry.AddOrderBy(DriverPaymentResults.DriverPaymentResultIDEntityColumn, System.ComponentModel.ListSortDirection.Descending);
			qry.Top = 1;
			return pPayment.PersistenceManager.GetEntity<DriverPaymentResults>(qry);
		}


		public static EntityList<DriverPaymentResults> GetByRefNum(PersistenceManager pPM, string pRefNo, CardProcessors pCardProcessor) {
			RdbQuery qry = new RdbQuery(typeof(DriverPaymentResults), DriverPaymentResults.ReferenceEntityColumn, EntityQueryOp.EQ, pRefNo);
			qry.AddClause(DriverPaymentResults.CreditCardProcessorEntityColumn, EntityQueryOp.EQ, pCardProcessor.ToString());
			qry.AddOrderBy(DriverPaymentResults.TransDateEntityColumn);

			return pPM.GetEntities<DriverPaymentResults>(qry);
		}
		#endregion

	}

	#region EntityPropertyDescriptors.DriverPaymentResultsPropertyDescriptor
	////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
	////    For example: 
	////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
	//public static partial class EntityPropertyDescriptors {
	//	public partial class DriverPaymentResultsPropertyDescriptor : BaseEntityPropertyDescriptor {
	//    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
	//    public AdaptedPropertyDescriptor Age {
	//      get { return this.Get("Age"); }
	//    }
	//  }
	//} 
	#endregion

}
