﻿using Dapper;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace CabRideEngineDapper.StoredProc {
	public class usp_SystemDefaultsDialie {

		public string StoreTrackData { get; set; }
		public string RunChargeAsManualTransaction { get; set; }
		public string ConvertToSimpleTrackData { get; set; }
		public string SwitchToManual { get; set; }

		public static usp_SystemDefaultsDialie Execute(SqlConnection pConn, string pGroup) {
			return pConn.Query<usp_SystemDefaultsDialie>("usp_SystemDefaultsDict_Flattened", new { ReferenceGroup = pGroup }, commandType: CommandType.StoredProcedure).DefaultIfEmpty(new usp_SystemDefaultsDialie()).FirstOrDefault();
		}

	}
}
