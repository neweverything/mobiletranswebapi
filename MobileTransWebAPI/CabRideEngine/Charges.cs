﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

using Csla.Validation;

namespace CabRideEngine {
    [Serializable]
    public sealed class Charges : ChargesDataRow {

        //private decimal mTotal = 0;

        #region Constructors -- DO NOT MODIFY
        // Do not create constructors for this class
        // Developers cannot create instances of this class with the "new" keyword
        // You should write a static Create method instead; see the "Suggested Customization" region
        // Please review the documentation to learn about entity creation and inheritance

        // This private constructor prevents accidental instance creation via 'new'
        private Charges() : this(null) {
        }

        // Typed DataSet constructor (needed by framework) - DO NOT REMOVE
        public Charges(DataRowBuilder pRowBuilder)
            : base(pRowBuilder) {
        }

        #endregion

        #region Suggested Customizations

        //    // Use this factory method template to create new instances of this class
        //    public static Charges Create(PersistenceManager pManager,
        //      ... your creation parameters here ...) { 
        //
        //      Charges aCharges = pManager.CreateEntity<Charges>();
        //
        //      // if this object type requires a unique id and you have implemented
        //      // the IIdGenerator interface implement the following line
        //      pManager.GenerateId(aCharges, // add id column here //);
        //
        //      // Add custom code here
        //
        //      aCharges.AddToManager();
        //      return aCharges;
        //    }

        //    // Implement this method if you want your object to sort in a specific order
        //    public override int CompareTo(Object pObject) {
        //    }

        //    // Implement this method to customize the null object version of this class
        //    protected override void UpdateNullEntity() {
        //    }

        #endregion

        // Add additional logic to your business object here...
        public static Charges Create(PersistenceManager pManager, long pReservationID) {

            Charges oCharge = (Charges)pManager.CreateEntity(typeof(Charges));

            pManager.GenerateId(oCharge, Charges.ChargeIDEntityColumn);
            oCharge.ReservationID = pReservationID;
            oCharge.DisplayOrder = 0;
            oCharge.AddToManager();
            return oCharge;
        }

        protected override void AddRules(RuleList pList) {
            PropertyRequiredRule.AddToList(pList, DescriptionColumn.ColumnName);
        }

        protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
            if (IsDeserializing)
                return;
            base.OnColumnChanging(pArgs);
            DoAudit(pArgs);
        }

        private void DoAudit(DataColumnChangeEventArgs pArgs) {
            if (!this.PersistenceManager.IsClient) {
                return;
            }
            if (this.RowState == DataRowState.Added) {
                return;
            }
            if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
                AuditRecord = ChargesAudit.Create(this.PersistenceManager, this);
            }
            //AuditRecord[pArgs.Column.ColumnName] = pArgs.ProposedValue;
        }

        public override decimal Amount {
            get {
                return base.Amount;
            }
            set {
                //force to 2 decimal places
                base.Amount = Math.Round(value, 2);
            }
        }
        private static void SetCharge(Reservations pReserve, string pChargeType, decimal pAmount, byte pDisplayOrder, bool pSaveCharge, string pModifiedBy ) {

            if (Convert.ToBoolean(pReserve.ExternalCharges)) {
                return;
            }
            PersistenceManager oPM = pReserve.PersistenceManager;
            long reserve = pReserve.ReservationID;
            RdbQuery oQry = new RdbQuery(typeof(Charges), Charges.ReservationIDEntityColumn, EntityQueryOp.EQ, reserve);
            oQry.AddClause(Charges.DescriptionEntityColumn, EntityQueryOp.EQ, pChargeType);
            Charges oCharge = oPM.GetEntity<Charges>(oQry);
            if (oCharge.IsNullEntity) {
                oCharge = Charges.Create(oPM, reserve);
            }
            if (!oCharge.ExternalCalc) {
                oCharge.Description = pChargeType;
                oCharge.Amount = pAmount;
                oCharge.DisplayOrder = pDisplayOrder;
                oCharge.ExternalCalc = false;
            }
            if (pSaveCharge) {
                oCharge.Save(pModifiedBy);
                oCharge.RemoveFromManager();
            }
        }


        public static void FillCharges(Reservations pReserve, PricedRide pFare) {
            try {
                FillCharges(pReserve, pFare, false, "");
            } catch (Exception ex) {
                throw ex;
            }
        }

        public static void FillCharges(Reservations pReserve, PricedRide pFare, bool pSaveCharge, string pModifiedBy) {
            try {
                SetCharge(pReserve, ChargeTypes.BaseFee, pFare.BasePrice - pFare.DropFee - pFare.AirportFee - pFare.OtherFee - pFare.TaxiPassFee - pFare.VoucherFee, 0, pSaveCharge, pModifiedBy);
                SetCharge(pReserve, ChargeTypes.AirportFee, pFare.AirportFee, 1, pSaveCharge, pModifiedBy);
                SetCharge(pReserve, ChargeTypes.DropFee, pFare.DropFee, 2, pSaveCharge, pModifiedBy);
                SetCharge(pReserve, ChargeTypes.OtherFee, pFare.OtherFee, 3, pSaveCharge, pModifiedBy);
                SetCharge(pReserve, ChargeTypes.ServiceFee, pFare.ServiceFee, 4, pSaveCharge, pModifiedBy);
                SetCharge(pReserve, ChargeTypes.BookingFee, pFare.BookingFee, 5, pSaveCharge, pModifiedBy);
                SetCharge(pReserve, ChargeTypes.TollsFee, pFare.TollsFee, 6, pSaveCharge, pModifiedBy);
                SetCharge(pReserve, ChargeTypes.VoucherFee, pFare.VoucherFee, 7, pSaveCharge, pModifiedBy);
                SetCharge(pReserve, ChargeTypes.Gratuity, pFare.Gratuity, 8, pSaveCharge, pModifiedBy);
                SetCharge(pReserve, ChargeTypes.TaxiPassFee, pFare.TaxiPassFee, 9, pSaveCharge, pModifiedBy);
                SetCharge(pReserve, ChargeTypes.Discount, pFare.DiscountFee, 10, pSaveCharge, pModifiedBy);
            } catch (Exception ex) {
                throw ex;
            }
        }

        public static Charges GetCharge(Reservations pReserve, string pDescription) {
            PersistenceManager oPM = pReserve.PersistenceManager;
            RdbQuery qry = new RdbQuery(typeof(Charges), Charges.ReservationIDEntityColumn, EntityQueryOp.EQ, pReserve.ReservationID);
            qry.AddClause(Charges.DescriptionEntityColumn, EntityQueryOp.EQ, pDescription);
            return oPM.GetEntity<Charges>(qry);
        }


    }

}
