﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Dapper;
using TaxiPassCommon;

using CabRideEngineDapper.Utils;

namespace CabRideEngineDapper {
	public partial class SalesRepAffiliates {

		private Affiliate mAffiliate;
		public Affiliate Affiliate {
			get {
				if (mAffiliate == null) {
					mAffiliate = Affiliate.GetAffiliate(this.AffiliateID);
				}
				return mAffiliate;
			}
		}

		public static List<SalesRepAffiliates> GetSalesRepAffiliatesBySalesRepID(long pSalesRepID) {
			using (var conn = SqlHelper.OpenSqlConnection()) {
				string sql = "select * from SalesRepAffiliates with (nolock) where SalesRepID = @SalesRepID";
				return conn.Query<SalesRepAffiliates>(sql, new { SalesRepID = pSalesRepID }).ToList();
			}
		}

	}
}
