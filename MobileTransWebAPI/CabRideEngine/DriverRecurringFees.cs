﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

using CabRideEngine.Utils;
using TaxiPassCommon;
using TaxiPassCommon.Banking;

using Csla.Validation;

namespace CabRideEngine {
    /// <summary>
    /// The developer class representing the DriverRecurringFees business object class.
    /// </summary>
    /// <remarks>
    /// <para>Place all application-specific business logic in this class.</para>
    /// <para>
    /// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
    /// and will not overwrite changes.
    /// </para>
    /// <para>
    /// This class inherits from the generated class, its companion in the pair of classes 
    /// defining a business object. Properties in the generated class may be overridden in 
    /// order to change the behavior or add business logic.
    /// </para>
    /// <para>
    /// This class is the final class in the inheritance chain for this business object. 
    /// It should not be inherited.
    /// </para>
    /// </remarks>
    [Serializable]
    public sealed partial class DriverRecurringFees : DriverRecurringFeesDataRow {

        #region Constructors -- DO NOT MODIFY

        // Do not create constructors for this class
        // Developers cannot create instances of this class with the "new" keyword
        // You should write a static Create method instead; see the "Suggested Customization" region
        // Please review the documentation to learn about entity creation and inheritance

        // This private constructor prevents accidental instance creation via 'new';
        // it is also required if the class is visible from a web service.
        // DO NOT REMOVE **
        private DriverRecurringFees() : this(null) { }

        /// <summary>
        /// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
        /// </summary>
        /// <param name="pRowBuilder"></param>
        public DriverRecurringFees(DataRowBuilder pRowBuilder)
            : base(pRowBuilder) {
        }

        #endregion

        #region Suggested Customizations

        //    // Use this factory method template to create new instances of this class
        //    public static DriverRecurringFees Create(PersistenceManager pManager,
        //      ... your creation parameters here ...) { 
        //
        //      DriverRecurringFees aDriverRecurringFees = pManager.CreateEntity<DriverRecurringFees>();
        //
        //      // if this object type requires a unique id and you have implemented
        //      // the IIdGenerator interface implement the following line
        //      pManager.GenerateId(aDriverRecurringFees, // add id column here //);
        //
        //      // Add custom code here
        //
        //      aDriverRecurringFees.AddToManager();
        //      return aDriverRecurringFees;
        //    }

        //    // Implement this method if you want your object to sort in a specific order
        //    public override int CompareTo(Object pObject) {
        //    }

        //    // Implement this method to customize the null object version of this class
        //    protected override void UpdateNullEntity() {
        //    }

        #endregion

        // Add additional logic to your business object here...

        #region Standard Entity logic to create/audit/validate the entity

        // Add additional logic to your business object here...
        public static DriverRecurringFees Create(Drivers pDriver) {
            PersistenceManager oPM = pDriver.PersistenceManager;

            DriverRecurringFees rec = null;

            try {
                rec = oPM.CreateEntity<DriverRecurringFees>();

                // Using the IdeaBlade Id Generation technique
                oPM.GenerateId(rec, DriverRecurringFees.DriverRecurringFeeIDEntityColumn);
                rec.DriverID = pDriver.DriverID;
                TimeZoneConverter convert = new TimeZoneConverter();
                rec.StartDate =  DateTime.Today;
                rec.AddToManager();

            } catch (Exception ex) {
                throw ex;
            }
            return rec;
        }

        protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
            if (IsDeserializing) {
                return;
            }
            base.OnColumnChanging(pArgs);
            DoAudit(pArgs);
        }

        private void DoAudit(DataColumnChangeEventArgs pArgs) {
            if (!this.PersistenceManager.IsClient) {
                return;
            }
            if (this.RowState == DataRowState.Added) {
                return;
            }
            if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
                AuditRecord = DriverRecurringFeesAudit.Create(this.PersistenceManager, this);
            }
        }

        protected override void AddRules(RuleList pList) {
            pList.Add(IsExpenseTypeUnique, ExpenseTypeIDEntityColumn.ColumnName);
        }

        private bool IsExpenseTypeUnique(object pTarget, RuleArgs e) {
            DriverRecurringFees rec = pTarget as DriverRecurringFees;

            RdbQuery qry = new RdbQuery(typeof(DriverRecurringFees), DriverRecurringFees.ExpenseTypeIDEntityColumn, EntityQueryOp.EQ, rec.ExpenseTypeID);
            qry.AddClause(DriverRecurringFees.DriverIDEntityColumn, EntityQueryOp.EQ, rec.DriverID);
            qry.AddClause(DriverRecurringFees.DriverRecurringFeeIDEntityColumn, EntityQueryOp.NE, rec.DriverRecurringFeeID);
            EntityList<DriverRecurringFees> oList = this.PersistenceManager.GetEntities<DriverRecurringFees>(qry);
            if (oList.Count > 0) {
                e.Description = "A recurring fee already exists for " + rec.ExpenseTypes.Description;
                return false;
            }

            return true;
        }
        #endregion

        public static EntityList<DriverRecurringFees> GetActiveFees(PersistenceManager pPM) {
            RdbQuery qry = new RdbQuery(typeof(DriverRecurringFees), DriverRecurringFees.CancelledEntityColumn, EntityQueryOp.EQ, false);
            qry.AddClause(DriverRecurringFees.StartDateEntityColumn, EntityQueryOp.LE, DateTime.Now);
            qry.AddOrderBy(DriverRecurringFees.DriverIDEntityColumn);
            return pPM.GetEntities<DriverRecurringFees>(qry, QueryStrategy.DataSourceThenCache);
        }

        public DateTime? GetLastRecurringFeeDate() {
            RdbQuery qry = new RdbQuery(typeof(DriverPayments), DriverPayments.ChargeByEntityColumn, EntityQueryOp.EQ, DriverPayments.RECURRING_FEES);
            qry.AddClause(DriverPayments.ReferenceNoEntityColumn, EntityQueryOp.EQ, "D" + this.DriverRecurringFeeID.ToString("F0"));
            qry.AddClause(DriverPayments.DriverIDEntityColumn, EntityQueryOp.EQ, this.DriverID);
            qry.AddOrderBy(DriverPayments.ChargeDateEntityColumn, System.ComponentModel.ListSortDirection.Descending);

            qry.Top = 1;
            return this.PersistenceManager.GetEntity<DriverPayments>(qry, QueryStrategy.DataSourceThenCache).ChargeBackDate;
        }


    }

    #region EntityPropertyDescriptors.DriverRecurringFeesPropertyDescriptor
    ////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
    ////    For example: 
    ////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
    //public static partial class EntityPropertyDescriptors {
    //	public partial class DriverRecurringFeesPropertyDescriptor : BaseEntityPropertyDescriptor {
    //    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
    //    public AdaptedPropertyDescriptor Age {
    //      get { return this.Get("Age"); }
    //    }
    //  }
    //} 
    #endregion

}