﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using System.Text;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

using CabRideEngine.Utils;
using Csla.Validation;

namespace CabRideEngine {
	/// <summary>
	/// The developer class representing the RedeemerShiftVouchers business object class.
	/// </summary>
	/// <remarks>
	/// <para>Place all application-specific business logic in this class.</para>
	/// <para>
	/// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
	/// and will not overwrite changes.
	/// </para>
	/// <para>
	/// This class inherits from the generated class, its companion in the pair of classes 
	/// defining a business object. Properties in the generated class may be overridden in 
	/// order to change the behavior or add business logic.
	/// </para>
	/// <para>
	/// This class is the final class in the inheritance chain for this business object. 
	/// It should not be inherited.
	/// </para>
	/// </remarks>
	[Serializable]
	public sealed partial class RedeemerShiftVouchers : RedeemerShiftVouchersDataRow {

		#region Constructors -- DO NOT MODIFY

		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new';
		// it is also required if the class is visible from a web service.
		// DO NOT REMOVE **
		private RedeemerShiftVouchers() : this(null) { }

		/// <summary>
		/// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
		/// </summary>
		/// <param name="pRowBuilder"></param>
		public RedeemerShiftVouchers(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static RedeemerShiftVouchers Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      RedeemerShiftVouchers aRedeemerShiftVouchers = pManager.CreateEntity<RedeemerShiftVouchers>();
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aRedeemerShiftVouchers, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aRedeemerShiftVouchers.AddToManager();
		//      return aRedeemerShiftVouchers;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion
		public static RedeemerShiftVouchers GetOrCreate(DriverPayments pPayment) {

			RedeemerShiftVouchers rec = GetByDriverPaymentID(pPayment.PersistenceManager, pPayment.DriverPaymentID);
			if (rec.IsNullEntity) {
				rec = pPayment.PersistenceManager.CreateEntity<RedeemerShiftVouchers>();
				rec.DriverPaymentID = pPayment.DriverPaymentID;
				rec.AddToManager();
			}

			return rec;
		}

		public static RedeemerShiftVouchers GetByDriverPaymentID(PersistenceManager pPM, long pDriverPaymentID) {
			RdbQuery qry = new RdbQuery(typeof(RedeemerShiftVouchers), RedeemerShiftVouchers.DriverPaymentIDEntityColumn, EntityQueryOp.EQ, pDriverPaymentID);
			return pPM.GetEntity<RedeemerShiftVouchers>(qry);
		}


		// Add additional logic to your business object here...
		protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
			if (IsDeserializing) {
				return;
			}
			base.OnColumnChanging(pArgs);
			DoAudit(pArgs);
		}

		private void DoAudit(DataColumnChangeEventArgs pArgs) {
			if (!this.PersistenceManager.IsClient) {
				return;
			}
			if (this.RowState == DataRowState.Added) {
				return;
			}
			if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
				AuditRecord = RedeemerShiftVouchersAudit.Create(this.PersistenceManager, this);
			}
		}

		//protected override void AddRules(RuleList pList) {
		//    PropertyRequiredRule.AddToList(pList, RedeemerShiftVouchersAudit.RedeemerShiftIDEntityColumn.ColumnName);
		//    PropertyRequiredRule.AddToList(pList, RedeemerShiftVouchersAudit.DriverPaymentIDEntityColumn.ColumnName);
		//    PropertyRequiredRule.AddToList(pList, RedeemerShiftVouchersAudit.MatchedAmountEntityColumn.ColumnName);
		//    PropertyRequiredRule.AddToList(pList, RedeemerShiftVouchersAudit.RowVersionEntityColumn.ColumnName);
		//}


		// ***********************************************************************************
		// Lookup the RedeemerShiftVoucher record by Voucher No
		// ***********************************************************************************
		public static DataTable GetByVoucherNo(PersistenceManager pPM, string pVoucherNo) {
			StringBuilder sql = new StringBuilder();
			sql.Append("SELECT redeemershiftvouchers.redeemershiftvoucherid ");
			sql.Append(", redeemershiftvouchers.redeemershiftid ");
			sql.Append(", redeemershiftvouchers.driverpaymentid ");
			sql.Append(" FROM   driverpayments ");
			sql.Append(" INNER JOIN redeemershiftvouchers ON driverpayments.driverpaymentid = redeemershiftvouchers.driverpaymentid ");
			sql.Append(" where driverpayments.transno = '");
			sql.Append(pVoucherNo);
			sql.Append("'");
			return SQLHelper.GetRecords(pPM, sql.ToString());
		}

	}

	#region EntityPropertyDescriptors.RedeemerShiftVouchersPropertyDescriptor
	////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
	////    For example: 
	////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
	//public static partial class EntityPropertyDescriptors {
	//	public partial class RedeemerShiftVouchersPropertyDescriptor : BaseEntityPropertyDescriptor {
	//    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
	//    public AdaptedPropertyDescriptor Age {
	//      get { return this.Get("Age"); }
	//    }
	//  }
	//} 
	#endregion

}