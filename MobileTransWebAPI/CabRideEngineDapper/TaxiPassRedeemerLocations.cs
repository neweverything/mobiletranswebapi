﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Dapper;
using TaxiPassCommon;

using CabRideEngineDapper.Utils;
using System.Data.SqlClient;

namespace CabRideEngineDapper {
	public partial class TaxiPassRedeemerLocations {

		#region Related Tables
		private TaxiPassRedeemerAccounts mTaxiPassRedeemerAccounts;
		public TaxiPassRedeemerAccounts TaxiPassRedeemerAccounts {
			get {
				if (mTaxiPassRedeemerAccounts == null) {
					mTaxiPassRedeemerAccounts = TaxiPassRedeemerAccounts.GetAccount(this.TaxiPassRedeemerAccountID);
				}
				return mTaxiPassRedeemerAccounts;
			}
		}

		#endregion

		#region Custom Fields

		[Editable(false)]
		public string RedeemerAccountName {
			get {
				return this.TaxiPassRedeemerAccounts.CompanyName;
			}
		}
		#endregion


		public static TaxiPassRedeemerLocations GetLocation(long pTaxiPassRedeemerLocationID) {
			using (var conn = SqlHelper.OpenSqlConnection()) {
                return GetLocation(conn, pTaxiPassRedeemerLocationID);
			}
		}

        public static TaxiPassRedeemerLocations GetLocation(SqlConnection pConn, long pTaxiPassRedeemerLocationID) {
                return pConn.Get<TaxiPassRedeemerLocations>(pTaxiPassRedeemerLocationID);
        }
        public static List<TaxiPassRedeemerLocations> GetLocations(TaxiPassRedeemerAccounts pTaxiPassRedeemerAccount) {
			string sql = @"SELECT * FROM TaxiPassRedeemerLocations 
							WHERE TaxiPassRedeemerAccountID = @TaxiPassRedeemerAccountID
							Order By Location";
			using (var conn = SqlHelper.OpenSqlConnection()) {
				return conn.Query<TaxiPassRedeemerLocations>(sql, new { TaxiPassRedeemerAccountID = pTaxiPassRedeemerAccount.TaxiPassRedeemerAccountID }).ToList();
			}
		}

		public static List<TaxiPassRedeemerLocations> GetActiveLocations(TaxiPassRedeemerAccounts pTaxiPassRedeemerAccount) {
			string sql = @"SELECT * FROM TaxiPassRedeemerLocations
							WHERE TaxiPassRedeemerAccountID = @TaxiPassRedeemerAccountID
								AND Active = 1
							Order By Location";
			using (var conn = SqlHelper.OpenSqlConnection()) {
				return conn.Query<TaxiPassRedeemerLocations>(sql, new { TaxiPassRedeemerAccountID = pTaxiPassRedeemerAccount.TaxiPassRedeemerAccountID }).ToList();
			}
		}

		public static TaxiPassRedeemerLocations GetByStoreNo(TaxiPassRedeemerAccounts pTaxiPassRedeemerAccount, String pStoreNo) {
			string sql = @"SELECT * FROM TaxiPassRedeemerLocations
							WHERE TaxiPassRedeemerAccountID = @TaxiPassRedeemerAccountID
								AND StoreNo = @StoreNo
								AND Active = 1";
			using (var conn = SqlHelper.OpenSqlConnection()) {
				return conn.Query<TaxiPassRedeemerLocations>(sql, new { TaxiPassRedeemerAccountID = pTaxiPassRedeemerAccount.TaxiPassRedeemerAccountID, StoreNo = pStoreNo }).DefaultIfEmpty(TaxiPassRedeemerLocations.GetNullEntity()).FirstOrDefault();
			}
		}

		

	}
}
