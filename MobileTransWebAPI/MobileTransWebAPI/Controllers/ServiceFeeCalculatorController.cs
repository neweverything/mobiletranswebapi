﻿using CabRideEngineDapper;
using CabRideEngineDapper.Utils;
using MobileTransWebAPI.Models;
using Newtonsoft.Json;
using System;
using System.Web.Http;
using TaxiPassCommon;

namespace MobileTransWebAPI.Controllers {
	public class ServiceFeeCalculatorController : ApiController {


		public ServiceFeeResult Get() {

			ServiceFee fee = new ServiceFee() {
				AffiliateID = 2729,
				CashierID = 110095,
				Total = 10,
				CardNumber = "4111111111111111"
				//eCheck = true
			};

			return Get(JsonConvert.SerializeObject(fee));
		}


		// GET 
		public ServiceFeeResult Get(string pRequest) {
			ServiceFeeResult result = new ServiceFeeResult();

			ServiceFee fee = JsonConvert.DeserializeObject<ServiceFee>(pRequest);

			using (var conn = SqlHelper.OpenSqlConnection()) {
				AffiliateDrivers affDriver = AffiliateDrivers.GetDriver(conn, fee.AffiliateDriverID);

				if (affDriver.AffiliateID != fee.AffiliateID) {
					result.ErrorMsg = "Invalid Access";
					return result;
				}
			}

			try {
				using (var conn = SqlHelper.OpenSqlConnection()) {

					Drivers driver = Drivers.GetChargeCard(conn, fee.AffiliateID);
					DriverPayments pay = DriverPayments.Create(driver);
					pay.GetSetCardNumber = "" + fee.CardNumber;
					if (fee.eCheck) {
						pay.CardType = "eCheck";
					}
					pay.SetFare = fee.Total;

					result.ServiceFee = pay.TaxiPassFee;
					result.DriverFee = pay.DriverFee.GetValueOrDefault(0);
					result.CardType = $"{pay.CardType} {(pay.IsDebitCard ? "Debit" : "")}".Trim();
					result.CashierFee = result.DriverFee;
					if (result.CardType.IsNullOrEmpty()) {
						result.CardType = "Credit Card";
					}
					result.FeeDesc = DriverPayments.FeeDesc;
				}
			} catch (Exception ex) {
				result.ErrorMsg = ex.Message;
			}
			return result;
		}


	}
}
