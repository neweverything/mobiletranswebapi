﻿using Stripe;
using System;
using System.Collections.Generic;
using TaxiPassCommon.Banking.Models;

namespace TaxiPassCommon.Banking {
	public class StripeFP : IProcessor {

		private StripeCredentials mCredentials;

		public StripeFP(StripeCredentials pCredentials) {
			if (pCredentials.Timeout == 0) {
				pCredentials.Timeout = 30;
			}
			mCredentials = pCredentials;
		}

		public ProcessorResponse ChargeAuth(CCRequestRefNo pRequest) {
			var service = new StripeChargeService(mCredentials.APIKey);

			if (pRequest.TestCard) {
				service = new StripeChargeService(mCredentials.TestAPIKey);
			}


			StripeCharge response = new StripeCharge();
			try {
				response = service.Capture(pRequest.RefNum, Convert.ToInt32(pRequest.Amount * 100));
			} catch (Exception ex) {
				response.FailureCode = "99";
				response.FailureMessage = ex.Message;
				response.Status = "Declined";
			}

			return ProcessResponse(response, pRequest.Amount, TrxTypes.DelayedCapture, pRequest.TestCard);

		}

		public ProcessorResponse ChargeCard(CCRequest pRequest) {
			var service = new StripeChargeService(mCredentials.APIKey);

			if (pRequest.CardData.TestCard) {
				service = new StripeChargeService(mCredentials.TestAPIKey);
			}

			SourceCard card = FillCardInfo(pRequest);


			var myCharge = new StripeChargeCreateOptions();

			// always set these properties
			myCharge.Amount = Convert.ToInt32(pRequest.Amount * 100);
			myCharge.Currency = "usd";
			myCharge.StatementDescriptor = pRequest.AffiliateBillingDescriptor;

			// set this if you want to
			myCharge.Description = pRequest.VoucherNo;
			string salesPerson = pRequest.CardData.SalesPerson;
			if (salesPerson.IsNullOrEmpty()) {
				salesPerson = string.Format("D: {0}", pRequest.DriverID);
			}
			myCharge.Metadata = new System.Collections.Generic.Dictionary<string, string>();
			myCharge.Metadata.Add("Salesperson", salesPerson);



			// (not required) set this to false if you don't want to capture the charge yet - requires you call capture later
			myCharge.Capture = true;
			myCharge.SourceCard = card;


			StripeCharge response = new StripeCharge();
			try {
				response = service.Create(myCharge);
			} catch (Exception ex) {
				response.Status = "Declined";
				response.FailureMessage = ex.Message;
				response.FailureCode = "99";
			}

			return ProcessResponse(response, pRequest.Amount, TrxTypes.Sale, pRequest.CardData.TestCard);

		}

		public string GetTransactionHistory(DateTime pStart, DateTime pEnd) {
			throw new NotImplementedException();
		}

		public ProcessorResponse OpenCredit(CCRequest pRequest) {
			throw new NotImplementedException();
		}

		public ProcessorResponse PreAuthCard(CCRequest pRequest) {
			var service = new StripeChargeService(mCredentials.APIKey);

			if (pRequest.CardData.TestCard) {
				service = new StripeChargeService(mCredentials.TestAPIKey);
			}

			SourceCard card = FillCardInfo(pRequest);


			var myCharge = new StripeChargeCreateOptions();

			// always set these properties
			myCharge.Amount = Convert.ToInt32(pRequest.Amount * 100);
			myCharge.Currency = "usd";
			myCharge.StatementDescriptor = pRequest.AffiliateBillingDescriptor;

			// set this if you want to
			myCharge.Description = pRequest.VoucherNo;
			string salesPerson = pRequest.CardData.SalesPerson;
			if (salesPerson.IsNullOrEmpty()) {
				salesPerson = string.Format("D: {0}", pRequest.DriverID);
			}
			myCharge.Metadata = new System.Collections.Generic.Dictionary<string, string>();
			myCharge.Metadata.Add("Salesperson", salesPerson);



			// (not required) set this to false if you don't want to capture the charge yet - requires you call capture later
			myCharge.Capture = false;
			myCharge.SourceCard = card;


			StripeCharge response = new StripeCharge();
			try {
				response = service.Create(myCharge);
			} catch (Exception ex) {
				response.Status = "Declined";
				response.FailureMessage = ex.Message;
				response.FailureCode = "99";
			}

			return ProcessResponse(response, pRequest.Amount, TrxTypes.Authorization, pRequest.CardData.TestCard);

		}

		private ProcessorResponse ProcessResponse(StripeCharge pResponse, decimal pAuthAmount, string pTrxType, bool pTestCard) {
			ProcessorResponse oResult = new ProcessorResponse();

			oResult.TestCard = pTestCard;
			oResult.VerisignAccountID = mCredentials.VerisignAccountID;
			oResult.MerchantID = mCredentials.MerchantID;
			oResult.CreditCardProcessor = mCredentials.MyGateway();

			oResult.Result = pResponse.Status.Equals("succeeded", StringComparison.CurrentCultureIgnoreCase) ? "0" : pResponse.FailureCode;
			oResult.Reference = pResponse.Id;
			oResult.Message = pResponse.Status.Equals("succeeded", StringComparison.CurrentCultureIgnoreCase) ? "Approved" : pResponse.FailureMessage;
			oResult.Amount = (decimal)pResponse.Amount / 100;
			if (oResult.Amount == 0) {
				oResult.Amount = pAuthAmount;
			}
			if (oResult.Result.IsNullOrEmpty()) {
				oResult.Result = "0";
			}
			if (oResult.Result == "0" && !oResult.Message.Equals("Approved", StringComparison.CurrentCultureIgnoreCase)) {
				oResult.Result = "10";
			}
			//fixed for Chase results
			//if (oResult.Result != "0" && oResult.Message.StartsWith("Approve", StringComparison.CurrentCultureIgnoreCase)) {
			//    oResult.Result = "0";
			//}

			oResult.AuthCode = pResponse.Created.ToString();
			if (oResult.AuthCode.Length > 10) {
				oResult.AuthCode = oResult.AuthCode.Left(10);
			}
			if (pResponse.Source == null || pResponse.Source.Card == null || pResponse.Source.Card.AddressZipCheck.IsNullOrEmpty()) {
				oResult.AVS = "";
				oResult.AVSZip = "";
			} else {
				oResult.AVS = (pResponse.Source.Card.AddressLine1Check + " ").Left(1).ToUpper();
				oResult.AVSZip = (pResponse.Source.Card.AddressZipCheck + " ").Left(1).ToUpper();
			}
			try {
				oResult.CVV2Match = (pResponse.Source.Card.CvcCheck + " ").ToUpper().Left(1);
			} catch (Exception) {
			}

			oResult.TransType = pTrxType;
			oResult.TransDate = DateTime.Now;

			//// 12/5/11 Added for Carmel
			//oResult.AVSResultCode = pResponse.AvsResultCode;

			//placed as USAePay is now sending us failed trans with a result code of 0
			if (oResult.Result.Equals("0")) {
				if (!oResult.Message.IsNullOrEmpty() && !oResult.Message.Equals("Approved", StringComparison.CurrentCultureIgnoreCase)) {
					if (!oResult.Message.Equals("Unable to get tran details (p)", StringComparison.CurrentCultureIgnoreCase)) {
						oResult.Result = "1";
					}
				}
			}

			return oResult;
		}

		private ProcessorResponse ProcessResponse(StripeRefund pResponse, decimal pAuthAmount, string pTrxType, bool pTestCard) {
			ProcessorResponse oResult = new ProcessorResponse();

			oResult.TestCard = pTestCard;
			oResult.VerisignAccountID = mCredentials.VerisignAccountID;
			oResult.MerchantID = mCredentials.MerchantID;
			oResult.CreditCardProcessor = mCredentials.MyGateway();

			oResult.Result = pResponse.Status.Equals("succeeded", StringComparison.CurrentCultureIgnoreCase) ? "0" : "99";
			oResult.Reference = pResponse.Id;
			oResult.Message = pResponse.Status.Equals("succeeded", StringComparison.CurrentCultureIgnoreCase) ? "Approved" : pResponse.Reason;
			oResult.Amount = (decimal)pResponse.Amount / 100;
			if (oResult.Amount == 0) {
				oResult.Amount = pAuthAmount;
			}
			if (oResult.Result.IsNullOrEmpty()) {
				oResult.Result = "0";
			}
			if (oResult.Result == "0" && !oResult.Message.Equals("Approved", StringComparison.CurrentCultureIgnoreCase)) {
				oResult.Result = "10";
			}
			//fixed for Chase results
			//if (oResult.Result != "0" && oResult.Message.StartsWith("Approve", StringComparison.CurrentCultureIgnoreCase)) {
			//    oResult.Result = "0";
			//}

			//oResult.AuthCode = pResponse.AuthCode;
			oResult.AuthCode = pResponse.Created.ToString();
			if (oResult.AuthCode.Length > 10) {
				oResult.AuthCode = oResult.AuthCode.Left(10);
			}

			oResult.TransType = pTrxType;
			oResult.TransDate = DateTime.Now;

			//// 12/5/11 Added for Carmel
			//oResult.AVSResultCode = pResponse.AvsResultCode;

			//placed as USAePay is now sending us failed trans with a result code of 0
			if (oResult.Result.Equals("0")) {
				if (!oResult.Message.IsNullOrEmpty() && !oResult.Message.Equals("Approved", StringComparison.CurrentCultureIgnoreCase)) {
					if (!oResult.Message.Equals("Unable to get tran details (p)", StringComparison.CurrentCultureIgnoreCase)) {
						oResult.Result = "1";
					}
				}
			}

			return oResult;
		}


		private SourceCard FillCardInfo(CCRequest pRequest) {
			var card = new SourceCard {
				Number = pRequest.CardData.CardNo,
				ExpirationMonth = pRequest.CardData.CardExpiryMonth,
				ExpirationYear = pRequest.CardData.CardExpiry.Year.ToString(),
				Cvc = pRequest.CardData.CVV,
				Name = pRequest.CardData.CardHolder,
				AddressLine1 = pRequest.CardData.AvsStreet,
				AddressZip = pRequest.CardData.AvsZip
			};

			return card;
		}

		public ProcessorResponse Refund(CCRequestRefNo pRequest) {
			var service = new StripeRefundService(mCredentials.APIKey);

			if (pRequest.TestCard) {
				service = new StripeRefundService(mCredentials.TestAPIKey);
			}


			StripeRefund response = new StripeRefund();
			try {
				StripeRefundCreateOptions option = new StripeRefundCreateOptions() {
					Amount = Math.Abs(Convert.ToInt32(pRequest.Amount * 100)),
					Reason = StripeRefundReasons.Unknown
				};

				response = service.Create(pRequest.RefNum, option);
			} catch (Exception ex) {
				//response..FailureCode = "99";
				response.Reason = ex.Message;
				response.Status = "Declined";
			}

			return ProcessResponse(response, pRequest.Amount, TrxTypes.Credit, pRequest.TestCard);

		}

		public bool Void(CCRequestRefNo pRequest) {
			return Refund(pRequest).Result == "0";
		}

		public ProcessorResponse AvsCheck(CCRequest pRequest) {
			throw new NotImplementedException();
		}

		public ProcessorResponse ProcessCheck(CheckRequest pRequest) {
			throw new NotImplementedException();
		}

		public ProcessorResponse RefundCheck(CheckRequest pRequest) {
			throw new NotImplementedException();
		}

		public List<CheckStatus> GetCheckTransactionHistory(DateTime pStart, DateTime pEnd) {
			throw new NotImplementedException();
		}
	}
}
