﻿namespace MobileTransWebAPI.Models {
    public partial class TransactionRequest {
        public long AffiliateID { get; set; }
        public long AffiliateDriverID { get; set; }
        public long DriverPaymentID { get; set; }

    }
}