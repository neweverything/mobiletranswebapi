﻿using System;
using System.Net;
using TaxiPassCommon.Banking.Enums;
using TaxiPassCommon.Banking.Interfaces;
using TaxiPassCommon.com.cabpay.Services;

namespace TaxiPassCommon.Banking {
    public class TransCard : IPayCard {

        private com.TransCard.Service mService = new com.TransCard.Service();

        private Credentials mCredentials;
        private bool mProduction;

        public TransCard(IPayCardCredentials pCredentials, bool pProduction = true) {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls;
            Credentials cred = pCredentials as Credentials;

            mCredentials = cred;
            mProduction = pProduction;
            mService.Url = mCredentials.WebServiceURL;
        }

        private string ClientID {
            get {
                return mCredentials.ClientID;
            }
        }

        public string ClientPassword {
            get {
                string val = mCredentials.ClientPass;
                return val.DecryptIceKey();
            }
        }

        public string EmployeePIN {
            get {
                return mCredentials.EmpPIN;
            }
        }


        public FSV.CardRegistrationResponse RegisterCard(FSV.CardRegistration pRegistration) {
            TransCardDriverIdentification tcID = new TransCardDriverIdentification() {
                AddressLine1 = pRegistration.Address1,
                AddressLine2 = pRegistration.Address2,
                AddressLine3 = pRegistration.Address3,
                CardNumber = pRegistration.CardHolderCardID,
                City = pRegistration.City,
                CountryCode = "USA",
                DateOfBirth = pRegistration.DateOfBirth,
                Email = pRegistration.EmailAddress,
                FirstName = pRegistration.FirstName,
                IdCode = "S",
                IdCountry = "USA",
                IdState = pRegistration.State,
                IdNumber = pRegistration.SocialSecNum,
                LastName = pRegistration.LastName,
                MiddleName = pRegistration.MiddleName,
                Phone = pRegistration.MobilePhone,
                PostalCode = pRegistration.PostalCode,
                StateProvince = pRegistration.State,
            };
            if (tcID.IdNumber.IsNullOrEmpty()) {
                tcID.IdNumber = "123456789";
            }

            CardHolderIdentificationResult result = new CardHolderIdentificationResult();

            try {
                result = CardHolderIdentification(tcID);
            } catch (Exception ex) {
                result.CARDHOLDER_IDENTIFICATION_RET = new CARDHOLDERIDENTIFICATIONRET();
                result.CARDHOLDER_IDENTIFICATION_RET.ERROR_FOUND = "YES";
                result.CARDHOLDER_IDENTIFICATION_RET.ERROR_NUMBER = "2000";
                result.CARDHOLDER_IDENTIFICATION_RET.ERROR_MESSAGE = ex.Message;
            }

            FSV.CardRegistrationResponse transResult = new FSV.CardRegistrationResponse() {
                CardID = pRegistration.CardHolderCardID,
                PayCardType = PayCardTypes.TransCard.ToString(),
                ReturnCode = result.CARDHOLDER_IDENTIFICATION_RET.ERROR_NUMBER,
                ReturnMessage = result.CARDHOLDER_IDENTIFICATION_RET.ERROR_MESSAGE,
            };

            if (!result.CARDHOLDER_IDENTIFICATION_RET.ERROR_NUMBER.IsNullOrEmpty() && result.CARDHOLDER_IDENTIFICATION_RET.ERROR_NUMBER != "0") {
                transResult.ReturnCode = "-1";
                transResult.ReturnMessage = "Error updating Drivers Info on TransCard: " + result.CARDHOLDER_IDENTIFICATION_RET.ERROR_MESSAGE;
            } else {
                var validateResult = ValidateCard(tcID.CardNumber);
                if (("" + validateResult.ACTIVATION_REQUIRED).StartsWith("Y", StringComparison.CurrentCultureIgnoreCase)) {
                    //CabRideEngine.com.cabpay.CellPhoneWebService.CARD_ACTIVATION_RET actResult = mService.ActivateCard(this.TransCardAdminNo.EncryptIceKey());
                    //if (actResult.ERROR_FOUND.StartsWith("N", StringComparison.CurrentCultureIgnoreCase)) {
                    //    transResult = oService.ValidateCard(this.TransCardNumber);
                    //    this.TransCardAdminNo = transResult.ACCOUNT_NO;
                    //}
                }
            }

            return transResult;

        }


        private CardHolderIdentificationResult CardHolderIdentification(TransCardDriverIdentification pID) {
            string data = $"<CARDHOLDER_IDENTIFICATION>{LoginInfo()}";

            if (pID.CardNumber.Length < 15) {
                data += $@"<ACCOUNT_NO>{pID.CardNumber}</ACCOUNT_NO>
                           <CARD_NUMBER></CARD_NUMBER>";
            } else {
                data += $@"<CARD_NUMBER>{pID.CardNumber}</CARD_NUMBER>
                           <ACCOUNT_NO></ACCOUNT_NO>";
            }
            data += $@"<FIRST_NAME>{pID.FirstName}</FIRST_NAME>
                        <MIDDLE_NAME>{pID.MiddleName}</MIDDLE_NAME>
                        <LAST_NAME>{pID.LastName}</LAST_NAME>
                        <ADDRESS_LINE_1>{pID.AddressLine1}</ADDRESS_LINE_1>
                        <ADDRESS_LINE_2>{pID.AddressLine2}</ADDRESS_LINE_2>
                        <ADDRESS_LINE_3>{pID.AddressLine3}</ADDRESS_LINE_3>
                        <CITY>{pID.City}</CITY>
                        <STATE_PROVINCE>{pID.StateProvince}</STATE_PROVINCE>
                        <COUNTRY_CODE>{pID.CountryCode}</COUNTRY_CODE>
                        <POSTAL_CODE>{pID.PostalCode}</POSTAL_CODE>
                        <ID_CODE>{pID.IdCode}</ID_CODE>
                        <ID_NUMBER>{pID.IdNumber}</ID_NUMBER>
                        <ID_COUNTRY>{pID.IdCountry}</ID_COUNTRY>
                        <DATE_OF_BIRTH>{pID.DateOfBirth}</DATE_OF_BIRTH>
                        <PHONE>{pID.Phone}</PHONE>
                        <EMAIL>{pID.Email}</EMAIL>
                        <SECURITY_QUESTION>{pID.SecurityQuestion}</SECURITY_QUESTION>
                        <SECURITY_ANSWER>{pID.SecurityAnswer}</SECURITY_ANSWER>
                        </CARDHOLDER_IDENTIFICATION>";

            string sResult = mService.CARDHOLDER_IDENTIFICATION(data);

            CardHolderIdentificationResult result = sResult.XmlToJson<CardHolderIdentificationResult>();
            return result;

        }


        private VALIDATE_CARD_RET ValidateCard(string pCardNo) {
            string data = $@"<VALIDATE_CARD>
                                <CLIENT_ID>{ClientID}</CLIENT_ID>
                                <CLIENT_PASSWORD>{ClientPassword}</CLIENT_PASSWORD>
                                <LOCATION_ID>{mCredentials.LocationID}</LOCATION_ID>
                                <LOCATION_CITY>{mCredentials.LocationCity}</LOCATION_CITY>
                                <LOCATION_STATE>{mCredentials.LocationState}</LOCATION_STATE>
                                <LOCATION_COUNTRY>{mCredentials.LocationCountry}</LOCATION_COUNTRY>
                                <EMPLOYEE_PIN>{EmployeePIN}</EMPLOYEE_PIN>";
            if (pCardNo.Length < 15) {
                data += $@"<ACCOUNT_NO>{pCardNo}</ACCOUNT_NO>
                            <CARD_NUMBER></CARD_NUMBER>";
            } else {
                data += $@"<CARD_NUMBER>{pCardNo}</CARD_NUMBER>
                        <ACCOUNT_NO></ACCOUNT_NO>";
            }
            data += @"<ANI></ANI>
                     <DNIS></DNIS>
                    </VALIDATE_CARD>";

            string sResult = mService.VALIDATE_CARD(data);

            VALIDATE_CARD_RET result = MyXML.DeserializeObject<VALIDATE_CARD_RET>(sResult);
            return result;
        }


        public string LoginInfo() {
            string xml = $@"<CLIENT_ID>{ClientID}</CLIENT_ID>
                            <CLIENT_PASSWORD>{ClientPassword}</CLIENT_PASSWORD>
                            <LOCATION_ID>{mCredentials.LocationID}</LOCATION_ID>
                            <LOCATION_CITY>{mCredentials.LocationCity}</LOCATION_CITY>
                            <LOCATION_STATE>{mCredentials.LocationState}</LOCATION_STATE>
                            <LOCATION_COUNTRY>{mCredentials.LocationCountry}</LOCATION_COUNTRY>
                            <EMPLOYEE_PIN>{EmployeePIN}</EMPLOYEE_PIN>
                            <ANI>TRANSCARD</ANI>
                            <DNIS></DNIS>";
            return xml;
        }

        public FSV.VerifyConnectionResponse VerifyConnection() {
            return new FSV.VerifyConnectionResponse() {
                ReturnCode = "1",
                ReturnMessage = "Connected"
            };
        }

        public FundsTransferResponse TransferToPayCard(FSV.FundsTransfer pFunds) {
            throw new NotImplementedException();
        }

        public FundsTransferResponse TransferFromPayCard(string pPayCardNo, FSV.FundsTransfer pFunds) {
            throw new NotImplementedException();
        }

        public decimal GetFundingBalance() {
            throw new NotImplementedException();
        }

        public class Credentials : IPayCardCredentials {
            public string WebServiceURL { get; set; }

            public string ClientID { get; set; }
            public string ClientPass { get; set; }

            public string LocationID { get; set; }
            public string LocationCity { get; set; }
            public string LocationState { get; set; }
            public string LocationCountry { get; set; }

            public string EmpPIN { get; set; }
            public string AcctNo { get; set; }
            public string CurrencyCode { get; set; }
            public string TransCode { get; set; }
            public string FundingAccount { get; set; }

            public string ProgramID { get; set; }
            public string BillingTable { get; set; }
            public string PartnerStore { get; set; }


            public string UseProduction { get; set; }
            public bool UseProductionAsBoolen {
                get {
                    return ("" + UseProduction).Equals("true", StringComparison.CurrentCultureIgnoreCase);
                }
            }

            public PayCardTypes MyPayCardType() {
                return PayCardTypes.TransCard;
            }
        }

        public class CardHolderIdentificationResult {
            public CARDHOLDERIDENTIFICATIONRET CARDHOLDER_IDENTIFICATION_RET { get; set; }
        }

        public class CARDHOLDERIDENTIFICATIONRET {
            public string ERROR_FOUND { get; set; }
            public string ERROR_MESSAGE { get; set; }
            public string ERROR_NUMBER { get; set; }
            public string CARDHOLDER_IDENTIFIED { get; set; }
            public string REQUIRE_PIN_CHANGE { get; set; }
            public string REQUIRE_SSN { get; set; }
            public string REQUIRE_DOB { get; set; }
            public string REQUIRE_NAME { get; set; }
            public string REQUIRE_ADDRESS { get; set; }
            public string REQUIRE_EMAIL { get; set; }
            public string REQUIRE_PHONE { get; set; }
            public string REQUIRE_IDCHECK { get; set; }
            public string REQUIRE_LEXISNEXIS { get; set; }
            public string ID_REFERENCE_NUMBER { get; set; }
            public string IDCODE_DETAILS { get; set; }
        }

    }
}
