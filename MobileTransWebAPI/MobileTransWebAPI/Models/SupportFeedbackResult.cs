﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileTransWebAPI.Models {
	public class SupportFeedbackResult {

		public string ErrorMsg { get; set; }
		public bool Sent { get; set; }
	}
}