using System;
using System.Collections.Generic;
using System.Text;

namespace TaxiPassCommon.Banking {
	public class TTechTransaction {

		
		private string mSite;
		private TTechAction mAction;
		private long mTranID;
		private string mMerchantID;
		private short mLaneID;
		private decimal mAmount;
		private DateTime mTranDate;
		private string mLastName;
		private string mMiddleInitial;
		private string mCustomerName;
		private string mAddress;
		private string mSuite;
		private string mCity;
		private string mState;
		private string mZipCode;
		private string mPhone1;
		private string mPhone2;
		private string mCheckWriterID;
		private string mCompany;
		private long mCheckNumber;
		private string mCheckType;
		private string mABANumber;
		private string mAccount;
		private string mClassCode;
		private string mTranType;
		private string mApplication;
		private string mPaymentType;
		private string mImageReference;
		private string mTranReference;
		private int mTransactionCount;
		private string mBatchNo;

		public string PaidTo;

		//public bool NachaFile { get; set; }
		public string Affiliate { get; set; }
		
		public bool WireTransfer { get; set; }
		

		public string BatchNo {
			get {
				return mBatchNo;
			}
			set {
				mBatchNo = value;
			}
		}
	
		public int TransactionCount {
			get {
				return mTransactionCount;
			}
			set {
				mTransactionCount = value;
			}
		}

		public string CSVRecord {
			get {
				string sText = Site + ",";     //1
				sText += GetActionText(this.Action) + ","; //2
				sText += TaxiPassCommon.StringUtils.Right(this.TranID.ToString().PadLeft(6, '0'), 6) + ","; //3
				sText += ",,"; //4,5
				sText += this.MerchantID + ","; //6
				sText += this.LaneID + ","; //7
				sText += Math.Abs(this.Amount).ToString("00000000.00") + ","; //8

				if (this.Action == TTechAction.BatchOut) {
					sText += DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + ","; //9
				} else if (this.TranDate >= DateTime.Today) {
					sText += this.TranDate.ToString("MM/dd/yyyy") + " 00:00:00,"; //9
				} else {
					sText += this.TranDate.ToString("MM/dd/yyyy HH:mm:ss") + ","; //9
				}

				sText += ",";   //10
				sText += (this.LastName == "" ? "" : "\"" + this.LastName + "\"") + ",";    //11
				sText += (this.MiddleInitial == "" ? "" : "\"" + this.MiddleInitial + "\"") + ","; //12
				sText += (this.CustomerName == "" ? "" : "\"" + this.CustomerName + "\"") + ",";  //13
				sText += (this.Address == "" ? "" : "\"" + this.Address + "\"") + ","; //14
				sText += (this.Suite == "" ? "" : "\"" + this.Suite + "\"") + ","; //15
				sText += this.City + ",";   //16
				sText += this.State + ",";  //17
				sText += this.ZIPCode + ",";    //18
				sText += this.Phone1 + ","; //19
				sText += this.Phone2 + ",";   //20
				sText += this.CheckWriterID + ",";  //21
				sText += ",";   //22
				sText += "\"" + this.Company + "\",";   //23
				sText += this.CheckNumber + ",";   //24
				sText += this.CheckType + ",";   //25
				sText += this.ABANumber + ",";   //26
				sText += this.Account + ",";   //27
				sText += this.ClassCode + ",";   //28
				sText += this.TranType + ",";   //29
				sText += this.Application + ",";   //30
				sText += this.PaymentType + ",";   //31
				sText += this.ImageReference + ",";   //32
				sText += this.TranReference + "\r\n";  //33
				return sText;
			}
		}


		public string TranReference {
			get {
				return mTranReference == null ? "" : mTranReference;
			}
			set {
				mTranReference = value;
			}
		}

		public string ImageReference {
			get {
				return mImageReference == null ? "" : mImageReference;
			}
			set {
				mImageReference = value;
			}
		}

		public string PaymentType {
			get {
				return mPaymentType == null ? "" : mPaymentType;
			}
			set {
				mPaymentType = value;
			}
		}

		public string Application {
			get {
				return mApplication == null ? "" : mApplication;
			}
			set {
				mApplication = value;
			}
		}

		public string TranType {
			get {
				return mTranType == null ? "" : mTranType;
			}
			set {
				mTranType = value;
			}
		}

		public string ClassCode {
			get {
				return mClassCode == null ? "" : mClassCode;
			}
			set {
				mClassCode = value;
			}
		}

		public string Account {
			get {
				return mAccount == null ? "" : mAccount;
			}
			set {
				mAccount = value;
			}
		}

		public string ABANumber {
			get {
				return mABANumber == null ? "" : mABANumber;
			}
			set {
				mABANumber = value;
			}
		}

		public string CheckType {
			get {
				return mCheckType == null ? "" : mCheckType;
			}
			set {
				mCheckType = value;
			}
		}

		public long CheckNumber {
			get {
				return mCheckNumber;
			}
			set {
				mCheckNumber = value;
			}
		}

		public string Company {
			get {
				return mCompany == null ? "" : mCompany;
			}
			set {
				mCompany = value;
			}
		}

		public string CheckWriterID {
			get {
				return mCheckWriterID == null ? "" : mCheckWriterID;
			}
			set {
				mCheckWriterID = value;
			}
		}

		public string Phone2 {
			get {
				return mPhone2 == null ? "" : mPhone2;
			}
			set {
				mPhone2 = value;
			}
		}

		public string Phone1 {
			get {
				return mPhone1 == null ? "" : mPhone1;
			}
			set {
				mPhone1 = value;
			}
		}

		public string ZIPCode {
			get {
				return mZipCode == null ? "" : mZipCode;
			}
			set {
				mZipCode = value;
			}
		}

		public string State {
			get {
				return mState == null ? "" : mState;
			}
			set {
				mState = value;
			}
		}

		public string City {
			get {
				return mCity == null ? "" : mCity;
			}
			set {
				mCity = value;
			}
		}

		public string Suite {
			get {
				return mSuite == null ? "" : mSuite;
			}
			set {
				mSuite = value;
			}
		}

		public string Address {
			get {
				return mAddress == null ? "" : mAddress;
			}
			set {
				mAddress = value;
			}
		}

		public string CustomerName {
			get {
				return mCustomerName == null ? "" : mCustomerName;
			}
			set {
				mCustomerName = value;
			}
		}


		public string MiddleInitial {
			get {
				return mMiddleInitial == null ? "" : mMiddleInitial;
			}
			set {
				mMiddleInitial = value;
			}
		}

		public string LastName {
			get {
				return mLastName == null ? "" : mLastName;
			}
			set {
				mLastName = value;
			}
		}

		public DateTime TranDate {
			get {
				return mTranDate;
			}
			set {
				mTranDate = value;
			}
		}

		public decimal Amount {
			get {
				return mAmount;
			}
			set {
				mAmount = value;
			}
		}

		public short LaneID {
			get {
				return mLaneID;
			}
			set {
				mLaneID = value;
			}
		}

		public string MerchantID {
			get {
				return mMerchantID;
			}
			set {
				mMerchantID = value;
			}
		}

		public long TranID {
			get {
				return mTranID;
			}
			set {
				mTranID = value;
			}
		}

		public TTechAction Action {
			get {
				return mAction;
			}
			set {
				mAction = value;
			}
		}

		public string Site {
			get {
				return mSite;
			}
			set {
				mSite = value;
			}
		}

		public string GetActionText(TTechAction pAction) {
			string text = "";
			switch (pAction) {
				case TTechAction.Normal:
					text = "AUTH";
					break;
				case TTechAction.Void:
					text = "VOID";
					break;
				case TTechAction.OverRide:
					text = "OVRD";
					break;
				case TTechAction.Represent:
					text = "REPR";
					break;
				case TTechAction.BatchOut:
					text = "BOUT";
					break;
			}
			return text;
		}

		public string TransactionKey {
			get {
				return this.LaneID + "_" + this.MerchantID + "_" + this.ABANumber + "_" + this.Account + "_" + this.PaidTo;
			}
		}
	}


}
