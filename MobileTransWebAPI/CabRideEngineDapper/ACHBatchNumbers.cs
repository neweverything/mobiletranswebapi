﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CabRideEngineDapper {

	public class ACHBatchNumbers {

		private string mACHBatchNumber = "";
		private string mBatchNumber = "";

		public static string BatchNumberColumnName = "BatchNumber";
		public static string ACHBatchNumberColumnName = "ACHBatchNumber";

		public ACHBatchNumbers() {

		}

		public ACHBatchNumbers(string pBatchNumber, string pACHBatchNumber) {
			mBatchNumber = pBatchNumber;
			mACHBatchNumber = pACHBatchNumber;
		}

		public string ACHBatchNumber {
			get {
				return mACHBatchNumber;
			}
			set {
				mACHBatchNumber = value;
			}
		}

		public string BatchNumber {
			get {
				return mBatchNumber;
			}
			set {
				mBatchNumber = value;
			}
		}

		public bool TaxiTronic { get; set; }

	}
}
