﻿using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace CabRideEngineDapper.StoredProc {
    public class usp_BlackListCardsGetByNumber {

        public static List<BlackListCards> Execute(SqlConnection pConn, string pCardNo) {
            return pConn.Query<BlackListCards>("usp_BlackListCardsGetByNumber", new { Number = pCardNo }, commandType: CommandType.StoredProcedure).ToList();
        }



    }
}
