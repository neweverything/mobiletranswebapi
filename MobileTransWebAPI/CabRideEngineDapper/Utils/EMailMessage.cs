using System;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;
using TaxiPassCommon;
using TaxiPassCommon.Mail;

namespace CabRideEngineDapper.Utils {
	public class EMailMessage {
		private string mHost;
		private int mPort;
		private string mFrom;
		private string mBody;
		private string mSubject;
		private string mPassword;

		public bool UseWebServiceToSendEmail { get; set; }

		public EMailMessage(string pFrom, string pSubject, string pBody, string pPassword) {
			mFrom = pFrom;
			mSubject = pSubject;
			mBody = pBody;
			mPassword = pPassword;
		}

		public EMailMessage(EmailCredentials pCred, string pSubject, string pBody) {
			mHost = pCred.Host;
			mPort = pCred.Port;
			mFrom = pCred.From;
			mSubject = pSubject;
			mBody = pBody;
			mPassword = pCred.Password.Decrypt();
		}

		public EMailMessage(Reservations pReserve, string pTemplate) {

			MatchCollection oMatch = new Regex("{{[^}}]*}}|\\[[^]]*]", RegexOptions.Multiline | RegexOptions.IgnoreCase).Matches(pTemplate);

			foreach (Match match1 in oMatch) {
				string sText = match1.Value;
				if (sText.Contains("[from:")) {
					sText = sText.Replace("]", "");
					mFrom = sText.Split(':')[1];
					pTemplate = pTemplate.Replace(match1.Value, "");
				}
				if (sText.Contains("[subject:")) {
					sText = sText.Replace("]", "");
					mSubject = sText.Split(':')[1];
					MatchCollection oSubMatch = new Regex("{{[^}}]*}}", RegexOptions.Multiline | RegexOptions.IgnoreCase).Matches(mSubject);
					foreach (Match match2 in oSubMatch) {
						sText = match2.Value.Replace("{", "");
						sText = sText.Replace("}", "");

						try {
							object oObj = Eval(sText.Split('.'), pReserve);
							string sValue = oObj.ToString();
							mSubject = mSubject.Replace(match2.Value, sValue);
						} catch (Exception ex) {
							ErrorLog.ErrorRoutine(ex);
						}
					}
					pTemplate = pTemplate.Replace(match1.Value, "");
				}
				if (sText.Contains("[StarRating:")) {
					sText = sText.Replace("]", "");
					string sValue = sText.Split(':')[1];

					Reports oReport = Reports.GetReport("StarRating");
					string sURL = oReport.ReportPath + "?tp=";
					string sParams = "reserveid=" + pReserve.ReservationID.ToString();
					sParams += "&customerid=" + pReserve.CustomerID;
					sParams += "&value=" + sValue;

					Encryption oEncrypt = new Encryption();
					string sSalt = StringUtils.GenerateSalt();
					sParams = oEncrypt.Encrypt(sParams, sSalt) + sSalt;
					sURL += HttpUtility.UrlEncode(sParams);
					pTemplate = pTemplate.Replace(match1.Value, sURL);
				}

				if (sText.StartsWith("{")) {
					sText = sText.Replace("{", "");
					sText = sText.Replace("}", "");

					try {
						object oObj = Eval(sText.Split('.'), pReserve);
						string sValue = "";
						if (oObj != null) {
							sValue = oObj.ToString();
						}
						pTemplate = pTemplate.Replace(match1.Value, sValue);

					} catch (Exception ex) {
						throw new Exception("Error returned while parsing " + sText + ": " + ex.Message.ToString());
					}
				}
			}
			mBody = pTemplate;
		}

		public static object Eval(string[] pEval, object pObject) {
			int i = 0;
			Type oDataType = pObject.GetType();

			i = 1;
			object oObj = oDataType.InvokeMember(pEval[i], BindingFlags.GetProperty, null, pObject, null);

			for (i++; i < pEval.Length; i++) {
				oDataType = oObj.GetType();
				if (pEval[i].Right(2) == "()") {
					oObj = oDataType.InvokeMember(pEval[i].Substring(0, pEval[i].Length - 2), BindingFlags.InvokeMethod, null, oObj, null);
				} else {
					oObj = oDataType.InvokeMember(pEval[i], BindingFlags.GetProperty, null, oObj, null);
				}
			}

			return oObj;
		}



		public void SendMail(string pSendTo) {
			SendMail(pSendTo, true);
		}

		public void SendMail(string pSendTo, bool pIsBodyHTML) {
			TaxiPassCommon.Mail.EMailMessage email = new TaxiPassCommon.Mail.EMailMessage(mFrom, mSubject, mBody, mPassword);
			email.SendMail(pSendTo, pIsBodyHTML, mHost, mPort);
			
		}

		public string GetBody() {
			return mBody;
		}

	}
}
