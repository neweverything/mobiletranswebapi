﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CabRideEngineDapper {
    public partial class DriverPromotionAffiliates {
        public static List<DriverPromotionAffiliates> GetAffiliates(SqlConnection pConn, long pDriverPromotionID) {
            string sql = @"SELECT * FROM DriverPromotionAffiliates
                            WHERE DriverPromotionID = @DriverPromotionID
                            ORDER BY AffiliateID";
            return pConn.Query<DriverPromotionAffiliates>(sql, new { DriverPromotionID = pDriverPromotionID }).ToList();
        }
    }
}
