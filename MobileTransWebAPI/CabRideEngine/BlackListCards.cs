﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Linq;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

using CabRideEngine.Utils;
using TaxiPassCommon;

using Csla.Validation;
using System.Data.SqlClient;
using Dapper;

namespace CabRideEngine {
	/// <summary>
	/// The developer class representing the BlackListCards business object class.
	/// </summary>
	/// <remarks>
	/// <para>Place all application-specific business logic in this class.</para>
	/// <para>
	/// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
	/// and will not overwrite changes.
	/// </para>
	/// <para>
	/// This class inherits from the generated class, its companion in the pair of classes 
	/// defining a business object. Properties in the generated class may be overridden in 
	/// order to change the behavior or add business logic.
	/// </para>
	/// <para>
	/// This class is the final class in the inheritance chain for this business object. 
	/// It should not be inherited.
	/// </para>
	/// </remarks>
	[Serializable]
	public sealed partial class BlackListCards : BlackListCardsDataRow {

		#region Constructors -- DO NOT MODIFY

		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new';
		// it is also required if the class is visible from a web service.
		// DO NOT REMOVE **
		private BlackListCards() : this(null) { }

		/// <summary>
		/// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
		/// </summary>
		/// <param name="pRowBuilder"></param>
		public BlackListCards(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static BlackListCards Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      BlackListCards aBlackListCards = pManager.CreateEntity<BlackListCards>();
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aBlackListCards, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aBlackListCards.AddToManager();
		//      return aBlackListCards;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		// Add additional logic to your business object here...

		protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
			if (IsDeserializing) {
				return;
			}
			base.OnColumnChanging(pArgs);
			DoAudit(pArgs);
		}

		private void DoAudit(DataColumnChangeEventArgs pArgs) {
			if (!this.PersistenceManager.IsClient) {
				return;
			}
			if (this.RowState == DataRowState.Added) {
				return;
			}
			if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
				AuditRecord = BlackListCardsAudit.Create(this);
			}
		}

		#region Standard Entity logic to create/audit/validate the entity
		public static BlackListCards Create(PersistenceManager pPM) {
			BlackListCards rec = null;

			try {
				rec = pPM.CreateEntity<BlackListCards>();

				// Using the IdeaBlade Id Generation technique
				pPM.GenerateId(rec, BlackListCards.BlackListCardIDEntityColumn);
				rec.AddToManager();

			} catch (Exception ex) {
				throw ex;
			}
			return rec;
		}

		//protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
		//    if (IsDeserializing) {
		//        return;
		//    }
		//    base.OnColumnChanging(pArgs);
		//    DoAudit(pArgs);
		//}

		//private void DoAudit(DataColumnChangeEventArgs pArgs) {
		//    if (!this.PersistenceManager.IsClient) {
		//        return;
		//    }
		//    if (this.RowState == DataRowState.Added) {
		//        return;
		//    }
		//    if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
		//        AuditRecord = BlackListCardsAudit.Create(this.PersistenceManager, this);
		//    }
		//}

		[BindingBrowsable(false)]
		public override string EntityInstanceDisplayName {
			get {
				return this.CardNo;
			}
		}

		protected override void AddRules(RuleList pList) {
			PropertyRequiredRule.AddToList(pList, CardNoEntityColumn.ColumnName);
			pList.Add(IsCardNoUnique, CardNoEntityColumn.ColumnName);
		}

		private bool IsCardNoUnique(object pTarget, RuleArgs e) {
			BlackListCards rec = pTarget as BlackListCards;
			if (string.IsNullOrEmpty(rec.CardNo)) {
				return false;
			}
			RdbQuery qry = new RdbQuery(typeof(BlackListCards), CardNoEntityColumn, EntityQueryOp.EQ, rec.CardNo);
			qry.AddClause(BlackListCardIDEntityColumn, EntityQueryOp.NE, rec.BlackListCardID);
			EntityList<BlackListCards> oList = this.PersistenceManager.GetEntities<BlackListCards>(qry);
			if (oList.Count > 0) {
				e.Description = "Card number already exists";
				return false;
			}
			return true;
		}

		#endregion

		public override string CardNo {
			get {
				return base.CardNo.DecryptIceKey();
			}
			set {
				Number = CryptoFns.MD5HashUTF16ToString(value);
				base.CardNo = value.EncryptIceKey();
			}
		}

		public string CardNoEncrypted {
			get {
				if (base.CardNo.Length > 16) {
					return base.CardNo;
				} else {
					return base.CardNo.EncryptIceKey();
				}
			}
		}

		#region Retrieve Records

		public static EntityList<BlackListCards> GetAll(PersistenceManager pPM) {
			EntityList<BlackListCards> oList = pPM.GetEntities<BlackListCards>();
			//oList.ApplySort(BlackListCards.CardNoEntityColumn.ColumnName, System.ComponentModel.ListSortDirection.Ascending, false);
			return oList;
		}

		public static BlackListCards GetCard(PersistenceManager pPM, string pCardNo) {
			if (pCardNo.IsNullOrEmpty()) {
				return pPM.GetNullEntity<BlackListCards>();
			}

			string number = CryptoFns.MD5HashUTF16ToString(pCardNo);
			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(BlackListCards), string.Format("usp_BlackListCardsGetByNumber '{0}'", number));
			return pPM.GetEntity<BlackListCards>(qry, QueryStrategy.DataSourceThenCache);
		}

		public static bool IsBlackListed(SqlConnection pConn, string pCardNo) {
			bool result = false;
			if (!pCardNo.IsNullOrEmpty()) {
				string number = CryptoFns.MD5HashUTF16ToString(pCardNo);
				dynamic cardList = pConn.Query(string.Format("usp_BlackListCardsGetByNumber '{0}'", number)).ToList();
				foreach (var rec in cardList) {
					string cardNo = rec.CardNo;
					if (cardNo.DecryptIceKey().Equals(pCardNo)) {
						result = true;
						break;
					}
				}
			}
			return result;
		}

		#endregion


	}

	#region EntityPropertyDescriptors.BlackListCardsPropertyDescriptor
	////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
	////    For example: 
	////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
	//public static partial class EntityPropertyDescriptors {
	//	public partial class BlackListCardsPropertyDescriptor : BaseEntityPropertyDescriptor {
	//    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
	//    public AdaptedPropertyDescriptor Age {
	//      get { return this.Get("Age"); }
	//    }
	//  }
	//} 
	#endregion

}