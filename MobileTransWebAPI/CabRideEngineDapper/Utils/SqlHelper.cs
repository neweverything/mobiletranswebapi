﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using TaxiPassCommon;

namespace CabRideEngineDapper.Utils {
	public static class SqlHelper {

		public static SqlConnection OpenSqlConnection() {
			if (CabRideDapperSettings.DataSource.IsNullOrEmpty()) {
				CabRideDapperSettings.DataSource = "default";
			}

			return OpenSqlConnection(CabRideDapperSettings.SqlConnectionString);
		}

		[Obsolete]
		public static SqlConnection OpenSqlConnection(string pConnection) {
			SqlConnection connection = new SqlConnection(pConnection);			
			connection.Open();
			return connection;
		}

		public static SqlConnection OpenSqlAuditConnection() {
			if (CabRideDapperSettings.SqlAuditConnectionString == null || !CabRideDapperSettings.SqlAuditConnectionString.ToLower().Contains("audit")) {
				CabRideDapperSettings.SqlAuditConnectionString = AppHelper.Initializer.GetAuditConnectionString("default");
			}

			return OpenSqlAuditConnection(CabRideDapperSettings.SqlAuditConnectionString);
		}

		public static SqlConnection OpenSqlAuditConnection(string pConnection) {
			SqlConnection connection = new SqlConnection(pConnection);

			connection.Open();
			return connection;
		}


		public static T GetRecord<T>(string sql, DynamicParameters param = null) {
			//T x = (T)typeof(T).GetMethod("GetNullEntity", BindingFlags.Public | BindingFlags.Static).Invoke(null, null);
			using (var conn = SqlHelper.OpenSqlConnection()) {
				return conn.Query<T>(sql, param).SingleOrDefault();
			}
		}

		public static IEnumerable<T> GetRecords<T>(string sql, DynamicParameters param = null) {
			using (var conn = SqlHelper.OpenSqlConnection()) {
				return conn.Query<T>(sql, param);
			}
		}

		public static DateTime GetUTCServerTime() {
			string sql = @"SELECT GETUTCDATE() UTCDate";
			using (var conn = CabRideEngineDapper.Utils.SqlHelper.OpenSqlConnection()) {
				return conn.Query<DateTime>(sql).DefaultIfEmpty(DateTime.UtcNow).FirstOrDefault();
			}
		}

	}
}
