﻿using CabRideEngineDapper.Utils;
using Dapper;
using System.Collections.Generic;
using System.Linq;

namespace CabRideEngineDapper {
    public partial class AffiliateType {

        public static List<string> GetListForDropDown(bool pIncludeBlank = true) {
            string sql = @"SELECT AffiliateType FROM AffiliateType
                            ORDER BY AffiliateType";

            using (var conn = SqlHelper.OpenSqlConnection()) {
                var list = conn.Query<string>(sql).ToList();
                if (pIncludeBlank) {
                    list.Insert(0, "");
                }
                return list;
            }
        }
    }
}
