﻿using Dapper;
using System.Data.SqlClient;

namespace CabRideEngineDapper {
    public partial class CustomerAffiliate {

        public static CustomerAffiliate GetOrCreate(SqlConnection pConn, long pCustomerID, long pAffiliateID, string pModifiedBy, long? pPreferredDriverID) {
            string sql = @"DECLARE @Id BIGINT = (SELECT    CustomerAffiliateID
                                                  FROM      dbo.CustomerAffiliate
                                                  WHERE     CustomerID = @CustomerID AND AffiliateID = @AffiliateID
                                                 )
                            IF @Id IS NULL
                                BEGIN
                                    INSERT  INTO dbo.CustomerAffiliate
                                            (CustomerID
                                           , AffiliateID
                                           , PreferredDriverID
                                           , ModifiedBy
                                           , ModifiedDate
	                                        )
                                    VALUES  (@CustomerID
                                           , @AffiliateID
                                           , @PreferredDriverID
                                           , @ModifiedBy
                                           , GETDATE() 
	                                        )
                                END

	                        SELECT * FROM dbo.CustomerAffiliate";

            return pConn.Get<CustomerAffiliate>(sql, new { CustomerID = pCustomerID, AffiliateID = pAffiliateID, ModifiedBy = pModifiedBy, PreferredDriverID = pPreferredDriverID });
        }
    }
}
