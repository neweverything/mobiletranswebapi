﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CabRideEngineDapper {
    public partial class ZIPCode {

        public static ZIPCode GetZipCodeInfo(SqlConnection pConn, string pZipCode) {
            string sql = "SELECT * FROM ZIPCode WHERE ZIPCode = @ZipCode";
            return pConn.Get<ZIPCode>(sql, new { ZipCode = pZipCode });
        }
    }
}
