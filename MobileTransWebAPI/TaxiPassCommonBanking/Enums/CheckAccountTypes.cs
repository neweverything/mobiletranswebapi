﻿namespace TaxiPassCommon.Banking.Enums {
    public enum CheckAccountTypes {
        Checking,
        Savings
    }
}
