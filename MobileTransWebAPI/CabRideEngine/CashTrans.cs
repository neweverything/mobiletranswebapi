﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;
using TaxiPassCommon;

namespace CabRideEngine {
	/// <summary>
	/// The developer class representing the CashTrans business object class.
	/// </summary>
	/// <remarks>
	/// <para>Place all application-specific business logic in this class.</para>
	/// <para>
	/// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
	/// and will not overwrite changes.
	/// </para>
	/// <para>
	/// This class inherits from the generated class, its companion in the pair of classes 
	/// defining a business object. Properties in the generated class may be overridden in 
	/// order to change the behavior or add business logic.
	/// </para>
	/// <para>
	/// This class is the final class in the inheritance chain for this business object. 
	/// It should not be inherited.
	/// </para>
	/// </remarks>
	[Serializable]
	public sealed partial class CashTrans : CashTransDataRow {

		#region Constructors -- DO NOT MODIFY

		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new';
		// it is also required if the class is visible from a web service.
		// DO NOT REMOVE **
		private CashTrans() : this(null) { }

		/// <summary>
		/// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
		/// </summary>
		/// <param name="pRowBuilder"></param>
		public CashTrans(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static CashTrans Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      CashTrans aCashTrans = pManager.CreateEntity<CashTrans>();
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aCashTrans, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aCashTrans.AddToManager();
		//      return aCashTrans;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		// Add additional logic to your business object here...
		public static CashTrans Create(Drivers pDriver) {
			CashTrans rec = null;
			try {
				rec = pDriver.PersistenceManager.CreateEntity<CashTrans>();

				// Using the IdeaBlade Id Generation technique
				pDriver.PersistenceManager.GenerateId(rec, CashTrans.CashTransIDEntityColumn);
				rec.DriverID = pDriver.DriverID;
				rec.AffiliateID = pDriver.AffiliateID.Value;

				rec.AddToManager();

			} catch (Exception ex) {
				throw ex;
			}
			return rec;
		}

		public decimal Total {
			get {
				return this.Fare + this.AirportFee + this.MiscFee + this.Gratuity - this.Discount;
			}
		}

		public DateTime MyChargeDateTime {
			get {
				//Drivers oDriver = Drivers.GetDriver(this.PersistenceManager, this.DriverID);
				//if (oDriver.MyAffiliate.TimeZone == null) {
				return this.ChargeDate;
				//}
				//TimeZoneConverter oTimeZone = new TimeZoneConverter();
				//return oTimeZone.FromUniversalTime(oDriver.MyAffiliate.TimeZone, ChargeDate);
			}
		}

		public Vehicles MyVehicle {
			get {
				return Vehicles.GetVehicle(this.PersistenceManager, this.VehicleID.GetValueOrDefault(0));
			}
		}

		public AffiliateDrivers MyDriver {
			get {
				return AffiliateDrivers.GetDriver(this.PersistenceManager, this.AffiliateDriverID.GetValueOrDefault(0));
			}
		}

		public void FillAddress() {

			if (this.Latitude != 0 && this.Longitude != 0) {
				//SystemDefaults oDefaults = SystemDefaults.GetDefaults(this.PersistenceManager);
				//TaxiPassCommon.GeoCoding.MapPointService oService = new TaxiPassCommon.GeoCoding.MapPointService(oDefaults.MapPointUserName, oDefaults.MapPointPassword);
				//this.Address = oService.GetAddressFromGeoCode(this.Latitude.Value, this.Longitude.Value);
				try {
					com.TaxiPass.GeoCodeWS.GeoCodeService oService = new CabRideEngine.com.TaxiPass.GeoCodeWS.GeoCodeService();
					com.TaxiPass.GeoCodeWS.Address oAddr = oService.GetAddressByGeoCode(this.Latitude.ToString(), this.Longitude.ToString());
					this.Address = oAddr.FormattedAddress;
				} catch {
				}
			}

		}

		public static EntityList<CashTrans> GetTrans(Affiliate pAffiliate, DateTime pStart, DateTime pEnd, long pVehicleID) {
			TaxiPassCommon.TimeZoneConverter oTimeZone = new TimeZoneConverter();
			DateTime dStart = pStart; // oTimeZone.oUniversalTime(pAffiliate.TimeZone, pStart);
			DateTime dEnd = pEnd; // oTimeZone.oUniversalTime(pAffiliate.TimeZone, pEnd);
			RdbQuery qry = new RdbQuery(typeof(CashTrans), CashTrans.ChargeDateEntityColumn, EntityQueryOp.GE, dStart);
			qry.AddClause(CashTrans.ChargeDateEntityColumn, EntityQueryOp.LE, dEnd);
			qry.AddClause(CashTrans.AffiliateIDEntityColumn, EntityQueryOp.EQ, pAffiliate.AffiliateID);

			if (pVehicleID > 0) {
				qry.AddClause(CashTrans.VehicleIDEntityColumn, EntityQueryOp.EQ, pVehicleID);
			}

			qry.CommandTimeout = 300;
			return pAffiliate.PersistenceManager.GetEntities<CashTrans>(qry, QueryStrategy.DataSourceThenCache);
		}


		// ********************************************************************************
		// return CashTrans by ID
		// ********************************************************************************
		public static EntityList<CashTrans> GetTransByID(PersistenceManager pPM, long pCashTransID, string pCustomer) {
			RdbQuery qry = new RdbQuery(typeof(CashTrans), CashTrans.CashTransIDEntityColumn , EntityQueryOp.EQ, pCashTransID);
			if (!pCustomer.IsNullOrEmpty()) {
				qry.AddClause(CashTrans.SendReceiptToEntityColumn, EntityQueryOp.EQ, pCustomer);
			}
			qry.CommandTimeout = 300;
			return pPM.GetEntities<CashTrans>(qry, QueryStrategy.DataSourceThenCache);
		}

		// ********************************************************************************
		// return CashTrans for a customer within a date range
		// ********************************************************************************
		public static EntityList<CashTrans> GetTransBySendReceiptTo(PersistenceManager pPM, string pTo, DateTime pStart, DateTime pEnd) {
			RdbQuery qry = new RdbQuery(typeof(CashTrans), CashTrans.SendReceiptToEntityColumn, EntityQueryOp.EQ, pTo);
			qry.AddClause(CashTrans.ChargeDateEntityColumn, EntityQueryOp.GE, pStart);
			qry.AddClause(CashTrans.ChargeDateEntityColumn, EntityQueryOp.LE, pEnd);
			qry.CommandTimeout = 300;
			return pPM.GetEntities<CashTrans>(qry, QueryStrategy.DataSourceThenCache);
		}

	}

	#region EntityPropertyDescriptors.CashTransPropertyDescriptor
	////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
	////    For example: 
	////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
	//public static partial class EntityPropertyDescriptors {
	//	public partial class CashTransPropertyDescriptor : BaseEntityPropertyDescriptor {
	//    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
	//    public AdaptedPropertyDescriptor Age {
	//      get { return this.Get("Age"); }
	//    }
	//  }
	//} 
	#endregion

}