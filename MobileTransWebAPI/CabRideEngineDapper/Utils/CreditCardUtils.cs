﻿using CabRideEngineDapper.Enums;
using CabRideEngineDapper.StoredProc;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using TaxiPassCommon;
using TaxiPassCommon.Banking;
using TaxiPassCommon.Cryptography;

namespace CabRideEngineDapper.Utils {
	public static class CreditCardUtils {


		/// <summary>
		/// Decrypts the card no.
		/// </summary>
		/// <param name="pSalt">The customer.</param>
		/// <param name="pCardNumber">The card number.</param>
		/// <param name="pHideNumber">if set to <c>true</c> [hide number].</param>
		/// <returns></returns>
		public static string DecryptCardNo(string pSalt, string pCardNumber, bool pHideNumber = true) {
			if (pCardNumber.IsNullOrEmpty()) {
				return "";
			}

			string sCardNo = DoDecryption(pCardNumber, GetSalt(pSalt));
			if (sCardNo.IsNullOrEmpty()) {
				sCardNo = DoDecryption(pCardNumber, pSalt);
			}
			if (pHideNumber) {
				int len = sCardNo.Length - 5;
				string sNewCardNo = "";
				if (len > 5) {
					sNewCardNo = new string('*', len) + sCardNo.Substring(len);
				}
				return sNewCardNo;
			}
			return sCardNo;
		}

		/// <summary>
		/// Does the decryption.
		/// </summary>
		/// <param name="pCardNumber">The card number.</param>
		/// <param name="pSalt">The salt.</param>
		/// <returns></returns>
		private static string DoDecryption(string pCardNumber, string pSalt) {
			return CryptoFns.SimpleDESDecrypt(pCardNumber, pSalt);
		}

		/// <summary>
		/// Gets the salt.
		/// </summary>
		/// <param name="pCustomer">The customer.</param>
		/// <returns></returns>
		private static string GetSalt(string pSalt = "") {
			return (pSalt.IsNullOrEmpty() ? "taxipass" : "TaXi" + pSalt + "pAsS");
		}



		public static string EncryptCardNo(SqlConnection pConn, string pSalt, string pCardNumber, bool pSkipCardValidation = false) {
			pCardNumber = pCardNumber.Trim();

			if (pSkipCardValidation) {
				string salt = GetSalt(pSalt);
				return CryptoFns.SimpleDESEncrypt(pCardNumber, salt);
			} else if (ValidateCardNumber(pConn, pCardNumber)) {
				string salt = GetSalt(pSalt);
				return CryptoFns.SimpleDESEncrypt(pCardNumber, salt);
			} else {
				throw new Exception("Invalid Card Number!");
			}

		}



		public static bool ValidateCardNumber(SqlConnection pConn, string pCardNumber) {
			int length = pCardNumber.Length;
			if (length == 0) {
				return true;
			}

			if (length < 13) {
				return false;
			}


			if (DriverPromotions.IsPromoCard(pConn, pCardNumber)) {
				return true;
			}

			if (BlackListCards.IsBlackListed(pConn, pCardNumber)) {
				throw new Exception("Card cannot be used!");
			}


			int sum = 0;
			int offset = length % 2;
			byte[] digits = new System.Text.ASCIIEncoding().GetBytes(pCardNumber);
			//char[] digits = cardNumber.ToCharArray();

			for (int i = 0; i < length; i++) {
				digits[i] -= 48;
				if (((i + offset) % 2) == 0) {
					digits[i] *= 2;
				}
				sum += (digits[i] > 9) ? digits[i] - 9 : digits[i];
			}
			return ((sum % 10) == 0);
		}


		public static List<IGatewayCredentials> LoadGateWays(SqlConnection pConn, long pID, CreditCardChargeType pType, string pCardType) {
			List<IGatewayCredentials> gatewayList = new List<IGatewayCredentials>();

			List<StoredProc.usp_CardGatewaysGet> gateways = StoredProc.usp_CardGatewaysGet.GetGateWays(pConn, pID);
			//Console.WriteLine(gateways.Count);

			LitleCredentials litle = CreateLitleCredentials(gateways, pType);
			if (litle != null) {
				gatewayList.Add(litle);
			}

			USAePayCredentials ePay = CreateUSAePayCredentials(gateways, pType);
			if (ePay != null) {
				gatewayList.Add(ePay);
			}

			USAePayCredentials ePay1 = CreateUSAePayCredentials(gateways, pType, CardProcessors.USAePayECommerce.ToString());
			if (ePay1 != null && ePay == null) {
				gatewayList.Add(ePay1);
			} else if (ePay1 != null && ePay1.SourceKey != "" + ePay.SourceKey) {
				gatewayList.Add(ePay1);
			}

			VerisignCredentials verisign = CreateVerisignCredentials(gateways);
			if (verisign != null) {
				gatewayList.Add(verisign);
			}

			AuthorizeNetCredentials authNet = CreateAuthorizeNetCredentials(gateways);
			if (authNet != null) {
				gatewayList.Add(authNet);
			}

			StripeCredentials stripe = CreateStripeCredentials(gateways);
			if (stripe != null) {
				gatewayList.Add(stripe);
			}

			CardConnectCredentials cardConnect = CreateCardConnectCredentials(gateways, pType);
			if (cardConnect != null) {
				gatewayList.Add(cardConnect);
			}

			ProPayCredentials proPay = CreateProPayCredentials(gateways, pType);
			if (proPay != null) {
				gatewayList.Add(proPay);
			}

			NewtekCredentials newTek = CreateNewtekCredentials(gateways, pType);
			if (newTek != null) {
				gatewayList.Add(newTek);
			}

			return FilterGateways(pCardType, gatewayList);
		}

		private static ProPayCredentials CreateProPayCredentials(List<usp_CardGatewaysGet> pGateways, CreditCardChargeType pType, string pProcessor = "") {
			string processor = pProcessor;
			if (processor.IsNullOrEmpty()) {
				processor = string.Format("{0}{1}", CardProcessors.ProPay.ToString(), pType == CreditCardChargeType.Any ? "" : pType.ToString());
			} else if (!processor.StartsWith(CardProcessors.ProPay.ToString())) {
				return null;
			}
			var temp = (from p in pGateways
						where ("" + p.Processor).Equals(processor, StringComparison.CurrentCultureIgnoreCase)
						select p).ToList();
			if (temp.Count == 0) {
				processor = CardProcessors.ProPay.ToString();
				temp = (from p in pGateways
						where p.Processor.StartsWith(processor)
						select p).ToList();
				if (temp.Count == 0) {
					return null;
				}
			}

			ProPayCredentials cred = new ProPayCredentials();
			cred.CreditCardProcessor = processor.ToEnum<CardProcessors>();
			string salt = "";
			foreach (StoredProc.usp_CardGatewaysGet rec in temp) {
				switch (rec.ReferenceKey) {
					case "MerchantID":
						cred.MerchantID = rec.ValueString;
						break;

					case "Password":
						cred.AccountNo = rec.ValueString.DecryptIceKey();
						break;

					case "TestPassword":
						cred.TestAccountNo = rec.ValueString.DecryptIceKey();
						break;

					case "TestURL":
						cred.TestURL = rec.ValueString;
						break;

					case "TestUserID":
						cred.TestCertKey = rec.ValueString;
						break;

					case "Timeout":
						if (rec.ValueString.IsNullOrEmpty()) {
							cred.Timeout = 30;
						} else {
							cred.Timeout = Convert.ToInt32(rec.ValueString);
						}
						break;

					case "URL":
						cred.ProdURL = rec.ValueString;
						break;

					case "UserID":
						cred.CertKey = rec.ValueString;
						break;

					case "Validate":
						if (rec.ValueString.IsNullOrEmpty()) {
							cred.Validate = false;
						} else {
							cred.Validate = Convert.ToBoolean(rec.ValueString);
						}
						break;

					case "Salt":
						salt = rec.ValueString;
						break;

					case "TerminalType":
						cred.TerminalType = rec.ValueString;
						break;

					case "CardType":
						cred.CardType = rec.ValueString;
						break;

					case "SplitPayAcctNum":
						cred.SplitPayAcctNum = rec.ValueString;
						break;

				}
			}

			// decrypt passwords
			//if (!salt.IsNullOrEmpty()) {
			//    Encryption oDecrypt = new Encryption();
			//    //if (!cred.PinTest.IsNullOrEmpty()) {
			//    //    cred.PinTest = oDecrypt.Decrypt(cred.PinTest, VerisignAccounts.ENCRYPT_KEY);
			//    //}

			//    if (!cred.Pin.IsNullOrEmpty()) {
			//        cred.Pin = oDecrypt.Decrypt(cred.Pin, VerisignAccounts.ENCRYPT_KEY);
			//    }

			//}

			if (temp.Count > 0) {
				cred.VerisignAccountID = temp[0].CardGatewayGroupsID;
				cred.ProcessingOrder = temp[0].ProcessorOrder;
			}
			return cred;


		}

		private static CardConnectCredentials CreateCardConnectCredentials(List<StoredProc.usp_CardGatewaysGet> pDataList, CreditCardChargeType pType, string pProcessor = "") {
			string processor = pProcessor;
			if (processor.IsNullOrEmpty()) {
				processor = string.Format("{0}{1}", CardProcessors.CardConnect.ToString(), pType == CreditCardChargeType.Any ? "" : pType.ToString());
			} else if (!processor.StartsWith(CardProcessors.CardConnect.ToString())) {
				return null;
			}
			var temp = (from p in pDataList
						where ("" + p.Processor).Equals(processor, StringComparison.CurrentCultureIgnoreCase)
						select p).ToList();
			if (temp.Count == 0) {
				processor = CardProcessors.CardConnect.ToString();
				temp = (from p in pDataList
						where p.Processor.StartsWith(processor)
						select p).ToList();
				if (temp.Count == 0) {
					return null;
				}
			}

			CardConnectCredentials cred = new CardConnectCredentials();
			cred.CreditCardProcessor = processor.ToEnum<CardProcessors>();
			string salt = "";
			foreach (StoredProc.usp_CardGatewaysGet rec in temp) {
				switch (rec.ReferenceKey) {
					case "MerchantID":
						cred.MerchantID = rec.ValueString;
						break;

					case "Password":
						cred.Password = rec.ValueString.DecryptIceKey();
						break;

					case "TestPassword":
						cred.TestPassword = rec.ValueString.DecryptIceKey();
						break;

					case "TestURL":
						cred.TestURL = rec.ValueString;
						break;

					case "TestUserID":
						cred.TestUserName = rec.ValueString;
						break;

					case "Timeout":
						if (rec.ValueString.IsNullOrEmpty()) {
							cred.Timeout = 30;
						} else {
							cred.Timeout = Convert.ToInt32(rec.ValueString);
						}
						break;

					case "URL":
						cred.ProdURL = rec.ValueString;
						break;

					case "UserID":
						cred.UserName = rec.ValueString;
						break;

					case "Validate":
						if (rec.ValueString.IsNullOrEmpty()) {
							cred.Validate = false;
						} else {
							cred.Validate = Convert.ToBoolean(rec.ValueString);
						}
						break;

					case "Salt":
						salt = rec.ValueString;
						break;

					case "TerminalType":
						cred.TerminalType = rec.ValueString;
						break;

					case "CardType":
						cred.CardType = rec.ValueString;
						break;
				}
			}

			// decrypt passwords
			//if (!salt.IsNullOrEmpty()) {
			//    Encryption oDecrypt = new Encryption();
			//    //if (!cred.PinTest.IsNullOrEmpty()) {
			//    //    cred.PinTest = oDecrypt.Decrypt(cred.PinTest, VerisignAccounts.ENCRYPT_KEY);
			//    //}

			//    if (!cred.Pin.IsNullOrEmpty()) {
			//        cred.Pin = oDecrypt.Decrypt(cred.Pin, VerisignAccounts.ENCRYPT_KEY);
			//    }

			//}

			if (temp.Count > 0) {
				cred.VerisignAccountID = temp[0].CardGatewayGroupsID;
				cred.ProcessingOrder = temp[0].ProcessorOrder;
			}
			return cred;

		}

		private static StripeCredentials CreateStripeCredentials(List<usp_CardGatewaysGet> pDataList) {
			string processor = CardProcessors.Stripe.ToString();

			var temp = (from p in pDataList
						where ("" + p.Processor).Equals(processor, StringComparison.CurrentCultureIgnoreCase)
						select p).ToList();
			if (temp.Count == 0) {
				processor = CardProcessors.Stripe.ToString();
				temp = (from p in pDataList
						where p.Processor.Equals(processor)
						select p).ToList();
				if (temp.Count == 0) {
					return null;
				}
			}

			StripeCredentials cred = new StripeCredentials();

			foreach (StoredProc.usp_CardGatewaysGet rec in temp) {
				switch (rec.ReferenceKey) {
					case "MerchantID":
						cred.MerchantID = rec.ValueString;
						break;

					case "TestMerchantID":
						cred.TestMerchantID = rec.ValueString;
						break;

					case "TestUserID":
						cred.TestAPIKey = rec.ValueString;
						break;

					case "Timeout":
						if (rec.ValueString.IsNumeric()) {
							cred.Timeout = Convert.ToInt32(rec.ValueString);
						}
						break;

					case "UserID":
						cred.APIKey = rec.ValueString;
						break;


					case "CardType":
						cred.CardType = rec.ValueString;
						break;

					case "BillingDescriptor":
						cred.BillingDescriptor = rec.ValueString;
						break;

					case "ExcludeBillingDescriptorPrefix":
						cred.ExcludeBillingDescriptorPrefix = Convert.ToBoolean(rec.ValueString);
						break;
				}
			}

			if (temp.Count > 0) {
				cred.Validate = temp[0].Validate;
				cred.VerisignAccountID = temp[0].CardGatewayGroupsID;
				cred.ProcessingOrder = temp[0].ProcessorOrder;
			}
			cred.CreditCardProcessor = processor.ToEnum<CardProcessors>();

			return cred;

		}

		public static List<IGatewayCredentials> LoadGateWays(SqlConnection pConn, long pID, CardProcessors pProcessor, string pCardType) {
			List<IGatewayCredentials> gatewayList = new List<IGatewayCredentials>();

			var gateways = StoredProc.usp_CardGatewaysGet.GetGateWays(pConn, pID);

			LitleCredentials litle = CreateLitleCredentials(gateways, CreditCardChargeType.Any, pProcessor.ToString());
			if (litle != null) {
				gatewayList.Add(litle);
			}

			USAePayCredentials ePay = CreateUSAePayCredentials(gateways, CreditCardChargeType.Any, pProcessor.ToString());
			if (ePay != null) {
				gatewayList.Add(ePay);
			}

			VerisignCredentials verisign = CreateVerisignCredentials(gateways);
			if (verisign != null) {
				gatewayList.Add(verisign);
			}

			AuthorizeNetCredentials authNet = CreateAuthorizeNetCredentials(gateways);
			if (authNet != null) {
				gatewayList.Add(authNet);
			}

			StripeCredentials stripe = CreateStripeCredentials(gateways);
			if (stripe != null) {
				gatewayList.Add(stripe);
			}

			CardConnectCredentials cardConnect = CreateCardConnectCredentials(gateways, CreditCardChargeType.Any, pProcessor.ToString());
			if (cardConnect != null) {
				gatewayList.Add(cardConnect);
			}

			ProPayCredentials proPay = CreateProPayCredentials(gateways, CreditCardChargeType.Any, pProcessor.ToString());
			if (proPay != null) {
				gatewayList.Add(proPay);
			}

			NewtekCredentials newTek = CreateNewtekCredentials(gateways, CreditCardChargeType.Any, pProcessor.ToString());
			if (newTek != null) {
				gatewayList.Add(newTek);
			}


			var temp = FilterGateways(pCardType, gatewayList);

			return temp.Count == 0 ? gatewayList : temp;
		}



		private static List<IGatewayCredentials> FilterGateways(string pCardType, List<IGatewayCredentials> gatewayList) {
			// Get By CardType 1st
			List<IGatewayCredentials> newList = new List<IGatewayCredentials>();
			newList = (from p in gatewayList
					   where ("" + p.GetType().GetProperty("CardType").GetValue(p)).Equals(pCardType, StringComparison.CurrentCultureIgnoreCase)
					   orderby p.ProcessingOrder
					   select p).ToList();

			newList.AddRange((from p in gatewayList
							  where p.GetType().GetProperty("CardType").GetValue(p) == null
									|| p.GetType().GetProperty("CardType").GetValue(p).ToString().Equals("all", StringComparison.CurrentCultureIgnoreCase)
							  orderby p.ProcessingOrder
							  select p).ToList());

			// Don't know why the eCheck is double charging, hack till we can spend time to figure out what is really happening
			if (("" + pCardType).Equals("eCheck", StringComparison.CurrentCultureIgnoreCase)) {
				return new List<IGatewayCredentials> { newList[0] };
			}
			return newList;
		}

		/// <summary>
		/// Creates the USAePay credentials for the gateway processing.
		/// </summary>
		/// <param name="pAccount">The account record.</param>
		/// <returns></returns>
		public static USAePayCredentials CreateUSAePayCredentials(VerisignAccounts pAccount) {
			USAePayCredentials cred = new USAePayCredentials();
			cred.ProdURL = pAccount.EPayProdURL;
			cred.SourceKey = pAccount.EPaySourceKey;
			cred.Pin = pAccount.EPayPin;
			cred.TestURL = pAccount.EPayTestURL;
			cred.SourceKeyTest = pAccount.EPaySourceKeyTest;
			cred.PinTest = pAccount.EPayPinTest;
			cred.Timeout = pAccount.VerisignTimeout.GetValueOrDefault(0);
			cred.MerchantID = pAccount.EPayMerchantID;
			cred.VerisignAccountID = pAccount.VerisignAccountID;
			return cred;
		}

		public static USAePayCredentials CreateUSAePayCredentials(List<StoredProc.usp_CardGatewaysGet> pDataList, CreditCardChargeType pType, string pProcessor = "") {
			string processor = pProcessor;
			if (processor.IsNullOrEmpty()) {
				processor = string.Format("{0}{1}", CardProcessors.USAePay.ToString(), pType.ToString());
			} else if (!processor.StartsWith(CardProcessors.USAePay.ToString())) {
				return null;
			}

			List<StoredProc.usp_CardGatewaysGet> temp = (from p in pDataList
														 where ("" + p.Processor).Equals(processor)
														 select p).ToList();
			if (temp.Count == 0) {
				processor = CardProcessors.USAePay.ToString();
				temp = (from p in pDataList
						where p.Processor.Equals(processor)
						select p).ToList();
				if (temp.Count == 0) {
					return null;
				}
			}

			USAePayCredentials cred = new USAePayCredentials();
			cred.CreditCardProcessor = processor.ToEnum<CardProcessors>();
			string salt = "";
			foreach (StoredProc.usp_CardGatewaysGet rec in temp) {
				switch (rec.ReferenceKey) {
					case "MerchantID":
						cred.MerchantID = rec.ValueString;
						break;

					case "Password":
						cred.Pin = rec.ValueString.DecryptIceKey();
						break;

					case "TestPassword":
						cred.PinTest = rec.ValueString.DecryptIceKey();
						break;

					case "TestURL":
						cred.TestURL = rec.ValueString;
						break;

					case "TestUserID":
						cred.SourceKeyTest = rec.ValueString;
						break;

					case "Timeout":
						if (rec.ValueString.IsNullOrEmpty()) {
							cred.Timeout = 30;
						} else {
							cred.Timeout = Convert.ToInt32(rec.ValueString);
						}
						break;

					case "URL":
						cred.ProdURL = rec.ValueString;
						break;

					case "UserID":
						cred.SourceKey = rec.ValueString;
						break;

					case "Validate":
						if (rec.ValueString.IsNullOrEmpty()) {
							cred.Validate = false;
						} else {
							cred.Validate = Convert.ToBoolean(rec.ValueString);
						}
						break;

					case "Salt":
						salt = rec.ValueString;
						break;

					case "TerminalType":
						cred.TerminalType = rec.ValueString;
						break;

					case "CardType":
						cred.CardType = rec.ValueString;
						break;
				}
			}

			// decrypt passwords
			//if (!salt.IsNullOrEmpty()) {
			//    Encryption oDecrypt = new Encryption();
			//    //if (!cred.PinTest.IsNullOrEmpty()) {
			//    //    cred.PinTest = oDecrypt.Decrypt(cred.PinTest, VerisignAccounts.ENCRYPT_KEY);
			//    //}

			//    if (!cred.Pin.IsNullOrEmpty()) {
			//        cred.Pin = oDecrypt.Decrypt(cred.Pin, VerisignAccounts.ENCRYPT_KEY);
			//    }

			//}

			if (temp.Count > 0) {
				cred.VerisignAccountID = temp[0].CardGatewayGroupsID;
				cred.ProcessingOrder = temp[0].ProcessorOrder;
			}
			return cred;

		}

		/// <summary>
		/// Creates the USAePay credentials for the gateway processing.
		/// </summary>
		/// <param name="pAccount">The account record.</param>
		/// <returns></returns>
		public static LitleCredentials CreateLitleCredentials(List<StoredProc.usp_CardGatewaysGet> pDataList, CreditCardChargeType pType, string pProcessor = "") {
			string processor = pProcessor;
			if (processor.IsNullOrEmpty()) {
				processor = string.Format("{0}{1}", CardProcessors.Litle.ToString(), pType.ToString());
			} else if (!processor.StartsWith(CardProcessors.Litle.ToString())) {
				return null;
			}
			var temp = (from p in pDataList
						where ("" + p.Processor).Equals(processor, StringComparison.CurrentCultureIgnoreCase)
						select p).ToList();
			if (temp.Count == 0) {
				processor = CardProcessors.Litle.ToString();
				temp = (from p in pDataList
						where p.Processor.Equals(processor)
						select p).ToList();
				if (temp.Count == 0) {
					return null;
				}
			}

			LitleCredentials cred = new LitleCredentials();

			foreach (StoredProc.usp_CardGatewaysGet rec in temp) {
				switch (rec.ReferenceKey) {
					case "MerchantID":
						cred.MerchantID = rec.ValueString;
						break;

					case "GetRideMerchantID":
						cred.GetRideMerchantID = rec.ValueString;
						break;

					case "TestMerchantID":
						cred.TestMerchantID = rec.ValueString;
						break;

					case "Password":
						cred.Password = rec.ValueString.DecryptIceKey();
						break;

					case "TestPassword":
						cred.TestPassword = rec.ValueString.DecryptIceKey();
						break;

					case "TestURL":
						cred.TestURL = rec.ValueString;
						break;

					case "TestUserID":
						cred.TestUserName = rec.ValueString;
						break;

					case "Timeout":
						cred.Timeout = Convert.ToInt32(rec.ValueString);
						break;

					case "URL":
						cred.ProdURL = rec.ValueString;
						break;

					case "UserID":
						cred.UserName = rec.ValueString;
						break;

					case "TerminalType":
						cred.TerminalType = rec.ValueString;
						break;

					case "CardType":
						cred.CardType = rec.ValueString;
						break;

					case "BillingDescriptor":
						cred.BillingDescriptor = rec.ValueString;
						break;

					case "ExcludeBillingDescriptorPrefix":
						cred.ExcludeBillingDescriptorPrefix = Convert.ToBoolean(rec.ValueString);
						break;
				}
			}

			if (cred.UserName.IsNullOrEmpty()) {
				cred.UserName = cred.TestUserName;
			}
			if (cred.Password.IsNullOrEmpty()) {
				cred.Password = cred.TestPassword;
			}
			if (cred.MerchantID.IsNullOrEmpty()) {
				cred.MerchantID = cred.TestMerchantID;
			}
			if (cred.ProdURL.IsNullOrEmpty()) {
				cred.ProdURL = cred.TestURL;
			}
			if (temp.Count > 0) {
				cred.Validate = temp[0].Validate;
				cred.VerisignAccountID = temp[0].CardGatewayGroupsID;
				cred.ProcessingOrder = temp[0].ProcessorOrder;
			}
			cred.CreditCardProcessor = processor.ToEnum<CardProcessors>();

			return cred;
		}


		/// <summary>
		/// Creates the verisign credentials.
		/// </summary>
		/// <param name="pAccount">The Verisign Account record.</param>
		/// <returns></returns>
		public static VerisignCredentials CreateVerisignCredentials(VerisignAccounts pAccount) {
			VerisignCredentials cred = new VerisignCredentials();
			cred.ProdURL = pAccount.VerisignAddress;
			cred.TestURL = "pilot-" + pAccount.VerisignAddress;
			cred.Partner = pAccount.VerisignPartner;
			cred.Vendor = pAccount.VersignVendor;
			cred.UserName = pAccount.VerisignUserName;
			cred.Password = pAccount.VerisignPassword;
			cred.Port = pAccount.VerisignPort.GetValueOrDefault(443);
			cred.Timeout = pAccount.VerisignTimeout.GetValueOrDefault(0);
			cred.MerchantID = pAccount.VerisignMerchantID;
			cred.VerisignAccountID = pAccount.VerisignAccountID;
			return cred;
		}

		public static string DetermineCardType(SqlConnection pConn, string pCardNumber) {
			string sCardType = "";

			if (!pCardNumber.IsNullOrEmpty()) {

				pCardNumber = pCardNumber.Replace('*', '1');

				// card must be numeric
				if (pCardNumber.IsNumeric()) {
					StringBuilder sql = new StringBuilder();
					sql.AppendFormat("usp_DetermineCardType @CardNo = '{0}'", pCardNumber);

					dynamic cardTypeList = pConn.Query(sql.ToString()).ToList();
					foreach (var cardType in cardTypeList) {
						if (("" + cardType.Validation) == "") {
							continue;
						}
						string specialCardType = cardType.SpecialCardType;
						if (!specialCardType.IsNullOrEmpty()) {
							sCardType = cardType.SpecialCardType;
						} else if (Regex.IsMatch(pCardNumber, cardType.Validation)) {
							sCardType = cardType.CardType;
						}
						if (!sCardType.IsNullOrEmpty()) {
							break;
						}
					}
				}
			}
			return sCardType;
		}

		public static VerisignCredentials CreateVerisignCredentials(List<StoredProc.usp_CardGatewaysGet> pDataList) {
			var temp = (from p in pDataList
						where ("" + p.Processor).Equals(CardProcessors.Verisign.ToString())
						select p).ToList();
			if (temp.Count == 0) {
				return null;
			}

			VerisignCredentials cred = new VerisignCredentials();
			string salt = "";

			foreach (StoredProc.usp_CardGatewaysGet rec in temp) {
				switch (rec.ReferenceKey) {
					case "AccountName":
						cred.Vendor = rec.ValueString;
						break;

					case "MerchantID":
						cred.MerchantID = rec.ValueString;
						break;

					case "Partner":
						cred.Partner = rec.ValueString;
						break;

					case "Password":
						cred.Password = rec.ValueString.DecryptIceKey();
						break;

					case "Port":
						cred.Port = Convert.ToInt32(rec.ValueString);
						break;

					case "Salt":
						salt = rec.ValueString;
						break;

					case "TestURL":
						cred.TestURL = rec.ValueString;
						break;

					case "TestUserID":
						cred.TestUserName = rec.ValueString;
						break;

					case "TestPassword":
						cred.TestPassword = rec.ValueString.DecryptIceKey();
						break;

					case "Timeout":
						cred.Timeout = Convert.ToInt32(rec.ValueString);
						break;

					case "URL":
						cred.ProdURL = rec.ValueString;
						break;

					case "UserID":
						cred.UserName = rec.ValueString;
						break;

					case "CardType":
						cred.CardType = rec.ValueString;
						break;

				}
			}

			//// decrypt passwords
			//if (!salt.IsNullOrEmpty()) {
			//    Encryption oDecrypt = new Encryption();
			//    if (!cred.TestPassword.IsNullOrEmpty()) {
			//        cred.TestPassword = oDecrypt.Decrypt(cred.TestPassword, VerisignAccounts.ENCRYPT_KEY);
			//    }

			//    if (!cred.Password.IsNullOrEmpty()) {
			//        cred.Password = oDecrypt.Decrypt(cred.Password, VerisignAccounts.ENCRYPT_KEY);
			//    }

			//}

			if (temp.Count > 0) {
				cred.Validate = temp[0].Validate;
				cred.VerisignAccountID = temp[0].CardGatewayGroupsID;
				cred.ProcessingOrder = temp[0].ProcessorOrder;
			}

			return cred;
		}


		/// <summary>
		/// Creates the Authorize.Net credentials for the gateway processing.
		/// </summary>
		/// <param name="pAccount">The account record.</param>
		/// <returns></returns>
		public static AuthorizeNetCredentials CreateAuthorizeNetCredentials(List<StoredProc.usp_CardGatewaysGet> pDataList) {
			string processor = CardProcessors.AuthorizeNet.ToString();

			var temp = (from p in pDataList
						where ("" + p.Processor).Equals(processor, StringComparison.CurrentCultureIgnoreCase)
						select p).ToList();
			if (temp.Count == 0) {
				processor = CardProcessors.AuthorizeNet.ToString();
				temp = (from p in pDataList
						where p.Processor.Equals(processor)
						select p).ToList();
				if (temp.Count == 0) {
					return null;
				}
			}

			AuthorizeNetCredentials cred = new AuthorizeNetCredentials();

			foreach (StoredProc.usp_CardGatewaysGet rec in temp) {
				switch (rec.ReferenceKey) {
					case "MerchantID":
						cred.MerchantID = rec.ValueString;
						break;

					case "GetRideMerchantID":
						cred.GetRideMerchantID = rec.ValueString;
						break;

					case "TestMerchantID":
						cred.TestMerchantID = rec.ValueString;
						break;

					case "Password":
						cred.Password = rec.ValueString.DecryptIceKey();
						break;

					case "TestPassword":
						cred.TestPassword = rec.ValueString.DecryptIceKey();
						break;

					case "TestURL":
						cred.TestURL = rec.ValueString;
						break;

					case "TestUserID":
						cred.TestUserName = rec.ValueString;
						break;

					case "Timeout":
						if (rec.ValueString.IsNumeric()) {
							cred.Timeout = Convert.ToInt32(rec.ValueString);
						}
						break;

					case "URL":
						cred.ProdURL = rec.ValueString;
						break;

					case "UserID":
						cred.UserName = rec.ValueString;
						break;

					case "TerminalType":
						cred.TerminalType = rec.ValueString;
						break;

					case "CardType":
						cred.CardType = rec.ValueString;
						break;

					case "BillingDescriptor":
						cred.BillingDescriptor = rec.ValueString;
						break;

					case "ExcludeBillingDescriptorPrefix":
						cred.ExcludeBillingDescriptorPrefix = Convert.ToBoolean(rec.ValueString);
						break;
				}
			}

			if (cred.UserName.IsNullOrEmpty()) {
				cred.UserName = cred.TestUserName;
			}
			if (cred.Password.IsNullOrEmpty()) {
				cred.Password = cred.TestPassword;
			}
			if (cred.MerchantID.IsNullOrEmpty()) {
				cred.MerchantID = cred.TestMerchantID;
			}
			if (cred.ProdURL.IsNullOrEmpty()) {
				cred.ProdURL = cred.TestURL;
			}
			if (temp.Count > 0) {
				cred.Validate = temp[0].Validate;
				cred.VerisignAccountID = temp[0].CardGatewayGroupsID;
				cred.ProcessingOrder = temp[0].ProcessorOrder;
			}
			cred.CreditCardProcessor = processor.ToEnum<CardProcessors>();

			return cred;
		}


		public static NewtekCredentials CreateNewtekCredentials(List<StoredProc.usp_CardGatewaysGet> pDataList, CreditCardChargeType pType, string pProcessor = "") {
			string processor = pProcessor;
			if (processor.IsNullOrEmpty()) {
				processor = string.Format("{0}{1}", CardProcessors.Newtek.ToString(), pType == CreditCardChargeType.Any ? "" : pType.ToString());
			} else if (!processor.StartsWith(CardProcessors.Newtek.ToString())) {
				return null;
			}

			List<StoredProc.usp_CardGatewaysGet> temp = (from p in pDataList
														 where ("" + p.Processor).Equals(processor)
														 select p).ToList();
			if (temp.Count == 0) {
				processor = CardProcessors.Newtek.ToString();
				temp = (from p in pDataList
						where p.Processor.StartsWith(processor)
						select p).ToList();
				if (temp.Count == 0) {
					return null;
				}
			}

			NewtekCredentials cred = new NewtekCredentials();
			cred.CreditCardProcessor = processor.ToEnum<CardProcessors>();
			string salt = "";
			foreach (StoredProc.usp_CardGatewaysGet rec in temp) {
				switch (rec.ReferenceKey) {
					case "MerchantID":
						cred.MerchantID = rec.ValueString;
						break;

					case "Password":
						cred.Pin = rec.ValueString.DecryptIceKey();
						break;

					case "TestPassword":
						cred.PinTest = rec.ValueString.DecryptIceKey();
						break;

					case "TestURL":
						cred.TestURL = rec.ValueString;
						break;

					case "TestUserID":
						cred.SourceKeyTest = rec.ValueString;
						break;

					case "Timeout":
						if (rec.ValueString.IsNullOrEmpty()) {
							cred.Timeout = 30;
						} else {
							cred.Timeout = Convert.ToInt32(rec.ValueString);
						}
						break;

					case "URL":
						cred.ProdURL = rec.ValueString;
						break;

					case "UserID":
						cred.SourceKey = rec.ValueString;
						break;

					case "Validate":
						if (rec.ValueString.IsNullOrEmpty()) {
							cred.Validate = false;
						} else {
							cred.Validate = Convert.ToBoolean(rec.ValueString);
						}
						break;

					case "Salt":
						salt = rec.ValueString;
						break;

					case "TerminalType":
						cred.TerminalType = rec.ValueString;
						break;

					case "CardType":
						cred.CardType = rec.ValueString;
						break;
				}
			}

			// decrypt passwords
			//if (!salt.IsNullOrEmpty()) {
			//    Encryption oDecrypt = new Encryption();
			//    //if (!cred.PinTest.IsNullOrEmpty()) {
			//    //    cred.PinTest = oDecrypt.Decrypt(cred.PinTest, VerisignAccounts.ENCRYPT_KEY);
			//    //}

			//    if (!cred.Pin.IsNullOrEmpty()) {
			//        cred.Pin = oDecrypt.Decrypt(cred.Pin, VerisignAccounts.ENCRYPT_KEY);
			//    }

			//}

			if (temp.Count > 0) {
				cred.VerisignAccountID = temp[0].CardGatewayGroupsID;
				cred.ProcessingOrder = temp[0].ProcessorOrder;
			}
			return cred;

		}

		//private static CardConnectCredentials CreateCardConnectCredentials(List<usp_CardGatewaysGet> pDataList) {
		//    string processor = CardProcessors.CardConnect.ToString();

		//    var temp = (from p in pDataList
		//                where ("" + p.Processor).Equals(processor, StringComparison.CurrentCultureIgnoreCase)
		//                select p).ToList();
		//    if (temp.Count == 0) {
		//        processor = CardProcessors.CardConnect.ToString();
		//        temp = (from p in pDataList
		//                where p.Processor.StartsWith(processor)
		//                select p).ToList();
		//        if (temp.Count == 0) {
		//            return null;
		//        }
		//    }

		//    CardConnectCredentials cred = new CardConnectCredentials();

		//    foreach (StoredProc.usp_CardGatewaysGet rec in temp) {
		//        switch (rec.ReferenceKey) {
		//            case "MerchantID":
		//                cred.MerchantID = rec.ValueString;
		//                break;

		//            case "TestMerchantID":
		//                cred.TestMerchantID = rec.ValueString;
		//                break;

		//            case "Password":
		//                cred.Password = rec.ValueString.DecryptIceKey();
		//                break;

		//            case "TestPassword":
		//                cred.TestPassword = rec.ValueString.DecryptIceKey();
		//                break;

		//            case "TestURL":
		//                cred.TestURL = rec.ValueString;
		//                break;

		//            case "TestUserID":
		//                cred.TestUserName = rec.ValueString;
		//                break;

		//            case "Timeout":
		//                if (rec.ValueString.IsNumeric()) {
		//                    cred.Timeout = Convert.ToInt32(rec.ValueString);
		//                }
		//                break;

		//            case "URL":
		//                cred.ProdURL = rec.ValueString;
		//                break;

		//            case "UserID":
		//                cred.UserName = rec.ValueString;
		//                break;

		//            case "TerminalType":
		//                cred.TerminalType = rec.ValueString;
		//                break;

		//            case "CardType":
		//                cred.CardType = rec.ValueString;
		//                break;

		//            case "BillingDescriptor":
		//                cred.BillingDescriptor = rec.ValueString;
		//                break;

		//            case "ExcludeBillingDescriptorPrefix":
		//                cred.ExcludeBillingDescriptorPrefix = Convert.ToBoolean(rec.ValueString);
		//                break;
		//        }
		//    }

		//    if (cred.UserName.IsNullOrEmpty()) {
		//        cred.UserName = cred.TestUserName;
		//    }
		//    if (cred.Password.IsNullOrEmpty()) {
		//        cred.Password = cred.TestPassword;
		//    }
		//    if (cred.MerchantID.IsNullOrEmpty()) {
		//        cred.MerchantID = cred.TestMerchantID;
		//    }
		//    if (cred.ProdURL.IsNullOrEmpty()) {
		//        cred.ProdURL = cred.TestURL;
		//    }
		//    if (temp.Count > 0) {
		//        cred.Validate = temp[0].Validate;
		//        cred.VerisignAccountID = temp[0].CardGatewayGroupsID;
		//        cred.ProcessingOrder = temp[0].ProcessorOrder;
		//    }
		//    cred.CreditCardProcessor = processor.ToEnum<CardProcessors>();

		//    return cred;
		//}

		// ***************************************************************************************
		// 4/23/2013
		// Given a DriverPaymentID, return the correct CardGatewayGroupsID (old VerisignAccountID)
		// ***************************************************************************************
		public static long GetDriverPaymentCardGatewayGroupsID(SqlConnection pConn, string pTransNo, long pReservationID) {
			DynamicParameters parms = new DynamicParameters();
			if (pTransNo.IsNullOrEmpty()) {
				parms.Add("ReservationID", pReservationID);
			} else {
				parms.Add("TransNo", pTransNo);
			}

			return pConn.Query<long>("usp_DriverPaymentsGetProcessingGroupID", parms, commandType: CommandType.StoredProcedure).FirstOrDefault();
		}


		public static bool IsDebitCard(this string pCardNumber) {
			if (pCardNumber.IsNullOrEmpty()) {
				return false;
			}
			string cardType = "";
			string binType = "";

			using (var conn = SqlHelper.OpenSqlConnection()) {
				cardType = DetermineCardType(conn, pCardNumber);

				CardBin bin = CardBin.GetCardBinInfo(conn, pCardNumber);
				binType = "" + bin.DebitCredit;
				if (bin.IsNullEntity || binType.IsNullOrEmpty()) {
					BinList info = BinList.GetCardInfo(pCardNumber, cardType);
					if (info != null) {
						binType = "" + info.type;
					}
				}
				return binType.Equals("DEBIT", StringComparison.CurrentCultureIgnoreCase);

			}
		}
	}
}
