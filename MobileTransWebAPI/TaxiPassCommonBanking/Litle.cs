﻿using Litle.Sdk;
using System;
using System.Collections.Generic;
using TaxiPassCommon.Banking.Models;

namespace TaxiPassCommon.Banking {
	public class Litle : IProcessor {

		private LitleCredentials mCredentials;

		LitleOnline mService; // = new LitleOnline();

		public Litle(LitleCredentials pCredentials) {
			if (pCredentials.Timeout == 0) {
				pCredentials.Timeout = 30;
			}
			mCredentials = pCredentials;
		}

		private void InitService(bool pTestCard, bool pGetRideAcct) {
			Dictionary<string, string> config = new Dictionary<string, string>();
			config["url"] = pTestCard ? mCredentials.TestURL : mCredentials.ProdURL;
			config["reportGroup"] = "TaxiPass";
			config["username"] = pTestCard ? mCredentials.TestUserName : mCredentials.UserName;
			config["printxml"] = "false";
			config["version"] = "8.14";
			config["timeout"] = mCredentials.Timeout.ToString();
			config["proxyHost"] = "";
			if (pTestCard || !pGetRideAcct || mCredentials.GetRideMerchantID.IsNullOrEmpty()) {
				if (pTestCard) {
					if (mCredentials.TestMerchantID.IsNullOrEmpty()) {
						config["merchantId"] = mCredentials.MerchantID;
					} else {
						config["merchantId"] = mCredentials.TestMerchantID;
					}
				} else {
					config["merchantId"] = mCredentials.MerchantID;
				}
			} else {
				config["merchantId"] = mCredentials.GetRideMerchantID;
			}
			config["password"] = pTestCard ? mCredentials.TestPassword : mCredentials.Password;
			config["proxyPort"] = "";

			mService = new LitleOnline(config);
		}


		public ProcessorResponse PreAuthCard(CCRequest pRequest) {
			authorization auth = new authorization();
			auth.orderId = pRequest.VoucherNo;
			auth.id = pRequest.VoucherNo;
			auth.amount = Convert.ToInt64(pRequest.Amount * 100);
			auth.orderSource = orderSourceType.retail;

			//if (pRequest.CardData.TestCard && pRequest.CardData.CardNo != CardUtils.TEST_LITLE_VISA) {
			//    if (!pRequest.CardData.Track2.IsNullOrEmpty()) {
			//        pRequest.CardData.Track2 = pRequest.CardData.Track2.Replace(pRequest.CardData.CardNo, CardUtils.TEST_LITLE_VISA);
			//    }
			//    pRequest.CardData.CardNo = CardUtils.TEST_LITLE_VISA;
			//}
			auth.merchantData = new merchantDataType();
			//if (pRequest.CardData.SoftDescriptor != null) {
			//    auth.merchantData.affiliate = pRequest.CardData.SoftDescriptor.AffiliateName;
			//    auth.customBilling = FillSoftDescriptor(pRequest.CardData.SoftDescriptor, !pRequest.CardData.Track2.IsNullOrEmpty());
			//}

			if (pRequest.CardData.SoftDescriptor != null) {
				auth.orderId = string.Format("{0}-{1}", pRequest.VoucherNo, pRequest.CardData.SoftDescriptor.Phone);
				auth.merchantData.affiliate = pRequest.CardData.SoftDescriptor.AffiliateName;
				SetBillingDescriptor(ref pRequest);
				auth.customBilling = FillSoftDescriptor(pRequest.CardData.SoftDescriptor, !pRequest.CardData.Track2.IsNullOrEmpty());
			}


			auth.pos = new pos();
			auth.card = FillCardInfo(pRequest.CardData, auth.pos);
			auth.billToAddress = FillAVS(pRequest.CardData);
			auth.merchantData.merchantGroupingId = string.Format("D:{0}, AD{1}", pRequest.CardData.DriverID, pRequest.CardData.AffiliateDriverID);

			if (!mCredentials.TerminalType.IsNullOrEmpty()) {
				try {
					auth.orderSource = mCredentials.TerminalType.ToLower().ToEnum<orderSourceType>();
				} catch (Exception) {
				}
			}
			SetBillingDescriptor(ref pRequest);

			InitService(pRequest.CardData.TestCard, pRequest.GetRideTrans);
			authorizationResponse response = mService.Authorize(auth);
			var procResp = ProcessResponse(response, pRequest.Amount, pRequest.CardData.TestCard, auth.customBilling.descriptor);
			if (procResp.BillingDescriptor.IsNullOrEmpty() && !auth.customBilling.descriptor.IsNullOrEmpty()) {
				procResp.BillingDescriptor = auth.customBilling.descriptor;
			}
			return procResp;
		}

		private customBilling FillSoftDescriptor(SoftDescriptor pSoftDescriptor, bool pCardSwiped) {
			customBilling billing = new customBilling();

			string url = "TAXI-PASS.COM";
			if (pSoftDescriptor.CompanyURL == CompanyURL.GetRide) {
				url = "GET-RIDE.NET";
			} else {
				switch (pSoftDescriptor.CompanyURL) {
					case CompanyURL.ChargePass:
					case CompanyURL.StorePass:
					case CompanyURL.MedPass:
						url = "CHARGEPASS.CO";
						break;
				}
			}

			if (pSoftDescriptor.BillingDescriptor.IsNullOrEmpty()) {
				billing.descriptor = string.Format("TXP*{0} {1} {2}", url, pSoftDescriptor.City, pSoftDescriptor.StateProvince).PadRight(25).Left(25).Trim();
			} else {
				if (pSoftDescriptor.ExcludeBillingDescriptorPrefix) {
					billing.descriptor = pSoftDescriptor.BillingDescriptor.PadRight(25).Left(25).Trim();
				} else {
					billing.descriptor = string.Format("TXP*{0}", pSoftDescriptor.BillingDescriptor).PadRight(25).Left(25).Trim();
				}

			}
			//if (pCardSwiped) {
			billing.city = pSoftDescriptor.City; // string.Format("{0}, {1}", pSoftDescriptor.City, pSoftDescriptor.StateProvince); //pSoftDescriptor.City; // 
												 //} else {
			billing.url = url.PadRight(13).Left(13).Trim();     // URL can only be a max length of 13
			billing.phone = pSoftDescriptor.Phone;
			//}
			return billing;
		}

		public ProcessorResponse ChargeCard(CCRequest pRequest) {
			sale rec = new sale();
			rec.id = pRequest.VoucherNo;
			rec.orderId = pRequest.VoucherNo;
			rec.amount = Convert.ToInt64(pRequest.Amount * 100);
			rec.orderSource = orderSourceType.retail;

			rec.pos = new pos();
			rec.card = FillCardInfo(pRequest.CardData, rec.pos);
			rec.billToAddress = FillAVS(pRequest.CardData);
			rec.merchantData = new merchantDataType();
			rec.merchantData.merchantGroupingId = string.Format("D:{0}, AD{1}", pRequest.CardData.DriverID, pRequest.CardData.AffiliateDriverID);

			if (pRequest.CardData.SoftDescriptor != null) {
				rec.orderId = string.Format("{0}-{1}", pRequest.VoucherNo, pRequest.CardData.SoftDescriptor.Phone);
				rec.merchantData.affiliate = pRequest.CardData.SoftDescriptor.AffiliateName;
				SetBillingDescriptor(ref pRequest);
				rec.customBilling = FillSoftDescriptor(pRequest.CardData.SoftDescriptor, !pRequest.CardData.Track2.IsNullOrEmpty());
			}

			if (!mCredentials.TerminalType.IsNullOrEmpty()) {
				try {
					rec.orderSource = GetOrderSourceType(mCredentials.TerminalType);
				} catch (Exception) {
				}
			}

			InitService(pRequest.CardData.TestCard, pRequest.GetRideTrans);
			saleResponse response = mService.Sale(rec);
			return ProcessResponse(response, pRequest.Amount, pRequest.CardData.TestCard, rec.customBilling.descriptor);
		}

		private void SetBillingDescriptor(ref CCRequest pRequest) {
			if (pRequest.CardData.SoftDescriptor.BillingDescriptor.IsNullOrEmpty()) {
				if (!pRequest.AffiliateBillingDescriptor.IsNullOrEmpty()) {
					pRequest.CardData.SoftDescriptor.BillingDescriptor = pRequest.AffiliateBillingDescriptor;
				} else if (!mCredentials.BillingDescriptor.IsNullOrEmpty()) {
					pRequest.CardData.SoftDescriptor.BillingDescriptor = mCredentials.BillingDescriptor;
				}
				pRequest.CardData.SoftDescriptor.ExcludeBillingDescriptorPrefix = mCredentials.ExcludeBillingDescriptorPrefix;
			}
		}

		public ProcessorResponse ChargeAuth(CCRequestRefNo pRequest) {
			capture rec = new capture();
			rec.id = pRequest.VoucherNo;
			rec.litleTxnId = Convert.ToInt64(pRequest.RefNum);
			rec.amount = Convert.ToInt64(pRequest.Amount * 100);

			InitService(pRequest.TestCard, pRequest.GetRideTrans);
			captureResponse response = mService.Capture(rec);
			return ProcessResponse(response, pRequest.Amount, pRequest.TestCard);
		}

		public ProcessorResponse Refund(CCRequestRefNo pRequest) {
			credit rec = new credit();
			rec.id = pRequest.VoucherNo;
			rec.litleTxnId = Convert.ToInt64(pRequest.RefNum);
			// litle wants amount to be positve 
			pRequest.Amount = Math.Abs(pRequest.Amount);

			rec.amount = Convert.ToInt64(pRequest.Amount * 100);

			InitService(pRequest.TestCard, pRequest.GetRideTrans);
			creditResponse response = mService.Credit(rec);
			return ProcessResponse(response, pRequest.Amount, pRequest.TestCard);
		}

		public bool Void(CCRequestRefNo pRequest) {
			voidTxn rec = new voidTxn();
			rec.id = pRequest.VoucherNo;
			rec.litleTxnId = Convert.ToInt64(pRequest.RefNum);

			InitService(pRequest.TestCard, pRequest.GetRideTrans);
			litleOnlineResponseTransactionResponseVoidResponse response = mService.DoVoid(rec);
			return Convert.ToInt32(response.response) == 0;
		}



		public ProcessorResponse OpenCredit(CCRequest pRequest) {
			credit rec = new credit();
			rec.id = pRequest.VoucherNo;
			rec.orderId = pRequest.VoucherNo;
			rec.amount = Convert.ToInt64(pRequest.Amount * 100);
			rec.orderSource = orderSourceType.retail;

			rec.pos = new pos();
			rec.card = FillCardInfo(pRequest.CardData, rec.pos);
			rec.billToAddress = FillAVS(pRequest.CardData);
			rec.merchantData = new merchantDataType();
			rec.merchantData.merchantGroupingId = string.Format("D:{0}, AD{1}", pRequest.CardData.DriverID, pRequest.CardData.AffiliateDriverID);

			if (pRequest.CardData.SoftDescriptor != null) {
				rec.merchantData.affiliate = pRequest.CardData.SoftDescriptor.AffiliateName;
				rec.customBilling = FillSoftDescriptor(pRequest.CardData.SoftDescriptor, !pRequest.CardData.Track2.IsNullOrEmpty());
			}

			if (!mCredentials.TerminalType.IsNullOrEmpty()) {
				try {
					rec.orderSource = GetOrderSourceType(mCredentials.TerminalType);
				} catch (Exception) {
				}
			}

			InitService(pRequest.CardData.TestCard, pRequest.GetRideTrans);
			creditResponse response = mService.Credit(rec);
			return ProcessResponse(response, pRequest.Amount, pRequest.CardData.TestCard);

			//return Refund(pRequest);
		}




		public string GetTransactionHistory(DateTime pStart, DateTime pEnd) {
			throw new NotImplementedException();
		}

		private ProcessorResponse ProcessResponse(saleResponse pResponse, decimal pAuthAmount, bool pTestCard, string pSoftDescriptor) {
			ProcessorResponse oResult = new ProcessorResponse();

			//oResult.GatewayResponse = JSON.JsonHelper.JsonSerialize(pResponse);

			oResult.TestCard = pTestCard;
			oResult.VerisignAccountID = mCredentials.VerisignAccountID;
			oResult.MerchantID = mCredentials.MerchantID;
			oResult.CreditCardProcessor = mCredentials.MyGateway();

			oResult.Result = pResponse.response;
			oResult.Reference = pResponse.litleTxnId.ToString();
			oResult.Message = pResponse.message;
			oResult.Amount = Convert.ToDecimal(pResponse.approvedAmount);
			if (oResult.Amount == 0) {
				oResult.Amount = pAuthAmount;
			}
			if (oResult.Result.IsNullOrEmpty()) {
				oResult.Result = "0";
			}
			//fixed for Chase results
			if (oResult.Result != "0" && oResult.Message.StartsWith("Approve", StringComparison.CurrentCultureIgnoreCase)) {
				oResult.Result = "0";
			}
			if (oResult.Result == "346") {
				oResult.Message = string.Format("{0} ({1})", oResult.Message, pSoftDescriptor);
			}

			oResult.AuthCode = pResponse.authCode;
			if (pResponse.fraudResult != null) {
				oResult.AVS = pResponse.fraudResult.avsResult;
				//    oResult.AVS = "";
				//    oResult.AVSZip = "";
				//} else {
				//    oResult.AVS = pResponse.AvsResultCode.Substring(0, 1);
				//    try {
				//        oResult.AVSZip = pResponse.AvsResultCode.Substring(1, 1);
				//    } catch (Exception) {
				//        oResult.AVSZip = oResult.AVS;
				//    }
				oResult.CVV2Match = pResponse.fraudResult.cardValidationResult;
			}


			oResult.TransType = TrxTypes.Sale;
			oResult.TransDate = DateTime.Now;

			// 12/5/11 Added for Carmel
			//oResult.AVSResultCode = pResponse.AvsResultCode;

			//placed as USAePay is now sending us failed trans with a result code of 0
			if (oResult.Result.Equals("0")) {
				if (!oResult.Message.IsNullOrEmpty() && !oResult.Message.Equals("Approved", StringComparison.CurrentCultureIgnoreCase)) {
					if (!oResult.Message.Equals("Unable to get tran details (p)", StringComparison.CurrentCultureIgnoreCase)) {
						oResult.Result = "1";
					}
				}
			}
			oResult.BillingDescriptor = pSoftDescriptor;
			return oResult;
		}

		private ProcessorResponse ProcessResponse(authorizationResponse pResponse, decimal pAuthAmount, bool pTestCard, string pDescriptor) {
			ProcessorResponse oResult = new ProcessorResponse();

			//oResult.GatewayResponse = JSON.JsonHelper.JsonSerialize(pResponse);

			oResult.TestCard = pTestCard;
			oResult.VerisignAccountID = mCredentials.VerisignAccountID;
			oResult.MerchantID = mCredentials.MerchantID;
			oResult.CreditCardProcessor = mCredentials.MyGateway();

			oResult.Result = pResponse.response;
			oResult.Reference = pResponse.litleTxnId.ToString();
			oResult.Message = pResponse.message;
			oResult.Amount = Convert.ToDecimal(pResponse.approvedAmount);
			if (oResult.Amount == 0) {
				oResult.Amount = pAuthAmount;
			}
			if (oResult.Result.IsNullOrEmpty()) {
				oResult.Result = "0";
			}
			//fixed for Chase results
			if (oResult.Result != "0" && oResult.Message.StartsWith("Approve", StringComparison.CurrentCultureIgnoreCase)) {
				oResult.Result = "0";
			}

			if (oResult.Result == "346") {
				oResult.Message = string.Format("{0} ({1})", oResult.Message, pDescriptor);
			}


			oResult.AuthCode = pResponse.authCode;
			if (pResponse.fraudResult != null) {
				oResult.AVSResultCode = ConvertToUSAePayAVSResultCode(pResponse.fraudResult.avsResult);
				if (!oResult.AVSResultCode.IsNullOrEmpty()) {
					oResult.AVS = oResult.AVSResultCode.Left(1);
				}
				oResult.CVV2Match = pResponse.fraudResult.cardValidationResult;
			}

			oResult.TransType = TrxTypes.Authorization;
			oResult.TransDate = DateTime.Now;



			//placed as USAePay is now sending us failed trans with a result code of 0
			if (oResult.Result.Equals("0")) {
				if (!oResult.Message.IsNullOrEmpty() && !oResult.Message.Equals("Approved", StringComparison.CurrentCultureIgnoreCase)) {
					if (!oResult.Message.Equals("Unable to get tran details (p)", StringComparison.CurrentCultureIgnoreCase)) {
						oResult.Result = "1";
					}
				}
			}

			return oResult;
		}

		private string ConvertToUSAePayAVSResultCode(string pAVSResult) {
			if (pAVSResult.IsNullOrEmpty()) {
				return pAVSResult;
			}

			string result = pAVSResult.PadLeft(2, '0').Right(2);

			switch (result) {
				case "00": // 5-Digit zip and address match
					result = "YYY";
					break;

				case "01": // 9 - Digit zip and address match
					result = "YYX";
					break;

				case "02": // Postal code and address match
					result = "GGG";
					break;

				case "10": // 5 - Digit zip matches, address does not match
					result = "NYZ";
					break;

				case "11": // 9 - Digit zip matches, address does not match
					result = "NYW";
					break;

				case "12": // Zip does not match, address matches
					result = "YNA";
					break;

				case "13": // Postal code does not match, address matches
					result = "YYG";
					break;

				case "14": // Postal code matches, address not verified
					result = "YNN";
					break;

				case "20": // Neither zip nor address match
					result = "NNN";
					break;

				case "30": // AVS service not supported by issuer
					result = "XXS";
					break;

				case "31": // AVS system not available
				case "32": // Address unavailable
				case "33": // General error
				case "40": // Address failed Litle & Co.edit checks
					result = "XXR";
					break;

				case "34": // AVS not performed
					result = "XXU";
					break;

			}
			return result;
		}

		private ProcessorResponse ProcessResponse(captureResponse pResponse, decimal pAmount, bool pTestCard) {
			ProcessorResponse oResult = new ProcessorResponse();
			//oResult.GatewayResponse = JSON.JsonHelper.JsonSerialize(pResponse);

			oResult.TestCard = pTestCard;
			oResult.VerisignAccountID = mCredentials.VerisignAccountID;
			oResult.MerchantID = mCredentials.MerchantID;
			oResult.CreditCardProcessor = mCredentials.MyGateway();

			oResult.Result = pResponse.response;
			oResult.Reference = pResponse.litleTxnId.ToString();
			oResult.Message = pResponse.message;
			oResult.Amount = pAmount;
			if (oResult.Result.IsNullOrEmpty()) {
				oResult.Result = "0";
			}
			//fixed for Chase results
			if (oResult.Result != "0" && oResult.Message.StartsWith("Approve", StringComparison.CurrentCultureIgnoreCase)) {
				oResult.Result = "0";
			}

			oResult.TransType = TrxTypes.DelayedCapture;
			oResult.TransDate = DateTime.Now;

			if (oResult.Result.Equals("0")) {
				if (!oResult.Message.IsNullOrEmpty() && !oResult.Message.Equals("Approved", StringComparison.CurrentCultureIgnoreCase)) {
					if (!oResult.Message.Equals("Unable to get tran details (p)", StringComparison.CurrentCultureIgnoreCase)) {
						oResult.Result = "1";
					}
				}
			}

			return oResult;
		}

		private ProcessorResponse ProcessResponse(creditResponse pResponse, decimal pAmount, bool pTestCard) {
			ProcessorResponse oResult = new ProcessorResponse();
			//oResult.GatewayResponse = JSON.JsonHelper.JsonSerialize(pResponse);

			oResult.TestCard = pTestCard;
			oResult.VerisignAccountID = mCredentials.VerisignAccountID;
			oResult.MerchantID = mCredentials.MerchantID;
			oResult.CreditCardProcessor = mCredentials.MyGateway();

			oResult.Result = pResponse.response;
			oResult.Reference = pResponse.litleTxnId.ToString();
			oResult.Message = pResponse.message;
			oResult.Amount = pAmount;
			if (oResult.Result.IsNullOrEmpty()) {
				oResult.Result = "0";
			}
			//fixed for Chase results
			if (oResult.Result != "0" && oResult.Message.StartsWith("Approve", StringComparison.CurrentCultureIgnoreCase)) {
				oResult.Result = "0";
			}

			oResult.TransType = TrxTypes.Credit;
			oResult.TransDate = DateTime.Now;

			if (oResult.Result.Equals("0")) {
				if (!oResult.Message.IsNullOrEmpty() && !oResult.Message.Equals("Approved", StringComparison.CurrentCultureIgnoreCase)) {
					if (!oResult.Message.Equals("Unable to get tran details (p)", StringComparison.CurrentCultureIgnoreCase)) {
						oResult.Result = "1";
					}
				}
			}


			return oResult;
		}


		/// <summary>
		/// Fills the card info.
		/// </summary>
		/// <param name="pCardData">The card data.</param>
		/// <returns></returns>
		private cardType FillCardInfo(CreditCard pCardData, pos pPos) {
			cardType card = new cardType();


			if (pCardData.Track2.IsNullOrEmpty()) {
				card.number = pCardData.CardNo;
				card.expDate = pCardData.CardExpiryAsMMYY;
				card.cardValidationNum = pCardData.CVV;
				pPos.capability = posCapabilityTypeEnum.keyedonly;
				pPos.entryMode = posEntryModeTypeEnum.keyed;
				if (pCardData.CustomerSigned) {
					pPos.cardholderId = posCardholderIdTypeEnum.signature;
				} else {
					pPos.cardholderId = posCardholderIdTypeEnum.directmarket;
				}
			} else {
				card.track = pCardData.Track2;
				pPos.capability = posCapabilityTypeEnum.magstripe;
				pPos.entryMode = posEntryModeTypeEnum.completeread;
				pPos.cardholderId = posCardholderIdTypeEnum.signature;
			}

			card.type = FillCardType(pCardData.CardNo);
			return card;
		}

		private methodOfPaymentTypeEnum FillCardType(string pCardNo) {
			string cardType = CardUtils.DetermineCardType(pCardNo);

			switch (cardType.ToUpper()) {
				case "MASTERCARD":
					return methodOfPaymentTypeEnum.MC;

				case "VISA":
					return methodOfPaymentTypeEnum.VI;

				case "AMEX":
					return methodOfPaymentTypeEnum.AX;

				case "DISCOVER":
					return methodOfPaymentTypeEnum.DI;

				case "DINERS":
					return methodOfPaymentTypeEnum.DC;

				case "JCB":
					return methodOfPaymentTypeEnum.JC;

			}
			return methodOfPaymentTypeEnum.PP;

		}


		private contact FillAVS(CreditCard pCardData) {
			contact contact = null;
			if (!pCardData.AvsStreet.IsNullOrEmpty() || !pCardData.AvsZip.IsNullOrEmpty()) {
				contact = new contact();
				contact.addressLine1 = pCardData.AvsStreet;
				contact.zip = pCardData.AvsZip;

			}
			return contact;
		}


		private orderSourceType GetOrderSourceType(string pSourceType) {
			switch (pSourceType.ToLower()) {
				case "ecommerce":
					return orderSourceType.ecommerce;
				case "installment":
					return orderSourceType.installment;
				case "item3dsattempted":
					return orderSourceType.item3dsAttempted;
				case "item3dsauthenticated":
					return orderSourceType.item3dsAuthenticated;
				case "mailorder":
					return orderSourceType.mailorder;
				case "recurring":
					return orderSourceType.recurring;
				case "recurringtel":
					return orderSourceType.recurringtel;
				case "retail":
					return orderSourceType.retail;
				case "telephone":
					return orderSourceType.telephone;
			}
			return orderSourceType.retail;
		}


		public ProcessorResponse AvsOnly(CCRequest pRequest) {
			authorization auth = new authorization();
			auth.orderId = pRequest.VoucherNo;
			auth.id = pRequest.VoucherNo;
			auth.amount = 0;
			auth.orderSource = orderSourceType.retail;

			//if (pRequest.CardData.TestCard && pRequest.CardData.CardNo != CardUtils.TEST_LITLE_VISA) {
			//    if (!pRequest.CardData.Track2.IsNullOrEmpty()) {
			//        pRequest.CardData.Track2 = pRequest.CardData.Track2.Replace(pRequest.CardData.CardNo, CardUtils.TEST_LITLE_VISA);
			//    }
			//    pRequest.CardData.CardNo = CardUtils.TEST_LITLE_VISA;
			//}
			auth.merchantData = new merchantDataType();
			//if (pRequest.CardData.SoftDescriptor != null) {
			//    auth.merchantData.affiliate = pRequest.CardData.SoftDescriptor.AffiliateName;
			//    auth.customBilling = FillSoftDescriptor(pRequest.CardData.SoftDescriptor, !pRequest.CardData.Track2.IsNullOrEmpty());
			//}

			if (pRequest.CardData.SoftDescriptor != null) {
				auth.orderId = string.Format("{0}-{1}", pRequest.VoucherNo, pRequest.CardData.SoftDescriptor.Phone);
				auth.merchantData.affiliate = pRequest.CardData.SoftDescriptor.AffiliateName;
				SetBillingDescriptor(ref pRequest);
				auth.customBilling = FillSoftDescriptor(pRequest.CardData.SoftDescriptor, !pRequest.CardData.Track2.IsNullOrEmpty());
			}


			auth.pos = new pos();
			auth.card = FillCardInfo(pRequest.CardData, auth.pos);
			auth.billToAddress = FillAVS(pRequest.CardData);
			auth.merchantData.merchantGroupingId = string.Format("D:{0}, AD{1}", pRequest.CardData.DriverID, pRequest.CardData.AffiliateDriverID);

			if (!mCredentials.TerminalType.IsNullOrEmpty()) {
				try {
					auth.orderSource = mCredentials.TerminalType.ToLower().ToEnum<orderSourceType>();
				} catch (Exception) {
				}
			}
			SetBillingDescriptor(ref pRequest);

			InitService(pRequest.CardData.TestCard, pRequest.GetRideTrans);
			authorizationResponse response = mService.Authorize(auth);
			return ProcessResponse(response, 0, pRequest.CardData.TestCard, "");
		}

		public ProcessorResponse AvsCheck(CCRequest pRequest) {
			throw new NotImplementedException();
		}

		public ProcessorResponse ProcessCheck(CheckRequest pRequest) {
			throw new NotImplementedException();
		}

		public ProcessorResponse RefundCheck(CheckRequest pRequest) {
			throw new NotImplementedException();
		}

		public List<CheckStatus> GetCheckTransactionHistory(DateTime pStart, DateTime pEnd) {
			throw new NotImplementedException();
		}
	}
}
