﻿using CabRideEngineDapper;
using CabRideEngineDapper.Utils;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Hosting;
using System.Web.Http;
using TaxiPassCommon;


namespace MobileTransWebAPI.Controllers {
	public partial class CreditCardReceiptController {

		[HttpPatch]
		public bool EMailChargePassReceipt(string pTransNo, string pEmailTo) {

			using (var conn = SqlHelper.OpenSqlConnection()) {
				DriverPayments rec = DriverPayments.GetPayment(conn, pTransNo);

				if (!pEmailTo.Contains("@")) {
					return SendChargePassReceiptViaSMS(conn, rec, pEmailTo);
				}

				string sReceipt = CreateReceipt(rec);

				var cred = SystemDefaultsDict.GetEmailCreds(CabRideEngineDapper.Enums.EMailReportFrom.Receipt);
				TaxiPassCommon.Mail.EMailMessage email = new TaxiPassCommon.Mail.EMailMessage(cred, string.Format("{0} ChargePass Receipt", rec.Affiliate.Name), sReceipt);
				email.UseWebServiceToSendMail = Properties.Settings.Default.SendEmailViaWebService;
				email.SendMail(pEmailTo);
			}
			return true;
		}

		[HttpPatch]
		public bool EMailTaxiChargeReceipt(string pTransNo, string pEmailTo) {

			using (var conn = SqlHelper.OpenSqlConnection()) {
				DriverPayments rec = DriverPayments.GetPayment(conn, pTransNo);

				string sReceipt = BuildHTMLReceipt(conn, rec, ""); // CreateTaxiChargeReceipt(rec);

				var cred = SystemDefaultsDict.GetEmailCreds(CabRideEngineDapper.Enums.EMailReportFrom.Report);
				TaxiPassCommon.Mail.EMailMessage email = new TaxiPassCommon.Mail.EMailMessage(cred, string.Format("{0} Taxi-Charge Receipt", rec.Affiliate.Name), sReceipt);
				email.UseWebServiceToSendMail = Properties.Settings.Default.SendEmailViaWebService;
				email.SendMail(pEmailTo);
			}
			return true;
		}

		[HttpPatch]
		public string CreateReceipt(DriverPayments pPayments) {
			//string sPath = pRequest.MapPath(".");
			string sPath = HostingEnvironment.MapPath(string.Format("~/HTMLTemplates/{0}ChargePassReceipt.html", pPayments.Fare < 0 ? "Refund" : ""));
			string sReceipt = System.IO.File.ReadAllText(sPath);

			Affiliate affiliate = pPayments.Affiliate;
			string sLogo = affiliate.AffiliateLogo;

			sReceipt = sReceipt.Replace("{AffiliateLogo}", string.Format("<img src='{0}' alt='{1}' height='75'>", sLogo, affiliate.Name));
			//sReceipt = sReceipt.Replace("{CardHolder}", pPayments.CardHolder);

			string companyInfo = affiliate.Name;
			if (!affiliate.Line1.IsNullOrEmpty()) {
				companyInfo = string.Format("{0}<br>{1}", companyInfo, affiliate.Line1);
			}
			if (!affiliate.Line2.IsNullOrEmpty()) {
				companyInfo = string.Format("{0}, {1}", companyInfo, affiliate.Line2);
			}
			if (!affiliate.City.IsNullOrEmpty()) {
				companyInfo = string.Format("{0}<br>{1}, {2} {3}", companyInfo, affiliate.City, affiliate.State, affiliate.ZIPCode);
			} else if (!affiliate.State.IsNullOrEmpty()) {
				companyInfo = string.Format("{0}<br>{1} {2}", companyInfo, affiliate.State, affiliate.ZIPCode);
			}
			if (!affiliate.CorporatePhone.IsNullOrEmpty()) {
				companyInfo = string.Format("{0}<br>{1}", companyInfo, affiliate.CorporatePhone.FormattedPhoneNumber().ToUpper());
			}
			sReceipt = sReceipt.Replace("{CompanyInfo}", companyInfo);

			sReceipt = sReceipt.Replace("{PaymentDate}", pPayments.ChargeDate.Value.ToString("MMM dd, yyyy hh:mm tt"));

			string invNo = "";
			if (!pPayments.ReferenceNo.IsNullOrEmpty()) {
				invNo = string.Format("<br /><b>Invoice #:</b> {0}", pPayments.ReferenceNo);
			}

			sReceipt = sReceipt.Replace("{ReferenceNo}", invNo);
			sReceipt = sReceipt.Replace("{HideInvoice}", pPayments.ReferenceNo.IsNullOrEmpty() ? "hidden" : "visible");

			Reservations res = Reservations.GetResByDriverPaymentID(pPayments.DriverPaymentID);
			sReceipt = sReceipt.Replace("{HideResDate}", res.IsNullEntity ? "hidden" : "visible");
			string dateOfService = "";
			if (!res.IsNullEntity) {
				dateOfService = string.Format("<br /><b>Date of Service:</b> {0}", res.PickUpDate.ToString("MMM dd, yyyy hh:mm tt"));
			}
			sReceipt = sReceipt.Replace("{ResDate}", dateOfService);

			//sReceipt = sReceipt.Replace("{HideTest}", pPayments.Test ? "visible" : "hidden");
			if (pPayments.Test) {
				string msg = @"<tr>
                                    <td style='width:100%; text-align:center'>
                                        <br />
                                        <font size='5' color='#333'><span style='font-size:22px;font-weight:bold;color:red'>TEST TRANSACTION</span></font>
                                    </td>
                                </tr>";
				sReceipt = sReceipt.Replace("{TestMsg}", msg);
			} else {
				sReceipt = sReceipt.Replace("{TestMsg}", "");
			}

			string cardType = pPayments.CardType;
			if (cardType.IsNullOrEmpty()) {
				cardType = "Card";
			}
			sReceipt = sReceipt.Replace("{PaymentType}", pPayments.CardType);
			sReceipt = sReceipt.Replace("{TransNo}", pPayments.TransNo);

			string billingAddress = ("" + pPayments.CardAddress.DecryptIceKey()).Trim();
			if (!billingAddress.IsNullOrEmpty()) {
				billingAddress += "<br><br>";
			}
			billingAddress += string.Format("Card ending in {0}", pPayments.CardNumberDisplay);

			if (!pPayments.CorporateName.IsNullOrEmpty()) {
				billingAddress = string.Format("{0}<br>{1}", pPayments.CorporateName, billingAddress);
			} else if (!pPayments.CardHolder.IsNullOrEmpty()) {
				string name = pPayments.CardHolder.DecryptIceKey();
				if (name == pPayments.CardHolder) {
					Encryption enc = new Encryption();
					name = enc.Decrypt(name, pPayments.Salt);
				}
				billingAddress = string.Format("{0}<br>{1}", name, billingAddress);
			}

			sReceipt = sReceipt.Replace("{BillingInfo}", billingAddress);

			sReceipt = sReceipt.Replace("{CardNo}", pPayments.CardNumberDisplay);

			sReceipt = sReceipt.Replace("{AffiliateEmail}", affiliate.email);
			string emailVisibility = "visible";
			if (affiliate.email.IsNullOrEmpty()) {
				emailVisibility = "hidden";
			}
			sReceipt = sReceipt.Replace("{HideEmail}", emailVisibility);
			sReceipt = sReceipt.Replace("{AffiliatePhone}", affiliate.CorporatePhone.FormattedPhoneNumber().ToUpper());
			sReceipt = sReceipt.Replace("{Affiliate}", affiliate.Name);

			sReceipt = sReceipt.Replace("{SubTotal}", pPayments.Fare.ToString("C"));
			sReceipt = sReceipt.Replace("{TPFee}", pPayments.TaxiPassFee.ToString("C"));
			sReceipt = sReceipt.Replace("{TotalCharge}", pPayments.TotalCharge.ToString("C"));

			string softDesc = affiliate.DefaultPlatform;
			DriverPaymentRef desc = DriverPaymentRef.GetByGroupAndReference(pPayments.DriverPaymentID, "BillingDescriptor", "BillingDescriptor");
			if (!desc.ValueString.IsNullOrEmpty()) {
				softDesc = desc.ValueString;
			}
			sReceipt = sReceipt.Replace("{PaymentAppear}", softDesc);

			return sReceipt;
		}


		// *****************************************************************
		// Send the recept via sms
		// *****************************************************************
		[HttpPatch]
		public bool SendChargePassReceiptViaSMS(SqlConnection pConn, DriverPayments pPayments, string pCellNumber) {
			string id = String.Format("dp={0}", pPayments.TransNo).EncryptIceKey();
			string pMessage = BuildChargePassSMSReceipt(pPayments, id);
			var msg = Utils.Common.SendSMS(SystemDefaults.GetDefaults(pConn), pCellNumber, pMessage);
			return true;

		}


		private string BuildChargePassSMSReceipt(DriverPayments pPayments, string pID) {
			StringBuilder msg = new StringBuilder();

			string accountName = GetAccountName(pPayments, "");
			string url = Properties.Settings.Default.GetRideReceiptURL;
			if (accountName.IsNullOrEmpty()) {
				accountName = pPayments.Platform.ToString();
				url = Properties.Settings.Default.ChargePassReceiptURL;
			}
			msg.Append(String.Format("Receipt for {1} Voucher Number {0}", pPayments.Test ? "Test" : pPayments.TransNo, accountName));
			msg.Append(String.Format("\n\n{0}{1}", url, pID));

			return msg.ToString();
		}

		[HttpPatch]
		public static string GetAccountName(DriverPayments pDriverPayments, string pAccountName) {
			if (pAccountName.IsNullOrEmpty()) {
				using (var conn = SqlHelper.OpenSqlConnection()) {
					dynamic oRideAccounts = conn.Query("usp_Reservation_GetAccount_From_DP", new { TransNo = pDriverPayments.TransNo }, commandType: CommandType.StoredProcedure).FirstOrDefault();
					if (oRideAccounts != null && !oRideAccounts.AccountName.IsNullOrEmpty()) {
						pAccountName = oRideAccounts[0].AccountName;
					}
				}
			}
			return pAccountName;
		}


		// *************************************************************
		// Build a voucher receipt to email
		// Get Ride.TaxiPassFee is calculated:
		//		Calc total ServiceTipPerc
		//			'Half' to Gratuity
		//			Remainder goes to TPFee
		//		If a Ride coupon was used 
		//			Deduct the amt from TPFee
		// *************************************************************
		[HttpPatch]
		public string BuildHTMLReceipt(SqlConnection pConn, DriverPayments pDriverPayments, string pLogo) {
			string sStage = "0";

			decimal dGratuity = pDriverPayments.Gratuity;
			decimal dDiscount = pDriverPayments.Discount.GetValueOrDefault(0);
			bool blnIsRide = false;
			string sReceipt = "starting";
			string sMarketing = "";
			StringBuilder sWork = new StringBuilder();
			int iStart;
			int iEnd;
			string sAccountNo = "";
			try {
				string sPath = HostingEnvironment.MapPath("~/HTMLTemplates/VoucherReceipt.htm");
				// sPath = @"C:\My Projects\VS2010 TaxiPass\MobileTransWebAPI\MobileTransWebAPI\HTMLTemplates\VoucherReceipt.htm";
				//string sPath = Path.Combine(Properties.Settings.Default.TemplatePath, "VoucherReceipt.htm");
				//if (HttpContext.Current.Request.Url.ToString().ToLower().Contains("localhost")) {
				//	sPath = @"C:\My Projects\VS2010 TaxiPass\MobileTransWebAPI\MobileTransWebAPI\HTMLTemplates\VoucherReceipt.htm";
				//	pLogo = "ride";
				//}
				WebApiApplication.LogIt("Voucher Receipt Path", sPath);

				sReceipt = System.IO.File.ReadAllText(sPath);

				// Logo  
				// 8/28/2013 make everything GetRide
				string sLogo = "http://GetRide.Com/images/Logo250WBlack.png";
				if (IsTaxiPassReceipt(pDriverPayments)) {
					sLogo = "http://GetRide.Com/images/newtaxipass.png";
					pLogo = "Taxi";
				}
				if (pDriverPayments.Platform.Equals(Platforms.StorePass.ToString())) {
					sLogo = "http://receipt.storepass.co/images/store-pass.png";
				} else if (pDriverPayments.Platform.Equals(Platforms.ChargePass.ToString())) {
					sLogo = "http://receipt.chargepass.co/images/charge-pass.png";
				}
				string sLogoWidth = "280px";

				//string sCompany =  "TaxiPass";
				switch (pLogo.ToLower()) {
					case "taxi":
						sStage = "1";
						if (!IsTaxiPassReceipt(pDriverPayments)) {
							//sMarketing = "Save $5.00 on your first taxi when booked thru <a href='http://www.getride.com/App'>GetRide</a><span style='font-size: small'>&#0153;</span> iPhone/Android booking app";
						}
						break;

					case "ride":
						sStage = "2";

						blnIsRide = true;
						dGratuity = pDriverPayments.Gratuity + pDriverPayments.TaxiPassFee;
						dDiscount = pDriverPayments.RideCouponAmount;
						sMarketing = "Receipt";

						dynamic oRideAccounts = pConn.Query("usp_Reservation_GetAccount_From_DP", new { TransNo = pDriverPayments.TransNo }, commandType: CommandType.StoredProcedure).ToList();
						sStage = "3";

						if (oRideAccounts != null) {
							if (oRideAccounts.Length > 0) {
								sAccountNo = oRideAccounts[0]["AccountNo"].ToString();
								mAccountName = oRideAccounts[0]["AccountName"].ToString();

								sStage = "4";
								sWork.AppendFormat("<Div class='Rounded' style='background-color: #A4E1FF;'><span style='font-size: 24pt'>{0} Receipt</span><br>", mAccountName);
								if (oRideAccounts[0]["AccountPhone"].ToString().Length > 0) {
									// AccountPhone is already formatted (nnn) nnn-nnnn
									sWork.Append(oRideAccounts[0]["AccountPhone"]);
								}
								sWork.Append("</div>");
								sStage = "5";

								sMarketing = sWork.ToString();
								sLogoWidth = "0px";
								sLogo = "http://GetRide.Com/images/Spacer.png";
								sStage = "6";
							}
						}


						break;

					default:
						sStage = "7";
						sMarketing = "LimoPass  Receipt";
						break;
				}

				sStage = "8";
				sReceipt = sReceipt.Replace("|logo", sLogo);
				sStage = "9";
				sReceipt = sReceipt.Replace("|sLogoWidth", sLogoWidth);
				sStage = "10";

				// sMarketing
				if (("" + pDriverPayments.Affiliate.DefaultPlatform).Equals(Platforms.ChargePass.ToString(), StringComparison.CurrentCultureIgnoreCase)) {
					sMarketing = "";
				}
				sReceipt = sReceipt.Replace("|Marketing", sMarketing);


				// Fleet
				string sFleet = pDriverPayments.Affiliate.Name;
				if (pDriverPayments.Drivers.FleetID.HasValue) {
					if (!IsTaxiPassReceipt(pDriverPayments)) {
						sFleet = pDriverPayments.Drivers.Fleet.Fleet;
					}
				}
				sReceipt = sReceipt.Replace("|fleet", sFleet);

				// Cab #
				if (pDriverPayments.VehicleID.HasValue) {
					sReceipt = sReceipt.Replace("|taxinumber", pDriverPayments.Vehicles.VehicleNo);
				} else {
					iStart = sReceipt.IndexOf("<tr id='trCab'>");
					iEnd = sReceipt.IndexOf("</tr>", iStart) + 5;
					sReceipt = sReceipt.Substring(0, iStart - 1) + sReceipt.Substring(iEnd);
				}

				// Date
				sReceipt = sReceipt.Replace("|date", String.Format("{0:f}", pDriverPayments.ChargeDate));

				// TransNo
				sReceipt = sReceipt.Replace("|transno", pDriverPayments.TransNo);

				// Card
				sReceipt = sReceipt.Replace("|card", pDriverPayments.CardNumberDisplay);

				// Fare
				string[] valList = SystemDefaultsDict.GetByKey("Receipt", "EWRAffiliates", false).ValueString.Split('|');
				List<long> affIDList = new List<long>();
				foreach (string id in valList) {
					affIDList.Add(Convert.ToInt64(id));
				}

				string note = "";
				sReceipt = sReceipt.Replace("|fare", pDriverPayments.Fare.ToString("C"));
				if (pDriverPayments.Platform.Equals(Platforms.ChargePass.ToString())) {
					sReceipt = sReceipt.Replace("Fare:", "Total:");
				} else if (affIDList.Contains(pDriverPayments.AffiliateID.GetValueOrDefault(0))) {
					sReceipt = sReceipt.Replace("Fare:", "Fare plus Round Trip Tolls:");
					note = "*Please note round trip tolls to New York average $20+";
				}


				// Tolls
				if (pDriverPayments.Tolls.GetValueOrDefault(0) == 0) {
					iStart = sReceipt.IndexOf("<tr id='trTolls'>");
					iEnd = sReceipt.IndexOf("</tr>", iStart) + 5;
					sReceipt = sReceipt.Substring(0, iStart - 1) + sReceipt.Substring(iEnd);
				} else {
					sReceipt = sReceipt.Replace("|tolls", pDriverPayments.Tolls.GetValueOrDefault(0).ToString("C"));

				}


				// Airport Fee
				if (pDriverPayments.AirportFee == 0) {
					iStart = sReceipt.IndexOf("<tr id='trAirport'>");
					iEnd = sReceipt.IndexOf("</tr>", iStart) + 5;
					sReceipt = sReceipt.Substring(0, iStart - 1) + sReceipt.Substring(iEnd);
				} else {
					sReceipt = sReceipt.Replace("|airport", pDriverPayments.AirportFee.ToString("C"));

				}

				// Misc Fee
				if (pDriverPayments.MiscFee == 0) {
					iStart = sReceipt.IndexOf("<tr id='trMisc'>");
					iEnd = sReceipt.IndexOf("</tr>", iStart) + 5;
					sReceipt = sReceipt.Substring(0, iStart - 1) + sReceipt.Substring(iEnd);
				} else {
					sReceipt = sReceipt.Replace("|misc", pDriverPayments.MiscFee.ToString("C"));
				}

				// Gratuity
				if (blnIsRide) {
					sReceipt = sReceipt.Replace("+ Gratuity:", "+ Service:");
					sReceipt = sReceipt.Replace("|gratuity", dGratuity.ToString("C"));
				} else {
					if (dGratuity == 0) {
						iStart = sReceipt.IndexOf("<tr id='trGratuity'>");
						iEnd = sReceipt.IndexOf("</tr>", iStart) + 5;
						sReceipt = sReceipt.Substring(0, iStart - 1) + sReceipt.Substring(iEnd);
					} else {
						sReceipt = sReceipt.Replace("|gratuity", dGratuity.ToString("C"));
					}
				}

				// TaxiPass Fee
				if (pDriverPayments.TaxiPassFee == 0 || blnIsRide) {
					iStart = sReceipt.IndexOf("<tr id='trTaxiPassFee'>");
					iEnd = sReceipt.IndexOf("</tr>", iStart) + 5;
					sReceipt = sReceipt.Substring(0, iStart - 1) + sReceipt.Substring(iEnd);
				} else {
					if (pDriverPayments.Platform.Equals(Platforms.StorePass.ToString()) ||
						pDriverPayments.Platform.Equals(Platforms.MedPass.ToString()) ||
						pDriverPayments.Platform.Equals(Platforms.ZPass.ToString()) ||
						pDriverPayments.Platform.Equals(Platforms.ChargePass.ToString())) {
						sReceipt = sReceipt.Replace("+ Extras", "+ Processing");
					} else if (sLogo.Equals("http://GetRide.Com/images/Logo250WBlack.png")) {
						sReceipt = sReceipt.Replace("+ Extras", "+ Tech Fee");
					} else if (pDriverPayments.Platform.Equals(Platforms.EWRTablet.ToString())) {
						sReceipt = sReceipt.Replace("+ Extras", "+ Tech Fee");
					}
					sReceipt = sReceipt.Replace("|extra", pDriverPayments.TaxiPassFee.ToString("C"));
				}


				// Wait Time
				if (pDriverPayments.WaitTime == 0) {
					iStart = sReceipt.IndexOf("<tr id='trWaitTime'>");
					iEnd = sReceipt.IndexOf("</tr>", iStart) + 5;
					sReceipt = sReceipt.Substring(0, iStart - 1) + sReceipt.Substring(iEnd);
				} else {
					sReceipt = sReceipt.Replace("|waittime", pDriverPayments.WaitTime.Value.ToString("C"));
				}

				// Parking
				if (pDriverPayments.Parking == 0) {
					iStart = sReceipt.IndexOf("<tr id='trParking'>");
					iEnd = sReceipt.IndexOf("</tr>", iStart) + 5;
					sReceipt = sReceipt.Substring(0, iStart - 1) + sReceipt.Substring(iEnd);
				} else {
					sReceipt = sReceipt.Replace("|parking", pDriverPayments.Parking.Value.ToString("C"));
				}

				// Stops
				if (pDriverPayments.Stops == 0) {
					iStart = sReceipt.IndexOf("<tr id='trStops'>");
					iEnd = sReceipt.IndexOf("</tr>", iStart) + 5;
					sReceipt = sReceipt.Substring(0, iStart - 1) + sReceipt.Substring(iEnd);
				} else {
					sReceipt = sReceipt.Replace("|stops", pDriverPayments.Stops.Value.ToString("C"));
				}

				// Discount
				if (dDiscount == 0) {
					iStart = sReceipt.IndexOf("<tr id='trDiscount'>");
					iEnd = sReceipt.IndexOf("</tr>", iStart) + 5;
					sReceipt = sReceipt.Substring(0, iStart - 1) + sReceipt.Substring(iEnd);
				} else {
					sReceipt = sReceipt.Replace("|discount", dDiscount.ToString("C"));
				}

				// Total
				sReceipt = sReceipt.Replace("|total", pDriverPayments.TotalCharge.ToString("C"));

				// Signature image
				if (pDriverPayments.DriverPaymentAux.SignaturePath.IsNullOrEmpty()) {
					iStart = sReceipt.IndexOf("<tr id='trSignature'>");
					iEnd = sReceipt.IndexOf("</tr>", iStart) + 5;
					sReceipt = sReceipt.Substring(0, iStart - 1) + sReceipt.Substring(iEnd);
				} else {
					string ImgPath = ResizeSignatureImage(pConn, pDriverPayments.DriverPaymentAux.SignaturePath.Replace(@"\", "/"));
					sReceipt = sReceipt.Replace("|signature", ImgPath);
				}

				// Receipt.com hyperlink
				sWork.Remove(0, sWork.Length);
				if (IsTaxiPassReceipt(pDriverPayments)) {
					string sURL = "https://mobilmoney.zendesk.com/hc/en-us/requests/new";
					sWork.AppendFormat("<a href='{0}'>", sURL);
					sReceipt = sReceipt.Replace("|HeaderLink", sWork.ToString());

					sWork.Append("Click here for customer service</A>");
				} else {
					if (pDriverPayments.Platform.Equals(Platforms.ChargePass.ToString()) ||
						pDriverPayments.Platform.Equals(Platforms.StorePass.ToString()) ||
						pDriverPayments.Platform.Equals(Platforms.ZPass.ToString()) ||
						pDriverPayments.Platform.Equals(Platforms.MedPass.ToString())) {
						sWork = new StringBuilder();
						sReceipt = sReceipt.Replace("|HeaderLink", sWork.ToString());
					} else {
						string sURL = Properties.Settings.Default.GetRideReceiptURL;
						if (!sAccountNo.IsNullOrEmpty()) {
							sURL = sURL.Replace("http://", "http://" + sAccountNo + ".");
						}
						sWork.AppendFormat("<a href='{0}{1}'>", sURL, ("DP=" + pDriverPayments.TransNo).EncryptIceKey());
						sReceipt = sReceipt.Replace("|HeaderLink", sWork.ToString());

						sWork.Append("Get Receipt</A>");
					}
				}
				if (!sWork.ToString().IsNullOrEmpty()) {
					if (!note.IsNullOrEmpty()) {
						note = "<br><br><br>" + note;
					}
					note = sWork.ToString() + note;
				}
				sReceipt = sReceipt.Replace("|ReceiptLink", note);

				string footerMsg = pDriverPayments.Affiliate.ReceiptFooterText;
				if (!footerMsg.IsNullOrEmpty()) {
					footerMsg = $"{footerMsg}<br /><br />";
				}
				sReceipt = sReceipt.Replace("{footerMessage}", footerMsg);

				string referralLink = $"<a href='{SystemDefaultsDict.GetByKey("MarketingReferral", "Marketing_QR_Code_Url", false).ValueString}?driver_id={pDriverPayments.AffiliateDrivers.Cell}'>Click here</a>";
				sReceipt = sReceipt.Replace("|ReferralLink", referralLink);
			} catch (Exception ex) {
				sReceipt = ex.Message + Environment.NewLine + "Stage: " + sStage + Environment.NewLine + sReceipt;
			}


			return sReceipt;
		}

		[HttpPatch]
		public static bool IsTaxiPassReceipt(DriverPayments pDriverPayments) {
			return pDriverPayments.Platform.Equals(Platforms.Western.ToString())
				|| pDriverPayments.Platform.Equals(Platforms.TPPos.ToString())
				|| pDriverPayments.Platform.Equals(Platforms.NexStep.ToString())
				|| pDriverPayments.Platform.Equals(Platforms.Kiosk.ToString())
				|| pDriverPayments.Platform.Equals(Platforms.EWRTablet.ToString())
				|| pDriverPayments.Affiliate.DefaultPlatform.ToLower().Contains("ewr");
		}

		[HttpPatch]
		public static string GetReceiptURL(DriverPayments pDriverPayments) {
			string url = Properties.Settings.Default.GetRideReceiptURL;
			if (IsTaxiPassReceipt(pDriverPayments)) {
				url = Properties.Settings.Default.TaxiPassReceiptURL;
			}
			return url;
		}


		// **************************************************************
		// Voucher signatures are too big for Receipts, shrink em down
		// **************************************************************
		private string ResizeSignatureImage(SqlConnection pConn, string pImage) {
			SystemDefaults oSysDefaults = SystemDefaults.GetDefaults(pConn);

			string pImgRoot = Properties.Settings.Default.SignaturePath;
			if (!pImgRoot.EndsWith(@"\")) {
				pImgRoot += @"\";
			}

			string sNewPath = "";
			try {
				sNewPath = pImgRoot;

				Image oImg = Image.FromFile(sNewPath + pImage.Replace("/", @"\"));
				TaxiPassCommon.Images.ImageHelper oImgHelper = new TaxiPassCommon.Images.ImageHelper();
				Size oSize = new Size(340, 128);

				oImg = oImgHelper.ResizeImage(oImg, oSize);

				sNewPath += Path.GetDirectoryName(pImage) + @"\Resized\";
				if (!Directory.Exists(sNewPath)) {
					Directory.CreateDirectory(sNewPath);
				}
				if (!File.Exists(sNewPath + Path.GetFileName(pImage))) {
					oImg.Save(sNewPath + Path.GetFileName(pImage));
				}

				sNewPath = oSysDefaults.VoucherImageURL.ToString().ToLower().Replace("vouchers", "Signatures")
					+ Path.GetDirectoryName(pImage).Replace(@"\", "/")
					+ @"/Resized/" + Path.GetFileName(pImage);
			} catch (Exception ex) {
				sNewPath = "Error: " + ex.Message;
			}
			return sNewPath;
		}

		[HttpPatch]
		public void SendReceiptViaSMS(SqlConnection pConn, string pCellNumber, string pMessage) {
			Utils.Common.SendSMS(SystemDefaults.GetDefaults(pConn), pCellNumber, pMessage);
		}

		[HttpPatch]
		public static string GetSubjectLine(DriverPayments pDriverPayments, string pAccountName) {
			string subject = "NewTaxiPass Receipt";
			if (pDriverPayments.Platform.Equals(Platforms.StorePass.ToString())) {
				subject = "StorePass Receipt";
			} else if (pDriverPayments.Platform.Equals(Platforms.ChargePass.ToString())) {
				subject = "ChargePass Receipt";
			} else if (IsTaxiPassReceipt(pDriverPayments)) {
				subject = "TaxiPass Receipt";
			} else {
				pAccountName = GetAccountName(pDriverPayments, pAccountName);
				if (!pAccountName.IsNullOrEmpty()) {
					subject = pAccountName + " Receipt";
				}
			}
			return subject;
		}


		[HttpPatch]
		public string CreateTaxiChargeReceipt(DriverPayments pPayments) {
			//string sPath = pRequest.MapPath(".");
			string sPath = HostingEnvironment.MapPath(string.Format("~/HTMLTemplates/{0}TaxiChargeReceipt.html", pPayments.Fare < 0 ? "Refund" : ""));
			string sReceipt = System.IO.File.ReadAllText(sPath);

			Affiliate affiliate = pPayments.Affiliate;
			string sLogo = affiliate.AffiliateLogo;

			sReceipt = sReceipt.Replace("{AffiliateLogo}", string.Format("<img src='{0}' alt='{1}' height='75'>", sLogo, affiliate.Name));
			//sReceipt = sReceipt.Replace("{CardHolder}", pPayments.CardHolder);

			string companyInfo = affiliate.Name;
			if (!affiliate.Line1.IsNullOrEmpty()) {
				companyInfo = string.Format("{0}<br>{1}", companyInfo, affiliate.Line1);
			}
			if (!affiliate.Line2.IsNullOrEmpty()) {
				companyInfo = string.Format("{0}, {1}", companyInfo, affiliate.Line2);
			}
			if (!affiliate.City.IsNullOrEmpty()) {
				companyInfo = string.Format("{0}<br>{1}, {2} {3}", companyInfo, affiliate.City, affiliate.State, affiliate.ZIPCode);
			} else if (!affiliate.State.IsNullOrEmpty()) {
				companyInfo = string.Format("{0}<br>{1} {2}", companyInfo, affiliate.State, affiliate.ZIPCode);
			}
			if (!affiliate.CorporatePhone.IsNullOrEmpty()) {
				companyInfo = string.Format("{0}<br>{1}", companyInfo, affiliate.CorporatePhone.FormattedPhoneNumber().ToUpper());
			}
			sReceipt = sReceipt.Replace("{CompanyInfo}", companyInfo);

			sReceipt = sReceipt.Replace("{PaymentDate}", pPayments.ChargeDate.Value.ToString("MMM dd, yyyy hh:mm tt"));

			string invNo = "";
			if (!pPayments.ReferenceNo.IsNullOrEmpty()) {
				invNo = string.Format("<br /><b>Invoice #:</b> {0}", pPayments.ReferenceNo);
			}

			sReceipt = sReceipt.Replace("{ReferenceNo}", invNo);
			sReceipt = sReceipt.Replace("{HideInvoice}", pPayments.ReferenceNo.IsNullOrEmpty() ? "hidden" : "visible");

			Reservations res = Reservations.GetResByDriverPaymentID(pPayments.DriverPaymentID);
			sReceipt = sReceipt.Replace("{HideResDate}", res.IsNullEntity ? "hidden" : "visible");
			string dateOfService = "";
			if (!res.IsNullEntity) {
				dateOfService = string.Format("<br /><b>Date of Service:</b> {0}", res.PickUpDate.ToString("MMM dd, yyyy hh:mm tt"));
			}
			sReceipt = sReceipt.Replace("{ResDate}", dateOfService);

			//sReceipt = sReceipt.Replace("{HideTest}", pPayments.Test ? "visible" : "hidden");
			if (pPayments.Test) {
				string msg = @"<tr>
                                    <td style='width:100%; text-align:center'>
                                        <br />
                                        <font size='5' color='#333'><span style='font-size:22px;font-weight:bold;color:red'>TEST TRANSACTION</span></font>
                                    </td>
                                </tr>";
				sReceipt = sReceipt.Replace("{TestMsg}", msg);
			} else {
				sReceipt = sReceipt.Replace("{TestMsg}", "");
			}

			string cardType = pPayments.CardType;
			if (cardType.IsNullOrEmpty()) {
				cardType = "Card";
			}
			sReceipt = sReceipt.Replace("{PaymentType}", pPayments.CardType);
			sReceipt = sReceipt.Replace("{TransNo}", pPayments.TransNo);

			string billingAddress = ("" + pPayments.CardAddress.DecryptIceKey()).Trim();
			if (!billingAddress.IsNullOrEmpty()) {
				billingAddress += "<br><br>";
			}
			billingAddress += string.Format("Card ending in {0}", pPayments.CardNumberDisplay);

			if (!pPayments.CorporateName.IsNullOrEmpty()) {
				billingAddress = string.Format("{0}<br>{1}", pPayments.CorporateName, billingAddress);
			} else if (!pPayments.CardHolder.IsNullOrEmpty()) {
				string name = pPayments.CardHolder.DecryptIceKey();
				if (name == pPayments.CardHolder) {
					Encryption enc = new Encryption();
					name = enc.Decrypt(name, pPayments.Salt);
				}
				billingAddress = string.Format("{0}<br>{1}", name, billingAddress);
			}

			sReceipt = sReceipt.Replace("{BillingInfo}", billingAddress);

			sReceipt = sReceipt.Replace("{CardNo}", pPayments.CardNumberDisplay);

			sReceipt = sReceipt.Replace("{AffiliateEmail}", affiliate.email);
			string emailVisibility = "visible";
			if (affiliate.email.IsNullOrEmpty()) {
				emailVisibility = "hidden";
			}
			sReceipt = sReceipt.Replace("{HideEmail}", emailVisibility);
			sReceipt = sReceipt.Replace("{AffiliatePhone}", affiliate.CorporatePhone.FormattedPhoneNumber().ToUpper());
			sReceipt = sReceipt.Replace("{Affiliate}", affiliate.Name);

			sReceipt = sReceipt.Replace("{SubTotal}", pPayments.Fare.ToString("C"));
			sReceipt = sReceipt.Replace("{TPFee}", pPayments.TaxiPassFee.ToString("C"));
			sReceipt = sReceipt.Replace("{TotalCharge}", pPayments.TotalCharge.ToString("C"));

			string softDesc = affiliate.DefaultPlatform;
			DriverPaymentRef desc = DriverPaymentRef.GetByGroupAndReference(pPayments.DriverPaymentID, "BillingDescriptor", "BillingDescriptor");
			if (!desc.ValueString.IsNullOrEmpty()) {
				softDesc = desc.ValueString;
			}
			sReceipt = sReceipt.Replace("{PaymentAppear}", softDesc);
			string footerMsg = affiliate.ReceiptFooterText;
			if (!footerMsg.IsNullOrEmpty()) {
				footerMsg = $"{footerMsg}<br /><br />";
			}
			sReceipt = sReceipt.Replace("{footerMessage}", footerMsg);

			return sReceipt;
		}
	}
}