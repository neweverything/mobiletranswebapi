﻿
namespace MobileTransWebAPI.Models {
    public class AffFees {
        public decimal StartAmount { get; set; }
        public decimal EndAmount { get; set; }
        public decimal Fee { get; set; }
    }
}
