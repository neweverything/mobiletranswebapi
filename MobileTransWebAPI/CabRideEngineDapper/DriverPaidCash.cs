﻿using CabRideEngineDapper.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Dapper;

namespace CabRideEngineDapper {
	public partial class DriverPaidCash {
		public static DriverPaidCash GetPaidCash(long pDriverPaymentID) {
			string sql = @"SELECT * FROM dbo.DriverPaidCash
							WHERE DriverPaymentID = @DriverPaymentID";

			using (var conn = SqlHelper.OpenSqlConnection()) {
				return conn.Get<DriverPaidCash>(sql, new { DriverPaymentID = pDriverPaymentID });
			}
		}
	
	}
}
