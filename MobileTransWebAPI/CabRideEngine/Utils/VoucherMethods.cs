﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using IdeaBlade.Persistence;

using TaxiPassCommon;

namespace CabRideEngine.Utils {
	public static class VoucherMethods {

		[Serializable]
		public class VoucherDetails {
			public string VoucherNo { get; set; }
			public long AffiliateID { get; set; }
			public decimal Amount { get; set; }
			public bool Invalid { get; set; }
			public string InvalidMsg { get; set; }
		}


		// ********************************************************************************************
		// Voucher validation logic
		// ********************************************************************************************
		public static VoucherDetails ValidVoucher(PersistenceManager oPM, VoucherDetails Voucher) {

			StringBuilder sMsg = new StringBuilder();

			DriverPayments oPayment = DriverPayments.GetPayment(oPM, Voucher.VoucherNo); // DriverPayments.GetPayment(oPM, voucherNo, amount);
			if (oPayment.IsNullEntity) {
				//check if voucher number is barcode number
				if (Voucher.VoucherNo.Length > 11 && Voucher.VoucherNo.IsNumeric()) {
					oPayment = DriverPayments.GetPayment(oPM, Voucher.VoucherNo.FromBarCode());
				}
				if (oPayment.IsNullEntity) {
					Voucher.Invalid = true;
					sMsg.AppendLine("Voucher Number not found");
				}
			}

			

			if (oPayment.DriverTotal != 0 && oPayment.DriverTotal != Voucher.Amount) {
				Voucher.Invalid = true;
				sMsg.AppendLine("Amount Entered is not correct");
			}

			if (oPayment.DriverTotal == 0) {
				if (oPayment.DriverPaymentKioskAmount.Amount > 0 && oPayment.DriverPaymentKioskAmount.Amount != Voucher.Amount) {
					Voucher.Invalid = true;
					sMsg.AppendLine("Amount Entered is not correct");
				} else {
					if (Voucher.Amount == 0) {
						Voucher.Invalid = true;
						sMsg.AppendLine("Amount Entered must be greater than zero");
					}
				}
			}

			if (oPayment.Test) {
				Voucher.Invalid = true;
				sMsg.AppendLine("This is a test transaction");
			}

			if (oPayment.Failed) {
				Voucher.Invalid = true;
				sMsg.AppendLine("This is a failed transaction");
			}

			Voucher.AffiliateID = oPayment.AffiliateID.GetValueOrDefault(0);
			Voucher.InvalidMsg = sMsg.ToString();
			return Voucher;
		}


	}
}
