﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

using Csla.Validation;


namespace CabRideEngine {
	[Serializable]
	public sealed class Referrals : ReferralsDataRow {

		#region Constructors -- DO NOT MODIFY
		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new'
		private Referrals() : this(null) {
		}

		// Typed DataSet constructor (needed by framework) - DO NOT REMOVE
		public Referrals(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static Referrals Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      Referrals aReferrals = pManager.CreateEntity<Referrals>();
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aReferrals, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aReferrals.AddToManager();
		//      return aReferrals;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		// Add additional logic to your business object here...
		public static Referrals Create(PersistenceManager pPM) {
			Referrals oReferral = null;

			try {
				oReferral = (Referrals)pPM.CreateEntity(typeof(Referrals));

				// Using the IdeaBlade Id Generation technique
				//pPM.GenerateId(oReferral, Referrals.ReferralIDEntityColumn);

				oReferral.AddToManager();

			} catch (Exception ex) {
				throw ex;
			}
			return oReferral;
		}

		protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
			if (IsDeserializing)
				return;
			base.OnColumnChanging(pArgs);
			//DoAudit(pArgs);
		}

		private void DoAudit(DataColumnChangeEventArgs pArgs) {
			if (!this.PersistenceManager.IsClient) {
				return;
			}
			if (this.RowState == DataRowState.Added) {
				return;
			}
			if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
				AuditRecord = ReferralsAudit.Create(this.PersistenceManager, this);
			}
			//AuditRecord[pArgs.Column.ColumnName] = pArgs.ProposedValue;
		}

		protected override void AddRules(RuleList pList) {
			PropertyRequiredRule.AddToList(pList, ReferralEntityColumn.ColumnName);
			pList.Add(IsReferralUnique, ReferralEntityColumn.ColumnName);
		}

		private bool IsReferralUnique(object pTarget, RuleArgs e) {
			Referrals oReferral = (Referrals)pTarget;

			if (oReferral.Referral == null) {
				e.Description = "Referral cannot be blank!";
				return false;
			}

			RdbQuery oQry = new RdbQuery(typeof(Referrals), Referrals.ReferralEntityColumn, EntityQueryOp.EQ, oReferral.Referral);
			oQry.AddClause(Referrals.ReferralIDEntityColumn, EntityQueryOp.NE, oReferral.ReferralID);
			EntityList<Referrals> oReferrals = this.PersistenceManager.GetEntities<Referrals>(oQry);
			if (oReferrals.Count > 0) {
				e.Description = "Referral must be unique!";
				return false;
			}
			return true;
		}

		[BindingBrowsable(false)]
		public override string EntityInstanceDisplayName {
			get {
				return Referral;
			}
		}

	}

}
