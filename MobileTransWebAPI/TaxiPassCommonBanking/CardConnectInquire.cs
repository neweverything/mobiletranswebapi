﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TaxiPassCommon.Banking {
    public class CardConnectInquire {
        public DateTime SettlementDate { get; set; }
        public string respproc { get; set; }
        public string hostbatch { get; set; }
        public string refundtotal { get; set; }
        public string batchid { get; set; }
        public string chargetotal { get; set; }
        public string hoststat { get; set; }
        public string merchid { get; set; }
        public List<Txn> txns { get; set; }

        public decimal NetAmount {
            get {
                return chargetotal.AsDecimal() - refundtotal.AsDecimal();
            }
        }

        public int CreditsCount {
            get {
                return txns.Count(p => p.TransactionType == TrxTypes.Credit || p.setlamount.AsDecimal() < 0);
            }
        }

        public int SalesCount {
            get {
                return txns.Count(p => p.TransactionType == TrxTypes.Sale && p.setlamount.AsDecimal() > 0);
            }
        }


        public class Txn {
            public string setlamount { get; set; }
            public string setlstat { get; set; }
            public string salesdoc { get; set; }
            public string retref { get; set; }
            public CardConnectResponse Response { get; set; }


            public string TransNo {
                get {
                    string[] values = salesdoc.Split('|');
                    return values[0].Trim();
                }
            }

            public string TransactionType {
                get {
                    //string[] values = salesdoc.Split('|');
                    //if (values.Length > 2) {
                    //    return values[2].Trim();
                    //}
                    return setlamount.AsDecimal() > 0 ? TrxTypes.Sale : TrxTypes.Credit;
                }
            }

            public DateTime? TransDate {
                get {
                    string[] values = salesdoc.Split('|');
                    if (values.Length > 3) {
                        return values[4].Trim().AsLong().FromEpochTime("");
                    }
                    return null;
                }
            }
        }

    }
}
