﻿using CabRideEngine.Enums;
using CabRideEngine.Utils;
using CabRideEngineDapper.StoredProc;
using Dapper;
using IdeaBlade.Persistence;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Rdb;
using IdeaBlade.Util;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using TaxiPassCommon;
using TaxiPassCommon.Banking;
using TaxiPassCommon.Entities;



namespace CabRideEngine {
	/// <summary>
	/// The developer class representing the DriverPayments business object class.
	/// </summary>
	/// <remarks>
	/// <para>Place all application-specific business logic in this class.</para>
	/// <para>
	/// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
	/// and will not overwrite changes.
	/// </para>
	/// <para>
	/// This class inherits from the generated class, its companion in the pair of classes 
	/// defining a business object. Properties in the generated class may be overridden in 
	/// order to change the behavior or add business logic.
	/// </para>
	/// <para>
	/// This class is the final class in the inheritance chain for this business object. 
	/// It should not be inherited.
	/// </para>
	/// </remarks>
	[Serializable]
	[JsonObject(IsReference = true)]
	public sealed partial class DriverPayments : DriverPaymentsDataRow {

		#region Constructors -- DO NOT MODIFY

		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new';
		// it is also required if the class is visible from a web service.
		// DO NOT REMOVE **
		private DriverPayments()
			: this(null) {
		}

		/// <summary>
		/// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
		/// </summary>
		/// <param name="pRowBuilder"></param>
		public DriverPayments(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static DriverPayments Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      DriverPayments aDriverPayments = pManager.CreateEntity<DriverPayments>();
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aDriverPayments, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aDriverPayments.AddToManager();
		//      return aDriverPayments;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		public bool BypassFraudCheck = false;
		public static readonly string CHARGECARDS = "ChargeCards";
		public static readonly string RECURRING_FEES = "Recurring Fee";

		public string OriginalTransNo { get; set; }

		public TimeStats MyTimeStats = new TimeStats();

		#region Standard Entity logic to create/audit/validate the entity

		// Add additional logic to your business object here...
		public static DriverPayments Create(Drivers pDriver) {
			DriverPayments oPayment = null;
			try {
				oPayment = pDriver.PersistenceManager.CreateEntity<DriverPayments>();

				// Using the IdeaBlade Id Generation technique
				pDriver.PersistenceManager.GenerateId(oPayment, DriverPayments.DriverPaymentIDEntityColumn);
				oPayment["Salt"] = StringUtils.CreateSalt(6);
				oPayment.DriverID = pDriver.DriverID;
				oPayment.AffiliateID = pDriver.AffiliateID;
				oPayment.RequestAppTime = DateTime.Now.ToUniversalTime();

				oPayment.AddToManager();

				oPayment.TransNo = TaxiPassCommon.StringUtils.GetUniqueKey(6, false);
				oPayment.IgnoreCardZIP = false;
				oPayment.RedemptionFee = 0;
				oPayment.Stops = 0;
				oPayment.WaitTime = 0;
				oPayment.Tolls = 0;
				oPayment.Parking = 0;
				oPayment.DriverFee = 0;
				oPayment["Discount"] = 0;

				oPayment.Failed = true;

				DriverPaymentNotes notes = DriverPaymentNotes.Create(oPayment);
			} catch (Exception ex) {
				throw ex;
			}
			return oPayment;
		}

		public DriverPayments CreateAndRefund(decimal pRefundAmount, string pNote, bool pMarkDoNotPay, Platforms pPlatform) {
			DriverPayments refundRec = DriverPayments.Create(this.Drivers);
			refundRec.FareNoTaxiPassFee = -pRefundAmount;
			//need to calc TPFee 
			decimal nTotal = this.DriverTotal + refundRec.Fare;
			decimal nTPFee = DriverPayments.CalcFee(this.Affiliate, nTotal);
			if (this.TaxiPassFee == 0) {
				refundRec.TaxiPassFee = 0;
			} else if (this.DriverTotal == Math.Abs(refundRec.DriverTotal)) {
				refundRec.TaxiPassFee = -this.TaxiPassFee;
			} else {
				refundRec.TaxiPassFee = -(this.TaxiPassFee - nTPFee);
			}
			refundRec.CardNumber = this.DecryptCardNumber();
			refundRec.Expiration = this.DecryptCardExpiration();
			refundRec.AffiliateDriverID = this.AffiliateDriverID;
			refundRec.AffiliateID = this.AffiliateID;
			refundRec.CardSwiped = false;
			if (refundRec.DriverPaymentNotes.IsNullEntity) {
				DriverPaymentNotes.Create(refundRec);
			}
			pNote += string.Format("\nRefund of Original Voucher No: {0}", this.TransNo);
			refundRec.DriverPaymentNotes.Reason = pNote.Replace("<br />", "\n");
			refundRec.VehicleID = this.VehicleID;
			refundRec.ChargeBack = true;
			refundRec.ChargeBackInfo = refundRec.DriverPaymentNotes.Reason;
			if (refundRec.ChargeBackInfo.Length > 200) {
				refundRec.ChargeBackInfo = refundRec.ChargeBackInfo.Left(200);
			}
			refundRec.CorporateName = this.CorporateName;
			refundRec.ReferenceNo = this.ReferenceNo;

			TaxiPassCommon.TimeZoneConverter tzConvert = new TaxiPassCommon.TimeZoneConverter();
			refundRec.ChargeDate = tzConvert.ConvertTime(DateTime.Now, TimeZone.CurrentTimeZone.StandardName, this.Affiliate.TimeZone);
			refundRec.DriverPaid = this.DriverPaid;
			refundRec.DriverPaidDate = this.DriverPaidDate;
			refundRec.DoNotPay = pMarkDoNotPay;
			refundRec.CreditCardProcessor = this.CreditCardProcessor;
			refundRec.Platform = pPlatform.ToString();

			DriverPaymentResults oResult = null;
			try {
				oResult = refundRec.RefundCard(this.DriverPaymentChargeResults.Reference, this.TransNo);
			} catch (Exception ex) {
				throw ex;
			}
			if (oResult.Result == "0") {
				this.DoNotPay = pMarkDoNotPay;
				this.DriverPaymentNotes.Reason = "See Refund: Trans No: " + refundRec.TransNo + " Date: " + refundRec.MyChargeDateTime.Value.ToString("MM/dd/yyyy") + "\n\n" + this.DriverPaymentNotes.Reason;
				try {
					this.Save();
				} catch {
				}
			}
			return refundRec;
		}


		//protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
		//	if (IsDeserializing) {
		//		return;
		//	}
		//	base.OnColumnChanging(pArgs);
		//	DoAudit(pArgs);
		//}

		//private void DoAudit(DataColumnChangeEventArgs pArgs) {
		//	if (!this.PersistenceManager.IsClient) {
		//		return;
		//	}
		//	if (this.RowState == DataRowState.Added) {
		//		return;
		//	}
		//	if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
		//		AuditRecord = DriverPaymentsAudit.Create(this);
		//	}
		//}

		#endregion

		#region Override Properties

		// ********************************************************************************
		// 
		// ********************************************************************************
		public override SaveResult Save() {
			return Save(LoginManager.PrincipleUser.Identity.Name);
		}


		// ********************************************************************************
		// 
		// ********************************************************************************
		public override SaveResult Save(string sModifiedBy) {

			bool bFullAphabet = false;

			//ensure that the TransNo is unique
			if (this.RowState == DataRowState.Added) {
				if (this.Platform == null) {
					if (this.Affiliate.DefaultPlatform.IsNullOrEmpty()) {
						this.Platform = CHARGECARDS;
					} else {
						this.Platform = this.Affiliate.DefaultPlatform;
					}
				}
				bFullAphabet = this.Platform.Equals(CHARGECARDS, StringComparison.CurrentCultureIgnoreCase) ||
									this.Platform.Equals(Platforms.CCKiosk.ToString(), StringComparison.CurrentCultureIgnoreCase) ||
									this.Platform.Equals(Platforms.NexStep.ToString(), StringComparison.CurrentCultureIgnoreCase);
				//string prefix = GetVoucherPrefix(bFullAphabet);

				AssignUniqueTransNo(bFullAphabet);
			}
			if (this.DriverPaid && string.IsNullOrEmpty(this.DriverPaidTransNo)) {
				this.DriverPaidTransNo = TaxiPassCommon.StringUtils.GetUniqueKey(6);
			}

			// if the card # contains 5 'x' or 5 '*' then it wasn't loaded normally so bypass this step
			if (!base.CardNumber.ToLower().Contains("xxxxx") && !base.CardNumber.Contains("*****")) {
				if (!this.Test && CardUtils.IsTestCard(DecryptCardNumber())) {
					this.Test = true;
				}
			}

			if (this.ChargeBack) {
				if (this.TaxiPassFee > 0) {
					this.TaxiPassFee = -this.TaxiPassFee;
				}
			}
			if (!this.CreditCardProcessor.IsNullOrEmpty() && this.CreditCardProcessor.StartsWith("DriverProm", StringComparison.CurrentCultureIgnoreCase)) {
				this.DriverFee = 0;
			}

			if (!this.DriverPaymentGeoCodes.IsNullEntity && this.DriverPaymentGeoCodes.Address.IsNullOrEmpty() && this.DriverPaymentGeoCodes.Latitude.GetValueOrDefault(0) != 0) {
				FillAddress();
			}

			if (this.CardNumberDisplay.IsNullOrEmpty()) {
				this.CardNumberDisplay = this.DecryptCardNumber().CardNumberDisplay();
			}

			PersistenceManager oPM = this.PersistenceManager;
			EntityList<Entity> saveList = new EntityList<Entity>();
			saveList.ShouldRemoveDeletedEntities = false;

			saveList.Add(this);

			//update modified by and date
			DateTime now = DateTime.Now;
			try {
				TimeZoneConverter tzConvert = new TimeZoneConverter();
				now = tzConvert.ConvertTime(now, TimeZone.CurrentTimeZone.StandardName, this.Affiliate.TimeZone);
			} catch (Exception) {
			}

			saveList.UpdateModifedByInfo(sModifiedBy, now);

			//try to save 5 times incase of deadlocks or timeouts
			int saveCnt = 0;
			while (saveCnt < 3) {
				try {
					MyTimeStats.Info.Add(new TimeStats.ElapsedInfo { Type = "Save DriverPayment try: " + saveCnt.ToString(), Elapsed = DateTime.Now.Subtract(MyTimeStats.Start).TotalSeconds });
					this.PersistenceManager.SaveChanges(saveList);

					break;
				} catch (PersistenceManagerSaveException ex) {
					Thread.Sleep(1000);
					if (!this.AuthDate.HasValue) {
						MyTimeStats.Info.Add(new TimeStats.ElapsedInfo { Type = "Save error - AuthDate", Elapsed = DateTime.Now.Subtract(MyTimeStats.Start).TotalSeconds });

						if (this.DriverPaymentID < 0) {
							//try changing the voucher number
							AssignUniqueTransNo(bFullAphabet);
						}
					}

					if (saveCnt >= 5) {
						SaveError(ex);
						//throw ex;
						break;
					}
					saveCnt++;
				}
			}

			// Orig stats Save Driverpayment...
			if (MyTimeStats != null) {
				if (MyTimeStats.Info == null) {
					MyTimeStats.Info = new List<TimeStats.ElapsedInfo>();
				}
				MyTimeStats.Info.Add(new TimeStats.ElapsedInfo { Type = "Save DriverPayment end", Elapsed = DateTime.Now.Subtract(MyTimeStats.Start).TotalSeconds });
			}
			saveList.Add(this.DriverPaymentNotes);
			saveList.Add(this.DriverPaymentRedeemerNotes);
			saveList.Add(this.ACHDetail);
			saveList.Add(this.DriverPaymentVoucherImages);
			saveList.Add(this.DriverPaidCash);
			saveList.Add(this.DriverPaymentLossGain);
			saveList.Add(this.RedeemerShiftVouchers);
			saveList.Add(this.DriverPaymentGeoCodes);
			saveList.Add(this.DriverPaymentAux);
			saveList.Add(this.ReceiptRequest);

			EntityList<DriverPaymentRef> oDPRefs = new EntityList<DriverPaymentRef>();
			oDPRefs.ShouldRemoveDeletedEntities = false;
			oDPRefs.AddRange(this.PersistenceManager.GetEntities<DriverPaymentRef>(DataRowState.Added | DataRowState.Deleted | DataRowState.Modified));
			saveList.AddRange((from p in oDPRefs
							   where p.DriverPaymentID == this.DriverPaymentID
							   select p).ToArray());


			EntityList<DriverPaymentResults> resultList = new EntityList<DriverPaymentResults>();
			resultList.ShouldRemoveDeletedEntities = false;
			resultList.AddRange(this.PersistenceManager.GetEntities<DriverPaymentResults>(DataRowState.Added | DataRowState.Deleted | DataRowState.Modified));
			saveList.AddRange((from p in resultList
							   where p.DriverPaymentID == this.DriverPaymentID
							   select p).ToArray());
			//add audit tables
			saveList.AddRange(oPM.GetEntities<DriverPaymentsAudit>(DataRowState.Added | DataRowState.Modified).ToArray());
			saveList.AddRange(oPM.GetEntities<DriverPaymentNotesAudit>(DataRowState.Added | DataRowState.Modified).ToArray());
			saveList.AddRange(oPM.GetEntities<DriverPaymentRedeemerNotesAudit>(DataRowState.Added | DataRowState.Modified).ToArray());
			saveList.AddRange(oPM.GetEntities<ACHDetailAudit>(DataRowState.Added | DataRowState.Modified).ToArray());
			saveList.AddRange(oPM.GetEntities<DriverPaymentVoucherImageAudit>(DataRowState.Added | DataRowState.Modified).ToArray());
			saveList.AddRange(oPM.GetEntities<DriverPaidCashAudit>(DataRowState.Added | DataRowState.Modified).ToArray());
			saveList.AddRange(oPM.GetEntities<DriverPaymentLossGainAudit>(DataRowState.Added | DataRowState.Modified).ToArray());
			saveList.AddRange(oPM.GetEntities<RedeemerShiftVouchersAudit>(DataRowState.Added | DataRowState.Modified).ToArray());
			saveList.AddRange(oPM.GetEntities<DriverPaymentResultsAudit>(DataRowState.Added | DataRowState.Modified).ToArray());
			saveList.AddRange(oPM.GetEntities<DriverPaymentAuxAudit>(DataRowState.Added | DataRowState.Modified).ToArray());

			//update modified by and date
			saveList.UpdateModifedByInfo(sModifiedBy, now);

			SaveResult result = null;
			saveCnt = 0;
			while (saveCnt < 3) {
				try {
					MyTimeStats.Info.Add(new TimeStats.ElapsedInfo { Type = "SaveRecs try: " + saveCnt.ToString(), Elapsed = DateTime.Now.Subtract(MyTimeStats.Start).TotalSeconds });
					result = this.PersistenceManager.SaveChanges(saveList);
					break;
				} catch (PersistenceManagerSaveException ex) {
					if (saveCnt >= 2) {
						SaveError(ex);
						//throw ex;
						break;
					}
					saveCnt++;
				}
			}

			if (MyTimeStats != null) {
				if (MyTimeStats.Info == null) {
					MyTimeStats.Info = new List<TimeStats.ElapsedInfo>();
				}
				MyTimeStats.Info.Add(new TimeStats.ElapsedInfo { Type = "Save All Recs done", Elapsed = DateTime.Now.Subtract(MyTimeStats.Start).TotalSeconds });
			}
			//per Jason, send text message to out for 1st time use of IVR system
			SystemDefaults oDef = SystemDefaults.GetDefaults(this.PersistenceManager);
			string textTo = oDef.NewUserTransTextingPhone;
			if (!textTo.IsNullOrEmpty() && this.AffiliateDriverID > 0 && this.DriverPaid && ("" + this.Platform).Equals(Platforms.TaxiPay.ToString(), StringComparison.CurrentCultureIgnoreCase)) {

				Thread oThread = new Thread(new ThreadStart(delegate () {
					try {
						usp_DriverPayments_CountPlatform countRec = usp_DriverPayments_CountPlatform.ExecuteProc(this, false, Platforms.TaxiPay.ToString());
						bool sendText = true;
						if (!countRec.IsNullEntity) {
							sendText = (countRec.PlatformCount.GetValueOrDefault(0) == 0);
						}
						if (sendText) {
							string msg = this.AffiliateDrivers.Name + " from fleet " + this.Affiliate.Name + " did first " + this.DriverTotal.ToString("C") + " IVR charge";
							if (textTo.Contains('@')) {
								//send email
								CabRideEngineDapper.SystemDefaultsDict dict = CabRideEngineDapper.SystemDefaultsDict.GetByKey("EMail", "Receipt", false);
								string[] cred = dict.ValueString.Decrypt().Split('|');
								TaxiPassCommon.Mail.EMailMessage email = new TaxiPassCommon.Mail.EMailMessage(cred[0], msg, "", cred[1]);
								email.UseWebServiceToSendMail = Properties.Settings.Default.EMailViaWebService;
								email.SendMail(textTo);
							} else {
								//send Twilio Text Message
								string acctSid = oDef.TwilioAccountSID;
								string acctToken = oDef.TwilioAccountToken;
								string apiVersion = oDef.TwilioAPIVersion;
								string smsNumber = oDef.TwilioSMSNumber;

								TaxiPassCommon.Mail.TwilioTexting twilio = new TaxiPassCommon.Mail.TwilioTexting(acctSid, acctToken, apiVersion, smsNumber);
								var response = twilio.TwilioTextMessage(msg, textTo);
							}
						}
					} catch (Exception) {
					}
				}));

				// Start the thread
				oThread.Start();
			}
			return result;
		}

		private string GetVoucherPrefix(bool bFullAphabet) {
			string prefix = "";
			if (this.Platform.Equals(Platforms.CCKiosk.ToString(), StringComparison.CurrentCultureIgnoreCase) ||
							this.Platform.Equals(Platforms.NexStep.ToString(), StringComparison.CurrentCultureIgnoreCase)) {
				prefix = "K";
			} else {

				if (this.CardSwiped && (this.Platform.Equals(Platforms.Android.ToString()) || this.Platform.Equals(Platforms.iPhone.ToString()))) {
					prefix = "S";
				} else if (!this.CardSwiped && (this.Platform.Equals(Platforms.Android.ToString()) || this.Platform.Equals(Platforms.iPhone.ToString()))) {
					prefix = "M";
				} else if (this.Platform.Equals(Platforms.WebTerminal.ToString())) {
					prefix = "W";
				} else if (this.Platform.Equals(Platforms.TaxiPay.ToString())) {
					prefix = "P";
				} else if (Platform.Equals(Platforms.GetRide.ToString())) {
					prefix = "Q";
				} else if (Platform.Equals("KioskHuman", StringComparison.CurrentCultureIgnoreCase)) {
					prefix = "H";
					Platform = Platforms.Kiosk.ToString();
				} else if (Platform.Equals(Platforms.Credit.ToString())) {
					prefix = "CR";
				}

				if (Platform.Equals(Platforms.EWRA.ToString()) || ("" + this.Affiliate.DefaultPlatform).Equals(Platforms.EWRA.ToString())) {
					prefix = "E";
				} else if (Platform.Equals(Platforms.EWRA.ToString()) || ("" + this.Affiliate.DefaultPlatform).Equals(Platforms.EWRA.ToString())) {
					prefix = "Y";
				}
				if (ExceptionCharge) {
					prefix = "X";
				}
			}
			return prefix;
		}

		public void AssignUniqueTransNo(bool bFullAphabet) {
			string[] invalidVoucherNoStart = new string[] { "5", "W", "S", "M", "P", "X", "Q", "CR", "E", "Y" };
			string startVoucherNo = GetVoucherPrefix(bFullAphabet);
			int voucherNoSize = 6;

			if (!startVoucherNo.IsNullOrEmpty() && !this.TransNo.StartsWith(startVoucherNo, StringComparison.CurrentCultureIgnoreCase)) {
				this.TransNo = TaxiPassCommon.StringUtils.GetUniqueKey(voucherNoSize, bFullAphabet, startVoucherNo);
			}

			MyTimeStats.Info.Add(new TimeStats.ElapsedInfo { Type = "Start UniqueTransNo", Elapsed = DateTime.Now.Subtract(MyTimeStats.Start).TotalSeconds });
			while (true) {
				MyTimeStats.GetUniqueTransNoCount++;

				if (startVoucherNo.IsNullOrEmpty()) {
					if (invalidVoucherNoStart.FirstOrDefault(p => this.TransNo.StartsWith(p, StringComparison.CurrentCultureIgnoreCase)) != null) {
						this.TransNo = TaxiPassCommon.StringUtils.GetUniqueKey(voucherNoSize, bFullAphabet, startVoucherNo);
						continue;
					}
				}

				if (!OriginalTransNo.IsNullOrEmpty()) {
					TransNo = string.Format("{0}-{1}", OriginalTransNo, TransNo.Left(3));
				}

				RdbQuery qry = new RdbQuery(typeof(DriverPayments), TransNoEntityColumn, EntityQueryOp.EQ, this.TransNo);
				qry.AddClause(DriverPayments.DriverPaymentIDEntityColumn, EntityQueryOp.NE, this.DriverPaymentID);
				EntityList<DriverPayments> oList = this.PersistenceManager.GetEntities<DriverPayments>(qry, QueryStrategy.DataSourceThenCache);
				if (oList.Count < 1) {
					break;
				}
				this.TransNo = TaxiPassCommon.StringUtils.GetUniqueKey(voucherNoSize, bFullAphabet, startVoucherNo);
			}
			MyTimeStats.Info.Add(new TimeStats.ElapsedInfo { Type = "End UniqueTransNo", Elapsed = DateTime.Now.Subtract(MyTimeStats.Start).TotalSeconds });
		}


		// ****************************************************************************************************************
		// Save all the data to disk first 
		// Notify us that there was a problem saving
		// ****************************************************************************************************************
		public void SaveError(PersistenceManagerSaveException ex) {
			//string work;
			var drives = System.IO.DriveInfo.GetDrives();
			var drive = drives.FirstOrDefault(p => p.DriveType != System.IO.DriveType.CDRom && !p.RootDirectory.Name.Equals(@"C:\"));
			string driveName = @"C:\";
			if (drive != null) {
				driveName = drive.RootDirectory.Name;
			}
			string sPath = string.Format(@"{0}DriverPaymentsSaveError\", driveName);

			if (!System.IO.Directory.Exists(sPath)) {
				System.IO.Directory.CreateDirectory(sPath);
			}

			StringBuilder sFileName = new StringBuilder("DriverPayments_");
			sFileName.AppendFormat("{0}_", this.TransNo);
			sFileName.AppendFormat("{0}.txt", DateTime.Now.ToString("yyyyMMdd"));

			StringBuilder sObjects = new StringBuilder();
			string work = ObjectDisplayer.ObjectToString(this);
			string work2 = ObjectDisplayer.ObjectToXMLString(this, "");

			StringBuilder sHtml = new StringBuilder();
			sHtml.AppendLine("<hr>");
			sHtml.AppendLine(String.Format("{0} DriverPayments Save Error ({1})<br>", DateTime.Now.ToString(), this.TransNo));
			sHtml.AppendLine(String.Format("File Created: {0}", sPath + sFileName));
			sHtml.AppendLine(String.Format("Executing App: {0}", System.AppDomain.CurrentDomain.BaseDirectory));

			sHtml.AppendLine("<br><br>** Error Info **<br>");
			sHtml.AppendLine(String.Format("<br>TransNo: {0}", this.TransNo));
			sHtml.AppendLine("Message: " + ex.Message + "<br>");

			if (ex.InnerException != null) {
				sHtml.AppendLine("<br>");
				sHtml.AppendLine("Inner Message: " + ex.InnerException.Message + "<br>");
			}
			sHtml.AppendLine("<br><br>Stack Trace: " + ex.StackTrace + "<br>");

			sHtml.AppendLine("Entities with Errors<br><br>");
			if (ex.SaveResult != null && ex.SaveResult.EntitiesWithErrors != null) {
				foreach (Entity x in ex.SaveResult.EntitiesWithErrors) {
					sHtml.AppendLine(ObjectDisplayer.ObjectToXMLString(x, "").EncryptIceKey());
					sHtml.AppendLine(string.Format("{0} - {1}", x.GetType().Name, JsonConvert.SerializeObject(x.ItemArray).EncryptIceKey()));
					sHtml.AppendLine("<br><br><br>");
				}
			}

			try {
				System.IO.File.WriteAllText(sFileName.ToString(), sHtml.ToString());
			} catch {
			}

			try {
				CabRideEngineDapper.SystemDefaultsDict dict = CabRideEngineDapper.SystemDefaultsDict.GetByKey("EMail", "Receipt", false);
				string[] cred = dict.ValueString.Decrypt().Split('|');
				TaxiPassCommon.Mail.EMailMessage oMail = new TaxiPassCommon.Mail.EMailMessage(cred[0], String.Format("DriverPayments Save Error ({0})", this.TransNo), sHtml.ToString(), cred[1]);
				//oMail.UseWebServiceToSendMail = Properties.Settings.Default.EMailViaWebService;
				oMail.SendMail(Properties.Settings.Default.EMailErrorsTo);
			} catch (Exception) {
			}
		}


		// ********************************************************************************
		// 
		// ********************************************************************************
		public void FillAddress() {

			// do nothing for now, GeoNames is causing a slow down in processing

			////new Thread(new ThreadStart(() => {
			//	if (this.DriverPaymentGeoCodes.Latitude.GetValueOrDefault(0) != 0 && this.DriverPaymentGeoCodes.Longitude.GetValueOrDefault(0) != 0 && this.DriverPaymentGeoCodes.Address.IsNullOrEmpty()) {
			//		try {

			//			try {
			//				com.TaxiPass.GeoCodeWS.GeoCodeService oService = new CabRideEngine.com.TaxiPass.GeoCodeWS.GeoCodeService();
			//				com.TaxiPass.GeoCodeWS.Address oAddr = oService.GetAddressByGeoCode(this.DriverPaymentGeoCodes.Latitude.Value.ToString(), this.DriverPaymentGeoCodes.Longitude.Value.ToString());
			//				this.DriverPaymentGeoCodes.Address = oAddr.Street;
			//				if (!DriverPaymentGeoCodes.Address.IsNullOrEmpty() && oAddr.Distance > 0) {
			//					this.DriverPaymentGeoCodes.Address += " within " + oAddr.Distance + " feet";
			//				}
			//				this.DriverPaymentGeoCodes.Save();
			//			} catch {
			//			}
			//		} catch {
			//		}
			//	}

			//	if (this.DriverPaymentGeoCodes.StartLatitude.GetValueOrDefault(0) != 0 && this.DriverPaymentGeoCodes.StartLongitude.GetValueOrDefault(0) != 0 && this.DriverPaymentGeoCodes.StartAddress.IsNullOrEmpty()) {
			//		try {
			//			if (this.DriverPaymentGeoCodes.StartLatitude != this.DriverPaymentGeoCodes.Latitude && this.DriverPaymentGeoCodes.StartLongitude.GetValueOrDefault(0) != this.DriverPaymentGeoCodes.Longitude) {
			//				if (DriverPaymentGeoCodes.Address.IsNullOrEmpty()) {
			//					try {
			//						com.TaxiPass.GeoCodeWS.GeoCodeService oService = new CabRideEngine.com.TaxiPass.GeoCodeWS.GeoCodeService();
			//						com.TaxiPass.GeoCodeWS.Address oAddr = oService.GetAddressByGeoCode(this.DriverPaymentGeoCodes.StartLatitude.Value.ToString(), this.DriverPaymentGeoCodes.StartLongitude.Value.ToString());
			//						this.DriverPaymentGeoCodes.StartAddress = oAddr.Street;
			//						if (!DriverPaymentGeoCodes.StartAddress.IsNullOrEmpty() && oAddr.Distance > 0) {
			//							this.DriverPaymentGeoCodes.StartAddress += " within " + oAddr.Distance + " feet";
			//						}
			//						this.DriverPaymentGeoCodes.Save();
			//					} catch {
			//					}
			//				} else {
			//					this.DriverPaymentGeoCodes.StartAddress = this.DriverPaymentGeoCodes.Address;
			//				}
			//			}
			//		} catch {
			//		}
			//	}
			////})).Start();
		}


		// ********************************************************************************
		// 
		// ********************************************************************************
		public override string CardNumberDisplay {
			get {
				if (base.CardNumberDisplay.IsNullOrEmpty()) {
					base.CardNumberDisplay = this.DecryptCardNumber().CardNumberDisplay();
				}
				return base.CardNumberDisplay;
			}
			set {
				base.CardNumberDisplay = value;
			}
		}

		// ********************************************************************************
		// Encrypt Card Holder Name
		// ********************************************************************************
		public override string CardHolder {
			get {
				return base.CardHolder.DecryptIceKey();
			}
			set {
				if (value.IsNullOrEmpty()) {
					base.CardHolder = value;
				} else {
					base.CardHolder = value.EncryptIceKey();
				}
			}
		}

		// ********************************************************************************
		// Return N****NNNN for a card number
		// ********************************************************************************
		public override string CardNumber {
			get {
				try {
					if (!string.IsNullOrEmpty(base.CardNumber)) {
						string sCardNo = DecryptCardNumber();
						if (string.IsNullOrEmpty(sCardNo)) {
							return sCardNo;
						}
						sCardNo = sCardNo.Left(1) + "******" + sCardNo.Right(5);
						return sCardNo;
					}
					return "";
				} catch (Exception ex) {
					return ex.Message;
				}
			}
			set {
				value = value.ToUpper().Replace("%B", "").Replace(";", "").Replace(" ", "");
				if (!value.IsNumeric()) {
					try {
						value = value.DecryptIceKey();
					} catch (Exception) {

					}
				}
				Encryption oEncrypt = new Encryption();
				string sValue = "";
				while (true) {
					sValue = oEncrypt.TripleDESEncrypt(value, this.Salt);
					if (oEncrypt.TripleDESDecrypt(sValue, this.Salt) == value) {
						break;
					}
					//encryption/decryption didn't work, try different salt
					this["Salt"] = StringUtils.CreateSalt(6);
				}
				base.CardNumber = sValue;
				this["Number"] = CryptoFns.MD5HashUTF16ToString(value);
				this.CardNumberDisplay = value.CardNumberDisplay();
				if (CardNumber.IsNullOrEmpty()) {
					this.CardType = "";
				} else {
					this.CardType = CabRideEngine.Utils.CreditCardUtils.DetermineCardType(this.PersistenceManager, DecryptCardNumber());
				}
			}
		}

		// ********************************************************************************
		// 
		// ********************************************************************************
		public override string Expiration {
			get {
				if (string.IsNullOrEmpty(base.Expiration)) {
					return base.Expiration;
				}
				Encryption oEncrypt = new Encryption();
				return oEncrypt.TripleDESDecrypt(base.Expiration, this.Salt);
			}

			set {
				Encryption oEncrypt = new Encryption();
				string sValue = oEncrypt.TripleDESEncrypt(value, this.Salt);
				base.Expiration = sValue;
			}
		}

		// ********************************************************************************
		// 
		// ********************************************************************************
		public string ExpirationAsDelimitedString {
			get {
				string sExp = Expiration;
				if (sExp.IsNullOrEmpty()) {
					return "";
				}
				return sExp.Insert(2, "/");
			}
		}


		// ********************************************************************************
		// 
		// ********************************************************************************
		public decimal FareNoTaxiPassFee {
			get {
				if (base.Fare == 0) {
					return this.DriverPaymentKioskAmount.Amount;
				}
				return base.Fare;
			}
			set {
				base.Fare = value;
				TaxiPassFee = 0;
			}
		}

		// ********************************************************************************
		// 
		// ********************************************************************************
		public override decimal Fare {
			get {
				return base.Fare;
			}
			set {
				base.Fare = value;
				TaxiPassFee = CalcFee(this.Drivers.MyAffiliate);
				DriverFee = CalcDriverFee(this.Drivers.MyAffiliate);
			}
		}

		// ********************************************************************************
		// 
		// ********************************************************************************
		public override decimal Gratuity {
			get {
				return base.Gratuity;
			}
			set {
				base.Gratuity = value;
				TaxiPassFee = CalcFee(this.Drivers.MyAffiliate);
				DriverFee = CalcDriverFee(this.Drivers.MyAffiliate);
			}
		}

		// ********************************************************************************
		// 
		// ********************************************************************************
		public override decimal AirportFee {
			get {
				return base.AirportFee;
			}
			set {
				base.AirportFee = value;
				TaxiPassFee = CalcFee(this.Drivers.MyAffiliate);
				DriverFee = CalcDriverFee(this.Drivers.MyAffiliate);
			}
		}

		// ********************************************************************************
		// 
		// ********************************************************************************
		public override decimal MiscFee {
			get {
				return base.MiscFee;
			}
			set {
				base.MiscFee = value;
				TaxiPassFee = CalcFee(this.Drivers.MyAffiliate);
				DriverFee = CalcDriverFee(this.Drivers.MyAffiliate);
			}
		}

		public override decimal? BookingFee {
			get {
				return base.BookingFee;
			}
			set {
				base.BookingFee = value;
				TaxiPassFee = CalcFee(this.Drivers.MyAffiliate);
				DriverFee = CalcDriverFee(this.Drivers.MyAffiliate);
			}
		}

		public override decimal? DropFee {
			get {
				return base.DropFee;
			}
			set {
				base.DropFee = value;
				TaxiPassFee = CalcFee(this.Drivers.MyAffiliate);
				DriverFee = CalcDriverFee(this.Drivers.MyAffiliate);
			}
		}
		// ********************************************************************************
		// 
		// ********************************************************************************
		public override decimal Discount {
			get {
				return base.Discount;
			}
			set {
				base.Discount = Math.Abs(value);
				TaxiPassFee = CalcFee(this.Drivers.MyAffiliate);
				DriverFee = CalcDriverFee(this.Drivers.MyAffiliate);
			}
		}

		// ********************************************************************************
		// 
		// ********************************************************************************
		public override DateTime? ChargeDate {
			get {
				return base.ChargeDate;
			}
			set {
				try {
					if (value != null && value.HasValue) {
						base.ChargeDate = value.Value;
						if (this.Drivers.MyAffiliate != null && !this.Drivers.MyAffiliate.TimeZone.IsNullOrEmpty()) {
							TimeZoneConverter oTimeZone = new TimeZoneConverter();
							base.ChargeDate = oTimeZone.ConvertTime(value.Value, TimeZone.CurrentTimeZone.StandardName, this.Drivers.MyAffiliate.TimeZone);
						}
					} else {
						base.ChargeDate = value;
					}
				} catch (Exception) {
					base.ChargeDate = value;
				}
			}
		}

		// ********************************************************************************
		// 
		// ********************************************************************************
		public override DateTime? ChargeBackDate {
			get {
				return base.ChargeBackDate;
			}
			set {
				base.ChargeBackDate = value;
				if (this.Drivers.MyAffiliate != null && this.Drivers.MyAffiliate.TimeZone != null && value.HasValue) {
					TimeZoneConverter oTimeZone = new TimeZoneConverter();
					base.ChargeBackDate = oTimeZone.ConvertTime(value.Value, TimeZone.CurrentTimeZone.StandardName, this.Drivers.MyAffiliate.TimeZone);
				}
			}
		}

		// ********************************************************************************
		// 
		// ********************************************************************************
		public override bool DriverPaid {
			get {
				return base.DriverPaid;
			}
			set {
				if (value) {
					base.DriverPaid = value;
					if (!this.DriverPaidDate.HasValue) {
						this.DriverPaidDate = DateTime.Now;
					}
				} else {
					if (this.ACHDetail.IsNullEntity) { //.DriverPaidDate.HasValue) {
						base.DriverPaid = value;
						this.DriverPaidDate = null;
						this.DriverPaidTransNo = null;
					}
				}
				if (("" + CardType).Equals("TPCard")) {
					base.DriverPaid = false;
					this.DriverPaidDate = null;
					this.DriverPaidTransNo = null;
				}
				if (!this.ChargeBack && value) {
					DoPerTransactionRecurringFee();
				}
				if (value) {
					if (this.TaxiPassRedeemerID != null || !this.DriverPaymentsPendingMatches.IsNullEntity) {
						long redeemerID = (this.TaxiPassRedeemerID == null ? this.DriverPaymentsPendingMatches.TaxiPassRedeemerID : this.TaxiPassRedeemerID.Value);
						TaxiPassRedeemers redeemer = TaxiPassRedeemers.GetRedeemer(this.PersistenceManager, redeemerID);
						if (redeemer.TaxiPassRedeemerAccounts.TrackShift) {
							try {
								RedeemerShift shift = redeemer.CurrentShift;
								if (!shift.IsNullEntity) {
									RedeemerShiftVouchers shiftVoucher = RedeemerShiftVouchers.GetOrCreate(this);
									shiftVoucher.RedeemerShiftID = shift.RedeemerShiftID;
								}
							} catch (Exception) {
							}
						}
					}
				}

			}
		}

		// ********************************************************************************
		// 
		// ********************************************************************************
		public override DateTime? DriverPaidDate {
			get {
				return base.DriverPaidDate;
			}
			set {
				base.DriverPaidDate = value;
				if (this.Drivers.MyAffiliate != null && this.Drivers.MyAffiliate.TimeZone != null && value.HasValue) {
					TimeZoneConverter oTimeZone = new TimeZoneConverter();
					base.DriverPaidDate = oTimeZone.ConvertTime(value.Value, TimeZone.CurrentTimeZone.StandardName, this.Drivers.MyAffiliate.TimeZone);
				}
			}
		}


		public override DateTime? ScanDate {
			get {
				return base.ScanDate;
			}
			set {
				base.ScanDate = value;
				if ((!this.Affiliate.TimeZone.IsNullOrEmpty() || !this.Affiliate.Markets.TimeZone.IsNullOrEmpty()) && value.HasValue) {
					TimeZoneConverter oTimeZone = new TimeZoneConverter();
					if (!this.Affiliate.TimeZone.IsNullOrEmpty()) {
						base.ScanDate = oTimeZone.ConvertTime(value.Value, TimeZone.CurrentTimeZone.StandardName, this.Affiliate.TimeZone);
					} else {
						base.ScanDate = oTimeZone.ConvertTime(value.Value, TimeZone.CurrentTimeZone.StandardName, this.Affiliate.Markets.TimeZone);
					}
				}
			}
		}


		// ********************************************************************************
		// 
		// ********************************************************************************
		public override decimal TaxiPassFee {
			get {
				return base.TaxiPassFee;
			}
			set {
				base.TaxiPassFee = CancelTaxiPassFee ? 0 : value;
			}
		}


		// ********************************************************************************
		// 3/8/12 CA AuthAmountOverride is primarily for Carmel so they can auth $1.00
		// ********************************************************************************
		public override decimal AuthAmount {
			get {
				return base.AuthAmount;
			}
			set {
				if (!this.Affiliate.AffiliateDriverDefaults.AuthAmountOverride.ToString().IsNullOrEmpty()) {
					if (this.Affiliate.AffiliateDriverDefaults.AuthAmountOverride > 0) {
						if (value < Convert.ToDecimal(this.Affiliate.AffiliateDriverDefaults.AuthAmountOverride)) {
							value = Convert.ToDecimal(this.Affiliate.AffiliateDriverDefaults.AuthAmountOverride);
						}
					}
				} else if (value < 25) {
					value = 25;
				}
				base.AuthAmount = value;
			}
		}

		public override string ReferenceNo {
			get {
				return base.ReferenceNo;
			}
			set {
				base.ReferenceNo = value;
			}
		}

		public string ReferenceNoDisplay {
			get {
				if (base.CreditCardProcessor.IsNullOrEmpty()) {
					return base.ReferenceNo;
				}
				if (this.CreditCardProcessor.Equals("DriverProm", StringComparison.CurrentCultureIgnoreCase)) {
					//"Promo: " + oPromo.DriverPromotionID.ToString() + " | " + this.AffiliateDriverID.Value.ToString("F0");
					if (base.ReferenceNo.StartsWith("Promo: ")) {
						string temp = base.ReferenceNo.Replace("Promo: ", "");
						string[] values = temp.Split('|');
						if (values[0].Trim().IsNumeric()) {
							long promoID = Convert.ToInt64(values[0].Trim());
							DriverPromotions promo = DriverPromotions.GetRecord(this.PersistenceManager, promoID);
							temp = base.ReferenceNo.Replace(": " + promoID, promo.Title);
							return temp;
						}
					}
					return base.ReferenceNo;
				}
				return base.ReferenceNo;
			}
		}

		#endregion

		#region Custom Properties
		/// <summary>
		/// Setting corporate name will force taxipass fee to be $0.00
		/// </summary>
		/// 
		//Is this payment written off

		//----------------------------------------------------------------------------------------
		// Exception Charge, used by Twilio to identify trans that we let driver to process even if
		// they do not have a CVV
		//----------------------------------------------------------------------------------------
		public bool ExceptionCharge { get; set; }


		public string TransNoAsBarCode {
			get {
				return TransNo.ToBarCode();
			}
		}

		[JsonIgnore]
		public bool WriteOff {
			get {
				return this.DriverPaymentAux.WriteOff.GetValueOrDefault(false);
			}
			set {
				if (this.DriverPaymentAux.IsNullEntity) {
					DriverPaymentAux.Create(this);
				}
				this.DriverPaymentAux.WriteOff = value;
			}
		}

		[JsonIgnore]
		public bool DoNotPay {
			get {
				return this.DriverPaymentAux.DoNotPay.GetValueOrDefault(false);
			}
			set {
				if (this.DriverPaymentAux.IsNullEntity) {
					if (this.RowState == DataRowState.Added) {
						this.Save();
					}
					DriverPaymentAux.Create(this);
				}
				this.DriverPaymentAux.DoNotPay = value;
				this["DoNotPay"] = value;
			}
		}

		[JsonIgnore]
		public long? DoNotPayReasonID {
			get {
				return this.DriverPaymentAux.DoNotPayReasonID;
			}
			set {
				if (this.DriverPaymentAux.IsNullEntity) {
					DriverPaymentAux.Create(this);
				}
				this.DriverPaymentAux.DoNotPayReasonID = value;
			}
		}



		[JsonIgnore]
		public bool StoreForward {
			get {
				return this.DriverPaymentAux.StoreForward;
			}
			set {
				if (this.DriverPaymentAux.IsNullEntity) {
					DriverPaymentAux.Create(this);
				}
				this.DriverPaymentAux.StoreForward = value;
			}
		}

		[JsonIgnore]
		private DateTime? mStoreForwardDate;
		public DateTime? StoreForwardDate {
			get {
				return mStoreForwardDate;
			}
			set {
				mStoreForwardDate = value;
			}
		}

		[JsonIgnore]
		public int? Passengers {
			get {
				return this.DriverPaymentAux.Passengers;
			}
			set {
				if (this.DriverPaymentAux.IsNullEntity) {
					DriverPaymentAux.Create(this);
				}
				this.DriverPaymentAux.Passengers = value;
			}
		}

		// ********************************************************************************
		// 
		// ********************************************************************************
		public bool CancelTaxiPassFee {
			get {
				return !string.IsNullOrEmpty(this.CorporateName);
			}
		}

		// ********************************************************************************
		// 
		// ********************************************************************************
		public string DecryptCardNumber() {
			if (base.CardNumber.IsNullOrEmpty()) {
				return "";
			}
			Encryption oEncrypt = new Encryption();
			return oEncrypt.TripleDESDecrypt(base.CardNumber, this.Salt);
		}

		// ********************************************************************************
		// 
		// ********************************************************************************
		public string DecryptCardExpiration() {
			Encryption oEncrypt = new Encryption();
			if (base.Expiration.IsNullOrEmpty()) {
				return "";
			}
			return oEncrypt.TripleDESDecrypt(base.Expiration, this.Salt);
		}

		// ********************************************************************************
		// 2/11/13 replaced dup code with a call to CalcFee(this.Affiliate, nTotal)
		// ********************************************************************************
		private decimal CalcFee(Affiliate pAffiliate) {
			decimal nTotal = Math.Abs(DriverTotal);
			if (this.Affiliate.AffiliateDriverDefaults.TPFeeOnFareOnly) {
				nTotal = Math.Abs(this.Fare);
			}

			return CalcFee(this.Affiliate, nTotal);
		}


		public static decimal CalcFee(Affiliate pAffiliate, decimal pTotal) {
			return CalcFee(pAffiliate.AffiliateFeeses.ToList(), pTotal, pAffiliate.AffiliateDriverDefaults);
		}

		public static decimal CalcFee(List<AffiliateFees> pAffiliateFees, decimal pTotal, AffiliateDriverDefaults pDefaults) {
			decimal feeAmt = 0;
			bool isPct = false;

			var myFees = pAffiliateFees.Where(p => p.BinType.IsNullOrEmpty() || p.BinType.Equals("Credit")).OrderBy(p => p.StartAmount).ToList();

			foreach (AffiliateFees fee in myFees) {
				if (Math.Abs(pTotal) >= fee.StartAmount && fee.EndAmount == 0) {
					feeAmt = fee.Fee;
					isPct = fee.IsPercent;
					break;
				} else if (Math.Abs(pTotal).IsBetween(fee.StartAmount, fee.EndAmount)) {
					feeAmt = fee.Fee;
					isPct = fee.IsPercent;
					break;
				}
			}
			if (pTotal == 0) {
				return 0;
			}
			if (feeAmt == 0) {
				feeAmt = pAffiliateFees.Count == 0 ? 0 : pAffiliateFees.Max(p => p.Fee);
			}
			if (isPct) {
				feeAmt = Math.Round(pTotal * (feeAmt / 100), 2, MidpointRounding.AwayFromZero);
			}

			decimal percFee = pDefaults.AdditionalTPPercentFee.GetValueOrDefault(0);
			percFee = Math.Round(pTotal * (percFee / 100), 2, MidpointRounding.AwayFromZero);
			feeAmt += percFee;

			return feeAmt;
		}

		// ********************************************************************************
		// Do not recalculate the driverfee if there is a kioskamount
		// ********************************************************************************
		private decimal CalcDriverFee(Affiliate pAffiliate) {

			if (!this.CreditCardProcessor.IsNullOrEmpty() && this.CreditCardProcessor.StartsWith("DriverProm", StringComparison.CurrentCultureIgnoreCase)) {
				return 0;
			}

			decimal addFee = 0;
			if (!CardSwiped) {
				addFee += pAffiliate.ManualTransFee;
			}

			if (this.DriverPaymentKioskAmount.IsNullEntity) {
				decimal nTotal = Math.Abs(DriverTotal);
				var myFees = pAffiliate.DriverFees.Where(p => p.BinType.IsNullOrEmpty() || p.BinType.Equals("Credit")).OrderBy(p => p.StartAmount).ToList();
				foreach (DriverFee fee in myFees) {
					if (IsBetween(fee.StartAmount, fee.EndAmount, nTotal)) {
						if (fee.PercentFee) {
							decimal perc = (fee.Fee / 100);
							decimal total = Math.Round(nTotal * perc, 2, MidpointRounding.AwayFromZero);
							return total + addFee;
						} else {
							return fee.Fee + addFee;
						}
					}
				}
			}
			return addFee;
		}


		// ********************************************************************************
		// 
		// ********************************************************************************
		public static decimal CalcDriverFee(Affiliate pAffiliate, decimal pTotal, bool pCardSwiped) {

			decimal addFee = 0;
			if (!pCardSwiped) {
				addFee += pAffiliate.ManualTransFee;
			}

			var myFees = pAffiliate.DriverFees.Where(p => p.BinType.IsNullOrEmpty() || p.BinType.Equals("Credit")).OrderBy(p => p.StartAmount).ToList();
			foreach (DriverFee fee in myFees) {
				if (IsBetween(fee.StartAmount, fee.EndAmount, Math.Abs(pTotal))) {
					if (fee.PercentFee) {
						decimal perc = (fee.Fee / 100);
						decimal total = Math.Round(pTotal * perc, 2, MidpointRounding.AwayFromZero);
						return Math.Abs(total + addFee);
					} else {
						return Math.Abs(fee.Fee + addFee);
					}
				}
			}
			return Math.Abs(addFee);
		}

		// ********************************************************************************
		// 3/26/12 CA Overload of default CalcDriverFee
		// Used to calculate fees on Chargeback vouchers for Carmel
		// ********************************************************************************
		public static decimal CalcDriverFee(Affiliate pAffiliate, decimal pTotal, bool pUseAbsoluteValue, bool pCardSwiped) {
			if (pUseAbsoluteValue) {
				pTotal = Math.Abs(pTotal);
			}
			return CalcDriverFee(pAffiliate, pTotal, pCardSwiped);
		}


		// ********************************************************************************
		// 
		// ********************************************************************************
		private static bool IsBetween(decimal pStart, decimal pEnd, decimal pValue) {
			if (pEnd == 0) {
				if (pValue >= pStart) {
					return true;
				}
			} else {
				if (pValue >= pStart && pValue <= pEnd) {
					return true;
				}
			}
			return false;
		}

		// ********************************************************************************
		// 
		// ********************************************************************************
		private string mCVV = "";
		public string CVV {
			get {
				return mCVV;
			}
			set {
				mCVV = value;
			}
		}

		// ********************************************************************************
		// 
		// ********************************************************************************
		[JsonIgnore]
		public decimal TotalCharge {
			get {
				return Fare
					+ TaxiPassFee
					+ Gratuity
					+ AirportFee
					+ MiscFee
					+ WaitTime.GetValueOrDefault(0)
					+ Tolls.GetValueOrDefault(0)
					+ Parking.GetValueOrDefault(0)
					+ Stops.GetValueOrDefault(0)
					+ DropFee.GetValueOrDefault(0)
					+ BookingFee.GetValueOrDefault(0)
					- RideCouponAmount
					- Discount;
			}
		}


		[JsonIgnore]
		public decimal TotalChargeNoCouponAmt {
			get {
				return Fare
					+ TaxiPassFee
					+ Gratuity
					+ AirportFee
					+ MiscFee
					+ WaitTime.GetValueOrDefault(0)
					+ Tolls.GetValueOrDefault(0)
					+ Parking.GetValueOrDefault(0)
					+ Stops.GetValueOrDefault(0)
					+ DropFee.GetValueOrDefault(0)
					+ BookingFee.GetValueOrDefault(0)
					- Discount;
			}
		}


		// ********************************************************************************
		// 5/28/2013 Add RideCouponAmount back into TotalCharge
		// ********************************************************************************
		[JsonIgnore]
		public decimal DriverTotal {
			get {
				return TotalChargeNoCouponAmt - TaxiPassFee;
			}
		}


		// ********************************************************************************
		// Driver Pays Fee amount
		// ********************************************************************************
		[JsonIgnore]
		public decimal DriverTotalWithDriverFee {
			get {
				return DriverTotal - this.DriverFee.GetValueOrDefault(0);
			}
		}

		// ***************************************************
		// Calc the drivertotal with kiosk amount (if no fare)
		// ***************************************************
		[JsonIgnore]
		public decimal DriverTotalWithKioskAmount {
			get {
				decimal dTrueFare;
				if (Fare != 0) {
					dTrueFare = Fare;
				} else {
					dTrueFare = this.DriverPaymentKioskAmount.Amount;
				}

				return dTrueFare
					+ Gratuity
					+ AirportFee
					+ MiscFee
					+ WaitTime.GetValueOrDefault(0)
					+ Tolls.GetValueOrDefault(0)
					+ Parking.GetValueOrDefault(0)
					+ Stops.GetValueOrDefault(0)
					+ DropFee.GetValueOrDefault(0)
					+ BookingFee.GetValueOrDefault(0)
					- Discount;
			}
		}

		// ********************************************************************************
		// 
		// ********************************************************************************
		[JsonIgnore]
		public decimal DriverTotalPaid {
			get {
				if (this.TaxiPassRedeemers.TaxiPassRedeemerAccounts.IncludeRedeemerRedemptionFeeInACH) {
					return DriverTotal - this.DriverFee.GetValueOrDefault(0);
				}
				return DriverTotal - this.RedemptionFee.GetValueOrDefault(0) - this.DriverFee.GetValueOrDefault(0);
			}
		}

		// ********************************************************************************
		// 
		// ********************************************************************************
		[JsonIgnore]
		public decimal RedeemerTotalPaid {
			get {
				decimal total = DriverTotal - this.DriverFee.GetValueOrDefault(0);
				if (this.TaxiPassRedeemers.TaxiPassRedeemerAccounts.IncludeRedeemerRedemptionFeeInACH) {
					total += RedemptionFee.GetValueOrDefault(0);
				}
				return total;
			}
		}

		// ********************************************************************************
		// 
		// ********************************************************************************
		[JsonIgnore]
		public DateTime? MyACHPaidDateTime {
			get {
				return this.ACHDetail.ACHPaidDate;
			}
		}

		// ********************************************************************************
		// 
		// ********************************************************************************
		[JsonIgnore]
		public string ACHPaidTo {
			get {
				return this.ACHDetail.ACHPaidTo;
			}
		}

		// ********************************************************************************
		// 
		// ********************************************************************************
		[JsonIgnore]
		public string ACHBankAccountNumber {
			get {
				return this.ACHDetail.ACHBankAccountNumber;
			}
		}

		// ********************************************************************************
		// 
		// ********************************************************************************
		public override DateTime? AuthDate {
			get {
				return base.AuthDate;
			}
			set {
				base.AuthDate = value;
				if (this.Drivers.MyAffiliate != null && this.Drivers.MyAffiliate.TimeZone != null && value.HasValue) {
					TimeZoneConverter oTimeZone = new TimeZoneConverter();
					base.AuthDate = oTimeZone.ConvertTime(value.Value, TimeZone.CurrentTimeZone.StandardName, this.Drivers.MyAffiliate.TimeZone);
				}
			}
		}

		// ********************************************************************************
		// 
		// ********************************************************************************
		public DateTime? MyAuthDateTime {
			get {
				return this.AuthDate;
			}
		}

		// ********************************************************************************
		// 
		// ********************************************************************************
		public DateTime? MyChargeDateTime {
			get {
				return this.ChargeDate;
			}
		}


		// ********************************************************************************
		// 
		// ********************************************************************************
		public DateTime? MyChargeBackDateTime {
			get {
				return this.ChargeBackDate;
			}
		}


		public DateTime? MyDriverPaidTime {
			get {
				return this.DriverPaidDate;
			}
		}


		public string DriverPaidByCompany {
			get {
				if (this.TaxiPassRedeemerID != null) {
					return this.TaxiPassRedeemers.TaxiPassRedeemerAccounts.CompanyName;
				}
				if (DriverPaid) {
					return "Direct ACH Payment";
				}
				return "";
			}
		}

		public string DriverPaidByRedeemer {
			get {
				if (this.TaxiPassRedeemerID != null && this.DriverPaymentsPendingMatches.IsNullEntity) {
					if (this.TaxiPassRedeemers.EmployeeNo.IsNullOrEmpty()) {
						return this.TaxiPassRedeemers.Name;
					} else {
						return this.TaxiPassRedeemers.EmployeeNo;
					}
				}
				if (DriverPaid) {
					if (!this.DriverPaymentsPendingMatches.IsNullEntity) {
						return this.DriverPaymentsPendingMatches.TaxiPassRedeemers.Name;
					}
					return "Direct ACH Payment";
				}
				return "";
			}
		}


		public static bool TagRedeemerTotals { get; set; }

		// ************************************************************
		// 8/30/12 CA Ride Coupon Amount to be deducted from ChargeCard
		// ************************************************************
		private decimal mRideCouponAmount;
		public decimal RideCouponAmount {
			get {
				if (mRideCouponAmount != 0) {
					return mRideCouponAmount;
				} else {
					EntityList<DriverPaymentRef> oRefs = DriverPaymentRef.GetByReference(this, "Coupon Amount");
					if (oRefs.Count > 0) {
						return decimal.Parse(oRefs[0].ValueString);
					} else {
						return 0;
					}
				}
			}
			set {
				mRideCouponAmount = value;
			}
		}

		private bool mTag = false;
		public bool Tag {
			get {
				return mTag;
			}
			set {
				bool bCalc = (mTag != value) && !Test;
				mTag = value;
				if (bCalc) {
					if (mTag) {
						mACHTagCount++;
						if (TagRedeemerTotals) {
							mACHTagTotal += this.RedeemerTotalPaid;
						} else {
							mACHTagTotal += this.DriverTotalPaid;
						}
					} else {
						mACHTagCount--;
						if (TagRedeemerTotals) {
							mACHTagTotal -= this.RedeemerTotalPaid;
						} else {
							mACHTagTotal -= this.DriverTotalPaid;
						}
					}
				}
			}
		}

		private string mACHProcessError;
		public string ACHProcessError {
			get {
				return mACHProcessError;
			}
			set {
				mACHProcessError = value;
			}
		}

		public string SortByNameChargeDate {
			get {
				return this.MyDriverName + " " + this.ChargeDate.GetValueOrDefault(new DateTime(1900, 01, 01)).ToString("yyyyMMdd HH:mm");
			}
		}

		public string SortByAffiliateDriverNameChargeDate {
			get {
				return this.AffiliateDrivers.Name + " " + this.ChargeDate.GetValueOrDefault(new DateTime(1900, 01, 01)).ToString("yyyyMMdd HH:mm");
			}
		}

		public string SortByDriverNameChargeDate {
			get {
				return this.Drivers.Name + " " + this.ChargeDate.GetValueOrDefault(new DateTime(1900, 01, 01)).ToString("yyyyMMdd HH:mm");
			}
		}

		public string SortByCellPhone {
			get {
				return this.Drivers.BoostPhone + " " + this.ChargeDate.GetValueOrDefault(new DateTime(1900, 01, 01)).ToString("yyyyMMdd HH:mm");
			}
		}

		public string SortByCellName {
			get {
				return this.Drivers.Name + " " + this.ChargeDate.GetValueOrDefault(new DateTime(1900, 01, 01)).ToString("yyyyMMdd HH:mm");
			}
		}

		public string SortByVehicle {
			get {
				return this.Vehicles.VehicleNo + " " + this.ChargeDate.GetValueOrDefault(new DateTime(1900, 01, 01)).ToString("yyyyMMdd HH:mm");
			}
		}

		public string SortByRedeemerLocation {
			get {
				return this.TaxiPassRedeemerLocations.Location + " " + this.TaxiPassRedeemers.Name + " " + this.MyDriverName;
			}
		}

		public string MyAffiliateName {
			get {
				if (!this.AffiliateDriverID.HasValue || this.AffiliateDriverID.Value < 1) {
					return this.Drivers.MyAffiliate.Name;
				}
				this.PersistenceManager.DefaultQueryStrategy = QueryStrategy.Normal;
				AffiliateDrivers affDriver = AffiliateDrivers.GetDriver(this.PersistenceManager, this.AffiliateDriverID.Value);
				return affDriver.Affiliate.Name;
			}
		}

		public string MyDriverName {
			get {
				if (!this.AffiliateDriverID.HasValue || this.AffiliateDriverID.Value < 1) {
					return this.Drivers.Name;
				}
				this.PersistenceManager.DefaultQueryStrategy = QueryStrategy.Normal;
				AffiliateDrivers affDriver = AffiliateDrivers.GetDriver(this.PersistenceManager, this.AffiliateDriverID.Value);
				return (affDriver.Name + " " + affDriver.LastName).Trim();
			}
		}

		public string MyDriverPhone {
			get {
				if (!this.AffiliateDriverID.HasValue || this.AffiliateDriverID.Value < 1) {
					return this.Drivers.Cell;
				}
				this.PersistenceManager.DefaultQueryStrategy = QueryStrategy.Normal;
				if (this.AffiliateDrivers.Cell.Length < 10) {
					return this.AffiliateDrivers.CallInCell;
				}
				return this.AffiliateDrivers.Cell;
			}
		}

		public string MyCellName {
			get {
				return this.Drivers.Name;
			}
		}

		public string MyVehicle {
			get {
				if (!this.VehicleID.HasValue || this.VehicleID.Value < 1) {
					return "";
				}
				return this.Vehicles.VehicleNo;
			}
		}

		public string MyRedeemer {
			get {
				if (!this.TaxiPassRedeemerID.HasValue || this.TaxiPassRedeemerID.Value < 1) {
					return "";
				}
				return this.TaxiPassRedeemers.Name;
			}
		}

		public string MyRedeemerAccount {
			get {
				if (!this.DriverPaymentsPendingMatches.IsNullEntity) {
					return this.DriverPaymentsPendingMatches.TaxiPassRedeemers.TaxiPassRedeemerAccounts.CompanyName;
				}
				return this.TaxiPassRedeemers.TaxiPassRedeemerAccounts.CompanyName;
			}
		}

		public string MyRedeemerAccountPhone {
			get {
				if (!this.DriverPaymentsPendingMatches.IsNullEntity) {
					return this.DriverPaymentsPendingMatches.TaxiPassRedeemers.TaxiPassRedeemerAccounts.Phone.FormattedPhoneNumber();
				}
				return this.TaxiPassRedeemers.TaxiPassRedeemerAccounts.Phone.FormattedPhoneNumber();
			}
		}

		public string RedemptionPlatform {
			get {
				if (this.TaxiPassRedeemerID.HasValue) {
					if (this.ACHDetail.ACHBankAccountNumber.IsNullOrEmpty()) {
						return "Redeemer";
					}
					if (this.ACHDetail.ACHBankAccountNumber.StartsWith("TransCard:", StringComparison.CurrentCultureIgnoreCase)) {
						return "Pay Card";
					}
					return "Redeemer";
				} else {
					if (this.ACHDetail.ACHPaidTo.IsNullOrEmpty()) {
						return "";
					}
					if (this.ACHDetail.ACHPaidTo.StartsWith("Redeemer:", StringComparison.CurrentCultureIgnoreCase)) {
						return "Redeemer";
					}
					if (this.ACHDetail.ACHPaidTo.StartsWith("Affiliate:", StringComparison.CurrentCultureIgnoreCase)) {
						return "Affiliate";
					}
					if (this.ACHDetail.ACHPaidTo.StartsWith("Cell:", StringComparison.CurrentCultureIgnoreCase)) {
						return "Owner";
					}
					if (this.ACHDetail.ACHPaidTo.StartsWith("Driver:", StringComparison.CurrentCultureIgnoreCase)) {
						return "Driver";
					}
					if (!this.ACHDetail.ACHPaidTo.IsNullOrEmpty()) {
						return "unknown";
					}
				}
				return "";
			}
		}


		public string MyRedeemerLocation {
			get {
				if (!this.TaxiPassRedeemerLocationID.HasValue || this.TaxiPassRedeemerLocationID.Value < 1) {
					return "";
				}
				return this.TaxiPassRedeemerLocations.Location;
			}
		}

		public string MyReferrer {
			get {
				return this.AffiliateDriverReferrals.AffiliateDrivers.Name;
			}
		}

		public string MyEMail {
			get {
				if (!this.AffiliateDriverID.HasValue || this.AffiliateDriverID.Value < 1) {
					return this.Drivers.EMail;
				}
				this.PersistenceManager.DefaultQueryStrategy = QueryStrategy.Normal;
				AffiliateDrivers affDriver = AffiliateDrivers.GetDriver(this.PersistenceManager, this.AffiliateDriverID.Value);
				return affDriver.EMail;
			}
		}

		public string MyPlatform {
			get {
				if (this.Platform.Equals("TaxiPay", StringComparison.CurrentCultureIgnoreCase) && this.Affiliate.IsLimo()) {
					return "LimoPay";
				}
				return this.Platform;
			}
		}


		public string PreAuthApprovalNumber {
			get {
				string sText = "";
				if (this.DriverPaymentAuthResults != null) {
					sText += this.DriverPaymentAuthResults.AuthCode;
				}
				return sText;
			}
		}

		public string ChargeApprovalNumber {
			get {
				string sText = "";
				if (this.DriverPaymentChargeResults != null) {
					sText = this.DriverPaymentChargeResults.AuthCode;
				}
				return sText;
			}
		}

		public string Notes {
			get {
				string note = this.DriverPaymentNotes.Reason;
				if (!string.IsNullOrEmpty(note)) {
					note += "\n\n";
				}
				note += this.ChargeBackInfo;
				return note;
			}
			set {
				if (this.DriverPaymentNotes.IsNullEntity) {
					DriverPaymentNotes.Create(this);
				}
				if (!string.IsNullOrEmpty(this.DriverPaymentNotes.Reason)) {
					this.DriverPaymentNotes.Reason += "\n\n";
				}
				this.DriverPaymentNotes.Reason += value;
			}
		}

		public DriverPaymentResults DriverPaymentAuthResults {
			get {
				return DriverPaymentResults.GetAuthApprovedResult(this);
			}
		}

		public DriverPaymentResults DriverPaymentChargeResults {
			get {
				return DriverPaymentResults.GetSaledApprovedResult(this);
			}
		}





		// **********************************************************************************************
		//
		// **********************************************************************************************
		public string ReceiptDetailsHTML {
			get {
				StringBuilder html = new StringBuilder();
				html.Append("Receipt Details:<br/><br/><Table border=1><tr><td>");

				html.Append("Fleet: ");
				html.Append(this.Affiliate.Name);
				html.Append("<br />");
				if (this.Affiliate.IsLimo()) {
					html.Append("Car # ");
					html.Append(this.MyVehicle);
					html.Append("<br />");
				} else {
					html.Append("Cab # ");
					html.Append(this.MyVehicle);
					html.Append("<br />");
				}
				html.Append("Driver: ");
				html.Append(this.MyDriverName);
				html.Append("<br />");
				html.Append("Date: ");
				html.Append(this.ChargeDate.Value.ToString("MM/dd/yyyy HH:mm"));
				html.Append("<br />");
				html.Append("Voucher #: ");
				html.Append(this.TransNo);
				html.Append("<br />");
				html.Append("CC #: ");
				html.Append(this.CardNumber);
				html.Append("<br />");
				if (!this.CardHolder.IsNullOrEmpty()) {
					html.Append("Card Holder: ");
					html.Append(this.CardHolder);
					html.Append("<br />");
				}
				html.Append("<br />");
				html.Append("Fare: ");
				html.Append(this.Fare.ToString("C"));
				html.Append("<br />");
				if (this.AirportFee > 0) {
					html.Append("Airport: ");
					html.Append(this.AirportFee.ToString("C"));
					html.Append("<br />");
				}
				if (this.MiscFee > 0) {
					html.Append("Other Fee: ");
					html.Append(this.MiscFee.ToString("C"));
					html.Append("<br />");
				}
				if (this.Gratuity > 0) {
					html.Append("Tip: ");
					html.Append(this.Gratuity.ToString("C"));
					html.Append("<br />");
				}
				if (this.Affiliate.IsLimo()) {
					html.Append("LimoPass ");
				} else {
					html.Append("TaxiPass ");
				}
				html.Append("Total: ");
				html.Append(this.DriverTotal.ToString("C"));
				html.Append("<br /><br>");

				// Added per Seth
				if (this.DriverPaidDate.HasValue) {
					html.AppendFormat("Redemption Date: {0}<br>", this.DriverPaidDate.ToString());
				}

				if (!this.MyRedeemer.IsNullOrEmpty()) {
					html.AppendFormat("Redeemer: {0}<br>", this.MyRedeemer);
				}

				if (!ReceiptRequest.ReceiptRequestTypes.IsNullEntity) {
					html.AppendFormat("Notes: {0}<br>", ReceiptRequest.ReceiptRequestTypes.Description);
				}


				html.Append("</td></tr></Table>");

				html.AppendFormat("<br><br><a href=\"mailto:{0}@taxipassreceipt.com\">{1}</a>", TransNo, CabRideEngineDapper.SystemDefaultsDict.GetByKey("ReceiptRequest", "EMailLinkText", false).ValueString);
				return html.ToString();
			}
		}

		public string TransNoToPhoneDigits {
			get {
				return StringUtils.TextToPhoneDigits(this.TransNo);
			}
		}

		public string SalesRep {
			get {
				string sName = "";

				if (!this.AffiliateDrivers.IsNullEntity) {
					AffiliateDriverReferrals referral = AffiliateDriverReferrals.GetByReferredDriver(this.PersistenceManager, this.AffiliateDrivers.AffiliateDriverID);
					sName = referral.AffiliateDrivers.Name;
				}
				return sName;
			}
		}

		public string ExtendRideAsYesString {
			get {
				if (!this.ExtendRide.HasValue) {
					return "";
				}
				return this.ExtendRide.GetValueOrDefault(false) ? "Yes" : "No";
			}
		}


		public bool IsPromo {
			get {
				return ("" + this.CreditCardProcessor).Contains("Prom");
			}
		}

		public bool HasVoucherImage {
			get {
				return !this.DriverPaymentVoucherImages.IsNullEntity;
			}
		}

		public string ReceiptRequestDateAsString {
			get {
				if (this.ReceiptRequest.IsNullEntity) {
					return "";
				}
				return this.ReceiptRequest.ReceiptRequest.ToShortDateString();
			}
		}

		public string RefundInfo {
			get {
				if (!this.ReceiptChargedBack.IsNullOrEmpty()) {
					return "ChargeBack Voucher: " + this.ReceiptChargedBack;
				} else {
					return DriverPaymentAux.GetRefundInfo(this.PersistenceManager, this.DriverPaymentID);
				}
			}
		}

		#endregion

		#region Static Properties

		private static decimal mACHTagTotal = 0;
		public decimal ACHTagTotal {
			get {
				return mACHTagTotal;
			}
			set {
				mACHTagTotal = value;
			}
		}

		private static int mACHTagCount = 0;
		public int ACHTagCount {
			get {
				return mACHTagCount;
			}
			set {
				mACHTagCount = value;
			}
		}



		public string SoftDescriptor {
			get {
				string softDesc = this.Affiliate.DefaultPlatform;
				DriverPaymentRef desc = DriverPaymentRef.GetByGroupReference(this.PersistenceManager, DriverPaymentID, "BillingDescriptor", "BillingDescriptor");
				if (!desc.ValueString.IsNullOrEmpty()) {
					softDesc = desc.ValueString;
				}
				return softDesc;
			}
		}
		#endregion

		// ********************************************************************************
		// 
		// ********************************************************************************
		public void CheckSuspendAccount(CardProcessingException ex) {
			try {
				if (ex.ErrorNo != 3) {
					if (!this.Affiliate.AffiliateDriverDefaults.ExcludeFromRedeemerStopPayment.GetValueOrDefault(false)) {

						if (!this.AffiliateDrivers.IsNullEntity) {
							if (!this.AffiliateDrivers.RedeemerStopPayment) {
								this.AffiliateDrivers.RedeemerStopPayment = true;

								TimeZoneConverter timeZone = new TimeZoneConverter();
								DateTime myTime = timeZone.ConvertTime(DateTime.Now, TimeZone.CurrentTimeZone.StandardName, this.Affiliate.TimeZone);
								if (this.AffiliateDrivers.Notes.IsNullOrEmpty()) {
									this.AffiliateDrivers.Notes += "\n";
								}
								this.AffiliateDrivers.Notes += myTime.ToString() + " -> Auto Fraud Collars tripped " + ex.MyMessage;
								this.AffiliateDrivers.Save();
							}
						} else {
							//this.Drivers.FraudSuspected = true;
							//this.Drivers.Save();
						}
					}
				}
			} catch (Exception) {
			}
		}

		#region Custom Methods


		public static EntityList<DriverPayments> CheckForFraud(DriverPayments pDriverPayments) {
			EntityList<DriverPayments> transList = new EntityList<DriverPayments>();
			SystemDefaults def = SystemDefaults.GetDefaults(pDriverPayments.PersistenceManager);
			if (def.FraudAlert > 0) {
				RdbQuery qry = new RdbQuery(typeof(DriverPayments), DriverPayments.AffiliateIDEntityColumn, EntityQueryOp.EQ, pDriverPayments.AffiliateID);
				qry.AddClause(DriverPayments.CardNumberEntityColumn, EntityQueryOp.EQ, pDriverPayments.CardNumber);
				qry.AddClause(DriverPayments.ChargeDateEntityColumn, EntityQueryOp.GE, pDriverPayments.ChargeDate.Value.AddMinutes(-def.FraudAlert));
				qry.AddClause(DriverPayments.VehicleIDEntityColumn, EntityQueryOp.EQ, pDriverPayments.VehicleID);
				transList = pDriverPayments.PersistenceManager.GetEntities<DriverPayments>(qry, QueryStrategy.DataSourceThenCache);
			}
			return transList;
		}


		public DriverPaymentResults PreAuthCard() {
			return PreAuthCard("");
		}


		public decimal AuthServiceFee = 0;

		public DriverPaymentResults PreAuthCard(string pCardInfo) {
			decimal dAuthAmount = 25;

			if (this.AuthAmount < 1) {
				throw new Exception("Auth amount is required");
			}

			// 12/12/2011 Added so Carmel can run a $1.00 address/CVV verification
			if (this.AuthAmount < 25) {
				if (!this.Affiliate.AffiliateDriverDefaults.AuthAmountOverride.ToString().IsNullOrEmpty()) {
					if (this.Affiliate.AffiliateDriverDefaults.AuthAmountOverride > 0) {
						dAuthAmount = Convert.ToDecimal(this.Affiliate.AffiliateDriverDefaults.AuthAmountOverride);
					}
				}
				this.AuthAmount = dAuthAmount;
			}

			// get fee and add to preauth amount
			MyTimeStats.Info.Add(new TimeStats.ElapsedInfo { Type = "Start Calc TP Fee", Elapsed = DateTime.Now.Subtract(MyTimeStats.Start).TotalSeconds });
			decimal tpFee = CalcFee(this.Affiliate, this.AuthAmount);
			MyTimeStats.Info.Add(new TimeStats.ElapsedInfo { Type = "End Calc TP Fee", Elapsed = DateTime.Now.Subtract(MyTimeStats.Start).TotalSeconds });
			this.AuthAmount += tpFee;
			AuthServiceFee += tpFee;

			CardProcessingFailureTypes oProcess = CardProcessingFailureTypes.OKToProcess;
			if (!BypassFraudCheck) {
				MyTimeStats.Info.Add(new TimeStats.ElapsedInfo { Type = "Start Fraud Check", Elapsed = DateTime.Now.Subtract(MyTimeStats.Start).TotalSeconds });
				BypassFraudCheck = this.Affiliate.AffiliateDriverDefaults.ByPassFraudCheck;
				MyTimeStats.Info.Add(new TimeStats.ElapsedInfo { Type = "End Fraud Check", Elapsed = DateTime.Now.Subtract(MyTimeStats.Start).TotalSeconds });
			}

			if (!BypassFraudCheck) {
				MyTimeStats.Info.Add(new TimeStats.ElapsedInfo { Type = "Start CanProcessCard", Elapsed = DateTime.Now.Subtract(MyTimeStats.Start).TotalSeconds });
				oProcess = CanProcessCard(this);
				MyTimeStats.Info.Add(new TimeStats.ElapsedInfo { Type = "End CanProcessCard", Elapsed = DateTime.Now.Subtract(MyTimeStats.Start).TotalSeconds });
			}
			this.Failed = true;
			switch (oProcess) {
				case CardProcessingFailureTypes.MaxDailyAmount:
				case CardProcessingFailureTypes.MaxDriverTransactions:
				case CardProcessingFailureTypes.MaxMonthlyAmount:
				case CardProcessingFailureTypes.MaxTransPerMonth:
				case CardProcessingFailureTypes.MaxTransPerWeek:
				case CardProcessingFailureTypes.MaxWeeklyAmount:
				case CardProcessingFailureTypes.OKToProcess:

					//this.Reason = "Not yet processed";
					this.AuthDate = DateTime.Now;
					this.FraudFailure = false;
					//if (this.DriverPaymentID < 0) {
					//    this.Save();
					//}

					break;

				//case CardProcessingFailureTypes.MaxTransPerMonthPerCard:
				//case CardProcessingFailureTypes.MaxTransPerWeekPerCard:
				//case CardProcessingFailureTypes.MaxCardTransactions:

				default:
					CardProcessingException ex = new CardProcessingException("Cannot Process Credit Card", oProcess, this.Drivers, this.AffiliateDrivers, Number);
					this.Failed = true;

					// this.DriverPaymentNotes.save added as we were not seeing fraud failures saved - 3/2/2010
					string sError = ex.Message;
					if (sError.IsNullOrEmpty()) {
						sError = "Cannot Process Credit Card - untrapped fraud parameter";
					}
					if (Platform == Platforms.EZRide.ToString() && !ex.MyMessage.IsNullOrEmpty()) {
						sError += " - " + ex.MyMessage;
					}
					this.DriverPaymentNotes.Reason = sError;
					this.DriverPaymentNotes.Save();

					this.AuthDate = DateTime.Now;
					if (!this.Affiliate.AffiliateDriverDefaults.ExcludeFromRedeemerStopPayment.GetValueOrDefault(false)) {
						this.FraudFailure = true;
					}
					this.Save();
					CheckSuspendAccount(ex);
					throw ex;
			}

			PersistenceManager oPM = this.PersistenceManager;

			string cardNo = DecryptCardNumber();

			// only allow this one test card for Carmel
			if (CardUtils.IsTestCard(cardNo) && this.AffiliateID == 161 && (!cardNo.Equals("4000200011112222") || this.CVV != "166" || this.DecryptCardExpiration() != "122030")) {
				throw new Exception("Cannot charge Test Card");
			}
			MyTimeStats.Info.Add(new TimeStats.ElapsedInfo { Type = "TestCard", Elapsed = DateTime.Now.Subtract(MyTimeStats.Start).TotalSeconds });

			//Check BlackList
			if (BlackListedCard(this.PersistenceManager, cardNo)) {
				throw new Exception("Card not accepted 2");
			}
			MyTimeStats.Info.Add(new TimeStats.ElapsedInfo { Type = "Black Listed Card", Elapsed = DateTime.Now.Subtract(MyTimeStats.Start).TotalSeconds });

			//Check BlackListBIN
			if (BlackListedBIN(cardNo)) {
				throw new Exception("Card unaccepted. Please try a different card to charge.");
			}
			MyTimeStats.Info.Add(new TimeStats.ElapsedInfo { Type = "Black Listed BIN", Elapsed = DateTime.Now.Subtract(MyTimeStats.Start).TotalSeconds });

			//Is it a TaxiPass Card 
			TaxiPassCards tpCard = TaxiPassCards.GetCardNo(oPM, cardNo);
			MyTimeStats.Info.Add(new TimeStats.ElapsedInfo { Type = "TaxiPass Card", Elapsed = DateTime.Now.Subtract(MyTimeStats.Start).TotalSeconds });
			#region TaxiPassCards
			if (!tpCard.IsNullEntity) {
				if (tpCard.CardNumber.IsNullOrEmpty()) {
					tpCard.CardNumber = CardNumberToHash(cardNo);
					tpCard.Save();
				}
				if (!tpCard.Active) {
					this.Failed = true;
					this.DriverPaymentNotes.Reason = "Card not accepted 3";
					this.AuthDate = DateTime.Now;
					this.Save();
					throw new Exception("Card not accepted 4");
				} else if (tpCard.TestCard.GetValueOrDefault(false)) {
					this.Test = true;
					this.DriverPaymentNotes.Reason = "Test TaxiPassCard used";
					this.AuthDate = DateTime.Now;
					this.Save();
				}


				DriverPaymentResults oResult = DriverPaymentResults.Create(this);
				oResult.Reference = "TP Card Check";
				oResult.TransType = VerisignTrxTypes.Authorization.ToString().Substring(0, 1);
				oResult.TransDate = DateTime.Now;


				if (!this.IgnoreCardZIP.GetValueOrDefault(true)) {
					if (!string.IsNullOrEmpty(this.CardZIPCode) && !string.IsNullOrEmpty(tpCard.ZIPCode)) {
						if (tpCard.ZIPCode != StringUtils.CleanPhoneNumbers(this.CardZIPCode)) {
							this.Failed = true;
							this.DriverPaymentNotes.Reason = "Invalid Billing Zip Code";

							oResult.Result = "-1";
							oResult.Message = "Invalid Billing Zip Code";
							oResult.Avszip = "N";
							oResult.Save();

							if (!this.AuthDate.HasValue) {
								this.AuthDate = DateTime.Now;
							}
							this.Save();
							return oResult;

						}
						oResult.Avszip = "Y";
					}
				}

				//get last transaction
				TaxiPassCardTransactions tpTrans = TaxiPassCardTransactions.GetLastTrans(tpCard);
				if (tpTrans.Balance - this.AuthAmount < -tpCard.MaxCredit) {
					this.Failed = true;
					this.DriverPaymentNotes.Reason = "Card Declined";

					oResult.Result = "-2";
					oResult.Reference = "TP Card";
					oResult.TransType = VerisignTrxTypes.Authorization;
					oResult.TransDate = DateTime.Now;
					oResult.Message = "Insufficient funds";
					oResult.Save();

					if (!this.AuthDate.HasValue) {
						this.AuthDate = DateTime.Now;
					}
					this.Save();
					return oResult;
				}


				oResult.AuthCode = StringUtils.GetUniqueKey(6);
				oResult.Message = "Approved";
				oResult.Result = "0";
				oResult.Save();

				this.DriverPaymentNotes.Reason = "";
				if (!this.AuthDate.HasValue) {
					this.AuthDate = DateTime.Now;
				}
				this.Save();
				return oResult;
			}
			#endregion TaxiPassCards

			//Is it a promo payment card
			EntityList<DriverPromotions> promoList = DriverPromotions.GetPromotionsByCardNo(this.PersistenceManager, this.DecryptCardNumber());
			MyTimeStats.Info.Add(new TimeStats.ElapsedInfo { Type = "Promo Card Check", Elapsed = DateTime.Now.Subtract(MyTimeStats.Start).TotalSeconds });
			if (promoList.Count > 0) {
				return DoDriverPromoPreAuth(promoList, cardNo, pCardInfo);
			}

			DriverPaymentResults oPayResults = null;

			CreditCard cardData = new CreditCard();
			cardData.AffiliateDriverID = this.AffiliateDriverID;
			cardData.Track2 = pCardInfo;
			string sComment = "Driver Swipe Entry";
			if (pCardInfo.IsNullOrEmpty()) {
				cardData.CardNo = cardNo;
				string sDate = DecryptCardExpiration();
				string sMonth = sDate.Left(2);
				string sYear = sDate.Right(2);
				cardData.CardExpiryAsMMYY = sMonth + sYear;
				sComment = "Driver Manual Entry";
			}
			cardData.CVV = mCVV;
			cardData.AvsZip = this.CardZIPCode;
			cardData.AvsStreet = this.CardAddress;

			// Soft Descriptors


			cardData.SoftDescriptor = FillSoftDescriptor();


			//Gateway gway = new Gateway(Utils.CreditCardUtils.LoadGateWays(this.Affiliate.MyVerisignAccount));
			if (this.RowState == DataRowState.Added) {
				this.Save();
			}
			this.VerisignAccountID = Utils.CreditCardUtils.GetDriverPaymentCardGatewayGroupsID(this.PersistenceManager, this.TransNo, 0);
			CreditCardChargeType chargeType = CreditCardChargeType.ECommerce;
			if (Platform != Platforms.GetRide.ToString()) {
				if (CardSwiped) {
					chargeType = CreditCardChargeType.Swipe;
				} else {
					chargeType = CreditCardChargeType.Manual;
				}
			}

			List<ProcessorResponse> resultList = new List<ProcessorResponse>();
			if (this.Test) {
				// simulate approved for test  trans
				ProcessorResponse resp = new ProcessorResponse() {
					Amount = this.TotalCharge,
					AuthCode = StringUtils.GetUniqueKey(6, true),
					CreditCardProcessor = CardProcessors.TPCard,
					Message = "Approved",
					Reference = StringUtils.GenerateRandomHex(10),
					Result = "0",
					TestCard = true,
					TransDate = ChargeDate.GetValueOrDefault(),
					TransType = "A",
					VerisignAccountID = 0
				};

				resultList.Add(resp);
			} else {
				Gateway gway = new Gateway(Utils.CreditCardUtils.LoadGateWays(this.PersistenceManager, this.VerisignAccountID.Value, chargeType, this.CardType));
				resultList = gway.PreAuthCard(new CCRequest(cardData, AuthAmount, sComment, TransNo, GetServiceType(), Platform, DriverID.ToString(), IsGetRideTrans));
			}

			foreach (ProcessorResponse response in resultList) {
				oPayResults = ProcessResponse(response, TrxTypes.Authorization, AuthAmount, null);
			}

			return oPayResults;

		}


		/// <summary>
		/// Gets a value indicating whether this trans should be run as GetRide.
		/// </summary>
		/// <value>
		///   <c>true</c> if [is get ride trans]; otherwise, <c>false</c>.
		/// </value>
		public bool IsGetRideTrans {
			get {
				bool val = false;
				//if (Platform == Platforms.GetRide.ToString() || (this.Drivers.GetRideDriver && Platform != Platforms.TaxiPay.ToString() && Platform != Platforms.NexStep.ToString())) {
				if (Platform == Platforms.GetRide.ToString() || (Platform != Platforms.TaxiPay.ToString() && Platform != Platforms.NexStep.ToString())) {
					SystemDefaultsDict dict = SystemDefaultsDict.GetByKey(PersistenceManager, "SoftDescriptor", "ShowGetRideUrlOnStatement", false);
					val = (dict.ValueString.Equals("true", StringComparison.CurrentCultureIgnoreCase));
				}
				return val;
			}
		}

		/// <summary>
		/// Fills the soft descriptor for use by Litle.
		/// </summary>
		/// <returns></returns>
		private SoftDescriptor FillSoftDescriptor() {
			SoftDescriptor desc = new SoftDescriptor();

			desc.Name = Affiliate.Markets.Market;
			if (!desc.Name.IsNullOrEmpty()) {
				desc.Name = desc.Name.PadRight(16, ' ').Left(16).Trim();
			}
			//if (Affiliate.City.IsNullOrEmpty()) {
			//	if (!Affiliate.Markets.City.IsNullOrEmpty()) {
			//		desc.City = Affiliate.Markets.City.PadRight(16, ' ').Left(16).Trim();
			//	}
			//} else {
			//	if (!Affiliate.City.IsNullOrEmpty()) {
			//		desc.City = Affiliate.City.PadRight(16, ' ').Left(16).Trim();
			//	}
			//}
			//if (Affiliate.State.IsNullOrEmpty()) {
			//	desc.StateProvince = Affiliate.Markets.State;
			//} else {
			//	desc.StateProvince = Affiliate.State;
			//}
			desc.StateProvince = "NEW YORK";  // Per Donna at Vantiv
			desc.AffiliateName = Affiliate.Name.PadRight(16, ' ').Left(16).Trim();
			desc.Internet = "http://contact.taxipass.com";
			//if (Affiliate.IsLimo()) {
			desc.Phone = "8888294729";
			//} else {
			//	desc.Phone = "800222TAXI";
			//}


			string affPlatform = Platform;
			if (Platform.Equals(CabRideEngineDapper.Platforms.WebTerminal.ToString(), StringComparison.CurrentCultureIgnoreCase) && !Affiliate.DefaultPlatform.IsNullOrEmpty()) {
				affPlatform = Affiliate.DefaultPlatform;
			}
			if (affPlatform.Equals(CabRideEngineDapper.Platforms.ChargePass.ToString(), StringComparison.CurrentCultureIgnoreCase)
				|| affPlatform.Equals(CabRideEngineDapper.Platforms.StorePass.ToString(), StringComparison.CurrentCultureIgnoreCase)
				|| affPlatform.Equals(CabRideEngineDapper.Platforms.MedPass.ToString(), StringComparison.CurrentCultureIgnoreCase)
				|| affPlatform.Equals(CabRideEngineDapper.Platforms.Dialie.ToString(), StringComparison.CurrentCultureIgnoreCase)) {
				//Platform = Affiliate.DefaultPlatform;
				desc.CompanyURL = CompanyURL.ChargePass;
				desc.Internet = "http://chargepass.co/";
			} else {
				if (IsGetRideTrans) {
					desc.CompanyURL = CompanyURL.GetRide;
				}
			}

			if (!this.Affiliate.BillingDescriptor.IsNullOrEmpty()) {
				desc.BillingDescriptor = this.Affiliate.BillingDescriptor;
			}
			return desc;
		}


		private DriverPaymentResults DoDriverPromoPreAuth(EntityList<DriverPromotions> pPromoList, string pCardNo, string pCardInfo) {
			bool promoFound = false;
			DriverPromotions myPromo = null;

			foreach (DriverPromotions oPromo in pPromoList) {
				if (oPromo.AllAffiliates || oPromo.DriverPromotionAffiliateses.FirstOrDefault(p => p.AffiliateID == this.AffiliateID) != null) {
					if (DateTime.Today >= oPromo.StartDate && DateTime.Today <= oPromo.EndDate) {
						promoFound = true;
						myPromo = oPromo;
						break;
					}
				}
			}

			if (promoFound) {
				this.TaxiPassFee = 0;
				EntityList<DriverPayments> payList = new EntityList<DriverPayments>();
				if (this.AffiliateDrivers.IsNullEntity) {
					payList = DriverPayments.GetPaymentByCardNo(this.Drivers, pCardNo, this.TransNo);
				} else {
					payList = DriverPayments.GetPaymentByCardNo(this.AffiliateDrivers, pCardNo, this.TransNo);
				}
				if (payList.Count > 0) {
					this.Failed = true;
					this.DriverPaymentNotes.Reason = "Card not accepted 5, promo already used";
					this.AuthDate = DateTime.Now;
					this.Save();
					throw new Exception(this.DriverPaymentNotes.Reason);
				}
				return ProcessResponse(myPromo, VerisignTrxTypes.Authorization);
			}

			this.Failed = true;
			this.DriverPaymentNotes.Reason = "Promo Card not valid";
			this.AuthDate = DateTime.Now;
			this.Save();
			throw new Exception(this.DriverPaymentNotes.Reason);

		}

		private DriverPaymentResults ProcessResponse(DriverPromotions pPromo, string pTrxType) {
			return ProcessResponse(pPromo, pTrxType, 0);
		}


		// *************************************************************************************************************
		// Process the CC response for promos
		// *************************************************************************************************************
		private DriverPaymentResults ProcessResponse(DriverPromotions pPromo, string pTrxType, long pAffiliateDriverID) {
			PersistenceManager oPM = this.PersistenceManager;

			if (this.DriverPaymentNotes.IsNullEntity) {
				DriverPaymentNotes.Create(this);
			}

			try {
				DriverPaymentResults oResult = DriverPaymentResults.Create(this);
				//oResult.Number = pReserve["CardNumber"].ToString();
				oResult.Result = "0";
				oResult.Reference = "Promo";
				oResult.Message = "Promo";
				oResult.AuthCode = "Promo";
				oResult.CVV2Match = "Y";
				oResult.Avszip = "Y";

				if (this.Affiliate.AffiliateDriverDefaults.TaxiPayCVV && !pPromo.CardCVV.IsNullOrEmpty() && this.CVV != pPromo.CardCVV) {
					oResult.Result = "1001";
					oResult.Message = "Invalid CVV";
					oResult.CVV2Match = "N";
				}
				if (!string.IsNullOrEmpty(this.CardZIPCode)) {
					if (this.CardZIPCode != pPromo.BillingZip) {
						oResult.Result = "1002";
						oResult.Message = "Invalid ZipCode";
						oResult.Avszip = "N";
					}
				}
				if (!pPromo.CardExpires.IsNullOrEmpty()) {
					string promoExpMonth = pPromo.CardExpires.Left(2);
					int year = Convert.ToInt32(pPromo.CardExpires.Right(2));
					if (year < 2000) {
						year += 2000;
					}
					string promoDate = promoExpMonth + year.ToString();
					if (this.Expiration != promoDate) {
						oResult.Result = "1005";
						oResult.Message = "Invalid Expiry Date";
					}
				}
				oResult.TransType = pTrxType;
				oResult.TransDate = DateTime.Now;
				oResult.CreditCardProcessor = TaxiPassCommon.Banking.CardProcessors.DriverPromo.ToString();
				oResult.Save();

				this.CreditCardProcessor = TaxiPassCommon.Banking.CardProcessors.DriverPromo.ToString();
				if (oResult.Result != "0") {
					this.DriverPaymentNotes.Reason = oResult.Message;
					this.Failed = true;
				} else {
					this.Failed = false;
					if (pTrxType == VerisignTrxTypes.Authorization) {
						this.AuthDate = DateTime.Now;

					} else {
						if (pTrxType == VerisignTrxTypes.Credit) {
							if (!this.ChargeDate.HasValue) {
								this.ChargeDate = DateTime.Now;
							}
						} else {
							this.ChargeDate = DateTime.Now;
						}

						if (this.Platform != Platforms.Redeemer.ToString()) {
							if (this.Drivers.DriverBankInfos.Count > 0) {
								DriverBankInfo oBank = this.Drivers.DriverBankInfos[0];
								//if (oBank.BankAccountNumber != "" && oBank.BankCode != "" && oBank.MerchantID != null && oBank.MerchantID > 0) {
								if (oBank.BankAccountNumber != "" && oBank.BankCode != "") {
									if (!this.Drivers.PaidByRedeemer && !this.AffiliateDrivers.PaidByRedeemer && this.AffiliateDrivers.TransCardNumber.IsNullOrEmpty()) {
										SetDriverPaid(true);
									}
								}
							}
							if (this.Drivers.MyAffiliate != null && this.Drivers.MyAffiliate.AutoPayFleet) {
								if (!this.Drivers.PaidByRedeemer && !this.AffiliateDrivers.PaidByRedeemer) {
									SetDriverPaid(false);
								}
								this.AffiliateID = this.Drivers.MyAffiliate.AffiliateID;

							}
						}
					}
				}
				this.ReferenceNo = "Promo: " + pPromo.DriverPromotionID.ToString() + (pAffiliateDriverID != 0 ? " | " + pAffiliateDriverID.ToString("F0") : "");
				//set TP fee to 0 to ensure books are correct
				this.TaxiPassFee = 0;
				this.DriverPaymentNotes.Reason = "";
				this.Save();

				return oResult;
			} catch (Exception ex) {
				throw ex;
			}

		}




		private string GetServiceType() {
			string sServiceType = this.Affiliate.ServiceType;
			if (string.IsNullOrEmpty(sServiceType)) {
				sServiceType = "Taxi";
			}
			sServiceType += " Service";
			return sServiceType;
		}


		public DriverPaymentResults ChargeCard(bool pForceCharge) {
			return ChargeCard("", pForceCharge);
		}

		public DriverPaymentResults ChargeCard() {
			return ChargeCard("", false);
		}

		public DriverPaymentResults ChargeCard(string pCardInfo) {
			return ChargeCard(pCardInfo, false);
		}


		// *************************************************************************
		// Charge the Card
		// *************************************************************************
		public bool JobAdjustment = false;
		public DriverPaymentResults ChargeCard(string pCardInfo, bool pForceNewCharge) {
			DateTime elapsedStart = DateTime.Now;

			#region Validations
			// Put in to trap bogus charge card calls
			if (Platform.IsNullOrEmpty()) {
				Platform = "" + this.Affiliate.DefaultPlatform;
			}

			if ((this.Fare == 0 || Fare < this.Affiliate.AffiliateDriverDefaults.StartingFare) && !JobAdjustment) {
				throw new Exception(string.Format("{1} must be greater than {0}", this.Affiliate.AffiliateDriverDefaults.StartingFare.ToString("C"), Platform.Equals(Platforms.Dialie.ToString(), StringComparison.CurrentCultureIgnoreCase) || Platform.Equals(Platforms.ChargePass.ToString(), StringComparison.CurrentCultureIgnoreCase) ? "Amount" : "Fare"));
			}


			// only allow this one test card for Carmel
			string cardNo = this.DecryptCardNumber();
			if (CardUtils.IsTestCard(cardNo) && this.AffiliateID == 161 && (!cardNo.Equals("4000200011112222") || this.CVV != "166" || this.DecryptCardExpiration() != "122030")) {
				throw new Exception("Cannot charge Test Card");
			}

			if (AffiliateDriverID.HasValue) {
				if (!AffiliateDrivers.Active) {
					throw new Exception(string.Format("Affiliate Driver: {0}-{1},  Account Frozen cannot charge card", AffiliateDriverID.Value, AffiliateDrivers.Name));
				}
			} else {
				if (!Drivers.Active) {
					throw new Exception(string.Format("Driver: {0}-{1},  Account Frozen cannot charge card", DriverID, Drivers.Name));
				}
			}


			//failed until card is charged
			this.Failed = true;

			// hg 2011-07/28 Force a save to prevent incorrect charges from happening
			// Always save first to avoid incorrect voucher numbers going to Credit card processor
			if (this.RowState == DataRowState.Added) {
				int tries = 0;
				while (tries < 3) {
					tries++;
					try {
						this.Save("Force Saved");
						break;
					} catch (Exception ex) {
						if (ex.Message.Equals("Query timeout expired")) {
							continue;
						}
						throw new Exception(ex.Message);
					}
				}
				CreateChargeRecords();
				var db = CabRideDrivenDB.Utils.SqlHelper.OpenCabRideDB();
				db.WriteEntities(ChargeList);
			}

			MyTimeStats.Key = this.TransNo;




			//PreAuth card 1st if manual entry and zip code is present to ensure card is valid before charging
			if (!this.AuthDate.HasValue && !this.CardSwiped) {
				if (!string.IsNullOrEmpty(this.CardZIPCode)) {
					if (!this.IgnoreCardZIP.GetValueOrDefault(true)) {
						if (this.AuthAmount == 0 || AuthAmount < DriverTotal) {
							this.AuthAmount = DriverTotal; // TotalCharge;  use Driver total as PreAuth up's it by the TaxiPass Fee
						}
						DriverPaymentResults preAuthResult = PreAuthCard();
						if (!preAuthResult.Result.Equals("0")) {
							return preAuthResult;
						}
					}
				}
				MyTimeStats.Info.Add(new TimeStats.ElapsedInfo { Type = "PreAuthCard", Elapsed = DateTime.Now.Subtract(MyTimeStats.Start).TotalSeconds });
			}

			CardProcessingFailureTypes oProcess = CardProcessingFailureTypes.OKToProcess;

			if (!BypassFraudCheck) {
				BypassFraudCheck = this.Affiliate.AffiliateDriverDefaults.ByPassFraudCheck;
			}


			if (!BypassFraudCheck && !this.Test) {
				oProcess = CanProcessCard(this);
				MyTimeStats.Info.Add(new TimeStats.ElapsedInfo { Type = "Fraud Check", Elapsed = DateTime.Now.Subtract(MyTimeStats.Start).TotalSeconds });
			}

			switch (oProcess) {
				case CardProcessingFailureTypes.MaxDailyAmount:
				case CardProcessingFailureTypes.MaxMonthlyAmount:
				case CardProcessingFailureTypes.MaxTransPerMonth:
				case CardProcessingFailureTypes.MaxTransPerWeek:
				case CardProcessingFailureTypes.MaxDriverTransactions:
				//case CardProcessingFailureTypes.MaxCardTransactions:
				//case CardProcessingFailureTypes.MaxTransPerMonthPerCard:
				//case CardProcessingFailureTypes.MaxTransPerWeekPerCard:
				case CardProcessingFailureTypes.MaxWeeklyAmount:
				case CardProcessingFailureTypes.OKToProcess:

					//this.Reason = "Not yet processed";
					if (!this.ChargeDate.HasValue) {
						this.ChargeDate = DateTime.Now;
					}
					this.FraudFailure = false;

					if (oProcess != CardProcessingFailureTypes.OKToProcess) {
						CardProcessingException ex = new CardProcessingException("Credit Card Fraud Collars Tripped", oProcess, this.Drivers, this.AffiliateDrivers, Number);
						CheckSuspendAccount(ex);
						SendOutFraudMessage(false, ex.MyMessage);
					}
					break;

				default: {
						CardProcessingException ex = new CardProcessingException("Cannot Process Credit Card", oProcess, this.Drivers, this.AffiliateDrivers, Number);
						this.DriverPaymentNotes.Reason = ex.MyMessage;
						this.ChargeDate = DateTime.Now;
						if (!this.Affiliate.AffiliateDriverDefaults.ExcludeFromRedeemerStopPayment.GetValueOrDefault(false)) {
							this.FraudFailure = true;
						}

						// make sure these columns are reset
						this.DriverPaid = false;
						this.DriverPaidDate = null;

						this.Save();
						CheckSuspendAccount(ex);
						SendOutFraudMessage(true);
						throw ex;
					}
			}

			//Check BlackList
			if (BlackListedCard(this.PersistenceManager, cardNo)) {
				throw new Exception("Card not accepted 6");
			}
			MyTimeStats.Info.Add(new TimeStats.ElapsedInfo { Type = "Black Listed Card Check", Elapsed = DateTime.Now.Subtract(MyTimeStats.Start).TotalSeconds });

			//Check BlackListBIN
			if (BlackListedBIN(cardNo)) {
				throw new Exception("Card unaccepted. Please try a different card to charge.");
			}
			MyTimeStats.Info.Add(new TimeStats.ElapsedInfo { Type = "Black Listed BIN", Elapsed = DateTime.Now.Subtract(MyTimeStats.Start).TotalSeconds });
			#endregion Validations

			//Is it a TaxiPass Card
			TaxiPassCards tpCard = TaxiPassCards.GetCardNo(this.PersistenceManager, cardNo);
			#region TaxiPassCards
			if (!tpCard.IsNullEntity) {
				#region TaxiPassCard
				if (!tpCard.Active) {
					this.Failed = true;
					this.DriverPaymentNotes.Reason = "Card not accepted 7";
					this.ChargeDate = DateTime.Now;
					this.Save();
					throw new Exception("Card not accepted 8");
				}


				DriverPaymentResults oResult = DriverPaymentResults.Create(this);
				oResult.Reference = "TP Card";
				oResult.TransType = VerisignTrxTypes.Sale.ToString().Substring(0, 1);
				oResult.TransDate = DateTime.Now;

				if (!this.IgnoreCardZIP.GetValueOrDefault(true)) {
					if (!string.IsNullOrEmpty(this.CardZIPCode) && !string.IsNullOrEmpty(tpCard.ZIPCode)) {
						if (tpCard.ZIPCode != StringUtils.CleanPhoneNumbers(this.CardZIPCode)) {
							this.Failed = true;
							this.DriverPaymentNotes.Reason = "Invalid Billing Zip Code";
							this.Save();

							oResult.Result = "-1";
							oResult.Message = "Invalid Billing Zip Code";
							oResult.Avszip = "N";
							oResult.Save();

							this.Save();
							return oResult;

						}
						oResult.Avszip = "Y";
					}
				}

				//get last transaction
				TaxiPassCardTransactions tpTrans = TaxiPassCardTransactions.GetLastTrans(tpCard);
				if (tpTrans.Balance - this.TotalCharge < -tpCard.MaxCredit) {
					this.Failed = true;
					this.DriverPaymentNotes.Reason = "Card Declined";
					this.Save();

					oResult.Result = "-2";
					oResult.Reference = "TP Card";
					oResult.TransType = VerisignTrxTypes.Authorization;
					oResult.TransDate = DateTime.Now;
					oResult.Message = "Insufficient funds";
					oResult.Save();

					this.Save();
					return oResult;
				}

				this.Save();
				oResult.AuthCode = StringUtils.GetUniqueKey(6);
				oResult.Message = "Approved";
				oResult.Result = "0";
				oResult.Save();

				this.Failed = false;
				this.DriverPaymentNotes.Reason = "";
				this.Save();

				TaxiPassCardTransactions trans = TaxiPassCardTransactions.AddTransaction(tpCard, -this.TotalCharge, this.DriverPaymentID);
				return oResult;
				#endregion TaxiPassCard
			}
			#endregion TaxiPassCards
			MyTimeStats.Info.Add(new TimeStats.ElapsedInfo { Type = "TP Card Check", Elapsed = DateTime.Now.Subtract(MyTimeStats.Start).TotalSeconds });

			DriverPaymentResults oPayResults = null;
			TaxiPassCommon.Banking.CardProcessors oProcessor = TaxiPassCommon.Banking.CardProcessors.Verisign;


			// if store forward check if another transaction took place via other entry system
			SetStoreForward();
			if (StoreForward) {
				if (StoreForwardDate.HasValue) {
					using (var conn = CabRideEngineDapper.Utils.SqlHelper.OpenSqlConnection()) {
						usp_SystemDefaultsDict_StoreForward sfRec = usp_SystemDefaultsDict_StoreForward.Execute(conn);

						string sql = @"SELECT  *
										FROM    dbo.DriverPayments
										WHERE   ChargeDate >= @StartDate
												AND ChargeDate <= @EndDate
												AND DriverPaymentID != @DriverPaymentID
												AND Number = @Number";

						DynamicParameters parms = new DynamicParameters();
						parms.Add("StartDate", StoreForwardDate.Value.AddHours(-sfRec.DupeCheckInHoursAsInt));
						parms.Add("EndDate", StoreForwardDate.Value.AddHours(sfRec.DupeCheckInHoursAsInt));
						parms.Add("Number", Number);
						parms.Add("DriverPaymentID", DriverPaymentID);

						if (!sfRec.DupeCheckCardAcrossAllDriversAsBool) {
							sql = string.Format("{0} AND AffiliateDriverID = @AffiliateDriverID", sql);
							parms.Add("AffiliateDriverID", AffiliateDriverID);
						}

						if (sfRec.MatchFareAmountAsBool) {
							sql = string.Format("{0} AND Fare = @Fare", sql);
							parms.Add("Fare", Fare);
						}
						List<CabRideEngineDapper.DriverPayments> payList = conn.Query<CabRideEngineDapper.DriverPayments>(sql, parms).ToList();
						if (payList.Count > 0) {
							string myNotes = string.Format("Duplicate StoreForward of {0}", payList[0].TransNo);
							Notes = myNotes;
							var resp = CreateDummyProcessorResponse();
							resp.Message = myNotes;
							DriverPaymentResults myResults = ProcessResponse(resp, VerisignTrxTypes.Sale, TotalCharge, null);
							Failed = true;
							Notes = myNotes;
							Save();
							return myResults;
						}
					}
				}
			}

			//Is it a promo payment card
			EntityList<DriverPromotions> promoList = DriverPromotions.GetPromotionsByCardNo(this.PersistenceManager, this.DecryptCardNumber());
			MyTimeStats.Info.Add(new TimeStats.ElapsedInfo { Type = "Promo Card Check", Elapsed = DateTime.Now.Subtract(MyTimeStats.Start).TotalSeconds });
			if (promoList.Count > 0) {
				oProcessor = TaxiPassCommon.Banking.CardProcessors.DriverPromo;
			} else {
				if (this.AuthAmount > 0) {
					// find auth and use this to charge the transaction
					oProcessor = CardProcessors.USAePay;
					if (!this.DriverPaymentAuthResults.CreditCardProcessor.IsNullOrEmpty()) {
						try {
							oProcessor = StringUtils.StringToEnum<TaxiPassCommon.Banking.CardProcessors>(this.DriverPaymentAuthResults.CreditCardProcessor);
						} catch (Exception) {

						}
					}
				} else {
					// 4/23/2013 probably obe
					if (((CardUtils.IsTestCard(cardNo) && !string.IsNullOrEmpty(this.Drivers.MyAffiliate.MyVerisignAccount.EPayTestURL)) ||
							(!CardUtils.IsTestCard(cardNo) && !string.IsNullOrEmpty(this.Drivers.MyAffiliate.MyVerisignAccount.EPayProdURL)))) {
						oProcessor = TaxiPassCommon.Banking.CardProcessors.USAePay;
					}
				}
			}

			if (oProcessor == TaxiPassCommon.Banking.CardProcessors.DriverPromo) {
				oPayResults = DoDriverPromoCharge(promoList, cardNo, pCardInfo);
			} else {

				this.VerisignAccountID = Utils.CreditCardUtils.GetDriverPaymentCardGatewayGroupsID(this.PersistenceManager, this.TransNo, 0);
				if (VerisignAccountID == 0) {
					throw new Exception("No Gateway assigned to Affiliate " + this.MyAffiliateName);
				}

				CreditCardChargeType chargeType = CreditCardChargeType.ECommerce;
				if (Platform != Platforms.GetRide.ToString()) {
					if (CardSwiped) {
						chargeType = CreditCardChargeType.Swipe;
					} else {
						chargeType = CreditCardChargeType.Manual;
					}
				}
				Gateway gway = new Gateway(Utils.CreditCardUtils.LoadGateWays(this.PersistenceManager, this.VerisignAccountID.Value, chargeType, CardType));
				CabRideEngineDapper.StoredProc.usp_SystemDefaultsDialie dialieDefaults = new CabRideEngineDapper.StoredProc.usp_SystemDefaultsDialie();
				if (this.Platform.Equals(Platforms.Dialie.ToString(), StringComparison.CurrentCultureIgnoreCase)) {
					using (var conn = CabRideEngineDapper.Utils.SqlHelper.OpenSqlConnection()) {
						dialieDefaults = CabRideEngineDapper.StoredProc.usp_SystemDefaultsDialie.Execute(conn, "Dialie");
						if (dialieDefaults.RunChargeAsManualTransaction.AsBool()) {
							chargeType = CreditCardChargeType.Manual;
							CardSwiped = false;
							pCardInfo = "";
						}
					}
				}

				if (!this.DriverPaymentAuthResults.IsNullEntity) {
					//card was auth'd previously, is same card being used for the charge?
					DriverPaymentsAudit audit = DriverPaymentsAudit.GetPayment(this);
					if (!audit.IsNullEntity && audit.Number != this.Number) {
						pForceNewCharge = true;
					}
				}
				if (TotalCharge > AuthAmount || AuthDate == null) {
					pForceNewCharge = true;
				}
				string sComment = "Driver Swipe Entry";
				if (pForceNewCharge) {
					CreditCard cardData = new CreditCard();
					cardData.AffiliateDriverID = AffiliateDriverID;
					cardData.DriverID = DriverID;
					if (string.IsNullOrEmpty(pCardInfo)) {
						cardData.CardNo = cardNo;
						string sDate = DecryptCardExpiration();
						string sMonth = sDate.Left(2);
						string sYear = sDate.Right(2);
						cardData.CardExpiryAsMMYY = sMonth + sYear;
						sComment = "Driver Manual Entry";
					} else {
						if (Platform.Equals(Platforms.Dialie.ToString(), StringComparison.CurrentCultureIgnoreCase)) {
							if (dialieDefaults.ConvertToSimpleTrackData.AsBool()) {
								if (pCardInfo.StartsWith("%B", StringComparison.CurrentCultureIgnoreCase)) {
									string track2 = pCardInfo;
									List<string> temp = track2.Split('^').ToList();
									string track1 = string.Format(";{0}={1}", temp[0].Replace("%B", ""), temp.Last());
									if (!track1.EndsWith("?")) {
										track1 += "?";
									}
									cardData.Track2 = track1;
								}
							} else {
								cardData.Track2 = pCardInfo;
							}
						} else {
							cardData.Track2 = pCardInfo;
						}
					}
					cardData.CVV = mCVV;
					cardData.AvsZip = this.CardZIPCode;
					cardData.AvsStreet = this.CardAddress;

					// Soft Descriptors
					cardData.SoftDescriptor = FillSoftDescriptor();

					if (Platform.Equals(Platforms.Dialie.ToString(), StringComparison.CurrentCultureIgnoreCase) && !cardData.Track2.IsNullOrEmpty()) {
						if (dialieDefaults.StoreTrackData.AsBool()) {
							var dpRef = CabRideEngineDapper.DriverPaymentRef.Create();
							dpRef.DriverPaymentID = this.DriverPaymentID;
							dpRef.ReferenceGroup = "Dialie";
							dpRef.ReferenceKey = "TrackData";
							dpRef.ValueString = cardData.Track2;
							if (pCardInfo != cardData.Track2) {
								dpRef.ValueString += " was " + pCardInfo;
							}
							dpRef.Save();
						}
					}


					// ride could be free based on discounts
					if (TotalCharge > 0) {
						List<ProcessorResponse> respList = new List<ProcessorResponse>();
						if (this.Test) {
							// simulate approved for test  trans
							ProcessorResponse resp = new ProcessorResponse() {
								Amount = this.TotalCharge,
								AuthCode = StringUtils.GetUniqueKey(6, true),
								CreditCardProcessor = CardProcessors.TPCard,
								Message = "Approved",
								Reference = StringUtils.GenerateRandomHex(10),
								Result = "0",
								TestCard = true,
								TransDate = ChargeDate.GetValueOrDefault(),
								TransType = "S",
								VerisignAccountID = 0
							};
							respList.Add(resp);
							oPayResults = ProcessResponse(resp, VerisignTrxTypes.Sale, TotalCharge, null);
						} else {
							respList = gway.ChargeCard(new CCRequest(cardData, TotalCharge, sComment, TransNo, GetServiceType(), Platform, SalesClerk(), IsGetRideTrans));
							foreach (ProcessorResponse resp in respList) {
								oPayResults = ProcessResponse(resp, VerisignTrxTypes.Sale, TotalCharge, null);
							}
						}
						//if ((Platform.Equals(Platforms.Dialie.ToString(), StringComparison.CurrentCultureIgnoreCase)
						//		|| Platform.Equals(Platforms.ChargePass.ToString(), StringComparison.CurrentCultureIgnoreCase))
						//		&& chargeType == CreditCardChargeType.Swipe && respList.FirstOrDefault(p => p.Result == "0") == null) {
						if (Platform.Equals(Platforms.Dialie.ToString(), StringComparison.CurrentCultureIgnoreCase)
								&& chargeType == CreditCardChargeType.Swipe && respList.FirstOrDefault(p => p.Result == "0") == null) {
							if (dialieDefaults.SwitchToManual.AsBool()) {
								chargeType = CreditCardChargeType.Manual;
								cardData.CardNo = cardNo;
								string sDate = DecryptCardExpiration();
								string sMonth = sDate.Left(2);
								string sYear = sDate.Right(2);
								cardData.CardExpiryAsMMYY = sMonth + sYear;
								sComment = "Driver Manual Entry";
								pCardInfo = "";
								cardData.Track2 = "";
								CardSwiped = false;
								gway = new Gateway(Utils.CreditCardUtils.LoadGateWays(this.PersistenceManager, this.VerisignAccountID.Value, chargeType, CardType));
								respList = gway.ChargeCard(new CCRequest(cardData, TotalCharge, sComment, TransNo, GetServiceType(), Platform, SalesClerk(), IsGetRideTrans));
								foreach (ProcessorResponse resp in respList) {
									System.Threading.Thread.Sleep(2000);
									oPayResults = ProcessResponse(resp, VerisignTrxTypes.Sale, TotalCharge, null);
								}
							}
						}
					} else {
						oPayResults = ProcessResponse(CreateDummyProcessorResponse(), VerisignTrxTypes.Sale, TotalCharge, null);
					}
				} else {
					//Delayed Sale
					string sRefNo = "";
					string sTranType = VerisignTrxTypes.DelayedCapture;
					DriverPaymentResults chargeAuthResult = null;
					foreach (DriverPaymentResults oResult in this.DriverPaymentResultses) {
						if ((oResult.Result == "0" || oResult.Message.StartsWith("Approved", StringComparison.CurrentCultureIgnoreCase)) && oResult.TransType == VerisignTrxTypes.Authorization) {
							sRefNo = oResult.Reference;
							chargeAuthResult = oResult;
							break;
						}
					}
					if (!CreditCardProcessor.IsNullOrEmpty()) {
						oProcessor = StringUtils.StringToEnum<TaxiPassCommon.Banking.CardProcessors>(this.CreditCardProcessor);
					}

					gway.SetProcessor(oProcessor);
					ProcessorResponse resp = gway.ChargeAuth(new CCRequestRefNo(sRefNo, TotalCharge, Test, SalesClerk(), TransNo, IsGetRideTrans, this.CardNumberDisplay));
					oPayResults = ProcessResponse(resp, sTranType, TotalCharge, chargeAuthResult);
				}
			}

			// hg 4/28/11 force payment if we need to mark as AutoPaid regardless of payment type and platform
			// ca 9/7/11 Only set DriverPaid if the charge was successful (oPayResults.Result.Trim() == "0")
			if (!this.DriverPaid
				&& oPayResults.Result.Trim() == "0"
				&& (this.Affiliate.AutoPayFleet
						|| (this.AffiliateDrivers.AutoPayManual && !this.CardSwiped)
						|| (this.AffiliateDrivers.AutoPaySwipe && this.CardSwiped)
					)
				) {
				if (!this.AffiliateDrivers.DriverInitiatedPayCardPayment
					&& !this.Affiliate.DriverInitiatedPayCardPayment
					&& !SystemDefaults.GetDefaults(this.PersistenceManager).DriverInitiatedPayCardPayment) {

					if (!this.AffiliateDrivers.PaidByRedeemer) {
						SetDriverPaid(false);
						MyTimeStats.Info.Add(new TimeStats.ElapsedInfo { Type = "SetDriverPaid", Elapsed = DateTime.Now.Subtract(MyTimeStats.Start).TotalSeconds });
					}
				}
			}


			CheckFreezeAccount();

			if (this.Affiliate.DoNotPay) {
				DoNotPay = true;
				DriverPaid = false;
				DriverPaidDate = null;
				DriverPaidTransNo = null;
			}

			this.Save();

			if (this.DriverPaid && !this.Failed && !this.Test && !this.DoNotPay) {
				DoPerTransactionRecurringFee();
				MyTimeStats.Info.Add(new TimeStats.ElapsedInfo { Type = "DoPerTransactionRecurringFee", Elapsed = DateTime.Now.Subtract(MyTimeStats.Start).TotalSeconds });
				// hg 03/12/12
				//moved to Redemption process, unless they are auto paid
				if (this.Affiliate.AutoPayFleet || (this.AffiliateDrivers.AutoPayManual && !this.CardSwiped)
													|| (this.AffiliateDrivers.AutoPaySwipe && this.CardSwiped)) {
					DoPromoPayments();
					MyTimeStats.Info.Add(new TimeStats.ElapsedInfo { Type = "PromoPayment", Elapsed = DateTime.Now.Subtract(MyTimeStats.Start).TotalSeconds });
				}
			}

			int elapsed = (int)DateTime.Now.Subtract(elapsedStart).TotalMilliseconds;
			var dpElapsedRef = DriverPaymentRef.GetCreate(this, "ElapsedTime", "ChargeCard Old");
			dpElapsedRef.ValueString = elapsed.ToString();
			dpElapsedRef.Save();


			return oPayResults;
		}



		public List<CabRideDrivenDB.DriverPaymentCharges> ChargeList = new List<CabRideDrivenDB.DriverPaymentCharges>();
		private void CreateChargeRecords() {
			if (CabRideDrivenDB.CabRideDrivenDBSettings.DataSource.IsNullOrEmpty()) {
				CabRideDrivenDB.CabRideDrivenDBSettings.DataSource = this.PersistenceManager.DataSourceExtension;
			}

			ChargeList.Add(CabRideDrivenDB.DriverPaymentCharges.Create(this.DriverPaymentID, "Fare", this.Fare));
			ChargeList.Add(CabRideDrivenDB.DriverPaymentCharges.Create(this.DriverPaymentID, "Gratuity", this.Gratuity));
			ChargeList.Add(CabRideDrivenDB.DriverPaymentCharges.Create(this.DriverPaymentID, "AirportFee", this.AirportFee));
			ChargeList.Add(CabRideDrivenDB.DriverPaymentCharges.Create(this.DriverPaymentID, "MiscFee", this.MiscFee));
			ChargeList.Add(CabRideDrivenDB.DriverPaymentCharges.Create(this.DriverPaymentID, "Tolls", this.Tolls.GetValueOrDefault(0)));
			ChargeList.Add(CabRideDrivenDB.DriverPaymentCharges.Create(this.DriverPaymentID, "Parking", this.Parking.GetValueOrDefault(0)));
			ChargeList.Add(CabRideDrivenDB.DriverPaymentCharges.Create(this.DriverPaymentID, "Stops", this.Stops.GetValueOrDefault(0)));
			ChargeList.Add(CabRideDrivenDB.DriverPaymentCharges.Create(this.DriverPaymentID, "WaitTime", this.WaitTime.GetValueOrDefault(0)));
			ChargeList.Add(CabRideDrivenDB.DriverPaymentCharges.Create(this.DriverPaymentID, "BookingFee", this.BookingFee.GetValueOrDefault(0)));
			ChargeList.Add(CabRideDrivenDB.DriverPaymentCharges.Create(this.DriverPaymentID, "DropFee", this.DropFee.GetValueOrDefault(0)));
			ChargeList.Add(CabRideDrivenDB.DriverPaymentCharges.Create(this.DriverPaymentID, "Discount", -Math.Abs(this.Discount)));
			ChargeList.Add(CabRideDrivenDB.DriverPaymentCharges.Create(this.DriverPaymentID, "TaxiPassFee", this.TaxiPassFee));

			ChargeList = ChargeList.Where(p => p.Amount != 0).ToList();
		}

		List<CabRideDrivenDB.DriverPaymentFees> FeeList = new List<CabRideDrivenDB.DriverPaymentFees>();
		private void CreateFeeRecords() {
			if (CabRideDrivenDB.CabRideDrivenDBSettings.DataSource.IsNullOrEmpty()) {
				CabRideDrivenDB.CabRideDrivenDBSettings.DataSource = this.PersistenceManager.DataSourceExtension;
			}
			FeeList.Add(CabRideDrivenDB.DriverPaymentFees.Create(this.DriverPaymentID, "DriverFee", this.DriverFee.GetValueOrDefault(0)));
			FeeList.Add(CabRideDrivenDB.DriverPaymentFees.Create(this.DriverPaymentID, "RedemptionFee", this.RedemptionFee.GetValueOrDefault(0)));

			FeeList = FeeList.Where(p => p.Amount != 0).ToList();
		}


		// *********************************************
		// Do we Freeze the account
		// *********************************************
		private void CheckFreezeAccount() {
			// Check if we need to freeze the account

			try {
				StringBuilder sql = new StringBuilder("usp_FreezeDriver ");
				sql.AppendFormat("	 @EndDate = '{0}'", DateTime.Now);
				if (AffiliateDriverID.HasValue) {
					sql.AppendFormat(", @AffiliateDriverID = {0}", AffiliateDriverID);
				} else {
					sql.AppendFormat(", @DriverID = {0}", DriverID);
				}
				Entity[] recList = DynamicEntities.DynamicDBCall(PersistenceManager, "FreezeAccount", sql.ToString());
				if (recList.Length > 0 && Convert.ToBoolean(recList[0]["AccountFrozen"])) {
					SystemDefaultsDict dict = SystemDefaultsDict.GetByKey(PersistenceManager, "Freeze Check", "TextMsg", false);
					if (AffiliateDriverID.HasValue) {
						AffiliateDrivers.SendDriverMsg(dict.ValueString, false, true);
					}
				}
			} catch (Exception) {
			}

		}


		private string SalesClerk() {
			return "DP, " + this.DriverID + ", " + this.AffiliateDriverID.GetValueOrDefault(0);
		}

		// *********************************************
		// Check for blacklisted cards
		// *********************************************
		public bool BlackListedCard(PersistenceManager pPM, string pCardNo) {
			bool Blacklisted = false;

			SqlConnection conn = CabRideEngine.Utils.SQLHelper.OpenSqlConnection(pPM);
			dynamic oBlackList = conn.Query(string.Format("usp_BlackListCardsGetByNumber '{0}'", TaxiPassCommon.Cryptography.CryptoFns.MD5HashUTF16ToString(pCardNo))).ToList();

			foreach (var rec in oBlackList) {
				string cardNo = rec.CardNo; // need to do it this way as dynamic string can't process DecryptIceKey
				if (cardNo.DecryptIceKey().Equals(pCardNo)) {
					this.Failed = true;
					this.DriverPaymentNotes.Reason = "Card not accepted 1";
					this.AuthDate = DateTime.Now;
					this.Save();
					Blacklisted = true;
					break;
				}
			}


			return Blacklisted;
		}

		public bool BlackListedBIN(string pCardNo) {
			bool blackListed = false;
			using (var conn = CabRideEngineDapper.Utils.SqlHelper.OpenSqlConnection()) {
				CabRideEngineDapper.BlackListBIN bin = CabRideEngineDapper.BlackListBIN.GetByBIN(conn, pCardNo.Left(6));
				blackListed = !bin.IsNullEntity;
				if (blackListed) {
					this.Failed = true;
					this.DriverPaymentNotes.Reason = "Sorry, We do not accept charges from this Card's Institution";
					this.AuthDate = DateTime.Now;
					this.Save();
				}
			}
			return blackListed;
		}


		/// <summary>
		/// Sends the out fraud message.
		/// </summary>
		/// <param name="pTransBlocked">if set to <c>true</c> [trans blocked].</param>
		public void SendOutFraudMessage(bool pTransBlocked) {
			SendOutFraudMessage(pTransBlocked, "");
		}

		// **********************************************************************************************
		//
		// **********************************************************************************************
		public void SendOutFraudMessage(bool pTransBlocked, string pMsg) {
			StringBuilder body = new StringBuilder();

			PersistenceManager oPM = this.PersistenceManager;
			SystemDefaults oDefaults = SystemDefaults.GetDefaults(oPM);
			if (!oDefaults.FraudAlertEMail.IsNullOrEmpty()) {
				try {
					if (!this.AffiliateDrivers.IsNullEntity) {
						body.Append("Affiliate: ");
						body.Append(this.AffiliateDrivers.Affiliate.Name);
						body.Append("<br />");
						body.Append("Affiliate Driver ID: ");
						body.Append(this.AffiliateDrivers.AffiliateDriverID);
						body.Append("<br />");
						body.Append("Affiliate Driver: ");
						body.Append(this.AffiliateDrivers.Name);
						body.Append("<br />");
						body.Append("Cell: ");
						body.Append(this.AffiliateDrivers.Cell);
						body.Append("<br /><br />");
					}

					body.Append("Driver ID: ");
					body.Append(this.Drivers.DriverID);
					body.Append("<br />");
					body.Append("Driver: ");
					body.Append(this.Drivers.Name);
					body.Append("<br />");
					body.Append("Cell: ");
					body.Append(this.Drivers.Cell.FormattedPhoneNumber());
					body.Append("<br /><br />");
					body.Append("Reason: ");
					body.Append((this.DriverPaymentNotes.Reason.IsNullOrEmpty() ? pMsg : this.DriverPaymentNotes.Reason));
					body.Append("<br /><br />");

					body.Append("Trans No: ");
					body.Append(this.TransNo);
					body.Append("<br />");
					body.Append("Card No: ");
					body.Append(this.CardNumberDisplay);
					body.Append("<br />");

					CabRideEngineDapper.SystemDefaultsDict dict = CabRideEngineDapper.SystemDefaultsDict.GetByKey("EMail", "Receipt", false);
					string[] cred = dict.ValueString.Decrypt().Split('|');
					TaxiPassCommon.Mail.EMailMessage mail = new TaxiPassCommon.Mail.EMailMessage(cred[0], pTransBlocked ? "Fraud Alert" : "Stop Payment", body.ToString(), cred[1]);
					mail.UseWebServiceToSendMail = Properties.Settings.Default.EMailViaWebService;
					SystemDefaultsDict oSysDict = SystemDefaultsDict.GetByKey(oPM, "Stop Payment", "EmailRecipients", false);
					mail.SendMail(oSysDict.ValueString);
				} catch (Exception) {

				}
			}
		}

		/// <summary>
		/// Does the promo payments.
		/// </summary>
		public void DoPromoPayments() {
			PersistenceManager oPM = this.PersistenceManager;

			if (this.CreditCardProcessor.IsNullOrEmpty()
				|| this.CreditCardProcessor.Equals("DriverProm", StringComparison.CurrentCultureIgnoreCase)
				|| this.Test
				|| this.Failed
				|| this.DoNotPay) {
				return;
			}

			if (!this.AffiliateDriverID.HasValue && this.Drivers.Name.Equals(CHARGECARDS, StringComparison.CurrentCultureIgnoreCase)) {
				return;
			}

			EntityList<DriverPromotions> promoList = DriverPromotions.GetPromotionsForAffiliate(this.Affiliate);
			var x = (from p in promoList
					 where p.VoucherCount > 0 && (p.DriverPayout > 0 || p.ReferrerPayout > 0)
					 select p).ToList();
			if (x.Count == 0) {
				return;
			}

			long fleetDriverID = this.AffiliateDriverID.GetValueOrDefault(0);  //.Value;

			promoList = new EntityList<DriverPromotions>(x);

			foreach (DriverPromotions oPromo in promoList) {
				if (oPromo.NewDriversOnly) {
					if (this.AffiliateDrivers.IsNullEntity) {
						if (this.Drivers.SignUpDate < oPromo.StartDate) {
							string appVers = "" + this.Drivers.AppVersion;
							if (!appVers.StartsWith("a", StringComparison.CurrentCultureIgnoreCase) && appVers.StartsWith("i", StringComparison.CurrentCultureIgnoreCase)) {
								continue;
							}
						}
					} else {
						if (this.AffiliateDrivers.SignUpDate < oPromo.StartDate) {
							string appVers = "" + this.Drivers.AppVersion;
							if (!appVers.StartsWith("a", StringComparison.CurrentCultureIgnoreCase) && appVers.StartsWith("i", StringComparison.CurrentCultureIgnoreCase)) {
								continue;
							}
						}
					}
				}
				if (oPromo.Platform != this.Platform) {
					continue;
				}
				if (oPromo.SwipeOnly && !this.CardSwiped) {
					continue;
				}


				EntityList<DriverPayments> driverPaidPromos = DriverPayments.GetPaymentsByReferenceNo(this.AffiliateDrivers, "Promo: " + oPromo.DriverPromotionID.ToString());
				if (driverPaidPromos.Count == 0) {
					driverPaidPromos = DriverPayments.GetPaymentsByReferenceNo(this.AffiliateDrivers, "Promo: " + oPromo.Title.Trim());
				}
				if (driverPaidPromos.Count == 0 && !this.Drivers.Name.Equals(CHARGECARDS, StringComparison.CurrentCultureIgnoreCase)) {
					driverPaidPromos = DriverPayments.GetPaymentsByReferenceNo(this.Drivers, "Promo: " + oPromo.DriverPromotionID.ToString());
				}
				if (driverPaidPromos.Count == 0 && !this.Drivers.Name.Equals(CHARGECARDS, StringComparison.CurrentCultureIgnoreCase)) {
					driverPaidPromos = DriverPayments.GetPaymentsByReferenceNo(this.Drivers, "Promo: " + oPromo.Title.Trim());
				}

				// 7/26/2011 had to change from Title as the string length exceeded DriverPayments.ReferenceNo
				//string temp = "Promo: " + oPromo.Title.Trim() + " | " + this.AffiliateDriverID.Value.ToString("F0");
				string temp = "Promo: " + oPromo.DriverPromotionID.ToString() + " | " + this.AffiliateDriverID.GetValueOrDefault(0).ToString("F0");

				EntityList<DriverPayments> referrerPaidPromos = DriverPayments.GetPaymentsByReferenceNo(oPM, temp);
				if (referrerPaidPromos.Count == 0) {
					temp = "Promo: " + oPromo.Title.Trim() + " | " + this.AffiliateDriverID.GetValueOrDefault(0).ToString("F0");
					referrerPaidPromos = DriverPayments.GetPaymentsByReferenceNo(oPM, temp);
				}

				long transCount = DriverPayments.GetChargeCountForPromos(this.AffiliateDrivers, oPromo.StartDate);
				transCount += GetChargeCountForPromos(this.Drivers, oPromo.StartDate);

				if (oPromo.OneTimeUse) {
					if (transCount >= oPromo.VoucherCount) {

						if (driverPaidPromos.Count == 0 && oPromo.DriverPayout > 0) {
							PayPromoToAffiliateDriver(oPromo);

							if (referrerPaidPromos.Count == 0) {
								PayPromoToReferrer(oPromo);
							} else {
								AffiliateDriverReferrals referral = AffiliateDriverReferrals.GetByReferredDriver(oPM, this.AffiliateDriverID.Value);
								if (!referral.IsNullEntity) {
									var canPayAgain = (from p in referrerPaidPromos
													   where p.ChargeDate >= referral.ReferralDate
													   select p).ToList();
									if (canPayAgain.Count == 0) {
										PayPromoToReferrer(oPromo);
									}
								}
							}
						} else if (referrerPaidPromos.Count == 0) {
							PayPromoToReferrer(oPromo);
						} else {
							AffiliateDriverReferrals referral = AffiliateDriverReferrals.GetByReferredDriver(oPM, this.AffiliateDriverID.Value);
							if (!referral.IsNullEntity) {
								var canPayAgain = (from p in referrerPaidPromos
												   where p.ChargeDate >= referral.ReferralDate
												   select p).ToList();
								if (canPayAgain.Count == 0) {
									PayPromoToReferrer(oPromo);
								}
							}
						}
					}
				} else {
					//check if driver needs another promo payment
					long paymentsProcessed = transCount / oPromo.VoucherCount;
					if (paymentsProcessed > driverPaidPromos.Count || paymentsProcessed > referrerPaidPromos.Count) {
						PayPromoToAffiliateDriver(oPromo);
						PayPromoToReferrer(oPromo);
					}
				}
			}
		}

		/// <summary>
		/// Pays the promo to referrer.
		/// </summary>
		/// <param name="pDriverPromotions">The driver promotions.</param>
		private void PayPromoToReferrer(DriverPromotions pDriverPromotions) {
			//check if driver was referred
			if (!this.AffiliateDriverID.HasValue) {
				return;
			}
			if (pDriverPromotions.ReferrerPayout == 0) {
				return;
			}

			PersistenceManager oPM = this.PersistenceManager;
			AffiliateDriverReferrals referral = AffiliateDriverReferrals.GetByReferredDriver(oPM, this.AffiliateDriverID.Value);
			if (referral.IsNullEntity) {
				return;
			}

			//if android only pay when driver swipes card
			if (("" + referral.Platform).StartsWith(Platforms.Android.ToString(), StringComparison.CurrentCultureIgnoreCase)) {
				if (GetChargeCountForPromos(this.AffiliateDrivers, referral.ReferralDate, true) == 0) {
					return;
				}
			}

			// get the driver account of the sales rep
			Drivers salesRepDriver = Drivers.GetDriverByCell(this.PersistenceManager, referral.AffiliateDrivers.Cell);
			if (salesRepDriver.IsNullEntity) {
				salesRepDriver = Drivers.GetChargeCard(referral.AffiliateDrivers.Affiliate);
			}

			DriverPayments newPromo = DriverPayments.Create(salesRepDriver);
			newPromo.AffiliateDriverID = referral.AffiliateDriverID;
			newPromo.Fare = pDriverPromotions.ReferrerPayout;
			newPromo.Failed = false;
			newPromo.ChargeDate = DateTime.Now;
			newPromo.FraudFailure = false;
			newPromo.Platform = pDriverPromotions.Platform;
			newPromo.ChargeBy = this.TransNo;
			newPromo.CardNumber = pDriverPromotions.CardNumber;
			newPromo.CVV = pDriverPromotions.CardCVV;
			string promoExpMonth = pDriverPromotions.CardExpires.Left(2);
			int year = Convert.ToInt32(pDriverPromotions.CardExpires.Right(2));
			if (year < 2000) {
				year += 2000;
			}
			string promoDate = promoExpMonth + year.ToString();
			newPromo.Expiration = promoDate;

			if (newPromo.AffiliateDrivers.AutoPayManual || newPromo.AffiliateDrivers.AutoPaySwipe) {
				if (!newPromo.AffiliateDrivers.BankAccountNumber.IsNullOrEmpty() || !newPromo.AffiliateDrivers.TransCardAdminNo.IsNullOrEmpty()) {
					newPromo = SetDriverPaid(newPromo);
				}
			}

			newPromo.Save();
			newPromo.ProcessResponse(pDriverPromotions, VerisignTrxTypes.Sale, this.AffiliateDriverID.Value);

			// Only send a message if there is one to send
			if (!pDriverPromotions.ReferrerPayoutMessage.IsNullOrEmpty()) {
				string msg = DriverPayments.ProcessMessage(pDriverPromotions.ReferrerPayoutMessage, newPromo, this, false);
				//We are sending 2 driverpayments because we need referred(this) driver information and referrer(newPromo) driver information
				newPromo.AffiliateDrivers.SendDriverMsg(msg, false, true);
			}
		}


		// **************************************************************************************
		// Pay the Aff Driver's promo
		// **************************************************************************************
		private void PayPromoToAffiliateDriver(DriverPromotions pDriverPromotions) {
			if (!this.AffiliateDriverID.HasValue && this.Drivers.Name.Equals(CHARGECARDS, StringComparison.CurrentCultureIgnoreCase)) {
				return;
			}
			if (pDriverPromotions.DriverPayout == 0) {
				return;
			}

			DriverPayments newPromo = DriverPayments.Create(this.Drivers);
			if (this.AffiliateDriverID.GetValueOrDefault(0) > 0) {
				newPromo.AffiliateDriverID = this.AffiliateDriverID;
			}
			newPromo.DriverID = this.DriverID;
			newPromo.Fare = pDriverPromotions.DriverPayout;
			newPromo.Failed = false;
			newPromo.ChargeDate = DateTime.Now;
			newPromo.FraudFailure = false;
			newPromo.Platform = pDriverPromotions.Platform;
			newPromo.ChargeBy = this.TransNo;
			newPromo.CardNumber = pDriverPromotions.CardNumber;
			newPromo.CVV = pDriverPromotions.CardCVV;
			string promoExpMonth = pDriverPromotions.CardExpires.Left(2);
			int year = Convert.ToInt32(pDriverPromotions.CardExpires.Right(2));
			if (year < 2000) {
				year += 2000;
			}
			string promoDate = promoExpMonth + year.ToString();
			newPromo.Expiration = promoDate;


			if (newPromo.AffiliateDrivers.AutoPayManual || newPromo.AffiliateDrivers.AutoPaySwipe) {
				if (!newPromo.AffiliateDrivers.BankAccountNumber.IsNullOrEmpty() || !newPromo.AffiliateDrivers.TransCardAdminNo.IsNullOrEmpty()) {
					newPromo = SetDriverPaid(newPromo);
				}
			}

			newPromo.Save();
			newPromo.ProcessResponse(pDriverPromotions, VerisignTrxTypes.Sale);
			if (pDriverPromotions.Platform == Platforms.Android.ToString() || pDriverPromotions.Platform == Platforms.iPhone.ToString()) {
				string msg = ProcessMessage(pDriverPromotions.DriverPayoutMessage, newPromo, this, false);
				if (!msg.IsNullOrEmpty()) {
					if (newPromo.AffiliateDrivers.IsNullEntity) {
						newPromo.Drivers.SendMsgViaTwilio(msg);
					} else {
						newPromo.AffiliateDrivers.SendDriverMsg(msg, false, true);
					}
				}
			} else {
				string url = "http://cabpay.com/Twilio/TwilioSayPromo.aspx?z=";
				string param = "promo=" + pDriverPromotions.DriverPromotionID.ToString("F0");
				param += "&driver=" + this.AffiliateDriverID.Value.ToString("F0");
				param += "&referrer=0";
				param += "&payment=" + newPromo.DriverPaymentID.ToString("F0");
				AffiliateDrivers.CallDriver(url + param.EncryptIceKey());
			}

		}


		public static string ProcessMessage(string pMsg, DriverPayments pPayment, bool pPhoneticise) {
			return ProcessMessage(pMsg, pPayment, pPayment.PersistenceManager.GetNullEntity<DriverPayments>(), pPhoneticise);
		}


		public static string ProcessMessage(string pMsg, DriverPayments pPayment, DriverPayments pReferredPayment, bool pPhoneticise) {
			//If we want to do any changes in the msg ("comes from driverpromotions table") we want to send we need to put DriverPayment or Referred titles to the begining of the objects to specify where it is belong to. 
			if (pMsg.IsNullOrEmpty()) {
				return pMsg;
			}

			MatchCollection oMatch = new Regex("{{[^}}]*}}|\\[[^]]*]", RegexOptions.Multiline | RegexOptions.IgnoreCase).Matches(pMsg);
			foreach (Match match1 in oMatch) {
				string sText = match1.Value;

				if (sText.StartsWith("{")) {
					sText = sText.Replace("{", "");
					sText = sText.Replace("}", "");
					try {
						DriverPayments myRec = pPayment;
						if (sText.StartsWith("Referred")) {//this logic added if we want to pull referred information
							myRec = pReferredPayment;
						}
						object oObj = CabRideEngine.Utils.EMailMessage.Eval(sText.Split('.'), myRec);
						string sValue = "";
						if (oObj != null) {
							sValue = oObj.ToString();
						}
						if (pPhoneticise) {
							if (sText.Contains("TransNo")) {
								sValue = StringUtils.Phoneticise(sValue, true);
							}
						}
						pMsg = pMsg.Replace(match1.Value, sValue);

					} catch (Exception ex) {
						throw new Exception("Error returned while parsing " + sText + ": " + ex.Message.ToString());
					}



				}
			}
			return pMsg;
		}



		// **********************************************************************************************
		//
		// **********************************************************************************************
		public void DoPerTransactionRecurringFee() {
			StringBuilder sWork = new StringBuilder();

			if (this.ChargeBack || this.Drivers.DriverRecurringFeeses.Count == 0) {
				return;
			}
			List<DriverRecurringFees> fees = (from p in this.Drivers.DriverRecurringFeeses
											  where p.ChargePerTrans && !p.Cancelled && !p.Closed
											  select p).ToList();
			if (fees.Count == 0) {
				return;
			}
			foreach (DriverRecurringFees oFee in fees) {
				//if we already charged for this transaction, don't charge again
				DriverPayments recurringfee = GetPerTransactionRecurringFee(false);
				if (!recurringfee.IsNullEntity) {
					return;
				}
				StringBuilder sql = new StringBuilder();
				sql.Append("SELECT SUM(Fare + Gratuity + AirportFee + MiscFee - Discount) FROM DriverPayments  WITH (NOLOCK) WHERE ChargeBy = '");
				sql.Append(RECURRING_FEES);
				sql.Append("' AND ReferenceNo = 'D");
				sql.Append(oFee.DriverRecurringFeeID);
				sql.Append("'");

				DataTable table = SQLHelper.GetRecords(this.PersistenceManager, sql.ToString());
				decimal amountPaid = 0;
				if (table.Rows.Count > 0) {
					if (table.Rows[0][0].GetType() != typeof(DBNull) && table.Rows[0][0] != null) {
						amountPaid = Convert.ToDecimal(table.Rows[0][0]);
					}
				}
				amountPaid = Math.Abs(amountPaid);
				decimal chargeFee = oFee.Amount;
				decimal balance = oFee.MaxTotalCharges - amountPaid;
				if (oFee.MaxTotalCharges > 0) {
					if ((oFee.MaxTotalCharges - amountPaid) < chargeFee) {
						chargeFee = oFee.MaxTotalCharges - amountPaid;
					}
					if (balance == 0) {
						oFee.Closed = true;
						oFee.Save();
						return;
					}
				}

				DriverPayments oPayment = DriverPayments.Create(this.Drivers);
				oPayment.AffiliateDriverID = this.AffiliateDriverID;
				oPayment.AffiliateID = this.AffiliateID;
				oPayment.ChargeBack = true;
				oPayment.ChargeBackDate = DateTime.Now;
				oPayment.ChargeDate = oPayment.ChargeBackDate;

				sWork.Remove(0, sWork.Length);
				if (oFee.MaxTotalCharges > 0) {
					sWork.Append(oFee.ExpenseTypes.Description);
					sWork.Append(" (Total Charge: ");
					sWork.Append(oFee.MaxTotalCharges.ToString("C"));
					sWork.Append(" -  Total Paid: ");
					sWork.Append(amountPaid.ToString("C"));
					sWork.Append(" = ");
					sWork.Append(balance.ToString("C"));
					sWork.Append(" Payment: ");
					sWork.Append(chargeFee.ToString("C"));
					sWork.Append(" -> New Balance: ");
					sWork.Append((balance - chargeFee).ToString("C"));
					sWork.Append(")");
				} else {
					sWork.Append(oFee.ExpenseTypes.Description);
					sWork.Append(" (Charge: ");
					sWork.Append(oFee.Amount.ToString("C"));
					sWork.Append(")");
				}
				oPayment.ChargeBackInfo = sWork.ToString();

				oPayment.FareNoTaxiPassFee = -chargeFee;
				oPayment.ChargeBy = RECURRING_FEES;
				oPayment.PreAuthBy = this.TransNo;
				oPayment.ReferenceNo = "D" + oFee.DriverRecurringFeeID.ToString("F0");
				oPayment.DriverPaid = true;
				oPayment.DriverPaidDate = oPayment.ChargeBackDate;
				oPayment.Save();

				DriverPaymentAux oAux = DriverPaymentAux.Create(oPayment);
				oAux.ChargeBackDriverPaymentID = this.DriverPaymentID;
				oAux.Save();

				this.ReceiptChargedBack = oPayment.TransNo;
				this.Save();
			}
		}


		public DriverPayments GetPerTransactionRecurringFee(bool pNotACHd) {
			RdbQuery qry = new RdbQuery(typeof(DriverPayments), DriverPayments.PreAuthByEntityColumn, EntityQueryOp.EQ, this.TransNo);
			qry.AddClause(DriverPayments.ChargeByEntityColumn, EntityQueryOp.EQ, RECURRING_FEES);
			DriverPayments recurringfee = this.PersistenceManager.GetEntity<DriverPayments>(qry);

			if (!pNotACHd) {
				if (!recurringfee.ACHDetail.IsNullEntity) {
					recurringfee = this.PersistenceManager.GetNullEntity<DriverPayments>();
				}

			}
			return recurringfee;
		}

		public EntityList<DriverPayments> GetRecurringFeesByDriver(bool pNotACHd) {
			RdbQuery qry = new RdbQuery(typeof(DriverPayments), DriverPayments.DriverIDEntityColumn, EntityQueryOp.EQ, this.DriverID);
			qry.AddClause(DriverPayments.ChargeByEntityColumn, EntityQueryOp.EQ, RECURRING_FEES);
			qry.AddOrderBy(DriverPayments.ChargeBackDateEntityColumn);
			EntityList<DriverPayments> list = this.PersistenceManager.GetEntities<DriverPayments>(qry, QueryStrategy.DataSourceThenCache);
			if (pNotACHd) {
				var x = (from p in list where p.ACHDetail.IsNullEntity select p).ToList();
			}

			// CA 3/18:  Fund test DriverPayments with no AffiliateID
			foreach (DriverPayments oDP in list) {
				if (oDP.AffiliateID.ToString().IsNullOrEmpty()) {
					oDP.AffiliateID = this.Affiliate.AffiliateID;
				}
			}

			return list;
		}


		// ******************************************************************************************
		// Create a dummy TransactionResponse for when we don't really call USAePay
		// ******************************************************************************************
		private TaxiPassCommon.Banking.com.USAePay.TransactionResponse CreateDummyUSAePayResponse() {
			TaxiPassCommon.Banking.com.USAePay.TransactionResponse oResponse = new TaxiPassCommon.Banking.com.USAePay.TransactionResponse();
			oResponse.Result = "0";
			oResponse.Error = "Approved";
			oResponse.RefNum = "Zero charge";
			oResponse.AuthCode = "-999";
			return oResponse;
		}

		/// <summary>
		/// Creates the dummy processor response.
		/// </summary>
		/// <returns></returns>
		private ProcessorResponse CreateDummyProcessorResponse() {
			ProcessorResponse response = new ProcessorResponse();
			response.Result = "0";
			response.Message = "Approved";
			response.Reference = "Zero charge";
			response.AuthCode = "-999";
			return response;
		}



		/// <summary>
		/// Charges the auth amount.
		/// </summary>
		/// <returns></returns>
		/// <exception cref="System.Exception">Card was not PreAuthed</exception>
		public DriverPaymentResults ChargeAuthAmount() {

			//VerisignAccounts oAcct = this.Affiliate.MyVerisignAccount;
			this.VerisignAccountID = Utils.CreditCardUtils.GetDriverPaymentCardGatewayGroupsID(this.PersistenceManager, this.TransNo, 0);
			bool testCard = CardUtils.IsTestCard(this.DecryptCardNumber());

			string sTranType = VerisignTrxTypes.Sale;
			double chargeAmount = Convert.ToDouble(this.TotalCharge);

			decimal authAmount = this.AuthAmount;
			string cardProcessor = "";

			ProcessorResponse response = new ProcessorResponse();
			DriverPaymentResults chargeAuthResult = null;
			if (AuthDate == null) {
				throw new Exception("Card was not PreAuthed");
			} else {
				//Delayed Sale
				string sRefNo = "";
				sTranType = VerisignTrxTypes.DelayedCapture;


				foreach (DriverPaymentResults oResult in this.DriverPaymentResultses) {
					if ((oResult.Result == "0" || oResult.Message.StartsWith("Approved", StringComparison.CurrentCultureIgnoreCase)) && oResult.TransType == VerisignTrxTypes.Authorization) {
						sRefNo = oResult.Reference;
						if (oResult.Amount > 0) {
							authAmount = oResult.Amount;
						}
						cardProcessor = oResult.CreditCardProcessor;
						chargeAuthResult = oResult;
						break;
					}
				}

				CreditCardChargeType chargeType = CreditCardChargeType.ECommerce;
				if (Platform != Platforms.GetRide.ToString()) {
					if (CardSwiped) {
						chargeType = CreditCardChargeType.Swipe;
					} else {
						chargeType = CreditCardChargeType.Manual;
					}
				}
				Gateway gway = new Gateway(Utils.CreditCardUtils.LoadGateWays(this.PersistenceManager, this.VerisignAccountID.Value, chargeType, CardType));
				gway.SetProcessor(cardProcessor.ToEnum<CardProcessors>());
				response = gway.ChargeAuth(new CCRequestRefNo(sRefNo, authAmount, testCard, string.Format("AD:{0} D:{1}", AffiliateDriverID, DriverID), TransNo, IsGetRideTrans, this.CardNumberDisplay));
			}
			return ProcessResponse(response, sTranType, authAmount, chargeAuthResult);
		}

		/// <summary>
		/// Does the driver promo charge.
		/// </summary>
		/// <param name="pPromoList">The promo list.</param>
		/// <param name="pCardNo">The card no.</param>
		/// <param name="pCardInfo">The card info (swipe).</param>
		/// <returns></returns>
		/// <exception cref="System.Exception"></exception>
		private DriverPaymentResults DoDriverPromoCharge(EntityList<DriverPromotions> pPromoList, string pCardNo, string pCardInfo) {

			bool promoFound = false;
			DriverPromotions myPromo = null;

			foreach (DriverPromotions oPromo in pPromoList) {
				if (oPromo.AllAffiliates || oPromo.DriverPromotionAffiliateses.FirstOrDefault(p => p.AffiliateID == this.AffiliateID) != null) {
					if (DateTime.Today >= oPromo.StartDate && DateTime.Today <= oPromo.EndDate) {
						if (pCardNo.Equals(oPromo.CardNumber) && this.CVV == oPromo.CardCVV && oPromo.CardExpires.Equals(this.Expiration.Left(2) + this.Expiration.Right(2))) {
							promoFound = true;
							myPromo = oPromo;
							break;
						}
					}
				}
			}

			if (promoFound) {
				EntityList<DriverPayments> payList = new EntityList<DriverPayments>();
				if (this.AffiliateDrivers.IsNullEntity) {
					payList = DriverPayments.GetPaymentByCardNo(this.Drivers, pCardNo, this.TransNo);
				} else {
					payList = DriverPayments.GetPaymentByCardNo(this.AffiliateDrivers, pCardNo, this.TransNo);
				}

				if (payList.Count > 0) {
					this.Failed = true;
					this.DriverPaymentNotes.Reason = "Card not accepted 9, promo already used";
					this.AuthDate = DateTime.Now;
					this.Save();
					throw new Exception(this.DriverPaymentNotes.Reason);
				}
			} else {
				this.Failed = true;
				this.DriverPaymentNotes.Reason = "Promo not valid";
				this.AuthDate = DateTime.Now;
				this.Save();
			}

			this.FareNoTaxiPassFee = myPromo.DriverPayout;
			string sTranType = VerisignTrxTypes.Sale;

			if (TotalCharge > AuthAmount || AuthDate == null) {
				sTranType = VerisignTrxTypes.Sale;
			} else {
				//Delayed Sale
				sTranType = VerisignTrxTypes.DelayedCapture;
			}
			return ProcessResponse(myPromo, sTranType);
		}


		/// <summary>
		/// Refunds the card.
		/// </summary>
		/// <param name="pDriverPaymentsReference">The reference no from the processor.</param>
		/// <returns></returns>
		public DriverPaymentResults RefundCard(string pDriverPaymentsReference, string pOriginalTransNo) {
			DateTime elapsedStart = DateTime.Now;
			TaxiPassCommon.Banking.CardProcessors oProcessor = TaxiPassCommon.Banking.CardProcessors.Verisign;

			if (!this.CreditCardProcessor.IsNullOrEmpty()) {
				oProcessor = this.CreditCardProcessor.ToEnum<CardProcessors>();
			}

			if (this.RowState == DataRowState.Added) {
				this.Save();
			}
			DriverPaymentRef payRef = DriverPaymentRef.GetCreate(this, "Refund", "Original TransNo");
			payRef.ValueString = pOriginalTransNo;
			payRef.Save();

			Gateway gway = null;
			//VerisignAccounts oAcct = this.Affiliate.MyVerisignAccount;
			//Gateway gway = oAcct.GetGateway(oProcessor, VerisignTrxTypes.Credit.ToString());
			if (VerisignAccountID.HasValue && VerisignAccountID.Value > 0) {
				List<IGatewayCredentials> acctList = Utils.CreditCardUtils.LoadGateWays(PersistenceManager, VerisignAccountID.Value, oProcessor, CardType);
				gway = new Gateway(acctList.FirstOrDefault(p => p.MyGateway() == oProcessor));
			} else {
				CreditCardChargeType chargeType = CreditCardChargeType.ECommerce;
				if (Platform != Platforms.GetRide.ToString()) {
					if (CardSwiped) {
						chargeType = CreditCardChargeType.Swipe;
					} else {
						chargeType = CreditCardChargeType.Manual;
					}
				}
				gway = Affiliate.GetGateway(oProcessor, chargeType, CardType);
			}
			ProcessorResponse response = gway.Refund(new CCRequestRefNo(pDriverPaymentsReference, Math.Round(this.TotalCharge, 2), this.Test, string.Format("AD:{0} D:{1}", AffiliateDriverID, DriverID), TransNo, IsGetRideTrans, this.CardNumberDisplay));

			int elapsed = (int)DateTime.Now.Subtract(elapsedStart).TotalMilliseconds;
			var dpElapsedRef = DriverPaymentRef.GetCreate(this, "ElapsedTime", "Refund Old");
			dpElapsedRef.ValueString = elapsed.ToString();
			dpElapsedRef.Save();

			return ProcessResponse(response, VerisignTrxTypes.Credit, this.TotalCharge, null);
		}

		/// <summary>
		/// Voids a voucher.
		/// </summary>
		/// <param name="pDriverPaymentsReference">The reference no from the processor.</param>
		/// <returns></returns>
		public DriverPaymentResults VoidTrans(string pDriverPaymentsReference) {
			TaxiPassCommon.Banking.CardProcessors oProcessor = TaxiPassCommon.Banking.CardProcessors.Verisign;

			if (!string.IsNullOrEmpty(this.CreditCardProcessor)) {
				oProcessor = StringUtils.StringToEnum<TaxiPassCommon.Banking.CardProcessors>(this.CreditCardProcessor);
			}
			CreditCardChargeType chargeType = CreditCardChargeType.ECommerce;
			if (Platform != Platforms.GetRide.ToString()) {
				if (CardSwiped) {
					chargeType = CreditCardChargeType.Swipe;
				} else {
					chargeType = CreditCardChargeType.Manual;
				}
			}


			Gateway gway = null;
			//VerisignAccounts oAcct = this.Affiliate.MyVerisignAccount;
			//Gateway gway = oAcct.GetGateway(oProcessor, VerisignTrxTypes.Credit.ToString());
			if (VerisignAccountID.HasValue && VerisignAccountID.Value > 0) {
				List<IGatewayCredentials> acctList = Utils.CreditCardUtils.LoadGateWays(PersistenceManager, VerisignAccountID.Value, chargeType, CardType);
				gway = new Gateway(acctList.FirstOrDefault(p => p.MyGateway() == oProcessor));
			} else {
				gway = Affiliate.GetGateway(oProcessor, chargeType, CardType);
			}
			ProcessorResponse response = gway.Void(new CCRequestRefNo(pDriverPaymentsReference, Math.Round(this.TotalCharge, 2), this.Test, string.Format("AD:{0} D:{1}", AffiliateDriverID, DriverID), TransNo, IsGetRideTrans, this.CardNumberDisplay));
			return ProcessResponse(response, VerisignTrxTypes.Void, this.TotalCharge, null);
		}



		// *************************************************************************************************************************************
		// Process charge/preauth response
		// *************************************************************************************************************************************
		public DriverPaymentResults ProcessResponse(ProcessorResponse pResponse, string pTrxType, decimal pAmount, DriverPaymentResults pChargeAuthResults) {
			PersistenceManager oPM = this.PersistenceManager;

			try {
				this.AffiliateID = this.Drivers.MyAffiliate.AffiliateID;
				DriverPaymentResults oResult = DriverPaymentResults.Create(this);
				oResult.Amount = pAmount;
				oResult.Result = pResponse.Result;
				oResult.Message = pResponse.Message;

				oResult.Reference = pResponse.Reference;

				oResult.AuthCode = pResponse.AuthCode;
				oResult.Avs = pResponse.AVS;
				oResult.Avszip = pResponse.AVSZip;
				oResult.CVV2Match = pResponse.CVV2Match;

				// 12/5/11 Added for Carmel
				oResult.AVSResultCode = pResponse.AVSResultCode;

				// copied data from Auth for Carmel as they were complaining that we were not sending them the data
				// USAePay only sends the data once if during the preAuth
				if (pChargeAuthResults != null && !pChargeAuthResults.IsNullEntity) {
					if (oResult.Avs.IsNullOrEmpty()) {
						oResult.Avs = pChargeAuthResults.Avs;
					}
					if (oResult.Avszip.IsNullOrEmpty()) {
						oResult.Avszip = pChargeAuthResults.Avszip;
					}
					if (oResult.CVV2Match.IsNullOrEmpty()) {
						oResult.CVV2Match = pChargeAuthResults.CVV2Match;
					}
					if (oResult.AVSResultCode.IsNullOrEmpty()) {
						oResult.AVSResultCode = pChargeAuthResults.AVSResultCode;
					}
				}


				oResult.TransType = pTrxType;
				oResult.TransDate = DateTime.Now;

				// store processor for refunds and charge backs
				oResult.CreditCardProcessor = pResponse.CreditCardProcessor.ToString();
				this.CreditCardProcessor = oResult.CreditCardProcessor;

				if (!pResponse.BillingDescriptor.IsNullOrEmpty()) {
					var refRec = DriverPaymentRef.GetCreate(this, "BillingDescriptor", "BillingDescriptor");
					refRec.ValueString = pResponse.BillingDescriptor;
					bool saved = false;

					while (!saved) {
						try {
							refRec.Save();
							saved = true;
						} catch {
						}
					}
				}


				this.MyTimeStats.Info.Add(new TimeStats.ElapsedInfo { Type = "Processed Results", Elapsed = DateTime.Now.Subtract(MyTimeStats.Start).TotalSeconds });
				bool isStoreForward = SetStoreForward();
				// store processor for refunds and charge backs
				if (pChargeAuthResults != null && pChargeAuthResults.DriverPayments.TaxiPassRedeemerID.HasValue && oResult.Message.Equals("Transactions has already been settled.", StringComparison.CurrentCultureIgnoreCase)) {
					this.Failed = false;
					oResult.Result = "0";
					oResult.Message = "Approved";
				}
				if (oResult.Result != "0") {
					if (this.DriverPaymentNotes.IsNullEntity) {
						DriverPaymentNotes.Create(this);
					}
					this.DriverPaymentNotes.Reason = oResult.Message;
					if (("" + this.DriverPaymentNotes.Reason).Contains("Requires Voice Authentication")) {
						this.DriverPaymentNotes.Reason = "Card Declined";
					}
					this.Failed = true;
					if (isStoreForward && Fare > 0) {
						if (this.Affiliate.AffiliateDriverDefaults.StoreForward > 0) {
							this.Failed = false;

							UpdateStoreForwardDeclined(true);
						}
					}

				} else {
					this.Failed = false;
					if (isStoreForward && Fare > 0) {
						UpdateStoreForwardDeclined(false);
					}

					DriverPaymentNotes oNotes = this.DriverPaymentNotes;
					if (oNotes.IsNullEntity) {
						oNotes = DriverPaymentNotes.Create(this);
					}
					oNotes.Reason = "";

					if (pTrxType == VerisignTrxTypes.Authorization) {
						this.AuthDate = DateTime.Now;
						if (!this.CardSwiped) {
							// 12/2 New flag for Carmel
							if (this.Affiliate.AffiliateDriverDefaults.IgnoreFailedZipOnCharge.GetValueOrDefault(false)) {
								this.IgnoreCardZIP = true;
							}

							// simulate Failed trans for test cards when CVV > 500
							if (Test && !CVV.IsNullOrEmpty() && Convert.ToInt32(CVV) > 500) {
								this.Failed = true;
								oResult.Result = "-1";
								oResult.Message = "Invalid CVV";
								oNotes.Reason = oResult.Message;
							}
							if (!this.IgnoreCardZIP.GetValueOrDefault(false) && !string.IsNullOrEmpty(this.CardZIPCode)) {
								if (!oResult.Avszip.IsNullOrEmpty() && oResult.Avszip.Equals("N", StringComparison.CurrentCultureIgnoreCase)) {
									this.Failed = true;
									oResult.Result = "-1";
									oResult.Message = "Invalid Billing Zip Code";
									oNotes.Reason = oResult.Message;
									//oResult.Save();
								} else {
									if (this.Test && !string.IsNullOrEmpty(this.CardZIPCode)) {
										try {
											int zip = Convert.ToInt32(this.CardZIPCode);
											if (zip >= 50000) {
												this.Failed = true;
												oResult.Result = "-1";
												oResult.Message = "Invalid Billing Zip Code";
												oNotes.Reason = oResult.Message;
												//oResult.Save();
											}
										} catch {
										}
									}
								}

							}
						}
						this.MyTimeStats.Info.Add(new TimeStats.ElapsedInfo { Type = "Result Auth", Elapsed = DateTime.Now.Subtract(MyTimeStats.Start).TotalSeconds });
					} else if (pTrxType == VerisignTrxTypes.Void) {
						if (oResult.Result == "0") {
							// ensure transaction is not paid out by failing voucher
							this.Failed = true;
							this.DoNotPay = true;
						}

						this.MyTimeStats.Info.Add(new TimeStats.ElapsedInfo { Type = "Result Void", Elapsed = DateTime.Now.Subtract(MyTimeStats.Start).TotalSeconds });
					} else {
						if (pTrxType == VerisignTrxTypes.Credit) {
							if (!this.ChargeDate.HasValue) {
								this.ChargeDate = DateTime.Now;
							}
						} else {
							this.ChargeDate = DateTime.Now;
						}
						if (this.Platform != Platforms.Redeemer.ToString()) {
							if (this.Drivers.DriverBankInfos.Count > 0) {
								DriverBankInfo oBank = this.Drivers.DriverBankInfos[0];
								if (oBank.BankAccountNumber != "" && oBank.BankCode != "" && this.AffiliateDrivers.TransCardNumber.IsNullOrEmpty()) {
									SetDriverPaid(true);
								}
							}
							SetDriverPaid(false);
							this.MyTimeStats.Info.Add(new TimeStats.ElapsedInfo { Type = "SetDriverPaid", Elapsed = DateTime.Now.Subtract(MyTimeStats.Start).TotalSeconds });
						}
					}
				}
				bool bSaved = false;
				while (!bSaved) {
					try {
						this.Save();
						bSaved = true;
					} catch (Exception) {
						//force system to save
						//chances are system deadlocked
					}
				}
				this.MyTimeStats.Info.Add(new TimeStats.ElapsedInfo { Type = "Save DriverPaymentResults", Elapsed = DateTime.Now.Subtract(MyTimeStats.Start).TotalSeconds });
				return oResult;
			} catch (Exception ex) {
				throw ex;
			}

		}

		private bool SetStoreForward() {
			bool isStoreForward = ("" + this.ReferenceNo).StartsWith("StoreForward:");
			if (isStoreForward) {
				//DriverPaymentRef sfRef = DriverPaymentRef.GetCreate(this, "StoreForward", "IsStoreForward");
				//sfRef.ValueString = true.ToString();
				//sfRef.Save();
				StoreForward = true;
				try {
					List<string> data = ReferenceNo.Split('-').ToList();
					if (data.Count == 3) {
						string myDate = data[1].Trim();
						int year = Convert.ToInt32(myDate.Left(4));
						myDate = myDate.Remove(0, 4);
						int month = Convert.ToInt32(myDate.Left(2));
						myDate = myDate.Remove(0, 2);
						int day = Convert.ToInt32(myDate.Left(2));
						myDate = myDate.Remove(0, 2);
						int hour = Convert.ToInt32(myDate.Left(2));
						myDate = myDate.Remove(0, 2);
						int minute = Convert.ToInt32(myDate);

						StoreForwardDate = new DateTime(year, month, day, hour, minute, 0);
					}
				} catch (Exception) {
				}
			}
			return isStoreForward;
		}

		private void UpdateStoreForwardDeclined(bool pDeclined) {
			bool sendEMail = false;
			DriverPaymentRef sfDeclinedRef = DriverPaymentRef.GetCreate(this, "StoreForward", "Declined");
			sendEMail = sfDeclinedRef.RowState == DataRowState.Added;
			sfDeclinedRef.ValueString = pDeclined.ToString();
			sfDeclinedRef.Save();
			WriteOff = pDeclined;

			if (pDeclined && !this.Test) {
				this.AffiliateDrivers.Notes += string.Format("{0}{1} RedeemerStopPayment due to StoreForward Declined", this.AffiliateDrivers.Notes.IsNullOrEmpty() ? "" : "\r\n", DateTime.Today);
				this.AffiliateDrivers.RedeemerStopPayment = true;
				this.AffiliateDrivers.Save();
			}

			//Send EMAIL out
			if (sendEMail && pDeclined) {
				using (var conn = CabRideEngineDapper.Utils.SqlHelper.OpenSqlConnection()) {
					CabRideEngineDapper.SystemDefaultsDict dict = CabRideEngineDapper.SystemDefaultsDict.GetByKey(conn, "StoreForward", "EMailFailedTransTo", false);
					if (dict.IsNullEntity || dict.ValueString.IsNullOrEmpty()) {
						return;
					}
					StringBuilder msg = new StringBuilder();
					msg.AppendLine(string.Format("Driver's Name: {0}<br />", this.AffiliateDrivers.Name));
					msg.AppendLine(string.Format("Phone Number: {0}<br /><br />", this.AffiliateDrivers.Cell));
					msg.AppendLine(string.Format("Voucher Number: {0}<br />", this.TransNo));
					msg.AppendLine(string.Format("Driver Total: {0}<br />", this.DriverTotal.ToString("C")));

					CabRideEngineDapper.SystemDefaultsDict dict1 = CabRideEngineDapper.SystemDefaultsDict.GetByKey("EMail", "Receipt", false);
					string[] cred = dict1.ValueString.Decrypt().Split('|');
					TaxiPassCommon.Mail.EMailMessage email = new TaxiPassCommon.Mail.EMailMessage(cred[0], "Failed StoreForward", msg.ToString(), cred[1]);
					email.UseWebServiceToSendMail = Properties.Settings.Default.EMailViaWebService;
					email.SendMail(dict.ValueString);
				}
			}
		}

		private void IsOpenCredit(bool pOpenCredit) {
			DriverPaymentRef rec = DriverPaymentRef.GetCreate(this, "OpenCredit", "IsOpenCredit");
			rec.ValueString = pOpenCredit.ToString();
			rec.Save();
		}


		// ***************************************************************************
		// 11/17 centralized/standarized the call to DriverPaid = true
		// mark as paid:
		//		Fleet set to autopay
		//		Manually entered (IVR) and not swiped
		//		Auto pay for swiped and the card was swiped
		// ***************************************************************************
		public void SetDriverPaid(bool pForcePaid) {
			if (this.Test || this.Failed || ("" + this.CardType).Equals("TPCard", StringComparison.CurrentCultureIgnoreCase)) {
				return;
			}

			if (this.Drivers.MyAffiliate != null
				&& (this.Drivers.MyAffiliate.AutoPayFleet
				|| (this.AffiliateDrivers.AutoPayManual && !this.CardSwiped)
				|| (this.AffiliateDrivers.AutoPaySwipe && this.CardSwiped))) {

				bool driverInitiated = this.Affiliate.DriverInitiatedPayCardPayment && !this.AffiliateDrivers.TransCardNumber.IsNullOrEmpty();

				if ((!this.Drivers.PaidByRedeemer && !driverInitiated) || pForcePaid) {
					// 
					if (!this.AffiliateDrivers.RedeemerStopPayment) {
						DriverPaid = true;
						DriverPaidDate = DateTime.Now;
						RedemptionFee += this.Affiliate.AffiliateDriverDefaults.DriverInitiatedRedemptionFee;
					}
				}
			}
		}


		// **************************************************************************
		// Overload of default SetDriverPaid()
		// **************************************************************************
		public DriverPayments SetDriverPaid(DriverPayments pDriverPayments) {
			if (pDriverPayments.Test || pDriverPayments.Failed || ("" + this.CardType).Equals("TPCard", StringComparison.CurrentCultureIgnoreCase)) {
				return pDriverPayments;
			}
			if (pDriverPayments.Drivers.MyAffiliate != null
				&& (pDriverPayments.Drivers.MyAffiliate.AutoPayFleet
				|| (pDriverPayments.AffiliateDrivers.AutoPayManual && !pDriverPayments.CardSwiped)
				|| (pDriverPayments.AffiliateDrivers.AutoPaySwipe && pDriverPayments.CardSwiped))) {

				if (!pDriverPayments.Drivers.PaidByRedeemer && !pDriverPayments.Affiliate.DriverInitiatedPayCardPayment) {
					if (!pDriverPayments.AffiliateDrivers.RedeemerStopPayment) {
						DriverPaid = true;
						DriverPaidDate = DateTime.Now;
						RedemptionFee += pDriverPayments.Affiliate.AffiliateDriverDefaults.DriverInitiatedRedemptionFee;
					}
				}
			}
			return pDriverPayments;
		}



		// ************************************************************************************
		// Create a charge-back for the entire DriverTotal amount
		// ************************************************************************************
		public DriverPayments DoChargeBack(string pChargeBackReason) {
			Drivers oDriver = this.Drivers;
			DriverPayments oPayment = DriverPayments.Create(oDriver);
			oPayment.AffiliateID = this.AffiliateID;
			oPayment.AffiliateDriverID = this.AffiliateDriverID;

			//oPayment.TransNo = textBoxTransNo.Text;
			oPayment.FareNoTaxiPassFee = this.DriverTotal;
			oPayment.ChargeBackDate = DateTime.Now;
			oPayment.ChargeBackInfo = "Charge Back of TransNo: " + this.TransNo + "\n\n" + pChargeBackReason;
			oPayment.ChargeBack = true;
			oPayment.ChargeDate = DateTime.Now;
			oPayment.DriverPaid = true;
			oPayment.DriverPaidDate = DateTime.Now;
			oPayment.TaxiPassRedeemerID = this.TaxiPassRedeemerID;
			oPayment.Save();

			DriverPaymentAux oAux = DriverPaymentAux.Create(oPayment);
			oAux.ChargeBackDriverPaymentID = oPayment.DriverPaymentID;
			oAux.Save();

			this.ReceiptChargedBack = oPayment.TransNo;
			this.Save();

			return oPayment;
		}


		// ************************************************************************************
		// Create a charge-back for a partial amount
		// ************************************************************************************
		public DriverPayments DoChargeBack(string pChargeBackReason, decimal pAmount) {
			Drivers oDriver = this.Drivers;
			DriverPayments oPayment = DriverPayments.Create(oDriver);
			oPayment.AffiliateID = this.AffiliateID;
			oPayment.AffiliateDriverID = this.AffiliateDriverID;

			//oPayment.TransNo = textBoxTransNo.Text;
			oPayment.OriginalTransNo = this.TransNo;
			oPayment.FareNoTaxiPassFee = -Math.Abs(pAmount);
			oPayment.ChargeBackDate = DateTime.Now;
			oPayment.ChargeBackInfo = "Charge Back of TransNo: " + this.TransNo + "\n\n" + pChargeBackReason;
			oPayment.ChargeBack = true;
			oPayment.ChargeDate = DateTime.Now;
			oPayment.DriverPaid = true;
			oPayment.DriverPaidDate = DateTime.Now;
			oPayment.TaxiPassRedeemerID = this.TaxiPassRedeemerID;
			oPayment.Failed = false;
			oPayment.Platform = Platforms.CustServiceCreditCardChargeBack.ToString();
			oPayment.Save();

			DriverPaymentAux oAux = DriverPaymentAux.Create(oPayment);
			oAux.ChargeBackDriverPaymentID = oPayment.DriverPaymentID;
			oAux.Save();

			if (!this.ReceiptRequest.IsNullEntity) {
				this.ReceiptRequest.ChargeBackDueDate = DateTime.Today;
			}

			this.ReceiptChargedBack = oPayment.TransNo;
			this.Save();

			CreateChargeBackFee(oDriver, this, oPayment.TransNo);

			return oPayment;
		}

		public static DriverPayments CreateChargeBackFee(Drivers pDriver, DriverPayments pPayment, string pChargeBackTransNo) {
			DriverPayments chargeBackFee = null;
			if (pPayment.Affiliate.ChargeBackFee > 0) {
				chargeBackFee = DriverPayments.Create(pDriver);
				chargeBackFee.OriginalTransNo = pPayment.TransNo;
				chargeBackFee.AffiliateID = pPayment.AffiliateID;
				chargeBackFee.AffiliateDriverID = pPayment.AffiliateDriverID;

				//oPayment.TransNo = textBoxTransNo.Text;
				chargeBackFee.DriverFee = pPayment.Affiliate.ChargeBackFee;
				chargeBackFee.ChargeBackDate = DateTime.Now;
				chargeBackFee.ChargeBackInfo = $"Fee for Chargeback of Voucher: {pChargeBackTransNo} with original voucher {pPayment.TransNo}";
				chargeBackFee.ChargeBack = true;
				chargeBackFee.ChargeDate = DateTime.Now;
				chargeBackFee.DriverPaid = true;
				chargeBackFee.DriverPaidDate = DateTime.Now;
				chargeBackFee.TaxiPassRedeemerID = pPayment.TaxiPassRedeemerID;
				chargeBackFee.Failed = false;
				chargeBackFee.Platform = Platforms.ChargeBackFeeRevenue.ToString();
				chargeBackFee.Save();

				DriverPaymentAux chargeBackFeeAux = DriverPaymentAux.Create(chargeBackFee);
				chargeBackFeeAux.ChargeBackDriverPaymentID = pPayment.DriverPaymentID;
				chargeBackFeeAux.Save();
			}
			return chargeBackFee;
		}

		public decimal ProcessRedemptionFee(TaxiPassRedeemerAccounts pRedeemerAccount) {
			if (!this.CreditCardProcessor.IsNullOrEmpty()) {
				if (!this.CreditCardProcessor.Equals("DriverProm")) {
					RedeemerFees recFee = RedeemerFees.GetRedemeerFee(pRedeemerAccount, this.Platform);
					if (recFee.Percent) {
						this.RedemptionFee = Math.Round(this.DriverTotal * (recFee.Fee / 100), 2, MidpointRounding.AwayFromZero);
					} else {
						this.RedemptionFee = recFee.Fee;
					}
				}
			}
			return this.RedemptionFee.GetValueOrDefault(0);
		}
		#endregion

		#region Static Methods
		public static string CardNumberToHash(string pCardNumber) {
			return CryptoFns.MD5HashUTF16ToString(pCardNumber);
		}


		// ********************************************************************************************************
		// 4/23/2013 Tax the max from either Drivers or AffilaiteDrivers
		// removed "bool pCheckDriver" as an input argument
		// ********************************************************************************************************
		public static CardProcessingFailureTypes CanProcessCard(DriverPayments pDriverPayments) {
			decimal dMax;
			string sCardNumber = pDriverPayments.DecryptCardNumber();
			decimal totalAmount = pDriverPayments.DriverTotal;

			Drivers oDriver = pDriverPayments.Drivers;
			AffiliateDrivers oAffDriver = pDriverPayments.AffiliateDrivers;

			string sCardNo = CardNumberToHash(sCardNumber);

			PersistenceManager oPM = pDriverPayments.PersistenceManager;
			SystemDefaults oDefault = SystemDefaults.GetDefaults(oPM);

			if (DriverPromotions.GetPromotionsByCardNo(oPM, sCardNumber).Count > 0) {
				return CardProcessingFailureTypes.OKToProcess;
			}

			if (sCardNo.IsNullOrEmpty()) {
				return CardProcessingFailureTypes.MissingCardNumber;
			}


			TimeZoneConverter tzConvert = new TimeZoneConverter();
			DateTime now = tzConvert.ConvertTime(DateTime.Now, TimeZone.CurrentTimeZone.StandardName, pDriverPayments.Affiliate.TimeZone);
			bool bCheckDriver = true;
			if (oDriver.Name.Equals("Charge Card", StringComparison.CurrentCultureIgnoreCase) && pDriverPayments.Platform != Platforms.WebTerminal.ToString() && pDriverPayments.AffiliateDriverID.HasValue) {
				bCheckDriver = false;
			} else {
				if (!oDriver.Name.Equals("Charge Card", StringComparison.CurrentCultureIgnoreCase)) {
					bCheckDriver = true;
				}
			}
			usp_DriverPayments_FraudCheck fraudTotals = usp_DriverPayments_FraudCheck.ExecuteProc(pDriverPayments, bCheckDriver, now);

			if (oDefault.DriverMinTransTime != 0 && fraudTotals.LastChargeDate.HasValue) {
				TimeSpan oTime = now.Subtract(fraudTotals.LastChargeDate.Value);
				if (Math.Abs(oTime.TotalSeconds) < oDefault.DriverMinTransTime) {
					return CardProcessingFailureTypes.CardUsedTooSoon;
				}
			}


			if (fraudTotals.DailyTransCountPerCard >= oAffDriver.GetCardChargedMaxNumber) {
				return CardProcessingFailureTypes.MaxCardTransactions;
			}


			if (fraudTotals.DailyTransCount >= oAffDriver.GetMaxTrxPerDay) {
				return CardProcessingFailureTypes.MaxDriverTransactions;
			}

			dMax = oDriver.GetMaxDailyAmount;
			if (oAffDriver.GetMaxDailyAmount > dMax) {
				dMax = oAffDriver.GetMaxDailyAmount;
			}
			if (fraudTotals.DailyTransTotal > dMax) {
				return CardProcessingFailureTypes.MaxDailyAmount;
			}


			//Check Fraud Measures for Month
			dMax = oDriver.GetMaxTransPerMonth;
			if (oAffDriver.GetMaxTransPerMonth > dMax) {
				dMax = oAffDriver.GetMaxTransPerMonth;
			}
			if (fraudTotals.MonthTransCount > dMax) {
				return CardProcessingFailureTypes.MaxTransPerMonth;
			}

			dMax = oDriver.GetMaxTransPerMonthPerCard;
			if (oAffDriver.GetMaxTransPerMonthPerCard > dMax) {
				dMax = oAffDriver.GetMaxTransPerMonthPerCard;
			}
			if (fraudTotals.MonthTransCountPerCard > dMax) {
				return CardProcessingFailureTypes.MaxTransPerMonthPerCard;
			}

			dMax = oDriver.GetMaxMonthlyAmount;
			if (oAffDriver.GetMaxMonthlyAmount > dMax) {
				dMax = oAffDriver.GetMaxMonthlyAmount;
			}
			if (fraudTotals.MonthTotal + totalAmount > dMax) {
				return CardProcessingFailureTypes.MaxMonthlyAmount;
			}


			//Check Fraud Measures for Week
			dMax = oDriver.GetMaxTransPerWeek;
			if (oAffDriver.GetMaxTransPerWeek > dMax) {
				dMax = oAffDriver.GetMaxTransPerWeek;
			}
			if (fraudTotals.WeekTransCount > dMax) {
				return CardProcessingFailureTypes.MaxTransPerWeek;
			}

			dMax = oDriver.GetMaxTransPerWeekPerCard;
			if (oAffDriver.GetMaxTransPerWeekPerCard > dMax) {
				dMax = oAffDriver.GetMaxTransPerWeekPerCard;
			}
			if (fraudTotals.WeekTransCountPerCard > dMax) {
				return CardProcessingFailureTypes.MaxTransPerWeekPerCard;
			}

			dMax = oDriver.GetMaxWeeklyAmount;
			if (oAffDriver.GetMaxWeeklyAmount > dMax) {
				dMax = oAffDriver.GetMaxWeeklyAmount;
			}
			if (fraudTotals.WeekTotal + totalAmount > dMax) {
				return CardProcessingFailureTypes.MaxWeeklyAmount;
			}

			decimal maxSwipeAmount = oDriver.GetChargeMaxSwipeAmount;
			dMax = oDriver.GetChargeMaxAmount;
			if (oAffDriver.GetChargeMaxAmount > dMax) {
				dMax = oAffDriver.GetChargeMaxAmount;
			}

			//allow exception charges to have same max charge as swipe amount
			if (maxSwipeAmount > 0 && (pDriverPayments.CardSwiped || pDriverPayments.ExceptionCharge)) {
				if (totalAmount > maxSwipeAmount) {
					return CardProcessingFailureTypes.MaxChargeAmountExceeded;
				}
			} else {
				if (totalAmount > dMax) {
					return CardProcessingFailureTypes.MaxChargeAmountExceeded;
				}
			}

			using (var conn = CabRideEngineDapper.Utils.SqlHelper.OpenSqlConnection()) {
				usp_TotalChargesPerCardAndAffiliate cardTotals = usp_TotalChargesPerCardAndAffiliate.Execute(conn, pDriverPayments.AffiliateID.Value, now, pDriverPayments.Number);
				if (cardTotals.MaxDayAmount > 0 && totalAmount + cardTotals.TotalDay > cardTotals.MaxDayAmount) {
					return CardProcessingFailureTypes.MaxDayAmountPerCardExceeded;
				}
				if (cardTotals.MaxWeekAmount > 0 && totalAmount + cardTotals.TotalWeek > cardTotals.MaxWeekAmount) {
					return CardProcessingFailureTypes.MaxWeekAmountPerCardExceeded;
				}

				dMax = cardTotals.MaxMonthAmount;
				if (oAffDriver.AffiliateDriverFraudDefaults.MaxChargePerMonthPerCard > dMax) {
					dMax = oAffDriver.AffiliateDriverFraudDefaults.MaxChargePerMonthPerCard;
				}

				if (dMax > 0 && totalAmount + cardTotals.TotalMonth > dMax) {
					return CardProcessingFailureTypes.MaxMonthAmountPerCardExceeded;
				}
			}

			return CardProcessingFailureTypes.OKToProcess;
		}




		/// <summary>
		/// Max number of transactions reached.
		/// </summary>
		/// <param name="pDriver">The driver.</param>
		/// <returns></returns>
		public static bool MaxTransactionsReached(Drivers pDriver) {

			PersistenceManager oPM = pDriver.PersistenceManager;

			DateTime dStart = DateTime.Now.AddHours(-24);
			TimeZoneConverter convert = new TimeZoneConverter();
			dStart = convert.ConvertTime(dStart, TimeZone.CurrentTimeZone.StandardName, pDriver.MyAffiliate.TimeZone);

			RdbQuery qry = new RdbQuery(typeof(DriverPayments), DriverIDEntityColumn, EntityQueryOp.EQ, pDriver.DriverID);
			qry.AddClause(ChargeDateEntityColumn, EntityQueryOp.GE, dStart);
			qry.AddClause(FailedEntityColumn, EntityQueryOp.EQ, false);
			qry.AddClause(TestEntityColumn, EntityQueryOp.EQ, false);
			int iCount = oPM.GetEntities<DriverPayments>(qry).Count;

			return (iCount >= pDriver.GetMaxTrxPerDay);
		}


		/// <summary>
		/// Max number of transactions reached.
		/// </summary>
		/// <param name="pDriver">The p driver.</param>
		/// <param name="pAffDriver">The p aff driver.</param>
		/// <returns></returns>
		public static bool MaxTransactionsReached(Drivers pDriver, AffiliateDrivers pAffDriver) {

			PersistenceManager oPM = pDriver.PersistenceManager;

			DateTime dStart = DateTime.Now.AddHours(-24);
			TimeZoneConverter convert = new TimeZoneConverter();
			dStart = convert.ConvertTime(dStart, TimeZone.CurrentTimeZone.StandardName, pAffDriver.Affiliate.TimeZone);

			RdbQuery qry = new RdbQuery(typeof(DriverPayments), DriverIDEntityColumn, EntityQueryOp.EQ, pDriver.DriverID);
			qry.AddClause(DriverPayments.AffiliateDriverIDEntityColumn, EntityQueryOp.EQ, pAffDriver.AffiliateDriverID);
			qry.AddClause(DriverPayments.ChargeDateEntityColumn, EntityQueryOp.GE, dStart);
			qry.AddClause(TestEntityColumn, EntityQueryOp.EQ, false);
			qry.AddClause(FailedEntityColumn, EntityQueryOp.EQ, false);
			int iCount = oPM.GetEntities<DriverPayments>(qry).Count;

			return (iCount >= pDriver.GetMaxTrxPerDay);
		}




		/// <summary>
		/// Convert Payment to TTech transaction object.
		/// </summary>
		/// <returns></returns>
		public TTechTransaction PaymentToTTechTransaction() {
			return PaymentToTTechTransaction(0);
		}


		/// <summary>
		/// Convert Payment to TTech transaction object.
		/// </summary>
		/// <param name="pLaneID">The lane ID.</param>
		/// <returns></returns>
		public TTechTransaction PaymentToTTechTransaction(short pLaneID) {
			return PaymentToTTechTransaction(pLaneID, false);
		}


		/// <summary>
		/// Convert Payment to TTech transaction object.
		/// </summary>
		/// <param name="pLaneID">The p lane ID.</param>
		/// <param name="pBofA">if set to <c>true</c> [p bof A].</param>
		/// <returns></returns>
		public bool TTechDriverFeeChargeBack { get; set; }


		public TTechTransaction PaymentToTTechTransaction(short pLaneID, bool pBofA) {
			DriverPayments oPayment = this;
			SystemDefaults oDefault = SystemDefaults.GetDefaults(this.PersistenceManager);
			TTechTransaction oTTech = new TTechTransaction();
			PayInfo oPayInfo = null;

			bool payDriver = !oPayment.TaxiPassRedeemerID.HasValue
							|| oPayment.TaxiPassRedeemerID.Value < 1
							|| oPayment.TaxiPassRedeemers.TaxiPassRedeemerAccounts.PayDriverPayCard && !oPayment.DriverPaidCash.PaidInCash;

			//check if redeemer has payment info, if not then pay driver
			if (!payDriver) {
				if (oPayment.TaxiPassRedeemerID.HasValue) {
					TaxiPassRedeemerAccounts acct = oPayment.TaxiPassRedeemers.TaxiPassRedeemerAccounts;
					if (acct.BofAAccountInfo.IsNullOrEmpty() && acct.BankAccountNumber.IsNullOrEmpty() && acct.TransCardNumber.IsNullOrEmpty()) {
						payDriver = true;
					}
				}
			}
			if (payDriver) {
				if (oPayment.AffiliateDriverID > 0) {
					try {
						oPayInfo = oPayment.AffiliateDrivers.GetPayInfo();
					} catch (Exception ex) {
						Console.WriteLine(ex.Message);
					}
					//while (true) {
					//	System.Windows.Forms.Application.DoEvents();
					//}

				}
				if (oPayInfo == null) {
					if (oPayment.Drivers.DriverBankInfos.Count > 0 && CheckAccount(oPayment.Drivers.DriverBankInfos[0])) {
						oPayInfo = oPayment.Drivers.DriverBankInfos[0].GetPayInfo();
					}
				}
			}
			if (oPayInfo == null) {
				//do we pay redeemer or affiliate?
				if (!pBofA) {
					if (oPayment.TaxiPassRedeemerID.HasValue && oPayment.TaxiPassRedeemerID.Value > 0) {
						if (!oPayment.DriverPaymentsPendingMatches.IsNullEntity && oPayment.DriverPaymentsPendingMatches.TaxiPassRedeemerID != 0) {
							oPayInfo = oPayment.DriverPaymentsPendingMatches.TaxiPassRedeemers.TaxiPassRedeemerAccounts.GetPayInfo();
						} else {
							oPayInfo = oPayment.TaxiPassRedeemers.TaxiPassRedeemerAccounts.GetPayInfo();
						}
					} else {
						if (!oPayment.AffiliateID.HasValue) {
							oPayment.AffiliateID = oPayment.Drivers.AffiliateID;
						}
						if (oPayment.Affiliate.AffiliateBankInfos.Count > 0) {
							oPayInfo = oPayment.Affiliate.AffiliateBankInfos[0].GetPayInfo();
						}
					}
				}
			}

			if ((!pBofA && oPayInfo == null) || !oPayment.DriverPaid) {
				return null;
			}


			if (!payDriver && oPayment.TaxiPassRedeemerID.HasValue && oPayment.TaxiPassRedeemerID.Value > 0 &&
					(!oPayment.TaxiPassRedeemers.TaxiPassRedeemerAccounts.PayDriverPayCard || !oPayment.TaxiPassRedeemers.TaxiPassRedeemerAccounts.BofAAccountInfo.IsNullOrEmpty())) {
				if (oPayment.DriverPaymentsPendingMatches.IsNullEntity) {
					oTTech.Affiliate = "Redeemer: " + oPayment.TaxiPassRedeemers.TaxiPassRedeemerAccounts.CompanyName;
				} else {
					oTTech.Affiliate = "Redeemer: " + oPayment.DriverPaymentsPendingMatches.TaxiPassRedeemers.TaxiPassRedeemerAccounts.CompanyName;
				}
			} else {
				oTTech.Affiliate = oPayment.Drivers.MyAffiliate.Name;
			}

			//oTTech.NachaFile = oPay.NachaFile;
			oTTech.Site = oDefault.TTechSite;
			oTTech.Action = TTechAction.Normal;

			oTTech.TranID = oPayment.DriverPaymentID;

			if (oPayment.ChargeBack || TTechDriverFeeChargeBack) {
				oTTech.LaneID = pLaneID++;
			} else {
				oTTech.LaneID = pLaneID;  // oPay.MyMerchant.LaneID;
			}
			if (oTTech.Affiliate.StartsWith("Redeemer:")) {
				oTTech.Amount = oPayment.RedeemerTotalPaid;
			} else {

				// 3/7/12 CA ACHDriverFeeAsDebit is for Carmel 
				// TTechDriverFeeChargeBack is set in CabRideAutoACH after the transactions are processed
				if (oPayment.Affiliate.AffiliateDriverDefaults.ACHDriverFeeAsDebit) {
					if (TTechDriverFeeChargeBack) {
						oTTech.Amount = oPayment.DriverFee.GetValueOrDefault(0);
					} else {
						// Take whatever is set in the DB.  Carmel rolls their chargebacks into the "Credit" line
						oTTech.Amount = oPayment.DriverTotal;
					}
				} else {
					//oTTech.Amount = oPayment.DriverTotalPaid;
					oTTech.Amount = Math.Abs(oPayment.DriverTotalPaid);
				}
			}

			if (oPayment.DriverPaidDate.HasValue) {
				oTTech.TranDate = oPayment.DriverPaidDate.Value;
			} else {
				oTTech.TranDate = DateTime.Today;
			}
			oTTech.CheckNumber = oTTech.TranID;  //?
			if (TTechDriverFeeChargeBack) {
				oTTech.CheckNumber = Convert.ToInt64("999" + Math.Abs(oTTech.CheckNumber).ToString());
			}
			if (!pBofA) {
				oTTech.PaidTo = oPayInfo.PaidTo;
				oTTech.CustomerName = oPayInfo.BankAccountHolder;
				oTTech.Address = oPayInfo.BankAddress;
				oTTech.Suite = oPayInfo.BankSuite;
				oTTech.City = oPayInfo.BankCity;
				oTTech.State = oPayInfo.BankState;
				oTTech.ZIPCode = oPayInfo.BankZIPCode;
				try {
					oTTech.Phone1 = oPayment.AffiliateDrivers.Cell.IsNullOrEmpty() ? oPayment.Drivers.Cell : oPayment.AffiliateDrivers.Cell;
				} catch (Exception) {
					oTTech.Phone1 = "CELL";
				}
				if (oTTech.Phone1.Trim().Equals("CELL", StringComparison.CurrentCultureIgnoreCase)) {
					oTTech.Phone1 = oPayment.Affiliate.CorporatePhone;
				}
				oTTech.Company = oPayInfo.BankAccountPersonal ? "" : oPayment.Drivers.MyAffiliate.Name;
				oTTech.CheckType = oPayInfo.BankAccountPersonal ? "PERSONAL" : "BUSINESS";
				oTTech.ABANumber = oPayInfo.BankCode;
				oTTech.Account = oPayInfo.BankAccountNumber;
				oTTech.ClassCode = oPayInfo.BankAccountPersonal ? TaxiPassCommon.Banking.NachaGenerator.StandardEntryTypes.PPD.ToString() :
															  TaxiPassCommon.Banking.NachaGenerator.StandardEntryTypes.CCD.ToString();
			}

			if ((oPayment.ChargeBack && !oPayment.Affiliate.AffiliateDriverDefaults.ACHDriverFeeAsDebit) || TTechDriverFeeChargeBack) {
				oTTech.TranType = "DEBIT";
				oTTech.Application = "DDA";
				oTTech.PaymentType = "S";
			} else {
				oTTech.TranType = "CREDIT";
				oTTech.Application = "DDA";
				oTTech.PaymentType = "S";
				if (oPayInfo != null) {
					oTTech.WireTransfer = oPayInfo.WireTransfer;
				}
			}
			oTTech.TranReference = oPayment.DriverPaymentID.ToString();

			return oTTech;
		}

		private bool CheckAccount(DriverBankInfo pBankInfo) {
			if (string.IsNullOrEmpty(pBankInfo.MerchantInfo.MerchantNo)) {
				return false;
			}
			if (string.IsNullOrEmpty(pBankInfo.BankAccountNumber)) {
				return false;
			}
			if (string.IsNullOrEmpty(pBankInfo.BankCode)) {
				return false;
			}
			return true;
		}


		// **********************************************************************************************************
		// Returns total ACHPayments for today for a given card
		// **********************************************************************************************************
		public static decimal GetTodaysAchPaidAmount(TaxiPassRedeemerAccounts pRedeemerAcct, string sCardNo) {
			StringBuilder sql = new StringBuilder();
			sql.Append("select SUM(Fare + Gratuity + AirportFee + MiscFee - Discount) from DriverPayments  WITH (NOLOCK)");
			sql.Append("left join ACHDetail on ACHDetail.DriverPaymentID = DriverPayments.DriverPaymentID ");
			sql.Append("where ACHPaidDate >= '");
			sql.Append(DateTime.Today);
			sql.Append("' and ACHPaidDate < '");
			sql.Append(DateTime.Today.AddDays(1));
			sql.Append("' and ACHBankAccountNumber = 'TransCard: ");
			sql.Append(sCardNo.Right(5));
			sql.Append("' and ");
			sql.Append("TaxiPassRedeemerID in (Select TaxiPassRedeemerID from TaxiPassRedeemers where TaxiPassRedeemerAccountID = ");
			sql.Append(pRedeemerAcct.TaxiPassRedeemerAccountID);
			sql.Append(")");

			DataTable oDT = SQLHelper.GetRecords(pRedeemerAcct.PersistenceManager, sql.ToString());
			if (oDT.Rows.Count == 0) {
				return 0;
			}
			if (oDT.Rows[0][0].GetType() == typeof(DBNull)) {
				return 0;
			}
			return Convert.ToDecimal(oDT.Rows[0][0]);
		}



		#endregion

		#region Retrieve Records

		// ********************************************************************************
		// 
		// ********************************************************************************
		public static EntityList<DriverPayments> GetPendingTransactions(Drivers pDriver) {
			return GetPendingTransactions(pDriver, true);
		}


		// ********************************************************************************
		// 
		// ********************************************************************************
		public static EntityList<DriverPayments> GetPendingTransactions(Drivers pDriver, bool pIncludeCorpAccounts) {
			return GetPendingTransactions(pDriver, pIncludeCorpAccounts, true);
		}

		public static EntityList<DriverPayments> GetPendingTransactions(Drivers pDriver, bool pIncludeCorpAccounts, bool pAllDates) {
			RdbQuery qry = new RdbQuery(typeof(DriverPayments), FareEntityColumn, EntityQueryOp.EQ, 0);
			if (!pAllDates) {
				qry.AddClause(DriverPayments.AuthDateEntityColumn, EntityQueryOp.GE, DateTime.Today.AddDays(-30));
			}
			if (!pIncludeCorpAccounts) {
				qry.AddClause(DriverPayments.CorporateNameEntityColumn, EntityQueryOp.IsNull, true);
				qry.AddClause(DriverPayments.CorporateNameEntityColumn, EntityQueryOp.EQ, "");
				qry.AddOperator(EntityBooleanOp.Or);
			}
			qry.AddClause(DriverPayments.FailedEntityColumn, EntityQueryOp.EQ, false);
			qry.AddClause(DriverIDEntityColumn, EntityQueryOp.EQ, pDriver.DriverID);

			return pDriver.PersistenceManager.GetEntities<DriverPayments>(qry);
		}

		/// <summary>
		/// Gets the pending transactions.
		/// </summary>
		/// <param name="pAffiliate">The affiliate.</param>
		/// <returns></returns>
		public static EntityList<DriverPayments> GetPendingTransactions(Affiliate pAffiliate) {
			return GetPendingTransactions(pAffiliate, true);
		}

		/// <summary>
		/// Gets the pending transactions.
		/// </summary>
		/// <param name="pAffiliate">The affiliate.</param>
		/// <param name="pIncludeCorpAccounts">if set to <c>true</c> [include corp accounts].</param>
		/// <returns></returns>
		public static EntityList<DriverPayments> GetPendingTransactions(Affiliate pAffiliate, bool pIncludeCorpAccounts) {
			return GetPendingTransactions(pAffiliate, pIncludeCorpAccounts, true);
		}
		public static EntityList<DriverPayments> GetPendingTransactions(Affiliate pAffiliate, bool pIncludeCorpAccounts, bool pAllDates) {
			RdbQuery qry = new RdbQuery(typeof(DriverPayments), FareEntityColumn, EntityQueryOp.EQ, 0);
			if (!pAllDates) {
				qry.AddClause(DriverPayments.AuthDateEntityColumn, EntityQueryOp.GE, DateTime.Today.AddDays(-30));
			}
			if (!pIncludeCorpAccounts) {
				qry.AddClause(DriverPayments.CorporateNameEntityColumn, EntityQueryOp.IsNull, true);
				qry.AddClause(DriverPayments.CorporateNameEntityColumn, EntityQueryOp.EQ, "");
				qry.AddOperator(EntityBooleanOp.Or);
			}
			qry.AddClause(DriverPayments.FailedEntityColumn, EntityQueryOp.EQ, false);
			qry.AddClause(DriverPayments.TestEntityColumn, EntityQueryOp.EQ, false);
			qry.AddSpan(EntityRelations.Drivers_DriverPayments);
			qry.AddClause(Drivers.AffiliateIDEntityColumn, EntityQueryOp.EQ, pAffiliate.AffiliateID);


			return pAffiliate.PersistenceManager.GetEntities<DriverPayments>(qry);
		}


		/// <summary>
		/// Gets the payment.
		/// </summary>
		/// <param name="pPM">The PM.</param>
		/// <param name="pDriverPaymentID">The driver payment ID.</param>
		/// <returns></returns>
		public static DriverPayments GetPayment(PersistenceManager pPM, long pDriverPaymentID) {
			DriverPayments oDP = pPM.GetNullEntity<DriverPayments>();
			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(DriverPayments), "usp_DriverPayments_Get @DriverPaymentID = " + pDriverPaymentID.ToString());
			EntityList<DriverPayments> oData = pPM.GetEntities<DriverPayments>(qry);
			if (oData.Count > 0) {
				oDP = oData[0];
			}
			return oDP;

			//RdbQuery qry = new RdbQuery(typeof(DriverPayments), DriverPaymentIDEntityColumn, EntityQueryOp.EQ, pDriverPaymentID);
			//return pPM.GetEntity<DriverPayments>(qry);
		}

		/// <summary>
		/// Gets the pre auth record.
		/// </summary>
		/// <param name="pDriver">The driver.</param>
		/// <param name="pCardNumber">The card number.</param>
		/// <returns></returns>
		public static DriverPayments GetPreAuthRecord(Drivers pDriver, string pCardNumber) {
			string sCardNo = CardNumberToHash(pCardNumber);

			RdbQuery qry = new RdbQuery(typeof(DriverPayments), DriverIDEntityColumn, EntityQueryOp.EQ, pDriver.DriverID);
			qry.AddClause(NumberEntityColumn, EntityQueryOp.EQ, sCardNo);
			qry.AddClause(FailedEntityColumn, EntityQueryOp.EQ, false);
			qry.AddClause(FareEntityColumn, EntityQueryOp.EQ, 0);
			PersistenceManager oPM = pDriver.PersistenceManager;
			EntityList<DriverPayments> oList = oPM.GetEntities<DriverPayments>(qry);
			foreach (DriverPayments oPay in oList) {
				if (oPay.DecryptCardNumber() == pCardNumber) {
					return oPay;
				}
			}
			return oPM.GetNullEntity<DriverPayments>();
		}

		/// <summary>
		/// Gets the pre auth record.
		/// </summary>
		/// <param name="pDriver">The driver.</param>
		/// <param name="pCardNumber">The card number.</param>
		/// <returns></returns>
		public static DriverPayments GetPreAuthRecord(AffiliateDrivers pDriver, string pCardNumber) {
			string sCardNo = CardNumberToHash(pCardNumber);

			RdbQuery qry = new RdbQuery(typeof(DriverPayments), AffiliateDriverIDEntityColumn, EntityQueryOp.EQ, pDriver.AffiliateDriverID);
			qry.AddClause(NumberEntityColumn, EntityQueryOp.EQ, sCardNo);
			qry.AddClause(FailedEntityColumn, EntityQueryOp.EQ, false);
			qry.AddClause(FareEntityColumn, EntityQueryOp.EQ, 0);
			PersistenceManager oPM = pDriver.PersistenceManager;
			EntityList<DriverPayments> oList = oPM.GetEntities<DriverPayments>(qry);
			foreach (DriverPayments oPay in oList) {
				if (oPay.DecryptCardNumber() == pCardNumber) {
					return oPay;
				}
			}
			return oPM.GetNullEntity<DriverPayments>();
		}


		/// <summary>
		/// Gets the charges.
		/// </summary>
		/// <param name="pDriver">The driver.</param>
		/// <param name="pStartDate">The start date.</param>
		/// <param name="pIncludeFailed">if set to <c>true</c> [include failed].</param>
		/// <returns></returns>
		public static EntityList<DriverPayments> GetCharges(Drivers pDriver, DateTime pStartDate, bool pIncludeFailed) {
			PersistenceManager oPM = pDriver.PersistenceManager;
			RdbQuery qry = new RdbQuery(typeof(DriverPayments), DriverIDEntityColumn, EntityQueryOp.EQ, pDriver.DriverID);
			qry.AddClause(DriverPayments.ChargeDateEntityColumn, EntityQueryOp.GE, pStartDate.Date);
			qry.AddClause(FareEntityColumn, EntityQueryOp.NE, 0);
			if (!pIncludeFailed) {
				qry.AddClause(FailedEntityColumn, EntityQueryOp.EQ, false);
			}
			qry.AddOrderBy(ChargeDateEntityColumn, System.ComponentModel.ListSortDirection.Descending);
			return oPM.GetEntities<DriverPayments>(qry);
		}

		/// <summary>
		/// Gets the charges.
		/// </summary>
		/// <param name="pDriver">The driver.</param>
		/// <param name="pStartDate">The start date.</param>
		/// <param name="pIncludeFailed">if set to <c>true</c> [include failed].</param>
		/// <returns></returns>
		public static EntityList<DriverPayments> GetCharges(AffiliateDrivers pDriver, DateTime pStartDate, bool pIncludeFailed) {
			PersistenceManager oPM = pDriver.PersistenceManager;
			RdbQuery qry = new RdbQuery(typeof(DriverPayments), DriverPayments.AffiliateDriverIDEntityColumn, EntityQueryOp.EQ, pDriver.AffiliateDriverID);
			qry.AddClause(DriverPayments.ChargeDateEntityColumn, EntityQueryOp.GE, pStartDate.Date);
			qry.AddClause(FareEntityColumn, EntityQueryOp.NE, 0);
			if (!pIncludeFailed) {
				qry.AddClause(FailedEntityColumn, EntityQueryOp.EQ, false);
			}
			qry.AddOrderBy(ChargeDateEntityColumn, System.ComponentModel.ListSortDirection.Descending);
			return oPM.GetEntities<DriverPayments>(qry);
		}


		// **********************************************************************************************
		// Get Charges for either an Aff Driver or Driver
		// **********************************************************************************************
		public static EntityList<DriverPayments> GetChargesForDriver(PersistenceManager pPM, long pDriverID, long pAffiliateDriverID) {
			StringBuilder sql = new StringBuilder();
			if (pDriverID + pAffiliateDriverID != 0) {
				sql.AppendFormat("usp_DriverPaymentsGetChargesForDriver @DriverID = {0}, @AffiliateDriverID = {1}", pDriverID, pAffiliateDriverID);
			} else {
				sql.Append("Select null as DriverPaymentID");
			}

			PassthruRdbQuery qryPass = new PassthruRdbQuery(typeof(DriverPayments), sql.ToString());
			EntityList<DriverPayments> oCharges = pPM.GetEntities<DriverPayments>(qryPass);
			return oCharges;
		}

		// **********************************************************************************************
		// Charge Count for Promos by AffiliateDriver
		// **********************************************************************************************
		public static long GetChargeCountForPromos(AffiliateDrivers pDriver, DateTime pStartDate) {
			return GetChargeCountForPromos(pDriver, pStartDate, false);
		}

		/// <summary>
		/// Gets the charge count for promos.
		/// </summary>
		/// <param name="pDriver">The driver.</param>
		/// <param name="pStartDate">The start date.</param>
		/// <param name="pSwipeOnly">if set to <c>true</c> [swipe only].</param>
		/// <returns></returns>
		public static long GetChargeCountForPromos(AffiliateDrivers pDriver, DateTime pStartDate, bool pSwipeOnly) {
			StringBuilder sql = new StringBuilder();
			sql.Append("select COUNT(DriverPaymentID) from DriverPayments  WITH (NOLOCK)");
			sql.Append(String.Format(" where AffiliateDriverID = {0}", pDriver.AffiliateDriverID));
			sql.Append(String.Format(" and ChargeDate >= '{0}'", pStartDate.ToShortDateString()));
			sql.Append(" and Failed = 0 and Test = 0 and Fare > 0");
			if (pSwipeOnly) {
				sql.Append(" and CardSwiped = 1");
			}
			sql.Append(" and (CreditCardProcessor != 'DriverProm' or CreditCardProcessor is null) ");
			DataTable table = SQLHelper.GetRecords(pDriver.PersistenceManager, sql.ToString());
			return Convert.ToInt64(table.Rows[0][0]);
		}

		// **********************************************************************************************
		// Charge Count for Promos by Driver
		// **********************************************************************************************
		public static long GetChargeCountForPromos(Drivers pDriver, DateTime pStartDate) {
			return GetChargeCountForPromos(pDriver, pStartDate, false);
		}

		/// <summary>
		/// Gets the charge count for promos.
		/// </summary>
		/// <param name="pDriver">The driver.</param>
		/// <param name="pStartDate">The start date.</param>
		/// <param name="pSwipeOnly">if set to <c>true</c> [swipe only].</param>
		/// <returns></returns>
		public static long GetChargeCountForPromos(Drivers pDriver, DateTime pStartDate, bool pSwipeOnly) {
			StringBuilder sql = new StringBuilder();
			sql.Append("select COUNT(DriverPaymentID) from DriverPayments  WITH (NOLOCK)");
			sql.Append(String.Format(" where DriverID = {0} AND AffiliateDriverID is null", pDriver.DriverID));
			sql.Append(String.Format(" and ChargeDate >= '{0}'", pStartDate.ToShortDateString()));
			sql.Append(" and Failed = 0 and Test = 0 and Fare > 0");
			if (pSwipeOnly) {
				sql.Append(" and CardSwiped = 1");
			}
			sql.Append(" and (CreditCardProcessor != 'DriverProm' or CreditCardProcessor is null) ");
			DataTable table = SQLHelper.GetRecords(pDriver.PersistenceManager, sql.ToString());
			return Convert.ToInt64(table.Rows[0][0]);
		}

		public static EntityList<DriverPayments> GetCharges(Drivers pDriver, DateTime pStartDate, DateTime pEndDate, bool pIncludeFailed) {
			PersistenceManager oPM = pDriver.PersistenceManager;
			RdbQuery qry = new RdbQuery(typeof(DriverPayments), DriverIDEntityColumn, EntityQueryOp.EQ, pDriver.DriverID);
			qry.AddClause(DriverPayments.ChargeDateEntityColumn, EntityQueryOp.GE, pStartDate.Date);
			qry.AddClause(DriverPayments.ChargeDateEntityColumn, EntityQueryOp.LE, pEndDate.Date);
			qry.AddClause(FareEntityColumn, EntityQueryOp.NE, 0);
			if (!pIncludeFailed) {
				qry.AddClause(FailedEntityColumn, EntityQueryOp.EQ, false);
			}
			qry.AddOrderBy(ChargeDateEntityColumn, System.ComponentModel.ListSortDirection.Descending);
			return oPM.GetEntities<DriverPayments>(qry);
		}

		public static EntityList<DriverPayments> GetCharges(AffiliateDrivers pDriver, DateTime pStartDate, DateTime pEndDate, bool pIncludeFailed) {
			PersistenceManager oPM = pDriver.PersistenceManager;
			RdbQuery qry = new RdbQuery(typeof(DriverPayments), DriverPayments.AffiliateDriverIDEntityColumn, EntityQueryOp.EQ, pDriver.AffiliateDriverID);
			qry.AddClause(DriverPayments.ChargeDateEntityColumn, EntityQueryOp.GE, pStartDate.Date);
			qry.AddClause(DriverPayments.ChargeDateEntityColumn, EntityQueryOp.LE, pEndDate);
			qry.AddClause(FareEntityColumn, EntityQueryOp.NE, 0);
			if (!pIncludeFailed) {
				qry.AddClause(FailedEntityColumn, EntityQueryOp.EQ, false);
			}
			qry.AddOrderBy(ChargeDateEntityColumn, System.ComponentModel.ListSortDirection.Descending);
			return oPM.GetEntities<DriverPayments>(qry);
		}

		public static EntityList<DriverPayments> GetCharges(PersistenceManager pPM, DateTime pStartDate, DateTime pEndDate, bool pIncludeFailed) {
			RdbQuery qry = new RdbQuery(typeof(DriverPayments));
			qry.AddClause(DriverPayments.ChargeDateEntityColumn, EntityQueryOp.GE, pStartDate);
			qry.AddClause(DriverPayments.ChargeDateEntityColumn, EntityQueryOp.LE, pEndDate);
			qry.AddClause(FareEntityColumn, EntityQueryOp.NE, 0);
			if (!pIncludeFailed) {
				qry.AddClause(FailedEntityColumn, EntityQueryOp.EQ, false);
			}
			qry.AddOrderBy(ChargeDateEntityColumn, System.ComponentModel.ListSortDirection.Descending);
			return pPM.GetEntities<DriverPayments>(qry);
		}

		public static EntityList<DriverPayments> GetPayments(Drivers pDriver, DateTime pStart, DateTime pEnd, DriverPaymentStatus pStatus) {
			RdbQuery qry = new RdbQuery(typeof(DriverPayments), ChargeDateEntityColumn, EntityQueryOp.GE, pStart);
			qry.AddClause(ChargeDateEntityColumn, EntityQueryOp.LE, pEnd);
			qry.AddClause(DriverIDEntityColumn, EntityQueryOp.EQ, pDriver.DriverID);
			switch (pStatus) {
				case DriverPaymentStatus.paid:
					qry.AddClause(DriverPaidEntityColumn, EntityQueryOp.EQ, true);
					break;
				case DriverPaymentStatus.unpaid:
					qry.AddClause(DriverPaidEntityColumn, EntityQueryOp.EQ, false);
					break;
			}

			return pDriver.PersistenceManager.GetEntities<DriverPayments>(qry);
		}

		public static EntityList<DriverPayments> GetPayments(AffiliateDrivers pAffDriver, DateTime pStart, DateTime pEnd, DriverPaymentStatus pStatus) {
			RdbQuery qry = new RdbQuery(typeof(DriverPayments), ChargeDateEntityColumn, EntityQueryOp.GE, pStart);
			qry.AddClause(ChargeDateEntityColumn, EntityQueryOp.LE, pEnd);
			qry.AddClause(DriverPayments.AffiliateDriverIDEntityColumn, EntityQueryOp.EQ, pAffDriver.AffiliateDriverID);
			switch (pStatus) {
				case DriverPaymentStatus.paid:
					qry.AddClause(DriverPaidEntityColumn, EntityQueryOp.EQ, true);
					break;
				case DriverPaymentStatus.unpaid:
					qry.AddClause(DriverPaidEntityColumn, EntityQueryOp.EQ, false);
					break;
			}

			return pAffDriver.PersistenceManager.GetEntities<DriverPayments>(qry);
		}


		// **********************************************************************************************
		//
		// **********************************************************************************************
		public static EntityList<DriverPayments> GetPayments(TaxiPassRedeemers pRedeemer, DateTime pStart, DateTime pEnd) {
			RdbQuery qry = new RdbQuery(typeof(DriverPayments), DriverPaidDateEntityColumn, EntityQueryOp.GE, pStart);
			qry.AddClause(DriverPaidDateEntityColumn, EntityQueryOp.LE, pEnd);
			qry.AddClause(DriverPaidEntityColumn, EntityQueryOp.EQ, true);
			List<long> redeemers = new List<long>();
			if (pRedeemer.Admin) {
				redeemers = (from o in pRedeemer.TaxiPassRedeemerAccounts.TaxiPassRedeemerses
							 select o.TaxiPassRedeemerID).ToList();
				qry.AddClause(DriverPayments.TaxiPassRedeemerIDEntityColumn, EntityQueryOp.In, redeemers);
			} else {
				qry.AddClause(DriverPayments.TaxiPassRedeemerIDEntityColumn, EntityQueryOp.EQ, pRedeemer.TaxiPassRedeemerID);
			}
			qry.AddOrderBy(DriverPayments.DriverPaidDateEntityColumn);

			EntityList<DriverPayments> oList = pRedeemer.PersistenceManager.GetEntities<DriverPayments>(qry);

			//add pending matches if any
			StringBuilder sql = new StringBuilder();
			sql.Append("select DriverPayments.* from DriverPayments  WITH (NOLOCK)");
			sql.Append(" left join DriverPaymentsPendingMatches on DriverPaymentsPendingMatches.DriverPaymentID = DriverPayments.DriverPaymentID");
			sql.Append("where DriverPaymentsPendingMatches.TaxiPassRedeemerID ");
			if (redeemers.Count == 0) {
				sql.Append(" = ");
				sql.Append("pRedeemer.TaxiPassRedeemerID.ToString()");
			} else {
				sql.Append("in (");
				int intCount = 0;
				foreach (long id in redeemers) {
					intCount++;
					sql.Append(id);
					if (intCount < redeemers.Count) {
						sql.Append(",");
					}

				}
			}
			sql.Append(" and DriverPayments.DriverPaidDate >= '");
			sql.Append(pStart);
			sql.Append("' and " + "DriverPayments.DriverPaidDate <= '");
			sql.Append(pEnd);
			sql.Append("' ");
			sql.Append("Order By DriverPayments.DriverPaidDate");
			PassthruRdbQuery qryPass = new PassthruRdbQuery(typeof(DriverPayments), sql.ToString());
			EntityList<DriverPayments> matchList = pRedeemer.PersistenceManager.GetEntities<DriverPayments>(qryPass);
			oList.AddRange(matchList);

			oList.ApplySort(DriverPaidDateEntityColumn.ColumnName, System.ComponentModel.ListSortDirection.Ascending, true);
			return oList;
		}

		public static DriverPayments GetPayment(PersistenceManager pPM, string pTransNo, decimal pAmount) {
			return GetPayment(pPM, pTransNo, pAmount, false);
		}

		public static DriverPayments GetPayment(PersistenceManager pPM, string pTransNo, decimal pAmount, bool pIgnoreAmount) {
			return GetPayment(pPM, pTransNo, pAmount, pIgnoreAmount, false);
		}

		public static DriverPayments GetPayment(PersistenceManager pPM, string pTransNo, decimal pAmount, bool pIgnoreAmount, bool pCheckWithDriverFee) {
			DriverPayments oPay = GetPayment(pPM, pTransNo);

			if (!oPay.IsNullEntity) {
				if (!pIgnoreAmount) {
					if (pCheckWithDriverFee) {
						if (oPay.Fare > 0 && oPay.DriverTotalWithDriverFee != pAmount) {
							return pPM.GetNullEntity<DriverPayments>();
						}
					} else {
						if (oPay.Fare > 0 && oPay.DriverTotal != pAmount && oPay.DriverTotalWithDriverFee != pAmount) {
							return pPM.GetNullEntity<DriverPayments>();
						}
					}
				}
			}
			return oPay;
		}

		public static EntityList<DriverPayments> GetPaymentByCellPhone(PersistenceManager pPM, string pCellPhone, decimal pAmount) {
			return GetPaymentByCellPhone(pPM, pCellPhone, pAmount, false);
		}

		public static EntityList<DriverPayments> GetPaymentByCellPhone(PersistenceManager pPM, string pCellPhone, decimal pAmount, bool pIncludePaidTrans) {
			EntityList<AffiliateDrivers> driverList = AffiliateDrivers.GetDriversByCellPhone(pPM, pCellPhone);
			List<long> driverIDs = (from p in driverList
									select p.AffiliateDriverID).ToList();

			RdbQuery qry = new RdbQuery(typeof(DriverPayments), DriverPayments.AffiliateDriverIDEntityColumn, EntityQueryOp.In, driverIDs);
			if (!pIncludePaidTrans) {
				qry.AddClause(DriverPayments.DriverPaidEntityColumn, EntityQueryOp.EQ, false);
			}
			qry.AddClause(DriverPayments.FailedEntityColumn, EntityQueryOp.EQ, false);
			qry.AddClause(DriverPayments.TestEntityColumn, EntityQueryOp.EQ, false);
			qry.AddSpan(EntityRelations.DriverPayments_ACHDetail);

			EntityList<DriverPayments> payList = pPM.GetEntities<DriverPayments>(qry);
			List<DriverPayments> goodList = new List<DriverPayments>();
			if (pIncludePaidTrans) {
				goodList = (from p in payList
							where p.DriverTotal == pAmount
							select p).ToList();
			} else {
				goodList = (from p in payList
							where p.DriverTotal == pAmount && p.ACHDetail.IsNullEntity
							select p).ToList();
			}

			return new EntityList<DriverPayments>(goodList); //[0];
		}


		public static DriverPayments GetPayment(PersistenceManager pPM, string pTransNo, string pCellPhone) {

			DriverPayments oPay = GetPayment(pPM, pTransNo);
			if (!oPay.IsNullEntity) {
				if (oPay.AffiliateDrivers.Cell != pCellPhone && oPay.AffiliateDrivers.CallInCell != pCellPhone) {
					return pPM.GetNullEntity<DriverPayments>();
				}
			}
			return oPay;
		}



		public static DriverPayments GetPaymentbyPaymentID(PersistenceManager pPM, string pPaymentID) {
			return GetPaymentbyPaymentID(pPM, pPaymentID, pPM.DefaultQueryStrategy);
		}

		public static DriverPayments GetPaymentbyPaymentID(PersistenceManager pPM, string pPaymentID, QueryStrategy pQueryStrategy) {
			RdbQuery qry = new RdbQuery(typeof(DriverPayments), DriverPaymentIDEntityColumn, EntityQueryOp.EQ, pPaymentID);
			DriverPayments oPay = pPM.GetEntity<DriverPayments>(qry, pQueryStrategy);
			return oPay;
		}

		public static DriverPayments GetPayment(PersistenceManager pPM, string pTransNo) {
			return GetPayment(pPM, pTransNo, pPM.DefaultQueryStrategy);
		}

		public static DriverPayments GetPayment(PersistenceManager pPM, string pTransNo, QueryStrategy pQueryStrategy) {
			pTransNo = DetermineRealVoucherNo(pTransNo);

			RdbQuery qry = new RdbQuery(typeof(DriverPayments), TransNoEntityColumn, EntityQueryOp.EQ, pTransNo);
			DriverPayments oPay = pPM.GetEntity<DriverPayments>(qry, pQueryStrategy);
			return oPay;
		}


		// ************************************************************************************************************
		// Public Get DriverPayments:  If "Tag" = true, this is a Ride Voucher 
		// ************************************************************************************************************
		public static DriverPayments GetRideDriverPayments(PersistenceManager pPM, string pTransNo) {
			return GetRideRelatedData(pPM, pTransNo, "", "")[0];
		}

		// ************************************************************************************************************
		// Public Get DriverPayments:  If "Tag" = true, this is a Ride Voucher 
		// ************************************************************************************************************
		public static EntityList<DriverPayments> GetRideDriverPaymentsByCardNoAndDate(PersistenceManager pPM, string pCardNumber, DateTime pDate) {
			return GetRideRelatedData(pPM, "", pCardNumber, pDate.ToShortDateString());
		}

		// ************************************************************************************************************
		// Get DriverPayments:  If "Tag" = true, this is a Ride Voucher 
		// ************************************************************************************************************
		private static EntityList<DriverPayments> GetRideRelatedData(PersistenceManager pPM, string pTransNo, string pCardNumber, string pDate) {
			string sDelimiter = "";
			int iArgCount = 0;

			StringBuilder sql = new StringBuilder();
			sql.Append("usp_DriverPaymentsRideGet ");
			if (!pTransNo.IsNullOrEmpty()) {
				sql.AppendFormat(" @TransNo = '{0}'", pTransNo);
				iArgCount++;
			}
			if (!pCardNumber.IsNullOrEmpty()) {
				if (iArgCount > 0) {
					sDelimiter = ", ";
				}
				sql.AppendFormat("{0}@CardNumber = '{1}'", sDelimiter, pCardNumber);
				iArgCount++;
			}
			if (!pCardNumber.IsNullOrEmpty()) {
				if (iArgCount > 0) {
					sDelimiter = ", ";
				}
				sql.AppendFormat("{0}@ChargeDate = '{1}'", sDelimiter, pDate);
				iArgCount++;
			}

			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(DriverPayments), sql.ToString());
			EntityList<DriverPayments> oData = pPM.GetEntities<DriverPayments>(qry);
			if (oData.Count == 0) {
				oData.Add(pPM.GetNullEntity<DriverPayments>());
			}
			return oData;
		}



		public static DriverPayments GetPayment(Affiliate pAffiliate, string pTransNo) {
			pTransNo = DetermineRealVoucherNo(pTransNo);

			RdbQuery qry = new RdbQuery(typeof(DriverPayments), TransNoEntityColumn, EntityQueryOp.EQ, pTransNo);
			qry.AddClause(DriverPayments.AffiliateIDEntityColumn, EntityQueryOp.EQ, pAffiliate.AffiliateID);
			DriverPayments oPay = pAffiliate.PersistenceManager.GetEntity<DriverPayments>(qry);
			return oPay;
		}

		public static DriverPayments GetPaymentByReferenceNo(Affiliate pAffiliate, string pReferenceNo) {
			return GetPaymentByReferenceNo(pAffiliate, pReferenceNo, true);
		}

		public static DriverPayments GetPaymentByReferenceNo(Affiliate pAffiliate, string pReferenceNo, bool pIncludeFailed) {
			RdbQuery qry = new RdbQuery(typeof(DriverPayments), ReferenceNoEntityColumn, EntityQueryOp.EQ, pReferenceNo);
			qry.AddClause(DriverPayments.AffiliateIDEntityColumn, EntityQueryOp.EQ, pAffiliate.AffiliateID);
			if (!pIncludeFailed) {
				qry.AddClause(FailedEntityColumn, EntityQueryOp.EQ, false);
			}
			DriverPayments oPay = pAffiliate.PersistenceManager.GetEntity<DriverPayments>(qry);
			return oPay;
		}

		// ********************************************************************************
		// Return true if a reference no exists for an affiliate
		// ********************************************************************************
		public static bool PaymentExistsByReference(PersistenceManager pPM, long pAffiliateID, string pReferenceNo) {
			StringBuilder sql = new StringBuilder();
			sql.Append("SELECT CASE WHEN COUNT(Found) = 0 THEN cast(0 AS BIT) ");
			sql.Append("ELSE cast(1 AS BIT) END Found ");
			sql.Append("FROM (SELECT cast(1 AS BIT) AS Found ");
			sql.Append("FROM DriverPayments ");
			sql.AppendFormat("WHERE affiliateid = {0}", pAffiliateID);
			sql.AppendFormat("AND referenceno = '{0}'", pReferenceNo);
			sql.Append(") AS Sub");

			Entity[] oResults = DynamicEntities.DynamicDBCall(pPM, sql.ToString());
			return bool.Parse(oResults[0]["Found"].ToString());
		}

		public static EntityList<DriverPayments> GetPaymentByReferenceNo(PersistenceManager pPM, long[] pAffiliateIDs, string pReferenceNo) {
			RdbQuery qry = new RdbQuery(typeof(DriverPayments), ReferenceNoEntityColumn, EntityQueryOp.EQ, pReferenceNo);
			qry.AddClause(DriverPayments.AffiliateIDEntityColumn, EntityQueryOp.In, pAffiliateIDs);
			EntityList<DriverPayments> oPayments = pPM.GetEntities<DriverPayments>(qry);
			return oPayments;
		}

		public static EntityList<DriverPayments> GetPaymentsByReferenceNo(AffiliateDrivers pAffiliateDriver, string pReferenceNo) {
			RdbQuery qry = new RdbQuery(typeof(DriverPayments), ReferenceNoEntityColumn, EntityQueryOp.EQ, pReferenceNo);
			qry.AddClause(DriverPayments.AffiliateDriverIDEntityColumn, EntityQueryOp.EQ, pAffiliateDriver.AffiliateDriverID);
			return pAffiliateDriver.PersistenceManager.GetEntities<DriverPayments>(qry);
		}

		public static EntityList<DriverPayments> GetPaymentsByReferenceNo(Drivers pDriver, string pReferenceNo) {
			RdbQuery qry = new RdbQuery(typeof(DriverPayments), ReferenceNoEntityColumn, EntityQueryOp.EQ, pReferenceNo);
			qry.AddClause(DriverPayments.DriverIDEntityColumn, EntityQueryOp.EQ, pDriver.DriverID);
			return pDriver.PersistenceManager.GetEntities<DriverPayments>(qry);
		}

		public static EntityList<DriverPayments> GetPaymentsByReferenceNo(PersistenceManager pPM, string pReferenceNo) {
			RdbQuery qry = new RdbQuery(typeof(DriverPayments), ReferenceNoEntityColumn, EntityQueryOp.EQ, pReferenceNo);
			return pPM.GetEntities<DriverPayments>(qry);
		}


		// **********************************************************************************************
		// return a list of payment recs by Trans No's
		// **********************************************************************************************
		public static EntityList<DriverPayments> GetPayments(PersistenceManager pPM, List<string> pTransNumbers) {
			RdbQuery qry = new RdbQuery(typeof(DriverPayments), TransNoEntityColumn, EntityQueryOp.In, pTransNumbers);
			EntityList<DriverPayments> payList = pPM.GetEntities<DriverPayments>(qry);
			return payList;
		}


		// **********************************************************************************************
		// return a list of payment recs by PaymentIDs
		// **********************************************************************************************
		public static EntityList<DriverPayments> GetPayments(PersistenceManager pPM, List<long> pDriverPaymentIDs) {
			RdbQuery qry = new RdbQuery(typeof(DriverPayments), DriverPaymentIDEntityColumn, EntityQueryOp.In, pDriverPaymentIDs);
			EntityList<DriverPayments> payList = pPM.GetEntities<DriverPayments>(qry);
			return payList;
		}




		// **********************************************************************************************
		// 4/13/2010 - Make sure we are dealing with a 6 char (not a 12) voucher no
		// **********************************************************************************************
		public static string DetermineRealVoucherNo(string pTransNo) {
			if (pTransNo.IsNumeric() & pTransNo.ToString().Length > 6) {
				pTransNo = pTransNo.FromBarCode();
			}
			return pTransNo;
		}


		// **********************************************************************************************
		// check for recurring fees and deduct
		// **********************************************************************************************
		public decimal DriverPaidProcessRecurringFee(TaxiPassRedeemers pRedeemer, AffiliateDrivers pPayeeDriver, DateTime pDriverPaidDate, string pUpdatedBy) {
			return DriverPaidProcessRecurringFee(pRedeemer, pPayeeDriver.Name, pPayeeDriver.TransCardNumber, pDriverPaidDate, pUpdatedBy);
		}

		public decimal DriverPaidProcessRecurringFee(TaxiPassRedeemers pRedeemer, string pPayeeDriverName, string pPayeeDriverTransCardNumber, DateTime pDriverPaidDate, string pUpdatedBy) {
			string transCardOwner = pPayeeDriverName;
			string transCardNumber = pPayeeDriverTransCardNumber;
			decimal recurringFeeTotal = 0;

			DriverPayments fee = this.GetPerTransactionRecurringFee(true);
			if (fee.IsNullEntity) {
				//check if there are other fees
				EntityList<DriverPayments> list = this.GetRecurringFeesByDriver(true);
				if (list.Count > 0) {
					fee = list[0];
				}
			}
			if (!fee.IsNullEntity) {
				if (pRedeemer.TaxiPassRedeemerAccounts.VoucherValidationOnly && !pRedeemer.TaxiPassRedeemerAccounts.MatchVoucherOnly) {
					// do nothing
				} else {
					if (!pRedeemer.TaxiPassRedeemerAccounts.MatchVoucherOnly) {
						if (fee.ACHDetail.IsNullEntity) {
							ACHDetail.Create(fee);
						}
						fee.ACHDetail.ACHPaidDate = pDriverPaidDate;
						fee.ACHDetail.ACHPaidTo = "From " + fee.MyDriverName;
						fee.ACHDetail.ACHBankAccountNumber = "Less Cash";
					}
				}

				try {
					if (fee.RowState != DataRowState.Unchanged) {
						fee.Save();
						recurringFeeTotal += Math.Abs(fee.DriverTotalWithKioskAmount);
					}
				} catch {
				}
			}

			return recurringFeeTotal;
		}


		// **********************************************************************************************
		//
		// **********************************************************************************************
		public static EntityList<DriverPayments> GetPaidTransactions(TaxiPassRedeemerAccounts pAccount, DriverPaymentStatus pStatus, DateTime pStart, DateTime pEnd) {
			StringBuilder sql = new StringBuilder();
			sql.Append("SELECT DriverPayments.* from DriverPayments  WITH (NOLOCK) ");
			sql.Append(" left join ACHDetail on DriverPayments.DriverPaymentID = ACHDetail.DriverPaymentID ");
			sql.Append(" WHERE (DriverPaidDate between '");
			sql.Append(pStart);
			sql.Append("' and '");
			sql.Append(pEnd);
			sql.Append("')  and (TaxiPassRedeemerID in (select TaxiPassRedeemerID from TaxiPassRedeemers where TaxiPassRedeemerAccountID = ");
			sql.Append(pAccount.TaxiPassRedeemerAccountID);
			sql.Append(")) ");
			if (pStatus == DriverPaymentStatus.unpaid) {
				sql.Append("AND ACHDetail.ACHPaidDate is null ");
			} else if (pStatus == DriverPaymentStatus.paid) {
				sql.Append(" AND ACHDetail.ACHPaidDate is not null ");
			}
			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(DriverPayments), sql.ToString());
			return pAccount.PersistenceManager.GetEntities<DriverPayments>(qry);

		}

		// **********************************************************************************************
		//
		// **********************************************************************************************
		public static EntityList<DriverPayments> GetAchPaidTransactions(TaxiPassRedeemerAccounts pAccount, DriverPaymentStatus pStatus, DateTime pStart, DateTime pEnd) {
			StringBuilder sql = new StringBuilder();
			sql.Append("SELECT DriverPayments.* from DriverPayments  WITH (NOLOCK) ");
			sql.Append(" left join ACHDetail on DriverPayments.DriverPaymentID = ACHDetail.DriverPaymentID ");
			sql.Append(" WHERE (ACHPaidDate between '");
			sql.Append(pStart);
			sql.Append("' and '");
			sql.Append(pEnd);
			sql.Append("')  and (TaxiPassRedeemerID in (select TaxiPassRedeemerID from TaxiPassRedeemers where TaxiPassRedeemerAccountID = ");
			sql.Append(pAccount.TaxiPassRedeemerAccountID);
			sql.Append(")) ");
			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(DriverPayments), sql.ToString());
			EntityList<DriverPayments> achList = pAccount.PersistenceManager.GetEntities<DriverPayments>(qry);

			//cache ACH detail info
			string sql1 = "SELECT * FROM ACHDetail with (NOLOCK) WHERE DriverPaymentID IN (" + sql.ToString().Replace("DriverPayments.*", "DriverPayments.DriverPaymentID") + ")";
			qry = new PassthruRdbQuery(typeof(ACHDetail), sql1);
			pAccount.PersistenceManager.GetEntities<ACHDetail>(qry);
			return achList;

		}


		// **********************************************************************************************
		//
		// **********************************************************************************************
		public static EntityList<DriverPayments> GetRedeemerTransactionsToACH(TaxiPassRedeemerAccounts pAccount, DateTime pEnd) {
			EntityList<DriverPayments> oList = new EntityList<DriverPayments>();
			//StringBuilder sql = new StringBuilder();
			//sql.Append("select DISTINCT DriverPayments.* from DriverPayments  WITH (NOLOCK) ");
			//sql.Append("inner join TaxiPassRedeemers on TaxiPassRedeemers.TaxiPassRedeemerID = DriverPayments.TaxiPassRedeemerID ");
			//sql.Append(" LEFT OUTER join ACHDetail ON DriverPayments.DriverPaymentID = ACHDetail.DriverPaymentID ");
			//sql.Append(" LEFT JOIN DriverPaymentAux ON DriverPayments.DriverPaymentID = DriverPaymentAux.DriverPaymentID ");
			//sql.Append("where Failed = 0 and Test = 0 and  DriverPaid = 1 and ");
			//sql.Append("(DriverPaidDate <= '");
			//sql.Append(pEnd);
			//sql.Append("' OR ChargeBack = 1) ");
			//sql.Append(" and TaxiPassRedeemers.TaxiPassRedeemerAccountID = ");
			//sql.Append(pAccount.TaxiPassRedeemerAccountID);
			//sql.Append(" and ACHDetail.DriverPaymentID is null");
			//sql.Append(" AND (DriverPaymentAux.DoNotPay IS NULL OR DriverPaymentAux.DoNotPay = 0) ");
			//sql.Append(" order by DriverPaidDate ");
			string sql = $@"SELECT DISTINCT DriverPayments.*
                            FROM DriverPayments WITH (NOLOCK)
                            INNER JOIN TaxiPassRedeemers ON TaxiPassRedeemers.TaxiPassRedeemerID = DriverPayments.TaxiPassRedeemerID
                            LEFT OUTER JOIN ACHDetail ON DriverPayments.DriverPaymentID = ACHDetail.DriverPaymentID
                            LEFT JOIN DriverPaymentAux ON DriverPayments.DriverPaymentID = DriverPaymentAux.DriverPaymentID
                            WHERE Failed = 0
                                     AND Test = 0
                                     AND DriverPaid = 1
                                     AND(DriverPaidDate <= '{pEnd}'
                                             OR ChargeBack = 1
                                         )
                                     AND TaxiPassRedeemers.TaxiPassRedeemerAccountID = {pAccount.TaxiPassRedeemerAccountID}
                                     AND ACHDetail.DriverPaymentID IS NULL
                                     AND (   (   DriverPaymentAux.DoNotPay IS NULL
                                             OR DriverPaymentAux.DoNotPay = 0
                                         )
                                         OR (   (   DriverPaymentAux.DoNotPay = 1
                                                    OR DriverPaymentAux.DoNotPay = 1
                                                )
                                                AND DriverPaidDate >= '6/1/2017'
                                            )
                                     )
                            ORDER BY DriverPaidDate";

			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(DriverPayments), sql.ToString());
			qry.CommandTimeout = 300;
			oList = pAccount.PersistenceManager.GetEntities<DriverPayments>(qry);
			return oList;
		}

		// **********************************************************************************************
		//
		// **********************************************************************************************
		public static DataTable GetRedeemerPaidPendingMatchesTransactionsToACH(PersistenceManager pPM) {
			StringBuilder sql = new StringBuilder();
			sql.Append("SELECT DriverPaymentsPendingMatches.DriverPaymentID  FROM DriverPaymentsPendingMatches  WITH (NOLOCK) ");
			sql.Append(" INNER JOIN DriverPayments ON DriverPaymentsPendingMatches.DriverPaymentID = DriverPayments.DriverPaymentID ");
			sql.Append(" WHERE (DriverPayments.DriverPaid = 1) ");
			sql.Append(" and DriverPaymentsPendingMatches.DriverPaymentID not in (select DriverPaymentID from ACHDetail)");

			return SQLHelper.GetRecords(pPM, sql.ToString());
		}

		public static EntityList<DriverPayments> GetPayments(Affiliate pAffiliate, long pDriver, long pAffiliateDriverID, DateTime pStart, DateTime pEnd, DriverPaymentStatus pStatus) {
			return GetPayments(pAffiliate, pDriver, pAffiliateDriverID, pStart, pEnd, pStatus, 0, true);
		}

		public static EntityList<DriverPayments> GetPayments(Affiliate pAffiliate, long pDriver, long pAffiliateDriverID, DateTime pStart, DateTime pEnd, DriverPaymentStatus pStatus, long pVehicleID) {
			return GetPayments(pAffiliate, pDriver, pAffiliateDriverID, pStart, pEnd, pStatus, pVehicleID, true);
		}

		public static EntityList<DriverPayments> GetPayments(EntityList<Affiliate> pAffiliateList, long pDriver, long pAffiliateDriverID, DateTime pStart, DateTime pEnd, DriverPaymentStatus pStatus, long pVehicleID, bool pIncludeTest) {

			DateTime dStart = pStart;
			DateTime dEnd = pEnd;
			StringBuilder sSQL = new StringBuilder();
			sSQL.Append("SELECT distinct * FROM DriverPayments  WITH (NOLOCK)");
			sSQL.Append("where ");
			sSQL.Append(DriverPayments.ChargeDateEntityColumn.ColumnName);
			sSQL.Append(" >= '");
			sSQL.Append(dStart.ToShortDateString());
			sSQL.Append("' ");
			sSQL.Append("AND ");
			sSQL.Append(DriverPayments.ChargeDateEntityColumn.ColumnName);
			sSQL.Append(" <= '");
			sSQL.Append(dEnd.ToShortDateString());
			sSQL.Append("' ");

			if (pDriver > 0) {
				sSQL.Append("AND ");
				sSQL.Append(DriverPayments.DriverIDEntityColumn.ColumnName);
				sSQL.Append(" = ");
				sSQL.Append(pDriver);
				sSQL.Append(" ");
			} else {
				sSQL.Append("AND ");
				sSQL.Append(DriverPayments.DriverIDEntityColumn.ColumnName);
				sSQL.Append(" In (SELECT DriverID From Drivers WHERE ");
				sSQL.Append(Drivers.AffiliateIDEntityColumn.ColumnName);
				sSQL.Append(" IN (");
				foreach (Affiliate rec in pAffiliateList) {
					sSQL.Append(rec.AffiliateID);
					if (pAffiliateList.IndexOf(rec) < pAffiliateList.Count - 1) {
						sSQL.Append(",");
					}
				}
			}

			if (pAffiliateDriverID > 0) {
				sSQL.Append("AND ");
				sSQL.Append(DriverPayments.AffiliateDriverIDEntityColumn.ColumnName);
				sSQL.Append(" = ");
				sSQL.Append(pAffiliateDriverID);
				sSQL.Append(" ");
			}
			if (pVehicleID > 0) {
				sSQL.Append("AND ");
				sSQL.Append(DriverPayments.VehicleIDEntityColumn.ColumnName);
				sSQL.Append(" = ");
				sSQL.Append(pVehicleID);
				sSQL.Append(" ");
			}
			switch (pStatus) {
				case DriverPaymentStatus.paid:
					sSQL.Append("AND ");
					sSQL.Append(DriverPayments.DriverPaidEntityColumn.ColumnName);
					sSQL.Append(" = 1 ");
					break;
				case DriverPaymentStatus.unpaid:
					sSQL.Append("AND ");
					sSQL.Append(DriverPayments.DriverPaidEntityColumn.ColumnName);
					sSQL.Append(" = 0 ");
					break;
			}

			if (!pIncludeTest) {
				sSQL.Append("AND ");
				sSQL.Append(DriverPayments.TestEntityColumn.ColumnName);
				sSQL.Append(" = 0 ");
			}

			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(DriverPayments), sSQL.ToString());
			return pAffiliateList[0].PersistenceManager.GetEntities<DriverPayments>(qry);
		}

		public static EntityList<DriverPayments> GetPayments(Affiliate pAffiliate, long pDriver, long pAffiliateDriverID, DateTime pStart, DateTime pEnd, DriverPaymentStatus pStatus, long pVehicleID, bool pIncludeTest) {
			TaxiPassCommon.TimeZoneConverter oTimeZone = new TimeZoneConverter();
			DateTime dStart = pStart;
			DateTime dEnd = pEnd;
			RdbQuery qry = new RdbQuery(typeof(DriverPayments), ChargeDateEntityColumn, EntityQueryOp.GE, dStart);
			qry.AddClause(ChargeDateEntityColumn, EntityQueryOp.LE, dEnd);

			if (pDriver > 0) {
				qry.AddClause(DriverIDEntityColumn, EntityQueryOp.EQ, pDriver);
			} else {
				List<long> driverIDs = new List<long>();
				EntityList<Drivers> driverList = Drivers.GetDrivers(pAffiliate);
				foreach (Drivers oDriver in driverList) {
					driverIDs.Add(oDriver.DriverID);
				}

				qry.AddClause(DriverPayments.DriverIDEntityColumn, EntityQueryOp.In, driverIDs);
			}
			if (pAffiliateDriverID > 0) {
				qry.AddClause(DriverPayments.AffiliateDriverIDEntityColumn, EntityQueryOp.EQ, pAffiliateDriverID);
			}
			if (pVehicleID > 0) {
				qry.AddClause(DriverPayments.VehicleIDEntityColumn, EntityQueryOp.EQ, pVehicleID);
			}

			switch (pStatus) {
				case DriverPaymentStatus.paid:
					qry.AddClause(DriverPaidEntityColumn, EntityQueryOp.EQ, true);
					break;
				case DriverPaymentStatus.unpaid:
					qry.AddClause(DriverPaidEntityColumn, EntityQueryOp.EQ, false);
					break;
			}

			if (!pIncludeTest) {
				qry.AddClause(DriverPayments.TestEntityColumn, EntityQueryOp.EQ, false);
			}

			qry.AddSpan(EntityRelations.Drivers_DriverPayments);
			qry.AddSpan(EntityRelations.Vehicles_DriverPayments);
			qry.AddSpan(EntityRelations.AffiliateDrivers_DriverPayments);
			qry.AddSpan(EntityRelations.DriverPayments_DriverPaymentResults);
			qry.AddSpan(EntityRelations.DriverPayments_DriverPaymentVoucherImages);

			qry.CommandTimeout = 300;
			return pAffiliate.PersistenceManager.GetEntities<DriverPayments>(qry, QueryStrategy.DataSourceThenCache);
		}

		public static EntityList<DriverPayments> GetTransactions(Affiliate pAffiliate, DateTime pStart, DateTime pEnd, bool pIncludeTest, bool pIncludeFailed) {
			return GetTransactions(pAffiliate, pStart, pEnd, pIncludeTest, pIncludeFailed, false);
		}


		// **************************************************************************************
		// Get a list of transactions for an affiliate within a date range
		// **************************************************************************************
		public static EntityList<DriverPayments> GetTransactions(Affiliate pAffiliate
																, DateTime pStart
																, DateTime pEnd
																, bool pIncludeTest
																, bool pIncludeFailed
																, bool pAddSpans) {
			if (pAffiliate == null) {
				return new EntityList<DriverPayments>();
			}
			TaxiPassCommon.TimeZoneConverter oTimeZone = new TimeZoneConverter();
			DateTime dStart = pStart;
			DateTime dEnd = pEnd;
			object[] objDates = new object[2] { pStart, pEnd };

			RdbQuery qry = new RdbQuery(typeof(DriverPayments), AuthDateEntityColumn, EntityQueryOp.Between, objDates);
			qry.AddClause(ChargeDateEntityColumn, EntityQueryOp.Between, objDates);
			qry.AddOperator(EntityBooleanOp.Or);

			qry.AddClause(AffiliateIDEntityColumn, EntityQueryOp.EQ, pAffiliate.AffiliateID);


			// drivers sub query
			RdbSubquery subQry = new RdbSubquery(EntityRelationLink.From(EntityRelations.Drivers_DriverPayments, QueryDirection.ParentQuery));
			subQry.AddClause(Drivers.AffiliateIDEntityColumn, EntityQueryOp.EQ, pAffiliate.AffiliateID);
			qry.AddSubquery(subQry);
			qry.AddOperator(EntityBooleanOp.Or);

			if (!pIncludeFailed) {
				qry.AddClause(FailedEntityColumn, EntityQueryOp.EQ, false);
			}
			if (!pIncludeTest) {
				qry.AddClause(TestEntityColumn, EntityQueryOp.EQ, false);
			}

			if (pAddSpans) {
				qry.AddSpan(EntityRelations.DriverPayments_ACHDetail);
				qry.AddSpan(EntityRelations.Affiliate_DriverPayments);
				qry.AddSpan(EntityRelations.DriverPayments_DriverPaymentValidation);
				qry.AddSpan(EntityRelations.DriverPayments_DriverPaymentsPendingMatches);
				qry.AddSpan(EntityRelations.DriverPayments_DriverPaymentKioskAmount);
				qry.AddSpan(EntityRelations.TaxiPassRedeemers_DriverPayments);
				qry.AddSpan(EntityRelations.DriverPayments_DriverPaymentNotes);

				qry.AddSpan(EntityRelations.DriverPayments_DriverPaymentVoucherImages);
			}

			EntityList<DriverPayments> payList = pAffiliate.PersistenceManager.GetEntities<DriverPayments>(qry);


			payList = new EntityList<DriverPayments>((from p in payList
													  select p).Distinct().ToList());
			return payList;
		}




		public static EntityList<DriverPayments> GetTransactionsByChargeDate(Affiliate pAffiliate, DateTime pStart, DateTime pEnd, bool pIncludeTest, bool pIncludeFailed) {
			return GetTransactionsByChargeDate(pAffiliate.PersistenceManager, pAffiliate.AffiliateID, pStart, pEnd, pIncludeTest, pIncludeFailed);
		}

		public static EntityList<DriverPayments> GetTransactionsByChargeDate(PersistenceManager pPM, long pAffiliateID, DateTime pStart, DateTime pEnd, bool pIncludeTest, bool pIncludeFailed) {
			if (pAffiliateID == 0) {
				return new EntityList<DriverPayments>();
			}
			Affiliate pAffiliate = Affiliate.GetAffiliate(pPM, pAffiliateID);

			TaxiPassCommon.TimeZoneConverter oTimeZone = new TimeZoneConverter();
			DateTime dStart = pStart;
			DateTime dEnd = pEnd;
			RdbQuery qry = new RdbQuery(typeof(DriverPayments), ChargeDateEntityColumn, EntityQueryOp.GE, dStart);
			qry.AddClause(ChargeDateEntityColumn, EntityQueryOp.LE, dEnd);

			List<long> driverIDs = new List<long>();
			EntityList<Drivers> driverList = Drivers.GetDrivers(pAffiliate);
			foreach (Drivers oDriver in driverList) {
				driverIDs.Add(oDriver.DriverID);
			}
			qry.AddClause(DriverPayments.DriverIDEntityColumn, EntityQueryOp.In, driverIDs);

			if (!pIncludeFailed) {
				qry.AddClause(DriverPayments.FailedEntityColumn, EntityQueryOp.EQ, false);
			}
			if (!pIncludeTest) {
				qry.AddClause(DriverPayments.TestEntityColumn, EntityQueryOp.EQ, false);
			}

			return pAffiliate.PersistenceManager.GetEntities<DriverPayments>(qry);
		}

		public static EntityList<DriverPayments> GetPaymentsToAch(Affiliate pAffiliate
															, long pDriver
															, long pAffiliateDriverID
															, DateTime pStart
															, DateTime pEnd
															, DriverPaymentStatus pStatus
															, bool pIncludeTest
															, bool pIncludeFailed) {
			return GetPaymentsToAch(pAffiliate
									, pDriver
									, pAffiliateDriverID
									, pStart
									, pEnd
									, pStatus
									, pIncludeTest
									, pIncludeFailed
									, false);
		}


		// ***********************************************************************************************
		// Proc used by CabRideAutoACH
		// SQL is identical to GetPaymentsToAch(Affiliate pAffiliate... but procs provide speed
		// ***********************************************************************************************
		public static EntityList<DriverPayments> GetCabRideAutoACH(Affiliate pAffiliate
																, DateTime pEnd) {
			StringBuilder sql = new StringBuilder();
			sql.AppendFormat("usp_DriverPaymentsCabRideAutoACH @AffiliateID = {0}", pAffiliate.AffiliateID);
			sql.AppendFormat(", @End = '{0}'", pEnd.ToString());
			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(DriverPayments), sql.ToString());
			qry.CommandTimeout = 1200;

			EntityList<DriverPayments> oList = pAffiliate.PersistenceManager.GetEntities<DriverPayments>(qry);

			return oList;
		}

		// **********************************************************************************************
		// Use GetCabRideAutoACH for CabRideAutoACH
		// **********************************************************************************************
		public static EntityList<DriverPayments> GetPaymentsToAch(Affiliate pAffiliate
																, long pDriver
																, long pAffiliateDriverID
																, DateTime pStart
																, DateTime pEnd
																, DriverPaymentStatus pStatus
																, bool pIncludeTest
																, bool pIncludeFailed
																, bool pChargeBackDateAsEndDate) {
			StringBuilder sql = new StringBuilder(@"SELECT DriverPayments.* 
													FROM DriverPayments  WITH (NOLOCK) 
													LEFT OUTER JOIN achdetail ON DriverPayments.DriverPaymentID = ACHDetail.DriverPaymentID 
													WHERE ((failed = 0 and test = 0 ) ");

			if (pIncludeTest) {
				sql.Append(" or test = 1 ");
			}
			if (pIncludeFailed) {
				sql.Append(" or failed = 1 ");
			}
			sql.Append(")");

			if (pDriver > 0) {
				sql.Append("and DriverID = ");
				sql.Append(pDriver);
			} else {
				sql.AppendFormat(" AND (DriverID in (Select DriverID from Drivers Where AffiliateID = {0})", pAffiliate.AffiliateID);
				sql.AppendFormat(" OR AffiliateID = {0})", pAffiliate.AffiliateID);
			}

			if (pAffiliateDriverID > 0) {
				sql.Append(" AND AffiliateDriverID = ");
				sql.Append(pAffiliateDriverID);
				//sql.Append(" AND ");
			}

			if (pStatus != DriverPaymentStatus.paid) {
				sql.AppendFormat(" AND ((ChargeDate >= '{0}' AND ChargeDate <= '{1}') OR (ChargeDate >= '{0}' AND ChargeBack = 1", pStart, pEnd);
				if (pChargeBackDateAsEndDate) {
					sql.Append(" AND ChargeDate <= '");
					sql.Append(pEnd);
					sql.Append("'");
				}
				sql.Append(")) ");
			}


			if (pStatus == DriverPaymentStatus.paid) {
				sql.Append("and achdetail.ACHPaidDate BETWEEN '");
				sql.Append(pStart);
				sql.Append("' AND '");
				sql.Append(pEnd);
				sql.Append("' ");

			} else if (pStatus == DriverPaymentStatus.unpaid) {
				sql.Append(" and ACHDetail.DriverPaymentID is null ");
			}

			sql.Append("Order By ChargeDate ");
			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(DriverPayments), sql.ToString());
			qry.CommandTimeout = 120;
			EntityList<DriverPayments> oList = pAffiliate.PersistenceManager.GetEntities<DriverPayments>(qry);

			mACHTagCount = 0;
			mACHTagTotal = 0;
			return oList;
		}



		// **********************************************************************************************
		// For ACH Payments to make for  CabRideAutoACH
		// **********************************************************************************************
		public static EntityList<DriverPayments> GetCabRidePaymentsToAch(Affiliate pAffiliate
																, DateTime pStart
																, DateTime pEnd) {
			StringBuilder sql = new StringBuilder();

			sql.AppendFormat("usp_AutoACH @AffiliateID	= {0}", pAffiliate.AffiliateID);
			sql.AppendFormat(", @StartDate	= '{0}'", pStart.ToShortDateString());
			sql.AppendFormat(", @EndDate	= '{0}'", pEnd.ToString());
			sql.AppendFormat(", @Status	= 'UnPaid'");

			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(DriverPayments), sql.ToString());
			qry.CommandTimeout = 120;
			EntityList<DriverPayments> oPayments = pAffiliate.PersistenceManager.GetEntities<DriverPayments>(qry);

			return oPayments;
		}



		public static EntityList<DriverPayments> GetPaymentsToAch(TaxiPassRedeemerAccounts pAccount, DateTime pStart, DateTime pEnd, DriverPaymentStatus pStatus, bool pIncludeTest, bool pIncludeFailed) {
			StringBuilder sql = new StringBuilder();
			StringBuilder RedeemerIDs = new StringBuilder();

			sql.Append("SELECT * ");
			sql.Append(" FROM    DriverPayments");
			sql.Append(" left outer join ACHDetail on DriverPayments.DriverPaymentID = ACHDetail.DriverPaymentID ");
			sql.Append(" where ChargeDate >='");
			sql.Append(pStart.ToShortDateString());
			sql.Append(" '");
			sql.Append(" and ChargeDate <= '");
			sql.Append(pEnd.ToShortDateString());
			sql.Append(" '");
			if (!pIncludeFailed) {
				sql.Append(" and Failed = 0");
			}
			if (!pIncludeTest) {
				sql.Append(" and Test = 0");
			}
			// pull list of redeemerIDs
			if (!pAccount.IsNullEntity) {
				foreach (TaxiPassRedeemers oRedeemer in pAccount.TaxiPassRedeemerses) {
					if (!RedeemerIDs.ToString().IsNullOrEmpty()) {
						RedeemerIDs.Append(", ");
					}
					RedeemerIDs.Append(oRedeemer.TaxiPassRedeemerID);
				}

				sql.Append(" and TaxiPassRedeemerID in (");
				sql.Append(RedeemerIDs.ToString());
				sql.Append(")");
			}
			if (pStatus == DriverPaymentStatus.paid) {
				sql.Append(" and ACHDetail.DriverPaymentID is not null ");
			} else {
				sql.Append(" and ACHDetail.DriverPaymentID is null ");
			}

			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(DriverPayments), sql.ToString());
			qry.CommandTimeout = 120;

			EntityList<DriverPayments> oList = new EntityList<DriverPayments>();
			try {
				oList = pAccount.PersistenceManager.GetEntities<DriverPayments>(qry, QueryStrategy.DataSourceOnly);
			} catch (Exception) {

			}


			foreach (DriverPayments oPay in oList) {
				oPay.Tag = false;
			}
			mACHTagCount = 0;
			mACHTagTotal = 0;
			return oList;
		}


		// **********************************************************************************************
		// Aggregate the GetPaymentsToAch data
		// **********************************************************************************************
		public static DriverTransactionTotals GetPaymentsToACHTotals(PersistenceManager pPM
																, DateTime pStart
																, DateTime pEnd
																, DriverPaymentStatus pStatus
																, bool pIncludeTest
																, bool pIncludeFailed) {
			StringBuilder sql = new StringBuilder();
			sql.Append("SELECT SUM(TaxiPassFee) TaxiPassFee ");
			sql.Append(" , SUM(TotalCharge - TaxiPassFee) DriverTotal ");
			sql.Append(" , SUM(TotalCharge) TotalCharge ");
			sql.Append(" FROM ( ");
			sql.Append(" select DRIVERPAYMENTS.Fare ");
			sql.Append(" + DRIVERPAYMENTS.Gratuity ");
			sql.Append(" + DRIVERPAYMENTS.AirportFee  ");
			sql.Append(" + DRIVERPAYMENTS.MiscFee  ");
			sql.Append(" + DRIVERPAYMENTS.WaitTime  ");
			sql.Append(" + DRIVERPAYMENTS.Tolls  ");
			sql.Append(" + DRIVERPAYMENTS.Parking  ");
			sql.Append(" + DRIVERPAYMENTS.Stops  ");
			sql.Append(" - DRIVERPAYMENTS.Discount AS TotalCharge ");
			sql.Append(" , DRIVERPAYMENTS.TaxiPassFee from DriverPayments  WITH (NOLOCK) ");
			sql.Append(" left join ACHDetail on ACHDetail.DriverPaymentID = DriverPayments.DriverPaymentID ");
			sql.Append(" where ChargeDate <= '");
			sql.Append(pEnd);
			sql.Append("' and ChargeDate >= '");
			sql.Append(pStart);
			sql.Append("'");

			if (!pIncludeFailed) {
				sql.Append(" and Failed=0 ");
			}
			if (!pIncludeTest) {
				sql.Append(" and Test=0 ");
			}

			if (pStatus == DriverPaymentStatus.paid) {
				sql.Append(" and ACHDetail.ACHPaidDate is not null ");
			} else if (pStatus == DriverPaymentStatus.unpaid) {
				sql.Append("and ACHDetail.ACHPaidDate is null ");
			}
			sql.Append(") Base ");
			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(DriverPayments), sql.ToString());
			qry.CommandTimeout = 120;

			DriverTransactionTotals oTotals = new DriverTransactionTotals();
			Entity[] oResults = DynamicEntities.DynamicDBCall(pPM, sql.ToString());
			if (oResults.Length > 0) {
				oTotals.Driver = decimal.Parse(oResults[0]["DriverTotal"].ToString());
				oTotals.TaxiPass = decimal.Parse(oResults[0]["TaxiPassFee"].ToString());
				oTotals.Transactions = decimal.Parse(oResults[0]["TotalCharge"].ToString());
			}
			return oTotals;
		}


		// **********************************************************************************************
		// Any changes to this formula must be reflected in GetPaymentsToACHTotals too!!!!
		// **********************************************************************************************
		public static EntityList<DriverPayments> GetPaymentsToAch(PersistenceManager pPM
																, DateTime pStart
																, DateTime pEnd
																, DriverPaymentStatus pStatus
																, bool pIncludeTest
																, bool pIncludeFailed) {
			StringBuilder sql = new StringBuilder();
			sql.Append("select DriverPayments.* from DriverPayments  WITH (NOLOCK) ");
			sql.Append(" left join ACHDetail on ACHDetail.DriverPaymentID = DriverPayments.DriverPaymentID ");
			sql.Append(" where ChargeDate <= '");
			sql.Append(pEnd);
			sql.Append("' and ChargeDate >= '");
			sql.Append(pStart);
			sql.Append("'");

			if (!pIncludeFailed) {
				sql.Append(" and Failed=0 ");
			}
			if (!pIncludeTest) {
				sql.Append(" and Test=0 ");
			}

			if (pStatus == DriverPaymentStatus.paid) {
				sql.Append(" and ACHDetail.ACHPaidDate is not null ");
			} else if (pStatus == DriverPaymentStatus.unpaid) {
				sql.Append("and ACHDetail.ACHPaidDate is null ");
			}
			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(DriverPayments), sql.ToString());
			qry.CommandTimeout = 120;
			EntityList<DriverPayments> oList = pPM.GetEntities<DriverPayments>(qry, QueryStrategy.Normal);

			mACHTagCount = 0;
			mACHTagTotal = 0;
			return oList;
		}

		// **********************************************************************************************
		//
		// **********************************************************************************************
		public static List<ACHBatchNumbers> GetUniqueACHBatchNumbers(Affiliate pAffiliate) {
			return GetUniqueACHBatchNumbers(pAffiliate.PersistenceManager, pAffiliate.AffiliateID);
		}

		public static List<ACHBatchNumbers> GetUniqueACHBatchNumbers(PersistenceManager pPM, long pAffiliateID) {
			AdoHelper oHelper = new AdoHelper();
			RdbKey aRdbKey = pPM.DataSourceResolver.GetDataSourceKey("Default") as RdbKey;
			oHelper = aRdbKey.AdoHelper;

			DriverPayments oPayment = pPM.GetNullEntity<DriverPayments>();
			StringBuilder sql = new StringBuilder();
			sql.Append("SELECT DISTINCT ACHDetail.ACHBatchNumber FROM ACHDetail with (NOLOCK) ");
			sql.Append(" inner join DriverPayments on DriverPayments.DriverPaymentID = ACHDetail.DriverPaymentID ");
			sql.Append(" WHERE AffiliateID = ");
			sql.Append(pAffiliateID);
			sql.Append(" ORDER BY ACHBatchNumber DESC");

			List<ACHBatchNumbers> batchList = new List<ACHBatchNumbers>();

			IDbConnection aConnection = oHelper.CreateDbConnection(true);
			aConnection.Open();
			using (aConnection) {
				IDbCommand aCommand = oHelper.CreateDbCommand(aConnection);
				aCommand.CommandText = sql.ToString();
				IDataReader aDataReader = aCommand.ExecuteReader();
				int i = 0;
				while (aDataReader.Read()) {
					if (!aDataReader.IsDBNull(0)) {
						string sTemp = aDataReader.GetValue(0).ToString();
						if (!sTemp.IsNullOrEmpty()) {
							string sVal = sTemp.Substring(0, 4) + "/";
							sVal += sTemp.Substring(4, 2) + "/";
							sVal += sTemp.Substring(6, 2) + " ";
							sVal += sTemp.Substring(8, 2);
							ACHBatchNumbers oBatch = new ACHBatchNumbers(sVal, sTemp);
							batchList.Add(oBatch);
						}
						i++;
					}
					if (i > 30) {
						break;
					}
				}
				aDataReader.Close();
			}

			return batchList;
		}


		// **********************************************************************************************
		//
		// **********************************************************************************************
		public static List<ACHBatchNumbers> GetUniqueACHBatchNumbers(TaxiPassRedeemerAccounts pRedeemer) {
			AdoHelper oHelper = new AdoHelper();
			RdbKey aRdbKey = pRedeemer.PersistenceManager.DataSourceResolver.GetDataSourceKey("Default") as RdbKey;
			oHelper = aRdbKey.AdoHelper;

			DriverPayments oPayment = pRedeemer.PersistenceManager.GetNullEntity<DriverPayments>();
			StringBuilder sql = new StringBuilder();
			sql.Append("SELECT DISTINCT ACHDetail.ACHBatchNumber ");
			sql.Append(" FROM ACHDetail WITH (NOLOCK) ");
			sql.Append(" INNER JOIN DriverPayments ON DriverPayments.DriverPaymentID = ACHDetail.DriverPaymentID  ");
			sql.Append(" INNER JOIN TaxiPassRedeemers on DriverPayments.TaxiPassRedeemerID = TaxiPassRedeemers.TaxiPassRedeemerID ");
			sql.Append(" where TaxiPassRedeemers.TaxiPassRedeemerAccountID = ");
			sql.Append(pRedeemer.TaxiPassRedeemerAccountID.ToString());
			sql.Append(" union ");

			sql.Append(" SELECT DISTINCT ACHDetail.ACHBatchNumber ");
			sql.Append(" FROM ACHDetail WITH (NOLOCK) ");
			sql.Append(" INNER JOIN DriverPayments ON DriverPayments.DriverPaymentID = ACHDetail.DriverPaymentID  ");
			sql.Append(" INNER JOIN DriverPaymentsPendingMatches ON DriverPayments.DriverPaymentID = DriverPaymentsPendingMatches.DriverPaymentID  ");
			sql.Append(" INNER JOIN TaxiPassRedeemers on DriverPaymentsPendingMatches.TaxiPassRedeemerID = TaxiPassRedeemers.TaxiPassRedeemerID ");
			sql.Append(" where TaxiPassRedeemers.TaxiPassRedeemerAccountID = ");
			sql.Append(pRedeemer.TaxiPassRedeemerAccountID.ToString());

			sql.Append(" ORDER BY ACHBatchNumber DESC ");



			List<ACHBatchNumbers> batchList = new List<ACHBatchNumbers>();

			IDbConnection aConnection = oHelper.CreateDbConnection(true);
			aConnection.Open();
			using (aConnection) {
				IDbCommand aCommand = oHelper.CreateDbCommand(aConnection);
				aCommand.CommandText = sql.ToString();
				IDataReader aDataReader = aCommand.ExecuteReader();
				int i = 0;
				while (aDataReader.Read()) {
					if (!aDataReader.IsDBNull(0)) {
						string sTemp = aDataReader.GetValue(0).ToString();
						if (!sTemp.IsNullOrEmpty() && sTemp.Length > 10) {
							string sVal = sTemp.Substring(0, 4) + "/";
							sVal += sTemp.Substring(4, 2) + "/";
							sVal += sTemp.Substring(6, 2) + " ";
							sVal += sTemp.Substring(8, 2);
							ACHBatchNumbers oBatch = new ACHBatchNumbers(sVal, sTemp);
							batchList.Add(oBatch);
						}
						i++;
					}
					if (i > 30) {
						break;
					}
				}
				aDataReader.Close();
			}

			return batchList;
		}

		// **********************************************************************************************
		//
		// **********************************************************************************************
		public static EntityList<DriverPayments> GetPaymentsByACHBatchNumber(PersistenceManager pPM, long pACHBatchNumber, string pABA, string pAccountNo) {
			StringBuilder sql = new StringBuilder();
			sql.Append("SELECT DriverPayments.* from DriverPayments  WITH (NOLOCK) ");
			sql.Append(" inner join ACHDetail on DriverPayments.DriverPaymentID = ACHDetail.DriverPaymentID ");
			sql.Append(" WHERE ACHBatchNumber = '");
			sql.Append(pACHBatchNumber);
			sql.Append("' and ACHBankCode = '");
			sql.Append(pABA);
			sql.Append("' and ACHBankAccountNumber = '");
			sql.Append(pAccountNo);
			sql.Append("'");
			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(DriverPayments), sql.ToString());
			return pPM.GetEntities<DriverPayments>(qry);
		}


		// ******************************************************************************************
		// Return the next sequential batch number for pBatchNo
		// pInput:  yyyymmdd
		// ******************************************************************************************
		public static short GetNextBatchNumber(PersistenceManager pPM, string pInput, short pMin) {
			short sNumber = 0;

			StringBuilder sql = new StringBuilder();
			sql.Append("SELECT MAX(Cast(Substring(cast(achbatchnumber as varchar(20)), 9, 2) as int) + 1) Num ");
			sql.Append(" FROM ACHDetail WITH (NOLOCK)");
			sql.AppendFormat(" WHERE ACHBatchNumber LIKE '{0}%'", pInput);

			Entity[] oResults = DynamicEntities.DynamicDBCall(pPM, "CabRideACH", sql.ToString());
			if (oResults != null) {
				if (oResults.Length > 0) {
					if (!oResults[0]["Num"].ToString().IsNullOrEmpty()) {
						sNumber = short.Parse(oResults[0]["Num"].ToString());
						if (sNumber < pMin) {
							sNumber = pMin;
						}
					} else {
						sNumber = pMin;
					}
				}
			}
			sNumber++;

			return sNumber;
		}

		public static int GetNextBatchNumber(PersistenceManager pPM, string pInput, string pOrgin) {
			int batchNo = 0;
			bool findBatch = true;
			while (findBatch) {
				batchNo++;
				// sBatchNo is in a legacy format, batchNo is passed to the payment processor(s)
				string sBatchNo = DateTime.Today.ToString("yyyyMMdd") + batchNo.ToString().PadLeft(2, '0') + pOrgin;
				try {
					if (!DriverPayments.ACHBatchNumberExists(pPM, Convert.ToInt64(sBatchNo))) {
						findBatch = false;
					}
				} catch (Exception ex) {
					Console.WriteLine(ex.Message);
				}
			}

			return batchNo;
		}


		// **********************************************************************************************
		//
		// **********************************************************************************************
		public static EntityList<DriverPayments> GetPaymentByACHBatchNumber(Affiliate pAffiliate, long pACHBatchNumber) {
			StringBuilder sql = new StringBuilder();
			sql.Append("SELECT DriverPayments.* from DriverPayments  WITH (NOLOCK) ");
			sql.Append(" inner join ACHDetail on DriverPayments.DriverPaymentID = ACHDetail.DriverPaymentID ");
			sql.Append(" WHERE ACHDetail.ACHBatchNumber = '");
			sql.Append(pACHBatchNumber);
			sql.Append("' and AffiliateID = ");
			sql.Append(pAffiliate.AffiliateID);
			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(DriverPayments), sql.ToString());
			return pAffiliate.PersistenceManager.GetEntities<DriverPayments>(qry);
		}

		// **********************************************************************************************
		//
		// **********************************************************************************************
		public static EntityList<DriverPayments> GetPaymentByACHBatchNumber(TaxiPassRedeemerAccounts pRedeemerAccount, long pACHBatchNumber) {

			StringBuilder sql = new StringBuilder();
			sql.Append(" SELECT DriverPayments.* ");
			sql.Append(" FROM DriverPayments WITH (NOLOCK) ");
			sql.Append(" INNER JOIN ACHDetail ON DriverPayments.DriverPaymentID = ACHDetail.DriverPaymentID ");
			sql.Append(" INNER JOIN TaxiPassRedeemers on DriverPayments.TaxiPassRedeemerID = TaxiPassRedeemers.TaxiPassRedeemerID ");
			sql.Append(" WHERE ACHBatchNumber = ' ");
			sql.Append(pACHBatchNumber.ToString());
			sql.Append("' and TaxiPassRedeemers.TaxiPassRedeemerAccountID = ");
			sql.Append(pRedeemerAccount.TaxiPassRedeemerAccountID.ToString());

			sql.Append(" union ");

			sql.Append(" SELECT DriverPayments.* ");
			sql.Append(" FROM DriverPayments WITH (NOLOCK) ");
			sql.Append(" INNER JOIN ACHDetail ON DriverPayments.DriverPaymentID = ACHDetail.DriverPaymentID ");
			sql.Append(" INNER JOIN DriverPaymentsPendingMatches ON DriverPayments.DriverPaymentID = DriverPaymentsPendingMatches.DriverPaymentID  ");
			sql.Append(" INNER JOIN TaxiPassRedeemers on DriverPaymentsPendingMatches.TaxiPassRedeemerID = TaxiPassRedeemers.TaxiPassRedeemerID ");
			sql.Append(" WHERE ACHBatchNumber = ' ");
			sql.Append(pACHBatchNumber.ToString());
			sql.Append("' and TaxiPassRedeemers.TaxiPassRedeemerAccountID = ");
			sql.Append(pRedeemerAccount.TaxiPassRedeemerAccountID.ToString());

			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(DriverPayments), sql.ToString());
			return pRedeemerAccount.PersistenceManager.GetEntities<DriverPayments>(qry);
		}

		// **********************************************************************************************
		//
		// **********************************************************************************************
		public DateTime? GetLastACHDate() {
			StringBuilder sql = new StringBuilder();
			sql.Append("SELECT top(1) ACHDetail.* from ACHDetail  WITH (NOLOCK) ");
			sql.Append(" inner join DriverPayments on DriverPayments.DriverPaymentID = ACHDetail.DriverPaymentID ");
			sql.Append(" WHERE AffiliateID = ");
			sql.Append(this.AffiliateID);
			sql.Append(" order by ACHPaidDate desc");
			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(ACHDetail), sql.ToString());
			return this.PersistenceManager.GetEntity<ACHDetail>(qry).ACHPaidDate;
		}


		// **********************************************************************************************
		// 
		// **********************************************************************************************
		public static EntityList<DriverPayments> GetReceiptRequests(PersistenceManager pPM, DateTime pStart, DateTime pEnd) {
			RdbQuery qry = new RdbQuery(typeof(DriverPayments));
			qry.AddSpan(EntityRelations.DriverPayments_ReceiptRequest);

			RdbSubquery subQry = new RdbSubquery(EntityRelationLink.From(EntityRelations.DriverPayments_ReceiptRequest, QueryDirection.ChildQuery));
			subQry.AddClause(ReceiptRequest.ReceiptRequestEntityColumn, EntityQueryOp.GE, pStart);
			subQry.AddClause(ReceiptRequest.ReceiptRequestEntityColumn, EntityQueryOp.LE, pEnd);
			qry.AddSubquery(subQry);

			return pPM.GetEntities<DriverPayments>(qry, QueryStrategy.DataSourceThenCache);
		}


		// **********************************************************************************************
		//
		// **********************************************************************************************
		public static EntityList<DriverPayments> GetReceiptRemindersDue(PersistenceManager pPM) {
			StringBuilder sql = new StringBuilder();
			DateTime dStart = DateTime.Today.AddDays(-1);
			sql.Append("SELECT DriverPayments.* FROM DriverPayments  WITH (NOLOCK) ");
			sql.Append("INNER JOIN ReceiptRequest ON DriverPayments.DriverPaymentID = ReceiptRequest.DriverPaymentID ");
			sql.Append(" WHERE ReceiptRequest.ReceiptRequest >= '");
			sql.Append(dStart);
			sql.Append("'");
			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(DriverPayments), sql.ToString());
			return pPM.GetEntities<DriverPayments>(qry, QueryStrategy.DataSourceThenCache);
		}


		// **********************************************************************************************
		//
		// **********************************************************************************************
		public static EntityList<DriverPayments> GetPendingReceiptChargeBacks(PersistenceManager pPM, bool pReporting) {
			SystemDefaults oDefaults = SystemDefaults.GetDefaults(pPM);
			DateTime date = DateTime.Today.AddDays(-oDefaults.RequestReceiptChargeDays);

			if (pReporting) {
				date = date.AddDays(-1);
			}
			StringBuilder sql = new StringBuilder();
			sql.Append("SELECT DriverPayments.* FROM DriverPayments  WITH (NOLOCK) ");
			sql.Append(" INNER JOIN ReceiptRequest ON DriverPayments.DriverPaymentID = ReceiptRequest.DriverPaymentID ");
			sql.Append(" WHERE ReceiptRequest.ReceiptRequestDue = '");
			sql.Append(date);
			sql.Append("' AND ReceiptRequest.ValidReceiptReceived is NULL AND DriverPayments.ReceiptChargedBack is NULL AND ReceiptRequest.InquiryFulfilled = 0");
			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(DriverPayments), sql.ToString());
			EntityList<DriverPayments> payList = pPM.GetEntities<DriverPayments>(qry, QueryStrategy.DataSourceThenCache);
			return payList;
		}

		public static EntityList<DriverPayments> GetPaymentByCardNo(Drivers pDriver, string pCardNumber, string pExcludeVoucherNo) {
			return GetPaymentByCardNo(pDriver, pCardNumber, pExcludeVoucherNo, null);
		}

		public static EntityList<DriverPayments> GetPaymentByCardNo(Drivers pDriver, string pCardNumber, string pExcludeVoucherNo, DateTime? pStart) {
			string sCardNo = CardNumberToHash(pCardNumber);

			RdbQuery qry = new RdbQuery(typeof(DriverPayments), DriverIDEntityColumn, EntityQueryOp.EQ, pDriver.DriverID);
			if (pStart.HasValue) {
				qry.AddClause(DriverPayments.ChargeDateEntityColumn, EntityQueryOp.GE, pStart);
			}
			qry.AddClause(NumberEntityColumn, EntityQueryOp.EQ, sCardNo);
			qry.AddClause(FailedEntityColumn, EntityQueryOp.EQ, false);
			qry.AddClause(FareEntityColumn, EntityQueryOp.GE, 0);
			qry.AddClause(TransNoEntityColumn, EntityQueryOp.NE, pExcludeVoucherNo);

			PersistenceManager oPM = pDriver.PersistenceManager;
			EntityList<DriverPayments> oList = oPM.GetEntities<DriverPayments>(qry);
			EntityList<DriverPayments> paymentList = new EntityList<DriverPayments>();
			foreach (DriverPayments oPay in oList) {
				if (oPay.DecryptCardNumber() == pCardNumber) {
					paymentList.Add(oPay);
				}
			}
			return paymentList;
		}

		public static EntityList<DriverPayments> GetPaymentByCardNo(AffiliateDrivers pDriver, string pCardNumber, string pExcludeVoucherNo) {
			return GetPaymentByCardNo(pDriver, pCardNumber, pExcludeVoucherNo, null);
		}

		public static EntityList<DriverPayments> GetPaymentByCardNo(AffiliateDrivers pDriver, string pCardNumber, string pExcludeVoucherNo, DateTime? pStart) {
			string sCardNo = CardNumberToHash(pCardNumber);

			RdbQuery qry = new RdbQuery(typeof(DriverPayments), AffiliateDriverIDEntityColumn, EntityQueryOp.EQ, pDriver.AffiliateDriverID);
			if (pStart.HasValue) {
				qry.AddClause(DriverPayments.ChargeDateEntityColumn, EntityQueryOp.GE, pStart);
			}
			qry.AddClause(NumberEntityColumn, EntityQueryOp.EQ, sCardNo);
			qry.AddClause(FailedEntityColumn, EntityQueryOp.EQ, false);
			qry.AddClause(FareEntityColumn, EntityQueryOp.GE, 0);
			qry.AddClause(TransNoEntityColumn, EntityQueryOp.NE, pExcludeVoucherNo);

			PersistenceManager oPM = pDriver.PersistenceManager;
			EntityList<DriverPayments> oList = oPM.GetEntities<DriverPayments>(qry);
			EntityList<DriverPayments> paymentList = new EntityList<DriverPayments>();
			foreach (DriverPayments oPay in oList) {
				if (oPay.DecryptCardNumber() == pCardNumber) {
					paymentList.Add(oPay);
				}
			}
			return paymentList;
		}

		public static EntityList<DriverPayments> GetPaymentsByCardNo(PersistenceManager pPM, string pCardNumber) {
			EntityList<DriverPayments> paymentList = new EntityList<DriverPayments>();
			if (pCardNumber.Length > 14) {
				string sCardNo = CardNumberToHash(pCardNumber);

				RdbQuery qry = new RdbQuery(typeof(DriverPayments), NumberEntityColumn, EntityQueryOp.EQ, sCardNo);
				qry.AddClause(FailedEntityColumn, EntityQueryOp.EQ, false);
				qry.AddClause(FareEntityColumn, EntityQueryOp.GE, 0);

				EntityList<DriverPayments> oList = pPM.GetEntities<DriverPayments>(qry);
				foreach (DriverPayments oPay in oList) {
					if (oPay.DecryptCardNumber() == pCardNumber) {
						paymentList.Add(oPay);
					}
				}
			} else {
				RdbQuery qry = new RdbQuery(typeof(DriverPayments), CardNumberDisplayEntityColumn, EntityQueryOp.EndsWith, pCardNumber);
				qry.AddClause(FailedEntityColumn, EntityQueryOp.EQ, false);
				qry.AddClause(FareEntityColumn, EntityQueryOp.GE, 0);
				qry.AddOrderBy(DriverPayments.ChargeDateEntityColumn);
				paymentList = pPM.GetEntities<DriverPayments>(qry);
			}
			return paymentList;

		}


		// ************************************************************************************************
		// Used by TaxiPassReceipt to lookup Trans by card # and date
		// ************************************************************************************************
		public static EntityList<DriverPayments> GetPaymentsByCardNoAndDate(PersistenceManager pPM
																						, string pCardNumber
																						, DateTime pDate) {
			EntityList<DriverPayments> paymentList = new EntityList<DriverPayments>();
			RdbQuery qry = new RdbQuery(typeof(DriverPayments), CardNumberDisplayEntityColumn, EntityQueryOp.EndsWith, pCardNumber);
			qry.AddClause(FailedEntityColumn, EntityQueryOp.EQ, false);
			qry.AddClause(FareEntityColumn, EntityQueryOp.GE, 0);
			qry.AddClause(DriverPayments.ChargeDateEntityColumn, EntityQueryOp.IsNotNull);
			qry.AddClause(DriverPayments.ChargeDateEntityColumn, EntityQueryOp.GE, pDate);
			qry.AddClause(DriverPayments.ChargeDateEntityColumn, EntityQueryOp.LE, pDate.AddDays(1).AddMilliseconds(-1));

			qry.AddOrderBy(DriverPayments.ChargeDateEntityColumn);
			paymentList = pPM.GetEntities<DriverPayments>(qry);

			return paymentList;

		}

		public static EntityList<DriverPayments> GetPaymentsByCardNoAndDate(PersistenceManager pPM, string pCardNumber, DateTime pStartDate, DateTime pEndDate) {
			EntityList<DriverPayments> paymentList = new EntityList<DriverPayments>();
			RdbQuery qry = new RdbQuery(typeof(DriverPayments), CardNumberDisplayEntityColumn, EntityQueryOp.EndsWith, pCardNumber);
			qry.AddClause(FailedEntityColumn, EntityQueryOp.EQ, false);
			qry.AddClause(FareEntityColumn, EntityQueryOp.GE, 0);
			qry.AddClause(DriverPayments.ChargeDateEntityColumn, EntityQueryOp.IsNotNull);
			qry.AddClause(DriverPayments.ChargeDateEntityColumn, EntityQueryOp.GE, pStartDate);
			qry.AddClause(DriverPayments.ChargeDateEntityColumn, EntityQueryOp.LE, pEndDate.AddDays(1).AddMilliseconds(-1));

			qry.AddOrderBy(DriverPayments.ChargeDateEntityColumn);
			paymentList = pPM.GetEntities<DriverPayments>(qry);

			return paymentList;

		}

		public static EntityList<DriverPayments> GetPaymentsByAmount(AffiliateDrivers pDriver, decimal pAmount, string pTransNoAsDigits) {
			RdbQuery qry = new RdbQuery(typeof(DriverPayments), DriverPayments.AffiliateDriverIDEntityColumn, EntityQueryOp.EQ, pDriver.AffiliateDriverID);
			qry.AddClause(DriverPayments.FareEntityColumn, EntityQueryOp.EQ, pAmount);
			qry.AddClause(DriverPayments.FailedEntityColumn, EntityQueryOp.EQ, false);
			qry.AddClause(DriverPayments.TestEntityColumn, EntityQueryOp.EQ, false);
			qry.AddClause(DriverPayments.DriverPaidDateEntityColumn, EntityQueryOp.IsNull);
			qry.AddSpan(EntityRelations.DriverPayments_ACHDetail);
			EntityList<DriverPayments> list = pDriver.PersistenceManager.GetEntities<DriverPayments>(qry, QueryStrategy.DataSourceThenCache);
			if (!string.IsNullOrEmpty(pTransNoAsDigits)) {
				return new EntityList<DriverPayments>(list.FindAll(p => p.DriverTotal == pAmount && p.TransNoToPhoneDigits == pTransNoAsDigits && p.ACHDetail.IsNullEntity));
			}
			return list;
		}

		public static EntityList<DriverPayments> GetPaymentsByAmount(Drivers pDriver, decimal pAmount, string pTransNoAsDigits) {
			RdbQuery qry = new RdbQuery(typeof(DriverPayments), DriverPayments.DriverIDEntityColumn, EntityQueryOp.EQ, pDriver.DriverID);
			qry.AddClause(DriverPayments.FareEntityColumn, EntityQueryOp.LE, pAmount);
			qry.AddClause(DriverPayments.FailedEntityColumn, EntityQueryOp.EQ, 0);
			qry.AddClause(DriverPayments.TestEntityColumn, EntityQueryOp.EQ, 0);
			qry.AddClause(DriverPayments.DriverPaidDateEntityColumn, EntityQueryOp.IsNull);
			qry.AddSpan(EntityRelations.DriverPayments_ACHDetail);
			EntityList<DriverPayments> list = pDriver.PersistenceManager.GetEntities<DriverPayments>(qry, QueryStrategy.DataSourceThenCache);
			return new EntityList<DriverPayments>(list.FindAll(p => p.DriverTotal == pAmount && p.TransNoToPhoneDigits == pTransNoAsDigits && p.ACHDetail.IsNullEntity));
		}

		public static EntityList<DriverPayments> GetPayments(Affiliate pAffiliate, EntityList<AffiliateDriverReferrals> pAffiliateDrivers, DateTime pStart, DateTime pEnd) {
			//TaxiPassCommon.TimeZoneConverter oTimeZone = new TimeZoneConverter();
			DateTime dStart = pStart;
			DateTime dEnd = pEnd;
			RdbQuery qry = new RdbQuery(typeof(DriverPayments), ChargeDateEntityColumn, EntityQueryOp.GE, dStart);
			qry.AddClause(ChargeDateEntityColumn, EntityQueryOp.LE, dEnd);
			qry.AddClause(DriverPayments.AffiliateIDEntityColumn, EntityQueryOp.EQ, pAffiliate.AffiliateID);

			List<long> driverIDs = new List<long>();
			foreach (AffiliateDriverReferrals oDriver in pAffiliateDrivers) {
				driverIDs.Add(oDriver.ReferredDriverID);
			}
			qry.AddClause(DriverPayments.AffiliateDriverIDEntityColumn, EntityQueryOp.In, driverIDs);

			qry.AddSpan(EntityRelations.Drivers_DriverPayments);
			qry.AddSpan(EntityRelations.Vehicles_DriverPayments);
			qry.AddSpan(EntityRelations.AffiliateDrivers_DriverPayments);
			qry.AddSpan(EntityRelations.DriverPayments_DriverPaymentResults);

			qry.CommandTimeout = 300;
			return pAffiliate.PersistenceManager.GetEntities<DriverPayments>(qry, QueryStrategy.DataSourceThenCache);
		}

		// **********************************************************************************************
		//
		// **********************************************************************************************
		public static List<ACHBatchNumbers> GetAffiliateDriverUniqueACHBatchNumbers(PersistenceManager pPM, long pAffiliateDriverID) {
			List<ACHBatchNumbers> achList = new List<ACHBatchNumbers>();

			AffiliateDrivers affDriver = AffiliateDrivers.GetDriver(pPM, pAffiliateDriverID);

			if (affDriver == null || affDriver.BankAccountNumber == null) {
				return achList;
			}

			AdoHelper oHelper = new AdoHelper();
			RdbKey aRdbKey = pPM.DataSourceResolver.GetDataSourceKey("Default") as RdbKey;
			oHelper = aRdbKey.AdoHelper;

			DriverPayments oPayment = pPM.GetNullEntity<DriverPayments>();
			StringBuilder sql = new StringBuilder();
			sql.Append("SELECT DISTINCT ");
			sql.Append(oPayment.ACHDetail.ACHBatchNumberColumn.ColumnName);
			sql.Append(" FROM ");
			sql.Append(oPayment.ACHDetail.Table.TableName.Split(':')[1]);
			sql.Append(" WHERE ");
			sql.Append(oPayment.ACHDetail.ACHBankCodeColumn.ColumnName);
			sql.Append(" = '");
			sql.Append(affDriver.BankCode);
			sql.Append("' AND ");
			sql.Append(oPayment.ACHDetail.ACHBankAccountNumberColumn.ColumnName);
			sql.Append(" = '");
			sql.Append(affDriver.BankAccountNumber);
			sql.Append("' ORDER BY ");
			sql.Append(oPayment.ACHDetail.ACHBatchNumberColumn.ColumnName);
			sql.Append(" DESC");

			IDbConnection aConnection = oHelper.CreateDbConnection(true);
			aConnection.Open();
			using (aConnection) {
				IDbCommand aCommand = oHelper.CreateDbCommand(aConnection);
				aCommand.CommandText = sql.ToString();
				IDataReader aDataReader = aCommand.ExecuteReader();
				int i = 0;
				while (aDataReader.Read()) {
					if (!aDataReader.IsDBNull(0)) {
						string sTemp = aDataReader.GetValue(0).ToString();

						string sVal = sTemp.Substring(0, 4) + "/";
						sVal += sTemp.Substring(4, 2) + "/";
						sVal += sTemp.Substring(6, 2) + " ";
						sVal += sTemp.Substring(8, 2);
						ACHBatchNumbers oBatch = new ACHBatchNumbers(sVal, sTemp);
						achList.Add(oBatch);
						i++;
					}
					if (i > 30) {
						break;
					}
				}
				aDataReader.Close();
			}

			return achList;
		}

		public static bool ACHBatchNumberExists(PersistenceManager pPM, long pACHBatchNumber) {
			RdbQuery qry = new RdbQuery(typeof(ACHDetail), ACHDetail.ACHBatchNumberEntityColumn, EntityQueryOp.EQ, pACHBatchNumber);
			qry.Top = 1;
			return !pPM.GetEntity<ACHDetail>(qry).IsNullEntity;
		}

		// **********************************************************************************************
		//
		// **********************************************************************************************
		public static EntityList<DriverPayments> GetPaymentsByACHBatchNumber(AffiliateDrivers pAffiliateDriver, long pACHBatchNumber) {
			StringBuilder sql = new StringBuilder();
			sql.Append("SELECT DriverPayments.* FROM DriverPayments  WITH (NOLOCK) ");
			sql.Append(" INNER JOIN ACHDetail ON ACHDetail.DriverPaymentID = DriverPayments.DriverPaymentID ");
			sql.Append(" WHERE ACHBatchNumber = '");
			sql.Append(pACHBatchNumber);
			sql.Append("' and ACHBankCode = '");
			sql.Append(pAffiliateDriver.BankCode);
			sql.Append("' AND ACHBankAccountNumber = '");
			sql.Append(pAffiliateDriver.BankAccountNumber);
			sql.Append("' ORDER BY ChargeDate");
			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(DriverPayments), sql.ToString());
			return pAffiliateDriver.PersistenceManager.GetEntities<DriverPayments>(qry);
		}


		// **********************************************************************************************
		//
		// **********************************************************************************************
		public static EntityList<DriverPayments> GetACHdPayments(PersistenceManager pPM, DateTime pStart, DateTime pEnd) {
			StringBuilder sql = new StringBuilder();
			sql.Append("SELECT DriverPayments.* FROM ACHDetail with (NOLOCK) ");
			sql.Append("INNER JOIN DriverPayments ON ACHDetail.DriverPaymentID = DriverPayments.DriverPaymentID ");
			sql.Append("WHERE ACHDetail.ACHPaidDate >= '");
			sql.Append(pStart);
			sql.Append("' and ACHDetail.ACHPaidDate <= '");
			sql.Append(pEnd.AddDays(1).AddSeconds(-1));
			sql.Append("' ORDER BY ACHDetail.ACHBatchNumber");
			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(DriverPayments), sql.ToString());
			return pPM.GetEntities<DriverPayments>(qry);
		}


		// **********************************************************************************************
		//
		// **********************************************************************************************
		public static EntityList<DriverPayments> GetACHdPayments(PersistenceManager pPM, DateTime pStart, DateTime pEnd, Boolean Sorted) {
			StringBuilder sql = new StringBuilder();
			sql.Append("SELECT DriverPayments.* FROM ACHDetail with (NOLOCK) ");
			sql.Append(" INNER JOIN DriverPayments ON ACHDetail.DriverPaymentID = DriverPayments.DriverPaymentID ");
			sql.Append(" WHERE ACHDetail.ACHPaidDate >= '");
			sql.Append(pStart);
			sql.Append("' and ACHDetail.ACHPaidDate <= '");
			sql.Append(pEnd.AddDays(1).AddSeconds(-1));
			sql.Append("' ORDER BY ACHDetail.ACHBankHolder, ACHDetail.ACHBankCode, ACHDetail.ACHBankAccountNumber, ACHDetail.ACHBatchNumber");
			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(DriverPayments), sql.ToString());
			return pPM.GetEntities<DriverPayments>(qry);
		}

		public static DriverPayments GetLastAffDriverPayment(PersistenceManager pPM, long pAffiliateDriverID) {
			RdbQuery qry = new RdbQuery(typeof(DriverPayments), DriverPayments.AffiliateDriverIDEntityColumn, EntityQueryOp.EQ, pAffiliateDriverID);
			qry.AddOrderBy(DriverPayments.ChargeDateEntityColumn, System.ComponentModel.ListSortDirection.Descending);
			qry.Top = 1;
			return pPM.GetEntity<DriverPayments>(qry);
		}

		public static DriverPayments GetLastDriverPayment(PersistenceManager pPM, long pDriverID) {
			RdbQuery qry = new RdbQuery(typeof(DriverPayments), DriverPayments.DriverIDEntityColumn, EntityQueryOp.EQ, pDriverID);
			qry.AddOrderBy(DriverPayments.ChargeDateEntityColumn, System.ComponentModel.ListSortDirection.Descending);
			qry.Top = 1;
			return pPM.GetEntity<DriverPayments>(qry);
		}

		public static DriverPayments GetLastPayment(Affiliate pAffiliate) {
			RdbQuery qry = new RdbQuery(typeof(DriverPayments), DriverPayments.AffiliateIDEntityColumn, EntityQueryOp.EQ, pAffiliate.AffiliateID);
			qry.AddOrderBy(DriverPayments.ChargeDateEntityColumn, System.ComponentModel.ListSortDirection.Descending);
			qry.Top = 1;
			return pAffiliate.PersistenceManager.GetEntity<DriverPayments>(qry);
		}

		public static DriverPayments GetLastAffiliateDriver(Drivers pDriver) {
			RdbQuery qry = new RdbQuery(typeof(DriverPayments), DriverPayments.DriverIDEntityColumn, EntityQueryOp.EQ, pDriver.DriverID);
			qry.AddClause(DriverPayments.AffiliateDriverIDEntityColumn, EntityQueryOp.IsNotNull);
			qry.AddClause(DriverPayments.TestEntityColumn, EntityQueryOp.EQ, false);
			qry.AddClause(DriverPayments.FailedEntityColumn, EntityQueryOp.EQ, false);
			qry.AddOrderBy(DriverPayments.ChargeDateEntityColumn, System.ComponentModel.ListSortDirection.Descending);
			qry.Top = 1;
			return pDriver.PersistenceManager.GetEntity<DriverPayments>(qry);
		}

		public static DriverPayments GetLastVehiclePayment(PersistenceManager pPM, long pVehicleID) {
			RdbQuery qry = new RdbQuery(typeof(DriverPayments), DriverPayments.VehicleIDEntityColumn, EntityQueryOp.EQ, pVehicleID);
			qry.AddOrderBy(DriverPayments.ChargeDateEntityColumn, System.ComponentModel.ListSortDirection.Descending);
			qry.Top = 1;
			return pPM.GetEntity<DriverPayments>(qry);
		}


		// **********************************************************************************************
		//
		// **********************************************************************************************
		public static EntityList<DriverPayments> GetPaymentsByBankInfo(PersistenceManager pPM, string pBankCode, string pAcctNo) {
			StringBuilder sql = new StringBuilder();
			sql.Append("SELECT DriverPayments.* from DriverPayments  WITH (NOLOCK) ");
			sql.Append(" inner join ACHDetail on DriverPayments.DriverPaymentID = ACHDetail.DriverPaymentID ");
			sql.Append(" WHERE ACHBankCode = '");
			sql.Append(pBankCode);
			sql.Append("' and ACHBankAccountNumber = '");
			sql.Append(pAcctNo);
			sql.Append("' and ACHError = 0");
			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(DriverPayments), sql.ToString());
			return pPM.GetEntities<DriverPayments>(qry);
		}

		// **********************************************************************************************
		//
		// **********************************************************************************************
		public static EntityList<DriverPayments> GetFailedACH(PersistenceManager pPM) {
			StringBuilder sql = new StringBuilder();
			sql.Append("SELECT DriverPayments.* from DriverPayments  WITH (NOLOCK) ");
			sql.Append(" inner join ACHDetail on DriverPayments.DriverPaymentID = ACHDetail.DriverPaymentID ");
			sql.Append(" WHERE ACHDetail.ACHError = 1");
			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(DriverPayments), sql.ToString());
			return pPM.GetEntities<DriverPayments>(qry);
		}

		public static EntityList<DriverPayments> GetRedeemedTransactions(TaxiPassRedeemers pRedeemer, DateTime pDateRedeemed) {
			RdbQuery qry = new RdbQuery(typeof(DriverPayments), DriverPayments.TaxiPassRedeemerIDEntityColumn, EntityQueryOp.EQ, pRedeemer.TaxiPassRedeemerID);
			qry.AddClause(DriverPayments.DriverPaidDateEntityColumn, EntityQueryOp.EQ, pDateRedeemed);
			qry.AddOrderBy(DriverPayments.TransNoEntityColumn);
			return pRedeemer.PersistenceManager.GetEntities<DriverPayments>(qry);
		}


		// **********************************************************************************************
		//
		// **********************************************************************************************
		public static int GetPaymentsCount(Affiliate pAffiliate, DateTime pStartDate, DateTime pEndDate) {
			StringBuilder sql = new StringBuilder();
			sql.Append("select COUNT(DriverPaymentID) from DriverPayments  WITH (NOLOCK) ");
			sql.Append(" where ChargeDate >= '");
			sql.Append(pStartDate.ToString());
			sql.Append("' and  ChargeDate <= '");
			sql.Append(pEndDate.ToString());
			sql.Append("' and AffiliateID = ");
			sql.Append(pAffiliate.AffiliateID);
			sql.Append(" and Test = 0 and Failed = 0");

			DataTable oTable = Utils.SQLHelper.GetRecords(pAffiliate.PersistenceManager, sql.ToString());
			if (oTable.Rows.Count > 0) {
				return Convert.ToInt32(oTable.Rows[0][0]);
			}
			return 0;
		}


		// **********************************************************************************************
		//
		// **********************************************************************************************
		public static int GetPaymentsCount(AffiliateDrivers pAffDriver, DateTime pStartDate) {
			StringBuilder sql = new StringBuilder();
			sql.Append("select COUNT(DriverPaymentID) from DriverPayments  WITH (NOLOCK) ");
			sql.Append(" where ChargeDate >= '");
			sql.Append(pStartDate.ToString());
			sql.Append("' and AffiliateDriverID = ");
			sql.Append(pAffDriver.AffiliateDriverID);
			sql.Append("and Test = 0 and Failed = 0");

			DataTable oTable = Utils.SQLHelper.GetRecords(pAffDriver.PersistenceManager, sql.ToString());
			if (oTable.Rows.Count > 0) {
				return Convert.ToInt32(oTable.Rows[0][0]);
			}
			return 0;
		}


		// **********************************************************************************************
		//
		// **********************************************************************************************
		public static int GetPaymentsCount(Drivers pDriver, DateTime pStartDate, DateTime pEndDate) {
			StringBuilder sql = new StringBuilder();
			sql.Append("select COUNT(DriverPaymentID) from DriverPayments WITH (NOLOCK) ");
			sql.Append(" where ChargeDate >= '");
			sql.Append(pStartDate.ToString());
			sql.Append("' and  ChargeDate <= '");
			sql.Append(pEndDate.ToString());
			sql.Append("' and DriverID = ");
			sql.Append(pDriver.DriverID);
			sql.Append("and Test = 0 and Failed = 0");

			DataTable oTable = Utils.SQLHelper.GetRecords(pDriver.PersistenceManager, sql.ToString());
			if (oTable.Rows.Count > 0) {
				return Convert.ToInt32(oTable.Rows[0][0]);
			}
			return 0;
		}


		public static EntityList<DriverPayments> GetPaymentsByDriverPaymentIDs(PersistenceManager pPM, List<long> pDriverPaymentIDs) {
			RdbQuery qry = new RdbQuery(typeof(DriverPayments), DriverPayments.DriverPaymentIDEntityColumn, EntityQueryOp.In, pDriverPaymentIDs);
			return pPM.GetEntities<DriverPayments>(qry);
		}

		public static EntityList<DriverPayments> GetPaymentsBySql(PersistenceManager pPM, string pSQL) {
			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(DriverPayments), pSQL);
			return pPM.GetEntities<DriverPayments>(qry);
		}


		// **********************************************************************************************
		//
		// **********************************************************************************************
		public static EntityList<DriverPayments> GetForRedeemerNotifications(TaxiPassRedeemerAccounts pRedeemerAccount, DateTime pAchDate) {
			StringBuilder redeemerSQL = new StringBuilder();
			redeemerSQL.Append("(select TaxiPassRedeemerID from TaxiPassRedeemers where TaxiPassRedeemerAccountID = ");
			redeemerSQL.Append(pRedeemerAccount.TaxiPassRedeemerAccountID);
			redeemerSQL.Append(")");

			StringBuilder sql = new StringBuilder();
			sql.Append("select DriverPayments.* from DriverPayments WITH (NOLOCK) ");
			sql.Append("left join ACHDetail on ACHDetail.DriverPaymentID = DriverPayments.DriverPaymentID ");
			sql.Append("left join DriverPaymentsPendingMatches on DriverPayments.DriverPaymentID = DriverPaymentsPendingMatches.DriverPaymentID ");
			sql.Append("where ACHDetail.ACHPaidDate > '");
			sql.Append(pAchDate.ToShortDateString());
			sql.Append("' and ACHPaidDate < '");
			sql.Append(pAchDate.AddDays(1).ToShortDateString());
			sql.Append("' and (DriverPayments.TaxiPassRedeemerID in ");
			sql.Append(redeemerSQL);
			sql.Append(" OR DriverPaymentsPendingMatches.TaxiPassRedeemerID in ");
			sql.Append(redeemerSQL);
			sql.Append(")");

			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(DriverPayments), sql.ToString());
			return pRedeemerAccount.PersistenceManager.GetEntities<DriverPayments>(qry);
		}


		// ***************************************************************************
		// Returns Affilaites ACH'd reqs for a given date
		// used by CAbRideAutoACH. TransactionReport
		// ***************************************************************************
		public static EntityList<DriverPayments> GetACHTransactionsForAffiliate(PersistenceManager pPM, long pAffiliateID, DateTime pDate) {
			StringBuilder sql = new StringBuilder();
			sql.Append("select DriverPayments.*  ");
			sql.Append(" FROM driverpayments  WITH (NOLOCK) ");
			sql.Append(" inner join ACHDetail on driverpayments.DriverPaymentID = ACHDetail.DriverPaymentID ");
			sql.Append(" left outer join AffiliateDrivers on driverpayments.AffiliateDriverID = AffiliateDrivers.AffiliateDriverID ");
			sql.Append(" where convert(varchar, ACHDetail.ACHPaidDate, 101) = ");
			sql.Append(" convert(dateTime, '");
			sql.Append(pDate.ToShortDateString());
			sql.Append("', 101) ");
			sql.Append(" and driverpayments.AffiliateID = ");
			sql.Append(pAffiliateID);
			sql.Append(" order by ChargeBack, AffiliateDrivers.Name ");

			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(DriverPayments), sql.ToString());
			return pPM.GetEntities<DriverPayments>(qry);
		}


		public static EntityList<DriverPayments> GetTransactionsToCashier(Affiliate pAffiliate, EntityList<AffiliateDrivers> pAffiliateDriversList, Vehicles pVehicle, string pTransNo, bool pDriverPaid, QueryStrategy pQueryStrategy) {

			//do not return items if we don't have at least 1 search criteria
			if (pAffiliateDriversList.Count == 0 && string.IsNullOrEmpty(pTransNo) && pVehicle.IsNullEntity) {
				return new EntityList<DriverPayments>();
			}


			RdbQuery qry = new RdbQuery(typeof(DriverPayments), CashierNameEntityColumn, EntityQueryOp.IsNull, true);
			if (pAffiliateDriversList.Count > 0) {
				List<long> driverIDs = (from p in pAffiliateDriversList
										select p.AffiliateDriverID).ToList();
				qry.AddClause(DriverPayments.AffiliateDriverIDEntityColumn, EntityQueryOp.In, driverIDs);
			}
			if (!string.IsNullOrEmpty(pTransNo)) {
				qry.AddClause(DriverPayments.TransNoEntityColumn, EntityQueryOp.EQ, pTransNo);
			}
			if (!pVehicle.IsNullEntity) {
				qry.AddClause(DriverPayments.VehicleIDEntityColumn, EntityQueryOp.EQ, pVehicle.VehicleID);
			}

			qry.AddClause(DriverPayments.TestEntityColumn, EntityQueryOp.EQ, false);
			qry.AddClause(DriverPayments.FailedEntityColumn, EntityQueryOp.EQ, false);

			qry.AddClause(DriverPayments.AffiliateIDEntityColumn, EntityQueryOp.EQ, pAffiliate.AffiliateID);

			return pAffiliate.PersistenceManager.GetEntities<DriverPayments>(qry, pQueryStrategy);
		}


		// *************************************************************************************
		// Return a list of vouchers that have been failed or paid but have not charged the card
		// *************************************************************************************
		public static EntityList<DriverPayments> GetFailedAndDriverPaid(PersistenceManager pPM) {

			string sql = @"SELECT DISTINCT DriverPayments.*
							FROM    DriverPayments
							LEFT OUTER JOIN DriverPaymentResults ON DriverPayments.DriverPaymentID = DriverPaymentResults.DriverPaymentID
																	AND DriverPaymentResults.TransType IN ('D', 'S')
							LEFT OUTER JOIN DriverPaymentAux ON DriverPayments.DriverPaymentID = DriverPaymentAux.DriverPaymentID
							WHERE   (DriverPaymentAux.WriteOff = 0
									 OR DriverPaymentAux.WriteOff IS NULL
									)
									AND AuthDate >= '1/1/2015'
									AND Fare > 0
									AND Failed = 1
									AND DriverPaid = 1
							ORDER BY TransNo";

			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(DriverPayments), sql.ToString());
			return pPM.GetEntities<DriverPayments>(qry, QueryStrategy.DataSourceThenCache);
		}
		#endregion

		public static EntityList<DriverPayments> GetVTSTransactions(Affiliate pAffiliate, string pCabNumber, string pNumService, string pCardNumber, string pBankApproval, decimal pAmount) {
			string number = CardNumberToHash(pCardNumber);
			Vehicles vehi = Vehicles.GetVehicle(pAffiliate, pCabNumber);

			RdbQuery qry = new RdbQuery(typeof(DriverPayments), AffiliateIDEntityColumn, EntityQueryOp.EQ, pAffiliate.AffiliateID);
			qry.AddClause(ReferenceNoEntityColumn, EntityQueryOp.EQ, pNumService);
			qry.AddClause(NumberEntityColumn, EntityQueryOp.EQ, number);
			qry.AddClause(VehicleIDEntityColumn, EntityQueryOp.EQ, vehi.VehicleID);

			RdbSubquery subQry = new RdbSubquery(EntityRelationLink.From(EntityRelations.DriverPayments_DriverPaymentResults, QueryDirection.ChildQuery));
			subQry.AddClause(DriverPaymentResults.MessageEntityColumn, EntityQueryOp.EQ, pBankApproval);
			subQry.AddClause(DriverPaymentResults.AmountEntityColumn, EntityQueryOp.EQ, pAmount);

			qry.AddSubquery(subQry);

			return pAffiliate.PersistenceManager.GetEntities<DriverPayments>(qry);
		}


		public static EntityList<DriverPayments> GetPaymentsToAchForAffiliateDriver(PersistenceManager pPM, long pAffiliateDriverID, DateTime pStart, DateTime pEnd) {
			string sql = @"SELECT DISTINCT
                                    DriverPayments.*
                            FROM    DriverPayments WITH (NOLOCK)
                            LEFT JOIN ACHDetail ON DriverPayments.DriverPaymentID = ACHDetail.DriverPaymentID
                            LEFT JOIN DriverPaymentAux ON DriverPayments.DriverPaymentID = DriverPaymentAux.DriverPaymentID
                            LEFT JOIN AffiliateDrivers ON AffiliateDrivers.AffiliateDriverID = DriverPayments.AffiliateDriverID
                            WHERE   DriverPayments.AffiliateDriverID = {0}
                                    AND ChargeDate >= '{1}'
                                    AND ChargeDate <= '{2}'
                                    AND ACHDetail.DriverPaymentID IS NULL
                                    AND Failed = 0
                                    AND DriverPayments.Test = 0
                                    AND (DriverPaymentAux.DoNotPay IS NULL
                                         OR DriverPaymentAux.DoNotPay = 0
                                        )
                                    AND AffiliateDrivers.Active = 1
                                    AND RedeemerStopPayment = 0
                                    AND AffiliateDrivers.FraudSuspected = 0
                                    AND DriverPaid = 0
                            ORDER BY ChargeDate";

			sql = string.Format(sql, pAffiliateDriverID, pStart, pEnd);
			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(DriverPayments), sql);
			return pPM.GetEntities<DriverPayments>(qry, QueryStrategy.DataSourceThenCache);
		}


		public static EntityList<DriverPayments> GetPaymentsForDriverInitiatedProcess(AffiliateDrivers pDriver) {
			StringBuilder sql = new StringBuilder();
			sql.Append("SELECT DriverPayments.* FROM DriverPayments  WITH (NOLOCK) ");
			sql.Append("left join ACHDetail on ACHDetail.DriverPaymentID = DriverPayments.DriverPaymentID ");
			sql.Append("left join AffiliateDrivers on Affiliatedrivers.AffiliateDriverID = DriverPayments.AffiliateDriverID ");
			sql.Append("where TaxiPassRedeemerID is null ");
			sql.AppendFormat("and DriverPayments.AffiliateDriverID = {0} ", pDriver.AffiliateDriverID);
			sql.Append("and ACHDetailID is null ");
			sql.Append("and ((CardSwiped = 1 and AutoPaySwipe = 1) or (CardSwiped = 0 and AutoPayManual = 1)) ");
			sql.Append("and Test = 0 and Failed = 0 and ChargeBack = 0 ");
			sql.Append("order by ChargeDate");

			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(DriverPayments), sql.ToString());
			return pDriver.PersistenceManager.GetEntities<DriverPayments>(qry, QueryStrategy.DataSourceThenCache);
		}

		public static EntityList<DriverPayments> GetSwipePaymentsForMasterVoucher(Drivers pDriver, AffiliateDrivers pAffDriver) {
			StringBuilder sql = new StringBuilder();
			sql.Append("SELECT DriverPayments.* FROM DriverPayments ");
			sql.Append("LEFT JOIN DriverPaymentAux on DriverPaymentAux.DriverPaymentID = DriverPayments.DriverPaymentID ");
			sql.AppendFormat("WHERE MasterVoucherID IS NULL AND DriverID = {0} AND DriverPaid = 0 AND Test = 0 AND Failed = 0 AND CardSwiped = 1 ", pDriver.DriverID);
			if (!pAffDriver.IsNullEntity) {
				sql.AppendFormat("AND AffiliateDriverID = {0} ", pAffDriver.AffiliateDriverID);
			}

			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(DriverPayments), sql.ToString());
			return pDriver.PersistenceManager.GetEntities<DriverPayments>(qry);
		}

		public static EntityList<DriverPayments> GetAllPaymentsForMasterVoucher(Drivers pDriver, AffiliateDrivers pAffDriver) {
			StringBuilder sql = new StringBuilder();
			sql.Append("SELECT DriverPayments.* FROM DriverPayments ");
			sql.Append("LEFT JOIN DriverPaymentAux on DriverPaymentAux.DriverPaymentID = DriverPayments.DriverPaymentID ");
			sql.AppendFormat("WHERE MasterVoucherID IS NULL AND DriverID = {0} AND DriverPaid = 0 AND Test = 0 AND Failed = 0 ", pDriver.DriverID);
			if (!pAffDriver.IsNullEntity) {
				sql.AppendFormat("AND AffiliateDriverID = {0} ", pAffDriver.AffiliateDriverID);
			}

			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(DriverPayments), sql.ToString());
			return pDriver.PersistenceManager.GetEntities<DriverPayments>(qry);
		}

		public PayAccountResult PayAccount(TaxiPassRedeemers pRedeemer, AffiliateDrivers pDriver, decimal pTotalPay, DateTime pDriverPaidDate) {
			PayAccountResult result = new PayAccountResult();

			if (this.Failed || this.Test) {
				return new PayAccountResult();
			}


			if (!this.ACHDetail.ACHPaidDate.HasValue) {
				TaxiPassRedeemerAccounts acct = this.DriverPaymentsPendingMatches.TaxiPassRedeemers.TaxiPassRedeemerAccounts;
				if (acct.IsNullEntity && pRedeemer.IsNullEntity) {
					//Check if we pay driver
					//					if (pDriver.TransCardCardNumber.IsNullOrEmpty()) {
					result.Error = true;
					result.ErrorMsg = "No PayCard on file to make payment to";
					//					} else {
					//						FundPayCard(pDriver);
					//					}
					return result;
				} else {

					this.TaxiPassRedeemerID = pRedeemer.TaxiPassRedeemerID;
					//if (!acct.TransCardNumber.IsNullOrEmpty()) {
					//    result = PayRedeemerAccount(pRedeemer, pTotalPay, pDriverPaidDate);
					//    if (!result.Error && !this.DriverPaid) {
					//        DriverPaid = true;
					//        DriverPaidDate = pDriverPaidDate;
					//    }

					//} else 
					if (pRedeemer.TaxiPassRedeemerAccounts.MarkAsACHd) {
						if (this.ACHDetail.IsNullEntity) {
							ACHDetail.Create(this);
						}
						this.ACHDetail.ACHPaidDate = DateTime.Now;
						this.ACHDetail.ACHPaidTo = pRedeemer.TaxiPassRedeemerAccounts.CompanyName;
						this.ACHDetail.Save();
					}
				}
			}
			try {
				this.Save();
			} catch (Exception) {
			}
			return result;
		}


		/// <summary>
		/// Opens the credit.
		/// </summary>
		/// <param name="pCardInfo">The application card information.</param>
		/// <returns></returns>
		/// <exception cref="System.Exception">
		/// Fare must not be 0
		/// </exception>
		public DriverPaymentResults OpenCredit(string pCardInfo) {
			#region Validations
			// Put in to trap bogus charge card calls
			if (this.Fare == 0) {
				throw new Exception("Fare must not be 0");
			}


			// only allow this one test card for Carmel
			string cardNo = this.DecryptCardNumber();
			if (CardUtils.IsTestCard(cardNo) && this.AffiliateID == 161 && (!cardNo.Equals("4000200011112222") || this.CVV != "166" || this.DecryptCardExpiration() != "122030")) {
				throw new Exception("Cannot charge Test Card");
			}

			if (AffiliateDriverID.HasValue) {
				if (!AffiliateDrivers.Active) {
					throw new Exception(string.Format("Affiliate Driver: {0}-{1},  Account Frozen cannot credit card", AffiliateDriverID.Value, AffiliateDrivers.Name));
				}
			} else {
				if (!Drivers.Active) {
					throw new Exception(string.Format("Driver: {0}-{1},  Account Frozen cannot credit card", DriverID, Drivers.Name));
				}
			}

			//failed until card is credited
			this.Failed = true;
			this.ChargeBack = true;

			// hg 2011-07/28 Force a save to prevent incorrect charges from happening
			// Always save first to avoid incorrect voucher numbers going to Credit card processor
			if (this.RowState == DataRowState.Added) {
				int tries = 0;
				while (tries < 3) {
					tries++;
					try {
						this.Save("Force Saved");
						break;
					} catch (Exception ex) {
						if (ex.Message.Equals("Query timeout expired")) {
							continue;
						}
						throw new Exception(ex.Message);
					}
				}
			}

			IsOpenCredit(true);

			MyTimeStats.Key = this.TransNo;


			//PreAuth card 1st if manual entry and zip code is present to ensure card is valid before charging
			if (!this.AuthDate.HasValue && !this.CardSwiped) {
				if (!string.IsNullOrEmpty(this.CardZIPCode)) {
					if (!this.IgnoreCardZIP.GetValueOrDefault(true)) {
						if (this.AuthAmount == 0 || AuthAmount < DriverTotal) {
							this.AuthAmount = DriverTotal; // TotalCharge;  use Driver total as PreAuth up's it by the TaxiPass Fee
						}
						DriverPaymentResults preAuthResult = PreAuthCard();
						if (!preAuthResult.Result.Equals("0")) {
							return preAuthResult;
						}
					}
				}
				MyTimeStats.Info.Add(new TimeStats.ElapsedInfo { Type = "PreAuthCard", Elapsed = DateTime.Now.Subtract(MyTimeStats.Start).TotalSeconds });
			}

			//CardProcessingFailureTypes oProcess = CardProcessingFailureTypes.OKToProcess;

			if (!this.ChargeDate.HasValue) {
				this.ChargeDate = DateTime.Now;
			}
			this.FraudFailure = false;


			//Check BlackList
			if (BlackListedCard(this.PersistenceManager, cardNo)) {
				throw new Exception("Card not accepted 6");
			}
			MyTimeStats.Info.Add(new TimeStats.ElapsedInfo { Type = "Black Listed Card Check", Elapsed = DateTime.Now.Subtract(MyTimeStats.Start).TotalSeconds });
			#endregion Validations

			DriverPaymentResults oPayResults = null;
			//TaxiPassCommon.Banking.CardProcessors oProcessor = TaxiPassCommon.Banking.CardProcessors.Verisign;

			//// 4/23/2013 probably obe
			//if (((CardUtils.IsTestCard(cardNo) && !string.IsNullOrEmpty(this.Drivers.MyAffiliate.MyVerisignAccount.EPayTestURL)) ||
			//		(!CardUtils.IsTestCard(cardNo) && !string.IsNullOrEmpty(this.Drivers.MyAffiliate.MyVerisignAccount.EPayProdURL)))) {
			//	oProcessor = TaxiPassCommon.Banking.CardProcessors.USAePay;
			//}


			this.VerisignAccountID = Utils.CreditCardUtils.GetDriverPaymentCardGatewayGroupsID(this.PersistenceManager, this.TransNo, 0);
			CreditCardChargeType chargeType = CreditCardChargeType.ECommerce;
			if (Platform != Platforms.GetRide.ToString()) {
				if (CardSwiped) {
					chargeType = CreditCardChargeType.Swipe;
				} else {
					chargeType = CreditCardChargeType.Manual;
				}
			}
			Gateway gway = new Gateway(Utils.CreditCardUtils.LoadGateWays(this.PersistenceManager, this.VerisignAccountID.Value, chargeType, CardType));

			string sComment = "Driver Swipe Entry";
			CreditCard cardData = new CreditCard();
			cardData.AffiliateDriverID = AffiliateDriverID;
			cardData.DriverID = DriverID;
			if (string.IsNullOrEmpty(pCardInfo)) {
				cardData.CardNo = cardNo;
				string sDate = DecryptCardExpiration();
				string sMonth = sDate.Left(2);
				string sYear = sDate.Right(2);
				cardData.CardExpiryAsMMYY = sMonth + sYear;
				sComment = "Driver Manual Entry";
			} else {
				cardData.Track2 = pCardInfo;
			}
			cardData.CVV = mCVV;
			cardData.AvsZip = this.CardZIPCode;
			cardData.AvsStreet = this.CardAddress;

			// Soft Descriptors
			cardData.SoftDescriptor = FillSoftDescriptor();


			List<ProcessorResponse> respList = gway.OpenCredit(new CCRequest(cardData, TotalCharge, sComment, TransNo, GetServiceType(), Platform, SalesClerk(), IsGetRideTrans));
			foreach (ProcessorResponse resp in respList) {
				oPayResults = ProcessResponse(resp, VerisignTrxTypes.Credit, TotalCharge, null);
			}

			// hg 4/28/11 force payment if we need to mark as AutoPaid regardless of payment type and platform
			// ca 9/7/11 Only set DriverPaid if the charge was successful (oPayResults.Result.Trim() == "0")
			//if (!this.DriverPaid && oPayResults.Result.Trim() == "0") {
			//	if (!this.AffiliateDrivers.DriverInitiatedPayCardPayment
			//		&& !this.Affiliate.DriverInitiatedPayCardPayment
			//		&& !SystemDefaults.GetDefaults(this.PersistenceManager).DriverInitiatedPayCardPayment) {

			//		if (!this.AffiliateDrivers.PaidByRedeemer) {
			//			SetDriverPaid(false);
			//			MyTimeStats.Info.Add(new TimeStats.ElapsedInfo { Type = "SetDriverPaid", Elapsed = DateTime.Now.Subtract(MyTimeStats.Start).TotalSeconds });
			//		}
			//	}
			//}


			//CheckFreezeAccount();

			this.Save();


			return oPayResults;
		}


		// **********************************************************************************************
		// Transfers the money to the Affiliate Driver
		// **********************************************************************************************
		//public TransCard.LOAD_CARD_FUNDED_RET FundTransCard(AffiliateDrivers pPayeeDriver) {

		//	decimal decAmount = 0;
		//	TransCard.LOAD_CARD_FUNDED_RET fundResults = new TransCard.LOAD_CARD_FUNDED_RET();
		//	fundResults.ERROR_FOUND = "Yes";
		//	fundResults.CURRENT_CARD_BALANCE = "";

		//	if (pPayeeDriver.IsNullEntity
		//		|| pPayeeDriver.TransCardNumber.IsNullOrEmpty()
		//		|| this.Affiliate.DriverInitiatedPayCardPayment
		//		|| this.Test
		//		|| this.DoNotPay
		//		|| this.Failed
		//		|| pPayeeDriver.RedeemerStopPayment) {

		//		fundResults.ERROR_FOUND = "No";
		//		fundResults.CURRENT_CARD_BALANCE = "";
		//		return fundResults;
		//	}

		//	ACHDetail achRec = ACHDetail.GetTransactionByDriverPaymentID(this.PersistenceManager, this.DriverPaymentID, QueryStrategy.DataSourceThenCache);
		//	if (!achRec.ACHPaidDate.HasValue) {
		//		if (achRec.IsNullEntity) {
		//			achRec = ACHDetail.Create(this);
		//		}
		//		try {
		//			if (pPayeeDriver.PayCardRealTimePay || pPayeeDriver.Affiliate.PayCardRealTimePay) {
		//				decAmount = this.DriverTotalPaid;

		//				if (decAmount == 0) {
		//					decAmount = DriverPaymentKioskAmount.Amount;
		//				}

		//				if (decAmount == 0) {
		//					fundResults.ERROR_FOUND = "Yes";
		//					achRec.ACHErrorReason = "No Fare amount found";

		//				} else {

		//					PayCard oPayCardTo = new PayCard();
		//					oPayCardTo.SetCardNumber(this.PersistenceManager, pPayeeDriver.TransCardNumber);
		//					oPayCardTo.RemoveFundsFromCard = false;

		//					TransCard oCard = new TransCard(oPayCardTo.TCDefaults.GetTransCardAccess());

		//					achRec.AmountPaid = decAmount;

		//					TransCard.CardInfo oFundingInfo = new TransCard.CardInfo();
		//					oFundingInfo.CardNumber = oPayCardTo.TCDefaults.FundingAccount;
		//					oFundingInfo.ComData = oPayCardTo.TCDefaults.ComData;

		//					fundResults = oCard.LoadCardFunds(oPayCardTo.TCCardInfo, oFundingInfo, decAmount, this.TransNo);
		//				}
		//			}

		//		} catch (Exception ex) {
		//			fundResults.ERROR_FOUND = "Yes";
		//			fundResults.CURRENT_CARD_BALANCE = "";
		//			string msg = ex.Message;
		//		}

		//		if (fundResults.ACCOUNT_NO.IsNullOrEmpty() || fundResults.ERROR_FOUND.StartsWith("y", StringComparison.CurrentCultureIgnoreCase)) {
		//			if (pPayeeDriver.PayCardRealTimePay || pPayeeDriver.Affiliate.PayCardRealTimePay) {
		//				try {
		//					TransCardPaymentFailures failedRec = TransCardPaymentFailures.Create(this);
		//					if (fundResults.ERROR_MESSAGE.IsNullOrEmpty()) {
		//						failedRec.ErrorMessage = "TransCard communication failure";
		//					} else {
		//						failedRec.ErrorMessage = fundResults.ERROR_MESSAGE;
		//					}
		//					failedRec.Save();
		//				} catch (Exception) {
		//					//don't blow up due to not being able to save this record
		//				}

		//			} else {
		//				achRec.ACHErrorReason = "Batch Required";
		//				achRec.BatchTime = DateTime.Now;
		//			}
		//		} else {
		//			achRec.ACHErrorReason = "Paid: " + this.DriverTotalPaid.ToString("C");
		//			achRec.AmountPaid = this.DriverTotalPaid;
		//		}
		//		achRec.ACHPaidDate = DateTime.Now;
		//		achRec.ACHPaidTo = pPayeeDriver.Name;
		//		achRec.ACHBankAccountNumber = "TransCard: " + pPayeeDriver.TransCardNumber.Right(5);

		//		if (pPayeeDriver.TransCardAdminNo.IsNullOrEmpty()) {
		//			pPayeeDriver.TransCardAdminNo = fundResults.ACCOUNT_NO;
		//			try {
		//				pPayeeDriver.Save();
		//			} catch (Exception) {
		//			}
		//		}
		//		achRec.TransCardAdminNo = pPayeeDriver.TransCardAdminNo;
		//		achRec.ExternalConfNo = fundResults.REFERENCE_NUMBER;
		//		//}
		//		if (!this.DriverPaid) {
		//			DriverPaid = true;
		//			DriverPaidDate = achRec.ACHPaidDate;
		//		}
		//	}
		//	int i = 0;

		//	// try to save 5 times
		//	while (i < 5) {
		//		try {
		//			this.Save();
		//			try {
		//				// Only send affirmative message if no errors
		//				if (fundResults.ERROR_FOUND.StartsWith("n", StringComparison.CurrentCultureIgnoreCase)) {
		//					CabRideEngine.Utils.Misc.SendTransCardPaymentSMS(this, pPayeeDriver, fundResults.CURRENT_CARD_BALANCE);
		//				} else {
		//					StringBuilder msg = new StringBuilder("Voucher ");
		//					msg.Append(this.TransNo);
		//					msg.Append(" received. ");
		//					msg.Append(this.DriverTotalPaid.ToString("C"));
		//					if (this.RedemptionFee.GetValueOrDefault(0) > 0) {
		//						msg.Append(" (" + this.DriverTotal.ToString("C") + " - " + this.RedemptionFee.Value.ToString("C") + " redemption fee)");
		//					}
		//					if (this.DriverFee.GetValueOrDefault(0) > 0) {
		//						msg.Append(" - (" + this.DriverFee.Value.ToString("C") + " driver fee)");
		//					}
		//					msg.Append(" will be summed with other vouchers and added to card *");
		//					msg.Append(pPayeeDriver.TransCardNumber.Right(5));

		//					msg.Append(". Another text will be sent when we add funds to your card.");
		//					pPayeeDriver.SendDriverMsg(msg.ToString(), false);
		//				}
		//			} catch (Exception) {
		//			}
		//			break;
		//		} catch (Exception) {
		//		}
		//		i++;
		//	}
		//	return fundResults;
		//}


	}


	public class PayAccountResult {
		public bool Error { get; set; }
		public string ErrorMsg { get; set; }
	}

	public enum DriverPaymentStatus {
		unpaid,
		paid,
		all
	}

	public class DriverTransactionTotals {
		public DriverTransactionTotals() {
			Transactions = 0;
			TaxiPass = 0;
			Driver = 0;
			DriverTotal = 0;
		}

		public string Name { get; set; }
		public decimal Transactions { get; set; } //= 0;
		public decimal TaxiPass { get; set; } //= 0;
		public decimal Driver { get; set; } //= 0;
		public decimal DriverTotal { get; set; } //= 0;
	}


	public class ACHBatchNumbers {

		private string mACHBatchNumber = "";
		private string mBatchNumber = "";

		public static string BatchNumberColumnName = "BatchNumber";
		public static string ACHBatchNumberColumnName = "ACHBatchNumber";


		public ACHBatchNumbers(string pBatchNumber, string pACHBatchNumber) {
			mBatchNumber = pBatchNumber;
			mACHBatchNumber = pACHBatchNumber;
		}

		public string ACHBatchNumber {
			get {
				return mACHBatchNumber;
			}
			set {
				mACHBatchNumber = value;
			}
		}

		public string BatchNumber {
			get {
				return mBatchNumber;
			}
			set {
				mBatchNumber = value;
			}
		}

		public bool TaxiTronic { get; set; }

	}

	#region Exception
	public class CardProcessingException : ApplicationException {
		private CardProcessingFailureTypes mFailure = CardProcessingFailureTypes.OKToProcess;
		public string MyMessage { get; set; }
		public int ErrorNo { get; set; }

		public CardProcessingException() {
		}

		public CardProcessingException(string pMessage, CardProcessingFailureTypes pFailure, Drivers pDriver, AffiliateDrivers pAffDriver, string pCardNumberHashed)
			: base(pMessage) {

			//usp_TotalChargesPerCardAndAffiliate cardTotals =new usp_TotalChargesPerCardAndAffiliate();
			//using (var conn = CabRideEngineDapper.Utils.SqlHelper.OpenSqlConnection()) {
			//	cardTotals = usp_TotalChargesPerCardAndAffiliate.Execute(conn, pDriver.AffiliateID.Value, DateTime.UtcNow, pCardNumberHashed);
			//}

			mFailure = pFailure;
			switch (pFailure) {
				case CardProcessingFailureTypes.MaxCardTransactions:
					//MyMessage = "Card exceeded number of times for taxi payment";
					if (pAffDriver.IsNullEntity) {
						MyMessage = "Max number of " + pDriver.GetCardChargedMaxNumber.ToString() + " transaction/Card/24 hour period has been reached";
					} else {
						MyMessage = "Max number of " + pAffDriver.GetCardChargedMaxNumber.ToString() + " transaction/Card/24 hour period has been reached";
					}
					ErrorNo = 1;
					break;

				case CardProcessingFailureTypes.MaxDriverTransactions:
					if (pAffDriver.IsNullEntity) {
						MyMessage = "Your " + pDriver.GetMaxTrxPerDay.ToString() + " Transactions/24 hour period has been reached";

					} else {
						MyMessage = "Your " + pAffDriver.GetMaxTrxPerDay.ToString() + " Transactions/24 hour period has been reached";
					}
					ErrorNo = 2;
					break;

				case CardProcessingFailureTypes.CardUsedTooSoon:
					if (pAffDriver.IsNullEntity) {
						MyMessage = "Same Card cannot be used within " + pDriver.GetMinTransTime.ToString() + " seconds of last transaction";
					} else {
						MyMessage = "Same Card cannot be used within " + pAffDriver.GetMinTransTime.ToString() + " seconds of last transaction";
					}
					ErrorNo = 3;
					break;

				case CardProcessingFailureTypes.MaxChargeAmountExceeded:
					if (pAffDriver.IsNullEntity) {
						MyMessage = "You cannot charge more than " + pDriver.GetChargeMaxAmount;
					} else {
						MyMessage = "You cannot charge more than " + pAffDriver.GetChargeMaxAmount;
					}
					ErrorNo = 4;
					break;

				case CardProcessingFailureTypes.MaxTransPerMonth:
					if (pAffDriver.IsNullEntity) {
						MyMessage = "Max number of " + pDriver.GetMaxTransPerMonth.ToString() + " transaction/30 Day period has been reached";
					} else {
						MyMessage = "Max number of " + pAffDriver.GetMaxTransPerMonth.ToString() + " transaction/30 Day period has been reached";
					}
					ErrorNo = 5;
					break;

				case CardProcessingFailureTypes.MaxTransPerMonthPerCard:
					if (pAffDriver.IsNullEntity) {
						MyMessage = "Max number of " + pDriver.GetMaxTransPerMonthPerCard.ToString() + " transaction/Card/30 Day period has been reached";
					} else {
						MyMessage = "Max number of " + pAffDriver.GetMaxTransPerMonthPerCard.ToString() + " transaction/Card/30 Day period has been reached";
					}
					ErrorNo = 6;
					break;

				case CardProcessingFailureTypes.MaxTransPerWeek:
					if (pAffDriver.IsNullEntity) {
						MyMessage = "Max number of " + pDriver.GetMaxTransPerWeek.ToString() + " transaction/7 Day period has been reached";
					} else {
						MyMessage = "Max number of " + pAffDriver.GetMaxTransPerWeek.ToString() + " transaction/7 Day period has been reached";
					}
					ErrorNo = 7;
					break;

				case CardProcessingFailureTypes.MaxTransPerWeekPerCard:
					if (pAffDriver.IsNullEntity) {
						MyMessage = "Max number of " + pDriver.GetMaxTransPerWeekPerCard.ToString() + " transaction/Card/7 Day period has been reached";
					} else {
						MyMessage = "Max number of " + pAffDriver.GetMaxTransPerWeekPerCard.ToString() + " transaction/Card/7 Day period has been reached";
					}
					ErrorNo = 8;
					break;

				case CardProcessingFailureTypes.AuthorizedByAnotherDriver:
					MyMessage = "Card is already PreAuthed by another driver";
					ErrorNo = 9;
					break;

				case CardProcessingFailureTypes.Authorized:
					MyMessage = "Card is already PreAuthed by you";
					ErrorNo = 10;
					break;

				case CardProcessingFailureTypes.MissingCardNumber:
					MyMessage = "Missing credit card number";
					ErrorNo = 12;
					break;


				case CardProcessingFailureTypes.MaxDayAmountPerCardExceeded:
					MyMessage = "Card will exceed daily charge limit allowed";
					ErrorNo = 14;
					break;

				case CardProcessingFailureTypes.MaxWeekAmountPerCardExceeded:
					MyMessage = "Card will exceed weekly charge limit allowed";
					ErrorNo = 14;
					break;

				case CardProcessingFailureTypes.MaxMonthAmountPerCardExceeded:
					MyMessage = "Card will exceed monthly charge limit allowed";
					ErrorNo = 16;
					break;

				default:
					MyMessage = pFailure.ToString();
					if (MyMessage.IsNullOrEmpty()) {
						MyMessage = pMessage;
					}
					ErrorNo = 11;
					break;
			}
		}

		public CardProcessingException(string pMessage, Exception pInner)
			: base(pMessage, pInner) {
		}

		public CardProcessingException(SerializationInfo pInfo, StreamingContext pContext)
			: base(pInfo, pContext) {

		}
		#endregion
		public CardProcessingFailureTypes FailureType {
			get {
				return mFailure;
			}
		}

	}

}
