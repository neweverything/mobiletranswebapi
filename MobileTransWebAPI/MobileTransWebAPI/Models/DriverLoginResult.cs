﻿using System;
using System.Collections.Generic;

namespace MobileTransWebAPI.Models {
	public class DriverLoginResult {
		public string ErrorMsg { get; set; }

		public long AffiliateID { get; set; }

		public long CashierID { get; set; }
		public long AffiliateDriverID {
			get {
				return CashierID;
			}
			set {
				CashierID = value;
			}
		}



		private long mDriverID;
		public long DriverID {
			get {
				return mDriverID;
			}
			set {
				mDriverID = value;
				DeviceID = value;
			}
		}
		public long RegisterID {
			get {
				return mDriverID;
			}
			set {
				DriverID = value;
			}
		}

		public string ServiceType { get; set; }
		public string CardDataEntryType { get; set; }

		public string DriverName { get; set; }
		public string AffiliateName { get; set; }
		public string AffiliateStreet { get; set; }
		public string AffiliateSuite { get; set; }
		public string AffiliateCity { get; set; }
		public string AffiliateState { get; set; }
		public string AffiliateZIPCode { get; set; }
		public string AffiliatePhone { get; set; }
		public string AffiliateLogo { get; set; }
		public string ISOLogo { get; set; }

		public bool PaidByRedeemer { get; set; }

		public string TaxiPayCell { get; set; }
		public string TaxiPayPin { get; set; }

		public long VehicleID { get; set; }
		public string VehicleNo { get; set; }

		public bool DispatchAvailable { get; set; }
		public bool Active { get; set; }

		public decimal StartingFare { get; set; }
		public decimal StartingAmount {
			get {
				return StartingFare;
			}
		}

		public decimal ChargeMaxSwipeAmount { get; set; }
		public decimal ChargeMaxAmount { get; set; }

		public string AdminCode { get; set; }
		public List<AffFees> TPFeesList { get; set; }

		public bool ChargeKioskSlip { get; set; }
		public bool FraudSuspected { get; set; }
		public bool HasBankInfo { get; set; }

		public int StoreForward { get; set; }
		public int SelfRedemptionMaxVoucherAge { get; set; }
		public string TimeZone { get; set; }

		private long mDeviceID;
		public long DeviceID {
			get {
				return mDeviceID;
			}
			set {
				if (mDriverID == 0) {
					mDriverID = value;
				}
				mDeviceID = value;
			}
		}

		public bool HasPayCard { get; set; }
		public string TransCardAdminNo { get; set; }

		public bool AskWorkingCity { get; set; }

		public bool SelfRedemption { get; set; }
		public decimal SelfRedemptionFee { get; set; }

		public bool FundPayCardDelayed { get; set; }
		public int FundPayCardDelayedMinusHours { get; set; }

		public decimal AdditionalTPPercentFee { get; set; }
		public bool DisplayLogout { get; set; }
		public bool AskForTip { get; set; }
		public bool AskForTolls { get; set; }
		public string ReceiptPrinter { get; set; }
		public string ReceiptFooterText { get; set; }
		public string ForceUpdateTime { get; set; }

		public string SSN { get; set; }
		public bool AskWhoPaysTPFee { get; set; }

		public string EMail { get; set; }
		public bool AllowReferrals { get; set; }

		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string City { get; set; }
		public string State { get; set; }
		public string Country { get; set; }
		public string ZIPCode { get; set; }
		public string Street { get; set; }
		public DateTime? DateOfBirth { get; set; }

		public string BankRouting { get; set; }
		public string BankAcctNo { get; set; }
		public string BankHolder { get; set; }

		public bool DriverCanSubmitCheck { get; set; }

		public HackLicense DriverLicense { get; set; }
		public HackLicense HackLicense { get; set; }

		public string TerminalID { get; set; }
		public string Sceret { get; set; }
	}
}