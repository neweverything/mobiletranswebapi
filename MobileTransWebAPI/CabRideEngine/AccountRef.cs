using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

namespace CabRideEngine {
	/// <summary>
	/// The developer class representing the AccountRef business object class.
	/// </summary>
	/// <remarks>
	/// <para>Place all application-specific business logic in this class.</para>
	/// <para>
	/// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
	/// and will not overwrite changes.
	/// </para>
	/// <para>
	/// This class inherits from the generated class, its companion in the pair of classes 
	/// defining a business object. Properties in the generated class may be overridden in 
	/// order to change the behavior or add business logic.
	/// </para>
	/// <para>
	/// This class is the final class in the inheritance chain for this business object. 
	/// It should not be inherited.
	/// </para>
	/// </remarks>
	[Serializable]
	public sealed partial class AccountRef : AccountRefDataRow {

		#region Constructors -- DO NOT MODIFY

		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new';
		// it is also required if the class is visible from a web service.
		// DO NOT REMOVE **
		private AccountRef() : this(null) { }

		/// <summary>
		/// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
		/// </summary>
		/// <param name="pRowBuilder"></param>
		public AccountRef(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static AccountRef Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      AccountRef aAccountRef = pManager.CreateEntity<AccountRef>();
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aAccountRef, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aAccountRef.AddToManager();
		//      return aAccountRef;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		// Add additional logic to your business object here...
		public static AccountRef GetCreate(Accounts pAccount, string pGroup, string pReference, string pDefaultValue) {

			//AffiliateRef rec = SelectRecord(pAffiliate, pGroup, pReference);
			//if (pAccount.PersistenceManager.DefaultQueryStrategy != QueryStrategy.Normal) {
			//	pAccount.PersistenceManager.DefaultQueryStrategy = QueryStrategy.Normal;
			//}
			AccountRef rec = GetByGroupReference(pAccount, pGroup, pReference);
			if (rec.IsNullEntity) {
				try {
					rec = pAccount.PersistenceManager.CreateEntity<AccountRef>();
					rec.AccountID = pAccount.AccountID;
					rec.ReferenceGroup = pGroup;
					rec.ReferenceKey = pReference;
					rec.ValueString = pDefaultValue;
					rec.AddToManager();
                    if (pAccount.RowState != DataRowState.Added) {
                        rec.Save(LoginManager.PrincipleUser.Identity.Name);
                    }
				} catch (Exception ex) {
					throw ex;
				}
			}
			return rec;
		}

		public static AccountRef GetByGroupReference(Accounts pAccount, string pGroup, string pReference) {
			return GetByGroupReference(pAccount.PersistenceManager, pAccount.AccountID, pGroup, pReference);
		}

		public static AccountRef GetByGroupReference(PersistenceManager pPM, long pAccountID, string pGroup, string pReference) {
			RdbQuery qry = new RdbQuery(typeof(AccountRef), ReferenceKeyEntityColumn, EntityQueryOp.EQ, pReference);
			qry.AddClause(AccountRef.ReferenceGroupEntityColumn, EntityQueryOp.EQ, pGroup);
			qry.AddClause(AccountRef.AccountIDEntityColumn, EntityQueryOp.EQ, pAccountID);
			AccountRef rec = pPM.GetEntity<AccountRef>(qry);
			return rec;
		}

	}

	#region EntityPropertyDescriptors.AccountRefPropertyDescriptor
	////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
	////    For example: 
	////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
	//public static partial class EntityPropertyDescriptors {
	//	public partial class AccountRefPropertyDescriptor : BaseEntityPropertyDescriptor {
	//    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
	//    public AdaptedPropertyDescriptor Age {
	//      get { return this.Get("Age"); }
	//    }
	//  }
	//} 
	#endregion

}