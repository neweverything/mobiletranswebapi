﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace CabRideEngineDapper.StoredProc {
	public class usp_TotalChargesPerCardAndAffiliate {
		public decimal TotalDay { get; set; }
		public decimal TotalWeek { get; set; }
		public decimal TotalMonth { get; set; }
		public decimal MaxDayAmount { get; set; }
		public decimal MaxWeekAmount { get; set; }
		public decimal MaxMonthAmount { get; set; }

		public static usp_TotalChargesPerCardAndAffiliate Execute(SqlConnection pConn, long pAffiliateID, DateTime pChargeDate, string pCardNumber) {
			return pConn.Query<usp_TotalChargesPerCardAndAffiliate>("usp_TotalChargesPerCardAndAffiliate", new { AffiliateID = pAffiliateID, StartDate = pChargeDate, CardNumber = pCardNumber }, commandType: CommandType.StoredProcedure).FirstOrDefault();
		}
	}
}
