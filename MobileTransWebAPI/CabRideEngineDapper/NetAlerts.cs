﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Dapper;
using TaxiPassCommon;
using CabRideEngineDapper.Utils;
using System.Data.SqlClient;

namespace CabRideEngineDapper {
    public partial class NetAlerts {

        private Drivers mDriver;

        public Drivers Drivers {
            get {
                if (mDriver == null) {
                    mDriver = Drivers.GetDriver(this.DriverID);
                }
                return mDriver;
            }
        }


        public static NetAlerts Create(Drivers pDriver) {
            NetAlerts rec = NetAlerts.Create();
            rec.DriverID = pDriver.DriverID;

            return rec;
        }


        public static NetAlerts Create(SqlConnection pConn, AffiliateDrivers pDriver) {
            NetAlerts rec = NetAlerts.Create();
            rec.AffiliateDriverID = pDriver.AffiliateDriverID;

            rec.DriverID = pConn.Query<long>("SELECT DriverID FROM dbo.Drivers WHERE Cell = @Cell", new { Cell = pDriver.Cell }).FirstOrDefault();

            return rec;
        }

        public static List<NetAlerts> GetNetAlerts(SqlConnection pConn, List<Affiliate> pAffiliateList, DateTime pStart, DateTime pEnd) {
            string sql = @"SELECT * FROM NetAlerts
							LEFT JOIN Drivers ON Drivers.DriverID = NetAlerts.DriverID
							WHERE (MessageDate >= @Start AND MessageDate <= @End)
								OR (ResponseDate >= @Start AND ResponseDate <= @End)
								AND AffiliateID IN @AffiliateIDs";

            List<long> affIdList = (from p in pAffiliateList select p.AffiliateID).ToList();

            return pConn.Query<NetAlerts>(sql, new { Start = pStart, End = pEnd, AffiliateIDs = affIdList }).ToList();
        }

        public static List<NetAlerts> GetNetAlerts(SqlConnection pConn, long pAffiliateID, DateTime pStart, DateTime pEnd) {
            string sql = @"SELECT * FROM NetAlerts
							LEFT JOIN Drivers ON Drivers.DriverID = NetAlerts.DriverID
							WHERE (MessageDate >= @Start AND MessageDate <= @End)
								OR (ResponseDate >= @Start AND ResponseDate <= @End)
								AND AffiliateID = @AffiliateID";

            return pConn.Query<NetAlerts>(sql, new { Start = pStart, End = pEnd, AffiliateID = pAffiliateID }).ToList();
        }
    }
}
