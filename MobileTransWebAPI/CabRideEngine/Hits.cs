﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Util;
using IdeaBlade.Persistence.Rdb;

namespace CabRideEngine {
  [Serializable]
  public sealed class Hits : HitsDataRow {
  
  #region Constructors -- DO NOT MODIFY
    // Do not create constructors for this class
    // Developers cannot create instances of this class with the "new" keyword
    // You should write a static Create method instead; see the "Suggested Customization" region
    // Please review the documentation to learn about entity creation and inheritance

    // This private constructor prevents accidental instance creation via 'new'
    private Hits() : this(null) {}

    // Typed DataSet constructor (needed by framework) - DO NOT REMOVE
    public Hits(DataRowBuilder pRowBuilder) 
      : base(pRowBuilder) {
    }

  #endregion
    
  #region Suggested Customizations
  
//    // Use this factory method template to create new instances of this class
//    public static Hits Create(PersistenceManager pManager,
//      ... your creation parameters here ...) { 
//
//      Hits aHits = pManager.CreateEntity<Hits>();
//
//      // if this object type requires a unique id and you have implemented
//      // the IIdGenerator interface implement the following line
//      pManager.GenerateId(aHits, // add id column here //);
//
//      // Add custom code here
//
//      aHits.AddToManager();
//      return aHits;
//    }

//    // Implement this method if you want your object to sort in a specific order
//    public override int CompareTo(Object pObject) {
//    }

//    // Implement this method to customize the null object version of this class
//    protected override void UpdateNullEntity() {
//    }

  #endregion
    
    // Add additional logic to your business object here...

      public static Hits Create(PersistenceManager pManager) {

          Hits oHit = (Hits)pManager.CreateEntity(typeof(Hits));
          pManager.GenerateId(oHit, Hits.HitIDEntityColumn);

          oHit.AddToManager();
          return oHit;
      }

      public static void IncrementCount(PersistenceManager pManager, string pPage, string pReferral, string pMarket, string pClickThru) {

          RdbQuery oQry = new RdbQuery(typeof(Hits), Hits.PageEntityColumn, EntityQueryOp.EQ, pPage);
          oQry.AddClause(Hits.ReferralEntityColumn, EntityQueryOp.EQ, pReferral);
          oQry.AddClause(Hits.MarketEntityColumn, EntityQueryOp.EQ, pMarket);
          oQry.AddClause(Hits.ClickThruEntityColumn, EntityQueryOp.EQ, pClickThru);

          Hits oHit = pManager.GetEntity<Hits>(oQry);

          if (oHit.IsNullEntity) {
              oHit = Hits.Create(pManager);
              oHit.Page = pPage;
              oHit.Referral = pReferral;
              oHit.Market = pMarket;
              oHit.ClickThru = pClickThru;
          }

          oHit.Count++;

          try {
              SaveResult oResult = oHit.Save();
              if (!oResult.Ok) {
                  throw oResult.Exception;
              }
          } catch {
              oHit.RemoveFromManager();
          }

      }
  }
  
}
