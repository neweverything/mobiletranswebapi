﻿using CabRideEngineDapper.Utils;
using Dapper;
using System.Linq;

namespace CabRideEngineDapper.StoredProc {
	public class usp_AffiliateDriverTransCounts {
		public int TodayCount { get; set; }
		public decimal TodayTotal { get; set; }
		public int ThisWeekCount { get; set; }
		public decimal ThisWeekTotal { get; set; }

		public static usp_AffiliateDriverTransCounts Execute(long pAffiliateId, long pAffiliateDriverId) {

			using (var conn = SqlHelper.OpenSqlConnection()) {
				return conn.Query<usp_AffiliateDriverTransCounts>("usp_AffiliateDriverTransCounts", new { AffiliateId = pAffiliateId, AffiliateDriverId = pAffiliateDriverId }, commandType: System.Data.CommandType.StoredProcedure).FirstOrDefault();
			}
		}
	}
}
