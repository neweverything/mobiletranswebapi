﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Util;
using IdeaBlade.Persistence.Rdb;
using System.Text;

namespace CabRideEngine {
    [Serializable]
    public sealed class CoverageZones : CoverageZonesDataRow {

		private bool mTag;

        #region Constructors -- DO NOT MODIFY
        // Do not create constructors for this class
        // Developers cannot create instances of this class with the "new" keyword
        // You should write a static Create method instead; see the "Suggested Customization" region
        // Please review the documentation to learn about entity creation and inheritance

        // This private constructor prevents accidental instance creation via 'new'
        private CoverageZones() : this(null) {
        }

        // Typed DataSet constructor (needed by framework) - DO NOT REMOVE
        public CoverageZones(DataRowBuilder pRowBuilder)
            : base(pRowBuilder) {
        }

        #endregion

        #region Suggested Customizations

        //    // Use this factory method template to create new instances of this class
        //    public static CoverageZones Create(PersistenceManager pManager,
        //      ... your creation parameters here ...) { 
        //
        //      CoverageZones aCoverageZones = pManager.CreateEntity<CoverageZones>();
        //
        //      // if this object type requires a unique id and you have implemented
        //      // the IIdGenerator interface implement the following line
        //      pManager.GenerateId(aCoverageZones, // add id column here //);
        //
        //      // Add custom code here
        //
        //      aCoverageZones.AddToManager();
        //      return aCoverageZones;
        //    }

        //    // Implement this method if you want your object to sort in a specific order
        //    public override int CompareTo(Object pObject) {
        //    }

        //    // Implement this method to customize the null object version of this class
        //    protected override void UpdateNullEntity() {
        //    }

        #endregion

        // Add additional logic to your business object here...


        // Add additional logic to your business object here...
        public static CoverageZones Create(PersistenceManager pPM) {
            CoverageZones oZone = null;

            try {
                // Creates the Country but it is not yet accessible to the application
                oZone = (CoverageZones)pPM.CreateEntity(typeof(CoverageZones));

                // Using the IdeaBlade Id Generation technique
                pPM.GenerateId(oZone, CoverageZones.CoverageZoneIDEntityColumn);

                // CRITICAL: must tell emp to add itself to its PM
                oZone.AddToManager();

                // Add custom code here

            } catch (Exception ex) {
                throw ex;
            }
            return oZone;
        }


		public override bool OtherAreas {
			get {
				return base.OtherAreas;
			}
			set {
				if (value) {
					RdbQuery oQry = new RdbQuery(typeof(CoverageZones), CoverageZones.OtherAreasEntityColumn, EntityQueryOp.EQ, value);
					oQry.AddClause(CoverageZones.CoverageZoneIDEntityColumn, EntityQueryOp.NE, this.CoverageZoneID);
					EntityList<CoverageZones> oZones = this.PersistenceManager.GetEntities<CoverageZones>(oQry);
					if (oZones.Count > 0) {
						throw new Exception("You can only have 1 Coverage Zone flagged as OtherArea");
					}
				}
				base.OtherAreas = value;
			}
		}

        public override string CoverageZone {
            get {
                return base.CoverageZone;
            }
            set {
                RdbQuery oQry = new RdbQuery(typeof(CoverageZones), CoverageZoneEntityColumn, EntityQueryOp.EQ, value.ToUpper());
                oQry.AddClause(CoverageZoneIDEntityColumn, EntityQueryOp.NE, CoverageZoneID);
                EntityList<CoverageZones> oZones = this.PersistenceManager.GetEntities<CoverageZones>(oQry);
                if (oZones.Count > 0) {
                    throw new Exception("Coverage Zone must be unique");
                }
                base.CoverageZone = value.ToUpper();
            }
        }

        public static bool GetOtherAreas(CoverageZones oZone) {
            return oZone.OtherAreas;
        }

        
        protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
            if (IsDeserializing)
                return;
            base.OnColumnChanging(pArgs);
            DoAudit(pArgs);
        }

        private void DoAudit(DataColumnChangeEventArgs pArgs) {
            if (!this.PersistenceManager.IsClient) {
                return;
            }
            if (this.RowState == DataRowState.Added) {
                return;
            }
            if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
                AuditRecord = CoverageZoneAudit.Create(this.PersistenceManager, this);
            }
            //AuditRecord[pArgs.Column.ColumnName] = pArgs.ProposedValue;
        }

		public bool Tag {
			get {
				return mTag;
			}
			set {
				mTag = value;
			}
		}

        public static CoverageZones GetOtherArea(PersistenceManager pPM) {
            RdbQuery qry = new RdbQuery(typeof(CoverageZones), OtherAreasEntityColumn, EntityQueryOp.EQ, true);
            return pPM.GetEntity<CoverageZones>(qry);
        }

		// ********************************************************************************
		// get all zones
		// ********************************************************************************
		public static EntityList<CoverageZones> GetCoverageZones(PersistenceManager pPM) {
			return GetData(pPM);
		}

		// ******************************************************************************
		// Call the proc
		// ******************************************************************************
		private static EntityList<CoverageZones> GetData(PersistenceManager pPM) {
			StringBuilder sql = new StringBuilder();
			sql.Append("usp_CoverageZonesGet ");
			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(CoverageZones), sql.ToString());
			EntityList<CoverageZones> oCoverageZones = pPM.GetEntities<CoverageZones>(qry);
			if (oCoverageZones.Count == 0) {
				oCoverageZones.Add(new CoverageZones());
			}
			return oCoverageZones;
		}
    }

}
