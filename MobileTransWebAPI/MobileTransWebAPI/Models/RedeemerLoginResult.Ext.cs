﻿using CabRideEngineDapper.Utils;
using Dapper;

namespace MobileTransWebAPI.Models {
    public partial class RedeemerLoginResult {

        public static RedeemerLoginResult GetRedeemerInfo(string pEmail) {
            string sql = @"SELECT  TaxiPassRedeemers.TaxiPassRedeemerAccountID
                                    , TaxiPassRedeemerID
                                    , CompanyName
                                    , Password
                                    , PayDriverPayCard
                                    , PayCardRealTimePay
                                    , TaxiPassRedeemers.Active RedeemerActive
                                    , TaxiPassRedeemerAccounts.Active RedeemerAccountActive
                                    , Salt
                            FROM    TaxiPassRedeemers
                            LEFT JOIN TaxiPassRedeemerAccounts ON TaxiPassRedeemerAccounts.TaxiPassRedeemerAccountID = TaxiPassRedeemers.TaxiPassRedeemerAccountID
                            WHERE   email = @Email";

            using (var conn = SqlHelper.OpenSqlConnection()) {
                var rec = conn.Get<RedeemerLoginResult>(sql, new { Email = pEmail });

                return rec;

            }
        }


    }
}