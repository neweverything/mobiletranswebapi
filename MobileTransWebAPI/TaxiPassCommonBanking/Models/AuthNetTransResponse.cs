﻿using System.Collections.Generic;

namespace TaxiPassCommon.Banking.Models {
	public class AuthNetTransResponse {

		public TransactionResponse transactionResponse { get; set; }
		public Messages messages { get; set; }

		public class Message {
			public string code { get; set; }
			public string description { get; set; }
		}

		public class TransactionResponse {
			public string responseCode { get; set; }
			public string authCode { get; set; }
			public string avsResultCode { get; set; }
			public string cvvResultCode { get; set; }
			public string cavvResultCode { get; set; }
			public string transId { get; set; }
			public string refTransID { get; set; }
			public string transHash { get; set; }
			public string testRequest { get; set; }
			public string accountNumber { get; set; }
			public string accountType { get; set; }
			public List<Message> messages { get; set; }
			public string transHashSha2 { get; set; }
			public int SupplementalDataQualificationIndicator { get; set; }
			public string networkTransId { get; set; }
		}

		public class Message2 {
			public string code { get; set; }
			public string text { get; set; }
		}

		public class Messages {
			public string resultCode { get; set; }
			public List<Message2> message { get; set; }
		}





	}
}
