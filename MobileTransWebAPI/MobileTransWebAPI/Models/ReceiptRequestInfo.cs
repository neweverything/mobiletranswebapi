﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileTransWebAPI.Models {
	public class ReceiptRequestInfo {
		public string TransNo { get; set; }
		public string ReceiptTo { get; set; }
		public string RequestTime { get; set; }
		public string JobID { get; set; }
		public long AffiliateID { get; set; }
		public bool IsNewCustomer { get; set; }
	}
}