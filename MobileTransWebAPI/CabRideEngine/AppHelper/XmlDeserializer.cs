﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace AppHelper {
	public class XmlDeserializer<T> {
		public static T Deserialize(string xmlFilePath) {
			using (FileStream stream = new FileStream(xmlFilePath, FileMode.Open)) {
				return Deserialize(stream);
			}
		}
		public static T Deserialize(Stream xmlFileStream) {
			return (T)Serializer(typeof(T)).Deserialize(xmlFileStream);
		}

		public static T Deserialize(TextReader textReader) {
			return (T)Serializer(typeof(T)).Deserialize(textReader);
		}

		public static T Deserialize(XmlReader xmlReader) {
			return (T)Serializer(typeof(T)).Deserialize(xmlReader);
		}

		public static T Deserialize(XmlReader xmlReader, string encodingStyle) {
			return (T)Serializer(typeof(T)).Deserialize(xmlReader, encodingStyle);
		}

		public static T Deserialize(XmlReader xmlReader, XmlDeserializationEvents events) {
			return (T)Serializer(typeof(T)).Deserialize(xmlReader, events);
		}

		public static T Deserialize(XmlReader xmlReader, string encodingStyle, XmlDeserializationEvents events) {
			return (T)Serializer(typeof(T)).Deserialize(xmlReader, encodingStyle, events);
		}

		private static XmlSerializer mSerializer = null;
		private static XmlSerializer Serializer(Type t) {
			if (mSerializer == null) {
				mSerializer = new XmlSerializer(t);
			}
			return mSerializer;
		}

	}
}
