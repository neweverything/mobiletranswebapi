﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileTransWebAPI.Models {
	public class SupportFeedback {
		public long DriverID { get; set; }
		public long AffiliateDriverID { get; set; }
		public string Feedback { get; set; }
		public string Key { get; set; }
	}
}