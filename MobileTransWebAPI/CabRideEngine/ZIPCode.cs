﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Collections.Generic;
using System.Collections.Specialized;

using IdeaBlade.Persistence;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;
using IdeaBlade.Rdb;

using CabRideEngine.Utils;

using TaxiPassCommon;
using System.Text;

namespace CabRideEngine {
    [Serializable]
    public sealed class ZIPCode : ZIPCodeDataRow {

        private const string TABLENAME = "CabRideEngine:ZIPCode";

        private ZIPCodeAudit mZIPCodeAudit;

        #region Constructors -- DO NOT MODIFY
        // Do not create constructors for this class
        // Developers cannot create instances of this class with the "new" keyword
        // You should write a static Create method instead; see the "Suggested Customization" region
        // Please review the documentation to learn about entity creation and inheritance

        // This private constructor prevents accidental instance creation via 'new'
        private ZIPCode()
            : this(null) {
        }

        // Typed DataSet constructor (needed by framework) - DO NOT REMOVE
        public ZIPCode(DataRowBuilder pRowBuilder)
            : base(pRowBuilder) {
        }

        #endregion

        #region Suggested Customizations

        //    // Use this factory method template to create new instances of this class
        //    public static ZIPCode Create(PersistenceManager pManager,
        //      ... your creation parameters here ...) { 
        //
        //      ZIPCode aZIPCode = (ZIPCode) pManager.CreateEntity(typeof(ZIPCode));
        //
        //      // if this object type requires a unique id and you have implemented
        //      // the IIdGenerator interface implement the following line
        //      pManager.GenerateId(aZIPCode, // add id column here //);
        //
        //      // Add custom code here
        //
        //      aZIPCode.AddToManager();
        //      return aZIPCode;
        //    }

        //    // Implement this method if you want your object to sort in a specific order
        //    public override int CompareTo(Object pObject) {
        //    }

        //    // Implement this method to customize the null object version of this class
        //    protected override void UpdateNullEntity() {
        //    }

        #endregion


        private string msError;

        // Add additional logic to your business object here...
        public static ZIPCode Create(PersistenceManager pPM, string sCountry, string sState) {
            ZIPCode oZIPCode = null;

            try {
                // Creates the ZIPCode but it is not yet accessible to the application
                oZIPCode = (ZIPCode)pPM.CreateEntity(typeof(ZIPCode));

                // Using the IdeaBlade Id Generation technique
                //pPM.GenerateId(emp, Employee.IdEntityColumn);
                oZIPCode.Country = sCountry;
                oZIPCode.State = sState;

                // CRITICAL: must tell emp to add itself to its PM
                oZIPCode.AddToManager();

                // Add custom code here
                //emp.FirstName = pFirstName; // new Employees always get a name
                //emp.LastName = pLastName;


            } catch (Exception ex) {
                throw ex;
            }
            return oZIPCode;
        }


        public override EntityValidationInfo Validate(EntityValidationContext pContext) {
            EntityValidationInfo oResult;

            ZIPCode = ZIPCode.ToUpper();
            City = StringUtils.ProperCase(City);
            County = StringUtils.ProperCase(County);

            if (ZIPCode == "") {
                oResult = new EntityValidationInfo(false, msError);
                return oResult;
            }

            if (!CityValidate()) {
                oResult = new EntityValidationInfo(false, msError);
            }

            if (!StateValidate()) {
                oResult = new EntityValidationInfo(false, msError);
            }

            CountyValidate();

            if (!CountryValidate()) {
                oResult = new EntityValidationInfo(false, msError);
            }


            oResult = new EntityValidationInfo(true, "");
            return oResult;
        }


        public bool ZIPCodeValidate() {
            bool bOK = true;
            ZIPCode = ZIPCode.ToUpper();
            if (ZIPCode == "") {
                msError = "ZIP Code cannot be blank";
                bOK = false;
            }
            return bOK;
        }

        public bool CityValidate() {
            bool bOK = true;
            City = StringUtils.ProperCase(City);
            if (City == "") {
                msError = "City cannot be blank";
                bOK = false;
            }
            return bOK;
        }

        public bool CountyValidate() {
            bool bOK = true;
            County = StringUtils.ProperCase(County);
            return bOK;
        }

        public bool StateValidate() {
            bool bOK = true;
            State = StringUtils.ProperCase(State);
            if (County == "") {
                msError = "State cannot be blank";
                bOK = false;
            }
            return bOK;
        }

        public bool CountryValidate() {
            bool bOK = true;
            if (Country == "") {
                msError = "Country cannot be blank";
                bOK = false;
            }
            return bOK;
        }

        public string ErrorString {
            get {
                return msError;
            }
        }

        protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
            if (IsDeserializing)
                return;
            base.OnColumnChanging(pArgs);
            DoZIPCodeAudit(pArgs);
        }

        private void DoZIPCodeAudit(DataColumnChangeEventArgs pArgs) {
            if (!this.PersistenceManager.IsClient) {
                return;
            }
            if ((mZIPCodeAudit == null) || (mZIPCodeAudit.RowState == DataRowState.Unchanged)) {
                mZIPCodeAudit = ZIPCodeAudit.Create(this.PersistenceManager, this);
            }
            mZIPCodeAudit[pArgs.Column.ColumnName] = pArgs.ProposedValue;
        }


        /// <summary>
        /// Return List of cities where we have coverage to pickup
        /// </summary>
        /// <param name="pPM"></param>
        /// <param name="pCountry"></param>
        /// <param name="pState"></param>
        /// <returns></returns>
        public static StringCollection GetWorkingCitiesForCountryState(PersistenceManager pPM, string pCountry, string pState) {
            return GetWorkingCitiesForCountryState(pPM, pCountry, pState, "");
        }
        public static StringCollection GetWorkingCitiesForCountryState(PersistenceManager pPM, string pCountry, string pState, string pCityFilter) {

            AdoHelper oHelper = new AdoHelper();
            RdbKey aRdbKey = pPM.DataSourceResolver.GetDataSourceKey("Default") as RdbKey;
            oHelper = aRdbKey.AdoHelper;

            string sqlSelect = "SELECT DISTINCT ZIPCode.City FROM ZIPCode " +
                                "INNER JOIN CoverageArea ON ZIPCode.ZIPCode = CoverageArea.ZIPCode " +
                                "INNER JOIN Rates ON CoverageArea.CoverageZoneID = Rates.PUCoverageZoneID " +
                                "WHERE ZIPCode.Country = '" + pCountry + "' AND ZIPCode.State = '" + pState + "' AND Rates.Rate > 0 ";

            //string sqlSelect = "SELECT DISTINCT ZIPCode.City FROM CoverageArea INNER JOIN ZIPCode ON CoverageArea.ZIPCode = ZIPCode.ZIPCode " +
            //                   "WHERE ZIPCode.State = '" + pState + "' and ZIPCode.Country = '" + pCountry + "' ";
            if (pCityFilter != "") {
                sqlSelect += "AND ZIPCode.City Like '" + StringUtils.Sanitizer(pCityFilter) + "%' ";
            }

            sqlSelect += "ORDER BY ZIPCode.City ASC";


            StringCollection workingCities = new StringCollection();
            IDbConnection aConnection = oHelper.CreateDbConnection(true);
            aConnection.Open();
            using (aConnection) {
                IDbCommand aCommand = oHelper.CreateDbCommand(aConnection);
                aCommand.CommandText = sqlSelect;
                IDataReader aDataReader = aCommand.ExecuteReader();
                while (aDataReader.Read()) {
                    workingCities.Add(aDataReader.GetString(0));
                }
                aDataReader.Close();
            }

            return workingCities;
        }

        public static StringCollection GetAllCitiesForCountryState(PersistenceManager pPM, string pCountry, string pState) {
            return GetAllCitiesForCountryState(pPM, pCountry, pState, "");
        }


        public static StringCollection GetAllCitiesForCountryState(PersistenceManager pPM, string pCountry, string pState, string pCityFilter) {
            AdoHelper oHelper = new AdoHelper();
            RdbKey aRdbKey = pPM.DataSourceResolver.GetDataSourceKey("Default") as RdbKey;
            oHelper = aRdbKey.AdoHelper;

            string sqlSelect = "SELECT DISTINCT City FROM ZIPCode " +
                               "WHERE State = '" + pState + "' and Country = '" + pCountry + "' ";
            if (pCityFilter != "") {
                sqlSelect += "and City Like '" + StringUtils.Sanitizer(pCityFilter) + "%' ";
            }

            sqlSelect += "ORDER BY City ASC";


            StringCollection allCities = new StringCollection();
            IDbConnection aConnection = oHelper.CreateDbConnection(true);
            aConnection.Open();
            using (aConnection) {
                IDbCommand aCommand = oHelper.CreateDbCommand(aConnection);
                aCommand.CommandText = sqlSelect;
                IDataReader aDataReader = aCommand.ExecuteReader();
                while (aDataReader.Read()) {
                    allCities.Add(aDataReader.GetString(0));
                }
                aDataReader.Close();
            }

            return allCities;
        }


		// ************************************************************************************************************
		// Public method
		// ************************************************************************************************************
		public static EntityList<ZIPCode> GetZipCodes(PersistenceManager pPM, string pCountry, string pState, string pCity, string pZipCode){
			return GetData(pPM, pCountry, pState, pCity, pZipCode);
		}

		// ************************************************************************************************************
		// Get the data
		// ************************************************************************************************************
		private static EntityList<ZIPCode> GetData(PersistenceManager pPM, string pCountry, string pState, string pCity, string pZipCode) {
			string sDelimiter = "";
			int iArgCount = 0;

			StringBuilder sql = new StringBuilder();
			sql.Append("usp_ZIPCodeGet ");
			if (!pCountry.IsNullOrEmpty()) {
				sql.AppendFormat(" @Country = '{0}'", pCountry);
				iArgCount++;
			}
			if (!pState.IsNullOrEmpty()) {
				if (iArgCount > 0) {
					sDelimiter = ", ";
				}
				sql.AppendFormat("{0}@State = '{1}'", sDelimiter, pState);
				iArgCount++;
			}
			if (!pCity.IsNullOrEmpty()) {
				if (iArgCount > 0) {
					sDelimiter = ", ";
				}
				sql.AppendFormat("{0}@City = '{1}'", sDelimiter, StringUtils.Sanitizer(pCity));
				iArgCount++;
			}
			if (!pZipCode.IsNullOrEmpty()) {
				if (iArgCount > 0) {
					sDelimiter = ", ";
				}
				sql.AppendFormat("{0}@ZipCode = '{1}'", sDelimiter, pZipCode);
				iArgCount++;
			}
			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(ZIPCode), sql.ToString());
			EntityList<ZIPCode> oData = pPM.GetEntities<ZIPCode>(qry);
			if (oData.Count == 0) {
				oData.Add(pPM.GetNullEntity<ZIPCode>());
			}
			return oData;
		}


        public static StringCollection GetZipCodesForCountryStateCity(PersistenceManager pPM, string pCountry, string pState, string pCity) {
            AdoHelper oHelper = new AdoHelper();
            RdbKey aRdbKey = pPM.DataSourceResolver.GetDataSourceKey("Default") as RdbKey;
            oHelper = aRdbKey.AdoHelper;

            string sqlSelect = "SELECT ZIPCode FROM ZIPCode " +
                               "WHERE State = '" + pState + "' and Country = '" + pCountry + "' " +
                                "and City = '" + StringUtils.Sanitizer(pCity) + "' ";

            sqlSelect += "ORDER BY ZIPCode ASC";


            StringCollection allZips = new StringCollection();
            IDbConnection aConnection = oHelper.CreateDbConnection(true);
            aConnection.Open();
            using (aConnection) {
                IDbCommand aCommand = oHelper.CreateDbCommand(aConnection);
                aCommand.CommandText = sqlSelect;
                IDataReader aDataReader = aCommand.ExecuteReader();
                while (aDataReader.Read()) {
                    allZips.Add(aDataReader.GetString(0).Trim());
                }
                aDataReader.Close();
            }

            return allZips;

        }

        public static ZIPCode GetZipCodeInfo(PersistenceManager pPM, string pZipCode) {
            RdbQuery qry = new RdbQuery(typeof(ZIPCode), CabRideEngine.ZIPCode.ZIPCodeEntityColumn, EntityQueryOp.EQ, pZipCode);
            return pPM.GetEntity<ZIPCode>(qry);
        }

    }

}
