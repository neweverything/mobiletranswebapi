﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using System.Web.Script.Serialization;

using IdeaBlade.Persistence;

using TaxiPassCommon;

using CabRideEngine;
using CabRideEngine.Utils;

using MobileTransWebAPI.Models;

namespace MobileTransWebAPI.Controllers {
	public class StatesController : ApiController {

		public StatesResult Get(string pCountryCode) {
			StatesResult result = new StatesResult();

			JavaScriptSerializer js = new JavaScriptSerializer();

			try {
				PersistenceManager oPM = WebApiApplication.UserPM;
				if (oPM == null) {
					result.ErrorMsg = "Could not connect to database";
					return result;
				}

				EntityList<States> stateList = States.GetStatesByCountry(oPM, pCountryCode);
				result.StateNames = (from p in stateList
									 orderby p.StateName
									 select new State {
										 StateName = p.StateName,
										 StateCode = p.State
									 }).ToList();

			} catch (Exception ex) {
				result.ErrorMsg = ex.Message;
			}

			return result;

		}

	}
}
