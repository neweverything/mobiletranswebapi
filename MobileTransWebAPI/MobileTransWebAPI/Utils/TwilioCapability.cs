﻿using JWT;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;

namespace Twilio {
    public class TwilioCapability {
        private string _accountSid;

        private string _authToken;

        private string _clientName;

        private List<ScopeURI> _scopes;

        public TwilioCapability(string accountSid, string authToken) {
            this._accountSid = accountSid;
            this._authToken = authToken;
            this._scopes = new List<ScopeURI>();
        }

        private void Allow(string service, string capability, object prms) {
            this._scopes.Add(new ScopeURI(service, capability, prms));
        }

        public void AllowClientIncoming(string clientName) {
            if (Regex.IsMatch(clientName, "\\W")) {
                throw new ArgumentException("Only alphanumeric characters allowed in client name.");
            }
            if (string.IsNullOrEmpty(clientName)) {
                throw new ArgumentException("Client name must not be a zero length string or NULL.");
            }
            this._clientName = clientName;
            this.Allow("client", "incoming", null);
        }

        public void AllowClientOutgoing(string applicationSid) {
            this.AllowClientOutgoing(applicationSid, null);
        }

        public void AllowClientOutgoing(string applicationSid, object appParams) {
            this.Allow("client", "outgoing", new { appSid = applicationSid, appParams = appParams });
        }

        private static int ConvertToUnixTimestamp(DateTime date) {
            DateTime dateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            return (int)Math.Floor((date - dateTime).TotalSeconds);
        }

        public string GenerateToken() {
            return this.GenerateToken(3600);
        }

        public string GenerateToken(int ttlSeconds) {
            string str = string.Join(" ", (
                from s in this._scopes
                select s.ToString(this._clientName)).ToArray<string>());
            string str1 = this._accountSid;
            DateTime utcNow = DateTime.UtcNow;
            var variable = new { iss = str1, exp = TwilioCapability.ConvertToUnixTimestamp(utcNow.AddSeconds((double)ttlSeconds)), scope = str };
            return JsonWebToken.Encode(variable, this._authToken, 0);
        }
    }
}