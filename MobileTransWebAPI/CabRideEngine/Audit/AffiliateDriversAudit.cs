﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Persistence;
using IdeaBlade.Util;

using TaxiPassCommon;

namespace CabRideEngine {
    /// <summary>
    /// The developer class representing the AffiliateDriversAudit business object class.
    /// </summary>
    /// <remarks>
    /// <para>Place all application-specific business logic in this class.</para>
    /// <para>
    /// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
    /// and will not overwrite changes.
    /// </para>
    /// <para>
    /// This class inherits from the generated class, its companion in the pair of classes 
    /// defining a business object. Properties in the generated class may be overridden in 
    /// order to change the behavior or add business logic.
    /// </para>
    /// <para>
    /// This class is the final class in the inheritance chain for this business object. 
    /// It should not be inherited.
    /// </para>
    /// </remarks>
    [Serializable]
    public sealed partial class AffiliateDriversAudit : AffiliateDriversAuditDataRow {

        #region Constructors -- DO NOT MODIFY

        // Do not create constructors for this class
        // Developers cannot create instances of this class with the "new" keyword
        // You should write a static Create method instead; see the "Suggested Customization" region
        // Please review the documentation to learn about entity creation and inheritance

        // This private constructor prevents accidental instance creation via 'new';
        // it is also required if the class is visible from a web service.
        // DO NOT REMOVE **
        private AffiliateDriversAudit()
            : this(null) {
        }

        /// <summary>
        /// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
        /// </summary>
        /// <param name="pRowBuilder"></param>
        public AffiliateDriversAudit(DataRowBuilder pRowBuilder)
            : base(pRowBuilder) {
        }

        #endregion

        #region Suggested Customizations

        //    // Use this factory method template to create new instances of this class
        //    public static AffiliateDriversAudit Create(PersistenceManager pManager,
        //      ... your creation parameters here ...) { 
        //
        //      AffiliateDriversAudit aAffiliateDriversAudit = pManager.CreateEntity<AffiliateDriversAudit>();
        //
        //      // if this object type requires a unique id and you have implemented
        //      // the IIdGenerator interface implement the following line
        //      pManager.GenerateId(aAffiliateDriversAudit, // add id column here //);
        //
        //      // Add custom code here
        //
        //      aAffiliateDriversAudit.AddToManager();
        //      return aAffiliateDriversAudit;
        //    }

        //    // Implement this method if you want your object to sort in a specific order
        //    public override int CompareTo(Object pObject) {
        //    }

        //    // Implement this method to customize the null object version of this class
        //    protected override void UpdateNullEntity() {
        //    }

        #endregion

        // Add additional logic to your business object here...
        public static AffiliateDriversAudit Create(PersistenceManager pManager, AffiliateDrivers pAffiliateDrivers) {
            AffiliateDriversAudit oAudit = pManager.CreateEntity<AffiliateDriversAudit>();
            pManager.GenerateId(oAudit, AffiliateDriversAudit.AffiliateDriverHistoryIDEntityColumn);

            for (int i = 0; i < pAffiliateDrivers.Table.Columns.Count; i++) {
                if (oAudit.Table.Columns.Contains(pAffiliateDrivers.Table.Columns[i].ColumnName)) {
                    oAudit[pAffiliateDrivers.Table.Columns[i].ColumnName] = pAffiliateDrivers[i];
                }
            }
            oAudit.AddToManager();

            return oAudit;
        }

        public override string TransCardNumber {
            get {
                return base.TransCardNumber.DecryptIceKey();
            }
            //set {
            //    base.TransCardNumber = value.EncryptIceKey();
            //    base.TransCardCardNumber = CustomerCardNumberDisplay(value);
            //}
        }

        public static AffiliateDriversAudit GetFirstRecord(AffiliateDrivers pAffiliateDrivers) {
            RdbQuery qry = new RdbQuery(typeof(AffiliateDriversAudit), AffiliateDriversAudit.AffiliateDriverIDEntityColumn, EntityQueryOp.EQ, pAffiliateDrivers.AffiliateDriverID);
            qry.AddOrderBy(AffiliateDriversAudit.RowVersionEntityColumn);
            qry.Top = 1;
            return pAffiliateDrivers.PersistenceManager.GetEntity<AffiliateDriversAudit>(qry);
        }

        public static EntityList<AffiliateDriversAudit> GetDriver(AffiliateDrivers pDriver) {
            RdbQuery qry = new RdbQuery(typeof(AffiliateDriversAudit), AffiliateDriversAudit.AffiliateDriverIDEntityColumn, EntityQueryOp.EQ, pDriver.AffiliateDriverID);
            qry.AddOrderBy(RowVersionEntityColumn);
            return pDriver.PersistenceManager.GetEntities<AffiliateDriversAudit>(qry);
        }

        public static EntityList<AffiliateDriversAudit> GetDriversWithPayCard(PersistenceManager pPM) {
            RdbQuery qry = new RdbQuery(typeof(AffiliateDriversAudit), AffiliateDriversAudit.TransCardCardNumberEntityColumn, EntityQueryOp.IsNotNull, true);
            qry.AddClause(AffiliateDriversAudit.TransCardCardNumberEntityColumn, EntityQueryOp.NE, "");
            qry.AddOrderBy(AffiliateDriversAudit.AffiliateIDEntityColumn, System.ComponentModel.ListSortDirection.Ascending);
            return pPM.GetEntities<AffiliateDriversAudit>(qry, QueryStrategy.DataSourceThenCache);
        }

        public static EntityList<AffiliateDriversAudit> GetDriversByPayCard(PersistenceManager pPM, string pPayCard) {
            EntityList<AffiliateDriversAudit> driverList = new EntityList<AffiliateDriversAudit>();
            if (string.IsNullOrEmpty(pPayCard)) {
                return driverList;
            }

            string sCard = pPayCard.CardNumberDisplay();

            RdbQuery qry = new RdbQuery(typeof(AffiliateDriversAudit), AffiliateDriversAudit.TransCardCardNumberEntityColumn, EntityQueryOp.EQ, sCard);
            EntityList<AffiliateDriversAudit> list = pPM.GetEntities<AffiliateDriversAudit>(qry, QueryStrategy.DataSourceThenCache);
            foreach (AffiliateDriversAudit rec in list) {
                if (rec.TransCardNumber.Equals(pPayCard)) {
                    driverList.Add(rec);
                }
            }
            return driverList;
        }

        public static AffiliateDriversAudit GetByPayCardAdminNo(PersistenceManager pPM, string pPayCard) {
            if (string.IsNullOrEmpty(pPayCard)) {
                return pPM.GetNullEntity<AffiliateDriversAudit>();
            }

            RdbQuery qry = new RdbQuery(typeof(AffiliateDriversAudit), AffiliateDriversAudit.TransCardAdminNoEntityColumn, EntityQueryOp.EQ, pPayCard);
            qry.AddClause(TaxiPassRedeemerAccounts.ActiveEntityColumn, EntityQueryOp.EQ, true);
            return pPM.GetEntity<AffiliateDriversAudit>(qry, QueryStrategy.DataSourceThenCache);
        }

        public static AffiliateDriversAudit GetDriverByPayCard(PersistenceManager pPM, string pPayCard) {
            if (string.IsNullOrEmpty(pPayCard)) {
                return pPM.GetNullEntity<AffiliateDriversAudit>();
            }

            string sCard = pPayCard.CardNumberDisplay();

            RdbQuery qry = new RdbQuery(typeof(AffiliateDriversAudit), AffiliateDriversAudit.TransCardCardNumberEntityColumn, EntityQueryOp.EQ, sCard);
            EntityList<AffiliateDriversAudit> list = pPM.GetEntities<AffiliateDriversAudit>(qry, QueryStrategy.DataSourceThenCache);
            foreach (AffiliateDriversAudit rec in list) {
                if (rec.TransCardNumber.Equals(pPayCard)) {
                    return rec;
                }
            }

            return pPM.GetNullEntity<AffiliateDriversAudit>();
        }

        public static EntityList<AffiliateDriversAudit> GetDriversByName(PersistenceManager pPM, string pName) {
            if (string.IsNullOrEmpty(pName)) {
                return new EntityList<AffiliateDriversAudit>();
            }
            RdbQuery qry = new RdbQuery(typeof(AffiliateDriversAudit), AffiliateDriversAudit.NameEntityColumn, EntityQueryOp.EQ, pName);
            return pPM.GetEntities<AffiliateDriversAudit>(qry);
        }

        public static AffiliateDriversAudit GetByBankInfo(PersistenceManager pPM, string pRouting, string pAccountNo) {
            RdbQuery qry = new RdbQuery(typeof(AffiliateDriversAudit), AffiliateDriversAudit.BankCodeEntityColumn, EntityQueryOp.EQ, pRouting);
            qry.AddClause(AffiliateDriversAudit.BankAccountNumberEntityColumn, EntityQueryOp.EQ, pAccountNo);
            qry.AddOrderBy(AffiliateDriversAudit.RowVersionEntityColumn, System.ComponentModel.ListSortDirection.Descending);
            return pPM.GetEntity<AffiliateDriversAudit>(qry);
        }

    }

    #region EntityPropertyDescriptors.AffiliateDriversAuditPropertyDescriptor
    ////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
    ////    For example: 
    ////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
    //public static partial class EntityPropertyDescriptors {
    //	public partial class AffiliateDriversAuditPropertyDescriptor : AdaptedPropertyDescriptor {
    //    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
    //    public AdaptedPropertyDescriptor Age {
    //      get { return this.Get("Age"); }
    //    }
    //  }
    //} 
    #endregion

}
