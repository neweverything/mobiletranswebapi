﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Util;
using IdeaBlade.Persistence.Rdb;

namespace CabRideEngine {
    /// <summary>
    /// The developer class representing the DriverBankInfoAudit business object class.
    /// </summary>
    /// <remarks>
    /// <para>Place all application-specific business logic in this class.</para>
    /// <para>
    /// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
    /// and will not overwrite changes.
    /// </para>
    /// <para>
    /// This class inherits from the generated class, its companion in the pair of classes 
    /// defining a business object. Properties in the generated class may be overridden in 
    /// order to change the behavior or add business logic.
    /// </para>
    /// <para>
    /// This class is the final class in the inheritance chain for this business object. 
    /// It should not be inherited.
    /// </para>
    /// </remarks>
    [Serializable]
    public sealed partial class DriverBankInfoAudit : DriverBankInfoAuditDataRow {

        #region Constructors -- DO NOT MODIFY

        // Do not create constructors for this class
        // Developers cannot create instances of this class with the "new" keyword
        // You should write a static Create method instead; see the "Suggested Customization" region
        // Please review the documentation to learn about entity creation and inheritance

        // This private constructor prevents accidental instance creation via 'new';
        // it is also required if the class is visible from a web service.
        // DO NOT REMOVE **
        private DriverBankInfoAudit()
            : this(null) {
        }

        /// <summary>
        /// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
        /// </summary>
        /// <param name="pRowBuilder"></param>
        public DriverBankInfoAudit(DataRowBuilder pRowBuilder)
            : base(pRowBuilder) {
        }

        #endregion

        #region Suggested Customizations

        //    // Use this factory method template to create new instances of this class
        //    public static DriverBankInfoAudit Create(PersistenceManager pManager,
        //      ... your creation parameters here ...) { 
        //
        //      DriverBankInfoAudit aDriverBankInfoAudit = pManager.CreateEntity<DriverBankInfoAudit>();
        //
        //      // if this object type requires a unique id and you have implemented
        //      // the IIdGenerator interface implement the following line
        //      pManager.GenerateId(aDriverBankInfoAudit, // add id column here //);
        //
        //      // Add custom code here
        //
        //      aDriverBankInfoAudit.AddToManager();
        //      return aDriverBankInfoAudit;
        //    }

        //    // Implement this method if you want your object to sort in a specific order
        //    public override int CompareTo(Object pObject) {
        //    }

        //    // Implement this method to customize the null object version of this class
        //    protected override void UpdateNullEntity() {
        //    }

        #endregion

        // Add additional logic to your business object here...
        public static DriverBankInfoAudit Create(DriverBankInfo pBankInfo) {
			PersistenceManager oPM = pBankInfo.PersistenceManager;
            DriverBankInfoAudit oAudit = oPM.CreateEntity<DriverBankInfoAudit>();
            oPM.GenerateId(oAudit, DriverBankInfoAudit.DriverBankInfoHistoryIDEntityColumn);

            for (int i = 0; i < pBankInfo.Table.Columns.Count; i++) {
                if (oAudit.Table.Columns.Contains(pBankInfo.Table.Columns[i].ColumnName)) {
                    oAudit[pBankInfo.Table.Columns[i].ColumnName] = pBankInfo[i];
                }
            }
            oAudit.AddToManager();

            return oAudit;
        }

        public static DriverBankInfoAudit GetByBankInfo(PersistenceManager pPM, string pRouting, string pAccountNo) {
            RdbQuery qry = new RdbQuery(typeof(DriverBankInfoAudit), DriverBankInfoAudit.BankCodeEntityColumn, EntityQueryOp.EQ, pRouting);
            qry.AddClause(DriverBankInfoAudit.BankAccountNumberEntityColumn, EntityQueryOp.EQ, pAccountNo);
            qry.AddOrderBy(DriverBankInfoAudit.RowVersionEntityColumn, System.ComponentModel.ListSortDirection.Descending);
            return pPM.GetEntity<DriverBankInfoAudit>(qry);
        }

    }

    #region EntityPropertyDescriptors.DriverBankInfoAuditPropertyDescriptor
    ////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
    ////    For example: 
    ////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
    //public static partial class EntityPropertyDescriptors {
    //	public partial class DriverBankInfoAuditPropertyDescriptor : AdaptedPropertyDescriptor {
    //    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
    //    public AdaptedPropertyDescriptor Age {
    //      get { return this.Get("Age"); }
    //    }
    //  }
    //} 
    #endregion

}
