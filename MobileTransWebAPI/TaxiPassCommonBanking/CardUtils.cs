﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TaxiPassCommon.Banking {
	public static class CardUtils {

		public static bool IsTestCard(string pCardNo) {
			bool bTestCard = false;
			switch (pCardNo) {
				case "5555555555554444":
				case "4111111111111111":
				case "4012888888881881":
				case "5105105105105100":
				case "378282246310005":
				case "371449635398431":
				case "378734493671000":
				case "6011111111111117":
				case "6011000990139424":
				case "5551212833217698":
				case "4000200011112222":
				case "4000100011112224":
				case TEST_LITLE_VISA:  // Litle test card
					bTestCard = true;
					break;
			}
			return bTestCard;
		}

		public static string TEST_CARD_WHITE = "5551212833217698";
		public static string TEST_CARD_NEIL = "4003000123456781";
		public static string TEST_CARD_VISA = "4111111111111111";
		public const string TEST_LITLE_VISA = "4457010000000009";
		public static string USE_TEST_CARD = string.Format(";4111111111111111={0}121010000047520001?", DateTime.Today.Year.ToString().Right(2));

	}
}
