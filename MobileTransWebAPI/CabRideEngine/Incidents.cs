﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Util;

using Csla.Validation;
using System.Text;
using TaxiPassCommon;
using IdeaBlade.Persistence.Rdb;

namespace CabRideEngine {
	[Serializable]
	public sealed class Incidents : IncidentsDataRow {

		#region Constructors -- DO NOT MODIFY
		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new'
		private Incidents()
			: this(null) {
		}

		// Typed DataSet constructor (needed by framework) - DO NOT REMOVE
		public Incidents(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static Incidents Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      Incidents aIncidents = pManager.CreateEntity<Incidents>();
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aIncidents, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aIncidents.AddToManager();
		//      return aIncidents;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		// Add additional logic to your business object here...
		public static Incidents Create(PersistenceManager pPM, String pIncidentType, long pTableID) {
			Incidents oIncident = null;

			try {
				oIncident = (Incidents)pPM.CreateEntity(typeof(Incidents));

				// Using the IdeaBlade Id Generation technique
				//pPM.GenerateId(oIncident, Incidents.IncidentIDEntityColumn);
				oIncident.IncidentType = pIncidentType;
				oIncident.TableID = pTableID;

				oIncident.AddToManager();

			} catch (Exception ex) {
				throw ex;
			}
			return oIncident;
		}

		protected override void AddRules(Csla.Validation.RuleList pList) {
			PropertyRequiredRule.AddToList(pList, IncidentNoteEntityColumn.ColumnName);
		}


		protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
			if (IsDeserializing)
				return;
			base.OnColumnChanging(pArgs);
			//DoAudit(pArgs);
		}

		private void DoAudit(DataColumnChangeEventArgs pArgs) {
			if (!this.PersistenceManager.IsClient) {
				return;
			}
			if (this.RowState == DataRowState.Added) {
				return;
			}
			if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
				AuditRecord = IncidentsAudit.Create(this.PersistenceManager, this);
			}
			//AuditRecord[pArgs.Column.ColumnName] = pArgs.ProposedValue;
		}

		// ******************************************************************************
		// Get all Drivers for an Affiliate
		// ******************************************************************************
		public static EntityList<Incidents> GetIncidents(PersistenceManager pPM, string pIncidentType) {
			return GetData(pPM, pIncidentType);
		}

		// ******************************************************************************
		// Call the proc
		// ******************************************************************************
		private static EntityList<Incidents> GetData(PersistenceManager pPM, string pIncidentType) {
			//string sDelimiter = "";
			int iArgCount = 0;
			StringBuilder sql = new StringBuilder();
			sql.Append("usp_IncidentsGet ");
			if (!pIncidentType.IsNullOrEmpty()) {
				sql.AppendFormat(" @IncidentType = '{0}'", pIncidentType);
				iArgCount++;
			}

			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(Incidents), sql.ToString());
			EntityList<Incidents> oData = pPM.GetEntities<Incidents>(qry);
			if (oData.Count == 0) {
				oData.Add(pPM.GetNullEntity<Incidents>());
			}
			return oData;
		}
	}
}
