﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Dapper;
using System.Data;

namespace CabRideEngineDapper {
	public class usp_RedeemerMasterVoucherGet {
		public long RedeemerMasterVoucherID { get; set; }
		public bool SignatureRequired { get; set; }
		public int? DeviceLoginID { get; set; }
		public string DriverSignature { get; set; }
		public bool SigSaved { get; set; }
		public bool Paid { get; set; }
		public string Fleet { get; set; }
		public string Driver { get; set; }
		public string DriverCell { get; set; }
		public int DriverNoImprintCount { get; set; }
		public bool RedeemerStopPayment { get; set; }
		public bool FraudSuspected { get; set; }
		public bool FraudDriverLicense { get; set; }
		public long DriverPaymentID { get; set; }
		public string VoucherNo { get; set; }
		public decimal Fare { get; set; }
		public decimal KioskAmount { get; set; }
		public decimal Parking { get; set; }
		public decimal Stops { get; set; }
		public decimal Gratuity { get; set; }
		public decimal Tolls { get; set; }
		public decimal WaitTime { get; set; }
		public decimal Discount { get; set; }
		public decimal MiscFee { get; set; }
		public decimal DriverTotal { get; set; }
		public decimal RedemptionFee { get; set; }
		public decimal DriverFee { get; set; }
		public decimal RedeemedAmount { get; set; }
		public bool NoImprint { get; set; }
		public bool HasImage { get; set; }
		public bool IsPromo { get; set; }
		public string InvalidMsg { get; set; }
		public bool DriverPaid { get; set; }
		public string Redeemer { get; set; }


		public static List<usp_RedeemerMasterVoucherGet> Execute(SqlConnection pConn, long pTaxiPassRedeemerID, bool pPaid = false) {
			DynamicParameters param = new DynamicParameters();
			param.Add("TaxiPassRedeemerID", pTaxiPassRedeemerID);
			param.Add("Paid", pPaid);

			return pConn.Query<usp_RedeemerMasterVoucherGet>("usp_RedeemerMasterVoucherGet", param, commandType: CommandType.StoredProcedure).ToList();
		}
	}
}
