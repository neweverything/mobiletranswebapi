﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TaxiPassCommon.Banking.Models.GlobalCashCard {

	public class TransHistory {
		public string RESPONSECODE { get; set; }
		public List<RESULT> RESULTS { get; set; }
		public string STATUS { get; set; }
		public int COUNT { get; set; }

		public void UpdateBalances() {

			try {
				if (RESULTS == null || RESULTS.Count == 0) {
					return;
				}

				RESULTS.Last().BALANCE = RESULTS.Last().NET;
				RESULTS.Last().CARDNUMBER = RESULTS.Last().CARDNUMBER.CardNumberDisplay();
				for (int i = RESULTS.Count - 2; i >= 0; i--) {
					RESULTS[i].BALANCE = RESULTS[i + 1].BALANCE + RESULTS[i].NET;
					RESULTS[i].CARDNUMBER = RESULTS[i].CARDNUMBER.CardNumberDisplay();
				}
			} catch (Exception ex) {
				Console.WriteLine(ex.Message);
			}
		}
	}

	public class RESULT {
		public string TRANSID { get; set; }
		public string TRANSDATE { get; set; }
		public string TRANSTIME { get; set; }
		public string CARDNUMBER { get; set; }
		public string DESCRIPTION { get; set; }
		public decimal NET { get; set; }
		public decimal BALANCE { get; set; }
	}


}
