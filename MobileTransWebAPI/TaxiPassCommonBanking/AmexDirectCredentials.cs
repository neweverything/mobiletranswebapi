﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TaxiPassCommon.Banking {
	public class AmexDirectCredentials : IGatewayCredentials {

		public int ProcessingOrder { get; set; }


		public CardProcessors MyGateway() {
			
			return CardProcessors.AmexDirect;

		}

	}
}
