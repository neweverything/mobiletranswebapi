﻿using System;
using System.Collections.Generic;
using System.Net;
using TaxiPassCommon.Banking.Models;
using static TaxiPassCommon.Banking.ProPayRequest;

namespace TaxiPassCommon.Banking {
	public partial class ProPay : IProcessor {

		private ProPayCredentials mCredentials;

		public ProPay(ProPayCredentials pCredentials) {
			ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls;

			if (pCredentials.Timeout == 0) {
				pCredentials.Timeout = 30;
			}
			mCredentials = pCredentials;
		}


		public ProcessorResponse AvsCheck(CCRequest pRequest) {
			throw new NotImplementedException();
		}

		public ProcessorResponse ChargeAuth(CCRequestRefNo pRequest) {

			string url = mCredentials.ProdURL;
			string certKey = mCredentials.CertKey;
			string acctNo = mCredentials.AccountNo;
			if (pRequest.TestCard) {
				url = mCredentials.TestURL;
				certKey = mCredentials.TestCertKey;
				acctNo = mCredentials.TestAccountNo;
			}


			string termID = pRequest.DriverID;
			if (pRequest.TestCard) {
				termID = "e930c9";
			}

			var processRequest = new XmlTransactionRequest() {
				CertificationString = certKey,
				TerminalID = termID
			};

			var xmlTransaction = new XmlProcessTransaction {
				TransType = "06",       // ChargeAuth
				AccountNumber = acctNo,
				Amount = (pRequest.Amount * 100).ToString("F0"),
				InvoiceNumber = pRequest.VoucherNo,
				ReferenceNo = pRequest.RefNum
			};
			processRequest.Transactions.Add(xmlTransaction);
			string request = XmlSerializer<XmlTransactionRequest>.WriteToString(processRequest);


			var response = ProPayRequest.SubmitRequest(url, request);

			return ProcessResponse(response, pRequest.Amount, pRequest.TestCard, TrxTypes.DelayedCapture);
		}

		public ProcessorResponse ChargeCard(CCRequest pRequest) {
			string url = mCredentials.ProdURL;
			string certKey = mCredentials.CertKey;
			string acctNo = mCredentials.AccountNo;
			if (pRequest.CardData.TestCard) {
				url = mCredentials.TestURL;
				certKey = mCredentials.TestCertKey;
				acctNo = mCredentials.TestAccountNo;
			}

			string termID = pRequest.DriverID;
			if (pRequest.CardData.TestCard) {
				termID = "e930c9";
			}

			var processRequest = new XmlTransactionRequest() {
				CertificationString = certKey,
				TerminalID = termID
			};

			var xmlTransaction = new XmlProcessTransaction {
				TransType = "04",       // Charge Card
				AccountNumber = acctNo,
				Amount = (pRequest.Amount * 100).ToString("F0"),
				Zip = pRequest.CardData.AvsZip,
				InvoiceNumber = pRequest.VoucherNo,
				Comment1 = string.Format("D:{0}, AD{1}", pRequest.CardData.DriverID, pRequest.CardData.AffiliateDriverID),
			};
			if (!pRequest.CardData.Track2.IsNullOrEmpty()) {
				if (pRequest.CardData.Track2.StartsWith("%B")) {
					xmlTransaction.Track1 = pRequest.CardData.Track2;
				} else {
					xmlTransaction.Track2 = pRequest.CardData.Track2;
				}
			} else {
				xmlTransaction.CreditCardNumber = pRequest.CardData.CardNo;
				xmlTransaction.ExpirationDate = pRequest.CardData.CardExpiryAsMMYY;
				xmlTransaction.CVV2 = pRequest.CardData.CVV;
			}
			if (pRequest.SplitPayAmount > 0) {
				xmlTransaction.SplitPayAmount = (pRequest.SplitPayAmount * 100).ToString("F0");
				xmlTransaction.SplitPayAcctNum = mCredentials.SplitPayAcctNum;
				xmlTransaction.TransType = "33";
			}

			if (pRequest.CardData.SoftDescriptor != null) {
				xmlTransaction.Comment2 = string.Format("{0}-{1}", pRequest.VoucherNo, pRequest.CardData.SoftDescriptor.Phone);
				//-------
				//rec.merchantData.affiliate = pRequest.CardData.SoftDescriptor.AffiliateName;
				//SetBillingDescriptor(ref pRequest);
				//rec.customBilling = FillSoftDescriptor(pRequest.CardData.SoftDescriptor, !pRequest.CardData.Track2.IsNullOrEmpty());
				var softDescriptor = pRequest.CardData.SoftDescriptor;
				if (softDescriptor.BillingDescriptor.IsNullOrEmpty()) {
					xmlTransaction.TransactionMerchantDescriptor = $"TXP*{url} {softDescriptor.City} {softDescriptor.StateProvince}".PadRight(25).Left(25).Trim();
				} else {
					if (softDescriptor.ExcludeBillingDescriptorPrefix) {
						xmlTransaction.TransactionMerchantDescriptor = softDescriptor.BillingDescriptor.PadRight(25).Left(25).Trim();
					} else {
						xmlTransaction.TransactionMerchantDescriptor = $"TXP*{softDescriptor.BillingDescriptor}".PadRight(25).Left(25).Trim();
					}

				}

				//-----
			}

			processRequest.Transactions.Add(xmlTransaction);
			string request = XmlSerializer<XmlTransactionRequest>.WriteToString(processRequest);


			var response = ProPayRequest.SubmitRequest(url, request);

			return ProcessResponse(response, pRequest.Amount, pRequest.CardData.TestCard, TrxTypes.Authorization);
		}

		public string GetTransactionHistory(DateTime pStart, DateTime pEnd) {
			throw new NotImplementedException();
		}

		public ProcessorResponse OpenCredit(CCRequest pRequest) {
			string url = mCredentials.ProdURL;
			string certKey = mCredentials.CertKey;
			string acctNo = mCredentials.AccountNo;
			if (pRequest.CardData.TestCard) {
				url = mCredentials.TestURL;
				certKey = mCredentials.TestCertKey;
				acctNo = mCredentials.TestAccountNo;
			}


			string termID = pRequest.DriverID;
			if (pRequest.CardData.TestCard) {
				termID = "e930c9";
			}

			var processRequest = new XmlTransactionRequest() {
				CertificationString = certKey,
				TerminalID = termID
			};

			var xmlTransaction = new XmlProcessTransaction {
				TransType = "35",       // Credit
				AccountNumber = acctNo,
				Amount = (pRequest.Amount * 100).ToString("F0"),
				Zip = pRequest.CardData.AvsZip,
				InvoiceNumber = pRequest.VoucherNo,
				Comment1 = string.Format("D:{0}, AD{1}", pRequest.CardData.DriverID, pRequest.CardData.AffiliateDriverID)
			};
			if (!pRequest.CardData.Track2.IsNullOrEmpty()) {
				if (pRequest.CardData.Track2.StartsWith("%B")) {
					xmlTransaction.Track1 = pRequest.CardData.Track2;
				} else {
					xmlTransaction.Track2 = pRequest.CardData.Track2;
				}
			} else {
				xmlTransaction.CreditCardNumber = pRequest.CardData.CardNo;
				xmlTransaction.ExpirationDate = pRequest.CardData.CardExpiryAsMMYY;
				xmlTransaction.CVV2 = pRequest.CardData.CVV;
			}

			processRequest.Transactions.Add(xmlTransaction);
			string request = XmlSerializer<XmlTransactionRequest>.WriteToString(processRequest);


			var response = ProPayRequest.SubmitRequest(url, request);
			return ProcessResponse(response, pRequest.Amount, pRequest.CardData.TestCard, TrxTypes.OpenCredit);

		}

		public ProcessorResponse PreAuthCard(CCRequest pRequest) {
			string url = mCredentials.ProdURL;
			string certKey = mCredentials.CertKey;
			string acctNo = mCredentials.AccountNo;
			if (pRequest.CardData.TestCard) {
				url = mCredentials.TestURL;
				certKey = mCredentials.TestCertKey;
				acctNo = mCredentials.TestAccountNo;
			}

			string termID = pRequest.DriverID;
			if (pRequest.CardData.TestCard) {
				termID = "e930c9";
			}

			var processRequest = new XmlTransactionRequest() {
				CertificationString = certKey,
				TerminalID = termID
			};

			var xmlTransaction = new XmlProcessTransaction {
				TransType = "05",
				AccountNumber = acctNo,
				Amount = (pRequest.Amount * 100).ToString("F0"),
				Zip = pRequest.CardData.AvsZip,
				InvoiceNumber = pRequest.VoucherNo,
				Comment1 = string.Format("D:{0}, AD{1}", pRequest.CardData.DriverID, pRequest.CardData.AffiliateDriverID),
			};
			if (!pRequest.CardData.Track2.IsNullOrEmpty()) {
				if (pRequest.CardData.Track2.StartsWith("%B")) {
					xmlTransaction.Track1 = pRequest.CardData.Track2;
				} else {
					xmlTransaction.Track2 = pRequest.CardData.Track2;
				}
			} else {
				xmlTransaction.CreditCardNumber = pRequest.CardData.CardNo;
				xmlTransaction.ExpirationDate = pRequest.CardData.CardExpiryAsMMYY;
				xmlTransaction.CVV2 = pRequest.CardData.CVV;
			}

			processRequest.Transactions.Add(xmlTransaction);
			string request = XmlSerializer<XmlTransactionRequest>.WriteToString(processRequest);


			var response = ProPayRequest.SubmitRequest(url, request);

			return ProcessResponse(response, pRequest.Amount, pRequest.CardData.TestCard, TrxTypes.Authorization);

		}

		public ProcessorResponse ProcessCheck(CheckRequest pRequest) {
			throw new NotImplementedException();
		}

		public ProcessorResponse RefundCheck(CheckRequest pRequest) {
			throw new NotImplementedException();
		}

		public ProcessorResponse Refund(CCRequestRefNo pRequest) {
			string url = mCredentials.ProdURL;
			string certKey = mCredentials.CertKey;
			string acctNo = mCredentials.AccountNo;
			if (pRequest.TestCard) {
				url = mCredentials.TestURL;
				certKey = mCredentials.TestCertKey;
				acctNo = mCredentials.TestAccountNo;
			}


			string termID = pRequest.DriverID;
			if (pRequest.TestCard) {
				termID = "e930c9";
			}

			var processRequest = new XmlTransactionRequest() {
				CertificationString = certKey,
				TerminalID = termID
			};

			var xmlTransaction = new XmlProcessTransaction {
				TransType = "07",       // Refund or Void
				AccountNumber = acctNo,
				Amount = Math.Abs(pRequest.Amount * 100).ToString("F0"),
				InvoiceNumber = pRequest.VoucherNo,
				ReferenceNo = pRequest.RefNum
			};
			processRequest.Transactions.Add(xmlTransaction);
			string request = XmlSerializer<XmlTransactionRequest>.WriteToString(processRequest);


			var response = ProPayRequest.SubmitRequest(url, request);

			return ProcessResponse(response, pRequest.Amount, pRequest.TestCard, TrxTypes.Credit);

		}

		public bool Void(CCRequestRefNo pRequest) {
			string url = mCredentials.ProdURL;
			string certKey = mCredentials.CertKey;
			string acctNo = mCredentials.AccountNo;
			if (pRequest.TestCard) {
				url = mCredentials.TestURL;
				certKey = mCredentials.TestCertKey;
				acctNo = mCredentials.TestAccountNo;
			}


			string termID = pRequest.DriverID;
			if (pRequest.TestCard) {
				termID = "e930c9";
			}

			var processRequest = new XmlTransactionRequest() {
				CertificationString = certKey,
				TerminalID = termID
			};

			var xmlTransaction = new XmlProcessTransaction {
				TransType = "07",       // Refund or Void
				AccountNumber = acctNo,
				Amount = Math.Abs(pRequest.Amount * 100).ToString("F0"),
				InvoiceNumber = pRequest.VoucherNo,
				ReferenceNo = pRequest.RefNum
			};
			processRequest.Transactions.Add(xmlTransaction);
			string request = XmlSerializer<XmlTransactionRequest>.WriteToString(processRequest);


			var response = ProPayRequest.SubmitRequest(url, request);

			return response.XMLResponse.XMLTrans.status.Equals("00");

		}

		private ProcessorResponse ProcessResponse(ProPayResponse pResponse, decimal pAuthAmount, bool pTestCard, string pTransType) {
			ProcessorResponse oResult = new ProcessorResponse();

			oResult.TestCard = pTestCard;
			oResult.VerisignAccountID = mCredentials.VerisignAccountID;
			oResult.MerchantID = mCredentials.MerchantID;
			oResult.CreditCardProcessor = mCredentials.MyGateway();

			oResult.Result = Convert.ToInt32(pResponse.XMLResponse.XMLTrans.status).ToString();
			oResult.Reference = pResponse.XMLResponse.XMLTrans.transNum;
			if (pResponse.XMLResponse.XMLTrans.responseCode.IsNullOrEmpty() || Convert.ToInt32(pResponse.XMLResponse.XMLTrans.responseCode) == 0) {
				oResult.Message = GetStatus(pResponse.XMLResponse.XMLTrans.status).Status;
			} else {
				oResult.Message = $"{GetStatus(pResponse.XMLResponse.XMLTrans.status).Status} - {GetResponse(pResponse.XMLResponse.XMLTrans.responseCode).Response}";
			}
			if (oResult.Message.Equals("Success", StringComparison.CurrentCultureIgnoreCase)) {
				oResult.Message = "Approved";
			}
			oResult.Amount = Convert.ToDecimal(pResponse.XMLResponse.XMLTrans.GrossAmt.AsDecimal() / 100);
			if (oResult.Amount == 0) {
				oResult.Amount = pAuthAmount;
			}
			if (oResult.Result.IsNullOrEmpty()) {
				oResult.Result = "0";
			}
			//fixed for Chase results
			if (oResult.Result != "0" && oResult.Message.StartsWith("Approve", StringComparison.CurrentCultureIgnoreCase)) {
				oResult.Result = "0";
			}


			oResult.AuthCode = pResponse.XMLResponse.XMLTrans.authCode;
			oResult.AVSResultCode = pResponse.XMLResponse.XMLTrans.AVS; // ConvertToUSAePayAVSResultCode(pResponse.fraudResult.avsResult);
			oResult.CVV2Match = pResponse.XMLResponse.XMLTrans.CVV2Resp;

			oResult.TransType = pTransType;
			oResult.TransDate = DateTime.Now;



			//placed as USAePay is now sending us failed trans with a result code of 0
			if (oResult.Result.Equals("0")) {
				if (!oResult.Message.IsNullOrEmpty() && !oResult.Message.Equals("Approved", StringComparison.CurrentCultureIgnoreCase)) {
					if (!oResult.Message.Equals("Unable to get tran details (p)", StringComparison.CurrentCultureIgnoreCase)) {
						oResult.Result = "1";
					}
				}
			}

			return oResult;
		}

		public List<CheckStatus> GetCheckTransactionHistory(DateTime pStart, DateTime pEnd) {
			throw new NotImplementedException();
		}

		//private string ConvertToUSAePayAVSResultCode(string pAVSResult) {
		//    if (pAVSResult.IsNullOrEmpty()) {
		//        return pAVSResult;
		//    }

		//    string result = pAVSResult.PadLeft(2, '0').Right(2);

		//    switch (result) {
		//        case "00": // 5-Digit zip and address match
		//            result = "YYY";
		//            break;

		//        case "01": // 9 - Digit zip and address match
		//            result = "YYX";
		//            break;

		//        case "02": // Postal code and address match
		//            result = "GGG";
		//            break;

		//        case "10": // 5 - Digit zip matches, address does not match
		//            result = "NYZ";
		//            break;

		//        case "11": // 9 - Digit zip matches, address does not match
		//            result = "NYW";
		//            break;

		//        case "12": // Zip does not match, address matches
		//            result = "YNA";
		//            break;

		//        case "13": // Postal code does not match, address matches
		//            result = "YYG";
		//            break;

		//        case "14": // Postal code matches, address not verified
		//            result = "YNN";
		//            break;

		//        case "20": // Neither zip nor address match
		//            result = "NNN";
		//            break;

		//        case "30": // AVS service not supported by issuer
		//            result = "XXS";
		//            break;

		//        case "31": // AVS system not available
		//        case "32": // Address unavailable
		//        case "33": // General error
		//        case "40": // Address failed Litle & Co.edit checks
		//            result = "XXR";
		//            break;

		//        case "34": // AVS not performed
		//            result = "XXU";
		//            break;

		//    }
		//    return result;
		//}



	}
}
