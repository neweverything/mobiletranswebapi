﻿using Newtonsoft.Json.Linq;

namespace TaxiPassCommon.Banking {
    public class CardConnectRequest {
        public string Currency = "USD";

        public string MerchID { get; set; }
        public string CardType { get; set; }
        public string CardNo { get; set; }
        public string Expiry { get; set; }
        public string CVV { get; set; }
        public decimal Amount { get; set; }
        public string TransNo { get; set; }
        public string SwipeData { get; set; }
        public string SalesPers { get; set; }
        public string Capture { get; set; }
        public string Tokenize { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string ZipCode { get; set; }
        public string ReferenceNo { get; set; }

        public JObject AsJObject() {
            JObject request = new JObject();

            // Merchant ID
            request.Add("merchid", MerchID);

            if (ReferenceNo.IsNullOrEmpty()) {
                if (SwipeData.IsNullOrEmpty()) {
                    // Card Type
                    request.Add("accttype", CardType);
                    // Card Number
                    request.Add("account", CardNo);
                    // Card Expiry
                    request.Add("expiry", Expiry);
                    // Card CCV2
                    request.Add("cvv2", CVV);
                } else {
                    request.Add("track", SwipeData);
                }
            }

            // Transaction amount
            request.Add("amount", (Amount * 100).ToString("F0"));
            // Transaction currency
            request.Add("currency", Currency);
            // Order ID
            request.Add("orderid", TransNo);

            request.Add("termid", SalesPers);

            if (!Capture.IsNullOrEmpty()) {
                request.Add("capture", Capture);
            }

            //// Cardholder Name
            //request.Add("name", "Test User");

            if (!Street.IsNullOrEmpty()) {
                request.Add("address", Street);
            }
            if (!City.IsNullOrEmpty()) {
                request.Add("city", City);
            }
            if (!State.IsNullOrEmpty()) {
                request.Add("region", State);
            }
            if (!Country.IsNullOrEmpty()) {
                request.Add("country", Country);
            }
            if (!ZipCode.IsNullOrEmpty()) {
                request.Add("postal", ZipCode);
            }

            if (!Tokenize.IsNullOrEmpty()) {
                request.Add("tokenize", Tokenize);
            }

            if (!ReferenceNo.IsNullOrEmpty()) {
                request.Add("retref", ReferenceNo);
            }
            return request;
        }


    }
}
