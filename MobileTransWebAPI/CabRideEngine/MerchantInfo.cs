﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Util;
using IdeaBlade.Persistence.Rdb;

using CabRideEngine.Utils;
using TaxiPassCommon;

using Csla.Validation;
using System.Text;

namespace CabRideEngine {
    /// <summary>
    /// The developer class representing the MerchantInfo business object class.
    /// </summary>
    /// <remarks>
    /// <para>Place all application-specific business logic in this class.</para>
    /// <para>
    /// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
    /// and will not overwrite changes.
    /// </para>
    /// <para>
    /// This class inherits from the generated class, its companion in the pair of classes 
    /// defining a business object. Properties in the generated class may be overridden in 
    /// order to change the behavior or add business logic.
    /// </para>
    /// <para>
    /// This class is the final class in the inheritance chain for this business object. 
    /// It should not be inherited.
    /// </para>
    /// </remarks>
    [Serializable]
    public sealed partial class MerchantInfo : MerchantInfoDataRow {

        #region Constructors -- DO NOT MODIFY

        // Do not create constructors for this class
        // Developers cannot create instances of this class with the "new" keyword
        // You should write a static Create method instead; see the "Suggested Customization" region
        // Please review the documentation to learn about entity creation and inheritance

        // This private constructor prevents accidental instance creation via 'new';
        // it is also required if the class is visible from a web service.
        // DO NOT REMOVE **
        private MerchantInfo()
            : this(null) {
        }

        /// <summary>
        /// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
        /// </summary>
        /// <param name="pRowBuilder"></param>
        public MerchantInfo(DataRowBuilder pRowBuilder)
            : base(pRowBuilder) {
        }

        #endregion

        #region Suggested Customizations

        //    // Use this factory method template to create new instances of this class
        //    public static MerchantInfo Create(PersistenceManager pManager,
        //      ... your creation parameters here ...) { 
        //
        //      MerchantInfo aMerchantInfo = pManager.CreateEntity<MerchantInfo>();
        //
        //      // if this object type requires a unique id and you have implemented
        //      // the IIdGenerator interface implement the following line
        //      pManager.GenerateId(aMerchantInfo, // add id column here //);
        //
        //      // Add custom code here
        //
        //      aMerchantInfo.AddToManager();
        //      return aMerchantInfo;
        //    }

        //    // Implement this method if you want your object to sort in a specific order
        //    public override int CompareTo(Object pObject) {
        //    }

        //    // Implement this method to customize the null object version of this class
        //    protected override void UpdateNullEntity() {
        //    }

        #endregion

        // Add additional logic to your business object here...
        public static MerchantInfo Create(PersistenceManager pPM) {
            MerchantInfo oInfo = null;
            try {
                oInfo = pPM.CreateEntity<MerchantInfo>();

                // Using the IdeaBlade Id Generation technique
                //pPM.GenerateId(oInfo, MerchantInfo.MerchantIDEntityColumn);
                oInfo.AddToManager();

            } catch (Exception ex) {
                throw ex;
            }
            return oInfo;
        }

        protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
            if (IsDeserializing) {
                return;
            }
            base.OnColumnChanging(pArgs);
            //DoAudit(pArgs);
        }

        private void DoAudit(DataColumnChangeEventArgs pArgs) {
            if (!this.PersistenceManager.IsClient) {
                return;
            }
            if (this.RowState == DataRowState.Added) {
                return;
            }
            if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
                AuditRecord = MerchantInfoAudit.Create(this.PersistenceManager, this);
            }
        }

        [BindingBrowsable(false)]
        public override string EntityInstanceDisplayName {
            get {
                string sText = this.Description;
                if (string.IsNullOrEmpty(sText)) {
                    sText = this.MerchantNo;
                }
                return sText;
            }
        }

        protected override void AddRules(RuleList pList) {
            PropertyRequiredRule.AddToList(pList, MerchantNoColumn.ColumnName);
            //PropertyRequiredRule.AddToList(pList, DescriptionEntityColumn.ColumnName);
            pList.Add(IsMerchantNoUnique, MerchantNoColumn.ColumnName);
        }

        private bool IsMerchantNoUnique(object pTarget, RuleArgs e) {
            MerchantInfo oInfo = pTarget as MerchantInfo;

            RdbQuery qry = new RdbQuery(typeof(MerchantInfo), MerchantInfo.MerchantNoEntityColumn, EntityQueryOp.EQ, oInfo.MerchantNo);
            qry.AddClause(MerchantInfo.MerchantIDEntityColumn, EntityQueryOp.NE, oInfo.MerchantID);
            EntityList<MerchantInfo> oList = this.PersistenceManager.GetEntities<MerchantInfo>(qry);
            if (oList.Count > 0) {
                e.Description = "This Merchant No already exists";
                return false;
            }
            return true;
        }

        private EntityList<MerchantInfo> GetChangedList(PersistenceManager pPM) {
            return pPM.GetEntities<MerchantInfo>(DataRowState.Added | DataRowState.Modified | DataRowState.Deleted);
        }

		// ******************************************************************************
		// Get all recs
		// ******************************************************************************
        public static EntityList<MerchantInfo> GetMerchants(PersistenceManager pPM) {
			return GetData(pPM, "","","");
        }

		// ******************************************************************************
		// Get by Verisign Acct ID and cardtype
		// ******************************************************************************
        public static MerchantInfo GetRecord(PersistenceManager pPM, long pVerisignAcct, string pCardType) {
			EntityList<MerchantInfo> oRecs = GetData(pPM, "", pVerisignAcct.ToString(), pCardType);
			return oRecs[0];
        }


		// ******************************************************************************
		// Call the proc
		// ******************************************************************************
		private static EntityList<MerchantInfo> GetData(PersistenceManager pPM, string pMerchantNo, string pVerisignAcct, string pCardType) {
			string sDelimiter = "";
			int iArgCount = 0;
			StringBuilder sql = new StringBuilder();
			sql.Append("usp_MerchantInfoGet ");
			if (!pMerchantNo.IsNullOrEmpty()) {
				sql.AppendFormat(" @MerchantNo = '{0}'", pMerchantNo);
				iArgCount++;
			}
			if (!pVerisignAcct.IsNullOrEmpty()) {
				if (iArgCount > 0) {
					sDelimiter = ", ";
				}
				sql.AppendFormat("{0}@VerisignAccountID = {1}", sDelimiter, pVerisignAcct);
				iArgCount++;
			}
			if (!pCardType.IsNullOrEmpty()) {
				if (iArgCount > 0) {
					sDelimiter = ", ";
				}
				sql.AppendFormat("{0}@CardType = {1}", sDelimiter, pCardType);
				iArgCount++;
			}
			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(MerchantInfo), sql.ToString());
			EntityList<MerchantInfo> oData = pPM.GetEntities<MerchantInfo>(qry);
			if (oData.Count == 0) {
				oData.Add(pPM.GetNullEntity<MerchantInfo>());
			}
			return oData;
		}

    }

    #region EntityPropertyDescriptors.MerchantInfoPropertyDescriptor
    ////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
    ////    For example: 
    ////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
    //public static partial class EntityPropertyDescriptors {
    //	public partial class MerchantInfoPropertyDescriptor : BaseEntityPropertyDescriptor {
    //    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
    //    public AdaptedPropertyDescriptor Age {
    //      get { return this.Get("Age"); }
    //    }
    //  }
    //} 
    #endregion

}
