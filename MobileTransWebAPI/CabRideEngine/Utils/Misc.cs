﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using IdeaBlade.Persistence;
using TaxiPassCommon;

namespace CabRideEngine.Utils {
	public static class Misc {

		public static void SendTransCardPaymentSMS(DriverPayments oPay, AffiliateDrivers pTransCardOwner, string pCardBalance) {
			PersistenceManager oPM = oPay.PersistenceManager;

			SystemDefaults oDef = SystemDefaults.GetDefaults(oPM);
			try {
				string carrier = "";
				//AffiliateDrivers driver = oPay.AffiliateDrivers;
				PagerTypes oPager = oPM.GetNullEntity<PagerTypes>();

				if (pTransCardOwner.CellProviderID.HasValue) {
					oPager = PagerTypes.GetPagerType(oPM, pTransCardOwner.CellProviderID.Value);
					carrier = "" + oPager.Url;
				}
				StringBuilder msg = new StringBuilder();
				if (oPay.ChargeBack) { // && !oPay.ChargeBy.Equals("Recurring Fee",StringComparison.CurrentCultureIgnoreCase)) {

					return; //Jason 09/21/2010 doesn't want any messages going to the driver

					//msg = oPay.DriverTotalPaid.ToString("C") + " has been deducted from your card *" + pTransCardOwner.TransCardNumber.Right(5);
					//if (!oPay.ChargeBackInfo.IsNullOrEmpty()) {
					//    msg += "/nFor " + oPay.ChargeBackInfo;
					//}
				} else {
					msg.Append(oPay.DriverTotalPaid.ToString("C"));
					msg.Append(" has been added to card *");
					msg.Append(pTransCardOwner.TransCardNumber.Right(5));
					if (oPay.RedemptionFee.GetValueOrDefault(0) > 0) {
						msg.Append(" (");
						msg.Append(oPay.DriverTotal.ToString("C"));
						msg.Append(" - ");
						msg.Append(oPay.RedemptionFee.Value.ToString("C"));
						msg.Append(" redemption fee)");
					}
					if (oPay.DriverFee.GetValueOrDefault(0) > 0) {
						msg.Append(" - (");
						msg.Append(oPay.DriverFee.Value.ToString("C"));
						msg.Append(" driver fee)");
					}
					msg.Append(" for Voucher ");
					msg.Append(oPay.TransNo);
					msg.Append(".");
				}
				if (!pCardBalance.IsNullOrEmpty()) {
					msg.Append("\nNew card bal: ");
					msg.Append(pCardBalance);
				}

				pTransCardOwner.SendDriverMsg(msg.ToString(), false, true);
			} catch {
			}
		}

		public static void SendTransCardPaymentSMS(DriverPayments pPay, TaxiPassRedeemerAccounts pTransCardOwner, string pCardBalance) {
			SendTransCardPaymentSMS(pPay, pTransCardOwner, 0, pCardBalance);
		}

		public static void SendTransCardPaymentSMS(DriverPayments pPay, TaxiPassRedeemerAccounts pTransCardOwner, decimal pAmount, string pCardBalance) {
			if (pTransCardOwner.DisableMessaging) {
				return;
			}
			PersistenceManager oPM = pPay.PersistenceManager;

			SystemDefaults oDef = SystemDefaults.GetDefaults(oPM);
			try {
				PagerTypes oPager = oPM.GetNullEntity<PagerTypes>();

				StringBuilder msg = new StringBuilder();
				if (pPay.ChargeBack) {
					return; //Jason 09/21/2010 doesn't want any messages going to the driver

					//msg = pPay.DriverTotalPaid.ToString("C") + " has been deducted from your card *" + pPay.ACHDetail.ACHBankAccountNumber.Right(5);
					//if (!pPay.ChargeBackInfo.IsNullOrEmpty()) {
					//    msg += "/nFor " + pPay.ChargeBackInfo;
					//}
				} else {

					//PayBeforeMatch will pass in pAmount
					if (pAmount == 0) {
						pAmount = pPay.RedeemerTotalPaid;
					}
					msg.Append(pAmount.ToString("C"));
					msg.Append(" has been added to card *");
					msg.Append(pPay.ACHDetail.ACHBankAccountNumber.Right(5));
					//if (pPay.RedemptionFee.GetValueOrDefault(0) > 0) {
					//    msg += " (" + pPay.DriverTotal.ToString("C") + " - " + pPay.RedemptionFee.Value.ToString("C") + " redemption fee)";
					//}
					if (pPay.DriverFee.GetValueOrDefault(0) > 0) {
						msg.Append(" (");
						msg.Append(pPay.DriverTotal.ToString("C"));
						msg.Append(" - ");
						msg.Append(pPay.DriverFee.Value.ToString("C"));
						msg.Append(" driver fee)");
					}
					msg.Append(" for Voucher ");
					msg.Append(pPay.TransNo);
					msg.Append(".");
				}
				if (!pCardBalance.IsNullOrEmpty()) {
					msg.Append("\nNew card bal: ");
					msg.Append(pCardBalance);
				}

				pTransCardOwner.SendMsg(msg.ToString());
			} catch {
			}
		}

		public static void SendDirectDepositPaymentSMS(DriverPayments oPay) {
			PersistenceManager oPM = oPay.PersistenceManager;

			SystemDefaults oDef = SystemDefaults.GetDefaults(oPM);
			try {
				string carrier = "";
				//AffiliateDrivers driver = oPay.AffiliateDrivers;
				PagerTypes oPager = oPM.GetNullEntity<PagerTypes>();

				AffiliateDrivers oOwner = oPay.AffiliateDrivers;
				if (oOwner.CellProviderID.HasValue) {
					oPager = PagerTypes.GetPagerType(oPM, oOwner.CellProviderID.Value);
					carrier = "" + oPager.Url;
				}

				string msg = "";
				if (oPay.ChargeBack) {
					msg = oPay.DriverTotalPaid.ToString("C") + " has been deducted from your bank account *" + oOwner.BankAccountNumber.Right(5);
					if (!oPay.ChargeBackInfo.IsNullOrEmpty()) {
						msg += "/nFor " + oPay.ChargeBackInfo;
					}
				} else {
					msg = oPay.DriverTotalPaid.ToString("C") + " has been added to bank account *" + oOwner.BankAccountNumber.Right(5);
					if (oPay.RedemptionFee.GetValueOrDefault(0) > 0) {
						msg += " (" + oPay.DriverTotal.ToString("C") + " - " + oPay.RedemptionFee.Value.ToString("C") + " redemption fee)";
					}
					if (oPay.DriverFee.GetValueOrDefault(0) > 0) {
						msg += " - (" + oPay.DriverFee.Value.ToString("C") + " driver fee)";
					}
					msg += " for Transit Check " + oPay.TransNo;
				}
				msg += "/nAllow 2 business days for transfer.";
				oOwner.SendDriverMsg(msg, false, true);
			} catch {
			}
		}


		public static void SendTransCardPaymentSMS(List<DriverPayments> pPayments, CabRideEngineDapper.AffiliateDrivers pTransCardOwner, string pCardBalance) {

			using (var conn = CabRideEngineDapper.Utils.SqlHelper.OpenSqlConnection()) {
				CabRideEngineDapper.SystemDefaults oDef = CabRideEngineDapper.SystemDefaults.GetDefaults(conn);
				try {
					string carrier = "";
					//AffiliateDrivers driver = oPay.AffiliateDrivers;
					CabRideEngineDapper.PagerTypes oPager = CabRideEngineDapper.PagerTypes.GetNullEntity();

					if (pTransCardOwner.CellProviderID.HasValue) {
						oPager = CabRideEngineDapper.PagerTypes.GetPagerType(conn, pTransCardOwner.CellProviderID.Value);
						carrier = "" + oPager.URL;
					}
					StringBuilder msg = new StringBuilder();
					decimal driverTotalPaid = pPayments.Where(p => !p.Failed && !p.Test).Sum(p => p.DriverTotalPaid);
					decimal driverFee = pPayments.Where(p => !p.Failed && !p.Test).Sum(p => p.DriverFee.GetValueOrDefault(0));

					msg.Append(driverTotalPaid.ToString("C"));
					msg.Append(" has been added to card *");
					msg.Append(pTransCardOwner.TransCardNumber.DecryptIceKey().Right(5));
					decimal redemptionFee = pPayments.Where(p => !p.Failed && !p.Test).Sum(p => p.RedemptionFee.GetValueOrDefault(0));
					if (redemptionFee > 0) {
						decimal driverTotal = pPayments.Where(p => !p.Failed && !p.Test).Sum(p => p.DriverTotal);
						msg.Append(" (");
						msg.Append(driverTotal.ToString("C"));
						msg.Append(" - ");
						msg.Append(redemptionFee.ToString("C"));
						msg.Append(" redemption fee)");
					}
					if (driverFee > 0) {
						msg.Append(" - (");
						msg.Append(driverFee.ToString("C"));
						msg.Append(" driver fee)");
					}
					if (pPayments.Count > 10) {
						int cnt = pPayments.Where(p => !p.Failed && !p.Test).Count();
						msg.AppendFormat(" for {0} Vouchers.", cnt);
					} else {
						msg.Append(pPayments.Count > 1 ? " for Vouchers " : " for Voucher ");
						msg.Append(string.Join(",", pPayments.Where(p => !p.Failed && !p.Test).Select(p => p.TransNo).ToList()));
						msg.Append(".");
					}
					if (!pCardBalance.IsNullOrEmpty()) {
						msg.Append("\nNew card bal: ");
						msg.Append(pCardBalance);
					}
					pTransCardOwner.SendDriverMsg(msg.ToString(), false, true);
				} catch {
				}
			}

		}

	}

	public class ItemValue {
		public ItemValue() {

		}

		public ItemValue(string item, string value) {
			Item = item;
			Value = value;
		}

		public string Item { get; set; }
		public string Value { get; set; }
	}
}
