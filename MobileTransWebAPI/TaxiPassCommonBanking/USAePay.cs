﻿using CsvHelper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using TaxiPassCommon.Banking.com.USAePay;
using TaxiPassCommon.Banking.Models;

namespace TaxiPassCommon.Banking {
	public class USAePay : IProcessor {

		private USAePayCredentials mCredentials;

		private com.USAePay.usaepayService mService = new com.USAePay.usaepayService();

		public USAePay(USAePayCredentials pCredentials) {
			ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls;

			mService.Url = pCredentials.ProdURL;
			if (pCredentials.Timeout == 0) {
				pCredentials.Timeout = 30;
			}
			mService.Timeout = pCredentials.Timeout * 1000;

			mCredentials = pCredentials;
		}

		/// <summary>
		/// PreAuthorize the xredit card.
		/// </summary>
		/// <param name="pCardData">The card data.</param>
		/// <param name="pAuthAmount">The auth amount.</param>
		/// <param name="pComments">The comments.</param>
		/// <param name="pVoucherNo">The voucher no.</param>
		/// <param name="pService">The service type.</param>
		/// <returns></returns>
		public ProcessorResponse PreAuthCard(CCRequest pRequest) {

			if (pRequest.CardData.TestCard) {
				mService.Url = mCredentials.TestURL;
			}

			//skip USAePay if we do not have a URL to post to
			if (mService.Url.IsNullOrEmpty()) {
				return null;
			}

			com.USAePay.TransactionRequestObject trans = FillCardInfo(pRequest);

			var ePayResponse = mService.runAuthOnly(GetToken(pRequest.CardData.TestCard), trans);

			return ProcessResponse(ePayResponse, pRequest.Amount, TrxTypes.Authorization, pRequest.CardData.TestCard);
		}

		/// <summary>
		/// Fills the card info.
		/// </summary>
		/// <param name="pCardData">Card Data.</param>
		/// <param name="pTransAmount">Amount.</param>
		/// <param name="pComments">Comments.</param>
		/// <param name="pVoucherNo">Voucher no.</param>
		/// <param name="pService">The Service Type.</param>
		/// <returns></returns>
		private com.USAePay.TransactionRequestObject FillCardInfo(CCRequest pRequest) {
			com.USAePay.CreditCardData cardData = new com.USAePay.CreditCardData();

			if (pRequest.CardData.Track2.IsNullOrEmpty()) {
				cardData.CardNumber = pRequest.CardData.CardNo;
				string sMonth = pRequest.CardData.CardExpiryMonth;
				string sYear = pRequest.CardData.CardExpiryYear;
				cardData.CardExpiration = sMonth + sYear;
				cardData.CardPresent = !pRequest.VoucherNo.StartsWith("WEB");
			} else {
				cardData.MagStripe = pRequest.CardData.Track2;
				cardData.CardPresent = true;
			}
			cardData.CardCode = pRequest.CardData.CVV;
			cardData.AvsZip = pRequest.CardData.AvsZip;
			cardData.AvsStreet = pRequest.CardData.AvsStreet;
			if (!mCredentials.TerminalType.IsNullOrEmpty()) {
				cardData.TermType = mCredentials.TerminalType;
			}

			com.USAePay.TransactionRequestObject trans = new com.USAePay.TransactionRequestObject();
			trans.CreditCardData = cardData;

			trans.Details = new com.USAePay.TransactionDetail();
			trans.Details.Amount = (double)pRequest.Amount;
			trans.Details.AmountSpecified = true;
			trans.Details.NonTax = true;
			trans.Details.Tax = 0;

			trans.Details.Comments = pRequest.Comments;
			trans.Details.OrderID = pRequest.VoucherNo;

			trans.Details.PONum = pRequest.VoucherNo;
			trans.Details.Description = pRequest.Service;

			trans.Details.Clerk = pRequest.CardData.SalesPerson;
			if (trans.Details.Clerk.IsNullOrEmpty()) {
				trans.Details.Clerk = string.Format("D: {0}", pRequest.DriverID);
			}

			if (cardData.CardPresent && !cardData.MagStripe.IsNullOrEmpty()) {
				trans.Details.Terminal = pRequest.DriverID.ToString();
			}

			List<FieldValue> fieldValueList = new List<FieldValue>();
			fieldValueList.Add(new FieldValue {
				Field = "Driver Fee",
				Value = pRequest.DriverFee.ToString("F2")
			});

			fieldValueList.Add(new FieldValue {
				Field = "Service Fee",
				Value = pRequest.TaxiPassFee.ToString("F2")
			});

			trans.CustomFields = fieldValueList.ToArray();

			return trans;
		}


		/// <summary>
		/// Charges the card.
		/// </summary>
		/// <param name="pCardData">The p card data.</param>
		/// <param name="pChargeAmount">The p charge amount.</param>
		/// <param name="pComments">The p comments.</param>
		/// <param name="pVoucherNo">The p voucher no.</param>
		/// <param name="pService">The p service.</param>
		/// <param name="pDriverID">The p driver I ds.</param>
		/// <returns></returns>
		public ProcessorResponse ChargeCard(CCRequest pRequest) {
			if (pRequest.CardData.TestCard) {
				mService.Url = mCredentials.TestURL;
			}

			//skip USAePay if we do not have a URL to post to
			if (mService.Url.IsNullOrEmpty()) {
				return null;
			}

			com.USAePay.TransactionRequestObject trans = FillCardInfo(pRequest);
			//if (pRequest.DriverID.IsNullOrEmpty()) {
			//    if (pRequest.CardData.AffiliateDriverID.HasValue) {
			//        trans.Details.Clerk = pRequest.CardData.AffiliateDriverID.Value.ToString();
			//    } else {
			//        trans.Details.Clerk = pRequest.CardData.DriverID.ToString();
			//    }
			//} else {
			//    trans.Details.Clerk = pRequest.DriverID;
			//}
			var ePayResponse = mService.runSale(GetToken(pRequest.CardData.TestCard), trans);

			return ProcessResponse(ePayResponse, pRequest.Amount, TrxTypes.Sale, pRequest.CardData.TestCard);
		}


		/// <summary>
		/// Charges the Pre Auth.
		/// </summary>
		/// <param name="pRefNum">The PreAUth Reference Number.</param>
		/// <param name="pChargeAmount">The amount to Charge.</param>
		/// <param name="pTestCard">if set to <c>true</c> [test card].</param>
		/// <returns></returns>
		public ProcessorResponse ChargeAuth(CCRequestRefNo pRequest) {
			if (pRequest.TestCard) {
				mService.Url = mCredentials.TestURL;
			}

			//skip USAePay if we do not have a URL to post to
			if (mService.Url.IsNullOrEmpty()) {
				return null;
			}

			var ePayResponse = mService.captureTransaction(GetToken(pRequest.TestCard), pRequest.RefNum, (double)pRequest.Amount);

			return ProcessResponse(ePayResponse, pRequest.Amount, TrxTypes.DelayedCapture, pRequest.TestCard);
		}


		/// <summary>
		/// Refunds the specified amount from a previously charged trans using the Reference number.
		/// </summary>
		/// <param name="pRefNum">The Ref Num.</param>
		/// <param name="pRefundAmount">The refund amount.</param>
		/// <param name="pTestCard">if set to <c>true</c> [test card].</param>
		/// <returns></returns>
		public ProcessorResponse Refund(CCRequestRefNo pRequest) {
			if (pRequest.TestCard) {
				mService.Url = mCredentials.TestURL;
			}

			// must be positive otherwise a full refund occurs
			var ePayResponse = mService.refundTransaction(GetToken(pRequest.TestCard), pRequest.RefNum, (double)Math.Abs(pRequest.Amount));


			return ProcessResponse(ePayResponse, pRequest.Amount, TrxTypes.Credit, pRequest.TestCard);
		}


		/// <summary>
		/// Voids the previously charged transaction using the reference number supplied by USAePay.
		/// </summary>
		/// <param name="pRefNum">The reference number.</param>
		/// <param name="pTestCard">if set to <c>true</c> [test card].</param>
		/// <returns></returns>
		public bool Void(CCRequestRefNo pRequest) {
			if (pRequest.TestCard) {
				mService.Url = mCredentials.TestURL;
			}
			return mService.voidTransaction(GetToken(pRequest.TestCard), pRequest.RefNum);
		}



		/// <summary>
		/// credits a customers credit card, like a refund but without need for a reference nol.
		/// </summary>
		/// <param name="pRequest">The credit/refund request.</param>
		/// <returns></returns>
		public ProcessorResponse OpenCredit(CCRequest pRequest) {
			if (pRequest.CardData.TestCard) {
				mService.Url = mCredentials.TestURL;
			}

			//skip USAePay if we do not have a URL to post to
			if (mService.Url.IsNullOrEmpty()) {
				return null;
			}

			com.USAePay.TransactionRequestObject trans = FillCardInfo(pRequest);

			var ePayResponse = mService.runCredit(GetToken(pRequest.CardData.TestCard), trans);

			return ProcessResponse(ePayResponse, pRequest.Amount, TrxTypes.Credit, pRequest.CardData.TestCard);
		}

		/// <summary>
		/// Processes the response.
		/// </summary>
		/// <param name="pResponse">The response.</param>
		/// <param name="pAuthAmount">The auth amount.</param>
		/// <param name="pTrxType">Type of the Transaction.</param>
		/// <param name="pTestCard">if set to <c>true</c> [test card].</param>
		/// <returns></returns>
		private ProcessorResponse ProcessResponse(com.USAePay.TransactionResponse pResponse, decimal pAuthAmount, string pTrxType, bool pTestCard) {
			ProcessorResponse oResult = new ProcessorResponse();

			oResult.TestCard = pTestCard;
			oResult.VerisignAccountID = mCredentials.VerisignAccountID;
			oResult.MerchantID = mCredentials.MerchantID;
			oResult.CreditCardProcessor = mCredentials.MyGateway();

			if (("" + pResponse.Error).Equals("AP", StringComparison.CurrentCultureIgnoreCase)) {
				pResponse.Error = "Approved";
			}



			oResult.Result = pResponse.ErrorCode;
			oResult.Reference = pResponse.RefNum;
			oResult.Message = pResponse.Error;
			oResult.Amount = (decimal)pResponse.ConvertedAmount;
			if (oResult.Amount == 0) {
				oResult.Amount = pAuthAmount;
			}
			if (oResult.Result.IsNullOrEmpty()) {
				oResult.Result = "0";
			}
			//fixed for Chase results
			if (oResult.Result != "0" && ("" + oResult.Message).StartsWith("Approv", StringComparison.CurrentCultureIgnoreCase)) {
				oResult.Result = "0";
			}

			if (oResult.Result == "0" && (oResult.Message.IsNullOrEmpty() || oResult.Message.Equals("Transaction Approved", StringComparison.CurrentCultureIgnoreCase))) {
				oResult.Message = "Approved";
			}

			if (pResponse.AvsResultCode.Length == 1) {
				pResponse.AvsResultCode = ConvertToFullAVSResultCode(pResponse.AvsResultCode);
			}

			oResult.AuthCode = pResponse.AuthCode;
			if (pResponse.AvsResultCode.IsNullOrEmpty()) {
				oResult.AVS = "";
				oResult.AVSZip = "";
			} else {
				oResult.AVS = pResponse.AvsResultCode.Substring(0, 1);
				try {
					oResult.AVSZip = pResponse.AvsResultCode.Substring(1, 1);
				} catch (Exception) {
					oResult.AVSZip = oResult.AVS;
				}
			}
			oResult.CVV2Match = pResponse.CardCodeResultCode;

			oResult.TransType = pTrxType;
			oResult.TransDate = DateTime.Now;

			// 12/5/11 Added for Carmel
			oResult.AVSResultCode = pResponse.AvsResultCode;

			//placed as USAePay is now sending us failed trans with a result code of 0
			if (("" + oResult.Result).Equals("0")) {
				if (!oResult.Message.IsNullOrEmpty() && !oResult.Message.ToUpper().Contains("APPROV")) {
					if (!oResult.Message.Equals("Unable to get tran details (p)", StringComparison.CurrentCultureIgnoreCase)) {
						oResult.Result = "1";
					}
				}
			}

			return oResult;
		}


		/// <summary>
		/// Gets the token to process a USAePay transaction.
		/// </summary>
		/// <param name="pTestCard">if set to <c>true</c> then card we are processing is a known test card.</param>
		/// <returns></returns>
		private com.USAePay.ueSecurityToken GetToken(bool pTestCard) {
			com.USAePay.ueSecurityToken token = new com.USAePay.ueSecurityToken();
			token.SourceKey = pTestCard ? mCredentials.SourceKeyTest : mCredentials.SourceKey;
			token.ClientIP = "127.0.0.1";
			token.PinHash = new com.USAePay.ueHash();
			token.PinHash.Seed = pTestCard ? mCredentials.PinTest : mCredentials.Pin;
			token.PinHash.Type = "md5";

			string preHashValue = token.SourceKey + token.PinHash.Seed + token.PinHash.Seed; //Put together the pieces
			token.PinHash.HashValue = GenerateHash(preHashValue);

			return token;
		}


		/// <summary>
		/// Generates the hash key required by USAePay.
		/// </summary>
		/// <param name="SourceText">The source text.</param>
		/// <returns></returns>
		private string GenerateHash(string SourceText) {
			MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();

			byte[] ByteHash = md5.ComputeHash(Encoding.Default.GetBytes(SourceText));

			StringBuilder sb = new StringBuilder();
			for (int c = 0; c <= ByteHash.Length - 1; c++) {
				sb.AppendFormat("{0:x2}", ByteHash[c]);
			}

			return sb.ToString();
		}



		/// <summary>
		/// Gets the transaction history.
		/// </summary>
		/// <param name="pStart">The p start.</param>
		/// <param name="pEnd">The p end.</param>
		/// <returns></returns>
		public string GetTransactionHistory(DateTime pStart, DateTime pEnd) {
			string sData = mService.getTransactionReport(GetToken(false), pStart.ToString("yyyy/MM/dd"), pEnd.ToString("yyyy/MM/dd"), "CreditCard: Sales by Date", "csv");
			return sData.Base64Decode();
		}

		public List<CheckStatus> GetCheckTransactionHistory(DateTime pStart, DateTime pEnd) {
			string format = "csv";
			string report = "Check: All Transactions by Date";

			mService.Timeout = 60000 * 5;
			string data = mService.getTransactionReport(GetToken(false), pStart.ToString("yyyy/MM/dd"), pEnd.ToString("yyyy/MM/dd"), report, format);

			return CreateCheckStatusList(data);
		}


		public static List<CheckStatus> CreateCheckStatusList(string pData) {
			string temp = pData.Base64Decode();

			List<string> dataList = temp.Split(new string[] { "\n" }, StringSplitOptions.RemoveEmptyEntries).ToList();
			dataList[0] = dataList[0].Replace(" ", "").Replace("Error", "StatusMessage");
			string data = string.Join("\r\n", dataList);


			using (TextReader sr = new StringReader(data)) {
				var csv = new CsvReader(sr);
				List<CheckStatus> checkStatuses = csv.GetRecords<CheckStatus>().ToList();
				checkStatuses = checkStatuses.Where(p => !p.StatusMessage.Equals("Back Office Exception (61)", StringComparison.CurrentCultureIgnoreCase)).ToList();
				return checkStatuses;
			}
		}

		public com.USAePay.TransactionObject GetTransaction(string pRefNum, bool pTestSystem) {
			return mService.getTransaction(GetToken(pTestSystem), pRefNum);
		}

		/// <summary>
		/// Searches the transactions by date.
		/// </summary>
		/// <param name="pDate">The p date.</param>
		/// <returns></returns>
		public com.USAePay.TransactionSearchResult SearchTransactionsByDate(DateTime pDate, int pLimit = 1000) {
			Boolean matchAll = true;

			com.USAePay.SearchParam myStart = new com.USAePay.SearchParam();
			myStart.Field = "DateTime";
			myStart.Type = "gt";
			myStart.Value = pDate.ToString("yyyy-MM-dd");

			List<com.USAePay.SearchParam> search = new List<com.USAePay.SearchParam>();
			search.Add(myStart);

			com.USAePay.TransactionSearchResult result = new com.USAePay.TransactionSearchResult();

			int pos = 0;
			int limit = pLimit;  // size of result set

			result = mService.searchTransactions(GetToken(false), search.ToArray(), matchAll, pos.ToString(), limit.ToString(), "");
			if (limit > 999) {
				List<com.USAePay.TransactionObject> transList = new List<com.USAePay.TransactionObject>(result.Transactions);
				while (Convert.ToInt32(result.TransactionsMatched) > transList.Count) {
					pos += 1000;
					var moreResult = mService.searchTransactions(GetToken(false), search.ToArray(), matchAll, pos.ToString(), limit.ToString(), "");
					transList.AddRange(moreResult.Transactions);
				}
				result.Transactions = transList.ToArray();
			}
			return result;
		}

		/// <summary>
		/// Searches the transactions by date.
		/// </summary>
		/// <param name="pDate">The p date.</param>
		/// <returns></returns>
		public com.USAePay.TransactionSearchResult SearchTransactionsByDate(DateTime pStartDate, DateTime pEndDate, int pLimit = 9999) {
			Boolean matchAll = true;

			com.USAePay.SearchParam myStart = new com.USAePay.SearchParam();
			myStart.Field = "DateTime";
			myStart.Type = "gt";
			myStart.Value = pStartDate.ToString("yyyy-MM-dd");

			com.USAePay.SearchParam myEnd = new com.USAePay.SearchParam();
			myEnd.Field = "DateTime";
			myEnd.Type = "lt";
			myEnd.Value = pEndDate.AddDays(1).ToString("yyyy-MM-dd");

			List<com.USAePay.SearchParam> search = new List<com.USAePay.SearchParam>();
			search.Add(myStart);
			search.Add(myEnd);

			com.USAePay.TransactionSearchResult result = new com.USAePay.TransactionSearchResult();

			int pos = 0;
			int limit = pLimit;  // size of result set

			result = mService.searchTransactions(GetToken(false), search.ToArray(), matchAll, pos.ToString(), limit.ToString(), "");
			if (limit > 999) {
				List<com.USAePay.TransactionObject> transList = new List<com.USAePay.TransactionObject>(result.Transactions);
				while (Convert.ToInt32(result.TransactionsMatched) > transList.Count) {
					pos += 1000;
					var moreResult = mService.searchTransactions(GetToken(false), search.ToArray(), matchAll, pos.ToString(), limit.ToString(), "");
					transList.AddRange(moreResult.Transactions);
				}
				result.Transactions = transList.ToArray();
			}

			List<string> batchNumbers = (from p in result.Transactions
										 select p.Response.BatchNum).Distinct().ToList();
			foreach (string batchNo in batchNumbers) {
				var batchInfo = GetBatchInfo(batchNo);
				foreach (var trans in result.Transactions) {
					if (trans.Response.BatchNum == batchNo) {
						//trans.Response.BatchDate = batchInfo[0].DateTime;
					}
				}
			}

			return result;
		}


		public BatchStatus GetBatchStatus(string pBatchNum) {
			BatchStatus result = mService.getBatchStatus(GetToken(false), pBatchNum);
			return result;
		}

		/// <summary>
		/// Searches the transactions by date.
		/// </summary>
		/// <param name="pDate">The p date.</param>
		/// <returns></returns>
		//public com.USAePay.TransactionSearchResult SearchVTermTransactionsByDate(DateTime pStartDate, DateTime pEndDate, int pLimit = 9999) {
		//    Boolean matchAll = true;

		//    com.USAePay.SearchParam myStart = new com.USAePay.SearchParam();
		//    myStart.Field = "DateTime";
		//    myStart.Type = "gt";
		//    myStart.Value = pStartDate.ToString("yyyy-MM-dd");

		//    com.USAePay.SearchParam myEnd = new com.USAePay.SearchParam();
		//    myEnd.Field = "DateTime";
		//    myEnd.Type = "lt";
		//    myEnd.Value = pEndDate.AddDays(1).ToString("yyyy-MM-dd");

		//    com.USAePay.SearchParam source = new com.USAePay.SearchParam();
		//    myEnd.Field = "source";
		//    myEnd.Type = "eq";
		//    myEnd.Value = "vterm";

		//    List<com.USAePay.SearchParam> search = new List<com.USAePay.SearchParam>();
		//    search.Add(myStart);
		//    search.Add(myEnd);

		//    com.USAePay.TransactionSearchResult result = new com.USAePay.TransactionSearchResult();

		//    int pos = 0;
		//    int limit = pLimit;  // size of result set

		//    result = mService.searchTransactions(GetToken(false), search.ToArray(), matchAll, pos.ToString(), limit.ToString());
		//    if (limit > 999) {
		//        List<com.USAePay.TransactionObject> transList = new List<com.USAePay.TransactionObject>(result.Transactions);
		//        while (Convert.ToInt32(result.TransactionsMatched) > transList.Count) {
		//            pos += 1000;
		//            var moreResult = mService.searchTransactions(GetToken(false), search.ToArray(), matchAll, pos.ToString(), limit.ToString());
		//            transList.AddRange(moreResult.Transactions);
		//        }
		//        result.Transactions = transList.ToArray();
		//    }
		//    return result;
		//}



		/// <summary>
		/// Searches the transactions by auth code.
		/// </summary>
		/// <param name="pAuthCode">The p auth code.</param>
		/// <param name="pTestCard">if set to <c>true</c> [p test card].</param>
		/// <returns></returns>
		public com.USAePay.TransactionSearchResult SearchTransactionsByAuthCode(string pAuthCode, bool pTestCard) {
			Boolean matchAll = true;

			com.USAePay.SearchParam myStart = new com.USAePay.SearchParam();
			myStart.Field = "AuthCode";
			myStart.Type = "eq";
			myStart.Value = pAuthCode;

			List<com.USAePay.SearchParam> search = new List<com.USAePay.SearchParam>();
			search.Add(myStart);

			com.USAePay.TransactionSearchResult result = new com.USAePay.TransactionSearchResult();

			int pos = 0;
			int limit = 9999;  // size of result set

			result = mService.searchTransactions(GetToken(pTestCard), search.ToArray(), matchAll, pos.ToString(), limit.ToString(), "");
			return result;
		}

		/// <summary>
		/// Searches the transactions by trans no.
		/// </summary>
		/// <param name="pTransNo">The p trans no.</param>
		/// <returns></returns>
		public com.USAePay.TransactionSearchResult SearchTransactionsByTransNo(string pTransNo) {
			Boolean matchAll = true;

			com.USAePay.SearchParam myStart = new com.USAePay.SearchParam();
			myStart.Field = "PoNum";
			myStart.Type = "eq";
			myStart.Value = pTransNo;

			List<com.USAePay.SearchParam> search = new List<com.USAePay.SearchParam>();
			search.Add(myStart);

			com.USAePay.TransactionSearchResult result = new com.USAePay.TransactionSearchResult();

			int pos = 0;
			int limit = 10;  // size of result set

			mService.Timeout = 5 * 60 * 1000;
			result = mService.searchTransactions(GetToken(false), search.ToArray(), matchAll, pos.ToString(), limit.ToString(), "");

			return result;
		}

		/// <summary>
		/// Searches the transactions by ref num.
		/// </summary>
		/// <param name="pRefNum">The p ref num.</param>
		/// <returns></returns>
		public com.USAePay.TransactionSearchResult SearchTransactionsByRefNum(string pRefNum) {
			Boolean matchAll = true;

			com.USAePay.SearchParam myStart = new com.USAePay.SearchParam();
			myStart.Field = "response.refnum";
			myStart.Type = "eq";
			myStart.Value = pRefNum;

			List<com.USAePay.SearchParam> search = new List<com.USAePay.SearchParam>();
			search.Add(myStart);

			com.USAePay.TransactionSearchResult result = new com.USAePay.TransactionSearchResult();

			int pos = 0;
			int limit = 9999;  // size of result set

			result = mService.searchTransactions(GetToken(false), search.ToArray(), matchAll, pos.ToString(), limit.ToString(), "");

			return result;
		}

		/// <summary>
		/// Gets the batch numbers.
		/// </summary>
		/// <param name="pStart">The p start.</param>
		/// <param name="pEnd">The p end.</param>
		/// <returns></returns>
		public com.USAePay.BatchSearchResult GetBatchNumbers(DateTime pStart, DateTime pEnd) {
			bool matchAll = true;
			string start = "0";
			string limit = "10000";  //max number of items that can be returned

			com.USAePay.SearchParam myStart = new com.USAePay.SearchParam();
			myStart.Field = "Closed";
			myStart.Type = "gt";
			myStart.Value = pStart.ToString("yyyy-MM-dd");

			com.USAePay.SearchParam myEnd = new com.USAePay.SearchParam();
			myEnd.Field = "Closed";
			myEnd.Type = "lt";
			myEnd.Value = pEnd.AddDays(1).ToString("yyyy-MM-dd");

			List<com.USAePay.SearchParam> search = new List<com.USAePay.SearchParam>();
			search.Add(myStart);
			search.Add(myEnd);

			return mService.searchBatches(GetToken(false), search.ToArray(), matchAll, start, limit, "");
		}

		/// <summary>
		/// Gets the open batch.
		/// </summary>
		/// <returns></returns>
		public com.USAePay.BatchStatus GetOpenBatch() {
			return mService.getBatchStatus(GetToken(false), "0");
		}

		/// <summary>
		/// Closes the batch.
		/// </summary>
		/// <param name="pBatchNumber">The p batch number.</param>
		/// <returns></returns>
		public bool CloseBatch(string pBatchNumber) {
			return mService.closeBatch(GetToken(false), pBatchNumber);
		}

		/// <summary>
		/// Gets the batch info.
		/// </summary>
		/// <param name="pBatchNumber">The p batch number.</param>
		/// <returns></returns>
		public List<com.USAePay.TransactionObject> GetBatchInfo(string pBatchNumber) {
			return mService.getBatchTransactions(GetToken(false), pBatchNumber).ToList();
		}

		private string ConvertToFullAVSResultCode(string pAVSResult) {
			if (pAVSResult.IsNullOrEmpty()) {
				return pAVSResult;
			}

			string result = pAVSResult;

			switch (result) {
				case "Y": // 5-Digit zip and address match
					result = "YYY";
					break;

				case "X": // 9 - Digit zip and address match
					result = "YYX";
					break;

				case "02": // Postal code and address match
					result = "GGG";
					break;

				case "Z": // 5 - Digit zip matches, address does not match
					result = "NYZ";
					break;

				case "W": // 9 - Digit zip matches, address does not match
					result = "NYW";
					break;

				case "A": // Zip does not match, address matches
					result = "YNA";
					break;

				case "13": // Postal code does not match, address matches
					result = "YYG";
					break;

				case "14": // Postal code matches, address not verified
					result = "YNN";
					break;

				case "N": // Neither zip nor address match
					result = "NNN";
					break;

				case "G": // AVS service not supported by issuer
					result = "XXS";
					break;

				case "U": // AVS system not available
				case "R": // Address unavailable
				case "S": // General error
				case "40": // Address failed Litle & Co.edit checks
					result = "XXR";
					break;

				case "34": // AVS not performed
					result = "XXU";
					break;

			}
			return result;
		}

		public ProcessorResponse AvsCheck(CCRequest pRequest) {
			throw new NotImplementedException();
		}

		public ProcessorResponse ProcessCheck(CheckRequest pRequest) {
			if (pRequest.CheckData.TestAccount) {
				mService.Url = mCredentials.TestURL;
			}

			//skip USAePay if we do not have a URL to post to
			if (mService.Url.IsNullOrEmpty()) {
				return null;
			}

			TransactionRequestObject trans = FillCheckInfo(pRequest);
			var ePayResponse = mService.runCheckSale(GetToken(pRequest.CheckData.TestAccount), trans);

			return ProcessResponse(ePayResponse, pRequest.Amount, TrxTypes.Sale, pRequest.CheckData.TestAccount);
		}

		private TransactionRequestObject FillCheckInfo(CheckRequest pRequest) {
			CheckData data = new CheckData();

			data.Account = pRequest.CheckData.AccountNo;
			data.AccountType = pRequest.CheckData.AccountType.ToString();
			data.CheckNumber = pRequest.CheckData.CheckNo;
			data.DriversLicense = pRequest.CheckData.DriverLicenseNo;
			data.DriversLicenseState = pRequest.CheckData.DriverLicenseState;
			data.Routing = pRequest.CheckData.RoutingNo;
			//data.SSN = pRequest.CheckData.SSN;

			TransactionRequestObject trans = new TransactionRequestObject();
			trans.CheckData = data;

			trans.Details = new TransactionDetail();
			trans.Details.Amount = (double)pRequest.Amount;
			trans.Details.AmountSpecified = true;
			trans.Details.NonTax = true;
			trans.Details.Tax = 0;

			trans.Details.Comments = pRequest.Comments;
			trans.Details.OrderID = pRequest.VoucherNo;

			trans.Details.PONum = pRequest.VoucherNo;
			trans.Details.Description = pRequest.Service;

			trans.Details.Clerk = pRequest.CheckData.SalesPerson;
			if (trans.Details.Clerk.IsNullOrEmpty()) {
				trans.Details.Clerk = string.Format("D: {0}", pRequest.DriverID);
			}

			trans.Details.Terminal = pRequest.DriverID.ToString();
			trans.AccountHolder = pRequest.CheckData.CustomerName;

			return trans;
		}

		public ProcessorResponse RefundCheck(CheckRequest pRequest) {
			throw new NotImplementedException();
		}

	}
}
