﻿using RestSharp;
using System;

namespace TaxiPassCommon.Banking {
	public class BinList {

		public static BinList GetCardInfo(string pCardNumber, string pCardType) {
			BinList bin = new BinList();
			bin.type = "CREDIT";

			if (pCardNumber.IsNullOrEmpty() || !pCardNumber.IsNumeric() || ("" + pCardType).Equals("AMEX", StringComparison.CurrentCultureIgnoreCase)) {
				return bin;
			}

			try {
				string url = $"https://lookup.binlist.net/{pCardNumber.Left(6)}";

				RestClient client = new RestClient(url);
				return client.Execute<BinList>(new RestRequest()).Data;
			} catch {
				return null;
			}
		}


		public string scheme { get; set; }
		public Number number { get; set; }
		public string type { get; set; }
		public string brand { get; set; }
		public bool prepaid { get; set; }
		public Bank bank { get; set; }
		public Country country { get; set; }


		public class Number {
			public int length { get; set; }
			public string prefix { get; set; }
			public bool luhn { get; set; }
		}

		public class Bank {
			public string name { get; set; }
			public string logo { get; set; }
			public string url { get; set; }
			public string city { get; set; }
			public string phone { get; set; }
		}

		public class Country {
			public string alpha2 { get; set; }
			public string name { get; set; }
			public string numeric { get; set; }
			public int latitude { get; set; }
			public int longitude { get; set; }
		}




	}
}
