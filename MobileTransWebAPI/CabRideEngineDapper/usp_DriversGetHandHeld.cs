﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TaxiPassCommon;

namespace CabRideEngineDapper {
	public class usp_DriversGetHandHeld {
		public long DriverID { get; set; }
		public bool Active { get; set; }
		public string Name { get; set; }
		public string CellPhone { get; set; }
		public string BoostPhone { get; set; }
		public string AppVersion { get; set; }
		public string DriverNo { get; set; }
		public string VehicleNo { get; set; }
		public bool DriverRideQ { get; set; }
		public bool AffiliateRideQ { get; set; }
		public string AffiliateName { get; set; }
		public DateTime? GPSPulse { get; set; }
		public bool ProfileURL { get; set; }
		public bool Assigned { get; set; }
		public bool VIP { get; set; }
		public bool LicenseValidated { get; set; }
		public string RideAvailability { get; set; }

		public string CellPhoneFormatted {
			get {
				return CellPhone.FormattedPhoneNumber();
			}
		}

		public string BoostPhoneFormatted {
			get {
				return BoostPhone.FormattedPhoneNumber();
			}
		}

	}
}
