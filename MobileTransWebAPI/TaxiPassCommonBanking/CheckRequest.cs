﻿namespace TaxiPassCommon.Banking {
    public class CheckRequest {

        public BankCheck CheckData = new BankCheck();
        public decimal Amount { get; set; }
        public decimal SplitPayAmount { get; set; }

        public string Comments { get; set; }
        public string VoucherNo { get; set; }

        public string Service { get; set; }
        public string Platform { get; set; }

        public string DriverID { get; set; }

        public string AffiliateBillingDescriptor { get; set; }

        public CheckRequest(BankCheck pCheckData, decimal pAmount, string pComments, string pVoucherNo, string pService, string pPlatform, string pDriverID, decimal pSplitPayAmount = 0) {
            CheckData = pCheckData;
            Amount = pAmount;
            Comments = pComments;
            VoucherNo = pVoucherNo;
            Service = pService;
            Platform = pPlatform;
            DriverID = pDriverID;

            SplitPayAmount = pSplitPayAmount;
        }
    }
}
