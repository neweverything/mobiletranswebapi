﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Util;

namespace CabRideEngine {
    [Serializable]
    public sealed class ParkingLocations : ParkingLocationsDataRow {

        #region Constructors -- DO NOT MODIFY
        // Do not create constructors for this class
        // Developers cannot create instances of this class with the "new" keyword
        // You should write a static Create method instead; see the "Suggested Customization" region
        // Please review the documentation to learn about entity creation and inheritance

        // This private constructor prevents accidental instance creation via 'new'
        private ParkingLocations() : this(null) {
        }

        // Typed DataSet constructor (needed by framework) - DO NOT REMOVE
        public ParkingLocations(DataRowBuilder pRowBuilder)
            : base(pRowBuilder) {
        }

        #endregion

        #region Suggested Customizations

        //    // Use this factory method template to create new instances of this class
        //    public static ParkingLocations Create(PersistenceManager pManager,
        //      ... your creation parameters here ...) { 
        //
        //      ParkingLocations aParkingLocations = pManager.CreateEntity<ParkingLocations>();
        //
        //      // if this object type requires a unique id and you have implemented
        //      // the IIdGenerator interface implement the following line
        //      pManager.GenerateId(aParkingLocations, // add id column here //);
        //
        //      // Add custom code here
        //
        //      aParkingLocations.AddToManager();
        //      return aParkingLocations;
        //    }

        //    // Implement this method if you want your object to sort in a specific order
        //    public override int CompareTo(Object pObject) {
        //    }

        //    // Implement this method to customize the null object version of this class
        //    protected override void UpdateNullEntity() {
        //    }

        #endregion

        // Add additional logic to your business object here...
        public static ParkingLocations Create(PersistenceManager pPM, long pParkingCompanyID) {
            ParkingLocations oLocation = null;
            try {
                oLocation = (ParkingLocations)pPM.CreateEntity(typeof(ParkingLocations));
                oLocation.ParkingCompanyID = pParkingCompanyID;

                pPM.GenerateId(oLocation, ParkingLocations.ParkingLocationIDEntityColumn);

                oLocation.AddToManager();

            } catch (Exception ex) {
                throw ex;
            }
            return oLocation;
        }

        protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
            if (IsDeserializing)
                return;
            base.OnColumnChanging(pArgs);
            DoAudit(pArgs);
        }

        private void DoAudit(DataColumnChangeEventArgs pArgs) {
            if (!this.PersistenceManager.IsClient) {
                return;
            }
            if (this.RowState == DataRowState.Added) {
                return;
            }
            if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
                AuditRecord = ParkingLocationsAudit.Create(this.PersistenceManager, this);
            }
            //AuditRecord[pArgs.Column.ColumnName] = pArgs.ProposedValue;
        }

    }

}
