﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

using Csla.Validation;

namespace CabRideEngine {
	[Serializable]
	public sealed class AccountTypes : AccountTypesDataRow {


		#region Constructors -- DO NOT MODIFY
		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new'
		private AccountTypes() : this(null) {
		}

		// Typed DataSet constructor (needed by framework) - DO NOT REMOVE
		public AccountTypes(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static AccountTypes Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      AccountTypes aAccountTypes = pManager.CreateEntity<AccountTypes>();
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aAccountTypes, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aAccountTypes.AddToManager();
		//      return aAccountTypes;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		// Add additional logic to your business object here...
		public static AccountTypes Create(PersistenceManager pManager) {

			AccountTypes oAddress = (AccountTypes)pManager.CreateEntity(typeof(AccountTypes));

			//pManager.GenerateId(oAddress, AccountTypes.AccountTypeIDEntityColumn);
			oAddress.AddToManager();
			return oAddress;
		}

		public override string AccountType {
			get {
				return base.AccountType;
			}
			set {
				if (base.AccountType != null && base.AccountType.ToUpper() == "CORPORATE") {
					if (base.AccountType.ToUpper() != value.ToUpper()) {
						throw new Exception("Corporate account type cannot be modified");
					}
				}
				base.AccountType = value;
			}
		}
		protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
			if (IsDeserializing)
				return;
			base.OnColumnChanging(pArgs);
			//DoAudit(pArgs);
		}

		private void DoAudit(DataColumnChangeEventArgs pArgs) {
			if (!this.PersistenceManager.IsClient) {
				return;
			}
            if (this.RowState == DataRowState.Added) {
                return;
            }
			if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
                AuditRecord = AccountTypesAudit.Create(this.PersistenceManager, this);
			}
			//AuditRecord[pArgs.Column.ColumnName] = pArgs.ProposedValue;
		}


		protected override void AddRules(RuleList pList) {
			PropertyRequiredRule.AddToList(pList, AccountTypeEntityColumn.ColumnName);
			pList.Add(IsAccountTypeUnique, AccountTypeEntityColumn.ColumnName);

			//base.AddRules(pList);
		}

		private bool IsAccountTypeUnique(object pTarget, RuleArgs e) {
			AccountTypes oType = (AccountTypes)pTarget;

			RdbQuery oQry = new RdbQuery(typeof(AccountTypes), AccountTypes.AccountTypeEntityColumn, EntityQueryOp.EQ, oType.AccountType);
			oQry.AddClause(AccountTypes.AccountTypeIDEntityColumn, EntityQueryOp.NE, oType.AccountTypeID);
			EntityList<AccountTypes> oAccountTypes = this.PersistenceManager.GetEntities<AccountTypes>(oQry);
			if (oAccountTypes.Count > 0) {
				e.Description = "Address Type must be unique!";
				return false;
			}
			return true;
		}

		[BindingBrowsable(false)]
		public override string EntityInstanceDisplayName {
			get {
				return AccountType;
			}
		}


	}

}
