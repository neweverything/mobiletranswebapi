using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Runtime.Serialization;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

namespace CabRideEngine {
 
  #region RateTypeDataTable
  /// <summary>
  /// The generated <see cref="T:IdeaBlade.Persistence.EntityTable"/> class  
  /// containing all <see cref="RateType"/> objects.
  /// </summary>
  /// <remarks>
  /// ****************************************************
  /// <para>** DO NOT MODIFY THIS CLASS - AutoGenerated Code</para>
  /// <para>** Any changes made to this code will be lost</para>
  /// <para>** the next time this code is regenerated.</para>
  /// <para>**</para>
  /// <para>** Generated from:   EntityBaseclass.template</para>
  /// <para>** Last modified:    2014-11-21T12:08:22.2249942-08:00</para>
  /// <para>** Template version: 3.110127</para>
  /// ****************************************************
  /// </remarks>
  [BindingBrowsable(false)]
  [Serializable]
  public partial class RateTypeDataTable : BaseEntityDataTable, 
     IdeaBlade.Persistence.Rdb.IRdbTable {
  
    /// <summary>
    /// Initializes a new instance of the RateTypeDataTable class with no arguments.
    /// </summary>
    public RateTypeDataTable() {}

    /// <summary>
    /// Initializes a new instance of the RateTypeDataTable class 
    /// with the <see cref="System.Runtime.Serialization.SerializationInfo"/> 
    /// and the <see cref="System.Runtime.Serialization.StreamingContext"/>.
    /// </summary>
    /// <param name="pInfo">The data needed to serialize or deserialize an object.</param>
    /// <param name="pContext">The source and destination of a given serialized stream.</param>
    protected RateTypeDataTable(SerializationInfo pInfo, StreamingContext pContext) :
      base(pInfo, pContext) {}

    /// <summary>
    /// Creates a new instance of the <see cref="RateTypeDataRow"/>.
    /// Constructs a row from the builder.
    /// <para>** For internal use only. **</para>
    /// </summary>
    /// <param name="pRowBuilder"></param>
    /// <returns>The <see cref="RateTypeDataRow"/>.</returns>
    protected override DataRow NewRowFromBuilder(DataRowBuilder pRowBuilder) {
      return new RateType(pRowBuilder);
    }

    /// <summary>Returns the <see cref="System.Type"/> of <see cref="RateType"/>.</summary>
    protected override Type GetRowType() {
      return typeof(RateType);
    }
    
    /// <summary>
    /// Gets the name of the <see cref="T:IdeaBlade.Rdb.RdbKey"/> or 
    /// <see cref="T:IdeaBlade.Persistence.WS.WsKey"/> defined in <b>IdeaBlade.ibconfig</b>. 
    /// </summary>
    /// <remarks>For this <see cref="RateType"/>, the DataSourceKeyName="default".</remarks>
    public override String DataSourceKeyName {
      get { return @"default"; }
    }

    #region Column properties
    //**************************************
    //* Column properties
    //**************************************
    /// <summary>Gets the RateTypeID <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn RateTypeIDColumn {
      get {
        if (mRateTypeIDColumn!=null) return mRateTypeIDColumn;
        mRateTypeIDColumn = GetColumn("RateTypeID", true);
        return mRateTypeIDColumn;
      }
    }
    private DataColumn mRateTypeIDColumn;
    
    /// <summary>Gets the RateType <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn RateTypeColumn {
      get {
        if (mRateTypeColumn!=null) return mRateTypeColumn;
        mRateTypeColumn = GetColumn("RateType", true);
        return mRateTypeColumn;
      }
    }
    private DataColumn mRateTypeColumn;
    
    /// <summary>Gets the AffiliateID <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn AffiliateIDColumn {
      get {
        if (mAffiliateIDColumn!=null) return mAffiliateIDColumn;
        mAffiliateIDColumn = GetColumn("AffiliateID", true);
        return mAffiliateIDColumn;
      }
    }
    private DataColumn mAffiliateIDColumn;
    
    /// <summary>Gets the RowVersion <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn RowVersionColumn {
      get {
        if (mRowVersionColumn!=null) return mRowVersionColumn;
        mRowVersionColumn = GetColumn("RowVersion", true);
        return mRowVersionColumn;
      }
    }
    private DataColumn mRowVersionColumn;
    
    /// <summary>Gets the ModifiedBy <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn ModifiedByColumn {
      get {
        if (mModifiedByColumn!=null) return mModifiedByColumn;
        mModifiedByColumn = GetColumn("ModifiedBy", true);
        return mModifiedByColumn;
      }
    }
    private DataColumn mModifiedByColumn;
    
    /// <summary>Gets the ModifiedDate <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn ModifiedDateColumn {
      get {
        if (mModifiedDateColumn!=null) return mModifiedDateColumn;
        mModifiedDateColumn = GetColumn("ModifiedDate", true);
        return mModifiedDateColumn;
      }
    }
    private DataColumn mModifiedDateColumn;
    
    /// <summary>Gets the CreatedBy <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn CreatedByColumn {
      get {
        if (mCreatedByColumn!=null) return mCreatedByColumn;
        mCreatedByColumn = GetColumn("CreatedBy", true);
        return mCreatedByColumn;
      }
    }
    private DataColumn mCreatedByColumn;
    
    /// <summary>Gets the CreatedDate <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn CreatedDateColumn {
      get {
        if (mCreatedDateColumn!=null) return mCreatedDateColumn;
        mCreatedDateColumn = GetColumn("CreatedDate", true);
        return mCreatedDateColumn;
      }
    }
    private DataColumn mCreatedDateColumn;
    
    #endregion    

    /// <summary>Creates the schema mapping information for this RateTypeDataTable.</summary>
    /// <returns>
    /// The <see cref="T:IdeaBlade.Persistence.TableMappingInfo"/> for this RateTypeDataTable.
    /// </returns>
    protected override TableMappingInfo CreateTableMappingInfo() {
      RdbTableMappingInfo mappingInfo = new RdbTableMappingInfo(this);
      mappingInfo.SourceOwnerName = "dbo"; 
      mappingInfo.SourceTableName = "RateType"; 
      mappingInfo.ConcurrencyColumnName = "RowVersion"; 
      // ColumnSelectionOption: selectAllColumns
      mappingInfo.SourceColumnNames = "*"; 
   
      DataColumnMappingCollection columnMappings = mappingInfo.TableMapping.ColumnMappings;
      columnMappings.Clear();
      columnMappings.Add("RateTypeID", "RateTypeID");
      columnMappings.Add("RateType", "RateType");
      columnMappings.Add("AffiliateID", "AffiliateID");
      columnMappings.Add("RowVersion", "RowVersion");
      columnMappings.Add("ModifiedBy", "ModifiedBy");
      columnMappings.Add("ModifiedDate", "ModifiedDate");
      columnMappings.Add("CreatedBy", "CreatedBy");
      columnMappings.Add("CreatedDate", "CreatedDate");
      return mappingInfo;
    }
  
    /// <summary>** For internal use only. **</summary>
    protected override void UpdateTableMappingInfo() {
      base.UpdateTableMappingInfo();
      this.TableMappingInfo.ColumnUpdateMap["RowVersion"] = "RowVersion + 1";
    }

    /// <summary>
    /// Initializes RateType <see cref="System.Data.DataColumn"/> properties.
    /// </summary>
    public override void InitColumnProperties() {
      base.InitColumnProperties();

      RateTypeIDColumn.Caption = "RateTypeID";
      RateTypeColumn.Caption = "RateType";
      AffiliateIDColumn.Caption = "AffiliateID";
      RowVersionColumn.Caption = "RowVersion";
      ModifiedByColumn.Caption = "ModifiedBy";
      ModifiedDateColumn.Caption = "ModifiedDate";
      CreatedByColumn.Caption = "CreatedBy";
      CreatedDateColumn.Caption = "CreatedDate";
    }
  }
  #endregion
  
  #region RateTypeDataRow
  /// <summary>
  /// The generated base class for <see cref="RateType"/>.
  /// </summary>
  /// <remarks>
  /// <para>
  /// It represents a row of persistable data in the <see cref="RateTypeDataTable"/>
  /// and derives from <see cref="T:IdeaBlade.Persistence.Entity"/>.
	/// </para>
  /// ****************************************************
  /// <para>** DO NOT MODIFY THIS CLASS - AutoGenerated Code</para>
  /// <para>** Any changes made to this code will be lost</para>
  /// <para>** the next time this code is regenerated.</para>
  /// <para>**</para>
  /// <para>** Generated from:   EntityBaseclass.template</para>
  /// <para>** Last modified:    2014-11-21T12:08:22.2249942-08:00</para>
  /// <para>** Template version: 3.110127</para>
  /// ****************************************************
  /// </remarks>  
  [Serializable]
  public abstract partial class RateTypeDataRow : BaseEntity, 
     IdeaBlade.Persistence.Rdb.IRdbEntity {
  
    /// <summary>
    /// Initializes a new instance of the RateTypeDataRow.
    /// Constructs a row from the builder.
    /// <para>** For internal use only. **</para>
    /// </summary>
    /// <param name="pRowBuilder"></param>
    protected RateTypeDataRow(DataRowBuilder pRowBuilder)
      : base(pRowBuilder) {
    }

    /// <summary>Gets the <see cref="RateTypeDataTable"/>.</summary>
    [BindingBrowsable(false)]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public new RateTypeDataTable TypedTable {
      get { return (RateTypeDataTable) base.Table; }
    }

    /// <summary>Returns the <see cref="System.Type"/> of <see cref="RateTypeDataTable"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public static Type GetTableType() {
      return typeof(RateTypeDataTable);
    }

    /// <summary>
    /// Gets the SQL 'where' clause used to restrict which 
    /// database records are accessible via this entity. 
    /// </summary>
    /// <remarks>
    /// The default property value is defined in the Current Class 
    /// Properties tab in the <b>IdeaBlade DevForce Object Mapping Tool</b>.
    /// <para>
    /// The 'where' clause will be used, in addition to any other 
    /// query-specific criteria, in every retrieval of entities of this type.
    /// </para>
    /// </remarks>
    [BindingBrowsable(false)]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public virtual string SqlWhereClause {
      get { return ""; }
    }
    
    /// <summary>
    /// Determines whether to use the 'distinct' keyword when 
    /// generating the SQL select clause for objects of this type.
    /// </summary>
    /// <remarks>Defaults to false, but may be overridden in subclasses.</remarks>
    [BindingBrowsable(false)]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public virtual bool ForceSqlDistinct {
      get { return false; }
    }
    

    #region Enums
    //**************************************
    //* Enums
    //**************************************
    #endregion
	
    #region Relation properties
    //**************************************
    //* Relation properties
    //**************************************
	  /// <summary>Gets the Rateses relation property.</summary>
    [RelationProperty("RateType_Rates", 
    QueryDirection.ChildQuery)]
    public virtual ReadOnlyEntityList<Rates> Rateses {
      get { 
        ReadOnlyEntityList<Rates> result_;
        if (GetInterceptor<ReadOnlyEntityList<Rates>>("Rateses", GetRatesesImpl, out result_)) return result_;
        return GetRatesesImpl();
      }
    }
    private ReadOnlyEntityList<Rates> GetRatesesImpl() {
      return this.GetManagedChildren<Rates>(EntityRelations.RateType_Rates);
    } 
    
    #endregion
    
    #region EntityColumn definitions
    //**************************************
    //* Static EntityColumn definitions
    //**************************************
    /// <summary>The RateTypeID <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn RateTypeIDEntityColumn =
      new EntityColumn(typeof(RateType), "RateTypeID", typeof(System.Int64), false, true, true, DataSourceAccessType.ReadWrite);      
    /// <summary>The RateType <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn RateTypeEntityColumn =
      new EntityColumn(typeof(RateType), "RateType", typeof(System.String), false, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The AffiliateID <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn AffiliateIDEntityColumn =
      new EntityColumn(typeof(RateType), "AffiliateID", typeof(System.Int64), false, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The RowVersion <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn RowVersionEntityColumn =
      new EntityColumn(typeof(RateType), "RowVersion", typeof(System.Int32), false, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The ModifiedBy <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn ModifiedByEntityColumn =
      new EntityColumn(typeof(RateType), "ModifiedBy", typeof(System.String), true, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The ModifiedDate <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn ModifiedDateEntityColumn =
      new EntityColumn(typeof(RateType), "ModifiedDate", typeof(System.DateTime), true, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The CreatedBy <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn CreatedByEntityColumn =
      new EntityColumn(typeof(RateType), "CreatedBy", typeof(System.String), true, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The CreatedDate <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn CreatedDateEntityColumn =
      new EntityColumn(typeof(RateType), "CreatedDate", typeof(System.DateTime), true, false, false, DataSourceAccessType.ReadWrite);      
    #endregion
    
    #region Properties
    //**************************************
    //* RateTypeID methods
    //**************************************
    /// <summary>Gets the RateTypeID <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn RateTypeIDColumn {
      get { return TypedTable.RateTypeIDColumn; }
    }

    /// <summary>Gets the RateTypeID.</summary>
    [DBDataType(typeof(System.Int64))]
    public virtual System.Int64 RateTypeID {
      get { 
        System.Int64 result_;
        if (GetInterceptor<System.Int64>("RateTypeID", GetRateTypeIDImpl, out result_)) return result_;
        return GetRateTypeIDImpl();
      }
    }
    private System.Int64 GetRateTypeIDImpl() {
      return (System.Int64) GetColumnValue(RateTypeIDColumn, typeof(System.Int64), false); 
    }
    
    //**************************************
    //* RateType methods
    //**************************************
    /// <summary>Gets the RateType <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn RateTypeColumn {
      get { return TypedTable.RateTypeColumn; }
    }

    /// <summary>Gets the RateType.</summary>
    [MaxTextLength(50)]
    [DBDataType(typeof(System.String))]
    public virtual System.String RateType {
      get { 
        System.String result_;
        if (GetInterceptor<System.String>("RateType", GetRateTypeImpl, out result_)) return result_;
        return GetRateTypeImpl();
      }
    }
    private System.String GetRateTypeImpl() {
      return (System.String) GetColumnValue(RateTypeColumn, typeof(System.String), false); 
    }
    
    //**************************************
    //* AffiliateID methods
    //**************************************
    /// <summary>Gets the AffiliateID <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn AffiliateIDColumn {
      get { return TypedTable.AffiliateIDColumn; }
    }

    /// <summary>Gets or sets the AffiliateID.</summary>
    [DBDataType(typeof(System.Int64))]
    public virtual System.Int64 AffiliateID {
      get { 
        System.Int64 result_;
        if (GetInterceptor<System.Int64>("AffiliateID", GetAffiliateIDImpl, out result_)) return result_;
        return GetAffiliateIDImpl();
      }
      set { 
          if (!SetInterceptor<System.Int64>("AffiliateID", value, SetAffiliateIDImpl)) {
            SetAffiliateIDImpl(value);
          }  
      }    
    }
    private System.Int64 GetAffiliateIDImpl() {
      return (System.Int64) GetColumnValue(AffiliateIDColumn, typeof(System.Int64), false); 
    }
    private void SetAffiliateIDImpl(System.Int64 value) {
      SetColumnValue(AffiliateIDColumn, value);
    }
    
    //**************************************
    //* RowVersion methods
    //**************************************
    /// <summary>Gets the RowVersion <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn RowVersionColumn {
      get { return TypedTable.RowVersionColumn; }
    }

    /// <summary>Gets or sets the RowVersion.</summary>
    [DBDataType(typeof(System.Int32))]
    public virtual System.Int32 RowVersion {
      get { 
        System.Int32 result_;
        if (GetInterceptor<System.Int32>("RowVersion", GetRowVersionImpl, out result_)) return result_;
        return GetRowVersionImpl();
      }
      set { 
          if (!SetInterceptor<System.Int32>("RowVersion", value, SetRowVersionImpl)) {
            SetRowVersionImpl(value);
          }  
      }    
    }
    private System.Int32 GetRowVersionImpl() {
      return (System.Int32) GetColumnValue(RowVersionColumn, typeof(System.Int32), false); 
    }
    private void SetRowVersionImpl(System.Int32 value) {
      SetColumnValue(RowVersionColumn, value);
    }
    
    //**************************************
    //* ModifiedBy methods
    //**************************************
    /// <summary>Gets the ModifiedBy <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn ModifiedByColumn {
      get { return TypedTable.ModifiedByColumn; }
    }

    /// <summary>Gets or sets the ModifiedBy.</summary>
    [MaxTextLength(250)]
    [DBDataType(typeof(System.String))]
    public virtual System.String ModifiedBy {
      get { 
        System.String result_;
        if (GetInterceptor<System.String>("ModifiedBy", GetModifiedByImpl, out result_)) return result_;
        return GetModifiedByImpl();
      }
      set { 
          if (!SetInterceptor<System.String>("ModifiedBy", value, SetModifiedByImpl)) {
            SetModifiedByImpl(value);
          }  
      }    
    }
    private System.String GetModifiedByImpl() {
      return (System.String) GetColumnValue(ModifiedByColumn, typeof(System.String), true); 
    }
    private void SetModifiedByImpl(System.String value) {
      SetColumnValue(ModifiedByColumn, value);
    }
    
    //**************************************
    //* ModifiedDate methods
    //**************************************
    /// <summary>Gets the ModifiedDate <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn ModifiedDateColumn {
      get { return TypedTable.ModifiedDateColumn; }
    }

    /// <summary>Gets or sets the ModifiedDate.</summary>
    [DBDataType(typeof(System.DateTime))]
    public virtual Nullable<System.DateTime> ModifiedDate {
      get { 
        Nullable<System.DateTime> result_;
        if (GetInterceptor<Nullable<System.DateTime>>("ModifiedDate", GetModifiedDateImpl, out result_)) return result_;
        return GetModifiedDateImpl();
      }
      set { 
          if (!SetInterceptor<Nullable<System.DateTime>>("ModifiedDate", value, SetModifiedDateImpl)) {
            SetModifiedDateImpl(value);
          }
      }    
    }
    private Nullable<System.DateTime> GetModifiedDateImpl() {
      return (Nullable<System.DateTime>) GetColumnValue(ModifiedDateColumn, typeof(System.DateTime), true); 
    }
    private void SetModifiedDateImpl(Nullable<System.DateTime> value) {
      SetColumnValue(ModifiedDateColumn, value);
    }
    
    //**************************************
    //* CreatedBy methods
    //**************************************
    /// <summary>Gets the CreatedBy <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn CreatedByColumn {
      get { return TypedTable.CreatedByColumn; }
    }

    /// <summary>Gets or sets the CreatedBy.</summary>
    [MaxTextLength(250)]
    [DBDataType(typeof(System.String))]
    public virtual System.String CreatedBy {
      get { 
        System.String result_;
        if (GetInterceptor<System.String>("CreatedBy", GetCreatedByImpl, out result_)) return result_;
        return GetCreatedByImpl();
      }
      set { 
          if (!SetInterceptor<System.String>("CreatedBy", value, SetCreatedByImpl)) {
            SetCreatedByImpl(value);
          }  
      }    
    }
    private System.String GetCreatedByImpl() {
      return (System.String) GetColumnValue(CreatedByColumn, typeof(System.String), true); 
    }
    private void SetCreatedByImpl(System.String value) {
      SetColumnValue(CreatedByColumn, value);
    }
    
    //**************************************
    //* CreatedDate methods
    //**************************************
    /// <summary>Gets the CreatedDate <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn CreatedDateColumn {
      get { return TypedTable.CreatedDateColumn; }
    }

    /// <summary>Gets or sets the CreatedDate.</summary>
    [DBDataType(typeof(System.DateTime))]
    public virtual Nullable<System.DateTime> CreatedDate {
      get { 
        Nullable<System.DateTime> result_;
        if (GetInterceptor<Nullable<System.DateTime>>("CreatedDate", GetCreatedDateImpl, out result_)) return result_;
        return GetCreatedDateImpl();
      }
      set { 
          if (!SetInterceptor<Nullable<System.DateTime>>("CreatedDate", value, SetCreatedDateImpl)) {
            SetCreatedDateImpl(value);
          }
      }    
    }
    private Nullable<System.DateTime> GetCreatedDateImpl() {
      return (Nullable<System.DateTime>) GetColumnValue(CreatedDateColumn, typeof(System.DateTime), true); 
    }
    private void SetCreatedDateImpl(Nullable<System.DateTime> value) {
      SetColumnValue(CreatedDateColumn, value);
    }
    
    #endregion

  }
  #endregion

  #region EntityPropertyDescriptors.RateTypePropertyDescriptor
  public static partial class EntityPropertyDescriptors {

    /// <summary>
    /// Gets the RateTypePropertyDescriptor for <see cref="RateType"/>.
    /// </summary>
    public static RateTypePropertyDescriptor RateType {
      get { return RateTypePropertyDescriptor.Instance; }
    }
    
    /// <summary>
    /// The generated class containing <see cref="RateType"/> PropertyDescriptors.
    /// </summary>
    public partial class RateTypePropertyDescriptor : BaseEntityPropertyDescriptor {
	
			/// <summary>
      /// Initializes a new instance of the RateTypePropertyDescriptor class. 
      /// </summary>
      /// <param name="pParentDescriptor">The parent <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/></param>
      /// <param name="pPropertyPath">The type of the property value</param>
			public RateTypePropertyDescriptor(AdaptedPropertyDescriptor pParentDescriptor, String pPropertyPath)
        : base(pParentDescriptor, pPropertyPath) {}
		
      /// <summary>
      /// Initializes a new instance of the RateTypePropertyDescriptor class. 
      /// </summary>
      /// <param name="pComponentType">The type of object to which the property is bound</param>
      protected RateTypePropertyDescriptor(Type pComponentType)
        : base(pComponentType) {}

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for RateTypeID.</summary>
      public AdaptedPropertyDescriptor RateTypeID {
        get { return Get("RateTypeID"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for RateType.</summary>
      public AdaptedPropertyDescriptor RateType {
        get { return Get("RateType"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for AffiliateID.</summary>
      public AdaptedPropertyDescriptor AffiliateID {
        get { return Get("AffiliateID"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for RowVersion.</summary>
      public AdaptedPropertyDescriptor RowVersion {
        get { return Get("RowVersion"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for ModifiedBy.</summary>
      public AdaptedPropertyDescriptor ModifiedBy {
        get { return Get("ModifiedBy"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for ModifiedDate.</summary>
      public AdaptedPropertyDescriptor ModifiedDate {
        get { return Get("ModifiedDate"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for CreatedBy.</summary>
      public AdaptedPropertyDescriptor CreatedBy {
        get { return Get("CreatedBy"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for CreatedDate.</summary>
      public AdaptedPropertyDescriptor CreatedDate {
        get { return Get("CreatedDate"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Rateses.</summary>
      public AdaptedPropertyDescriptor Rateses {
        get { return Get("Rateses"); }
      }
						
     internal new static RateTypePropertyDescriptor Instance = new RateTypePropertyDescriptor(typeof(RateType));
    }
  }
  #endregion
  
}
