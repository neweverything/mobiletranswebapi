﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using TaxiPassCommon.Banking.Models;

namespace TaxiPassCommon.Banking {
	public class CardConnect : IProcessor {

		private CardConnectCredentials mCredentials;

		public CardConnect(CardConnectCredentials pCredentials) {
			ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls;

			if (pCredentials.Timeout == 0) {
				pCredentials.Timeout = 30;
			}
			mCredentials = pCredentials;
		}

		public ProcessorResponse ChargeAuth(CCRequestRefNo pRequest) {
			CardConnectRequest request = new CardConnectRequest() {
				Amount = pRequest.Amount,
				MerchID = mCredentials.MerchantID,
				SalesPers = pRequest.DriverID,
				TransNo = pRequest.VoucherNo,
				ReferenceNo = pRequest.RefNum,
				Capture = "Y"
			};

			if (request.SalesPers.IsNullOrEmpty()) {
				request.SalesPers = string.Format("D: {0}", pRequest.DriverID);
			}

			request.TransNo = string.Format("{0} | {1} | {2} | {3}", request.TransNo, string.Format("D: {0}", pRequest.DriverID), TrxTypes.DelayedCapture, DateTime.Now.ToEpochTime());

			if (request.SalesPers.IsNullOrEmpty()) {
				request.SalesPers = string.Format("D: {0}", pRequest.DriverID);
			}

			// Create the REST client
			string url = mCredentials.ProdURL;
			string user = mCredentials.UserName;
			string pwd = mCredentials.Password;
			if (pRequest.TestCard) {
				url = mCredentials.TestURL;
				user = mCredentials.TestUserName;
				pwd = mCredentials.TestPassword;
				request.MerchID = mCredentials.TestMerchantID;
			}
			CardConnectRestClient client = new CardConnectRestClient(url, user, pwd);
			// Send an AuthTransaction request
			JObject response = client.captureTransaction(request.AsJObject());
			string js = response.Root.ToString();
			Console.WriteLine(js);
			return ProcessResponse(response, pRequest.Amount, pRequest.TestCard, TrxTypes.DelayedCapture, "");
		}

		public ProcessorResponse ChargeCard(CCRequest pRequest) {
			CardConnectRequest request = new CardConnectRequest() {
				Amount = pRequest.Amount,
				CardNo = pRequest.CardData.CardNo,
				CardType = pRequest.CardData.CardType,
				CVV = pRequest.CardData.CVV,
				Expiry = pRequest.CardData.CardExpiryAsMMYY,
				MerchID = mCredentials.MerchantID,
				SalesPers = pRequest.DriverID,
				SwipeData = pRequest.CardData.Track2,
				TransNo = pRequest.VoucherNo,
				ZipCode = pRequest.CardData.AvsZip,
				Street = pRequest.CardData.AvsStreet,
				Capture = "Y"
			};

			if (!pRequest.CardData.SalesPerson.IsNullOrEmpty()) {
				request.TransNo = string.Format("{0} | {1} | {2} | {3}", request.TransNo, pRequest.CardData.SalesPerson, TrxTypes.Sale, DateTime.Now.ToEpochTime());
			} else {
				request.TransNo = string.Format("{0} | {1} | {2} | {3}", request.TransNo, string.Format("D: {0}", pRequest.DriverID), TrxTypes.Sale, DateTime.Now.ToEpochTime());
			}

			if (request.SalesPers.IsNullOrEmpty()) {
				request.SalesPers = string.Format("D: {0}", pRequest.DriverID);
			}

			// Create the REST client
			string url = mCredentials.ProdURL;
			string user = mCredentials.UserName;
			string pwd = mCredentials.Password;
			if (pRequest.CardData.TestCard) {
				url = mCredentials.TestURL;
				user = mCredentials.TestUserName;
				pwd = mCredentials.TestPassword;
				request.MerchID = mCredentials.TestMerchantID;
			}
			CardConnectRestClient client = new CardConnectRestClient(url, user, pwd);
			// Send an AuthTransaction request
			JObject response = client.authorizeTransaction(request.AsJObject());
			return ProcessResponse(response, pRequest.Amount, pRequest.CardData.TestCard, TrxTypes.Sale, "");
		}

		public string GetTransactionHistory(DateTime pStart, DateTime pEnd) {
			string url = mCredentials.ProdURL;
			string user = mCredentials.UserName;
			string pwd = mCredentials.Password;
			string merID = mCredentials.MerchantID;

			bool test = false;
			if (test) {
				url = mCredentials.TestURL;
				user = mCredentials.TestUserName;
				pwd = mCredentials.TestPassword;
				merID = mCredentials.TestMerchantID;
			}
			List<CardConnectInquire> txns = new List<CardConnectInquire>();

			CardConnectRestClient client = new CardConnectRestClient(url, user, pwd);
			while (pStart < pEnd) {
				Console.WriteLine("Processing Date: {0}", pStart);
				JArray responses = client.settlementStatus(merID, pStart.ToString("MMdd"));

				if (responses != null) {
					foreach (JObject response in responses) {
						string data = response.ToString();
						CardConnectInquire inq = JsonConvert.DeserializeObject<CardConnectInquire>(data);
						inq.SettlementDate = pStart;

						var temp = inq.txns.Where(p => p.salesdoc.ToLower().Contains("han")).ToList();
						Console.WriteLine(temp.Count);


						foreach (var trans in inq.txns) {
							trans.Response = InquireTransaction(trans.retref, test);
							//if (trans.Response..Contains("HAN")) {
							//    Console.WriteLine("stop");
							//}
						}
						txns.Add(inq);
					}
				}
				pStart = pStart.AddDays(1);
			}

			return JsonConvert.SerializeObject(txns);

		}


		public List<CardConnectInquire> GetTransactionHistoryByDateRange(DateTime pStart, DateTime pEnd) {
			string url = mCredentials.ProdURL;
			string user = mCredentials.UserName;
			string pwd = mCredentials.Password;
			string merID = mCredentials.MerchantID;

			bool test = false;
			if (test) {
				url = mCredentials.TestURL;
				user = mCredentials.TestUserName;
				pwd = mCredentials.TestPassword;
				merID = mCredentials.TestMerchantID;
			}

			List<CardConnectInquire> txns = new List<CardConnectInquire>();

			CardConnectRestClient client = new CardConnectRestClient(url, user, pwd);
			while (pStart < pEnd) {
				Console.WriteLine("Processing Date: {0}", pStart);
				JArray responses = client.settlementStatus(merID, pStart.ToString("MMdd"));
				if (responses != null) {
					foreach (JObject response in responses) {
						CardConnectInquire inq = JsonConvert.DeserializeObject<CardConnectInquire>(response.ToString());
						inq.SettlementDate = pStart;
						foreach (var trans in inq.txns) {
							trans.Response = InquireTransaction(trans.retref, test);
						}
						txns.Add(inq);
					}
				}
				pStart = pStart.AddDays(1);
			}

			return txns;

		}

		public CardConnectResponse InquireTransaction(String retref, bool pTest) {
			string url = mCredentials.ProdURL;
			string user = mCredentials.UserName;
			string pwd = mCredentials.Password;
			string merchID = mCredentials.MerchantID;

			if (pTest) {
				url = mCredentials.TestURL;
				user = mCredentials.TestUserName;
				pwd = mCredentials.TestPassword;
				merchID = mCredentials.TestMerchantID;
			}

			CardConnectRestClient client = new CardConnectRestClient(url, user, pwd);

			// Send an inquire Transaction request
			JObject response = client.inquireTransaction(merchID, retref);

			return JsonConvert.DeserializeObject<CardConnectResponse>(response.Root.ToString());
		}

		public ProcessorResponse OpenCredit(CCRequest pRequest) {
			CardConnectRequest request = new CardConnectRequest() {
				Amount = -Math.Abs(pRequest.Amount),
				CardNo = pRequest.CardData.CardNo,
				CardType = pRequest.CardData.CardType,
				CVV = pRequest.CardData.CVV,
				Expiry = pRequest.CardData.CardExpiryAsMMYY,
				MerchID = mCredentials.MerchantID,
				SalesPers = pRequest.DriverID,
				SwipeData = pRequest.CardData.Track2,
				TransNo = pRequest.VoucherNo,
				ZipCode = pRequest.CardData.AvsZip,
				Street = pRequest.CardData.AvsStreet,
				Capture = "Y"
			};

			if (!pRequest.CardData.SalesPerson.IsNullOrEmpty()) {
				request.TransNo = string.Format("{0} | {1} | {2} | {3}", request.TransNo, pRequest.CardData.SalesPerson, TrxTypes.OpenCredit, DateTime.Now.ToEpochTime());
			} else {
				request.TransNo = string.Format("{0} | {1} | {2} | {3}", request.TransNo, string.Format("D: {0}", pRequest.DriverID), TrxTypes.OpenCredit, DateTime.Now.ToEpochTime());
			}

			if (request.SalesPers.IsNullOrEmpty()) {
				request.SalesPers = string.Format("D: {0}", pRequest.DriverID);
			}

			// Create the REST client
			string url = mCredentials.ProdURL;
			string user = mCredentials.UserName;
			string pwd = mCredentials.Password;
			if (pRequest.CardData.TestCard) {
				url = mCredentials.TestURL;
				user = mCredentials.TestUserName;
				pwd = mCredentials.TestPassword;
				request.MerchID = mCredentials.TestMerchantID;
			}
			CardConnectRestClient client = new CardConnectRestClient(url, user, pwd);
			// Send an AuthTransaction request
			JObject response = client.authorizeTransaction(request.AsJObject());
			return ProcessResponse(response, pRequest.Amount, pRequest.CardData.TestCard, TrxTypes.Sale, "");
		}

		public ProcessorResponse PreAuthCard(CCRequest pRequest) {
			CardConnectRequest request = new CardConnectRequest() {
				Amount = pRequest.Amount,
				CardNo = pRequest.CardData.CardNo,
				CardType = pRequest.CardData.CardType,
				CVV = pRequest.CardData.CVV,
				Expiry = pRequest.CardData.CardExpiryAsMMYY,
				MerchID = mCredentials.MerchantID,
				SalesPers = pRequest.DriverID,
				SwipeData = pRequest.CardData.Track2,
				TransNo = pRequest.VoucherNo,
				ZipCode = pRequest.CardData.AvsZip,
				Street = pRequest.CardData.AvsStreet
			};

			if (!pRequest.CardData.SalesPerson.IsNullOrEmpty()) {
				request.TransNo = string.Format("{0} | {1} | {2} | {3}", request.TransNo, pRequest.CardData.SalesPerson, TrxTypes.Authorization, DateTime.Now.ToEpochTime());
			} else {
				request.TransNo = string.Format("{0} | {1} | {2} | {3}", request.TransNo, string.Format("D: {0}", pRequest.DriverID), TrxTypes.Authorization, DateTime.Now.ToEpochTime());
			}

			if (request.SalesPers.IsNullOrEmpty()) {
				request.SalesPers = string.Format("D: {0}", pRequest.DriverID);
			}

			// Create the REST client
			string url = mCredentials.ProdURL;
			string user = mCredentials.UserName;
			string pwd = mCredentials.Password;
			if (pRequest.CardData.TestCard) {
				url = mCredentials.TestURL;
				user = mCredentials.TestUserName;
				pwd = mCredentials.TestPassword;
				request.MerchID = mCredentials.TestMerchantID;
			}
			CardConnectRestClient client = new CardConnectRestClient(url, user, pwd);
			// Send an AuthTransaction request
			JObject response = client.authorizeTransaction(request.AsJObject());
			string js = response.Root.ToString();
			Console.WriteLine(js);
			return ProcessResponse(response, pRequest.Amount, pRequest.CardData.TestCard, TrxTypes.Authorization, "");
		}

		public ProcessorResponse Refund(CCRequestRefNo pRequest) {
			CardConnectRequest request = new CardConnectRequest() {
				Amount = Math.Abs(pRequest.Amount),
				MerchID = mCredentials.MerchantID,
				ReferenceNo = pRequest.RefNum,
				Capture = "Y"
			};

			request.TransNo = string.Format("{0} | {1} | {2} | {3}", request.TransNo, string.Format("D: {0}", pRequest.DriverID), TrxTypes.Credit, DateTime.Now.ToEpochTime());

			if (request.SalesPers.IsNullOrEmpty()) {
				request.SalesPers = string.Format("D: {0}", pRequest.DriverID);
			}

			// Create the REST client
			string url = mCredentials.ProdURL;
			string user = mCredentials.UserName;
			string pwd = mCredentials.Password;
			if (pRequest.TestCard) {
				url = mCredentials.TestURL;
				user = mCredentials.TestUserName;
				pwd = mCredentials.TestPassword;
				request.MerchID = mCredentials.TestMerchantID;
			}
			CardConnectRestClient client = new CardConnectRestClient(url, user, pwd);
			// Send an AuthTransaction request
			JObject response = client.refundTransaction(request.AsJObject());
			string js = response.Root.ToString();
			Console.WriteLine(js);
			return ProcessResponse(response, pRequest.Amount, pRequest.TestCard, TrxTypes.Credit, "");

		}

		public bool Void(CCRequestRefNo pRequest) {
			CardConnectRequest request = new CardConnectRequest() {
				Amount = pRequest.Amount,
				MerchID = mCredentials.MerchantID,
				ReferenceNo = pRequest.RefNum,
				Capture = "Y"
			};

			request.TransNo = string.Format("{0} | {1} | {2} | {3}", request.TransNo, string.Format("D: {0}", pRequest.DriverID), TrxTypes.Void, DateTime.Now.ToEpochTime());

			// Create the REST client
			string url = mCredentials.ProdURL;
			string user = mCredentials.UserName;
			string pwd = mCredentials.Password;
			if (pRequest.TestCard) {
				url = mCredentials.TestURL;
				user = mCredentials.TestUserName;
				pwd = mCredentials.TestPassword;
				request.MerchID = mCredentials.TestMerchantID;
			}
			CardConnectRestClient client = new CardConnectRestClient(url, user, pwd);
			// Send an AuthTransaction request
			JObject response = client.voidTransaction(request.AsJObject());
			string js = response.Root.ToString();
			Console.WriteLine(js);
			var myResp = ProcessResponse(response, pRequest.Amount, pRequest.TestCard, TrxTypes.Credit, "");
			return myResp.Result.Equals("0");
		}

		private ProcessorResponse ProcessResponse(JObject pResponse, decimal pAmount, bool pTestCard, string pTransType, string pSoftDescriptor) {
			ProcessorResponse oResult = new ProcessorResponse();

			CardConnectResponse response = JsonConvert.DeserializeObject<CardConnectResponse>(pResponse.Root.ToString());
			//oResult.GatewayResponse = JSON.JsonHelper.JsonSerialize(pResponse);

			oResult.TestCard = pTestCard;
			oResult.VerisignAccountID = mCredentials.VerisignAccountID;
			oResult.MerchantID = response.merchid;
			oResult.CreditCardProcessor = mCredentials.MyGateway();

			oResult.Result = Convert.ToInt32(response.respcode).ToString();
			oResult.Reference = response.retref;
			if (response.resptext.Equals("No reason to decline", StringComparison.InvariantCulture)) {
				response.resptext = "Approved";
			}
			if (!response.resptext.IsNullOrEmpty()) {
				oResult.Message = response.resptext.ToLower().Contains("approv") ? "Approved" : response.resptext;
			} else {
				oResult.Message = "Unknown error";
				if (oResult.Result.IsNullOrEmpty() || oResult.Result.Equals("0")) {
					oResult.Result = "99";
				}
			}
			oResult.Amount = pAmount;
			if (oResult.Result.IsNullOrEmpty()) {
				oResult.Result = "0";
			}
			//fixed for Chase results
			if (oResult.Result != "0" && oResult.Message.StartsWith("Approve", StringComparison.CurrentCultureIgnoreCase)) {
				oResult.Result = "0";
			}
			if (oResult.Result == "346") {
				oResult.Message = string.Format("{0} ({1})", oResult.Message, pSoftDescriptor);
			}

			oResult.AuthCode = response.authcode;

			response.avsresp = ("" + response.avsresp).ToUpper();
			oResult.AVSResultCode = ConvertToUSAePayAVSResultCode(response.avsresp);
			oResult.AVSZip = response.avsresp;
			oResult.AVS = (" " + oResult.AVSResultCode).Right(1).Trim();
			oResult.CVV2Match = response.cvvresp;


			oResult.TransType = pTransType;
			oResult.TransDate = DateTime.Now;

			if (oResult.Result == "0" && oResult.AuthCode.IsNullOrEmpty()) {
				string num = new Random((int)DateTime.UtcNow.ToEpochTime()).Next(1000000).ToString();
				oResult.AuthCode = "T" + num.PadLeft(5, '0').Right(5);
			}

			oResult.BillingDescriptor = pSoftDescriptor;
			return oResult;
		}

		private string ConvertToUSAePayAVSResultCode(string pAVSResult) {
			if (pAVSResult.IsNullOrEmpty()) {
				return pAVSResult;
			}

			string result = pAVSResult;

			switch (result) {
				case "0":
					result = "NA ";
					break;

				case "A": // Zip does not match, address matches
					result = "YNA";
					break;

				case "B": // Street Address match for international transaction; Postal Code not verified
					result = "YYG";
					break;

				case "C": // International address not verified because of incompatible format
					result = "NNC";
					break;

				case "D": // Street Address match for international transaction
					result = "YYG"; // maybe
					break;

				case "E": // Not a MOTO order
					result = "XXS"; // maybe
					break;

				case "G": // Non-US issuer does not participate
					result = "XXG"; // maybe
					break;

				case "I": // Address not verified for international transaction
					result = "NNC";
					break;

				case "M": // Street address match for international transaction
					result = "YYG"; // maybe
					break;

				case "N": // No address or ZIP match
					result = "NNN"; // maybe
					break;

				case "P": // Postal Code match for international transaction
					result = "YGG";
					break;

				case "R": // Retry – Issuer unavailable
					result = "XXR";
					break;

				case "S": // Service not supported
					result = "XXS";
					break;

				case "U": // Address verification unavailable
					result = "XXU"; // maybe
					break;

				case "W": // 9 - Digit zip matches, address does not match
					result = "NYW";
					break;

				case "X": // 9 - Digit zip and address match
					result = "YYX";
					break;

				case "Y": // 5-Digit zip and address match
					result = "YYY";
					break;

				case "Z": // 5 - Digit zip matches, address does not match
					result = "NYZ";
					break;
			}
			return result;
		}

		public ProcessorResponse AvsCheck(CCRequest pRequest) {
			CardConnectRequest request = new CardConnectRequest() {
				Amount = 0,
				CardNo = pRequest.CardData.CardNo,
				CardType = pRequest.CardData.CardType,
				CVV = pRequest.CardData.CVV,
				Expiry = pRequest.CardData.CardExpiryAsMMYY,
				MerchID = mCredentials.MerchantID,
				SalesPers = pRequest.DriverID,
				SwipeData = pRequest.CardData.Track2,
				TransNo = pRequest.VoucherNo,
				ZipCode = pRequest.CardData.AvsZip,
				Street = pRequest.CardData.AvsStreet
			};

			if (request.SalesPers.IsNullOrEmpty()) {
				request.SalesPers = string.Format("D: {0}", pRequest.DriverID);
			}

			// Create the REST client
			string url = mCredentials.ProdURL;
			string user = mCredentials.UserName;
			string pwd = mCredentials.Password;
			if (pRequest.CardData.TestCard) {
				url = mCredentials.TestURL;
				user = mCredentials.TestUserName;
				pwd = mCredentials.TestPassword;
				request.MerchID = mCredentials.TestMerchantID;
			}
			CardConnectRestClient client = new CardConnectRestClient(url, user, pwd);
			// Send an AuthTransaction request

			JObject response = client.authorizeTransaction(request.AsJObject());
			string js = response.Root.ToString();
			Console.WriteLine(js);
			return ProcessResponse(response, pRequest.Amount, pRequest.CardData.TestCard, TrxTypes.Authorization, "");
		}

		public ProcessorResponse ProcessCheck(CheckRequest pRequest) {
			throw new NotImplementedException();
		}

		public ProcessorResponse RefundCheck(CheckRequest pRequest) {
			throw new NotImplementedException();
		}

		public List<CheckStatus> GetCheckTransactionHistory(DateTime pStart, DateTime pEnd) {
			throw new NotImplementedException();
		}
	}

}
