﻿using System;

namespace MobileTransWebAPI.Models {
    public class RefundResult {
        public string CardNo { get; set; }
        public long DriverPaymentID { get; set; }
        public decimal DriverTotal { get; set; }
        public string ErrorMsg { get; set; }
        public bool IsTestCard { get; set; }
        public string NewVoucherNo { get; set; }
        public DateTime RefundDate { get; set; }
        public string SoftDescriptor { get; set; }
        public decimal TPFee { get; set; }
        public string TransType { get; set; }
        public string VoucherNo { get; set; }

    }
}