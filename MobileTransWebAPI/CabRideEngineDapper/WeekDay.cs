﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CabRideEngineDapper {
	public class WeekDay {
		public int Day { get; set; }
		public string Name { get; set; }

		public static List<WeekDay> GetForDropDown() {
			List<WeekDay> dayList = new List<WeekDay>();
			dayList.Add(new WeekDay() {
				Day = 0,
				Name = "Do not Batch"
			});

			for (int i = 0; i < 7; i++) {
				dayList.Add(new WeekDay() {
					Day = i + 1,
					Name = ((DayOfWeek)i).ToString()
				});
			}

			return dayList;
		}

	}
}
