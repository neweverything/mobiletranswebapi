﻿using CabRideEngineDapper.Utils;
using Dapper;

namespace CabRideEngineDapper {
    public partial class TransCardDefaults {

        public static TransCardDefaults GetDefaults(long pID) {
            using (var conn = SqlHelper.OpenSqlConnection()) {
                return conn.Get<TransCardDefaults>(pID);
            }
        }
    }
}
