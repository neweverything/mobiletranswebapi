﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using TaxiPassCommon;

namespace CabRideEngineDapper {
    public partial class Repairs {

        #region Related Tables

        private AffiliateDrivers mAffiliateDrivers;
        public AffiliateDrivers AffiliateDrivers {
            get {
                if (mAffiliateDrivers == null) {
                    mAffiliateDrivers = CabRideEngineDapper.AffiliateDrivers.GetDriver(this.AffiliateDriverID);
                }
                return mAffiliateDrivers;
            }
        }

        private Drivers mDrivers;
        public Drivers Drivers {
            get {
                if (mDrivers == null) {
                    mDrivers = CabRideEngineDapper.Drivers.GetDriver(this.DriverID);
                }
                return mDrivers;
            }
        }

        #endregion

        public static Repairs Create(AffiliateDrivers pAffiliateDrivers) {
            Repairs oRec = null;
            try {
                oRec = RepairsTable.Create();

                //oPM.GenerateId(oRec, Repairs.RepairIDEntityColumn);
                oRec.AffiliateDriverID = pAffiliateDrivers.AffiliateDriverID;
                TimeZoneConverter convert = new TimeZoneConverter();
                oRec.RepairRequestDate = convert.ConvertTime(DateTime.Now, TimeZone.CurrentTimeZone.StandardName, pAffiliateDrivers.Affiliate.TimeZone);

            } catch (Exception ex) {
                throw ex;
            }
            return oRec;
        }


        public override int Save() {
            if (IsNewRecord) {
                this.RepairID = NextID.GetNextKey(Repairs.TableDef.TABLE_NAME);
            }
            return base.Save();
        }
        public override int Save(SqlConnection pConn, string pModifiedBy) {
            if (IsNewRecord) {
                this.RepairID = NextID.GetNextKey(Repairs.TableDef.TABLE_NAME);
            }

            return base.Save(pConn, pModifiedBy);
        }



        [Editable(false)]
        public string AffiliateDriverName {
            get {
                return this.AffiliateDrivers.Name;
            }
        }

        [Editable(false)]
        public string AffiliateDriverCellFormattedPhone {
            get {
                return AffiliateDriverCell.FormattedPhoneNumber();
            }
        }

        [Editable(false)]
        public string AffiliateDriverCell {
            get {
                if (this.AffiliateDrivers.Cell.Trim().Length == 10) {
                    return this.AffiliateDrivers.Cell;
                }
                return this.AffiliateDrivers.CallInCell;
            }
        }

        [Editable(false)]
        public string AffiliateName {
            get {
                return this.AffiliateDrivers.Affiliate.Name;
            }
        }

        [Editable(false)]
        public string DriverName {
            get {
                return this.Drivers.Name;
            }
        }

        [Editable(false)]
        public DateTime LocalRepairRequestDate {
            get {
                if (this.AffiliateDrivers.Affiliate.TimeZone == null) {
                    return this.RepairRequestDate;
                }
                TimeZoneConverter oTimeZone = new TimeZoneConverter();
                return oTimeZone.FromUniversalTime(this.AffiliateDrivers.Affiliate.TimeZone, RepairRequestDate);
            }
        }

        [Editable(false)]
        public DateTime? LocalRepairClosedDate {
            get {
                if (!this.ClosedDate.HasValue) {
                    return ClosedDate;
                }
                if (this.AffiliateDrivers.Affiliate.TimeZone == null) {
                    return this.ClosedDate;
                }
                TimeZoneConverter oTimeZone = new TimeZoneConverter();
                return oTimeZone.FromUniversalTime(this.AffiliateDrivers.Affiliate.TimeZone, ClosedDate.Value);
            }
        }

        public static List<Repairs> GetRepairs(SqlConnection pConn, DateTime pStart, DateTime pEnd, bool pIncludeClosed) {
            string sql = @"SELECT  *
                            FROM    Repairs
                            WHERE   RepairRequestDate >= @Start
                                    AND RepairRequestDate <= @End";
            if (!pIncludeClosed) {
                sql += " AND ClosedDateEntityColumn IS NULL";
            }

            return pConn.Query<Repairs>(sql, new { Start = pStart, End = pEnd }).ToList();
        }
    }
}
