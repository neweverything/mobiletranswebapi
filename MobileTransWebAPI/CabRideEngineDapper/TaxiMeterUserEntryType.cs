﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CabRideEngineDapper {
	public class TaxiMeterUserEntryType {
		private string mCode;
		private string mDescription;

		private bool mIsNullEntity = false;

		private static List<TaxiMeterUserEntryType> msEntities;
		private static TaxiMeterUserEntryType msNullEntity;

		public enum UserEntryTypeEnum {
			FullUsage,
			SwipeOnly,
			MiniMenu,
			PreAuthOnly,
			Voucher,
			VoucherMenu,
			VoucherMenuFare,
			VoucherMenuManual
		}

		static TaxiMeterUserEntryType() {

			// Create the null entity
			msNullEntity = new TaxiMeterUserEntryType("", "");
			msNullEntity.mIsNullEntity = true;

			// Populate list of Routing Types.
			msEntities = new List<TaxiMeterUserEntryType>();
			msEntities.Add(new TaxiMeterUserEntryType(Convert.ToInt16(UserEntryTypeEnum.FullUsage).ToString(), "Full Access"));
			msEntities.Add(new TaxiMeterUserEntryType(Convert.ToInt16(UserEntryTypeEnum.SwipeOnly).ToString(), "Swipe Only"));
			msEntities.Add(new TaxiMeterUserEntryType(Convert.ToInt16(UserEntryTypeEnum.MiniMenu).ToString(), "Mini Menu"));
			msEntities.Add(new TaxiMeterUserEntryType(Convert.ToInt16(UserEntryTypeEnum.PreAuthOnly).ToString(), "PreAuth Only"));
			msEntities.Add(new TaxiMeterUserEntryType(Convert.ToInt16(UserEntryTypeEnum.Voucher).ToString(), "Voucher Only"));
			msEntities.Add(new TaxiMeterUserEntryType(Convert.ToInt16(UserEntryTypeEnum.VoucherMenu).ToString(), "Voucher Menu"));
			msEntities.Add(new TaxiMeterUserEntryType(Convert.ToInt16(UserEntryTypeEnum.VoucherMenuFare).ToString(), "Voucher Menu with Fare"));
		}

		private TaxiMeterUserEntryType(string pCode, string pDescription) {
			mCode = pCode;
			mDescription = pDescription;
		}

		/// <summary>Get the CellPhoneDataEntry null entity.</summary>
		public static TaxiMeterUserEntryType NullEntity {
			get {
				return msNullEntity;
			}
		}

		/// <summary>Get the list of all CellPhoneDataEntry.</summary>
		public static List<TaxiMeterUserEntryType> GetAll() {
			return msEntities;
		}

		/// <summary>Get the CellPhoneDataEntry by Code.</summary>
		public static TaxiMeterUserEntryType GetById(string pCode) {
			foreach (TaxiMeterUserEntryType oRec in msEntities) {
				if (oRec.Code == pCode) {
					return oRec;
				}
			}
			return NullEntity;
		}

		/// <summary>Return true if this is the null entity</summary>
		//[BindingBrowsable(false)]
		public bool IsNullEntity {
			get {
				return mIsNullEntity;
			}
		}

		/// <summary>Get the Code</summary>
		public string Code {
			get {
				return mCode;
			}
		}

		public string Description {
			get {
				return mDescription;
			}
		}

	}
}
