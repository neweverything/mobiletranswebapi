﻿namespace TaxiPassCommon.Banking.com.USAePay {
    public partial class BatchSatus {

        private string batchNumField;

        public string BatchNum {
            get {
                return batchNumField;
            }
            set {
                batchNumField = value;
            }
        }
    }
}
