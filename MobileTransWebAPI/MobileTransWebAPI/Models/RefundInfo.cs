﻿namespace MobileTransWebAPI.Models {
    public class RefundInfo {
        public int RequestTime { get; set; }
        public string AccountNo { get; set; }
        public string AffiliateID { get; set; }
        public string Platform { get; set; }
        public string RefundReason { get; set; }
        public decimal RefundAmount { get; set; }
        public string OrigVoucherNo { get; set; }
        public bool DoNotRefundServiceFee { get; set; }

    }
}