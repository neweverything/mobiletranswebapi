﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

namespace CabRideEngine {
    [Serializable]
    public sealed class Feedback : FeedbackDataRow {

        #region Constructors -- DO NOT MODIFY
        // Do not create constructors for this class
        // Developers cannot create instances of this class with the "new" keyword
        // You should write a static Create method instead; see the "Suggested Customization" region
        // Please review the documentation to learn about entity creation and inheritance

        // This private constructor prevents accidental instance creation via 'new'
        private Feedback()
            : this(null) {
        }

        // Typed DataSet constructor (needed by framework) - DO NOT REMOVE
        public Feedback(DataRowBuilder pRowBuilder)
            : base(pRowBuilder) {
        }

        #endregion

        #region Suggested Customizations

        //    // Use this factory method template to create new instances of this class
        //    public static Feedback Create(PersistenceManager pManager,
        //      ... your creation parameters here ...) { 
        //
        //      Feedback aFeedback = pManager.CreateEntity<Feedback>();
        //
        //      // if this object type requires a unique id and you have implemented
        //      // the IIdGenerator interface implement the following line
        //      pManager.GenerateId(aFeedback, // add id column here //);
        //
        //      // Add custom code here
        //
        //      aFeedback.AddToManager();
        //      return aFeedback;
        //    }

        //    // Implement this method if you want your object to sort in a specific order
        //    public override int CompareTo(Object pObject) {
        //    }

        //    // Implement this method to customize the null object version of this class
        //    protected override void UpdateNullEntity() {
        //    }

        #endregion

        // Add additional logic to your business object here...
        public static Feedback Create(PersistenceManager pManager) {
            Feedback oFeedback = null;
            try {
                oFeedback = (Feedback)pManager.CreateEntity(typeof(Feedback));

                // Using the IdeaBlade Id Generation technique
                //pManager.GenerateId(oFeedback, Feedback.FeedBackIDEntityColumn);
                oFeedback.CityRequest = false;

                oFeedback.AddToManager();

            } catch (Exception ex) {
                throw ex;
            }
            return oFeedback;
        }

        protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
            if (IsDeserializing)
                return;
            base.OnColumnChanging(pArgs);
            //DoAudit(pArgs);
        }

        private void DoAudit(DataColumnChangeEventArgs pArgs) {
            if (!this.PersistenceManager.IsClient) {
                return;
            }
            if (this.RowState == DataRowState.Added) {
                return;
            }
            if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
                AuditRecord = FeedbackAudit.Create(this.PersistenceManager, this);
            }
            //AuditRecord[pArgs.Column.ColumnName] = pArgs.ProposedValue;
        }

        public override bool Closed {
            get {
                return base.Closed;
            }
            set {
                if (value) {
                    ResolvedBy = User.GetUser(this.PersistenceManager, LoginManager.PrincipleUser.Identity.Name).FullName;
                }
                base.Closed = value;
            }
        }

        public override void SaveSecurityCheck(System.Security.Principal.IPrincipal pPrincipal) {
            ModifiedBy = pPrincipal.Identity.Name;
            ModifiedDate = DateTime.Now;
            base.SaveSecurityCheck(pPrincipal);
        }

        public static Feedback GetFeedBack(PersistenceManager oPM, long pFeedbackID) {
            RdbQuery qry = new RdbQuery(typeof(Feedback), Feedback.FeedBackIDEntityColumn, EntityQueryOp.EQ, pFeedbackID);
            return oPM.GetEntity<Feedback>(qry);
        }

        public static EntityList<Feedback> GetFeedBackList(PersistenceManager pPM, DateTime pStart, DateTime pEnd, FeedBackListType pType) {
            RdbQuery qry = new RdbQuery(typeof(Feedback), Feedback.CityRequestEntityColumn, EntityQueryOp.EQ, false);
            qry.AddClause(Feedback.FeedbackDateEntityColumn, EntityQueryOp.GE, pStart);
            qry.AddClause(Feedback.FeedbackDateEntityColumn, EntityQueryOp.LE, pEnd);
            switch (pType) {
                case FeedBackListType.Open:
                    qry.AddClause(Feedback.ClosedEntityColumn, EntityQueryOp.NE, true);
                    break;
                case FeedBackListType.Closed:
                    qry.AddClause(Feedback.ClosedEntityColumn, EntityQueryOp.EQ, true);
                    break;
            }
            return pPM.GetEntities<Feedback>(qry, QueryStrategy.DataSourceThenCache);
        }

        public static EntityList<Feedback> GetCityRequestList(PersistenceManager pPM, DateTime pStart, DateTime pEnd, FeedBackListType pType) {
            RdbQuery qry = new RdbQuery(typeof(Feedback), Feedback.CityRequestEntityColumn, EntityQueryOp.EQ, true);
            qry.AddClause(Feedback.FeedbackDateEntityColumn, EntityQueryOp.GE, pStart);
            qry.AddClause(Feedback.FeedbackDateEntityColumn, EntityQueryOp.LE, pEnd);
            switch (pType) {
                case FeedBackListType.Open:
                    qry.AddClause(Feedback.ClosedEntityColumn, EntityQueryOp.NE, true);
                    break;
                case FeedBackListType.Closed:
                    qry.AddClause(Feedback.ClosedEntityColumn, EntityQueryOp.EQ, true);
                    break;
            }
            return pPM.GetEntities<Feedback>(qry, QueryStrategy.DataSourceThenCache);
        }

    }

    public enum FeedBackListType {
        Open,
        Closed,
        All
    }

}
