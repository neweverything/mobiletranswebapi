﻿using CabRideEngineDapper.Utils;
using Dapper;
using System;
using System.Data.SqlClient;
using System.Linq;
using TaxiPassCommon;

namespace CabRideEngineDapper {
    public partial class AffiliateDriverDefaults {

        #region Related Tables
        private Affiliate mAffiliate;
        public Affiliate Affiliate {
            get {
                if (mAffiliate == null) {
                    mAffiliate = Affiliate.GetAffiliate(this.AffiliateID);
                }
                return mAffiliate;
            }
        }
        #endregion

        public static AffiliateDriverDefaults Create(Affiliate pAffiliate) {
            AffiliateDriverDefaults oDefault = null;
            SystemDefaults sysDefault = SystemDefaults.GetDefaults();
            try {
                // Creates the State but it is not yet accessible to the application
                oDefault = AffiliateDriverDefaults.Create();
                oDefault.AffiliateID = pAffiliate.AffiliateID;
                oDefault.TaxiPayCVV = true;
                oDefault.ValidateCardImprintAmount = 75;
                oDefault.IVRSignUp = true;
                oDefault.DriverPaysSwipeFee = 1;
                oDefault.DriverPaysSwipeFeePerAmount = 25;

                oDefault.CardChargedMaxNumber = sysDefault.DriverCardChargedMaxNumber;
                oDefault.MaxTransactions = sysDefault.DriverMaxTransactions;
                oDefault.ChargeMaxAmount = sysDefault.DriverChargeMaxAmount;
                oDefault.ChargeMaxSwipeAmount = sysDefault.DriverChargeMaxSwipeAmount;
                oDefault.MaxTransPerCardPerMaxSwipe = sysDefault.MaxTransPerCardPerMaxSwipe;
                oDefault.MaxTransPerWeek = sysDefault.DriverMaxTransPerWeek;
                oDefault.MaxTransPerWeekPerCard = sysDefault.DriverMaxTransPerWeekPerCard;
                oDefault.MaxTransPerMonth = sysDefault.DriverMaxTransPerMonth;
                oDefault.MaxTransPerMonthPerCard = sysDefault.DriverMaxTransPerMonthPerCard;
                oDefault.MaxDailyAmount = sysDefault.DriverMaxDailyAmount;
                oDefault.MaxWeeklyAmount = sysDefault.DriverMaxWeeklyAmount;
                oDefault.MaxMonthlyAmount = sysDefault.DriverMaxMonthlyAmount;
                oDefault.ChargeMaxSwipeAmount = 250M;

                oDefault.StartingFare = 5;
                oDefault.DriverCanSubmitCheck = true;

                //oDefault.AddToManager();
            } catch (Exception ex) {
                throw ex;
            }
            return oDefault;
        }

        public static AffiliateDriverDefaults GetDefault(Affiliate pAffiliate) {
            using (var conn = SqlHelper.OpenSqlConnection()) {
                return GetDefault(conn, pAffiliate);
            }
        }

        public static AffiliateDriverDefaults GetDefault(SqlConnection pConn, Affiliate pAffiliate) {
            string sql = "SELECT * FROM AffiliateDriverDefaults WHERE AffiliateID = @AffiliateID";
            return pConn.Query<AffiliateDriverDefaults>(sql, new { AffiliateID = pAffiliate.AffiliateID }).DefaultIfEmpty(AffiliateDriverDefaults.GetNullEntity()).FirstOrDefault();
        }

        public static AffiliateDriverDefaults GetDefault(SqlConnection pConn, long pAffiliateID) {
            string sql = "SELECT * FROM AffiliateDriverDefaults WHERE AffiliateID = @AffiliateID";
            return pConn.Query<AffiliateDriverDefaults>(sql, new { AffiliateID = pAffiliateID }).DefaultIfEmpty(AffiliateDriverDefaults.GetNullEntity()).FirstOrDefault();
        }


        private string mDefaultVehicleType = "";
        [Editable(false)]
        public string DefaultVehicleType {
            get {
                if (mDefaultVehicleType.IsNullOrEmpty()) {
                    using (var conn = SqlHelper.OpenSqlConnection()) {
                        mDefaultVehicleType = AffiliateRef.GetCreate(conn, AffiliateID, "GetRide", "DefaultVehicleType", "Taxi").ValueString;
                    }
                }
                return mDefaultVehicleType;
            }
            set {
                mDefaultVehicleType = value;
            }
        }

        public decimal CalcSwipeFee(decimal pDriverTotal) {
            decimal driverFee = 0;
            if (AffiliateID != 161 && DriverPaysSwipeFee > 0) {
                if (DriverPaysSwipeFeePerAmount > 0) {
                    decimal dFeeAmt = DriverPaysSwipeFeePerAmount;
                    int multiplier = Convert.ToInt32(pDriverTotal / dFeeAmt);

                    if (pDriverTotal - (dFeeAmt * multiplier) > 0) {
                        multiplier++;
                    }
                    driverFee = DriverPaysSwipeFee * multiplier;
                } else {
                    driverFee += DriverPaysSwipeFee;
                }

            }

            return driverFee;
        }

        public int GetAuthLimit1() {
            int amount = 75;
            if (AuthAmount1.HasValue && AuthAmount1 > 0) {
                amount = AuthAmount1.Value;
            }
            return amount;
        }

        public int GetAuthLimit2() {
            int amount = 100;
            if (AuthAmount2.HasValue && AuthAmount2 > 0) {
                amount = AuthAmount2.Value;
            }
            return amount;
        }

        public int GetAuthLimit3() {
            int amount = Convert.ToInt32(this.ChargeMaxAmount);
            if (amount < 1) {
                amount = Convert.ToInt32(SystemDefaults.GetDefaults().DriverChargeMaxAmount);
            }
            if (amount < 0) {
                amount = 150;
            }
            return amount;
        }


    }

}
