﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using TaxiPassCommon;


namespace CabRideEngineDapper {
    public class ReceiptRequestBrowse {

        public string ACHPaidTo { get; set; }
        public string AffiliateName { get; set; }
        public decimal AirportFee { get; set; }
        public string CardNumber { get; set; }
        public string Salt { get; set; }
        public string CardNumberDisplay { get; set; }
        public string Expiration { get; set; }
        public bool CardSwiped { get; set; }
        public string CardType { get; set; }
        public string CorporateName { get; set; }
        public string ValidationCity { get; set; }
        public string ValidationLicense { get; set; }
        public string ValidationDriver { get; set; }
        public string ValidationState { get; set; }
        public string ValidationStreet { get; set; }
        public decimal Fare { get; set; }
        public decimal Gratuity { get; set; }
        public decimal MiscFee { get; set; }
        public DateTime? ACHPaidDate { get; set; }
        public DateTime ChargeDate { get; set; }
        public string MyDriverName { get; set; }
        public string MyRedeemerAccount { get; set; }
        public string MyRedeemerAccountPhone { get; set; }
        public string MyVehicle { get; set; }
        public string Platform { get; set; }
        public string ReceiptChargedBack { get; set; }
        public DateTime? ChargeBackACHDate { get; set; }
        public DateTime? DateOfWinLoss { get; set; }
        public bool InquiryFulfilled { get; set; }
        public DateTime ReceiptRequest { get; set; }
        public DateTime ReceiptRequestDue { get; set; }
        public string ReceiptRequestNotes { get; set; }
        public string ReceiptRequestTypeDescription { get; set; }
        public DateTime? ValidReceiptReceived { get; set; }
        public bool? NoImprint { get; set; }
        public bool? NoSignature { get; set; }
        public bool? Win { get; set; }
        public decimal TaxiPassFee { get; set; }
        public string TransNo { get; set; }
        public decimal NetLoss { get; set; }
        public decimal TotalCharge { get; set; }
        public bool Failed { get; set; }
        public bool Test { get; set; }

        public bool ChargeBack911 { get; set; }
        public string CBStatus { get; set; }
        public DateTime? CBStatusUpdated { get; set; }


        public int TransactionDay { get; set; }
        public int TransactionMonth { get; set; }
        public int TransactionYear { get; set; }
        public string AuthCode { get; set; }
        public string Reference { get; set; }
        public string CardNo4 { get; set; }
        public string CaseNumber { get; set; }

        public string Gateway { get; set; }
        public string MerchantID { get; set; }
        public string AccountName { get; set; }
        public string MID { get; set; }
        public string MIDDescription { get; set; }

        public string TransNoAsBarCode {
            get {
                return TransNo.ToBarCode();
            }
        }

        public string ExpirationAsDelimitedString {
            get {
                if (Expiration.IsNullOrEmpty()) {
                    return Expiration;
                }
                Encryption oEncrypt = new Encryption();
                return oEncrypt.TripleDESDecrypt(Expiration, Salt);
            }
        }

        //public string CardNumberDecrypted {
        //	get {
        //		try {
        //			if (!CardNumber.IsNullOrEmpty()) {
        //				string sCardNo = DecryptCardNumber();
        //				return sCardNo;
        //			}
        //			return "";
        //		} catch (Exception ex) {
        //			return ex.Message;
        //		}
        //	}
        //}

        public string DecryptCardNumber() {
            if (CardNumber.IsNullOrEmpty()) {
                return "";
            }
            Encryption oEncrypt = new Encryption();
            return oEncrypt.TripleDESDecrypt(CardNumber, Salt);
        }

        public string RedeemerPhone {
            get {
                return MyRedeemerAccountPhone.FormattedPhoneNumber();
            }
        }

        public static List<ReceiptRequestBrowse> Execute(SqlConnection pConn, DateTime pStart, DateTime pEnd, bool pByDueDate = false, long pAffiliateID = 0, string pPlatfrom = "") {
            string sql = $@"SELECT      ACHPaidTo
                                      , Affiliate.Name AffiliateName
                                      , AirportFee
                                      , CardNumber
                                      , DriverPayments.Salt
                                      , CardNumberDisplay
                                      , Expiration
                                      , CardSwiped
                                      , CardType
                                      , CorporateName
                                      , DriverPaymentValidation.City ValidationCity
                                      , DriverPaymentValidation.LicenseNo ValidationLicense
                                      , DriverPaymentValidation.Name ValidationDriver
                                      , DriverPaymentValidation.State ValidationState
                                      , DriverPaymentValidation.Street ValidationStreet
                                      , Fare
                                      , Gratuity
                                      , MiscFee
                                      , ACHDetail.ACHPaidDate
                                      , ChargeDate
                                      , ISNULL(AffiliateDrivers.Name + ' ' + AffiliateDrivers.LastName, Drivers.Name) MyDriverName
                                      , ISNULL(pendingRedeemerAcct.CompanyName, TaxiPassRedeemerAccounts.CompanyName) MyRedeemerAccount
                                      , ISNULL(pendingRedeemerAcct.Phone, TaxiPassRedeemerAccounts.Phone) MyRedeemerAccountPhone
                                      , Vehicles.VehicleNo MyVehicle
                                      , Platform
                                      , ReceiptChargedBack
                                      , ChargeBackACH.ACHPaidDate ChargeBackACHDate
                                      , DateOfWinLoss
                                      , InquiryFulfilled
                                      , ReceiptRequest
                                      , ReceiptRequestDue
                                      , ReceiptRequestNotes
                                      , ReceiptRequestTypes.Description ReceiptRequestTypeDescription
                                      , ValidReceiptReceived
                                      , NoImprint
                                      , NoSignature
                                      , Win
                                      , TaxiPassFee
                                      , TransNo
                                      , NetLoss
                                      , Fare + TaxiPassFee + Gratuity + AirportFee + MiscFee + ISNULL(WaitTime, 0) + ISNULL(Tolls, 0) + ISNULL(Parking, 0) + ISNULL(Stops, 0) + ISNULL(DropFee, 0) + ISNULL(BookingFee, 0) - ISNULL(Coupon.CouponAmount, 0) - Discount TotalCharge
                                      , Failed
                                      , DriverPayments.Test
                                      , ISNULL(ChargeBack911, 0) ChargeBack911
                                      , CBStatus
                                      , CBStatusUpdated
                                      , DAY(ChargeDate) TransactionDay
                                      , MONTH(ChargeDate) TransactionMonth
                                      , YEAR(ChargeDate) TransactionYear
                                      , DriverPaymentResults.AuthCode
                                      , DriverPaymentResults.Reference
                                      , RIGHT(CardNumberDisplay, 4) CardNo4
                                      , CaseNumber
                                      , GroupName Gateway
                                      , CASE WHEN DriverPayments.CreditCardProcessor = 'Dejavoo' THEN DejavooMerchantID.ValueString
                                             ELSE CASE WHEN DriverPayments.CardType = 'AMEX' THEN ISNULL(AmexMerchantID.ValueString, MerchantID.ValueString)
                                                       ELSE MerchantID.ValueString
                                                  END
                                        END MerchantID
                                      , ISNULL(AccountName.ValueString, DejavooMerchantInfo.ValueString) AccountName
                                      , MID.ValueString MID
                                      , MIDDesc.Description MIDDescription
                            FROM        dbo.DriverPayments
                            LEFT JOIN   ReceiptRequest ON ReceiptRequest.DriverPaymentID = DriverPayments.DriverPaymentID
                            LEFT JOIN   dbo.ACHDetail ON ACHDetail.DriverPaymentID = DriverPayments.DriverPaymentID
                            LEFT JOIN   dbo.Affiliate ON Affiliate.AffiliateID = DriverPayments.AffiliateID
                            LEFT JOIN   dbo.DriverPaymentValidation ON DriverPaymentValidation.DriverPaymentID = DriverPayments.DriverPaymentID
                            LEFT JOIN   Drivers ON Drivers.DriverID = DriverPayments.DriverID
                            LEFT JOIN   dbo.AffiliateDrivers ON AffiliateDrivers.AffiliateDriverID = DriverPayments.AffiliateDriverID
                            LEFT JOIN   dbo.TaxiPassRedeemers ON TaxiPassRedeemers.TaxiPassRedeemerID = DriverPayments.TaxiPassRedeemerID
                            LEFT JOIN   dbo.TaxiPassRedeemerAccounts ON TaxiPassRedeemerAccounts.TaxiPassRedeemerAccountID = TaxiPassRedeemers.TaxiPassRedeemerAccountID
                            LEFT JOIN   dbo.DriverPaymentsPendingMatches ON DriverPaymentsPendingMatches.DriverPaymentID = DriverPayments.DriverPaymentID
                            LEFT JOIN   dbo.TaxiPassRedeemers pendingRedeemer ON pendingRedeemer.TaxiPassRedeemerID = DriverPaymentsPendingMatches.TaxiPassRedeemerID
                            LEFT JOIN   dbo.TaxiPassRedeemerAccounts pendingRedeemerAcct ON pendingRedeemerAcct.TaxiPassRedeemerAccountID = pendingRedeemer.TaxiPassRedeemerAccountID
                            LEFT JOIN   dbo.Vehicles ON Vehicles.VehicleID = DriverPayments.VehicleID
                            LEFT JOIN   dbo.ReceiptRequestTypes ON ReceiptRequestTypes.ReceiptRequestTypeID = ReceiptRequest.ReceiptRequestTypeID
                            LEFT JOIN   CardGatewayGroups WITH (NOLOCK) ON CardGatewayGroupsID = DriverPayments.VerisignAccountID
                            LEFT JOIN   DriverPaymentResults ON DriverPaymentResults.DriverPaymentID = DriverPayments.DriverPaymentID
                                                                AND TransType IN ( 'S', 'D' )
                                                                AND Result = '0'
                            OUTER APPLY (   SELECT CAST(ValueString AS SMALLMONEY) CouponAmount
                                            FROM   dbo.DriverPaymentRef WITH (NOLOCK)
                                            WHERE  ReferenceKey = 'Coupon Amount'
                                                   AND DriverPaymentID = dbo.DriverPayments.DriverPaymentID) Coupon
                            OUTER APPLY (   SELECT ACHPaidDate
                                            FROM   ACHDetail
                                            WHERE  DriverPaymentID IN (   SELECT DriverPaymentID
                                                                          FROM   dbo.DriverPayments newDP
                                                                          WHERE  TransNo = DriverPayments.ReceiptChargedBack )) ChargeBackACH
                            OUTER APPLY (   SELECT TOP 1 *
                                            FROM   dbo.DriverPaymentResults WITH (NOLOCK)
                                            WHERE  DriverPaymentID = DriverPayments.DriverPaymentID
                                                   AND Result = '0'
                                                   AND (   TransType = 'S'
                                                           OR TransType = 'D'
                                                           OR TransType = 'C')) chargeResult
                            OUTER APPLY (   SELECT ValueString
                                            FROM   CardGatewayDetails WITH (NOLOCK)
                                            WHERE  CardGatewaysID IN (   SELECT CardGatewaysID
                                                                         FROM   CardGateways WITH (NOLOCK)
                                                                         WHERE  CardGatewayGroupsID = CardGatewayGroups.CardGatewayGroupsID
                                                                                AND Processor = chargeResult.CreditCardProcessor )
                                                   AND ReferenceKey = 'MerchantID') MerchantID
                            OUTER APPLY (   SELECT ValueString
                                            FROM   DriverPaymentRef WITH (NOLOCK)
                                            WHERE  DriverPaymentID = dbo.DriverPayments.DriverPaymentID
                                                   AND ReferenceGroup = 'MerchantInfo'
                                                   AND ReferenceKey = 'MerchantID') DejavooMerchantID
                            OUTER APPLY (   SELECT ValueString
                                            FROM   CardGatewayDetails WITH (NOLOCK)
                                            WHERE  CardGatewaysID IN (   SELECT CardGatewaysID
                                                                         FROM   CardGateways WITH (NOLOCK)
                                                                         WHERE  CardGatewayGroupsID = CardGatewayGroups.CardGatewayGroupsID
                                                                                AND Processor = chargeResult.CreditCardProcessor )
                                                   AND ReferenceKey = 'AmexMerchantNo') AmexMerchantID
                            OUTER APPLY (   SELECT ValueString
                                            FROM   CardGatewayDetails WITH (NOLOCK)
                                            WHERE  CardGatewaysID IN (   SELECT CardGatewaysID
                                                                            FROM   CardGateways WITH (NOLOCK)
                                                                            WHERE  CardGatewayGroupsID = CardGatewayGroups.CardGatewayGroupsID
                                                                                AND Processor = chargeResult.CreditCardProcessor
                                                                        )
                                                    AND ReferenceKey = 'AccountName'
                                        ) AccountName
                            OUTER APPLY (   SELECT ValueString
                                            FROM   DriverPaymentRef WITH (NOLOCK)
                                            WHERE  DriverPaymentID = dbo.DriverPayments.DriverPaymentID
                                                    AND ReferenceGroup = 'MerchantInfo'
                                                    AND ReferenceKey = 'Info'
                                        ) DejavooMerchantInfo
                            OUTER APPLY (   SELECT ValueString
                                            FROM   dbo.AffiliateRef
                                            WHERE  ReferenceGroup = 'Dejavoo'
                                                    AND ReferenceKey = 'MerchantID'
                                                    AND AffiliateID = DriverPayments.AffiliateID
                                        ) MID
                            OUTER APPLY (   SELECT Description
                                            FROM   MIDs
                                            WHERE  MID = MID.ValueString
                                        ) MIDDesc
					WHERE   {(pByDueDate ? "ReceiptRequestDue" : "ReceiptRequest")} >= @StartDate
                            AND {(pByDueDate ? "ReceiptRequestDue" : "ReceiptRequest")} <= @EndDate
							{(pAffiliateID == 0 ? "" : "AND DriverPayments.AffiliateID = @AffiliateID")}
							{(pPlatfrom.IsNullOrEmpty() ? "" : "AND Platform = @Platform")}
                    ORDER BY ReceiptRequest";

            DynamicParameters parms = new DynamicParameters();
            parms.Add("StartDate", pStart);
            parms.Add("EndDate", pEnd);
            if (pAffiliateID != 0) {
                parms.Add("AffiliateID", pAffiliateID);
            }
            if (!pPlatfrom.IsNullOrEmpty()) {
                parms.Add("Platform", pPlatfrom);
            }
            try {
                return pConn.Query<ReceiptRequestBrowse>(sql, parms).ToList();
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
            return new List<ReceiptRequestBrowse>();
        }
    }
}
