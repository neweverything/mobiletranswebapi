﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.ComponentModel;
using System.Text;
using System.Linq;

using IdeaBlade.Persistence;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

using TaxiPassCommon;

namespace CabRideEngine {
	[Serializable]
	public sealed class States:StatesDataRow {

		#region Constructors -- DO NOT MODIFY
		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new'
		private States() : this(null) {
		}

		// Typed DataSet constructor (needed by framework) - DO NOT REMOVE
		public States(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static States Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      States aStates = (States) pManager.CreateEntity(typeof(States));
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aStates, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aStates.AddToManager();
		//      return aStates;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		// Add additional logic to your business object here...
		public static States Create(PersistenceManager pPM, string sCountry) {
			States oState = null;

			try {
				// Creates the State but it is not yet accessible to the application
				oState = (States)pPM.CreateEntity(typeof(States));

				// Using the IdeaBlade Id Generation technique
				//pPM.GenerateId(emp, Employee.IdEntityColumn);

				// CRITICAL: must tell emp to add itself to its PM
				oState.AddToManager();

				// Add custom code here
				oState.Country = sCountry;

			} catch (Exception ex) {
				throw ex;
			}
			return oState;
		}

		
		protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
			if (IsDeserializing)
				return;
			base.OnColumnChanging(pArgs);
			DoAudit(pArgs);
		}

		private void DoAudit(DataColumnChangeEventArgs pArgs) {
			if (!this.PersistenceManager.IsClient) {
				return;
			}
			if (this.RowState == DataRowState.Added) {
				return;
			}
			if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
				AuditRecord = StatesAudit.Create(this.PersistenceManager, this);
			}
			//AuditRecord[pArgs.Column.ColumnName] = pArgs.ProposedValue;
		}

		public override string State {
			get {
				return base.State;
			}
			set {
				base.State = value.ToUpper().Trim();
			}
		}

		// ****************************************************
		// Get just the state name
		// ****************************************************
		public string FindStateCode(string pStateName) {
			EntityList<States> oStates = GetData(this.PersistenceManager, "", pStateName);
			if (oStates.Count == 0) {
				return "";
			} else {
				return oStates[0].State;
			}
		}

		// ****************************************************
		// Get a state, regardless of country
		// ****************************************************
		public static States GetState(PersistenceManager pPM, string pStateName) {
			EntityList<States> oStates = GetData(pPM, "", pStateName);
			if (oStates.Count == 0) {
				return pPM.GetNullEntity<States>();
			} else if (oStates.Count == 1) {
				return oStates[0];
			} else {
				if (pStateName.Length > 2) {
					return oStates.FirstOrDefault(p => p.StateName.Equals(pStateName, StringComparison.CurrentCultureIgnoreCase));
				}
				return oStates.FirstOrDefault(p => p.State.Equals(pStateName, StringComparison.CurrentCultureIgnoreCase));
			}
		}

		// ****************************************************
		// Get all states
		// ****************************************************
		public static EntityList<States> GetStatesByCountry(PersistenceManager pPM, string pCountryCode) {
			if (string.IsNullOrEmpty(pCountryCode)) {
				pCountryCode = Countries.GetDefaultCountry(pPM).Country;
			}

			EntityList<States> oStates = GetData(pPM, pCountryCode, "");
			return oStates;
		}

		// ****************************************************
		// Get a single state
		// ****************************************************
		public static States GetStateForCountry(PersistenceManager pPM, string pCountryCode, string pStateCodeOrName) {
			if (string.IsNullOrEmpty(pCountryCode)) {
				pCountryCode = Countries.GetDefaultCountry(pPM).Country;
			}
			EntityList<States> oStates = GetData(pPM, pCountryCode, pStateCodeOrName);
			if (oStates.Count == 0) {
				return pPM.GetNullEntity<States>();
			} else {
				return oStates[0];
			}
		}

		// ****************************************************
		// Call the acutal "Get" proc
		// ****************************************************
		private static EntityList<States> GetData(PersistenceManager pPM, string pCountryCode, string pStateCodeOrName) {
			StringBuilder sql = new StringBuilder();
			sql.Append("usp_StatesGetByCountry ");
			if (!pCountryCode.IsNullOrEmpty()) {
				sql.AppendFormat(" @Country = '{0}'", pCountryCode);
			}
			if (!pStateCodeOrName.IsNullOrEmpty()) {
				if (!pCountryCode.IsNullOrEmpty()) {
					sql.Append(",");
				}
				sql.AppendFormat(" @StateName = '{0}'", pStateCodeOrName);
			}
			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(States), sql.ToString());
			EntityList<States> oStates = pPM.GetEntities<States>(qry);
			return oStates;
		}

	}


}
