﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using IdeaBlade.Persistence;

namespace CabRideEngine.Utils {
	public class FraudCheck {

		// **************************************************************************
		// Return true if there is any fraud suspected
		// **************************************************************************
		public static bool DriverFraudCheck(PersistenceManager pPM, AffiliateDrivers pAffDriver, Drivers pDriver) {
			bool blnFraud = false;

			StringBuilder sql = new StringBuilder();
			sql.Append("usp_FraudCheck ");
			if (pAffDriver != null) {
				sql.AppendFormat(" @AffiliateDriverID = {0}", pAffDriver.AffiliateDriverID);
			} else { 
				sql.AppendFormat(" @DriverID = {0}", pDriver.DriverID);
			}
			Entity[] oResults = DynamicEntities.DynamicDBCall(pPM, sql.ToString());
			blnFraud = bool.Parse(oResults[0]["FraudAlert"].ToString());

			return blnFraud;
		}


		// **************************************************************************
		// Will check the Aff Driver's Receipt requests against the values in
		// SystemDefaultsDict:
		//		Fraud Check	ReceiptRequestLookBackDays
		//		Fraud Check	ReceiptRequestCount
		// and if driver has met the values will flag the driver as FraudSuspected
		// Will return True if Fraud was detected
		// **************************************************************************
		public static bool CheckAffDriverReceiptRequest(PersistenceManager pPM, long pAffiliateDriverID, string pUser) {
			StringBuilder sql = new StringBuilder();
			sql.AppendFormat("usp_ReceiptRequestFraudCheck @AffiliateDriverID={0}, @ModifiedBy = '{1}'"
				, pAffiliateDriverID
				, pUser);
			Entity[] oResults = DynamicEntities.DynamicDBCall(pPM, sql.ToString());

			return bool.Parse(oResults[0]["FraudDetected"].ToString());
		}



		// **********************************************************************************************
		// If AffiliateDriver.FraudSuspected
		//		load all Driver's Licenses ever used to redeem into FraudDriverLic table
		// Else
		//		Remove all Driver's Licenses in FraudDriverLic table associated by AffiliateDriverID
		// Also adds/removes entries into BlackListDevices if the AffDriver.cell -> Driver.Cell -> BlackListDevice.SerialNo
		// link exists
		// **********************************************************************************************
		public static Entity[] UpdateAffDriverFraudLicences(PersistenceManager pPM, long pAffiliateDriverID, string pUser) {
			StringBuilder sql = new StringBuilder();
			sql.AppendFormat("usp_FraudDriverLicenseUpdateFromAffDriver @AffiliateDriverID ={0}, @ModifiedBy = '{1}'", pAffiliateDriverID, pUser);
			Entity[] oFraudLic = DynamicEntities.DynamicDBCall(pPM, sql.ToString());
			return oFraudLic;
		}
	}
}
