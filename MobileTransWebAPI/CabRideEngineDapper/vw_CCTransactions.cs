﻿using TaxiPassCommon;

namespace CabRideEngineDapper {
    public partial class vw_CCTransactions {

        public string Message { get; set; }
        public string VTSRowID { get; set; }

        private string mTerminalID;
        public string TerminalID {
            get {
                return mTerminalID;
            }
            set {
                mTerminalID = ("" + value).Split('/')[0];
            }
        }

        public string GridViewKey {
            get {
                return string.Format("{0}|{1}|{2}|{3}|{4}|{5}|{6}|{7}",
                                    TransNo,
                                    DriverTotalFull.GetValueOrDefault(0).ToString("C"),
                                    DriverTotalFull.GetValueOrDefault(0).ToString("C"),
                                    DriverTotalShort.GetValueOrDefault(0).ToString("C"),
                                    ChargeDate.HasValue ? ChargeDate.Value.ToString("MM/dd/yyyy hh:mm tt") : "",
                                    VehicleNo,
                                    CardNumberDisplay,
                                    TaxiPassFee.ToString("C"));
            }
        }

        public string CardHolderDecrypted {
            get {
                return CardHolder.DecryptIceKey();
            }
        }
    }
}
