﻿namespace CabRideEngineDapper {
    public partial class DriverRecurringFees {

        private ExpenseTypes mExpenseTypes;
        public ExpenseTypes ExpenseTypes {
            get {
                if (mExpenseTypes == null) {
                    mExpenseTypes = ExpenseTypes.GetByID(this.ExpenseTypeID);
                }
                return mExpenseTypes;
            }
        }
    }
}
