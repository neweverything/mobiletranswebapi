using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

using Csla.Validation;

namespace CabRideEngine {
	/// <summary>
	/// The developer class representing the ShiftName business object class.
	/// </summary>
	/// <remarks>
	/// <para>Place all application-specific business logic in this class.</para>
	/// <para>
	/// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
	/// and will not overwrite changes.
	/// </para>
	/// <para>
	/// This class inherits from the generated class, its companion in the pair of classes 
	/// defining a business object. Properties in the generated class may be overridden in 
	/// order to change the behavior or add business logic.
	/// </para>
	/// <para>
	/// This class is the final class in the inheritance chain for this business object. 
	/// It should not be inherited.
	/// </para>
	/// </remarks>
	[Serializable]
	public sealed partial class ShiftName : ShiftNameDataRow {

		#region Constructors -- DO NOT MODIFY

		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new';
		// it is also required if the class is visible from a web service.
		// DO NOT REMOVE **
		private ShiftName() : this(null) { }

		/// <summary>
		/// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
		/// </summary>
		/// <param name="pRowBuilder"></param>
		public ShiftName(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static ShiftName Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      ShiftName aShiftName = pManager.CreateEntity<ShiftName>();
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aShiftName, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aShiftName.AddToManager();
		//      return aShiftName;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		// Add additional logic to your business object here...
		public static ShiftName Create(PersistenceManager pPM) {
			ShiftName rec = null;
			try {
				rec = pPM.CreateEntity<ShiftName>();
				rec.Number = GetNextShiftNumber(pPM);
				rec.AddToManager();


			} catch (Exception ex) {
				throw ex;
			}
			return rec;
		}

		protected override void AddRules(RuleList pList) {
			PropertyRequiredRule.AddToList(pList, NameEntityColumn.ColumnName);
			PropertyRequiredRule.AddToList(pList, NumberEntityColumn.ColumnName);
			pList.Add(IsNameUnique, NameEntityColumn.ColumnName);
			pList.Add(IsNumberUnique, NameEntityColumn.ColumnName);
		}

		private bool IsNameUnique(object pTarget, RuleArgs e) {
			ShiftName rec = (ShiftName)pTarget;

			RdbQuery qry = new RdbQuery(typeof(ShiftName), ShiftName.NameEntityColumn, EntityQueryOp.EQ, rec.Name);
			qry.AddClause(ShiftName.ShiftNameIDEntityColumn, EntityQueryOp.NE, rec.ShiftNameID);
			EntityList<ShiftName> shiftList = this.PersistenceManager.GetEntities<ShiftName>(qry);
			if (shiftList.Count > 0) {
				e.Description = "Shift Name must be unique!";
				return false;
			}
			return true;
		}

		private bool IsNumberUnique(object pTarget, RuleArgs e) {
			ShiftName rec = (ShiftName)pTarget;

			RdbQuery qry = new RdbQuery(typeof(ShiftName), ShiftName.NumberEntityColumn, EntityQueryOp.EQ, rec.Number);
			qry.AddClause(ShiftName.NumberEntityColumn, EntityQueryOp.NE, rec.Number);
			EntityList<ShiftName> shiftList = this.PersistenceManager.GetEntities<ShiftName>(qry);
			if (shiftList.Count > 0) {
				e.Description = "Number must be unique!";
				return false;
			}
			return true;
		}

		[BindingBrowsable(false)]
		public override string EntityInstanceDisplayName {
			get {
				return Name;
			}
		}

		protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
			if (IsDeserializing)
				return;
			base.OnColumnChanging(pArgs);
			//DoAudit(pArgs);
		}

		private void DoAudit(DataColumnChangeEventArgs pArgs) {
			if (!this.PersistenceManager.IsClient) {
				return;
			}
			if (this.RowState == DataRowState.Added) {
				return;
			}
			if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
				AuditRecord = ShiftNameAudit.Create(this);
			}
		}

		public static EntityList<ShiftName> GetAll(PersistenceManager pPM) {
			EntityList<ShiftName> typeList = pPM.GetEntities<ShiftName>();
			typeList.ApplySort(ShiftName.NumberEntityColumn.ColumnName, System.ComponentModel.ListSortDirection.Ascending, false);
			return typeList;
		}

		public static ShiftName GetByID(PersistenceManager pPM, long pShiftNameID) {
			return pPM.GetEntity<ShiftName>(new PrimaryKey(typeof(ShiftName), pShiftNameID));
		}

		public static int GetNextShiftNumber(PersistenceManager pPM) {
			string sql = "SELECT TOP 1 * from ShiftName ORDER BY Number Desc";
			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(ShiftName), sql);
			return pPM.GetEntity<ShiftName>(qry).Number + 1;
		}

	}

	#region EntityPropertyDescriptors.ShiftNamePropertyDescriptor
	////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
	////    For example: 
	////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
	//public static partial class EntityPropertyDescriptors {
	//	public partial class ShiftNamePropertyDescriptor : BaseEntityPropertyDescriptor {
	//    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
	//    public AdaptedPropertyDescriptor Age {
	//      get { return this.Get("Age"); }
	//    }
	//  }
	//} 
	#endregion

}