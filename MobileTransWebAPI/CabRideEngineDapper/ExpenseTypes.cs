﻿using CabRideEngineDapper.Utils;
using Dapper;

namespace CabRideEngineDapper {
    public partial class ExpenseTypes {

        public static ExpenseTypes GetByID(long pExpenseTypeID) {
            string sql = @"SELECT  *
                            FROM    dbo.ExpenseTypes
                            WHERE   ExpenseTypeID = @ExpenseTypeID";
            using (var conn = SqlHelper.OpenSqlConnection()) {
                return conn.Get<ExpenseTypes>(sql, new { ExpenseTypeID = pExpenseTypeID });
            }
        }
    }
}
