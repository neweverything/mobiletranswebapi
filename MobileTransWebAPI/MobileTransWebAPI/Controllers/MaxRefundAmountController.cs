﻿using CabRideEngineDapper;
using CabRideEngineDapper.Utils;
using Dapper;
using MobileTransWebAPI.Models;
using Newtonsoft.Json;
using System;
using System.Web.Http;
using TaxiPassCommon;

namespace MobileTransWebAPI.Controllers {
    public class MaxRefundAmountController : ApiController {

        // GET api/maxrefundamount
        public MaxRefundResult Get(string pRequest) {
            MaxRefundResult result = new MaxRefundResult();

            MaxRefund refund = JsonConvert.DeserializeObject<MaxRefund>(pRequest.Decrypt());
            string sql = @"SELECT TOP 1
                                    *
                            FROM    DriverPayments
                            WHERE   AffiliateID = @AffiliateID
                                    AND AffiliateDriverID = @AffiliateDriverID
                                    AND Number = @Number
                                    AND Fare > 0
                                    AND Failed = 0
                                    AND Test = 0
                                    AND ChargeDate IS NOT NULL
                            ORDER BY DriverPaymentID DESC";

            using (var conn = SqlHelper.OpenSqlConnection()) {
                DriverPayments rec = conn.Get<DriverPayments>(sql, new { AffiliateID = refund.AffiliateID, AffiliateDriverID = refund.AffiliateDriverID, Number = refund.Number });
                if (rec.IsNullEntity) {
                    result.ErrorMsg = "No transactions found to refund for this card";
                    return result;
                }
                decimal refundedAmount = Math.Abs(DriverPaymentRef.GetRefundedAmount(rec.TransNo));
                result.MaxRefundAmount = rec.DriverTotal - refundedAmount;
                if (result.MaxRefundAmount <= 0) {
                    result.ErrorMsg = "Last transaction has already been refunded";
                }
                result.TransNo = rec.TransNo;
            }

            return result;
        }



    }
}
