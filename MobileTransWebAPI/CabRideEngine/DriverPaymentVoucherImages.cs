﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Text;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

using TaxiPassCommon;
using Csla.Validation;

namespace CabRideEngine {
	/// <summary>
	/// The developer class representing the DriverPaymentVoucherImages business object class.
	/// </summary>
	/// <remarks>
	/// <para>Place all application-specific business logic in this class.</para>
	/// <para>
	/// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
	/// and will not overwrite changes.
	/// </para>
	/// <para>
	/// This class inherits from the generated class, its companion in the pair of classes 
	/// defining a business object. Properties in the generated class may be overridden in 
	/// order to change the behavior or add business logic.
	/// </para>
	/// <para>
	/// This class is the final class in the inheritance chain for this business object. 
	/// It should not be inherited.
	/// </para>
	/// </remarks>
	[Serializable]
	public sealed partial class DriverPaymentVoucherImages : DriverPaymentVoucherImagesDataRow {

		#region Constructors -- DO NOT MODIFY

		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new';
		// it is also required if the class is visible from a web service.
		// DO NOT REMOVE **
		private DriverPaymentVoucherImages() : this(null) { }

		/// <summary>
		/// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
		/// </summary>
		/// <param name="pRowBuilder"></param>
		public DriverPaymentVoucherImages(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static DriverPaymentVoucherImages Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      DriverPaymentVoucherImages aDriverPaymentVoucherImages = pManager.CreateEntity<DriverPaymentVoucherImages>();
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aDriverPaymentVoucherImages, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aDriverPaymentVoucherImages.AddToManager();
		//      return aDriverPaymentVoucherImages;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		// Add additional logic to your business object here...
		public static DriverPaymentVoucherImages Create(DriverPayments pDriverPayments) {
			RdbQuery qry = new RdbQuery(typeof(DriverPaymentVoucherImages), DriverPaymentVoucherImages.DriverPaymentIDEntityColumn, EntityQueryOp.EQ, pDriverPayments.DriverPaymentID);

			DriverPaymentVoucherImages rec = pDriverPayments.PersistenceManager.GetEntity<DriverPaymentVoucherImages>(qry);
			if (rec.IsNullEntity || rec.DriverPaymentID == 0) {
				rec = pDriverPayments.PersistenceManager.CreateEntity<DriverPaymentVoucherImages>();
				pDriverPayments.PersistenceManager.GenerateId(rec, DriverPaymentVoucherImages.DriverPaymentVoucherImageIDEntityColumn);
				rec.DriverPaymentID = pDriverPayments.DriverPaymentID;
				rec.AddToManager();

				// Set the scan date for VoucherImages
				if (!rec.DriverPayments.ScanDate.HasValue) {
					//if (rec.DriverPayments.Affiliate.TimeZone.IsNullOrEmpty()) {
					rec.DriverPayments.ScanDate = DateTime.Now;
					//} else {
					//	TimeZoneConverter convert = new TimeZoneConverter();
					//	rec.DriverPayments.ScanDate = convert.ConvertTime(DateTime.Now, TimeZone.CurrentTimeZone.StandardName, rec.DriverPayments.Affiliate.TimeZone);
					//}
					// Did this way to prevent the VoucherImage table from being saved.  
					// ScanDate was not being saved
					EntityList<DriverPayments> saveRecs = new EntityList<DriverPayments>();
					saveRecs.Add(rec.DriverPayments);
					pDriverPayments.PersistenceManager.SaveChanges(saveRecs);
				}

			}
			return rec;
		}

		protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
			if (IsDeserializing) {
				return;
			}
			base.OnColumnChanging(pArgs);
			DoAudit(pArgs);
		}

		private void DoAudit(DataColumnChangeEventArgs pArgs) {
			if (!this.PersistenceManager.IsClient) {
				return;
			}
			if (this.RowState == DataRowState.Added) {
				return;
			}
			if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
				AuditRecord = DriverPaymentVoucherImageAudit.Create(this);
			}
		}

		public override bool Matched {
			get {
				return base.Matched;
			}
			set {
				base.Matched = value;
				if (value) {
					this.DriverPayments.MatchDate = DateTime.Now;
					if (Amount > 0) {
						this.DriverPayments.MatchAmount = this.Amount;
					}
				} else {
					this.DriverPayments.MatchDate = null;
					this.DriverPayments.MatchAmount = 0;
				}
			}
		}

		public override decimal Amount {
			get {
				return base.Amount;
			}
			set {
				base.Amount = value;
				if (this.Matched) {
					this.DriverPayments.MatchAmount = value;
				}
			}
		}

		public string MyImageURL {
			get {
				SystemDefaults def = SystemDefaults.GetDefaults(this.PersistenceManager);
				string url = def.VoucherImageURL;


				// We are no longer storing images at cckiosk.com - don't execute this code
				//if (this.DriverPayments.Platform == Platforms.CCKiosk.ToString() ) {
				//if (this.ImageURL.Substring(4, 1) != @"/") {
				//	url = def.CCKioskVoucherImageURL;
				//}
				string imageURL = this.ImageURL.Replace('\\', '/');

				if (!url.EndsWith("/") && !this.ImageURL.StartsWith("/")) {
					url += "/";
				}
				if (imageURL.StartsWith("http", StringComparison.CurrentCultureIgnoreCase)) {
					url = imageURL;
				} else {
					url += imageURL;
				}
				return url;
			}
		}

		public DateTime DateReceived {
			get {
				DriverPaymentVoucherImageAudit rec = DriverPaymentVoucherImageAudit.GetFirstVoucherImage(this);
				if (rec.IsNullEntity) {
					return this.ModifiedDate.Value;
				}
				return rec.ModifiedDate.Value;
			}
		}

		public DateTime DateReceivedLocalTime {
			get {
				if (!this.DriverPayments.Affiliate.TimeZone.IsNullOrEmpty()) {
					TimeZoneConverter timeZone = new TimeZoneConverter();
					return timeZone.FromUniversalTime(this.DriverPayments.Affiliate.TimeZone, this.DateReceived);
				}
				return TimeZone.CurrentTimeZone.ToLocalTime(this.DateReceived);
			}
		}

		public DateTime ModifiedDateLocalTime {
			get {
				if (!this.DriverPayments.Affiliate.TimeZone.IsNullOrEmpty()) {
					TimeZoneConverter timeZone = new TimeZoneConverter();
					return timeZone.FromUniversalTime(this.DriverPayments.Affiliate.TimeZone, this.ModifiedDate.Value);
				}
				return TimeZone.CurrentTimeZone.ToLocalTime(this.ModifiedDate.Value);
			}
		}


		#region Retreive Records
		// Grab all vouchers ready to be matched
		public static EntityList<DriverPaymentVoucherImages> GetAllValidUnMatched(PersistenceManager pPM) {
			StringBuilder sql = new StringBuilder();
			sql.Append("SELECT * ");
			sql.Append(" FROM DriverPaymentVoucherImages with (nolock) ");
			sql.Append(" inner join DriverPayments on DriverPaymentVoucherImages.DriverPaymentID = DriverPayments.DriverPaymentID");
			sql.Append(" WHERE matched = 0 ");
			sql.Append(" and Invalid = 0 ");
			sql.Append(" and DriverPayments.Platform not in ('CCKiosk', 'NexStep') ");
			sql.Append(" and driverpaymentvoucherimageid not in ");
			sql.Append(" (SELECT driverpaymentvoucherimageid ");
			sql.Append("FROM   dbo.matcherrowlock  with (nolock) ");
			sql.Append("WHERE  ( datelocked >= CASE WHEN matcherrowlock.externallyprocessed = 1 THEN Dateadd(HOUR, -2, Getdate())  ");
			sql.Append("ELSE Dateadd(mi, -15, Getdate()) ");
			sql.Append("END )) ");

			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(Affiliate), sql.ToString());

			EntityList<DriverPaymentVoucherImages> list = new EntityList<DriverPaymentVoucherImages>();
			try {
				list = pPM.GetEntities<DriverPaymentVoucherImages>(qry);

			} catch (Exception) {
			}
			return list;
		}

		public static EntityList<DriverPaymentVoucherImages> GetAllInValid(PersistenceManager pPM) {
			RdbQuery qry = new RdbQuery(typeof(DriverPaymentVoucherImages), DriverPaymentVoucherImages.MatchedEntityColumn, EntityQueryOp.EQ, false);
			qry.AddClause(DriverPaymentVoucherImages.InvalidEntityColumn, EntityQueryOp.EQ, true);
			return pPM.GetEntities<DriverPaymentVoucherImages>(qry, QueryStrategy.DataSourceThenCache);
		}

		#endregion
	}

	#region EntityPropertyDescriptors.DriverPaymentVoucherImagesPropertyDescriptor
	////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
	////    For example: 
	////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
	//public static partial class EntityPropertyDescriptors {
	//	public partial class DriverPaymentVoucherImagesPropertyDescriptor : BaseEntityPropertyDescriptor {
	//    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
	//    public AdaptedPropertyDescriptor Age {
	//      get { return this.Get("Age"); }
	//    }
	//  }
	//} 
	#endregion

}