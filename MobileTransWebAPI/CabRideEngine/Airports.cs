﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using System.ComponentModel;

using IdeaBlade.Persistence;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;
using IdeaBlade.Rdb;

using Csla.Validation;

using TaxiPassCommon;
using TaxiPassCommon.GeoCoding;

namespace CabRideEngine {
	[Serializable]
	public sealed class Airports : AirportsDataRow {

		#region Constructors -- DO NOT MODIFY
		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new'
		private Airports() : this(null) {
		}

		// Typed DataSet constructor (needed by framework) - DO NOT REMOVE
		public Airports(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static Airports Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      Airports aAirport = (Airports) pManager.CreateEntity(typeof(Airports));
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aAirport, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aAirport.AddToManager();
		//      return aAirport;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		// Add additional logic to your business object here...
		public static Airports Create(PersistenceManager pManager, long pCoverageZoneID) {

			Airports oAirport = (Airports)pManager.CreateEntity(typeof(Airports));

			//pManager.GenerateId(oAirport, CabRideEngine.Airports.AirportIDEntityColumn);
			oAirport.CoverageZoneID = pCoverageZoneID;

			oAirport.AddToManager();
			return oAirport;
		}

		//protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
		//	if (IsDeserializing)
		//		return;
		//	base.OnColumnChanging(pArgs);
		//	DoAudit(pArgs);
		//}

		//private void DoAudit(DataColumnChangeEventArgs pArgs) {
		//	if (!this.PersistenceManager.IsClient) {
		//		return;
		//	}
		//	if (this.RowState == DataRowState.Added) {
		//		return;
		//	}
		//	if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
		//		AuditRecord = AirportsAudit.Create(this.PersistenceManager, this);
		//	}
		//	//AuditRecord[pArgs.Column.ColumnName] = pArgs.ProposedValue;
		//}

		public override string Airport {
			get {
				return base.Airport;
			}
			set {
				value = value.Trim().ToUpper();
				if (value == "") {
					throw new Exception("Airport code cannot be blank");
				}

				Address oAddress = null;
				try {
					SystemDefaults oDefaults = SystemDefaults.GetDefaults(this.PersistenceManager);
					MapPointService oService = new MapPointService(oDefaults.MapPointUserName, oDefaults.MapPointPassword);
					oAddress = oService.FindAirport(value);
				} catch (Exception ex) {
					throw ex;
				}

				if (oAddress == null) {
					throw new Exception("Unknown Airport Code: " + value);
				}

				string sTemp = "" + oAddress.Name;
				if (sTemp.Contains("[")) {
					sTemp = sTemp.Split('[')[1];
				}
				if (sTemp.Contains("]")) {
					sTemp = sTemp.Split(']')[0];
				}
				base.Airport = value;

				foreach (CoverageZones oZone in this.PersistenceManager.GetEntities<CoverageZones>()) {
					if (oZone.CoverageZone == value) {
						CoverageZoneID = oZone.CoverageZoneID;
						break;
					}
				}

				base.Name = sTemp;
				base.Latitude = oAddress.Latitude;
				base.Longitude = oAddress.Longitude;

				OnPropertyChanged("Airport");
			}
		}

		protected override void AddRules(RuleList pList) {
			PropertyRequiredRule.AddToList(pList, Airports.AirportEntityColumn.ColumnName);
			pList.Add(IsAirportUnique, AirportEntityColumn.ColumnName);

			//base.AddRules(pList);
		}

		private bool IsAirportUnique(object pTarget, RuleArgs e) {
			Airports oAirport = (Airports)pTarget;

			RdbQuery oQry = new RdbQuery(typeof(Airports), Airports.AirportEntityColumn, EntityQueryOp.EQ, oAirport.Airport);
			oQry.AddClause(Airports.AirportIDEntityColumn, EntityQueryOp.NE, oAirport.AirportID);
			EntityList<Airports> oAirports = this.PersistenceManager.GetEntities<Airports>(oQry);
			if (oAirports.Count > 0) {
				e.Description = "Airport code must be unique!";
				return false;
			}
			return true;
		}

		[BindingBrowsable(false)]
		public override string EntityInstanceDisplayName {
			get {
				return Airport;
			}
		}

		public static Airports GetAirport(PersistenceManager pPM, string pAirportCode) {
			RdbQuery oQry = new RdbQuery(typeof(Airports), Airports.AirportEntityColumn, EntityQueryOp.EQ, pAirportCode);
			return pPM.GetEntity<Airports>(oQry);
		}

		public static EntityList<Airports> GetAirportsForCountryState(PersistenceManager pPM, string pCountryCode, string pStateCode) {
			RdbQuery oQry = new RdbQuery(typeof(Airports), Airports.CountryEntityColumn, EntityQueryOp.EQ, pCountryCode);
			oQry.AddClause(Airports.StateEntityColumn, EntityQueryOp.EQ, pStateCode);
			oQry.AddClause(Airports.SecondaryStateEntityColumn, EntityQueryOp.EQ, pStateCode);
			oQry.AddOperator(EntityBooleanOp.Or);
			oQry.AddOrderBy(Airports.NameEntityColumn, ListSortDirection.Ascending);

			EntityList<Airports> oAirports = pPM.GetEntities<Airports>(oQry);
			
			return oAirports;
		}

		public String AirportNameAndCity {
			get {
				string sName = this.Name;
				if (this.City != null && this.City != "") {
					sName += " - " + this.City;
				}
				return sName;
			}
		}

		public static Airports GetAirportByZipCode(PersistenceManager pPM, string pZipCode) {
			RdbQuery oQry = new RdbQuery(typeof(Airports), Airports.ZIPCodeEntityColumn, EntityQueryOp.EQ, pZipCode);
			return pPM.GetEntity<Airports>(oQry);
		}

	}


	public class PickUpAirports {
		private string mCode;
		private string mName;
		private string mCity;

		private bool mIsNullEntity = false;

		private static BindableList<PickUpAirports> msEntities;
		private static PickUpAirports msNullEntity;
		
		public PickUpAirports(PersistenceManager pPM, string pState, string pCountry) {

			// Create the null entity
			msNullEntity = new PickUpAirports("", "", "");
			msNullEntity.mIsNullEntity = true;

			// Populate list of Routing Types.
			msEntities = new BindableList<PickUpAirports>();

			AdoHelper oHelper = new AdoHelper();
			RdbKey aRdbKey = pPM.DataSourceResolver.GetDataSourceKey("Default") as RdbKey;
			oHelper = aRdbKey.AdoHelper;

			string sqlSelect = "SELECT DISTINCT  Airports.AirportID,  Airports.Airport, Airports.Name, Airports.City FROM Airports " +
								"INNER JOIN CoverageZones ON Airports.CoverageZoneID = CoverageZones.CoverageZoneID " +
								"INNER JOIN CoverageArea ON CoverageZones.CoverageZoneID = CoverageArea.CoverageZoneID " +
								"INNER JOIN Rates ON CoverageArea.CoverageZoneID = Rates.PUCoverageZoneID " +
								"Where Airports.Country = " + StringUtils.Quoted(pCountry) + " AND (Airports.State = " + StringUtils.Quoted(pState) +
								" OR Airports.SecondaryState = " + StringUtils.Quoted(pState) + ") " +
								"ORDER BY Airports.Name";
			
			IDbConnection aConnection = oHelper.CreateDbConnection(true);
			aConnection.Open();
			using (aConnection) {
				IDbCommand aCommand = oHelper.CreateDbCommand(aConnection);
				aCommand.CommandText = sqlSelect;
				IDataReader aDataReader = aCommand.ExecuteReader();
				while (aDataReader.Read()) {
					msEntities.Add(new PickUpAirports(aDataReader.GetString(1), aDataReader.GetString(2), aDataReader.GetString(3)));
				}
				aDataReader.Close();
			}
		}

		/// <summary>Get the Routing Types null entity.</summary>
		public static PickUpAirports NullEntity {
			get {
				return msNullEntity;
			}
		}

		/// <summary>Return true if this is the null entity</summary>
		[BindingBrowsable(false)]
		public bool IsNullEntity {
			get {
				return mIsNullEntity;
			}
		}

		 /// <summary>Constructor (Private)</summary>
		private PickUpAirports(string pCode, string pName, string pCity) {
			mCode = pCode;
			mName = pName;
			mCity = pCity;
		}

		/// <summary>Get the list of all Pick Up Airports.</summary>
		public BindableList<PickUpAirports> GetAll() {
			return msEntities;
		}
		public string Airport {
			get {
				return mCode;
			}
			set {
				mCode = value;
			}
		}

		public string Name {
			get {
				return mName;
			}
			set {
				mName = value;
			}
		}

		public string City {
			get {
				return mCity;
			}
			set {
				mCity = value;
			}
		}

		public string AirportNameAndCity {
			get {
				string sName = this.Name;
				if (this.City != null && this.City != "") {
					sName += " - " + this.City;
				}
				return sName;
			}
		}

	}

}
