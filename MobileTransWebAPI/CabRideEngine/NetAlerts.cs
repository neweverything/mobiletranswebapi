﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

using TaxiPassCommon;
using System.Linq;
using System.Collections.Generic;

namespace CabRideEngine {
    /// <summary>
    /// The developer class representing the NetAlerts business object class.
    /// </summary>
    /// <remarks>
    /// <para>Place all application-specific business logic in this class.</para>
    /// <para>
    /// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
    /// and will not overwrite changes.
    /// </para>
    /// <para>
    /// This class inherits from the generated class, its companion in the pair of classes 
    /// defining a business object. Properties in the generated class may be overridden in 
    /// order to change the behavior or add business logic.
    /// </para>
    /// <para>
    /// This class is the final class in the inheritance chain for this business object. 
    /// It should not be inherited.
    /// </para>
    /// </remarks>
    [Serializable]
    public sealed partial class NetAlerts : NetAlertsDataRow {

        #region Constructors -- DO NOT MODIFY

        // Do not create constructors for this class
        // Developers cannot create instances of this class with the "new" keyword
        // You should write a static Create method instead; see the "Suggested Customization" region
        // Please review the documentation to learn about entity creation and inheritance

        // This private constructor prevents accidental instance creation via 'new';
        // it is also required if the class is visible from a web service.
        // DO NOT REMOVE **
        private NetAlerts() : this(null) { }

        /// <summary>
        /// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
        /// </summary>
        /// <param name="pRowBuilder"></param>
        public NetAlerts(DataRowBuilder pRowBuilder)
            : base(pRowBuilder) {
        }

        #endregion

        #region Suggested Customizations

        //    // Use this factory method template to create new instances of this class
        //    public static NetAlerts Create(PersistenceManager pManager,
        //      ... your creation parameters here ...) { 
        //
        //      NetAlerts aNetAlerts = pManager.CreateEntity<NetAlerts>();
        //
        //      // if this object type requires a unique id and you have implemented
        //      // the IIdGenerator interface implement the following line
        //      pManager.GenerateId(aNetAlerts, // add id column here //);
        //
        //      // Add custom code here
        //
        //      aNetAlerts.AddToManager();
        //      return aNetAlerts;
        //    }

        //    // Implement this method if you want your object to sort in a specific order
        //    public override int CompareTo(Object pObject) {
        //    }

        //    // Implement this method to customize the null object version of this class
        //    protected override void UpdateNullEntity() {
        //    }

        #endregion

        // Add additional logic to your business object here...
        public static NetAlerts Create(Drivers pDriver) {
            PersistenceManager oPM = pDriver.PersistenceManager;
            NetAlerts rec = oPM.CreateEntity<NetAlerts>();

            try {
                // Using the IdeaBlade Id Generation technique
                oPM.GenerateId(rec, NetAlerts.NetAlertIDEntityColumn);
                rec.DriverID = pDriver.DriverID;
                rec.AddToManager();
            } catch (Exception ex) {
                throw ex;
            }
            return rec;
        }

        public override SaveResult Save() {
            if (MessageDate == null) {
                MessageDate = DateTime.Now;
            }

            if (string.IsNullOrEmpty(this.Address)) {
                FillAddress();
            }

            if (!this.ResponseAffiliateDriverID.HasValue && this.ResponseDate.HasValue) {
                DriversGPSHistory rec = DriversGPSHistory.GetDriverRecordForDateTime(this.Drivers, this.ResponseDate.Value);
                TimeSpan myTime = this.ResponseDate.Value.Subtract(rec.GPSTime);
                if (!this.Drivers.MyAffiliate.AffiliateDriverDefaults.IsNullEntity) {
                    if (myTime.Days == 0 && myTime.Hours == 0 && myTime.Minutes <= this.Drivers.MyAffiliate.AffiliateDriverDefaults.GPSDriverLocationUpdate) {
                        this.ResponseAffiliateDriverID = rec.AffiliateDriverID;
                    }
                }
            }
            return base.Save();
        }

        public void FillAddress() {
            if (this.Latitude.HasValue && this.Longitude.HasValue) {
                if (this.Latitude != 0 && this.Longitude != 0) {
                    try {
                        com.TaxiPass.GeoCodeWS.GeoCodeService oService = new CabRideEngine.com.TaxiPass.GeoCodeWS.GeoCodeService();
                        com.TaxiPass.GeoCodeWS.Address oAddr = oService.GetAddressByGeoCode(this.Latitude.Value.ToString(), this.Longitude.Value.ToString());
                        this.Address = oAddr.Street;
                        if (oAddr.Distance > 0) {
                            this.Address += " within " + oAddr.Distance + " feet";
                        }
                    } catch {
                    }
                }
            }
        }


        public override DateTime MessageDate {
            get {
                return base.MessageDate;
            }
            set {
                TimeZoneConverter convert = new TimeZoneConverter();
                base.MessageDate = convert.ConvertTime(value, TimeZone.CurrentTimeZone.StandardName, this.Drivers.MyAffiliate.TimeZone);
            }
        }

        //public DateTime MessageDateLocalTime {
        //    get {
        //        if (this.Drivers.MyAffiliate.IsNullEntity || string.IsNullOrEmpty(this.Drivers.MyAffiliate.TimeZone)) {
        //            return this.MessageDate;
        //        }
        //        TimeZoneConverter oTimeZone = new TimeZoneConverter();
        //        return oTimeZone.FromUniversalTime(this.Drivers.MyAffiliate.TimeZone, MessageDate);
        //    }
        //}


        public override DateTime? ResponseDate {
            get {
                return base.ResponseDate;
            }
            set {
                base.ResponseDate = value;
                if (value.HasValue) {
                    TimeZoneConverter convert = new TimeZoneConverter();
                    base.ResponseDate = convert.ConvertTime(value.Value, TimeZone.CurrentTimeZone.StandardName, this.Drivers.MyAffiliate.TimeZone);
                }
            }
        }


        public string MyDriver {
            get {
                return this.Drivers.Name;
            }
        }

        public string AffiliateDriverName {
            get {
                if (this.AffiliateDriverID.HasValue) {
                    AffiliateDrivers rec = AffiliateDrivers.GetDriver(this.PersistenceManager, this.AffiliateDriverID.Value);
                    return rec.Name;
                }
                return this.MessageTo;
            }
        }

        public string DriverMessage {
            get {
                return this.MessageFromDriver ? "Yes" : "";
            }
        }

        public string ResponseAffiliateDriverName {
            get {
                if (!this.ResponseAffiliateDriverID.HasValue && this.ResponseDate.HasValue) {
                    DriversGPSHistory rec = DriversGPSHistory.GetDriverRecordForDateTime(this.Drivers, this.ResponseDate.Value);
                    TimeSpan myTime = this.ResponseDate.Value.Subtract(rec.GPSTime);
                    //if (this.Drivers.MyAffiliate.AffiliateDriverDefaultses.Count > 0) {
                    int mins = this.Drivers.MyAffiliate.AffiliateDriverDefaults.GPSDriverLocationUpdate.GetValueOrDefault(0) / 60;
                    if (myTime.Days == 0 && myTime.Hours == 0 && myTime.Minutes <= mins) {
                        this.ResponseAffiliateDriverID = rec.AffiliateDriverID;
                    }
                    //}
                }
                return this.AffiliateDrivers.Name;
            }
        }

        public static NetAlerts GetNetAlert(PersistenceManager pPM, long pNetAlertID) {
            RdbQuery qry = new RdbQuery(typeof(NetAlerts), NetAlerts.NetAlertIDEntityColumn, EntityQueryOp.EQ, pNetAlertID);
            return pPM.GetEntity<NetAlerts>(qry, QueryStrategy.DataSourceThenCache);
        }

        public static EntityList<NetAlerts> GetNetAlerts(Affiliate pAffiliate, DateTime pStart, DateTime pEnd) {
            RdbQuery qry = new RdbQuery(typeof(NetAlerts), NetAlerts.MessageDateEntityColumn, EntityQueryOp.GE, pStart);
            qry.AddClause(NetAlerts.MessageDateEntityColumn, EntityQueryOp.LE, pEnd);
            qry.AddClause(NetAlerts.ResponseDateEntityColumn, EntityQueryOp.GE, pStart);
            qry.AddClause(NetAlerts.ResponseDateEntityColumn, EntityQueryOp.LE, pEnd);
            qry.AddOperator(EntityBooleanOp.And);
            qry.AddOperator(EntityBooleanOp.Or);
            qry.AddOperator(EntityBooleanOp.And);
            EntityList<NetAlerts> list = pAffiliate.PersistenceManager.GetEntities<NetAlerts>(qry, QueryStrategy.DataSourceThenCache);
            var x = (from p in list
                     where p.Drivers.AffiliateID == pAffiliate.AffiliateID
                     select p).ToList();

            return new EntityList<NetAlerts>(x);
        }

        public static EntityList<NetAlerts> GetNetAlerts(EntityList<Affiliate> pAffiliateList, DateTime pStart, DateTime pEnd) {
            RdbQuery qry = new RdbQuery(typeof(NetAlerts), NetAlerts.MessageDateEntityColumn, EntityQueryOp.GE, pStart);
            qry.AddClause(NetAlerts.MessageDateEntityColumn, EntityQueryOp.LE, pEnd);
            qry.AddClause(NetAlerts.ResponseDateEntityColumn, EntityQueryOp.GE, pStart);
            qry.AddClause(NetAlerts.ResponseDateEntityColumn, EntityQueryOp.LE, pEnd);
            qry.AddOperator(EntityBooleanOp.And);
            qry.AddOperator(EntityBooleanOp.Or);
            qry.AddOperator(EntityBooleanOp.And);
            EntityList<NetAlerts> list = pAffiliateList[0].PersistenceManager.GetEntities<NetAlerts>(qry, QueryStrategy.DataSourceThenCache);

            List<long> affIdList = (from p in pAffiliateList select p.AffiliateID).ToList();

            var x = (from p in list
                     where affIdList.Contains(p.Drivers.AffiliateID.GetValueOrDefault(0))
                     select p).ToList();

            return new EntityList<NetAlerts>(x);
        }

    }

    #region EntityPropertyDescriptors.NetAlertsPropertyDescriptor
    ////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
    ////    For example: 
    ////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
    //public static partial class EntityPropertyDescriptors {
    //	public partial class NetAlertsPropertyDescriptor : BaseEntityPropertyDescriptor {
    //    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
    //    public AdaptedPropertyDescriptor Age {
    //      get { return this.Get("Age"); }
    //    }
    //  }
    //} 
    #endregion

}