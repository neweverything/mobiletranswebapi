﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Dapper;
using TaxiPassCommon;

using CabRideEngineDapper.Utils;
using System.Data;

namespace CabRideEngineDapper {
	public partial class RedeemerCashAccounting {

		public static List<RedeemerCashAccounting> GetForTrackedAccounts(TaxiPassRedeemerAccounts pRedeemerAccount, DateTime pStartDate, DateTime pEndDate) {
			DynamicParameters param = new DynamicParameters();
			param.Add("RedeemerAccountID", pRedeemerAccount.TaxiPassRedeemerAccountID);
			param.Add("StartDate", pStartDate);
			param.Add("EndDate", pEndDate);

			using (var conn = SqlHelper.OpenSqlConnection()) {
				return conn.Query<RedeemerCashAccounting>("usp_RedeemerCashAccounting_TrackedAccounts", param, commandType: CommandType.StoredProcedure).ToList();
			}

		}
	}
}
