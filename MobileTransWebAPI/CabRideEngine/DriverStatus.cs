﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

using Csla.Validation;

using TaxiPassCommon;
using System.Text;

namespace CabRideEngine {
    /// <summary>
    /// The developer class representing the DriverStatus business object class.
    /// </summary>
    /// <remarks>
    /// <para>Place all application-specific business logic in this class.</para>
    /// <para>
    /// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
    /// and will not overwrite changes.
    /// </para>
    /// <para>
    /// This class inherits from the generated class, its companion in the pair of classes 
    /// defining a business object. Properties in the generated class may be overridden in 
    /// order to change the behavior or add business logic.
    /// </para>
    /// <para>
    /// This class is the final class in the inheritance chain for this business object. 
    /// It should not be inherited.
    /// </para>
    /// </remarks>
    [Serializable]
    public sealed partial class DriverStatus : DriverStatusDataRow {

        #region Constructors -- DO NOT MODIFY

        // Do not create constructors for this class
        // Developers cannot create instances of this class with the "new" keyword
        // You should write a static Create method instead; see the "Suggested Customization" region
        // Please review the documentation to learn about entity creation and inheritance

        // This private constructor prevents accidental instance creation via 'new';
        // it is also required if the class is visible from a web service.
        // DO NOT REMOVE **
        private DriverStatus() : this(null) { }

        /// <summary>
        /// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
        /// </summary>
        /// <param name="pRowBuilder"></param>
        public DriverStatus(DataRowBuilder pRowBuilder)
            : base(pRowBuilder) {
        }

        #endregion

        #region Suggested Customizations

        //    // Use this factory method template to create new instances of this class
        //    public static DriverStatus Create(PersistenceManager pManager,
        //      ... your creation parameters here ...) { 
        //
        //      DriverStatus aDriverStatus = pManager.CreateEntity<DriverStatus>();
        //
        //      // if this object type requires a unique id and you have implemented
        //      // the IIdGenerator interface implement the following line
        //      pManager.GenerateId(aDriverStatus, // add id column here //);
        //
        //      // Add custom code here
        //
        //      aDriverStatus.AddToManager();
        //      return aDriverStatus;
        //    }

        //    // Implement this method if you want your object to sort in a specific order
        //    public override int CompareTo(Object pObject) {
        //    }

        //    // Implement this method to customize the null object version of this class
        //    protected override void UpdateNullEntity() {
        //    }

        #endregion

        // Add additional logic to your business object here...
        public static DriverStatus Create(PersistenceManager pPM) {
            DriverStatus rec = null;
            try {
                rec = pPM.CreateEntity<DriverStatus>();

                // Using the IdeaBlade Id Generation technique
                pPM.GenerateId(rec, DriverStatus.DriverStatusIDEntityColumn);

                rec.AddToManager();

            } catch (Exception ex) {
                throw ex;
            }
            return rec;
        }

        protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
            if (IsDeserializing)
                return;
            base.OnColumnChanging(pArgs);
            DoAudit(pArgs);
        }

        private void DoAudit(DataColumnChangeEventArgs pArgs) {
            if (!this.PersistenceManager.IsClient) {
                return;
            }
            if (this.RowState == DataRowState.Added) {
                return;
            }
            if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
                AuditRecord = DriverStatusAudit.Create(this.PersistenceManager, this);
            }
            //AuditRecord[pArgs.Column.ColumnName] = pArgs.ProposedValue;
        }

        protected override void AddRules(RuleList pList) {
            PropertyRequiredRule.AddToList(pList, StatusEntityColumn.ColumnName);
            pList.Add(IsStatusUnique, StatusEntityColumn.ColumnName);

            base.AddRules(pList);
        }


        private bool IsStatusUnique(object pTarget, RuleArgs e) {
            DriverStatus rec = pTarget as DriverStatus;

            RdbQuery qry = new RdbQuery(typeof(DriverStatus), DriverStatus.StatusEntityColumn, EntityQueryOp.EQ, rec.Status);
            qry.AddClause(DriverStatus.DriverStatusIDEntityColumn, EntityQueryOp.NE, rec.DriverStatusID);
            EntityList<DriverStatus> recList = this.PersistenceManager.GetEntities<DriverStatus>(qry, QueryStrategy.DataSourceThenCache);
            if (recList.Count > 0) {
                e.Description = "Status must be unique!";
                return false;
            }
            return true;
        }

        public override bool DefaultStatus {
            get {
                return base.DefaultStatus;
            }
            set {
                base.DefaultStatus = value;
                if (value) {
                    EntityList<DriverStatus> recList = this.PersistenceManager.GetEntities<DriverStatus>();
                    foreach (DriverStatus rec in recList) {
                        if (rec.DriverStatusID != this.DriverStatusID) {
                            rec.DefaultStatus = false;
                        }
                    }
                }
            }
        }

		// *******************************************************************
		// Get all status'
		// *******************************************************************
		public static EntityList<DriverStatus> GetAll(PersistenceManager pPM) {
			return GetData(pPM, "", "", "");
		}

		// *******************************************************************
		// Get all default status
		// *******************************************************************
        public static DriverStatus GetDefault(PersistenceManager pPM) {
			EntityList<DriverStatus> oStats = GetData(pPM, "", "1", "");
			if (oStats.Count == 0) {
				return pPM.GetNullEntity<DriverStatus>();
			} else {
				return oStats[0];
			}
        }

		// *******************************************************************
		// Get a status
		// *******************************************************************
        public static DriverStatus GetStatus(PersistenceManager pPM, string pStatus) {
			EntityList<DriverStatus> oStats = GetData(pPM, pStatus, "", "");
			if (oStats.Count == 0) {
				return pPM.GetNullEntity<DriverStatus>();
			} else {
				return oStats[0];
			}
        }

		// *******************************************************************
		// Get a status by ID
		// *******************************************************************
        public static DriverStatus GetStatus(PersistenceManager pPM, long pDriverStatusID) {
			EntityList<DriverStatus> oStats = GetData(pPM, "", "", pDriverStatusID.ToString());
			if (oStats.Count == 0) {
				return pPM.GetNullEntity<DriverStatus>();
			} else {
				return oStats[0];
			}
        }

		// ****************************************************
		// Call the acutal "Get" proc
		// pDefaultStatus is either "", "1" for true, "0" for false
		// ****************************************************
		private static EntityList<DriverStatus> GetData(PersistenceManager pPM, string pStatus, string pDefaultStatus, string pID) {
			StringBuilder sql = new StringBuilder();
			string sDelimiter = "";
			int iArgCount = 0;

			sql.Append("usp_DriverStatusGet ");
			if (!pStatus.IsNullOrEmpty()) {
				sql.AppendFormat(" @Status = '{0}'", pStatus);
				iArgCount++;
			}
			if (!pDefaultStatus.IsNullOrEmpty()) {
				if (iArgCount > 0) {
					sDelimiter = ", ";
				}
				sql.AppendFormat("{0} @DefaultStatus = {1}", sDelimiter, pDefaultStatus);
				iArgCount++;
			}
			if (!pDefaultStatus.IsNullOrEmpty()) {
				if (iArgCount > 0) {
					sDelimiter = ", ";
				}
				sql.AppendFormat("{0} @DriverStatusID = {1}", sDelimiter, pID);
				iArgCount++;
			}
			
			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(DriverStatus), sql.ToString());
			EntityList<DriverStatus> oPagerTypes = pPM.GetEntities<DriverStatus>(qry);
			return oPagerTypes;
		}


    }

    #region EntityPropertyDescriptors.DriverStatusPropertyDescriptor
    ////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
    ////    For example: 
    ////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
    //public static partial class EntityPropertyDescriptors {
    //	public partial class DriverStatusPropertyDescriptor : BaseEntityPropertyDescriptor {
    //    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
    //    public AdaptedPropertyDescriptor Age {
    //      get { return this.Get("Age"); }
    //    }
    //  }
    //} 
    #endregion

}