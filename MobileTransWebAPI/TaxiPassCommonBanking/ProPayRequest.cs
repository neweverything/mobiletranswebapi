﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace TaxiPassCommon.Banking {
	public class ProPayRequest {


		public static ProPayResponse SubmitRequest(string pURL, string pRequest) {
			byte[] dataToSend = Encoding.UTF8.GetBytes(pRequest);

			ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

			//Change the following URL to point to production instead of integration
			WebRequest webRequest = WebRequest.Create(pURL);
			webRequest.Method = "POST";
			webRequest.ContentLength = dataToSend.Length;
			webRequest.ContentType = "text/xml";
			Stream dataStream = webRequest.GetRequestStream();
			dataStream.Write(dataToSend, 0, dataToSend.Length);
			dataStream.Close();

			string response = string.Empty;

			try {
				WebResponse apiResponse = webRequest.GetResponse();


				using (StreamReader sr = new StreamReader(apiResponse.GetResponseStream())) {
					response += sr.ReadToEnd();
				}
			} catch (WebException wex) {
				HttpWebResponse httpResponse = wex.Response as HttpWebResponse;
				using (Stream responseStream = httpResponse.GetResponseStream())
				using (StreamReader reader = new StreamReader(responseStream)) {
					response = reader.ReadToEnd();
				}
			}
			//RestClient client = new RestClient(pURL);
			//RestRequest request = new RestRequest(Method.POST);
			//request.RequestFormat = DataFormat.Xml;
			//request.AddBody(pRequest);
			//var response = client.Execute(request);

			XmlDocument doc = new XmlDocument();
			doc.LoadXml(response);
			string js = JsonConvert.SerializeXmlNode(doc);

			ProPayResponse resp = JsonConvert.DeserializeObject<ProPayResponse>(js);
			return resp;
		}


		public static class XmlSerializer<T> {
			public static XmlSerializer Serializer = new XmlSerializer(typeof(T));

			/// <summary>
			/// Writes to a string <paramref name="data"/> using <see cref="Encoding.UTF8"/>.
			/// </summary>
			/// <remarks>
			/// This defaults the encoding to <see cref="Encoding.UTF8"/> because that is what xml internal uses 
			/// to read in xml transactions.
			/// </remarks>
			/// <param name="data">The data to write to a string.</param>        
			/// <returns>A string representation of <paramref name="data"/>.</returns>
			public static string WriteToString(T data) {
				return WriteToString(data, Encoding.UTF8);
			}

			/// <summary>
			/// Writes to a string <paramref name="data"/> using <paramref name="encoding"/>.
			/// </summary>
			/// <param name="data">The data to write to a string.</param>
			/// <param name="encoding">The encoding to use when writing to a string.</param>
			/// <returns>A string representation of <paramref name="data"/>.</returns>
			public static string WriteToString(T data, Encoding encoding) {
				string retVal;
				using (MemoryStream memoryStream = new MemoryStream()) {
					using (XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, encoding)) {
						Serializer.Serialize(xmlTextWriter, data);
					}

					retVal = encoding.GetString(memoryStream.ToArray());
				}

				return retVal;
			}
		}


		public class XmlProcessTransaction : XmlTransaction {
			/// <summary>
			/// The account number assigned by ProPay.
			/// </summary>
			[XmlElement("accountNum")]
			public string AccountNumber = string.Empty;

			[XmlElement("track1")]
			public string Track1 = string.Empty;

			[XmlElement("track2")]
			public string Track2 = string.Empty;

			/// <summary>
			/// The credit card CVV code.
			/// </summary>
			[XmlElement("CVV2")]
			public string CVV2 = string.Empty;

			/// <summary>
			/// The credit card number used to pay for the renewal.
			/// </summary>
			[XmlElement("ccNum")]
			public string CreditCardNumber = string.Empty;

			/// <summary>
			/// The credit card expiration date in ‘mmdd’ format.
			/// </summary>
			[XmlElement("expDate")]
			public string ExpirationDate = string.Empty;

			/// <summary>
			/// Cardholder address. Necessary for valid AVS responses on ProPay accounts held in foreign currencies.
			/// *Max length is 40 for multi-currency transactions
			/// </summary>
			[XmlElement("addr")]
			public string Address = string.Empty;

			/// <summary>
			/// Cardholder Address second line
			/// </summary>
			[XmlElement("addr2")]
			public string Address2 = string.Empty;

			/// <summary>
			/// Cardholder Address third line.
			/// </summary>
			[XmlElement("addr3")]
			public string Address3 = string.Empty;

			/// <summary>
			/// The value representing the number of pennies in USD, or the number of [currency] without decimals.
			/// </summary>
			[XmlElement("amount")]
			public string Amount = string.Empty;

			/// <summary>
			/// Cardholder apartment number.
			/// *Do not use if using addr2 instead.
			/// </summary>
			[XmlElement("aptNum")]
			public string ApartmentNumber = string.Empty;

			/// <summary>
			/// Not needed if track data used
			/// </summary>
			[XmlElement("cardholderName")]
			public string CardHolderName = string.Empty;

			/// <summary>
			/// Cardholder City. Necessary for valid AVS responses on ProPay accounts held in foreign currencies.
			/// </summary>
			[XmlElement("city")]
			public string City = string.Empty;

			/// <summary>
			/// Merchant transaction descriptor.
			/// </summary>
			[XmlElement("comment1")]
			public string Comment1 = string.Empty;

			/// <summary>
			/// Merchant transaction descriptor.
			/// </summary>
			[XmlElement("comment2")]
			public string Comment2 = string.Empty;


			[XmlElement("TransactionMerchantDescriptor")]
			public string TransactionMerchantDescriptor = string.Empty;

			/// <summary>
			/// Cardholder country. Necessary for valid AVS responses on ProPay accounts held in foreign currencies
			/// </summary>
			[XmlElement("country")]
			public string Country = string.Empty;

			/// <summary>
			///ISO standard 3 character currency code for a foreign currency transaction. Amex and Discover are not
			///supported on Multi-Currency transactions(Auth in one currency, settle in another).
			/// </summary>
			[XmlElement("CurrencyCode")]
			public string CurrencyCode = string.Empty;

			/// <summary>
			/// Valid Values:
			///  Y
			///  N
			///Defaults to N if not passed or if an invalid entry is detected.
			/// </summary>
			[XmlElement("DebtRepayment")]
			public string DebtRepayment = string.Empty;

			/// <summary>
			/// Optional for Threat Metrix. Status 133 is returned when declined by Threat Metrix.
			/// </summary>
			[XmlElement("InputIpAddress")]
			public string InputIpAddress = string.Empty;

			/// <summary>
			/// Transactions are rejected as duplicate when the same card is charged for the same amount with the same
			/// invoice number, including blank invoices, in a 60 second period.
			/// </summary>
			[XmlElement("invNum")]
			public string InvoiceNumber = string.Empty;

			/// <summary>
			/// Valid Values:
			///  Y
			///  N
			///  R
			///Defaults to N if not passed.
			/// </summary>
			[XmlElement("recurringPayment")]
			public string RecurringPayment = string.Empty;

			/// <summary>
			/// Required for, and obtained from Threat Metrix fraud prevention solution
			/// </summary>
			[XmlElement("SessionId")]
			public string SessionId = string.Empty;

			/// <summary>
			/// Omit unless specially instructed by ProPay.
			/// Passing a customer email address will create an email receipt to be sent from ProPay.
			/// </summary>
			[XmlElement("sourceEmail")]
			public string SourceEmail = string.Empty;

			/// <summary>
			/// The state.
			/// </summary>
			[XmlElement("state")]
			public string State = string.Empty;

			/// <summary>
			/// The US zip code of the credit card. 5 or 9 digits without a dash for US cards. Omit for international credit cards.
			/// </summary>
			[XmlElement("zip")]
			public string Zip = string.Empty;

			/// <summary>
			/// The US zip code of the credit card. 5 or 9 digits without a dash for US cards. Omit for international credit cards.
			/// </summary>
			[XmlElement("transNum")]
			public string ReferenceNo = string.Empty;

			[XmlElement("RecAccntNum")]
			public string SplitPayAcctNum = string.Empty;

			[XmlElement("secondaryAmount")]
			public string SplitPayAmount = string.Empty;
		}


		[XmlInclude(typeof(XmlProcessTransaction))]
		public class XmlTransaction {
			/// <summary>
			/// The transaction type.
			/// </summary>
			[XmlElement("transType")]
			public string TransType = string.Empty;
		}

		/// <summary>
		/// The XML request object.
		/// </summary>
		[XmlRoot("XMLRequest")]
		public class XmlTransactionRequest {
			/// <summary>
			/// Supplied by ProPay, Used to access the API.
			/// </summary>
			[XmlElement("certStr")]
			public string CertificationString = string.Empty;

			/// <summary>
			/// Omit unless specifically instructed by ProPay, Used to access the API.
			/// </summary>
			[XmlElement("termid")]
			public string TerminalID = string.Empty;

			/// <summary>
			/// The XML transactions to process.
			/// </summary>
			[XmlElement("XMLTrans")]
			public List<XmlTransaction> Transactions = new List<XmlTransaction>();
		}
	}
}
