using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Runtime.Serialization;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

namespace CabRideEngine {
 
  #region ATMCassetteAuditDataTable
  /// <summary>
  /// The generated <see cref="T:IdeaBlade.Persistence.EntityTable"/> class  
  /// containing all <see cref="ATMCassetteAudit"/> objects.
  /// </summary>
  /// <remarks>
  /// ****************************************************
  /// <para>** DO NOT MODIFY THIS CLASS - AutoGenerated Code</para>
  /// <para>** Any changes made to this code will be lost</para>
  /// <para>** the next time this code is regenerated.</para>
  /// <para>**</para>
  /// <para>** Generated from:   EntityBaseclass.template</para>
  /// <para>** Last modified:    2012-03-15T13:24:48.4668218-07:00</para>
  /// <para>** Template version: 3.110127</para>
  /// ****************************************************
  /// </remarks>
  [BindingBrowsable(false)]
  [Serializable]
  public partial class ATMCassetteAuditDataTable : BaseEntityDataTable, 
     IdeaBlade.Persistence.Rdb.IRdbTable {
  
    /// <summary>
    /// Initializes a new instance of the ATMCassetteAuditDataTable class with no arguments.
    /// </summary>
    public ATMCassetteAuditDataTable() {}

    /// <summary>
    /// Initializes a new instance of the ATMCassetteAuditDataTable class 
    /// with the <see cref="System.Runtime.Serialization.SerializationInfo"/> 
    /// and the <see cref="System.Runtime.Serialization.StreamingContext"/>.
    /// </summary>
    /// <param name="pInfo">The data needed to serialize or deserialize an object.</param>
    /// <param name="pContext">The source and destination of a given serialized stream.</param>
    protected ATMCassetteAuditDataTable(SerializationInfo pInfo, StreamingContext pContext) :
      base(pInfo, pContext) {}

    /// <summary>
    /// Creates a new instance of the <see cref="ATMCassetteAuditDataRow"/>.
    /// Constructs a row from the builder.
    /// <para>** For internal use only. **</para>
    /// </summary>
    /// <param name="pRowBuilder"></param>
    /// <returns>The <see cref="ATMCassetteAuditDataRow"/>.</returns>
    protected override DataRow NewRowFromBuilder(DataRowBuilder pRowBuilder) {
      return new ATMCassetteAudit(pRowBuilder);
    }

    /// <summary>Returns the <see cref="System.Type"/> of <see cref="ATMCassetteAudit"/>.</summary>
    protected override Type GetRowType() {
      return typeof(ATMCassetteAudit);
    }
    
    /// <summary>
    /// Gets the name of the <see cref="T:IdeaBlade.Rdb.RdbKey"/> or 
    /// <see cref="T:IdeaBlade.Persistence.WS.WsKey"/> defined in <b>IdeaBlade.ibconfig</b>. 
    /// </summary>
    /// <remarks>For this <see cref="ATMCassetteAudit"/>, the DataSourceKeyName="Audit".</remarks>
    public override String DataSourceKeyName {
      get { return @"Audit"; }
    }

    #region Column properties
    //**************************************
    //* Column properties
    //**************************************
    /// <summary>Gets the ATMCassetteHistoryID <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn ATMCassetteHistoryIDColumn {
      get {
        if (mATMCassetteHistoryIDColumn!=null) return mATMCassetteHistoryIDColumn;
        mATMCassetteHistoryIDColumn = GetColumn("ATMCassetteHistoryID", true);
        return mATMCassetteHistoryIDColumn;
      }
    }
    private DataColumn mATMCassetteHistoryIDColumn;
    
    /// <summary>Gets the ATMCassetteID <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn ATMCassetteIDColumn {
      get {
        if (mATMCassetteIDColumn!=null) return mATMCassetteIDColumn;
        mATMCassetteIDColumn = GetColumn("ATMCassetteID", true);
        return mATMCassetteIDColumn;
      }
    }
    private DataColumn mATMCassetteIDColumn;
    
    /// <summary>Gets the Atmid <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn AtmidColumn {
      get {
        if (mAtmidColumn!=null) return mAtmidColumn;
        mAtmidColumn = GetColumn("Atmid", true);
        return mAtmidColumn;
      }
    }
    private DataColumn mAtmidColumn;
    
    /// <summary>Gets the CassetteNumber <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn CassetteNumberColumn {
      get {
        if (mCassetteNumberColumn!=null) return mCassetteNumberColumn;
        mCassetteNumberColumn = GetColumn("CassetteNumber", true);
        return mCassetteNumberColumn;
      }
    }
    private DataColumn mCassetteNumberColumn;
    
    /// <summary>Gets the PreDenomination <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn PreDenominationColumn {
      get {
        if (mPreDenominationColumn!=null) return mPreDenominationColumn;
        mPreDenominationColumn = GetColumn("PreDenomination", true);
        return mPreDenominationColumn;
      }
    }
    private DataColumn mPreDenominationColumn;
    
    /// <summary>Gets the PreAmount <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn PreAmountColumn {
      get {
        if (mPreAmountColumn!=null) return mPreAmountColumn;
        mPreAmountColumn = GetColumn("PreAmount", true);
        return mPreAmountColumn;
      }
    }
    private DataColumn mPreAmountColumn;
    
    /// <summary>Gets the PostDenomination <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn PostDenominationColumn {
      get {
        if (mPostDenominationColumn!=null) return mPostDenominationColumn;
        mPostDenominationColumn = GetColumn("PostDenomination", true);
        return mPostDenominationColumn;
      }
    }
    private DataColumn mPostDenominationColumn;
    
    /// <summary>Gets the PostAmount <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn PostAmountColumn {
      get {
        if (mPostAmountColumn!=null) return mPostAmountColumn;
        mPostAmountColumn = GetColumn("PostAmount", true);
        return mPostAmountColumn;
      }
    }
    private DataColumn mPostAmountColumn;
    
    /// <summary>Gets the ChangeDate <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn ChangeDateColumn {
      get {
        if (mChangeDateColumn!=null) return mChangeDateColumn;
        mChangeDateColumn = GetColumn("ChangeDate", true);
        return mChangeDateColumn;
      }
    }
    private DataColumn mChangeDateColumn;
    
    /// <summary>Gets the RowVersion <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn RowVersionColumn {
      get {
        if (mRowVersionColumn!=null) return mRowVersionColumn;
        mRowVersionColumn = GetColumn("RowVersion", true);
        return mRowVersionColumn;
      }
    }
    private DataColumn mRowVersionColumn;
    
    /// <summary>Gets the ModifiedDate <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn ModifiedDateColumn {
      get {
        if (mModifiedDateColumn!=null) return mModifiedDateColumn;
        mModifiedDateColumn = GetColumn("ModifiedDate", true);
        return mModifiedDateColumn;
      }
    }
    private DataColumn mModifiedDateColumn;
    
    #endregion    

    /// <summary>Creates the schema mapping information for this ATMCassetteAuditDataTable.</summary>
    /// <returns>
    /// The <see cref="T:IdeaBlade.Persistence.TableMappingInfo"/> for this ATMCassetteAuditDataTable.
    /// </returns>
    protected override TableMappingInfo CreateTableMappingInfo() {
      RdbTableMappingInfo mappingInfo = new RdbTableMappingInfo(this);
      mappingInfo.SourceOwnerName = "dbo"; 
      mappingInfo.SourceTableName = "ATMCassetteAudit"; 
      mappingInfo.ConcurrencyColumnName = ""; 
      // ColumnSelectionOption: selectAllColumns
      mappingInfo.SourceColumnNames = "*"; 
   
      DataColumnMappingCollection columnMappings = mappingInfo.TableMapping.ColumnMappings;
      columnMappings.Clear();
      columnMappings.Add("ATMCassetteHistoryID", "ATMCassetteHistoryID");
      columnMappings.Add("ATMCassetteID", "ATMCassetteID");
      columnMappings.Add("ATMID", "Atmid");
      columnMappings.Add("CassetteNumber", "CassetteNumber");
      columnMappings.Add("PreDenomination", "PreDenomination");
      columnMappings.Add("PreAmount", "PreAmount");
      columnMappings.Add("PostDenomination", "PostDenomination");
      columnMappings.Add("PostAmount", "PostAmount");
      columnMappings.Add("ChangeDate", "ChangeDate");
      columnMappings.Add("RowVersion", "RowVersion");
      columnMappings.Add("ModifiedDate", "ModifiedDate");
      return mappingInfo;
    }
  
    /// <summary>** For internal use only. **</summary>
    protected override void UpdateTableMappingInfo() {
      base.UpdateTableMappingInfo();
    }

    /// <summary>
    /// Initializes ATMCassetteAudit <see cref="System.Data.DataColumn"/> properties.
    /// </summary>
    public override void InitColumnProperties() {
      base.InitColumnProperties();

      ATMCassetteHistoryIDColumn.Caption = "ATMCassetteHistoryID";
      ATMCassetteIDColumn.Caption = "ATMCassetteID";
      AtmidColumn.Caption = "ATMID";
      CassetteNumberColumn.Caption = "CassetteNumber";
      PreDenominationColumn.Caption = "PreDenomination";
      PreAmountColumn.Caption = "PreAmount";
      PostDenominationColumn.Caption = "PostDenomination";
      PostAmountColumn.Caption = "PostAmount";
      ChangeDateColumn.Caption = "ChangeDate";
      RowVersionColumn.Caption = "RowVersion";
      ModifiedDateColumn.Caption = "ModifiedDate";
    }
  }
  #endregion
  
  #region ATMCassetteAuditDataRow
  /// <summary>
  /// The generated base class for <see cref="ATMCassetteAudit"/>.
  /// </summary>
  /// <remarks>
  /// <para>
  /// It represents a row of persistable data in the <see cref="ATMCassetteAuditDataTable"/>
  /// and derives from <see cref="T:IdeaBlade.Persistence.Entity"/>.
	/// </para>
  /// ****************************************************
  /// <para>** DO NOT MODIFY THIS CLASS - AutoGenerated Code</para>
  /// <para>** Any changes made to this code will be lost</para>
  /// <para>** the next time this code is regenerated.</para>
  /// <para>**</para>
  /// <para>** Generated from:   EntityBaseclass.template</para>
  /// <para>** Last modified:    2012-03-15T13:24:48.4668218-07:00</para>
  /// <para>** Template version: 3.110127</para>
  /// ****************************************************
  /// </remarks>  
  [Serializable]
  public abstract partial class ATMCassetteAuditDataRow : BaseEntity, 
     IdeaBlade.Persistence.Rdb.IRdbEntity {
  
    /// <summary>
    /// Initializes a new instance of the ATMCassetteAuditDataRow.
    /// Constructs a row from the builder.
    /// <para>** For internal use only. **</para>
    /// </summary>
    /// <param name="pRowBuilder"></param>
    protected ATMCassetteAuditDataRow(DataRowBuilder pRowBuilder)
      : base(pRowBuilder) {
    }

    /// <summary>Gets the <see cref="ATMCassetteAuditDataTable"/>.</summary>
    [BindingBrowsable(false)]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public new ATMCassetteAuditDataTable TypedTable {
      get { return (ATMCassetteAuditDataTable) base.Table; }
    }

    /// <summary>Returns the <see cref="System.Type"/> of <see cref="ATMCassetteAuditDataTable"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public static Type GetTableType() {
      return typeof(ATMCassetteAuditDataTable);
    }

    /// <summary>
    /// Gets the SQL 'where' clause used to restrict which 
    /// database records are accessible via this entity. 
    /// </summary>
    /// <remarks>
    /// The default property value is defined in the Current Class 
    /// Properties tab in the <b>IdeaBlade DevForce Object Mapping Tool</b>.
    /// <para>
    /// The 'where' clause will be used, in addition to any other 
    /// query-specific criteria, in every retrieval of entities of this type.
    /// </para>
    /// </remarks>
    [BindingBrowsable(false)]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public virtual string SqlWhereClause {
      get { return ""; }
    }
    
    /// <summary>
    /// Determines whether to use the 'distinct' keyword when 
    /// generating the SQL select clause for objects of this type.
    /// </summary>
    /// <remarks>Defaults to false, but may be overridden in subclasses.</remarks>
    [BindingBrowsable(false)]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public virtual bool ForceSqlDistinct {
      get { return false; }
    }
    

    #region Enums
    //**************************************
    //* Enums
    //**************************************
    #endregion
	
    #region Relation properties
    //**************************************
    //* Relation properties
    //**************************************
    #endregion
    
    #region EntityColumn definitions
    //**************************************
    //* Static EntityColumn definitions
    //**************************************
    /// <summary>The ATMCassetteHistoryID <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn ATMCassetteHistoryIDEntityColumn =
      new EntityColumn(typeof(ATMCassetteAudit), "ATMCassetteHistoryID", typeof(System.Int64), false, true, true, DataSourceAccessType.ReadWrite);      
    /// <summary>The ATMCassetteID <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn ATMCassetteIDEntityColumn =
      new EntityColumn(typeof(ATMCassetteAudit), "ATMCassetteID", typeof(System.Int64), false, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The Atmid <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn AtmidEntityColumn =
      new EntityColumn(typeof(ATMCassetteAudit), "Atmid", typeof(System.Int64), false, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The CassetteNumber <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn CassetteNumberEntityColumn =
      new EntityColumn(typeof(ATMCassetteAudit), "CassetteNumber", typeof(System.Int32), true, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The PreDenomination <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn PreDenominationEntityColumn =
      new EntityColumn(typeof(ATMCassetteAudit), "PreDenomination", typeof(System.Int32), true, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The PreAmount <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn PreAmountEntityColumn =
      new EntityColumn(typeof(ATMCassetteAudit), "PreAmount", typeof(System.Decimal), true, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The PostDenomination <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn PostDenominationEntityColumn =
      new EntityColumn(typeof(ATMCassetteAudit), "PostDenomination", typeof(System.Int32), true, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The PostAmount <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn PostAmountEntityColumn =
      new EntityColumn(typeof(ATMCassetteAudit), "PostAmount", typeof(System.Decimal), true, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The ChangeDate <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn ChangeDateEntityColumn =
      new EntityColumn(typeof(ATMCassetteAudit), "ChangeDate", typeof(System.DateTime), true, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The RowVersion <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn RowVersionEntityColumn =
      new EntityColumn(typeof(ATMCassetteAudit), "RowVersion", typeof(System.Int32), false, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The ModifiedDate <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn ModifiedDateEntityColumn =
      new EntityColumn(typeof(ATMCassetteAudit), "ModifiedDate", typeof(System.DateTime), false, false, false, DataSourceAccessType.ReadWrite);      
    #endregion
    
    #region Properties
    //**************************************
    //* ATMCassetteHistoryID methods
    //**************************************
    /// <summary>Gets the ATMCassetteHistoryID <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn ATMCassetteHistoryIDColumn {
      get { return TypedTable.ATMCassetteHistoryIDColumn; }
    }

    /// <summary>Gets the ATMCassetteHistoryID.</summary>
    [DBDataType(typeof(System.Int64))]
    public virtual System.Int64 ATMCassetteHistoryID {
      get { 
        System.Int64 result_;
        if (GetInterceptor<System.Int64>("ATMCassetteHistoryID", GetATMCassetteHistoryIDImpl, out result_)) return result_;
        return GetATMCassetteHistoryIDImpl();
      }
    }
    private System.Int64 GetATMCassetteHistoryIDImpl() {
      return (System.Int64) GetColumnValue(ATMCassetteHistoryIDColumn, typeof(System.Int64), false); 
    }
    
    //**************************************
    //* ATMCassetteID methods
    //**************************************
    /// <summary>Gets the ATMCassetteID <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn ATMCassetteIDColumn {
      get { return TypedTable.ATMCassetteIDColumn; }
    }

    /// <summary>Gets or sets the ATMCassetteID.</summary>
    [DBDataType(typeof(System.Int64))]
    public virtual System.Int64 ATMCassetteID {
      get { 
        System.Int64 result_;
        if (GetInterceptor<System.Int64>("ATMCassetteID", GetATMCassetteIDImpl, out result_)) return result_;
        return GetATMCassetteIDImpl();
      }
      set { 
          if (!SetInterceptor<System.Int64>("ATMCassetteID", value, SetATMCassetteIDImpl)) {
            SetATMCassetteIDImpl(value);
          }  
      }    
    }
    private System.Int64 GetATMCassetteIDImpl() {
      return (System.Int64) GetColumnValue(ATMCassetteIDColumn, typeof(System.Int64), false); 
    }
    private void SetATMCassetteIDImpl(System.Int64 value) {
      SetColumnValue(ATMCassetteIDColumn, value);
    }
    
    //**************************************
    //* Atmid methods
    //**************************************
    /// <summary>Gets the Atmid <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn AtmidColumn {
      get { return TypedTable.AtmidColumn; }
    }

    /// <summary>Gets or sets the Atmid.</summary>
    [DBDataType(typeof(System.Int64))]
    public virtual System.Int64 Atmid {
      get { 
        System.Int64 result_;
        if (GetInterceptor<System.Int64>("Atmid", GetAtmidImpl, out result_)) return result_;
        return GetAtmidImpl();
      }
      set { 
          if (!SetInterceptor<System.Int64>("Atmid", value, SetAtmidImpl)) {
            SetAtmidImpl(value);
          }  
      }    
    }
    private System.Int64 GetAtmidImpl() {
      return (System.Int64) GetColumnValue(AtmidColumn, typeof(System.Int64), false); 
    }
    private void SetAtmidImpl(System.Int64 value) {
      SetColumnValue(AtmidColumn, value);
    }
    
    //**************************************
    //* CassetteNumber methods
    //**************************************
    /// <summary>Gets the CassetteNumber <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn CassetteNumberColumn {
      get { return TypedTable.CassetteNumberColumn; }
    }

    /// <summary>Gets or sets the CassetteNumber.</summary>
    [DBDataType(typeof(System.Int32))]
    public virtual Nullable<System.Int32> CassetteNumber {
      get { 
        Nullable<System.Int32> result_;
        if (GetInterceptor<Nullable<System.Int32>>("CassetteNumber", GetCassetteNumberImpl, out result_)) return result_;
        return GetCassetteNumberImpl();
      }
      set { 
          if (!SetInterceptor<Nullable<System.Int32>>("CassetteNumber", value, SetCassetteNumberImpl)) {
            SetCassetteNumberImpl(value);
          }
      }    
    }
    private Nullable<System.Int32> GetCassetteNumberImpl() {
      return (Nullable<System.Int32>) GetColumnValue(CassetteNumberColumn, typeof(System.Int32), true); 
    }
    private void SetCassetteNumberImpl(Nullable<System.Int32> value) {
      SetColumnValue(CassetteNumberColumn, value);
    }
    
    //**************************************
    //* PreDenomination methods
    //**************************************
    /// <summary>Gets the PreDenomination <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn PreDenominationColumn {
      get { return TypedTable.PreDenominationColumn; }
    }

    /// <summary>Gets or sets the PreDenomination.</summary>
    [DBDataType(typeof(System.Int32))]
    public virtual Nullable<System.Int32> PreDenomination {
      get { 
        Nullable<System.Int32> result_;
        if (GetInterceptor<Nullable<System.Int32>>("PreDenomination", GetPreDenominationImpl, out result_)) return result_;
        return GetPreDenominationImpl();
      }
      set { 
          if (!SetInterceptor<Nullable<System.Int32>>("PreDenomination", value, SetPreDenominationImpl)) {
            SetPreDenominationImpl(value);
          }
      }    
    }
    private Nullable<System.Int32> GetPreDenominationImpl() {
      return (Nullable<System.Int32>) GetColumnValue(PreDenominationColumn, typeof(System.Int32), true); 
    }
    private void SetPreDenominationImpl(Nullable<System.Int32> value) {
      SetColumnValue(PreDenominationColumn, value);
    }
    
    //**************************************
    //* PreAmount methods
    //**************************************
    /// <summary>Gets the PreAmount <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn PreAmountColumn {
      get { return TypedTable.PreAmountColumn; }
    }

    /// <summary>Gets or sets the PreAmount.</summary>
    [DBDataType(typeof(System.Decimal))]
    public virtual Nullable<System.Decimal> PreAmount {
      get { 
        Nullable<System.Decimal> result_;
        if (GetInterceptor<Nullable<System.Decimal>>("PreAmount", GetPreAmountImpl, out result_)) return result_;
        return GetPreAmountImpl();
      }
      set { 
          if (!SetInterceptor<Nullable<System.Decimal>>("PreAmount", value, SetPreAmountImpl)) {
            SetPreAmountImpl(value);
          }
      }    
    }
    private Nullable<System.Decimal> GetPreAmountImpl() {
      return (Nullable<System.Decimal>) GetColumnValue(PreAmountColumn, typeof(System.Decimal), true); 
    }
    private void SetPreAmountImpl(Nullable<System.Decimal> value) {
      SetColumnValue(PreAmountColumn, value);
    }
    
    //**************************************
    //* PostDenomination methods
    //**************************************
    /// <summary>Gets the PostDenomination <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn PostDenominationColumn {
      get { return TypedTable.PostDenominationColumn; }
    }

    /// <summary>Gets or sets the PostDenomination.</summary>
    [DBDataType(typeof(System.Int32))]
    public virtual Nullable<System.Int32> PostDenomination {
      get { 
        Nullable<System.Int32> result_;
        if (GetInterceptor<Nullable<System.Int32>>("PostDenomination", GetPostDenominationImpl, out result_)) return result_;
        return GetPostDenominationImpl();
      }
      set { 
          if (!SetInterceptor<Nullable<System.Int32>>("PostDenomination", value, SetPostDenominationImpl)) {
            SetPostDenominationImpl(value);
          }
      }    
    }
    private Nullable<System.Int32> GetPostDenominationImpl() {
      return (Nullable<System.Int32>) GetColumnValue(PostDenominationColumn, typeof(System.Int32), true); 
    }
    private void SetPostDenominationImpl(Nullable<System.Int32> value) {
      SetColumnValue(PostDenominationColumn, value);
    }
    
    //**************************************
    //* PostAmount methods
    //**************************************
    /// <summary>Gets the PostAmount <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn PostAmountColumn {
      get { return TypedTable.PostAmountColumn; }
    }

    /// <summary>Gets or sets the PostAmount.</summary>
    [DBDataType(typeof(System.Decimal))]
    public virtual Nullable<System.Decimal> PostAmount {
      get { 
        Nullable<System.Decimal> result_;
        if (GetInterceptor<Nullable<System.Decimal>>("PostAmount", GetPostAmountImpl, out result_)) return result_;
        return GetPostAmountImpl();
      }
      set { 
          if (!SetInterceptor<Nullable<System.Decimal>>("PostAmount", value, SetPostAmountImpl)) {
            SetPostAmountImpl(value);
          }
      }    
    }
    private Nullable<System.Decimal> GetPostAmountImpl() {
      return (Nullable<System.Decimal>) GetColumnValue(PostAmountColumn, typeof(System.Decimal), true); 
    }
    private void SetPostAmountImpl(Nullable<System.Decimal> value) {
      SetColumnValue(PostAmountColumn, value);
    }
    
    //**************************************
    //* ChangeDate methods
    //**************************************
    /// <summary>Gets the ChangeDate <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn ChangeDateColumn {
      get { return TypedTable.ChangeDateColumn; }
    }

    /// <summary>Gets or sets the ChangeDate.</summary>
    [DBDataType(typeof(System.DateTime))]
    public virtual Nullable<System.DateTime> ChangeDate {
      get { 
        Nullable<System.DateTime> result_;
        if (GetInterceptor<Nullable<System.DateTime>>("ChangeDate", GetChangeDateImpl, out result_)) return result_;
        return GetChangeDateImpl();
      }
      set { 
          if (!SetInterceptor<Nullable<System.DateTime>>("ChangeDate", value, SetChangeDateImpl)) {
            SetChangeDateImpl(value);
          }
      }    
    }
    private Nullable<System.DateTime> GetChangeDateImpl() {
      return (Nullable<System.DateTime>) GetColumnValue(ChangeDateColumn, typeof(System.DateTime), true); 
    }
    private void SetChangeDateImpl(Nullable<System.DateTime> value) {
      SetColumnValue(ChangeDateColumn, value);
    }
    
    //**************************************
    //* RowVersion methods
    //**************************************
    /// <summary>Gets the RowVersion <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn RowVersionColumn {
      get { return TypedTable.RowVersionColumn; }
    }

    /// <summary>Gets or sets the RowVersion.</summary>
    [DBDataType(typeof(System.Int32))]
    public virtual System.Int32 RowVersion {
      get { 
        System.Int32 result_;
        if (GetInterceptor<System.Int32>("RowVersion", GetRowVersionImpl, out result_)) return result_;
        return GetRowVersionImpl();
      }
      set { 
          if (!SetInterceptor<System.Int32>("RowVersion", value, SetRowVersionImpl)) {
            SetRowVersionImpl(value);
          }  
      }    
    }
    private System.Int32 GetRowVersionImpl() {
      return (System.Int32) GetColumnValue(RowVersionColumn, typeof(System.Int32), false); 
    }
    private void SetRowVersionImpl(System.Int32 value) {
      SetColumnValue(RowVersionColumn, value);
    }
    
    //**************************************
    //* ModifiedDate methods
    //**************************************
    /// <summary>Gets the ModifiedDate <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn ModifiedDateColumn {
      get { return TypedTable.ModifiedDateColumn; }
    }

    /// <summary>Gets or sets the ModifiedDate.</summary>
    [DBDataType(typeof(System.DateTime))]
    public virtual System.DateTime ModifiedDate {
      get { 
        System.DateTime result_;
        if (GetInterceptor<System.DateTime>("ModifiedDate", GetModifiedDateImpl, out result_)) return result_;
        return GetModifiedDateImpl();
      }
      set { 
          if (!SetInterceptor<System.DateTime>("ModifiedDate", value, SetModifiedDateImpl)) {
            SetModifiedDateImpl(value);
          }  
      }    
    }
    private System.DateTime GetModifiedDateImpl() {
      return (System.DateTime) GetColumnValue(ModifiedDateColumn, typeof(System.DateTime), false); 
    }
    private void SetModifiedDateImpl(System.DateTime value) {
      SetColumnValue(ModifiedDateColumn, value);
    }
    
    #endregion

  }
  #endregion

  #region EntityPropertyDescriptors.ATMCassetteAuditPropertyDescriptor
  public static partial class EntityPropertyDescriptors {

    /// <summary>
    /// Gets the ATMCassetteAuditPropertyDescriptor for <see cref="ATMCassetteAudit"/>.
    /// </summary>
    public static ATMCassetteAuditPropertyDescriptor ATMCassetteAudit {
      get { return ATMCassetteAuditPropertyDescriptor.Instance; }
    }
    
    /// <summary>
    /// The generated class containing <see cref="ATMCassetteAudit"/> PropertyDescriptors.
    /// </summary>
    public partial class ATMCassetteAuditPropertyDescriptor : BaseEntityPropertyDescriptor {
	
			/// <summary>
      /// Initializes a new instance of the ATMCassetteAuditPropertyDescriptor class. 
      /// </summary>
      /// <param name="pParentDescriptor">The parent <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/></param>
      /// <param name="pPropertyPath">The type of the property value</param>
			public ATMCassetteAuditPropertyDescriptor(AdaptedPropertyDescriptor pParentDescriptor, String pPropertyPath)
        : base(pParentDescriptor, pPropertyPath) {}
		
      /// <summary>
      /// Initializes a new instance of the ATMCassetteAuditPropertyDescriptor class. 
      /// </summary>
      /// <param name="pComponentType">The type of object to which the property is bound</param>
      protected ATMCassetteAuditPropertyDescriptor(Type pComponentType)
        : base(pComponentType) {}

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for ATMCassetteHistoryID.</summary>
      public AdaptedPropertyDescriptor ATMCassetteHistoryID {
        get { return Get("ATMCassetteHistoryID"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for ATMCassetteID.</summary>
      public AdaptedPropertyDescriptor ATMCassetteID {
        get { return Get("ATMCassetteID"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Atmid.</summary>
      public AdaptedPropertyDescriptor Atmid {
        get { return Get("Atmid"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for CassetteNumber.</summary>
      public AdaptedPropertyDescriptor CassetteNumber {
        get { return Get("CassetteNumber"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for PreDenomination.</summary>
      public AdaptedPropertyDescriptor PreDenomination {
        get { return Get("PreDenomination"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for PreAmount.</summary>
      public AdaptedPropertyDescriptor PreAmount {
        get { return Get("PreAmount"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for PostDenomination.</summary>
      public AdaptedPropertyDescriptor PostDenomination {
        get { return Get("PostDenomination"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for PostAmount.</summary>
      public AdaptedPropertyDescriptor PostAmount {
        get { return Get("PostAmount"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for ChangeDate.</summary>
      public AdaptedPropertyDescriptor ChangeDate {
        get { return Get("ChangeDate"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for RowVersion.</summary>
      public AdaptedPropertyDescriptor RowVersion {
        get { return Get("RowVersion"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for ModifiedDate.</summary>
      public AdaptedPropertyDescriptor ModifiedDate {
        get { return Get("ModifiedDate"); }
      }

     internal new static ATMCassetteAuditPropertyDescriptor Instance = new ATMCassetteAuditPropertyDescriptor(typeof(ATMCassetteAudit));
    }
  }
  #endregion
  
}
