﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

using Csla.Validation;

namespace CabRideEngine {
    [Serializable]
    public sealed class AffiliateMapping : AffiliateMappingDataRow {

        #region Constructors -- DO NOT MODIFY
        // Do not create constructors for this class
        // Developers cannot create instances of this class with the "new" keyword
        // You should write a static Create method instead; see the "Suggested Customization" region
        // Please review the documentation to learn about entity creation and inheritance

        // This private constructor prevents accidental instance creation via 'new'
        private AffiliateMapping() : this(null) {
        }

        // Typed DataSet constructor (needed by framework) - DO NOT REMOVE
        public AffiliateMapping(DataRowBuilder pRowBuilder)
            : base(pRowBuilder) {
        }

        #endregion

        #region Suggested Customizations

        //    // Use this factory method template to create new instances of this class
        //    public static AffiliateMapping Create(PersistenceManager pManager,
        //      ... your creation parameters here ...) { 
        //
        //      AffiliateMapping aAffiliateMapping = pManager.CreateEntity<AffiliateMapping>();
        //
        //      // if this object type requires a unique id and you have implemented
        //      // the IIdGenerator interface implement the following line
        //      pManager.GenerateId(aAffiliateMapping, // add id column here //);
        //
        //      // Add custom code here
        //
        //      aAffiliateMapping.AddToManager();
        //      return aAffiliateMapping;
        //    }

        //    // Implement this method if you want your object to sort in a specific order
        //    public override int CompareTo(Object pObject) {
        //    }

        //    // Implement this method to customize the null object version of this class
        //    protected override void UpdateNullEntity() {
        //    }

        #endregion

        // Add additional logic to your business object here...
        public static AffiliateMapping Create(PersistenceManager pPM, long pAffiliateID) {
            AffiliateMapping oMapping = null;

            try {
                oMapping = (AffiliateMapping)pPM.CreateEntity(typeof(AffiliateMapping));

                // Using the IdeaBlade Id Generation technique
                //pPM.GenerateId(oMapping, AffiliateMapping.AffiliateMapIDEntityColumn);

                oMapping.AffiliateID = pAffiliateID;
                oMapping.AddToManager();

            } catch (Exception ex) {
                throw ex;
            }
            return oMapping;
        }

        protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
            if (IsDeserializing)
                return;
            base.OnColumnChanging(pArgs);
            //DoAudit(pArgs);
        }

        private void DoAudit(DataColumnChangeEventArgs pArgs) {
            if (!this.PersistenceManager.IsClient) {
                return;
            }
            if (this.RowState == DataRowState.Added) {
                return;
            }
            if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
                AuditRecord = AffiliateMappingAudit.Create(this.PersistenceManager, this);
            }
            //AuditRecord[pArgs.Column.ColumnName] = pArgs.ProposedValue;
        }

        protected override void AddRules(RuleList pList) {
            PropertyRequiredRule.AddToList(pList, AffiliateMapping.AffiliateIDEntityColumn.ColumnName);
            PropertyRequiredRule.AddToList(pList, AffiliateMapping.SystemEntityColumn.ColumnName);
            //PropertyRequiredRule.AddToList(pList, AffiliateMapping.OtherIDEntityColumn.ColumnName);
            //pList.Add(IsUnique, AffiliateMapping.OtherIDEntityColumn.ColumnName);
        }

        private bool IsUnique(object pTarget, RuleArgs e) {
            AffiliateMapping oMapping = (AffiliateMapping)pTarget;

            RdbQuery oQry = new RdbQuery(typeof(AffiliateMapping), AffiliateMapping.AffiliateIDEntityColumn, EntityQueryOp.EQ, oMapping.AffiliateID);
            oQry.AddClause(AffiliateMapping.OtherIDEntityColumn, EntityQueryOp.EQ, oMapping.OtherID);
            oQry.AddClause(AffiliateMapping.SystemEntityColumn, EntityQueryOp.EQ, oMapping.System);
            oQry.AddClause(AffiliateMapping.AffiliateMapIDEntityColumn, EntityQueryOp.NE, oMapping.AffiliateMapID);
            EntityList<AffiliateMapping> oMappings = this.PersistenceManager.GetEntities<AffiliateMapping>(oQry);
            if (oMappings.Count > 0) {
                e.Description = "Affiliate Mapping must be unique!";
                return false;
            }
            return true;
        }

    }

}
