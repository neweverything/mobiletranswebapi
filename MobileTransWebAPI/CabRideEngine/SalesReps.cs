﻿using Csla.Validation;
using IdeaBlade.Persistence;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;
using System;
using System.Data;
using TaxiPassCommon;

namespace CabRideEngine {
	/// <summary>
	/// The developer class representing the SalesReps business object class.
	/// </summary>
	/// <remarks>
	/// <para>Place all application-specific business logic in this class.</para>
	/// <para>
	/// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
	/// and will not overwrite changes.
	/// </para>
	/// <para>
	/// This class inherits from the generated class, its companion in the pair of classes 
	/// defining a business object. Properties in the generated class may be overridden in 
	/// order to change the behavior or add business logic.
	/// </para>
	/// <para>
	/// This class is the final class in the inheritance chain for this business object. 
	/// It should not be inherited.
	/// </para>
	/// </remarks>
	[Serializable]
	public sealed partial class SalesReps : SalesRepsDataRow {

		#region Constructors -- DO NOT MODIFY

		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new';
		// it is also required if the class is visible from a web service.
		// DO NOT REMOVE **
		private SalesReps()
			: this(null) {
		}

		/// <summary>
		/// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
		/// </summary>
		/// <param name="pRowBuilder"></param>
		public SalesReps(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static SalesReps Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      SalesReps aSalesReps = pManager.CreateEntity<SalesReps>();
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aSalesReps, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aSalesReps.AddToManager();
		//      return aSalesReps;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		// Add additional logic to your business object here...
		public static SalesReps Create(PersistenceManager pPM) {
			SalesReps anEntity = null;

			try {
				anEntity = pPM.CreateEntity<SalesReps>();

				// Using the IdeaBlade Id Generation technique
				//pPM.GenerateId(anEntity, SalesRepIDEntityColumn);
				anEntity.Country = Countries.GetDefaultCountry(pPM).Country;
				anEntity.AddToManager();

			} catch (Exception ex) {
				throw ex;
			}
			return anEntity;
		}

		protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
			if (IsDeserializing) {
				return;
			}
			base.OnColumnChanging(pArgs);
			//DoAudit(pArgs);
		}

		private void DoAudit(DataColumnChangeEventArgs pArgs) {
			if (!this.PersistenceManager.IsClient) {
				return;
			}
			if (this.RowState == DataRowState.Added) {
				return;
			}
			if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
				AuditRecord = SalesRepsAudit.Create(this.PersistenceManager, this);
			}
		}

		[BindingBrowsable(false)]
		public override string EntityInstanceDisplayName {
			get {
				return this.Name;
			}
		}

		protected override void AddRules(RuleList pList) {
			PropertyRequiredRule.AddToList(pList, NameEntityColumn.ColumnName);
			PropertyRequiredRule.AddToList(pList, CellEntityColumn.ColumnName);
			pList.Add(IsNameUnique, NameEntityColumn.ColumnName);
			pList.Add(IsEMailUnique, EMailEntityColumn.ColumnName);
		}

		private bool IsNameUnique(object pTarget, RuleArgs e) {
			SalesReps oRecord = pTarget as SalesReps;

			RdbQuery qry = new RdbQuery(typeof(SalesReps), NameEntityColumn, EntityQueryOp.EQ, oRecord.Name);
			qry.AddClause(SalesRepIDEntityColumn, EntityQueryOp.NE, oRecord.SalesRepID);
			EntityList<SalesReps> oList = this.PersistenceManager.GetEntities<SalesReps>(qry);
			if (oList.Count > 0) {
				e.Description = "A Sales Rep already exists with the name: " + oRecord.Name;
				return false;
			}
			return true;
		}

		private bool IsEMailUnique(object pTarget, RuleArgs e) {
			SalesReps oRecord = pTarget as SalesReps;

			RdbQuery qry = new RdbQuery(typeof(SalesReps), EMailEntityColumn, EntityQueryOp.EQ, oRecord.EMail);
			qry.AddClause(SalesRepIDEntityColumn, EntityQueryOp.NE, oRecord.SalesRepID);
			EntityList<SalesReps> oList = this.PersistenceManager.GetEntities<SalesReps>(qry);
			if (oList.Count > 0) {
				e.Description = "A Sales Rep already exists with the email: " + oRecord.Name;
				return false;
			}
			return true;
		}

		public static EntityList<SalesReps> GetAll(PersistenceManager pPM) {
			EntityList<SalesReps> oList = pPM.GetEntities<SalesReps>();
			oList.ApplySort(SalesReps.NameEntityColumn.ColumnName, System.ComponentModel.ListSortDirection.Ascending, false);
			return oList;
		}

		public static SalesReps GetSalesRep(PersistenceManager pPM, string pEMail) {
			RdbQuery qry = new RdbQuery(typeof(SalesReps), EMailEntityColumn, EntityQueryOp.EQ, pEMail);
			return pPM.GetEntity<SalesReps>(qry);
		}

		public AffiliateDrivers MyDriver {
			get {
				AffiliateDrivers rec = AffiliateDrivers.GetDriverByCallInCell(this.PersistenceManager, this.Cell);
				return rec;
			}
		}

		public string CellFormatted {
			get {
				return Cell.FormattedPhoneNumber();
			}
		}
	}

	#region EntityPropertyDescriptors.SalesRepsPropertyDescriptor
	////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
	////    For example: 
	////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
	//public static partial class EntityPropertyDescriptors {
	//	public partial class SalesRepsPropertyDescriptor : BaseEntityPropertyDescriptor {
	//    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
	//    public AdaptedPropertyDescriptor Age {
	//      get { return this.Get("Age"); }
	//    }
	//  }
	//} 
	#endregion

}
