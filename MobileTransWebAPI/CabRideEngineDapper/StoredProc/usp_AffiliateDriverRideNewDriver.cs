﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

using Dapper;
using System.Data;
using CabRideEngineDapper.Utils;

namespace CabRideEngineDapper {
	public class usp_AffiliateDriverRideNewDriver {
		public long AffiliateID { get; set; }
		public long AffiliateDriverID { get; set; }
		public long DriverID { get; set; }
		public decimal DriverMaxGratuity { get; set; }
		public decimal ChargeMaxAmount { get; set; }
		public bool NewDriver { get; set; }
		public string Message { get; set; }
		public string ErrorMsg { get; set; }
		public decimal Discount { get; set; }
		public decimal StartAmount { get; set; }
		public decimal EndAmount { get; set; }
		public decimal Fee { get; set; }

		public static List<usp_AffiliateDriverRideNewDriver> Execute(string pCell, long pCustomerDeviceID, string pModifiedBy) {
			using (var conn = SqlHelper.OpenSqlConnection()) {
				return Execute(conn, pCell, pCustomerDeviceID, pModifiedBy);
			}
		}

		public static List<usp_AffiliateDriverRideNewDriver> Execute(SqlConnection pConn, string pCell, long pCustomerDeviceID, string pModifiedBy) {
			DynamicParameters param = new DynamicParameters();
			param.Add("Cell", pCell);
			param.Add("CustomerDeviceID", pCustomerDeviceID);
			param.Add("ModifiedBy", pModifiedBy);
			return pConn.Query<usp_AffiliateDriverRideNewDriver>("usp_AffiliateDriverRideNewDriver", param, commandType: CommandType.StoredProcedure).ToList();
		}
	}
}
