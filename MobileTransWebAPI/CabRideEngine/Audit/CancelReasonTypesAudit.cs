﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Util;

namespace CabRideEngine {
    [Serializable]
    public sealed class CancelReasonTypesAudit : CancelReasonTypesAuditDataRow {

        #region Constructors -- DO NOT MODIFY
        // Do not create constructors for this class
        // Developers cannot create instances of this class with the "new" keyword
        // You should write a static Create method instead; see the "Suggested Customization" region
        // Please review the documentation to learn about entity creation and inheritance

        // This private constructor prevents accidental instance creation via 'new'
        private CancelReasonTypesAudit()
            : this(null) {
        }

        // Typed DataSet constructor (needed by framework) - DO NOT REMOVE
        public CancelReasonTypesAudit(DataRowBuilder pRowBuilder)
            : base(pRowBuilder) {
        }

        #endregion

        #region Suggested Customizations

        //    // Use this factory method template to create new instances of this class
        //    public static CancelReasonTypesAudit Create(PersistenceManager pManager,
        //      ... your creation parameters here ...) { 
        //
        //      CancelReasonTypesAudit aCancelReasonTypesAudit = pManager.CreateEntity<CancelReasonTypesAudit>();
        //
        //      // if this object type requires a unique id and you have implemented
        //      // the IIdGenerator interface implement the following line
        //      pManager.GenerateId(aCancelReasonTypesAudit, // add id column here //);
        //
        //      // Add custom code here
        //
        //      aCancelReasonTypesAudit.AddToManager();
        //      return aCancelReasonTypesAudit;
        //    }

        //    // Implement this method if you want your object to sort in a specific order
        //    public override int CompareTo(Object pObject) {
        //    }

        //    // Implement this method to customize the null object version of this class
        //    protected override void UpdateNullEntity() {
        //    }

        #endregion

        // Add additional logic to your business object here...

        public static CancelReasonTypesAudit Create(PersistenceManager pManager, CancelReasonTypes pCancelReasonType) {
            CancelReasonTypesAudit oReason = (CancelReasonTypesAudit)pManager.CreateEntity(typeof(CancelReasonTypesAudit));
            //pManager.GenerateId(oReason, CancelReasonTypesAudit.CancelReasonTypeHistoryIDEntityColumn);

            for (int i = 0; i < pCancelReasonType.Table.Columns.Count; i++) {
                if (oReason.Table.Columns.Contains(pCancelReasonType.Table.Columns[i].ColumnName)) {
                    oReason[pCancelReasonType.Table.Columns[i].ColumnName] = pCancelReasonType[i];
                }
            }
            oReason.AddToManager();

            return oReason;
        }
    }

}
