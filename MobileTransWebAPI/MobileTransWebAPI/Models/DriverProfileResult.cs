﻿using System;

namespace MobileTransWebAPI.Models {
	public class DriverProfileResult {
		public string ErrorMsg { get; set; }
		public bool Ok { get; set; }


		public long AffiliateID { get; set; }
		public long DriverID { get; set; }
		public long AffiliateDriverID { get; set; }

		public string DriverName { get; set; }
		public string FleetName { get; set; }

		public string Street { get; set; }
		public string City { get; set; }
		public string State { get; set; }

		public string VehicleNo { get; set; }
		public string PIN { get; set; }

		public string DriverInfo { get; set; }
		public string SSN { get; set; }

		public string FirstName { get; set; }
		public string LastName { get; set; }
		public DateTime? BirthDate { get; set; }
	}
}