﻿using CabRideEngineDapper.Utils;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using TaxiPassCommon;


namespace CabRideEngineDapper {
    public partial class ACHDetail {

        private DriverPayments mDriverPayment;
        public DriverPayments DriverPayment {
            get {
                if (mDriverPayment == null) {
                    using (var conn = SqlHelper.OpenSqlConnection()) {
                        mDriverPayment = DriverPayments.GetPayment(conn, this.DriverPaymentID);
                    }
                }
                return mDriverPayment;
            }
        }


        public static ACHDetail Create(DriverPayments pPayment) {
            ACHDetail rec = ACHDetailTable.Create();
            rec.DriverPaymentID = pPayment.DriverPaymentID;
            pPayment.ACHDetail = rec;
            return rec;
        }

        public static ACHDetail Create(long pDriverPaymentID) {
            ACHDetail rec = ACHDetailTable.Create();
            rec.DriverPaymentID = pDriverPaymentID;
            return rec;
        }

        public static ACHDetail GetByDriverPaymentID(long pDriverPaymentID) {
            using (var conn = SqlHelper.OpenSqlConnection()) {
                return GetByDriverPaymentID(conn, pDriverPaymentID);
            }
        }

        public static ACHDetail GetByDriverPaymentID(SqlConnection pConn, long pDriverPaymentID) {
            string sql = "SELECT * FROM ACHDetail WHERE DriverPaymentID = @DriverPaymentID";
            return pConn.Query<ACHDetail>(sql, new { DriverPaymentID = pDriverPaymentID }).DefaultIfEmpty(ACHDetail.GetNullEntity()).FirstOrDefault();
        }


        public static List<long> GetDistinctBatchNumbersForTransCardAlreadyBatched(string pTransCardAdminNo, DateTime pStart, DateTime pEnd) {
            if (pTransCardAdminNo.IsNullOrEmpty()) {
                return new List<long>();
            }

            string sql = @"SELECT DISTINCT ISNULL(ACHBatchNumber, 0)
                            FROM    ACHDetail
                            WHERE   TransCardAdminNo = @TransCardAdminNo
                                    AND TransCardAdminNo IS NOT NULL
                                    AND ACHErrorReason != 'Batch Required'
                                    AND ACHPaidDate >= @StartDate
                                    AND ACHPaidDate <= @EndDate";

            using (var conn = SqlHelper.OpenSqlConnection()) {
                var results = conn.Query<long>(sql, new { TransCardAdminNo = pTransCardAdminNo, StartDate = pStart, EndDate = pEnd }).ToList();
                return results.Where(p => p != 0).ToList();

            }
        }

        public static List<long> GetDistinctBatchNumbersForTransCardAlreadyBatched(AffiliateDrivers pDriver, DateTime pStart, DateTime pEnd) {
            if (pDriver.TransCardAdminNo.IsNullOrEmpty()) {
                return new List<long>();
            }

            string sql = @"SELECT DISTINCT ISNULL(ACHBatchNumber, 0) ACHBatchNumber
                            FROM    ACHDetail
                            WHERE   TransCardAdminNo = @TransCardAdminNo
                                    AND TransCardAdminNo IS NOT NULL
                                    AND ACHErrorReason != 'Batch Required'
                                    AND ACHPaidDate >= @Start
                                    AND ACHPaidDate <= @End";

            using (var conn = SqlHelper.OpenSqlConnection()) {
                return conn.Query<long>(sql, new { TransCardAdminNo = pDriver.TransCardAdminNo, Start = pStart.Date, End = pEnd }).ToList();
            }
        }


        public override int Save() {
            if (IsNewRecord) {
                this.ACHDetailID = NextID.GetNextKey(ACHDetail.TableDef.TABLE_NAME);
            }
            return base.Save();
        }
        public override int Save(SqlConnection pConn, string pModifiedBy) {
            if (IsNewRecord) {
                this.ACHDetailID = NextID.GetNextKey(ACHDetail.TableDef.TABLE_NAME);
            }

            return base.Save(pConn, pModifiedBy);
        }


        public static List<ACHDetail> GetTransCardsWaitingToBatch(SqlConnection pConn) {
            string sql = @"SELECT  * FROM ACHDetail
                            LEFT JOIN DriverPayments ON DriverPayments.DriverPaymentID = ACHDetail.DriverPaymentID
                            WHERE   ACHErrorReason = 'Batch Required'
                            ORDER BY TransCardAdminNo";

            return pConn.Query<ACHDetail, DriverPayments, ACHDetail>(sql
                        , (ach, dp) => { ach.mDriverPayment = dp; return ach; }
                        , splitOn: "DriverPaymentID").ToList();

        }

        public static List<ACHDetail> GetTransCardsWaitingToBatch(SqlConnection pConn, string pTransCardAdminNo) {
            string sql = @"SELECT  * FROM ACHDetail
                            LEFT JOIN DriverPayments ON DriverPayments.DriverPaymentID = ACHDetail.DriverPaymentID
                            WHERE   ACHErrorReason = 'Batch Required'
                                    AND TransCardAdminNo = @TransCardAdminNo";
            return pConn.Query<ACHDetail, DriverPayments, ACHDetail>(sql
                        , (ach, dp) => { ach.mDriverPayment = dp; return ach; }
                        , new { TransCardAdminNo = pTransCardAdminNo }
                        , splitOn: "DriverPaymentID").ToList();
        }



    }
}
