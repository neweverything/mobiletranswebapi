﻿namespace TaxiPassCommon.Banking {
    public class ProPayResponse {

        public Response XMLResponse { get; set; }

        public class XMLTrans {
            public string transType { get; set; }
            public string status { get; set; }
            public string accountNum { get; set; }
            public string invNum { get; set; }
            public string transNum { get; set; }
            public string authCode { get; set; }
            public string AVS { get; set; }
            public string CVV2Resp { get; set; }
            public string responseCode { get; set; }
            public string NetAmt { get; set; }
            public string GrossAmt { get; set; }
            public string GrossAmtLessNetAmt { get; set; }
            public string PerTransFee { get; set; }
            public string Rate { get; set; }

        }

        public class Response {
            public XMLTrans XMLTrans { get; set; }
        }


    }
}
