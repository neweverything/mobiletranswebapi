﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TaxiPassCommon.Banking {
	public interface IGatewayCredentials {
		CardProcessors MyGateway();

		int ProcessingOrder { get; set; }
		string TerminalType { get; set; }
		
	}
}
