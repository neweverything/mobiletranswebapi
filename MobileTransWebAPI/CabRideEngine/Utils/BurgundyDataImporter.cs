using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Text;
using System.IO;

using System.ComponentModel;

using IdeaBlade.Persistence;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

using CabRideEngine;

using TaxiPassCommon;
using TaxiPassCommon.GeoCoding;

namespace CabRideEngine.Utils {
	public class BurgundyDataImporter {
		private string mTextData;

		private PersistenceManager mPM;
		private EntityList<States> mStates;
		private EntityList<Countries> mCountries;
		private EntityList<Airports> mAirports;

		private DataTable mTable;

		private string mIssues = "";
		private string mFileName;

		public BurgundyDataImporter(string pBurgundyData, PersistenceManager pPM) {
			mPM = pPM;
			mTextData = pBurgundyData;
			Text2DB();
			PopulateTables();
		}


		public BurgundyDataImporter(PersistenceManager pPM, string pURL, bool pSaveToDisk) {
			mPM = pPM;
			mFileName = pURL;

			// load up for speed performance
			mStates = mPM.GetEntities<States>();
			mCountries = mPM.GetEntities<Countries>();
			mAirports = mPM.GetEntities<Airports>();

			WebCrawler oSpider = new WebCrawler();
			mTextData = oSpider.GetTextData(pURL);
			Text2DB();
			if (pSaveToDisk) {
				File.AppendAllText(pURL.Substring(pURL.LastIndexOf('/')), mTextData);
			} else {
				PopulateTables();
			}
		}

		private void Text2DB() {

			if (mTextData.Length == 0) {
				return;
			}
			char[] aDelimiters = new char[] { '\n', '\r' };
			string[] aText = mTextData.Split(aDelimiters, StringSplitOptions.RemoveEmptyEntries);
			string[] aHeaders = aText[0].Split('|');

			mTable = new DataTable("Burgundy");
			for (int i = 0; i < aHeaders.Length; i++) {
				mTable.Columns.Add(aHeaders[i], typeof(String));
			}
			for (int i = 1; i < aText.Length; i++) {
				string[] aDataRow = aText[i].Split('|');
				DataRow oRow = mTable.NewRow();
				for (int j = 0; j < aHeaders.Length; j++) {
					oRow[aHeaders[j]] = aDataRow[j].ToString();
				}
				mTable.Rows.Add(oRow);
			}

		}

		private void PopulateTables() {


			for (int i = 0; i < mTable.Rows.Count; i++) {
				// locate or add new customer
				//bool bNewRecord = false;
				DataRow oRow = mTable.Rows[i];
				string sPhone = oRow["ContactNumber"].ToString();

				RdbQuery oQry = new RdbQuery(typeof(Customer), Customer.HomePhoneEntityColumn, EntityQueryOp.EQ, sPhone);
				oQry.AddOperator(EntityBooleanOp.Or);
				oQry.AddClause(Customer.WorkPhoneEntityColumn, EntityQueryOp.EQ, sPhone);
				oQry.AddOperator(EntityBooleanOp.Or);
				oQry.AddClause(Customer.CellPhoneEntityColumn, EntityQueryOp.EQ, sPhone);
				oQry.AddOperator(EntityBooleanOp.Or);
				oQry.AddClause(Customer.EMailEntityColumn, EntityQueryOp.EQ, oRow["EMail"].ToString());
				oQry.AddOperator(EntityBooleanOp.Or);
				Customer oCust = mPM.GetEntity<Customer>(oQry);
				if (oCust.IsNullEntity) {
					//bNewRecord = true;
					oCust = Customer.Create(mPM);
					oCust.HomePhone = sPhone;
					oCust.Name = oRow["Contact"].ToString();
				}
				oCust.EMail = oRow["EMail"].ToString();
				oCust.CostCenter = oRow["CostCenter"].ToString();
				oCust.AccountNo = oRow["AccountNumber"].ToString();
				oCust.Street = oRow["Building"].ToString() + " " + oRow["Street"].ToString();
				oCust.City = oRow["Town"].ToString();
				oCust.Country = oRow["Country"].ToString();
				oCust.ZIPCode = oRow["PostCode"].ToString();
				try {
					mPM.SaveChanges(mPM.GetEntities<Customer>(DataRowState.Added | DataRowState.Modified));
				} catch (Exception ex) {
					throw ex;
				}

				//capture card info for the customer
				string sCardNo = oRow["CardNumber"].ToString();
				CustomerCards oCard = null;
				if (sCardNo.Length > 0) {
					oQry = new RdbQuery(typeof(CustomerCards), CustomerCards.CustomerIDEntityColumn, EntityQueryOp.EQ, oCust.CustomerID);
					oQry.AddClause(CustomerCards.NumberEntityColumn, EntityQueryOp.EQ, CryptoFns.SimpleDESEncrypt(sCardNo, oCust.Salt));
					oCard = mPM.GetEntity<CustomerCards>(oQry);
					if (oCard.IsNullEntity) {
						oCard = CustomerCards.Create(mPM, oCust.CustomerID);
					}
					try {
						oCard.Number = sCardNo;
						oCard.CardType = CardTypes.GetCardType(mPM, sCardNo);
					} catch {
					}
					oCard.Expiration = new DateTime(Convert.ToInt16(oRow["CardExpiryYear"]), Convert.ToInt16(oRow["CardExpiryMonth"]), 1);
					oCard.Name = oCust.Name;
				}
				try {
					mPM.SaveChanges(mPM.GetEntities<CustomerCards>(DataRowState.Added | DataRowState.Modified));
				} catch (PersistenceManagerSaveException ex) {
					throw ex;
				}


				//capture reservation info
				oQry = new RdbQuery(typeof(Reservations), Reservations.ExternalReferenceIDEntityColumn, EntityQueryOp.EQ, oRow["JobID"]);
				oQry.AddClause(Reservations.SalesSystemEntityColumn, EntityQueryOp.EQ, oRow["BrandName"].ToString());
				Reservations reserve = mPM.GetEntity<Reservations>(oQry);
				if (reserve.IsNullEntity) {
					reserve = Reservations.Create(mPM, oCust.CustomerID);
					reserve.ExternalReferenceID = oRow["JobID"].ToString();
					reserve.SalesSystem = oRow["BrandName"].ToString();
				}
				reserve.BillTo = oCust.BillTo;
				if (reserve.BillTo == 0) {
					reserve.BillTo = reserve.CustomerID;
				}

				RunTypes oRunType = RunTypes.GetRunType(mPM, oRow["JourneyType"].ToString(), true);
				reserve.RunType = oRunType.RunType;

				ServiceTypes serviceType = ServiceTypes.GetServiceType(mPM, oRow["ServiceType"].ToString(), true);
				reserve.ServiceType = serviceType.ServiceType;

				reserve.AccountNo = oRow["AccountNumber"].ToString();
				reserve.Name = oRow["Passenger"].ToString();
				DateTimeConverter oConverter = new DateTimeConverter();
				DateTime date = ConvertToDate(oRow["PUDate"].ToString());
				DateTime oDate = (DateTime)oConverter.ConvertFromString(date.ToShortDateString() + " " + oRow["PUTime"].ToString());
				reserve.PickUpDate = oDate;
				reserve.Reference = oRow["Reference"].ToString();
				reserve.EstMileage = Convert.ToInt16(oRow["Mileage"]);
				reserve.ExternalCharges = true;
				reserve.CardNumber = sCardNo;
				reserve.CardHolder = oCard.Name;
				reserve.CardType = oCard.CardType;

				//find the affiliate based on the name
				string sCircuitName = oRow["CircuitName"].ToString();
				if (sCircuitName.Length != 0) {
					oQry = new RdbQuery(typeof(AffiliateMapping), AffiliateMapping.NameEntityColumn, EntityQueryOp.EQ, sCircuitName);
					oQry.AddClause(AffiliateMapping.SystemEntityColumn, EntityQueryOp.EQ, reserve.SalesSystem);
					AffiliateMapping oMap = mPM.GetEntity<AffiliateMapping>(oQry);
					if (oMap.IsNullEntity) {
						mIssues += "Could not map " + sCircuitName + " from external system " + reserve.SalesSystem + " to an affiliate for reservation: " + reserve.ReservationID + "\n";
					} else {
						reserve.AffiliateID = oMap.AffiliateID;
					}
				}
				reserve.Vehicle = oRow["CarNumber"].ToString();
				mPM.SaveChanges(mPM.GetEntities<Reservations>(DataRowState.Added | DataRowState.Modified));


				//capture routing info
				short iRoutingOrder = 1;
				oQry = new RdbQuery(typeof(Routing), Routing.ReservationIDEntityColumn, EntityQueryOp.EQ, reserve.ReservationID);
				oQry.AddClause(Routing.TypeEntityColumn, EntityQueryOp.EQ, Routing_Types.PickUp);
				Routing oRouting = mPM.GetEntity<Routing>(oQry);
				if (oRouting.IsNullEntity) {
					oRouting = Routing.Create(mPM, reserve.ReservationID);
				}
				oRouting.Type = "P";
				oRouting.RoutingOrder = iRoutingOrder;
				iRoutingOrder++;
				if (oRow["PUTown"].ToString().Contains("APT (")) {
					//airport run
					string sAirport = oRow["PUTown"].ToString().Split('(')[1].Substring(0, 3);
					oRouting.RoutingType = RoutingTypes.Airport;
					oRouting.Arrival = true;
					oRouting.Terminal = oRow["PUAddress"].ToString();
					oRouting.Airport = sAirport;
					Airports oAirport = Airports.GetAirport(mPM, sAirport);
					oRouting.Latitude = oAirport.Latitude;
					oRouting.Longitude = oAirport.Longitude;
				} else {
					oRouting.RoutingType = RoutingTypes.Address;
					oRouting.Street = oRow["PUAddress"].ToString();
					oRouting.City = oRow["PUTown"].ToString();
					oRouting.State = mStates[0].FindStateCode(oRow["PUState"].ToString());
					oRouting.Country = mCountries[0].FindCountryCode(oRow["PUCountry"].ToString());
					oRouting.ZIPCode = oRow["PUZip"].ToString();
					Address oAddr = new Address();
					oAddr.Street = oRouting.Street;
					oAddr.City = oRouting.City;
					oAddr.State = oRouting.State;
					oAddr.Country = oRouting.Country;
					oAddr.ZIP = oRouting.ZIPCode;
					SystemDefaults oDefaults = SystemDefaults.GetDefaults(mPM);
					MapPointService oMapPoint = new MapPointService(oDefaults.MapPointUserName, oDefaults.MapPointPassword);
					if (oMapPoint.FindAddress(oAddr)) {
						oRouting.Latitude = oAddr.Latitude;
						oRouting.Longitude = oAddr.Longitude;
					}
				}

				if (oRow["Stop1"].ToString().Trim().Length > 0) {
					mIssues += "Need to handle stops, see External JobID " + reserve.ExternalReferenceID + " in file " + mFileName + "\n";
				}

				if (oRow["Stop2"].ToString().Trim().Length > 0) {
					mIssues += "Need to handle stops, see External JobID " + reserve.ExternalReferenceID + " in file " + mFileName + "\n";
				}

				if (oRow["Stop2"].ToString().Trim().Length > 0) {
					mIssues += "Need to handle stops, see External JobID " + reserve.ExternalReferenceID + " in file " + mFileName + "\n";
				}

				oQry = new RdbQuery(typeof(Routing), Routing.ReservationIDEntityColumn, EntityQueryOp.EQ, reserve.ReservationID);
				oQry.AddClause(Routing.TypeEntityColumn, EntityQueryOp.EQ, Routing_Types.DropOff);
				Routing oDORouting = mPM.GetEntity<Routing>(oQry);
				if (oDORouting.IsNullEntity) {
					oDORouting = Routing.Create(mPM, reserve.ReservationID);
				}
				oDORouting.Type = "D";
				oDORouting.RoutingOrder = iRoutingOrder;
				if (oRow["DropTown"].ToString().Contains("APT (")) {
					//airport run
					string sAirport = oRow["DropTown"].ToString().Split('(')[1].Substring(0, 3);
					oDORouting.RoutingType = RoutingTypes.Airport;
					oDORouting.Arrival = false;
					oDORouting.Terminal = oRow["DropAddress"].ToString();
					oDORouting.Airport = sAirport;
					Airports oAirport = Airports.GetAirport(mPM, sAirport);
					oDORouting.Latitude = oAirport.Latitude;
					oDORouting.Longitude = oAirport.Longitude;
				} else {
					oDORouting.RoutingType = RoutingTypes.Address;
					oDORouting.Street = oRow["DropAddress"].ToString();
					oDORouting.City = oRow["DropTown"].ToString();
					oDORouting.State = mStates[0].FindStateCode(oRow["DropState"].ToString());
					oDORouting.Country = mCountries[0].FindCountryCode(oRow["DropCountry"].ToString());
					oDORouting.ZIPCode = oRow["DropZip"].ToString();
					Address oAddr = new Address();
					oAddr.Street = oDORouting.Street;
					oAddr.City = oDORouting.City;
					oAddr.State = oDORouting.State;
					oAddr.Country = oDORouting.Country;
					oAddr.ZIP = oDORouting.ZIPCode;
					SystemDefaults oDefaults = SystemDefaults.GetDefaults(mPM);
					MapPointService oMapPoint = new MapPointService(oDefaults.MapPointUserName, oDefaults.MapPointPassword);
					if (oMapPoint.FindAddress(oAddr)) {
						oDORouting.Latitude = oAddr.Latitude;
						oDORouting.Longitude = oAddr.Longitude;
					}
				}
				mPM.SaveChanges(mPM.GetEntities<Routing>(DataRowState.Added | DataRowState.Modified));


				//capture charges
				AddCharge(mPM, reserve.ReservationID, ChargeTypes.BaseFee, Convert.ToDecimal(oRow["CustBaseAmount"]), 1);
				AddCharge(mPM, reserve.ReservationID, ChargeTypes.Gratuity, Convert.ToDecimal(oRow["CustGratAmount"]), 2);
				AddCharge(mPM, reserve.ReservationID, ChargeTypes.Discount, Convert.ToDecimal(oRow["CustDiscAmount"]), 3);
				mPM.SaveChanges(mPM.GetEntities<Charges>(DataRowState.Added | DataRowState.Modified));

			}

		}

		private void AddCharge(PersistenceManager pPM, long pReserveID, string pDescription, decimal pAmount, byte pDisplayOrder) {
			RdbQuery oQry = new RdbQuery(typeof(Charges), Charges.DescriptionEntityColumn, EntityQueryOp.EQ, pDescription);
			oQry.AddClause(Charges.ReservationIDEntityColumn, EntityQueryOp.EQ, pReserveID);
			Charges oCharge = mPM.GetEntity<Charges>(oQry);
			if (oCharge.IsNullEntity) {
				oCharge = Charges.Create(pPM, pReserveID);
			}
			oCharge.Description = pDescription;
			oCharge.Amount = pAmount;
			oCharge.DisplayOrder = pDisplayOrder;
			oCharge.ExternalCalc = true;
		}

		private DateTime ConvertToDate(string pDate) {
			// date is in dd/mm/yy format
			int iDay = Convert.ToInt16(pDate.Substring(0, 2));
			int iMonth = Convert.ToInt16(pDate.Substring(3, 2));
			int iYear = Convert.ToInt16(pDate.Substring(6));
			if (iYear < 2000) {
				iYear += 2000;
			}
			return new DateTime(iYear, iMonth, iDay);
		}

	}
}
