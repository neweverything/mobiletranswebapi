﻿using CabRideEngineDapper.Utils;
using Dapper;
using System.Collections.Generic;
using System.Linq;

namespace CabRideEngineDapper {

	public partial class InvoiceLineItem {


		public static InvoiceLineItem GetOrCreate(long pInvoiceID, string pDescription) {
			string sql = @"SELECT *
							FROM InvoiceLineItem
							WHERE InvoiceID = @InvoiceID
								  AND Description = @Description";

			using (var conn = SqlHelper.OpenSqlConnection()) {
				InvoiceLineItem rec = conn.Get<InvoiceLineItem>(sql, new { InvoiceID = pInvoiceID, Description = pDescription });
				if (rec.IsNullEntity) {
					rec = InvoiceLineItem.Create();
					rec.InvoiceID = pInvoiceID;
					rec.Description = pDescription;
				}
				return rec;
			}
		}

		public static InvoiceLineItem GetOrCreate(long pInvoiceID, long pCustomFieldID) {
			string sql = @"SELECT *
							FROM InvoiceLineItem
							WHERE InvoiceID = @InvoiceID
								  AND AccountCustomFieldID = @AccountCustomFieldID";

			using (var conn = SqlHelper.OpenSqlConnection()) {
				InvoiceLineItem rec = conn.Get<InvoiceLineItem>(sql, new { InvoiceID = pInvoiceID, AccountCustomFieldID = pCustomFieldID });
				if (rec.IsNullEntity) {
					rec = InvoiceLineItem.Create();
					rec.InvoiceID = pInvoiceID;
					rec.AccountCustomFieldID = pCustomFieldID;
				}
				return rec;
			}
		}

		public static List<InvoiceLineItem> GetLineItems(long pInvoiceID) {
			using (var conn = SqlHelper.OpenSqlConnection()) {
				string sql = @"SELECT * FROM InvoiceLineItem
								WHERE InvoiceID = @InvoiceID
								ORDER BY InvoiceLineItemID";
				return conn.Query<InvoiceLineItem>(sql, new { InvoiceID = pInvoiceID }).ToList();
			}
		}

		public static int DeleteExtraLineItems(long pInvoiceID, long pAccountID) {
			string sql = @"DELETE FROM InvoiceLineItem
							WHERE InvoiceID = @InvoiceID
								  AND AccountCustomFieldID IS NOT NULL 
								  AND AccountCustomFieldID NOT IN (	  SELECT AccountCustomFieldID
																	  FROM AccountCustomField
																	  WHERE AccountID = @AccountID )";
			using (var conn = SqlHelper.OpenSqlConnection()) {
				return conn.Execute(sql, new { InvoiceID = pInvoiceID, AccountID = pAccountID });
			}
		}
	}
}