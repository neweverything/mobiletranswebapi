﻿using Dapper;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace CabRideEngineDapper {
	public partial class USBankRequest {

		public static List<USBankRequest> GetOutstandingRequests(SqlConnection pConn, string pPayCardType) {
			string sql = @"SELECT *
							FROM USBankRequest
							WHERE Completed = 0 AND PayCardType = @PayCardType
							ORDER BY CreatedDate";


			return pConn.Query<USBankRequest>(sql, new { PayCardType = pPayCardType }).ToList();
		}

	}
}
