﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Runtime.Serialization;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

namespace CabRideEngine {
 
  #region RedeemerCashTransactionsAuditDataTable
  /// <summary>
  /// The generated <see cref="T:IdeaBlade.Persistence.EntityTable"/> class  
  /// containing all <see cref="RedeemerCashTransactionsAudit"/> objects.
  /// </summary>
  /// <remarks>
  /// ****************************************************
  /// <para>** DO NOT MODIFY THIS CLASS - AutoGenerated Code</para>
  /// <para>** Any changes made to this code will be lost</para>
  /// <para>** the next time this code is regenerated.</para>
  /// <para>**</para>
  /// <para>** Generated from:   EntityBaseclass.template</para>
  /// <para>** Last modified:    2011-05-11T11:07:26.2565485-07:00</para>
  /// <para>** Template version: 3.071204</para>
  /// ****************************************************
  /// </remarks>
  [BindingBrowsable(false)]
  [Serializable]
  public partial class RedeemerCashTransactionsAuditDataTable : IdeaBlade.Persistence.EntityTable, 
     IdeaBlade.Persistence.Rdb.IRdbTable {
  
    /// <summary>
    /// Initializes a new instance of the RedeemerCashTransactionsAuditDataTable class with no arguments.
    /// </summary>
    public RedeemerCashTransactionsAuditDataTable() {}

    /// <summary>
    /// Initializes a new instance of the RedeemerCashTransactionsAuditDataTable class 
    /// with the <see cref="System.Runtime.Serialization.SerializationInfo"/> 
    /// and the <see cref="System.Runtime.Serialization.StreamingContext"/>.
    /// </summary>
    /// <param name="pInfo">The data needed to serialize or deserialize an object.</param>
    /// <param name="pContext">The source and destination of a given serialized stream.</param>
    protected RedeemerCashTransactionsAuditDataTable(SerializationInfo pInfo, StreamingContext pContext) :
      base(pInfo, pContext) {}

    /// <summary>
    /// Creates a new instance of the <see cref="RedeemerCashTransactionsAuditDataRow"/>.
    /// Constructs a row from the builder.
    /// <para>** For internal use only. **</para>
    /// </summary>
    /// <param name="pRowBuilder"></param>
    /// <returns>The <see cref="RedeemerCashTransactionsAuditDataRow"/>.</returns>
    protected override DataRow NewRowFromBuilder(DataRowBuilder pRowBuilder) {
      return new RedeemerCashTransactionsAudit(pRowBuilder);
    }

    /// <summary>Returns the <see cref="System.Type"/> of <see cref="RedeemerCashTransactionsAudit"/>.</summary>
    protected override Type GetRowType() {
      return typeof(RedeemerCashTransactionsAudit);
    }
    
    /// <summary>
    /// Gets the name of the <see cref="T:IdeaBlade.Rdb.RdbKey"/> or 
    /// <see cref="T:IdeaBlade.Persistence.WS.WsKey"/> defined in <b>IdeaBlade.ibconfig</b>. 
    /// </summary>
    /// <remarks>For this <see cref="RedeemerCashTransactionsAudit"/>, the DataSourceKeyName="Audit".</remarks>
    public override String DataSourceKeyName {
      get { return @"Audit"; }
    }

    #region Column properties
    //**************************************
    //* Column properties
    //**************************************
    /// <summary>Gets the RedeemerCashTransactionsHistoryID <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn RedeemerCashTransactionsHistoryIDColumn {
      get {
        if (mRedeemerCashTransactionsHistoryIDColumn!=null) return mRedeemerCashTransactionsHistoryIDColumn;
        mRedeemerCashTransactionsHistoryIDColumn = GetColumn("RedeemerCashTransactionsHistoryID", true);
        return mRedeemerCashTransactionsHistoryIDColumn;
      }
    }
    private DataColumn mRedeemerCashTransactionsHistoryIDColumn;
    
    /// <summary>Gets the RedeemerCashTransactionsID <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn RedeemerCashTransactionsIDColumn {
      get {
        if (mRedeemerCashTransactionsIDColumn!=null) return mRedeemerCashTransactionsIDColumn;
        mRedeemerCashTransactionsIDColumn = GetColumn("RedeemerCashTransactionsID", true);
        return mRedeemerCashTransactionsIDColumn;
      }
    }
    private DataColumn mRedeemerCashTransactionsIDColumn;
    
    /// <summary>Gets the RedeemerCashDrawerID <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn RedeemerCashDrawerIDColumn {
      get {
        if (mRedeemerCashDrawerIDColumn!=null) return mRedeemerCashDrawerIDColumn;
        mRedeemerCashDrawerIDColumn = GetColumn("RedeemerCashDrawerID", true);
        return mRedeemerCashDrawerIDColumn;
      }
    }
    private DataColumn mRedeemerCashDrawerIDColumn;
    
    /// <summary>Gets the TransactionTime <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn TransactionTimeColumn {
      get {
        if (mTransactionTimeColumn!=null) return mTransactionTimeColumn;
        mTransactionTimeColumn = GetColumn("TransactionTime", true);
        return mTransactionTimeColumn;
      }
    }
    private DataColumn mTransactionTimeColumn;
    
    /// <summary>Gets the TransactionAmount <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn TransactionAmountColumn {
      get {
        if (mTransactionAmountColumn!=null) return mTransactionAmountColumn;
        mTransactionAmountColumn = GetColumn("TransactionAmount", true);
        return mTransactionAmountColumn;
      }
    }
    private DataColumn mTransactionAmountColumn;
    
    /// <summary>Gets the TransCardBalance <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn TransCardBalanceColumn {
      get {
        if (mTransCardBalanceColumn!=null) return mTransCardBalanceColumn;
        mTransCardBalanceColumn = GetColumn("TransCardBalance", true);
        return mTransCardBalanceColumn;
      }
    }
    private DataColumn mTransCardBalanceColumn;
    
    /// <summary>Gets the TransactionNotes <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn TransactionNotesColumn {
      get {
        if (mTransactionNotesColumn!=null) return mTransactionNotesColumn;
        mTransactionNotesColumn = GetColumn("TransactionNotes", true);
        return mTransactionNotesColumn;
      }
    }
    private DataColumn mTransactionNotesColumn;
    
    /// <summary>Gets the RowVersion <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn RowVersionColumn {
      get {
        if (mRowVersionColumn!=null) return mRowVersionColumn;
        mRowVersionColumn = GetColumn("RowVersion", true);
        return mRowVersionColumn;
      }
    }
    private DataColumn mRowVersionColumn;
    
    /// <summary>Gets the ModifiedBy <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn ModifiedByColumn {
      get {
        if (mModifiedByColumn!=null) return mModifiedByColumn;
        mModifiedByColumn = GetColumn("ModifiedBy", true);
        return mModifiedByColumn;
      }
    }
    private DataColumn mModifiedByColumn;
    
    /// <summary>Gets the ModifiedDate <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn ModifiedDateColumn {
      get {
        if (mModifiedDateColumn!=null) return mModifiedDateColumn;
        mModifiedDateColumn = GetColumn("ModifiedDate", true);
        return mModifiedDateColumn;
      }
    }
    private DataColumn mModifiedDateColumn;
    
    #endregion    

    /// <summary>Creates the schema mapping information for this RedeemerCashTransactionsAuditDataTable.</summary>
    /// <returns>
    /// The <see cref="T:IdeaBlade.Persistence.TableMappingInfo"/> for this RedeemerCashTransactionsAuditDataTable.
    /// </returns>
    protected override TableMappingInfo CreateTableMappingInfo() {
      RdbTableMappingInfo mappingInfo = new RdbTableMappingInfo(this);
      mappingInfo.SourceOwnerName = "dbo"; 
      mappingInfo.SourceTableName = "RedeemerCashTransactionsAudit"; 
      mappingInfo.ConcurrencyColumnName = ""; 
      // ColumnSelectionOption: selectAllColumns
      mappingInfo.SourceColumnNames = "*"; 
   
      DataColumnMappingCollection columnMappings = mappingInfo.TableMapping.ColumnMappings;
      columnMappings.Clear();
      columnMappings.Add("RedeemerCashTransactionsHistoryID", "RedeemerCashTransactionsHistoryID");
      columnMappings.Add("RedeemerCashTransactionsID", "RedeemerCashTransactionsID");
      columnMappings.Add("RedeemerCashDrawerID", "RedeemerCashDrawerID");
      columnMappings.Add("TransactionTime", "TransactionTime");
      columnMappings.Add("TransactionAmount", "TransactionAmount");
      columnMappings.Add("TransCardBalance", "TransCardBalance");
      columnMappings.Add("TransactionNotes", "TransactionNotes");
      columnMappings.Add("RowVersion", "RowVersion");
      columnMappings.Add("ModifiedBy", "ModifiedBy");
      columnMappings.Add("ModifiedDate", "ModifiedDate");
      return mappingInfo;
    }
  
    /// <summary>** For internal use only. **</summary>
    protected override void UpdateTableMappingInfo() {
      base.UpdateTableMappingInfo();
    }

    /// <summary>
    /// Initializes RedeemerCashTransactionsAudit <see cref="System.Data.DataColumn"/> properties.
    /// </summary>
    public override void InitColumnProperties() {
      base.InitColumnProperties();

      RedeemerCashTransactionsHistoryIDColumn.Caption = "RedeemerCashTransactionsHistoryID";
      RedeemerCashTransactionsIDColumn.Caption = "RedeemerCashTransactionsID";
      RedeemerCashDrawerIDColumn.Caption = "RedeemerCashDrawerID";
      TransactionTimeColumn.Caption = "TransactionTime";
      TransactionAmountColumn.Caption = "TransactionAmount";
      TransCardBalanceColumn.Caption = "TransCardBalance";
      TransactionNotesColumn.Caption = "TransactionNotes";
      RowVersionColumn.Caption = "RowVersion";
      ModifiedByColumn.Caption = "ModifiedBy";
      ModifiedDateColumn.Caption = "ModifiedDate";
    }
  }
  #endregion
  
  #region RedeemerCashTransactionsAuditDataRow
  /// <summary>
  /// The generated base class for <see cref="RedeemerCashTransactionsAudit"/>.
  /// </summary>
  /// <remarks>
  /// <para>
  /// It represents a row of persistable data in the <see cref="RedeemerCashTransactionsAuditDataTable"/>
  /// and derives from <see cref="T:IdeaBlade.Persistence.Entity"/>.
	/// </para>
  /// ****************************************************
  /// <para>** DO NOT MODIFY THIS CLASS - AutoGenerated Code</para>
  /// <para>** Any changes made to this code will be lost</para>
  /// <para>** the next time this code is regenerated.</para>
  /// <para>**</para>
  /// <para>** Generated from:   EntityBaseclass.template</para>
  /// <para>** Last modified:    2011-05-11T11:07:26.2565485-07:00</para>
  /// <para>** Template version: 3.071204</para>
  /// ****************************************************
  /// </remarks>  
  [Serializable]
  public abstract partial class RedeemerCashTransactionsAuditDataRow : IdeaBlade.Persistence.Entity, 
     IdeaBlade.Persistence.Rdb.IRdbEntity {
  
    /// <summary>
    /// Initializes a new instance of the RedeemerCashTransactionsAuditDataRow.
    /// Constructs a row from the builder.
    /// <para>** For internal use only. **</para>
    /// </summary>
    /// <param name="pRowBuilder"></param>
    protected RedeemerCashTransactionsAuditDataRow(DataRowBuilder pRowBuilder)
      : base(pRowBuilder) {
    }

    /// <summary>Gets the <see cref="RedeemerCashTransactionsAuditDataTable"/>.</summary>
    [BindingBrowsable(false)]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public new RedeemerCashTransactionsAuditDataTable TypedTable {
      get { return (RedeemerCashTransactionsAuditDataTable) base.Table; }
    }

    /// <summary>Returns the <see cref="System.Type"/> of <see cref="RedeemerCashTransactionsAuditDataTable"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public static Type GetTableType() {
      return typeof(RedeemerCashTransactionsAuditDataTable);
    }

    /// <summary>
    /// Gets the SQL 'where' clause used to restrict which 
    /// database records are accessible via this entity. 
    /// </summary>
    /// <remarks>
    /// The default property value is defined in the Current Class 
    /// Properties tab in the <b>IdeaBlade DevForce Object Mapping Tool</b>.
    /// <para>
    /// The 'where' clause will be used, in addition to any other 
    /// query-specific criteria, in every retrieval of entities of this type.
    /// </para>
    /// </remarks>
    [BindingBrowsable(false)]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public virtual string SqlWhereClause {
      get { return ""; }
    }
    
    /// <summary>
    /// Determines whether to use the 'distinct' keyword when 
    /// generating the SQL select clause for objects of this type.
    /// </summary>
    /// <remarks>Defaults to false, but may be overridden in subclasses.</remarks>
    [BindingBrowsable(false)]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public virtual bool ForceSqlDistinct {
      get { return false; }
    }
    

    #region Enums
    //**************************************
    //* Enums
    //**************************************
    #endregion
	
    #region Relation properties
    //**************************************
    //* Relation properties
    //**************************************
    #endregion
    
    #region EntityColumn definitions
    //**************************************
    //* Static EntityColumn definitions
    //**************************************
    /// <summary>The RedeemerCashTransactionsHistoryID <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn RedeemerCashTransactionsHistoryIDEntityColumn =
      new EntityColumn(typeof(RedeemerCashTransactionsAudit), "RedeemerCashTransactionsHistoryID", typeof(System.Int64), false, true, true, DataSourceAccessType.ReadWrite);      
    /// <summary>The RedeemerCashTransactionsID <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn RedeemerCashTransactionsIDEntityColumn =
      new EntityColumn(typeof(RedeemerCashTransactionsAudit), "RedeemerCashTransactionsID", typeof(System.Int64), false, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The RedeemerCashDrawerID <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn RedeemerCashDrawerIDEntityColumn =
      new EntityColumn(typeof(RedeemerCashTransactionsAudit), "RedeemerCashDrawerID", typeof(System.Int64), false, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The TransactionTime <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn TransactionTimeEntityColumn =
      new EntityColumn(typeof(RedeemerCashTransactionsAudit), "TransactionTime", typeof(System.DateTime), false, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The TransactionAmount <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn TransactionAmountEntityColumn =
      new EntityColumn(typeof(RedeemerCashTransactionsAudit), "TransactionAmount", typeof(System.Decimal), false, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The TransCardBalance <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn TransCardBalanceEntityColumn =
      new EntityColumn(typeof(RedeemerCashTransactionsAudit), "TransCardBalance", typeof(System.Decimal), false, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The TransactionNotes <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn TransactionNotesEntityColumn =
      new EntityColumn(typeof(RedeemerCashTransactionsAudit), "TransactionNotes", typeof(System.String), false, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The RowVersion <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn RowVersionEntityColumn =
      new EntityColumn(typeof(RedeemerCashTransactionsAudit), "RowVersion", typeof(System.Int32), false, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The ModifiedBy <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn ModifiedByEntityColumn =
      new EntityColumn(typeof(RedeemerCashTransactionsAudit), "ModifiedBy", typeof(System.String), false, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The ModifiedDate <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn ModifiedDateEntityColumn =
      new EntityColumn(typeof(RedeemerCashTransactionsAudit), "ModifiedDate", typeof(System.DateTime), false, false, false, DataSourceAccessType.ReadWrite);      
    #endregion
    
    #region Properties
    //**************************************
    //* RedeemerCashTransactionsHistoryID methods
    //**************************************
    /// <summary>Gets the RedeemerCashTransactionsHistoryID <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn RedeemerCashTransactionsHistoryIDColumn {
      get { return TypedTable.RedeemerCashTransactionsHistoryIDColumn; }
    }

    /// <summary>Gets the RedeemerCashTransactionsHistoryID.</summary>
    [DBDataType(typeof(System.Int64))]
    public virtual System.Int64 RedeemerCashTransactionsHistoryID {
      get { 
        System.Int64 result_;
        if (GetInterceptor<System.Int64>("RedeemerCashTransactionsHistoryID", GetRedeemerCashTransactionsHistoryIDImpl, out result_)) return result_;
        return GetRedeemerCashTransactionsHistoryIDImpl();
      }
    }
    private System.Int64 GetRedeemerCashTransactionsHistoryIDImpl() {
      return (System.Int64) GetColumnValue(RedeemerCashTransactionsHistoryIDColumn, typeof(System.Int64), false); 
    }
    
    //**************************************
    //* RedeemerCashTransactionsID methods
    //**************************************
    /// <summary>Gets the RedeemerCashTransactionsID <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn RedeemerCashTransactionsIDColumn {
      get { return TypedTable.RedeemerCashTransactionsIDColumn; }
    }

    /// <summary>Gets or sets the RedeemerCashTransactionsID.</summary>
    [DBDataType(typeof(System.Int64))]
    public virtual System.Int64 RedeemerCashTransactionsID {
      get { 
        System.Int64 result_;
        if (GetInterceptor<System.Int64>("RedeemerCashTransactionsID", GetRedeemerCashTransactionsIDImpl, out result_)) return result_;
        return GetRedeemerCashTransactionsIDImpl();
      }
      set { 
          if (!SetInterceptor<System.Int64>("RedeemerCashTransactionsID", value, SetRedeemerCashTransactionsIDImpl)) {
            SetRedeemerCashTransactionsIDImpl(value);
          }  
      }    
    }
    private System.Int64 GetRedeemerCashTransactionsIDImpl() {
      return (System.Int64) GetColumnValue(RedeemerCashTransactionsIDColumn, typeof(System.Int64), false); 
    }
    private void SetRedeemerCashTransactionsIDImpl(System.Int64 value) {
      SetColumnValue(RedeemerCashTransactionsIDColumn, value);
    }
    
    //**************************************
    //* RedeemerCashDrawerID methods
    //**************************************
    /// <summary>Gets the RedeemerCashDrawerID <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn RedeemerCashDrawerIDColumn {
      get { return TypedTable.RedeemerCashDrawerIDColumn; }
    }

    /// <summary>Gets or sets the RedeemerCashDrawerID.</summary>
    [DBDataType(typeof(System.Int64))]
    public virtual System.Int64 RedeemerCashDrawerID {
      get { 
        System.Int64 result_;
        if (GetInterceptor<System.Int64>("RedeemerCashDrawerID", GetRedeemerCashDrawerIDImpl, out result_)) return result_;
        return GetRedeemerCashDrawerIDImpl();
      }
      set { 
          if (!SetInterceptor<System.Int64>("RedeemerCashDrawerID", value, SetRedeemerCashDrawerIDImpl)) {
            SetRedeemerCashDrawerIDImpl(value);
          }  
      }    
    }
    private System.Int64 GetRedeemerCashDrawerIDImpl() {
      return (System.Int64) GetColumnValue(RedeemerCashDrawerIDColumn, typeof(System.Int64), false); 
    }
    private void SetRedeemerCashDrawerIDImpl(System.Int64 value) {
      SetColumnValue(RedeemerCashDrawerIDColumn, value);
    }
    
    //**************************************
    //* TransactionTime methods
    //**************************************
    /// <summary>Gets the TransactionTime <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn TransactionTimeColumn {
      get { return TypedTable.TransactionTimeColumn; }
    }

    /// <summary>Gets or sets the TransactionTime.</summary>
    [DBDataType(typeof(System.DateTime))]
    public virtual System.DateTime TransactionTime {
      get { 
        System.DateTime result_;
        if (GetInterceptor<System.DateTime>("TransactionTime", GetTransactionTimeImpl, out result_)) return result_;
        return GetTransactionTimeImpl();
      }
      set { 
          if (!SetInterceptor<System.DateTime>("TransactionTime", value, SetTransactionTimeImpl)) {
            SetTransactionTimeImpl(value);
          }  
      }    
    }
    private System.DateTime GetTransactionTimeImpl() {
      return (System.DateTime) GetColumnValue(TransactionTimeColumn, typeof(System.DateTime), false); 
    }
    private void SetTransactionTimeImpl(System.DateTime value) {
      SetColumnValue(TransactionTimeColumn, value);
    }
    
    //**************************************
    //* TransactionAmount methods
    //**************************************
    /// <summary>Gets the TransactionAmount <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn TransactionAmountColumn {
      get { return TypedTable.TransactionAmountColumn; }
    }

    /// <summary>Gets or sets the TransactionAmount.</summary>
    [DBDataType(typeof(System.Decimal))]
    public virtual System.Decimal TransactionAmount {
      get { 
        System.Decimal result_;
        if (GetInterceptor<System.Decimal>("TransactionAmount", GetTransactionAmountImpl, out result_)) return result_;
        return GetTransactionAmountImpl();
      }
      set { 
          if (!SetInterceptor<System.Decimal>("TransactionAmount", value, SetTransactionAmountImpl)) {
            SetTransactionAmountImpl(value);
          }  
      }    
    }
    private System.Decimal GetTransactionAmountImpl() {
      return (System.Decimal) GetColumnValue(TransactionAmountColumn, typeof(System.Decimal), false); 
    }
    private void SetTransactionAmountImpl(System.Decimal value) {
      SetColumnValue(TransactionAmountColumn, value);
    }
    
    //**************************************
    //* TransCardBalance methods
    //**************************************
    /// <summary>Gets the TransCardBalance <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn TransCardBalanceColumn {
      get { return TypedTable.TransCardBalanceColumn; }
    }

    /// <summary>Gets or sets the TransCardBalance.</summary>
    [DBDataType(typeof(System.Decimal))]
    public virtual System.Decimal TransCardBalance {
      get { 
        System.Decimal result_;
        if (GetInterceptor<System.Decimal>("TransCardBalance", GetTransCardBalanceImpl, out result_)) return result_;
        return GetTransCardBalanceImpl();
      }
      set { 
          if (!SetInterceptor<System.Decimal>("TransCardBalance", value, SetTransCardBalanceImpl)) {
            SetTransCardBalanceImpl(value);
          }  
      }    
    }
    private System.Decimal GetTransCardBalanceImpl() {
      return (System.Decimal) GetColumnValue(TransCardBalanceColumn, typeof(System.Decimal), false); 
    }
    private void SetTransCardBalanceImpl(System.Decimal value) {
      SetColumnValue(TransCardBalanceColumn, value);
    }
    
    //**************************************
    //* TransactionNotes methods
    //**************************************
    /// <summary>Gets the TransactionNotes <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn TransactionNotesColumn {
      get { return TypedTable.TransactionNotesColumn; }
    }

    /// <summary>Gets or sets the TransactionNotes.</summary>
    [MaxTextLength(1000)]
    [DBDataType(typeof(System.String))]
    public virtual System.String TransactionNotes {
      get { 
        System.String result_;
        if (GetInterceptor<System.String>("TransactionNotes", GetTransactionNotesImpl, out result_)) return result_;
        return GetTransactionNotesImpl();
      }
      set { 
          if (!SetInterceptor<System.String>("TransactionNotes", value, SetTransactionNotesImpl)) {
            SetTransactionNotesImpl(value);
          }  
      }    
    }
    private System.String GetTransactionNotesImpl() {
      return (System.String) GetColumnValue(TransactionNotesColumn, typeof(System.String), false); 
    }
    private void SetTransactionNotesImpl(System.String value) {
      SetColumnValue(TransactionNotesColumn, value);
    }
    
    //**************************************
    //* RowVersion methods
    //**************************************
    /// <summary>Gets the RowVersion <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn RowVersionColumn {
      get { return TypedTable.RowVersionColumn; }
    }

    /// <summary>Gets or sets the RowVersion.</summary>
    [DBDataType(typeof(System.Int32))]
    public virtual System.Int32 RowVersion {
      get { 
        System.Int32 result_;
        if (GetInterceptor<System.Int32>("RowVersion", GetRowVersionImpl, out result_)) return result_;
        return GetRowVersionImpl();
      }
      set { 
          if (!SetInterceptor<System.Int32>("RowVersion", value, SetRowVersionImpl)) {
            SetRowVersionImpl(value);
          }  
      }    
    }
    private System.Int32 GetRowVersionImpl() {
      return (System.Int32) GetColumnValue(RowVersionColumn, typeof(System.Int32), false); 
    }
    private void SetRowVersionImpl(System.Int32 value) {
      SetColumnValue(RowVersionColumn, value);
    }
    
    //**************************************
    //* ModifiedBy methods
    //**************************************
    /// <summary>Gets the ModifiedBy <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn ModifiedByColumn {
      get { return TypedTable.ModifiedByColumn; }
    }

    /// <summary>Gets or sets the ModifiedBy.</summary>
    [MaxTextLength(100)]
    [DBDataType(typeof(System.String))]
    public virtual System.String ModifiedBy {
      get { 
        System.String result_;
        if (GetInterceptor<System.String>("ModifiedBy", GetModifiedByImpl, out result_)) return result_;
        return GetModifiedByImpl();
      }
      set { 
          if (!SetInterceptor<System.String>("ModifiedBy", value, SetModifiedByImpl)) {
            SetModifiedByImpl(value);
          }  
      }    
    }
    private System.String GetModifiedByImpl() {
      return (System.String) GetColumnValue(ModifiedByColumn, typeof(System.String), false); 
    }
    private void SetModifiedByImpl(System.String value) {
      SetColumnValue(ModifiedByColumn, value);
    }
    
    //**************************************
    //* ModifiedDate methods
    //**************************************
    /// <summary>Gets the ModifiedDate <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn ModifiedDateColumn {
      get { return TypedTable.ModifiedDateColumn; }
    }

    /// <summary>Gets or sets the ModifiedDate.</summary>
    [DBDataType(typeof(System.DateTime))]
    public virtual System.DateTime ModifiedDate {
      get { 
        System.DateTime result_;
        if (GetInterceptor<System.DateTime>("ModifiedDate", GetModifiedDateImpl, out result_)) return result_;
        return GetModifiedDateImpl();
      }
      set { 
          if (!SetInterceptor<System.DateTime>("ModifiedDate", value, SetModifiedDateImpl)) {
            SetModifiedDateImpl(value);
          }  
      }    
    }
    private System.DateTime GetModifiedDateImpl() {
      return (System.DateTime) GetColumnValue(ModifiedDateColumn, typeof(System.DateTime), false); 
    }
    private void SetModifiedDateImpl(System.DateTime value) {
      SetColumnValue(ModifiedDateColumn, value);
    }
    
    #endregion

  }
  #endregion

  #region EntityPropertyDescriptors.RedeemerCashTransactionsAuditPropertyDescriptor
  public static partial class EntityPropertyDescriptors {

    /// <summary>
    /// Gets the RedeemerCashTransactionsAuditPropertyDescriptor for <see cref="RedeemerCashTransactionsAudit"/>.
    /// </summary>
    public static RedeemerCashTransactionsAuditPropertyDescriptor RedeemerCashTransactionsAudit {
      get { return RedeemerCashTransactionsAuditPropertyDescriptor.Instance; }
    }
    
    /// <summary>
    /// The generated class containing <see cref="RedeemerCashTransactionsAudit"/> PropertyDescriptors.
    /// </summary>
    public partial class RedeemerCashTransactionsAuditPropertyDescriptor : AdaptedPropertyDescriptor {
	
			/// <summary>
      /// Initializes a new instance of the RedeemerCashTransactionsAuditPropertyDescriptor class. 
      /// </summary>
      /// <param name="pParentDescriptor">The parent <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/></param>
      /// <param name="pPropertyPath">The type of the property value</param>
			public RedeemerCashTransactionsAuditPropertyDescriptor(AdaptedPropertyDescriptor pParentDescriptor, String pPropertyPath)
        : base(pParentDescriptor, pPropertyPath) {}
		
      /// <summary>
      /// Initializes a new instance of the RedeemerCashTransactionsAuditPropertyDescriptor class. 
      /// </summary>
      /// <param name="pComponentType">The type of object to which the property is bound</param>
      protected RedeemerCashTransactionsAuditPropertyDescriptor(Type pComponentType)
        : base(pComponentType) {}

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for RedeemerCashTransactionsHistoryID.</summary>
      public AdaptedPropertyDescriptor RedeemerCashTransactionsHistoryID {
        get { return Get("RedeemerCashTransactionsHistoryID"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for RedeemerCashTransactionsID.</summary>
      public AdaptedPropertyDescriptor RedeemerCashTransactionsID {
        get { return Get("RedeemerCashTransactionsID"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for RedeemerCashDrawerID.</summary>
      public AdaptedPropertyDescriptor RedeemerCashDrawerID {
        get { return Get("RedeemerCashDrawerID"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for TransactionTime.</summary>
      public AdaptedPropertyDescriptor TransactionTime {
        get { return Get("TransactionTime"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for TransactionAmount.</summary>
      public AdaptedPropertyDescriptor TransactionAmount {
        get { return Get("TransactionAmount"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for TransCardBalance.</summary>
      public AdaptedPropertyDescriptor TransCardBalance {
        get { return Get("TransCardBalance"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for TransactionNotes.</summary>
      public AdaptedPropertyDescriptor TransactionNotes {
        get { return Get("TransactionNotes"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for RowVersion.</summary>
      public AdaptedPropertyDescriptor RowVersion {
        get { return Get("RowVersion"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for ModifiedBy.</summary>
      public AdaptedPropertyDescriptor ModifiedBy {
        get { return Get("ModifiedBy"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for ModifiedDate.</summary>
      public AdaptedPropertyDescriptor ModifiedDate {
        get { return Get("ModifiedDate"); }
      }

     internal static RedeemerCashTransactionsAuditPropertyDescriptor Instance = new RedeemerCashTransactionsAuditPropertyDescriptor(typeof(RedeemerCashTransactionsAudit));
    }
  }
  #endregion
  
}
