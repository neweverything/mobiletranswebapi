﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

using Csla.Validation;
using TaxiPassCommon;

namespace CabRideEngine {
    /// <summary>
    /// The developer class representing the AffiliateDriversRecurringFees business object class.
    /// </summary>
    /// <remarks>
    /// <para>Place all application-specific business logic in this class.</para>
    /// <para>
    /// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
    /// and will not overwrite changes.
    /// </para>
    /// <para>
    /// This class inherits from the generated class, its companion in the pair of classes 
    /// defining a business object. Properties in the generated class may be overridden in 
    /// order to change the behavior or add business logic.
    /// </para>
    /// <para>
    /// This class is the final class in the inheritance chain for this business object. 
    /// It should not be inherited.
    /// </para>
    /// </remarks>
    [Serializable]
    public sealed partial class AffiliateDriversRecurringFees : AffiliateDriversRecurringFeesDataRow {

        #region Constructors -- DO NOT MODIFY

        // Do not create constructors for this class
        // Developers cannot create instances of this class with the "new" keyword
        // You should write a static Create method instead; see the "Suggested Customization" region
        // Please review the documentation to learn about entity creation and inheritance

        // This private constructor prevents accidental instance creation via 'new';
        // it is also required if the class is visible from a web service.
        // DO NOT REMOVE **
        private AffiliateDriversRecurringFees() : this(null) { }

        /// <summary>
        /// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
        /// </summary>
        /// <param name="pRowBuilder"></param>
        public AffiliateDriversRecurringFees(DataRowBuilder pRowBuilder)
            : base(pRowBuilder) {
        }

        #endregion

        #region Suggested Customizations

        //    // Use this factory method template to create new instances of this class
        //    public static AffiliateDriversRecurringFees Create(PersistenceManager pManager,
        //      ... your creation parameters here ...) { 
        //
        //      AffiliateDriversRecurringFees aAffiliateDriversRecurringFees = pManager.CreateEntity<AffiliateDriversRecurringFees>();
        //
        //      // if this object type requires a unique id and you have implemented
        //      // the IIdGenerator interface implement the following line
        //      pManager.GenerateId(aAffiliateDriversRecurringFees, // add id column here //);
        //
        //      // Add custom code here
        //
        //      aAffiliateDriversRecurringFees.AddToManager();
        //      return aAffiliateDriversRecurringFees;
        //    }

        //    // Implement this method if you want your object to sort in a specific order
        //    public override int CompareTo(Object pObject) {
        //    }

        //    // Implement this method to customize the null object version of this class
        //    protected override void UpdateNullEntity() {
        //    }

        #endregion

        #region Standard Entity logic to create/audit/validate the entity

        // Add additional logic to your business object here...
        public static AffiliateDriversRecurringFees Create(AffiliateDrivers pAffiliateDriver) {

            PersistenceManager oPM = pAffiliateDriver.PersistenceManager;
            AffiliateDriversRecurringFees rec = null;

            try {
                rec = oPM.CreateEntity<AffiliateDriversRecurringFees>();

                // Using the IdeaBlade Id Generation technique
                oPM.GenerateId(rec, AffiliateDriversRecurringFees.AffiliateDriversRecurringFeeIDEntityColumn);
                rec.AffiliateDriverID = pAffiliateDriver.AffiliateDriverID;
                TimeZoneConverter convert = new TimeZoneConverter();
                rec.StartDate = convert.ConvertTime(DateTime.Now, TimeZone.CurrentTimeZone.StandardName, pAffiliateDriver.Affiliate.TimeZone).Date;
                rec.AddToManager();

            } catch (Exception ex) {
                throw ex;
            }
            return rec;
        }

        protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
            if (IsDeserializing) {
                return;
            }
            base.OnColumnChanging(pArgs);
            DoAudit(pArgs);
        }

        private void DoAudit(DataColumnChangeEventArgs pArgs) {
            if (!this.PersistenceManager.IsClient) {
                return;
            }
            if (this.RowState == DataRowState.Added) {
                return;
            }
            if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
                AuditRecord = AffiliateDriversRecurringFeesAudit.Create(this.PersistenceManager, this);
            }
        }

        protected override void AddRules(RuleList pList) {
            pList.Add(IsExpenseTypeUnique, ExpenseTypeIDEntityColumn.ColumnName);
        }

        private bool IsExpenseTypeUnique(object pTarget, RuleArgs e) {
            AffiliateDriversRecurringFees rec = pTarget as AffiliateDriversRecurringFees;

            RdbQuery qry = new RdbQuery(typeof(AffiliateDriversRecurringFees), AffiliateDriversRecurringFees.ExpenseTypeIDEntityColumn, EntityQueryOp.EQ, rec.ExpenseTypeID);
            qry.AddClause(AffiliateDriversRecurringFees.AffiliateDriverIDEntityColumn, EntityQueryOp.EQ, rec.AffiliateDriverID);
            qry.AddClause(AffiliateDriversRecurringFees.AffiliateDriversRecurringFeeIDEntityColumn, EntityQueryOp.NE, rec.AffiliateDriversRecurringFeeID);
            EntityList<AffiliateDriversRecurringFees> oList = this.PersistenceManager.GetEntities<AffiliateDriversRecurringFees>(qry);
            if (oList.Count > 0) {
                e.Description = "A recurring fee already exists for " + rec.ExpenseTypes.Description;
                return false;
            }

            return true;
        }
        #endregion

        // Add additional logic to your business object here...
        public static EntityList<AffiliateDriversRecurringFees> GetActiveFees(PersistenceManager pPM) {
            RdbQuery qry = new RdbQuery(typeof(AffiliateDriversRecurringFees), AffiliateDriversRecurringFees.CancelledEntityColumn, EntityQueryOp.EQ, false);
            qry.AddClause(AffiliateDriversRecurringFees.StartDateEntityColumn, EntityQueryOp.LE, DateTime.Now);
            qry.AddOrderBy(AffiliateDriversRecurringFees.AffiliateDriverIDEntityColumn);
            return pPM.GetEntities<AffiliateDriversRecurringFees>(qry, QueryStrategy.DataSourceThenCache);
        }

        public DateTime? GetLastRecurringFeeDate() {
            RdbQuery qry = new RdbQuery(typeof(DriverPayments), DriverPayments.ChargeByEntityColumn, EntityQueryOp.EQ, DriverPayments.RECURRING_FEES);
			qry.AddClause(DriverPayments.ReferenceNoEntityColumn, EntityQueryOp.EQ, "AD" + this.AffiliateDriversRecurringFeeID.ToString("F0"));
            qry.AddClause(DriverPayments.AffiliateDriverIDEntityColumn, EntityQueryOp.EQ, this.AffiliateDriverID);
            qry.AddOrderBy(DriverPayments.ChargeDateEntityColumn, System.ComponentModel.ListSortDirection.Descending);

            qry.Top = 1;
            return this.PersistenceManager.GetEntity<DriverPayments>(qry, QueryStrategy.DataSourceThenCache).ChargeBackDate;
        }


    }

    #region EntityPropertyDescriptors.AffiliateDriversRecurringFeesPropertyDescriptor
    ////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
    ////    For example: 
    ////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
    //public static partial class EntityPropertyDescriptors {
    //	public partial class AffiliateDriversRecurringFeesPropertyDescriptor : BaseEntityPropertyDescriptor {
    //    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
    //    public AdaptedPropertyDescriptor Age {
    //      get { return this.Get("Age"); }
    //    }
    //  }
    //} 
    #endregion

}