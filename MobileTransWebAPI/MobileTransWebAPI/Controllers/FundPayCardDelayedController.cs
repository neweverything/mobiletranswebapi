﻿using CabRideEngine;
using CabRideEngineDapper.Utils;
using IdeaBlade.Persistence;
using MobileTransWebAPI.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Http;
using TaxiPassCommon;
using TaxiPassCommon.Banking;
using TaxiPassCommon.Banking.Enums;
using TaxiPassCommon.Banking.Interfaces;

namespace MobileTransWebAPI.Controllers {

	public class FundPayCardDelayedController : ApiController {

		public FundsTransferResponse Get() {
			return FundPayCardDelayed("43B5C7893C781BC7671440E31B6134D3195CDD926A967D756B4BECDC070D7DAEEB4E80A4764366D019113F12F9DFB944F8DCB944A06EDA85FE3BFB0B43BDB95C2043C078528DC1D33A8E26823C71FA1F76B0AEFCEE8A5BDA0A2501BD38B56F8E93F84001D8034FF6350AD4F98C1E8D1D724E0B93209086D296B6CB90189037BB377DAF096851ADB98D17125F3DE52EEFAC7970F6CE248EA0FFD891E6CD6456EE011B2B4F39FFAC3634E5D4937BD1369D41B252D7EE3C8CF8DD87F185F4CAB188809AF0C7C62282F55DC8A5A53A6D34760DC8B0C179E910584C35CFCC6D5F325CA0598EDF97B7254841AEE8F0F071C221931846F2DE70348459F9FE6229788771AE5F6FBBEFBF72EE790B0CF0A703C44B8B64BEA44BDB1E3F50AF0446C2FBB1C08FDED9D20F80AAED1F1E999373A509931A7A54CC9188F3A08813C7744DDB5B9A7F6AC16FD565C68FA1D56F468DB61CC5E9591E10D87A5CD11EA59A67554A630EE199EA41EF990EBDDB8BA052DEC57FFFD2C45EB62549FF578D60D59F59AD2DA0E1D9386654E0710CFDA563902FC18D8461429D69B5AB3D45C060D25E715C7A8C42858EEDC9741CF84A3B25179AF37124D55645868DE47524AE861F67A9B85402A621B9C35CDF991F82730BA9CD9273AB4F6CAAA53D526066BEB7D4B8");
		}

		[HttpGet]
		public FundsTransferResponse FundPayCardDelayed(string pRequest) {
			FundsTransferResponse fundResults = new FundsTransferResponse();

			WebApiApplication.LogIt("FundPayCardDelayed:", pRequest, "FundPayCardDelayed_{0}");

			PersistenceManager mPM = WebApiApplication.UserPM;
			if (mPM == null) {
				fundResults.ReturnMessage = "Could not connect to database";
				return fundResults;
			}

			AffDriverLoginInfo info = new AffDriverLoginInfo();
			try {
				string request = pRequest.DecryptIceKey();
				info = JsonConvert.DeserializeObject<AffDriverLoginInfo>(request);

				DateTime requestDate = Convert.ToDateTime(info.RequestTime);

				if (WebApiApplication.GetUTCServerTime().Subtract(requestDate).TotalMinutes > 1) {
					fundResults.ReturnMessage = "Invalid Access";
					return fundResults;
				}

				if (!info.Phone.IsNullOrEmpty()) {
					using (var conn = SqlHelper.OpenSqlConnection()) {
						CabRideEngineDapper.AffiliateDrivers pPayeeDriver = CabRideEngineDapper.AffiliateDrivers.GetDriverByCallInCell(conn, info.Phone);

						if (pPayeeDriver.IsNullEntity || pPayeeDriver.RedeemerStopPayment || pPayeeDriver.TransCardNumber.IsNullOrEmpty()) {
							fundResults.ReturnCode = "-1";
							fundResults.ReturnMessage = "You cannot get paid at this time, please contact TaxiPass to resolve";
							return fundResults;
						}

						int missedDejavoo = FundingPayCardInfo.GetMissedDejavooBatchCount(pPayeeDriver.AffiliateDriverID);
						if (missedDejavoo > 0) {
							fundResults.ReturnCode = "-1";
							fundResults.ReturnMessage = "Please settle batch or bring credit card machine to trailer for payment";
							return fundResults;
						}

						string error = "";
						DateTime dtEnd = FundingPayCardInfoDelayedController.CalcAffiliateEndDate(pPayeeDriver.Affiliate, out error);
						fundResults.ReturnMessage = error;

						if (fundResults.ReturnMessage.IsNullOrEmpty()) {
							string holidayName = CabRideEngineDapper.BankHoliday.GetByDate(conn, DateTime.Today).Name;
							if (!holidayName.IsNullOrEmpty()) {
								fundResults.ReturnMessage = "No funding today due to bank holiday: " + holidayName;
							} else if (dtEnd == FundingPayCardInfoDelayedController.DefaultDate) {
								fundResults.ReturnMessage = "Funding not available today";
							}
						}
						if (!fundResults.ReturnMessage.IsNullOrEmpty()) {
							return fundResults;
						}

						DateTime start = dtEnd.AddDays(-Convert.ToDouble(SystemDefaultsDict.GetByKey(mPM, "DriverPay", "Do Not Pay Age", false).ValueString));

						EntityList<DriverPayments> payList = DriverPayments.GetPaymentsToAchForAffiliateDriver(mPM, pPayeeDriver.AffiliateDriverID, start, dtEnd);
						if (payList.Count == 0) {
							fundResults.ReturnCode = "-1";
							fundResults.ReturnMessage = "No vouchers available for funding";
							return fundResults;
						}
						decimal decAmount = 0;

						//FSV.Credentials cred = new FSV.Credentials();
						//cred = conn.Query<FSV.Credentials>("usp_SystemDefaultsDict_Flattened", new { ReferenceGroup = "FSV" }, commandType: CommandType.StoredProcedure).FirstOrDefault();
						IPayCardCredentials cred = new FSV.Credentials();
						CabRideEngineDapper.AffiliateDriverRef rec;
						if (pPayeeDriver.PayCardType.IsNullOrEmpty()) {
							pPayeeDriver.PayCardType = PayCardTypes.USBank.ToString();
						}
						CabRideEngineDapper.PayCardInfo fsvInfo = CabRideEngineDapper.PayCardInfo.GetByType(conn, pPayeeDriver.PayCardType);
						if (fsvInfo.PayCardType.Equals(PayCardTypes.TransCard.ToString(), StringComparison.CurrentCultureIgnoreCase)) {
							CabRideEngineDapper.TransCardDefaults transCardDefaults = CabRideEngineDapper.TransCardDefaults.GetDefaults(fsvInfo.UseProduction ? fsvInfo.PassCode.ToLong() : fsvInfo.TestPassCode.ToLong());
							cred = JsonConvert.DeserializeObject<TransCard.Credentials>(JsonConvert.SerializeObject(transCardDefaults));
						} else {
							cred = JsonConvert.DeserializeObject<FSV.Credentials>(JsonConvert.SerializeObject(fsvInfo)); //  conn.Query<FSV.Credentials>("usp_SystemDefaultsDict_Flattened", new { ReferenceGroup = "FSV" }, commandType: CommandType.StoredProcedure).FirstOrDefault();
						}


						rec = CabRideEngineDapper.AffiliateDriverRef.GetOrCreate(conn, pPayeeDriver.AffiliateDriverID, "FSV", "Registration");

						//FSV fsv = new FSV(cred, cred.UseProductionAsBoolen);
						//verifyResponse = fsv.VerifyConnection();
						PayCardGateway payCardGateWay = new PayCardGateway(cred);
						FSV.VerifyConnectionResponse verifyResponse = payCardGateWay.VerifyConnection();
						if (verifyResponse.ReturnCode != "1") {
							fundResults.ReturnCode = "-1";
							fundResults.ReturnMessage = "Could not verify connection to USBank: " + verifyResponse.ReturnMessage;
							return fundResults;
						}

						//FSV.CardRegistrationResponse resp = JsonConvert.DeserializeObject<FSV.CardRegistrationResponse>(rec.ValueString);
						//FSV.FundsTransfer transfer = new FSV.FundsTransfer(cred);
						//transfer.FromCardID = cred.AdjustmentCard.DecryptIceKey();
						//transfer.FromCardPassCode = cred.AdjustmentCode.DecryptIceKey();
						//transfer.ToCardID = resp.CardID;
						FSV.CardRegistrationResponse resp = JsonConvert.DeserializeObject<FSV.CardRegistrationResponse>(rec.ValueString);
						FSV.FundsTransfer transfer = new FSV.FundsTransfer(cred);
						transfer.ToCardID = resp.CardID;

						List<CabRideEngineDapper.ACHDetail> achRecList = new List<CabRideEngineDapper.ACHDetail>();

						string transNo = "";
						foreach (DriverPayments pay in payList) {
							if (pay.Test || pay.DoNotPay || pay.Failed) {
								continue;
							}
							//ACHDetail achRec = ACHDetail.GetTransactionByDriverPaymentID(mPM, pay.DriverPaymentID, QueryStrategy.DataSourceThenCache);
							CabRideEngineDapper.ACHDetail achRec = CabRideEngineDapper.ACHDetail.GetByDriverPaymentID(pay.DriverPaymentID);
							if (!achRec.ACHPaidDate.HasValue) {
								if (achRec.IsNullEntity) {
									//achRec = ACHDetail.Create(pay);
									achRec = CabRideEngineDapper.ACHDetail.Create(pay.DriverPaymentID);
								}
								//if (pPayeeDriver.PayCardRealTimePay || pPayeeDriver.Affiliate.PayCardRealTimePay) {
								//if (pay.RedemptionFee == 0) {
								//    pay.RedemptionFee = pay.Affiliate.AffiliateDriverDefaults.DriverInitiatedPayCardRedemptionFee;
								//}
								decAmount = pay.DriverTotalPaid; // - pay.Affiliate.AffiliateDriverDefaults.DriverInitiatedPayCardRedemptionFee.GetValueOrDefault(0);

								if (decAmount == 0) {
									decAmount = pay.DriverPaymentKioskAmount.Amount;
								}

								if (decAmount == 0) {
									achRec.ACHErrorReason = "No Fare amount found";
								} else {
									achRec.AmountPaid = decAmount;
									transfer.TransferAmount += decAmount;
								}

								achRec.ACHErrorReason = "Paid: " + pay.DriverTotalPaid.ToString("C");
								//}
								achRec.ACHPaidDate = DateTime.Now;
								achRec.ACHPaidTo = pPayeeDriver.Name;
								achRec.ACHBankAccountNumber = "PayCard: " + pPayeeDriver.TransCardNumber.DecryptIceKey().Right(5);

								achRec.TransCardAdminNo = pPayeeDriver.TransCardAdminNo;
								//achRec.Save(SessionHelper.EMail(Session));
								if (transNo.IsNullOrEmpty()) {
									transNo = pay.TransNo;
								} else {
									transNo = string.Format("{0}-{1}", transNo, pay.TransNo);
								}
								achRecList.Add(achRec);

								if (!pay.DriverPaid && achRec.AmountPaid.GetValueOrDefault(0) != 0) {
									pay.DriverPaid = true;
									pay.DriverPaidDate = achRec.ACHPaidDate;
									pay.Save(info.UserID);

									CabRideEngineDapper.DriverPaymentRef dpRef = CabRideEngineDapper.DriverPaymentRefTable.Create();
									dpRef.DriverPaymentID = pay.DriverPaymentID;
									dpRef.ReferenceGroup = "ACHToPayCard";
									dpRef.ReferenceKey = "AdminNo";
									dpRef.ValueString = pPayeeDriver.TransCardAdminNo;
									dpRef.Save(info.UserID);
								}
							}
						}

						transfer.TransferAmount = achRecList.Distinct().Sum(p => p.AmountPaid.GetValueOrDefault(0));
						transfer.ReferenceFromCard = string.Format("ADriverID: {0}|{1}", pPayeeDriver.AffiliateDriverID, transNo);
						if (transfer.ReferenceFromCard.Length > 100) {
							long masterVoucherID = payList[0].DriverPaymentID;
							foreach (var achRec in achRecList) {
								CabRideEngineDapper.DriverPaymentAux aux = CabRideEngineDapper.DriverPaymentAux.GetOrCreate(achRec.DriverPaymentID);
								aux.RowState = DataRowState.Unchanged;
								aux.MasterVoucherID = masterVoucherID;
								aux.Save(info.UserID);
							}
							transfer.ReferenceFromCard = string.Format("ADriverID: {0}|^{1}", pPayeeDriver.AffiliateDriverID, masterVoucherID);
						}
						transfer.ReferenceToCard = "Driver Payment";

						//var funded = fsv.TransferToPayCard(transfer);
						PayCardGateway payCardGateway = new PayCardGateway(cred);
						var funded = payCardGateway.TransferToPayCard(transfer);
						if (funded.ReturnCode == "1") {
							fundResults.ReturnCode = funded.ReturnCode;
							fundResults.ReturnMessage = funded.ReturnMessage;
							CabRideEngine.Utils.Misc.SendTransCardPaymentSMS(payList.ToList(), pPayeeDriver, "");
							//mPM.SaveChanges(achRecList);
							foreach (var achRec in achRecList) {
								achRec.Save();
							}
							CabRideEngineDapper.SystemDefaultsDict fundBal = CabRideEngineDapper.SystemDefaultsDict.GetOrCreate(conn, "FSV", "FundingAccountBalance");
							fundBal.ValueString = funded.FromCardBalance;
							fundBal.Save();

							CabRideEngineDapper.USBankPayCardPaid usBank = CabRideEngineDapper.USBankPayCardPaid.Create();
							usBank.AdminNo = transfer.ToCardID;
							usBank.AffiliateDriverID = pPayeeDriver.AffiliateDriverID;
							usBank.CardEnding = pPayeeDriver.TransCardCardNumber.Right(5);
							usBank.DebitTransaction = transfer.TransferAmount;
							usBank.FundingTransDate = DateTime.Now;
							usBank.LastName = pPayeeDriver.LastName.IsNullOrEmpty() ? pPayeeDriver.Name : pPayeeDriver.LastName;
							//usBank.TaxiPassRedeemerID = info.tax
							usBank.TransactionDetail = transfer.ReferenceFromCard;
							usBank.PayCardType = pPayeeDriver.PayCardType;
							usBank.TransactionID = funded.Unused2;
							usBank.Save();

						} else {
							fundResults.ReturnCode = funded.ReturnCode;
							//fundResults.ReturnMessage = string.Format("Payment of {0} to be posted later to your card\n{1}", transfer.TransferAmount.ToString("C"), funded.ReturnMessage);
							fundResults.ReturnMessage = string.Format("Payment of {0} to be posted later to your card\n", transfer.TransferAmount.ToString("C"));
							pPayeeDriver.SendDriverMsg(string.Format("Payment of {0} to be posted later to your card", transfer.TransferAmount.ToString("C")), false);

							CabRideEngineDapper.USBankPayCardPaid usBank = CabRideEngineDapper.USBankPayCardPaid.Create();
							usBank.AdminNo = transfer.ToCardID;
							usBank.AffiliateDriverID = pPayeeDriver.AffiliateDriverID;
							usBank.CardEnding = pPayeeDriver.TransCardCardNumber.Right(5);
							usBank.DebitTransaction = transfer.TransferAmount;
							usBank.FundingTransDate = DateTime.Now;
							usBank.LastName = pPayeeDriver.LastName.IsNullOrEmpty() ? pPayeeDriver.Name : pPayeeDriver.LastName;
							//usBank.TaxiPassRedeemerID = info.TaxiPassRedeemerID == 0 ? redeemerID : info.TaxiPassRedeemerID;
							usBank.TransactionDetail = transfer.ReferenceFromCard;
							usBank.PayCardType = pPayeeDriver.PayCardType;
							usBank.TransactionID = funded.Unused2;
							usBank.Note = fundResults.ReturnMessage;
							usBank.Save();
						}
					}
				}

			} catch (Exception ex) {
				fundResults.ReturnMessage = ex.Message;
			}

			return fundResults;
		}

		// public EntityList<DriverPayments> GetPayList(PersistenceManager pPM, long pAffiliateDriverID) {
		//     string sql = @"DECLARE @LookBackDays INT = (SELECT CONVERT(INT, ValueString)
		//							 FROM   SystemDefaultsDict
		//							 WHERE  ReferenceGroup = 'PayCard'
		//									AND ReferenceKey = 'PayDriverChargeDateMinusDays'
		//							)

		//SELECT  DISTINCT
		//		DriverPayments.*
		//FROM    dbo.DriverPayments
		//LEFT JOIN ACHDetail ON ACHDetail.DriverPaymentID = DriverPayments.DriverPaymentID
		//LEFT JOIN DriverPaymentAux ON DriverPaymentAux.DriverPaymentID = DriverPayments.DriverPaymentID
		//WHERE   AffiliateDriverID = {0}
		//		AND ACHPaidDate IS NULL
		//		AND Failed = 0
		//		AND Test = 0
		//		AND DATEADD(DAY, @LookBackDays, ChargeDate) < GETUTCDATE()
		//		AND ChargeDate >= '1/16/2016'
		//		AND ISNULL(DriverPayments.DoNotPay, 0) = 0 
		//		AND ISNULL(DriverPaymentAux.DoNotPay, 0) = 0
		//		AND TaxiPassRedeemerID IS NULL";

		//     PassthruRdbQuery qry = new PassthruRdbQuery(typeof(DriverPayments), string.Format(sql, pAffiliateDriverID));
		//     return pPM.GetEntities<DriverPayments>(qry);
		// }
	}
}