﻿

namespace MobileTransWebAPI.Models {
	public class CardInfoResults {

		public bool Valid { get; set; }
		public string ErrorMsg { get; set; }
		public string Token { get; set; }

		public string CardBin { get; set; }
	}
}