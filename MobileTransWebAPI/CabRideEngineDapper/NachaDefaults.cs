﻿using CabRideEngineDapper.Utils;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace CabRideEngineDapper {

	public partial class NachaDefaults {

		public static NachaDefaults GetDefaults(SqlConnection pConn) {
			List<NachaDefaults> list = pConn.Query<NachaDefaults>("SELECT * FROM dbo.NachaDefaults").ToList();
			if (list.Count == 0) {
				NachaDefaults rec = NachaDefaultsTable.Create();
				return rec;
			}
			return list[0];
		}

		public static List<NachaDefaults> GetDefaultList(SqlConnection pConn) {
			List<NachaDefaults> list = pConn.Query<NachaDefaults>("SELECT * FROM dbo.NachaDefaults ORDER By StartDate").ToList();
			return list;
		}

		public static NachaDefaults GetDefault(bool pNewFundingAccount) {
			using (var conn = SqlHelper.OpenSqlConnection()) {
				List<NachaDefaults> list = GetDefaultList(conn);
				if (list.Count == 0) {
					NachaDefaults rec = Create();
					return rec;
				}

				if (list.Count == 1) {
					return list[0];
				}

				bool test = false;
				if (test) {
					return list[2];
				}

				return pNewFundingAccount ? list.FirstOrDefault(p => p.NachaDefaultID != 1 && p.StartDate < DateTime.Today && (!p.EndDate.HasValue || p.EndDate >= DateTime.Today)) : list.FirstOrDefault(p => p.NachaDefaultID == 1);
			}
		}
	}
}
