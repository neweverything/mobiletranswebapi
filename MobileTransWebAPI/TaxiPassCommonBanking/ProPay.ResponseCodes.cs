﻿namespace TaxiPassCommon.Banking {
    public partial class ProPay {

        public ResponseInfo GetResponse(string pResponseCode) {
            string response = "";
            string desc = "";

            switch (pResponseCode) {
                case "00":
                    response = "Success";
                    desc = "The transaction was successful";
                    break;

                case "1":
                    response = "Transaction blocked by issuer";
                    desc = "The transaction was blocked by the card holder’s credit card company. The card holder will need to call the number on the back of the card to find out why.";
                    break;

                case "4":
                    response = "Pick up card and deny transaction";
                    desc = "There is a problem with the card.  Merchant should keep the card and call the number on the back of the card to determine the issue.";
                    break;

                case "5":
                    response = "Problem with the account";
                    desc = "Do Not Honor.There is a problem with the customer’s account or the card company doesn’t recognize the billing address on file.Try adding the cardholder’s correct billing address information and try the card one more time.";
                    break;

                case "6":
                    response = "Customer requested stop to recurring payment";
                    break;

                case "7":
                    response = "Customer requested stop to all recurring payments";
                    desc = "There is a problem with the card.  The card has been marked for fraud.  Merchant should keep the card and call the number on the back of the card to determine the issue.";
                    break;

                case "8":
                    response = "Honor with ID only";
                    desc = "Only honor the card with verification of card holder’s ID";
                    break;

                case "9":
                    response = "Unpaid items on customer account";
                    break;

                case "12":
                    response = "Invalid transaction";
                    desc = "Verify that the credit card information is input correctly and run the transaction again";
                    break;

                case "13":
                    response = "Amount Error";
                    break;

                case "14":
                    response = "Invalid card number";
                    desc = "The card number that was entered is not valid. Verify the card number and re - enter";
                    break;

                case "15":
                    response = "No such issuer.  Could not route transaction";
                    break;

                case "16":
                    response = "Refund error";
                    break;

                case "17":
                    response = "Over limit";
                    desc = "The credit card is over its allowed limit";
                    break;

                case "19":
                    response = "Reenter transaction or the merchant account may be boarded incorrectly";
                    break;

                case "25":
                    response = "Invalid terminal";
                    break;

                case "41":
                    response = "Lost card";
                    desc = "Card has been marked as lost. Fraud has been reported on the account. Merchant should keep the card and call the number on the back of the card to determine the issue.";
                    break;

                case "43":
                    response = "Stolen card";
                    desc = "Card has been marked as stolen.  Fraud has been reported on the account.  Merchant should keep the card and call the number on the back of the card to determine the issue.";
                    break;

                case "51":
                    response = "Insufficient funds";
                    desc = "There are not enough available funds on the card to complete the transaction";
                    break;

                case "52":
                    response = "No such account";
                    break;

                case "54":
                    response = "Expired card The card has expired";
                    break;

                case "55":
                    response = "Incorrect PIN";
                    break;

                case "57":
                    response = "Bank does not allow this type of purchase";
                    desc = "The card - issuing bank does not allow this type of purchase on the card.  The card holder will need to call the number on the back of the card for more information.";
                    break;

                case "58":
                    response = "Credit card network does not allow this type of purchase for your merchant account.";
                    break;

                case "61":
                    response = "Exceeds issuer withdrawal limit";
                    desc = "The card cannot be used until the withdrawal limit has been increased";
                    break;

                case "62":
                    response = "Issuer does not allow this card to be charged for your business.";
                    desc = "The card cannot be used to purchase from your business.  The credit card company does not allow purchases from your business type for this card.";
                    break;

                case "63":
                    response = "Security Violation";
                    break;

                case "65":
                    response = "Activity limit exceeded";
                    break;

                case "75":
                    response = "PIN tries exceeded";
                    break;

                case "76":
                    response = "Unable to locate account";
                    break;

                case "78":
                    response = "Account not recognized";
                    break;

                case "80":
                    response = "Invalid Date";
                    break;

                case "82":
                    response = "Invalid CVV2";
                    break;

                case "83":
                    response = "Cannot verify the PIN";
                    break;

                case "85":
                    response = "Service not supported for this card";
                    break;

                case "93":
                    response = "Cannot complete transaction.  Customer should call 800 number.";
                    desc = "The transaction cannot be completed. The customer will need to call the number on the back of the card to determine the issue.";
                    break;

                case "95":
                    response = "Misc Error Transaction failure";
                    desc = "May be due to input data.See details of < Response > tag.";
                    break;

                case "96":
                    response = "Issuer system malfunction or timeout.";
                    break;

                case "97":
                    response = "Approved for a lesser amount.  ProPay will not settle and consider this a decline.";
                    break;

                case "98":
                    response = "Failure HV";
                    break;

                case "99":
                    response = "Generic decline or unable to parse issuer response code.";
                    desc = "Additional data may be returned in the < Response > tag for International merchants";
                    break;
            }

            return new ResponseInfo() {
                Response = response,
                Desc = desc
            };
        }

        public class ResponseInfo {
            public string Response { get; set; }
            public string Desc { get; set; }
        }
    }
}
