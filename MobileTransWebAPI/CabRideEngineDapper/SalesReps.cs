﻿using CabRideEngineDapper.Utils;
using Dapper;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace CabRideEngineDapper {


	public partial class SalesReps {

		private List<SalesRepAffiliates> mSalesRepAffiliates;
		public List<SalesRepAffiliates> SalesRepAffiliateses {
			get {
				if (mSalesRepAffiliates == null) {
					mSalesRepAffiliates = SalesRepAffiliates.GetSalesRepAffiliatesBySalesRepID(this.SalesRepID);
				}
				return mSalesRepAffiliates;
			}
			set {
				mSalesRepAffiliates = value;
			}
		}


		public static SalesReps GetSalesRep(long pSalesRepID) {
			string sql = "Select * FROM SalesReps WHERE SalesRepID = @SalesRepID";
			using (var conn = SqlHelper.OpenSqlConnection()) {
				var rec = conn.Query<SalesReps>(sql, new { SalesRepID = pSalesRepID }).DefaultIfEmpty(SalesReps.GetNullEntity()).FirstOrDefault();
				//rec.RowState = System.Data.DataRowState.Unchanged;
				return rec;
			}
		}


		public static SalesReps GetSalesRep(SqlConnection pConn, string pEMail) {
			string sql = "Select * FROM SalesReps WHERE EMail = @EMail";
			var rec = pConn.Query<SalesReps>(sql, new { EMail = pEMail }).DefaultIfEmpty(SalesReps.GetNullEntity()).FirstOrDefault();
			//rec.RowState = RowState.Current;
			return rec;
		}


		public static List<SalesReps> GetAllForDropDowns(SqlConnection pConn) {
			string sql = @"SELECT SalesRepID, NAME FROM SalesReps WITH (NOLOCK)
							ORDER BY Name";

			return pConn.Query<SalesReps>(sql).ToList();
			//list.ForEach(p => p.RowState = System.Data.DataRowState.Unchanged);
			//return list;

		}
	}
}
