﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

namespace CabRideEngine {
	[Serializable]
	public sealed class Rates : RatesDataRow {

		#region Constructors -- DO NOT MODIFY
		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new'
		private Rates()
			: this(null) {
		}

		// Typed DataSet constructor (needed by framework) - DO NOT REMOVE
		public Rates(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static Rates Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      Rates aRates = (Rates) pManager.CreateEntity(typeof(Rates));
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aRates, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aRates.AddToManager();
		//      return aRates;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		// Add additional logic to your business object here...
		public static Rates Create(PersistenceManager pManager) {

			Rates rec = (Rates)pManager.CreateEntity(typeof(Rates));

			// if this object type requires a unique id and you have implemented
			// the IIdGenerator interface implement the following line
			//pManager.GenerateId(rec, RateIDEntityColumn);

			// Add custom code here
			//aRates["PUCoverageZoneID"] = pPUCoverageZoneID;
			rec.Rate = 0;
			rec.DropFee = 0;
			rec.Airport = 0;
			rec.VoucherPct = 0;
			rec.Voucher = 0;
			rec.Tolls = 0;
			rec.TollsPct = 0;
			rec.ShortRide = 0;
			rec.LongRide = 0;
			rec.RushHour = 0;
			rec.TipPct = 0;
			rec.Tip = 0;
			rec.TaxiPass = 0;
			rec.TaxiPassFactor = 0;
			rec.MinShortRide = 0;

			rec.VehicleTypeID = VehicleTypes.GetTaxi(pManager).VehicleTypeID;

			rec.AddToManager();
			return rec;
		}

		public override long DOCoverageZoneID {
			get {
				return base.DOCoverageZoneID;
			}
			set {
				// for accounts don't check at this stage as they have multiple vehicle types for same coverage area
				if (!AccountID.HasValue) {
					//validate that this would be a unique
					RdbQuery oQry = new RdbQuery(typeof(Rates), PUCoverageZoneIDEntityColumn, EntityQueryOp.EQ, PUCoverageZoneID);
					oQry.AddClause(DOCoverageZoneIDEntityColumn, EntityQueryOp.EQ, value);
					oQry.AddClause(RateIDEntityColumn, EntityQueryOp.NE, RateID);
					//if (AccountID.HasValue) {
					//	oQry.AddClause(AccountIDEntityColumn, EntityQueryOp.EQ, AccountID);
					//} else {
						oQry.AddClause(AccountIDEntityColumn, EntityQueryOp.IsNull, true);
					//}
					if (this.PersistenceManager.GetEntities<Rates>(oQry).Count > 0) {
						throw new Exception("Drop Off Zone must be unique");
					}
				}
				base.DOCoverageZoneID = value;
			}
		}

		//protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
		//	if (IsDeserializing)
		//		return;
		//	base.OnColumnChanging(pArgs);
		//	DoAudit(pArgs);
		//}

		//private void DoAudit(DataColumnChangeEventArgs pArgs) {
		//	if (!this.PersistenceManager.IsClient) {
		//		return;
		//	}
		//	if (this.RowState == DataRowState.Added) {
		//		return;
		//	}
		//	if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
		//		AuditRecord = RatesAudit.Create(this.PersistenceManager, this);
		//	}
		//	//AuditRecord[pArgs.Column.ColumnName] = pArgs.ProposedValue;
		//}

		public static EntityList<Rates> GetRates(PersistenceManager pPM, long pPUZone, long pDOZone) {
			RdbQuery oQry = new RdbQuery(typeof(Rates), Rates.PUCoverageZoneIDEntityColumn, EntityQueryOp.EQ, pPUZone);
			oQry.AddClause(Rates.DOCoverageZoneIDEntityColumn, EntityQueryOp.EQ, pDOZone);

			return pPM.GetEntities<Rates>(oQry);
		}

		public static EntityList<Rates> GetRates(PersistenceManager pPM, long pPUZone) {
			RdbQuery oQry = new RdbQuery(typeof(Rates), Rates.PUCoverageZoneIDEntityColumn, EntityQueryOp.EQ, pPUZone);
			return pPM.GetEntities<Rates>(oQry);
		}

		public static bool UpdateRates(PersistenceManager pPM, long pAffiliateID, decimal pAmount, string pRateType) {
			pRateType = pRateType.ToLower();

			Utils.ProgressForm oForm = new CabRideEngine.Utils.ProgressForm();
			oForm.Show();

			Affiliate oAffiliate = Affiliate.GetAffiliate(pPM, pAffiliateID);
			oForm.Max = oAffiliate.AffiliateCoverageZoneses.Count;
			int i = 0;
			foreach (AffiliateCoverageZones oZone in oAffiliate.AffiliateCoverageZoneses) {
				oForm.Message = "Processing Zone: " + oZone.CoverageZones.CoverageZone;
				oForm.Value++;
				foreach (Rates oRate in oZone.CoverageZones.Rateses) {
					if (oRate.RateType.ToLower().Trim() == pRateType) {
						if (oRate.RowState == DataRowState.Unchanged) {
							oRate.Rate += pAmount;
							i++;
						}
					}

				}
			}



			//EntityList<Rates> oRecs = pPM.GetEntities<Rates>();
			//foreach (Rates oRate in oRecs) {
			//    if (oRate.RateType.ToLower().Trim() == pRateType) {
			//        foreach (AffiliateCoverageZones oAffiliate in oRate.CoverageZones.AffiliateCoverageZoneses) {
			//            if (oAffiliate.AffiliateID == pAffiliateID) {
			//                oRate.Rate += pAmount;
			//                break;
			//            }
			//        }
			//    }
			//}
			try {
				EntityList<Rates> changed = pPM.GetEntities<Rates>(DataRowState.Modified);
				pPM.SaveChanges();
			} catch (PersistenceManagerSaveException ex) {
				throw ex;
			} finally {
				oForm.Close();
			}
			return true;
		}

	}
}
