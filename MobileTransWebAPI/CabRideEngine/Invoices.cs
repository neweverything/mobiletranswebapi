﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Util;
using IdeaBlade.Persistence.Rdb;

namespace CabRideEngine {
    /// <summary>
    /// The developer class representing the Invoices business object class.
    /// </summary>
    /// <remarks>
    /// <para>Place all application-specific business logic in this class.</para>
    /// <para>
    /// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
    /// and will not overwrite changes.
    /// </para>
    /// <para>
    /// This class inherits from the generated class, its companion in the pair of classes 
    /// defining a business object. Properties in the generated class may be overridden in 
    /// order to change the behavior or add business logic.
    /// </para>
    /// <para>
    /// This class is the final class in the inheritance chain for this business object. 
    /// It should not be inherited.
    /// </para>
    /// </remarks>
    [Serializable]
    public sealed partial class Invoices : InvoicesDataRow {

        #region Constructors -- DO NOT MODIFY

        // Do not create constructors for this class
        // Developers cannot create instances of this class with the "new" keyword
        // You should write a static Create method instead; see the "Suggested Customization" region
        // Please review the documentation to learn about entity creation and inheritance

        // This private constructor prevents accidental instance creation via 'new';
        // it is also required if the class is visible from a web service.
        // DO NOT REMOVE **
        private Invoices()
            : this(null) {
        }

        /// <summary>
        /// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
        /// </summary>
        /// <param name="pRowBuilder"></param>
        public Invoices(DataRowBuilder pRowBuilder)
            : base(pRowBuilder) {
        }

        #endregion

        #region Suggested Customizations

        //    // Use this factory method template to create new instances of this class
        //    public static Invoices Create(PersistenceManager pManager,
        //      ... your creation parameters here ...) { 
        //
        //      Invoices aInvoices = pManager.CreateEntity<Invoices>();
        //
        //      // if this object type requires a unique id and you have implemented
        //      // the IIdGenerator interface implement the following line
        //      pManager.GenerateId(aInvoices, // add id column here //);
        //
        //      // Add custom code here
        //
        //      aInvoices.AddToManager();
        //      return aInvoices;
        //    }

        //    // Implement this method if you want your object to sort in a specific order
        //    public override int CompareTo(Object pObject) {
        //    }

        //    // Implement this method to customize the null object version of this class
        //    protected override void UpdateNullEntity() {
        //    }

        #endregion

        // Add additional logic to your business object here...
        public static Invoices Create(Affiliate pAffiliate) {
            Invoices oInvoice = null;
            PersistenceManager oPM = pAffiliate.PersistenceManager;
            try {
                oInvoice = (Invoices)oPM.CreateEntity(typeof(Invoices));

                // Using the IdeaBlade Id Generation technique
                oPM.GenerateId(oInvoice, Invoices.InvoiceIDEntityColumn);
                oInvoice.AffiliateID = pAffiliate.AffiliateID;
                oInvoice.AddToManager();

            } catch (Exception ex) {
                throw ex;
            }
            return oInvoice;
        }

        public static Invoices Create(PersistenceManager pPM) {
            Invoices oInvoice = null;
            try {
                oInvoice = (Invoices)pPM.CreateEntity(typeof(Invoices));
                pPM.GenerateId(oInvoice, Invoices.InvoiceIDEntityColumn);
                oInvoice.AddToManager();

            } catch (Exception ex) {
                throw ex;
            }
            return oInvoice;
        }

        protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
            if (IsDeserializing) {
                return;
            }
            base.OnColumnChanging(pArgs);
            DoAudit(pArgs);
        }

        private void DoAudit(DataColumnChangeEventArgs pArgs) {
            if (!this.PersistenceManager.IsClient) {
                return;
            }
            if (this.RowState == DataRowState.Added) {
                return;
            }
            if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
                AuditRecord = InvoicesAudit.Create(this.PersistenceManager, this);
            }
        }

        public decimal ActualAffiliateAmount {
            get {
                decimal amount = 0;
                foreach (Reservations oRes in this.Reservationses) {
                    amount += oRes.AffiliateTotal;
                }
                return amount;
            }
        }


        public static EntityList<Invoices> GetInvoices(PersistenceManager pPM, DateTime pStartDate, DateTime pEndDate) {
            return GetInvoices(pPM, pStartDate, pEndDate, -1);
        }

        public static EntityList<Invoices> GetInvoices(PersistenceManager pPM, DateTime pStartDate, DateTime pEndDate, long pAffiliateID) {

            EntityQuery qry = new EntityQuery(typeof(Invoices), Invoices.InvoiceDateEntityColumn, EntityQueryOp.GE, pStartDate.Date);
            qry.AddClause(InvoiceDateEntityColumn, EntityQueryOp.LT, pEndDate.Date.AddDays(1));
            if (pAffiliateID != -1) {
                qry.AddClause(Invoices.AffiliateIDEntityColumn, EntityQueryOp.LT, pAffiliateID);
            }
            qry.AddSpan(EntityRelations.Invoices_Reservations);
            return pPM.GetEntities<Invoices>(qry, QueryStrategy.DataSourceThenCache);
        }


    }

    #region EntityPropertyDescriptors.InvoicesPropertyDescriptor
    ////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
    ////    For example: 
    ////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
    //public static partial class EntityPropertyDescriptors {
    //	public partial class InvoicesPropertyDescriptor : BaseEntityPropertyDescriptor {
    //    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
    //    public AdaptedPropertyDescriptor Age {
    //      get { return this.Get("Age"); }
    //    }
    //  }
    //} 
    #endregion

}
