﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using TaxiPassCommon.Banking.Enums;
using TaxiPassCommon.Banking.Interfaces;
using TaxiPassCommon.Banking.Models.GlobalCashCard;

namespace TaxiPassCommon.Banking {
	public class GlobalCashCard : IPayCard {

		//com.GlobalCashCard.GCCAPISERVICE mService = new com.GlobalCashCard.GCCAPISERVICE();

		private GlobalCashCardCredentials mCredentials;
		private bool mProduction;


		public GlobalCashCard(IPayCardCredentials pCredentials, bool pProduction = true) {
			ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls;


			mCredentials = JsonConvert.DeserializeObject<GlobalCashCardCredentials>(JsonConvert.SerializeObject(pCredentials));
			mProduction = pProduction;
			mCredentials.UseProduction = pProduction;


		}

		public string Url {
			get {
				string url = mCredentials.TestURL;
				if (mProduction) {
					url = mCredentials.ProdURL;
				}
				return url;
			}
		}


		public string UserName {
			get {
				return mProduction ? mCredentials.UserName : mCredentials.TestUserName;
			}
		}

		public string Password {
			get {
				return mProduction ? mCredentials.Password : mCredentials.TestPassword;
			}
		}


		public string ClientID {
			get {
				return mCredentials.Certification;
			}
		}

		public string ClientSecret {
			get {
				return mCredentials.PassCode;
			}
		}

		public FSV.CardRegistrationResponse RegisterCard(FSV.CardRegistration pRegistration) {
			FSV.CardRegistrationResponse regResponse = new FSV.CardRegistrationResponse();

			CardHolder holder = new CardHolder {
				username = UserName,
				password = Password,
				address = pRegistration.Address1,
				city = pRegistration.City,
				country = pRegistration.Country.IsNullOrEmpty() ? "US" : pRegistration.Country,
				dob = GetDate(pRegistration.DateOfBirth),
				email = pRegistration.EmailAddress,
				firstname = pRegistration.FirstName,
				govid = pRegistration.GovIDValue,
				govidissuer = pRegistration.State,
				govidtype = "USDRIVE",
				govid_expdate = GetDate(pRegistration.GovExpire),
				govid_issuedate = GetDate(pRegistration.GovIssued),
				lastname = pRegistration.LastName,
				initial = (pRegistration.MiddleName + " ").Left(1).Trim(),
				phone = pRegistration.MobilePhone,
				state = pRegistration.State,
				zipcode = pRegistration.PhysicalZip,
				cardnumber = pRegistration.Reserved1,
				custid = pRegistration.Reference.Split(' ').Last(),

			};
			//if (holder.govid_issuedate.Year < 2000) {
			//	holder.govid_issuedate = null;
			//}
			OAuthToken token = GetToken();

			if (!token.error.IsNullOrEmpty()) {
				regResponse.ReturnMessage = token.error;
				regResponse.ReturnCode = "99";
				return regResponse;
			}


			CardHolderResults cardHolderResults = GetCardHolder(token, holder.custid);


			var client = new RestClient(Url);
			//var request = new RestRequest("addAtm", Method.POST);
			RestRequest request;
			if (cardHolderResults.RESPONSECODE == "00" && cardHolderResults.COUNT > 0) {
				request = new RestRequest("updateCardholder", Method.POST);
			} else {
				request = new RestRequest("addCardholder", Method.POST);
			}
			request.AddHeader("cache-control", "no-cache");
			request.AddHeader("content-type", "application/x-www-form-urlencoded; charset=UTF-8");
			request.AddHeader("Authorization", $"Bearer {token.access_token}");

			request.AddParameter("username", UserName);
			request.AddParameter("password", Password);
			request.AddParameter("keyfield", holder.custid);
			request.AddParameter("accountnumber", pRegistration.CardHolderCardID);
			request.AddParameter("cardnumber", pRegistration.Reserved1);
			request.AddParameter("firstname", pRegistration.FirstName);
			request.AddParameter("initial", (pRegistration.MiddleName + " ").Left(1).Trim());
			request.AddParameter("lastname", pRegistration.LastName);
			request.AddParameter("dob", GetDate(pRegistration.DateOfBirth));
			request.AddParameter("govid", pRegistration.SocialSecNum); // pRegistration.GovIDValue);
			request.AddParameter("govidtype", "SSN");
			request.AddParameter("govidissuer", pRegistration.State);
			//request.AddParameter("govid_expdate", GetDate(pRegistration.GovExpire));
			request.AddParameter("address", pRegistration.Address1);
			request.AddParameter("city", pRegistration.City);
			request.AddParameter("state", pRegistration.State);
			request.AddParameter("zipcode", pRegistration.PhysicalZip);
			request.AddParameter("country", pRegistration.Country);
			request.AddParameter("phone", pRegistration.MobilePhone);
			request.AddParameter("custid", pRegistration.Reference.Split(' ').Last());
			//request.AddParameter("un", pRegistration.Reserved10);
			//request.AddParameter("pw", pRegistration.Reserved11);
			request.AddParameter("email", pRegistration.EmailAddress);

			string content = "";
			IRestResponse<RegisterCardHolderResponse> response = null;
			while (content.IsNullOrEmpty()) {
				response = client.Execute<RegisterCardHolderResponse>(request);
				Console.WriteLine(response.Content);
				content = response.Content;
				if (content.IsNullOrEmpty()) {
					System.Threading.Thread.Sleep(1000);
				}
			}

			regResponse.ReturnMessage = response.Data.DESCRIPTION.IsNullOrEmpty() ? response.Data.STATUS : response.Data.DESCRIPTION;
			regResponse.ReturnCode = response.Data.RESPONSECODE.Equals("00") && regResponse.ReturnMessage.Equals("success", StringComparison.CurrentCultureIgnoreCase) ? "1" : response.Data.RESPONSECODE;
			if (response.Data.RESULTS != null && response.Data.RESULTS.Count > 0) {
				regResponse.CardID = pRegistration.CardHolderCardID;
				//regResponse.ATTMID = JsonConvert.SerializeObject(response);
				regResponse.PayToNumber = pRegistration.Reserved1.Encrypt();
				regResponse.PayCardType = mCredentials.MyPayCardType().ToString();

				if (response.Data.RESULTS[0].CARDNUMBER.Right(4) != pRegistration.Reserved1.Right(4)) {
					var replaceResults = ReplaceCard(token, pRegistration, holder, cardHolderResults);
					if (replaceResults.RESPONSECODE == "00") {

					}
				}
			} else if (regResponse.ReturnCode.Equals("1") && !regResponse.ReturnMessage.Equals("success", StringComparison.CurrentCultureIgnoreCase)) {
				regResponse.ReturnCode = $"-{regResponse.ReturnCode}";
			}



			return regResponse;
		}

		private ReplaceCardResults ReplaceCard(OAuthToken token, FSV.CardRegistration pRegistration, CardHolder holder, CardHolderResults cardHolderResults) {
			var client = new RestClient(Url);
			var request = new RestRequest("replaceCard", Method.POST);
			request.AddHeader("cache-control", "no-cache");
			request.AddHeader("content-type", "application/x-www-form-urlencoded; charset=UTF-8");
			request.AddHeader("Authorization", $"Bearer {token.access_token}");

			request.AddParameter("username", UserName);
			request.AddParameter("password", Password);
			request.AddParameter("un", pRegistration.Reserved10);
			request.AddParameter("keyfield", holder.custid);
			request.AddParameter("cardnumber", cardHolderResults.RESULTS[0].CARDNUMBER);
			request.AddParameter("newcardnumber", pRegistration.Reserved1);
			var response = client.Execute<ReplaceCardResults>(request);

			if (response.Data == null) {
				try {
					response.Data = JsonConvert.DeserializeObject<ReplaceCardResults>(response.Content);
				} catch (Exception ex) {
					Console.WriteLine(ex.Message);
				}
			}
			return response.Data;
		}

		private static DateTime GetDate(string pDate) {
			if (pDate.IsNullOrEmpty()) {
				return new DateTime(1900, 1, 1);
			}
			int year = pDate.Left(4).ToInt();
			int month = pDate.Substring(4, 2).ToInt();
			int day = pDate.Right(2).ToInt();
			return new DateTime(year, month, day);
		}

		public OAuthToken GetToken() {
			var client = new RestClient(mCredentials.TokenURL);
			var request = new RestRequest(Method.POST);
			request.AddHeader("cache-control", "no-cache");
			request.AddHeader("content-type", "application/x-www-form-urlencoded");
			string cred = $"grant_type=client_credentials&client_id={ClientID}&client_secret={ClientSecret}";
			//string cred = $"grant_type=client_credentials&client_id=a5588bd088f64d8983335435659a01bf&client_secret=a8ee29ffdaf24360adb8d81bd0931ac8";
			request.AddParameter("application/x-www-form-urlencoded", cred, ParameterType.RequestBody);
			//request.AddBody(cred);
			var response = client.Execute<OAuthToken>(request);

			return response.Data;
		}

		public FundsTransferResponse TransferFromPayCard(string pPayCardNo, FSV.FundsTransfer pFunds) {
			OAuthToken token = GetToken();
			var client = new RestClient(Url);
			var request = new RestRequest("debitCard", Method.POST);
			request.AddHeader("cache-control", "no-cache");
			request.AddHeader("content-type", "application/x-www-form-urlencoded; charset=UTF-8");
			request.AddHeader("Authorization", $"Bearer {token.access_token}");

			request.AddParameter("username", UserName);
			request.AddParameter("password", Password);
			request.AddParameter("accountnumber", pFunds.ToCardID);
			request.AddParameter("net", pFunds.TransferAmount);
			request.AddParameter("description", pFunds.ReferenceFromCard);

			request.AddParameter("keyfield", pFunds.Reserved1);

			var response = client.Execute<LoadCardResults>(request);
			Console.WriteLine(response.Content);

			FundsTransferResponse resp = new FundsTransferResponse();
			resp.ReturnCode = response.Data.RESPONSECODE;
			resp.ReturnMessage = response.Data.STATUS;
			if (response.Data.RESPONSECODE.Equals("00") && response.Data.STATUS.Equals("success", StringComparison.CurrentCultureIgnoreCase)) {
				resp.ReturnCode = "1";
				resp.FromCardBalance = GetFundingBalance().ToString("F2");
			} else if (resp.ReturnCode.Equals("00")) {
				resp.ReturnCode = $"1{resp.ReturnCode}";
			}

			if (response.Data.RESULTS != null) {
				var result = response.Data.RESULTS;
				resp.Unused1 = result.TRANSID;
				resp.Unused2 = result.PAYROLLID;
			}


			return resp;
		}

		public FundsTransferResponse TransferToPayCard(FSV.FundsTransfer pFunds) {
			//TransferFromPayCard("", pFunds);
			OAuthToken token = GetToken();
			var client = new RestClient(Url);
			//var request = new RestRequest("addAtm", Method.POST);
			var request = new RestRequest("loadCard", Method.POST);
			request.AddHeader("cache-control", "no-cache");
			request.AddHeader("content-type", "application/x-www-form-urlencoded; charset=UTF-8");
			request.AddHeader("Authorization", $"Bearer {token.access_token}");

			request.AddParameter("username", UserName);
			request.AddParameter("password", Password);
			request.AddParameter("accountnumber", pFunds.ToCardID);
			request.AddParameter("net", pFunds.TransferAmount);
			request.AddParameter("description", pFunds.ReferenceFromCard);

			request.AddParameter("keyfield", pFunds.Reserved1);

			var response = client.Execute<Models.GlobalCashCard.LoadCardResults>(request);
			Console.WriteLine(response.Content);

			FundsTransferResponse resp = new FundsTransferResponse();
			resp.ReturnCode = response.Data.RESPONSECODE;
			resp.ReturnMessage = response.Data.STATUS;
			if (!response.Data.DESCRIPTION.IsNullOrEmpty()) {
				resp.ReturnMessage = response.Data.DESCRIPTION;
			}
			if (response.Data.RESPONSECODE.Equals("00") && response.Data.STATUS.Equals("success", StringComparison.CurrentCultureIgnoreCase)) {
				resp.ReturnCode = "1";
			} else if (resp.ReturnCode.Equals("00")) {
				resp.ReturnCode = $"1{resp.ReturnCode}";
			}

			if (response.Data.RESULTS != null) {
				var result = response.Data.RESULTS;
				resp.Unused1 = result.TRANSID;
				resp.Unused2 = result.PAYROLLID;
			}

			try {
				resp.FromCardBalance = GetFundingBalance().ToString("F2");
			} catch (Exception) {
			}

			return resp;
		}

		public FSV.VerifyConnectionResponse VerifyConnection() {
			return new FSV.VerifyConnectionResponse {
				ReturnCode = "1",
				ReturnMessage = ""
			};
		}

		public decimal GetFundingBalance() {

			OAuthToken token = GetToken();
			var client = new RestClient(Url);
			//var request = new RestRequest("addAtm", Method.POST);
			var request = new RestRequest("getFundBalance", Method.POST);
			request.AddHeader("cache-control", "no-cache");
			request.AddHeader("content-type", "application/x-www-form-urlencoded; charset=UTF-8");
			request.AddHeader("Authorization", $"Bearer {token.access_token}");

			request.AddParameter("username", UserName);
			request.AddParameter("password", Password);
			var response = client.Execute<FundBalanceResult>(request);
			if (response.Data != null && response.Data.RESPONSECODE.Equals("00")) {
				return response.Data.RESULTS.BALANCE.AsDecimal();
			}
			return 0;
		}

		public CardHolderResults GetCardHolder(OAuthToken pToken, string pAffiliateDriverID) {
			var client = new RestClient(Url);
			//var request = new RestRequest("addAtm", Method.POST);
			var request = new RestRequest("getCardholder", Method.POST);
			request.AddHeader("cache-control", "no-cache");
			request.AddHeader("content-type", "application/x-www-form-urlencoded; charset=UTF-8");
			request.AddHeader("Authorization", $"Bearer {pToken.access_token}");

			request.AddParameter("username", UserName);
			request.AddParameter("password", Password);
			request.AddParameter("keyfield", pAffiliateDriverID);

			var response = client.Execute<CardHolderResults>(request);
			//Console.WriteLine(response.Content);

			if (response.Data != null) {
				return response.Data;
			}
			return new CardHolderResults();
		}

		public TransHistory GetHistory(string pAffiliateDriverID) {

			OAuthToken token = GetToken();

			var client = new RestClient(Url);
			//var request = new RestRequest("addAtm", Method.POST);
			var request = new RestRequest("getHistory", Method.POST);
			request.AddHeader("cache-control", "no-cache");
			request.AddHeader("content-type", "application/x-www-form-urlencoded; charset=UTF-8");
			request.AddHeader("Authorization", $"Bearer {token.access_token}");

			request.AddParameter("username", UserName);
			request.AddParameter("password", Password);
			request.AddParameter("keyfield", pAffiliateDriverID);

			var response = client.Execute<TransHistory>(request);
			//Console.WriteLine(response.Content);

			if (response.Data != null) {
				return response.Data;
			}
			return new TransHistory();
		}


		public decimal GetBalance(string pAffiliateDriverID) {
			decimal balance = 0;
			OAuthToken token = GetToken();

			var client = new RestClient(Url);
			//var request = new RestRequest("addAtm", Method.POST);
			var request = new RestRequest("getBalance", Method.POST);
			request.AddHeader("cache-control", "no-cache");
			request.AddHeader("content-type", "application/x-www-form-urlencoded; charset=UTF-8");
			request.AddHeader("Authorization", $"Bearer {token.access_token}");

			request.AddParameter("username", UserName);
			request.AddParameter("password", Password);
			request.AddParameter("keyfield", pAffiliateDriverID);

			var response = client.Execute(request);
			Console.WriteLine(response.Content);

			//if (response.Data != null) {
			//	return response.Data;
			//}
			return balance;
		}



		public class GlobalCashCardCredentials : IPayCardCredentials {
			public string ProdURL { get; set; }
			public string UserName { get; set; }
			public string Password { get; set; }

			public string TestURL { get; set; }
			public string TestUserName { get; set; }
			public string TestPassword { get; set; }

			public string Certification { get; set; }
			public string PassCode { get; set; }

			public string TokenURL { get; set; }


			public bool UseProduction { get; set; }

			public bool UseProductionAsBoolen {
				get {
					return UseProduction;
				}
			}

			public PayCardTypes MyPayCardType() {
				return PayCardTypes.GlobalCashCard;
			}


		}

		public class CardHolder {
			public string username { get; set; }
			public string password { get; set; }


			public string cardnumber { get; set; }
			public string custid { get; set; }

			public string firstname { get; set; }
			public string initial { get; set; }
			public string lastname { get; set; }
			public DateTime dob { get; set; }
			public string govid { get; set; }
			public string govidtype { get; set; }
			public string govidissuer { get; set; }
			public DateTime govid_issuedate { get; set; }
			public DateTime govid_expdate { get; set; }
			public string address { get; set; }
			public string city { get; set; }
			public string state { get; set; }
			public string zipcode { get; set; }
			public string country { get; set; }
			public string phone { get; set; }
			public string un { get; set; }
			public string pw { get; set; }
			public string email { get; set; }
		}

		public class RegisterCardHolderResponse {
			public string RESPONSECODE { get; set; }
			public string STATUS { get; set; }
			public string DESCRIPTION { get; set; }

			public List<RESULT> RESULTS { get; set; }

			public class RESULT {
				public string GOVID { get; set; }
				public string GOVIDTYPE { get; set; }
				public long PHONE { get; set; }
				public int ZIPCODE { get; set; }
				public string INITIAL { get; set; }
				public string STATE { get; set; }
				public string FIRSTNAME { get; set; }
				public string EMAIL { get; set; }
				public string CITY { get; set; }
				public string COUNTRY { get; set; }
				public string CARDNUMBER { get; set; }
				public string LASTNAME { get; set; }
				public string DOB { get; set; }
				public string ADDRESS { get; set; }
				public long KEYFIELD { get; set; }
				public string TRACKNUM { get; set; }
				public string GOVIDISSUER { get; set; }
			}
		}

		public class OAuthToken {
			public string access_token { get; set; }
			public string token_type { get; set; }
			public int expires_in { get; set; }
			public string error { get; set; }
		}
	}
}
