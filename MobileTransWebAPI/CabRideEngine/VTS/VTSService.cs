﻿using IdeaBlade.Persistence;
using IdeaBlade.Persistence.Rdb;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using TaxiPassCommon;
using TaxiPassCommon.Entities;


namespace CabRideEngine.VTS {
	public class VTSService {

		private string mUserID;
		private string mPassword;
		private string mMerchantID;

		private org.taxitronic.taxiService.Taxi_Service mService;
		private org.taxitronic.vtsService.Service mVtsService;

		public List<string> transErrors = new List<string>();

		private TaxiPassCommon.TimeZoneConverter mTimeZoneConverter = new TaxiPassCommon.TimeZoneConverter();

		public VTSService(Affiliate pAffiliate) {
			mService = new org.taxitronic.taxiService.Taxi_Service();
			mVtsService = new org.taxitronic.vtsService.Service();

			mUserID = pAffiliate.AffiliateDriverDefaults.TaxiTronicUserName;
			mPassword = pAffiliate.AffiliateDriverDefaults.TaxiTronicPassword;
			mMerchantID = pAffiliate.AffiliateDriverDefaults.TaxiTronicMerchant;
		}


		public System.Data.DataSet GetTransactionsAsDataSet(DateTime pStartDate, DateTime pEndDate, string pCabNumber) {

			string xmlData = "";
			try {
				xmlData = mService.Get_Trans_By_Date_Cab(mUserID,
															 mPassword,
															 mMerchantID,
															 pStartDate.ToString(),
															 pEndDate.ToString(),
															 pCabNumber);
				if (!xmlData.IsNullOrEmpty()) {
					xmlData = xmlData.Replace("&", "_");
				}
			} catch (Exception ex) {
				throw new Exception("VTS Error getting Transactions: " + ex.Message);
			}

			try {
				XMLConverter xml = new XMLConverter();
				return xml.ConvertXMLToDataSet(xmlData);
			} catch {
				throw new Exception(xmlData);
			}

		}

		public List<Tran> GetTransactions(DateTime pStartDate, DateTime pEndDate, string pCabNumber) {

			DataSet oDS = GetTransactionsAsDataSet(pStartDate, pEndDate, pCabNumber);
			if (oDS.Tables.Count == 0) {
				return new List<Tran>();
			}
			List<Tran> recList = (from p in oDS.Tables[0].AsEnumerable()
								  select new Tran {
									  RowID = p.Field<string>("ROWID"),
									  IPNumber = p.Field<string>("IPNUMBER"),
									  DateTime = Convert.ToDateTime(p.Field<string>("DATETIME")),
									  CardNumber = p.Field<string>("CRNUMBER"),
									  Amount = Convert.ToDecimal(p.Field<string>("AMOUNT")),
									  CabNumber = p.Field<string>("CABNUMBER"),
									  Note = p.Field<string>("NOTE"),
									  TripDate = p.Field<string>("TRIPDATE").IsNullOrEmpty() ? new DateTime(2000, 1, 1) : Convert.ToDateTime(p.Field<string>("TRIPDATE")),
									  TripTimeStart = p.Field<string>("TRIPDATE").IsNullOrEmpty() ? new DateTime(2000, 1, 1) : Convert.ToDateTime(p.Field<string>("TRIPDATE") + " " + p.Field<string>("TRIPTIMESTART")),
									  TripTimeEnd = p.Field<string>("TRIPDATE").IsNullOrEmpty() ? new DateTime(2000, 1, 1) : Convert.ToDateTime(p.Field<string>("TRIPDATE") + " " + p.Field<string>("TRIPTIMEEND")),
									  TripFare = Convert.ToDecimal(p.Field<string>("TRIPFARE")),
									  TripTrips = Convert.ToDecimal(p.Field<string>("TRIPTRIPS")),
									  TripExtras = Convert.ToDecimal(p.Field<string>("TRIPEXTRAS")),
									  TripTips = Convert.ToDecimal(p.Field<string>("TRIPTIPS")),
									  TripDist = Convert.ToDecimal(p.Field<string>("TRIPDIST")),
									  TripToll = Convert.ToDecimal(p.Field<string>("TRIPTOLL")),
									  TripDriverID = p.Field<string>("TRIPDRIVERID"),
									  StoreForward = p.Field<string>("storeforward"),
									  NumService = p.Field<string>("NUM_SERVICE"),
									  CardType = p.Field<string>("CC_TYPE"),
									  BankApproval = p.Field<string>("BANK_APPROVAL")
								  }).ToList<Tran>();

			return recList;
		}

		//public DataSet GetUnReconciledTransactions() {

		//    string xmlData = mVTSService.Get_Trans_UnReconcile(mUserID,
		//                                                    mPassword,
		//                                                    mMerchantID,
		//                                                    "DUP");
		//    if (!xmlData.IsNullOrEmpty()) {
		//        xmlData = xmlData.Replace("&", "_");
		//    }
		//    int i = 1;
		//    int j = 0;
		//    string sFind = "<BANK_ACTIONCODE>";
		//    while (i > 0) {
		//        i = xmlData.IndexOf(sFind, i, StringComparison.CurrentCultureIgnoreCase);
		//        if (i > 0) {
		//            j = xmlData.IndexOf(sFind, i + 2, StringComparison.CurrentCultureIgnoreCase);
		//            xmlData = xmlData.Substring(0, i) + xmlData.Substring(j + sFind.Length);
		//        }
		//    }
		//    xmlData = xmlData.Replace("<", System.Environment.NewLine + "<");

		//    System.IO.File.WriteAllText(@"g:\temp\unrec.xml", xmlData);
		//    //convert XML Data to data table
		//    if (xmlData.ToUpper().Contains("ERROR") && !xmlData.ToUpper().Contains("GOWANS")) {
		//        throw new Exception(xmlData);
		//    }
		//    XMLConverter xml = new XMLConverter();
		//    return xml.ConvertXMLToDataSet(xmlData);

		//}

		//public DataSet GetDupeTrans(DateTime pStartDate, DateTime pEndDate, string pCabNumber) {

		//    string xmlData = "";
		//    try {
		//        xmlData = mVTSService.Get_Trans_By_Date_Cab(mUserID,
		//                                                     mPassword,
		//                                                     mMerchantID,
		//                                                     pStartDate.ToString(),
		//                                                     pEndDate.ToString(),
		//                                                     pCabNumber,
		//                                                     "DUP");
		//        if (!xmlData.IsNullOrEmpty()) {
		//            xmlData = xmlData.Replace("&", "_");
		//        }
		//    } catch (Exception ex) {
		//        throw new Exception("VTS Error getting Dupe Trans: " + ex.Message);
		//    }

		//    //convert XML Data to data table
		//    //if (xmlData.ToUpper().Contains("ERROR") && !xmlData.ToUpper().Contains("GOWANS")) {
		//    //    throw new Exception(xmlData);
		//    //}
		//    try {
		//        XMLConverter xml = new XMLConverter();
		//        return xml.ConvertXMLToDataSet(xmlData);
		//    } catch {
		//        throw new Exception(xmlData);
		//    }

		//}

		public List<Tran> GetDeclinedStoreForwardTrans(DateTime pStartDate, DateTime pEndDate, string pCabNumber) {

			string xmlData = "";
			try {
				xmlData = mVtsService.Get_Trans_By_Date_Cab(mUserID,
																mPassword,
																mMerchantID,
																pStartDate.ToString(),
																pEndDate.ToString(),
																pCabNumber,
																"DC");
				if (!xmlData.IsNullOrEmpty()) {
					xmlData = xmlData.Replace("&", "_");
				}
			} catch (Exception ex) {
				throw new Exception("VTS Error getting Declined Store Forward Trans: " + ex.Message);
			}

			//convert XML Data to data table
			try {
				XMLConverter xml = new XMLConverter();
				DataSet oDS = xml.ConvertXMLToDataSet(xmlData);

				if (oDS.Tables == null || oDS.Tables.Count == 0) {
					return new List<Tran>();
				}

				List<Tran> recList = (from p in oDS.Tables[0].AsEnumerable()
									  where !p.Field<string>("BANK_ACTIONCODE").Equals("094") && p.Field<string>("storeforward").Equals("Y", StringComparison.CurrentCultureIgnoreCase)
									  select new Tran {
										  RowID = p.Field<string>("ROWID"),
										  IPNumber = p.Field<string>("IPNUMBER"),
										  DateTime = Convert.ToDateTime(p.Field<string>("DATETIME")),
										  CardNumber = p.Field<string>("CRNUMBER"),
										  Amount = Convert.ToDecimal(p.Field<string>("AMOUNT")),
										  CabNumber = p.Field<string>("CABNUMBER"),
										  Note = p.Field<string>("NOTE"),
										  TripDate = p.Field<string>("TRIPDATE").IsNullOrEmpty() ? new DateTime(2000, 1, 1) : Convert.ToDateTime(p.Field<string>("TRIPDATE")),
										  TripTimeStart = p.Field<string>("TRIPDATE").IsNullOrEmpty() ? new DateTime(2000, 1, 1) : Convert.ToDateTime(p.Field<string>("TRIPDATE") + " " + p.Field<string>("TRIPTIMESTART")),
										  TripTimeEnd = p.Field<string>("TRIPDATE").IsNullOrEmpty() ? new DateTime(2000, 1, 1) : Convert.ToDateTime(p.Field<string>("TRIPDATE") + " " + p.Field<string>("TRIPTIMEEND")),
										  TripFare = Convert.ToDecimal(p.Field<string>("TRIPFARE")),
										  TripTrips = Convert.ToDecimal(p.Field<string>("TRIPTRIPS")),
										  TripExtras = Convert.ToDecimal(p.Field<string>("TRIPEXTRAS")),
										  TripTips = Convert.ToDecimal(p.Field<string>("TRIPTIPS")),
										  TripDist = Convert.ToDecimal(p.Field<string>("TRIPDIST")),
										  TripToll = Convert.ToDecimal(p.Field<string>("TRIPTOLL")),
										  TripDriverID = p.Field<string>("TRIPDRIVERID"),
										  StoreForward = p.Field<string>("storeforward"),
										  NumService = p.Field<string>("NUM_SERVICE"),
										  CardType = p.Field<string>("CC_TYPE"),
										  BankApproval = p.Field<string>("BANK_APPROVAL"),
										  BankActionCode = p.Field<string>("BANK_ACTIONCODE"),
										  Tax = Convert.ToDecimal(p.Field<string>("TAX"))
									  }).ToList<Tran>();

				return recList;
			} catch {
				throw new Exception(xmlData);
			}

		}

		public List<Tran> GetDupeTransNew(DateTime pStartDate, DateTime pEndDate, string pCabNumber) {

			string xmlData = "";
			try {
				xmlData = mVtsService.Get_Trans_By_Date_Cab(mUserID,
															 mPassword,
															 mMerchantID,
															 pStartDate.ToString(),
															 pEndDate.ToString(),
															 pCabNumber,
															 "DUP");
				if (!xmlData.IsNullOrEmpty()) {
					xmlData = xmlData.Replace("&", "_");
				}
			} catch (Exception ex) {
				throw new Exception("VTS Error getting Dupe Trans: " + ex.Message);
			}

			//convert XML Data to data table
			try {
				XMLConverter xml = new XMLConverter();
				DataSet oDS = xml.ConvertXMLToDataSet(xmlData);

				if (oDS.Tables == null || oDS.Tables.Count == 0) {
					return new List<Tran>();
				}

				List<Tran> recList = (from p in oDS.Tables[0].AsEnumerable()
									  where p.Field<string>("BANK_APPROVAL").StartsWith("AP DUP%", StringComparison.CurrentCultureIgnoreCase) ||
											p.Field<string>("BANK_APPROVAL").StartsWith("DUP%", StringComparison.CurrentCultureIgnoreCase) ||
											p.Field<string>("BANK_ACTIONCODE").Equals("094")
									  select new Tran {
										  RowID = p.Field<string>("ROWID"),
										  IPNumber = p.Field<string>("IPNUMBER"),
										  DateTime = Convert.ToDateTime(p.Field<string>("DATETIME")),
										  CardNumber = p.Field<string>("CRNUMBER"),
										  Amount = Convert.ToDecimal(p.Field<string>("AMOUNT")),
										  CabNumber = p.Field<string>("CABNUMBER"),
										  Note = p.Field<string>("NOTE"),
										  TripDate = p.Field<string>("TRIPDATE").IsNullOrEmpty() ? new DateTime(2000, 1, 1) : Convert.ToDateTime(p.Field<string>("TRIPDATE")),
										  TripTimeStart = p.Field<string>("TRIPDATE").IsNullOrEmpty() ? new DateTime(2000, 1, 1) : Convert.ToDateTime(p.Field<string>("TRIPDATE") + " " + p.Field<string>("TRIPTIMESTART")),
										  TripTimeEnd = p.Field<string>("TRIPDATE").IsNullOrEmpty() ? new DateTime(2000, 1, 1) : Convert.ToDateTime(p.Field<string>("TRIPDATE") + " " + p.Field<string>("TRIPTIMEEND")),
										  TripFare = Convert.ToDecimal(p.Field<string>("TRIPFARE")),
										  TripTrips = Convert.ToDecimal(p.Field<string>("TRIPTRIPS")),
										  TripExtras = Convert.ToDecimal(p.Field<string>("TRIPEXTRAS")),
										  TripTips = Convert.ToDecimal(p.Field<string>("TRIPTIPS")),
										  TripDist = Convert.ToDecimal(p.Field<string>("TRIPDIST")),
										  TripToll = Convert.ToDecimal(p.Field<string>("TRIPTOLL")),
										  TripDriverID = p.Field<string>("TRIPDRIVERID"),
										  StoreForward = p.Field<string>("storeforward"),
										  NumService = p.Field<string>("NUM_SERVICE"),
										  CardType = p.Field<string>("CC_TYPE"),
										  BankApproval = p.Field<string>("BANK_APPROVAL"),
										  BankActionCode = p.Field<string>("BANK_ACTIONCODE"),
										  Tax = Convert.ToDecimal(p.Field<string>("TAX"))
									  }).ToList<Tran>();

				return recList;
			} catch {
				throw new Exception(xmlData);
			}
		}


		public long ProcessTransactions(Affiliate pAffiliate, DateTime pStateDate, DateTime pEndDate) {
			return ProcessTransactions(pAffiliate, pStateDate, pEndDate, GlobalSettings.MySessionBundle.Principal.Identity.Name, "");
		}



		public long ProcessTransactions(Affiliate pAffiliate, DateTime pStartDate, DateTime pEndDate, string pUserName, string pCabNumber) {
			List<Tran> recList = new List<Tran>();
			try {
				recList = GetTransactions(pStartDate, pEndDate, pCabNumber);
			} catch (Exception ex) {
				throw ex;
			}
			Console.WriteLine(String.Format("Retrieved {0} records", recList.Count));

			string taxiTronicTimeZone = SystemDefaults.GetDefaults(pAffiliate.PersistenceManager).VTSTimeZone;
			if (taxiTronicTimeZone.IsNullOrEmpty()) {
				throw new Exception("Please setup VTS Time Zone in System Defaults");
			}

			EntityList<Entity> saveTrans = new EntityList<Entity>();
			transErrors = new List<string>();

			//done to bring in transactions for faster processing
			DriverPayments.GetTransactions(pAffiliate, pStartDate.AddDays(-1), pEndDate.AddDays(2), true, true);

			List<string> transList = GetTransactionHistory(pAffiliate, pStartDate, pEndDate);
			//check if fleet has sub-fleets
			EntityList<Affiliate> subFleets = Affiliate.GetSubFleets(pAffiliate);
			foreach (Affiliate rec in subFleets) {
				transList.AddRange(GetTransactionHistory(rec, pStartDate, pEndDate));
			}
			pAffiliate.PersistenceManager.DefaultQueryStrategy = QueryStrategy.Normal;

			foreach (Tran row in recList) {
				Console.WriteLine(String.Format("Processing Row ID {0}", row.RowID));
				if (!row.CardNumber.IsNullOrEmpty()) {
					if (!transList.Contains(row.RowID)) {
						string taxiNumber = row.CabNumber;

						Affiliate useAffiliate = GetAffiliateForTaxiNumber(pAffiliate, subFleets, taxiNumber);
						Drivers driver = GetDriver(row, useAffiliate);
						AffiliateDrivers affDriver = GetAffiliateDriver(driver);

						DriverPayments tran = DriverPayments.Create(driver);
						tran.AffiliateDriverID = affDriver.AffiliateDriverID;
						DriverPaymentAux.Create(tran);

						if (tran.RowState == DataRowState.Added) {
							tran = FillTran(useAffiliate, taxiTronicTimeZone, tran, row);
							if (tran != null) {
								AddTransactionsToSave(saveTrans, tran);
							}
						}
					}
					//if (saveTrans.Count % 100 == 0) {
					//    saveTrans.UpdateModifedByInfo(LoginManager.PrincipleUser.Identity.Name, mTimeZoneConverter.ConvertTime(DateTime.Now, TimeZone.CurrentTimeZone.StandardName, pAffiliate.TimeZone));
					//    pAffiliate.PersistenceManager.SaveChanges(saveTrans);
					//}
				}
			}
			if (saveTrans.Count > 0) {

				SaveRecords(pAffiliate, pUserName, saveTrans);

				//check for possible fraud
				if (!string.IsNullOrEmpty(pAffiliate.FraudEMailTo)) {
					List<DriverPayments> payList = (from p in saveTrans
													where p.GetType() == typeof(DriverPayments)
													select (DriverPayments)p).ToList();
					foreach (DriverPayments oTran in payList) {
						EntityList<DriverPayments> fraudList = DriverPayments.CheckForFraud(oTran);
						if (fraudList.Count > 1) {
							string sMsg = "<table>";
							sMsg += "<tr><td colspan='5'>Affiliate: " + oTran.Affiliate.Name + "Possible Fraud</td></tr>";
							sMsg += "<tr><td colspan='5'>Driver: " + oTran.Drivers.Name + "</td></tr>";
							sMsg += "<tr><td>&nbsp;</td></tr>";
							sMsg += "<tr><td>Taxi Number</td><td>Time</td><td>Card Number</td><td>Total</td><td>Store Forward</td></tr>";
							foreach (DriverPayments rec in fraudList) {
								sMsg += "<tr>" +
										"<td>" + rec.Vehicles.VehicleNo + "</td>" +
										"<td>" + rec.ChargeDate.Value.ToString("hh:mm tt") + "</td>" +
										"<td>" + rec.CardNumber + "</td>" +
										"<td>" + rec.TotalCharge.ToString("C") + "</td>" +
										"<td>" + rec.DriverPaymentAux.StoreForward + "</td></tr>";
							}
							CabRideEngineDapper.SystemDefaultsDict dict = CabRideEngineDapper.SystemDefaultsDict.GetByKey("EMail", "Receipt", false);
							string[] cred = dict.ValueString.Decrypt().Split('|');
							TaxiPassCommon.Mail.EMailMessage email = new TaxiPassCommon.Mail.EMailMessage(cred[0], oTran.Affiliate.Name + " Possible Fraud", sMsg, cred[1]);
							email.UseWebServiceToSendMail = Properties.Settings.Default.EMailViaWebService;
							email.SendMail(pAffiliate.FraudEMailTo);
						}
					}
				}

			}

			return (from p in saveTrans
					where p.GetType() == typeof(DriverPayments)
					select p).ToList().Count;
		}

		private static void AddTransactionsToSave(EntityList<Entity> saveTrans, DriverPayments tran) {
			if (!saveTrans.Contains(tran)) {
				//tran.Save();  //done to ensure that we can get a unique TransNo
				saveTrans.Add(tran);
				saveTrans.AddRange(tran.DriverPaymentResultses.ToArray());
				saveTrans.Add(tran.DriverPaymentAux);
				saveTrans.Add(tran.DriverPaymentGeoCodes);
				saveTrans.Add(tran.DriverPaymentNotes);
			}
		}

		private void SaveRecords(Affiliate pAffiliate, string pUserName, EntityList<Entity> saveTrans) {
			Console.WriteLine(String.Format("Saving {0} Records", saveTrans.Count));

			saveTrans.UpdateModifedByInfo(pUserName, mTimeZoneConverter.ConvertTime(DateTime.Now, TimeZone.CurrentTimeZone.StandardName, pAffiliate.TimeZone));

			bool loop = true;
			int errorCnt = 0;
			SaveResult saveResult = null;
			while (loop) {
				try {
					pAffiliate.PersistenceManager.SaveChanges(saveTrans);
					loop = false;
				} catch (PersistenceManagerSaveException ex) {
					loop = false;
					foreach (Entity ent in ex.SaveResult.EntitiesWithErrors) {
						if (ent.GetType() == typeof(DriverPayments)) {
							loop = true;
							Console.WriteLine("Save Failed: trying to change the voucher number and save again");
							DriverPayments rec = ent as DriverPayments;
							rec.AssignUniqueTransNo(true);
						}
					}
					errorCnt = (from p in ex.SaveResult.EntitiesWithErrors
								where p.GetType() != typeof(DriverPayments)
								select p).ToList().Count;
					if (errorCnt > 0) {
						saveResult = ex.SaveResult;
					}
				}
			}
			if (errorCnt > 0) {
				CabRideEngineDapper.SystemDefaultsDict dict = CabRideEngineDapper.SystemDefaultsDict.GetByKey("EMail", "Receipt", false);
				string[] cred = dict.ValueString.Decrypt().Split('|');

				TaxiPassCommon.Mail.EMailMessage email = new TaxiPassCommon.Mail.EMailMessage(cred[0], "Error saving data", ObjectDisplayer.ObjectToXMLString(saveResult.EntitiesWithErrors, ""), cred[1]);
				email.SendMail(Properties.Settings.Default.EMailErrorsTo);
			}
		}


		private static Affiliate GetAffiliateForTaxiNumber(Affiliate pAffiliate, EntityList<Affiliate> subFleets, string taxiNumber) {
			Affiliate useAffiliate = null;
			if (subFleets.Count == 0 || !taxiNumber.IsNumeric()) {
				useAffiliate = pAffiliate;
			} else {
				//determine fleet trans belongs to
				int iTaxiNumber = Convert.ToInt32(taxiNumber);
				foreach (Affiliate rec in subFleets) {
					if (iTaxiNumber >= rec.VtsTaxiNumberStart && iTaxiNumber <= rec.VtsTaxiNumberEnd) {
						useAffiliate = rec;
						break;
					}
				}
				if (useAffiliate == null) {
					useAffiliate = pAffiliate;
				}
			}
			return useAffiliate;
		}

		private static Type typeGetTransactionHistory = null;
		private static List<string> GetTransactionHistory(Affiliate pAffiliate, DateTime pStartDate, DateTime pEndDate) {
			string start = pStartDate.AddDays(-1).ToShortDateString();
			string end = pEndDate.AddDays(2).ToShortDateString();

			StringBuilder sSQL = new StringBuilder("SELECT VTSRowID FROM DriverPayments ");
			sSQL.Append("LEFT JOIN DriverPaymentAux ON DriverPaymentAux.DriverPaymentID = DriverPayments.DriverPaymentID ");
			sSQL.Append(String.Format("WHERE ChargeDate >= '{0}' AND ChargeDate <= '{1}' AND AffiliateID = {2} AND VTSRowID IS NOT NULL", start, end, pAffiliate.AffiliateID));

			if (typeGetTransactionHistory == null) {
				typeGetTransactionHistory = DynamicEntityTypeBuilder.CreateType("GetTransactionHistory", "Default");
			}

			PassthruRdbQuery qry = new PassthruRdbQuery(typeGetTransactionHistory, sSQL.ToString());
			Entity[] list = pAffiliate.PersistenceManager.GetEntities(qry);

			List<string> rowIDList = (from p in list
									  select p[0].ToString()).ToList();

			return rowIDList;

		}

		private decimal CalcFee(Affiliate pAffiliate, decimal pAmount) {
			foreach (AffiliateFees fee in pAffiliate.AffiliateFeeses) {
				if (Math.Abs(pAmount) >= fee.StartAmount && fee.EndAmount == 0) {
					return (pAmount >= 0) ? fee.Fee : -fee.Fee;
				} else if (Math.Abs(pAmount).IsBetween(fee.StartAmount, fee.EndAmount + fee.Fee)) {
					return (pAmount >= 0) ? fee.Fee : -fee.Fee;
				}
			}
			return pAffiliate.AffiliateFeeses.Max(p => p.Fee);
		}




		//public long FixTransactions(Affiliate pAffiliate, DateTime pStateDate, DateTime pEndDate) {
		//    DataSet taxiTronicDS = null;
		//    try {
		//        taxiTronicDS = GetTransactions(pStateDate, pEndDate, "");
		//    } catch (Exception ex) {
		//        throw ex;
		//    }

		//    if (taxiTronicDS == null) {
		//        throw new Exception("No transactions returned, check MerchantID, User ID and Password");
		//    }

		//    if (taxiTronicDS.Tables.Count == 0) {
		//        return 0;
		//    }

		//    string taxiTronicTimeZone = SystemDefaults.GetDefaults(pAffiliate.PersistenceManager).TaxiTronicTimeZone.ToString();
		//    if (taxiTronicTimeZone == null || taxiTronicTimeZone == "") {
		//        throw new Exception("Please setup TaxiTronic Time Zone in System Defaults");
		//    }

		//    TaxiPassCommon.TimeZoneConverter oZone = new TaxiPassCommon.TimeZoneConverter();
		//    int i = 0;
		//    foreach (DataRow row in taxiTronicDS.Tables[0].Rows) {
		//        if (row["CRNUMBER"].ToString() != "") {
		//            Transactions tran = Transactions.GetTransactionByTaxiTronicID(pAffiliate.PersistenceManager, row["ROWID"].ToString());
		//            if (!tran.IsNullEntity && tran.AffiliateID == pAffiliate.AffiliateID) {
		//                if (string.IsNullOrEmpty(tran.NumService)) {
		//                    i++;
		//                    tran.NumService = row["NUM_SERVICE"].ToString();
		//                    string[] approval = row["BANK_APPROVAL"].ToString().Split(',');
		//                    tran.CardApproval = approval[approval.Length - 1];
		//                    tran.BankApproval = row["BANK_APPROVAL"].ToString();
		//                    tran.StartTime = row["TRIPTIMESTART"].ToString();
		//                    tran.EndTime = row["TRIPTIMEEND"].ToString();
		//                    tran.TotalDistance = Convert.ToDecimal(row["TRIPDIST"]);
		//                    tran.Driver = row["TRIPDRIVERID"].ToString();
		//                    tran.StoreForward = row["storeforward"].ToString();

		//                    if (Convert.ToDouble(tran.AffiliateAmount) == 3.33 || Convert.ToDouble(tran.AffiliateAmount) == 4.53) {
		//                        tran.TestTransaction = true;
		//                    }
		//                    if (tran.Note.ToLower().Contains("test")) {
		//                        tran.TestTransaction = true;
		//                    }
		//                }
		//                //tran.Save(pUserName);
		//            } else {
		//                if (!tran.IsNullEntity) {
		//                    System.Console.WriteLine(tran.AffiliateID);
		//                }
		//            }
		//            if (i > 100) {
		//                i = 0;
		//                tran.PersistenceManager.SaveChanges();
		//            }
		//        }
		//    }
		//    pAffiliate.PersistenceManager.SaveChanges();
		//    return taxiTronicDS.Tables[0].Rows.Count;
		//}

		public long ProcessDeclinedStoreForwardTransactions(Affiliate pAffiliate, DateTime pStartDate, DateTime pEndDate, string pUserName, string pCabNumber) {
			List<Tran> recList = new List<Tran>();
			try {
				Console.WriteLine(String.Format("Retrieving Declined Store Forward Transactions for {0}", pAffiliate.Name));
				recList = GetDeclinedStoreForwardTrans(pStartDate, pEndDate, pCabNumber);
			} catch (Exception ex) {
				throw ex;
			}

			Console.WriteLine(String.Format("Retrieved {0} records", recList.Count));

			if (recList.Count == 0) {
				return 0;
			}

			string taxiTronicTimeZone = SystemDefaults.GetDefaults(pAffiliate.PersistenceManager).VTSTimeZone;
			if (taxiTronicTimeZone.IsNullOrEmpty()) {
				throw new Exception("Please setup TaxiTronic Time Zone in System Defaults");
			}

			EntityList<Entity> saveTrans = new EntityList<Entity>();
			transErrors = new List<string>();

			//done to bring in transactions for faster processing
			DriverPayments.GetTransactions(pAffiliate, pStartDate.AddDays(-1), pEndDate.AddDays(2), true, true);

			List<string> transList = GetTransactionHistory(pAffiliate, pStartDate, pEndDate);
			//check if fleet has sub-fleets
			EntityList<Affiliate> subFleets = Affiliate.GetSubFleets(pAffiliate);
			foreach (Affiliate rec in subFleets) {
				transList.AddRange(GetTransactionHistory(rec, pStartDate, pEndDate));
			}

			pAffiliate.PersistenceManager.DefaultQueryStrategy = QueryStrategy.Normal;
			foreach (Tran row in recList) {
				Console.WriteLine(String.Format("Processing Row ID {0}", row.RowID));

				if (!row.CardNumber.IsNullOrEmpty() && !row.BankApproval.IsNullOrEmpty()) {

					if (!row.RowID.IsNullOrEmpty() && !transList.Contains(row.RowID)) {
						Affiliate useAffiliate = GetAffiliateForTaxiNumber(pAffiliate, subFleets, row.CabNumber);
						Drivers driver = GetDriver(row, useAffiliate);
						AffiliateDrivers affDriver = GetAffiliateDriver(driver);

						DriverPayments tran = DriverPayments.Create(driver);
						tran.AffiliateDriverID = affDriver.AffiliateDriverID;
						DriverPaymentAux.GetOrCreate(tran);

						if (tran.RowState == DataRowState.Added) {
							tran.Failed = true;
							tran.DriverPaymentAux.FailType = "Declined";
							tran = FillTran(useAffiliate, taxiTronicTimeZone, tran, row);
							if (tran != null) {
								AddTransactionsToSave(saveTrans, tran);
							}
						}
					}
				}
			}

			if (saveTrans.Count > 0) {
				SaveRecords(pAffiliate, pUserName, saveTrans);
			}

			return (from p in saveTrans
					where p.GetType() == typeof(DriverPayments)
					select p).ToList().Count;
		}

		private static AffiliateDrivers GetAffiliateDriver(Drivers pDriver) {
			AffiliateDrivers driver = AffiliateDrivers.GetDriverByCellPhone(pDriver.MyAffiliate, pDriver.Cell);
			if (driver.IsNullEntity) {
				driver = AffiliateDrivers.Create(pDriver.MyAffiliate);
				driver.Cell = pDriver.Cell;
				driver.CallInCell = pDriver.Cell;
				driver.Name = pDriver.Name;

				try {
					driver.Save();
				} catch (Exception ex) {
					CabRideEngineDapper.SystemDefaultsDict dict = CabRideEngineDapper.SystemDefaultsDict.GetByKey("EMail", "Receipt", false);
					string[] cred = dict.ValueString.Decrypt().Split('|');
					TaxiPassCommon.Mail.EMailMessage email = new TaxiPassCommon.Mail.EMailMessage(cred[0], "CabRide VTS Import error GetAffiliateDriver", ex.Message, cred[1]);
					email.UseWebServiceToSendMail = Properties.Settings.Default.EMailViaWebService;
					email.SendMail(Properties.Settings.Default.EMailErrorsTo);
					System.Console.WriteLine(ex.Message);
					driver = pDriver.PersistenceManager.GetNullEntity<AffiliateDrivers>();
				}
			}
			return driver;
		}

		private static Drivers GetDriver(Tran row, Affiliate useAffiliate) {
			string driverName = row.TripDriverID;
			if (driverName.IsNullOrEmpty()) {
				driverName = "VTS Driver";
			}
			if (row.System.IsNullOrEmpty()) {
				row.System = "VTS";
			}
			Drivers driver = Drivers.GetDriverByTaxiNumber(useAffiliate, row.CabNumber);

			if (driver.IsNullEntity) {
				driver = Drivers.GetDriverByCellPhone(useAffiliate, driverName);
				if (driver.IsNullEntity) {
					driver = Drivers.Create(useAffiliate.PersistenceManager);
					driver.AffiliateID = useAffiliate.AffiliateID;
					driver.TaxiTronic = true;
					driver.BoostPhone = driverName;
					driver.Cell = driverName;
					driver.Name = driverName;
					driver.EMail = string.Format("{0}@{1}.com", driverName, row.System);
					driver.AppVersion = row.System;
					driver.VehicleNo = row.CabNumber;
					try {
						driver.Save();
					} catch (Exception ex) {
						CabRideEngineDapper.SystemDefaultsDict dict = CabRideEngineDapper.SystemDefaultsDict.GetByKey("EMail", "Receipt", false);
						string[] cred = dict.ValueString.Decrypt().Split('|');
						TaxiPassCommon.Mail.EMailMessage email = new TaxiPassCommon.Mail.EMailMessage(cred[0], "CabRide VTS Import error GetDriver", ex.Message, cred[1]);
						email.UseWebServiceToSendMail = Properties.Settings.Default.EMailViaWebService;
						email.SendMail(Properties.Settings.Default.EMailErrorsTo);
						System.Console.WriteLine(ex.Message);
					}
				} else {
					driver.VehicleNo = row.CabNumber;
					driver.Save();
				}
			}
			return driver;
		}


		public long ProcessDupeTransactions(Affiliate pAffiliate, DateTime pStartDate, DateTime pEndDate, string pUserName, string pCabNumber) {
			List<Tran> recList = new List<Tran>();
			try {
				recList = GetDupeTransNew(pStartDate, pEndDate, pCabNumber);
			} catch (Exception ex) {
				throw ex;
			}

			Console.WriteLine(String.Format("Retrieved {0} records", recList.Count));

			if (recList.Count == 0) {
				return 0;
			}


			string taxiTronicTimeZone = SystemDefaults.GetDefaults(pAffiliate.PersistenceManager).VTSTimeZone;
			if (taxiTronicTimeZone == null || taxiTronicTimeZone == "") {
				throw new Exception("Please setup TaxiTronic Time Zone in System Defaults");
			}

			//    TaxiPassCommon.TimeZoneConverter oZone = new TaxiPassCommon.TimeZoneConverter();
			EntityList<Entity> saveTrans = new EntityList<Entity>();
			transErrors = new List<string>();

			//done to bring in transactions for faster processing
			DriverPayments.GetTransactions(pAffiliate, pStartDate.AddDays(-1), pEndDate.AddDays(2), true, true);

			List<string> transList = GetTransactionHistory(pAffiliate, pStartDate, pEndDate);
			//check if fleet has sub-fleets
			EntityList<Affiliate> subFleets = Affiliate.GetSubFleets(pAffiliate);
			foreach (Affiliate rec in subFleets) {
				transList.AddRange(GetTransactionHistory(rec, pStartDate, pEndDate));
			}

			pAffiliate.PersistenceManager.DefaultQueryStrategy = QueryStrategy.Normal;
			foreach (Tran row in recList) {
				Console.WriteLine(String.Format("Processing Row ID {0}", row.RowID));
				if (!row.CardNumber.IsNullOrEmpty()) {
					if (!row.RowID.IsNullOrEmpty() && !transList.Contains(row.RowID)) {
						//check if trans has already been added to prevent streaming and overpayment to fleets
						Affiliate useAffiliate = GetAffiliateForTaxiNumber(pAffiliate, subFleets, row.CabNumber);
						Drivers driver = GetDriver(row, useAffiliate);
						AffiliateDrivers affDriver = GetAffiliateDriver(driver);


						EntityList<DriverPayments> dupTrans = DriverPayments.GetVTSTransactions(useAffiliate, row.CabNumber, row.NumService, row.CardNumber, row.BankApproval, row.Amount);
						if (dupTrans.Count == 0) {
							DriverPayments tran = DriverPayments.Create(driver);
							tran.AffiliateDriverID = affDriver.AffiliateDriverID;
							DriverPaymentAux.GetOrCreate(tran);

							if (tran.RowState == DataRowState.Added) {
								tran.Failed = true;
								tran.DriverPaymentAux.FailType = "Dupe";
								tran = FillTran(useAffiliate, taxiTronicTimeZone, tran, row);
								if (tran != null) {
									AddTransactionsToSave(saveTrans, tran);
								}
							}
						}
					}
				}
			}
			if (saveTrans.Count > 0) {
				try {
					pAffiliate.PersistenceManager.SaveChanges(saveTrans);
				} catch (PersistenceManagerSaveException ex) {
					System.IO.File.AppendAllText(@"c:\temp\TaxiTronicErrors.txt", ex.Message);
				}
			}
			return saveTrans.Count;
		}


		private DriverPayments FillTran(Affiliate pAffiliate, string taxiTronicTimeZone, DriverPayments pDriverPayments, Tran pTran) {
			try {
				TaxiStatus taxiStat = TaxiStatus.GetDefault(pAffiliate.PersistenceManager);

				Vehicles vehicle = pAffiliate.PersistenceManager.GetNullEntity<Vehicles>();
				if (!pTran.CabNumber.IsNullOrEmpty()) {
					vehicle = Vehicles.GetVehicle(pAffiliate, pTran.CabNumber);
					if (vehicle.IsNullEntity) {
						vehicle = Vehicles.Create(pAffiliate);
						vehicle.VehicleNo = pTran.CabNumber;
						vehicle.OutOfService = false;
						vehicle.TaxiStatusID = taxiStat.TaxiStatusID;
						vehicle.VehicleTypeID = VehicleTypes.GetTaxi(pAffiliate.PersistenceManager).VehicleTypeID;
						vehicle.Save();
					} else {
						if (vehicle.OutOfService || vehicle.TaxiStatusID != taxiStat.TaxiStatusID) {
							vehicle.OutOfService = false;
							vehicle.TaxiStatusID = taxiStat.TaxiStatusID;
							vehicle.Save();
						}
					}

					//            VehicleActivity activity = VehicleActivity.GetDriver(vehicle, tran.MDataDate);
					//            if (!activity.IsNullEntity) {
					//                AffiliateUsers oUser = AffiliateUsers.GetDriver(pAffiliate, activity.AffiliateUserID);
					//                if (!oUser.IsNullEntity) {
					//                    tran.Driver = oUser.TAPermit;
					//                }
					//            }
					//        }
				}

				pDriverPayments.ChargeDate = pTran.DateTime;
				pDriverPayments.CardNumber = pTran.CardNumber;
				pDriverPayments.CardType = CardTypes.GetCardTypeByVtsID(pAffiliate.PersistenceManager, pTran.CardType).CardType;
				//tran.TransAmount = Convert.ToDecimal(row["AMOUNT"]);
				if (!vehicle.IsNullEntity) {
					pDriverPayments.VehicleID = vehicle.VehicleID;
				}
				pDriverPayments.Notes = pTran.Note;
				pDriverPayments.Fare = pTran.Amount - CalcFee(pAffiliate, pTran.Amount);
				//        tran.TaxiPassAmount = CalcFee(pAffiliate, tran.TransAmount);
				//        tran.AffiliateAmount = tran.TransAmount - tran.TaxiPassAmount;
				pDriverPayments.ChargeDate = mTimeZoneConverter.ConvertTime(pDriverPayments.ChargeDate.Value, taxiTronicTimeZone, pAffiliate.TimeZone);
				pDriverPayments.ReferenceNo = pTran.NumService;
				string[] approval = pTran.BankApproval.Split(',');
				DriverPaymentResults results = DriverPaymentResults.Create(pDriverPayments);
				results.AuthCode = approval[approval.Length - 1];
				results.Message = pTran.BankApproval;
				results.TransType = VerisignTrxTypes.Sale;
				results.TransDate = DateTime.Now;
				results.Amount = pTran.Amount;
				if (pDriverPayments.Failed) {
					results.Result = "1";
					results.CreditCardProcessor = "VTS Failed";
				} else {
					results.Result = "0";
					results.CreditCardProcessor = "VTS Approved";
				}
				//        tran.CardApproval = approval[approval.Length - 1];
				//        tran.BankApproval = row["BANK_APPROVAL"].ToString();
				DriverPaymentGeoCodes geo = DriverPaymentGeoCodes.Create(pDriverPayments);
				geo.GPSAcquiredTime = pTran.TripTimeEnd;
				geo.StartGPSAcquiredTime = pTran.TripTimeStart;
				geo.TotalDistance = pTran.TripDist;
				if (geo.TotalDistance > 5000) {
					geo.TotalDistance = 5000;
				}


				pDriverPayments.DriverPaymentAux.StoreForward = pTran.StoreForward.Equals("Y", StringComparison.CurrentCultureIgnoreCase);
				pDriverPayments.DriverPaymentAux.VTSRowID = pTran.RowID;
				//        //need to add to running total
				//        Transactions lastTran = Transactions.GetLastCabRunningTotal(tran);
				//        decimal nTotal = 0;
				//        if (!lastTran.IsNullEntity) {
				//            nTotal = lastTran.RunningCabTotal;
				//        }
				//        tran.RunningCabTotal = nTotal + tran.AffiliateAmount;


				pDriverPayments.Test = (pDriverPayments.TotalCharge == 3.33M || pDriverPayments.TotalCharge == 4.53M);
				if (!pDriverPayments.Test) {
					pDriverPayments.Test = pDriverPayments.Notes.ToLower().Contains("test");
				}

				if (!pTran.Note.IsNullOrEmpty() && pTran.Amount > 0) {
					if (pDriverPayments.ReferenceNo.IsNullOrEmpty() || Convert.ToInt32(pDriverPayments.ReferenceNo) < 1) {
						pDriverPayments.DriverPaymentAux.Recharged = true;
					}
				}
				pDriverPayments.Platform = Platforms.Victory.ToString();

				if (pAffiliate.AutoPayFleet || pDriverPayments.AffiliateDrivers.AutoPayManual || pDriverPayments.AffiliateDrivers.AutoPaySwipe) {
					if (!pDriverPayments.Drivers.PaidByRedeemer && !pDriverPayments.AffiliateDrivers.RedeemerStopPayment) {
						pDriverPayments.DriverPaid = true;
						pDriverPayments.DriverPaidDate = DateTime.Now;
					}
				}

				if (pDriverPayments.DriverPaid && !pDriverPayments.Failed && !pDriverPayments.Test) {
					pDriverPayments.DoPerTransactionRecurringFee();

					//if (!pDriverPayments.TaxiPassRedeemerID.HasValue) {
					//    pDriverPayments.FundPayCard(pDriverPayments.AffiliateDrivers);
					//}
				}
			} catch (Exception ex) {
				transErrors.Add("Affiliate: " + pAffiliate.Name + " TaxiTronic ID: " + pTran.RowID + " -> " + ex.Message);
				return null;
			}
			return pDriverPayments;
		}


	}
}
