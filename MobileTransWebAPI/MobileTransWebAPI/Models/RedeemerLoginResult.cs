﻿using TaxiPassCommon;

namespace MobileTransWebAPI.Models {
    public partial class RedeemerLoginResult {

        public long TaxiPassRedeemerAccountID { get; set; }
        public long TaxiPassRedeemerID { get; set; }
        public string CompanyName { get; set; }
        public string Password { get; set; }
        public bool PayDriverPayCard { get; set; }
        public bool PayCardRealTimePay { get; set; }
        public bool RedeemerActive { get; set; }
        public bool RedeemerAccountActive { get; set; }
        public string Salt { get; set; }
        public string ErrorMsg { get; set; }


        public string PasswordDecrypted {
            get {
                Encryption oEncrypt = new Encryption();
                string sPwd = oEncrypt.Decrypt(Password, Salt);
                return sPwd;
            }
        }
    }
}