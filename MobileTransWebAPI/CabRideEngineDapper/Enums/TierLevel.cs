﻿namespace CabRideEngineDapper.Enums {
	public enum TierLevel {
		CallCenter = 1,
		TaxiPass,
		NewtekTier1,
		NewtekTier2Admin
	}
}
