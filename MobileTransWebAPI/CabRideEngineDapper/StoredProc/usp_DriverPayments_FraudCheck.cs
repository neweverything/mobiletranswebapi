﻿using Dapper;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace CabRideEngineDapper.StoredProc {
    public class usp_DriverPayments_FraudCheck {

        public DateTime? LastChargeDate { get; set; }
        public DateTime? LastAuthDateByDriver { get; set; }
        public DateTime? LastAuthDate { get; set; }
        public Int32 DailyTransCountPerCard { get; set; }
        public Int32 DailyTransCount { get; set; }
        public Decimal DailyTransTotal { get; set; }
        public Int32 MonthTransCount { get; set; }
        public Decimal MonthTotal { get; set; }
        public Int32 MonthTransCountPerCard { get; set; }
        public Int32 WeekTransCount { get; set; }
        public Decimal WeekTotal { get; set; }
        public Int32 WeekTransCountPerCard { get; set; }

        public Int32 DailyDeclinedCount { get; set; }
        public Int32 WeekDeclinedCount { get; set; }
        public Int32 MonthDeclinedCount { get; set; }

        public static usp_DriverPayments_FraudCheck ExecuteProc(SqlConnection pConn, DriverPayments pDriverPayments, bool pIsDriver, DateTime pTranDate) {
            string pCardHash = pDriverPayments.Number;
            string pCardNumberDisplay = pDriverPayments.CardNumberDisplay;
            string pTransNo = pDriverPayments.TransNo;

            long driverID = pIsDriver ? pDriverPayments.DriverID : pDriverPayments.AffiliateDriverID.Value;
            return pConn.Query<usp_DriverPayments_FraudCheck>("usp_DriverPayments_FraudCheck", new { checkDriverID = driverID, cardNumberDisplay = pCardNumberDisplay, cardHash = pCardHash, transNo = pTransNo, curDate = pTranDate, IsDriver = pIsDriver }, commandType: CommandType.StoredProcedure).FirstOrDefault();
        }
    }
}
