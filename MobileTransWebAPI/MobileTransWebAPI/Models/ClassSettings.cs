﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MobileTransWebAPI.Models {
	// Generic replacement for a dictionary object in ChargeInfo's Settings.Dict
	public class ClassSettings {
		public string Key { get; set; }
		public string Value { get; set; }
	}
}