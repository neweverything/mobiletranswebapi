﻿using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace CabRideEngineDapper {
	public partial class ReceiptRequestTypes {

		public ReceiptRequestTypes() {
			Description = "";
		}

		public static ReceiptRequestTypes GetOrCreate(SqlConnection pConn, string pRequestType, string pModifiedBy) {
			return pConn.Query<ReceiptRequestTypes>("usp_ReceiptRequestTypesGetOrCreateByDescription", new { Desc = pRequestType, ModifiedBy = pModifiedBy }, commandType: CommandType.StoredProcedure).DefaultIfEmpty(ReceiptRequestTypes.GetNullEntity()).FirstOrDefault();
		}


		public static ReceiptRequestTypes GetbyID(SqlConnection pConn, long pRequestTypeID) {
			return pConn.Get<ReceiptRequestTypes>(pRequestTypeID);
		}


		public static List<ReceiptRequestTypes> GetAll(SqlConnection pConn) {
			string sql = @"SELECT  *
							FROM    dbo.ReceiptRequestTypes
							ORDER BY Description";

			return pConn.Query<ReceiptRequestTypes>(sql).ToList();
		}

		public static List<ReceiptRequestTypes> GetActive(SqlConnection pConn) {
			string sql = @"SELECT  *
							FROM    dbo.ReceiptRequestTypes
							WHERE Active = 1
							ORDER BY Description";

			return pConn.Query<ReceiptRequestTypes>(sql).ToList();
		}
	}
}
