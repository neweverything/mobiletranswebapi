﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;
using System.Text;
using TaxiPassCommon;

namespace CabRideEngine {
	[Serializable]
	public sealed class BlackOutDates : BlackOutDatesDataRow {

		#region Constructors -- DO NOT MODIFY
		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new'
		private BlackOutDates() : this(null) {
		}

		// Typed DataSet constructor (needed by framework) - DO NOT REMOVE
		public BlackOutDates(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static BlackOutDates Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      BlackOutDates aBlackOutDates = (BlackOutDates) pManager.CreateEntity(typeof(BlackOutDates));
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aBlackOutDates, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aBlackOutDates.AddToManager();
		//      return aBlackOutDates;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		// Add additional logic to your business object here...
		public static BlackOutDates Create(PersistenceManager pPM, long pAffiliateID) {
			BlackOutDates oBlackOut = null;

			try {
				oBlackOut = (BlackOutDates)pPM.CreateEntity(typeof(BlackOutDates));

				// Using the IdeaBlade Id Generation technique
				//pPM.GenerateId(AffliateIDEntityColumn, Employee.IdEntityColumn);

				oBlackOut["AffiliateID"] = pAffiliateID;
				oBlackOut.AddToManager();

			} catch (Exception ex) {
				throw ex;
			}
			return oBlackOut;
		}

		protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
			if (IsDeserializing)
				return;
			base.OnColumnChanging(pArgs);
			DoAudit(pArgs);
		}

		private void DoAudit(DataColumnChangeEventArgs pArgs) {
			if (!this.PersistenceManager.IsClient) {
				return;
			}
            if (this.RowState == DataRowState.Added) {
                return;
            }
			if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
				AuditRecord = BlackOutDatesAudit.Create(this.PersistenceManager, this);
			}
			//AuditRecord[pArgs.Column.ColumnName] = pArgs.ProposedValue;
		}

        public static bool SetBlackOutDate(PersistenceManager pPM, DateTime pBlackOutDate, string pDescription) {
            EntityList<Affiliate> oCabs = pPM.GetEntities<Affiliate>();


            foreach (Affiliate myCab in oCabs) {
                RdbQuery qry = new RdbQuery(typeof(BlackOutDates), BlackOutDates.AffiliateIDEntityColumn, EntityQueryOp.EQ, myCab.AffiliateID);
                qry.AddClause(BlackOutDates.DayEntityColumn, EntityQueryOp.EQ, pBlackOutDate);
                BlackOutDates myDate = pPM.GetEntity<BlackOutDates>(qry);
                if (myDate.IsNullEntity) {
                    myDate = BlackOutDates.Create(pPM, myCab.AffiliateID);
                }
                myDate.Day = pBlackOutDate;
                myDate.Description = pDescription;
            }
            try {
                pPM.SaveChanges();
                return true;
            } catch (PersistenceManagerSaveException ex) {
                throw ex;
            }
        }

		// ******************************************************************************
		// Get all Blackoutdates for an Affiliate
		// ******************************************************************************
		public static EntityList<BlackOutDates> GetBlackOutDates(PersistenceManager pPM, long pAffiliateID) {
			return GetData(pPM, pAffiliateID.ToString());
		}

		// ******************************************************************************
		// Call the proc
		// ******************************************************************************
		private static EntityList<BlackOutDates> GetData(PersistenceManager pPM, string pAffiliateID) {
			//string sDelimiter = "";
			int iArgCount = 0;
			StringBuilder sql = new StringBuilder();
			sql.Append("usp_BlackOutDatesGet ");
			if (!pAffiliateID.IsNullOrEmpty()) {
				sql.AppendFormat(" @AffiliateID = {0}", pAffiliateID);
				iArgCount++;
			}

			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(BlackOutDates), sql.ToString());
			EntityList<BlackOutDates> oData = pPM.GetEntities<BlackOutDates>(qry);
			//if (oData.Count == 0) {
			//	oData.Add(new BlackOutDates());
			//}
			return oData;
		}
	}

}
