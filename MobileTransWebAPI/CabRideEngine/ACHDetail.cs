﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Linq;

using System.Collections.Generic;
using System.Text;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

using TaxiPassCommon;
using Csla.Validation;
using TaxiPassCommon.Banking;

namespace CabRideEngine {
	/// <summary>
	/// The developer class representing the ACHDetail business object class.
	/// </summary>
	/// <remarks>
	/// <para>Place all application-specific business logic in this class.</para>
	/// <para>
	/// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
	/// and will not overwrite changes.
	/// </para>
	/// <para>
	/// This class inherits from the generated class, its companion in the pair of classes 
	/// defining a business object. Properties in the generated class may be overridden in 
	/// order to change the behavior or add business logic.
	/// </para>
	/// <para>
	/// This class is the final class in the inheritance chain for this business object. 
	/// It should not be inherited.
	/// </para>
	/// </remarks>
	[Serializable]
	public sealed partial class ACHDetail : ACHDetailDataRow {

		#region Constructors -- DO NOT MODIFY

		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new';
		// it is also required if the class is visible from a web service.
		// DO NOT REMOVE **
		private ACHDetail() : this(null) { }

		/// <summary>
		/// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
		/// </summary>
		/// <param name="pRowBuilder"></param>
		public ACHDetail(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static ACHDetail Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      ACHDetail aACHDetail = pManager.CreateEntity<ACHDetail>();
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aACHDetail, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aACHDetail.AddToManager();
		//      return aACHDetail;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		// Add additional logic to your business object here...
		#region Standard Entity logic to create/audit/validate the entity
		public static ACHDetail Create(DriverPayments pDriverPayment) {
			ACHDetail rec = null;
			try {
				rec = pDriverPayment.PersistenceManager.CreateEntity<ACHDetail>();

				// Using the IdeaBlade Id Generation technique
				pDriverPayment.PersistenceManager.GenerateId(rec, ACHDetail.ACHDetailIDEntityColumn);
				rec.DriverPaymentID = pDriverPayment.DriverPaymentID;
				rec.AddToManager();

			} catch (Exception ex) {
				throw ex;
			}
			return rec;
		}

		public static ACHDetail CreatePayCardBatch(AffiliateDrivers pPayeeDriver, DriverPayments pDriverPayment, bool pForceDriverInitiatedPayment) {
			PersistenceManager oPM = pDriverPayment.PersistenceManager;
			
			ACHDetail achRec = ACHDetail.GetTransactionByDriverPaymentID(pDriverPayment.PersistenceManager, pDriverPayment.DriverPaymentID, QueryStrategy.DataSourceThenCache);
			if (achRec.ACHPaidDate.HasValue) {
				return achRec;
			}

			if (achRec.IsNullEntity) {
				achRec = ACHDetail.Create(pDriverPayment);
			}
			achRec.ACHErrorReason = "Batch Required";
			achRec.BatchTime = DateTime.Now;
			achRec.ACHPaidDate = DateTime.Now;
			achRec.ACHPaidTo = pPayeeDriver.Name;
			achRec.ACHBankAccountNumber = "TransCard: " + pPayeeDriver.TransCardNumber.Right(5);

			achRec.TransCardAdminNo = pPayeeDriver.TransCardAdminNo;
			pDriverPayment.SetDriverPaid(pForceDriverInitiatedPayment);

			pDriverPayment.Save();

			return achRec;
		}


		protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
			if (IsDeserializing) {
				return;
			}
			base.OnColumnChanging(pArgs);
			DoAudit(pArgs);
		}

		private void DoAudit(DataColumnChangeEventArgs pArgs) {
			if (!this.PersistenceManager.IsClient) {
				return;
			}
			if (this.RowState == DataRowState.Added) {
				return;
			}
			if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
				AuditRecord = ACHDetailAudit.Create(this);
			}
		}


		//public bool SetLocalDateTimes(object pTarget, RuleArgs e) {
		//    //update Driver Detail dates
		//    ACHDetail rec = pTarget as ACHDetail;
		//    return rec.DriverPayments.SetLocalDateTimes(rec.DriverPayments, e);
		//}
		#endregion

		public override DateTime? ACHPaidDate {
			get {
				return base.ACHPaidDate;
			}
			set {
				base.ACHPaidDate = value;
				if (value.HasValue) {
					string timeZone = this.DriverPayments.Affiliate.TimeZone;
					if (!timeZone.IsNullOrEmpty()) {
						TimeZoneConverter convert = new TimeZoneConverter();
						base.ACHPaidDate = convert.ConvertTime(value.Value, TimeZone.CurrentTimeZone.StandardName, timeZone);
					}
				}
			}
		}

		public override DateTime? BatchTime {
			get {
				return base.BatchTime;
			}
			set {
				base.BatchTime = value;
				if (value.HasValue) {
					string timeZone = this.DriverPayments.Affiliate.TimeZone;
					if (!timeZone.IsNullOrEmpty()) {
						TimeZoneConverter convert = new TimeZoneConverter();
						base.BatchTime = convert.ConvertTime(value.Value, TimeZone.CurrentTimeZone.StandardName, timeZone);
					}
				}
			}
		}

		public override bool? Validated {
			get {
				return base.Validated;
			}
			set {
				base.Validated = value;
				if (value.HasValue && !this.ValidatedDate.HasValue) {
					this.ValidatedDate = DateTime.Now;
				}
			}
		}

		public override DateTime? ValidatedDate {
			get {
				return base.ValidatedDate;
			}
			set {
				base.ValidatedDate = value;
				if (value.HasValue) {
					string timeZone = this.DriverPayments.Affiliate.TimeZone;
					if (!timeZone.IsNullOrEmpty()) {
						TimeZoneConverter tzConvert = new TimeZoneConverter();
						base.ValidatedDate = tzConvert.ConvertTime(value.Value, TimeZone.CurrentTimeZone.StandardName, timeZone);
					}
				}
			}
		}
		public DateTime? ACHPaidDateTimeLocal {
			get {
				return this.ACHPaidDate;
			}
		}

		public static EntityList<ACHDetail> GetTransCardTransactions(PersistenceManager pPM, string pTransCard, DateTime pStart, DateTime pEnd) {
			RdbQuery qry = new RdbQuery(typeof(ACHDetail), ACHDetail.ACHBankAccountNumberEntityColumn, EntityQueryOp.EQ, "TransCard: " + pTransCard.Right(5));
			qry.AddClause(ACHDetail.ACHPaidDateEntityColumn, EntityQueryOp.GE, pStart);
			qry.AddClause(ACHPaidDateEntityColumn, EntityQueryOp.LE, pEnd);
			return pPM.GetEntities<ACHDetail>(qry);
		}

		public static EntityList<ACHDetail> GetTransCardAdminTransactions(PersistenceManager pPM, string pTransCardAdminNo, DateTime pStart, DateTime pEnd) {
			RdbQuery qry = new RdbQuery(typeof(ACHDetail), ACHDetail.TransCardAdminNoEntityColumn, EntityQueryOp.EQ, pTransCardAdminNo);
			qry.AddClause(ACHDetail.ACHPaidDateEntityColumn, EntityQueryOp.GE, pStart);
			qry.AddClause(ACHPaidDateEntityColumn, EntityQueryOp.LE, pEnd);
			qry.AddSpan(EntityRelations.DriverPayments_ACHDetail);
			return pPM.GetEntities<ACHDetail>(qry);
		}

		public static List<string> GetUniqueTransCardAdminNumbers(PersistenceManager pPM, bool pAll) {
			return GetUniqueTransCardAdminNumbers(pPM, pAll, false);
		}

		public static List<string> GetUniqueTransCardAdminNumbers(PersistenceManager pPM, bool pAll, bool pExludeBatchRequired) {
			string sql = "SELECT DISTINCT TransCardAdminNo FROM AchDetail ";
			if (!pAll) {
				sql += "WHERE (ACHDetail.ExternalConfNo is null or ACHDetail.ExternalConfNo = '' or ACHDetail.ExternalConfNo = 'hani') ";
			}
			if (pExludeBatchRequired) {
				sql += " AND ACHErrorReason != 'Batch Required' ";
			}
			sql += "ORDER BY TransCardAdminNo";
			DataTable oTable = Utils.SQLHelper.GetRecords(pPM, sql);
			var adminNumberList = (from p in oTable.AsEnumerable()
								   select p.Field<string>("TransCardAdminNo")).ToList();

			return adminNumberList;
		}

		public static ACHDetail GetTransaction(PersistenceManager pPM, string pTransCard, DateTime pStart, DateTime pEnd, decimal pDriverTotal) {
			EntityList<ACHDetail> detailList = GetTransCardTransactions(pPM, pTransCard, pStart, pEnd);
			foreach (ACHDetail rec in detailList) {
				if (rec.DriverPayments.DriverTotal == pDriverTotal) {
					return rec;
				}
			}
			return pPM.GetNullEntity<ACHDetail>();
		}

		public static ACHDetail GetTransactionByDriverPaymentID(PersistenceManager pPM, long pDriverPaymentID, QueryStrategy pQueryStrategy) {
			RdbQuery qry = new RdbQuery(typeof(ACHDetail), ACHDetail.DriverPaymentIDEntityColumn, EntityQueryOp.EQ, pDriverPaymentID);
			return pPM.GetEntity<ACHDetail>(qry, pQueryStrategy);
		}

		public static EntityList<ACHDetail> GetTransactionsByTransCardAdminNo(PersistenceManager pPM, string pTransCardAdminNo, bool pMissingExternalConfNo, QueryStrategy pQueryStrategy) {
			RdbQuery qry = new RdbQuery(typeof(ACHDetail), ACHDetail.TransCardAdminNoEntityColumn, EntityQueryOp.EQ, pTransCardAdminNo);
			if (pMissingExternalConfNo) {
				qry.AddClause(ACHDetail.ExternalConfNoEntityColumn, EntityQueryOp.IsNull, true);
				qry.AddClause(ACHDetail.ExternalConfNoEntityColumn, EntityQueryOp.EQ, "");
				qry.AddOperator(EntityBooleanOp.Or);
			}
			qry.AddOrderBy(ACHDetail.ACHPaidDateEntityColumn);
			qry.AddSpan(EntityRelations.DriverPayments_ACHDetail);
			return pPM.GetEntities<ACHDetail>(qry, pQueryStrategy);
		}

		public static EntityList<ACHDetail> GetTransactionsByTransCardAdminNoRequiringValidations(PersistenceManager pPM, string pTransCardAdminNo, DateTime pStartDate, QueryStrategy pQueryStrategy) {
			StringBuilder sql = new StringBuilder();
			sql.Append("SELECT * FROM ACHDetail ");
			sql.Append(String.Format("WHERE TransCardAdminNo = '{0}' ", pTransCardAdminNo));
			sql.Append(String.Format("AND ACHPaidDate >= '{0}' ", pStartDate.ToShortDateString()));
			sql.Append("AND ((ACHErrorReason IS NULL OR ACHErrorReason = '') ");
			sql.Append(" OR (ACHErrorReason != 'Batch Required' AND ACHErrorReason != 'Paid Batch')) ");

			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(ACHDetail), sql.ToString());
			return pPM.GetEntities<ACHDetail>(qry, pQueryStrategy);
		}

		public static EntityList<ACHDetail> GetBatchedTransactionsByTransCardAdminNoRequiringValidations(PersistenceManager pPM, string pTransCardAdminNo, DateTime pStartDate, QueryStrategy pQueryStrategy) {
			StringBuilder sql = new StringBuilder();
			sql.Append("SELECT * FROM ACHDetail ");
			sql.Append(String.Format("WHERE TransCardAdminNo = '{0}' AND Validated = 0 AND ACHErrorReason LIKE 'Paid Batch%' ", pTransCardAdminNo));
			sql.Append(String.Format("AND ACHPaidDate >= '{0}' ", pStartDate.ToShortDateString()));
			sql.Append("ORDER BY ACHBankAccountNumber");

			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(ACHDetail), sql.ToString());
			return pPM.GetEntities<ACHDetail>(qry, pQueryStrategy);
		}

		public static DataTable GetTranactions(PersistenceManager pPM, List<long> pAffilateDriverIDList) {
			var x = from p in pAffilateDriverIDList
					select p.ToString();

			string sql = "select * from ACHDetail " +
						 "left join DriverPayments on DriverPayments.DriverPaymentid = ACHDetail.DriverPaymentID " +
						 "where AffiliateDriverID in (" + string.Join(",", x.ToArray()) + ") " +
						 "order by ACHPaidDate";

			DataTable oTable = Utils.SQLHelper.GetRecords(pPM, sql);

			return oTable;
		}


		public static EntityList<ACHDetail> GetTransCardTransWithMissingAdmin(PersistenceManager pPM) {
			RdbQuery qry = new RdbQuery(typeof(ACHDetail), ACHDetail.ACHBankAccountNumberEntityColumn, EntityQueryOp.StartsWith, "TransCard:");
			qry.AddClause(ACHDetail.TransCardAdminNoEntityColumn, EntityQueryOp.IsNull);
			qry.AddClause(ACHDetail.TransCardAdminNoEntityColumn, EntityQueryOp.EQ, "");
			qry.AddOperator(EntityBooleanOp.Or);
			qry.AddOrderBy(ACHDetail.ACHBankAccountNumberEntityColumn);
			qry.Top = 100;
			return pPM.GetEntities<ACHDetail>(qry);
		}

		public static ACHDetail GetByExternalConfNo(PersistenceManager pPM, string pConfNo) {
			RdbQuery qry = new RdbQuery(typeof(ACHDetail), ACHDetail.ExternalConfNoEntityColumn, EntityQueryOp.EQ, pConfNo);
			return pPM.GetEntity<ACHDetail>(qry, QueryStrategy.DataSourceThenCache);
		}

		public static ACHDetail GetByExternalConfNo(PersistenceManager pPM, string pConfNo, string pTransCardNo, decimal pDriverTotal) {
			RdbQuery qry = new RdbQuery(typeof(ACHDetail), ACHDetail.ExternalConfNoEntityColumn, EntityQueryOp.EQ, pConfNo);
			qry.AddClause(ACHDetail.TransCardAdminNoEntityColumn, EntityQueryOp.EQ, pTransCardNo);
			qry.AddSpan(EntityRelations.DriverPayments_ACHDetail);
			EntityList<ACHDetail> recList = pPM.GetEntities<ACHDetail>(qry, QueryStrategy.DataSourceThenCache);

			ACHDetail rec = recList.FirstOrDefault(p => p.AmountPaid == pDriverTotal || p.DriverPayments.DriverTotalPaid == pDriverTotal);
			if (rec == null) {
				return pPM.GetNullEntity<ACHDetail>();
			}
			return rec;
		}

		public static EntityList<ACHDetail> GetTransByACHBatchNo(PersistenceManager pPM, long pAchBatchNo) {
			return GetTransByACHBatchNo(pPM, pAchBatchNo, "");
		}

		public static EntityList<ACHDetail> GetTransByACHBatchNo(PersistenceManager pPM, long pAchBatchNo, string pTransCardAdminNo) {
			RdbQuery qry = new RdbQuery(typeof(ACHDetail), ACHDetail.ACHBatchNumberEntityColumn, EntityQueryOp.EQ, pAchBatchNo);
			if (!pTransCardAdminNo.IsNullOrEmpty()) {
				qry.AddClause(ACHDetail.TransCardAdminNoEntityColumn, EntityQueryOp.EQ, pTransCardAdminNo);
			}
			qry.AddClause(ACHDetail.ACHErrorEntityColumn, EntityQueryOp.EQ, false);
			qry.AddClause(ACHDetail.ACHErrorEntityColumn, EntityQueryOp.EQ, null);
			qry.AddOperator(EntityBooleanOp.Or);
			qry.AddSpan(EntityRelations.DriverPayments_ACHDetail);
			return pPM.GetEntities<ACHDetail>(qry, QueryStrategy.DataSourceThenCache);
		}

		public static EntityList<ACHDetail> GetTransByBofAAccountNo(PersistenceManager pPM, string pBofAAccount, DateTime pStartDate, DateTime pEndDate) {
			RdbQuery qry = new RdbQuery(typeof(ACHDetail), ACHDetail.ACHBankCodeEntityColumn, EntityQueryOp.EQ, "BofA");
			qry.AddClause(ACHDetail.ACHBankHolderEntityColumn, EntityQueryOp.EQ, pBofAAccount);
			qry.AddClause(ACHPaidDateEntityColumn, EntityQueryOp.GE, pStartDate);
			qry.AddClause(ACHPaidDateEntityColumn, EntityQueryOp.LE, pEndDate);
			qry.AddClause(ACHDetail.ACHErrorEntityColumn, EntityQueryOp.EQ, false);
			qry.AddSpan(EntityRelations.DriverPayments_ACHDetail);
			qry.AddOrderBy(ACHBatchNumberEntityColumn);

			return pPM.GetEntities<ACHDetail>(qry, QueryStrategy.DataSourceThenCache);
		}

		public static EntityList<ACHDetail> GetTransByBofAAccountNo(PersistenceManager pPM, List<string> pBofAAccountNoList, DateTime pStartDate, DateTime pEndDate) {
			RdbQuery qry = new RdbQuery(typeof(ACHDetail), ACHDetail.ACHBankCodeEntityColumn, EntityQueryOp.EQ, "BofA");
			qry.AddClause(ACHDetail.ACHBankHolderEntityColumn, EntityQueryOp.In, pBofAAccountNoList);
			qry.AddClause(ACHPaidDateEntityColumn, EntityQueryOp.GE, pStartDate);
			qry.AddClause(ACHPaidDateEntityColumn, EntityQueryOp.LE, pEndDate);
			qry.AddClause(ACHDetail.ACHErrorEntityColumn, EntityQueryOp.EQ, false);
			qry.AddSpan(EntityRelations.DriverPayments_ACHDetail);
			qry.AddOrderBy(ACHBatchNumberEntityColumn);

			return pPM.GetEntities<ACHDetail>(qry, QueryStrategy.DataSourceThenCache);
		}

		public static EntityList<ACHDetail> GetTransferredToBanks(PersistenceManager pPM, DateTime pStartDate, DateTime pEndDate) {
			RdbQuery qry = new RdbQuery(typeof(ACHDetail), ACHDetail.ACHBankCodeEntityColumn, EntityQueryOp.IsNotNull, true);
			qry.AddClause(ACHDetail.ACHBankCodeEntityColumn, EntityQueryOp.NE, "BofA");
			qry.AddClause(ACHPaidDateEntityColumn, EntityQueryOp.GE, pStartDate);
			qry.AddClause(ACHPaidDateEntityColumn, EntityQueryOp.LE, pEndDate);
			qry.AddClause(ACHDetail.ACHErrorEntityColumn, EntityQueryOp.NE, true);
			qry.AddClause(ACHDetail.ACHErrorEntityColumn, EntityQueryOp.IsNull, true);
			qry.AddOperator(EntityBooleanOp.Or);
			qry.AddSpan(EntityRelations.DriverPayments_ACHDetail);
			qry.AddOrderBy(ACHBatchNumberEntityColumn);

			return pPM.GetEntities<ACHDetail>(qry, QueryStrategy.DataSourceThenCache);
		}

		public static EntityList<ACHDetail> GetTransCardsWaitingToBatch(PersistenceManager pPM) {
			RdbQuery qry = new RdbQuery(typeof(ACHDetail), ACHDetail.ACHErrorReasonEntityColumn, EntityQueryOp.EQ, "Batch Required");
			qry.AddOrderBy(TransCardAdminNoEntityColumn);
			qry.AddSpan(EntityRelations.DriverPayments_ACHDetail);
			return pPM.GetEntities<ACHDetail>(qry);
		}

		public static EntityList<ACHDetail> GetTransCardsWaitingToBatch(PersistenceManager pPM, string pTransCardAdminNo) {
			RdbQuery qry = new RdbQuery(typeof(ACHDetail), ACHDetail.ACHErrorReasonEntityColumn, EntityQueryOp.EQ, "Batch Required");
			qry.AddClause(TransCardAdminNoEntityColumn, EntityQueryOp.EQ, pTransCardAdminNo);
			qry.AddSpan(EntityRelations.DriverPayments_ACHDetail);
			return pPM.GetEntities<ACHDetail>(qry);
		}

		public static EntityList<ACHDetail> GetTransCardsAlreadyBatched(PersistenceManager pPM, string pTransCardAdminNo, DateTime pStart, DateTime pEnd) {
			RdbQuery qry = new RdbQuery(typeof(ACHDetail), ACHDetail.ACHErrorReasonEntityColumn, EntityQueryOp.NE, "Batch Required");
			qry.AddClause(TransCardAdminNoEntityColumn, EntityQueryOp.EQ, pTransCardAdminNo);
			qry.AddClause(ACHPaidDateEntityColumn, EntityQueryOp.GE, pStart);
			qry.AddClause(ACHPaidDateEntityColumn, EntityQueryOp.LE, pEnd);
			qry.AddSpan(EntityRelations.DriverPayments_ACHDetail);
			return pPM.GetEntities<ACHDetail>(qry);
		}

		private static Type mGetDistinctBatchNumbersForTransCardAlreadyBatched = null;
		public static List<long> GetDistinctBatchNumbersForTransCardAlreadyBatched(AffiliateDrivers pDriver, DateTime pStart, DateTime pEnd) {
			if (pDriver.TransCardAdminNo.IsNullOrEmpty()) {
				return new List<long>();
			}

			if (mGetDistinctBatchNumbersForTransCardAlreadyBatched == null) {
				mGetDistinctBatchNumbersForTransCardAlreadyBatched = DynamicEntityTypeBuilder.CreateType("GetDistinctBatchNumbersForTransCardAlreadyBatched", "Default");
			}

			StringBuilder sql = new StringBuilder("SELECT DISTINCT ACHBatchNumber ");
			sql.Append("FROM AchDetail ");
			sql.Append(String.Format("WHERE TransCardAdminNo = '{0}' AND ", pDriver.TransCardAdminNo));
			sql.Append("TransCardAdminNo IS NOT NULL AND ACHErrorReason != 'Batch Required' ");
			sql.Append(String.Format("AND ACHPaidDate >= '{0}' AND ACHPaidDate <= '{1}'", pStart.Date.ToShortDateString(), pEnd.ToString()));

			PassthruRdbQuery qry = new PassthruRdbQuery(mGetDistinctBatchNumbersForTransCardAlreadyBatched, sql.ToString());
			Entity[] list = pDriver.PersistenceManager.GetEntities(qry);

			return (from p in list
					select p.Field<long?>("ACHBatchNumber").GetValueOrDefault(0)).ToList();

		}


		public static EntityList<ACHDetail> GetTransByACHErrorReason(PersistenceManager pPM, string pReason) {
			RdbQuery qry = new RdbQuery(typeof(ACHDetail), ACHDetail.ACHErrorReasonEntityColumn, EntityQueryOp.EQ, pReason);
			qry.AddOrderBy(TransCardAdminNoEntityColumn);
			qry.AddSpan(EntityRelations.DriverPayments_ACHDetail);
			return pPM.GetEntities<ACHDetail>(qry);
		}

		public static EntityList<ACHDetail> GetTransCardMissedPayments(PersistenceManager pPM, bool pIncludeHeyTaxi) {
			StringBuilder sql = new StringBuilder();
			sql.AppendLine("SELECT  ACHDetail.* FROM ACHDetail ");
			sql.AppendLine("inner join DriverPayments on DriverPayments.DriverPaymentID = ACHDetail.DriverPaymentID");
			sql.AppendLine("where TransCardAdminNo is not null and Validated = 0 and ACHPaidDate >= '1/1/2011' and ACHErrorReason is null");
			if (!pIncludeHeyTaxi) {
				sql.AppendLine("and AffiliateID != 81");
			}
			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(ACHDetail), sql.ToString());
			return pPM.GetEntities<ACHDetail>(qry);
		}
	}

	#region EntityPropertyDescriptors.ACHDetailPropertyDescriptor
	////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
	////    For example: 
	////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
	//public static partial class EntityPropertyDescriptors {
	//	public partial class ACHDetailPropertyDescriptor : BaseEntityPropertyDescriptor {
	//    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
	//    public AdaptedPropertyDescriptor Age {
	//      get { return this.Get("Age"); }
	//    }
	//  }
	//} 
	#endregion

}