﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Reflection;
using System.Web;

namespace Twilio {
    public class ScopeURI {
        public string _service;

        public string _privilege;

        public Dictionary<string, object> _prms;

        public ScopeURI(string service, string privilege, object prms) {
            this._service = service;
            this._privilege = privilege;
            this._prms = this.ObjectToUrlQuery(prms);
        }

        public ScopeURI(string service, string privilege, Dictionary<string, object> prms) {
            this._service = service;
            this._privilege = privilege;
            this._prms = prms;
        }

        private Dictionary<string, object> ObjectToUrlQuery(object obj) {
            if (obj == null) {
                return new Dictionary<string, object>();
            }
            PropertyInfo[] properties = obj.GetType().GetProperties();
            Dictionary<string, object> strs = new Dictionary<string, object>();
            PropertyInfo[] propertyInfoArray = properties;
            for (int i = 0; i < (int)propertyInfoArray.Length; i++) {
                PropertyInfo propertyInfo = propertyInfoArray[i];
                Type propertyType = propertyInfo.PropertyType;
                object value = propertyInfo.GetValue(obj, null);
                string name = propertyInfo.Name;
                if (value != null) {
                    if (propertyType.IsPrimitive || propertyType == typeof(string)) {
                        strs.Add(name, value);
                    } else {
                        strs.Add(name, this.ObjectToUrlQuery(value));
                    }
                }
            }
            return strs;
        }

        public static ScopeURI Parse(string uri) {
            if (!uri.StartsWith("scope:")) {
                throw new FormatException("Not a scope URI according to scheme");
            }
            char[] chrArray = new char[] { '?' };
            string[] strArrays = uri.Split(chrArray, 2);
            NameValueCollection nameValueCollection = new NameValueCollection();
            if ((int)strArrays.Length > 1) {
                nameValueCollection = HttpUtility.ParseQueryString(strArrays[1]);
            }
            string str = strArrays[0];
            char[] chrArray1 = new char[] { ':' };
            string[] strArrays1 = str.Split(chrArray1, 3);
            if ((int)strArrays1.Length != 3) {
                throw new FormatException("Not enough parts for scope URI");
            }
            return new ScopeURI(strArrays1[1], strArrays1[2], nameValueCollection);
        }

        public string ToString(string clientName) {
            if (!string.IsNullOrEmpty(clientName)) {
                this._prms.Add("clientName", clientName);
            }
            string str = string.Format("scope:{0}:{1}", this._service, this._privilege);
            if (this._prms.Count > 0) {
                string str1 = "";
                foreach (KeyValuePair<string, object> _prm in this._prms) {
                    if (!string.IsNullOrEmpty(str1)) {
                        str1 = string.Concat(str1, "&");
                    }
                    object value = _prm.Value;
                    if (_prm.Value is Dictionary<string, object>) {
                        string str2 = "";
                        foreach (KeyValuePair<string, object> keyValuePair in (Dictionary<string, object>)_prm.Value) {
                            if (!string.IsNullOrEmpty(str2)) {
                                str2 = string.Concat(str2, HttpUtility.UrlEncode("&"));
                            }
                            str2 = string.Format("{0}{1}={2}", str2, keyValuePair.Key, keyValuePair.Value);
                        }
                        value = HttpUtility.UrlEncode(str2);
                    }
                    str1 = string.Format("{0}{1}={2}", str1, _prm.Key, value);
                }
                str = string.Concat(str, "?", str1);
            }
            return str;
        }
    }
}