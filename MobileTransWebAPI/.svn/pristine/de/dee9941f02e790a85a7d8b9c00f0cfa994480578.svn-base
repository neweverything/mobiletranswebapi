﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

using CabRideEngine;

using TaxiPassCommon;

using MobileTransWebAPI.Models;
using CabRideEngine.Utils;
using IdeaBlade.Persistence;
using MobileTransWebAPI.Controllers;

namespace MobileTransWebAPI.Utils {
	public static class Common {

		private const string DRIVER_PUSH_TABLE_NAME = "DriverPushMessage";
		private const string DRIVER_PUSH_FIELD_NAME = "PushMessageID";

		//---------------------------------------------------------------------------------------------------------
		// Create Vehicle Record
		//---------------------------------------------------------------------------------------------------------
		public static Vehicles CreateVehicleRecord(Affiliate pAffiliate, string pVehicleNo, string pCreatedBy) {
			Vehicles vehi = Vehicles.Create(pAffiliate);
			vehi.VehicleNo = pVehicleNo;
			string carType = "Sedan";
			if (pAffiliate.ServiceType.Equals("Taxi", StringComparison.CurrentCultureIgnoreCase)) {
				carType = "Taxi";
			}
			vehi.VehicleTypeID = VehicleTypes.GetAll(pAffiliate.PersistenceManager).FirstOrDefault(p => p.VehicleType == carType).VehicleTypeID;
			if (!pCreatedBy.IsNullOrEmpty()) {
				vehi.Save(pCreatedBy);
			}
			return vehi;
		}

		//---------------------------------------------------------------------------------------------------------
		// Create Affiliate Driver Record
		//---------------------------------------------------------------------------------------------------------
		public static AffiliateDrivers CreateAffiliateDriverRecord(Drivers oDriver, AffiliateDrivers affDriver) {
			//Create Affiliate Driver
			NewSignUpInfo newInfo = new NewSignUpInfo();
			newInfo.CabNo = oDriver.VehicleNo;
			newInfo.Cell = oDriver.Cell;
			newInfo.DriverID = oDriver.DriverID;
			affDriver = CreateNewDriver(oDriver, newInfo);
			if (!affDriver.IsNullEntity) {
				oDriver.Active = affDriver.Active;
				oDriver.Save();
			}
			return affDriver;
		}

		//---------------------------------------------------------------------------------------------------------
		// Create Driver Record
		//---------------------------------------------------------------------------------------------------------
		public static AffiliateDrivers CreateNewDriver(Drivers pDriver, NewSignUpInfo pSignUpInfo) {
			PersistenceManager oPM = pDriver.PersistenceManager;
			Affiliate oAffiliate = pDriver.MyAffiliate;

			if (pSignUpInfo.Cell.Length != 10) {
				return oPM.GetNullEntity<AffiliateDrivers>();
			}

			if (oAffiliate.IsNullEntity) {
				// find or create a State level Affiliate
				if (!pDriver.State.IsNullOrEmpty()) {
					oAffiliate = Affiliate.GetDefaultAffiliateForState(oPM, pDriver.State);
					pDriver.AffiliateID = oAffiliate.AffiliateID;
					pDriver.Save();
				}
			}

			if (pDriver.AffiliateID.GetValueOrDefault(0) == 0) {
				return oPM.GetNullEntity<AffiliateDrivers>();
			}

			AffiliateDrivers affDriver = AffiliateDrivers.GetDriverByCallInCell(oPM, pDriver.Cell);
			if (affDriver.IsNullEntity) {
				affDriver = AffiliateDrivers.Create(oAffiliate);
				affDriver.Active = true;
				affDriver.CallInCell = pDriver.Cell;
				affDriver.Cell = pDriver.Cell;
				affDriver.Name = pDriver.Name;
				if (pDriver.Pin.IsNullOrEmpty()) {
					affDriver.Pin = pDriver.Cell.Right(4);
				} else {
					affDriver.Pin = pDriver.Pin;
				}
				affDriver.State = pDriver.State;
				affDriver.City = pDriver.City;
				affDriver.Language = "EN";

				affDriver.Active = true;
				pDriver.Active = true;
			}

			affDriver.PaymentMethod = (int)PaymentMethod.Redeemer;

			if (!pSignUpInfo.CabNo.IsNullOrEmpty()) {
				Vehicles oVehicle = Vehicles.GetVehicle(affDriver.Affiliate, pSignUpInfo.CabNo);
				if (oVehicle.IsNullEntity) {
					oVehicle = CreateVehicleRecord(affDriver.Affiliate, pSignUpInfo.CabNo, "");
					oVehicle.LicenseState = affDriver.State;
					oVehicle.Save(String.Format("Mobile SignUp - {0}", pDriver.Cell));
				}
				affDriver.VehicleID = oVehicle.VehicleID;
			}

			affDriver.Save(String.Format("Mobile SignUp - {0}", pDriver.Cell));

			return affDriver;
		}


		//---------------------------------------------------------------------------------------------------------
		// Create Affiliate Driver Record
		//---------------------------------------------------------------------------------------------------------
		public static DriverInfo GetDriverInfo(Drivers pDriver, AffiliateDrivers pAffiliateDrivers) {
			StringBuilder sql = new StringBuilder();
			DriverInfo driverInfo = new DriverInfo();

			PersistenceManager oPM = pDriver.PersistenceManager;

			bool bForceLogin = false;
			AffiliateDriverDefaults driverDefaults = oPM.GetNullEntity<AffiliateDriverDefaults>();
			if (pAffiliateDrivers.IsNullEntity) {
				pAffiliateDrivers = Utils.Common.CreateAffiliateDriverRecord(pDriver, pAffiliateDrivers);
			}
			driverInfo.AffiliateDriverID = pAffiliateDrivers.AffiliateDriverID;
			driverInfo.DriverName = pAffiliateDrivers.Name;
			driverInfo.City = pAffiliateDrivers.City;
			driverInfo.State = pAffiliateDrivers.State;
			driverInfo.PaidByRedeemer = pAffiliateDrivers.PaidByRedeemer;

			bForceLogin = false;
			if (pAffiliateDrivers.CallInCell.IsNullOrEmpty()) {
				driverInfo.TaxiPayCell = pAffiliateDrivers.Cell;
			} else {
				driverInfo.TaxiPayCell = pAffiliateDrivers.CallInCell;
			}
			driverDefaults = pAffiliateDrivers.Affiliate.AffiliateDriverDefaults;

			driverInfo.Cell = pDriver.Cell;
			driverInfo.DriverID = pDriver.DriverID;
			if (pDriver.Pin.IsNullOrEmpty() && !pAffiliateDrivers.Pin.IsNullOrEmpty()) {
				pDriver.Pin = pAffiliateDrivers.Pin;
				pDriver.Save();
			}
			driverInfo.DriverPin = pDriver.Pin;

			driverInfo.AffiliateID = pDriver.MyAffiliate.AffiliateID;
			driverInfo.CardDataEntryType = pDriver.CardDataEntryType;
			driverInfo.ForceLogin = bForceLogin;
			driverInfo.AffiliateName = pDriver.MyAffiliate.Name;
			if (string.IsNullOrEmpty(pDriver.MyAffiliate.ServiceType)) {
				driverInfo.ServiceType = "Taxi";
			} else {
				driverInfo.ServiceType = pDriver.MyAffiliate.ServiceType;
			}
			driverInfo.VehiclesCount = pDriver.MyAffiliate.Vehicleses.Count;
			driverInfo.DriversCount = pDriver.MyAffiliate.AffiliateDriverses.Count;
			driverInfo.TaxiMeterUsageType = pDriver.TaxiMeter;
			driverInfo.Active = pDriver.Active && !pDriver.FraudSuspected;


			SystemDefaults oDefaults = SystemDefaults.GetDefaults(oPM);
			driverInfo.FraudMessage = oDefaults.DriverFraudMessage;
			driverInfo.NewSignUpLiveCall = oDefaults.NewDriverSignUpLiveCall;
			driverInfo.NewDriverWelcomeMsg = oDefaults.NewSignUpWelcomeMsg;

			int gpsUpdate = oDefaults.GPSDriverLocationUpdate.GetValueOrDefault(0);
			decimal amount = 0;
			decimal startingFare = 0;

			bool bReceiptDisplayMaxAmt = false;

			string sGPSBadMsg = "Charge Cards needs to be restarted.  Please restart app";
			int iGPSWarningTime = 0;
			int iGPSRepeatMsgTime = 0;
			decimal maxTip = oDefaults.DriverMaxGratuity;
			string sWeakSignal = "";
			string sTPFeeMsg = "";

			bool bTPFeeOnFareOnly = false;
			if (!driverDefaults.IsNullEntity) {
				if (driverDefaults.CardVerificationAmount.HasValue) {
					amount = driverDefaults.CardVerificationAmount.Value;
				}
				if (driverDefaults.GPSDriverLocationUpdate.HasValue) {
					gpsUpdate = driverDefaults.GPSDriverLocationUpdate.Value;
				}
				if (driverDefaults.ReceiptDisplayMaxAmt.HasValue) {
					bReceiptDisplayMaxAmt = driverDefaults.ReceiptDisplayMaxAmt.Value;
				}
				startingFare = driverDefaults.StartingFare;
				if (!string.IsNullOrEmpty(driverDefaults.BadGPSWarningMsg) && driverDefaults.BadGPSWarningMsg.Trim().Length > 0) {
					sGPSBadMsg = driverDefaults.BadGPSWarningMsg.Trim();
				}
				iGPSWarningTime = driverDefaults.BadGPSWarningTime;
				iGPSRepeatMsgTime = driverDefaults.BadGPSWarningRepeatTime;

				if (driverDefaults.DriverMaxGratuity > 0) {
					maxTip = driverDefaults.DriverMaxGratuity;
				}

				sWeakSignal = driverDefaults.DriverWeakSignalMessage;
				sTPFeeMsg = driverDefaults.TPFeeMsg;
				bTPFeeOnFareOnly = driverDefaults.TPFeeOnFareOnly;
			}
			if (!pDriver.TPFeeMsg.IsNullOrEmpty()) {
				sTPFeeMsg = pDriver.TPFeeMsg;
			}

			driverInfo.MaxTip = maxTip;
			driverInfo.GPSWarningMsg = sGPSBadMsg;
			driverInfo.GPSWarningTime = iGPSWarningTime;
			driverInfo.GPSWarningRepeatTime = iGPSRepeatMsgTime;
			driverInfo.PassengerVerificationMsg = oDefaults.PassengerCardVerificationMsg;
			driverInfo.PassengerVerificationAmt = amount;
			
			driverInfo.GPSUpdateTime = gpsUpdate;
			
			// 1/3/2013 Remove this once everybody has been upgraded from vers 2.99.2
			SystemDefaultsDict oDict = SystemDefaultsDict.GetByKey(oPM, "Ride", "MaxGPSUpdateTime", false);
			if (!oDict.IsNullEntity){
				driverInfo.GPSMaxUpdateTime = int.Parse(oDict.ValueString);
			}
			// Remove the above


			EntityList<SystemDefaultsDict> oSettings = SystemDefaultsDict.GetGroup(oPM, "Ride Driver Settings", false);
			foreach (SystemDefaultsDict oSetting in oSettings) {
				if (!oSetting.IsNullEntity) {
					if (driverInfo.Settings == null) {
						driverInfo.Settings = new Dictionary<string, string>();
					}
					driverInfo.Settings.Add(oSetting.ReferenceKey, oSetting.ValueString);
				}
			}
			

			driverInfo.StartingFare = startingFare;
			driverInfo.AdminCode = oDefaults.CellAdminCode;

			driverInfo.ReceiptDisplayMaxAmt = bReceiptDisplayMaxAmt;
			decimal nAmount = 0;
			if (bReceiptDisplayMaxAmt) {
				foreach (AffiliateFees oFee in pDriver.MyAffiliate.AffiliateFeeses) {
					if (oFee.Fee > nAmount) {
						nAmount = oFee.Fee;
					}
				}
			}
			driverInfo.ReceiptMaxAmt = nAmount;

			nAmount = driverDefaults.DriverAutoPreAuthPct;
			if (nAmount == 0) {
				nAmount = oDefaults.DriverAutoPreAuthPct;
				if (nAmount == 0) {
					nAmount = 30;
				}
			}

			driverInfo.AutoPreAuthPct = nAmount;
			driverInfo.PrinterType = pDriver.PrinterType;

			if (sWeakSignal.IsNullOrEmpty()) {
				sWeakSignal = oDefaults.DriverWeakSignalMessage;
			}
			driverInfo.WeakSignalMessage = sWeakSignal;
			driverInfo.TPFeeMsg = sTPFeeMsg;
			driverInfo.ChargeMaxAmount = pDriver.GetChargeMaxAmount;
			driverInfo.ChargeMaxSwipeAmount = pDriver.GetChargeMaxSwipeAmount;
			if (driverInfo.ChargeMaxSwipeAmount == 0) {
				driverInfo.ChargeMaxSwipeAmount = driverInfo.ChargeMaxAmount;
			}
			driverInfo.TPFeeOnFareOnly = bTPFeeOnFareOnly;
			driverInfo.TaxiPayCell = pDriver.Cell;
			driverInfo.TaxiPayPin = pDriver.Pin;

			string sStatus = "";
			foreach (AffiliateDispatchStatus oStatus in pDriver.MyAffiliate.AffiliateDispatchStatuses) {
				sStatus += oStatus.Status.Trim() + "|";
			}
			if (sStatus != "") {
				sStatus = StringUtils.Left(sStatus, sStatus.Length - 1);
			}
			driverInfo.StatusList = sStatus;
			driverInfo.VivoTech = pDriver.VivoTech;

			// Determine RideQ Dispatch availabiliy
			CabDriverRef oCDRef = CabDriverRef.GetRecord(oPM, "CabRideEngine:Drivers", pDriver.DriverID, "RideQ Dispatch");
			driverInfo.DispatchJobs = (!oCDRef.IsNullEntity && ("" + oCDRef.ValueString).Equals("1")) || driverDefaults.DispatchJobs;


			driverInfo.ReceiptTitle = driverDefaults.ReceiptTitle;
			bool noPassFee = driverDefaults.NoPassengerFee || pDriver.NoPassengerFee;
			driverInfo.NoPassengerFee = noPassFee;
			bool autoTip = pDriver.AutoTipDriverPaysFee || driverDefaults.AutoTipDriverPaysFee;
			driverInfo.AutoTipDriverFee = autoTip;
			decimal tipPercent = driverDefaults.AutoTipPercent;
			if (tipPercent == 0) {
				tipPercent = oDefaults.AutoTipPercent;
			}
			driverInfo.AutoTipPercent = tipPercent;
			driverInfo.WAPUrl = driverDefaults.WebAppURL;
			driverInfo.DisableCharging = driverDefaults.HHDisableCharging;
			driverInfo.ChargingJob = driverDefaults.HHChargeForJobsOnly;
			driverInfo.DriverNo = pDriver.DriverNo;
			driverInfo.VehicleNo = pDriver.VehicleNo;
			if (!driverInfo.VehicleNo.IsNullOrEmpty()) {
				Vehicles vehiRec = Vehicles.GetVehicle(pDriver.MyAffiliate, pDriver.VehicleNo);
				if (!vehiRec.IsNullEntity) {
					driverInfo.VehicleID = vehiRec.VehicleID;
					driverInfo.VehicleKeyCode = vehiRec.KeyCode;
					driverInfo.VehicleRFID = vehiRec.Rfid;
				}
			}
			driverInfo.AskForCabNo = driverDefaults.HHAskForCabNo;
			driverInfo.ChargeKioskSlip = driverDefaults.HHChargeKioskSlip;

			if (driverDefaults.HHUseReservePhoneOnReceipt) {
				driverInfo.ReceiptPhoneNo = pDriver.MyAffiliate.ReservePhone.FormattedPhoneNumber();
			}
			driverInfo.InBackSeat = pDriver.InBackSeat;

			var x = (from p in pDriver.MyAffiliate.AffiliateFeeses
					 orderby p.StartAmount
					 select new { p.StartAmount, p.EndAmount, p.Fee }).ToList();

			driverInfo.TPFeesList = x;

			EntityList<CardTypes> cardTypeList = CardTypes.GetAllCardTypes(oPM);
			driverInfo.CardTypeList = (from p in cardTypeList
									   select new { p.CardType, p.Validation }).ToList();

			//Get Mobile Version Info
			MobileAppInfo info = MobileAppInfo.GetAppInfo(oPM, CabRideEngine.Enums.MobileAppNames.ChargeCards);
			if (pDriver.AppVersion.StartsWith("i")) {
				driverInfo.AndroidAppVersion = info.IPhoneVersion.GetValueOrDefault(0);
				driverInfo.MinAppVersion = info.MinIPhoneVersion.GetValueOrDefault(0);
			} else {
				driverInfo.AndroidAppVersion = info.AndroidVersion.GetValueOrDefault(0);
				driverInfo.AndroidUpdateDelay = info.AndroidATTDelayHours;
				driverInfo.MinAppVersion = info.MinAndroidVersion.GetValueOrDefault(0);
				if (info.AndroidUpdated.HasValue) {
					TimeZoneConverter tzConvert = new TimeZoneConverter();
					DateTime updated = info.AndroidUpdated.Value;
					if (!pDriver.MyAffiliate.TimeZone.IsNullOrEmpty()) {
						updated = tzConvert.ConvertTime(info.AndroidUpdated.Value, "Eastern", pDriver.MyAffiliate.TimeZone);
					}
					driverInfo.AndroidUpdatedTime = updated.ToString("MM/dd/yyyy HH:mm");
				}
			}
			if (Properties.Settings.Default.ForceUpdateCells.Split('|').Contains(pDriver.Cell)) {
				driverInfo.AndroidAppVersion = Properties.Settings.Default.ForceUpdateVersion;
				driverInfo.MinAppVersion = Properties.Settings.Default.ForceUpdateMinVersion;
			}

			driverInfo.TrackTripLog = pDriver.TrackTrips;
			driverInfo.FareFirst = pDriver.FareFirst;

			driverInfo.SignatureRequiredAmt = oDefaults.SignatureRequiredAmount;

			driverInfo.TrackSwipeCounts = pDriver.TrackSwipeCounts || driverDefaults.TrackSwipeCounts;
			driverInfo.HasPayCard = !pAffiliateDrivers.TransCardAdminNo.IsNullOrEmpty();
			driverInfo.DriverInitiatedPayment = pAffiliateDrivers.DriverInitiatedPayCardPayment || pAffiliateDrivers.Affiliate.DriverInitiatedPayCardPayment;

			driverInfo.AskWhoPaysTPFee = pDriver.AskWhoPaysTPFee || driverDefaults.AskWhoPaysTPFee || oDefaults.AskWhoPaysTPFee;
			driverInfo.DisplayDriverFeeMsgAmount = pDriver.DisplayDriverFeeMsgAmount;
			driverInfo.FleetName = pDriver.Fleets.Fleet;
			if (driverInfo.FleetName.IsNullOrEmpty()) {
				driverInfo.FleetName = pAffiliateDrivers.Fleets.Fleet;
			}
			var feeList = (from p in pDriver.MyAffiliate.DriverFees
						   orderby p.StartAmount
						   select p).Take(1).ToList();

			if (feeList.Count > 0) {
				DriverFee fee = feeList[0];
				string driverFeeMsg = String.Format("Additional {0} Driver Fee applies to ${1}-${2} Swipe Charges\n\n$1 Voucher\nRedemption Fee still applies", fee.Fee.ToString("C"), fee.StartAmount.ToString("F0"), fee.EndAmount.ToString("F0"));
				driverInfo.DriverFeeMsg = driverFeeMsg;
			}


			TaxiPassRedeemers redeemer = TaxiPassRedeemers.GetRedeemerByPhone(oPM, driverInfo.Cell);
			driverInfo.IsRedeemer = !redeemer.IsNullEntity && redeemer.Active;
			driverInfo.DispatchCarmel = driverDefaults.CarmelDispatch;
			driverInfo.DispatchCCSi = driverDefaults.CCSiDispatch;
			driverInfo.CreateMasterVoucher = driverDefaults.DriverCreateMasterVoucher;

			driverInfo.DriverPaySwipeFee = driverDefaults.DriverPaysSwipeFee;
			driverInfo.DriverPaySwipeFeePerAmount = driverDefaults.DriverPaysSwipeFeePerAmount;
			driverInfo.OnBoardingRequired = true;

			driverInfo.OnBoardingRequired = !pDriver.IsRegistered;

			//no need to check if image exists as we now populate bank account info in the system
			driverInfo.HasBankInfo = !pAffiliateDrivers.BankAccountNumber.IsNullOrEmpty() || pDriver.DriverBankInfos.Count > 0; // || !pAffiliateDrivers.AffiliateDriverVoidedCheck.CheckURL.IsNullOrEmpty();

			EntityList<PushMessage> msgList = PushMessage.GetMessages(pDriver);
			EntityList<CabDriverRef> readList = CabDriverRef.GetRecords(oPM, DRIVER_PUSH_TABLE_NAME, pDriver.DriverID, DRIVER_PUSH_FIELD_NAME);
			List<long> readIDList = (from p in readList
									 select Convert.ToInt64(p.ValueString)).ToList();

			msgList = new EntityList<PushMessage>((from p in msgList where !readIDList.Contains(p.PushMessageID) select p).ToList());

			if (driverInfo.OnBoardingRequired) {
				foreach (PushMessage msg in msgList.Where(p => p.OnBoardingMsg).ToList()) {
					driverInfo.OnBoardingMessages.Add(string.Format("{0}|{1}|{2}", msg.PushMessageID, msg.OneTimeMessage, msg.Message));
				}
			}

			foreach (PushMessage msg in msgList.Where(p => !p.OnBoardingMsg).ToList()) {
				driverInfo.PushMessages.Add(string.Format("{0}|{1}|{2}", msg.PushMessageID, msg.OneTimeMessage, msg.Message));
			}

			driverInfo.UpdateDriverProfileTimer = SystemDefaultsDict.SmartPhoneUpdateDriverProfileDuration(oPM);
			if (driverInfo.UpdateDriverProfileTimer == 0) {
				driverInfo.UpdateDriverProfileTimer = 30;
			}

			// Lookup the timeout for push messages
			/*
			driverInfo.PushMessageTimeout = 30;
			if (pDriver.MyAffiliate == null || pDriver.MyAffiliate.IsNullEntity || !pDriver.MyAffiliate.MarketID.HasValue) {
				driverInfo.PushMessageTimeout = Properties.Settings.Default.DefaultPushTimeout;
			} else {

				sql.AppendFormat("usp_MarketRefGetPushTimeout @MarketID={0}", pDriver.MyAffiliate.MarketID);
				Entity[] oTimeOut = DynamicEntities.DynamicDBCall(oPM, sql.ToString());
				if (oTimeOut.Length > 0) {
					driverInfo.PushMessageTimeout = int.Parse(oTimeOut[0]["TimeOut"].ToString());
				}
			}
			*/
			driverInfo.CanSubmitCheck = driverDefaults.DriverCanSubmitCheck;

			// Get Ride dispatch availability
			driverInfo.DispatchAvailable = true;
			sql.Remove(0, sql.Length);
			sql.AppendFormat("usp_DriverGetDispatchAvailability @DriverID = {0}", pDriver.DriverID);
			Entity[] oDriverAvail = DynamicEntities.DynamicDBCall(oPM, sql.ToString());
			if (oDriverAvail != null) {
				driverInfo.DispatchAvailable = bool.Parse(oDriverAvail[0]["Availability"].ToString());
				if (!oDriverAvail[0]["AvailableTime"].ToString().IsNullOrEmpty()) {
					driverInfo.DispatchAvailableTime = oDriverAvail[0]["AvailableTime"].ToString();
				}
			}

			
			return driverInfo;
		}
	}
}