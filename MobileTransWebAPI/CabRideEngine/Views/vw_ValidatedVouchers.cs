﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;
using System.Text;

namespace CabRideEngine {
	/// <summary>
	/// The developer class representing the vw_ValidatedVouchers business object class.
	/// </summary>
	/// <remarks>
	/// <para>Place all application-specific business logic in this class.</para>
	/// <para>
	/// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
	/// and will not overwrite changes.
	/// </para>
	/// <para>
	/// This class inherits from the generated class, its companion in the pair of classes 
	/// defining a business object. Properties in the generated class may be overridden in 
	/// order to change the behavior or add business logic.
	/// </para>
	/// <para>
	/// This class is the final class in the inheritance chain for this business object. 
	/// It should not be inherited.
	/// </para>
	/// </remarks>
	[Serializable]
	public sealed partial class vw_ValidatedVouchers : vw_ValidatedVouchersDataRow {

		#region Constructors -- DO NOT MODIFY

		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new';
		// it is also required if the class is visible from a web service.
		// DO NOT REMOVE **
		private vw_ValidatedVouchers() : this(null) { }

		/// <summary>
		/// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
		/// </summary>
		/// <param name="pRowBuilder"></param>
		public vw_ValidatedVouchers(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static vw_ValidatedVouchers Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      vw_ValidatedVouchers avw_ValidatedVouchers = pManager.CreateEntity<vw_ValidatedVouchers>();
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(avw_ValidatedVouchers, // add id column here //);
		//
		//      // Add custom code here
		//
		//      avw_ValidatedVouchers.AddToManager();
		//      return avw_ValidatedVouchers;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		// Add additional logic to your business object here...
		public static EntityList<vw_ValidatedVouchers> GetValidatedRecords(TaxiPassRedeemers pRedeemer, DateTime pStart, DateTime pEnd) {
			//RdbQuery qry = new RdbQuery(typeof(vw_ValidatedVouchers), ValidateDateEntityColumn, EntityQueryOp.GE, pStart.Date);
			//qry.AddClause(ValidateDateEntityColumn, EntityQueryOp.LT, pEnd.Date.AddDays(1));
			//if (!pRedeemer.Admin) {
			//    qry.AddClause(RedeemerIDEntityColumn, EntityQueryOp.EQ, pRedeemer.TaxiPassRedeemerID);
			//}
			//qry.AddOrderBy(ValidateDateEntityColumn);
			//return pRedeemer.PersistenceManager.GetEntities<vw_ValidatedVouchers>(qry);

			if (pStart == pEnd) {
				pEnd = pEnd.AddDays(1);
			}

			StringBuilder sql = new StringBuilder("SELECT * FROM vw_ValidatedVouchers WHERE ValidateDate >= '");
			sql.Append(pStart.ToString("MM/dd/yyyy hh:mm tt"));
			sql.Append("' AND ValidateDate < '");
			sql.Append(pEnd.ToString("MM/dd/yyyy hh:mm tt"));
			sql.Append("'");
			if (!pRedeemer.Admin) {
				sql.Append(" And RedeemerID = ");
				sql.Append(pRedeemer.TaxiPassRedeemerID);
			}
			sql.Append(" ORDER BY ValidateDate");

			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(vw_ValidatedVouchers), sql.ToString());
			return pRedeemer.PersistenceManager.GetEntities<vw_ValidatedVouchers>(qry);
		}
	}

	#region EntityPropertyDescriptors.vw_ValidatedVouchersPropertyDescriptor
	////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
	////    For example: 
	////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
	//public static partial class EntityPropertyDescriptors {
	//	public partial class vw_ValidatedVouchersPropertyDescriptor : AdaptedPropertyDescriptor {
	//    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
	//    public AdaptedPropertyDescriptor Age {
	//      get { return this.Get("Age"); }
	//    }
	//  }
	//} 
	#endregion

}