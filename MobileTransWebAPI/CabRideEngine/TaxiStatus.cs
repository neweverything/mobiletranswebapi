using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

using Csla.Validation;

namespace CabRideEngine {
	/// <summary>
	/// The developer class representing the TaxiStatus business object class.
	/// </summary>
	/// <remarks>
	/// <para>Place all application-specific business logic in this class.</para>
	/// <para>
	/// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
	/// and will not overwrite changes.
	/// </para>
	/// <para>
	/// This class inherits from the generated class, its companion in the pair of classes 
	/// defining a business object. Properties in the generated class may be overridden in 
	/// order to change the behavior or add business logic.
	/// </para>
	/// <para>
	/// This class is the final class in the inheritance chain for this business object. 
	/// It should not be inherited.
	/// </para>
	/// </remarks>
	[Serializable]
	public sealed partial class TaxiStatus : TaxiStatusDataRow {

		#region Constructors -- DO NOT MODIFY

		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new';
		// it is also required if the class is visible from a web service.
		// DO NOT REMOVE **
		private TaxiStatus() : this(null) { }

		/// <summary>
		/// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
		/// </summary>
		/// <param name="pRowBuilder"></param>
		public TaxiStatus(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static TaxiStatus Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      TaxiStatus aTaxiStatus = pManager.CreateEntity<TaxiStatus>();
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aTaxiStatus, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aTaxiStatus.AddToManager();
		//      return aTaxiStatus;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		// Add additional logic to your business object here...
		public static TaxiStatus Create(PersistenceManager pPM) {
			TaxiStatus rec = null;
			try {
				rec = pPM.CreateEntity<TaxiStatus>();

				// Using the IdeaBlade Id Generation technique
				//pPM.GenerateId(rec, TaxiStatus.TaxiStatusIDEntityColumn);

				rec.AddToManager();

			} catch (Exception ex) {
				throw ex;
			}
			return rec;
		}

		protected override void AddRules(RuleList pList) {
			PropertyRequiredRule.AddToList(pList, StatusEntityColumn.ColumnName);
			pList.Add(IsStatusUnique, StatusEntityColumn.ColumnName);

			base.AddRules(pList);
		}


		private bool IsStatusUnique(object pTarget, RuleArgs e) {
			TaxiStatus rec = pTarget as TaxiStatus;

			RdbQuery qry = new RdbQuery(typeof(TaxiStatus), TaxiStatus.StatusEntityColumn, EntityQueryOp.EQ, rec.Status);
			qry.AddClause(TaxiStatus.TaxiStatusIDEntityColumn, EntityQueryOp.NE, rec.TaxiStatusID);
			EntityList<TaxiStatus> recList = this.PersistenceManager.GetEntities<TaxiStatus>(qry, QueryStrategy.DataSourceThenCache);
			if (recList.Count > 0) {
				e.Description = "Status must be unique!";
				return false;
			}
			return true;
		}

		protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
			if (IsDeserializing)
				return;
			base.OnColumnChanging(pArgs);
			//DoAudit(pArgs);
		}

		private void DoAudit(DataColumnChangeEventArgs pArgs) {
			if (!this.PersistenceManager.IsClient) {
				return;
			}
			if (this.RowState == DataRowState.Added) {
				return;
			}
			if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
				AuditRecord = TaxiStatusAudit.Create(this);
			}
		}

		public override bool DefaultStatus {
			get {
				return base.DefaultStatus;
			}
			set {
				base.DefaultStatus = value;
				if (value) {
					EntityList<TaxiStatus> recList = this.PersistenceManager.GetEntities<TaxiStatus>();
					foreach (TaxiStatus rec in recList) {
						if (rec.TaxiStatusID != this.TaxiStatusID) {
							rec.DefaultStatus = false;
						}
					}
				}
			}
		}

		public static EntityList<TaxiStatus> GetAll(PersistenceManager pPM) {
			RdbQuery qry = new RdbQuery(typeof(TaxiStatus));
			qry.AddOrderBy(TaxiStatus.StatusEntityColumn);
			return pPM.GetEntities<TaxiStatus>(qry);
		}

		public static TaxiStatus GetDefault(PersistenceManager pPM) {
			RdbQuery qry = new RdbQuery(typeof(TaxiStatus), TaxiStatus.DefaultStatusEntityColumn, EntityQueryOp.EQ, true);
			return pPM.GetEntity<TaxiStatus>(qry);
		}

		public static TaxiStatus GetStatus(PersistenceManager pPM, string pStatus) {
			RdbQuery qry = new RdbQuery(typeof(TaxiStatus), TaxiStatus.StatusEntityColumn, EntityQueryOp.EQ, pStatus);
			return pPM.GetEntity<TaxiStatus>(qry);
		}
	}

	#region EntityPropertyDescriptors.TaxiStatusPropertyDescriptor
	////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
	////    For example: 
	////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
	//public static partial class EntityPropertyDescriptors {
	//	public partial class TaxiStatusPropertyDescriptor : BaseEntityPropertyDescriptor {
	//    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
	//    public AdaptedPropertyDescriptor Age {
	//      get { return this.Get("Age"); }
	//    }
	//  }
	//} 
	#endregion

}