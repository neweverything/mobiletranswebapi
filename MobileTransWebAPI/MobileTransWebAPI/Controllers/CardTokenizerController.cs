﻿using CabRideEngineDapper;
using CabRideEngineDapper.Utils;
using Dapper;
using MobileTransWebAPI.Models;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Linq;
using System.Web.Http;
using TaxiPassCommon;

namespace MobileTransWebAPI.Controllers {

	public class CardTokenizerController : ApiController {
		// GET api/cardtokenizer
		public CardInfoResults Get() {
			//MobileTransWebAPI.Models.CardInfo card = new MobileTransWebAPI.Models.CardInfo() {
			//	AffiliateCode = "JDSA",
			//	AccountNo = "ChargePass",
			//	CustomerID = 0,
			//	AffiliateID = 2653,
			//	CardNumber = "4388000068646826",
			//	ExpMonth = 7,
			//	ExpYear = 20,
			//	//CardHolder = "Test eCheck",
			//	//CardType = "eCheck",
			//	Street = "123 Main St",
			//	City = "Test",
			//	State = "NY",
			//	Zip = "10001",
			//	CVV = "123",
			//	AccountHolderName = "Test eCheck",
			//	BankRoutingNo = "322274925",
			//	BankAccountNo = "2040408",
			//	BankAccountType = "Checking"

			//};
			string js = File.ReadAllText(@"c:\temp\test.json");

			Models.CardInfo card = JsonConvert.DeserializeObject<Models.CardInfo>(js);
			return Post(card);
		}

		//// GET api/cardtokenizer/5
		//public string Get(int id) {
		//    return "value";
		//}

		// POST api/cardtokenizer
		public CardInfoResults Post([FromBody]MobileTransWebAPI.Models.CardInfo cardInfo) {
			CardInfoResults results = new CardInfoResults();

			if (cardInfo.AffiliateID > 0 && cardInfo.CustomerID == 0) {
				// Create or Get Customer
				Affiliate aff = Affiliate.GetAffiliate(cardInfo.AffiliateID);
				if (!aff.AffiliateCode.Equals(cardInfo.AffiliateCode)) {
					results.Valid = false;
					results.ErrorMsg = "Invalid Account";
					return results;
				}

				if (!cardInfo.BankRoutingNo.IsNullOrEmpty()) {
					var oInfo = CabRideEngineDapper.FedACHDir.GetByRoutingNo(cardInfo.BankRoutingNo);
					if (oInfo.IsNullEntity) {
						results.Valid = false;
						results.ErrorMsg = "Routing number is not valid";
						return results;
					}
					if (!new string[] { "Savings", "Checking" }.Contains(cardInfo.BankAccountType)) {
						results.Valid = false;
						results.ErrorMsg = "Account type must be Checking or Savings";
						return results;
					}
				}

				CustomerCards card = CustomerCards.GetCardByCardHolder(cardInfo.AffiliateID, cardInfo.Name, cardInfo.Number);
				if (card.IsNullEntity) {
					if (card.CustomerID == 0) {
						// Check if Customer Name exists
						Customer customer = Customer.GetCustomerByNameAndPhone(cardInfo.Name, cardInfo.AffiliateID, cardInfo.PhoneNo);
						if (customer.IsNullEntity) {
							try {
								customer = Customer.Create(cardInfo.AccountNo);
								customer.AffiliateID = cardInfo.AffiliateID;
								customer.Name = cardInfo.Name;
								customer.Street = cardInfo.Street;
								customer.Suite = cardInfo.Suite;
								customer.City = cardInfo.City;
								customer.State = cardInfo.State;
								customer.ZIPCode = cardInfo.Zip;
								customer.CellPhone = cardInfo.PhoneNo;
								customer.AccountNo = cardInfo.AccountNo;
								customer.Save();
							} catch (Exception ex) {
								Console.WriteLine(ex.Message);
								results.Valid = false;
								results.ErrorMsg = $"Saving Customer Error";
								return results;
							}
						}
						card.CustomerID = customer.CustomerID;
					}
				}
				cardInfo.CustomerID = card.CustomerID;

			}

			Customer cust = Customer.GetCustomer(cardInfo.CustomerID);
			if (cust.IsNullEntity || cust.AccountNo != cardInfo.AccountNo || cardInfo.AccountNo.IsNullOrEmpty()) {
				results.Valid = false;
				results.ErrorMsg = "Invalid Customer ID or Account No";
				return results;
			}

			if (cardInfo.ExpYear < 100 && cardInfo.BankRoutingNo.IsNullOrEmpty()) {
				cardInfo.ExpYear += 2000;
			}

			if (cardInfo.BankRoutingNo.IsNullOrEmpty() && (cardInfo.ExpMonth == 0 || cardInfo.ExpMonth > 12 || cardInfo.ExpYear < DateTime.Now.Year)) {
				results.Valid = false;
				results.ErrorMsg = "Invalid Expiry Month or Year";
				return results;
			}

			try {
				using (var conn = SqlHelper.OpenSqlConnection()) {
					CustomerCards myCard = CustomerCards.GetNullEntity();
					bool skipValidation = false;
					if (cardInfo.BankRoutingNo.IsNullOrEmpty()) {
						myCard = CustomerCards.GetCard(cardInfo.CustomerID, cardInfo.Number);
					} else {
						skipValidation = true;
						myCard = CustomerCards.GetECheck(cardInfo.CustomerID, cardInfo.Number);
					}
					if (myCard.IsNullEntity) {
						myCard = CustomerCards.Create();
						myCard.CustomerID = cust.CustomerID;
						myCard.EncryptCard(cardInfo.Number, skipValidation);
						myCard.CardNumberDisplay = cardInfo.Number.CardNumberDisplay();
					}
					if (cardInfo.BankRoutingNo.IsNullOrEmpty()) {
						myCard.CardExpiryAsMMYY = string.Format("{0}{1}", cardInfo.ExpMonth.ToString().PadLeft(2, '0'), cardInfo.ExpYear);
						myCard.CardType = cardInfo.CardType;
						myCard.CVVForGrid = cardInfo.CVV;
					} else {
						myCard.CardType = "eCheck";
						myCard.BankRoutingNo = cardInfo.BankRoutingNo;
						myCard.CardNumberDisplay = $"{cardInfo.BankRoutingNo}-{cardInfo.Number.Right(3)}".CardNumberDisplay();
						myCard.BankAccountType = cardInfo.BankAccountType;
					}
					myCard.Name = cardInfo.Name;
					myCard.Street = cardInfo.Street;
					myCard.Suite = cardInfo.Suite;
					myCard.City = cardInfo.City;
					myCard.State = cardInfo.State;
					myCard.ZIPCode = cardInfo.Zip;

					myCard.Save();

					results.Token = conn.Query<string>("usp_TokenCreate", new { TableName = "CustomerCards", ID = myCard.CustCardID, TokenLength = 20, ModifiedBy = CabRideDapperSettings.LoggedInUser, ModifiedDate = DateTime.UtcNow }, commandType: System.Data.CommandType.StoredProcedure).FirstOrDefault();
					results.Valid = true;

					if (!cardInfo.CardNumber.IsNullOrEmpty()) {
						CardBin bin = CardBin.GetCardBinInfo(conn, cardInfo.CardNumber);
						results.CardBin = JsonConvert.SerializeObject(bin);
					}

				}
			} catch (Exception ex) {
				results.ErrorMsg = ex.Message;
			}

			return results;
		}

		//// PUT api/cardtokenizer/5
		//public void Put(int id, [FromBody]string value) {
		//}

		//// DELETE api/cardtokenizer/5
		//public void Delete(int id) {
		//}
	}
}
