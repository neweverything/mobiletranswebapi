﻿namespace MobileTransWebAPI.Models {
	public class KeyValue {

		public KeyValue(string pKey, object pValue) {
			Key = pKey;
			Value = pValue;
		}

		public string Key { get; set; }
		public object Value { get; set; }
	}
}