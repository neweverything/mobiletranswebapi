using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using System.Text;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;
using System.Collections.Generic;

namespace CabRideEngine {
	/// <summary>
	/// The developer class representing the Fleets business object class.
	/// </summary>
	/// <remarks>
	/// <para>Place all application-specific business logic in this class.</para>
	/// <para>
	/// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
	/// and will not overwrite changes.
	/// </para>
	/// <para>
	/// This class inherits from the generated class, its companion in the pair of classes 
	/// defining a business object. Properties in the generated class may be overridden in 
	/// order to change the behavior or add business logic.
	/// </para>
	/// <para>
	/// This class is the final class in the inheritance chain for this business object. 
	/// It should not be inherited.
	/// </para>
	/// </remarks>
	[Serializable]
	public sealed partial class Fleets : FleetsDataRow {

		#region Constructors -- DO NOT MODIFY

		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" Fleet
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new';
		// it is also required if the class is visible from a web service.
		// DO NOT REMOVE **
		private Fleets() : this(null) { }

		/// <summary>
		/// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
		/// </summary>
		/// <param name="pRowBuilder"></param>
		public Fleets(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static Fleets Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      Fleets aFleets = pManager.CreateEntity<Fleets>();
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aFleets, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aFleets.AddToManager();
		//      return aFleets;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		// Add additional logic to your business object here...
		public static Fleets Create(PersistenceManager pPM) {
			Fleets oFleet = null;

			try {
				// Creates the Country but it is not yet accessible to the application
				oFleet = (Fleets)pPM.CreateEntity(typeof(Fleets));
				oFleet.CreatedDate = DateTime.UtcNow;
				oFleet.AddToManager();

			} catch (Exception ex) {
				throw ex;
			}
			return oFleet;
		}

		// ******************************************************************
		// Get all Fleets
		// ******************************************************************
		public static EntityList<Fleets> GetFleets(PersistenceManager mPM) {
			EntityList<Fleets> oFleets = mPM.GetEntities<Fleets>();
			oFleets.ApplySort(Fleets.FleetEntityColumn.ColumnName, System.ComponentModel.ListSortDirection.Ascending, false);
			return oFleets;
		}

		// ******************************************************************
		// Get a Fleet by name
		// ******************************************************************
		public static Fleets GetFleetByName(PersistenceManager mPM, string pFleet) {
			RdbQuery qry = new RdbQuery(typeof(Fleets), Fleets.FleetEntityColumn, EntityQueryOp.EQ, pFleet);
			return mPM.GetEntity<Fleets>(qry);
		}

		// ******************************************************************
		// Get a Fleet by ID
		// ******************************************************************
		public static Fleets GetFleetByID(PersistenceManager mPM, long pFleetID) {
			RdbQuery qry = new RdbQuery(typeof(Fleets), Fleets.FleetIDEntityColumn, EntityQueryOp.EQ, pFleetID);
			return mPM.GetEntity<Fleets>(qry);
		}
	}

	#region EntityPropertyDescriptors.FleetsPropertyDescriptor
	////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
	////    For example: 
	////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
	//public static partial class EntityPropertyDescriptors {
	//	public partial class FleetsPropertyDescriptor : BaseEntityPropertyDescriptor {
	//    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
	//    public AdaptedPropertyDescriptor Age {
	//      get { return this.Get("Age"); }
	//    }
	//  }
	//} 
	#endregion

}