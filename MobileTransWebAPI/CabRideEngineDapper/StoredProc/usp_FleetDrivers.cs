﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Dapper;
using TaxiPassCommon;
using CabRideEngineDapper.Utils;
using System.Data;


namespace CabRideEngineDapper {
	public partial class usp_FleetDrivers {

		public long AffiliateID { get; set; }
		public long AffiliateDriverID { get; set; }
		public string Market { get; set; }
		public string AffiliateName { get; set; }
		public string DriverName { get; set; }
		public string Cell { get; set; }
		public string CallInCell { get; set; }
		public string EMail { get; set; }
		public bool Active { get; set; }
		public string Status { get; set; }
		public DateTime SignUpDate { get; set; }
		public string TransCardCardNumber { get; set; }
		public string TransCardAdminNo { get; set; }
		public DateTime? LastTransDate { get; set; }
		public int? TransTotalCount { get; set; }
		public decimal? TransDriverTotals { get; set; }
		public decimal? TransTPFeesTotal { get; set; }
		public string LastDriverLicense_LicenseNo { get; set; }
		public string LastDriverLicense_Name { get; set; }
		public string LastDriverLicense_Street { get; set; }
		public string LastDriverLicense_City { get; set; }
		public string LastDriverLicense_State { get; set; }
		public bool? NoImprint { get; set; }
		public DateTime? DriverStatusUpdated { get; set; }
		public string DriverStatusUpdatedBy { get; set; }
		public string Fleet { get; set; }

		public static List<usp_FleetDrivers> Execute(SqlConnection pConn, string pCellOrName = "", int pTop = 200) {
			DynamicParameters param = new DynamicParameters();
			param.Add("CellOrName", pCellOrName);
			param.Add("Top", pTop);
			return pConn.Query<usp_FleetDrivers>("usp_FleetDriver", param, commandType: CommandType.StoredProcedure, commandTimeout: 300).ToList();
		}
	
	}
}
