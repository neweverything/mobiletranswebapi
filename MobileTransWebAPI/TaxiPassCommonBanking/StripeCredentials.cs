﻿namespace TaxiPassCommon.Banking {
    public class StripeCredentials : IGatewayCredentials {

        public StripeCredentials() {
            CreditCardProcessor = CardProcessors.Stripe;
        }

        public string ProdURL { get; set; }
        public string TestURL { get; set; }

        public string MerchantID { get; set; }
        public string APIKey { get; set; }

        public string TestAPIKey { get; set; }
        public string TestMerchantID { get; set; }

        public int Timeout { get; set; }

        public long VerisignAccountID { get; set; }

        public bool Validate { get; set; }

        public int ProcessingOrder { get; set; }

        public CardProcessors CreditCardProcessor { get; set; }

        public string CardType { get; set; }

        public string BillingDescriptor { get; set; }

        public bool ExcludeBillingDescriptorPrefix { get; set; }

        public string TerminalType { get; set; }

        public CardProcessors MyGateway() {
            return CreditCardProcessor;
        }


    }
}
