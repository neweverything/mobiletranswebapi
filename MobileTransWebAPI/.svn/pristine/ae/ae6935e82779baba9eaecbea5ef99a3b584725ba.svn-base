﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using System.Web.Script.Serialization;

using IdeaBlade.Persistence;

using TaxiPassCommon;

using CabRideEngine;
using CabRideEngine.Utils;

using MobileTransWebAPI.Models;

namespace MobileTransWebAPI.Controllers {
	public class SignUpNewDriverController : ApiController {

		public DriverSignUpResult GetResultSignUpNewDriver(string pRequest) {
			JavaScriptSerializer js = new JavaScriptSerializer();
			DriverSignUpResult result = new DriverSignUpResult();

			DriverSignUpInfo oDriverSignUpInfo = js.Deserialize<DriverSignUpInfo>(pRequest.DecryptIceKey());
			WebApiApplication.LogIt("SignUpNewDriver", pRequest);

			DateTime requestDate = Convert.ToDateTime(oDriverSignUpInfo.RequestKey.DecryptIceKey());
			if (DateTime.Now.ToUniversalTime().Subtract(requestDate).TotalMinutes > 1) {
				result.ErrorMsg = "Invalid Access";
				return result;
			}

			PersistenceManager oPM = WebApiApplication.UserPM;
			if (oPM == null) {
				result.ErrorMsg = "Could not connect to database";
				return result;
			}


			Drivers oDriver = Drivers.GetDriverByPhone(oPM, oDriverSignUpInfo.NewDriverCellNo);
			if (!oDriver.IsNullEntity) {
				result.ErrorMsg = String.Format("Driver is already using the TaxiPass app on the {0} {1} phone", oDriver.HardwareManufacturer, ("" + oDriver.HardwareOS).Split(' ')[0]);
				return result;
			}

			AffiliateDrivers oAffiliateDriver = AffiliateDrivers.GetDriver(oPM, oDriverSignUpInfo.ReferringAffiliateDriverID);
			if (oAffiliateDriver.IsNullEntity || oAffiliateDriver.Cell != oDriverSignUpInfo.ReferringAffiliateDriverCell) {
				result.ErrorMsg = "Referred Driver Access Error";
				return result;
			}

			if (!oAffiliateDriver.Affiliate.AffiliateDriverDefaults.IVRSignUp) {
				result.ErrorMsg = "Sign up new driver is currently unavailable";
				return result;
			}

			AffiliateDrivers affDriver = AffiliateDrivers.GetDriverByCallInCell(oPM, oDriverSignUpInfo.NewDriverCellNo);
			if (affDriver.IsNullEntity) {
				affDriver = CreateNewDriver(oAffiliateDriver, oDriverSignUpInfo);
				//affDriver = AffiliateDrivers.Create(myDriver.Affiliate);
				affDriver.Name = oDriverSignUpInfo.NewDriverCellNo;
				affDriver.Cell = oDriverSignUpInfo.NewDriverCellNo;
				affDriver.Save();
			}

			oDriver = Drivers.CreateAndroidDriver(affDriver, oDriverSignUpInfo.NewDriverCellNo);
			if (!oDriverSignUpInfo.CabNo.IsNullOrEmpty()) {
				oDriver.VehicleNo = oDriverSignUpInfo.CabNo;
			}
			if (oDriver.VehicleNo.IsNullOrEmpty()) {
				oDriver.VehicleNo = affDriver.Vehicles.VehicleNo;
			}
			if (oDriver.EMail.IsNullOrEmpty()) {
				oDriver.EMail = oDriver.BoostPhone;
			}


			oDriver.Save(String.Format("Mobile Sign Up - {0}", oAffiliateDriver.Name));
			//send link to phone
			oDriver.SendMsgToPhone("TaxiPass App: http://bit.ly/cabpay");

			return result;
		}

		private AffiliateDrivers CreateNewDriver(AffiliateDrivers pReferringDriver, DriverSignUpInfo pSignUpInfo) {
			PersistenceManager oPM = pReferringDriver.PersistenceManager;
			Affiliate oAffiliate = pReferringDriver.Affiliate;
			if (pSignUpInfo.NewDriverCellNo.Length != 10) {
				return oPM.GetNullEntity<AffiliateDrivers>();
			}
			AffiliateDrivers affDriver = AffiliateDrivers.GetDriverByCallInCell(oPM, pSignUpInfo.NewDriverCellNo);
			if (affDriver.IsNullEntity) {
				affDriver = AffiliateDrivers.Create(oAffiliate);
				affDriver.Active = true;
				affDriver.CallInCell = pSignUpInfo.NewDriverCellNo;
				affDriver.Cell = pSignUpInfo.NewDriverCellNo;
				affDriver.Name = pSignUpInfo.NewDriverCellNo;
				affDriver.Pin = pSignUpInfo.NewDriverCellNo.Right(4);
				affDriver.Language = "EN";
			}

			affDriver.PaymentMethod = (int)PaymentMethod.Redeemer;

			if (!pSignUpInfo.CabNo.IsNullOrEmpty()) {
				Vehicles oVehicle = Vehicles.GetVehicle(affDriver.Affiliate, pSignUpInfo.CabNo);
				if (oVehicle.IsNullEntity) {
					oVehicle = Utils.Common.CreateVehicleRecord(affDriver.Affiliate, pSignUpInfo.CabNo, String.Format("Mobile SignUp - {0}", pReferringDriver.Cell));
				}
				affDriver.VehicleID = oVehicle.VehicleID;
			}


			affDriver.Save(String.Format("Mobile SignUp - {0}", pReferringDriver.Cell));

			string platform = Platforms.Android.ToString();
			//if (pSignUpInfo.iPhone) {
			//	platform = Platforms.iPhone.ToString();
			//}
			if (!pReferringDriver.IsNullEntity && pReferringDriver.Cell != affDriver.Cell) {
				AffiliateDriverReferrals referral = AffiliateDriverReferrals.GetByReferredDriver(oPM, affDriver.AffiliateDriverID);
				if (referral.IsNullEntity) {
					referral = AffiliateDriverReferrals.Create(pReferringDriver, affDriver.AffiliateDriverID, platform);
				} else {
					referral.ReferralDate = DateTime.Now;
					referral.AffiliateDriverID = pSignUpInfo.ReferringAffiliateDriverID;
					referral.Platform = platform;
					referral.Save(String.Format("Twilio Caller - {0}", pSignUpInfo));
				}
			}

			return affDriver;
		}

	}
}
