﻿using System.Collections.Generic;
using TaxiPassCommon.Banking.Models;

namespace MobileTransWebAPI.Models {

	public class CheckStatusResponse {
		public int ErrorCode { get; set; }
		public string ErrorMsg { get; set; }
		public List<CheckStatus> CheckStatusList { get; set; }
	}
	
}