using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Runtime.Serialization;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

namespace CabRideEngine {
 
  #region ZIPCodeAuditDataTable
  /// <summary>
  /// The generated <see cref="T:IdeaBlade.Persistence.EntityTable"/> class  
  /// containing all <see cref="ZIPCodeAudit"/> objects.
  /// </summary>
  /// <remarks>
  /// ****************************************************
  /// <para>** DO NOT MODIFY THIS CLASS - AutoGenerated Code</para>
  /// <para>** Any changes made to this code will be lost</para>
  /// <para>** the next time this code is regenerated.</para>
  /// <para>**</para>
  /// <para>** Generated from:   EntityBaseclass.template</para>
  /// <para>** Last modified:    2011-06-29T13:00:28.3777579-07:00</para>
  /// <para>** Template version: 3.110127</para>
  /// ****************************************************
  /// </remarks>
  [BindingBrowsable(false)]
  [Serializable]
  public partial class ZIPCodeAuditDataTable : IdeaBlade.Persistence.EntityTable, 
     IdeaBlade.Persistence.Rdb.IRdbTable {
  
    /// <summary>
    /// Initializes a new instance of the ZIPCodeAuditDataTable class with no arguments.
    /// </summary>
    public ZIPCodeAuditDataTable() {}

    /// <summary>
    /// Initializes a new instance of the ZIPCodeAuditDataTable class 
    /// with the <see cref="System.Runtime.Serialization.SerializationInfo"/> 
    /// and the <see cref="System.Runtime.Serialization.StreamingContext"/>.
    /// </summary>
    /// <param name="pInfo">The data needed to serialize or deserialize an object.</param>
    /// <param name="pContext">The source and destination of a given serialized stream.</param>
    protected ZIPCodeAuditDataTable(SerializationInfo pInfo, StreamingContext pContext) :
      base(pInfo, pContext) {}

    /// <summary>
    /// Creates a new instance of the <see cref="ZIPCodeAuditDataRow"/>.
    /// Constructs a row from the builder.
    /// <para>** For internal use only. **</para>
    /// </summary>
    /// <param name="pRowBuilder"></param>
    /// <returns>The <see cref="ZIPCodeAuditDataRow"/>.</returns>
    protected override DataRow NewRowFromBuilder(DataRowBuilder pRowBuilder) {
      return new ZIPCodeAudit(pRowBuilder);
    }

    /// <summary>Returns the <see cref="System.Type"/> of <see cref="ZIPCodeAudit"/>.</summary>
    protected override Type GetRowType() {
      return typeof(ZIPCodeAudit);
    }
    
    /// <summary>
    /// Gets the name of the <see cref="T:IdeaBlade.Rdb.RdbKey"/> or 
    /// <see cref="T:IdeaBlade.Persistence.WS.WsKey"/> defined in <b>IdeaBlade.ibconfig</b>. 
    /// </summary>
    /// <remarks>For this <see cref="ZIPCodeAudit"/>, the DataSourceKeyName="Audit".</remarks>
    public override String DataSourceKeyName {
      get { return @"Audit"; }
    }

    #region Column properties
    //**************************************
    //* Column properties
    //**************************************
    /// <summary>Gets the ZIPCodeHistoryId <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn ZIPCodeHistoryIdColumn {
      get {
        if (mZIPCodeHistoryIdColumn!=null) return mZIPCodeHistoryIdColumn;
        mZIPCodeHistoryIdColumn = GetColumn("ZIPCodeHistoryId", true);
        return mZIPCodeHistoryIdColumn;
      }
    }
    private DataColumn mZIPCodeHistoryIdColumn;
    
    /// <summary>Gets the ZIPCode <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn ZIPCodeColumn {
      get {
        if (mZIPCodeColumn!=null) return mZIPCodeColumn;
        mZIPCodeColumn = GetColumn("ZIPCode", true);
        return mZIPCodeColumn;
      }
    }
    private DataColumn mZIPCodeColumn;
    
    /// <summary>Gets the City <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn CityColumn {
      get {
        if (mCityColumn!=null) return mCityColumn;
        mCityColumn = GetColumn("City", true);
        return mCityColumn;
      }
    }
    private DataColumn mCityColumn;
    
    /// <summary>Gets the State <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn StateColumn {
      get {
        if (mStateColumn!=null) return mStateColumn;
        mStateColumn = GetColumn("State", true);
        return mStateColumn;
      }
    }
    private DataColumn mStateColumn;
    
    /// <summary>Gets the County <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn CountyColumn {
      get {
        if (mCountyColumn!=null) return mCountyColumn;
        mCountyColumn = GetColumn("County", true);
        return mCountyColumn;
      }
    }
    private DataColumn mCountyColumn;
    
    /// <summary>Gets the Country <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn CountryColumn {
      get {
        if (mCountryColumn!=null) return mCountryColumn;
        mCountryColumn = GetColumn("Country", true);
        return mCountryColumn;
      }
    }
    private DataColumn mCountryColumn;
    
    /// <summary>Gets the RowVersion <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn RowVersionColumn {
      get {
        if (mRowVersionColumn!=null) return mRowVersionColumn;
        mRowVersionColumn = GetColumn("RowVersion", true);
        return mRowVersionColumn;
      }
    }
    private DataColumn mRowVersionColumn;
    
    /// <summary>Gets the ModifiedBy <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn ModifiedByColumn {
      get {
        if (mModifiedByColumn!=null) return mModifiedByColumn;
        mModifiedByColumn = GetColumn("ModifiedBy", true);
        return mModifiedByColumn;
      }
    }
    private DataColumn mModifiedByColumn;
    
    /// <summary>Gets the ModifiedDate <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn ModifiedDateColumn {
      get {
        if (mModifiedDateColumn!=null) return mModifiedDateColumn;
        mModifiedDateColumn = GetColumn("ModifiedDate", true);
        return mModifiedDateColumn;
      }
    }
    private DataColumn mModifiedDateColumn;
    
    #endregion    

    /// <summary>Creates the schema mapping information for this ZIPCodeAuditDataTable.</summary>
    /// <returns>
    /// The <see cref="T:IdeaBlade.Persistence.TableMappingInfo"/> for this ZIPCodeAuditDataTable.
    /// </returns>
    protected override TableMappingInfo CreateTableMappingInfo() {
      RdbTableMappingInfo mappingInfo = new RdbTableMappingInfo(this);
      mappingInfo.SourceOwnerName = "dbo"; 
      mappingInfo.SourceTableName = "ZIPCodeAudit"; 
      mappingInfo.ConcurrencyColumnName = ""; 
      // ColumnSelectionOption: selectAllColumns
      mappingInfo.SourceColumnNames = "*"; 
   
      DataColumnMappingCollection columnMappings = mappingInfo.TableMapping.ColumnMappings;
      columnMappings.Clear();
      columnMappings.Add("ZIPCodeHistoryId", "ZIPCodeHistoryId");
      columnMappings.Add("ZIPCode", "ZIPCode");
      columnMappings.Add("City", "City");
      columnMappings.Add("State", "State");
      columnMappings.Add("County", "County");
      columnMappings.Add("Country", "Country");
      columnMappings.Add("RowVersion", "RowVersion");
      columnMappings.Add("ModifiedBy", "ModifiedBy");
      columnMappings.Add("ModifiedDate", "ModifiedDate");
      return mappingInfo;
    }
  
    /// <summary>** For internal use only. **</summary>
    protected override void UpdateTableMappingInfo() {
      base.UpdateTableMappingInfo();
    }

    /// <summary>
    /// Initializes ZIPCodeAudit <see cref="System.Data.DataColumn"/> properties.
    /// </summary>
    public override void InitColumnProperties() {
      base.InitColumnProperties();

      ZIPCodeHistoryIdColumn.Caption = "ZIPCodeHistoryId";
      ZIPCodeColumn.Caption = "ZIPCode";
      CityColumn.Caption = "City";
      StateColumn.Caption = "State";
      CountyColumn.Caption = "County";
      CountryColumn.Caption = "Country";
      RowVersionColumn.Caption = "RowVersion";
      ModifiedByColumn.Caption = "ModifiedBy";
      ModifiedDateColumn.Caption = "ModifiedDate";
    }
  }
  #endregion
  
  #region ZIPCodeAuditDataRow
  /// <summary>
  /// The generated base class for <see cref="ZIPCodeAudit"/>.
  /// </summary>
  /// <remarks>
  /// <para>
  /// It represents a row of persistable data in the <see cref="ZIPCodeAuditDataTable"/>
  /// and derives from <see cref="T:IdeaBlade.Persistence.Entity"/>.
	/// </para>
  /// ****************************************************
  /// <para>** DO NOT MODIFY THIS CLASS - AutoGenerated Code</para>
  /// <para>** Any changes made to this code will be lost</para>
  /// <para>** the next time this code is regenerated.</para>
  /// <para>**</para>
  /// <para>** Generated from:   EntityBaseclass.template</para>
  /// <para>** Last modified:    2011-06-29T13:00:28.3777579-07:00</para>
  /// <para>** Template version: 3.110127</para>
  /// ****************************************************
  /// </remarks>  
  [Serializable]
  public abstract partial class ZIPCodeAuditDataRow : IdeaBlade.Persistence.Entity, 
     IdeaBlade.Persistence.Rdb.IRdbEntity {
  
    /// <summary>
    /// Initializes a new instance of the ZIPCodeAuditDataRow.
    /// Constructs a row from the builder.
    /// <para>** For internal use only. **</para>
    /// </summary>
    /// <param name="pRowBuilder"></param>
    protected ZIPCodeAuditDataRow(DataRowBuilder pRowBuilder)
      : base(pRowBuilder) {
    }

    /// <summary>Gets the <see cref="ZIPCodeAuditDataTable"/>.</summary>
    [BindingBrowsable(false)]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public new ZIPCodeAuditDataTable TypedTable {
      get { return (ZIPCodeAuditDataTable) base.Table; }
    }

    /// <summary>Returns the <see cref="System.Type"/> of <see cref="ZIPCodeAuditDataTable"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public static Type GetTableType() {
      return typeof(ZIPCodeAuditDataTable);
    }

    /// <summary>
    /// Gets the SQL 'where' clause used to restrict which 
    /// database records are accessible via this entity. 
    /// </summary>
    /// <remarks>
    /// The default property value is defined in the Current Class 
    /// Properties tab in the <b>IdeaBlade DevForce Object Mapping Tool</b>.
    /// <para>
    /// The 'where' clause will be used, in addition to any other 
    /// query-specific criteria, in every retrieval of entities of this type.
    /// </para>
    /// </remarks>
    [BindingBrowsable(false)]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public virtual string SqlWhereClause {
      get { return ""; }
    }
    
    /// <summary>
    /// Determines whether to use the 'distinct' keyword when 
    /// generating the SQL select clause for objects of this type.
    /// </summary>
    /// <remarks>Defaults to false, but may be overridden in subclasses.</remarks>
    [BindingBrowsable(false)]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public virtual bool ForceSqlDistinct {
      get { return false; }
    }
    

    #region Enums
    //**************************************
    //* Enums
    //**************************************
    #endregion
	
    #region Relation properties
    //**************************************
    //* Relation properties
    //**************************************
    #endregion
    
    #region EntityColumn definitions
    //**************************************
    //* Static EntityColumn definitions
    //**************************************
    /// <summary>The ZIPCodeHistoryId <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn ZIPCodeHistoryIdEntityColumn =
      new EntityColumn(typeof(ZIPCodeAudit), "ZIPCodeHistoryId", typeof(System.Int64), false, true, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The ZIPCode <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn ZIPCodeEntityColumn =
      new EntityColumn(typeof(ZIPCodeAudit), "ZIPCode", typeof(System.String), false, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The City <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn CityEntityColumn =
      new EntityColumn(typeof(ZIPCodeAudit), "City", typeof(System.String), false, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The State <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn StateEntityColumn =
      new EntityColumn(typeof(ZIPCodeAudit), "State", typeof(System.String), false, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The County <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn CountyEntityColumn =
      new EntityColumn(typeof(ZIPCodeAudit), "County", typeof(System.String), true, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The Country <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn CountryEntityColumn =
      new EntityColumn(typeof(ZIPCodeAudit), "Country", typeof(System.String), false, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The RowVersion <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn RowVersionEntityColumn =
      new EntityColumn(typeof(ZIPCodeAudit), "RowVersion", typeof(System.Int32), false, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The ModifiedBy <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn ModifiedByEntityColumn =
      new EntityColumn(typeof(ZIPCodeAudit), "ModifiedBy", typeof(System.String), true, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The ModifiedDate <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn ModifiedDateEntityColumn =
      new EntityColumn(typeof(ZIPCodeAudit), "ModifiedDate", typeof(System.DateTime), true, false, false, DataSourceAccessType.ReadWrite);      
    #endregion
    
    #region Properties
    //**************************************
    //* ZIPCodeHistoryId methods
    //**************************************
    /// <summary>Gets the ZIPCodeHistoryId <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn ZIPCodeHistoryIdColumn {
      get { return TypedTable.ZIPCodeHistoryIdColumn; }
    }

    /// <summary>Gets the ZIPCodeHistoryId.</summary>
    [DBDataType(typeof(System.Int64))]
    public virtual System.Int64 ZIPCodeHistoryId {
      get { 
        System.Int64 result_;
        if (GetInterceptor<System.Int64>("ZIPCodeHistoryId", GetZIPCodeHistoryIdImpl, out result_)) return result_;
        return GetZIPCodeHistoryIdImpl();
      }
    }
    private System.Int64 GetZIPCodeHistoryIdImpl() {
      return (System.Int64) GetColumnValue(ZIPCodeHistoryIdColumn, typeof(System.Int64), false); 
    }
    
    //**************************************
    //* ZIPCode methods
    //**************************************
    /// <summary>Gets the ZIPCode <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn ZIPCodeColumn {
      get { return TypedTable.ZIPCodeColumn; }
    }

    /// <summary>Gets or sets the ZIPCode.</summary>
    [MaxTextLength(10)]
    [DBDataType(typeof(System.String))]
    public virtual System.String ZIPCode {
      get { 
        System.String result_;
        if (GetInterceptor<System.String>("ZIPCode", GetZIPCodeImpl, out result_)) return result_;
        return GetZIPCodeImpl();
      }
      set { 
          if (!SetInterceptor<System.String>("ZIPCode", value, SetZIPCodeImpl)) {
            SetZIPCodeImpl(value);
          }  
      }    
    }
    private System.String GetZIPCodeImpl() {
      return (System.String) GetColumnValue(ZIPCodeColumn, typeof(System.String), false); 
    }
    private void SetZIPCodeImpl(System.String value) {
      SetColumnValue(ZIPCodeColumn, value);
    }
    
    //**************************************
    //* City methods
    //**************************************
    /// <summary>Gets the City <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn CityColumn {
      get { return TypedTable.CityColumn; }
    }

    /// <summary>Gets or sets the City.</summary>
    [MaxTextLength(30)]
    [DBDataType(typeof(System.String))]
    public virtual System.String City {
      get { 
        System.String result_;
        if (GetInterceptor<System.String>("City", GetCityImpl, out result_)) return result_;
        return GetCityImpl();
      }
      set { 
          if (!SetInterceptor<System.String>("City", value, SetCityImpl)) {
            SetCityImpl(value);
          }  
      }    
    }
    private System.String GetCityImpl() {
      return (System.String) GetColumnValue(CityColumn, typeof(System.String), false); 
    }
    private void SetCityImpl(System.String value) {
      SetColumnValue(CityColumn, value);
    }
    
    //**************************************
    //* State methods
    //**************************************
    /// <summary>Gets the State <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn StateColumn {
      get { return TypedTable.StateColumn; }
    }

    /// <summary>Gets or sets the State.</summary>
    [MaxTextLength(2)]
    [DBDataType(typeof(System.String))]
    public virtual System.String State {
      get { 
        System.String result_;
        if (GetInterceptor<System.String>("State", GetStateImpl, out result_)) return result_;
        return GetStateImpl();
      }
      set { 
          if (!SetInterceptor<System.String>("State", value, SetStateImpl)) {
            SetStateImpl(value);
          }  
      }    
    }
    private System.String GetStateImpl() {
      return (System.String) GetColumnValue(StateColumn, typeof(System.String), false); 
    }
    private void SetStateImpl(System.String value) {
      SetColumnValue(StateColumn, value);
    }
    
    //**************************************
    //* County methods
    //**************************************
    /// <summary>Gets the County <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn CountyColumn {
      get { return TypedTable.CountyColumn; }
    }

    /// <summary>Gets or sets the County.</summary>
    [MaxTextLength(30)]
    [DBDataType(typeof(System.String))]
    public virtual System.String County {
      get { 
        System.String result_;
        if (GetInterceptor<System.String>("County", GetCountyImpl, out result_)) return result_;
        return GetCountyImpl();
      }
      set { 
          if (!SetInterceptor<System.String>("County", value, SetCountyImpl)) {
            SetCountyImpl(value);
          }  
      }    
    }
    private System.String GetCountyImpl() {
      return (System.String) GetColumnValue(CountyColumn, typeof(System.String), true); 
    }
    private void SetCountyImpl(System.String value) {
      SetColumnValue(CountyColumn, value);
    }
    
    //**************************************
    //* Country methods
    //**************************************
    /// <summary>Gets the Country <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn CountryColumn {
      get { return TypedTable.CountryColumn; }
    }

    /// <summary>Gets or sets the Country.</summary>
    [MaxTextLength(10)]
    [DBDataType(typeof(System.String))]
    public virtual System.String Country {
      get { 
        System.String result_;
        if (GetInterceptor<System.String>("Country", GetCountryImpl, out result_)) return result_;
        return GetCountryImpl();
      }
      set { 
          if (!SetInterceptor<System.String>("Country", value, SetCountryImpl)) {
            SetCountryImpl(value);
          }  
      }    
    }
    private System.String GetCountryImpl() {
      return (System.String) GetColumnValue(CountryColumn, typeof(System.String), false); 
    }
    private void SetCountryImpl(System.String value) {
      SetColumnValue(CountryColumn, value);
    }
    
    //**************************************
    //* RowVersion methods
    //**************************************
    /// <summary>Gets the RowVersion <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn RowVersionColumn {
      get { return TypedTable.RowVersionColumn; }
    }

    /// <summary>Gets or sets the RowVersion.</summary>
    [DBDataType(typeof(System.Int32))]
    public virtual System.Int32 RowVersion {
      get { 
        System.Int32 result_;
        if (GetInterceptor<System.Int32>("RowVersion", GetRowVersionImpl, out result_)) return result_;
        return GetRowVersionImpl();
      }
      set { 
          if (!SetInterceptor<System.Int32>("RowVersion", value, SetRowVersionImpl)) {
            SetRowVersionImpl(value);
          }  
      }    
    }
    private System.Int32 GetRowVersionImpl() {
      return (System.Int32) GetColumnValue(RowVersionColumn, typeof(System.Int32), false); 
    }
    private void SetRowVersionImpl(System.Int32 value) {
      SetColumnValue(RowVersionColumn, value);
    }
    
    //**************************************
    //* ModifiedBy methods
    //**************************************
    /// <summary>Gets the ModifiedBy <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn ModifiedByColumn {
      get { return TypedTable.ModifiedByColumn; }
    }

    /// <summary>Gets or sets the ModifiedBy.</summary>
    [MaxTextLength(100)]
    [DBDataType(typeof(System.String))]
    public virtual System.String ModifiedBy {
      get { 
        System.String result_;
        if (GetInterceptor<System.String>("ModifiedBy", GetModifiedByImpl, out result_)) return result_;
        return GetModifiedByImpl();
      }
      set { 
          if (!SetInterceptor<System.String>("ModifiedBy", value, SetModifiedByImpl)) {
            SetModifiedByImpl(value);
          }  
      }    
    }
    private System.String GetModifiedByImpl() {
      return (System.String) GetColumnValue(ModifiedByColumn, typeof(System.String), true); 
    }
    private void SetModifiedByImpl(System.String value) {
      SetColumnValue(ModifiedByColumn, value);
    }
    
    //**************************************
    //* ModifiedDate methods
    //**************************************
    /// <summary>Gets the ModifiedDate <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn ModifiedDateColumn {
      get { return TypedTable.ModifiedDateColumn; }
    }

    /// <summary>Gets or sets the ModifiedDate.</summary>
    [DBDataType(typeof(System.DateTime))]
    public virtual Nullable<System.DateTime> ModifiedDate {
      get { 
        Nullable<System.DateTime> result_;
        if (GetInterceptor<Nullable<System.DateTime>>("ModifiedDate", GetModifiedDateImpl, out result_)) return result_;
        return GetModifiedDateImpl();
      }
      set { 
          if (!SetInterceptor<Nullable<System.DateTime>>("ModifiedDate", value, SetModifiedDateImpl)) {
            SetModifiedDateImpl(value);
          }
      }    
    }
    private Nullable<System.DateTime> GetModifiedDateImpl() {
      return (Nullable<System.DateTime>) GetColumnValue(ModifiedDateColumn, typeof(System.DateTime), true); 
    }
    private void SetModifiedDateImpl(Nullable<System.DateTime> value) {
      SetColumnValue(ModifiedDateColumn, value);
    }
    
    #endregion

  }
  #endregion

  #region EntityPropertyDescriptors.ZIPCodeAuditPropertyDescriptor
  public static partial class EntityPropertyDescriptors {

    /// <summary>
    /// Gets the ZIPCodeAuditPropertyDescriptor for <see cref="ZIPCodeAudit"/>.
    /// </summary>
    public static ZIPCodeAuditPropertyDescriptor ZIPCodeAudit {
      get { return ZIPCodeAuditPropertyDescriptor.Instance; }
    }
    
    /// <summary>
    /// The generated class containing <see cref="ZIPCodeAudit"/> PropertyDescriptors.
    /// </summary>
    public partial class ZIPCodeAuditPropertyDescriptor : AdaptedPropertyDescriptor {
	
			/// <summary>
      /// Initializes a new instance of the ZIPCodeAuditPropertyDescriptor class. 
      /// </summary>
      /// <param name="pParentDescriptor">The parent <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/></param>
      /// <param name="pPropertyPath">The type of the property value</param>
			public ZIPCodeAuditPropertyDescriptor(AdaptedPropertyDescriptor pParentDescriptor, String pPropertyPath)
        : base(pParentDescriptor, pPropertyPath) {}
		
      /// <summary>
      /// Initializes a new instance of the ZIPCodeAuditPropertyDescriptor class. 
      /// </summary>
      /// <param name="pComponentType">The type of object to which the property is bound</param>
      protected ZIPCodeAuditPropertyDescriptor(Type pComponentType)
        : base(pComponentType) {}

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for ZIPCodeHistoryId.</summary>
      public AdaptedPropertyDescriptor ZIPCodeHistoryId {
        get { return Get("ZIPCodeHistoryId"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for ZIPCode.</summary>
      public AdaptedPropertyDescriptor ZIPCode {
        get { return Get("ZIPCode"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for City.</summary>
      public AdaptedPropertyDescriptor City {
        get { return Get("City"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for State.</summary>
      public AdaptedPropertyDescriptor State {
        get { return Get("State"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for County.</summary>
      public AdaptedPropertyDescriptor County {
        get { return Get("County"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Country.</summary>
      public AdaptedPropertyDescriptor Country {
        get { return Get("Country"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for RowVersion.</summary>
      public AdaptedPropertyDescriptor RowVersion {
        get { return Get("RowVersion"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for ModifiedBy.</summary>
      public AdaptedPropertyDescriptor ModifiedBy {
        get { return Get("ModifiedBy"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for ModifiedDate.</summary>
      public AdaptedPropertyDescriptor ModifiedDate {
        get { return Get("ModifiedDate"); }
      }

     internal static ZIPCodeAuditPropertyDescriptor Instance = new ZIPCodeAuditPropertyDescriptor(typeof(ZIPCodeAudit));
    }
  }
  #endregion
  
}
