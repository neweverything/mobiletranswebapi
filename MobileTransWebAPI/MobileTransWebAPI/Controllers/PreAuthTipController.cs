﻿using CabRideEngine;
using CabRideEngineDapper.Utils;
using IdeaBlade.Persistence;
using MobileTransWebAPI.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using TaxiPassCommon;

namespace MobileTransWebAPI.Controllers {

	public class PreAuthTipController : ApiController {

		private int mExpiryYear = 0;
		private int mExpiryMonth = 0;
		private string mCardNo = "";


		public ChargeResult Get() {
			string temp = "AE233211B7143CB9C99FCD4EAA33E413C8E87B4E4F82F0968FD880776739490A31181E0EC701535EA77FA2843B1362AAC85BD6192CADA937629A8BBAA4C5DF3AEF58204D284E28E8C674403D8678F7E841EA431BA38020E926E6DF7A7FA7692C2D2BA24269C1CAF76105965BBE945E60B1093DE2312C3B61352392AFDFEC99D4A941982EC301BDC8CD12D65D44CD314868AE66A47A4A620BA78D2713CEC274477F708CECD2FBC956DFFAADE02D7C6D6671F0C056BC34068E9534837D591D322C9CC4CE8053B7A3A63FBDD1A98719840ACE195413BBECFEEEFCFF1E405FEED7914807ABF1DA24CF9430F78003AFC0B2C564ABF3156BB2A16E5244AF996E1EE139E55E46BF705171D70B45C309A252FF45368E72467543FB3F445F45C1DCE66F021101041A7F6DDAB644BF445A0CDEC7324DB5A09904CA39FEFA9649441D96300D334829818B97FF5BD3E47B00C24BE120AB9ED37D6E44D727B3BD8009F8A9663A91283A031B6678589B835C659FDD2948BD7C4FE652B8BD185B5CDCF3886AE495FCF094A3EEF51A3B27A8D698BB0AED9469211B9D3D2133A3B8BB42863B62E6C26E5229D91A6DFD664E317D44C16C0ACE5698068F4C4A4092CC2CECA66CDF05D7883B4337109785BF67ABA0013B3174661EBADD773AB2D6D892380E6B99E983EC64C36DBF0A4A473C10F49D7355DC8E3F22EC01D550802F941B8C4950EA5AD6E6BBE0630B959C0AF59E6A1B8385BC33504AB7113A1525857947333468F42113AB83A0B84A59B0E7B8827ABF47AD93C403753B93622A0558D4BB371205A540B5B8C3D4E5FE9FB17F6F81917F6C33D8F271AE7B8D2DC2F7AEDFBA1682E434BDD38FCAA8A82CDD682B03BECC0568024BDEB5513B25797808A9A01247C0727A70837769211B9D3D2133A39F3A5A7DAE03B3439D0BEC6DFBD6452347C045F10C571641F4FDB0E329E440A3205A201DE659BD5EEC83DBC783BCC9CFD5E8F7F94775B65037F6BBD74704C3CD6E9327B3CA451778A55429DCCCBFE9AEF2D4F5B839E314EC6A0B839EDDBF445AE9A92B9E1016A4BE2C28FE65383C04ADF2B60867461D9E4CDD83982CF7E6EF60FBC4F2D5837C1C4EDFFAADE02D7C6D6660DA9503638154C9A3A3245C3A78C2CC2803CD71F29A0E0B2978CC9B43654DEDBCBA166AD7ACEC5229711D9DF29F3EAD4F65E788FA7682F6A8DC70E7A76918C7FBBC1042E67393419EB76698A44D82F55038439366B9A368085EA216AB29DD54039A5F91336F4CD94F61CC9BCD2E82135D6F1B94644EFD392992C86B1595C4C8495108B876475ACBDFD119F0B07022EE144CCF62375FEDFC403EAC54ACC7397439EDB8556D84E3E8E671D619FCF03EF6727CD688187913B06F4924E6B9454FA67D6F64B3B2D255505E7D176D2E10753E67F9F02174383FCFBF94EB5CCD2A42370CF7F387C1FEABDC2585356A93359B8660E8B4ED9F78D5230DF619A698449CB0EEE82C8455BDD91A56C03FC82D792017913982E1".DecryptIceKey();

			ChargeInfo info = JsonConvert.DeserializeObject<ChargeInfo>(temp);
			return PreAuthCard(info);
		}

		public ChargeResult PreAuthCard(ChargeInfo pChargeInfo) {
			ChargeResult oChargeResult = new ChargeResult();

			PersistenceManager oPM = WebApiApplication.UserPM; //.GetPersistenceManager(Application);
			if (oPM == null) {
				oChargeResult.ErrorMsg = "Could not load persistence manager";
				return oChargeResult;
			}

			WebApiApplication.LogIt("PreAuthTip", JsonConvert.SerializeObject(pChargeInfo).EncryptIceKey());
			SystemDefaults defaults = SystemDefaults.GetDefaults(oPM);

			if (pChargeInfo.Platform != Platforms.EZRide.ToString()) {
				DateTime requestDate = Convert.ToDateTime(pChargeInfo.RequestTime);
				if (WebApiApplication.GetUTCServerTime().Subtract(requestDate).TotalMinutes > 1) {
					oChargeResult.ErrorMsg = "Invalid Access";
					return oChargeResult;
				}
			}

			if (pChargeInfo.CardNo.IsNullOrEmpty()) {
				if (!pChargeInfo.CardToken.IsNullOrEmpty()) {
					using (var conn = SqlHelper.OpenSqlConnection()) {
						var cardInfo = CabRideEngineDapper.CustomerCards.GetCardByToken(conn, pChargeInfo.CardToken, -1);
						if (!cardInfo.IsNullEntity) {
							pChargeInfo.CardNo = cardInfo.DecryptCard();
							pChargeInfo.CardNumberDisplay = cardInfo.CardNumberDisplay;
							pChargeInfo.CardHolder = cardInfo.NameDecrypted;
							//pChargeInfo.CardType = cardInfo.CardType;
							pChargeInfo.CVV = cardInfo.Verification.DecryptIceKey();
							pChargeInfo.ExpMonth = cardInfo.Expiration.Value.Month;
							pChargeInfo.ExpYear = cardInfo.Expiration.Value.Year;
						}
					}
				}
			}
			List<string> testCards = defaults.TreatAsTestCards.DecryptIceKey().Split('|').ToList();

			if (!pChargeInfo.SwipeData.IsNullOrEmpty()) {
				string swipeData = pChargeInfo.SwipeData;
				if (pChargeInfo.homeATMEncrypted) {
					swipeData = pChargeInfo.DecryptHomeATMSwipe();
				}
				if (swipeData.IsNullOrEmpty()) {
					pChargeInfo.ErrorMsg = "Could not decode HomeATM swipe data";

				} else {

					if (testCards.FirstOrDefault(p => swipeData.Contains(p)) != null) {
						swipeData = string.Format(";4111111111111111={0}121010000047520001?", (DateTime.Today.Year + 2).ToString().Right(2));
					}
					if (swipeData.StartsWith("%B") && swipeData.Contains("?")) {
						swipeData = swipeData.Left(swipeData.IndexOf("?") + 1);
					}
					if (setSwipedCard(swipeData)) {
						pChargeInfo.CardNo = mCardNo; // swipeData.Split('=')[0].Replace(";", "");
						pChargeInfo.ExpMonth = mExpiryMonth;
						pChargeInfo.ExpYear = mExpiryYear;
					}
					pChargeInfo.SwipeData = swipeData;
				}


				if (pChargeInfo.SwipeData.StartsWith(";%")) {
					pChargeInfo.SwipeData = pChargeInfo.SwipeData.Substring(1);
				}
			}

			// Driver/Device lookup
			Drivers oDriver;
			if (pChargeInfo.DriverID > 0) {
				oDriver = Drivers.GetDriver(oPM, pChargeInfo.DriverID);
			} else if (pChargeInfo.DeviceID.IsNullOrEmpty()) {
				Affiliate oAff = Affiliate.GetAffiliate(oPM, pChargeInfo.AffiliateID);
				oDriver = Drivers.GetChargeCard(oAff);
			} else {
				oDriver = Drivers.GetDriverPhoneSubNoOrSerial(oPM, pChargeInfo.AffiliateID, pChargeInfo.DeviceID);
			}
			if (oDriver.IsNullEntity) {
				oChargeResult.ErrorMsg = "Unknown Device ID '" + pChargeInfo.DeviceID + "'";
				return oChargeResult;
			}


			AffiliateDrivers actualDriver = oPM.GetNullEntity<AffiliateDrivers>();
			if (pChargeInfo.AffDriverID > 0) {
				actualDriver = AffiliateDrivers.GetDriver(oPM, pChargeInfo.AffDriverID);
				if (actualDriver.IsNullEntity) {
					oChargeResult.ErrorMsg = "Driver does not exist";
					return oChargeResult;
				} else {
					if (actualDriver.AffiliateID != oDriver.AffiliateID) {
						oChargeResult.ErrorMsg = "Error Driver and Device are not configured correctly";
						return oChargeResult;
					}
				}
			}


			decimal tipAmt = 0;
			if (oDriver.MyAffiliate.AffiliateDriverDefaults.AutoTipPercent > 0) {
				tipAmt = Math.Round(pChargeInfo.Fare * (oDriver.MyAffiliate.AffiliateDriverDefaults.AutoTipPercent / 100), 2);
			} else {
				tipAmt = Math.Round(pChargeInfo.Fare * 0.35M, 2);
			}


			// Create the DriverPayment record
			DriverPayments oPayment = DriverPayments.Create(oDriver);
			oPayment.AuthAmount = pChargeInfo.Fare + tipAmt;
			oPayment.Fare = pChargeInfo.Fare;


			oPayment.CardNumber = pChargeInfo.CardNo;
			oPayment.Expiration = pChargeInfo.CardExpires;
			oPayment.ReferenceNo = pChargeInfo.ReferenceNo;

			if (!pChargeInfo.CVV.IsNullOrEmpty()) {
				oPayment.CVV = pChargeInfo.CVV;
			}
			if (!pChargeInfo.BillingZipCode.IsNullOrEmpty()) {
				oPayment.CardZIPCode = pChargeInfo.BillingZipCode;
			} else if (!pChargeInfo.ZipCode.IsNullOrEmpty()) {
				oPayment.CardZIPCode = pChargeInfo.ZipCode;
			}

			oPayment.AffiliateID = oDriver.AffiliateID;


			if (!actualDriver.IsNullEntity) {
				oPayment.AffiliateDriverID = actualDriver.AffiliateDriverID;
			}
			if (!pChargeInfo.VehicleNo.IsNullOrEmpty()) {
				Vehicles oVehicle = Vehicles.GetVehicle(oPM, long.Parse(pChargeInfo.VehicleNo));
				if (!oVehicle.IsNullEntity) {
					oPayment.VehicleID = oVehicle.VehicleID;
				}
			}

			if (oPayment.Affiliate.AffiliateDriverDefaults.TPFeeOnFareOnly) {
				oPayment.TaxiPassFee = DriverPayments.CalcFee(oPayment.Affiliate, pChargeInfo.Fare);
			}

			oPayment.Platform = pChargeInfo.Platform;
			if (oPayment.Platform.IsNullOrEmpty()) {
				oPayment.Platform = oPayment.Affiliate.DefaultPlatform;
			}
			oPayment.Save();

			oPayment.CardSwiped = false;
			if (!pChargeInfo.SwipeData.IsNullOrEmpty()) {
				oPayment.CardSwiped = (pChargeInfo.SwipeData.Trim().Length > 0);
			}
			DriverPaymentResults oResult = null;

			// Pre-auth the credit card
			try {
				//lock (thisLock) {
				if (oPayment.CardSwiped) {
					oResult = oPayment.PreAuthCard(pChargeInfo.SwipeData);
				} else {
					oResult = oPayment.PreAuthCard();
				}
				//}
				if (oResult.Result != "0") {
					oChargeResult.ErrorMsg = "Auth Failed: " + oResult.Message;
					return oChargeResult;
				}
				oChargeResult.Fare = oPayment.Fare;
				oChargeResult.AuthCode = oResult.AuthCode;
				oChargeResult.VoucherNo = oResult.DriverPayments.TransNo;
				oChargeResult.AuthDate = oResult.DriverPayments.MyAuthDateTime.Value.ToString("MM/dd/yyyy HH:mm");
				oChargeResult.IsTestCard = oResult.DriverPayments.Test;

				oChargeResult.AVSResultCode = oResult.AVSResultCode;
				oChargeResult.CVVResultCode = oResult.CVV2Match;
				oChargeResult.CardNo = oPayment.CardNumberDisplay;
				oChargeResult.TransType = oResult.TransType;
				//if (oResult.DriverPayments.Test) {
				//	oChargeResult.ErrorMsg = "TEST TRANSACTION";
				//}

			} catch {
				if (pChargeInfo.Platform.Equals(Platforms.EZRide.ToString())) {
					oChargeResult.ErrorMsg = oPayment.DriverPaymentNotes.Reason;
				} else {
					oChargeResult.ErrorMsg = SystemDefaults.GetDefaults(oPM).DriverFraudMessage;
				}
				return oChargeResult;
			}


			return oChargeResult;
		}


		private bool setSwipedCard(string pValue) {
			try {
				pValue = pValue.Trim();


				if (pValue.Equals("exit", StringComparison.CurrentCultureIgnoreCase)) {
					return true;
				}

				//fix swipe data 
				if ((pValue.StartsWith(";") || pValue.StartsWith("%B")) && pValue.Contains("?")) {
					pValue = pValue.Left(pValue.IndexOf("?") + 1);
				}

				if ((pValue.StartsWith(";%") || pValue.StartsWith("%B")) && pValue.EndsWith("?") && pValue.IndexOf("=") > 0) {
					String workData = pValue;
					int startPos = pValue.IndexOf("?") + 1;
					pValue = pValue.Substring(startPos);
					int endPos = pValue.IndexOf("=");
					mCardNo = pValue.Left(endPos);
					if (mCardNo.StartsWith(";")) {
						mCardNo = mCardNo.Substring(1);
					}
					String dateData = pValue.Substring(endPos + 1);
					setExpiryYear(Int32.Parse(dateData.Substring(0, 2)));
					setExpiryMonth(Int32.Parse(dateData.Substring(2, 2)));

					pValue = workData;
					startPos = pValue.IndexOf("^") + 1;
					if (startPos >= 0) {
						workData = pValue.Substring(startPos);
						endPos = workData.IndexOf("^");
						if (endPos > 0) {
							//CardName = workData.Substring(0, endPos).Trim();
						}
					}

					//setTrack1(value.Substring(0, value.IndexOf(";")));

				} else if ((pValue.StartsWith(";%") || pValue.StartsWith("%B")) && pValue.EndsWith("?") && pValue.IndexOf("^") > 0 && pValue.IndexOf('=') < 0) {
					string[] workData = pValue.Split('^');

					//int startPos = value.IndexOf("?") + 1;
					//int endPos = value.IndexOf("=");
					if (workData.Length != 3) {
						try {

						} catch (Exception) {
						}
					} else {
						mCardNo = workData[0].Replace("%B", "").Replace(";%", "").Replace(" ", "");

						//CardName = workData[1].Trim();

						String dateData = workData[2];
						setExpiryYear(Int32.Parse(dateData.Substring(0, 2)));
						setExpiryMonth(Int32.Parse(dateData.Substring(2, 2)));

						//setTrack1(value);
					}
				} else if ((pValue.StartsWith(";") || pValue.StartsWith("';")) && pValue.EndsWith("?") && pValue.IndexOf("=") > 0) {
					//if (value.IndexOf(Globals.TEST_CARD) > 0) {
					//    value = Globals.USE_TEST_CARD;
					//}
					//mSwipeInfo = value;
					String sText = pValue.Substring(1);
					sText = sText.Substring(0, sText.Length - 1);
					mCardNo = sText.Split('=')[0];
					if (mCardNo.Length < 14 || !mCardNo.IsNumeric()) {
						//mErrorMsg = "Bad Swipe";
						return false;
					}
					sText = sText.Split('=')[1];
					mExpiryYear = Int32.Parse(sText.Substring(0, 2));
					sText = sText.Substring(2);
					mExpiryMonth = Int32.Parse(sText.Substring(0, 2));
					sText = sText.Substring(2);
					//ServiceCode = sText.Substring(0, 3);
					sText = sText.Substring(3);

				} else if (pValue.StartsWith(";0") && pValue.EndsWith("?")) {   // TaxiPass Cards
																				//if (value.IndexOf(Globals.TEST_CARD) > 0) {
																				//    value = Globals.USE_TEST_CARD;
																				//}
																				//mSwipeInfo = value;
																				//sText = sText.Substring(1, sText.Length - 1);
					mCardNo = pValue.Substring(1, pValue.Length - 2);
					mExpiryYear = 9999;
					mExpiryMonth = 12;
					//ServiceCode = "101";

				} else {
					//mErrorMsg = "Card Swipe Read Error";
					return false;
				}
				if (mCardNo.Length < 14 || !mCardNo.IsNumeric()) {
					//mErrorMsg = "Bad Swipe";
					return false;
				}
			} catch (Exception) {
				return false;
				//throw new Exception("Error Validating card, Please try again " + ex.Message);
			}
			return true;
		}

		private String getExpiration() {
			String month = mExpiryMonth.ToString();
			String year = mExpiryYear.ToString();
			return StringUtils.PadLeft(month, 2, "0") + StringUtils.PadLeft(year, 2, "0");
		}

		private int getExpiryMonth() {
			return mExpiryMonth;
		}

		private void setExpiryMonth(int value) {
			mExpiryMonth = value;
		}

		private int getExpiryYear() {

			return mExpiryYear;
		}

		private void setExpiryYear(int value) {
			if (value < 1000) {
				value += 2000;
			}
			mExpiryYear = value;
		}
	}
}
