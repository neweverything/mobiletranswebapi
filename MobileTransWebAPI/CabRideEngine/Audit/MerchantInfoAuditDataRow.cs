using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Runtime.Serialization;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

namespace CabRideEngine {
 
  #region MerchantInfoAuditDataTable
  /// <summary>
  /// The generated <see cref="T:IdeaBlade.Persistence.EntityTable"/> class  
  /// containing all <see cref="MerchantInfoAudit"/> objects.
  /// </summary>
  /// <remarks>
  /// ****************************************************
  /// <para>** DO NOT MODIFY THIS CLASS - AutoGenerated Code</para>
  /// <para>** Any changes made to this code will be lost</para>
  /// <para>** the next time this code is regenerated.</para>
  /// <para>**</para>
  /// <para>** Generated from:   EntityBaseclass.template</para>
  /// <para>** Last modified:    2012-03-15T13:24:35.2068032-07:00</para>
  /// <para>** Template version: 3.110127</para>
  /// ****************************************************
  /// </remarks>
  [BindingBrowsable(false)]
  [Serializable]
  public partial class MerchantInfoAuditDataTable : IdeaBlade.Persistence.EntityTable, 
     IdeaBlade.Persistence.Rdb.IRdbTable {
  
    /// <summary>
    /// Initializes a new instance of the MerchantInfoAuditDataTable class with no arguments.
    /// </summary>
    public MerchantInfoAuditDataTable() {}

    /// <summary>
    /// Initializes a new instance of the MerchantInfoAuditDataTable class 
    /// with the <see cref="System.Runtime.Serialization.SerializationInfo"/> 
    /// and the <see cref="System.Runtime.Serialization.StreamingContext"/>.
    /// </summary>
    /// <param name="pInfo">The data needed to serialize or deserialize an object.</param>
    /// <param name="pContext">The source and destination of a given serialized stream.</param>
    protected MerchantInfoAuditDataTable(SerializationInfo pInfo, StreamingContext pContext) :
      base(pInfo, pContext) {}

    /// <summary>
    /// Creates a new instance of the <see cref="MerchantInfoAuditDataRow"/>.
    /// Constructs a row from the builder.
    /// <para>** For internal use only. **</para>
    /// </summary>
    /// <param name="pRowBuilder"></param>
    /// <returns>The <see cref="MerchantInfoAuditDataRow"/>.</returns>
    protected override DataRow NewRowFromBuilder(DataRowBuilder pRowBuilder) {
      return new MerchantInfoAudit(pRowBuilder);
    }

    /// <summary>Returns the <see cref="System.Type"/> of <see cref="MerchantInfoAudit"/>.</summary>
    protected override Type GetRowType() {
      return typeof(MerchantInfoAudit);
    }
    
    /// <summary>
    /// Gets the name of the <see cref="T:IdeaBlade.Rdb.RdbKey"/> or 
    /// <see cref="T:IdeaBlade.Persistence.WS.WsKey"/> defined in <b>IdeaBlade.ibconfig</b>. 
    /// </summary>
    /// <remarks>For this <see cref="MerchantInfoAudit"/>, the DataSourceKeyName="Audit".</remarks>
    public override String DataSourceKeyName {
      get { return @"Audit"; }
    }

    #region Column properties
    //**************************************
    //* Column properties
    //**************************************
    /// <summary>Gets the MerchantHistoryID <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn MerchantHistoryIDColumn {
      get {
        if (mMerchantHistoryIDColumn!=null) return mMerchantHistoryIDColumn;
        mMerchantHistoryIDColumn = GetColumn("MerchantHistoryID", true);
        return mMerchantHistoryIDColumn;
      }
    }
    private DataColumn mMerchantHistoryIDColumn;
    
    /// <summary>Gets the MerchantID <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn MerchantIDColumn {
      get {
        if (mMerchantIDColumn!=null) return mMerchantIDColumn;
        mMerchantIDColumn = GetColumn("MerchantID", true);
        return mMerchantIDColumn;
      }
    }
    private DataColumn mMerchantIDColumn;
    
    /// <summary>Gets the VerisignAccountID <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn VerisignAccountIDColumn {
      get {
        if (mVerisignAccountIDColumn!=null) return mVerisignAccountIDColumn;
        mVerisignAccountIDColumn = GetColumn("VerisignAccountID", true);
        return mVerisignAccountIDColumn;
      }
    }
    private DataColumn mVerisignAccountIDColumn;
    
    /// <summary>Gets the MerchantNo <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn MerchantNoColumn {
      get {
        if (mMerchantNoColumn!=null) return mMerchantNoColumn;
        mMerchantNoColumn = GetColumn("MerchantNo", true);
        return mMerchantNoColumn;
      }
    }
    private DataColumn mMerchantNoColumn;
    
    /// <summary>Gets the Description <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn DescriptionColumn {
      get {
        if (mDescriptionColumn!=null) return mDescriptionColumn;
        mDescriptionColumn = GetColumn("Description", true);
        return mDescriptionColumn;
      }
    }
    private DataColumn mDescriptionColumn;
    
    /// <summary>Gets the BankName <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn BankNameColumn {
      get {
        if (mBankNameColumn!=null) return mBankNameColumn;
        mBankNameColumn = GetColumn("BankName", true);
        return mBankNameColumn;
      }
    }
    private DataColumn mBankNameColumn;
    
    /// <summary>Gets the BankAccountNo <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn BankAccountNoColumn {
      get {
        if (mBankAccountNoColumn!=null) return mBankAccountNoColumn;
        mBankAccountNoColumn = GetColumn("BankAccountNo", true);
        return mBankAccountNoColumn;
      }
    }
    private DataColumn mBankAccountNoColumn;
    
    /// <summary>Gets the CardType <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn CardTypeColumn {
      get {
        if (mCardTypeColumn!=null) return mCardTypeColumn;
        mCardTypeColumn = GetColumn("CardType", true);
        return mCardTypeColumn;
      }
    }
    private DataColumn mCardTypeColumn;
    
    /// <summary>Gets the RowVersion <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn RowVersionColumn {
      get {
        if (mRowVersionColumn!=null) return mRowVersionColumn;
        mRowVersionColumn = GetColumn("RowVersion", true);
        return mRowVersionColumn;
      }
    }
    private DataColumn mRowVersionColumn;
    
    /// <summary>Gets the ModifiedBy <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn ModifiedByColumn {
      get {
        if (mModifiedByColumn!=null) return mModifiedByColumn;
        mModifiedByColumn = GetColumn("ModifiedBy", true);
        return mModifiedByColumn;
      }
    }
    private DataColumn mModifiedByColumn;
    
    /// <summary>Gets the ModifiedDate <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn ModifiedDateColumn {
      get {
        if (mModifiedDateColumn!=null) return mModifiedDateColumn;
        mModifiedDateColumn = GetColumn("ModifiedDate", true);
        return mModifiedDateColumn;
      }
    }
    private DataColumn mModifiedDateColumn;
    
    #endregion    

    /// <summary>Creates the schema mapping information for this MerchantInfoAuditDataTable.</summary>
    /// <returns>
    /// The <see cref="T:IdeaBlade.Persistence.TableMappingInfo"/> for this MerchantInfoAuditDataTable.
    /// </returns>
    protected override TableMappingInfo CreateTableMappingInfo() {
      RdbTableMappingInfo mappingInfo = new RdbTableMappingInfo(this);
      mappingInfo.SourceOwnerName = "dbo"; 
      mappingInfo.SourceTableName = "MerchantInfoAudit"; 
      mappingInfo.ConcurrencyColumnName = ""; 
      // ColumnSelectionOption: selectAllColumns
      mappingInfo.SourceColumnNames = "*"; 
   
      DataColumnMappingCollection columnMappings = mappingInfo.TableMapping.ColumnMappings;
      columnMappings.Clear();
      columnMappings.Add("MerchantHistoryID", "MerchantHistoryID");
      columnMappings.Add("MerchantID", "MerchantID");
      columnMappings.Add("VerisignAccountID", "VerisignAccountID");
      columnMappings.Add("MerchantNo", "MerchantNo");
      columnMappings.Add("Description", "Description");
      columnMappings.Add("BankName", "BankName");
      columnMappings.Add("BankAccountNo", "BankAccountNo");
      columnMappings.Add("CardType", "CardType");
      columnMappings.Add("RowVersion", "RowVersion");
      columnMappings.Add("ModifiedBy", "ModifiedBy");
      columnMappings.Add("ModifiedDate", "ModifiedDate");
      return mappingInfo;
    }
  
    /// <summary>** For internal use only. **</summary>
    protected override void UpdateTableMappingInfo() {
      base.UpdateTableMappingInfo();
    }

    /// <summary>
    /// Initializes MerchantInfoAudit <see cref="System.Data.DataColumn"/> properties.
    /// </summary>
    public override void InitColumnProperties() {
      base.InitColumnProperties();

      MerchantHistoryIDColumn.Caption = "MerchantHistoryID";
      MerchantIDColumn.Caption = "MerchantID";
      VerisignAccountIDColumn.Caption = "VerisignAccountID";
      MerchantNoColumn.Caption = "MerchantNo";
      DescriptionColumn.Caption = "Description";
      BankNameColumn.Caption = "BankName";
      BankAccountNoColumn.Caption = "BankAccountNo";
      CardTypeColumn.Caption = "CardType";
      RowVersionColumn.Caption = "RowVersion";
      ModifiedByColumn.Caption = "ModifiedBy";
      ModifiedDateColumn.Caption = "ModifiedDate";
    }
  }
  #endregion
  
  #region MerchantInfoAuditDataRow
  /// <summary>
  /// The generated base class for <see cref="MerchantInfoAudit"/>.
  /// </summary>
  /// <remarks>
  /// <para>
  /// It represents a row of persistable data in the <see cref="MerchantInfoAuditDataTable"/>
  /// and derives from <see cref="T:IdeaBlade.Persistence.Entity"/>.
	/// </para>
  /// ****************************************************
  /// <para>** DO NOT MODIFY THIS CLASS - AutoGenerated Code</para>
  /// <para>** Any changes made to this code will be lost</para>
  /// <para>** the next time this code is regenerated.</para>
  /// <para>**</para>
  /// <para>** Generated from:   EntityBaseclass.template</para>
  /// <para>** Last modified:    2012-03-15T13:24:35.2068032-07:00</para>
  /// <para>** Template version: 3.110127</para>
  /// ****************************************************
  /// </remarks>  
  [Serializable]
  public abstract partial class MerchantInfoAuditDataRow : IdeaBlade.Persistence.Entity, 
     IdeaBlade.Persistence.Rdb.IRdbEntity {
  
    /// <summary>
    /// Initializes a new instance of the MerchantInfoAuditDataRow.
    /// Constructs a row from the builder.
    /// <para>** For internal use only. **</para>
    /// </summary>
    /// <param name="pRowBuilder"></param>
    protected MerchantInfoAuditDataRow(DataRowBuilder pRowBuilder)
      : base(pRowBuilder) {
    }

    /// <summary>Gets the <see cref="MerchantInfoAuditDataTable"/>.</summary>
    [BindingBrowsable(false)]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public new MerchantInfoAuditDataTable TypedTable {
      get { return (MerchantInfoAuditDataTable) base.Table; }
    }

    /// <summary>Returns the <see cref="System.Type"/> of <see cref="MerchantInfoAuditDataTable"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public static Type GetTableType() {
      return typeof(MerchantInfoAuditDataTable);
    }

    /// <summary>
    /// Gets the SQL 'where' clause used to restrict which 
    /// database records are accessible via this entity. 
    /// </summary>
    /// <remarks>
    /// The default property value is defined in the Current Class 
    /// Properties tab in the <b>IdeaBlade DevForce Object Mapping Tool</b>.
    /// <para>
    /// The 'where' clause will be used, in addition to any other 
    /// query-specific criteria, in every retrieval of entities of this type.
    /// </para>
    /// </remarks>
    [BindingBrowsable(false)]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public virtual string SqlWhereClause {
      get { return ""; }
    }
    
    /// <summary>
    /// Determines whether to use the 'distinct' keyword when 
    /// generating the SQL select clause for objects of this type.
    /// </summary>
    /// <remarks>Defaults to false, but may be overridden in subclasses.</remarks>
    [BindingBrowsable(false)]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public virtual bool ForceSqlDistinct {
      get { return false; }
    }
    

    #region Enums
    //**************************************
    //* Enums
    //**************************************
    #endregion
	
    #region Relation properties
    //**************************************
    //* Relation properties
    //**************************************
    #endregion
    
    #region EntityColumn definitions
    //**************************************
    //* Static EntityColumn definitions
    //**************************************
    /// <summary>The MerchantHistoryID <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn MerchantHistoryIDEntityColumn =
      new EntityColumn(typeof(MerchantInfoAudit), "MerchantHistoryID", typeof(System.Int64), false, true, true, DataSourceAccessType.ReadWrite);      
    /// <summary>The MerchantID <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn MerchantIDEntityColumn =
      new EntityColumn(typeof(MerchantInfoAudit), "MerchantID", typeof(System.Int64), false, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The VerisignAccountID <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn VerisignAccountIDEntityColumn =
      new EntityColumn(typeof(MerchantInfoAudit), "VerisignAccountID", typeof(System.Int64), true, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The MerchantNo <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn MerchantNoEntityColumn =
      new EntityColumn(typeof(MerchantInfoAudit), "MerchantNo", typeof(System.String), false, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The Description <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn DescriptionEntityColumn =
      new EntityColumn(typeof(MerchantInfoAudit), "Description", typeof(System.String), false, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The BankName <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn BankNameEntityColumn =
      new EntityColumn(typeof(MerchantInfoAudit), "BankName", typeof(System.String), true, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The BankAccountNo <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn BankAccountNoEntityColumn =
      new EntityColumn(typeof(MerchantInfoAudit), "BankAccountNo", typeof(System.String), true, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The CardType <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn CardTypeEntityColumn =
      new EntityColumn(typeof(MerchantInfoAudit), "CardType", typeof(System.String), true, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The RowVersion <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn RowVersionEntityColumn =
      new EntityColumn(typeof(MerchantInfoAudit), "RowVersion", typeof(System.Int32), false, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The ModifiedBy <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn ModifiedByEntityColumn =
      new EntityColumn(typeof(MerchantInfoAudit), "ModifiedBy", typeof(System.String), true, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The ModifiedDate <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn ModifiedDateEntityColumn =
      new EntityColumn(typeof(MerchantInfoAudit), "ModifiedDate", typeof(System.DateTime), true, false, false, DataSourceAccessType.ReadWrite);      
    #endregion
    
    #region Properties
    //**************************************
    //* MerchantHistoryID methods
    //**************************************
    /// <summary>Gets the MerchantHistoryID <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn MerchantHistoryIDColumn {
      get { return TypedTable.MerchantHistoryIDColumn; }
    }

    /// <summary>Gets the MerchantHistoryID.</summary>
    [DBDataType(typeof(System.Int64))]
    public virtual System.Int64 MerchantHistoryID {
      get { 
        System.Int64 result_;
        if (GetInterceptor<System.Int64>("MerchantHistoryID", GetMerchantHistoryIDImpl, out result_)) return result_;
        return GetMerchantHistoryIDImpl();
      }
    }
    private System.Int64 GetMerchantHistoryIDImpl() {
      return (System.Int64) GetColumnValue(MerchantHistoryIDColumn, typeof(System.Int64), false); 
    }
    
    //**************************************
    //* MerchantID methods
    //**************************************
    /// <summary>Gets the MerchantID <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn MerchantIDColumn {
      get { return TypedTable.MerchantIDColumn; }
    }

    /// <summary>Gets or sets the MerchantID.</summary>
    [DBDataType(typeof(System.Int64))]
    public virtual System.Int64 MerchantID {
      get { 
        System.Int64 result_;
        if (GetInterceptor<System.Int64>("MerchantID", GetMerchantIDImpl, out result_)) return result_;
        return GetMerchantIDImpl();
      }
      set { 
          if (!SetInterceptor<System.Int64>("MerchantID", value, SetMerchantIDImpl)) {
            SetMerchantIDImpl(value);
          }  
      }    
    }
    private System.Int64 GetMerchantIDImpl() {
      return (System.Int64) GetColumnValue(MerchantIDColumn, typeof(System.Int64), false); 
    }
    private void SetMerchantIDImpl(System.Int64 value) {
      SetColumnValue(MerchantIDColumn, value);
    }
    
    //**************************************
    //* VerisignAccountID methods
    //**************************************
    /// <summary>Gets the VerisignAccountID <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn VerisignAccountIDColumn {
      get { return TypedTable.VerisignAccountIDColumn; }
    }

    /// <summary>Gets or sets the VerisignAccountID.</summary>
    [DBDataType(typeof(System.Int64))]
    public virtual Nullable<System.Int64> VerisignAccountID {
      get { 
        Nullable<System.Int64> result_;
        if (GetInterceptor<Nullable<System.Int64>>("VerisignAccountID", GetVerisignAccountIDImpl, out result_)) return result_;
        return GetVerisignAccountIDImpl();
      }
      set { 
          if (!SetInterceptor<Nullable<System.Int64>>("VerisignAccountID", value, SetVerisignAccountIDImpl)) {
            SetVerisignAccountIDImpl(value);
          }
      }    
    }
    private Nullable<System.Int64> GetVerisignAccountIDImpl() {
      return (Nullable<System.Int64>) GetColumnValue(VerisignAccountIDColumn, typeof(System.Int64), true); 
    }
    private void SetVerisignAccountIDImpl(Nullable<System.Int64> value) {
      SetColumnValue(VerisignAccountIDColumn, value);
    }
    
    //**************************************
    //* MerchantNo methods
    //**************************************
    /// <summary>Gets the MerchantNo <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn MerchantNoColumn {
      get { return TypedTable.MerchantNoColumn; }
    }

    /// <summary>Gets or sets the MerchantNo.</summary>
    [MaxTextLength(16)]
    [DBDataType(typeof(System.String))]
    public virtual System.String MerchantNo {
      get { 
        System.String result_;
        if (GetInterceptor<System.String>("MerchantNo", GetMerchantNoImpl, out result_)) return result_;
        return GetMerchantNoImpl();
      }
      set { 
          if (!SetInterceptor<System.String>("MerchantNo", value, SetMerchantNoImpl)) {
            SetMerchantNoImpl(value);
          }  
      }    
    }
    private System.String GetMerchantNoImpl() {
      return (System.String) GetColumnValue(MerchantNoColumn, typeof(System.String), false); 
    }
    private void SetMerchantNoImpl(System.String value) {
      SetColumnValue(MerchantNoColumn, value);
    }
    
    //**************************************
    //* Description methods
    //**************************************
    /// <summary>Gets the Description <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn DescriptionColumn {
      get { return TypedTable.DescriptionColumn; }
    }

    /// <summary>Gets or sets the Description.</summary>
    [MaxTextLength(50)]
    [DBDataType(typeof(System.String))]
    public virtual System.String Description {
      get { 
        System.String result_;
        if (GetInterceptor<System.String>("Description", GetDescriptionImpl, out result_)) return result_;
        return GetDescriptionImpl();
      }
      set { 
          if (!SetInterceptor<System.String>("Description", value, SetDescriptionImpl)) {
            SetDescriptionImpl(value);
          }  
      }    
    }
    private System.String GetDescriptionImpl() {
      return (System.String) GetColumnValue(DescriptionColumn, typeof(System.String), false); 
    }
    private void SetDescriptionImpl(System.String value) {
      SetColumnValue(DescriptionColumn, value);
    }
    
    //**************************************
    //* BankName methods
    //**************************************
    /// <summary>Gets the BankName <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn BankNameColumn {
      get { return TypedTable.BankNameColumn; }
    }

    /// <summary>Gets or sets the BankName.</summary>
    [MaxTextLength(50)]
    [DBDataType(typeof(System.String))]
    public virtual System.String BankName {
      get { 
        System.String result_;
        if (GetInterceptor<System.String>("BankName", GetBankNameImpl, out result_)) return result_;
        return GetBankNameImpl();
      }
      set { 
          if (!SetInterceptor<System.String>("BankName", value, SetBankNameImpl)) {
            SetBankNameImpl(value);
          }  
      }    
    }
    private System.String GetBankNameImpl() {
      return (System.String) GetColumnValue(BankNameColumn, typeof(System.String), true); 
    }
    private void SetBankNameImpl(System.String value) {
      SetColumnValue(BankNameColumn, value);
    }
    
    //**************************************
    //* BankAccountNo methods
    //**************************************
    /// <summary>Gets the BankAccountNo <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn BankAccountNoColumn {
      get { return TypedTable.BankAccountNoColumn; }
    }

    /// <summary>Gets or sets the BankAccountNo.</summary>
    [MaxTextLength(20)]
    [DBDataType(typeof(System.String))]
    public virtual System.String BankAccountNo {
      get { 
        System.String result_;
        if (GetInterceptor<System.String>("BankAccountNo", GetBankAccountNoImpl, out result_)) return result_;
        return GetBankAccountNoImpl();
      }
      set { 
          if (!SetInterceptor<System.String>("BankAccountNo", value, SetBankAccountNoImpl)) {
            SetBankAccountNoImpl(value);
          }  
      }    
    }
    private System.String GetBankAccountNoImpl() {
      return (System.String) GetColumnValue(BankAccountNoColumn, typeof(System.String), true); 
    }
    private void SetBankAccountNoImpl(System.String value) {
      SetColumnValue(BankAccountNoColumn, value);
    }
    
    //**************************************
    //* CardType methods
    //**************************************
    /// <summary>Gets the CardType <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn CardTypeColumn {
      get { return TypedTable.CardTypeColumn; }
    }

    /// <summary>Gets or sets the CardType.</summary>
    [MaxTextLength(20)]
    [DBDataType(typeof(System.String))]
    public virtual System.String CardType {
      get { 
        System.String result_;
        if (GetInterceptor<System.String>("CardType", GetCardTypeImpl, out result_)) return result_;
        return GetCardTypeImpl();
      }
      set { 
          if (!SetInterceptor<System.String>("CardType", value, SetCardTypeImpl)) {
            SetCardTypeImpl(value);
          }  
      }    
    }
    private System.String GetCardTypeImpl() {
      return (System.String) GetColumnValue(CardTypeColumn, typeof(System.String), true); 
    }
    private void SetCardTypeImpl(System.String value) {
      SetColumnValue(CardTypeColumn, value);
    }
    
    //**************************************
    //* RowVersion methods
    //**************************************
    /// <summary>Gets the RowVersion <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn RowVersionColumn {
      get { return TypedTable.RowVersionColumn; }
    }

    /// <summary>Gets or sets the RowVersion.</summary>
    [DBDataType(typeof(System.Int32))]
    public virtual System.Int32 RowVersion {
      get { 
        System.Int32 result_;
        if (GetInterceptor<System.Int32>("RowVersion", GetRowVersionImpl, out result_)) return result_;
        return GetRowVersionImpl();
      }
      set { 
          if (!SetInterceptor<System.Int32>("RowVersion", value, SetRowVersionImpl)) {
            SetRowVersionImpl(value);
          }  
      }    
    }
    private System.Int32 GetRowVersionImpl() {
      return (System.Int32) GetColumnValue(RowVersionColumn, typeof(System.Int32), false); 
    }
    private void SetRowVersionImpl(System.Int32 value) {
      SetColumnValue(RowVersionColumn, value);
    }
    
    //**************************************
    //* ModifiedBy methods
    //**************************************
    /// <summary>Gets the ModifiedBy <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn ModifiedByColumn {
      get { return TypedTable.ModifiedByColumn; }
    }

    /// <summary>Gets or sets the ModifiedBy.</summary>
    [MaxTextLength(100)]
    [DBDataType(typeof(System.String))]
    public virtual System.String ModifiedBy {
      get { 
        System.String result_;
        if (GetInterceptor<System.String>("ModifiedBy", GetModifiedByImpl, out result_)) return result_;
        return GetModifiedByImpl();
      }
      set { 
          if (!SetInterceptor<System.String>("ModifiedBy", value, SetModifiedByImpl)) {
            SetModifiedByImpl(value);
          }  
      }    
    }
    private System.String GetModifiedByImpl() {
      return (System.String) GetColumnValue(ModifiedByColumn, typeof(System.String), true); 
    }
    private void SetModifiedByImpl(System.String value) {
      SetColumnValue(ModifiedByColumn, value);
    }
    
    //**************************************
    //* ModifiedDate methods
    //**************************************
    /// <summary>Gets the ModifiedDate <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn ModifiedDateColumn {
      get { return TypedTable.ModifiedDateColumn; }
    }

    /// <summary>Gets or sets the ModifiedDate.</summary>
    [DBDataType(typeof(System.DateTime))]
    public virtual Nullable<System.DateTime> ModifiedDate {
      get { 
        Nullable<System.DateTime> result_;
        if (GetInterceptor<Nullable<System.DateTime>>("ModifiedDate", GetModifiedDateImpl, out result_)) return result_;
        return GetModifiedDateImpl();
      }
      set { 
          if (!SetInterceptor<Nullable<System.DateTime>>("ModifiedDate", value, SetModifiedDateImpl)) {
            SetModifiedDateImpl(value);
          }
      }    
    }
    private Nullable<System.DateTime> GetModifiedDateImpl() {
      return (Nullable<System.DateTime>) GetColumnValue(ModifiedDateColumn, typeof(System.DateTime), true); 
    }
    private void SetModifiedDateImpl(Nullable<System.DateTime> value) {
      SetColumnValue(ModifiedDateColumn, value);
    }
    
    #endregion

  }
  #endregion

  #region EntityPropertyDescriptors.MerchantInfoAuditPropertyDescriptor
  public static partial class EntityPropertyDescriptors {

    /// <summary>
    /// Gets the MerchantInfoAuditPropertyDescriptor for <see cref="MerchantInfoAudit"/>.
    /// </summary>
    public static MerchantInfoAuditPropertyDescriptor MerchantInfoAudit {
      get { return MerchantInfoAuditPropertyDescriptor.Instance; }
    }
    
    /// <summary>
    /// The generated class containing <see cref="MerchantInfoAudit"/> PropertyDescriptors.
    /// </summary>
    public partial class MerchantInfoAuditPropertyDescriptor : AdaptedPropertyDescriptor {
	
			/// <summary>
      /// Initializes a new instance of the MerchantInfoAuditPropertyDescriptor class. 
      /// </summary>
      /// <param name="pParentDescriptor">The parent <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/></param>
      /// <param name="pPropertyPath">The type of the property value</param>
			public MerchantInfoAuditPropertyDescriptor(AdaptedPropertyDescriptor pParentDescriptor, String pPropertyPath)
        : base(pParentDescriptor, pPropertyPath) {}
		
      /// <summary>
      /// Initializes a new instance of the MerchantInfoAuditPropertyDescriptor class. 
      /// </summary>
      /// <param name="pComponentType">The type of object to which the property is bound</param>
      protected MerchantInfoAuditPropertyDescriptor(Type pComponentType)
        : base(pComponentType) {}

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for MerchantHistoryID.</summary>
      public AdaptedPropertyDescriptor MerchantHistoryID {
        get { return Get("MerchantHistoryID"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for MerchantID.</summary>
      public AdaptedPropertyDescriptor MerchantID {
        get { return Get("MerchantID"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for VerisignAccountID.</summary>
      public AdaptedPropertyDescriptor VerisignAccountID {
        get { return Get("VerisignAccountID"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for MerchantNo.</summary>
      public AdaptedPropertyDescriptor MerchantNo {
        get { return Get("MerchantNo"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Description. Aliased with an '_' because Description is reserved.</summary>
      public AdaptedPropertyDescriptor Description_ {
        get { return Get("Description"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for BankName.</summary>
      public AdaptedPropertyDescriptor BankName {
        get { return Get("BankName"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for BankAccountNo.</summary>
      public AdaptedPropertyDescriptor BankAccountNo {
        get { return Get("BankAccountNo"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for CardType.</summary>
      public AdaptedPropertyDescriptor CardType {
        get { return Get("CardType"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for RowVersion.</summary>
      public AdaptedPropertyDescriptor RowVersion {
        get { return Get("RowVersion"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for ModifiedBy.</summary>
      public AdaptedPropertyDescriptor ModifiedBy {
        get { return Get("ModifiedBy"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for ModifiedDate.</summary>
      public AdaptedPropertyDescriptor ModifiedDate {
        get { return Get("ModifiedDate"); }
      }

     internal static MerchantInfoAuditPropertyDescriptor Instance = new MerchantInfoAuditPropertyDescriptor(typeof(MerchantInfoAudit));
    }
  }
  #endregion
  
}
