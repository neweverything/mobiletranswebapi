﻿using System.Web.Http;

namespace MobileTransWebAPI.Controllers {
    public class VersionController : ApiController {
        // GET: Version
        public string Get() {
            return "01/09/2018";
        }
    }
}