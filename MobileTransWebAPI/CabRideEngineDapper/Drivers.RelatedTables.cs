﻿using CabRideEngineDapper.Utils;
using System.Collections.Generic;

namespace CabRideEngineDapper {
    public partial class Drivers {


        public Affiliate MyAffiliate {
            get {
                return Affiliate;
            }
        }

        private Affiliate mAffiliate;
        public Affiliate Affiliate {
            get {
                if (mAffiliate == null || (mAffiliate.IsNullEntity && AffiliateID.HasValue)) {
                    if (AffiliateID.HasValue) {
                        mAffiliate = Affiliate.GetAffiliate(AffiliateID.Value);
                    } else {
                        mAffiliate = Affiliate.GetNullEntity();
                    }
                }
                return mAffiliate;
            }
        }

        private SalesReps mSalesRep;
        public SalesReps SalesReps {
            get {
                if (!this.SalesRepID.HasValue) {
                    return CabRideEngineDapper.SalesReps.GetNullEntity();
                }
                if (mSalesRep == null || mSalesRep.SalesRepID != this.SalesRepID) {
                    mSalesRep = SalesReps.GetSalesRep(this.SalesRepID.Value);
                }
                return mSalesRep;
            }
        }

        private List<DriverBankInfo> mDriverBankInfos;
        public List<DriverBankInfo> DriverBankInfos {
            get {
                if (mDriverBankInfos == null) {
                    mDriverBankInfos = DriverBankInfo.GetBankInfo(this.DriverID);
                }
                return mDriverBankInfos;
            }
        }

        private DriversGPSLocation mDriversGPSLocation;
        public DriversGPSLocation DriversGPSLocation {
            get {
                if (mDriversGPSLocation == null) {
                    using (var conn = SqlHelper.OpenSqlConnection()) {
                        mDriversGPSLocation = DriversGPSLocation.GetByDriverID(conn, this.DriverID);
                    }
                }
                return mDriversGPSLocation;
            }
        }

        private List<DriverRecurringFees> mDriverRecurringFeeses;
        public List<DriverRecurringFees> DriverRecurringFeeses {
            get {
                if (mDriverRecurringFeeses == null) {
                    mDriverRecurringFeeses = DriverRecurringFees.GetByDriverID(this.DriverID);
                }
                return mDriverRecurringFeeses;
            }
        }

        private Fleets mFleet;
        public Fleets Fleet {
            get {
                if (this.FleetID.HasValue) {
                    if (mFleet == null || mFleet.IsNullEntity) {
                        mFleet = Fleets.GetFleet(this.FleetID.GetValueOrDefault(0));
                    }
                } else {
                    mFleet = FleetsTable.GetNullEntity();
                }
                return mFleet;
            }
        }
    }
}
