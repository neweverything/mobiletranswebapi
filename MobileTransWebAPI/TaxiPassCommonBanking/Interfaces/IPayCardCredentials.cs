﻿using TaxiPassCommon.Banking.Enums;

namespace TaxiPassCommon.Banking.Interfaces {
    public interface IPayCardCredentials {
        PayCardTypes MyPayCardType();

        bool UseProductionAsBoolen { get; }
    }
}
