﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CabRideEngine.Enums {
	public enum CreditCardChargeType {
		Manual,
		Swipe,
		ECommerce,
		Any
	}
}
