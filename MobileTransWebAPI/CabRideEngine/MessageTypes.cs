using System;
using System.Collections.Generic;
using System.Text;

namespace CabRideEngine {

	public enum MessageTypes : int {
		AddConnectedEntity,
		CreationError,
		CurrentPasswordIncorrect,
		DeleteSelected,
		DifferentPassword,
		DoesNotExist,
		EmptyGridReport,
		EnterPassword,
		EnterUsername,
		IncorrectPassword,
		InvalidPassword,
		InvalidPasswordLength,
		InvalidUserName,
		Login,
		LoginCancelButton,
		LoginFailed,
		LoginOKButton,
		NewIncorrectPassword,
		NonUniqueProjectName,
		NonUniqueUserName,
		PasswordChanged,
		PasswordChangeFailed,
		PendingChanges,
		ReportingError,
		SaveFailed,
		SearchCriteriaNeeded,
		TaskNotDeletable,
		TasksNotDeletable,
		UnmatchedPasswords,
	}
}
