﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text;


using IdeaBlade.Persistence;
using IdeaBlade.Util;
using IdeaBlade.Persistence.Rdb;

using CabRideEngine.Utils;
using TaxiPassCommon;

namespace CabRideEngine {
	/// <summary>
	/// The developer class representing the DriversGPSHistory business object class.
	/// </summary>
	/// <remarks>
	/// <para>Place all application-specific business logic in this class.</para>
	/// <para>
	/// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
	/// and will not overwrite changes.
	/// </para>
	/// <para>
	/// This class inherits from the generated class, its companion in the pair of classes 
	/// defining a business object. Properties in the generated class may be overridden in 
	/// order to change the behavior or add business logic.
	/// </para>
	/// <para>
	/// This class is the final class in the inheritance chain for this business object. 
	/// It should not be inherited.
	/// </para>
	/// </remarks>
	[Serializable]
	public sealed partial class DriversGPSHistory : DriversGPSHistoryDataRow {

		#region Constructors -- DO NOT MODIFY

		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new';
		// it is also required if the class is visible from a web service.
		// DO NOT REMOVE **
		private DriversGPSHistory()
			: this(null) {
		}

		/// <summary>
		/// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
		/// </summary>
		/// <param name="pRowBuilder"></param>
		public DriversGPSHistory(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static DriversGPSHistory Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      DriversGPSHistory aDriversGPSHistory = pManager.CreateEntity<DriversGPSHistory>();
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aDriversGPSHistory, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aDriversGPSHistory.AddToManager();
		//      return aDriversGPSHistory;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		// Add additional logic to your business object here...
		public static DriversGPSHistory Create(Drivers pDriver, DateTime pGPSTime) {
			PersistenceManager oPM = pDriver.PersistenceManager;
			DriversGPSHistory oRec = null;
			try {
				oRec = GetRecordByDateTime(pDriver, pGPSTime);
				if (oRec.IsNullEntity) {
					oRec = oPM.CreateEntity<DriversGPSHistory>();
					oRec["DriverID"] = pDriver.DriverID;
					oRec["GPSTime"] = pGPSTime;
					oRec.AddToManager();
				}
			} catch (Exception ex) {
				throw ex;
			}
			return oRec;
		}

		// *******************************************************
		// Will return RideQ GMT entries or regular history recs
		// *******************************************************
		public static EntityList<DriversGPSHistory> GetRecords(PersistenceManager pPM, long pDriverID, DateTime pStart, DateTime pEnd, string pTimeZone) {
			TimeZoneConverter tzConvert = new TimeZoneConverter();
			DateTime dGMTStart = tzConvert.FromUniversalTime(pTimeZone, pStart);
			DateTime dGMTEnd = tzConvert.FromUniversalTime(pTimeZone, pEnd);

			StringBuilder sql = new StringBuilder();
			sql.AppendFormat("usp_DriversGPSHistory_GetDriver @DriverID = {0}", pDriverID);
			sql.AppendFormat(", @GPSStart = '{0}'", pStart.ToString());
			sql.AppendFormat(", @GPSEnd = '{0}'", pEnd.ToString());
			sql.AppendFormat(", @GPSGMTStart = '{0}'", dGMTStart.ToString());
			sql.AppendFormat(", @GPSGMTEnd = '{0}'", dGMTEnd.ToString());

			/*
			sql.Append("SELECT * FROM DriversGPSHistory with (nolock)");
			sql.AppendFormat("where DriverID = {0}", pDriverID);
			sql.Append(" and (((Message <> 'RideQ' or Message is null)");
				sql.AppendFormat(" and GPSTime >= '{0}'", pStart.ToString());
				sql.AppendFormat(" and GPSTime <= '{0}')", pEnd.ToString());
			sql.Append(" or (Message = 'RideQ' ");
				sql.Append(" and LatLonAccuracy <= 60 ");
				sql.AppendFormat(" and GPSTime >= '{0}'",dGMTStart.ToString());
				sql.AppendFormat(" and GPSTime <= '{0}'))", dGMTEnd.ToString());
			 */
			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(DriversGPSHistory), sql.ToString());
			return pPM.GetEntities<DriversGPSHistory>(qry);
		}
		
		
		public static EntityList<DriversGPSHistory> GetRecords(Drivers pDriver, DateTime pStart, DateTime pEnd) {
			RdbQuery qry = new RdbQuery(typeof(DriversGPSHistory), DriversGPSHistory.DriverIDEntityColumn, EntityQueryOp.EQ, pDriver.DriverID);
			qry.AddClause(DriversGPSHistory.GPSTimeEntityColumn, EntityQueryOp.GE, pStart);
			qry.AddClause(DriversGPSHistory.GPSTimeEntityColumn, EntityQueryOp.LE, pEnd);
			qry.AddOrderBy(DriversGPSHistory.GPSTimeEntityColumn);
			return pDriver.PersistenceManager.GetEntities<DriversGPSHistory>(qry, QueryStrategy.DataSourceThenCache);
		}

		public static DriversGPSHistory GetRecordByDateTime(Drivers pDriver, DateTime pDate) {
			RdbQuery qry = new RdbQuery(typeof(DriversGPSHistory), DriversGPSHistory.DriverIDEntityColumn, EntityQueryOp.EQ, pDriver.DriverID);
			qry.AddClause(DriversGPSHistory.GPSTimeEntityColumn, EntityQueryOp.EQ, pDate);
			return pDriver.PersistenceManager.GetEntity<DriversGPSHistory>(qry, QueryStrategy.DataSourceThenCache);
		}

		public static DriversGPSHistory GetLastSatReading(DriversGPSLocation pGPS) {
			RdbQuery qry = new RdbQuery(typeof(DriversGPSHistory), DriversGPSHistory.DriverIDEntityColumn, EntityQueryOp.EQ, pGPS.DriverID);
			qry.AddClause(DriversGPSHistory.GPSTimeEntityColumn, EntityQueryOp.GT, DateTime.Today.AddDays(-1));
			qry.AddClause(DriversGPSHistory.LocationMethodEntityColumn, EntityQueryOp.EQ, "327681");
			qry.AddClause(DriversGPSHistory.LocationMethodEntityColumn, EntityQueryOp.EQ, "Sat");
			qry.AddOperator(EntityBooleanOp.Or);
			qry.AddOrderBy(DriversGPSHistory.GPSTimeEntityColumn, System.ComponentModel.ListSortDirection.Descending);
			return pGPS.PersistenceManager.GetEntity<DriversGPSHistory>(qry);
		}

		public static DriversGPSHistory GetLastAddressRec(PersistenceManager pPM, double pLatitude, double pLongitude, QueryStrategy pQueryStrategy) {
			RdbQuery qry = new RdbQuery(typeof(DriversGPSHistory), DriversGPSHistory.LatitudeEntityColumn, EntityQueryOp.EQ, pLatitude);
			qry.AddClause(DriversGPSHistory.LongitudeEntityColumn, EntityQueryOp.EQ, pLongitude);
			qry.AddClause(DriversGPSHistory.AddressEntityColumn, EntityQueryOp.IsNotNull);
			qry.AddClause(DriversGPSHistory.AddressEntityColumn, EntityQueryOp.NE, "");
			qry.AddOrderBy(DriversGPSHistory.GPSTimeEntityColumn, System.ComponentModel.ListSortDirection.Descending);
			return pPM.GetEntity<DriversGPSHistory>(qry, pQueryStrategy);
		}

		public static EntityList<DriversGPSHistory> GetSpeed(Affiliate pAffiliate, DateTime pStart, DateTime pEnd, int pSpeed) {
			return GetSpeed(pAffiliate, pStart, pEnd, pSpeed, 0);
		}

		public static EntityList<DriversGPSHistory> GetSpeed(Affiliate pAffiliate, DateTime pStart, DateTime pEnd, int pSpeed, long pDriverID) {
			TaxiPassCommon.TimeZoneConverter oConverter = new TaxiPassCommon.TimeZoneConverter();
			//pStart = oConverter.oUniversalTime(pAffiliate.TimeZone, pStart);
			//pEnd = oConverter.oUniversalTime(pAffiliate.TimeZone, pEnd);
			RdbQuery qry = new RdbQuery(typeof(DriversGPSHistory), DriversGPSHistory.GPSTimeEntityColumn, EntityQueryOp.GE, pStart);
			qry.AddClause(DriversGPSHistory.GPSTimeEntityColumn, EntityQueryOp.LE, pEnd);
			qry.AddClause(DriversGPSHistory.SpeedEntityColumn, EntityQueryOp.GE, pSpeed);
			qry.AddSpan(CabRideEngine.EntityRelations.Drivers_DriversGPSHistory);

			if (pDriverID > 0) {
				qry.AddClause(CabRideEngine.DriversGPSHistory.DriverIDEntityColumn, EntityQueryOp.EQ, pDriverID);
			} else {

				List<long> driverList = new List<long>();
				EntityList<Drivers> myDrivers = Drivers.GetDrivers(pAffiliate);
				foreach (Drivers oDriver in myDrivers) {
					driverList.Add(oDriver.DriverID);
				}
				qry.AddClause(DriversGPSHistory.DriverIDEntityColumn, EntityQueryOp.In, driverList);
			}
			//qry.AddClause(Drivers.AffiliateIDEntityColumn, EntityQueryOp.EQ, pAffiliate.AffiliateID);
			qry.AddOrderBy(DriversGPSHistory.GPSTimeEntityColumn);
			qry.AddOrderBy(DriversGPSHistory.SpeedEntityColumn);
			return pAffiliate.PersistenceManager.GetEntities<DriversGPSHistory>(qry, QueryStrategy.DataSourceThenCache);
		}

		public static DriversGPSHistory GetDriverRecordForDateTime(Drivers pDriver, DateTime pDate) {
			RdbQuery qry = new RdbQuery(typeof(DriversGPSHistory), DriversGPSHistory.DriverIDEntityColumn, EntityQueryOp.EQ, pDriver.DriverID);
			qry.AddClause(DriversGPSHistory.GPSTimeEntityColumn, EntityQueryOp.LE, pDate);
			qry.AddOrderBy(DriversGPSHistory.GPSTimeEntityColumn, System.ComponentModel.ListSortDirection.Descending);
			qry.Top = 1;
			return pDriver.PersistenceManager.GetEntity<DriversGPSHistory>(qry, QueryStrategy.DataSourceThenCache);
		}

		//public string LocalGPSDateTimeString {
		//    get {
		//        string sTime = "";
		//        if (this.Drivers.MyAffiliate != null && !string.IsNullOrEmpty(this.Drivers.MyAffiliate.TimeZone)) {
		//            TimeZoneConverter oTime = new TimeZoneConverter();
		//            sTime = oTime.FromUniversalTime(this.Drivers.MyAffiliate.TimeZone, this.GPSTime).ToString("MM/dd/yyyy hh:mm tt");
		//        } else {
		//            sTime = this.GPSTime.ToLocalTime().ToString("MM/dd/yyyy hh:mm tt");
		//        }
		//        return sTime;
		//    }
		//}

		//public DateTime LocalGPSAcquiredDateTime {
		//    get {
		//        if (!this.GPSAcquiredTime.HasValue) {
		//            return LocalGPSDateTime;
		//        }
		//        if (this.Drivers.MyAffiliate != null && !string.IsNullOrEmpty(this.Drivers.MyAffiliate.TimeZone)) {
		//            TimeZoneConverter oTime = new TimeZoneConverter();
		//            return oTime.FromUniversalTime(this.Drivers.MyAffiliate.TimeZone, this.GPSAcquiredTime.Value);
		//        }
		//        return this.GPSAcquiredTime.Value.ToLocalTime();
		//    }
		//}

		//public DateTime LocalGPSDateTime {
		//    get {
		//        if (this.Drivers.MyAffiliate != null && !string.IsNullOrEmpty(this.Drivers.MyAffiliate.TimeZone)) {
		//            TimeZoneConverter oTime = new TimeZoneConverter();
		//            return oTime.FromUniversalTime(this.Drivers.MyAffiliate.TimeZone, this.GPSTime);
		//        }
		//        return this.GPSTime.ToLocalTime();
		//    }
		//}

		//public string LocalMeterStatusDateTimeString {
		//    get {
		//        string sTime = "";
		//        if (this.MeterStatusTime.HasValue) {
		//            if (this.Drivers.MyAffiliate != null && !string.IsNullOrEmpty(this.Drivers.MyAffiliate.TimeZone)) {
		//                TimeZoneConverter oTime = new TimeZoneConverter();
		//                sTime = oTime.FromUniversalTime(this.Drivers.MyAffiliate.TimeZone, this.MeterStatusTime.Value).ToString("MM/dd/yyyy hh:mm tt");
		//            } else {
		//                sTime = this.MeterStatusTime.Value.ToLocalTime().ToString("MM/dd/yyyy hh:mm tt");
		//            }
		//        }
		//        return sTime;
		//    }
		//}

		//public DateTime? LocalMeterStatusDateTime {
		//    get {
		//        if (this.MeterStatusTime.HasValue) {
		//            if (this.Drivers.MyAffiliate != null && !string.IsNullOrEmpty(this.Drivers.MyAffiliate.TimeZone)) {
		//                TimeZoneConverter oTime = new TimeZoneConverter();
		//                return oTime.FromUniversalTime(this.Drivers.MyAffiliate.TimeZone, this.MeterStatusTime.Value);
		//            } else {
		//                return this.MeterStatusTime.Value.ToLocalTime();
		//            }
		//        }
		//        return this.GPSTime;
		//    }
		//}

		public bool IsCellTowerReading() {
			if (this.LocationMethod == null) {
				return false;
			}
			return (!this.LocationMethod.StartsWith("Sat", StringComparison.CurrentCultureIgnoreCase) && !this.LocationMethod.Equals("327681"));
		}

		public string Name {
			get {
				return this.Drivers.Name;
			}
		}

	}

	#region EntityPropertyDescriptors.DriversGPSHistoryPropertyDescriptor
	////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
	////    For example: 
	////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
	//public static partial class EntityPropertyDescriptors {
	//	public partial class DriversGPSHistoryPropertyDescriptor : AdaptedPropertyDescriptor {
	//    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
	//    public AdaptedPropertyDescriptor Age {
	//      get { return this.Get("Age"); }
	//    }
	//  }
	//} 
	#endregion

}
