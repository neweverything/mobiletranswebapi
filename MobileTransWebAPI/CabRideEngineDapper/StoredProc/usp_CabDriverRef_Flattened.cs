﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Dapper;


namespace CabRideEngineDapper {
	public partial class usp_CabDriverRef_Flattened {

		public string AppInBackground { get; set; }
		public string CertLocation { get; set; }
		public string DeviceToken { get; set; }
		public string DeviceType { get; set; }
		public string FraudCleared { get; set; }
		public string IsRegistered { get; set; }
		public string MaxPassengers { get; set; }
		public string PickUpLocations { get; set; }
		public string PricingInfo { get; set; }
		public string ProfileThumbURL { get; set; }
		public string ProfileURL { get; set; }
		public string RideAvailability { get; set; }
		public string RideQDispatch { get; set; }
		

		public static usp_CabDriverRef_Flattened Execute(SqlConnection pConn, long pDriverID) {
			return pConn.Query<usp_CabDriverRef_Flattened>("usp_CabDriverRef_Flattened", new { DriverID = pDriverID }, commandType: CommandType.StoredProcedure).FirstOrDefault();
		}
	}
}
