﻿using CabRideEngineDapper;
using Dapper;
using IdeaBlade.Persistence;
using RestSharp;
using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using TaxiPassCommon;
using TaxiPassCommon.Mail;

namespace MobileTransWebAPI {
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class WebApiApplication : System.Web.HttpApplication {
        protected void Application_Start() {
            AreaRegistration.RegisterAllAreas();
            // this will give info when in the .content when things go boom
            GlobalConfiguration.Configuration.IncludeErrorDetailPolicy = IncludeErrorDetailPolicy.Always;

            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            TaxiPassCommon.GlobalSettings.DecryptIdeaBladeConnectionStrings();

            CabRideDapperSettings.DataSource = GetDataSource();
            CabRideDapperSettings.SqlConnectionString = AppHelper.Initializer.GetConnectionString(CabRideEngineDapper.CabRideDapperSettings.DataSource);
            CabRideDapperSettings.LoggedInUser = Properties.Settings.Default.DefaultUserLogin;

        }


        public static PersistenceManager UserPM {
            get {
                try {
                    String sDataEnvironment = GetDataSource();

                    PersistenceManager oPM = new PersistenceManager(false, sDataEnvironment);
                    oPM.Connect();
                    LoginCredential credential = new LoginCredential(Properties.Settings.Default.DefaultUserLogin, Properties.Settings.Default.DefaultUserPassword, "xxx");
                    oPM.Login(credential);

                    // set reads to uncommitted to improve performance
                    TransactionSettings transSettings = new TransactionSettings(UseDTCOption.False, System.Transactions.IsolationLevel.ReadUncommitted, new TimeSpan(0, 0, 30));
                    oPM.DefaultQueryStrategy = new QueryStrategy(FetchStrategy.CacheThenDataSource, MergeStrategy.PreserveChanges, transSettings);

                    return oPM;
                } catch (Exception ex) {

                    // Create a title that includes the machine name
                    StringBuilder sTitle = new StringBuilder();
                    sTitle.AppendFormat("{1} StartUp Error [{0}]", System.Environment.MachineName, Properties.Settings.Default.SystemName);

                    string html = "Could not log in to DB: " + ex.Message.ToString();
                    EMailMessage mail = new EMailMessage(Properties.Settings.Default.EMailFrom, sTitle.ToString(), html);
                    mail.SendMail(Properties.Settings.Default.ErrorEmailRecipient);
                }
                return null;
            }
        }

        private static string GetDataSource() {
            String sDataEnvironment = "default";
            if (Properties.Settings.Default.SelectDataEnvironment) {
                sDataEnvironment = Properties.Settings.Default.DataEnvironment;
            }
            return sDataEnvironment;
        }

        public static TimeZoneConverter MyTZConverter = new TimeZoneConverter();

        //public static CabRideEngineEF.CabRideEntities UserCtx {
        //	get {
        //		try {
        //			string dataEnvironment = "default";
        //			if (Properties.Settings.Default.SelectDataEnvironment) {
        //				dataEnvironment = Properties.Settings.Default.DataEnvironment;
        //			}


        //			CabRideEngineEF.CabRideEntities ctx = CabRideEngineEF.EFGlobalSettings.GetCabRideEntities(dataEnvironment);

        //			return ctx;
        //		} catch (Exception ex) {
        //			TaxiPassCommon.Web.Log.ServerLog.ServerLogIt("UserCtx", "Could not connect to Database: " + ex.Message.ToString());
        //		}
        //		return null;
        //	}
        //}

        public static void LogIt(string pTitle, string pMsg) {
            if (Properties.Settings.Default.VerboseLogging) {
                try {
                    string path = Path.GetDirectoryName(Properties.Settings.Default.LogFile);
                    if (!Directory.Exists(path)) {
                        Directory.CreateDirectory(path);
                    }
                    string logFile = string.Format(Properties.Settings.Default.LogFile, DateTime.Today.ToString("yyyyMMdd"));
                    TaxiPassCommon.Web.Log.ServerLog.ServerLogIt(logFile, pTitle, pMsg);
                } catch (Exception) {
                }
            }
        }

        public static void LogIt(string pTitle, string pMsg, string pFileName) {
            if (Properties.Settings.Default.VerboseLogging) {
                try {
                    string path = Path.GetDirectoryName(Properties.Settings.Default.LogFile);
                    if (!Directory.Exists(path)) {
                        Directory.CreateDirectory(path);
                    }
                    pFileName = Path.Combine(path, pFileName);
                    string logFile = string.Format(pFileName, DateTime.Today.ToString("yyyyMMdd"));
                    TaxiPassCommon.Web.Log.ServerLog.ServerLogIt(logFile, pTitle, pMsg);
                } catch (Exception) {
                }
            }
        }


        public static DateTime GetUTCServerTime() {
            string sql = @"SELECT GETUTCDATE() UTCDate";
            using (var conn = CabRideEngineDapper.Utils.SqlHelper.OpenSqlConnection()) {
                return conn.Query<DateTime>(sql).DefaultIfEmpty(DateTime.UtcNow).FirstOrDefault();
            }
        }

        public static RestClient GetWebService() {
            string url = "https://cabpay156.1800cabride.com/mobiletranswebapi/api"; // "https://cabpay.com/mobiletranswebapi/api";

            RestClient client = new RestClient(url);
            return client;
        }

        //protected void Application_BeginRequest(object sender, EventArgs e) {
        //    try {
        //        if (HttpContext.Current.Request.AppRelativeCurrentExecutionFilePath.ToLower().Contains("creditcardreceipt")) {
        //            byte[] inputStream = new byte[HttpContext.Current.Request.ContentLength];

        //            // Read entire request inputstream
        //            HttpContext.Current.Request.InputStream.Read(inputStream, 0, inputStream.Length);

        //            //Set stream back to beginning
        //            HttpContext.Current.Request.InputStream.Position = 0;

        //            //Get  XML request

        //            string requestString = ASCIIEncoding.ASCII.GetString(inputStream);
        //            //LogIt("Begin Request", requestString, "stream.txt");
        //            TaxiPassCommon.Web.Log.ServerLog.ServerLogIt(@"e:\temp\stream.txt", "Begin Request", HttpContext.Current.Request.AppRelativeCurrentExecutionFilePath + "?" + requestString + "    " + HttpContext.Current.Request.HttpMethod);
        //        }
        //    } catch (Exception ex) {
        //        //LogIt("Begin Request Error", ex.Message, "stream.txt");
        //        try {
        //            TaxiPassCommon.Web.Log.ServerLog.ServerLogIt(@"e:\temp\stream.txt", "Begin Request Error", ex.Message);
        //        } catch (Exception) {
        //        }
        //    }
        //}
    }
}