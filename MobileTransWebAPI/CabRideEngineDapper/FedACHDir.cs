﻿using CabRideEngineDapper.Utils;
using Dapper;
using System.Data.SqlClient;

namespace CabRideEngineDapper {
    public partial class FedACHDir {

        public static FedACHDir GetOrCreate(string pRoutingNo) {
            using (var conn = SqlHelper.OpenSqlConnection()) {
                return GetOrCreate(conn, pRoutingNo);
            }
        }

        public static FedACHDir GetOrCreate(SqlConnection pConn, string pRoutingNo) {
            FedACHDir rec = GetByRoutingNo(pConn, pRoutingNo);
            if (rec.IsNullEntity) {
                rec = FedACHDirTable.Create();
            }
            return rec;
        }

        public static FedACHDir GetByRoutingNo(string pRoutingNo) {
            using (var conn = SqlHelper.OpenSqlConnection()) {
                return GetByRoutingNo(conn, pRoutingNo);
            }
        }

        public static FedACHDir GetByRoutingNo(SqlConnection pConn, string pRoutingNo) {
            string sql = @"SELECT * FROM FedACHDir
                           WHERE Routing = @RoutingNo";

            return pConn.Get<FedACHDir>(sql, new { RoutingNo = pRoutingNo });
        }
    }
}
