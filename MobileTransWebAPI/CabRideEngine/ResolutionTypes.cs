﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

using Csla.Validation;

namespace CabRideEngine {
	[Serializable]
	public sealed class ResolutionTypes : ResolutionTypesDataRow {

		#region Constructors -- DO NOT MODIFY
		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new'
		private ResolutionTypes() : this(null) {
		}

		// Typed DataSet constructor (needed by framework) - DO NOT REMOVE
		public ResolutionTypes(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static ResolutionTypes Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      ResolutionTypes aResolutionTypes = pManager.CreateEntity<ResolutionTypes>();
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aResolutionTypes, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aResolutionTypes.AddToManager();
		//      return aResolutionTypes;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		// Add additional logic to your business object here...
		public static ResolutionTypes Create(PersistenceManager pPM) {
			ResolutionTypes oType = null;

			try {
				oType = (ResolutionTypes)pPM.CreateEntity(typeof(ResolutionTypes));

				// Using the IdeaBlade Id Generation technique
				//pPM.GenerateId(oType, ResolutionTypes.ResolutionTypeIDEntityColumn);

				oType.AddToManager();

			} catch (Exception ex) {
				throw ex;
			}
			return oType;
		}

		public override string ResolutionType {
			get {
				return base.ResolutionType;
			}
			set {
				

				base.ResolutionType = value;
			}
		}

		protected override void AddRules(RuleList pList) {
			PropertyRequiredRule.AddToList(pList, ResolutionTypes.ResolutionTypeEntityColumn.ColumnName);
			pList.Add(IsResolutionTypeUnique, ResolutionTypeEntityColumn.ColumnName);

		}

		private bool IsResolutionTypeUnique(object pTarget, RuleArgs e) {
			ResolutionTypes oType = (ResolutionTypes)pTarget;

			RdbQuery oQry = new RdbQuery(typeof(ResolutionTypes), ResolutionTypes.ResolutionTypeEntityColumn, EntityQueryOp.EQ, oType.ResolutionType);
			oQry.AddClause(ResolutionTypes.ResolutionTypeIDEntityColumn, EntityQueryOp.NE, oType.ResolutionTypeID);
			EntityList<ResolutionTypes> oTypes = this.PersistenceManager.GetEntities<ResolutionTypes>(oQry);
			if (oTypes.Count > 0) {
				e.Description = "Resolution Type must be unique!";
				return false;
			}
			return true;
		}

		protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
			if (IsDeserializing)
				return;
			base.OnColumnChanging(pArgs);
			//DoAudit(pArgs);
		}

		private void DoAudit(DataColumnChangeEventArgs pArgs) {
			if (!this.PersistenceManager.IsClient) {
				return;
            }
            if (this.RowState == DataRowState.Added) {
                return;
            }
			if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
				AuditRecord = ResolutionTypesAudit.Create(this.PersistenceManager, this);
			}
			//AuditRecord[pArgs.Column.ColumnName] = pArgs.ProposedValue;
		}

		[BindingBrowsable(false)]
		public override string EntityInstanceDisplayName {
			get {
				return ResolutionType;
			}
		}


	}

}
