﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using System.Linq;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

using TaxiPassCommon;
using System.Text;

namespace CabRideEngine {
	/// <summary>
	/// The developer class representing the RedeemerAccountingTypeBalance business object class.
	/// </summary>
	/// <remarks>
	/// <para>Place all application-specific business logic in this class.</para>
	/// <para>
	/// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
	/// and will not overwrite changes.
	/// </para>
	/// <para>
	/// This class inherits from the generated class, its companion in the pair of classes 
	/// defining a business object. Properties in the generated class may be overridden in 
	/// order to change the behavior or add business logic.
	/// </para>
	/// <para>
	/// This class is the final class in the inheritance chain for this business object. 
	/// It should not be inherited.
	/// </para>
	/// </remarks>
	[Serializable]
	public sealed partial class RedeemerAccountingTypeBalance : RedeemerAccountingTypeBalanceDataRow {

		#region Constructors -- DO NOT MODIFY

		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new';
		// it is also required if the class is visible from a web service.
		// DO NOT REMOVE **
		private RedeemerAccountingTypeBalance() : this(null) { }

		/// <summary>
		/// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
		/// </summary>
		/// <param name="pRowBuilder"></param>
		public RedeemerAccountingTypeBalance(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static RedeemerAccountingTypeBalance Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      RedeemerAccountingTypeBalance aRedeemerAccountingTypeBalance = pManager.CreateEntity<RedeemerAccountingTypeBalance>();
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aRedeemerAccountingTypeBalance, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aRedeemerAccountingTypeBalance.AddToManager();
		//      return aRedeemerAccountingTypeBalance;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		// Add additional logic to your business object here...
		public static RedeemerAccountingTypeBalance Create(TaxiPassRedeemerAccounts pRedeemerAcct, long pCashAccountTypeID, long pRedeemerCashAccountingID) {
			RedeemerAccountingTypeBalance rec = null;
			try {
				rec = pRedeemerAcct.PersistenceManager.CreateEntity<RedeemerAccountingTypeBalance>();
				rec.RedeemerCashAccountingID = pRedeemerCashAccountingID;
				rec.CashAccountTypeID = pCashAccountTypeID;
				rec.TaxiPassRedeemerAccountID = pRedeemerAcct.TaxiPassRedeemerAccountID;
				rec.AddToManager(); // add 1st to allow IdeaBlade to pull other records as needed
				rec.TransferDate = DateTime.Now;
			} catch (Exception ex) {
				throw ex;
			}
			return rec;
		}


		public override DateTime TransferDate {
			get {
				return base.TransferDate;
			}
			set {
				TimeZoneConverter tzConvert = new TimeZoneConverter();
				base.TransferDate = tzConvert.ConvertTime(value, TimeZone.CurrentTimeZone.StandardName, this.TaxiPassRedeemerAccounts.Markets.TimeZone);
			}
		}


		public string Deposit {
			get {
				if (Amount > 0) {
					return Amount.ToString("C");
				}
				return "";
			}
		}

		public string Withdrawal {
			get {
				if (Amount < 0) {
					return Math.Abs(Amount).ToString("C");
				}
				return "";
			}
		}

		public string Description {
			get {
				if (Amount > 0) {
					return ("From: " + this.RedeemerCashAccounting.FromAccount + " To: " + this.RedeemerCashAccounting.ToAccount + " - " + this.RedeemerCashAccounting.Notes).Trim();
				} else {
					return ("To: " + this.RedeemerCashAccounting.ToAccount + " From: " + this.RedeemerCashAccounting.FromAccount + " - " + this.RedeemerCashAccounting.Notes).Trim();
				}
			}
		}

		public string DescriptionMultiLineHTML {
			get {
				if (Amount > 0) {
					return ("From: " + this.RedeemerCashAccounting.FromAccount + " To: " + this.RedeemerCashAccounting.ToAccount + "<br />" + this.RedeemerCashAccounting.Notes).Trim();
				} else {
					return ("To: " + this.RedeemerCashAccounting.ToAccount + " From: " + this.RedeemerCashAccounting.FromAccount + "<br />" + this.RedeemerCashAccounting.Notes).Trim();
				}
			}
		}

		public string AccountTypeDesc {
			get {
				return this.CashAccountType.Type;
			}
		}


		public static RedeemerAccountingTypeBalance GetLastBalance(TaxiPassRedeemerAccounts pRedeemerAcct, CashAccountType pAcctType) {
			RdbQuery qry = new RdbQuery(typeof(RedeemerAccountingTypeBalance), RedeemerAccountingTypeBalance.TaxiPassRedeemerAccountIDEntityColumn, EntityQueryOp.EQ, pRedeemerAcct.TaxiPassRedeemerAccountID);
			qry.AddClause(RedeemerAccountingTypeBalance.CashAccountTypeIDEntityColumn, EntityQueryOp.EQ, pAcctType.CashAccountTypeID);
			qry.AddOrderBy(RedeemerAccountingTypeBalance.TransferDateEntityColumn, System.ComponentModel.ListSortDirection.Descending);
			qry.Top = 1;
			return pRedeemerAcct.PersistenceManager.GetEntity<RedeemerAccountingTypeBalance>(qry, QueryStrategy.DataSourceOnly);
		}

		public static EntityList<RedeemerAccountingTypeBalance> GetBalanceByAccountType(TaxiPassRedeemerAccounts pRedeemerAcct, long pCashAccountTypeID, DateTime pStartDate, DateTime pEndDate) {
			RdbQuery qry = new RdbQuery(typeof(RedeemerAccountingTypeBalance), RedeemerAccountingTypeBalance.TaxiPassRedeemerAccountIDEntityColumn, EntityQueryOp.EQ, pRedeemerAcct.TaxiPassRedeemerAccountID);
			qry.AddClause(RedeemerAccountingTypeBalance.CashAccountTypeIDEntityColumn, EntityQueryOp.EQ, pCashAccountTypeID);
			qry.AddClause(RedeemerAccountingTypeBalance.TransferDateEntityColumn, EntityQueryOp.GE, pStartDate.Date);
			qry.AddClause(RedeemerAccountingTypeBalance.TransferDateEntityColumn, EntityQueryOp.LT, pEndDate.AddDays(1).Date);
			qry.AddOrderBy(RedeemerAccountingTypeBalance.TransferDateEntityColumn, System.ComponentModel.ListSortDirection.Descending);
			return pRedeemerAcct.PersistenceManager.GetEntities<RedeemerAccountingTypeBalance>(qry, QueryStrategy.DataSourceThenCache);
		}

		public static EntityList<RedeemerAccountingTypeBalance> GetBalances(TaxiPassRedeemerAccounts pRedeemerAcct, DateTime pStartDate, DateTime pEndDate) {
			RdbQuery qry = new RdbQuery(typeof(RedeemerAccountingTypeBalance), RedeemerAccountingTypeBalance.TaxiPassRedeemerAccountIDEntityColumn, EntityQueryOp.EQ, pRedeemerAcct.TaxiPassRedeemerAccountID);
			qry.AddClause(RedeemerAccountingTypeBalance.TransferDateEntityColumn, EntityQueryOp.GE, pStartDate.Date);
			qry.AddClause(RedeemerAccountingTypeBalance.TransferDateEntityColumn, EntityQueryOp.LT, pEndDate.AddDays(1).Date);
			qry.AddOrderBy(RedeemerAccountingTypeBalance.TransferDateEntityColumn, System.ComponentModel.ListSortDirection.Descending);
			return pRedeemerAcct.PersistenceManager.GetEntities<RedeemerAccountingTypeBalance>(qry, QueryStrategy.DataSourceThenCache);
		}

		public static EntityList<RedeemerAccountingTypeBalance> GetLastBalancesForDate(TaxiPassRedeemerAccounts pRedeemerAcct, DateTime pDate) {
			StringBuilder sql = new StringBuilder();
			sql.Append("execute usp_RedeemerAccountingTypeBalance_GetLastBalancesForDate ");
			sql.Append(pRedeemerAcct.TaxiPassRedeemerAccountID.ToString("F0"));
			sql.Append(", '");
			sql.Append(pDate.ToString("MM/dd/yyyy"));
			sql.Append("'");
			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(RedeemerAccountingTypeBalance), sql.ToString());
			return pRedeemerAcct.PersistenceManager.GetEntities<RedeemerAccountingTypeBalance>(qry);
		}
	}

	#region EntityPropertyDescriptors.RedeemerAccountingTypeBalancePropertyDescriptor
	////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
	////    For example: 
	////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
	//public static partial class EntityPropertyDescriptors {
	//	public partial class RedeemerAccountingTypeBalancePropertyDescriptor : BaseEntityPropertyDescriptor {
	//    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
	//    public AdaptedPropertyDescriptor Age {
	//      get { return this.Get("Age"); }
	//    }
	//  }
	//} 
	#endregion

}