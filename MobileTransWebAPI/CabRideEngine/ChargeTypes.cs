using System;
using System.Collections.Generic;
using System.Text;

using IdeaBlade.Util;

namespace CabRideEngine {
    public class ChargeTypes {

        public static string BaseFee {
            get {
                return "Base Fee";
            }
        }

        public static string DropFee {
            get {
                return "Drop";
            }
        }

        public static string AirportFee {
            get {
                return "Airport";
            }
        }

        public static string VoucherFee {
            get {
                return "Voucher";
            }
        }

        public static string TollsFee {
            get {
                return "Tolls";
            }
        }

        public static string Gratuity {
            get {
                return "Gratuity";
            }
        }

        public static string TaxiPassFee {
            get {
                return "TaxiPass";
            }
        }

        public static string OtherFee {
            get {
                return "Other Fees";
            }
        }

        public static string Discount {
            get {
                return "Discount";
            }
        }

        public static string BookingFee {
            get {
                return "Booking Fee";
            }
        }

        public static string ServiceFee {
            get {
                return "Service Fee";
            }
        }

        private string mChargeType;
        private bool mIsNullEntity = false;

        private static BindableList<ChargeTypes> msEntities;
        private static ChargeTypes msNullEntity;

        /// <summary>Class Constructor (Private)</summary>
        static ChargeTypes() {

            // Create the null entity
            msNullEntity = new ChargeTypes("<none>");
            msNullEntity.mIsNullEntity = true;

            // Populate list of Charge Types.
            msEntities = new BindableList<ChargeTypes>();
            msEntities.Add(new ChargeTypes(BaseFee));
            msEntities.Add(new ChargeTypes(DropFee));
            msEntities.Add(new ChargeTypes(AirportFee));
            msEntities.Add(new ChargeTypes(VoucherFee));
            msEntities.Add(new ChargeTypes(TollsFee));
            msEntities.Add(new ChargeTypes(Gratuity));
            msEntities.Add(new ChargeTypes(TaxiPassFee));
            msEntities.Add(new ChargeTypes(OtherFee));
            msEntities.Add(new ChargeTypes(Discount));
            msEntities.Add(new ChargeTypes(BookingFee));
            msEntities.Add(new ChargeTypes(ServiceFee));
        }

        /// <summary>Constructor (Private)</summary>
        private ChargeTypes(string pChargeType) {
            mChargeType = pChargeType;
        }

        /// <summary>Get the Incident Types null entity.</summary>
        public static ChargeTypes NullEntity {
            get {
                return msNullEntity;
            }
        }

        /// <summary>Get the list of all Incident Types.</summary>
        public static BindableList<ChargeTypes> GetAll() {
            return msEntities;
        }

        /// <summary>Get the list of all Incident Types.</summary>
        public static ChargeTypes GetById(string pId) {
            if (String.IsNullOrEmpty(pId))
                return NullEntity;
            foreach (ChargeTypes oChargeType in msEntities) {
                if (oChargeType.ChargeType == pId)
                    return oChargeType;
            }
            return NullEntity;
        }

        /// <summary>Return true if this is the null entity</summary>
        [BindingBrowsable(false)]
        public bool IsNullEntity {
            get {
                return mIsNullEntity;
            }
        }

        /// <summary>Get the Charge Type</summary>
        public string ChargeType {
            get {
                return mChargeType;
            }
        }



    }
}
