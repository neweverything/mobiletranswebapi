﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using System.Collections.Specialized;

using IdeaBlade.Persistence;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;
using IdeaBlade.Rdb;

namespace CabRideEngine {
    [Serializable]
    public sealed class ParkingCompanies : ParkingCompaniesDataRow {

        #region Constructors -- DO NOT MODIFY
        // Do not create constructors for this class
        // Developers cannot create instances of this class with the "new" keyword
        // You should write a static Create method instead; see the "Suggested Customization" region
        // Please review the documentation to learn about entity creation and inheritance

        // This private constructor prevents accidental instance creation via 'new'
        private ParkingCompanies()
            : this(null) {
        }

        // Typed DataSet constructor (needed by framework) - DO NOT REMOVE
        public ParkingCompanies(DataRowBuilder pRowBuilder)
            : base(pRowBuilder) {
        }

        #endregion

        #region Suggested Customizations

        //    // Use this factory method template to create new instances of this class
        //    public static ParkingCompanies Create(PersistenceManager pManager,
        //      ... your creation parameters here ...) { 
        //
        //      ParkingCompanies aParkingCompanies = pManager.CreateEntity<ParkingCompanies>();
        //
        //      // if this object type requires a unique id and you have implemented
        //      // the IIdGenerator interface implement the following line
        //      pManager.GenerateId(aParkingCompanies, // add id column here //);
        //
        //      // Add custom code here
        //
        //      aParkingCompanies.AddToManager();
        //      return aParkingCompanies;
        //    }

        //    // Implement this method if you want your object to sort in a specific order
        //    public override int CompareTo(Object pObject) {
        //    }

        //    // Implement this method to customize the null object version of this class
        //    protected override void UpdateNullEntity() {
        //    }

        #endregion

        // Add additional logic to your business object here...
        public static ParkingCompanies Create(PersistenceManager pPM) {
            ParkingCompanies oCompany = null;
            try {
                oCompany = (ParkingCompanies)pPM.CreateEntity(typeof(ParkingCompanies));

                // Using the IdeaBlade Id Generation technique
                pPM.GenerateId(oCompany, ParkingCompanies.ParkingCompanyIDEntityColumn);
                oCompany.Country = Countries.GetDefaultCountry(pPM).Country;

                oCompany.AddToManager();

            } catch (Exception ex) {
                throw ex;
            }
            return oCompany;
        }

        protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
            if (IsDeserializing)
                return;
            base.OnColumnChanging(pArgs);
            DoAudit(pArgs);
        }

        private void DoAudit(DataColumnChangeEventArgs pArgs) {
            if (!this.PersistenceManager.IsClient) {
                return;
            }
            if (this.RowState == DataRowState.Added) {
                return;
            }
            if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
                AuditRecord = ParkingCompaniesAudit.Create(this.PersistenceManager, this);
            }
            //AuditRecord[pArgs.Column.ColumnName] = pArgs.ProposedValue;
        }


        public static StringCollection GetCountries(PersistenceManager pPM) {
            AdoHelper oHelper = new AdoHelper();
            RdbKey aRdbKey = pPM.DataSourceResolver.GetDataSourceKey(typeof(ParkingCompanies)) as RdbKey;
            oHelper = aRdbKey.AdoHelper;

            string sqlSelect = "SELECT DISTINCT " + ParkingCompanies.CountryEntityColumn.ColumnName + " FROM " +
                                "ParkingCompanies ";


            sqlSelect += "ORDER BY " + ParkingCompanies.CountryEntityColumn.ColumnName;


            StringCollection oCountries = new StringCollection();
            IDbConnection aConnection = oHelper.CreateDbConnection(true);
            aConnection.Open();
            using (aConnection) {
                IDbCommand aCommand = oHelper.CreateDbCommand(aConnection);
                aCommand.CommandText = sqlSelect;
                IDataReader aDataReader = aCommand.ExecuteReader();
                while (aDataReader.Read()) {
                    oCountries.Add(aDataReader.GetString(0));
                }
                aDataReader.Close();
            }

            return oCountries;
        }


        public static BindableList<ParkingStates> GetStates(PersistenceManager pPM, string pCountry) {

            AdoHelper oHelper = new AdoHelper();
            RdbKey aRdbKey = pPM.DataSourceResolver.GetDataSourceKey(typeof(ParkingCompanies)) as RdbKey;
            oHelper = aRdbKey.AdoHelper;

            string sqlSelect = "SELECT DISTINCT States.State, States.StateName FROM ParkingCompanies " +
                                "INNER JOIN States ON ParkingCompanies.Country = States.Country AND " +
                                            "ParkingCompanies.State = States.State " +
                                "WHERE ParkingCompanies.Country = '" + pCountry + "' " +
                                "ORDER BY StateName ASC";

            BindableList<ParkingStates> oStates = new BindableList<ParkingStates>();

            IDbConnection aConnection = oHelper.CreateDbConnection(true);
            aConnection.Open();
            using (aConnection) {
                IDbCommand aCommand = oHelper.CreateDbCommand(aConnection);
                aCommand.CommandText = sqlSelect;
                IDataReader aDataReader = aCommand.ExecuteReader();
                while (aDataReader.Read()) {
                    ParkingStates oState = new ParkingStates();
                    oState.State = aDataReader[States.StateEntityColumn.ColumnName].ToString();
                    oState.Name = aDataReader[States.StateNameEntityColumn.ColumnName].ToString();
                    oStates.Add(oState);
                }
                aDataReader.Close();
            }

            return oStates;
        }

        public static BindableList<ParkingAirports> GetAirports(PersistenceManager pPM, string pCountry, string pState) {
            BindableList<ParkingAirports> oAirports = new BindableList<ParkingAirports>();

            AdoHelper oHelper = new AdoHelper();
            RdbKey aRdbKey = pPM.DataSourceResolver.GetDataSourceKey(typeof(ParkingCompanies)) as RdbKey;
            oHelper = aRdbKey.AdoHelper;

            string sqlSelect = "SELECT DISTINCT  Airports.AirportID, Airports.Name FROM ParkingCompanies " +
                                "INNER JOIN ParkingLocations ON ParkingCompanies.ParkingCompanyID = ParkingLocations.ParkingCompanyID " +
                                "INNER JOIN Airports ON ParkingLocations.AirportID = Airports.AirportID " +
                                "WHERE ParkingCompanies.Country = '" + pCountry + "' AND ParkingCompanies.State = '" + pState + "' " +
                                "ORDER BY Airports.Name";


            IDbConnection aConnection = oHelper.CreateDbConnection(true);
            aConnection.Open();
            using (aConnection) {
                IDbCommand aCommand = oHelper.CreateDbCommand(aConnection);
                aCommand.CommandText = sqlSelect;
                IDataReader aDataReader = aCommand.ExecuteReader();
                while (aDataReader.Read()) {
                    ParkingAirports oAir = new ParkingAirports();
                    oAir.AirportID = Convert.ToInt64(aDataReader["AirportID"]);
                    oAir.AirportName = aDataReader["Name"].ToString();
                    oAirports.Add(oAir);
                }
                aDataReader.Close();
            }

            return oAirports;

        }



    }


    public class ParkingStates {
        private string mState;
        private string mName;

        public string Name {
            get {
                return mName;
            }
            set {
                mName = value;
            }
        }

        public string State {
            get {
                return mState;
            }
            set {
                mState = value;
            }
        }
    }

    public class ParkingAirports {

        private long mAirportID;
        private string mAirportName;

        public string AirportName {
            get {
                return mAirportName;
            }
            set {
                mAirportName = value;
            }
        }

        public long AirportID {
            get {
                return mAirportID;
            }
            set {
                mAirportID = value;
            }
        }
    }

}
