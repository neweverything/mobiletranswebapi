﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Collections;

using TaxiPassCommon;


namespace CabRideEngine.Utils {
    public static class TransCardUtils {
        public static void ChangeCardPin(AffiliateDrivers pDriver) {
            SystemDefaults oDefaults = SystemDefaults.GetDefaults(pDriver.PersistenceManager);

            string acctSid = oDefaults.TwilioAccountSID;
            string acctToken = oDefaults.TwilioAccountToken;
            string apiVersion = oDefaults.TwilioAPIVersion;

            string callerID = oDefaults.TwilioMMSNumber;

            TwilioRest.Account account = new TwilioRest.Account(acctSid, acctToken);


            string sendDigits = new string('w', 2 * 2);
            //card number
            sendDigits += "*ww" + pDriver.TransCardNumber;
            //wait 1 secs
            sendDigits += new string('w', 1 * 2);
            //old pin
            sendDigits += pDriver.TransCardID; // "0423";
            //wait 1 secs
            sendDigits += new string('w', 1 * 2);
            //new pin
            sendDigits += pDriver.Pin; // "8294";
            //wait 1 secs
            sendDigits += new string('w', 1 * 2);
            //confirm new pin
            sendDigits += "1";

            Hashtable h = new Hashtable();
            h.Add("Caller", callerID);
            h.Add("Called", "800-831-6565");
            h.Add("SendDigits", sendDigits);
            h.Add("Url", "https://cabpay.com/CellPhoneWebServices/TransCardChangePin.aspx");
            h.Add("Method", "GET");
            string msg = account.request(String.Format("/{0}/Accounts/{1}/Calls", apiVersion, acctSid), "POST", h);
            Console.WriteLine(msg);
        }
    }
}
