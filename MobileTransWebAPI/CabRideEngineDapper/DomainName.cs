﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace CabRideEngineDapper {
    public partial class DomainName {

        public static List<DomainName> GetAll(SqlConnection pConn) {
            string sql = @"SELECT * FROM DomainName
                            ORDER BY Name";

            return pConn.Query<DomainName>(sql).ToList();
        }

        public static DateTime GetLastUpdated(SqlConnection pConn) {
            string sql = @"SELECT MAX(ModifiedDate) FROM DomainName";

            return pConn.Get<DateTime>(sql);
        }
    }
}
