﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileTransWebAPI.Models {
	public class StatesResult {

		public string ErrorMsg { get; set; }

		public List<State> StateNames { get; set; }
	}

	public class State {
		public string StateCode { get; set; }
		public string StateName { get; set; }
	}
}
