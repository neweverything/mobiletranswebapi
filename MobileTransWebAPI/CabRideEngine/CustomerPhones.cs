using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

using TaxiPassCommon;

namespace CabRideEngine {
  /// <summary>
  /// The developer class representing the CustomerPhones business object class.
  /// </summary>
  /// <remarks>
  /// <para>Place all application-specific business logic in this class.</para>
  /// <para>
  /// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
  /// and will not overwrite changes.
  /// </para>
  /// <para>
  /// This class inherits from the generated class, its companion in the pair of classes 
  /// defining a business object. Properties in the generated class may be overridden in 
  /// order to change the behavior or add business logic.
  /// </para>
  /// <para>
  /// This class is the final class in the inheritance chain for this business object. 
  /// It should not be inherited.
  /// </para>
  /// </remarks>
  [Serializable]
  public sealed partial class CustomerPhones : CustomerPhonesDataRow {
  
    #region Constructors -- DO NOT MODIFY
    
    // Do not create constructors for this class
    // Developers cannot create instances of this class with the "new" keyword
    // You should write a static Create method instead; see the "Suggested Customization" region
    // Please review the documentation to learn about entity creation and inheritance

    // This private constructor prevents accidental instance creation via 'new';
    // it is also required if the class is visible from a web service.
    // DO NOT REMOVE **
    private CustomerPhones() : this(null) {}

    /// <summary>
    /// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
    /// </summary>
    /// <param name="pRowBuilder"></param>
    public CustomerPhones(DataRowBuilder pRowBuilder) 
      : base(pRowBuilder) {
    }

    #endregion
    
    #region Suggested Customizations
  
//    // Use this factory method template to create new instances of this class
//    public static CustomerPhones Create(PersistenceManager pManager,
//      ... your creation parameters here ...) { 
//
//      CustomerPhones aCustomerPhones = pManager.CreateEntity<CustomerPhones>();
//
//      // if this object type requires a unique id and you have implemented
//      // the IIdGenerator interface implement the following line
//      pManager.GenerateId(aCustomerPhones, // add id column here //);
//
//      // Add custom code here
//
//      aCustomerPhones.AddToManager();
//      return aCustomerPhones;
//    }

//    // Implement this method if you want your object to sort in a specific order
//    public override int CompareTo(Object pObject) {
//    }

//    // Implement this method to customize the null object version of this class
//    protected override void UpdateNullEntity() {
//    }

    #endregion
    
    // Add additional logic to your business object here...
	public static CustomerPhones Create(PersistenceManager pManager, long pCustomerID) {
		CustomerPhones oPhones = null;
		try {
			oPhones = (CustomerPhones)pManager.CreateEntity(typeof(CustomerPhones));

			// Using the IdeaBlade Id Generation technique
			oPhones.CustomerID = pCustomerID;
			//oCards.Number = "";

			oPhones.AddToManager();

		} catch (Exception ex) {
			throw ex;
		}
		return oPhones;
	}

	// ******************************************************************************
	// Given a customerid and phone, look it up
	// ******************************************************************************
	public static CustomerPhones GetCustomerPhone(PersistenceManager pPM, long pCustomerID, string pPhone) {
		pPhone = StringUtils.CleanPhoneNumbers(pPhone);

		RdbQuery qry = new RdbQuery(typeof(CustomerPhones), CustomerPhones.CustomerIDEntityColumn, EntityQueryOp.EQ, pCustomerID);
		qry.AddClause(CustomerPhones.PhoneNumberEntityColumn, EntityQueryOp.EQ, pPhone);

		return pPM.GetEntity<CustomerPhones>(qry);
	}

	// ******************************************************************************
	// Given a customerid and phone "CustomerDeviceID", look it up
	// ******************************************************************************
	public static CustomerPhones GetCustomerPhoneByDeviceID(PersistenceManager pPM, long pCustomerID, long pCustomerDeviceID) {

		RdbQuery qry = new RdbQuery(typeof(CustomerPhones), CustomerPhones.CustomerIDEntityColumn, EntityQueryOp.EQ, pCustomerID);
		qry.AddClause(CustomerPhones.CustomerDeviceIDEntityColumn, EntityQueryOp.EQ, pCustomerDeviceID);

		return pPM.GetEntity<CustomerPhones>(qry);
	}




	// ******************************************************************************
	// Given a customerid and phone, look it up
	// ******************************************************************************
	public static EntityList<CustomerPhones> GetCustomerPhones(PersistenceManager pPM, long pCustomerID) {
		RdbQuery qry = new RdbQuery(typeof(CustomerPhones), CustomerPhones.CustomerIDEntityColumn, EntityQueryOp.EQ, pCustomerID);
		qry.AddOrderBy(CustomerPhones.PhoneTypeEntityColumn);

		return pPM.GetEntities<CustomerPhones>(qry);
	}


	// ******************************************************************************
	// Return a formatted phone number
	// ******************************************************************************
	public string FormattedPhone {
		get {
			return this.PhoneNumber.FormattedPhoneNumber();
		}
	}
    
  }
  
  #region EntityPropertyDescriptors.CustomerPhonesPropertyDescriptor
  ////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
  ////    For example: 
  ////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
  //public static partial class EntityPropertyDescriptors {
	//	public partial class CustomerPhonesPropertyDescriptor : BaseEntityPropertyDescriptor {
  //    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
  //    public AdaptedPropertyDescriptor Age {
  //      get { return this.Get("Age"); }
  //    }
  //  }
  //} 
  #endregion
  
}