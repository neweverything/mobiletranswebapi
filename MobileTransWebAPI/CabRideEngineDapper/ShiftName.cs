﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Dapper;
using TaxiPassCommon;

using CabRideEngineDapper.Utils;

namespace CabRideEngineDapper {
	public partial class ShiftName {


		public static ShiftName GetShift(long pShiftID) {
			using (var conn = SqlHelper.OpenSqlConnection()) {
				return conn.Get<ShiftName>(pShiftID);
			}
		}
	}
}
