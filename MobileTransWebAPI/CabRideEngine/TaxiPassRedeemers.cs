﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using System.Text;

using IdeaBlade.Persistence;
using IdeaBlade.Util;
using IdeaBlade.Persistence.Rdb;

using CabRideEngine.Utils;
using TaxiPassCommon;

using Csla.Validation;

namespace CabRideEngine {
	/// <summary>
	/// The developer class representing the TaxiPassRedeemers business object class.
	/// </summary>
	/// <remarks>
	/// <para>Place all application-specific business logic in this class.</para>
	/// <para>
	/// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
	/// and will not overwrite changes.
	/// </para>
	/// <para>
	/// This class inherits from the generated class, its companion in the pair of classes 
	/// defining a business object. Properties in the generated class may be overridden in 
	/// order to change the behavior or add business logic.
	/// </para>
	/// <para>
	/// This class is the final class in the inheritance chain for this business object. 
	/// It should not be inherited.
	/// </para>
	/// </remarks>
	[Serializable]
	public sealed partial class TaxiPassRedeemers : TaxiPassRedeemersDataRow {

		#region Constructors -- DO NOT MODIFY

		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new';
		// it is also required if the class is visible from a web service.
		// DO NOT REMOVE **
		private TaxiPassRedeemers()
			: this(null) {
		}

		/// <summary>
		/// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
		/// </summary>
		/// <param name="pRowBuilder"></param>
		public TaxiPassRedeemers(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static TaxiPassRedeemers Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      TaxiPassRedeemers aTaxiPassRedeemers = pManager.CreateEntity<TaxiPassRedeemers>();
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aTaxiPassRedeemers, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aTaxiPassRedeemers.AddToManager();
		//      return aTaxiPassRedeemers;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		// Add additional logic to your business object here...
		public static TaxiPassRedeemers Create(TaxiPassRedeemerAccounts pTaxiPassRedeemerAccount) {
			TaxiPassRedeemers rec = null;

			try {
				PersistenceManager oPM = pTaxiPassRedeemerAccount.PersistenceManager;

				rec = oPM.CreateEntity<TaxiPassRedeemers>();
				rec.TransCardFundAmount = 0;

				// Using the IdeaBlade Id Generation technique
				oPM.GenerateId(rec, TaxiPassRedeemers.TaxiPassRedeemerIDEntityColumn);
				rec.TaxiPassRedeemerAccountID = pTaxiPassRedeemerAccount.TaxiPassRedeemerAccountID;
				rec.Salt = TaxiPassCommon.StringUtils.GenerateSalt();

				rec.AddToManager();

			} catch (Exception ex) {
				throw ex;
			}
			return rec;
		}

		protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
			if (IsDeserializing) {
				return;
			}
			base.OnColumnChanging(pArgs);
			DoAudit(pArgs);
		}

		private void DoAudit(DataColumnChangeEventArgs pArgs) {
			if (!this.PersistenceManager.IsClient) {
				return;
			}
			if (this.RowState == DataRowState.Added) {
				return;
			}
			if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
				AuditRecord = TaxiPassRedeemersAudit.Create(this.PersistenceManager, this);
			}
		}

		[BindingBrowsable(false)]
		public override string EntityInstanceDisplayName {
			get {
				return this.Name;
			}
		}

		protected override void AddRules(RuleList pList) {
			PropertyRequiredRule.AddToList(pList, NameColumn.ColumnName);
			PropertyRequiredRule.AddToList(pList, EmailEntityColumn.ColumnName);
			PropertyRequiredRule.AddToList(pList, PasswordEntityColumn.ColumnName);
			pList.Add(IsEMailUnique, EmailEntityColumn.ColumnName);
		}

		private bool IsEMailUnique(object pTarget, RuleArgs e) {
			TaxiPassRedeemers oRecord = pTarget as TaxiPassRedeemers;

			RdbQuery qry = new RdbQuery(typeof(TaxiPassRedeemers), EmailEntityColumn, EntityQueryOp.EQ, oRecord.Email);
			qry.AddClause(TaxiPassRedeemerIDEntityColumn, EntityQueryOp.NE, oRecord.TaxiPassRedeemerID);
			EntityList<TaxiPassRedeemers> oList = this.PersistenceManager.GetEntities<TaxiPassRedeemers>(qry);
			if (oList.Count > 0) {
				e.Description = "A TaxiPass Redeemer already exists with the email: " + oRecord.Email;
				return false;
			}
			return true;
		}

		public override bool BrinksUser {
			get {
				return base.BrinksUser;
			}
			set {
				base.BrinksUser = value;
				if (value) {
					Admin = false;
				}
			}
		}

		public override string Password {
			get {
				return base.Password;
			}
			set {
				Encryption oEncrypt = new Encryption();
				string sPwd = oEncrypt.Encrypt(value, this.Salt);
				base.Password = sPwd;
			}
		}

		public string PasswordDecrypted {
			get {
				Encryption oEncrypt = new Encryption();
				string sPwd = oEncrypt.Decrypt(base.Password, this.Salt);
				return sPwd;
			}
		}

		public string FormattedPhone {
			get {
				return this.Phone.FormattedPhoneNumber();
			}
		}

		public string SmartWAPLink {
			get {
				return "http://driverpay.1800cabride.com/default.aspx?z=" + this.Phone.EncryptIceKey();
			}
		}

		public string i335WAPLinkCash {
			get {
				return "http://creditcardtaxi.com/cellphonewebservice1/WAPRedemptionCash.aspx?z=" + this.Phone.EncryptIceKey();
			}
		}

		public static TaxiPassRedeemers GetRedeemer(PersistenceManager pPM, long pTaxiPassRedeemerID) {
			RdbQuery qry = new RdbQuery(typeof(TaxiPassRedeemers), TaxiPassRedeemerIDEntityColumn, EntityQueryOp.EQ, pTaxiPassRedeemerID);
			return pPM.GetEntity<TaxiPassRedeemers>(qry);
		}

		public static TaxiPassRedeemers GetRedeemer(PersistenceManager pPM, string pEMail) {
			RdbQuery qry = new RdbQuery(typeof(TaxiPassRedeemers), EmailEntityColumn, EntityQueryOp.EQ, pEMail);
			return pPM.GetEntity<TaxiPassRedeemers>(qry);
		}

		public static TaxiPassRedeemers GetRedeemer(TaxiPassRedeemerAccounts pTaxiPassRedeemerAccount, string pEMail) {
			RdbQuery qry = new RdbQuery(typeof(TaxiPassRedeemers), EmailEntityColumn, EntityQueryOp.EQ, pEMail);
			qry.AddClause(TaxiPassRedeemerAccountIDEntityColumn, EntityQueryOp.EQ, pTaxiPassRedeemerAccount.TaxiPassRedeemerAccountID);
			return pTaxiPassRedeemerAccount.PersistenceManager.GetEntity<TaxiPassRedeemers>(qry);
		}

		public static EntityList<TaxiPassRedeemers> GetRedeemers(TaxiPassRedeemerAccounts pTaxiPassRedeemerAccount) {
			RdbQuery qry = new RdbQuery(typeof(TaxiPassRedeemers), TaxiPassRedeemerAccountIDEntityColumn, EntityQueryOp.EQ, pTaxiPassRedeemerAccount.TaxiPassRedeemerAccountID);
			qry.AddOrderBy(TaxiPassRedeemers.NameEntityColumn);
			return pTaxiPassRedeemerAccount.PersistenceManager.GetEntities<TaxiPassRedeemers>(qry);
		}

		public static EntityList<TaxiPassRedeemers> GetActiveRedeemers(TaxiPassRedeemerAccounts pTaxiPassRedeemerAccount) {
			RdbQuery qry = new RdbQuery(typeof(TaxiPassRedeemers), TaxiPassRedeemerAccountIDEntityColumn, EntityQueryOp.EQ, pTaxiPassRedeemerAccount.TaxiPassRedeemerAccountID);
			qry.AddClause(TaxiPassRedeemers.ActiveEntityColumn, EntityQueryOp.EQ, true);
			qry.AddOrderBy(TaxiPassRedeemers.NameEntityColumn);
			return pTaxiPassRedeemerAccount.PersistenceManager.GetEntities<TaxiPassRedeemers>(qry);
		}

		public static TaxiPassRedeemers GetRedeemerByPhone(PersistenceManager pPM, string pPhone) {
			RdbQuery qry = new RdbQuery(typeof(TaxiPassRedeemers), TaxiPassRedeemers.PhoneEntityColumn, EntityQueryOp.EQ, pPhone);
			return pPM.GetEntity<TaxiPassRedeemers>(qry);
		}

		public static EntityList<TaxiPassRedeemers> GetRedeemersByPhone(PersistenceManager pPM, string pPhone) {
			RdbQuery qry = new RdbQuery(typeof(TaxiPassRedeemers), TaxiPassRedeemers.PhoneEntityColumn, EntityQueryOp.EQ, pPhone);
			return pPM.GetEntities<TaxiPassRedeemers>(qry);
		}

		public static TaxiPassRedeemers GetRedeemerByIDOrPhone(TaxiPassRedeemerAccounts pTaxiPassRedeemerAccounts, string pID) {
			RdbQuery qry = new RdbQuery(typeof(TaxiPassRedeemers), TaxiPassRedeemers.TaxiPassRedeemerAccountIDEntityColumn, EntityQueryOp.EQ, pTaxiPassRedeemerAccounts.TaxiPassRedeemerAccountID);
			qry.AddClause(TaxiPassRedeemers.ActiveEntityColumn, EntityQueryOp.EQ, true);
			qry.AddClause(TaxiPassRedeemers.PhoneEntityColumn, EntityQueryOp.EQ, pID);
			//qry.AddClause(TaxiPassRedeemers.TaxiPassRedeemerIDEntityColumn, EntityQueryOp.EQ, Convert.ToInt64(pID));
			qry.AddClause(TaxiPassRedeemers.EmployeeNoEntityColumn, EntityQueryOp.EQ, pID);
			qry.AddOperator(EntityBooleanOp.Or);
			//qry.AddOperator(EntityBooleanOp.Or);
			return pTaxiPassRedeemerAccounts.PersistenceManager.GetEntity<TaxiPassRedeemers>(qry);
		}


		public static EntityList<TaxiPassRedeemers> GetShiftManagers(TaxiPassRedeemerAccounts pTaxiPassRedeemerAccounts) {
			RdbQuery qry = new RdbQuery(typeof(TaxiPassRedeemers), TaxiPassRedeemerAccountIDEntityColumn, EntityQueryOp.EQ, pTaxiPassRedeemerAccounts.TaxiPassRedeemerAccountID);
			qry.AddClause(ShiftManagerEntityColumn, EntityQueryOp.EQ, true);
			qry.AddOrderBy(NameEntityColumn);
			return pTaxiPassRedeemerAccounts.PersistenceManager.GetEntities<TaxiPassRedeemers>(qry);
		}


		public static EntityList<TaxiPassRedeemers> GetRedeemersThatRedeemedVouchers(TaxiPassRedeemerAccounts pRedeemerAccount, DateTime pDate) {
			StringBuilder sql = new StringBuilder();
			sql.Append("execute usp_TaxiPassRedeemers_GetRedeemersThatRedeemedVouchers ");
			sql.Append(pRedeemerAccount.TaxiPassRedeemerAccountID.ToString("F0"));
			sql.Append(", '");
			sql.Append(pDate.ToString("MM/dd/yyyy"));
			sql.Append("'");

			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(TaxiPassRedeemers), sql.ToString());
			return pRedeemerAccount.PersistenceManager.GetEntities<TaxiPassRedeemers>(qry);
		}





		// *****************************************************************************
		// returns true if either the Redeemer or his Account is set to PayBeforeMatch
		// *****************************************************************************
		public bool IsPayBeforeMatch() {
			return (this.PayBeforeMatch || this.TaxiPassRedeemerAccounts.PayBeforeMatch);
		}


		// *****************************************************************************
		// returns true if either the Redeemer or his Account is set to PayBeforeMatch
		// *****************************************************************************
		public bool CanUseRedemptionApp() {
			return (this.UseRedemptionApp || this.TaxiPassRedeemerAccounts.UseRedemptionApp);
		}


		//******************************************************************************
		//get redeemers current shift if any
		//******************************************************************************
		public RedeemerShift CurrentShift {
			get {
				return RedeemerShift.GetLastShiftByRedeemer(this);
			}
		}

	}

	#region EntityPropertyDescriptors.TaxiPassRedeemersPropertyDescriptor
	////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
	////    For example: 
	////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
	//public static partial class EntityPropertyDescriptors {
	//	public partial class TaxiPassRedeemersPropertyDescriptor : BaseEntityPropertyDescriptor {
	//    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
	//    public AdaptedPropertyDescriptor Age {
	//      get { return this.Get("Age"); }
	//    }
	//  }
	//} 
	#endregion

}
