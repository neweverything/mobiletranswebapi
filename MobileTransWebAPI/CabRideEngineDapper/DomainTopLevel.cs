﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace CabRideEngineDapper {
    public partial class DomainTopLevel {

        public static List<DomainTopLevel> GetAll(SqlConnection pConn) {
            string sql = @"SELECT * FROM DomainTopLevel
                            ORDER BY TopLevel";

            return pConn.Query<DomainTopLevel>(sql).ToList();
        }

        public static DateTime GetLastUpdated(SqlConnection pConn) {
            string sql = @"SELECT MAX(ModifiedDate) FROM DomainTopLevel";

            return pConn.Get<DateTime>(sql);
        }
    }
}
