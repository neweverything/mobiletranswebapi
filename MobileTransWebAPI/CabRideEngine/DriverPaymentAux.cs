using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Text;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

namespace CabRideEngine {
	/// <summary>
	/// The developer class representing the DriverPaymentAux business object class.
	/// </summary>
	/// <remarks>
	/// <para>Place all application-specific business logic in this class.</para>
	/// <para>
	/// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
	/// and will not overwrite changes.
	/// </para>
	/// <para>
	/// This class inherits from the generated class, its companion in the pair of classes 
	/// defining a business object. Properties in the generated class may be overridden in 
	/// order to change the behavior or add business logic.
	/// </para>
	/// <para>
	/// This class is the final class in the inheritance chain for this business object. 
	/// It should not be inherited.
	/// </para>
	/// </remarks>
	[Serializable]
	public sealed partial class DriverPaymentAux : DriverPaymentAuxDataRow {

		#region Constructors -- DO NOT MODIFY

		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new';
		// it is also required if the class is visible from a web service.
		// DO NOT REMOVE **
		private DriverPaymentAux() : this(null) { }

		/// <summary>
		/// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
		/// </summary>
		/// <param name="pRowBuilder"></param>
		public DriverPaymentAux(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static DriverPaymentAux Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      DriverPaymentAux aDriverPaymentAux = pManager.CreateEntity<DriverPaymentAux>();
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aDriverPaymentAux, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aDriverPaymentAux.AddToManager();
		//      return aDriverPaymentAux;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		// Add additional logic to your business object here...
		#region Standard Entity logic to create/audit/validate the entity
		public static DriverPaymentAux Create(DriverPayments pDriverPayment) {
			DriverPaymentAux oDriverPaymentAux = null;
			try {
				StringBuilder sql = new StringBuilder();
				sql.AppendFormat("usp_DriverPaymentAuxInsert @DriverPaymentID = {0}", pDriverPayment.DriverPaymentID);
				sql.AppendFormat(", @ModifiedBy = '{0}'", pDriverPayment.ModifiedBy);
				PassthruRdbQuery qry = new PassthruRdbQuery(typeof(DriverPaymentAux), sql.ToString());
				//oDriverPaymentAux = pDriverPayment.PersistenceManager.GetEntity<DriverPaymentAux>(qry);
				pDriverPayment.PersistenceManager.GetEntity<DriverPaymentAux>(qry);
				oDriverPaymentAux = pDriverPayment.DriverPaymentAux;

				/*
				rec = pDriverPayment.PersistenceManager.CreateEntity<DriverPaymentAux>();
				rec.DriverPaymentID = pDriverPayment.DriverPaymentID;
				rec.WriteOff = false;
				rec.DoNotPay = false;
				*/

			} catch (Exception ex) {
				throw ex;
			}
			return oDriverPaymentAux;
		}

		#endregion

		public static DriverPaymentAux Get(DriverPayments pPayment) {
			RdbQuery qry = new RdbQuery(typeof(DriverPaymentAux), DriverPaymentAux.DriverPaymentIDEntityColumn, EntityQueryOp.EQ, pPayment.DriverPaymentID);
			DriverPaymentAux rec = pPayment.PersistenceManager.GetEntity<DriverPaymentAux>(qry, QueryStrategy.Normal);
			return rec;
		}

		public static DriverPaymentAux GetOrCreate(DriverPayments pPayment) {
			RdbQuery qry = new RdbQuery(typeof(DriverPaymentAux), DriverPaymentAux.DriverPaymentIDEntityColumn, EntityQueryOp.EQ, pPayment.DriverPaymentID);

			DriverPaymentAux rec = pPayment.PersistenceManager.GetEntity<DriverPaymentAux>(qry, QueryStrategy.Normal);
			if (rec.IsNullEntity) {
				rec = Create(pPayment);
			}
			return rec;
		}

		// ***********************************************************
		// pChargeBackPaymentID = The Chargeback DriverPaymentID
		// ***********************************************************
		public static string GetRefundInfo(PersistenceManager pPM, long pChargeBackPaymentID) {
			string sGetRefundInfo = "";
			StringBuilder sql = new StringBuilder();
			sql.Append("SELECT  'Source Voucher: ' + ChargeBackSource.TransNo as RefundInfo");
			sql.Append(" FROM    DriverPaymentAux ");
			sql.Append(" inner join DriverPayments AS ChargeBackSource	ON DriverPaymentAux.ChargeBackDriverPaymentID = ChargeBackSource.DriverPaymentID ");
			sql.AppendFormat(" where DriverPaymentAux.DriverPaymentID = {0}", pChargeBackPaymentID.ToString());
			Entity[] oCards = DynamicEntities.DynamicDBCall(pPM, sql.ToString());
			if (oCards.Length > 0){
				sGetRefundInfo = oCards[0]["RefundInfo"].ToString();
			}

			return sGetRefundInfo;

		}

	}

	public enum DriverPaymentAuxFields {
		SignaturePath,
		StoreForward,
		Recharged,
		VTSRowID,
		WriteOff,
		FailedCharged,
		FailType
	}

	#region EntityPropertyDescriptors.DriverPaymentAuxPropertyDescriptor
	////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
	////    For example: 
	////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
	//public static partial class EntityPropertyDescriptors {
	//	public partial class DriverPaymentAuxPropertyDescriptor : BaseEntityPropertyDescriptor {
	//    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
	//    public AdaptedPropertyDescriptor Age {
	//      get { return this.Get("Age"); }
	//    }
	//  }
	//} 
	#endregion

}