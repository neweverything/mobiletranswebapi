﻿using CabRideEngineDapper.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Dapper;
using TaxiPassCommon;
using TaxiPassCommon.Cryptography;
using System.Data.SqlClient;

namespace CabRideEngineDapper {
	public partial class User {

		public static User GetUser(string pUserName) {
			using (var conn = SqlHelper.OpenSqlConnection()) {
				return GetUser(conn, pUserName);
			}
		}

		public static User GetUser(SqlConnection pConn, string pUserName) {
			string sql = "SELECT * FROM [User] WHERE UserName = @UserName AND IsLocked = 0";
			return pConn.Query<User>(sql, new { UserName = pUserName }).DefaultIfEmpty(User.GetNullEntity()).FirstOrDefault();
		}


		public void SetPassword(string pPassword) {
			if (pPassword.Length > 3) {
				UserPassword = HashPassword(pPassword + Salt);
			} else {
				throw new Exception("Password must be at least 4 characters");
			}
		}

		public static string HashPassword(String sPassword) {
			return CryptoFns.MD5HashUTF16ToString(sPassword);
		}

		[Editable(false)]
		public string FullName {
			get {
				return FirstName + " " + LastName;
			}
		}
	}
}
