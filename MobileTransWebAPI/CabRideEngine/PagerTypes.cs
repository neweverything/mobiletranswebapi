﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;
using IdeaBlade.Rdb;

using Csla.Validation;
using System.Text;

using TaxiPassCommon;

namespace CabRideEngine {
    /// <summary>
    /// The developer class representing the PagerTypes business object class.
    /// </summary>
    /// <remarks>
    /// <para>Place all application-specific business logic in this class.</para>
    /// <para>
    /// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
    /// and will not overwrite changes.
    /// </para>
    /// <para>
    /// This class inherits from the generated class, its companion in the pair of classes 
    /// defining a business object. Properties in the generated class may be overridden in 
    /// order to change the behavior or add business logic.
    /// </para>
    /// <para>
    /// This class is the final class in the inheritance chain for this business object. 
    /// It should not be inherited.
    /// </para>
    /// </remarks>
    [Serializable]
    public sealed partial class PagerTypes : PagerTypesDataRow {

        #region Constructors -- DO NOT MODIFY

        // Do not create constructors for this class
        // Developers cannot create instances of this class with the "new" keyword
        // You should write a static Create method instead; see the "Suggested Customization" region
        // Please review the documentation to learn about entity creation and inheritance

        // This private constructor prevents accidental instance creation via 'new';
        // it is also required if the class is visible from a web service.
        // DO NOT REMOVE **
        private PagerTypes() : this(null) {
        }

        /// <summary>
        /// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
        /// </summary>
        /// <param name="pRowBuilder"></param>
        public PagerTypes(DataRowBuilder pRowBuilder)
            : base(pRowBuilder) {
        }

        #endregion

        #region Suggested Customizations

        //    // Use this factory method template to create new instances of this class
        //    public static PagerTypes Create(PersistenceManager pManager,
        //      ... your creation parameters here ...) { 
        //
        //      PagerTypes aPagerTypes = pManager.CreateEntity<PagerTypes>();
        //
        //      // if this object type requires a unique id and you have implemented
        //      // the IIdGenerator interface implement the following line
        //      pManager.GenerateId(aPagerTypes, // add id column here //);
        //
        //      // Add custom code here
        //
        //      aPagerTypes.AddToManager();
        //      return aPagerTypes;
        //    }

        //    // Implement this method if you want your object to sort in a specific order
        //    public override int CompareTo(Object pObject) {
        //    }

        //    // Implement this method to customize the null object version of this class
        //    protected override void UpdateNullEntity() {
        //    }

        #endregion

        // Add additional logic to your business object here...
        public static PagerTypes Create(PersistenceManager pManager) {

            PagerTypes oType = (PagerTypes)pManager.CreateEntity(typeof(PagerTypes));

            pManager.GenerateId(oType, PagerTypes.PagerIDEntityColumn);
            oType.AddToManager();
            return oType;
        }


        protected override void AddRules(RuleList pList) {
            PropertyRequiredRule.AddToList(pList, PagerTypes.PagerTypeEntityColumn.ColumnName);
            PropertyRequiredRule.AddToList(pList, PagerTypes.UrlEntityColumn.ColumnName);
            pList.Add(IsPagerTypeUnique, PagerTypes.PagerTypeEntityColumn.ColumnName);
        }

        private bool IsPagerTypeUnique(object pTarget, RuleArgs e) {
            PagerTypes oType = (PagerTypes)pTarget;

            RdbQuery oQry = new RdbQuery(typeof(PagerTypes), PagerTypes.PagerTypeEntityColumn, EntityQueryOp.EQ, oType.PagerType);
            oQry.AddClause(PagerTypes.PagerIDEntityColumn, EntityQueryOp.NE, oType.PagerID);
            EntityList<PagerTypes> oPagerTypes = this.PersistenceManager.GetEntities<PagerTypes>(oQry);
            if (oPagerTypes.Count > 0) {
                e.Description = "Pager Type must be unique!";
                return false;
            }
            return true;
        }

        [BindingBrowsable(false)]
        public override string EntityInstanceDisplayName {
            get {
                return PagerType;
            }
        }

        protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
            if (IsDeserializing) {
                return;
            }
            base.OnColumnChanging(pArgs);
            DoAudit(pArgs);
        }

        private void DoAudit(DataColumnChangeEventArgs pArgs) {
            if (!this.PersistenceManager.IsClient) {
                return;
            }
            if (this.RowState == DataRowState.Added) {
                return;
            }
            if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
                AuditRecord = PagerTypesAudit.Create(this.PersistenceManager, this);
            }
        }

        public static EntityList<PagerTypes> GetAllPagerTypes(PersistenceManager pPM) {
			return GetData(pPM, "", "");
        }

        public static PagerTypes GetPagerType(PersistenceManager pPM, string pPagerType) {
			EntityList<PagerTypes> oPagerTypes = GetData(pPM, pPagerType, "");
			if (oPagerTypes.Count == 0) {
				return pPM.GetNullEntity<PagerTypes>();
			} else {
				return oPagerTypes[0];
			}

        }

        public static PagerTypes GetPagerType(PersistenceManager pPM, long pPagerTypeID) {
			EntityList<PagerTypes> oPagerTypes = GetData(pPM, "", pPagerTypeID.ToString());
			if (oPagerTypes.Count == 0) {
				return pPM.GetNullEntity<PagerTypes>();
			} else {
				return oPagerTypes[0];
			}
        }

		// ****************************************************
		// Call the acutal "Get" proc
		// ****************************************************
		private static EntityList<PagerTypes> GetData(PersistenceManager pPM, string pPagerType, string pPagerTypeID) {
			string sDelimiter = "";
			int iArgCount = 0;

			StringBuilder sql = new StringBuilder();
			
			sql.Append("usp_PagerTypesGet ");
			if (!pPagerType.IsNullOrEmpty()) {
				iArgCount++;
				sql.AppendFormat(" @PagerType = '{0}'", pPagerType);
			}
			if (!pPagerTypeID.IsNullOrEmpty()) {
				if (iArgCount > 0) {
					sDelimiter = ", ";
				}
				sql.AppendFormat("{0} @PagerID = {1}", sDelimiter, pPagerTypeID);
				iArgCount++;
			}
			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(PagerTypes), sql.ToString());
			EntityList<PagerTypes> oPagerTypes = pPM.GetEntities<PagerTypes>(qry);
			return oPagerTypes;
		}

    }

    #region EntityPropertyDescriptors.PagerTypesPropertyDescriptor
    ////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
    ////    For example: 
    ////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
    //public static partial class EntityPropertyDescriptors {
    //	public partial class PagerTypesPropertyDescriptor : BaseEntityPropertyDescriptor {
    //    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
    //    public AdaptedPropertyDescriptor Age {
    //      get { return this.Get("Age"); }
    //    }
    //  }
    //} 
    #endregion

}
