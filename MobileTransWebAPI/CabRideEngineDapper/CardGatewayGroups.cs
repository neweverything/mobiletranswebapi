﻿using CabRideEngineDapper.Utils;
using Dapper;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using TaxiPassCommon;

namespace CabRideEngineDapper {
    public partial class CardGatewayGroups {

        public static CardGatewayGroups GetGroup(long pCardGroupID) {
            using (var conn = SqlHelper.OpenSqlConnection()) {
                return conn.Get<CardGatewayGroups>(pCardGroupID);
            }
        }

        public static List<CardGatewayGroups> GetGateways(SqlConnection pConn) {
            string sql = @"SELECT * FROM CardGatewayGroups
                            WHERE CardGatewayGroupsID != 0
                            ORDER BY GroupName";

            return pConn.Query<CardGatewayGroups>(sql).ToList();
        }

        public static CardGatewayGroups GetDefaultAccount() {
            using (var conn = SqlHelper.OpenSqlConnection()) {
                string sql = "SELECT * FROM dbo.CardGatewayGroups WHERE DefaultGroup = 1";
                return conn.Get<CardGatewayGroups>(sql, null);
            }
        }

        [Editable(false)]
        public string CardGatewayGroupsIDAsString {
            get {
                return CardGatewayGroupsID.ToString();
            }
            set {
                CardGatewayGroupsID = value.ToInt();
            }
        }
    }
}
