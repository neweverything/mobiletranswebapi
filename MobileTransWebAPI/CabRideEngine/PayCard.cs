﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

using TaxiPassCommon;

namespace CabRideEngine {
	public class PayCard {

		private TaxiPassCommon.Banking.TransCard.CardInfo mTCCardInfo = new TaxiPassCommon.Banking.TransCard.CardInfo(); // used for web service calls
		private TransCardDefaults mTransCardDefaults;


		// *************************************************************************************
		// Given an Admin No/Card No, return the first card number and ID found
		// *************************************************************************************
		public void SetCardNumber(PersistenceManager pPM, string pCard) {
			List<Entity> oRet = new List<Entity>();
			mTCCardInfo.CardNumber = pCard;
			mTCCardInfo.AdminNo = "";
			mTCCardInfo.EmployeeID = "";
			try {
				StringBuilder sql = new StringBuilder();
				sql.Append(" SELECT DISTINCT TransCardNumber ");
				sql.Append(" , TransCardAdminNo ");
				sql.Append(" , ID ");
				sql.Append(", FirstName ");
				sql.Append(", LastName ");
				sql.Append(" FROM (SELECT TransCardNumber ");
				sql.Append(" , TransCardAdminNo ");
				sql.Append(" , AffiliateDriverID ID ");
				sql.Append(", CASE WHEN LEN(NAME) = 0 THEN '' ");
				sql.Append(" WHEN PatIndex('% %',Name) > 0 then REPLACE(SUBSTRING(Name, 1, PatIndex('% %',Name) - 1), ',','') ");
				sql.Append(" else '' end FirstName ");
				sql.Append(" , CASE WHEN LEN(NAME) = 0 THEN '' ");
				sql.Append(" WHEN PatIndex('% %',Name) > 0 then SUBSTRING(Name, PatIndex('% %',Name) + 1, LEN(NAME) - PatIndex('% %',Name) + 1 ) ");
				sql.Append(" ELSE Name ");
				sql.Append(" END LastName ");
				sql.Append(" FROM affiliateDrivers ");
				sql.Append(" WHERE TransCardAdminNo IS NOT NULL ");
				sql.Append(" AND LEN(TransCardAdminNo) > 0 ");

				sql.Append(" UNION ");

				sql.Append(" SELECT RedeemerPayCards.TransCardNumber ");
				sql.Append(" , RedeemerPayCards.TransCardAdminNo ");
				sql.Append(" , RedeemerPayCards.TaxiPassRedeemerAccountID ");
				sql.Append(", '' ");
				sql.Append(", CompanyName ");
				sql.Append(" FROM RedeemerPayCards ");
				sql.Append(" INNER JOIN TaxiPassRedeemerAccounts ON RedeemerPayCards.TaxiPassRedeemerAccountID = TaxiPassRedeemerAccounts.TaxiPassRedeemerAccountID ");
				sql.Append(" WHERE RedeemerPayCards.TransCardAdminNo IS NOT NULL ");
				sql.Append(" AND LEN(RedeemerPayCards.TransCardAdminNo) > 0 ");

				sql.Append(" UNION ");

				sql.Append(" SELECT TransCardNumber ");
				sql.Append(" , TransCardAdminNo ");
				sql.Append(" , TaxiPassRedeemerAccountID ");
				sql.Append(", '' ");
				sql.Append(", CompanyName ");
				sql.Append(" FROM TaxiPassRedeemerAccounts ");
				sql.Append(" WHERE TransCardAdminNo IS NOT NULL ");
				sql.Append(" AND LEN(TransCardAdminNo) > 0 ");
				sql.Append(" ) SubSelect ");


				Entity[] oCards = DynamicEntities.DynamicDBCall(pPM, sql.ToString());
				oRet = (from p in oCards
						where p["TransCardNumber"].ToString().DecryptIceKey() == pCard
						|| p["TransCardAdminNo"].ToString().Contains(pCard)
						select p).ToList();

				if (oRet.Count > 0){
					mTCCardInfo.AdminNo = oRet[0]["TransCardAdminNo"].ToString(); ;
					mTCCardInfo.CardNumber = oRet[0]["TransCardNumber"].ToString().DecryptIceKey(); ;
					mTCCardInfo.EmployeeID = oRet[0]["ID"].ToString();
					mTCCardInfo.FirstName = oRet[0]["FirstName"].ToString(); 
					mTCCardInfo.LastName = oRet[0]["LastName"].ToString();
				}
				mTransCardDefaults = TransCardDefaults.GetDefaults(pPM, mTCCardInfo.CardNumber);
				if (mTransCardDefaults == null) {
					mTransCardDefaults = TransCardDefaults.GetDefaultTransCard(pPM);
				}
				mTCCardInfo.ComData = mTransCardDefaults.ComData;
				mTCCardInfo.TransCardDefaultID = mTransCardDefaults.TransCardDefaultID;

			} catch (Exception ex) {
				Console.WriteLine(ex.Message);
			}

		}


		// *************************************************************************************
		// public access to private properties
		// *************************************************************************************
		public TaxiPassCommon.Banking.TransCard.CardInfo TCCardInfo {
			get {
				return mTCCardInfo;
			}
			set {
				mTCCardInfo = value;
			}
		}

		public TransCardDefaults TCDefaults{
			get {
				return mTransCardDefaults;
			}
			set {
				mTransCardDefaults = value;
			}
		}

		public bool RemoveFundsFromCard{
			get {
				return mTCCardInfo.RemoveFundsFromCard;
			}
			set {
				mTCCardInfo.RemoveFundsFromCard = value;
			}
		}

	}
}
