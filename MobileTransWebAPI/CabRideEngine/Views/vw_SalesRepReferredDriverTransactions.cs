using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Text;


using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

namespace CabRideEngine {
  /// <summary>
  /// The developer class representing the vw_SalesRepReferredDriverTransactions business object class.
  /// </summary>
  /// <remarks>
  /// <para>Place all application-specific business logic in this class.</para>
  /// <para>
  /// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
  /// and will not overwrite changes.
  /// </para>
  /// <para>
  /// This class inherits from the generated class, its companion in the pair of classes 
  /// defining a business object. Properties in the generated class may be overridden in 
  /// order to change the behavior or add business logic.
  /// </para>
  /// <para>
  /// This class is the final class in the inheritance chain for this business object. 
  /// It should not be inherited.
  /// </para>
  /// </remarks>
  [Serializable]
  public sealed partial class vw_SalesRepReferredDriverTransactions : vw_SalesRepReferredDriverTransactionsDataRow {
  
    #region Constructors -- DO NOT MODIFY
    
    // Do not create constructors for this class
    // Developers cannot create instances of this class with the "new" keyword
    // You should write a static Create method instead; see the "Suggested Customization" region
    // Please review the documentation to learn about entity creation and inheritance

    // This private constructor prevents accidental instance creation via 'new';
    // it is also required if the class is visible from a web service.
    // DO NOT REMOVE **
    private vw_SalesRepReferredDriverTransactions() : this(null) {}

    /// <summary>
    /// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
    /// </summary>
    /// <param name="pRowBuilder"></param>
    public vw_SalesRepReferredDriverTransactions(DataRowBuilder pRowBuilder) 
      : base(pRowBuilder) {
    }

    #endregion
    
    #region Suggested Customizations
  
//    // Use this factory method template to create new instances of this class
//    public static vw_SalesRepReferredDriverTransactions Create(PersistenceManager pManager,
//      ... your creation parameters here ...) { 
//
//      vw_SalesRepReferredDriverTransactions avw_SalesRepReferredDriverTransactions = pManager.CreateEntity<vw_SalesRepReferredDriverTransactions>();
//
//      // if this object type requires a unique id and you have implemented
//      // the IIdGenerator interface implement the following line
//      pManager.GenerateId(avw_SalesRepReferredDriverTransactions, // add id column here //);
//
//      // Add custom code here
//
//      avw_SalesRepReferredDriverTransactions.AddToManager();
//      return avw_SalesRepReferredDriverTransactions;
//    }

//    // Implement this method if you want your object to sort in a specific order
//    public override int CompareTo(Object pObject) {
//    }

//    // Implement this method to customize the null object version of this class
//    protected override void UpdateNullEntity() {
//    }

    #endregion
    
    // Add additional logic to your business object here...
	// ******************************************************************************************************************
	// Given a Referring Affiliate Driver ID, return all the transactions their referr-ees have had within a date range.
	// Used by TP1800CabRide.Admin.DriverReferralsBySalesRep.aspx
	// ******************************************************************************************************************
	public static EntityList<vw_SalesRepReferredDriverTransactions> GetSalesRepReferrals(PersistenceManager pPM, long pReferringAffDriverID, DateTime pStart, DateTime pEnd) {
		StringBuilder sql = new StringBuilder();
		sql.Append("Select * from vw_SalesRepReferredDriverTransactions ");
		sql.Append("where ChargeDate >= '");
		sql.Append(pStart.ToString());

		sql.Append("' and ChargeDate <= '");
		sql.Append(pEnd.ToString());

		sql.Append("' and AffiliateDriverID in ");
		sql.Append("(SELECT  ReferredDriverID ");
		sql.Append(" FROM AffiliateDriverReferrals ");
		sql.Append(" where AffiliateDriverID =  ");
		sql.Append(pReferringAffDriverID.ToString());
		sql.Append(") order by ChargeDate, DriverName");

		PassthruRdbQuery qry = new PassthruRdbQuery(typeof(vw_SalesRepReferredDriverTransactions), sql.ToString());
		return pPM.GetEntities<vw_SalesRepReferredDriverTransactions>(qry);

	}

  }
  
  #region EntityPropertyDescriptors.vw_SalesRepReferredDriverTransactionsPropertyDescriptor
  ////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
  ////    For example: 
  ////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
  //public static partial class EntityPropertyDescriptors {
	//	public partial class vw_SalesRepReferredDriverTransactionsPropertyDescriptor : AdaptedPropertyDescriptor {
  //    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
  //    public AdaptedPropertyDescriptor Age {
  //      get { return this.Get("Age"); }
  //    }
  //  }
  //} 
  #endregion
  
}