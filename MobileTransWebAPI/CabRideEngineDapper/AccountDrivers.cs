﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Dapper;



namespace CabRideEngineDapper {
	public partial class AccountDrivers {
		public static AccountDrivers Create(long pAccountID, long pDriverID) {
			AccountDrivers rec = new AccountDrivers();
			rec.AccountID = pAccountID;
			rec.DriverID = pDriverID;
			return rec;
		}


		public long Insert(SqlConnection pConn, string pModifiedBy) {
			CreatedDate = DateTime.Now;
			CreatedBy = pModifiedBy;
			ModifiedDate = CreatedDate;
			ModifiedBy = pModifiedBy;
			return (long)pConn.Insert(this);
		}

		public bool Update(SqlConnection pConn, string pModifiedBy) {
			ModifiedDate = DateTime.Now;
			ModifiedBy = pModifiedBy;
			return pConn.Update(this) > 0;
		}

	}
}
