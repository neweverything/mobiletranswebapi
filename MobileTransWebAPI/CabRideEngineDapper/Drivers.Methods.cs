﻿using CabRideEngineDapper.Utils;
using System;
using System.Data.SqlClient;
using TaxiPassCommon;

namespace CabRideEngineDapper {
    public partial class Drivers {


        public static Drivers Create(SqlConnection pConn) {
            Drivers oDriver = null;
            try {
                oDriver = DriversTable.Create();
                SystemDefaults oDefaults = SystemDefaults.GetDefaults(pConn);

                // Using the IdeaBlade Id Generation technique
                oDriver.Salt = StringUtils.GenerateSalt();
                oDriver.Country = Countries.GetDefaultCountry(pConn).Country;
                //oDriver.BankCountry = oDriver.Country;
                //oDriver.MaxTrxPerDay = oDefaults.DriverMaxTransactions;
                oDriver.MinPUTime = oDefaults.MinPickUpTime;

            } catch (Exception ex) {
                throw ex;
            }
            return oDriver;
        }


        public static Drivers CreateAndroidBackSeat(AffiliateDrivers pAffDriver, string pAndroidCellNo) {
            using (var conn = SqlHelper.OpenSqlConnection()) {
                Drivers driver = Drivers.GetNullEntity();
                if (!pAndroidCellNo.Equals("CELL")) {
                    driver = Drivers.GetDriverByBoostPhone(conn, pAndroidCellNo);
                }
                if (driver.IsNullEntity) {
                    driver = Create();
                }
                driver.ByPassValidation = true;

                if (pAffDriver.AffiliateID > 0) {
                    driver.AffiliateID = pAffDriver.AffiliateID;
                }
                driver.Name = pAffDriver.Name;
                driver.Street = pAffDriver.Street;
                driver.Suite = pAffDriver.Suite;
                driver.City = pAffDriver.City;
                driver.State = pAffDriver.State;
                driver.Country = pAffDriver.Country;
                driver.ZIPCode = pAffDriver.ZIPCode;
                driver.Cell = pAffDriver.Cell;
                driver.BoostPhone = pAndroidCellNo;
                driver.EMail = pAffDriver.EMail;
                if (driver.EMail.IsNullOrEmpty()) {
                    driver.EMail = driver.Cell;
                }
                driver.Active = pAffDriver.Active;
                driver.SerialNo = "Android Signup";
                driver.InBackSeat = false;
                driver.HHFareFirst = true;
                driver.HHTripLog = false;
                driver.TrackSwipeCounts = true;

                driver.CardDataEntryType = ((int)DataEntryTypeEnum.SwipeAndManual).ToString();
                driver.TaxiMeter = ((int)TaxiMeterUserEntryType.UserEntryTypeEnum.VoucherMenuFare).ToString();
                driver.PrinterType = ((int)HandHeldPrinterTypes.HandHeldPrinterTypesEnum.HomeATM).ToString();
                try {
                    driver.Save("");
                } catch (Exception) {

                }

                return driver;
            }
        }

        public static Drivers CreateAndroidDriver(AffiliateDrivers pAffDriver, string pAndroidCellNo) {
            Drivers driver = CreateAndroidBackSeat(pAffDriver, pAndroidCellNo);
            driver.InBackSeat = false;
            return driver;
        }


    }
}
