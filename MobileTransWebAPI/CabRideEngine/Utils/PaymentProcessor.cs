﻿using IdeaBlade.Persistence;
using IdeaBlade.Util;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using TaxiPassCommon;
using TaxiPassCommon.Banking;

namespace CabRideEngine.Utils {
	public static class PaymentProcessor {

		public static BindableList<TTechTransaction> ProcessTransactons(SystemDefaults oDefaults
																		, EntityList<DriverPayments> payList
																		, bool pChargeBack
																		, short pBatchNo
																		, bool pTransCardOnly
																		, DateTime pEndDate) {
			return ProcessTransactons(oDefaults, payList, pChargeBack, pBatchNo, pTransCardOnly, pEndDate, false, false);
		}



		public static BindableList<TTechTransaction> ProcessTransactons(SystemDefaults pSysDefaults
																		, EntityList<DriverPayments> pDriverPayments
																		, bool pChargeBack
																		, short pBatchNo
																		, bool pTransCardOnly
																		, DateTime pEndDate
																		, bool pIsBofAPayment
																		, bool pIgnorePayCard) {
			BindableList<TTechTransaction> oTrans = new BindableList<TTechTransaction>();
			foreach (DriverPayments oPay in pDriverPayments) {
				if (pDriverPayments.IndexOf(oPay) % 50 == 0) {
					Console.WriteLine("        {0} of {1}", pDriverPayments.IndexOf(oPay), pDriverPayments.Count);
				} else {
					//Console.Write(".");
				}

				//Console.WriteLine(string.Format("ProcessTransactons: {0} of {1} => {2}", payList.IndexOf(oPay), payList.Count, oPay.TransNo));
				if (!oPay.DriverPaid) {
					continue;
				}
				//if (oPay.DoNotPay) {
				//    continue;
				//}
				if (pChargeBack) {
					// 3/26/12 CA Carmel's Debit line (what they owe us) only includes Driver Fees
					if (!oPay.ChargeBack && !oPay.Affiliate.AffiliateDriverDefaults.ACHDriverFeeAsDebit) {
						//if (!oPay.ChargeBack && !oPay.TTechDriverFeeChargeBack) {
						continue;
					}
				} else {
					// 3/26/12 CA Carmel rolls chargebacks into the Credit line (Transactions - chargebacks = Total Payout)
					if (!oPay.Affiliate.AffiliateDriverDefaults.ACHDriverFeeAsDebit && oPay.ChargeBack) {
						// if (oPay.ChargeBack){
						continue;
					}
				}

				ACHDetail achRec = ACHDetail.GetTransactionByDriverPaymentID(oPay.PersistenceManager, oPay.DriverPaymentID, QueryStrategy.DataSourceThenCache);
				if (!achRec.IsNullEntity) {
					continue;
				}
				//hg remove
				//if (!oPay.TransNo.Equals("mrhvma", StringComparison.CurrentCultureIgnoreCase)) {
				//    continue;
				//}
				//if (oPay.DriverPaymentID != 104106) {
				//    continue;
				//}
				//if (oPay.TaxiPassRedeemerID != 224) {
				//    continue;
				//}

				// is this a pending match payment
				bool pendingMatch = (!oPay.DriverPaymentsPendingMatches.IsNullEntity);
				bool forceACH = oPay.TaxiPassRedeemers.TaxiPassRedeemerAccounts.AlwaysACH;
				//bool cardFunded = false;

				if (forceACH) {
					pendingMatch = false;
				}

				// Pending matches
				#region pendingMatch

				if (pendingMatch) {
					TaxiPassRedeemerAccounts acct = oPay.DriverPaymentsPendingMatches.TaxiPassRedeemers.TaxiPassRedeemerAccounts;

					if (!acct.BankAccountNumber.IsNullOrEmpty() && oPay.TaxiPassRedeemerID.HasValue) {
						//need to add code to fund bank account
						if (!oPay.DriverPaymentVoucherImages.IsNullEntity && oPay.DriverPaymentVoucherImages.Matched && oPay.MatchDate <= pEndDate) {
							pendingMatch = false;
						}

					} // else if (!acct.TransCardNumber.IsNullOrEmpty()
					  //            && oPay.ACHDetail.IsNullEntity
					  //            && oPay.DriverPaymentVoucherImages.Matched
					  //            && acct.BofAAccountInfo.IsNullOrEmpty()) { // && oPay.TaxiPassRedeemerID.HasValue) {

					//    if (pIgnorePayCard) {
					//        Console.WriteLine("Skipping PayCard: " + oPay.TaxiPassRedeemers.TaxiPassRedeemerAccounts.TransCardCardNumber.Right(5));
					//        continue;
					//    }
					//    Console.WriteLine("Processing PayCard: " + acct.TransCardNumber.Right(5));

					//    try {
					//        TransCard.LOAD_CARD_FUNDED_RET results = new TransCard.LOAD_CARD_FUNDED_RET();

					//        if (!oPay.ChargeBack) {
					//            results = oPay.FundPayCard(acct);
					//            string msg = oPay.DriverTotalPaid.ToString("C") + " has been added to card *" + acct.TransCardNumber.Right(5);
					//            if (oPay.RedemptionFee.GetValueOrDefault(0) > 0) {
					//                msg += " (includes " + oPay.RedemptionFee.Value.ToString("C") + " redemption fee)";
					//            }
					//            msg += " for Voucher " + oPay.TransNo + ".";

					//            if (!results.CURRENT_CARD_BALANCE.IsNullOrEmpty()) {
					//                msg += "\nNew card bal: " + results.CURRENT_CARD_BALANCE;
					//            }
					//            Console.WriteLine(msg);
					//            cardFunded = true;
					//        }

					//        oPay.Save();
					//        continue;

					//    } catch (Exception ex) {

					//        try {
					//            StringBuilder errorMsg = new StringBuilder();
					//            errorMsg.Append("Redeemer: ");
					//            errorMsg.Append(acct.CompanyName);
					//            errorMsg.Append(" Admin: ");
					//            errorMsg.Append(acct.TransCardAdminNo);
					//            errorMsg.Append("<br />");
					//            errorMsg.Append(ex.Message);
					//            errorMsg.Append("<br/>");
					//            errorMsg.Append(ex.StackTrace);
					//            EMailMessage mail = new EMailMessage("email@taxipass.com", "PayCard Error", errorMsg.ToString());
					//            mail.UseWebServiceToSendEmail = Properties.Settings.Default.EMailViaWebService;
					//            mail.SendMail("hani@taxipass.com");
					//        } catch (Exception) {
					//        }
					//    }
					//}
				}
				#endregion pendingMatch

				//is this a transcard payment
				//#region PayCard
				//TaxiPassRedeemerAccounts redeemerAcct = oPay.TaxiPassRedeemers.TaxiPassRedeemerAccounts;

				//if (!cardFunded
				//    && !oPay.AffiliateDrivers.TransCardNumber.IsNullOrEmpty()
				//    && !oPay.DriverPaidCash.PaidInCash
				//    && (!oPay.TaxiPassRedeemerID.HasValue
				//        && !pendingMatch)
				//    ||
				//    (!redeemerAcct.IsNullEntity
				//        && redeemerAcct.BankAccountNumber.IsNullOrEmpty()
				//        && redeemerAcct.TransCardNumber.IsNullOrEmpty()
				//        && !redeemerAcct.BofAAccountInfo.IsNullOrEmpty()
				//        && !pendingMatch && redeemerAcct.PayDriverPayCard
				//        && !oPay.AffiliateDrivers.TransCardNumber.IsNullOrEmpty()
				//        && !oPay.DriverPaidCash.PaidInCash)
				//    ) {

				//    if (pIgnorePayCard) {
				//        Console.WriteLine("Skipping PayCard: " + oPay.TaxiPassRedeemers.TaxiPassRedeemerAccounts.TransCardCardNumber.Right(5));
				//        continue;
				//    }

				//    Console.WriteLine("Processing PayCard: " + oPay.AffiliateDrivers.TransCardNumber.Right(5));
				//    try {

				//        TransCard.LOAD_CARD_FUNDED_RET results = new TransCard.LOAD_CARD_FUNDED_RET();
				//        if (oPay.ChargeBack) {
				//            results = oPay.ChargePayCard(oPay.AffiliateDrivers);
				//        } else {
				//            results = oPay.FundPayCard(oPay.AffiliateDrivers);
				//            StringBuilder msg = new StringBuilder();
				//            msg.Append(oPay.DriverTotalPaid.ToString("C"));
				//            msg.Append(" has been added to card *");
				//            msg.Append(oPay.AffiliateDrivers.TransCardNumber.Right(5));
				//            if (oPay.RedemptionFee.GetValueOrDefault(0) > 0) {
				//                msg.Append(" (");
				//                msg.Append(oPay.DriverTotal.ToString("C"));
				//                msg.Append(" - ");
				//                msg.Append(oPay.RedemptionFee.Value.ToString("C"));
				//                msg.Append(" redemption fee)");
				//            }
				//            msg.Append(" for Voucher ");
				//            msg.Append(oPay.TransNo);
				//            msg.Append(".");

				//            if (!results.CURRENT_CARD_BALANCE.IsNullOrEmpty()) {
				//                msg.Append("\nNew card bal: ");
				//                msg.Append(results.CURRENT_CARD_BALANCE);
				//            }
				//            Console.WriteLine(msg.ToString());
				//            cardFunded = true;
				//        }
				//        oPay.Save();
				//        continue;

				//    } catch (Exception ex) {
				//        try {
				//            StringBuilder errorMsg = new StringBuilder();
				//            errorMsg.Append("Driver ");
				//            errorMsg.Append(oPay.AffiliateDrivers.Name);
				//            errorMsg.Append(" Admin: ");
				//            errorMsg.Append(oPay.AffiliateDrivers.TransCardAdminNo);
				//            errorMsg.Append("<br />");
				//            errorMsg.Append(ex.Message);
				//            errorMsg.Append("<br/>");
				//            errorMsg.Append(ex.StackTrace);
				//            EMailMessage mail = new EMailMessage("email@taxipass.com", "PayCard Error", errorMsg.ToString());
				//            mail.UseWebServiceToSendEmail = Properties.Settings.Default.EMailViaWebService;
				//            mail.SendMail("hani@taxipass.com");
				//        } catch (Exception) {
				//        }
				//    }
				//}
				//#endregion  PayCard

				//does redeemer have paycard
				//#region redeemer Paycard validations
				//if (!cardFunded
				//    && oPay.TaxiPassRedeemerID.HasValue
				//    && !oPay.TaxiPassRedeemers.TaxiPassRedeemerAccounts.TransCardCardNumber.IsNullOrEmpty()
				//    && !pendingMatch
				//    && oPay.TaxiPassRedeemers.TaxiPassRedeemerAccounts.BofAAccountInfo.IsNullOrEmpty()) {

				//    if (pIgnorePayCard) {
				//        Console.WriteLine("Skipping PayCard: " + oPay.TaxiPassRedeemers.TaxiPassRedeemerAccounts.TransCardCardNumber.Right(5));
				//        continue;
				//    }
				//    Console.WriteLine("Processing PayCard: " + oPay.TaxiPassRedeemers.TaxiPassRedeemerAccounts.TransCardCardNumber.Right(5));
				//    try {
				//        TransCard.LOAD_CARD_FUNDED_RET results = new TransCard.LOAD_CARD_FUNDED_RET();
				//        if (oPay.ChargeBack) {
				//            //results = oPay.ChargePayCard(oPay.AffiliateDrivers);
				//        } else {
				//            results = oPay.FundPayCard(oPay.TaxiPassRedeemers.TaxiPassRedeemerAccounts);

				//            StringBuilder msg = new StringBuilder();
				//            msg.Append(oPay.RedeemerTotalPaid.ToString("C"));
				//            msg.Append(" has been added to card *");
				//            msg.Append(oPay.TaxiPassRedeemers.TaxiPassRedeemerAccounts.TransCardCardNumber.Right(5));

				//            if (oPay.RedemptionFee.GetValueOrDefault(0) > 0) {
				//                msg.Append(" (includes ");
				//                msg.Append(oPay.RedemptionFee.Value.ToString("C"));
				//                msg.Append(" redemption fee)");
				//            }
				//            msg.Append(" for Voucher ");
				//            msg.Append(oPay.TransNo);
				//            msg.Append(".");

				//            if (!results.CURRENT_CARD_BALANCE.IsNullOrEmpty()) {
				//                msg.Append("\nNew card bal: ");
				//                msg.Append(results.CURRENT_CARD_BALANCE);
				//            }
				//            Console.WriteLine(msg);
				//            cardFunded = true;
				//        }
				//        oPay.Save();
				//    } catch (Exception) {
				//    }
				//    continue;
				//}
				//#endregion redeemer Paycard validations

				if (pendingMatch && oPay.DriverPaymentVoucherImages.Matched && pIsBofAPayment) {
					pendingMatch = false;
				}

				if (pTransCardOnly || pendingMatch) {
					continue;
				}

				oPay.ACHProcessError = "";

				#region Bank Setup Validations
				if (oPay.ACHDetail.ACHBatchNumber == null && oPay.DriverPaid) {

					if (oPay.TaxiPassRedeemers.TaxiPassRedeemerAccounts.MarkAsACHd) {
						ACHDetail.Create(oPay);
						TimeZoneConverter convert = new TimeZoneConverter();
						oPay.ACHDetail.ACHPaidDate = DateTime.Now;
						oPay.ACHDetail.ACHPaidTo = oPay.TaxiPassRedeemers.TaxiPassRedeemerAccounts.CompanyName;
						oPay.ACHDetail.Save();

					} else {

						TTechTransaction oTran = oPay.PaymentToTTechTransaction(pBatchNo, pIsBofAPayment);

						//if (!pIsBofAPayment && (oTran == null || string.IsNullOrEmpty(oTran.MerchantID) || string.IsNullOrEmpty(oTran.Account) || string.IsNullOrEmpty(oTran.ABANumber))) {
						if (!pIsBofAPayment && (oTran == null
												|| string.IsNullOrEmpty(oTran.Account)
												|| string.IsNullOrEmpty(oTran.ABANumber))) {
							if (oTran == null) {
								oPay.ACHProcessError = "No banking info setup to make payments. ";
							} else {
								//if (string.IsNullOrEmpty(oTran.MerchantID)) {
								//    oPay.ACHProcessError = "Payment Account not setup for the redeemer. ";
								//}
								if (string.IsNullOrEmpty(oTran.Account)) {
									oPay.ACHProcessError += "Bank Account Number not setup for the redeemer. ";
								}
								if (string.IsNullOrEmpty(oTran.ABANumber)) {
									oPay.ACHProcessError += "ABA/Rounting not setup for the redeemer. ";
								}

							}
						} else {
							// 4/22/2013 - Don't payout Aff Drivers with RedeemerStopPayment
							if (oPay.AffiliateDriverID > 0) {
								try {
									if (oPay.AffiliateDrivers.RedeemerStopPayment && !oPay.TaxiPassRedeemerID.HasValue && !oPay.ChargeBack) {
										oPay.ACHProcessError += "Driver on RedeemerStopPayment";
									}
								} catch (Exception) {
								}
							}
							if (oPay.ACHProcessError.IsNullOrEmpty()) {
								if (oTrans.FirstOrDefault(p => p.TranID == oPay.DriverPaymentID) == null) {
									oTrans.Add(oTran);
								}
							}
						}
					}
				}
				#endregion Bank Setup Validations
			}
			return oTrans; // CreateFile(oDefaults, ref mailMsg, oTrans, rec);
		}

		public static string NachaFileName { get; set; }

		public static List<string> CreateFile(string pFilePath
									, SystemDefaults pDefaults
									, BindableList<TTechTransaction> pTrans
									, BindableList<TTechTransaction> pChargeBackTrans
									, bool pEstimateOnly
									, CabRideEngineDapper.NachaDefaults pNachaDefault) {
			return CreateFile(pFilePath, pDefaults, pTrans, pChargeBackTrans, pEstimateOnly, true, pNachaDefault);
		}

		// **************************************************************************
		// Generate the nacha file
		// **************************************************************************
		public static List<string> CreateFile(string pFilePath
											, SystemDefaults pDefaults
											, BindableList<TTechTransaction> pTrans
											, BindableList<TTechTransaction> pChargeBackTrans
											, bool pEstimateOnly
											, bool pACHNormalRun
											, CabRideEngineDapper.NachaDefaults pNachaDefault) {



			List<string> mailMsg = new List<string>();
			List<string> msgList = new List<string>();
			PersistenceManager oPM = pDefaults.PersistenceManager;

			try {
				//NachaDefaults nachaDefault = NachaDefaults.GetDefaults(oPM);

				//bool findBatch = true;

				string sWork = DateTime.Today.ToString("yyyyMMdd");
				int iBatchNo = DriverPayments.GetNextBatchNumber(oPM, sWork, pNachaDefault.Origin);



				DateTime fundingDate = TaxiPassCommon.DateUtils.CalcFundingDate(DateTime.Today, DateTime.Today, pACHNormalRun);


				NachaGenerator nachaGen = new NachaGenerator(pNachaDefault.BankName
															, pNachaDefault.Destination
															, pNachaDefault.DestinationName
															, pNachaDefault.Origin
															, pNachaDefault.OriginName
															, pNachaDefault.RoutingNumber
															, null
															, iBatchNo
															, "CabRide"
															, fundingDate);
				try {
					List<TTechTransaction> nachaTransList = new List<TTechTransaction>(pTrans);
					//if (pNachaDefault.NachaType != "BankUnited") {
					//	nachaTransList.AddRange(pChargeBackTrans);
					//}
					string fileName = "";
					fileName = nachaGen.CreateFile(pFilePath
													, nachaTransList
													, pNachaDefault.NachaType.ToEnum<NachaGenerator.eBankNames>() //.Citi
													, !pEstimateOnly);

					//BindableList<TTechTransaction> oFTPTrans = nachaGen.GetUpdatedTransactions();
					//QueryStrategy oStrategy = mPM.DefaultQueryStrategy;
					// mPM.DefaultQueryStrategy = QueryStrategy.Normal;
					List<string> missingRecords = new List<string>();

					TotalTrans totals = new TotalTrans();
					Console.WriteLine("{0} Transactions", nachaTransList.Count.ToString());
					foreach (TTechTransaction ftpTran in nachaTransList) {
						if (ftpTran.Affiliate.Contains("xxTransCard")) {
							continue;
						}

						DriverPayments myPay = DriverPayments.GetPayment(oPM, ftpTran.TranID);
						if (myPay.IsNullEntity) {
							missingRecords.Add(ftpTran.TranID.ToString());
						} else {
							if (nachaTransList.IndexOf(ftpTran) % 50 == 0) {
								Console.WriteLine("        {0} of {1}", nachaTransList.IndexOf(ftpTran), nachaTransList.Count);
							} else {
								//Console.Write(".");
							}

							if (myPay.ACHDetail.IsNullEntity) {

								// DEBIT - money they owe us
								if (ftpTran.TranType.Equals("DEBIT", StringComparison.CurrentCultureIgnoreCase)) {
									if (ftpTran.ClassCode == TaxiPassCommon.Banking.NachaGenerator.StandardEntryTypes.PPD.ToString()) {
										if (ftpTran.Affiliate.StartsWith("Redeemer")) {
											totals.PersonalDebit += myPay.RedeemerTotalPaid;
										} else {
											totals.PersonalDebit += myPay.DriverTotalPaid; // ftpTran.Amount;
										}

									} else {

										if (ftpTran.Affiliate.StartsWith("Redeemer")) {
											totals.CorpDebit += myPay.RedeemerTotalPaid;
										} else {
											if (myPay.Affiliate.AffiliateDriverDefaults.ACHDriverFeeAsDebit) {
												// 3/7/12 CA Carmel only has DriverFees as Debits
												totals.CorpDebit += myPay.DriverFee.GetValueOrDefault(0);

											} else {
												totals.CorpDebit += myPay.DriverTotalPaid; // ftpTran.Amount;
											}
										}
									}

								} else {

									// CREDITS - Money we owe them
									if (ftpTran.ClassCode == TaxiPassCommon.Banking.NachaGenerator.StandardEntryTypes.PPD.ToString()) {
										if (ftpTran.Affiliate.StartsWith("Redeemer")) {
											totals.PersonalCredit += myPay.RedeemerTotalPaid; // ftpTran.Amount;
										} else {
											totals.PersonalCredit += myPay.DriverTotalPaid; // ftpTran.Amount;
										}
									} else {
										if (ftpTran.Affiliate.StartsWith("Redeemer")) {
											if (ftpTran.WireTransfer) {
												totals.WireTransfer += myPay.RedeemerTotalPaid;
											} else {
												totals.CorpCredit += myPay.RedeemerTotalPaid;
											}
										} else {

											if (myPay.Affiliate.AffiliateDriverDefaults.ACHDriverFeeAsDebit) {
												// 3/26/12 CA Carmel's DriverFees are wrapped in the CorpCredit
												if (ftpTran.WireTransfer) {
													totals.WireTransfer += myPay.DriverTotal;
												} else {
													totals.CorpCredit += myPay.DriverTotal;
												}
											} else {
												if (ftpTran.WireTransfer) {
													totals.WireTransfer += myPay.DriverTotalPaid;
												} else {
													totals.CorpCredit += myPay.DriverTotalPaid; // ftpTran.Amount;
												}
											}
										}
									}
								}
							}
							if (!pEstimateOnly) {
								Console.WriteLine("Processing " + myPay.TransNo);
								ACHDetail ach = ACHDetail.GetTransactionByDriverPaymentID(oPM, myPay.DriverPaymentID, QueryStrategy.DataSourceThenCache);
								if (ach.IsNullEntity) {
									ach = ACHDetail.Create(myPay);
								}
								TimeZoneConverter convert = new TimeZoneConverter();
								ach.ACHPaidDate = DateTime.Now;
								string batchNo = DateTime.Today.ToString("yyyyMMdd") + iBatchNo.ToString("F0").PadLeft(2, '0') + pNachaDefault.Origin;
								if (batchNo.Length > 19) {
									batchNo = batchNo.Left(19);
								}
								ach.ACHBatchNumber = Convert.ToInt64(batchNo); // ftpTran.BatchNo);
								myPay.ACHProcessError = "";
								ach.ACHPaidTo = ftpTran.PaidTo;
								ach.ACHBankAccountNumber = ftpTran.Account;
								ach.ACHBankCode = ftpTran.ABANumber;
								ach.ACHBankHolder = ftpTran.CustomerName;
								ach.TTechBatchNumber = ftpTran.Phone1;
								try {
									myPay.Save();
								} catch (Exception ex) {
									string body = String.Format("Error saving Voucher {0}\n{1}", myPay.TransNo, ex.Message);
									var emailCred = CabRideEngineDapper.SystemDefaultsDict.GetEmailCreds(CabRideEngineDapper.Enums.EMailReportFrom.Receipt);
									EMailMessage mail = new EMailMessage(emailCred, "Create ACH File Error", body);
									mail.SendMail(Properties.Settings.Default.EMailErrorsTo);
								}
							}
							//foreach (var payOut in nachaGen.PayOutList) {
							//    if (payOut.BankAccountType.IsNullOrEmpty()) {
							//        continue;
							//    }

							//}
						}
					}


					if (nachaGen.PayOutList.Count > 0) {
						if (pEstimateOnly) {
							mailMsg.Add("<b>Estimated Payout for " + DateTime.Today.ToString("MM/dd/yyyy") + "</b>");
						} else {
							mailMsg.Add("<b>Created Nacha File: " + System.IO.Path.GetFileNameWithoutExtension(fileName) + "</b>");
							NachaFileName = fileName;
						}
						List<TTechGenerator.PayOut> sortedPayOut = (from p in nachaGen.PayOutList
																	where p.Name != "Total of Payouts"
																	orderby p.Affiliate
																	select p).ToList();

						//foreach (TTechGenerator.PayOut rec in oGen.PayOutList) {
						string lastAffiliate = "";
						using (var conn = CabRideEngineDapper.Utils.SqlHelper.OpenSqlConnection()) {
							DateTime now = DateTime.Now;
							foreach (TTechGenerator.PayOut rec in sortedPayOut) {
								//Address to Payout file here
								if (!pEstimateOnly) {
									CabRideEngineDapper.AchPayOut achPayout = null;
									try {
										achPayout = CabRideEngineDapper.AchPayOut.Create();
										achPayout.ACHDate = now;
										achPayout.Affiliate = rec.Affiliate;
										achPayout.Amount = rec.Amount;
										achPayout.BankAccountNo = rec.BankAccountNo;
										achPayout.BankAccountType = rec.BankAccountType;
										achPayout.BankAccountHolder = rec.BankHolderName;
										achPayout.BankRouting = rec.BankRouting;
										achPayout.BatchNo = rec.BatchNo;
										achPayout.LaneID = rec.LaneID;
										achPayout.Name = rec.Name;
										achPayout.PaidTo = rec.PaidTo;
										achPayout.PaymentType = rec.PaymentType;
										achPayout.TransType = rec.TransType;
										achPayout.WireTransfer = rec.WireTransfer;
										achPayout.Phone = rec.Phone;
										//achPayout.BatchNo = Convert.ToInt64(DateTime.Today.ToString("yyyyMMdd") + iBatchNo.ToString("F0").PadLeft(2, '0') + pNachaDefault.Origin);
										achPayout.Save();
									} catch (Exception ex) {
										string body = String.Format("Error saving AchPayout\n\n{0}\n{1}", JsonConvert.SerializeObject(achPayout), ex.Message);
										var emailCred = CabRideEngineDapper.SystemDefaultsDict.GetEmailCreds(CabRideEngineDapper.Enums.EMailReportFrom.Receipt);
										EMailMessage mail = new EMailMessage(emailCred, "Create ACH File Error", body);
										mail.SendMail(Properties.Settings.Default.EMailErrorsTo);
									}
								}
								if (lastAffiliate != rec.Affiliate) {
									mailMsg.Add("");
									mailMsg.Add("Payment for " + rec.Affiliate);
									lastAffiliate = rec.Affiliate;
								}
								mailMsg.Add(rec.TransType + " " + rec.Amount.ToString("C") + " for " + rec.Name);
							}
						}

						sortedPayOut = (from p in nachaGen.PayOutList
										where p.Name == "Total of Payouts"
										orderby p.Affiliate
										select p).ToList();

						foreach (TTechGenerator.PayOut rec in sortedPayOut) {
							if (rec.Name.IsNullOrEmpty()) {
								rec.Name = rec.Affiliate;
							}
							msgList.Add(rec.TransType + " " + rec.Amount.ToString("C") + " for " + rec.Name);
						}
						msgList.Add("");
						msgList.Add("SubTotal for Corp Credit: " + totals.CorpCredit.ToString("C"));
						msgList.Add("SubTotal for Corp Debit: " + totals.CorpDebit.ToString("C"));
						msgList.Add("SubTotal for Personal Credit: " + totals.PersonalCredit.ToString("C"));
						msgList.Add("SubTotal for Personal Debit: " + totals.PersonalDebit.ToString("C"));
						msgList.Add("SubTotal for Wire Transfer: " + totals.WireTransfer.ToString("C"));
						msgList.Add("");
						msgList.Add("");
						msgList.AddRange(mailMsg);

					}

					//if (pNachaDefault.NachaType == "BankUnited") {
					//	nachaTransList = new List<TTechTransaction>(pChargeBackTrans);
					//	nachaGen = new NachaGenerator(pNachaDefault.BankName
					//									, pNachaDefault.Destination
					//									, pNachaDefault.DestinationName
					//									, pNachaDefault.Origin
					//									, pNachaDefault.OriginName
					//									, pNachaDefault.RoutingNumber
					//									, null
					//									, iBatchNo + 1
					//									, "CabRide"
					//									, fundingDate);
					//	fileName = "";
					//	fileName = nachaGen.CreateFile(pFilePath
					//									, nachaTransList
					//									, pNachaDefault.NachaType.ToEnum<NachaGenerator.eBankNames>() //.Citi
					//									, !pEstimateOnly);

					//	//BindableList<TTechTransaction> oFTPTrans = nachaGen.GetUpdatedTransactions();
					//	//QueryStrategy oStrategy = mPM.DefaultQueryStrategy;
					//	// mPM.DefaultQueryStrategy = QueryStrategy.Normal;
					//	missingRecords = new List<string>();

					//	totals = new TotalTrans();
					//	Console.WriteLine("{0} Transactions", nachaTransList.Count.ToString());
					//	foreach (TTechTransaction ftpTran in nachaTransList) {
					//		if (ftpTran.Affiliate.Contains("xxTransCard")) {
					//			continue;
					//		}

					//		DriverPayments myPay = DriverPayments.GetPayment(oPM, ftpTran.TranID);
					//		if (myPay.IsNullEntity) {
					//			missingRecords.Add(ftpTran.TranID.ToString());
					//		} else {
					//			if (nachaTransList.IndexOf(ftpTran) % 50 == 0) {
					//				Console.WriteLine("        {0} of {1}", nachaTransList.IndexOf(ftpTran), nachaTransList.Count);
					//			} else {
					//				//Console.Write(".");
					//			}

					//			if (myPay.ACHDetail.IsNullEntity) {

					//				// DEBIT - money they owe us
					//				if (ftpTran.TranType.Equals("DEBIT", StringComparison.CurrentCultureIgnoreCase)) {
					//					if (ftpTran.ClassCode == TaxiPassCommon.Banking.NachaGenerator.StandardEntryTypes.PPD.ToString()) {
					//						if (ftpTran.Affiliate.StartsWith("Redeemer")) {
					//							totals.PersonalDebit += myPay.RedeemerTotalPaid;
					//						} else {
					//							totals.PersonalDebit += myPay.DriverTotalPaid; // ftpTran.Amount;
					//						}

					//					} else {

					//						if (ftpTran.Affiliate.StartsWith("Redeemer")) {
					//							totals.CorpDebit += myPay.RedeemerTotalPaid;
					//						} else {
					//							if (myPay.Affiliate.AffiliateDriverDefaults.ACHDriverFeeAsDebit) {
					//								// 3/7/12 CA Carmel only has DriverFees as Debits
					//								totals.CorpDebit += myPay.DriverFee.GetValueOrDefault(0);

					//							} else {
					//								totals.CorpDebit += myPay.DriverTotalPaid; // ftpTran.Amount;
					//							}
					//						}
					//					}

					//				} else {

					//					// CREDITS - Money we owe them
					//					if (ftpTran.ClassCode == TaxiPassCommon.Banking.NachaGenerator.StandardEntryTypes.PPD.ToString()) {
					//						if (ftpTran.Affiliate.StartsWith("Redeemer")) {
					//							totals.PersonalCredit += myPay.RedeemerTotalPaid; // ftpTran.Amount;
					//						} else {
					//							totals.PersonalCredit += myPay.DriverTotalPaid; // ftpTran.Amount;
					//						}
					//					} else {
					//						if (ftpTran.Affiliate.StartsWith("Redeemer")) {
					//							if (ftpTran.WireTransfer) {
					//								totals.WireTransfer += myPay.RedeemerTotalPaid;
					//							} else {
					//								totals.CorpCredit += myPay.RedeemerTotalPaid;
					//							}
					//						} else {

					//							if (myPay.Affiliate.AffiliateDriverDefaults.ACHDriverFeeAsDebit) {
					//								// 3/26/12 CA Carmel's DriverFees are wrapped in the CorpCredit
					//								if (ftpTran.WireTransfer) {
					//									totals.WireTransfer += myPay.DriverTotal;
					//								} else {
					//									totals.CorpCredit += myPay.DriverTotal;
					//								}
					//							} else {
					//								if (ftpTran.WireTransfer) {
					//									totals.WireTransfer += myPay.DriverTotalPaid;
					//								} else {
					//									totals.CorpCredit += myPay.DriverTotalPaid; // ftpTran.Amount;
					//								}
					//							}
					//						}
					//					}
					//				}
					//			}
					//			if (!pEstimateOnly) {
					//				Console.WriteLine("Processing " + myPay.TransNo);
					//				ACHDetail ach = ACHDetail.GetTransactionByDriverPaymentID(oPM, myPay.DriverPaymentID, QueryStrategy.DataSourceThenCache);
					//				if (ach.IsNullEntity) {
					//					ach = ACHDetail.Create(myPay);
					//				}
					//				TimeZoneConverter convert = new TimeZoneConverter();
					//				ach.ACHPaidDate = DateTime.Now;
					//				string batchNo = DateTime.Today.ToString("yyyyMMdd") + iBatchNo.ToString("F0").PadLeft(2, '0') + pNachaDefault.Origin;
					//				if (batchNo.Length > 19) {
					//					batchNo = batchNo.Left(19);
					//				}
					//				ach.ACHBatchNumber = Convert.ToInt64(batchNo); // ftpTran.BatchNo);
					//				myPay.ACHProcessError = "";
					//				ach.ACHPaidTo = ftpTran.PaidTo;
					//				ach.ACHBankAccountNumber = ftpTran.Account;
					//				ach.ACHBankCode = ftpTran.ABANumber;
					//				ach.ACHBankHolder = ftpTran.CustomerName;
					//				ach.TTechBatchNumber = ftpTran.Phone1;
					//				try {
					//					myPay.Save();
					//				} catch (Exception ex) {
					//					string body = String.Format("Error saving Voucher {0}\n{1}", myPay.TransNo, ex.Message);
					//					var emailCred = CabRideEngineDapper.SystemDefaultsDict.GetEmailCreds(CabRideEngineDapper.Enums.EMailReportFrom.Receipt);
					//					EMailMessage mail = new EMailMessage(emailCred, "Create ACH File Error", body);
					//					mail.SendMail("hani@taxipass.com");
					//				}
					//			}
					//			//foreach (var payOut in nachaGen.PayOutList) {
					//			//    if (payOut.BankAccountType.IsNullOrEmpty()) {
					//			//        continue;
					//			//    }

					//			//}
					//		}
					//	}


					//	if (nachaGen.PayOutList.Count > 0) {
					//		if (pEstimateOnly) {
					//			mailMsg.Add("<b>Estimated Payout for " + DateTime.Today.ToString("MM/dd/yyyy") + "</b>");
					//		} else {
					//			mailMsg.Add("<b>Created Nacha File: " + System.IO.Path.GetFileNameWithoutExtension(fileName) + "</b>");
					//			NachaFileName += "|" + fileName;
					//		}
					//		List<TTechGenerator.PayOut> sortedPayOut = (from p in nachaGen.PayOutList
					//													where p.Name != "Total of Payouts"
					//													orderby p.Affiliate
					//													select p).ToList();

					//		//foreach (TTechGenerator.PayOut rec in oGen.PayOutList) {
					//		string lastAffiliate = "";
					//		using (var conn = CabRideEngineDapper.Utils.SqlHelper.OpenSqlConnection()) {
					//			DateTime now = DateTime.Now;
					//			foreach (TTechGenerator.PayOut rec in sortedPayOut) {
					//				//Address to Payout file here
					//				if (!pEstimateOnly) {
					//					CabRideEngineDapper.AchPayOut achPayout = null;
					//					try {
					//						achPayout = CabRideEngineDapper.AchPayOut.Create();
					//						achPayout.ACHDate = now;
					//						achPayout.Affiliate = rec.Affiliate;
					//						achPayout.Amount = rec.Amount;
					//						achPayout.BankAccountNo = rec.BankAccountNo;
					//						achPayout.BankAccountType = rec.BankAccountType;
					//						achPayout.BankAccountHolder = rec.BankHolderName;
					//						achPayout.BankRouting = rec.BankRouting;
					//						achPayout.BatchNo = rec.BatchNo;
					//						achPayout.LaneID = rec.LaneID;
					//						achPayout.Name = rec.Name;
					//						achPayout.PaidTo = rec.PaidTo;
					//						achPayout.PaymentType = rec.PaymentType;
					//						achPayout.TransType = rec.TransType;
					//						achPayout.WireTransfer = rec.WireTransfer;
					//						achPayout.Phone = rec.Phone;
					//						//achPayout.BatchNo = Convert.ToInt64(DateTime.Today.ToString("yyyyMMdd") + iBatchNo.ToString("F0").PadLeft(2, '0') + pNachaDefault.Origin);
					//						achPayout.Save();
					//					} catch (Exception ex) {
					//						string body = String.Format("Error saving AchPayout\n\n{0}\n{1}", JsonConvert.SerializeObject(achPayout), ex.Message);
					//						var emailCred = CabRideEngineDapper.SystemDefaultsDict.GetEmailCreds(CabRideEngineDapper.Enums.EMailReportFrom.Receipt);
					//						EMailMessage mail = new EMailMessage(emailCred, "Create ACH File Error", body);
					//						mail.SendMail("hani@taxipass.com");
					//					}
					//				}
					//				if (lastAffiliate != rec.Affiliate) {
					//					mailMsg.Add("");
					//					mailMsg.Add("Payment for " + rec.Affiliate);
					//					lastAffiliate = rec.Affiliate;
					//				}
					//				mailMsg.Add(rec.TransType + " " + rec.Amount.ToString("C") + " for " + rec.Name);
					//			}
					//		}

					//		sortedPayOut = (from p in nachaGen.PayOutList
					//						where p.Name == "Total of Payouts"
					//						orderby p.Affiliate
					//						select p).ToList();

					//		foreach (TTechGenerator.PayOut rec in sortedPayOut) {
					//			if (rec.Name.IsNullOrEmpty()) {
					//				rec.Name = rec.Affiliate;
					//			}
					//			msgList.Add(rec.TransType + " " + rec.Amount.ToString("C") + " for " + rec.Name);
					//		}
					//		msgList.Add("");
					//		msgList.Add("SubTotal for Corp Credit: " + totals.CorpCredit.ToString("C"));
					//		msgList.Add("SubTotal for Corp Debit: " + totals.CorpDebit.ToString("C"));
					//		msgList.Add("SubTotal for Personal Credit: " + totals.PersonalCredit.ToString("C"));
					//		msgList.Add("SubTotal for Personal Debit: " + totals.PersonalDebit.ToString("C"));
					//		msgList.Add("SubTotal for Wire Transfer: " + totals.WireTransfer.ToString("C"));
					//		msgList.Add("");
					//		msgList.Add("");
					//		msgList.AddRange(mailMsg);

					//	}

					//}
				} catch (Exception ex) {
					Console.WriteLine(ex.Message);
				}

			} catch (Exception) {

			}

			return msgList;
		}

		private class TotalTrans {
			public decimal PersonalCredit { get; set; }
			public decimal PersonalDebit { get; set; }
			public decimal CorpCredit { get; set; }
			public decimal CorpDebit { get; set; }
			public decimal WireTransfer { get; set; }
		}
	}
}

