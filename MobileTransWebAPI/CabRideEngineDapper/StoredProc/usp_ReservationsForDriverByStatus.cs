﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

using Dapper;

namespace CabRideEngineDapper {
	public partial class usp_ReservationsForDriverByStatus {

		public long ReservationID { get; set; }
		public string AccountNo { get; set; }
		public string ReserveNo { get; set; }
		public long CustomerID { get; set; }
		public long DriverID { get; set; }
		public string Name { get; set; }
		public string CellPhone { get; set; }
		public string WorkPhone { get; set; }
		public string EMail { get; set; }
		public DateTime PickUpDate { get; set; }
		public DateTime LocalPickUpDate { get; set; }
		public DateTime UTCPUDateTime { get; set; }
		public DateTime? DropOffTime { get; set; }
		public string Status { get; set; }
		public bool Cancelled { get; set; }
		public string CardType { get; set; }
		public string CardNumber { get; set; }
		public decimal ExtraTip { get; set; }
		public string Notes { get; set; }
		public string ExternalReferenceID { get; set; }
		public string Customer { get; set; }
		public string CustomerPhone { get; set; }
		public int CustomerRating { get; set; }
		public bool SelectedDriver { get; set; }
		public bool RepeatCustomer { get; set; }
		public DateTime? FirstPing { get; set; }
		public int Lifespan { get; set; }
		public int CustomerRides { get; set; }
		public string PickupType { get; set; }
		public string PickupPlaceName { get; set; }
		public string PickupStreet { get; set; }
		public string PickupSuite { get; set; }
		public string PickupCity { get; set; }
		public string PickupState { get; set; }
		public string PickupZIPCode { get; set; }
		public string PickupCountry { get; set; }
		public string PickupRoutingType { get; set; }
		public string PickupAirport { get; set; }
		public string PickupAirline { get; set; }
		public string PickupFlightNo { get; set; }
		public string PickupTerminal { get; set; }
		public DateTime? PickupFlightTime { get; set; }
		public decimal PickupLatitude { get; set; }
		public decimal PickupLongitude { get; set; }

		public string DropOffType { get; set; }
		public string DropOffPlaceName { get; set; }
		public string DropOffStreet { get; set; }
		public string DropOffSuite { get; set; }
		public string DropOffCity { get; set; }
		public string DropOffState { get; set; }
		//public string PickupZIPCode { get; set; }
		public string DropOffCountry { get; set; }
		public string DropOffRoutingType { get; set; }
		public string DropOffAirport { get; set; }
		public string DropOffAirline { get; set; }
		public string DropOffFlightNo { get; set; }
		public string DropOffTerminal { get; set; }
		public DateTime? DropOffFlightTime { get; set; }
		public decimal DropOffLatitude { get; set; }
		public decimal DropOffLongitude { get; set; }

		public string PushAlertCode { get; set; }
		public string PushAlert { get; set; }

		public double DistanceToPickup { get; set; }
		public string Location { get; set; }
		public string DropOffLocation { get; set; }
		public string CardAuthToken { get; set; }
		public string CardAuthNumber { get; set; }
		public string CardAuthType { get; set; }

		public long CustomerAuthID { get; set; }
		public int Tier { get; set; }

		public string RunType { get; set; }
		public bool HasGetRide { get; set; }

		public List<usp_ReservationsForDriverByStatus> Execute(SqlConnection pConn, long DriverID, string Status = null, bool ActiveOnly = true, long? ReservationID = null, string ReserveNo = null) {
			DynamicParameters param = new DynamicParameters();
			param.Add("DriverID", DriverID);
			param.Add("Status", Status);
			param.Add("ActiveOnly", ActiveOnly);
			param.Add("ReservationID", ReservationID);
			param.Add("ReserveNo", ReserveNo);
			return pConn.Query<usp_ReservationsForDriverByStatus>("usp_ReservationsForDriverByStatus", param, commandType: CommandType.StoredProcedure).ToList();
		}

	}
}
