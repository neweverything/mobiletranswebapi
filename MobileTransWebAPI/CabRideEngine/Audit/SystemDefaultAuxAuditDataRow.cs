using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Runtime.Serialization;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

namespace CabRideEngine {
 
  #region SystemDefaultAuxAuditDataTable
  /// <summary>
  /// The generated <see cref="T:IdeaBlade.Persistence.EntityTable"/> class  
  /// containing all <see cref="SystemDefaultAuxAudit"/> objects.
  /// </summary>
  /// <remarks>
  /// ****************************************************
  /// <para>** DO NOT MODIFY THIS CLASS - AutoGenerated Code</para>
  /// <para>** Any changes made to this code will be lost</para>
  /// <para>** the next time this code is regenerated.</para>
  /// <para>**</para>
  /// <para>** Generated from:   EntityBaseclass.template</para>
  /// <para>** Last modified:    2011-09-26T13:13:50.2909758-07:00</para>
  /// <para>** Template version: 3.110127</para>
  /// ****************************************************
  /// </remarks>
  [BindingBrowsable(false)]
  [Serializable]
  public partial class SystemDefaultAuxAuditDataTable : IdeaBlade.Persistence.EntityTable, 
     IdeaBlade.Persistence.Rdb.IRdbTable {
  
    /// <summary>
    /// Initializes a new instance of the SystemDefaultAuxAuditDataTable class with no arguments.
    /// </summary>
    public SystemDefaultAuxAuditDataTable() {}

    /// <summary>
    /// Initializes a new instance of the SystemDefaultAuxAuditDataTable class 
    /// with the <see cref="System.Runtime.Serialization.SerializationInfo"/> 
    /// and the <see cref="System.Runtime.Serialization.StreamingContext"/>.
    /// </summary>
    /// <param name="pInfo">The data needed to serialize or deserialize an object.</param>
    /// <param name="pContext">The source and destination of a given serialized stream.</param>
    protected SystemDefaultAuxAuditDataTable(SerializationInfo pInfo, StreamingContext pContext) :
      base(pInfo, pContext) {}

    /// <summary>
    /// Creates a new instance of the <see cref="SystemDefaultAuxAuditDataRow"/>.
    /// Constructs a row from the builder.
    /// <para>** For internal use only. **</para>
    /// </summary>
    /// <param name="pRowBuilder"></param>
    /// <returns>The <see cref="SystemDefaultAuxAuditDataRow"/>.</returns>
    protected override DataRow NewRowFromBuilder(DataRowBuilder pRowBuilder) {
      return new SystemDefaultAuxAudit(pRowBuilder);
    }

    /// <summary>Returns the <see cref="System.Type"/> of <see cref="SystemDefaultAuxAudit"/>.</summary>
    protected override Type GetRowType() {
      return typeof(SystemDefaultAuxAudit);
    }
    
    /// <summary>
    /// Gets the name of the <see cref="T:IdeaBlade.Rdb.RdbKey"/> or 
    /// <see cref="T:IdeaBlade.Persistence.WS.WsKey"/> defined in <b>IdeaBlade.ibconfig</b>. 
    /// </summary>
    /// <remarks>For this <see cref="SystemDefaultAuxAudit"/>, the DataSourceKeyName="Audit".</remarks>
    public override String DataSourceKeyName {
      get { return @"Audit"; }
    }

    #region Column properties
    //**************************************
    //* Column properties
    //**************************************
    /// <summary>Gets the SystemDefaultAuxHistoryID <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn SystemDefaultAuxHistoryIDColumn {
      get {
        if (mSystemDefaultAuxHistoryIDColumn!=null) return mSystemDefaultAuxHistoryIDColumn;
        mSystemDefaultAuxHistoryIDColumn = GetColumn("SystemDefaultAuxHistoryID", true);
        return mSystemDefaultAuxHistoryIDColumn;
      }
    }
    private DataColumn mSystemDefaultAuxHistoryIDColumn;
    
    /// <summary>Gets the SystemDefaultAuxID <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn SystemDefaultAuxIDColumn {
      get {
        if (mSystemDefaultAuxIDColumn!=null) return mSystemDefaultAuxIDColumn;
        mSystemDefaultAuxIDColumn = GetColumn("SystemDefaultAuxID", true);
        return mSystemDefaultAuxIDColumn;
      }
    }
    private DataColumn mSystemDefaultAuxIDColumn;
    
    /// <summary>Gets the SystemDefaultID <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn SystemDefaultIDColumn {
      get {
        if (mSystemDefaultIDColumn!=null) return mSystemDefaultIDColumn;
        mSystemDefaultIDColumn = GetColumn("SystemDefaultID", true);
        return mSystemDefaultIDColumn;
      }
    }
    private DataColumn mSystemDefaultIDColumn;
    
    /// <summary>Gets the Field <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn FieldColumn {
      get {
        if (mFieldColumn!=null) return mFieldColumn;
        mFieldColumn = GetColumn("Field", true);
        return mFieldColumn;
      }
    }
    private DataColumn mFieldColumn;
    
    /// <summary>Gets the Value <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn ValueColumn {
      get {
        if (mValueColumn!=null) return mValueColumn;
        mValueColumn = GetColumn("Value", true);
        return mValueColumn;
      }
    }
    private DataColumn mValueColumn;
    
    /// <summary>Gets the RowVersion <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn RowVersionColumn {
      get {
        if (mRowVersionColumn!=null) return mRowVersionColumn;
        mRowVersionColumn = GetColumn("RowVersion", true);
        return mRowVersionColumn;
      }
    }
    private DataColumn mRowVersionColumn;
    
    /// <summary>Gets the ModifiedBy <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn ModifiedByColumn {
      get {
        if (mModifiedByColumn!=null) return mModifiedByColumn;
        mModifiedByColumn = GetColumn("ModifiedBy", true);
        return mModifiedByColumn;
      }
    }
    private DataColumn mModifiedByColumn;
    
    /// <summary>Gets the ModifiedDate <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn ModifiedDateColumn {
      get {
        if (mModifiedDateColumn!=null) return mModifiedDateColumn;
        mModifiedDateColumn = GetColumn("ModifiedDate", true);
        return mModifiedDateColumn;
      }
    }
    private DataColumn mModifiedDateColumn;
    
    #endregion    

    /// <summary>Creates the schema mapping information for this SystemDefaultAuxAuditDataTable.</summary>
    /// <returns>
    /// The <see cref="T:IdeaBlade.Persistence.TableMappingInfo"/> for this SystemDefaultAuxAuditDataTable.
    /// </returns>
    protected override TableMappingInfo CreateTableMappingInfo() {
      RdbTableMappingInfo mappingInfo = new RdbTableMappingInfo(this);
      mappingInfo.SourceOwnerName = "dbo"; 
      mappingInfo.SourceTableName = "SystemDefaultAuxAudit"; 
      mappingInfo.ConcurrencyColumnName = ""; 
      // ColumnSelectionOption: selectAllColumns
      mappingInfo.SourceColumnNames = "*"; 
   
      DataColumnMappingCollection columnMappings = mappingInfo.TableMapping.ColumnMappings;
      columnMappings.Clear();
      columnMappings.Add("SystemDefaultAuxHistoryID", "SystemDefaultAuxHistoryID");
      columnMappings.Add("SystemDefaultAuxID", "SystemDefaultAuxID");
      columnMappings.Add("SystemDefaultID", "SystemDefaultID");
      columnMappings.Add("Field", "Field");
      columnMappings.Add("Value", "Value");
      columnMappings.Add("RowVersion", "RowVersion");
      columnMappings.Add("ModifiedBy", "ModifiedBy");
      columnMappings.Add("ModifiedDate", "ModifiedDate");
      return mappingInfo;
    }
  
    /// <summary>** For internal use only. **</summary>
    protected override void UpdateTableMappingInfo() {
      base.UpdateTableMappingInfo();
    }

    /// <summary>
    /// Initializes SystemDefaultAuxAudit <see cref="System.Data.DataColumn"/> properties.
    /// </summary>
    public override void InitColumnProperties() {
      base.InitColumnProperties();

      SystemDefaultAuxHistoryIDColumn.Caption = "SystemDefaultAuxHistoryID";
      SystemDefaultAuxIDColumn.Caption = "SystemDefaultAuxID";
      SystemDefaultIDColumn.Caption = "SystemDefaultID";
      FieldColumn.Caption = "Field";
      ValueColumn.Caption = "Value";
      RowVersionColumn.Caption = "RowVersion";
      ModifiedByColumn.Caption = "ModifiedBy";
      ModifiedDateColumn.Caption = "ModifiedDate";
    }
  }
  #endregion
  
  #region SystemDefaultAuxAuditDataRow
  /// <summary>
  /// The generated base class for <see cref="SystemDefaultAuxAudit"/>.
  /// </summary>
  /// <remarks>
  /// <para>
  /// It represents a row of persistable data in the <see cref="SystemDefaultAuxAuditDataTable"/>
  /// and derives from <see cref="T:IdeaBlade.Persistence.Entity"/>.
	/// </para>
  /// ****************************************************
  /// <para>** DO NOT MODIFY THIS CLASS - AutoGenerated Code</para>
  /// <para>** Any changes made to this code will be lost</para>
  /// <para>** the next time this code is regenerated.</para>
  /// <para>**</para>
  /// <para>** Generated from:   EntityBaseclass.template</para>
  /// <para>** Last modified:    2011-09-26T13:13:50.2909758-07:00</para>
  /// <para>** Template version: 3.110127</para>
  /// ****************************************************
  /// </remarks>  
  [Serializable]
  public abstract partial class SystemDefaultAuxAuditDataRow : IdeaBlade.Persistence.Entity, 
     IdeaBlade.Persistence.Rdb.IRdbEntity {
  
    /// <summary>
    /// Initializes a new instance of the SystemDefaultAuxAuditDataRow.
    /// Constructs a row from the builder.
    /// <para>** For internal use only. **</para>
    /// </summary>
    /// <param name="pRowBuilder"></param>
    protected SystemDefaultAuxAuditDataRow(DataRowBuilder pRowBuilder)
      : base(pRowBuilder) {
    }

    /// <summary>Gets the <see cref="SystemDefaultAuxAuditDataTable"/>.</summary>
    [BindingBrowsable(false)]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public new SystemDefaultAuxAuditDataTable TypedTable {
      get { return (SystemDefaultAuxAuditDataTable) base.Table; }
    }

    /// <summary>Returns the <see cref="System.Type"/> of <see cref="SystemDefaultAuxAuditDataTable"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public static Type GetTableType() {
      return typeof(SystemDefaultAuxAuditDataTable);
    }

    /// <summary>
    /// Gets the SQL 'where' clause used to restrict which 
    /// database records are accessible via this entity. 
    /// </summary>
    /// <remarks>
    /// The default property value is defined in the Current Class 
    /// Properties tab in the <b>IdeaBlade DevForce Object Mapping Tool</b>.
    /// <para>
    /// The 'where' clause will be used, in addition to any other 
    /// query-specific criteria, in every retrieval of entities of this type.
    /// </para>
    /// </remarks>
    [BindingBrowsable(false)]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public virtual string SqlWhereClause {
      get { return ""; }
    }
    
    /// <summary>
    /// Determines whether to use the 'distinct' keyword when 
    /// generating the SQL select clause for objects of this type.
    /// </summary>
    /// <remarks>Defaults to false, but may be overridden in subclasses.</remarks>
    [BindingBrowsable(false)]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public virtual bool ForceSqlDistinct {
      get { return false; }
    }
    

    #region Enums
    //**************************************
    //* Enums
    //**************************************
    #endregion
	
    #region Relation properties
    //**************************************
    //* Relation properties
    //**************************************
    #endregion
    
    #region EntityColumn definitions
    //**************************************
    //* Static EntityColumn definitions
    //**************************************
    /// <summary>The SystemDefaultAuxHistoryID <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn SystemDefaultAuxHistoryIDEntityColumn =
      new EntityColumn(typeof(SystemDefaultAuxAudit), "SystemDefaultAuxHistoryID", typeof(System.Int64), false, true, true, DataSourceAccessType.ReadWrite);      
    /// <summary>The SystemDefaultAuxID <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn SystemDefaultAuxIDEntityColumn =
      new EntityColumn(typeof(SystemDefaultAuxAudit), "SystemDefaultAuxID", typeof(System.Int64), false, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The SystemDefaultID <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn SystemDefaultIDEntityColumn =
      new EntityColumn(typeof(SystemDefaultAuxAudit), "SystemDefaultID", typeof(System.Int64), false, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The Field <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn FieldEntityColumn =
      new EntityColumn(typeof(SystemDefaultAuxAudit), "Field", typeof(System.String), false, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The Value <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn ValueEntityColumn =
      new EntityColumn(typeof(SystemDefaultAuxAudit), "Value", typeof(System.String), false, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The RowVersion <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn RowVersionEntityColumn =
      new EntityColumn(typeof(SystemDefaultAuxAudit), "RowVersion", typeof(System.Int32), false, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The ModifiedBy <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn ModifiedByEntityColumn =
      new EntityColumn(typeof(SystemDefaultAuxAudit), "ModifiedBy", typeof(System.String), false, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The ModifiedDate <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn ModifiedDateEntityColumn =
      new EntityColumn(typeof(SystemDefaultAuxAudit), "ModifiedDate", typeof(System.DateTime), false, false, false, DataSourceAccessType.ReadWrite);      
    #endregion
    
    #region Properties
    //**************************************
    //* SystemDefaultAuxHistoryID methods
    //**************************************
    /// <summary>Gets the SystemDefaultAuxHistoryID <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn SystemDefaultAuxHistoryIDColumn {
      get { return TypedTable.SystemDefaultAuxHistoryIDColumn; }
    }

    /// <summary>Gets the SystemDefaultAuxHistoryID.</summary>
    [DBDataType(typeof(System.Int64))]
    public virtual System.Int64 SystemDefaultAuxHistoryID {
      get { 
        System.Int64 result_;
        if (GetInterceptor<System.Int64>("SystemDefaultAuxHistoryID", GetSystemDefaultAuxHistoryIDImpl, out result_)) return result_;
        return GetSystemDefaultAuxHistoryIDImpl();
      }
    }
    private System.Int64 GetSystemDefaultAuxHistoryIDImpl() {
      return (System.Int64) GetColumnValue(SystemDefaultAuxHistoryIDColumn, typeof(System.Int64), false); 
    }
    
    //**************************************
    //* SystemDefaultAuxID methods
    //**************************************
    /// <summary>Gets the SystemDefaultAuxID <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn SystemDefaultAuxIDColumn {
      get { return TypedTable.SystemDefaultAuxIDColumn; }
    }

    /// <summary>Gets or sets the SystemDefaultAuxID.</summary>
    [DBDataType(typeof(System.Int64))]
    public virtual System.Int64 SystemDefaultAuxID {
      get { 
        System.Int64 result_;
        if (GetInterceptor<System.Int64>("SystemDefaultAuxID", GetSystemDefaultAuxIDImpl, out result_)) return result_;
        return GetSystemDefaultAuxIDImpl();
      }
      set { 
          if (!SetInterceptor<System.Int64>("SystemDefaultAuxID", value, SetSystemDefaultAuxIDImpl)) {
            SetSystemDefaultAuxIDImpl(value);
          }  
      }    
    }
    private System.Int64 GetSystemDefaultAuxIDImpl() {
      return (System.Int64) GetColumnValue(SystemDefaultAuxIDColumn, typeof(System.Int64), false); 
    }
    private void SetSystemDefaultAuxIDImpl(System.Int64 value) {
      SetColumnValue(SystemDefaultAuxIDColumn, value);
    }
    
    //**************************************
    //* SystemDefaultID methods
    //**************************************
    /// <summary>Gets the SystemDefaultID <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn SystemDefaultIDColumn {
      get { return TypedTable.SystemDefaultIDColumn; }
    }

    /// <summary>Gets or sets the SystemDefaultID.</summary>
    [DBDataType(typeof(System.Int64))]
    public virtual System.Int64 SystemDefaultID {
      get { 
        System.Int64 result_;
        if (GetInterceptor<System.Int64>("SystemDefaultID", GetSystemDefaultIDImpl, out result_)) return result_;
        return GetSystemDefaultIDImpl();
      }
      set { 
          if (!SetInterceptor<System.Int64>("SystemDefaultID", value, SetSystemDefaultIDImpl)) {
            SetSystemDefaultIDImpl(value);
          }  
      }    
    }
    private System.Int64 GetSystemDefaultIDImpl() {
      return (System.Int64) GetColumnValue(SystemDefaultIDColumn, typeof(System.Int64), false); 
    }
    private void SetSystemDefaultIDImpl(System.Int64 value) {
      SetColumnValue(SystemDefaultIDColumn, value);
    }
    
    //**************************************
    //* Field methods
    //**************************************
    /// <summary>Gets the Field <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn FieldColumn {
      get { return TypedTable.FieldColumn; }
    }

    /// <summary>Gets or sets the Field.</summary>
    [MaxTextLength(50)]
    [DBDataType(typeof(System.String))]
    public virtual System.String Field {
      get { 
        System.String result_;
        if (GetInterceptor<System.String>("Field", GetFieldImpl, out result_)) return result_;
        return GetFieldImpl();
      }
      set { 
          if (!SetInterceptor<System.String>("Field", value, SetFieldImpl)) {
            SetFieldImpl(value);
          }  
      }    
    }
    private System.String GetFieldImpl() {
      return (System.String) GetColumnValue(FieldColumn, typeof(System.String), false); 
    }
    private void SetFieldImpl(System.String value) {
      SetColumnValue(FieldColumn, value);
    }
    
    //**************************************
    //* Value methods
    //**************************************
    /// <summary>Gets the Value <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn ValueColumn {
      get { return TypedTable.ValueColumn; }
    }

    /// <summary>Gets or sets the Value.</summary>
    [MaxTextLength(5000)]
    [DBDataType(typeof(System.String))]
    public virtual System.String Value {
      get { 
        System.String result_;
        if (GetInterceptor<System.String>("Value", GetValueImpl, out result_)) return result_;
        return GetValueImpl();
      }
      set { 
          if (!SetInterceptor<System.String>("Value", value, SetValueImpl)) {
            SetValueImpl(value);
          }  
      }    
    }
    private System.String GetValueImpl() {
      return (System.String) GetColumnValue(ValueColumn, typeof(System.String), false); 
    }
    private void SetValueImpl(System.String value) {
      SetColumnValue(ValueColumn, value);
    }
    
    //**************************************
    //* RowVersion methods
    //**************************************
    /// <summary>Gets the RowVersion <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn RowVersionColumn {
      get { return TypedTable.RowVersionColumn; }
    }

    /// <summary>Gets or sets the RowVersion.</summary>
    [DBDataType(typeof(System.Int32))]
    public virtual System.Int32 RowVersion {
      get { 
        System.Int32 result_;
        if (GetInterceptor<System.Int32>("RowVersion", GetRowVersionImpl, out result_)) return result_;
        return GetRowVersionImpl();
      }
      set { 
          if (!SetInterceptor<System.Int32>("RowVersion", value, SetRowVersionImpl)) {
            SetRowVersionImpl(value);
          }  
      }    
    }
    private System.Int32 GetRowVersionImpl() {
      return (System.Int32) GetColumnValue(RowVersionColumn, typeof(System.Int32), false); 
    }
    private void SetRowVersionImpl(System.Int32 value) {
      SetColumnValue(RowVersionColumn, value);
    }
    
    //**************************************
    //* ModifiedBy methods
    //**************************************
    /// <summary>Gets the ModifiedBy <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn ModifiedByColumn {
      get { return TypedTable.ModifiedByColumn; }
    }

    /// <summary>Gets or sets the ModifiedBy.</summary>
    [MaxTextLength(100)]
    [DBDataType(typeof(System.String))]
    public virtual System.String ModifiedBy {
      get { 
        System.String result_;
        if (GetInterceptor<System.String>("ModifiedBy", GetModifiedByImpl, out result_)) return result_;
        return GetModifiedByImpl();
      }
      set { 
          if (!SetInterceptor<System.String>("ModifiedBy", value, SetModifiedByImpl)) {
            SetModifiedByImpl(value);
          }  
      }    
    }
    private System.String GetModifiedByImpl() {
      return (System.String) GetColumnValue(ModifiedByColumn, typeof(System.String), false); 
    }
    private void SetModifiedByImpl(System.String value) {
      SetColumnValue(ModifiedByColumn, value);
    }
    
    //**************************************
    //* ModifiedDate methods
    //**************************************
    /// <summary>Gets the ModifiedDate <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn ModifiedDateColumn {
      get { return TypedTable.ModifiedDateColumn; }
    }

    /// <summary>Gets or sets the ModifiedDate.</summary>
    [DBDataType(typeof(System.DateTime))]
    public virtual System.DateTime ModifiedDate {
      get { 
        System.DateTime result_;
        if (GetInterceptor<System.DateTime>("ModifiedDate", GetModifiedDateImpl, out result_)) return result_;
        return GetModifiedDateImpl();
      }
      set { 
          if (!SetInterceptor<System.DateTime>("ModifiedDate", value, SetModifiedDateImpl)) {
            SetModifiedDateImpl(value);
          }  
      }    
    }
    private System.DateTime GetModifiedDateImpl() {
      return (System.DateTime) GetColumnValue(ModifiedDateColumn, typeof(System.DateTime), false); 
    }
    private void SetModifiedDateImpl(System.DateTime value) {
      SetColumnValue(ModifiedDateColumn, value);
    }
    
    #endregion

  }
  #endregion

  #region EntityPropertyDescriptors.SystemDefaultAuxAuditPropertyDescriptor
  public static partial class EntityPropertyDescriptors {

    /// <summary>
    /// Gets the SystemDefaultAuxAuditPropertyDescriptor for <see cref="SystemDefaultAuxAudit"/>.
    /// </summary>
    public static SystemDefaultAuxAuditPropertyDescriptor SystemDefaultAuxAudit {
      get { return SystemDefaultAuxAuditPropertyDescriptor.Instance; }
    }
    
    /// <summary>
    /// The generated class containing <see cref="SystemDefaultAuxAudit"/> PropertyDescriptors.
    /// </summary>
    public partial class SystemDefaultAuxAuditPropertyDescriptor : AdaptedPropertyDescriptor {
	
			/// <summary>
      /// Initializes a new instance of the SystemDefaultAuxAuditPropertyDescriptor class. 
      /// </summary>
      /// <param name="pParentDescriptor">The parent <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/></param>
      /// <param name="pPropertyPath">The type of the property value</param>
			public SystemDefaultAuxAuditPropertyDescriptor(AdaptedPropertyDescriptor pParentDescriptor, String pPropertyPath)
        : base(pParentDescriptor, pPropertyPath) {}
		
      /// <summary>
      /// Initializes a new instance of the SystemDefaultAuxAuditPropertyDescriptor class. 
      /// </summary>
      /// <param name="pComponentType">The type of object to which the property is bound</param>
      protected SystemDefaultAuxAuditPropertyDescriptor(Type pComponentType)
        : base(pComponentType) {}

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for SystemDefaultAuxHistoryID.</summary>
      public AdaptedPropertyDescriptor SystemDefaultAuxHistoryID {
        get { return Get("SystemDefaultAuxHistoryID"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for SystemDefaultAuxID.</summary>
      public AdaptedPropertyDescriptor SystemDefaultAuxID {
        get { return Get("SystemDefaultAuxID"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for SystemDefaultID.</summary>
      public AdaptedPropertyDescriptor SystemDefaultID {
        get { return Get("SystemDefaultID"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Field.</summary>
      public AdaptedPropertyDescriptor Field {
        get { return Get("Field"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Value.</summary>
      public AdaptedPropertyDescriptor Value {
        get { return Get("Value"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for RowVersion.</summary>
      public AdaptedPropertyDescriptor RowVersion {
        get { return Get("RowVersion"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for ModifiedBy.</summary>
      public AdaptedPropertyDescriptor ModifiedBy {
        get { return Get("ModifiedBy"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for ModifiedDate.</summary>
      public AdaptedPropertyDescriptor ModifiedDate {
        get { return Get("ModifiedDate"); }
      }

     internal static SystemDefaultAuxAuditPropertyDescriptor Instance = new SystemDefaultAuxAuditPropertyDescriptor(typeof(SystemDefaultAuxAudit));
    }
  }
  #endregion
  
}
