﻿using System;

namespace MobileTransWebAPI.Models {
    public class DailyTotalsResult {

        public string AffiliateName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string LoginName { get; set; }
        public string LoginNo { get; set; }
        public int Transactions { get; set; }
        public decimal Total { get; set; }

    }
}