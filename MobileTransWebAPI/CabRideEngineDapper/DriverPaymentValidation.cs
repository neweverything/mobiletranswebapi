﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Dapper;

namespace CabRideEngineDapper {
	public partial class DriverPaymentValidation {

		public static DriverPaymentValidation GetDriverInfo(SqlConnection pPM, string pDriverLicense) {
			string sql = @"SELECT TOP 1 *
							FROM    DriverPaymentValidation
							WHERE   LicenseNo = @DriverLicense
							ORDER BY DriverPaymentValidationID DESC";

			return pPM.Get<DriverPaymentValidation>(sql, new { DriverLicense = pDriverLicense });
		}

		public static DriverPaymentValidation GetDriverInfo(SqlConnection pPM, string pDriverLicense, string pState) {
			string sql = @"SELECT TOP 1
									*
							FROM    DriverPaymentValidation
							WHERE   LicenseNo = @DriverLicense AND State = @State
							ORDER BY DriverPaymentValidationID DESC";

			return pPM.Get<DriverPaymentValidation>(sql, new { DriverLicense = pDriverLicense, State = pState });
		}

		public static DriverPaymentValidation LastPaymentWithValidation(SqlConnection pConn, AffiliateDrivers pAffiliateDriver) {
			string sSQL = @"SELECT TOP 1 * FROM DriverPaymentValidation
							INNER JOIN DriverPayments ON DriverPaymentValidation.DriverPaymentID = DriverPayments.DriverPaymentID
							WHERE DriverPayments.AffiliateDriverID = @AffiliateDriverID 
							ORDER BY DriverPaymentValidation.ModifiedDate Desc";


			return pConn.Get<DriverPaymentValidation>(sSQL, new { AffiliateDriverID = pAffiliateDriver.AffiliateDriverID });
		}

	}
}
