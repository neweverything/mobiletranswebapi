﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

using Dapper;

using TaxiPassCommon;

namespace CabRideEngineDapper {
	public class usp_AchReportRedeemer {
		public string MyDriverName { get; set; }
		public string MyCellName { get; set; }
		public string MyRedeemer { get; set; }
		public DateTime MyChargeDateTime { get; set; }
		public decimal DriverTotal { get; set; }
		public decimal DriverFee { get; set; }
		public decimal RedemptionFee { get; set; }
		public decimal DriverTotalPaid { get; set; }
		public decimal RedeemerTotalPaid { get; set; }
		public string CardNumber { get; set; }
		public string TransNo { get; set; }
		public string ChargeApprovalNumber { get; set; }


		public static List<usp_AchReportRedeemer> Execute(SqlConnection pConn, long pRedeemerAccountID, string pBatchNumber) {
			return pConn.Query<usp_AchReportRedeemer>("usp_AchReportRedeemer", new { TaxiPassRedeemerAccountID = pRedeemerAccountID, ACHBatchNumber = pBatchNumber }, commandType: CommandType.StoredProcedure).ToList();
		}
	}
}
