﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaxiPassCommon.Banking {
    public static class VerisignTrxTypes {
        public const string Authorization = "A";
        public const string Sale = "S";
        public const string Credit = "C";
        public const string Void = "V";
        public const string VoiceAuthorization = "F";
        public const string Inquiry = "I";
        public const string DelayedCapture = "D";

        public static string GetTrxDescription(string pTrxType) {
            string sText = "";
            switch (pTrxType) {
                case VerisignTrxTypes.Authorization:
                    sText = "Authorization";
                    break;
                case VerisignTrxTypes.Sale:
                    sText = "Sale";
                    break;
                case VerisignTrxTypes.Credit:
                    sText = "Credit";
                    break;
                case VerisignTrxTypes.Void:
                    sText = "Void";
                    break;
                case VerisignTrxTypes.VoiceAuthorization:
                    sText = "Voice Authorization";
                    break;
                case VerisignTrxTypes.Inquiry:
                    sText = "Inquiry";
                    break;
                case VerisignTrxTypes.DelayedCapture:
                    sText = "Sale";
                    break;
            }
            return sText;
        }
    }

    public static class VerisignTenderTypes {
        public static string CreditCard = "C";
    }
}
