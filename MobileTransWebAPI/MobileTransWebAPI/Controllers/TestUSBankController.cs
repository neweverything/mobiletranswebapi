﻿using CabRideEngineDapper;
using CabRideEngineDapper.Utils;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TaxiPassCommon.Banking;

namespace MobileTransWebAPI.Controllers {
    public class TestUSBankController : ApiController {
        // GET api/testusbank
        public FSV.VerifyConnectionResponse Get() {
            FundsTransferResponse fundResults = new FundsTransferResponse();
            string url = "https://portalstage.paychekplus.com/fsvremote/services/fsvremote?WSDL";


            using (var conn = SqlHelper.OpenSqlConnection()) {
                AffiliateDrivers affDriver = AffiliateDrivers.GetDriverByCallInCell(conn, "9517751728");
                FSV.Credentials cred = new FSV.Credentials();
                cred.TestURL = url;
                cred.TestPassCode = "gVJ6@DbkZbeY5CHsyXnF";
                cred.TestUserName = "TAXIPASS";

                AffiliateDriverRef rec;
                cred = conn.Query<FSV.Credentials>("usp_SystemDefaultsDict_Flattened", new { ReferenceGroup = "FSV" }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                rec = AffiliateDriverRef.GetOrCreate(conn, affDriver.AffiliateDriverID, "FSV", "Registration");

                FSV fsv = new FSV(cred, false);
                FSV.VerifyConnectionResponse verifyResponse = new FSV.VerifyConnectionResponse();
                verifyResponse = fsv.VerifyConnection();
                return verifyResponse;
            }
        }

    }
}
