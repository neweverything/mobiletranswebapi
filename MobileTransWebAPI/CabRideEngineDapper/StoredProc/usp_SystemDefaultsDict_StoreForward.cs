﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace CabRideEngineDapper.StoredProc {
	public class usp_SystemDefaultsDict_StoreForward {
		public string DupeCheckInHours { get; set; }
		public string DupeCheckCardAcrossAllDrivers { get; set; }
		public string EMailFailedTransTo { get; set; }
		public string MatchFareAmount { get; set; }

		public int DupeCheckInHoursAsInt {
			get {
				return Convert.ToInt32(DupeCheckInHours);
			}
		}

		public bool DupeCheckCardAcrossAllDriversAsBool {
			get {
				return Convert.ToBoolean(DupeCheckCardAcrossAllDrivers);
			}
		}

		public bool MatchFareAmountAsBool {
			get {
				return Convert.ToBoolean(MatchFareAmount);
			}
		}

		public static usp_SystemDefaultsDict_StoreForward Execute(SqlConnection pConn) {
			return pConn.Query<usp_SystemDefaultsDict_StoreForward>("usp_SystemDefaultsDict_Flattened", new { ReferenceGroup = "StoreForward" }, commandType: CommandType.StoredProcedure).DefaultIfEmpty(new usp_SystemDefaultsDict_StoreForward()).FirstOrDefault();
		}
	}
}
