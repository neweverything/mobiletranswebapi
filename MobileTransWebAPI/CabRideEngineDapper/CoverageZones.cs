﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Dapper;
using TaxiPassCommon;

using CabRideEngineDapper.Utils;

namespace CabRideEngineDapper {
	public partial class CoverageZones {


		private Markets mMarket;
		public Markets Market {
			get {
				if (mMarket == null) {
					using (var conn = SqlHelper.OpenSqlConnection()) {
						string sql = "SELECT * FROM Markets WHERE MarketID = @marketID";
						mMarket = conn.Query<Markets>(sql, new { marketID = this.MarketID }).DefaultIfEmpty(Markets.GetNullEntity()).FirstOrDefault();
					}
				}
				return mMarket;
			}
			set {
				mMarket = value;
			}
		}


		private List<CoverageArea> mCoverageAreas;
		public List<CoverageArea> CoverageAreas {
			get {
				if (mCoverageAreas == null) {
					using (var conn = SqlHelper.OpenSqlConnection()) {
						mCoverageAreas = conn.GetList<CoverageArea>(new { CoverageZoneID = this.CoverageZoneID }).ToList();
					}
				}
				return mCoverageAreas;
			}
		}

		public static CoverageZones GetAirport(string pAirportCode) {
			using (var conn = SqlHelper.OpenSqlConnection()) {
				return conn.Query<CoverageZones>("SELECT * FROM CoverageZones WHERE CoverageZone = @AirportCode", new { AirportCode = pAirportCode }).DefaultIfEmpty(CoverageZones.GetNullEntity()).FirstOrDefault();
			}
		}
	}
}
