﻿using CabRideEngineDapper.Utils;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using TaxiPassCommon;

namespace CabRideEngineDapper {
    public partial class DriversGPSLocation {

        #region Related Tables
        private Drivers mDrivers;
        public Drivers Drivers {
            get {
                if (mDrivers == null) {
                    mDrivers = CabRideEngineDapper.Drivers.GetDriver(this.DriverID);
                }
                return mDrivers;
            }
        }
        #endregion

        private static DriversGPSLocation Create(Drivers pDriver) {
            string sql = "SELECT * FROM DriversGPSLocation WHERE DriverID = @DriverID";
            using (var conn = SqlHelper.OpenSqlConnection()) {
                DriversGPSLocation oDriversGPSLocation = conn.Get<DriversGPSLocation>(sql, new { DriverID = pDriver.DriverID });

                try {
                    if (oDriversGPSLocation.IsNullEntity) {
                        oDriversGPSLocation = Create();
                    }
                    oDriversGPSLocation.DriverID = pDriver.DriverID;

                } catch (Exception ex) {
                    throw ex;
                }
                return oDriversGPSLocation;
            }
        }

        public override int Save() {
            if (IsNewRecord) {
                this.DriverGPSLocationID = NextID.GetNextKey(DriversGPSLocation.TableDef.TABLE_NAME);
            }
            return base.Save();
        }
        public override int Save(SqlConnection pConn, string pModifiedBy) {
            if (IsNewRecord) {
                this.DriverGPSLocationID = NextID.GetNextKey(DriversGPSLocation.TableDef.TABLE_NAME);
            }

            return base.Save(pConn, pModifiedBy);
        }


        public static DriversGPSLocation GetRecord(Drivers pDriver) {
            string sql = "SELECT * FROM DriversGPSLocation WHERE DriverID = @DriverID";
            using (var conn = SqlHelper.OpenSqlConnection()) {
                DriversGPSLocation oRec = conn.Get<DriversGPSLocation>(sql, new { DriverID = pDriver.DriverID });
                if (oRec.IsNullEntity) {
                    oRec = Create(pDriver);
                    oRec.GoodSwipes = 0;
                    oRec.BadSwipes = 0;
                    oRec.Save();
                }
                return oRec;
            }
        }

        // ******************************************************************************************
        // Pass in a DriverID, return the gps record
        // ******************************************************************************************
        public static DriversGPSLocation GetByDriverID(SqlConnection pConn, long pDriverID) {
            string sql = "SELECT * FROM DriversGPSLocation WHERE DriverID = @DriverID";
            DriversGPSLocation oRec = pConn.Get<DriversGPSLocation>(sql, new { DriverID = pDriverID });
            if (oRec.IsNullEntity) {
                Drivers oDriver = Drivers.GetDriver(pConn, pDriverID);
                oRec = Create(oDriver);
                oRec.GoodSwipes = 0;
                oRec.BadSwipes = 0;
                oRec.Save();
            }
            return oRec;
        }

        public static DriversGPSLocation GetRecord(SqlConnection pConn, long pDriverGPSLocationID) {
            return pConn.Get<DriversGPSLocation>(pDriverGPSLocationID);
        }

        // *********************************************************************************************
        // Get all GPSLocs for an affiliate
        // *********************************************************************************************
        public static List<DriversGPSLocation> GetAffiliateDriverLocations(Affiliate pAffiliate) {
            using (var conn = SqlHelper.OpenSqlConnection()) {
                return conn.Query<DriversGPSLocation>("usp_DriversGPSLocation_GetByAffiliate", new { AffiliateID = pAffiliate.AffiliateID }, commandType: CommandType.StoredProcedure).ToList();
            }
        }

        public static List<DriversGPSLocation> GetInactiveDriverLocations(Affiliate pAffiliate) {
            if (pAffiliate.AffiliateDriverDefaults.IsNullEntity) {
                return new List<DriversGPSLocation>();
            }
            if (pAffiliate.AffiliateDriverDefaults.EmailInactiveDriversDuration < 1) {
                return new List<DriversGPSLocation>();
            }
            DateTime dDate = DateTime.Now.AddHours(-pAffiliate.AffiliateDriverDefaults.EmailInactiveDriversDuration);

            string sql = "SELECT * FROM DriversGPSLocation WHERE GPSTime <= @Date";
            using (var conn = SqlHelper.OpenSqlConnection()) {
                return conn.Query<DriversGPSLocation>(sql, new { Date = dDate }).ToList();
            }
        }

        public static DriversGPSLocation GetLastAffiliateDriverLocation(AffiliateDrivers pDriver) {
            string sql = @"SELECT  *
                            FROM    DriversGPSLocation
                            WHERE   AffiliateDriverID = @AffiliateDriverID
                            ORDER BY GPSTime DESC";
            using (var conn = SqlHelper.OpenSqlConnection()) {
                return conn.Get<DriversGPSLocation>(sql, new { AffiliateDriverID = pDriver.AffiliateDriverID });
            }
        }

        [Editable(false)]
        public DateTime? GetSetLocalGPSTime {
            get {
                return this.GPSTime;
            }
            set {
                if (value.HasValue) {
                    if (this.Drivers.MyAffiliate != null && !this.Drivers.MyAffiliate.TimeZone.IsNullOrEmpty()) {
                        TimeZoneConverter oTime = new TimeZoneConverter();
                        base.GPSTime = oTime.ConvertTime(value.Value, TimeZone.CurrentTimeZone.StandardName, this.Drivers.MyAffiliate.TimeZone);
                    } else {
                        base.GPSTime = value.Value.ToUniversalTime();
                    }
                } else {
                    base.GPSTime = value;
                }
            }
        }

        [Editable(false)]
        public DateTime? GetSetLocalMeterStatusTime {
            get {
                return base.MeterStatusTime;
            }
            set {
                if (value.HasValue) {
                    if (this.Drivers.MyAffiliate != null && !this.Drivers.MyAffiliate.TimeZone.IsNullOrEmpty()) {
                        TimeZoneConverter oTime = new TimeZoneConverter();
                        base.MeterStatusTime = oTime.ConvertTime(value.Value, TimeZone.CurrentTimeZone.StandardName, this.Drivers.MyAffiliate.TimeZone);
                    } else {
                        base.MeterStatusTime = value.Value.ToUniversalTime();
                    }
                } else {
                    base.MeterStatusTime = value;
                }
            }
        }

        [Editable(false)]
        public string LocalGPSDateTimeString {
            get {
                string sTime = "";
                if (this.GPSTime.HasValue) {
                    if (this.Drivers.MyAffiliate != null && !string.IsNullOrEmpty(this.Drivers.MyAffiliate.TimeZone)) {
                        TimeZoneConverter oTime = new TimeZoneConverter();
                        sTime = oTime.FromUniversalTime(this.Drivers.MyAffiliate.TimeZone, this.GPSTime.Value).ToString("MM /dd/yyyy hh:mm tt");
                    } else {
                        sTime = this.GPSTime.Value.ToLocalTime().ToString("MM/dd/yyyy hh:mm tt");
                    }
                }
                return sTime;
            }
        }
    }
}
