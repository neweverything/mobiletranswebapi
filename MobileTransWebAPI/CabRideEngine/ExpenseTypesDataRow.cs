using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Runtime.Serialization;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

namespace CabRideEngine {
 
  #region ExpenseTypesDataTable
  /// <summary>
  /// The generated <see cref="T:IdeaBlade.Persistence.EntityTable"/> class  
  /// containing all <see cref="ExpenseTypes"/> objects.
  /// </summary>
  /// <remarks>
  /// ****************************************************
  /// <para>** DO NOT MODIFY THIS CLASS - AutoGenerated Code</para>
  /// <para>** Any changes made to this code will be lost</para>
  /// <para>** the next time this code is regenerated.</para>
  /// <para>**</para>
  /// <para>** Generated from:   EntityBaseclass.template</para>
  /// <para>** Last modified:    2012-03-15T13:21:24.2165358-07:00</para>
  /// <para>** Template version: 3.110127</para>
  /// ****************************************************
  /// </remarks>
  [BindingBrowsable(false)]
  [Serializable]
  public partial class ExpenseTypesDataTable : BaseEntityDataTable, 
     IdeaBlade.Persistence.Rdb.IRdbTable {
  
    /// <summary>
    /// Initializes a new instance of the ExpenseTypesDataTable class with no arguments.
    /// </summary>
    public ExpenseTypesDataTable() {}

    /// <summary>
    /// Initializes a new instance of the ExpenseTypesDataTable class 
    /// with the <see cref="System.Runtime.Serialization.SerializationInfo"/> 
    /// and the <see cref="System.Runtime.Serialization.StreamingContext"/>.
    /// </summary>
    /// <param name="pInfo">The data needed to serialize or deserialize an object.</param>
    /// <param name="pContext">The source and destination of a given serialized stream.</param>
    protected ExpenseTypesDataTable(SerializationInfo pInfo, StreamingContext pContext) :
      base(pInfo, pContext) {}

    /// <summary>
    /// Creates a new instance of the <see cref="ExpenseTypesDataRow"/>.
    /// Constructs a row from the builder.
    /// <para>** For internal use only. **</para>
    /// </summary>
    /// <param name="pRowBuilder"></param>
    /// <returns>The <see cref="ExpenseTypesDataRow"/>.</returns>
    protected override DataRow NewRowFromBuilder(DataRowBuilder pRowBuilder) {
      return new ExpenseTypes(pRowBuilder);
    }

    /// <summary>Returns the <see cref="System.Type"/> of <see cref="ExpenseTypes"/>.</summary>
    protected override Type GetRowType() {
      return typeof(ExpenseTypes);
    }
    
    /// <summary>
    /// Gets the name of the <see cref="T:IdeaBlade.Rdb.RdbKey"/> or 
    /// <see cref="T:IdeaBlade.Persistence.WS.WsKey"/> defined in <b>IdeaBlade.ibconfig</b>. 
    /// </summary>
    /// <remarks>For this <see cref="ExpenseTypes"/>, the DataSourceKeyName="default".</remarks>
    public override String DataSourceKeyName {
      get { return @"default"; }
    }

    #region Column properties
    //**************************************
    //* Column properties
    //**************************************
    /// <summary>Gets the ExpenseTypeID <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn ExpenseTypeIDColumn {
      get {
        if (mExpenseTypeIDColumn!=null) return mExpenseTypeIDColumn;
        mExpenseTypeIDColumn = GetColumn("ExpenseTypeID", true);
        return mExpenseTypeIDColumn;
      }
    }
    private DataColumn mExpenseTypeIDColumn;
    
    /// <summary>Gets the Description <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn DescriptionColumn {
      get {
        if (mDescriptionColumn!=null) return mDescriptionColumn;
        mDescriptionColumn = GetColumn("Description", true);
        return mDescriptionColumn;
      }
    }
    private DataColumn mDescriptionColumn;
    
    /// <summary>Gets the RowVersion <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn RowVersionColumn {
      get {
        if (mRowVersionColumn!=null) return mRowVersionColumn;
        mRowVersionColumn = GetColumn("RowVersion", true);
        return mRowVersionColumn;
      }
    }
    private DataColumn mRowVersionColumn;
    
    /// <summary>Gets the ModifiedBy <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn ModifiedByColumn {
      get {
        if (mModifiedByColumn!=null) return mModifiedByColumn;
        mModifiedByColumn = GetColumn("ModifiedBy", true);
        return mModifiedByColumn;
      }
    }
    private DataColumn mModifiedByColumn;
    
    /// <summary>Gets the ModifiedDate <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn ModifiedDateColumn {
      get {
        if (mModifiedDateColumn!=null) return mModifiedDateColumn;
        mModifiedDateColumn = GetColumn("ModifiedDate", true);
        return mModifiedDateColumn;
      }
    }
    private DataColumn mModifiedDateColumn;
    
    #endregion    

    /// <summary>Creates the schema mapping information for this ExpenseTypesDataTable.</summary>
    /// <returns>
    /// The <see cref="T:IdeaBlade.Persistence.TableMappingInfo"/> for this ExpenseTypesDataTable.
    /// </returns>
    protected override TableMappingInfo CreateTableMappingInfo() {
      RdbTableMappingInfo mappingInfo = new RdbTableMappingInfo(this);
      mappingInfo.SourceOwnerName = "dbo"; 
      mappingInfo.SourceTableName = "ExpenseTypes"; 
      mappingInfo.ConcurrencyColumnName = "RowVersion"; 
      // ColumnSelectionOption: selectAllColumns
      mappingInfo.SourceColumnNames = "*"; 
   
      DataColumnMappingCollection columnMappings = mappingInfo.TableMapping.ColumnMappings;
      columnMappings.Clear();
      columnMappings.Add("ExpenseTypeID", "ExpenseTypeID");
      columnMappings.Add("Description", "Description");
      columnMappings.Add("RowVersion", "RowVersion");
      columnMappings.Add("ModifiedBy", "ModifiedBy");
      columnMappings.Add("ModifiedDate", "ModifiedDate");
      return mappingInfo;
    }
  
    /// <summary>** For internal use only. **</summary>
    protected override void UpdateTableMappingInfo() {
      base.UpdateTableMappingInfo();
      this.TableMappingInfo.ColumnUpdateMap["RowVersion"] = "RowVersion + 1";
    }

    /// <summary>
    /// Initializes ExpenseTypes <see cref="System.Data.DataColumn"/> properties.
    /// </summary>
    public override void InitColumnProperties() {
      base.InitColumnProperties();

      ExpenseTypeIDColumn.Caption = "ExpenseTypeID";
      DescriptionColumn.Caption = "Description";
      RowVersionColumn.Caption = "RowVersion";
      ModifiedByColumn.Caption = "ModifiedBy";
      ModifiedDateColumn.Caption = "ModifiedDate";
    }
  }
  #endregion
  
  #region ExpenseTypesDataRow
  /// <summary>
  /// The generated base class for <see cref="ExpenseTypes"/>.
  /// </summary>
  /// <remarks>
  /// <para>
  /// It represents a row of persistable data in the <see cref="ExpenseTypesDataTable"/>
  /// and derives from <see cref="T:IdeaBlade.Persistence.Entity"/>.
	/// </para>
  /// ****************************************************
  /// <para>** DO NOT MODIFY THIS CLASS - AutoGenerated Code</para>
  /// <para>** Any changes made to this code will be lost</para>
  /// <para>** the next time this code is regenerated.</para>
  /// <para>**</para>
  /// <para>** Generated from:   EntityBaseclass.template</para>
  /// <para>** Last modified:    2012-03-15T13:21:24.2165358-07:00</para>
  /// <para>** Template version: 3.110127</para>
  /// ****************************************************
  /// </remarks>  
  [Serializable]
  public abstract partial class ExpenseTypesDataRow : BaseEntity, 
     IdeaBlade.Persistence.Rdb.IRdbEntity {
  
    /// <summary>
    /// Initializes a new instance of the ExpenseTypesDataRow.
    /// Constructs a row from the builder.
    /// <para>** For internal use only. **</para>
    /// </summary>
    /// <param name="pRowBuilder"></param>
    protected ExpenseTypesDataRow(DataRowBuilder pRowBuilder)
      : base(pRowBuilder) {
    }

    /// <summary>Gets the <see cref="ExpenseTypesDataTable"/>.</summary>
    [BindingBrowsable(false)]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public new ExpenseTypesDataTable TypedTable {
      get { return (ExpenseTypesDataTable) base.Table; }
    }

    /// <summary>Returns the <see cref="System.Type"/> of <see cref="ExpenseTypesDataTable"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public static Type GetTableType() {
      return typeof(ExpenseTypesDataTable);
    }

    /// <summary>
    /// Gets the SQL 'where' clause used to restrict which 
    /// database records are accessible via this entity. 
    /// </summary>
    /// <remarks>
    /// The default property value is defined in the Current Class 
    /// Properties tab in the <b>IdeaBlade DevForce Object Mapping Tool</b>.
    /// <para>
    /// The 'where' clause will be used, in addition to any other 
    /// query-specific criteria, in every retrieval of entities of this type.
    /// </para>
    /// </remarks>
    [BindingBrowsable(false)]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public virtual string SqlWhereClause {
      get { return ""; }
    }
    
    /// <summary>
    /// Determines whether to use the 'distinct' keyword when 
    /// generating the SQL select clause for objects of this type.
    /// </summary>
    /// <remarks>Defaults to false, but may be overridden in subclasses.</remarks>
    [BindingBrowsable(false)]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public virtual bool ForceSqlDistinct {
      get { return false; }
    }
    

    #region Enums
    //**************************************
    //* Enums
    //**************************************
    #endregion
	
    #region Relation properties
    //**************************************
    //* Relation properties
    //**************************************
	  /// <summary>Gets the AffiliateRecurringFeeses relation property.</summary>
    [RelationProperty("ExpenseTypes_AffiliateRecurringFees", 
    QueryDirection.ChildQuery)]
    public virtual ReadOnlyEntityList<AffiliateRecurringFees> AffiliateRecurringFeeses {
      get { 
        ReadOnlyEntityList<AffiliateRecurringFees> result_;
        if (GetInterceptor<ReadOnlyEntityList<AffiliateRecurringFees>>("AffiliateRecurringFeeses", GetAffiliateRecurringFeesesImpl, out result_)) return result_;
        return GetAffiliateRecurringFeesesImpl();
      }
    }
    private ReadOnlyEntityList<AffiliateRecurringFees> GetAffiliateRecurringFeesesImpl() {
      return this.GetManagedChildren<AffiliateRecurringFees>(EntityRelations.ExpenseTypes_AffiliateRecurringFees);
    } 
    
	  /// <summary>Gets the DriverRecurringFeeses relation property.</summary>
    [RelationProperty("ExpenseTypes_DriverRecurringFees", 
    QueryDirection.ChildQuery)]
    public virtual ReadOnlyEntityList<DriverRecurringFees> DriverRecurringFeeses {
      get { 
        ReadOnlyEntityList<DriverRecurringFees> result_;
        if (GetInterceptor<ReadOnlyEntityList<DriverRecurringFees>>("DriverRecurringFeeses", GetDriverRecurringFeesesImpl, out result_)) return result_;
        return GetDriverRecurringFeesesImpl();
      }
    }
    private ReadOnlyEntityList<DriverRecurringFees> GetDriverRecurringFeesesImpl() {
      return this.GetManagedChildren<DriverRecurringFees>(EntityRelations.ExpenseTypes_DriverRecurringFees);
    } 
    
	  /// <summary>Gets the AffiliateDriversRecurringFeeses relation property.</summary>
    [RelationProperty("ExpenseTypes_AffiliateDriversRecurringFees", 
    QueryDirection.ChildQuery)]
    public virtual ReadOnlyEntityList<AffiliateDriversRecurringFees> AffiliateDriversRecurringFeeses {
      get { 
        ReadOnlyEntityList<AffiliateDriversRecurringFees> result_;
        if (GetInterceptor<ReadOnlyEntityList<AffiliateDriversRecurringFees>>("AffiliateDriversRecurringFeeses", GetAffiliateDriversRecurringFeesesImpl, out result_)) return result_;
        return GetAffiliateDriversRecurringFeesesImpl();
      }
    }
    private ReadOnlyEntityList<AffiliateDriversRecurringFees> GetAffiliateDriversRecurringFeesesImpl() {
      return this.GetManagedChildren<AffiliateDriversRecurringFees>(EntityRelations.ExpenseTypes_AffiliateDriversRecurringFees);
    } 
    
    #endregion
    
    #region EntityColumn definitions
    //**************************************
    //* Static EntityColumn definitions
    //**************************************
    /// <summary>The ExpenseTypeID <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn ExpenseTypeIDEntityColumn =
      new EntityColumn(typeof(ExpenseTypes), "ExpenseTypeID", typeof(System.Int64), false, true, true, DataSourceAccessType.ReadWrite);      
    /// <summary>The Description <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn DescriptionEntityColumn =
      new EntityColumn(typeof(ExpenseTypes), "Description", typeof(System.String), false, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The RowVersion <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn RowVersionEntityColumn =
      new EntityColumn(typeof(ExpenseTypes), "RowVersion", typeof(System.Int32), false, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The ModifiedBy <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn ModifiedByEntityColumn =
      new EntityColumn(typeof(ExpenseTypes), "ModifiedBy", typeof(System.String), true, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The ModifiedDate <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn ModifiedDateEntityColumn =
      new EntityColumn(typeof(ExpenseTypes), "ModifiedDate", typeof(System.DateTime), true, false, false, DataSourceAccessType.ReadWrite);      
    #endregion
    
    #region Properties
    //**************************************
    //* ExpenseTypeID methods
    //**************************************
    /// <summary>Gets the ExpenseTypeID <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn ExpenseTypeIDColumn {
      get { return TypedTable.ExpenseTypeIDColumn; }
    }

    /// <summary>Gets the ExpenseTypeID.</summary>
    [DBDataType(typeof(System.Int64))]
    public virtual System.Int64 ExpenseTypeID {
      get { 
        System.Int64 result_;
        if (GetInterceptor<System.Int64>("ExpenseTypeID", GetExpenseTypeIDImpl, out result_)) return result_;
        return GetExpenseTypeIDImpl();
      }
    }
    private System.Int64 GetExpenseTypeIDImpl() {
      return (System.Int64) GetColumnValue(ExpenseTypeIDColumn, typeof(System.Int64), false); 
    }
    
    //**************************************
    //* Description methods
    //**************************************
    /// <summary>Gets the Description <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn DescriptionColumn {
      get { return TypedTable.DescriptionColumn; }
    }

    /// <summary>Gets or sets the Description.</summary>
    [MaxTextLength(50)]
    [DBDataType(typeof(System.String))]
    public virtual System.String Description {
      get { 
        System.String result_;
        if (GetInterceptor<System.String>("Description", GetDescriptionImpl, out result_)) return result_;
        return GetDescriptionImpl();
      }
      set { 
          if (!SetInterceptor<System.String>("Description", value, SetDescriptionImpl)) {
            SetDescriptionImpl(value);
          }  
      }    
    }
    private System.String GetDescriptionImpl() {
      return (System.String) GetColumnValue(DescriptionColumn, typeof(System.String), false); 
    }
    private void SetDescriptionImpl(System.String value) {
      SetColumnValue(DescriptionColumn, value);
    }
    
    //**************************************
    //* RowVersion methods
    //**************************************
    /// <summary>Gets the RowVersion <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn RowVersionColumn {
      get { return TypedTable.RowVersionColumn; }
    }

    /// <summary>Gets or sets the RowVersion.</summary>
    [DBDataType(typeof(System.Int32))]
    public virtual System.Int32 RowVersion {
      get { 
        System.Int32 result_;
        if (GetInterceptor<System.Int32>("RowVersion", GetRowVersionImpl, out result_)) return result_;
        return GetRowVersionImpl();
      }
      set { 
          if (!SetInterceptor<System.Int32>("RowVersion", value, SetRowVersionImpl)) {
            SetRowVersionImpl(value);
          }  
      }    
    }
    private System.Int32 GetRowVersionImpl() {
      return (System.Int32) GetColumnValue(RowVersionColumn, typeof(System.Int32), false); 
    }
    private void SetRowVersionImpl(System.Int32 value) {
      SetColumnValue(RowVersionColumn, value);
    }
    
    //**************************************
    //* ModifiedBy methods
    //**************************************
    /// <summary>Gets the ModifiedBy <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn ModifiedByColumn {
      get { return TypedTable.ModifiedByColumn; }
    }

    /// <summary>Gets or sets the ModifiedBy.</summary>
    [MaxTextLength(100)]
    [DBDataType(typeof(System.String))]
    public virtual System.String ModifiedBy {
      get { 
        System.String result_;
        if (GetInterceptor<System.String>("ModifiedBy", GetModifiedByImpl, out result_)) return result_;
        return GetModifiedByImpl();
      }
      set { 
          if (!SetInterceptor<System.String>("ModifiedBy", value, SetModifiedByImpl)) {
            SetModifiedByImpl(value);
          }  
      }    
    }
    private System.String GetModifiedByImpl() {
      return (System.String) GetColumnValue(ModifiedByColumn, typeof(System.String), true); 
    }
    private void SetModifiedByImpl(System.String value) {
      SetColumnValue(ModifiedByColumn, value);
    }
    
    //**************************************
    //* ModifiedDate methods
    //**************************************
    /// <summary>Gets the ModifiedDate <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn ModifiedDateColumn {
      get { return TypedTable.ModifiedDateColumn; }
    }

    /// <summary>Gets or sets the ModifiedDate.</summary>
    [DBDataType(typeof(System.DateTime))]
    public virtual Nullable<System.DateTime> ModifiedDate {
      get { 
        Nullable<System.DateTime> result_;
        if (GetInterceptor<Nullable<System.DateTime>>("ModifiedDate", GetModifiedDateImpl, out result_)) return result_;
        return GetModifiedDateImpl();
      }
      set { 
          if (!SetInterceptor<Nullable<System.DateTime>>("ModifiedDate", value, SetModifiedDateImpl)) {
            SetModifiedDateImpl(value);
          }
      }    
    }
    private Nullable<System.DateTime> GetModifiedDateImpl() {
      return (Nullable<System.DateTime>) GetColumnValue(ModifiedDateColumn, typeof(System.DateTime), true); 
    }
    private void SetModifiedDateImpl(Nullable<System.DateTime> value) {
      SetColumnValue(ModifiedDateColumn, value);
    }
    
    #endregion

  }
  #endregion

  #region EntityPropertyDescriptors.ExpenseTypesPropertyDescriptor
  public static partial class EntityPropertyDescriptors {

    /// <summary>
    /// Gets the ExpenseTypesPropertyDescriptor for <see cref="ExpenseTypes"/>.
    /// </summary>
    public static ExpenseTypesPropertyDescriptor ExpenseTypes {
      get { return ExpenseTypesPropertyDescriptor.Instance; }
    }
    
    /// <summary>
    /// The generated class containing <see cref="ExpenseTypes"/> PropertyDescriptors.
    /// </summary>
    public partial class ExpenseTypesPropertyDescriptor : BaseEntityPropertyDescriptor {
	
			/// <summary>
      /// Initializes a new instance of the ExpenseTypesPropertyDescriptor class. 
      /// </summary>
      /// <param name="pParentDescriptor">The parent <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/></param>
      /// <param name="pPropertyPath">The type of the property value</param>
			public ExpenseTypesPropertyDescriptor(AdaptedPropertyDescriptor pParentDescriptor, String pPropertyPath)
        : base(pParentDescriptor, pPropertyPath) {}
		
      /// <summary>
      /// Initializes a new instance of the ExpenseTypesPropertyDescriptor class. 
      /// </summary>
      /// <param name="pComponentType">The type of object to which the property is bound</param>
      protected ExpenseTypesPropertyDescriptor(Type pComponentType)
        : base(pComponentType) {}

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for ExpenseTypeID.</summary>
      public AdaptedPropertyDescriptor ExpenseTypeID {
        get { return Get("ExpenseTypeID"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Description. Aliased with an '_' because Description is reserved.</summary>
      public AdaptedPropertyDescriptor Description_ {
        get { return Get("Description"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for RowVersion.</summary>
      public AdaptedPropertyDescriptor RowVersion {
        get { return Get("RowVersion"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for ModifiedBy.</summary>
      public AdaptedPropertyDescriptor ModifiedBy {
        get { return Get("ModifiedBy"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for ModifiedDate.</summary>
      public AdaptedPropertyDescriptor ModifiedDate {
        get { return Get("ModifiedDate"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for AffiliateRecurringFeeses.</summary>
      public AdaptedPropertyDescriptor AffiliateRecurringFeeses {
        get { return Get("AffiliateRecurringFeeses"); }
      }
						
      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for DriverRecurringFeeses.</summary>
      public AdaptedPropertyDescriptor DriverRecurringFeeses {
        get { return Get("DriverRecurringFeeses"); }
      }
						
      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for AffiliateDriversRecurringFeeses.</summary>
      public AdaptedPropertyDescriptor AffiliateDriversRecurringFeeses {
        get { return Get("AffiliateDriversRecurringFeeses"); }
      }
						
     internal new static ExpenseTypesPropertyDescriptor Instance = new ExpenseTypesPropertyDescriptor(typeof(ExpenseTypes));
    }
  }
  #endregion
  
}
