﻿using CabRideEngineDapper.Utils;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace CabRideEngineDapper {
	public partial class DriverPaymentResults {

		#region Related Tables

		private DriverPayments mDriverPayments;
		public DriverPayments DriverPayments {
			get {
				if (mDriverPayments == null || mDriverPayments.IsNullEntity) {
					using (var conn = SqlHelper.OpenSqlConnection()) {
						mDriverPayments = DriverPayments.GetPayment(conn, this.DriverPaymentID);
					}
				}
				return mDriverPayments;
			}
			set {
				mDriverPayments = value;
			}
		}
		#endregion

		public static DriverPaymentResults Create(DriverPayments pDriverPayment) {
			DriverPaymentResults oResult = null;
			try {
				oResult = Create();
				oResult.DriverPaymentID = pDriverPayment.DriverPaymentID;

				if (pDriverPayment.DriverPaymentResultses == null) {
					pDriverPayment.DriverPaymentResultses = new List<DriverPaymentResults>();
				}
				pDriverPayment.DriverPaymentResultses.Add(oResult);
			} catch (Exception ex) {
				throw ex;
			}
			return oResult;
		}


		public override int Save() {
			return Save("");
		}

		public override int Save(string pModifiedBy) {
			ModifiedDate = DateTime.UtcNow;
			ModifiedBy = string.Format("{0} | {1}", pModifiedBy, CabRideDapperSettings.GetAppName());
			if (IsNewRecord) {
				this.DriverPaymentResultID = NextID.GetNextKey(DriverPaymentResults.TableDef.TABLE_NAME);
			} else {
				RowVersion++;
			}
			if (("" + AVS).Length > 1) {
				AVS = AVS.Substring(0, 1);
			}
			return base.Save(pModifiedBy);
		}


		public static DriverPaymentResults GetAuthApprovedResult(DriverPayments pPayment) {
			string sql = @"SELECT TOP 1 * FROM DriverPaymentResults 
							WHERE DriverPaymentID = @DriverPaymentID 
									AND TransType = 'A'
									AND Result = '0'
							Order By DriverPaymentResultID Desc";

			using (var conn = SqlHelper.OpenSqlConnection()) {
				return conn.Query<DriverPaymentResults>(sql, new { DriverPaymentID = pPayment.DriverPaymentID }).DefaultIfEmpty(DriverPaymentResults.GetNullEntity()).FirstOrDefault();
			}
		}

		public static DriverPaymentResults GetSaledApprovedResult(DriverPayments pPayment) {
			return GetSaledApprovedResult(pPayment.DriverPaymentID);
		}

		public static DriverPaymentResults GetSaledApprovedResult(long pDriverPaymentID) {
			string sql = @"SELECT TOP 1 * FROM DriverPaymentResults 
							WHERE DriverPaymentID = @DriverPaymentID 
									AND TransType in ('D', 'S')
									AND Result = '0'
							Order By DriverPaymentResultID Desc";

			using (var conn = SqlHelper.OpenSqlConnection()) {
				return conn.Query<DriverPaymentResults>(sql, new { DriverPaymentID = pDriverPaymentID }).DefaultIfEmpty(DriverPaymentResults.GetNullEntity()).FirstOrDefault();
			}
		}

		public static DriverPaymentResults GetRefundApprovedResult(DriverPayments pPayment) {
			string sql = @"SELECT TOP 1 * FROM DriverPaymentResults 
							WHERE DriverPaymentID = @DriverPaymentID 
									AND TransType = 'C'
									AND Result = '0'
							Order By DriverPaymentResultID Desc";

			using (var conn = SqlHelper.OpenSqlConnection()) {
				return conn.Query<DriverPaymentResults>(sql, new { DriverPaymentID = pPayment.DriverPaymentID }).DefaultIfEmpty(DriverPaymentResults.GetNullEntity()).FirstOrDefault();
			}
		}

		public static List<DriverPaymentResults> GetResults(long pDriverPaymentID) {
			using (var conn = SqlHelper.OpenSqlConnection()) {
				return GetResults(conn, pDriverPaymentID);
			}
		}

		public static List<DriverPaymentResults> GetResults(SqlConnection pConn, long pDriverPaymentID) {
			string sql = @"SELECT * FROM DriverPaymentResults 
							WHERE DriverPaymentID = @DriverPaymentID 
							Order By DriverPaymentResultID Desc";

			return pConn.Query<DriverPaymentResults>(sql, new { DriverPaymentID = pDriverPaymentID }).DefaultIfEmpty(DriverPaymentResults.GetNullEntity()).ToList();
		}

		public static List<DriverPaymentResults> GetByReferenceNo(SqlConnection pConn, string pReference) {
			string sql = @"SELECT * FROM DriverPaymentResults WiTH (NOLOCK)
                            WHERE Reference = @Reference";

			return pConn.Query<DriverPaymentResults>(sql, new { Reference = pReference }).DefaultIfEmpty(DriverPaymentResults.GetNullEntity()).ToList();
		}

	}
}
