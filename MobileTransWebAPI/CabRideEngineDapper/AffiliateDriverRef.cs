﻿using Dapper;
using System.Data.SqlClient;
using System.Linq;

namespace CabRideEngineDapper {
    public partial class AffiliateDriverRef {

        public static AffiliateDriverRef GetOrCreate(SqlConnection pConn, long pAffiliateDriverID, string pGroup, string pKey) {
            string sql = @"SELECT  *
                            FROM    dbo.AffiliateDriverRef
                            WHERE   AffiliateDriverID = @AffiliateDriverID
                                    AND ReferenceGroup = @Group
                                    AND ReferenceKey = @Key";

            AffiliateDriverRef rec = pConn.Get<AffiliateDriverRef>(sql, new { AffiliateDriverID = pAffiliateDriverID, Group = pGroup, Key = pKey });
            if (rec.IsNullEntity) {
                rec = AffiliateDriverRef.Create();
                rec.AffiliateDriverID = pAffiliateDriverID;
                rec.ReferenceGroup = pGroup;
                rec.ReferenceKey = pKey;
            }
            return rec;
        }
    }
}
