﻿using CabRideEngineDapper.Utils;
using Dapper;

namespace CabRideEngineDapper {
    public partial class CustomerRecurringBilling {


        public static CustomerRecurringBilling GetRecord(long pCustomerRecurringBillingID) {
            using (var conn = SqlHelper.OpenSqlConnection()) {
                return conn.Get<CustomerRecurringBilling>(pCustomerRecurringBillingID);
            }
        }
    }
}
