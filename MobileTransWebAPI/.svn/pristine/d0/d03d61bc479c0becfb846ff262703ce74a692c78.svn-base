﻿using CabRideEngineDapper;
using CabRideEngineDapper.Utils;
using Dapper;
using MobileTransWebAPI.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using TaxiPassCommon;

namespace MobileTransWebAPI.Controllers {
	public class FundingPayCardUnpaidDriverVouchersController : ApiController {

		public FundingPayCardInfo Get() {
			string data = "C256B79FBB7B1D1BAEB17C0320BB194118BD4CF85A88084D0704A8C9EA2AA84BF1340F97FE9800F26911FCE2B0329B532F1F655769FB0AD2E7138BDAA4E7CFFD1ED39A785C7A471F9EDEB36B03B47C141C4FC56FDEDC847A2A6AF3E6755AE4D3F55B5EE0";
			return Get(data);
		}


		// GET api/fundingpaycardunpaiddrivervouchers
		public FundingPayCardInfo Get(string pRequest) {
			FundingPayCardInfo result = new FundingPayCardInfo();

			DriverInfo driver = JsonConvert.DeserializeObject<DriverInfo>(pRequest.Decrypt());

			using (var conn = SqlHelper.OpenSqlConnection()) {
				AffiliateDrivers affDriver = AffiliateDrivers.GetDriver(conn, driver.AffiliateDriverID);
				if (affDriver.Cell != driver.Cell) {
					result.ErrorMessage = "Invalid access";
					return result;
				}

				List<string> affList = SystemDefaultsDict.GetByKey("EWRSelfRedemption", "Affiliates", false).ValueString.Split('|').ToList();
				if (affList.Count > 0) {
					if (!affList.Contains(affDriver.AffiliateID.ToString())) {
						result.ErrorMessage = "Account not authorized to self redeem vouchers";
						return result;
					}
				}

				List<SystemDefaultsDict> payCardDefaults = SystemDefaultsDict.GetGroup(conn, "PayCard", false);
				string payCardType = payCardDefaults.FirstOrDefault(p => p.ReferenceKey.Equals("ForceUpgradeTo", StringComparison.CurrentCultureIgnoreCase)).ValueString;
				if (!payCardType.IsNullOrEmpty() && !affDriver.PayCardType.StartsWith(payCardType)) {
					result.ErrorMessage = payCardDefaults.FirstOrDefault(p => p.ReferenceKey.Equals("UpgradeMessage", StringComparison.CurrentCultureIgnoreCase)).ValueString;
					return result;
				}

				AffDriverLoginInfo info = new AffDriverLoginInfo();
				info.Phone = driver.Cell;


				FundingPayCardInfoController cntr = new FundingPayCardInfoController();
				result = cntr.GetFundingInfo(JsonConvert.SerializeObject(info));
				result.VoucherList = new List<Vouchers>();
				result.HasDriversLicenseImage = !DriverHackLicense.GetLicenseByAffDriverID(conn, affDriver.AffiliateDriverID).LicenseImageURL.IsNullOrEmpty();
				result.VehicleNo = affDriver.VehicleID.HasValue ? affDriver.Vehicles.VehicleNo : Drivers.GetDriverByCell(conn, affDriver.Cell).VehicleNo;

				List<DriverPayments> voucherList = GetVoucherList(driver.AffiliateDriverID);
				foreach (DriverPayments rec in voucherList) {
					result.VoucherList.Add(new Vouchers() {
						ChargeDate = rec.ChargeDate.Value,
						TransNo = rec.TransNo,
						DriverTotal = rec.DriverTotal
					});
				}

			}

			return result;
		}

		private List<DriverPayments> GetVoucherList(long pAffiliateDriverID) {
			string sql = @"DECLARE @LookBackDays INT = (SELECT CONVERT(INT, ValueString)
														 FROM   SystemDefaultsDict
														 WHERE  ReferenceGroup = 'PayCard'
																AND ReferenceKey = 'PayDriverChargeDateMinusDays'
														)

							DECLARE @MaxAge INT = (SELECT CONVERT(INT, ValueString)
                                                         FROM   SystemDefaultsDict
                                                         WHERE  ReferenceGroup = 'PayCard'
                                                                AND ReferenceKey = 'MaxAge'
                                                        )

                            SELECT  DISTINCT
									DriverPayments.*
							FROM    dbo.DriverPayments
							LEFT JOIN ACHDetail ON ACHDetail.DriverPaymentID = DriverPayments.DriverPaymentID
							LEFT JOIN DriverPaymentAux ON DriverPaymentAux.DriverPaymentID = DriverPayments.DriverPaymentID
							WHERE   AffiliateDriverID = @AffiliateDriverID
									AND ACHPaidDate IS NULL
									AND Failed = 0
									AND Test = 0
									AND DATEADD(DAY, @LookBackDays, ChargeDate) < GETUTCDATE()
									AND ChargeDate >= '1/16/2016'
                                    AND ChargeDate > DATEADD(DAY, -@MaxAge, GETUTCDATE())
									AND ISNULL(DriverPayments.DoNotPay, 0) = 0 
									AND ISNULL(DriverPaymentAux.DoNotPay, 0) = 0
									AND TaxiPassRedeemerID IS NULL";

			using (var conn = SqlHelper.OpenSqlConnection()) {
				return conn.Query<DriverPayments>(sql, new { AffiliateDriverID = pAffiliateDriverID }).ToList();
			}
		}

	}
}
