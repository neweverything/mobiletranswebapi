﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CabRideEngineDapper.Enums {
	public enum CreditCardChargeType {
		Manual,
		Swipe,
		ECommerce,
		Any
	}
}
