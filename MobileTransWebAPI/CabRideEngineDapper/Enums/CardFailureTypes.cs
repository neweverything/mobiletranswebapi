namespace CabRideEngineDapper {
    public enum CardProcessingFailureTypes {
        OKToProcess,
        Authorized,
        AuthorizedByAnotherDriver,
        MaxChargeAmountExceeded,
        MaxCardTransactions,
        MaxDriverTransactions,
        CardUsedTooSoon,
        MaxTransPerWeek,
        MaxTransPerMonth,
        MaxTransPerWeekPerCard,
        MaxTransPerMonthPerCard,
        MaxDailyAmount,
        MaxWeeklyAmount,
        MaxMonthlyAmount,
        MissingCardNumber,
        MaxDayAmountPerCardExceeded,
        MaxWeekAmountPerCardExceeded,
        MaxMonthAmountPerCardExceeded,
        MaxDailyDeclineExceeded,
        MaxWeeklyDeclineExceeded,
        MaxMonthlyDeclineExceeded,
        AverageChargeExceeded
    }
}
