﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

using Dapper;

namespace CabRideEngineDapper {
	public class usp_CustomerDeviceLogin {
		public long CustomerDeviceID { get; set; }
		public bool DriverFrozen { get; set; }
		public decimal StartingFare { get; set; }
		public decimal ChargeMaxAmount { get; set; }
		public string TPFeeList { get; set; }
		public decimal MaxTip { get; set; }
		public double LatestAppVersion { get; set; }
		public double MinAppVersion { get; set; }
		public string Email { get; set; }
		public bool NewDevice { get; set; }

		public static usp_CustomerDeviceLogin Execute(SqlConnection pConn, DynamicParameters pParms) {
			return pConn.Query<usp_CustomerDeviceLogin>("usp_CustomerDeviceLogin", pParms, commandType: CommandType.StoredProcedure).FirstOrDefault();
		}
	}
}
