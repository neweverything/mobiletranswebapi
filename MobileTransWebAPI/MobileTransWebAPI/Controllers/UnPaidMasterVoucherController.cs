﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using System.Text;
using System.Web.Script.Serialization;

using MobileTransWebAPI.Models;

using IdeaBlade.Persistence;

using TaxiPassCommon;
using CabRideEngine;

namespace MobileTransWebAPI.Controllers {
	public class UnPaidMasterVoucherController : ApiController {
		public TransListResult GetUnPaidMasterVoucher(string pTransRequest) {

			TransListResult result = new TransListResult();
			JavaScriptSerializer js = new JavaScriptSerializer();

			WebApiApplication.LogIt("GetUnPaidMasterVoucher", pTransRequest);

			PersistenceManager oPM = WebApiApplication.UserPM;
			if (oPM == null) {
				result.ErrorMsg = "Could not connect to database";
				return result;
			}

			TransListRequest info = js.Deserialize<TransListRequest>(pTransRequest.DecryptIceKey());


			if (info.DriverID < 1) {
				result.ErrorMsg = "Invalid Access";
				return result;
			}

			AffiliateDrivers affDriver = AffiliateDrivers.GetDriver(oPM, info.AffDriverID);
			Drivers driver = Drivers.GetDriver(oPM, info.DriverID);
			if (driver.IsNullEntity) {
				result.ErrorMsg = "Invalid Driver";
				return result;
			}

			EntityList<DriverPayments> payList = null;
			if (info.GetAllForMasterVoucher) {
				payList = DriverPayments.GetAllPaymentsForMasterVoucher(driver, affDriver);
			} else {
				payList = DriverPayments.GetSwipePaymentsForMasterVoucher(driver, affDriver);
			}

			var resultList = (from p in payList
							  select String.Format("{0},{1},{2},{3}", p.ChargeDate.Value.ToString("MM/dd"),
																		p.DriverTotalWithDriverFee.ToString("C"),
																		p.TransNo,
																		p.CardNumberDisplay.IsNullOrEmpty() ? "N/A" : p.CardNumberDisplay.Right(4))).ToList();

			result.Trans = string.Join("|", resultList.ToArray());

			EntityList<usp_MasterVoucherGetUnredeemed> masterList = usp_MasterVoucherGetUnredeemed.ExecuteProc(driver);
			var mastList = (from p in masterList
							select String.Format("{0},{1},{2},{3}", p.CreatedDate.ToString("MM/dd"),
																	p.VoucherCount,
																	p.DriverTotal.Value.ToString("C"),
																	p.TransNo.Insert(3, "-").Insert(7, "-"))).ToList();

			List<string> masterVoucherIdList = (from p in masterList
												select p.TransNo).ToList();

			IdeaBlade.Persistence.Rdb.RdbQuery qry = new IdeaBlade.Persistence.Rdb.RdbQuery(typeof(MasterVoucher), MasterVoucher.TransNoEntityColumn, EntityQueryOp.In, masterVoucherIdList);
			List<long> myMasterList = (from p in oPM.GetEntities<MasterVoucher>(qry)
									   select p.MasterVoucherID).ToList();

			DateTime dStart = DateTime.Today.AddMonths(-6);
			StringBuilder sql = new StringBuilder();
			sql.AppendLine("SELECT * FROM DriverPayments ");
			sql.AppendLine("WHERE failed = 0 AND test = 0 AND DriverPaid = 0 AND Fare > 0 AND ");
			sql.AppendFormat("DriverID = {0} AND AffiliateDriverID = {1} AND ChargeDate >= '{2}'", driver.DriverID, affDriver.AffiliateDriverID, dStart.ToString("MM/dd/yyyy"));

			EntityList<DriverPayments> unPaidList = DriverPayments.GetPaymentsBySql(oPM, sql.ToString());
			var swipeList = (from p in unPaidList
							 where p.CardSwiped && !myMasterList.Contains(p.DriverPaymentAux.MasterVoucherID.GetValueOrDefault(0))
							 select String.Format("{0},{1},{2},{3}", p.ChargeDate.Value.ToString("MM/dd"),
																	 p.DriverTotal.ToString("C"),
																	 p.TransNo,
																	 p.CardNumberDisplay.Right(4))).ToList();

			var manualList = (from p in unPaidList
							  where !p.CardSwiped && !myMasterList.Contains(p.DriverPaymentAux.MasterVoucherID.GetValueOrDefault(0))
							  select String.Format("{0},{1},{2},{3}", p.ChargeDate.Value.ToString("MM/dd"),
																	  p.DriverTotal.ToString("C"),
																	  p.TransNo,
																	  p.CardNumberDisplay.Right(4))).ToList();


			result.MasterVoucher = string.Join("|", mastList.ToArray());
			result.UnPaidSwipe = string.Join("|", swipeList.ToArray());
			result.UnPaidManual = string.Join("|", manualList.ToArray());

			return result;
		}
	}
}
