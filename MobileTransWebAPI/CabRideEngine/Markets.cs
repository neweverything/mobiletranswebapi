﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

using TaxiPassCommon;
using System.Text;

namespace CabRideEngine {
    [Serializable]
    public sealed class Markets : MarketsDataRow {

        #region Constructors -- DO NOT MODIFY
        // Do not create constructors for this class
        // Developers cannot create instances of this class with the "new" keyword
        // You should write a static Create method instead; see the "Suggested Customization" region
        // Please review the documentation to learn about entity creation and inheritance

        // This private constructor prevents accidental instance creation via 'new'
        private Markets() : this(null) {
        }

        // Typed DataSet constructor (needed by framework) - DO NOT REMOVE
        public Markets(DataRowBuilder pRowBuilder)
            : base(pRowBuilder) {
        }

        #endregion

        #region Suggested Customizations

        //    // Use this factory method template to create new instances of this class
        //    public static Markets Create(PersistenceManager pManager,
        //      ... your creation parameters here ...) { 
        //
        //      Markets aMarkets = pManager.CreateEntity<Markets>();
        //
        //      // if this object type requires a unique id and you have implemented
        //      // the IIdGenerator interface implement the following line
        //      pManager.GenerateId(aMarkets, // add id column here //);
        //
        //      // Add custom code here
        //
        //      aMarkets.AddToManager();
        //      return aMarkets;
        //    }

        //    // Implement this method if you want your object to sort in a specific order
        //    public override int CompareTo(Object pObject) {
        //    }

            // Implement this method to customize the null object version of this class
            protected override void UpdateNullEntity() {
                this.Market = "All Markets";
            }

        #endregion

        // Add additional logic to your business object here...
        public static Markets Create(PersistenceManager pPM) {
            Markets oMarket = null;

            try {
                // Creates the Country but it is not yet accessible to the application
                oMarket = (Markets)pPM.CreateEntity(typeof(Markets));
                pPM.GenerateId(oMarket, Markets.MarketIDEntityColumn);

                RdbQuery oQry = new RdbQuery(typeof(Countries), Countries.DefaultEntityColumn, EntityQueryOp.EQ, true);
                Countries oCountry = pPM.GetEntity<Countries>(oQry);
                oMarket.Country = oCountry.Country;

                oMarket.AddToManager();

            } catch (Exception ex) {
                throw ex;
            }
            return oMarket;
        }

        public override string Market {
            get {
                return base.Market;
            }
            set {
				value = StringUtils.ProperCase(value.Trim());
				if (value == "") {
					throw new Exception("Market cannot be blank");
				}
                RdbQuery oQry = new RdbQuery(typeof(Markets),MarketEntityColumn, EntityQueryOp.EQ, value);
                oQry.AddClause(MarketIDEntityColumn, EntityQueryOp.NE, MarketID);
                if (PersistenceManager.GetEntities<Markets>(oQry).Count > 0) {
                    throw new Exception("Market must be unique");
                }
                base.Market = value;
            }
        }

        public override EntityValidationInfo Validate(EntityValidationContext pContext) {
            EntityValidationInfo oResult = base.Validate(pContext);
            if (!oResult.IsValid) {
                return oResult;
            }
            if (Market == "") {
                oResult = new EntityValidationInfo(false, "Market must contain a value");
                //throw new Exception(oResult.Message);
            }
            return oResult;

        }

        public EntityValidationInfo ValidateMarket(string pNewMarket) {
            if (pNewMarket.Length == 0) {
                RowError = "Error";
                return new EntityValidationInfo(false, "Market can not be blank");
            }
            if ((RowState == DataRowState.Added) || (RowState == DataRowState.Modified && this.PrimaryKeyHasChanged)) {
                RdbQuery oQry = new RdbQuery(typeof(Markets), MarketEntityColumn, EntityQueryOp.EQ, pNewMarket);
                if (this.PersistenceManager.GetEntities<Markets>(oQry).Count > 1) {
                    this.SetColumnError("Market", Market + " already exists, value must be unique.");
                    return new EntityValidationInfo(false, Market + " already exists, value must be unique.");
                }
            }
            return new EntityValidationInfo(true, "");
        }

        public override string ModifiedBy {
            get {
                return base.ModifiedBy;
            }
            set {
                base.ModifiedBy = value;
            }
        }

        protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
            if (IsDeserializing)
                return;
            base.OnColumnChanging(pArgs);
            DoAudit(pArgs);
        }

        private void DoAudit(DataColumnChangeEventArgs pArgs) {
            if (!this.PersistenceManager.IsClient) {
                return;
            }
            if (this.RowState == DataRowState.Added) {
                return;
            }
            if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
                AuditRecord = MarketsAudit.Create(this.PersistenceManager, this);
            }
            //AuditRecord[pArgs.Column.ColumnName] = pArgs.ProposedValue;
        }

		// ******************************************************************************
		// Get all Markets in Market order
		// ******************************************************************************
        public static EntityList<Markets> GetMarkets(PersistenceManager pPM) {
			return GetData(pPM,"","");
        }

		// ******************************************************************************
		// Get a single Market
		// ******************************************************************************
        public static Markets GetMarket(PersistenceManager pPM, string pMarket) {
			EntityList<Markets> oMarkets = GetData(pPM, pMarket, "");
			return oMarkets[0];
        }

		// ******************************************************************************
		// Get all Markets in a Region
		// ******************************************************************************
		public static EntityList<Markets> GetMarketByRegion(PersistenceManager pPM, long pRegionID) {
			EntityList<Markets> oMarkets = GetData(pPM, "", pRegionID.ToString());
			return oMarkets;
		}

		// ******************************************************************************
		// Call the proc
		// ******************************************************************************
		private static EntityList<Markets> GetData(PersistenceManager pPM, string pMarket, string pRegionID) {
			string sDelimiter = "";
			int iArgCount = 0;
			StringBuilder sql = new StringBuilder();
			sql.Append("usp_MarketsGet ");
			if (!pMarket.IsNullOrEmpty()) {
				sql.AppendFormat(" @Market = '{0}'", pMarket);
				iArgCount++;
			}
			if (!pRegionID.IsNullOrEmpty()) {
				if (iArgCount > 0) {
					sDelimiter = ", ";
				}
				sql.AppendFormat("{0} @RegionID = {1}", sDelimiter, pRegionID);
				iArgCount++;
			}

			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(Markets), sql.ToString());
			EntityList<Markets> oMarkets = pPM.GetEntities<Markets>(qry);
			if (oMarkets.Count == 0) {
				oMarkets.Add(new Markets());
			}
			return oMarkets;
		}



		
		// ************************************************************************
		// Standardize the timezone entries
		// ************************************************************************
		public override string TimeZone {
			get {
				string myTimeZone = base.TimeZone;
				if (TimeZones.GetAll()[0].DisplayName.Contains("GMT")) {
					if (!myTimeZone.IsNullOrEmpty()) {
						myTimeZone = myTimeZone.Replace("UTC", "GMT");
					}
				} else {
					if (!myTimeZone.IsNullOrEmpty()) {
						myTimeZone = myTimeZone.Replace("GMT", "UTC");
					}
				}
				return myTimeZone;
			}
			set {
				base.TimeZone = value;
			}
		}
    }

}