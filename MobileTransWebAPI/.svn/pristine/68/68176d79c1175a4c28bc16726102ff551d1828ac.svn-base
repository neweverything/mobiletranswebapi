﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using System.Text;
using System.Web.Script.Serialization;

using MobileTransWebAPI.Models;

using IdeaBlade.Persistence;

using TaxiPassCommon;
using CabRideEngine;

namespace MobileTransWebAPI.Controllers {

	public class TransHistoryController : ApiController {

		//public TransListResult Get() {
		//    return GetTransHistory("BF6ADF1F4498D56879019944D0EF999084973F967DCEFF77663E1C1324DFE4EDAA67567C845B5EBF9B8D1772EA9C1534AD0814C03FC1BBE3C899B3821BB82DC3540475CE88A80D782C9A8C7BD547C0BFF8542926ADE91501F6E279D067431B88887511BE6E3B095F87D473641DC73BF24A725D98ABA20565914A81A20BC08AC7B6351BBFDFC577A9FD696F87433A8F8C7D96EB4C");
		//}



		public TransListResult GetTransHistory(string pTransRequest) {
			TransListResult result = new TransListResult();
			JavaScriptSerializer js = new JavaScriptSerializer();

			WebApiApplication.LogIt("GetTransHistory", pTransRequest);

			PersistenceManager oPM = WebApiApplication.UserPM;
			if (oPM == null) {
				result.ErrorMsg = "Could not connect to database";
				return result;
			}

			TransListRequest info = js.Deserialize<TransListRequest>(pTransRequest.DecryptIceKey());

			if (info.DriverID < 1) {
				result.ErrorMsg = "Invalid Access";
				return result;
			}

			Drivers oDriver = Drivers.GetDriver(oPM, info.DriverID);
			if (oDriver.IsNullEntity) {
				result.ErrorMsg = "Invalid Driver";
				return result;
			}

			try {
				DateTime requestDate = DateTime.Today.AddDays(-1);
				if (info != null) {
					requestDate = Convert.ToDateTime(info.RequestKey.DecryptIceKey());
				}

				if (Math.Abs(DateTime.Now.ToUniversalTime().Subtract(requestDate).TotalMinutes) > 1) {
					result.ErrorMsg = "Invalid Access";
					return result;
				}
			} catch (Exception) {
				result.ErrorMsg = "Invalid Access";
				return result;
			}


			AffiliateDrivers affDriver = AffiliateDrivers.GetDriver(oPM, info.AffDriverID);
			EntityList<DriverPayments> oList = new EntityList<DriverPayments>();
			int month = DateTime.Now.Month;
			int year = DateTime.Now.Year;
			DateTime dStart = new DateTime(year, month, 1);
			DateTime dEnd = dStart.AddMonths(1).AddSeconds(-1); // dStart.AddMonths(1).AddSeconds(-1);
			if (affDriver.IsNullEntity) {
				if (!info.CurrentMonth) {
					dStart = dStart.AddMonths(-1);
					dEnd = dStart.AddMonths(1).AddSeconds(-1);
				}
				oList.AddRange(DriverPayments.GetCharges(oDriver, dStart, dEnd, false));
			} else {
				if (!info.CurrentMonth) {
					dStart = dStart.AddMonths(-1);
					dEnd = dStart.AddMonths(1).AddSeconds(-1);
				}

				oList.AddRange(DriverPayments.GetCharges(affDriver, dStart, dEnd, false));

			}

			oList = new EntityList<DriverPayments>((from p in oList
													where !p.Test && !p.Failed
													select p).ToList());

			StringBuilder sPaid = new StringBuilder();
			StringBuilder sTrans = new StringBuilder();
			bool addDriverFee = oDriver.MyAffiliate.AffiliateDriverDefaults.CarmelDispatch;

			foreach (DriverPayments oPay in oList) {
				decimal nTotal = oPay.DriverTotalPaid; //		oPay.TotalCharge - oPay.TaxiPassFee - oPay.DriverFee.GetValueOrDefault(0);
				if (addDriverFee) {
					nTotal += oPay.DriverFee.GetValueOrDefault(0);
				}
				string sText = String.Format("{0},{1},{2},{3}|", oPay.ChargeDate.Value.ToString("MM/dd"), nTotal.ToString("C"), oPay.Test ? "Test" : oPay.TransNo, oPay.CardNumberDisplay.IsNullOrEmpty() ? "N/A" : oPay.CardNumberDisplay.Right(4));
				//sOutput.Append(StringUtils.XMLTag("Rec" + i.ToString(), sText));

				if (oPay.ACHDetail.ACHPaidDate.HasValue || oPay.TaxiPassRedeemerID.HasValue || oPay.DriverPaid) {
					sPaid.Append(sText);
				} else {
					sTrans.Append(sText);
				}
			}
			if (sPaid.Length > 2) {
				result.PaidTrans = sPaid.Remove(sPaid.Length - 1, 1).ToString();
			}
			if (sTrans.Length > 2) {
				result.Trans = sTrans.Remove(sTrans.Length - 1, 1).ToString();
			}

			return result;
		}


		public TransListResult GetTransHistoryWithBankPending(string pRequest) {
			TransListResult result = new TransListResult();
			JavaScriptSerializer js = new JavaScriptSerializer();

			WebApiApplication.LogIt("GetTransHistoryWithBankPending", pRequest);

			PersistenceManager oPM = WebApiApplication.UserPM;
			if (oPM == null) {
				result.ErrorMsg = "Could not connect to database";
				return result;
			}

			Drivers oDriver = oPM.GetNullEntity<Drivers>();
			TransListRequest info = new TransListRequest();
			try {
				info = js.Deserialize<TransListRequest>(pRequest.DecryptIceKey());

				if (info.DriverID < 1) {
					result.ErrorMsg = "Invalid Access";
					return result;
				}

				oDriver = Drivers.GetDriver(oPM, info.DriverID);
				if (oDriver.IsNullEntity) {
					result.ErrorMsg = "Invalid Driver";
					return result;
				}

				DateTime requestDate = DateTime.Today.AddDays(-1);
				if (info != null) {
					requestDate = Convert.ToDateTime(info.RequestKey.DecryptIceKey());
				}

				if (Math.Abs(DateTime.Now.ToUniversalTime().Subtract(requestDate).TotalMinutes) > 1) {
					result.ErrorMsg = "Invalid Access";
					return result;
				}
			} catch (Exception) {
				result.ErrorMsg = "Invalid Access";
				return result;
			}


			AffiliateDrivers affDriver = AffiliateDrivers.GetDriver(oPM, info.AffDriverID);
			EntityList<DriverPayments> oList = new EntityList<DriverPayments>();
			int month = DateTime.Now.Month;
			int year = DateTime.Now.Year;
			DateTime dStart = new DateTime(year, month, 1);
			DateTime dEnd = dStart.AddMonths(1).AddSeconds(-1); // dStart.AddMonths(1).AddSeconds(-1);
			if (affDriver.IsNullEntity) {
				if (!info.CurrentMonth) {
					dStart = dStart.AddMonths(-1);
					dEnd = dStart.AddMonths(1).AddSeconds(-1);
				}
				oList.AddRange(DriverPayments.GetCharges(oDriver, dStart, dEnd, false));
			} else {
				if (!info.CurrentMonth) {
					dStart = dStart.AddMonths(-1);
					dEnd = dStart.AddMonths(1).AddSeconds(-1);
				}

				oList.AddRange(DriverPayments.GetCharges(affDriver, dStart, dEnd, false));

			}

			oList = new EntityList<DriverPayments>((from p in oList
													where !p.Test && !p.Failed
													select p).ToList());

			StringBuilder sPaid = new StringBuilder();
			StringBuilder sTrans = new StringBuilder();
			StringBuilder sPending = new StringBuilder();

			bool addDriverFee = oDriver.MyAffiliate.AffiliateDriverDefaults.CarmelDispatch;

			foreach (DriverPayments oPay in oList) {
				decimal nTotal = oPay.DriverTotalPaid; //		oPay.TotalCharge - oPay.TaxiPassFee - oPay.DriverFee.GetValueOrDefault(0);
				if (addDriverFee) {
					nTotal += oPay.DriverFee.GetValueOrDefault(0);
				}
				string sText = String.Format("{0},{1},{2},{3}|", oPay.ChargeDate.Value.ToString("MM/dd"), nTotal.ToString("C"), oPay.Test ? "Test" : oPay.TransNo, oPay.CardNumberDisplay.IsNullOrEmpty() ? "N/A" : oPay.CardNumberDisplay.Right(4));
				//sOutput.Append(StringUtils.XMLTag("Rec" + i.ToString(), sText));

				if (!oPay.TaxiPassRedeemerID.HasValue && oPay.DriverPaid && (!oPay.ACHDetail.ACHPaidDate.HasValue || oPay.ACHDetail.ACHPaidDate.GetValueOrDefault(DateTime.Today.AddMonths(-1)) == DateTime.Today)) {
					sPending.Append(sText);
				} else if (oPay.ACHDetail.ACHPaidDate.HasValue || oPay.TaxiPassRedeemerID.HasValue || oPay.DriverPaid) {
					sPaid.Append(sText);
				} else {
					sTrans.Append(sText);
				}
			}
			if (sPaid.Length > 2) {
				result.PaidTrans = sPaid.Remove(sPaid.Length - 1, 1).ToString();
			}
			if (sTrans.Length > 2) {
				result.Trans = sTrans.Remove(sTrans.Length - 1, 1).ToString();
			}
			if (sPending.Length > 2) {
				result.PendingTrans = sPending.Remove(sPending.Length - 1, 1).ToString();
			}

			return result;
		}

	}
}
