﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Util;

namespace CabRideEngine {
    [Serializable]
    public sealed class Payments : PaymentsDataRow {

        #region Constructors -- DO NOT MODIFY
        // Do not create constructors for this class
        // Developers cannot create instances of this class with the "new" keyword
        // You should write a static Create method instead; see the "Suggested Customization" region
        // Please review the documentation to learn about entity creation and inheritance

        // This private constructor prevents accidental instance creation via 'new'
        private Payments()
            : this(null) {
        }

        // Typed DataSet constructor (needed by framework) - DO NOT REMOVE
        public Payments(DataRowBuilder pRowBuilder)
            : base(pRowBuilder) {
        }

        #endregion

        #region Suggested Customizations

        //    // Use this factory method template to create new instances of this class
        //    public static Payments Create(PersistenceManager pManager,
        //      ... your creation parameters here ...) { 
        //
        //      Payments aPayments = pManager.CreateEntity<Payments>();
        //
        //      // if this object type requires a unique id and you have implemented
        //      // the IIdGenerator interface implement the following line
        //      pManager.GenerateId(aPayments, // add id column here //);
        //
        //      // Add custom code here
        //
        //      aPayments.AddToManager();
        //      return aPayments;
        //    }

        //    // Implement this method if you want your object to sort in a specific order
        //    public override int CompareTo(Object pObject) {
        //    }

        //    // Implement this method to customize the null object version of this class
        //    protected override void UpdateNullEntity() {
        //    }

        #endregion

        // Add additional logic to your business object here...

        protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
            if (IsDeserializing)
                return;
            base.OnColumnChanging(pArgs);
            DoAudit(pArgs);
        }

        private void DoAudit(DataColumnChangeEventArgs pArgs) {
            if (!this.PersistenceManager.IsClient) {
                return;
            }
            if (this.RowState == DataRowState.Added) {
                return;
            }
            if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
                AuditRecord = PaymentsAudit.Create(this.PersistenceManager, this);
            }

        }

        public static Payments Create(PersistenceManager pPM, long pDepositID) {
            Payments oPayment = null;

            try {
                oPayment = (Payments)pPM.CreateEntity(typeof(Payments));
                pPM.GenerateId(oPayment, Payments.PaymentIDEntityColumn);
                oPayment.DepositID = pDepositID;
                oPayment.AddToManager();

            } catch (Exception ex) {
                throw ex;
            }
            return oPayment;
        }

    }

}
