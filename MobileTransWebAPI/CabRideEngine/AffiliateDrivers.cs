﻿using CabRideEngine.Utils;
using Csla.Validation;
using IdeaBlade.Persistence;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using TaxiPassCommon;
using TaxiPassCommon.Banking;



namespace CabRideEngine {
	/// <summary>
	/// The developer class representing the AffiliateDrivers business object class.
	/// </summary>
	/// <remarks>
	/// <para>Place all application-specific business logic in this class.</para>
	/// <para>
	/// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
	/// and will not overwrite changes.
	/// </para>
	/// <para>
	/// This class inherits from the generated class, its companion in the pair of classes 
	/// defining a business object. Properties in the generated class may be overridden in 
	/// order to change the behavior or add business logic.
	/// </para>
	/// <para>
	/// This class is the final class in the inheritance chain for this business object. 
	/// It should not be inherited.
	/// </para>
	/// </remarks>
	[Serializable]
	public sealed partial class AffiliateDrivers : AffiliateDriversDataRow {

		#region Constructors -- DO NOT MODIFY

		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new';
		// it is also required if the class is visible from a web service.
		// DO NOT REMOVE **
		private AffiliateDrivers()
			: this(null) {
		}

		/// <summary>
		/// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
		/// </summary>
		/// <param name="pRowBuilder"></param>
		public AffiliateDrivers(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static AffiliateDrivers Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      AffiliateDrivers aAffiliateDrivers = pManager.CreateEntity<AffiliateDrivers>();
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aAffiliateDrivers, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aAffiliateDrivers.AddToManager();
		//      return aAffiliateDrivers;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		// Add additional logic to your business object here...
		#region Standard Entity logic to create/audit/validate the entity

		// Add additional logic to your business object here...
		public static AffiliateDrivers Create(Affiliate pAffiliate) {
			PersistenceManager oPM = pAffiliate.PersistenceManager;

			AffiliateDrivers oDriver = null;

			try {
				oDriver = oPM.CreateEntity<AffiliateDrivers>();

				// Using the IdeaBlade Id Generation technique
				oPM.GenerateId(oDriver, AffiliateDrivers.AffiliateDriverIDEntityColumn);
				if (!pAffiliate.IsNullEntity) {
					oDriver.AffiliateID = pAffiliate.AffiliateID;
				}
				oDriver.Country = pAffiliate.Country;
				oDriver.State = pAffiliate.State;
				oDriver.LicenseState = pAffiliate.State;
				oDriver.PaidByRedeemer = false;
				oDriver.PaymentMethod = (int)Utils.PaymentMethod.Fleet;
				oDriver.SignUpDate = DateTime.Now;
				if (!oDriver.Affiliate.TimeZone.IsNullOrEmpty()) {
					TimeZoneConverter convert = new TimeZoneConverter();
					oDriver.SignUpDate = convert.ConvertTime(DateTime.Now, System.TimeZone.CurrentTimeZone.StandardName, oDriver.Affiliate.TimeZone);
				}
				oDriver.PayCardRealTimePay = false;
				oDriver.AddToManager();

			} catch (Exception ex) {
				throw ex;
			}
			return oDriver;
		}

		protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
			if (IsDeserializing) {
				return;
			}
			base.OnColumnChanging(pArgs);
			DoAudit(pArgs);
		}

		private void DoAudit(DataColumnChangeEventArgs pArgs) {
			if (!this.PersistenceManager.IsClient) {
				return;
			}
			if (this.RowState == DataRowState.Added) {
				return;
			}
			if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
				AuditRecord = AffiliateDriversAudit.Create(this.PersistenceManager, this);
			}
		}

		protected override void AddRules(RuleList pList) {
			PropertyRequiredRule.AddToList(pList, NameColumn.ColumnName);
			PropertyRequiredRule.AddToList(pList, CellEntityColumn.ColumnName);
			//PropertyRequiredRule.AddToList(pList, LicenseNoEntityColumn.ColumnName);
			pList.Add(IsCellUnique, CellEntityColumn.ColumnName);
			//pList.Add(IsLicenseNoUnique, LicenseNoEntityColumn.ColumnName);
			pList.Add(IsCallInCellNoUnique, CallInCellEntityColumn.ColumnName);

		}

		public override SaveResult Save() {

			if (this.RowState == DataRowState.Added && this.RedeemerStopPayment && this.Affiliate.AffiliateDriverDefaults.NewDriversExcludedFromRedeemerStopPayment.GetValueOrDefault(false)) {
				RedeemerStopPayment = false;
			}

			SaveResult result = base.Save();

			if (mWidgetKey != null && !mWidgetKey.ValueString.IsNullOrEmpty()) {
				mWidgetKey.Save();
			}
			if (mDriverHackLicense != null) {
				mDriverHackLicense.Save();
			}
			if (mAffiliateDriverLicense != null) {
				mAffiliateDriverLicense.Save();
			}

			return result;
		}

		public override SaveResult Save(string pModifiedBy) {
			if (this.RowState == DataRowState.Added && this.RedeemerStopPayment && this.Affiliate.AffiliateDriverDefaults.NewDriversExcludedFromRedeemerStopPayment.GetValueOrDefault(false)) {
				RedeemerStopPayment = false;
			}

			if (this.RowState == DataRowState.Added && this.RedeemerStopPayment && this.Affiliate.AffiliateDriverDefaults.NewDriversExcludedFromRedeemerStopPayment.GetValueOrDefault(false)) {
				RedeemerStopPayment = false;
			}
			SaveResult result = base.Save(pModifiedBy);

			if (mWidgetKey != null && !mWidgetKey.ValueString.IsNullOrEmpty()) {
				if (mWidgetKey.AffiliateDriverID == 0) {
					mWidgetKey.AffiliateDriverID = AffiliateDriverID;
				}
				mWidgetKey.Save();
			}
			if (mDriverHackLicense != null) {
				if (mDriverHackLicense.AffiliateDriverID == 0) {
					mDriverHackLicense.AffiliateDriverID = AffiliateDriverID;
				}
				mDriverHackLicense.Save();
			}
			if (mAffiliateDriverLicense != null) {
				if (mAffiliateDriverLicense.AffiliateDriverID <= 0) {
					mAffiliateDriverLicense.AffiliateDriverID = AffiliateDriverID;
				}
				mAffiliateDriverLicense.Save();
			}

			return result;
		}

		private bool IsCellUnique(object pTarget, RuleArgs e) {
			AffiliateDrivers oDriver = pTarget as AffiliateDrivers;
			if (string.IsNullOrEmpty(oDriver.Cell)) {
				return false;
			}
			if (oDriver.Cell.StartsWith("CELL", StringComparison.CurrentCultureIgnoreCase)) {
				return true;
			}
			RdbQuery qry = new RdbQuery(typeof(AffiliateDrivers), AffiliateDrivers.CellEntityColumn, EntityQueryOp.EQ, oDriver.Cell);
			qry.AddClause(AffiliateDrivers.AffiliateIDEntityColumn, EntityQueryOp.EQ, oDriver.AffiliateID);
			qry.AddClause(AffiliateDrivers.AffiliateDriverIDEntityColumn, EntityQueryOp.NE, oDriver.AffiliateDriverID);
			EntityList<AffiliateDrivers> oList = this.PersistenceManager.GetEntities<AffiliateDrivers>(qry);
			if (oList.Count > 0 && oDriver.Active) {
				e.Description = "A driver already exists with the Cell Phone: " + oDriver.Cell.FormattedPhoneNumber();
				return false;
			}

			if (oDriver.Cell.Length == 10) {
				qry = new RdbQuery(typeof(AffiliateDrivers), AffiliateDrivers.CellEntityColumn, EntityQueryOp.EQ, oDriver.Cell);
				qry.AddClause(AffiliateDrivers.AffiliateDriverIDEntityColumn, EntityQueryOp.NE, oDriver.AffiliateDriverID);
				qry.AddClause(AffiliateDrivers.ActiveEntityColumn, EntityQueryOp.EQ, true);
				oList = this.PersistenceManager.GetEntities<AffiliateDrivers>(qry);
				if (oList.Count > 0 && oDriver.Active) {
					e.Description = "A driver already exists with the Cell Phone: " + oDriver.Cell.FormattedPhoneNumber() + " See Fleet: " + oList[0].Affiliate.Name;
					return false;
				}
			}

			return true;
		}

		//private bool IsLicenseNoUnique(object pTarget, RuleArgs e) {
		//    AffiliateDrivers oDriver = pTarget as AffiliateDrivers;
		//    if (string.IsNullOrEmpty(oDriver.LicenseNo)) {
		//        return true;
		//    }
		//    RdbQuery qry = new RdbQuery(typeof(AffiliateDrivers), AffiliateDrivers.LicenseNoEntityColumn, EntityQueryOp.EQ, oDriver.LicenseNo);
		//    qry.AddClause(AffiliateDrivers.AffiliateDriverIDEntityColumn, EntityQueryOp.NE, oDriver.AffiliateDriverID);
		//    EntityList<AffiliateDrivers> oList = this.PersistenceManager.GetEntities<AffiliateDrivers>(qry);
		//    if (oList.Count > 0) {
		//        e.Description = "A driver already exists with this License No: " + oDriver.LicenseNo;
		//        return false;
		//    }
		//    return true;
		//}

		private bool IsCallInCellNoUnique(object pTarget, RuleArgs e) {
			AffiliateDrivers oDriver = pTarget as AffiliateDrivers;
			if (string.IsNullOrEmpty(oDriver.CallInCell)) {
				return true;
			}
			RdbQuery qry = new RdbQuery(typeof(AffiliateDrivers), AffiliateDrivers.CallInCellEntityColumn, EntityQueryOp.EQ, oDriver.CallInCell);
			qry.AddClause(AffiliateDrivers.CellEntityColumn, EntityQueryOp.EQ, oDriver.CallInCell);
			qry.AddOperator(EntityBooleanOp.Or);
			qry.AddClause(AffiliateDrivers.ActiveEntityColumn, EntityQueryOp.EQ, true);
			qry.AddClause(AffiliateDrivers.AffiliateDriverIDEntityColumn, EntityQueryOp.NE, oDriver.AffiliateDriverID);
			EntityList<AffiliateDrivers> oList = this.PersistenceManager.GetEntities<AffiliateDrivers>(qry);
			foreach (AffiliateDrivers rec in oList) {
				if (rec.AffiliateDriverID != oDriver.AffiliateDriverID && oDriver.Active) {
					e.Description = "A driver already exists with the Cell Phone: " + oDriver.Cell.FormattedPhoneNumber();
					e.Description += ".  See Fleet: " + rec.Affiliate.Name + ", Driver: " + rec.Name;
					return false;
				}
			}
			return true;
		}

		// ************************************************
		// return the base.FraudSuspected
		// ************************************************
		public bool GetFraudSuspected() {
			return base.FraudSuspected;
		}

		// ***************************************************
		// 8/13/2012 Fraud checking got a bit more complicated
		// 11/27/2012 Uncomplicated 
		// ***************************************************
		/*
		public override bool FraudSuspected {
			get {
				return Utils.FraudCheck.DriverFraudCheck(this.PersistenceManager, this, null);
			}
			set {
				base.FraudSuspected = value;
			}
		}
		*/
		#endregion

		#region Override Fields
		public override long? DriverStatusID {
			get {
				return base.DriverStatusID;
			}
			set {
				if (base.DriverStatusID != value) {
					DriverStatusUpdated = DateTime.Now;
					DriverStatusUpdatedBy = LoginManager.PrincipleUser.Identity.Name;
				}

				base.DriverStatusID = value;
			}
		}


		public override DateTime? DriverStatusUpdated {
			get {
				return base.DriverStatusUpdated;
			}
			set {
				if (this.Affiliate.TimeZone.IsNullOrEmpty() || !value.HasValue) {
					base.DriverStatusUpdated = value;
				} else {
					TimeZoneConverter convert = new TimeZoneConverter();
					if (value.HasValue) {
						base.DriverStatusUpdated = convert.ConvertTime(value.Value, TimeZone.CurrentTimeZone.StandardName, this.Affiliate.TimeZone);
					}
				}
			}
		}

		public override bool RedeemerStopPayment {
			get {
				return base.RedeemerStopPayment;
			}
			set {
				try {
					base["RedeemerStopPayment"] = value;
					if (value) {
						TimeZoneConverter convert = new TimeZoneConverter();
						if (this.Affiliate.TimeZone.IsNullOrEmpty()) {
							base.StopPaymentDate = DateTime.Now;
						} else {
							base.StopPaymentDate = convert.ConvertTime(DateTime.Now, TimeZone.CurrentTimeZone.StandardName, this.Affiliate.TimeZone);
						}
					}
				} catch (Exception ex) {
					Console.WriteLine(ex.Message);
				}
			}
		}
		#endregion

		#region Custom Fields

		[BindingBrowsable(false)]
		public override string EntityInstanceDisplayName {
			get {
				return Name;
			}
		}

		public string FormattedCellPhone {
			get {
				return this.Cell.FormattedPhoneNumber();
			}
		}

		public string FormattedCallInCell {
			get {
				return this.CallInCell.FormattedPhoneNumber();
			}
		}

		public long CellIdAsLong {
			get {
				return Convert.ToInt64(Cell);
			}
		}

		// *************************************************************************************
		// Return Vehicle Number or set Vehicle ID
		// *************************************************************************************
		public string VehicleNo {

			get {
				EntityList<Vehicles> oVehicleList = Vehicles.GetActiveVehicles(this.Affiliate);
				var vNumber = (from p in oVehicleList
							   where p.VehicleID == this.VehicleID
							   select p.VehicleNo).Take(1).ToList();
				if (vNumber.Count > 0) {
					return vNumber[0];
				} else {
					return "";
				}

			}
			set {
				EntityList<Vehicles> oVehicleList = Vehicles.GetActiveVehicles(this.Affiliate);
				var vID = (from p in oVehicleList
						   where p.VehicleNo == value
						   select p.VehicleID).Take(1).ToList();
				if (vID.Count > 0) {
					base.VehicleID = long.Parse(vID[0].ToString());
				} else {
					base.VehicleID = null;
				}
			}
		}

		private decimal mDriverTotal = 0;
		private decimal mTPFees = 0;

		public long TransTotalCount {
			get {
				int total = 0;
				mDriverTotal = 0;
				mTPFees = 0;
				foreach (DriverPayments rec in DriverPaymentses) {
					if (!rec.Test && !rec.Failed) {
						total++;
						mDriverTotal += rec.DriverTotal;
						mTPFees += rec.TaxiPassFee;
					}
				}
				return total;
			}
		}

		public decimal TransDriverTotals {
			get {
				if (mDriverTotal == 0) {
					long i = TransTotalCount;
				}

				return mDriverTotal;
			}
		}

		public decimal TransTPFeesTotal {
			get {
				if (mDriverTotal == 0) {
					long i = TransTotalCount;
				}
				return mTPFees;
			}
		}

		public string MyLanguage {
			get {
				return this.Languages.Name;
			}
		}

		public string MyAffiliateName {
			get {
				return this.Affiliate.Name;
			}
		}

		public string MyVehicle {
			get {
				return this.Vehicles.VehicleNo;
			}
		}

		public string MyVehicleSuspended {
			get {
				return this.Vehicles.Suspended ? "Yes" : "";
			}
		}

		public string SuspendedGridView {
			get {
				return Suspended ? "Yes" : "";
			}
		}

		public bool Suspended {
			get {
				if (string.IsNullOrEmpty(this.Affiliate.LicenseNumber)) {
					return false;
				}
				MasterDrivers oDriver = MasterDrivers.GetDriverByLicenseNo(this.Affiliate, this.TAPermit);
				return oDriver.IsNullEntity || oDriver.LicenseExpiration < DateTime.Today;
			}
		}

		public bool NoImprint {
			get {
				return (this.DriverPaymentNoImprints.Count > 0);
			}
		}


		public string TempNotes { get; set; }

		public string DriverLicenseHTMLImageTag {
			get {
				if (this.AffiliateDriverLicenses.IsNullEntity) {
					return "";
				}
				return "<a href='" + this.AffiliateDriverLicenses.MyImageURL + "' target='_blank'>Driver License</a>";
			}
		}

		public string TransCardType {
			get {
				if (TransCardCardNumber.IsNullOrEmpty()) {
					return "";
				}
				return Utils.CreditCardUtils.DetermineCardType(this.PersistenceManager, TransCardNumber);
			}
		}

		//public string MiddleName { get; set; }
		//public string LastName { get; set; }

		//public string FirstName {
		//	get {
		//		string firstName = "";

		//		if (this.Name.Contains(",")) {
		//			List<string> nameList = this.Name.Split(',').ToList();
		//			LastName = nameList[0];
		//			for (int i = 1; i < nameList.Count; i++) {
		//				if (nameList.Count - 1 == i) {
		//					firstName = nameList[i];
		//				} else {
		//					MiddleName += nameList[i] + " ";
		//				}
		//			}
		//		} else {
		//			List<string> nameList = this.Name.Split(' ').ToList();
		//			firstName = nameList[0];
		//			for (int i = 1; i < nameList.Count; i++) {
		//				if (nameList.Count - 1 == i) {
		//					LastName = nameList[i];
		//				} else {
		//					MiddleName += nameList[i] + " ";
		//				}
		//			}
		//		}
		//		return firstName;
		//	}
		//}

		#endregion

		#region Retrieve Records

		// **************************************************************************************
		// Get a Driver by ID
		// **************************************************************************************
		public static AffiliateDrivers GetDriver(PersistenceManager pPM, long pAffiliateDriverID) {
			EntityList<AffiliateDrivers> oAffDrivers = GetData(pPM, pAffiliateDriverID.ToString(), "", "", "");
			if (oAffDrivers.Count == 0) {
				return pPM.GetNullEntity<AffiliateDrivers>();
			} else {
				return oAffDrivers[0];
			}
		}

		// ****************************************************
		// Get a driver by Affiliate and name
		// ****************************************************
		public static AffiliateDrivers GetDriverByName(Affiliate pAffiliate, string pName) {
			EntityList<AffiliateDrivers> oAffDrivers = GetData(pAffiliate.PersistenceManager, "", pAffiliate.AffiliateID.ToString(), pName, "");
			if (oAffDrivers.Count == 0) {
				return pAffiliate.PersistenceManager.GetNullEntity<AffiliateDrivers>();
			} else {
				return oAffDrivers[0];
			}
		}

		public static AffiliateDrivers GetDriverByName(PersistenceManager pPM, long pAffiliateID, string pName) {
			EntityList<AffiliateDrivers> oAffDrivers = GetData(pPM, "", pAffiliateID.ToString(), pName, "");
			if (oAffDrivers.Count == 0) {
				return pPM.GetNullEntity<AffiliateDrivers>();
			} else {
				return oAffDrivers[0];
			}
		}


		// ****************************************************
		// Get a drivers by name
		// ****************************************************
		public static EntityList<AffiliateDrivers> GetDriversByName(PersistenceManager pPM, string pName) {
			return GetData(pPM, "", "", pName, "");
		}

		// ****************************************************
		// Get a drivers by AffiliateID and cell
		// ****************************************************
		public static AffiliateDrivers GetDriverByCellPhone(Affiliate pAffiliate, string pCellPhone) {
			return GetDriverByCellPhone(pAffiliate.PersistenceManager, pAffiliate.AffiliateID, pCellPhone);
		}

		// ****************************************************
		// Get a drivers by AffiliateID and cell
		// ****************************************************
		public static AffiliateDrivers GetDriverByCellPhone(PersistenceManager pPM, long pAffiliateID, string pCellPhone) {
			if (string.IsNullOrEmpty(pCellPhone)) {
				return pPM.GetNullEntity<AffiliateDrivers>();
			}

			EntityList<AffiliateDrivers> oAffDrivers = GetData(pPM, "", pAffiliateID.ToString(), "", pCellPhone);
			if (oAffDrivers.Count == 0) {
				return pPM.GetNullEntity<AffiliateDrivers>();
			} else {
				return oAffDrivers[0];
			}
		}


		// ****************************************************
		// Get a driver by cell 
		// ****************************************************
		public static EntityList<AffiliateDrivers> GetDriversByCellPhone(PersistenceManager pPM, string pCellPhone) {
			if (string.IsNullOrEmpty(pCellPhone)) {
				return new EntityList<AffiliateDrivers>();
			}
			return GetData(pPM, "", "", "", pCellPhone);
		}


		// ****************************************************
		// Get a driver by cell and pin
		// ****************************************************
		public static AffiliateDrivers GetDriverByCellPhone(PersistenceManager pPM, string pCellPhone, string pPin) {
			if (string.IsNullOrEmpty(pCellPhone) || string.IsNullOrEmpty(pPin)) {
				return pPM.GetNullEntity<AffiliateDrivers>();
			}
			EntityList<AffiliateDrivers> oAffDrivers = GetDriversByCellPhone(pPM, pCellPhone);
			foreach (AffiliateDrivers oRec in oAffDrivers) {
				if (oRec.Pin == pPin) {
					return oRec;
				}
			}

			return pPM.GetNullEntity<AffiliateDrivers>();
		}

		// ****************************************************
		// Get a Affiliate's drivers
		// ****************************************************
		public static EntityList<AffiliateDrivers> GetDrivers(Affiliate pAffiliate) {
			return GetData(pAffiliate.PersistenceManager, "", pAffiliate.AffiliateID.ToString(), "", "");
		}


		// ****************************************************
		// Call the acutal "Get" proc
		// ****************************************************
		private static EntityList<AffiliateDrivers> GetData(PersistenceManager pPM, string pAffiliateDriverID, string pAffiliateID, string pName, string pCell) {
			string sDelimiter = "";
			int iArgCount = 0;
			StringBuilder sql = new StringBuilder();
			sql.Append("usp_AffiliateDriverGet ");
			if (!pAffiliateDriverID.IsNullOrEmpty()) {
				sql.AppendFormat(" @AffiliateDriverID = {0}", pAffiliateDriverID);
				iArgCount++;
			}
			if (!pAffiliateID.IsNullOrEmpty()) {
				if (iArgCount > 0) {
					sDelimiter = ", ";
				}
				sql.AppendFormat("{0} @AffiliateID = {1}", sDelimiter, pAffiliateID);
				iArgCount++;
			}
			if (!pName.IsNullOrEmpty()) {
				if (iArgCount > 0) {
					sDelimiter = ", ";
				}
				sql.AppendFormat("{0} @Name = '{1}'", sDelimiter, pName);
				iArgCount++;
			}
			if (!pCell.IsNullOrEmpty()) {
				if (iArgCount > 0) {
					sDelimiter = ", ";
				}
				sql.AppendFormat("{0} @Cell = '{1}'", sDelimiter, pCell);
				iArgCount++;
			}


			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(AffiliateDrivers), sql.ToString());
			try {
				EntityList<AffiliateDrivers> oAffDrivers = pPM.GetEntities<AffiliateDrivers>(qry, QueryStrategy.DataSourceThenCache);
				return oAffDrivers;
			} catch (PersistenceServerException ex) {
				Console.WriteLine(ex.Message);
			}
			return new EntityList<AffiliateDrivers>();
		}



		public static EntityList<AffiliateDrivers> GetDriversWithLicences(Affiliate pAffiliate) {
			PersistenceManager oPM = pAffiliate.PersistenceManager;
			RdbQuery qry = new RdbQuery(typeof(AffiliateDrivers), AffiliateDrivers.AffiliateIDEntityColumn, EntityQueryOp.EQ, pAffiliate.AffiliateID);
			qry.AddSpan(EntityRelations.AffiliateDrivers_AffiliateDriverLicenses);
			qry.AddOrderBy(AffiliateDrivers.NameEntityColumn);
			return oPM.GetEntities<AffiliateDrivers>(qry);
		}


		public static EntityList<AffiliateDrivers> GetDriversWithLicences(EntityList<Affiliate> pAffiliateList) {
			PersistenceManager oPM = pAffiliateList[0].PersistenceManager;
			List<long> idList = (from p in pAffiliateList
								 select p.AffiliateID).ToList();
			RdbQuery qry = new RdbQuery(typeof(AffiliateDrivers), AffiliateDrivers.AffiliateIDEntityColumn, EntityQueryOp.In, idList);
			qry.AddSpan(EntityRelations.AffiliateDrivers_AffiliateDriverLicenses);
			qry.AddOrderBy(AffiliateDrivers.NameEntityColumn);
			return oPM.GetEntities<AffiliateDrivers>(qry);


		}



		public static EntityList<AffiliateDrivers> GetDrivers(EntityList<Affiliate> pAffiliateList) {
			PersistenceManager oPM = pAffiliateList[0].PersistenceManager;
			List<long> idList = (from p in pAffiliateList
								 select p.AffiliateID).ToList();
			RdbQuery qry = new RdbQuery(typeof(AffiliateDrivers), AffiliateDrivers.AffiliateIDEntityColumn, EntityQueryOp.In, idList);
			qry.AddOrderBy(AffiliateDrivers.NameEntityColumn);
			return oPM.GetEntities<AffiliateDrivers>(qry);
		}

		public static EntityList<AffiliateDrivers> GetDrivers(PersistenceManager pPM, int pTop) {

			RdbQuery qry = new RdbQuery(typeof(AffiliateDrivers));
			qry.AddOrderBy(AffiliateDrivers.NameEntityColumn);
			if (pTop > 0) {
				qry.Top = pTop;
			}
			return pPM.GetEntities<AffiliateDrivers>(qry);

		}

		public static EntityList<AffiliateDrivers> GetDrivers(Affiliate pAffiliate, List<long> pDriverList) {
			PersistenceManager oPM = pAffiliate.PersistenceManager;

			RdbQuery qry = new RdbQuery(typeof(AffiliateDrivers), AffiliateDrivers.AffiliateIDEntityColumn, EntityQueryOp.EQ, pAffiliate.AffiliateID);
			qry.AddClause(AffiliateDrivers.AffiliateDriverIDEntityColumn, EntityQueryOp.In, pDriverList);
			qry.AddOrderBy(AffiliateDrivers.NameEntityColumn);

			return oPM.GetEntities<AffiliateDrivers>(qry);
		}

		public static EntityList<AffiliateDrivers> GetDriversThatNeedSignUpDatesFixed(PersistenceManager pPM) {

			RdbQuery qry = new RdbQuery(typeof(AffiliateDrivers), AffiliateDrivers.SignUpDateEntityColumn, EntityQueryOp.LE, new DateTime(2000, 1, 1));
			qry.AddClause(AffiliateDrivers.SignUpDateEntityColumn, EntityQueryOp.IsNull);
			qry.AddOperator(EntityBooleanOp.Or);
			qry.AddOrderBy(AffiliateDrivers.NameEntityColumn);
			return pPM.GetEntities<AffiliateDrivers>(qry);

		}

		// ********************************************************************************************
		// returns all active aff drivers cell phones and trans card admin nos- used by redemption app
		// ********************************************************************************************
		public static DataTable GetActiveDriversPhone(PersistenceManager pPM) {
			DataTable oData = null;

			StringBuilder sql = new StringBuilder();
			sql.Append("usp_AffiliateDriverGet @Active=1 ");
			try {
				oData = SQLHelper.GetRecords(pPM, sql.ToString());
			} catch (Exception ex) {
				string msg = ex.Message;
			}
			return oData;

		}



		public static EntityList<AffiliateDrivers> GetActiveDrivers(EntityList<Affiliate> pAffiliateList) {
			PersistenceManager oPM = pAffiliateList[0].PersistenceManager;
			List<long> affIdList = (from p in pAffiliateList
									select p.AffiliateID).ToList();
			RdbQuery qry = new RdbQuery(typeof(AffiliateDrivers), AffiliateDrivers.AffiliateIDEntityColumn, EntityQueryOp.In, affIdList);
			qry.AddClause(AffiliateDrivers.ActiveEntityColumn, EntityQueryOp.EQ, true);
			qry.AddOrderBy(AffiliateDrivers.NameEntityColumn);
			return oPM.GetEntities<AffiliateDrivers>(qry);
		}

		// *************************************************************************
		// Get drivers by cell or name
		// *************************************************************************
		public static EntityList<AffiliateDrivers> GetDriversByCellOrName(PersistenceManager pPM, string pCellOrName) {
			if (string.IsNullOrEmpty(pCellOrName)) {
				return new EntityList<AffiliateDrivers>();
			}
			RdbQuery qry = new RdbQuery(typeof(AffiliateDrivers), AffiliateDrivers.CallInCellEntityColumn, EntityQueryOp.EQ, pCellOrName);
			qry.AddClause(AffiliateDrivers.CellEntityColumn, EntityQueryOp.EQ, pCellOrName);
			qry.AddClause(AffiliateDrivers.NameEntityColumn, EntityQueryOp.StartsWith, pCellOrName);
			qry.AddOperator(EntityBooleanOp.Or);
			qry.AddOperator(EntityBooleanOp.Or);
			return pPM.GetEntities<AffiliateDrivers>(qry, QueryStrategy.DataSourceThenCache);
		}


		public static EntityList<AffiliateDrivers> GetDriversByCellOrName(Affiliate pAffiliate, string pCellOrName) {
			if (string.IsNullOrEmpty(pCellOrName)) {
				return new EntityList<AffiliateDrivers>();
			}
			RdbQuery qry = new RdbQuery(typeof(AffiliateDrivers), AffiliateDrivers.CallInCellEntityColumn, EntityQueryOp.EQ, pCellOrName);
			qry.AddClause(AffiliateDrivers.CellEntityColumn, EntityQueryOp.EQ, pCellOrName);
			qry.AddClause(AffiliateDrivers.NameEntityColumn, EntityQueryOp.StartsWith, pCellOrName);
			qry.AddOperator(EntityBooleanOp.Or);
			qry.AddOperator(EntityBooleanOp.Or);
			qry.AddClause(AffiliateDrivers.AffiliateIDEntityColumn, EntityQueryOp.EQ, pAffiliate.AffiliateID);
			return pAffiliate.PersistenceManager.GetEntities<AffiliateDrivers>(qry);
		}


		// *************************************************************************
		// Return all AffDrivers for a Fleet
		// *************************************************************************
		public static EntityList<AffiliateDrivers> GetAllDriversForFleet(PersistenceManager pPM, Int64 pFleetID) {
			return GetData(pPM, "", pFleetID.ToString(), "", "", false, "");
		}


		// ********************************************************************************
		// Get a Fleet's active drivers
		// ********************************************************************************
		public static EntityList<AffiliateDrivers> GetActiveDrivers(Affiliate pAffiliate) {
			return GetData(pAffiliate.PersistenceManager, "", pAffiliate.AffiliateID.ToString(), "", "", true, "");
		}


		// *************************************************************************************************************
		// Given an Affiliate and Driver's license no, return the first found
		// *************************************************************************************************************
		public static AffiliateDrivers GetDriverByLicenseNo(PersistenceManager pPM, long pAffiliateID, string pLicNo) {
			return GetData(pPM, "", "", "", "", false, pLicNo)[0];

		}

		// *************************************************************************
		// Get driver by cell or CallInCell
		// *************************************************************************
		public static AffiliateDrivers GetDriverByCallInCell(PersistenceManager pPM, string pCellPhone) {
			if (string.IsNullOrEmpty(pCellPhone)) {
				return pPM.GetNullEntity<AffiliateDrivers>();
			}
			return GetData(pPM, "", "", "", pCellPhone, false, "")[0];
		}

		// *************************************************************************
		// Get active driver by cell or CallInCell
		// *************************************************************************
		public static AffiliateDrivers GetActiveDriverByCallInCell(PersistenceManager pPM, string pCellPhone) {
			if (string.IsNullOrEmpty(pCellPhone)) {
				return pPM.GetNullEntity<AffiliateDrivers>();
			}
			return GetData(pPM, "", "", "", pCellPhone, true, "")[0];
		}

		// *************************************************************************
		// Used by Admin site:  TasksAffiliate/AffDrivers.aspx
		// *************************************************************************
		public static EntityList<AffiliateDrivers> GetAllDrivers(PersistenceManager pPM) {
			return GetData(pPM, "", "", "", "", false, "");
		}

		// ******************************************************************************
		// Call the proc
		// ******************************************************************************
		private static EntityList<AffiliateDrivers> GetData(PersistenceManager pPM
														, string pAffiliateDriverID
														, string pAffiliateID
														, string pName
														, string pCell
														, bool pActive
														, string pLicNo) {
			string sDelimiter = "";
			int iArgCount = 0;

			StringBuilder sql = new StringBuilder();
			sql.Append("usp_AffiliateDriverGet ");
			if (!pAffiliateDriverID.IsNullOrEmpty()) {
				sql.AppendFormat("@AffiliateDriverID = {0}", pAffiliateDriverID);
				iArgCount++;
			}
			if (!pAffiliateID.IsNullOrEmpty()) {
				if (iArgCount > 0) {
					sDelimiter = ", ";
				}
				sql.AppendFormat("{0}@AffiliateID = {1}", sDelimiter, pAffiliateID);
				iArgCount++;
			}
			if (!pName.IsNullOrEmpty()) {
				if (iArgCount > 0) {
					sDelimiter = ", ";
				}
				sql.AppendFormat("{0}@Name = '{1}'", sDelimiter, pName);
				iArgCount++;
			}
			if (!pCell.IsNullOrEmpty()) {
				if (iArgCount > 0) {
					sDelimiter = ", ";
				}
				sql.AppendFormat("{0}@Cell = '{1}'", sDelimiter, pCell);
				iArgCount++;
			}
			if (!pLicNo.IsNullOrEmpty()) {
				if (iArgCount > 0) {
					sDelimiter = ", ";
				}
				sql.AppendFormat("{0}@LicenseNo = '{1}'", sDelimiter, pLicNo);
				iArgCount++;
			}
			if (pActive) {
				if (iArgCount > 0) {
					sDelimiter = ", ";
				}
				sql.AppendFormat("{0}@Active = 1", sDelimiter);
				iArgCount++;
			}

			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(AffiliateDrivers), sql.ToString());
			EntityList<AffiliateDrivers> oData = pPM.GetEntities<AffiliateDrivers>(qry);
			if (oData.Count == 0) {
				oData.Add(pPM.GetNullEntity<AffiliateDrivers>());
			}
			return oData;
		}

		public static EntityList<AffiliateDrivers> GetSalesReps(PersistenceManager pPM) {
			StringBuilder sSQL = new StringBuilder();
			sSQL.Append("SELECT AffiliateDrivers.* FROM AffiliateDrivers ");
			sSQL.Append("WHERE AffiliateDriverID IN ");
			sSQL.Append("(SELECT DISTINCT AffiliateDriverID FROM AffiliateDriverReferrals) ");
			sSQL.Append("ORDER BY Name");

			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(AffiliateDrivers), sSQL.ToString());
			return pPM.GetEntities<AffiliateDrivers>(qry);
		}

		public static EntityList<AffiliateDrivers> GetDriversBySSN(Affiliate pAffiliate, string pSSN) {
			if (string.IsNullOrEmpty(pSSN)) {
				return new EntityList<AffiliateDrivers>();
			}
			RdbQuery qry = new RdbQuery(typeof(AffiliateDrivers), AffiliateDrivers.SsnEntityColumn, EntityQueryOp.Contains, pSSN);
			qry.AddClause(AffiliateDrivers.AffiliateIDEntityColumn, EntityQueryOp.EQ, pAffiliate.AffiliateID);
			EntityList<AffiliateDrivers> recList = pAffiliate.PersistenceManager.GetEntities<AffiliateDrivers>(qry, QueryStrategy.DataSourceThenCache);

			return recList;
		}

		public static EntityList<AffiliateDrivers> GetDriversByTAPermit(Affiliate pAffiliate, string pTAPermit) {
			if (string.IsNullOrEmpty(pTAPermit)) {
				return new EntityList<AffiliateDrivers>();
			}
			RdbQuery qry = new RdbQuery(typeof(AffiliateDrivers), AffiliateDrivers.TAPermitEntityColumn, EntityQueryOp.Contains, pTAPermit);
			qry.AddClause(AffiliateDrivers.AffiliateIDEntityColumn, EntityQueryOp.EQ, pAffiliate.AffiliateID);
			EntityList<AffiliateDrivers> recList = pAffiliate.PersistenceManager.GetEntities<AffiliateDrivers>(qry, QueryStrategy.DataSourceThenCache);

			return recList;
		}

		public static AffiliateDrivers GetDriverByPayCard(PersistenceManager pPM, string pPayCard) {
			if (string.IsNullOrEmpty(pPayCard)) {
				return pPM.GetNullEntity<AffiliateDrivers>();
			}

			string sCard = CustomerCardNumberDisplay(pPayCard);
			string sCard1 = CustomerCardNumberDisplay(pPayCard, 6);

			StringBuilder sql = new StringBuilder();
			sql.Append("SELECT * FROM AffiliateDrivers ");
			sql.Append(String.Format("WHERE (TransCardCardNumber = '{0}' OR TransCardCardNumber = '{1}') ", sCard, sCard1));
			sql.Append("AND Active = 1");

			//RdbQuery qry = new RdbQuery(typeof(AffiliateDrivers), AffiliateDrivers.TransCardCardNumberEntityColumn, EntityQueryOp.EQ, sCard);
			//qry.AddClause(AffiliateDrivers.ActiveEntityColumn, EntityQueryOp.EQ, true);

			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(AffiliateDrivers), sql.ToString());
			EntityList<AffiliateDrivers> list = pPM.GetEntities<AffiliateDrivers>(qry, QueryStrategy.DataSourceThenCache);
			foreach (AffiliateDrivers rec in list) {
				if (rec.TransCardNumber.Equals(pPayCard)) {
					return rec;
				}
			}

			return pPM.GetNullEntity<AffiliateDrivers>();
		}

		// ****************************************************************************************
		// Look up an AffDriver by affiliate & paycard number
		// ****************************************************************************************
		public static AffiliateDrivers GetDriverByPayCard(Affiliate pAffiliate, string pPayCard) {
			if (string.IsNullOrEmpty(pPayCard)) {
				return pAffiliate.PersistenceManager.GetNullEntity<AffiliateDrivers>();
			}

			string sCard = CustomerCardNumberDisplay(pPayCard);
			string sCard1 = CustomerCardNumberDisplay(pPayCard, 6);

			StringBuilder sql = new StringBuilder();
			sql.Append("SELECT * FROM AffiliateDrivers ");
			sql.Append(String.Format("WHERE (TransCardCardNumber = '{0}' OR TransCardCardNumber = '{1}') ", sCard, sCard1));
			sql.Append(String.Format("AND AffiliateID = {0}", pAffiliate.AffiliateID));

			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(AffiliateDrivers), sql.ToString());
			//RdbQuery qry = new RdbQuery(typeof(AffiliateDrivers), AffiliateDrivers.TransCardCardNumberEntityColumn, EntityQueryOp.EQ, sCard);
			//qry.AddClause(AffiliateDrivers.AffiliateIDEntityColumn, EntityQueryOp.EQ, pAffiliate.AffiliateID);
			EntityList<AffiliateDrivers> list = pAffiliate.PersistenceManager.GetEntities<AffiliateDrivers>(qry, QueryStrategy.DataSourceThenCache);
			foreach (AffiliateDrivers rec in list) {
				if (rec.TransCardNumber.Equals(pPayCard)) {
					return rec;
				}
			}

			return pAffiliate.PersistenceManager.GetNullEntity<AffiliateDrivers>();
		}

		public static EntityList<AffiliateDrivers> GetDriversByPayCard(PersistenceManager pPM, string pPayCard) {
			EntityList<AffiliateDrivers> driverList = new EntityList<AffiliateDrivers>();
			if (string.IsNullOrEmpty(pPayCard)) {
				return driverList;
			}

			string sCard = CustomerCardNumberDisplay(pPayCard);
			string sCard1 = CustomerCardNumberDisplay(pPayCard, 6);

			StringBuilder sql = new StringBuilder();
			sql.Append("SELECT * FROM AffiliateDrivers ");
			sql.Append(String.Format("WHERE (TransCardCardNumber = '{0}' OR TransCardCardNumber = '{1}') ", sCard, sCard1));

			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(AffiliateDrivers), sql.ToString());

			//RdbQuery qry = new RdbQuery(typeof(AffiliateDrivers), AffiliateDrivers.TransCardCardNumberEntityColumn, EntityQueryOp.EQ, sCard);
			EntityList<AffiliateDrivers> list = pPM.GetEntities<AffiliateDrivers>(qry, QueryStrategy.DataSourceThenCache);
			foreach (AffiliateDrivers rec in list) {
				if (rec.TransCardNumber.Equals(pPayCard)) {
					driverList.Add(rec);
				}
			}
			return driverList;
		}

		public static AffiliateDrivers GetDriversByPayCardOrPhone(PersistenceManager pPM, string pPayCardOrPhone) {
			if (string.IsNullOrEmpty(pPayCardOrPhone)) {
				return pPM.GetNullEntity<AffiliateDrivers>();
			}

			string sCard = CustomerCardNumberDisplay(pPayCardOrPhone);
			string sCard1 = CustomerCardNumberDisplay(pPayCardOrPhone, 6);

			StringBuilder sql = new StringBuilder();
			sql.Append("SELECT * FROM AffiliateDrivers ");
			sql.Append(String.Format("WHERE (TransCardCardNumber = '{0}' OR TransCardCardNumber = '{1}') ", sCard, sCard1));
			sql.Append(String.Format("OR Cell = '{0}' ", pPayCardOrPhone));
			sql.Append(String.Format("OR CallInCell = '{0}' ", pPayCardOrPhone));

			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(AffiliateDrivers), sql.ToString());

			//RdbQuery qry = new RdbQuery(typeof(AffiliateDrivers), AffiliateDrivers.TransCardCardNumberEntityColumn, EntityQueryOp.EQ, sCard);
			//qry.AddClause(AffiliateDrivers.TransCardAdminNoEntityColumn, EntityQueryOp.EQ, pPayCardOrPhone);
			//qry.AddClause(AffiliateDrivers.CellEntityColumn, EntityQueryOp.EQ, pPayCardOrPhone);
			//qry.AddClause(AffiliateDrivers.CallInCellEntityColumn, EntityQueryOp.EQ, pPayCardOrPhone);
			//qry.AddOperator(EntityBooleanOp.Or);
			//qry.AddOperator(EntityBooleanOp.Or);
			//qry.AddOperator(EntityBooleanOp.Or);
			return pPM.GetEntity<AffiliateDrivers>(qry);
		}

		public static AffiliateDrivers GetByPayCardAdminNo(PersistenceManager pPM, string pPayCard) {
			if (string.IsNullOrEmpty(pPayCard)) {
				return pPM.GetNullEntity<AffiliateDrivers>();
			}

			RdbQuery qry = new RdbQuery(typeof(AffiliateDrivers), AffiliateDrivers.TransCardAdminNoEntityColumn, EntityQueryOp.EQ, pPayCard);
			qry.AddClause(TaxiPassRedeemerAccounts.ActiveEntityColumn, EntityQueryOp.EQ, true);
			return pPM.GetEntity<AffiliateDrivers>(qry, QueryStrategy.DataSourceThenCache);
		}

		public static EntityList<AffiliateDrivers> GetDriversWithPayCard(PersistenceManager pPM) {
			RdbQuery qry = new RdbQuery(typeof(AffiliateDrivers), AffiliateDrivers.TransCardCardNumberEntityColumn, EntityQueryOp.IsNotNull, true);
			qry.AddClause(AffiliateDrivers.TransCardCardNumberEntityColumn, EntityQueryOp.NE, "");
			qry.AddOrderBy(AffiliateDrivers.AffiliateIDEntityColumn, System.ComponentModel.ListSortDirection.Ascending);
			return pPM.GetEntities<AffiliateDrivers>(qry, QueryStrategy.DataSourceThenCache);
		}

		public static AffiliateDrivers GetByBankInfo(PersistenceManager pPM, string pRouting, string pAccountNo) {
			RdbQuery qry = new RdbQuery(typeof(AffiliateDrivers), AffiliateDrivers.BankCodeEntityColumn, EntityQueryOp.EQ, pRouting);
			qry.AddClause(AffiliateDrivers.BankAccountNumberEntityColumn, EntityQueryOp.EQ, pAccountNo);
			return pPM.GetEntity<AffiliateDrivers>(qry);
		}

		public static EntityList<AffiliateDrivers> GetDriversWithCustomFraud(PersistenceManager pPM) {
			StringBuilder sql = new StringBuilder();
			sql.Append("Select AffiliateDrivers.* from AffiliateDrivers ");
			sql.Append("right join AffiliateDriverFraudDefaults on AffiliateDriverFraudDefaults.AffiliateDriverID = AffiliateDrivers.AffiliateDriverID ");
			sql.Append("left join Affiliate on Affiliate.AffiliateID = AffiliateDrivers.AffiliateID ");
			sql.Append("where AffiliateDriverFraudDefaults.MaxDailyAmount != 0 or AffiliateDriverFraudDefaults.MaxMonthlyAmount != 0 or ");
			sql.Append("	  AffiliateDriverFraudDefaults.MaxTransactions != 0 or AffiliateDriverFraudDefaults.MaxTransPerMonth != 0 ");
			sql.Append("	  or AffiliateDriverFraudDefaults.MaxTransPerMonthPerCard != 0 or AffiliateDriverFraudDefaults.MaxTransPerWeek != 0 ");
			sql.Append("	  or AffiliateDriverFraudDefaults.MaxTransPerWeekPerCard != 0  or AffiliateDriverFraudDefaults.MaxWeeklyAmount != 0 ");
			sql.Append("	  or AffiliateDriverFraudDefaults.CardChargedMaxNumber != 0 or AffiliateDriverFraudDefaults.CardVerificationAmount != 0 ");
			sql.Append("order by Affiliate.Name, AffiliateDrivers.Name");

			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(AffiliateDrivers), sql.ToString());
			return pPM.GetEntities<AffiliateDrivers>(qry);
		}

		public static EntityList<AffiliateDrivers> GetDriversWithStopPayment(PersistenceManager pPM, DateTime pBeforeDate) {
			RdbQuery qry = new RdbQuery(typeof(AffiliateDrivers), RedeemerStopPaymentEntityColumn, EntityQueryOp.EQ, true);
			qry.AddClause(StopPaymentDateEntityColumn, EntityQueryOp.LT, pBeforeDate);
			return pPM.GetEntities<AffiliateDrivers>(qry);
		}

		//public static EntityList<AffiliateDrivers> GetComDataDriversNeedingActivation(TransCardDefaults pComDataAccount) {
		//    EntityList<AffiliateDrivers> driverList = new EntityList<AffiliateDrivers>();

		//    //EntityList<TransCardDefaults> cardList = TransCardDefaults.GetComDataList(pPM);
		//    //if(cardList.Count==0) {
		//    //    return driverList;
		//    //}
		//    if (!pComDataAccount.ComData) {
		//        return driverList;
		//    }
		//    StringBuilder sql = new StringBuilder();
		//    sql.Append("Select * from AffiliateDrivers ");
		//    sql.Append("WHERE (TransCardAdminNo is null OR TransCardAdminNo = '') AND ");
		//    sql.Append(String.Format("TransCardCardNumber LIKE '{0}%' ", pComDataAccount.CardPrefix));
		//    sql.Append("Order By AffiliateID");

		//    PassthruRdbQuery qry = new PassthruRdbQuery(typeof(AffiliateDrivers), sql.ToString());
		//    return pComDataAccount.PersistenceManager.GetEntities<AffiliateDrivers>(qry);
		//}

		#endregion

		public DriverPaymentValidation LastDriverLicense {
			get {
				return DriverPaymentValidation.LastPaymentWithValidation(this);
			}
		}


		// -------------------------------------------------------------------
		// Is this driver considered as a new driver for freezing the account
		// -------------------------------------------------------------------
		public bool IsNewDriver {
			get {
				StringBuilder sql = new StringBuilder("SELECT count(*) FROM DriverPayments ");
				sql.AppendFormat("WHERE AffiliateDriverID = {0}", AffiliateDriverID.ToString());

				DataTable table = SQLHelper.GetRecords(this.PersistenceManager, sql.ToString());
				return Affiliate.NewDriverVoucherCount >= Convert.ToInt32(table.Rows[0][0]);
			}
		}

		public decimal GetChargeMaxAmount {
			get {
				decimal nAmount = this.AffiliateDriverFraudDefaults.ChargeMaxAmount;

				if (nAmount < 1) {
					nAmount = this.Affiliate.AffiliateDriverDefaults.ChargeMaxAmount;
				}

				if (nAmount < 1) {
					nAmount = SystemDefaults.GetDefaults(this.PersistenceManager).DriverChargeMaxAmount;
				}
				//if no max defined set to a high number that probably will never occur
				if (nAmount == 0) {
					nAmount = 100000;
				}
				return nAmount;
			}
		}

		public PayInfo GetPayInfo() {
			if (string.IsNullOrEmpty(this.BankAccountNumber)) {
				return null;
			}
			PayInfo oPay = new PayInfo();
			oPay.NachaFile = this.Affiliate.SendNacha;
			oPay.BankAccountHolder = this.BankAccountHolder;
			oPay.BankAccountNumber = this.BankAccountNumber;
			oPay.BankAccountPersonal = this.BankAccountPersonal;
			oPay.BankAddress = this.BankAddress;
			oPay.BankCity = this.BankCity;
			oPay.BankCode = this.BankCode;
			oPay.BankState = this.BankState;
			oPay.BankSuite = this.BankSuite;
			oPay.BankZIPCode = this.BankZIPCode;
			if (oPay.MyMerchant == null) {
				oPay.MyMerchant = new Merchant();
			}
			oPay.MyMerchant.MerchantNo = this.MerchantInfo.MerchantNo;
			oPay.PaidTo = "Driver: " + this.Name;
			return oPay;
		}

		public string ActiveGridView {
			get {
				if (Active) {
					return "Yes";
				}
				return "";
			}
		}

		public int GetCardChargedMaxNumber {
			get {
				int maxNumber = this.AffiliateDriverFraudDefaults.CardChargedMaxNumber;

				if (maxNumber < 1) {
					maxNumber = this.Affiliate.AffiliateDriverDefaults.CardChargedMaxNumber;
				}

				if (maxNumber < 1) {
					maxNumber = SystemDefaults.GetDefaults(this.PersistenceManager).DriverCardChargedMaxNumber;
				}
				//if no max defined set to a high number that probably will never occur
				if (maxNumber == 0) {
					maxNumber = 100000;
				}
				return maxNumber;
			}
		}

		public int GetMaxTrxPerDay {
			get {
				int maxTrx = this.AffiliateDriverFraudDefaults.MaxTransactions;

				if (maxTrx < 1) {
					maxTrx = this.Affiliate.AffiliateDriverDefaults.MaxTransactions;
				}
				if (maxTrx < 1) {
					maxTrx = SystemDefaults.GetDefaults(this.PersistenceManager).DriverMaxTransactions;
				}
				//if no max defined set to a high number that probably will never occur
				if (maxTrx == 0) {
					maxTrx = 100000;
				}
				return maxTrx;
			}
		}

		public int GetMaxDailyAmount {
			get {
				int maxAmount = this.AffiliateDriverFraudDefaults.MaxDailyAmount;

				if (maxAmount < 1) {
					maxAmount = this.Affiliate.AffiliateDriverDefaults.MaxDailyAmount;
				}
				if (maxAmount < 1) {
					maxAmount = SystemDefaults.GetDefaults(this.PersistenceManager).DriverMaxDailyAmount;
				}
				//if no max defined set to a high number that probably will never occur
				if (maxAmount == 0) {
					maxAmount = 100000;
				}
				return maxAmount;
			}
		}

		public int GetMaxTransPerMonth {
			get {
				int max = this.AffiliateDriverFraudDefaults.MaxTransPerMonth;

				if (max < 1) {
					max = this.Affiliate.AffiliateDriverDefaults.MaxTransPerMonth;
				}

				if (max < 1) {
					max = SystemDefaults.GetDefaults(this.PersistenceManager).DriverMaxTransPerMonth;
				}

				//if no max defined set to a high number that probably will never occur
				if (max == 0) {
					max = 100000;
				}
				return max;
			}
		}

		public int GetMaxTransPerMonthPerCard {
			get {
				int max = this.AffiliateDriverFraudDefaults.MaxTransPerMonthPerCard;

				if (max < 1) {
					max = this.Affiliate.AffiliateDriverDefaults.MaxTransPerMonthPerCard;
				}
				if (max < 1) {
					max = SystemDefaults.GetDefaults(this.PersistenceManager).DriverMaxTransPerMonthPerCard;
				}

				//if no max defined set to a high number that probably will never occur
				if (max == 0) {
					max = 100000;
				}
				return max;
			}
		}

		public int GetMaxMonthlyAmount {
			get {
				int maxAmount = this.AffiliateDriverFraudDefaults.MaxMonthlyAmount;

				if (maxAmount < 1) {
					maxAmount = this.Affiliate.AffiliateDriverDefaults.MaxMonthlyAmount;
				}
				if (maxAmount < 1) {
					maxAmount = SystemDefaults.GetDefaults(this.PersistenceManager).DriverMaxMonthlyAmount;
				}
				//if no max defined set to a high number that probably will never occur
				if (maxAmount == 0) {
					maxAmount = 1000000;
				}
				return maxAmount;
			}
		}

		public int GetMaxTransPerWeek {
			get {
				int max = this.AffiliateDriverFraudDefaults.MaxTransPerWeek;

				if (max < 1) {
					max = this.Affiliate.AffiliateDriverDefaults.MaxTransPerWeek;
				}
				if (max < 1) {
					max = SystemDefaults.GetDefaults(this.PersistenceManager).DriverMaxTransPerWeek;
				}
				//if no max defined set to a high number that probably will never occur
				if (max == 0) {
					max = 100000;
				}
				return max;
			}
		}

		public int GetMaxTransPerWeekPerCard {
			get {
				int max = this.AffiliateDriverFraudDefaults.MaxTransPerWeekPerCard;

				if (max < 1) {
					max = this.Affiliate.AffiliateDriverDefaults.MaxTransPerWeekPerCard;
				}
				if (max < 1) {
					max = SystemDefaults.GetDefaults(this.PersistenceManager).DriverMaxTransPerWeekPerCard;
				}
				//if no max defined set to a high number that probably will never occur
				if (max == 0) {
					max = 100000;
				}
				return max;
			}
		}

		public int GetMinTransTime {
			get {
				return SystemDefaults.GetDefaults(this.PersistenceManager).DriverMinTransTime;
			}
		}

		public int GetMaxWeeklyAmount {
			get {
				int maxAmount = this.AffiliateDriverFraudDefaults.MaxWeeklyAmount;

				if (maxAmount < 1) {
					maxAmount = this.Affiliate.AffiliateDriverDefaults.MaxWeeklyAmount;
				}
				if (maxAmount < 1) {
					maxAmount = SystemDefaults.GetDefaults(this.PersistenceManager).DriverMaxWeeklyAmount;
				}
				//if no max defined set to a high number that probably will never occur
				if (maxAmount == 0) {
					maxAmount = 100000;
				}
				return maxAmount;
			}
		}

		public override string TransCardNumber {
			get {
				return base.TransCardNumber.DecryptIceKey();
			}
			set {
				bool changed = (this.TransCardNumber != value);

				if (value.IsNullOrEmpty()) {
					TransCardAdminNo = "";
					base.TransCardNumber = null;
					base.TransCardCardNumber = null;
				} else {
					base.TransCardNumber = value.EncryptIceKey();
					base.TransCardCardNumber = CustomerCardNumberDisplay(value, 6);
				}
				//if (changed && !TransCardNumber.IsNullOrEmpty()) {
				//    try {
				//        EntityList<TransCardDefaults> tcDefList = TransCardDefaults.GetComDataList(this.PersistenceManager);
				//        if (tcDefList.FirstOrDefault(p => value.StartsWith(p.CardPrefix)) == null) {
				//            com.cabpay.CellPhoneWebService.Services oService = new com.cabpay.CellPhoneWebService.Services();

				//            PayCard oPayCard = new PayCard();
				//            oPayCard.SetCardNumber(this.PersistenceManager, this.TransCardNumber);

				//            string sWork = Newtonsoft.Json.JsonConvert.SerializeObject(oPayCard.TCCardInfo).EncryptIceKey();
				//            sWork = oService.ValidateCard2(sWork).DecryptIceKey();
				//            TransCard.VALIDATE_CARD_RET valResult = Newtonsoft.Json.JsonConvert.DeserializeObject<TransCard.VALIDATE_CARD_RET>(sWork);

				//            if (valResult.ERROR_FOUND.StartsWith("Y", StringComparison.CurrentCultureIgnoreCase)) {
				//                this.TransCardAdminNo = null;
				//            } else {

				//                string sAdminNo = TaxiPassCommon.Banking.TransCard.TransCardAdminNoCalc(valResult.ComData, valResult.ACCOUNT_NO);
				//                this.TransCardAdminNo = sAdminNo;
				//            }
				//        } else {
				//            this.TransCardAdminNo = null;
				//        }
				//    } catch (Exception) {
				//        this.TransCardAdminNo = null;
				//    }
				//}
			}
		}

		public string TransCardNumberOnly {
			get {
				return base.TransCardNumber.DecryptIceKey();
			}
			set {
				base.TransCardNumber = value.EncryptIceKey();
				base.TransCardCardNumber = CustomerCardNumberDisplay(value, 6);
			}
		}

		private static string CustomerCardNumberDisplay(string pCardNo) {
			return CustomerCardNumberDisplay(pCardNo, 1);
		}

		private static string CustomerCardNumberDisplay(string pCardNo, int pKeepLeftLen) {
			if (pCardNo.Length > 10) {
				string stars = new string('*', 11 - pKeepLeftLen);
				return pCardNo.Left(pKeepLeftLen) + stars + pCardNo.Right(5);
			}
			return pCardNo;
		}

		public override string TransCardID {
			get {
				return base.TransCardID.DecryptIceKey();
			}
			set {
				if (base.TransCardID.DecryptIceKey() != value && !value.IsNullOrEmpty()) {
					if (TransCardNumber.StartsWith("5")) {
						// Pin = value;
						Pin = this.Cell.Right(4);


						//TransCardChangePIN mPinChange = TransCardChangePIN.Create(this.PersistenceManager);
						//mPinChange.NewPIN = Pin;
						//mPinChange.AffiliateDriverID = this.AffiliateDriverID;
						//mPinChange.Save();

					}
				}
				if (value.IsNullOrEmpty()) {
					base.TransCardID = null;
				} else {
					base.TransCardID = value.EncryptIceKey();
				}
			}
		}

		public override DateTime? TransCardExpiration {
			get {
				return base.TransCardExpiration;
			}
			set {
				if (value.HasValue) {
					base.TransCardExpiration = new DateTime(value.Value.Year, value.Value.Month, 1);
				} else {
					base.TransCardExpiration = value;
				}
			}
		}

		public override string Pin {
			get {
				return base.Pin.DecryptIceKey();
			}
			set {
				base.Pin = value.EncryptIceKey();
			}
		}

		public override DateTime SignUpDate {
			get {
				if (!this.IsNullEntity && base.SignUpDate.Year < 2000) {
					AffiliateDriversAudit rec = AffiliateDriversAudit.GetFirstRecord(this);
					if (!rec.IsNullEntity && rec.ModifiedDate.HasValue && rec.ModifiedDate.Value.Year > 2000) {
						base.SignUpDate = rec.ModifiedDate.Value;
					} else {
						if (this.ModifiedDate.HasValue) {
							base.SignUpDate = this.ModifiedDate.Value;
						} else {
							base.SignUpDate = new DateTime(2000, 1, 1);
						}
					}
				}
				if (base.SignUpDate.Year < 2000) {
					return new DateTime(2000, 1, 1);
				}
				return base.SignUpDate;
			}
			set {
				if (this.Affiliate.TimeZone.IsNullOrEmpty()) {
					base.SignUpDate = value;
				} else {
					TimeZoneConverter convert = new TimeZoneConverter();
					base.SignUpDate = convert.ConvertTime(value, TimeZone.CurrentTimeZone.StandardName, this.Affiliate.TimeZone);
				}
			}
		}

		private DateTime? mLastTransactionDate = null;
		public DateTime? LastTransactionDate {
			get {
				if (!mLastTransactionDate.HasValue) {
					DriverPayments rec = DriverPayments.GetLastAffDriverPayment(this.PersistenceManager, this.AffiliateDriverID);
					if (!rec.IsNullEntity) {
						mLastTransactionDate = rec.ChargeDate;
					}
				}
				return mLastTransactionDate;
			}
		}


		public PaymentMethod PaymentMethodEnum {
			get {
				return (PaymentMethod)this.PaymentMethod;
			}
		}

		//public decimal GetPayCardBalance() {
		//    try {
		//        TransCardDefaults tranDef = TransCardDefaults.GetDefaults(this.PersistenceManager, this.TransCardNumber);
		//        if (tranDef == null) {
		//            tranDef = TransCardDefaults.GetDefaultTransCard(this.PersistenceManager);
		//        }
		//        TransCard oCard = new TransCard(tranDef.GetTransCardAccess());
		//        TransCard.CardInfo oCardInfo = new TransCard.CardInfo();
		//        oCardInfo.CardNumber = this.TransCardNumber;
		//        oCardInfo.ComData = tranDef.ComData;
		//        TransCard.VALIDATE_CARD_RET cardResults = oCard.ValidateCard(oCardInfo);
		//        return Convert.ToDecimal(cardResults.CURRENT_CARD_BALANCE.Replace("$", ""));
		//    } catch (Exception) {
		//    }
		//    return 0;
		//}

		// *******************************************************************************************
		// Validate and activate the PayCard
		// *******************************************************************************************
		//public TransCard.VALIDATE_CARD_RET ActivateTransCard() {
		//    TransCard.VALIDATE_CARD_RET transResult = new TransCard.VALIDATE_CARD_RET();

		//    //update card if we have all info otherwise inactivate account until we do
		//    if (this.Name.IsNullOrEmpty()
		//        || this.Street.IsNullOrEmpty()
		//        || this.City.IsNullOrEmpty()
		//        || this.State.IsNullOrEmpty()
		//        || this.Country.IsNullOrEmpty()
		//        || !this.TransCardExpiration.HasValue
		//        || this.TransCardNumber.IsNullOrEmpty()
		//        || this.LicenseNo.IsNullOrEmpty()
		//        || this.LicenseState.IsNullOrEmpty()
		//        || !this.DateOfBirth.HasValue) {
		//        if (this.Active) {
		//            this.TransCardActive = false;
		//            transResult.ERROR_MESSAGE = "Cannot activate driver until Driver's License, address and Trans Card data is populated";
		//            transResult.ERROR_FOUND = "Y";
		//            transResult.ERROR_NUMBER = "-1";
		//        }
		//    } else {
		//        string cardNumber = this.TransCardNumber;
		//        //TransCard.CARDHOLDER_DETAIL_RET detail = oCard.CardHolderDetail(cardNumber);

		//        //just update changes even if it's the same
		//        CabRideEngine.com.cabpay.CellPhoneWebService.TransCardDriverIdentification oID = new CabRideEngine.com.cabpay.CellPhoneWebService.TransCardDriverIdentification();

		//        oID.EmployeeID = this.AffiliateDriverID.ToString();

		//        oID.AddressLine1 = this.Street;
		//        oID.AddressLine2 = this.Suite;
		//        oID.CardNumber = this.TransCardNumber;
		//        oID.City = this.City;
		//        oID.CountryCode = this.Country;
		//        oID.Email = this.EMail;

		//        if (this.Name.Contains(",")) {
		//            List<string> nameList = this.Name.Split(',').ToList();
		//            oID.LastName = nameList[0];
		//            for (int i = 1; i < nameList.Count; i++) {
		//                if (nameList.Count - 1 == i) {
		//                    oID.FirstName = nameList[i];
		//                } else {
		//                    oID.MiddleName += nameList[i] + " ";
		//                }
		//            }
		//        } else {
		//            List<string> nameList = this.Name.Split(' ').ToList();
		//            oID.FirstName = nameList[0];
		//            for (int i = 1; i < nameList.Count; i++) {
		//                if (nameList.Count - 1 == i) {
		//                    oID.LastName = nameList[i];
		//                } else {
		//                    oID.MiddleName += nameList[i] + " ";
		//                }
		//            }
		//        }
		//        oID.Phone = this.Cell;
		//        if (oID.Phone.Length < 10) {
		//            oID.Phone = this.CallInCell;
		//        }
		//        oID.PostalCode = this.ZIPCode;
		//        oID.StateProvince = this.State;
		//        if (this.DateOfBirth.HasValue) {
		//            oID.DateOfBirth = this.DateOfBirth.Value.ToString("yyyyMMdd");
		//        }
		//        if (this.USCitizen.GetValueOrDefault(0) == 1) {
		//            //oID.IdCode = "STATE DRIVERS LICENSE";
		//            oID.IdCode = "S";
		//            oID.IdNumber = this.Ssn; //.LicenseNo;
		//            //if (oID.IdNumber.IsNullOrEmpty()) {
		//            //    oID.IdNumber = "123456789";
		//            //}
		//        } else {
		//            oID.IdCode = "STATE DRIVERS LICENSE";
		//            oID.IdNumber = this.LicenseNo;
		//            //oID.IdState = this.LicenseState;
		//        }
		//        oID.IdCountry = "USA";  // "California"; // States.GetStatesByCountry(mPM, "USA").Find(p=>p.State.Equals(oDriver.State, StringComparison.CurrentCultureIgnoreCase); // "USA";

		//        try {
		//            CabRideEngine.com.cabpay.CellPhoneWebService.Services oService = new CabRideEngine.com.cabpay.CellPhoneWebService.Services();
		//            CabRideEngine.com.cabpay.CellPhoneWebService.CARDHOLDER_IDENTIFICATION_RET oResult = oService.CardHolderIdentification(oID);


		//            if (!oResult.ERROR_NUMBER.IsNullOrEmpty() && oResult.ERROR_NUMBER != "0") {
		//                transResult.ERROR_NUMBER = "-1";
		//                transResult.ERROR_FOUND = "Y";
		//                transResult.ERROR_MESSAGE = "Error updating Drivers Info on PayCard: " + oResult.ERROR_MESSAGE;

		//            } else {
		//                PayCard oPayCard = new PayCard();
		//                oPayCard.SetCardNumber(this.PersistenceManager, this.TransCardNumber);
		//                // set this as it isn't saved in the system yet.
		//                oPayCard.TCCardInfo.CardNumber = this.TransCardNumber;
		//                oPayCard.TCCardInfo.ComData = oResult.ComData;
		//                oPayCard.TCCardInfo.EmployeeID = oID.EmployeeID;
		//                oPayCard.TCCardInfo.CardHolderReferenceNumber = oResult.CardHolderReferenceNumber;
		//                oPayCard.TCCardInfo.FirstName = oID.FirstName;
		//                oPayCard.TCCardInfo.LastName = oID.LastName;

		//                if (oResult.ComData) {
		//                    // 1/27/12 ComData doesn't offer us a valid "ValidateCard" yet
		//                    if (oPayCard.TCCardInfo.AdminNo != oResult.CardHolderReferenceNumber) {
		//                        oPayCard.TCCardInfo.AdminNo = oResult.CardHolderReferenceNumber;
		//                        this.TransCardAdminNo = TaxiPassCommon.Banking.TransCard.TransCardAdminNoCalc(oResult.ComData, oResult.CardHolderReferenceNumber);
		//                        this.Save();
		//                    }
		//                    transResult.ACCOUNT_NO = this.TransCardAdminNo;
		//                    transResult.CARD_STATUS = "ACTIVE";
		//                    transResult.ACTIVATION_REQUIRED = "Y";
		//                    transResult.FIRST_NAME = oID.FirstName;
		//                    transResult.LAST_NAME = oID.LastName;
		//                    transResult.CARDHOLDER_IDENTIFIED = "Y";
		//                    transResult.CURRENT_CARD_BALANCE = "0";

		//                } else {

		//                    string sWork = Newtonsoft.Json.JsonConvert.SerializeObject(oPayCard.TCCardInfo).EncryptIceKey();
		//                    sWork = oService.ValidateCard2(sWork).DecryptIceKey();
		//                    transResult = Newtonsoft.Json.JsonConvert.DeserializeObject<TransCard.VALIDATE_CARD_RET>(sWork);
		//                }

		//                if (transResult.ACTIVATION_REQUIRED.StartsWith("Y", StringComparison.CurrentCultureIgnoreCase)) {
		//                    string sWork = Newtonsoft.Json.JsonConvert.SerializeObject(oPayCard.TCCardInfo).EncryptIceKey();
		//                    sWork = oService.ActivateCard2(sWork).DecryptIceKey();
		//                    TransCard.CARD_ACTIVATION_RET actResult = Newtonsoft.Json.JsonConvert.DeserializeObject<TransCard.CARD_ACTIVATION_RET>(sWork);

		//                    if (actResult.ERROR_FOUND.StartsWith("N", StringComparison.CurrentCultureIgnoreCase)) {
		//                        string sAdminNo = "";
		//                        if (!oResult.ComData) {
		//                            sWork = Newtonsoft.Json.JsonConvert.SerializeObject(oPayCard.TCCardInfo).EncryptIceKey();
		//                            sWork = oService.ValidateCard2(sWork).DecryptIceKey();
		//                            transResult = Newtonsoft.Json.JsonConvert.DeserializeObject<TransCard.VALIDATE_CARD_RET>(sWork);

		//                            sAdminNo = transResult.ACCOUNT_NO;
		//                        } else {
		//                            transResult.ACTIVATION_REQUIRED = "N";
		//                            sAdminNo = oResult.CardHolderReferenceNumber;

		//                        }
		//                        this.TransCardAdminNo = TaxiPassCommon.Banking.TransCard.TransCardAdminNoCalc(oResult.ComData, sAdminNo);
		//                    }

		//                    this.TransCardActive = true;
		//                    this.Save();
		//                }
		//            }
		//        } catch (Exception ex) {
		//            transResult.ERROR_NUMBER = "-1";
		//            transResult.ERROR_FOUND = "Y";
		//            transResult.ERROR_MESSAGE = "Error update TransCard Data " + ex.Message;
		//        }
		//    }
		//    return transResult;

		//}


		public string SendDriverMsg(string pMsg, bool pIfEmailAddressReturnAddressOnly) {
			return SendDriverMsg(pMsg, pIfEmailAddressReturnAddressOnly, false);
		}

		public string SendDriverMsg(string pMsg, bool pIfEmailAddressReturnAddressOnly, bool pSendAsSMS) {
			PersistenceManager oPM = this.PersistenceManager;
			SystemDefaults oDef = SystemDefaults.GetDefaults(oPM);


			if (this.Affiliate.DoNotTextDrivers) {
				return "";
			}

			string carrier = "";
			PagerTypes oPager = oPM.GetNullEntity<PagerTypes>();
			if (this.CellProviderID.HasValue) {
				oPager = PagerTypes.GetPagerType(oPM, this.CellProviderID.Value);
				carrier = "" + oPager.Url;
			}

			if (oPager.PagerType.Equals("Do Not Text", StringComparison.CurrentCultureIgnoreCase)) {
				return "";
			}

			string cell = this.Cell;
			if (cell.Length < 10) {
				cell = this.CallInCell;
			}
			if (cell.Length < 10 || cell.Length > 10) {
				return "";
			}

			TaxiPassCommon.Mail.EZTexting ezText = new TaxiPassCommon.Mail.EZTexting(oDef.EZTextingUserName, oDef.EZTextingPassword);
			if (carrier.IsNullOrEmpty()) {
				carrier = ezText.GetCarrier(cell);
				if (carrier.IsNullOrEmpty()) {
					oPager = PagerTypes.GetPagerType(oPM, "UNKNOWN");
				} else {
					oPager = PagerTypes.GetPagerType(oPM, carrier);
					if (oPager.IsNullEntity) {
						oPager = PagerTypes.Create(oPM);
						oPager.PagerType = carrier;
						try {
							oPager.Save();
						} catch {
						}
					}
				}
				if (!this.IsNullEntity && !oPager.IsNullEntity) {
					if (this.CellProviderID != oPager.PagerID) {
						this.CellProviderID = oPager.PagerID;
						try {
							this.Save();
						} catch (Exception) {
						}
					}
				}
				carrier = "" + oPager.Url;
			}
			if (carrier.IsNullOrEmpty() || carrier.Contains("taxipass.com") || pSendAsSMS) {
				bool bUseEzTexting = true;
				if (oPager.PagerType.Equals("TxtDrop", StringComparison.CurrentCultureIgnoreCase)) {
					TaxiPassCommon.Mail.TxtDropSMS sms = new TaxiPassCommon.Mail.TxtDropSMS("email@taxipass.com");
					string response = sms.Send(cell, pMsg);
					if (!response.ToLower().Contains("error")) {
						bUseEzTexting = false;
					}
				} else {
					string acctSid = oDef.TwilioAccountSID;
					string acctToken = oDef.TwilioAccountToken;
					string apiVersion = oDef.TwilioAPIVersion;
					string smsNumber = oDef.TwilioSMSNumber;

					TaxiPassCommon.Mail.TwilioTexting twilio = new TaxiPassCommon.Mail.TwilioTexting(acctSid, acctToken, apiVersion, smsNumber);
					var response = twilio.TwilioTextMessage(pMsg, cell);
					if (response.RestException != null && !response.RestException.Message.IsNullOrEmpty()) {
						CabRideEngineDapper.SystemDefaultsDict dict = CabRideEngineDapper.SystemDefaultsDict.GetByKey("EMail", "Receipt", false);
						string[] cred = dict.ValueString.Decrypt().Split('|');
						TaxiPassCommon.Mail.EMailMessage oMail = new TaxiPassCommon.Mail.EMailMessage(cred[0], "Affiliate Driver -> Twilio Texting Error", string.Format("Error: {0}<br>Message: {1}", response.RestException.Message, pMsg), cred[1]);
						oMail.UseWebServiceToSendMail = Properties.Settings.Default.EMailViaWebService;
						oMail.SendMail(Properties.Settings.Default.EMailErrorsTo, false);
					} else {
						bUseEzTexting = false;
					}
				}

				if (bUseEzTexting) {
					ezText.Send(cell, pMsg);
				}
			} else {
				string sFrom = Properties.Settings.Default.EMailFrom;
				if (!carrier.StartsWith("@")) {
					carrier = "@" + carrier;
				}
				string sSendToSMS = cell + carrier;
				if (!pIfEmailAddressReturnAddressOnly) {
					CabRideEngineDapper.SystemDefaultsDict dict = CabRideEngineDapper.SystemDefaultsDict.GetByKey("EMail", "Receipt", false);
					string[] cred = dict.ValueString.Decrypt().Split('|');
					TaxiPassCommon.Mail.EMailMessage oMail = new TaxiPassCommon.Mail.EMailMessage(cred[0], "", pMsg, cred[1]);
					oMail.UseWebServiceToSendMail = Properties.Settings.Default.EMailViaWebService;
					oMail.SendMail(sSendToSMS, false);
				}
				return sSendToSMS;
			}
			return "";
		}


		public void CallDriver(string pURLofMessage) {
			PersistenceManager oPM = this.PersistenceManager;
			SystemDefaults oDefaults = SystemDefaults.GetDefaults(oPM);

			string acctSid = oDefaults.TwilioAccountSID;
			string acctToken = oDefaults.TwilioAccountToken;
			string apiVersion = oDefaults.TwilioAPIVersion;

			string callerID = oDefaults.TwilioCallerID;

			TwilioRest.Account account = new TwilioRest.Account(acctSid, acctToken);

			Hashtable h = new Hashtable();
			h.Add("Caller", callerID);
			h.Add("Called", this.Cell);

			h.Add("Url", pURLofMessage);
			h.Add("Method", "GET");
			string msg = account.request(String.Format("/{0}/Accounts/{1}/Calls", apiVersion, acctSid), "POST", h);
			//Console.WriteLine(msg);
		}

		// *******************************************************************
		// Call using a specific Twilio number
		// *******************************************************************
		public void CallDriver(string pURLofMessage, string pGroup, string pReferenceKey) {
			PersistenceManager oPM = this.PersistenceManager;
			SystemDefaults oDefaults = SystemDefaults.GetDefaults(oPM);
			SystemDefaultsDict oDefDict = SystemDefaultsDict.GetByKey(oPM, pGroup, pReferenceKey, false);


			string acctSid = oDefaults.TwilioAccountSID;
			string acctToken = oDefaults.TwilioAccountToken;
			string apiVersion = oDefaults.TwilioAPIVersion;

			string callerID = oDefDict.ValueString;

			TwilioRest.Account account = new TwilioRest.Account(acctSid, acctToken);

			Hashtable h = new Hashtable();
			h.Add("Caller", callerID);
			h.Add("Called", this.Cell);

			h.Add("Url", pURLofMessage);
			h.Add("Method", "GET");
			//h.Add("IfMachine","Hangup");
			string msg = account.request(String.Format("/{0}/Accounts/{1}/Calls", apiVersion, acctSid), "POST", h);
			//Console.WriteLine(msg);
		}


		// *******************************************************************
		// Call using a specific Twilio number
		// *******************************************************************
		public void CallDriverHangupMachine(string pURLofMessage, string pGroup, string pReferenceKey) {
			PersistenceManager oPM = this.PersistenceManager;
			SystemDefaults oDefaults = SystemDefaults.GetDefaults(oPM);
			SystemDefaultsDict oDefDict = SystemDefaultsDict.GetByKey(oPM, pGroup, pReferenceKey, false);


			string acctSid = oDefaults.TwilioAccountSID;
			string acctToken = oDefaults.TwilioAccountToken;
			string apiVersion = oDefaults.TwilioAPIVersion;

			string callerID = oDefDict.ValueString;

			TwilioRest.Account account = new TwilioRest.Account(acctSid, acctToken);

			Hashtable h = new Hashtable();
			h.Add("Caller", callerID);
			h.Add("Called", this.Cell);

			h.Add("Url", pURLofMessage);
			h.Add("Method", "GET");
			h.Add("IfMachine", "Hangup");
			string msg = account.request(String.Format("/{0}/Accounts/{1}/Calls", apiVersion, acctSid), "POST", h);
			//Console.WriteLine(msg);
		}


		public void CallDriverWithMMSMessage(string pURLofMessage) {
			PersistenceManager oPM = this.PersistenceManager;

			SystemDefaults oDefaults = SystemDefaults.GetDefaults(oPM);

			string acctSid = oDefaults.TwilioAccountSID;
			string acctToken = oDefaults.TwilioAccountToken;
			string apiVersion = oDefaults.TwilioAPIVersion;

			string callerID = oDefaults.TwilioCallerID;
			if (this.SelfRedemption || this.Affiliate.AffiliateDriverDefaults.SelfRedemption) {
				callerID = oDefaults.TwilioMMSNumber;
			}

			TwilioRest.Account account = new TwilioRest.Account(acctSid, acctToken);

			Hashtable h = new Hashtable();
			h.Add("Caller", callerID);
			h.Add("Called", this.Cell);

			h.Add("Url", pURLofMessage);
			h.Add("Method", "GET");
			string msg = account.request(String.Format("/{0}/Accounts/{1}/Calls", apiVersion, acctSid), "POST", h);
			//Console.WriteLine(msg);

		}

		// *****************************************************************************
		// returns true if either the driver or his affiliate is set to PayBeforeMatch
		// *****************************************************************************
		public bool IsPayBeforeMatch() {
			bool bRet = false;

			if (this.PayBeforeMatch.HasValue) {
				bRet = this.PayBeforeMatch.Value;
			}

			if (!bRet & this.Affiliate.PayBeforeMatch.HasValue) {
				bRet = this.Affiliate.PayBeforeMatch.Value;
			}
			return bRet;
		}


		private CabRideEngineDapper.AffiliateDriverRef mWidgetKey;
		public string WidgetKey {
			get {
				CreateWidgetKey();
				return mWidgetKey.ValueString;
			}

		}

		public void CreateWidgetKey(bool pSaveChanges = false) {
			if (mWidgetKey == null || pSaveChanges) {
				using (var conn = CabRideEngineDapper.Utils.SqlHelper.OpenSqlConnection()) {
					mWidgetKey = CabRideEngineDapper.AffiliateDriverRef.GetOrCreate(conn, this.AffiliateDriverID, "Widget", "Key");
					if (pSaveChanges) {
						if (this.RowState == DataRowState.Added) {
							Save();
						}
						mWidgetKey.ValueString = string.Format("{0}|{1}|{2}", AffiliateID, AffiliateDriverID, Pin).EncryptIceKey();
						//mWidgetKey.Save();
					}
				}
			}
		}

		public void WidgetKeyReset() {
			mWidgetKey = null;
			CreateWidgetKey();
		}

		private CabRideEngineDapper.DriverHackLicense mDriverHackLicense;
		public string HackLicenseURL {
			get {
				if (mDriverHackLicense == null) {
					using (var conn = CabRideEngineDapper.Utils.SqlHelper.OpenSqlConnection()) {
						mDriverHackLicense = CabRideEngineDapper.DriverHackLicense.GetCreateByAffDriverID(conn, AffiliateDriverID);
					}
				}
				return mDriverHackLicense.LicenseImageURL;
			}

			set {
				if (mDriverHackLicense == null) {
					using (var conn = CabRideEngineDapper.Utils.SqlHelper.OpenSqlConnection()) {
						mDriverHackLicense = CabRideEngineDapper.DriverHackLicense.GetCreateByAffDriverID(conn, AffiliateDriverID);
					}
				}
				mDriverHackLicense.LicenseImageURL = value;
			}
		}


		private CabRideEngineDapper.AffiliateDriverLicenses mAffiliateDriverLicense;
		public string DriverLicenseURL {
			get {
				if (mAffiliateDriverLicense == null) {
					using (var conn = CabRideEngineDapper.Utils.SqlHelper.OpenSqlConnection()) {
						mAffiliateDriverLicense = CabRideEngineDapper.AffiliateDriverLicenses.GetOrCreate(conn, AffiliateDriverID);
					}
				}
				return mAffiliateDriverLicense.MyImageURL;
			}
			set {
				if (mAffiliateDriverLicense == null) {
					using (var conn = CabRideEngineDapper.Utils.SqlHelper.OpenSqlConnection()) {
						mAffiliateDriverLicense = CabRideEngineDapper.AffiliateDriverLicenses.GetOrCreate(conn, AffiliateDriverID);
					}
				}
				mAffiliateDriverLicense.LicenseURL = value;
			}
		}


		//public TransCard.LOAD_CARD_FUNDED_RET TransferWaitingToBatchToPayCard() {
		//    return TransferWaitingToBatchToPayCard(false);
		//}


		// **********************************************************************************************
		// Used by CellphoneWebService\Android\MobilTransations
		// **********************************************************************************************
		//public TransCard.LOAD_CARD_FUNDED_RET TransferWaitingToBatchToPayCard(bool pUseWebService) {
		//    PersistenceManager oPM = this.PersistenceManager;
		//    TimeZoneConverter tzConvert = new TimeZoneConverter();

		//    com.cabpay.CellPhoneWebService.LOAD_CARD_FUNDED_RET fundResultsWebService = new com.cabpay.CellPhoneWebService.LOAD_CARD_FUNDED_RET();
		//    TransCard.LOAD_CARD_FUNDED_RET fundResults = new TransCard.LOAD_CARD_FUNDED_RET();

		//    if (this.TransCardAdminNo.IsNullOrEmpty()) {
		//        fundResults.ERROR_MESSAGE = "No TransCard Admin Number to process";
		//        fundResults.ERROR_FOUND = "Yes";
		//        return fundResults;
		//    }
		//    PayCard oPayCard = new PayCard();
		//    oPayCard.SetCardNumber(this.PersistenceManager, TransCardAdminNo);
		//    oPayCard.RemoveFundsFromCard = false;

		//    EntityList<DriverPayments> payList = DriverPayments.GetPaymentsForDriverInitiatedProcess(this);
		//    foreach (DriverPayments rec in payList) {
		//        ACHDetail.CreatePayCardBatch(this, rec, true);
		//    }

		//    TransCard oCard = new TransCard(oPayCard.TCDefaults.GetTransCardAccess());

		//    EntityList<ACHDetail> totalList = ACHDetail.GetTransCardsWaitingToBatch(oPM, oPayCard.TCCardInfo.AdminNo);
		//    DateTime processDate = DateTime.Now;
		//    try {
		//        processDate = tzConvert.ConvertTime(DateTime.Now, TimeZone.CurrentTimeZone.StandardName, this.Affiliate.TimeZone);
		//    } catch (Exception) {
		//    }

		//    decimal driverTotal = totalList.Sum(p => p.DriverPayments.DriverTotalPaid);
		//    if (driverTotal == 0) {
		//        fundResults.ERROR_MESSAGE = "No Transactions to process";
		//        fundResults.ERROR_FOUND = "Yes";
		//        return fundResults;
		//    }

		//    try {
		//        long batchNo = Convert.ToInt64(processDate.ToString("yyyyMMddhhmmss") + this.AffiliateDriverID);

		//        Console.Write(String.Format("Processing: Driver: {0} transCard: {1} for {2}", this.Name, this.TransCardAdminNo, driverTotal.ToString("C")));

		//        if (pUseWebService) {
		//            StringBuilder sRequest = new StringBuilder();
		//            sRequest.AppendFormat("from={0}", oPayCard.TCDefaults.FundingAccount);
		//            sRequest.AppendFormat("&to={0}", oPayCard.TCCardInfo.CardNumber);
		//            sRequest.AppendFormat("&date={0}", DateTime.Now.ToUniversalTime());
		//            sRequest.AppendFormat("&voucherno=Batch: {0}", batchNo);

		//            sRequest.AppendFormat("&CardNo={0}", oPayCard.TCCardInfo.CardNumber);
		//            sRequest.AppendFormat("&ToEmployeeID={0}", oPayCard.TCCardInfo.EmployeeID);
		//            sRequest.AppendFormat("&TransCardDefaultID=", oPayCard.TCDefaults.TransCardDefaultID);
		//            sRequest.Append("&RemoveFundsFromCard=False");
		//            StringBuilder CallingInfo = new StringBuilder();
		//            CallingInfo.Append("AffiliateDrivers.TransferWaitingToBatchToPayCard: ");
		//            CallingInfo.Append(" | ");
		//            try {
		//                CallingInfo.Append(System.AppDomain.CurrentDomain.BaseDirectory);
		//            } catch (Exception) {
		//            }
		//            sRequest.AppendFormat("&CallingInfo={0}", CallingInfo.ToString());

		//            com.cabpay.CellPhoneWebService.Services service = new com.cabpay.CellPhoneWebService.Services();
		//            fundResultsWebService = service.TransferFunds2(sRequest.ToString().EncryptIceKey(), driverTotal);

		//        } else {

		//            TransCard.CardInfo oFundingInfo = new TransCard.CardInfo();
		//            oFundingInfo.CardNumber = oPayCard.TCDefaults.FundingAccount;
		//            oFundingInfo.ComData = oPayCard.TCDefaults.ComData;

		//            fundResults = oCard.LoadCardFunds(oPayCard.TCCardInfo, oFundingInfo, driverTotal, "Batch: " + batchNo);
		//        }
		//        bool error = false;
		//        if (pUseWebService) {
		//            error = fundResultsWebService.ERROR_FOUND.Equals("Yes", StringComparison.CurrentCultureIgnoreCase);
		//            fundResults.ERROR_MESSAGE = fundResultsWebService.ERROR_MESSAGE;
		//            fundResults.ERROR_FOUND = fundResultsWebService.ERROR_FOUND;
		//        } else {
		//            error = fundResults.ERROR_FOUND.Equals("Yes", StringComparison.CurrentCultureIgnoreCase);
		//        }
		//        if (error) {
		//            Console.WriteLine(" payment error");
		//            fundResults.ERROR_MESSAGE = "Could not fund card, try again later";
		//        } else {
		//            Console.WriteLine(" card funded");
		//            foreach (ACHDetail pay in totalList) {
		//                pay.ACHBatchNumber = batchNo;
		//                pay.ACHErrorReason = "Paid Batch Amount: " + driverTotal.ToString("C");
		//                pay.ACHPaidDate = DateTime.Now;
		//                pay.Save();
		//            }

		//            try {
		//                string msg = String.Format("{0} has been added to card *{1}", driverTotal.ToString("C"), this.TransCardNumber.Right(5));
		//                string curBalance = "";
		//                if (pUseWebService) {
		//                    curBalance = fundResultsWebService.CURRENT_CARD_BALANCE;
		//                } else {
		//                    curBalance = fundResults.CURRENT_CARD_BALANCE;
		//                }
		//                if (!curBalance.IsNullOrEmpty()) {
		//                    msg += "\nNew card bal: " + curBalance;
		//                }
		//                SendDriverMsg(msg, false, true);
		//                fundResults.ERROR_MESSAGE = msg;
		//                fundResults.ERROR_FOUND = "No";
		//                if (fundResults.REFERENCE_NUMBER.IsNullOrEmpty()) {
		//                    fundResults.REFERENCE_NUMBER = driverTotal.ToString("C");
		//                }
		//            } catch (Exception) {
		//            }

		//        }

		//    } catch (Exception ex) {
		//        fundResults.ERROR_FOUND = "Yes";
		//        fundResults.ERROR_MESSAGE = ex.Message;
		//        fundResults.CURRENT_CARD_BALANCE = "0";
		//    }

		//    return fundResults;
		//}
	}


	#region EntityPropertyDescriptors.AffiliateDriversPropertyDescriptor
	////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
	////    For example: 
	////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
	//public static partial class EntityPropertyDescriptors {
	//	public partial class AffiliateDriversPropertyDescriptor : BaseEntityPropertyDescriptor {
	//    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
	//    public AdaptedPropertyDescriptor Age {
	//      get { return this.Get("Age"); }
	//    }
	//  }
	//} 
	#endregion

}
