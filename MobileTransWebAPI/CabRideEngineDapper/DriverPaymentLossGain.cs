﻿using Dapper;
using System.Data.SqlClient;

namespace CabRideEngineDapper {
    public partial class DriverPaymentLossGain {

        public static DriverPaymentLossGain GetLossGain(SqlConnection pConn, long pDriverPaymentID) {
            string sql = @"SELECT * FROM DriverPaymentLossGain
                            WHERE DriverPaymentID = @DriverPaymentID";

            return pConn.Get<DriverPaymentLossGain>(sql, new { DriverPaymentID = pDriverPaymentID });
        }
    }
}
