﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MobileTransWebAPI.Models {
	public partial class DriverSignDevice {

		public long DeviceID { get; set; }
		public long DeviceLoginID { get; set; }
		public long RedeemerMasterVoucherID { get; set; }
		public string SerialNo { get; set; }
		public string PushToken { get; set; }
		public string LoginDate { get; set; }
		public string LogoutDate { get; set; }

		// Redeemer login info
		public string TPRedeemerEMail { get; set; }
		public string TPRedeemerPW { get; set; }
		public long TPRedeemerID { get; set; }

		public bool SigRequired { get; set; }
		public bool SigSaved { get; set; }
		public string Signature { get; set; } // bitmap to string
		public string SignatureAgreement { get; set; }

		// Voucher list
		public List<DriverSignVouchers> Vouchers { get; set; }

		// Errors
		public string ErrorMsg { get; set; }
	}
}