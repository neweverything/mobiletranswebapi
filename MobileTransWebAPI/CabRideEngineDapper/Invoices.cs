﻿using CabRideEngineDapper.Utils;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace CabRideEngineDapper {
	public partial class Invoices {

		private List<InvoiceLineItem> mLineItems;
		public List<InvoiceLineItem> LineItems {
			get {
				if (mLineItems == null) {
					mLineItems = InvoiceLineItem.GetLineItems(this.InvoiceID);
				}
				return mLineItems;
			}
		}

		private Affiliate mAffiliate;
		public Affiliate Affiliate {
			get {
				if (!this.AffiliateID.HasValue) {
					return Affiliate.GetNullEntity();
				}
				if (mAffiliate == null) {
					mAffiliate = Affiliate.GetAffiliate(this.AffiliateID.Value);
				}
				return mAffiliate;
			}
		}

		public static List<Invoices> GetInvoices(SqlConnection pConn, long pAffiliateID, DateTime pStart, DateTime pEnd) {
			string sql = @"SELECT *
							FROM Invoices
							WHERE AffiliateID = @AffiliateID
								  AND InvoiceDate >= @StartDate
								  AND InvoiceDate < @EndDate
							ORDER BY InvoiceDate
								   , DueDate";

			return pConn.Query<Invoices>(sql, new { AffiliateID = pAffiliateID, StartDate = pStart, EndDate = pEnd }).ToList();
		}

		public static Invoices GetInvoice(long pInvoiceID) {
			using (var conn = SqlHelper.OpenSqlConnection()) {
				return conn.Get<Invoices>(pInvoiceID);
			}
		}

		public static Invoices GetInvoiceByInvoiceNo(string pInvoiceNo) {
			string sql = @"SELECT *
							FROM Invoices
							WHERE InvoiceNo = @Number";

			using (var conn = SqlHelper.OpenSqlConnection()) {
				return conn.Get<Invoices>(sql, new { Number = pInvoiceNo });
			}
		}

		public static Invoices GetInvoice(long pAffiliateID, long pInvoiceID) {
			string sql = @"SELECT *
							FROM Invoices
							WHERE AffiliateID = @AffiliateID
								  AND InvoiceID = @InvoiceID";

			using (var conn = SqlHelper.OpenSqlConnection()) {
				return conn.Get<Invoices>(sql, new { AffiliateID = pAffiliateID, InvoiceID = pInvoiceID });
			}
		}

		public static Invoices GetInvoiceByEmailAndNumber(string pEmail, string pNumber) {
			string sql = @"SELECT *
							FROM Invoices
							WHERE EMail = @Email
								  AND InvoiceNo = @Number";

			using (var conn = SqlHelper.OpenSqlConnection()) {
				return conn.Get<Invoices>(sql, new { Email = pEmail, Number = pNumber });
			}
		}

		public static Invoices GetInvoiceByPhone(long pAffiliateID, string pPhoneNo) {
			string sql = @"SELECT TOP 1 *
							FROM Invoices
							WHERE AffiliateID = @AffiliateID
								  AND PhoneNo = @Number
							ORDER BY InvoiceID DESC";

			using (var conn = SqlHelper.OpenSqlConnection()) {
				return conn.Get<Invoices>(sql, new { AffiliateID = pAffiliateID, Number = pPhoneNo });
			}
		}

		public static List<Invoices> GetInvoicesByLineItems(long pAffiliateID, List<AccountCustomField> pSearchFields) {
			string sql = @"SELECT *
							FROM Invoices
							WHERE InvoiceID IN (   SELECT InvoiceID
												   FROM InvoiceLineItem
												   WHERE (	 AccountCustomFieldID = @AccountCustomFieldID1
															 AND Value = @Value1)
														 OR (	AccountCustomFieldID = @AccountCustomFieldID2
																AND Value = @Value2)
												   GROUP BY InvoiceID
												   HAVING Count(*) = 2 )
								  AND AffiliateID = @AffiliateID 
							ORDER BY Cancelled";

			using (var conn = SqlHelper.OpenSqlConnection()) {
				DynamicParameters parms = new DynamicParameters();
				parms.Add("AffiliateID", pAffiliateID);
				parms.Add("AccountCustomFieldID1", pSearchFields[0].AccountCustomFieldID);
				parms.Add("Value1", pSearchFields[0].DefaultValue);
				parms.Add("AccountCustomFieldID2", pSearchFields[1].AccountCustomFieldID);
				parms.Add("Value2", pSearchFields[1].DefaultValue);

				return conn.Query<Invoices>(sql, parms).ToList();
			}
		}

		public static Invoices GetInvoiceByChecksum(long pAffiliateID, string pInvoiceNo, string pChecksum) {
			string sql = @"SELECT *
							FROM Invoices
							WHERE AffiliateID = @AffiliateID
								  AND InvoiceNo = @InvoiceNo
								  AND Checksum = @Checksum
								  AND Cancelled = 0";
			using (var conn = SqlHelper.OpenSqlConnection()) {
				return conn.Get<Invoices>(sql, new { AffiliateID = pAffiliateID, InvoiceNo = pInvoiceNo, Checksum = pChecksum });
			}
		}

		public static int SetInvoicesCancelledByInvoiceNo(long pAffiliateID, string pInvoiceNo) {
			string sql = @"UPDATE Invoices
							SET Cancelled = 1
							WHERE InvoiceNo = @InvoiceNo
								  AND AffiliateID = @AffiliateID
								  AND Cancelled = 0";

			using (var conn = SqlHelper.OpenSqlConnection()) {
				return conn.Execute(sql, new { AffiliateID = pAffiliateID, InvoiceNo = pInvoiceNo });
			}
		}

		public static List<Invoices> GetReventicsInvoices() {
			string sql = @"SELECT *
							FROM Invoices
							WHERE AffiliateID IN ( SELECT AffiliateID FROM dbo.Affiliate WHERE MainContact = 'Reventics' )
								  AND Cancelled = 0
							ORDER BY AffiliateID";

			using (var conn = SqlHelper.OpenSqlConnection()) {
				return conn.Query<Invoices>(sql).ToList();
			}
		}

	}
}