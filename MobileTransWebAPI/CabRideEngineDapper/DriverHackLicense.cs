﻿using Dapper;
using System.Data.SqlClient;

namespace CabRideEngineDapper {
    public partial class DriverHackLicense {

        [Editable(false)]
        public string Cell { get; set; }

        [Editable(false)]
        public long AffiliateID { get; set; }

        public DriverHackLicense() {
            this.LicenseClass = "Taxi";
        }

        //public static DriverHackLicense GetLicense(SqlConnection pConn, long pDriverID) {
        //    string sql = "select * from dbo.DriverHackLicense with (nolock) where DriverID = @DriverID";
        //    return pConn.Query<DriverHackLicense>(sql, new { DriverID = pDriverID }).DefaultIfEmpty(DriverHackLicense.GetNullEntity()).FirstOrDefault();
        //}

        public static DriverHackLicense GetLicenseByAffDriverID(SqlConnection pConn, long pAffDriverID) {
            string sql = "select * from dbo.DriverHackLicense with (nolock) where AffiliateDriverID = @AffiliateDriverID";
            return pConn.Get<DriverHackLicense>(sql, new { AffiliateDriverID = pAffDriverID }); //.DefaultIfEmpty(DriverHackLicense.GetNullEntity()).FirstOrDefault();
        }

        public static DriverHackLicense GetCreateByAffDriverID(SqlConnection pConn, long pAffDriverID) {
            DriverHackLicense rec = GetLicenseByAffDriverID(pConn, pAffDriverID);
            if (rec.IsNullEntity) {
                rec = DriverHackLicense.Create();
                rec.AffiliateDriverID = pAffDriverID;
            }
            return rec;
        }
    }
}
