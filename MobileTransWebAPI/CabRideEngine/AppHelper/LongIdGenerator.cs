/// <changelog>
///   <item who="jtraband" when="May-09-2002">Created</item>
///   <item who="jtraband" when="Mar-03-2003">Updated for new IIdGenerator signatures</item>
/// </changelog>

using System;
using System.Collections;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.Common;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Diagnostics;

using IdeaBlade.Util;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence;
//using IdeaBlade.Persistence.Rdb;

namespace IdeaBladeRdbTest.Entities1 {

    [Serializable]
    public class LongIdGenerator : IIdGenerator {


        public LongIdGenerator() {
            // need to handle the case where the same generator class has multiple instances
            // in a single pm.  We want to distinguish the tempids
            mGeneratorId = msNumGenerators++;


        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pDsKey"></param>
        /// <returns></returns>
        public bool IsApplicable(IDataSourceKey pDsKey) {
            return true;
        }

        public object GetNextTempId(EntityColumn pColumn) {
            Object id;
            // modified to handle ints as well
            long longId = (long)-1 * (mTempIds.Count + 1) + (mGeneratorId * -10000);
            if (pColumn.DataType == typeof(long)) {
                id = longId;
            } else if (pColumn.DataType == typeof(int)) {
                id = Convert.ToInt32(longId);
            } else {
                throw new NotSupportedException("This id generator can only generate ids of type int32 and int64");
            }
            mTempIds.Add(new UniqueId(pColumn, id));
            return id;
        }

        public UniqueIdCollection TempIds {
            get {
                return mTempIds;
            }
        }

        public bool IsTempId(UniqueId pUniqueId) {
            long id = Convert.ToInt64(pUniqueId.Value);
            return (id < 0);
        }

        public void Reset() {
            mTempIds.Clear();
        }

        // This method will only be called from the PersistenceServer;
        public UniqueIdMap GetRealIdMap(UniqueIdCollection pTempIds, IDataSourceKey pDataSourceKey) {
            UniqueIdMap idMap = new UniqueIdMap();
            RdbKey aRdbKey = pDataSourceKey as RdbKey;
            mAdoHelper = aRdbKey.AdoHelper;
            long nextLongId = 0;

            Collection<string> colTable = new Collection<string>();
            Collection<int> colCount = new Collection<int>();
            for (int i = 0; i < pTempIds.Count; i++) {
                int iPos = colTable.IndexOf(pTempIds[i].Column.EntityType.Name);
                if (iPos < 0) {
                    colTable.Add(pTempIds[i].Column.EntityType.Name);
                    colCount.Add(1);
                } else {
                    colCount[iPos] = colCount[iPos] + 1;
                }
                //nextLongId = GetNextId(pTempIds[i].Column.EntityType.Name);
            }
            for (int i = 0; i < colTable.Count; i++) {
                nextLongId = GetNextId(colCount[i], colTable[i]);

                object nextId;
                foreach (UniqueId aUniqueId in pTempIds) {
                    if (aUniqueId.Column.EntityType.Name == colTable[i]) {
                        if (aUniqueId.Column.DataType == typeof(int)) {
                            nextId = Convert.ToInt32(nextLongId);
                        } else {
                            nextId = nextLongId;
                        }
                        idMap.Add(aUniqueId, nextId);
                        nextLongId++;
                    }
                }
            }
            return idMap;
        }

        //***********************************
        // Private & Protected
        //***********************************


        private long GetNextId(int pCount, string pTableName) {
            // Serialize access to GetNextId - use typeof() to lock static members
            lock (typeof(LongIdGenerator)) {
                TraceFns.Assert(pCount > 0, "GetNextId requires a positive integer as its arg.");
                TraceFns.Assert(pCount <= MaxGroupSize, "Cannot allocate more than " + MaxGroupSize + " in any one call to GetNextId");

                if (msNextId + pCount > msMaxNextId) {
                    AllocateMoreIds(pCount, pTableName);
                }
                long result = msNextId;
                msNextId += pCount;
                return result;
            }
        }

        private void AllocateMoreIds(int pCount, string pTableName) {
            String sqlSelect = "select NextId from NextId where Name = '" + pTableName + "'";
            String sqlUpdate = "update NextId set NextId={0} where Name = '" + pTableName + "' and NextId={1}";

            // allocate the larger of the amount requested or the default alloc group size
            //pCount = Math.Max(pCount, DefaultGroupSize);

            IDbConnection aConnection = mAdoHelper.CreateDbConnection(true);
            aConnection.Open();
            using (aConnection) {
                IDbCommand aCommand = mAdoHelper.CreateDbCommand(aConnection);

                for (int trys = 0; trys <= MaxTrys; trys++) {

                    aCommand.CommandText = sqlSelect;
                    IDataReader aDataReader = aCommand.ExecuteReader();
                    if (!aDataReader.Read()) {
                        //record does not exist create new record
                        aCommand.CommandText = "INSERT INTO NextId (Name, NextId) VALUES ('" + pTableName + "', 1)";
                        aDataReader.Close();
                        if (aCommand.ExecuteNonQuery() != 1) {
                            throw new IdeaBladeException("Unable to locate 'NextId' record");
                        }
                        aCommand.CommandText = sqlSelect;
                        aDataReader = aCommand.ExecuteReader();
                        if (!aDataReader.Read()) {
                            throw new IdeaBladeException("Unable to locate 'NextId' even after creating a new record");
                        }
                    }

                    long nextId = aDataReader.GetInt64(0);
                    long newNextId = nextId + pCount;
                    aDataReader.Close();

                    // do the update;
                    aCommand.CommandText = String.Format(sqlUpdate, newNextId, nextId);

                    // if only one record was affected - we're ok; otherwise try again.
                    if (aCommand.ExecuteNonQuery() == 1) {
                        msNextId = nextId;
                        msMaxNextId = newNextId;
                        return;
                    }
                    System.Threading.Thread.Sleep(1000);
                }
            }
            throw new IdeaBladeException("Unable to generate a new id");
        }

        private UniqueIdCollection mTempIds = new UniqueIdCollection();
        private int mGeneratorId;

        [NonSerialized]
        private AdoHelper mAdoHelper;

        private const int MaxTrys = 3;
        private const int DefaultGroupSize = 100;
        private const int MaxGroupSize = 1000;


        private static long msNextId = 0;
        private static long msMaxNextId = 0;
        private static int msNumGenerators = 0;

        //private class MyIDs {
        //    string TableName;
        //    int Count;
        //}

    }


}
