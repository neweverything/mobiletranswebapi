﻿using CabRideEngineDapper.Utils;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using TaxiPassCommon;

namespace CabRideEngineDapper {
    public partial class DriverZone {

        private Drivers mDriver;
        public Drivers Drivers {
            get {
                if (mDriver == null) {
                    mDriver = Drivers.GetDriver(this.DriverID);
                }
                return mDriver;
            }
        }

        public static List<DriverZone> GetZones(Drivers pDriver, DateTime pStartDate, DateTime pEndDate) {
            string sql = @"SELECT  *
                            FROM    DriverZone
                            WHERE   DriverID = @DriverID
                                    AND ZoneEntered >= @startUTC
                                    AND ZoneEntered <= @endUTC";

            TimeZoneConverter convert = new TimeZoneConverter();
            DateTime startUTC = convert.ToUniversalTime(pDriver.MyAffiliate.TimeZone, pStartDate);
            DateTime endUTC = convert.ToUniversalTime(pDriver.MyAffiliate.TimeZone, pEndDate);

            DynamicParameters parms = new DynamicParameters();
            parms.Add("DriverID", pDriver.DriverID);
            parms.Add("startUTC", startUTC);
            parms.Add("endUTC", endUTC);

            using (var conn = SqlHelper.OpenSqlConnection()) {
                return conn.Query<DriverZone>(sql, parms).ToList();
            }
        }

        [Editable(false)]
        public DateTime ZoneEnteredLocalTime {
            get {
                if (this.Drivers.MyAffiliate.TimeZone.IsNullOrEmpty()) {
                    return base.ZoneEntered;
                }
                TimeZoneConverter convert = new TimeZoneConverter();
                return convert.FromUniversalTime(this.Drivers.MyAffiliate.TimeZone, this.ZoneEntered);
            }
        }
    }
}
