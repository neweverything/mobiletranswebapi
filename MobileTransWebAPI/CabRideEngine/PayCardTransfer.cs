﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using System.Text;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

using TaxiPassCommon;

namespace CabRideEngine {
    /// <summary>
    /// The developer class representing the PayCardTransfer business object class.
    /// </summary>
    /// <remarks>
    /// <para>Place all application-specific business logic in this class.</para>
    /// <para>
    /// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
    /// and will not overwrite changes.
    /// </para>
    /// <para>
    /// This class inherits from the generated class, its companion in the pair of classes 
    /// defining a business object. Properties in the generated class may be overridden in 
    /// order to change the behavior or add business logic.
    /// </para>
    /// <para>
    /// This class is the final class in the inheritance chain for this business object. 
    /// It should not be inherited.
    /// </para>
    /// </remarks>
    [Serializable]
    public sealed partial class PayCardTransfer : PayCardTransferDataRow {

        #region Constructors -- DO NOT MODIFY

        // Do not create constructors for this class
        // Developers cannot create instances of this class with the "new" keyword
        // You should write a static Create method instead; see the "Suggested Customization" region
        // Please review the documentation to learn about entity creation and inheritance

        // This private constructor prevents accidental instance creation via 'new';
        // it is also required if the class is visible from a web service.
        // DO NOT REMOVE **
        private PayCardTransfer() : this(null) { }

        /// <summary>
        /// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
        /// </summary>
        /// <param name="pRowBuilder"></param>
        public PayCardTransfer(DataRowBuilder pRowBuilder)
            : base(pRowBuilder) {
        }

        #endregion

        #region Suggested Customizations

        //    // Use this factory method template to create new instances of this class
        //    public static PayCardTransfer Create(PersistenceManager pManager,
        //      ... your creation parameters here ...) { 
        //
        //      PayCardTransfer aPayCardTransfer = pManager.CreateEntity<PayCardTransfer>();
        //
        //      // if this object type requires a unique id and you have implemented
        //      // the IIdGenerator interface implement the following line
        //      pManager.GenerateId(aPayCardTransfer, // add id column here //);
        //
        //      // Add custom code here
        //
        //      aPayCardTransfer.AddToManager();
        //      return aPayCardTransfer;
        //    }

        //    // Implement this method if you want your object to sort in a specific order
        //    public override int CompareTo(Object pObject) {
        //    }

        //    // Implement this method to customize the null object version of this class
        //    protected override void UpdateNullEntity() {
        //    }

        #endregion

        // Add additional logic to your business object here...

		
		// *************************************************************************
		// Override the default save to check for RedeemerShift data
		// *************************************************************************
		public override SaveResult Save() {
			SaveResult oResult = base.Save();

			if (!this.Failed) {
				// Add to ATMAccess if one of these cards belongs to a Redeemer's shift
				StringBuilder sql = new StringBuilder();
				sql.Append("usp_RedeemerShiftCardCheck @CardNo1 = '");
				sql.Append(this.TransCardToAdminNo);
				sql.Append("', @CardNo2 = '");
				sql.Append(this.TransCardFromAdminNo);
				sql.Append("'");

				DataTable dtShift = CabRideEngine.Utils.SQLHelper.GetRecords(this.PersistenceManager, sql.ToString());
				if (dtShift.Rows.Count > 0) {
					string sShiftID = dtShift.Rows[0]["RedeemerShiftID"].ToString();
					string sTaxiPassRedeemerID = dtShift.Rows[0]["TaxiPassRedeemerID"].ToString();
					// ATMID 1 is the generic "ATM machine"
					sql.Remove(0, sql.Length);
					sql.Append("usp_ATMAccessAdd @ATMID = 1");

					sql.Append(", @FromAccount ='");
					sql.Append(this.TransCardFromAdminNo);

					sql.Append("', @ToAccount = '");
					sql.Append(this.TransCardToAdminNo);

					sql.Append("', @TransferAmount = ");
					sql.Append(this.Amount);

					sql.Append(", @AffiliateDriverID = null");

					sql.Append(", @TaxiPassRedeemerID = ");
					sql.Append(sTaxiPassRedeemerID);

					sql.Append(", @PayCardTransferID = ");
					sql.Append(this.PayCardTransferID.ToString());

					sql.Append(", @Description = 'PayCard Transfer: " + this.Notes.Replace("'","''"));

					sql.Append("', @FixID = null");

					sql.Append(", @RedeemerShiftID = ");
					sql.Append(sShiftID);
					CabRideEngine.Utils.SQLHelper.GetRecords(this.PersistenceManager, sql.ToString());
				}
			}

			return oResult;
		}



        public static PayCardTransfer Create(PersistenceManager pManager) {

            PayCardTransfer rec = pManager.CreateEntity<PayCardTransfer>();

            pManager.GenerateId(rec, PayCardTransfer.PayCardTransferIDEntityColumn);
            rec.AddToManager();
            return rec;
        }

        public static EntityList<PayCardTransfer> GetAll(PersistenceManager pPM) {
            RdbQuery qry = new RdbQuery(typeof(PayCardTransfer));
            qry.AddOrderBy(PayCardTransfer.ModifiedDateEntityColumn);
            return pPM.GetEntities<PayCardTransfer>(qry, QueryStrategy.DataSourceThenCache);
        }

        public static PayCardTransfer GetByExternalConfNo(PersistenceManager pPM, string pExternalConfNo) {
            RdbQuery qry = new RdbQuery(typeof(PayCardTransfer), PayCardTransfer.ExternalConfNoEntityColumn, EntityQueryOp.EQ, pExternalConfNo);
            return pPM.GetEntity<PayCardTransfer>(qry, QueryStrategy.DataSourceThenCache);
        }

        public static PayCardTransfer GetByAdminAndAmount(PersistenceManager pPM, string pTransCardAdminNo, decimal pAmount) {
            RdbQuery qry = new RdbQuery(typeof(PayCardTransfer), PayCardTransfer.AmountEntityColumn, EntityQueryOp.EQ, pAmount);
            qry.AddClause(PayCardTransfer.TransCardFromAdminNoEntityColumn, EntityQueryOp.EQ, pTransCardAdminNo);
            qry.AddClause(PayCardTransfer.TransCardToAdminNoEntityColumn, EntityQueryOp.EQ, pTransCardAdminNo);
            qry.AddOperator(EntityBooleanOp.Or);
            return pPM.GetEntity<PayCardTransfer>(qry, QueryStrategy.DataSourceThenCache);
        }

		public static EntityList<PayCardTransfer> GetListByAdminAndAmount(PersistenceManager pPM, string pTransCardAdminNo, decimal pAmount) {
			RdbQuery qry = new RdbQuery(typeof(PayCardTransfer), PayCardTransfer.AmountEntityColumn, EntityQueryOp.EQ, pAmount);
			qry.AddClause(PayCardTransfer.TransCardFromAdminNoEntityColumn, EntityQueryOp.EQ, pTransCardAdminNo);
			qry.AddClause(PayCardTransfer.TransCardToAdminNoEntityColumn, EntityQueryOp.EQ, pTransCardAdminNo);
			qry.AddOperator(EntityBooleanOp.Or);
			return pPM.GetEntities<PayCardTransfer>(qry, QueryStrategy.DataSourceThenCache);
		}


        public override string TransCardFromNumber {
            get {
                return base.TransCardFromNumber.DecryptIceKey();
            }
            set {
                base.TransCardFromNumber = value.EncryptIceKey();
                base.TransCardFromCardNumber = CustomerCardNumberDisplay(value);
            }
        }

        public override string TransCardToNumber {
            get {
                return base.TransCardToNumber.DecryptIceKey();
            }
            set {
                base.TransCardToNumber = value.EncryptIceKey();
                base.TransCardToCardNumber = CustomerCardNumberDisplay(value);
            }
        }

        private static string CustomerCardNumberDisplay(string pCardNo) {
            if (pCardNo.Length > 10) {
                string stars = new string('*', 10);
                return pCardNo.Left(1) + stars + pCardNo.Right(5);
            }
            return pCardNo;
        }

    }

    #region EntityPropertyDescriptors.PayCardTransferPropertyDescriptor
    ////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
    ////    For example: 
    ////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
    //public static partial class EntityPropertyDescriptors {
    //	public partial class PayCardTransferPropertyDescriptor : BaseEntityPropertyDescriptor {
    //    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
    //    public AdaptedPropertyDescriptor Age {
    //      get { return this.Get("Age"); }
    //    }
    //  }
    //} 
    #endregion

}