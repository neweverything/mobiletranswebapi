using System;
using System.Collections.Generic;
using System.Text;

using System.Diagnostics;
using System.Runtime.Serialization;

using IdeaBlade.Util;

using CabRideEngine.Utils;

namespace CabRideEngine {

	[Serializable]
	public class AppLoginException : IdeaBladeException {

		private const String LOGIN_EXCEPTION_TYPE = "AppLoginExceptionType";
		private const String MESSAGE_TYPE = "MessageType";
		private const String USER_NAME = "UserName";

		protected AppLoginException(SerializationInfo pInfo, StreamingContext pContext)
			: base(pInfo, pContext) {
			AppLoginExceptionType = (AppLoginExceptionType)pInfo.GetValue(LOGIN_EXCEPTION_TYPE, typeof(AppLoginExceptionType));
			MessageType = (MessageTypes)pInfo.GetValue(MESSAGE_TYPE, typeof(MessageTypes));
			UserName = (String)pInfo.GetValue(USER_NAME, typeof(String));
		}

		public override void GetObjectData(SerializationInfo pInfo, StreamingContext pContext) {
			base.GetObjectData(pInfo, pContext);
			pInfo.AddValue(LOGIN_EXCEPTION_TYPE, AppLoginExceptionType);
			pInfo.AddValue(MESSAGE_TYPE, MessageType);
			pInfo.AddValue(USER_NAME, UserName);
		}

		public AppLoginException(AppLoginExceptionType pAppLoginExceptionType, MessageTypes pMessageType)
			:
		  this(pAppLoginExceptionType, pMessageType, String.Empty) {
		}

		public AppLoginException(AppLoginExceptionType pAppLoginExceptionType, MessageTypes pMessageType, String pUserName) {
			AppLoginExceptionType = pAppLoginExceptionType;
			MessageType = pMessageType;
			UserName = pUserName;
		}

		public override string Message {
			get {
				return GetMessage(MessageType);
			}
		}

		public static string GetMessage(MessageTypes pMessageType) {
			// return String.Format(MessageCache.Messages.Message(MessageType), UserName);
			switch (pMessageType) {
				case MessageTypes.IncorrectPassword:
					return "Incorrect Password";
				case MessageTypes.InvalidPassword:
					return "Invalid Password";
				case MessageTypes.InvalidPasswordLength:
					return "Invalid Password Length";
				case MessageTypes.InvalidUserName:
					return "Invalid User Name";
				case MessageTypes.EnterUsername:
					return "Enter User Name";
				case MessageTypes.EnterPassword:
					return "Enter Password";
				default:
					return "Miscellaneous User Error";
			}
		}

		public readonly AppLoginExceptionType AppLoginExceptionType;
		public readonly MessageTypes MessageType;
		public readonly String UserName;

	}

	public enum AppLoginExceptionType {
		None,
		UserName,
		Password
	}


}

