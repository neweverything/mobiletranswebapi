﻿using System;
using TaxiPassCommon.Banking.Interfaces;
using static TaxiPassCommon.Banking.FSV;

namespace TaxiPassCommon.Banking {
	public class PayCardGateway {

		private IPayCard mPayCard;
		private IPayCardCredentials mCred;

		public PayCardGateway(IPayCardCredentials pCred) {
			mCred = pCred;

			switch (pCred.MyPayCardType()) {
				case Enums.PayCardTypes.USBank:
				//case Enums.PayCardTypes.USBankNewTek:
				case Enums.PayCardTypes.RapidPay:
					//case Enums.PayCardTypes.RapidPayNewTek:
					FSV fsv = new FSV(pCred, pCred.UseProductionAsBoolen);
					mPayCard = fsv;
					break;

				case Enums.PayCardTypes.TransCard:
					try {
						TransCard transCard = new TransCard(pCred, pCred.UseProductionAsBoolen);
						mPayCard = transCard;
					} catch (Exception ex) {
						Console.WriteLine(ex.Message);
					}
					break;

				case Enums.PayCardTypes.GlobalCashCard:
				case Enums.PayCardTypes.GlobalCashCardMM:
					try {
						GlobalCashCard card = new GlobalCashCard(pCred, pCred.UseProductionAsBoolen);
						mPayCard = card;
					} catch (Exception ex) {
						Console.WriteLine(ex.Message);
					}
					break;

			}
		}

		public CardRegistrationResponse RegisterCard(CardRegistration pRegistration) {
			try {
				return mPayCard.RegisterCard(pRegistration);
			} catch (Exception ex) {
				Console.WriteLine(ex.Message);
			}
			return new CardRegistrationResponse();
		}


		public FundsTransferResponse TransferToPayCard(FundsTransfer pTransfer) {
			try {
				return mPayCard.TransferToPayCard(pTransfer);
			} catch (Exception ex) {
				Console.WriteLine(ex.Message);
			}
			return new FundsTransferResponse();
		}

		public VerifyConnectionResponse VerifyConnection() {
			try {
				return mPayCard.VerifyConnection();
			} catch (Exception ex) {
				Console.WriteLine(ex.Message);
			}
			return new VerifyConnectionResponse();
		}

		public decimal GetFundingBalance() {
			try {
				return mPayCard.GetFundingBalance();
			} catch (Exception ex) {
				Console.WriteLine(ex.Message);
			}
			return 0;
		}
	}
}
