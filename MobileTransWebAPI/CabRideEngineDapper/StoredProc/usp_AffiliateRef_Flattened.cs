﻿using CabRideEngineDapper.Utils;
using Dapper;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace CabRideEngineDapper {

    [Serializable]
    public class usp_AffiliateRef_Flattened {
        public string BillingDescriptor { get; set; }
        public string EnablePayCardInfo { get; set; }
        public string BankInfoOnWebSite { get; set; }
        public string AverageVoucherAmount { get; set; }
        public string NewDriverVoucherCount { get; set; }
        public string VoucherCount { get; set; }
        public string ReceiptPromo { get; set; }
        public string SignaturePromo { get; set; }
        public string ExtraFees { get; set; }
        public string SmartPhoneFeeMaxAmt { get; set; }
        public string SmartPhoneFeeAmtPerRate { get; set; }
        public string SmartPhoneFeeAmt { get; set; }
        public string DisplayReadOnlyHideDontPayRefundFlags { get; set; }
        public string DoNotTextDrivers { get; set; }
        public string FundPayCardDelayedMinusHours { get; set; }
        public string FundPayCardDelayed { get; set; }
        public string MerchantID { get; set; }
        public string AmexMerchantID { get; set; }
        public string MerchantInfo { get; set; }
        public string DoNotPay { get; set; }
        public string SoftwareLicensing { get; set; }
        public string ReceiptTemplate { get; set; }
        public string Logo { get; set; }

        public static usp_AffiliateRef_Flattened Execute(SqlConnection pConn, long pAffiliateID) {
            return pConn.Query<usp_AffiliateRef_Flattened>("usp_AffiliateRef_Flattened", new { AffiliateID = pAffiliateID }, commandType: CommandType.StoredProcedure).FirstOrDefault();
        }

        public static usp_AffiliateRef_Flattened Execute(long pAffiliateID) {
            using (var conn = SqlHelper.OpenSqlConnection()) {
                return Execute(conn, pAffiliateID);
            }
        }

    }
}
