﻿using CabRideEngine;
using IdeaBlade.Persistence;
using MobileTransWebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Http;
using System.Web.Script.Serialization;
using TaxiPassCommon;

namespace MobileTransWebAPI.Controllers {

    public class TransHistoryController : ApiController {

        public TransListResult Get() {
            //return GetTransHistoryWithBankPending("6174D0EA968F72ECF2B49E6A617F822D52B00ADEBA0D5E7A229AAD4C87EE034D8AAFE565F67EF8D565ECF3D759EC600079F601DF48381042B6942163D25CAB559C5F5D98BD8A7627CCEC27E3025EAF4FFE5D3B0D508D8238845C106C7D37E12593B376BB203CF5B0E17437E7D8C3DA8E256D937FAE9DA4B68D4D40AB1765EDB7AEB8E5B5C809A188B3DA37851805312B864728CC");
            return GetTransHistory("F618C5A45279149B626AA319CE2E453874D0076656367ECC9F80943C8088170BE47A99A635DB5FEEF5B1DF30FED24426781EAF41A2CEFC06460CD09A6ED4A398D9BF1E84D7D081275BDA9B57BFD84A3AF3C02AC2B4ADF0EEC1AC123C9774A6B7FC7AFB84889B9C4F79058CFAA8ABA0840278467ED1CCBE838489B350EAB2821D7669ABD714CC583159E5BD5EF59F3548ECACC3EA");
        }



        public TransListResult GetTransHistory(string pTransRequest) {
            TransListResult result = new TransListResult();
            JavaScriptSerializer js = new JavaScriptSerializer();

            pTransRequest = pTransRequest.DecryptIceKey();
            WebApiApplication.LogIt("GetTransHistory", pTransRequest);

            PersistenceManager oPM = WebApiApplication.UserPM;
            if (oPM == null) {
                result.ErrorMsg = "Could not connect to database";
                return result;
            }

            TransListRequest info = js.Deserialize<TransListRequest>(pTransRequest);

            if (info.DriverID < 1) {
                result.ErrorMsg = "Invalid Access";
                return result;
            }

            Drivers oDriver = Drivers.GetDriver(oPM, info.DriverID);
            if (oDriver.IsNullEntity) {
                result.ErrorMsg = "Invalid Driver";
                return result;
            }

            try {
                DateTime requestDate = DateTime.Today.AddDays(-1);
                if (info != null) {
                    requestDate = info.RequestKey.DecryptIceKey().ToDateTime();
                }

                if (Math.Abs(WebApiApplication.GetUTCServerTime().Subtract(requestDate).TotalMinutes) > 1) {
                    result.ErrorMsg = "Invalid Access";
                    return result;
                }
            } catch (Exception) {
                result.ErrorMsg = "Invalid Access";
                return result;
            }


            AffiliateDrivers affDriver = AffiliateDrivers.GetDriver(oPM, info.AffDriverID);
            EntityList<DriverPayments> oList = new EntityList<DriverPayments>();
            int month = DateTime.Now.Month;
            int year = DateTime.Now.Year;
            DateTime dStart = new DateTime(year, month, 1);
            DateTime dEnd = dStart.AddMonths(1).AddSeconds(-1); // dStart.AddMonths(1).AddSeconds(-1);
            if (affDriver.IsNullEntity) {
                if (!info.CurrentMonth) {
                    dStart = dStart.AddMonths(-1);
                    dEnd = dStart.AddMonths(1).AddSeconds(-1);
                }
                oList.AddRange(DriverPayments.GetCharges(oDriver, dStart, dEnd, false));
            } else {
                if (!info.CurrentMonth) {
                    dStart = dStart.AddMonths(-1);
                    dEnd = dStart.AddMonths(1).AddSeconds(-1);
                }

                oList.AddRange(DriverPayments.GetCharges(affDriver, dStart, dEnd, false));

            }

            oList = new EntityList<DriverPayments>((from p in oList
                                                    where !p.Test && !p.Failed && !p.ChargeBack && p.Fare > 0
                                                    select p).ToList());

            StringBuilder sPaid = new StringBuilder();
            StringBuilder sTrans = new StringBuilder();
            bool addDriverFee = oDriver.MyAffiliate.AffiliateDriverDefaults.CarmelDispatch;

            foreach (DriverPayments oPay in oList) {
                decimal nTotal = oPay.DriverTotalPaid; //		oPay.TotalCharge - oPay.TaxiPassFee - oPay.DriverFee.GetValueOrDefault(0);
                if (addDriverFee) {
                    nTotal += oPay.DriverFee.GetValueOrDefault(0);
                }
                string sText = String.Format("{0},{1},{2},{3},{4},{5}|", oPay.ChargeDate.Value.ToString(info.FullDate ? "MM/dd/yyyy" : "MM/dd"), nTotal.ToString("C"), oPay.Test ? "Test" : oPay.TransNo, oPay.CardNumberDisplay.IsNullOrEmpty() ? "N/A" : oPay.CardNumberDisplay.Right(5), oPay.DriverPaidDate.HasValue ? oPay.DriverPaidDate.Value.ToString(info.FullDate ? "MM/dd/yyyy" : "MM/dd") : "", oPay.ACHDetail.ACHPaidDate.HasValue ? oPay.ACHDetail.ACHPaidDate.Value.ToString(info.FullDate ? "MM/dd/yyyy" : "MM/dd") : "");
                //sOutput.Append(StringUtils.XMLTag("Rec" + i.ToString(), sText));

                if (oPay.ACHDetail.ACHPaidDate.HasValue || oPay.TaxiPassRedeemerID.HasValue || oPay.DriverPaid) {
                    sPaid.Append(sText);
                } else {
                    sTrans.Append(sText);
                }
            }
            if (sPaid.Length > 2) {
                result.PaidTrans = sPaid.Remove(sPaid.Length - 1, 1).ToString();
            }
            if (sTrans.Length > 2) {
                result.Trans = sTrans.Remove(sTrans.Length - 1, 1).ToString();
            }

            return result;
        }


        public TransListResult GetTransHistoryWithBankPending(string pRequest) {
            TransListResult result = new TransListResult();
            JavaScriptSerializer js = new JavaScriptSerializer();

            pRequest = pRequest.DecryptIceKey();
            WebApiApplication.LogIt("GetTransHistoryWithBankPending", pRequest);

            PersistenceManager oPM = WebApiApplication.UserPM;
            if (oPM == null) {
                result.ErrorMsg = "Could not connect to database";
                return result;
            }

            Drivers oDriver = oPM.GetNullEntity<Drivers>();
            TransListRequest info = new TransListRequest();
            try {
                info = js.Deserialize<TransListRequest>(pRequest);

                if (info.DriverID < 1) {
                    result.ErrorMsg = "Invalid Access";
                    return result;
                }

                oDriver = Drivers.GetDriver(oPM, info.DriverID);
                if (oDriver.IsNullEntity) {
                    result.ErrorMsg = "Invalid Driver";
                    return result;
                }

                DateTime requestDate = DateTime.Today.AddDays(-1);
                if (info != null) {
                    requestDate = info.RequestKey.DecryptIceKey().ToDateTime();
                }

                if (Math.Abs(WebApiApplication.GetUTCServerTime().Subtract(requestDate).TotalMinutes) > 1) {
                    result.ErrorMsg = "Invalid Access";
                    return result;
                }
            } catch (Exception) {
                result.ErrorMsg = "Invalid Access";
                return result;
            }


            AffiliateDrivers affDriver = AffiliateDrivers.GetDriver(oPM, info.AffDriverID);
            List<CabRideEngineDapper.Custom.TransHistory> oList = new List<CabRideEngineDapper.Custom.TransHistory>(); // new EntityList<DriverPayments>();
            int month = DateTime.Now.Month;
            int year = DateTime.Now.Year;
            DateTime dStart = new DateTime(year, month, 1);
            DateTime dEnd = dStart.AddMonths(1).AddSeconds(-1); // dStart.AddMonths(1).AddSeconds(-1);
            if (affDriver.IsNullEntity) {
                if (!info.CurrentMonth) {
                    dStart = dStart.AddMonths(-1);
                    dEnd = dStart.AddMonths(1).AddSeconds(-1);
                }
                oList.AddRange(CabRideEngineDapper.Custom.TransHistory.GetTransHistoryForHandHelds(oDriver.DriverID, dStart, dEnd, Properties.Settings.Default.AllowTestTrans.Split('|').Contains(affDriver.Cell)));
            } else {
                if (!info.CurrentMonth) {
                    dStart = dStart.AddMonths(-1);
                    dEnd = dStart.AddMonths(1).AddSeconds(-1);
                }

                oList.AddRange(CabRideEngineDapper.Custom.TransHistory.GetTransHistory(affDriver.AffiliateDriverID, dStart, dEnd, Properties.Settings.Default.AllowTestTrans.Split('|').Contains(affDriver.Cell)));

            }

            //oList = new EntityList<DriverPayments>((from p in oList
            //                                        where (!p.Test || (Properties.Settings.Default.AllowTestTrans.Split('|').Contains(affDriver.Cell))) && !p.Failed && !p.ChargeBack && p.Fare > 0
            //                                        select p).ToList());

            StringBuilder sPaid = new StringBuilder();
            StringBuilder sTrans = new StringBuilder();
            StringBuilder sPending = new StringBuilder();

            bool addDriverFee = oDriver.MyAffiliate.AffiliateDriverDefaults.CarmelDispatch;

            string sAppVers = "0";
            if (!oDriver.AppVersion.IsNullOrEmpty()) {
                List<string> temp = oDriver.AppVersion.Substring(1).Split('.').ToList();
                while (temp.Count > 2) {
                    temp.RemoveAt(temp.Count - 1);
                }
                sAppVers = string.Join(".", temp.ToArray());
            }
            decimal appVers = Convert.ToDecimal(sAppVers);


            foreach (var oPay in oList) {
                decimal nTotal = oPay.DriverTotalPaid; //		oPay.TotalCharge - oPay.TaxiPassFee - oPay.DriverFee.GetValueOrDefault(0);
                if (addDriverFee) {
                    nTotal += oPay.DriverFee.GetValueOrDefault(0);
                }

                string sText;

                // added logic below to ensure we don't mess up existing drivers
                if ((affDriver.BankAccountNumber.IsNullOrEmpty() && affDriver.TransCardAdminNo.IsNullOrEmpty()) || appVers < 2.998M) {
                    sText = String.Format("{0},{1},{2},{3}|", oPay.ChargeDate.ToString(info.FullDate ? "MM/dd/yyyy" : "MM/dd"), nTotal.ToString("C"), oPay.Test ? "Test" : oPay.TransNo, oPay.CardNumberDisplay.IsNullOrEmpty() ? "N/A" : oPay.CardNumberDisplay.Right(5));
                } else {
                    sText = String.Format("{0},{1},{2},{3},{4},{5}|", oPay.ChargeDate.ToString(info.FullDate ? "MM/dd/yyyy" : "MM/dd"), nTotal.ToString("C"), oPay.Test ? "Test" : oPay.TransNo, oPay.CardNumberDisplay.IsNullOrEmpty() ? "N/A" : oPay.CardNumberDisplay.Right(5), oPay.DriverPaidDate.HasValue ? oPay.DriverPaidDate.Value.ToString(info.FullDate ? "MM/dd/yyyy" : "MM/dd") : "", oPay.ACHPaidDate.HasValue ? oPay.ACHPaidDate.Value.ToString(info.FullDate ? "MM/dd/yyyy" : "MM/dd") : "");
                }
                //sOutput.Append(StringUtils.XMLTag("Rec" + i.ToString(), sText));

                if (!oPay.TaxiPassRedeemerID.HasValue && oPay.DriverPaid && (!oPay.ACHPaidDate.HasValue || oPay.ACHPaidDate.GetValueOrDefault(DateTime.Today.AddMonths(-1)) == DateTime.Today)) {
                    sPending.Append(sText);
                } else if (oPay.ACHPaidDate.HasValue || oPay.TaxiPassRedeemerID.HasValue || oPay.DriverPaid) {
                    sPaid.Append(sText);
                } else {
                    sTrans.Append(sText);
                }
            }
            if (sPaid.Length > 2) {
                result.PaidTrans = sPaid.Remove(sPaid.Length - 1, 1).ToString();
            }
            if (sTrans.Length > 2) {
                result.Trans = sTrans.Remove(sTrans.Length - 1, 1).ToString();
            }
            if (sPending.Length > 2) {
                result.PendingTrans = sPending.Remove(sPending.Length - 1, 1).ToString();
            }

            return result;
        }

    }
}
