﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;
using Csla.Validation;
using TaxiPassCommon;

namespace CabRideEngine {
	/// <summary>
	/// The developer class representing the ReceiptRequest business object class.
	/// </summary>
	/// <remarks>
	/// <para>Place all application-specific business logic in this class.</para>
	/// <para>
	/// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
	/// and will not overwrite changes.
	/// </para>
	/// <para>
	/// This class inherits from the generated class, its companion in the pair of classes 
	/// defining a business object. Properties in the generated class may be overridden in 
	/// order to change the behavior or add business logic.
	/// </para>
	/// <para>
	/// This class is the final class in the inheritance chain for this business object. 
	/// It should not be inherited.
	/// </para>
	/// </remarks>
	[Serializable]
	public sealed partial class ReceiptRequest : ReceiptRequestDataRow {

		#region Constructors -- DO NOT MODIFY

		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new';
		// it is also required if the class is visible from a web service.
		// DO NOT REMOVE **
		private ReceiptRequest() : this(null) { }

		/// <summary>
		/// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
		/// </summary>
		/// <param name="pRowBuilder"></param>
		public ReceiptRequest(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static ReceiptRequest Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      ReceiptRequest aReceiptRequest = pManager.CreateEntity<ReceiptRequest>();
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aReceiptRequest, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aReceiptRequest.AddToManager();
		//      return aReceiptRequest;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		// Add additional logic to your business object here...
		public static ReceiptRequest Create(DriverPayments pDriverPayments) {
			PersistenceManager oPM = pDriverPayments.PersistenceManager;
			SystemDefaults defs = SystemDefaults.GetDefaults(oPM);

			RdbQuery qry = new RdbQuery(typeof(ReceiptRequest), CabRideEngine.ReceiptRequest.DriverPaymentIDEntityColumn, EntityQueryOp.EQ, pDriverPayments.DriverPaymentID);

			ReceiptRequest rec = oPM.GetEntity<ReceiptRequest>(qry);
			if (rec.IsNullEntity) {
				rec = oPM.CreateEntity<ReceiptRequest>();
				oPM.GenerateId(rec, CabRideEngine.ReceiptRequest.ReceiptRequestIDEntityColumn);
				rec.DriverPaymentID = pDriverPayments.DriverPaymentID;
				rec.ReceiptRequest = DateTime.Today;
				rec.ReceiptRequestDue = DateTime.Today.AddDays(defs.RequestReceiptDueDays);
				if (defs.RequestReceiptChargeDays != 0) {
					rec.ChargeBackDueDate = DateTime.Today.AddDays(defs.RequestReceiptChargeDays);
				}
				if (!pDriverPayments.DriverPaymentVoucherImages.IsNullEntity || !pDriverPayments.DriverPaymentAux.SignaturePath.IsNullOrEmpty()) {
					rec.ValidReceiptReceived = DateTime.Today;
				}
				rec.AddToManager();
			}
			return rec;
		}

		protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
			if (IsDeserializing) {
				return;
			}
			base.OnColumnChanging(pArgs);
			DoAudit(pArgs);
		}

		private void DoAudit(DataColumnChangeEventArgs pArgs) {
			if (!this.PersistenceManager.IsClient) {
				return;
			}
			if (this.RowState == DataRowState.Added) {
				return;
			}
			if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
				AuditRecord = ReceiptRequestAudit.Create(this);
			}
		}

		protected override void AddRules(RuleList pList) {
			PropertyRequiredRule.AddToList(pList, CabRideEngine.ReceiptRequest.ReceiptMerchantNoIDEntityColumn.ColumnName);
			PropertyRequiredRule.AddToList(pList, CaseNumberEntityColumn.ColumnName);
			PropertyRequiredRule.AddToList(pList, ReceiptRequestTypeIDEntityColumn.ColumnName);
			PropertyRequiredRule.AddToList(pList, ReceiptRequestNotesEntityColumn.ColumnName);
		}

		public static EntityList<ReceiptRequest> GetReceiptRequests(PersistenceManager pPM, DateTime pStart, DateTime pEnd) {
			RdbQuery qry = new RdbQuery(typeof(ReceiptRequest), CabRideEngine.ReceiptRequest.ReceiptRequestEntityColumn, EntityQueryOp.GE, pStart);
			qry.AddClause(CabRideEngine.ReceiptRequest.ReceiptRequestEntityColumn, EntityQueryOp.LE, pEnd);
			return pPM.GetEntities<ReceiptRequest>(qry, QueryStrategy.DataSourceThenCache);
		}

		public static ReceiptRequest GetReceiptRequest(DriverPayments pDriverPayment) {
			RdbQuery qry = new RdbQuery(typeof(ReceiptRequest), CabRideEngine.ReceiptRequest.DriverPaymentIDEntityColumn, EntityQueryOp.EQ, pDriverPayment.DriverPaymentID);
			return pDriverPayment.PersistenceManager.GetEntity<ReceiptRequest>(qry);
		}

		public override bool? ChargeBack911 {
			get {
				return base.ChargeBack911.GetValueOrDefault(false);
			}
			set {
				base.ChargeBack911 = value;
			}
		}
		public string CBStatusUpdatedAsString {
			get {
				if (CBStatusUpdated.HasValue) {
					return CBStatusUpdated.Value.ToString("MM/dd/yyyy hh:mm tt");
				}
				return "";
			}
		}


	}

	#region EntityPropertyDescriptors.ReceiptRequestPropertyDescriptor
	////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
	////    For example: 
	////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
	//public static partial class EntityPropertyDescriptors {
	//	public partial class ReceiptRequestPropertyDescriptor : BaseEntityPropertyDescriptor {
	//    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
	//    public AdaptedPropertyDescriptor Age {
	//      get { return this.Get("Age"); }
	//    }
	//  }
	//} 
	#endregion

}