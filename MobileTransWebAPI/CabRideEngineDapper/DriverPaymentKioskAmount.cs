﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Dapper;
using CabRideEngineDapper.Utils;

namespace CabRideEngineDapper {
	public partial class DriverPaymentKioskAmount {

		public static DriverPaymentKioskAmount GetKioskAmount(long pDriverPaymentID) {
			string sql = @"SELECT * FROM dbo.DriverPaymentKioskAmount
							WHERE DriverPaymentID = @DriverPaymentID";

			using (var conn = SqlHelper.OpenSqlConnection()) {
				return conn.Get<DriverPaymentKioskAmount>(sql, new { DriverPaymentID = pDriverPaymentID });
			}
		}
	}
}
