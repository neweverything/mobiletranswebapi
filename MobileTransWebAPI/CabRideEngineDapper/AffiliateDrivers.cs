﻿using CabRideEngineDapper.Enums;
using CabRideEngineDapper.Utils;
using Dapper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using TaxiPassCommon;
using TaxiPassCommon.Banking;

namespace CabRideEngineDapper {
	public partial class AffiliateDrivers {

		#region Related Tables
		private DriverStatus mDriverStatus;
		public DriverStatus DriverStatus {
			get {
				if (!DriverStatusID.HasValue) {
					return CabRideEngineDapper.DriverStatus.GetNullEntity();
				}
				if (mDriverStatus == null || mDriverStatus.DriverStatusID != this.DriverStatusID) {
					mDriverStatus = CabRideEngineDapper.DriverStatus.GetDriverStatus(this.DriverStatusID.Value);
				}
				return mDriverStatus;
			}
		}

		private AffiliateDriverReferrals mAffiliateDriverReferrals;
		public AffiliateDriverReferrals AffiliateDriverReferrals {
			get {
				if (mAffiliateDriverReferrals == null) {
					mAffiliateDriverReferrals = CabRideEngineDapper.AffiliateDriverReferrals.GetReferredDriver(this.AffiliateDriverID);
				}
				return mAffiliateDriverReferrals;
			}
		}

		private Affiliate mAffiliate;
		public Affiliate Affiliate {
			get {
				if (mAffiliate == null) {
					mAffiliate = Affiliate.GetAffiliate(this.AffiliateID);
				}
				return mAffiliate;
			}
		}

		private MerchantInfo mMerchantInfo;
		public MerchantInfo MerchantInfo {
			get {
				if (!this.MerchantID.HasValue) {
					return null;
				}
				if (mMerchantInfo == null) {
					mMerchantInfo = MerchantInfo.GetMerchant(this.MerchantID.Value);
				}
				return mMerchantInfo;
			}
		}

		private AffiliateDriverFraudDefaults mAffiliateDriverFraudDefaults;
		public AffiliateDriverFraudDefaults AffiliateDriverFraudDefaults {
			get {
				if (mAffiliateDriverFraudDefaults == null) {
					mAffiliateDriverFraudDefaults = CabRideEngineDapper.AffiliateDriverFraudDefaults.GetFraudDefaults(this.AffiliateDriverID);
				}
				return mAffiliateDriverFraudDefaults;
			}
		}

		private Vehicles mVehicle;
		public Vehicles Vehicles {
			get {
				if (mVehicle == null || mVehicle.IsNullEntity) {
					using (var conn = SqlHelper.OpenSqlConnection()) {
						mVehicle = Vehicles.GetVehicle(conn, this.VehicleID);
					}
				}
				return mVehicle;
			}
		}

		private AffiliateDriverLicenses mAffiliateDriverLicenses;
		public AffiliateDriverLicenses AffiliateDriverLicenses {
			get {
				if (mAffiliateDriverLicenses == null || mAffiliateDriverLicenses.IsNullEntity) {
					using (var conn = SqlHelper.OpenSqlConnection()) {
						mAffiliateDriverLicenses = AffiliateDriverLicenses.GetLicense(conn, this.AffiliateDriverID);
					}
				}
				return mAffiliateDriverLicenses;
			}
		}
		#endregion


		#region Custom Properties

		[Editable(false)]
		public bool ForceDejavooRedeemerStopPayment { get; set; }

		[Editable(false)]
		public int GetMaxMonthlyAmount {
			get {
				int maxAmount = this.AffiliateDriverFraudDefaults.MaxMonthlyAmount;

				if (maxAmount < 1) {
					maxAmount = this.Affiliate.AffiliateDriverDefaults.MaxMonthlyAmount;
				}
				if (maxAmount < 1) {
					maxAmount = SystemDefaults.GetDefaults().DriverMaxMonthlyAmount;
				}
				//if no max defined set to a high number that probably will never occur
				if (maxAmount == 0) {
					maxAmount = 1000000;
				}
				return maxAmount;
			}
		}

		[Editable(false)]
		public int GetMaxDailyAmount {
			get {
				int maxAmount = this.AffiliateDriverFraudDefaults.MaxDailyAmount;

				if (maxAmount < 1) {
					maxAmount = this.Affiliate.AffiliateDriverDefaults.MaxDailyAmount;
				}
				if (maxAmount < 1) {
					maxAmount = SystemDefaults.GetDefaults().DriverMaxDailyAmount;
				}
				//if no max defined set to a high number that probably will never occur
				if (maxAmount == 0) {
					maxAmount = 100000;
				}
				return maxAmount;
			}
		}

		[Editable(false)]
		public int GetMaxWeeklyAmount {
			get {
				int maxAmount = this.AffiliateDriverFraudDefaults.MaxWeeklyAmount;

				if (maxAmount < 1) {
					maxAmount = this.Affiliate.AffiliateDriverDefaults.MaxWeeklyAmount;
				}
				if (maxAmount < 1) {
					maxAmount = SystemDefaults.GetDefaults().DriverMaxWeeklyAmount;
				}
				//if no max defined set to a high number that probably will never occur
				if (maxAmount == 0) {
					maxAmount = 100000;
				}
				return maxAmount;
			}
		}

		[Editable(false)]
		public decimal GetChargeMaxAmount {
			get {
				decimal nAmount = this.AffiliateDriverFraudDefaults.ChargeMaxAmount;

				if (nAmount < 1) {
					nAmount = this.Affiliate.AffiliateDriverDefaults.ChargeMaxAmount;
				}

				if (nAmount < 1) {
					nAmount = SystemDefaults.GetDefaults().DriverChargeMaxAmount;
				}
				//if no max defined set to a high number that probably will never occur
				if (nAmount == 0) {
					nAmount = 100000;
				}
				return nAmount;
			}
		}

		[Editable(false)]
		public int GetCardChargedMaxNumber {
			get {
				int maxNumber = this.AffiliateDriverFraudDefaults.CardChargedMaxNumber;

				if (maxNumber < 1) {
					maxNumber = this.Affiliate.AffiliateDriverDefaults.CardChargedMaxNumber;
				}

				if (maxNumber < 1) {
					maxNumber = SystemDefaults.GetDefaults().DriverCardChargedMaxNumber;
				}
				//if no max defined set to a high number that probably will never occur
				if (maxNumber == 0) {
					maxNumber = 100000;
				}
				return maxNumber;
			}
		}

		[Editable(false)]
		public int GetMaxTrxPerDay {
			get {
				int maxTrx = this.AffiliateDriverFraudDefaults.MaxTransactions;

				if (maxTrx < 1) {
					maxTrx = this.Affiliate.AffiliateDriverDefaults.MaxTransactions;
				}
				if (maxTrx < 1) {
					maxTrx = SystemDefaults.GetDefaults().DriverMaxTransactions;
				}
				//if no max defined set to a high number that probably will never occur
				if (maxTrx == 0) {
					maxTrx = 100000;
				}
				return maxTrx;
			}
		}

		[Editable(false)]
		public int GetMinTransTime {
			get {
				return SystemDefaults.GetDefaults().DriverMinTransTime;
			}
		}

		[Editable(false)]
		public int GetMaxTransPerMonth {
			get {
				int max = this.AffiliateDriverFraudDefaults.MaxTransPerMonth;

				if (max < 1) {
					max = this.Affiliate.AffiliateDriverDefaults.MaxTransPerMonth;
				}

				if (max < 1) {
					max = SystemDefaults.GetDefaults().DriverMaxTransPerMonth;
				}

				//if no max defined set to a high number that probably will never occur
				if (max == 0) {
					max = 100000;
				}
				return max;
			}
		}

		[Editable(false)]
		public int GetMaxTransPerMonthPerCard {
			get {
				int max = this.AffiliateDriverFraudDefaults.MaxTransPerMonthPerCard;

				if (max < 1) {
					max = this.Affiliate.AffiliateDriverDefaults.MaxTransPerMonthPerCard;
				}
				if (max < 1) {
					max = SystemDefaults.GetDefaults().DriverMaxTransPerMonthPerCard;
				}

				//if no max defined set to a high number that probably will never occur
				if (max == 0) {
					max = 100000;
				}
				return max;
			}
		}

		[Editable(false)]
		public int GetMaxTransPerWeekPerCard {
			get {
				int max = this.AffiliateDriverFraudDefaults.MaxTransPerWeekPerCard;

				if (max < 1) {
					max = this.Affiliate.AffiliateDriverDefaults.MaxTransPerWeekPerCard;
				}
				if (max < 1) {
					max = SystemDefaults.GetDefaults().DriverMaxTransPerWeekPerCard;
				}
				//if no max defined set to a high number that probably will never occur
				if (max == 0) {
					max = 100000;
				}
				return max;
			}
		}

		[Editable(false)]
		public int GetMaxTransPerWeek {
			get {
				int max = this.AffiliateDriverFraudDefaults.MaxTransPerWeek;

				if (max < 1) {
					max = this.Affiliate.AffiliateDriverDefaults.MaxTransPerWeek;
				}
				if (max < 1) {
					max = SystemDefaults.GetDefaults().DriverMaxTransPerWeek;
				}
				//if no max defined set to a high number that probably will never occur
				if (max == 0) {
					max = 100000;
				}
				return max;
			}
		}

		[Editable(false)]
		public PaymentMethod PaymentMethodEnum {
			get {
				return (PaymentMethod)this.PaymentMethod;
			}
		}


		[Editable(false)]
		public string ComDataCardStatusDescription {
			get {
				return ComDataCardStatusDesc.GetStatus(ComDataCardStatus.GetValueOrDefault(0));
			}
		}

		[Editable(false)]
		public string FullName {
			get {
				return string.Format("{0} {1}", this.Name, this.LastName).Trim();
			}
		}

		[Editable(false)]
		public string CellFormatted {
			get {
				return Cell.FormattedPhoneNumber();
			}
		}

		//[Editable(false)]
		//public string LastName {
		//    get {
		//        using (var conn = SqlHelper.OpenSqlConnection()) {
		//            AffiliateDriverRef rec = AffiliateDriverRef.GetOrCreate(conn, this.AffiliateDriverID, "Name", "LastName");
		//            if (rec.ValueString.IsNullOrEmpty()) {
		//                if (Name.Contains(',')) {
		//                    rec.ValueString = Name.Split(',')[0];
		//                } else if (Name.Contains(' ')) {
		//                    rec.ValueString = Name.Split(' ').Last();
		//                } else {
		//                    rec.ValueString = "";
		//                }
		//                rec.Save();
		//            }
		//            return rec.ValueString;
		//        }
		//    }
		//}

		//[Editable(false)]
		//public string FirstName {
		//    get {
		//        using (var conn = SqlHelper.OpenSqlConnection()) {
		//            AffiliateDriverRef rec = AffiliateDriverRef.GetOrCreate(conn, this.AffiliateDriverID, "Name", "FirstName");
		//            if (rec.ValueString.IsNullOrEmpty()) {
		//                if (Name.Contains(',')) {
		//                    rec.ValueString = Name.Split(',').Last();
		//                } else if (Name.Contains(' ')) {
		//                    rec.ValueString = Name.Split(' ')[0];
		//                } else {
		//                    rec.ValueString = "";
		//                }
		//                rec.Save();
		//            }
		//            return rec.ValueString;
		//        }
		//    }
		//}

		#endregion

		public static AffiliateDrivers Create(long pAffiliateID) {
			Affiliate aff = Affiliate.GetAffiliate(pAffiliateID);
			return Create(aff);
		}

		public static AffiliateDrivers Create(Affiliate pAffiliate) {

			AffiliateDrivers oDriver = AffiliateDriversTable.Create();

			try {
				if (!pAffiliate.IsNullEntity) {
					oDriver.AffiliateID = pAffiliate.AffiliateID;
				}
				oDriver.Country = pAffiliate.Country;
				oDriver.State = pAffiliate.State;
				oDriver.LicenseState = pAffiliate.State;
				oDriver.PaidByRedeemer = false;
				oDriver.PaymentMethod = (int)Utils.PaymentMethod.Fleet;
				oDriver.SignUpDate = DateTime.Now;
				if (!pAffiliate.TimeZone.IsNullOrEmpty()) {
					TimeZoneConverter convert = new TimeZoneConverter();
					oDriver.SignUpDate = convert.ConvertTime(DateTime.Now, System.TimeZone.CurrentTimeZone.StandardName, pAffiliate.TimeZone);
				}
				oDriver.PayCardRealTimePay = false;
				oDriver.Active = true;

			} catch (Exception ex) {
				throw ex;
			}
			return oDriver;
		}


		public override int Save(string pModifiedBy) {
			if (RowState == DataRowState.Modified || RowState == DataRowState.Added) {
				ModifiedDate = DateTime.UtcNow;
				ModifiedBy = string.Format("{0} | {1}", pModifiedBy, CabRideDapperSettings.GetAppName());
				if (!ForceDejavooRedeemerStopPayment && Affiliate.AffiliateDriverDefaults.NewDriversExcludedFromRedeemerStopPayment.GetValueOrDefault(false)) {
					RedeemerStopPayment = false;
				}
			}
			if (IsNewRecord) {
				this.AffiliateDriverID = NextID.GetNextKey(AffiliateDrivers.TableDef.TABLE_NAME);
			} else {
				RowVersion++;
			}

			return base.Save(pModifiedBy);

		}

		public override int Save() {
			return Save(CabRideDapperSettings.LoggedInUser);
		}

		public override string PIN {
			get {
				return base.PIN.DecryptIceKey();
			}
			set {
				if (RowState == DataRowState.Detached) {
					base.PIN = value;
				} else {
					base.PIN = value.EncryptIceKey();
				}
			}
		}





		// **************************************************************************************
		// Get a Driver by ID
		// **************************************************************************************
		public static AffiliateDrivers GetDriver(long pAffiliateDriverID) {
			string sql = "Select * FROM AffiliateDrivers with (nolock) where AffiliateDriverID = @AffiliateDriverID";
			DynamicParameters param = new DynamicParameters();
			param.Add("AffiliateDriverID", pAffiliateDriverID);
			AffiliateDrivers affDriver = SqlHelper.GetRecords<AffiliateDrivers>(sql, param).DefaultIfEmpty(AffiliateDrivers.GetNullEntity()).FirstOrDefault();

			return affDriver;
		}

		public static AffiliateDrivers GetDriver(SqlConnection pConn, long pAffiliateDriverID) {
			//string sql = "Select * FROM AffiliateDrivers with (nolock) where AffiliateDriverID = @AffiliateDriverID";
			//DynamicParameters param = new DynamicParameters();
			//param.Add("AffiliateDriverID", pAffiliateDriverID);
			return pConn.Get<AffiliateDrivers>(pAffiliateDriverID);
		}

		public void CallDriver(string pURLofMessage) {

			SystemDefaults oDefaults = SystemDefaults.GetDefaults();

			//using (var conn = Utils.SqlHelper.OpenSqlConnection()) {
			string acctSid = oDefaults.TwilioAccountSID;
			string acctToken = oDefaults.TwilioAccountToken;
			string apiVersion = oDefaults.TwilioAPIVersion;

			string callerID = oDefaults.TwilioCallerID;

			TwilioRest.Account account = new TwilioRest.Account(acctSid, acctToken);

			Hashtable h = new Hashtable();
			h.Add("Caller", callerID);
			h.Add("Called", this.Cell);

			h.Add("Url", pURLofMessage);
			h.Add("Method", "GET");
			string msg = account.request(String.Format("/{0}/Accounts/{1}/Calls", apiVersion, acctSid), "POST", h);
			//Console.WriteLine(msg);
		}

		// *******************************************************************
		// Call using a specific Twilio number
		// *******************************************************************
		public void CallDriver(string pURLofMessage, string pGroup, string pReferenceKey) {
			SystemDefaults oDefaults = SystemDefaults.GetDefaults();
			SystemDefaultsDict oDefDict = SystemDefaultsDict.GetByKey(pGroup, pReferenceKey, false);


			string acctSid = oDefaults.TwilioAccountSID;
			string acctToken = oDefaults.TwilioAccountToken;
			string apiVersion = oDefaults.TwilioAPIVersion;

			string callerID = oDefDict.ValueString;

			TwilioRest.Account account = new TwilioRest.Account(acctSid, acctToken);

			Hashtable h = new Hashtable();
			h.Add("Caller", callerID);
			h.Add("Called", this.Cell);

			h.Add("Url", pURLofMessage);
			h.Add("Method", "GET");
			//h.Add("IfMachine","Hangup");
			string msg = account.request(String.Format("/{0}/Accounts/{1}/Calls", apiVersion, acctSid), "POST", h);
			//Console.WriteLine(msg);
		}

		//private static SystemDefaultsDict GetRecord(string sql, DynamicParameters param) {
		//	using (var conn = SqlHelper.OpenSqlConnection()) {
		//		return conn.Query<SystemDefaultsDict>(sql, param).SingleOrDefault();
		//	}
		//}


		public static AffiliateDrivers GetDriverByCallInCell(SqlConnection pPM, string pCellPhone) {
			if (pCellPhone.IsNullOrEmpty()) {
				return AffiliateDrivers.GetNullEntity();
			}
			AffiliateDrivers rec = GetData(pPM, "", "", "", pCellPhone, false, "")[0];
			rec.RowState = DataRowState.Unchanged;
			return rec;
		}


		public static AffiliateDrivers GetActiveDriverByCallInCell(SqlConnection pPM, string pCellPhone) {
			if (pCellPhone.IsNullOrEmpty()) {
				return AffiliateDrivers.GetNullEntity();
			}
			return GetData(pPM, "", "", "", pCellPhone, true, "")[0];
		}


		public static AffiliateDrivers GetDriverByCell(SqlConnection pConn, long pAffiliateID, string pCell) {
			string sql = @"SELECT  *
							FROM    dbo.AffiliateDrivers
							WHERE   ( Cell = @Cell
									  OR CallInCell = @Cell
									)
									AND AffiliateID = @AffiliateID";

			return pConn.Get<AffiliateDrivers>(sql, new { Cell = pCell, AffiliateID = pAffiliateID });
		}

		public static long GetPaymentRecordCountForDriver(SqlConnection pConn, long pAffiliateDriverID) {
			if (pAffiliateDriverID < 1) {
				return 0;
			}
			string sql = @"SELECT COUNT(*) Cnt FROM DriverPayments
                            WHERE AffiliateDriverID = @AffiliateDriverID";
			List<int> rec = pConn.Query<int>(sql, new { AffiliateDriverID = pAffiliateDriverID }).ToList();
			if (rec.Count == 0) {
				return 0;
			}
			return rec[0];
		}


		public static List<AffiliateDrivers> GetData(SqlConnection pConn, string pSql, DynamicParameters pParam, CommandType? pCommandType = null) {
			return pConn.Query<AffiliateDrivers>(pSql, pParam, commandType: pCommandType).ToList();
		}

		private static List<AffiliateDrivers> GetData(SqlConnection pConn, string pAffiliateDriverID
														, string pAffiliateID
														, string pName
														, string pCell
														, bool pActive = false
														, string pLicNo = "") {

			DynamicParameters param = new DynamicParameters();
			if (!pAffiliateDriverID.IsNullOrEmpty()) {
				param.Add("AffiliateDriverID", pAffiliateDriverID);
			}
			if (!pAffiliateID.IsNullOrEmpty()) {
				param.Add("AffiliateID", pAffiliateID);
			}
			if (!pName.IsNullOrEmpty()) {
				param.Add("Name", pName);
			}
			if (!pCell.IsNullOrEmpty()) {
				param.Add("Cell", pCell);
			}
			if (!pLicNo.IsNullOrEmpty()) {
				param.Add("LicenseNo", pLicNo);
			}
			if (pActive) {
				param.Add("Active", 1);
			}

			List<AffiliateDrivers> oData = pConn.Query<AffiliateDrivers>("usp_AffiliateDriverGet", param, commandType: CommandType.StoredProcedure).ToList();
			if (oData.Count == 0) {
				oData.Add(AffiliateDrivers.GetNullEntity());
			}
			return oData;
		}



		public static List<AffiliateDrivers> GetDrivers(long pAffiliateID) {
			string sql = "SELECT * FROM AffiliateDrivers WHERE AffiliateID = @AffiliateID ";
			using (var conn = SqlHelper.OpenSqlConnection()) {
				return conn.Query<AffiliateDrivers>(sql, new { AffiliateID = pAffiliateID }).ToList();
			}
		}


		public static List<AffiliateDrivers> GetDriversForDropDown(long pAffiliateID, bool pAddNullRecord = false, bool pActiveDriversOnly = true) {
			using (var conn = SqlHelper.OpenSqlConnection()) {
				return GetDriversForDropDown(conn, pAffiliateID, pAddNullRecord, pActiveDriversOnly);
			}
		}

		public static List<AffiliateDrivers> GetDriversForDropDown(SqlConnection pConn, long pAffiliateID, bool pAddNullRecord = false, bool pActiveDriversOnly = true) {
			string sql = string.Format(@"SELECT  AffiliateDriverID
								                , Name
                                                , Cell
							            FROM    dbo.AffiliateDrivers
							            WHERE   AffiliateID = @AffiliateID {0}
							            ORDER BY Name", pActiveDriversOnly ? "AND Active = 1" : "");


			List<AffiliateDrivers> data = pConn.Query<AffiliateDrivers>(sql, new { AffiliateID = pAffiliateID }).ToList();
			if (pAddNullRecord) {
				data.Insert(0, AffiliateDrivers.GetNullEntity());
			}
			return data;
		}


		public string SendDriverMsg(string pMsg, bool pIfEmailAddressReturnAddressOnly) {
			return SendDriverMsg(pMsg, pIfEmailAddressReturnAddressOnly, false);
		}

		public string SendDriverMsg(string pMsg, bool pIfEmailAddressReturnAddressOnly, bool pSendAsSMS) {
			using (var conn = SqlHelper.OpenSqlConnection()) {
				SystemDefaults oDef = SystemDefaults.GetDefaults(conn);

				string carrier = "";
				PagerTypes oPager = PagerTypes.GetNullEntity();
				if (this.CellProviderID.HasValue) {
					oPager = PagerTypes.GetPagerType(conn, this.CellProviderID.Value);
					carrier = "" + oPager.URL;
				}

				var doNotText = AffiliateRef.GetCreate(conn, AffiliateID, "Affiliate", "DoNotTextDrivers", false.ToString());
				if (Convert.ToBoolean(doNotText.ValueString) || ("" + oPager.PagerType).Equals("Do Not Text", StringComparison.CurrentCultureIgnoreCase)) {
					return "";
				}

				string cell = this.Cell;
				if (cell.Length < 10) {
					cell = this.CallInCell;
				}

				TaxiPassCommon.Mail.EZTexting ezText = new TaxiPassCommon.Mail.EZTexting(oDef.EZTextingUserName, oDef.EZTextingPassword);
				if (carrier.IsNullOrEmpty()) {
					carrier = ezText.GetCarrier(cell);
					if (carrier.IsNullOrEmpty()) {
						oPager = PagerTypes.GetPagerType(conn, "UNKNOWN");
					} else {
						oPager = PagerTypes.GetPagerType(conn, carrier);
						if (oPager.IsNullEntity) {
							oPager = PagerTypes.Create();
							oPager.PagerType = carrier;
							try {
								oPager.Save(CabRideDapperSettings.LoggedInUser);
							} catch {
							}
						}
					}
					if (!this.IsNullEntity && !oPager.IsNullEntity) {
						if (this.CellProviderID != oPager.PagerID) {
							this.CellProviderID = oPager.PagerID;
							try {
								this.Save(CabRideDapperSettings.LoggedInUser);
							} catch (Exception) {
							}
						}
					}
					carrier = "" + oPager.URL;
				}
				if (carrier.IsNullOrEmpty() || carrier.Contains("taxipass.com") || pSendAsSMS) {
					bool bUseEzTexting = true;
					if (oPager.PagerType.Equals("TxtDrop", StringComparison.CurrentCultureIgnoreCase)) {
						TaxiPassCommon.Mail.TxtDropSMS sms = new TaxiPassCommon.Mail.TxtDropSMS("email@taxipass.com");
						string response = sms.Send(cell, pMsg);
						if (!response.ToLower().Contains("error")) {
							bUseEzTexting = false;
						}
					} else {
						string acctSid = oDef.TwilioAccountSID;
						string acctToken = oDef.TwilioAccountToken;
						string apiVersion = oDef.TwilioAPIVersion;
						string smsNumber = oDef.TwilioSMSNumber;

						TaxiPassCommon.Mail.TwilioTexting twilio = new TaxiPassCommon.Mail.TwilioTexting(acctSid, acctToken, apiVersion, smsNumber);
						var response = twilio.TwilioTextMessage(pMsg, cell);
						if (response.RestException != null) {
							var cred = SystemDefaultsDict.GetEmailCreds(EMailReportFrom.Report);
							TaxiPassCommon.Mail.EMailMessage oMail = new TaxiPassCommon.Mail.EMailMessage(cred.From, "Affiliate Driver -> Twilio Texting Error", string.Format("Error: {0}<br>Message: {1}", response.RestException.Message, pMsg), cred.Password);
							oMail.UseWebServiceToSendMail = Properties.Settings.Default.EMailViaWebService;
							oMail.SendMail(Properties.Settings.Default.EMailErrorsTo, false);
						} else {
							bUseEzTexting = false;
						}
					}

					if (bUseEzTexting) {
						ezText.Send(cell, pMsg);
					}
				} else {
					//string sFrom = Properties.Settings.Default.EMailFrom;
					if (!carrier.StartsWith("@")) {
						carrier = "@" + carrier;
					}
					string sSendToSMS = cell + carrier;
					if (!pIfEmailAddressReturnAddressOnly) {
						var cred = SystemDefaultsDict.GetEmailCreds(EMailReportFrom.Receipt);
						TaxiPassCommon.Mail.EMailMessage oMail = new TaxiPassCommon.Mail.EMailMessage(cred.From, "", pMsg, cred.Password);
						oMail.UseWebServiceToSendMail = Properties.Settings.Default.EMailViaWebService;
						oMail.SendMail(sSendToSMS, false);
					}
					return sSendToSMS;
				}
			}
			return "";
		}


		public PayInfo GetPayInfo() {
			if (this.BankAccountNumber.IsNullOrEmpty()) {
				return null;
			}
			PayInfo oPay = new PayInfo();
			oPay.NachaFile = this.Affiliate.SendNacha;
			oPay.BankAccountHolder = this.BankAccountHolder;
			oPay.BankAccountNumber = this.BankAccountNumber;
			oPay.BankAccountPersonal = this.BankAccountPersonal;
			oPay.BankAddress = this.BankAddress;
			oPay.BankCity = this.BankCity;
			oPay.BankCode = this.BankCode;
			oPay.BankState = this.BankState;
			oPay.BankSuite = this.BankSuite;
			oPay.BankZIPCode = this.BankZIPCode;
			if (oPay.MyMerchant == null) {
				oPay.MyMerchant = new Merchant();
			}
			oPay.MyMerchant.MerchantNo = this.MerchantInfo.MerchantNo;
			oPay.PaidTo = "Driver: " + this.Name;
			return oPay;
		}



		// ****************************************************
		// Get a driver by cell 
		// ****************************************************
		public static List<AffiliateDrivers> GetDriversByCellPhone(SqlConnection pConn, string pCellPhone) {
			if (pCellPhone.IsNullOrEmpty()) {
				return new List<AffiliateDrivers>();
			}
			return GetData(pConn, "", "", "", pCellPhone);
		}

		public static AffiliateDrivers GetDriverFromTransNo(SqlConnection pConn, string pTransNo) {
			string sql = @"SELECT  AffiliateDrivers.*
							FROM    dbo.AffiliateDrivers
							LEFT JOIN driverpayments ON DriverPayments.AffiliateDriverID = AffiliateDrivers.AffiliateDriverID
							WHERE   TransNo = @TransNo";

			return pConn.Get<AffiliateDrivers>(sql, new { TransNo = pTransNo });
		}


		public static AffiliateDrivers GetDriverByPayCard(SqlConnection pConn, string pPayCard, bool pActive = false) {
			if (pPayCard.IsNullOrEmpty()) {
				return AffiliateDrivers.GetNullEntity();
			}

			string sCard = CustomerCardNumberDisplay(pPayCard);
			string sCard1 = CustomerCardNumberDisplay(pPayCard, 6);

			string sql = @"SELECT * FROM AffiliateDrivers 
							WHERE (TransCardCardNumber = @Card1 OR TransCardCardNumber = @Card2) ";
			if (pActive) {
				sql += "AND Active = 1";
			}

			List<AffiliateDrivers> list = pConn.Query<AffiliateDrivers>(sql, new { Card1 = sCard, Card2 = sCard1 }).ToList();
			foreach (AffiliateDrivers rec in list) {
				if (rec.TransCardNumber.DecryptIceKey().Equals(pPayCard)) {
					return rec;
				}
			}

			return AffiliateDrivers.GetNullEntity();
		}

		public static AffiliateDrivers GetDriverByTransCardAdminNo(SqlConnection pConn, string pTransCardAdminNo) {
			if (pTransCardAdminNo.IsNullOrEmpty()) {
				return AffiliateDrivers.GetNullEntity();
			}

			string sql = @"SELECT * FROM dbo.AffiliateDrivers
                            WHERE TransCardAdminNo = @TransCardAdminNo";

			return pConn.Get<AffiliateDrivers>(sql, new { TransCardAdminNo = pTransCardAdminNo });
		}


		public Twilio.SMSMessage SendMsgViaTwilio(SqlConnection pConn, string pMsg, string pTwilioNumber = "") {
			if (this.Affiliate.DoNotTextDrivers) {
				return new Twilio.SMSMessage();
			}

			SystemDefaults oDef = SystemDefaults.GetDefaults(pConn);

			string cell = this.Cell;
			string acctSid = oDef.TwilioAccountSID;
			string acctToken = oDef.TwilioAccountToken;
			string apiVersion = oDef.TwilioAPIVersion;

			if (pTwilioNumber.IsNullOrEmpty()) {
				pTwilioNumber = oDef.TwilioSMSNumber;
			}

			TaxiPassCommon.Mail.TwilioTexting twilio = new TaxiPassCommon.Mail.TwilioTexting(acctSid, acctToken, apiVersion, pTwilioNumber);
			return twilio.TwilioTextMessage(pMsg, cell);
		}


		public void SetTransCard(string pCardNumber) {
			TransCardCardNumber = CustomerCardNumberDisplay(pCardNumber, 6);
			TransCardNumber = pCardNumber.EncryptIceKey();
		}

		private static string CustomerCardNumberDisplay(string pCardNo, int pKeepLeftLen = 1) {
			if (pCardNo.Length > 10) {
				string stars = new string('*', 11 - pKeepLeftLen);
				return pCardNo.Left(pKeepLeftLen) + stars + pCardNo.Right(5);
			}
			return pCardNo;
		}


		// *************************************************************************************************************
		// Given an Affiliate and Driver's license no, return the first found
		// *************************************************************************************************************
		public static AffiliateDrivers GetDriverByLicenseNo(SqlConnection pConn, long pAffiliateID, string pLicNo) {
			return GetData(pConn, "", "", "", "", false, pLicNo)[0];

		}

		public static AffiliateDrivers GetDriverByLicenseNo(SqlConnection pConn, string pLicenseNo, string pState, bool pSkipDriverLicenseValidationCheck = false) {
			string sql = @"SELECT  *
							FROM    dbo.AffiliateDrivers
							WHERE   LicenseNo = @LicenseNo
									AND LicenseState = @LicenseState
									AND Active = 1
									AND LEN(Cell) = 10";

			AffiliateDrivers driver = pConn.Get<AffiliateDrivers>(sql, new { LicenseNo = pLicenseNo, LicenseState = pState });
			bool skip = true;
			if (driver.IsNullEntity && !skip) {
				sql = @"SELECT TOP 1
								AffiliateDrivers.AffiliateDriverID
							  , AffiliateDrivers.AffiliateID
							  , ISNULL(NULLIF(AffiliateDrivers.Name, 'Cell'), DriverPaymentValidation.Name) Name
							  , AffiliateDrivers.Street
							  , AffiliateDrivers.Suite
							  , AffiliateDrivers.City
							  , AffiliateDrivers.State
							  , AffiliateDrivers.Country
							  , AffiliateDrivers.ZIPCode
							  , AffiliateDrivers.Cell
							  , AffiliateDrivers.PIN
							  , AffiliateDrivers.SSN
							  , ISNULL(NULLIF(AffiliateDrivers.LicenseNo, ''), DriverPaymentValidation.LicenseNo) LicenseNo
							  , ISNULL(NULLIF(AffiliateDrivers.LicenseState, ''), DriverPaymentValidation.State) LicenseState
							  , AffiliateDrivers.Active
							  , AffiliateDrivers.FraudSuspected
							  , AffiliateDrivers.BankAccountHolder
							  , AffiliateDrivers.BankName
							  , AffiliateDrivers.BankAccountNumber
							  , AffiliateDrivers.BankCode
							  , AffiliateDrivers.BankAddress
							  , AffiliateDrivers.BankSuite
							  , AffiliateDrivers.BankCity
							  , AffiliateDrivers.BankState
							  , AffiliateDrivers.BankCountry
							  , AffiliateDrivers.BankZIPCode
							  , AffiliateDrivers.BankAccountPersonal
							  , AffiliateDrivers.MerchantID
							  , AffiliateDrivers.Merchant
							  , AffiliateDrivers.PaidByRedeemer
							  , AffiliateDrivers.EMail
							  , AffiliateDrivers.CallInCell
							  , AffiliateDrivers.DriverStatusID
							  , AffiliateDrivers.Notes
							  , AffiliateDrivers.TaxiTronicDriverID
							  , AffiliateDrivers.RedeemerStopPayment
							  , AffiliateDrivers.StopPaymentDate
							  , AffiliateDrivers.CellProviderID
							  , AffiliateDrivers.TAPermit
							  , AffiliateDrivers.Language
							  , AffiliateDrivers.VehicleID
							  , AffiliateDrivers.PaymentMethod
							  , AffiliateDrivers.TransCardNumber
							  , AffiliateDrivers.TransCardCardNumber
							  , AffiliateDrivers.TransCardExpiration
							  , AffiliateDrivers.TransCardID
							  , AffiliateDrivers.DateOfBirth
							  , AffiliateDrivers.AutoPaySwipe
							  , AffiliateDrivers.AutoPayManual
							  , AffiliateDrivers.TransCardActive
							  , AffiliateDrivers.SignUpDate
							  , AffiliateDrivers.SelfRedemption
							  , AffiliateDrivers.TransCardAdminNo
							  , AffiliateDrivers.DriverFee
							  , AffiliateDrivers.TransCardAsDriverIDOnly
							  , AffiliateDrivers.PayCardRealTimePay
							  , AffiliateDrivers.RowVersion
							  , AffiliateDrivers.ModifiedBy
							  , AffiliateDrivers.ModifiedDate
							  , AffiliateDrivers.PayBeforeMatch
							  , AffiliateDrivers.USCitizen
							  , AffiliateDrivers.DriverInitiatedPayCardPayment
							  , AffiliateDrivers.PayCardSystemID
							  , AffiliateDrivers.PayCardAssignedBy
							  , AffiliateDrivers.TransCardDefaultID
							  , AffiliateDrivers.FleetName
							  , AffiliateDrivers.FleetID
							  , AffiliateDrivers.DriverStatusUpdated
							  , AffiliateDrivers.DriverStatusUpdatedBy
							  , AffiliateDrivers.TAPermitCity
							  , AffiliateDrivers.ComDataCardStatus
							  , AffiliateDrivers.RequireNewPayCard
						FROM    AffiliateDrivers
						LEFT JOIN dbo.DriverPayments ON DriverPayments.AffiliateDriverID = AffiliateDrivers.AffiliateDriverID
						LEFT JOIN dbo.DriverPaymentValidation ON DriverPaymentValidation.DriverPaymentID = DriverPayments.DriverPaymentID
						WHERE   DriverPaymentValidation.State = @LicenseState
								AND DriverPaymentValidation.LicenseNo = @LicenseNo
						ORDER BY ChargeDate DESC";

				driver = pConn.Get<AffiliateDrivers>(sql, new { LicenseNo = pLicenseNo, LicenseState = pState });
			}
			return driver;
		}

		public static AffiliateDrivers GetRetailPayment(SqlConnection conn, long pAffiliateID) {
			string sql = @"SELECT  *
							FROM    dbo.AffiliateDrivers
							WHERE   Name = 'Retail Payment'
									AND AffiliateID = @AffiliateID";

			Affiliate aff = Affiliate.GetAffiliate(conn, pAffiliateID);
			AffiliateDrivers driver = conn.Get<AffiliateDrivers>(sql, new { AffiliateID = pAffiliateID });
			if (driver.IsNullEntity) {
				driver = AffiliateDrivers.Create(aff);
				driver.Name = "Retail Payment";
				driver.Cell = "999PAYMENT" + pAffiliateID;
				driver.EMail = string.Format("payment@{0}.com", aff.Name.Replace(" ", "_").Replace(",", ".").Replace("'", ""));
				driver.Active = true;
				driver.Save("bill-payment.org");
			}

			// driver should never be inactive
			if (!driver.Active) {
				driver.Active = true;
				if (CabRideDapperSettings.LoggedInUser.IsNullOrEmpty()) {
					CabRideDapperSettings.LoggedInUser = "bill-payment.org";
				}

				driver.Save();
			}

			return driver;
		}

		public static AffiliateDrivers GetRetailIVRPayment(SqlConnection conn, long pAffiliateID) {
			string sql = @"SELECT  *
							FROM    dbo.AffiliateDrivers
							WHERE   Name = 'Retail IVR Payment'
									AND AffiliateID = @AffiliateID";

			Affiliate aff = Affiliate.GetAffiliate(conn, pAffiliateID);
			AffiliateDrivers driver = conn.Get<AffiliateDrivers>(sql, new { AffiliateID = pAffiliateID });
			if (driver.IsNullEntity) {
				driver = AffiliateDrivers.Create(aff);
				driver.Name = "Retail IVR Payment";
				driver.Cell = "999IVRPYMT" + pAffiliateID;
				driver.EMail = string.Format("ivrpayment@{0}.com", aff.Name.Replace(" ", "_").Replace(",", ".").Replace("'", ""));
				driver.Active = true;
				driver.Save("IVRPayments");
			}

			// driver should never be inactive
			if (!driver.Active) {
				driver.Active = true;
				if (CabRideDapperSettings.LoggedInUser.IsNullOrEmpty()) {
					CabRideDapperSettings.LoggedInUser = "bill-payment.org";
				}

				driver.Save();
			}

			return driver;
		}


		public static AffiliateDrivers GetDriverByEMail(SqlConnection pConn, string pEmail) {
			string sql = @"SELECT * FROM dbo.AffiliateDrivers
						   WHERE EMail = @Email";

			return pConn.Get<AffiliateDrivers>(sql, new { Email = pEmail });
		}

		public void SetTransCardNumberClearAdminNoIfEmpty(string value) {
			bool changed = (this.TransCardNumber != value);

			if (value.IsNullOrEmpty()) {
				TransCardAdminNo = "";
				base.TransCardNumber = null;
				base.TransCardCardNumber = null;
			} else {
				base.TransCardNumber = value.EncryptIceKey();
				base.TransCardCardNumber = CustomerCardNumberDisplay(value, 6);
			}
		}

		public void CallDriverWithMMSMessage(string pURLofMessage) {
			SystemDefaults oDefaults = SystemDefaults.GetDefaults();

			string acctSid = oDefaults.TwilioAccountSID;
			string acctToken = oDefaults.TwilioAccountToken;
			string apiVersion = oDefaults.TwilioAPIVersion;

			string callerID = oDefaults.TwilioCallerID;
			if (this.SelfRedemption || this.Affiliate.AffiliateDriverDefaults.SelfRedemption) {
				callerID = oDefaults.TwilioMMSNumber;
			}

			TwilioRest.Account account = new TwilioRest.Account(acctSid, acctToken);

			Hashtable h = new Hashtable();
			h.Add("Caller", callerID);
			h.Add("Called", this.Cell);

			h.Add("Url", pURLofMessage);
			h.Add("Method", "GET");
			string msg = account.request(String.Format("/{0}/Accounts/{1}/Calls", apiVersion, acctSid), "POST", h);
			//Console.WriteLine(msg);

		}

		// ********************************************************************************
		// Get a Fleet's active drivers
		// ********************************************************************************
		public static List<AffiliateDrivers> GetActiveDrivers(Affiliate pAffiliate) {
			return GetData("", pAffiliate.AffiliateID.ToString(), "", "", true, "");
		}


		private static List<AffiliateDrivers> GetData(string pAffiliateDriverID, string pAffiliateID, string pName, string pCell, bool pActive = false, string pLicNo = "") {
			DynamicParameters parms = new DynamicParameters();

			if (!pAffiliateDriverID.IsNullOrEmpty()) {
				parms.Add("AffiliateDriverID", pAffiliateDriverID);
			}
			if (!pAffiliateID.IsNullOrEmpty()) {
				parms.Add("AffiliateID", pAffiliateID);
			}
			if (!pName.IsNullOrEmpty()) {
				parms.Add("Name", pName);
			}
			if (!pCell.IsNullOrEmpty()) {
				parms.Add("Cell", pCell);
			}
			if (!pLicNo.IsNullOrEmpty()) {
				parms.Add("LicenseNo", pLicNo);
			}
			if (pActive) {
				parms.Add("Active", true);
			}

			using (var conn = SqlHelper.OpenSqlConnection()) {

				//PassthruRdbQuery qry = new PassthruRdbQuery(typeof(AffiliateDrivers), sql.ToString());
				List<AffiliateDrivers> oData = conn.Query<AffiliateDrivers>("usp_AffiliateDriverGet", parms, commandType: CommandType.StoredProcedure).ToList();
				if (oData.Count == 0) {
					oData.Add(AffiliateDrivers.GetNullEntity());
				}

				return oData;
			}

		}

		public static AffiliateDrivers GetDriverByName(Affiliate pAffiliate, string pName) {
			List<AffiliateDrivers> oAffDrivers = GetData("", pAffiliate.AffiliateID.ToString(), pName, "");
			if (oAffDrivers.Count == 0) {
				return AffiliateDriversTable.GetNullEntity();
			} else {
				return oAffDrivers[0];
			}
		}

		public static AffiliateDrivers GetDriverByName(long pAffiliateID, string pName) {
			List<AffiliateDrivers> oAffDrivers = GetData("", pAffiliateID.ToString(), pName, "");
			if (oAffDrivers.Count == 0) {
				return AffiliateDriversTable.GetNullEntity();
			} else {
				return oAffDrivers[0];
			}
		}

		// ****************************************************
		// Get a drivers by AffiliateID and cell
		// ****************************************************
		public static AffiliateDrivers GetDriverByCellPhone(Affiliate pAffiliate, string pCellPhone) {
			using (var conn = SqlHelper.OpenSqlConnection()) {
				return GetDriverByCellPhone(conn, pAffiliate.AffiliateID, pCellPhone);
			}
		}

		// ****************************************************
		// Get a drivers by AffiliateID and cell
		// ****************************************************
		public static AffiliateDrivers GetDriverByCellPhone(SqlConnection pConn, long pAffiliateID, string pCellPhone) {
			if (pCellPhone.IsNullOrEmpty()) {
				return AffiliateDrivers.GetNullEntity();
			}

			List<AffiliateDrivers> oAffDrivers = GetData(pConn, "", pAffiliateID.ToString(), "", pCellPhone);
			if (oAffDrivers.Count == 0) {
				return AffiliateDrivers.GetNullEntity();
			} else {
				return oAffDrivers[0];
			}
		}

	}
}