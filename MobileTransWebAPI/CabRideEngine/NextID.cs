﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Util;
using IdeaBlade.Persistence.Rdb;

namespace CabRideEngine {
    [Serializable]
    public sealed class NextID : NextIDDataRow {

        #region Constructors -- DO NOT MODIFY
        // Do not create constructors for this class
        // Developers cannot create instances of this class with the "new" keyword
        // You should write a static Create method instead; see the "Suggested Customization" region
        // Please review the documentation to learn about entity creation and inheritance

        // This private constructor prevents accidental instance creation via 'new'
        private NextID() : this(null) { }

        // Typed DataSet constructor (needed by framework) - DO NOT REMOVE
        public NextID(DataRowBuilder pRowBuilder)
            : base(pRowBuilder) {
        }

        #endregion

        #region Suggested Customizations

        //    // Use this factory method template to create new instances of this class
        //    public static NextID Create(PersistenceManager pManager,
        //      ... your creation parameters here ...) { 
        //
        //      NextID aNextID = pManager.CreateEntity<NextID>();
        //
        //      // if this object type requires a unique id and you have implemented
        //      // the IIdGenerator interface implement the following line
        //      pManager.GenerateId(aNextID, // add id column here //);
        //
        //      // Add custom code here
        //
        //      aNextID.AddToManager();
        //      return aNextID;
        //    }

        //    // Implement this method if you want your object to sort in a specific order
        //    public override int CompareTo(Object pObject) {
        //    }

        //    // Implement this method to customize the null object version of this class
        //    protected override void UpdateNullEntity() {
        //    }

        #endregion

        // Add additional logic to your business object here...
        public static NextID Create(PersistenceManager pPM, string pTable) {
            NextID rec = null;
            try {
                rec = pPM.CreateEntity<NextID>();
                rec[NameEntityColumn.ColumnName] = pTable;
                rec.NextID = 0;
                rec.AddToManager();
            } catch (Exception ex) {
                throw ex;
            }
            return rec;
        }


        public static NextID Retrieve(PersistenceManager pPM, string pTable) {
            RdbQuery qry = new RdbQuery(typeof(NextID), NameEntityColumn, EntityQueryOp.EQ, pTable);
            NextID rec = pPM.GetEntity<NextID>(qry);
            if (rec.IsNullEntity) {
                rec = CabRideEngine.NextID.Create(pPM, pTable);
                rec.Save();
            }
            return rec;
        }
    }

}
