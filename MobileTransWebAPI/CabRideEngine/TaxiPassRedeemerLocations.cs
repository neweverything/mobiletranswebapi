﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

using CabRideEngine.Utils;
using TaxiPassCommon;

using Csla.Validation;

namespace CabRideEngine {
	/// <summary>
	/// The developer class representing the TaxiPassRedeemerLocations business object class.
	/// </summary>
	/// <remarks>
	/// <para>Place all application-specific business logic in this class.</para>
	/// <para>
	/// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
	/// and will not overwrite changes.
	/// </para>
	/// <para>
	/// This class inherits from the generated class, its companion in the pair of classes 
	/// defining a business object. Properties in the generated class may be overridden in 
	/// order to change the behavior or add business logic.
	/// </para>
	/// <para>
	/// This class is the final class in the inheritance chain for this business object. 
	/// It should not be inherited.
	/// </para>
	/// </remarks>
	[Serializable]
	public sealed partial class TaxiPassRedeemerLocations : TaxiPassRedeemerLocationsDataRow {

		#region Constructors -- DO NOT MODIFY

		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new';
		// it is also required if the class is visible from a web service.
		// DO NOT REMOVE **
		private TaxiPassRedeemerLocations() : this(null) { }

		/// <summary>
		/// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
		/// </summary>
		/// <param name="pRowBuilder"></param>
		public TaxiPassRedeemerLocations(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static TaxiPassRedeemerLocations Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      TaxiPassRedeemerLocations aTaxiPassRedeemerLocations = pManager.CreateEntity<TaxiPassRedeemerLocations>();
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aTaxiPassRedeemerLocations, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aTaxiPassRedeemerLocations.AddToManager();
		//      return aTaxiPassRedeemerLocations;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		// Add additional logic to your business object here...
		public static TaxiPassRedeemerLocations Create(TaxiPassRedeemerAccounts pTaxiPassRedeemerAccount) {
			TaxiPassRedeemerLocations rec = null;

			try {
				PersistenceManager oPM = pTaxiPassRedeemerAccount.PersistenceManager;

				rec = oPM.CreateEntity<TaxiPassRedeemerLocations>();
				
				// Using the IdeaBlade Id Generation technique
				oPM.GenerateId(rec, TaxiPassRedeemerLocations.TaxiPassRedeemerLocationIDEntityColumn);
				rec.TaxiPassRedeemerAccountID = pTaxiPassRedeemerAccount.TaxiPassRedeemerAccountID;
				rec.Country = pTaxiPassRedeemerAccount.Country;
				rec.State = pTaxiPassRedeemerAccount.State;
				rec.Active = true;

				rec.AddToManager();
			} catch (Exception ex) {
				throw ex;
			}
			return rec;
		}

		protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
			if (IsDeserializing) {
				return;
			}
			base.OnColumnChanging(pArgs);
			DoAudit(pArgs);
		}

		private void DoAudit(DataColumnChangeEventArgs pArgs) {
			if (!this.PersistenceManager.IsClient) {
				return;
			}
			if (this.RowState == DataRowState.Added) {
				return;
			}
			if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
				AuditRecord = TaxiPassRedeemerLocationsAudit.Create(this.PersistenceManager, this);
			}
		}

		[BindingBrowsable(false)]
		public override string EntityInstanceDisplayName {
			get {
				return this.Location;
			}
		}

		protected override void AddRules(RuleList pList) {
			PropertyRequiredRule.AddToList(pList, LocationEntityColumn.ColumnName);
			pList.Add(IsLocationUnique, LocationEntityColumn.ColumnName);
		}

		private bool IsLocationUnique(object pTarget, RuleArgs e) {
			TaxiPassRedeemerLocations oRecord = pTarget as TaxiPassRedeemerLocations;

			RdbQuery qry = new RdbQuery(typeof(TaxiPassRedeemerLocations), LocationEntityColumn, EntityQueryOp.EQ, oRecord.Location);
			qry.AddClause(TaxiPassRedeemerLocationIDEntityColumn, EntityQueryOp.NE, oRecord.TaxiPassRedeemerLocationID);
			qry.AddClause(TaxiPassRedeemerLocations.TaxiPassRedeemerAccountIDEntityColumn, EntityQueryOp.EQ, oRecord.TaxiPassRedeemerAccountID);
			EntityList<TaxiPassRedeemerLocations> oList = this.PersistenceManager.GetEntities<TaxiPassRedeemerLocations>(qry);
			if (oList.Count > 0) {
				e.Description = "This Location already exists: " + oRecord.Location;
				return false;
			}
			return true;
		}

		public string PhoneFormatted {
			get {
				return this.Phone.FormattedPhoneNumber();
			}
		}

		public static TaxiPassRedeemerLocations GetLocation(PersistenceManager pPM, long pTaxiPassRedeemerLocationID) {
			RdbQuery qry = new RdbQuery(typeof(TaxiPassRedeemerLocations), TaxiPassRedeemerLocationIDEntityColumn, EntityQueryOp.EQ, pTaxiPassRedeemerLocationID);
			return pPM.GetEntity<TaxiPassRedeemerLocations>(qry);
		}

		public static EntityList<TaxiPassRedeemerLocations> GetLocations(TaxiPassRedeemerAccounts pTaxiPassRedeemerAccount) {
			RdbQuery qry = new RdbQuery(typeof(TaxiPassRedeemerLocations), TaxiPassRedeemerAccountIDEntityColumn, EntityQueryOp.EQ, pTaxiPassRedeemerAccount.TaxiPassRedeemerAccountID);
			qry.AddOrderBy(TaxiPassRedeemerLocations.LocationEntityColumn);
			return pTaxiPassRedeemerAccount.PersistenceManager.GetEntities<TaxiPassRedeemerLocations>(qry);
		}

		public static EntityList<TaxiPassRedeemerLocations> GetActiveLocations(TaxiPassRedeemerAccounts pTaxiPassRedeemerAccount) {
			RdbQuery qry = new RdbQuery(typeof(TaxiPassRedeemerLocations), TaxiPassRedeemerAccountIDEntityColumn, EntityQueryOp.EQ, pTaxiPassRedeemerAccount.TaxiPassRedeemerAccountID);
			qry.AddClause(TaxiPassRedeemerLocations.ActiveEntityColumn, EntityQueryOp.EQ, true);
			qry.AddOrderBy(TaxiPassRedeemerLocations.LocationEntityColumn);
			return pTaxiPassRedeemerAccount.PersistenceManager.GetEntities<TaxiPassRedeemerLocations>(qry);
		}

		public static TaxiPassRedeemerLocations GetByStoreNo(TaxiPassRedeemerAccounts pTaxiPassRedeemerAccount, String pStoreNo) {
			RdbQuery qry = new RdbQuery(typeof(TaxiPassRedeemerLocations), TaxiPassRedeemerAccountIDEntityColumn, EntityQueryOp.EQ, pTaxiPassRedeemerAccount.TaxiPassRedeemerAccountID);
			qry.AddClause(TaxiPassRedeemerLocations.StoreNoEntityColumn, EntityQueryOp.EQ, pStoreNo);
			qry.AddClause(TaxiPassRedeemerLocations.ActiveEntityColumn, EntityQueryOp.EQ, true);
			return pTaxiPassRedeemerAccount.PersistenceManager.GetEntity<TaxiPassRedeemerLocations>(qry);
		}

		public string RedeemerAccountName {
			get {
				return this.TaxiPassRedeemerAccounts.CompanyName;
			}
		}
	}

	#region EntityPropertyDescriptors.TaxiPassRedeemerLocationsPropertyDescriptor
	////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
	////    For example: 
	////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
	//public static partial class EntityPropertyDescriptors {
	//	public partial class TaxiPassRedeemerLocationsPropertyDescriptor : BaseEntityPropertyDescriptor {
	//    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
	//    public AdaptedPropertyDescriptor Age {
	//      get { return this.Get("Age"); }
	//    }
	//  }
	//} 
	#endregion

}