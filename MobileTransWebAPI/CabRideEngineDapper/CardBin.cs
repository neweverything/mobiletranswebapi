﻿using Dapper;
using System;
using System.Data.SqlClient;
using TaxiPassCommon;
using TaxiPassCommon.Banking;

namespace CabRideEngineDapper {
	public partial class CardBin {

		public static CardBin GetCardBinInfo(SqlConnection pConn, string pCardNo) {
			string sql = @"SELECT * FROM CardBin
							WHERE Bin = @Bin";

			float bin = Convert.ToSingle(pCardNo.Left(6));

			CardBin cardBin = pConn.Get<CardBin>(sql, new { Bin = bin });
			if (cardBin.IsNullEntity) {
				try {
					string cardType = CardUtils.DetermineCardType(pCardNo);
					BinList info = BinList.GetCardInfo(pCardNo, cardType);
					if (info != null && info.bank != null) {
						cardBin = CardBin.Create();
						cardBin.Bin = bin;
						cardBin.CardBrand = info.brand;
						cardBin.Issuer = info.bank.name;
						cardBin.DebitCredit = ("" + info.type).ToUpper();
						cardBin.SubCardType = info.brand;
						try {
							cardBin.Country = info.country.name.ToUpper();
							cardBin.CountryCode = info.country.alpha2;
							cardBin.Country1 = info.country.alpha2;
							cardBin.Code = info.country.numeric.ToDouble();
						} catch {
						}
						try {
							cardBin.Geocode = info.bank.phone;
							cardBin.Website = info.bank.url;
						} catch {
						}

						if ((""+cardBin.CardBrand).ToLower().Contains("debit")) {
							cardBin.DebitCredit = "DEBIT";
						}

						cardBin.Save();
					}
				} catch (Exception) {
					cardBin = CardBin.GetNullEntity();
				}
			}
			return cardBin;
		}
	}
}
