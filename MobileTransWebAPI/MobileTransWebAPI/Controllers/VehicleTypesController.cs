﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using Dapper;
using CabRideEngineDapper.Utils;

namespace MobileTransWebAPI.Controllers {
	public class VehicleTypesController : ApiController {

		public List<string> Get() {
			List<string> list = new List<string>();

			using (var oConn = SqlHelper.OpenSqlConnection()) { 
				list = oConn.Query<string>("Select VehicleType FROM VehicleTypes ORDER BY VehicleType").ToList();
			}
			return list;
		}

	}
}
