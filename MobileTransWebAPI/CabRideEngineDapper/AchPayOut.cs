﻿using CabRideEngineDapper.Utils;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using TaxiPassCommon;

namespace CabRideEngineDapper {
    public partial class AchPayOut {

        public static List<AchPayOut> GetPayoutByDate(DateTime pDate) {
            DateTime endDate = pDate.Date.AddDays(1);

            string sql = @"SELECT  *
                            FROM    AchPayOut
                            WHERE   ACHDate >= @StartDate
                                    AND ACHDate <= @EndDate
                            ORDER BY BatchNo, Affiliate";

            using (var conn = SqlHelper.OpenSqlConnection()) {
                return conn.Query<AchPayOut>(sql, new { StartDate = pDate.Date, EndDate = endDate }).ToList();
            }
        }

        public static List<AchPayOut> GetPayoutByDate(DateTime pStartDate, DateTime pEndDate, string pAffiliate = null, string pBankAccountNo = null, string pName = null, decimal pAmount = 0) {
            string sql = @"SELECT  *
                            FROM    AchPayOut
                            WHERE   ACHDate >= @StartDate
                                    AND ACHDate <= @EndDate
                                    AND (@Affiliate IS NULL
                                         OR @Affiliate = ''
                                         OR Affiliate = @Affiliate
                                        )
                                    AND (@BankAccountNo IS NULL
                                         OR @BankAccountNo = ''
                                         OR BankAccountNo LIKE '%' + @BankAccountNo + '%'
                                        )
                                    AND (@Name IS NULL
                                         OR @Name = ''
                                         OR Name LIKE '%' + @Name + '%'
                                         OR PaidTo LIKE '%' + @Name + '%'
                                         OR BankAccountHolder LIKE '%' + @Name + '%'
                                        )
                                    AND (@Amount IS NULL
                                         OR @Amount = 0
                                         OR Amount = @Amount
                                        )
                            ORDER BY BatchNo
                                  , Affiliate";

            using (var conn = SqlHelper.OpenSqlConnection()) {
                return conn.Query<AchPayOut>(sql, new { StartDate = pStartDate.Date, EndDate = pEndDate.Date, Affiliate = pAffiliate, BankAccountNo = pBankAccountNo, Name = pName, Amount = pAmount }).ToList();
            }
        }

        //[Editable(false)]
        //public string Debit {
        //    get {
        //        if (TransType.Equals("debit", StringComparison.CurrentCultureIgnoreCase)) {
        //            return Amount.ToString("C");
        //        }
        //        return "";
        //    }
        //}

        //[Editable(false)]
        //public string Credit {
        //    get {
        //        if (TransType.Equals("credit", StringComparison.CurrentCultureIgnoreCase)) {
        //            return Amount.ToString("C");
        //        }
        //        return "";
        //    }
        //}

        //[Editable(false)]
        //public string WireTransferAmountAsString {
        //    get {
        //        if (TransType.Equals("Wire Transfer", StringComparison.CurrentCultureIgnoreCase)) {
        //            return Amount.ToString("C");
        //        }
        //        return "";
        //    }
        //}


        [Editable(false)]
        public decimal BusinessDebitAmount {
            get {
                if (("" + BankAccountType).Equals("BUSINESS", StringComparison.CurrentCultureIgnoreCase) && ("" + TransType).Equals("debit", StringComparison.CurrentCultureIgnoreCase)) {
                    return Amount;
                }
                return 0;
            }
        }

        [Editable(false)]
        public decimal PersonalDebitAmount {
            get {
                if (("" + BankAccountType).Equals("PERSONAL", StringComparison.CurrentCultureIgnoreCase) && ("" + TransType).Equals("debit", StringComparison.CurrentCultureIgnoreCase)) {
                    return Amount;
                }
                return 0;
            }
        }

        [Editable(false)]
        public decimal PersonalCreditAmount {
            get {
                if (("" + BankAccountType).Equals("PERSONAL", StringComparison.CurrentCultureIgnoreCase) && ("" + TransType).Equals("credit", StringComparison.CurrentCultureIgnoreCase)) {
                    return Amount;
                }
                return 0;
            }
        }

        [Editable(false)]
        public decimal BusinessCreditAmount {
            get {
                if (("" + BankAccountType).Equals("BUSINESS", StringComparison.CurrentCultureIgnoreCase) && ("" + TransType).Equals("credit", StringComparison.CurrentCultureIgnoreCase)) {
                    return Amount;
                }
                return 0;
            }
        }

        [Editable(false)]
        public decimal WireTransferAmount {
            get {
                if (("" + TransType).Equals("Wire Transfer", StringComparison.CurrentCultureIgnoreCase)) {
                    return Amount;
                }
                return 0;
            }
        }

        [Editable(false)]
        public decimal TotalTransactions {
            get {
                return BusinessCreditAmount - BusinessDebitAmount + PersonalCreditAmount - PersonalDebitAmount + WireTransferAmount;
            }
        }

        [Editable(false)]
        public string CitiName {
            get {
                return ("" + Name).PadRight(15, ' ').Left(15).Trim().ToUpper();
            }
        }
    }
}
