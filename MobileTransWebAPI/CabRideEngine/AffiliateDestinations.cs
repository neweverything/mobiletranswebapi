﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

using Csla.Validation;

namespace CabRideEngine {
	[Serializable]
	public sealed class AffiliateDestinations : AffiliateDestinationsDataRow {

		#region Constructors -- DO NOT MODIFY
		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new'
		private AffiliateDestinations() : this(null) {
		}

		// Typed DataSet constructor (needed by framework) - DO NOT REMOVE
		public AffiliateDestinations(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static AffiliateDestinations Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      AffiliateDestinations aAffiliateDestinations = pManager.CreateEntity<AffiliateDestinations>();
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aAffiliateDestinations, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aAffiliateDestinations.AddToManager();
		//      return aAffiliateDestinations;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		// Add additional logic to your business object here...
		public static AffiliateDestinations Create(PersistenceManager pPM, long pAffiliateID, long pCoverageZoneID) {
			AffiliateDestinations oDest = null;

			try {
				oDest = (AffiliateDestinations)pPM.CreateEntity(typeof(AffiliateDestinations));

				// Using the IdeaBlade Id Generation technique
				//pPM.GenerateId(oDest, AffiliateDestinations.AffiliateDestZoneIDEntityColumn);

				oDest.AffiliateID = pAffiliateID;
				oDest.CoverageZoneID = pCoverageZoneID;
				oDest.AddToManager();

			} catch (Exception ex) {
				throw ex;
			}
			return oDest;
		}

		public override long CoverageZoneID {
			get {
				return base.CoverageZoneID;
			}
			set {

				RdbQuery oQry = new RdbQuery(typeof(CoverageZones), CoverageZones.CoverageZoneIDEntityColumn, EntityQueryOp.EQ, value);
				if (this.PersistenceManager.GetEntities<CoverageZones>(oQry).Count < 1) {
					throw new Exception("Coverage Zone: Invalid value");
				}

				oQry = new RdbQuery(typeof(AffiliateDestinations), AffiliateDestinations.CoverageZoneIDEntityColumn, EntityQueryOp.EQ, value);
				oQry.AddClause(AffiliateDestinations.AffiliateIDEntityColumn, EntityQueryOp.EQ, this.AffiliateID);
				oQry.AddClause(AffiliateDestinations.AffiliateDestZoneIDEntityColumn, EntityQueryOp.NE, this.AffiliateDestZoneID);
				EntityList<AffiliateDestinations> oDest = this.PersistenceManager.GetEntities<AffiliateDestinations>(oQry);
				if (oDest.Count > 0) {
					throw new Exception("Coverage Zone: value must be unique!");
				}

				base.CoverageZoneID = value;
				OnPropertyChanged("CoverageZoneID");

			}
		}

		protected override void AddRules(RuleList pList) {
			PropertyRequiredRule.AddToList(pList, AffiliateDestinations.CoverageZoneIDEntityColumn.ColumnName);

			base.AddRules(pList);
		}

		protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
			if (IsDeserializing)
				return;
			base.OnColumnChanging(pArgs);
			//DoAudit(pArgs);
		}

		private void DoAudit(DataColumnChangeEventArgs pArgs) {
			if (!this.PersistenceManager.IsClient) {
				return;
			}
            if (this.RowState == DataRowState.Added) {
                return;
            }
			if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
				AuditRecord = AffiliateDestinationAudit.Create(this.PersistenceManager, this);
			}
			//AuditRecord[pArgs.Column.ColumnName] = pArgs.ProposedValue;
		}
		
	}

}
