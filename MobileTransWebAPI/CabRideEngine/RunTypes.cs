﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

using Csla.Validation;

namespace CabRideEngine {
    [Serializable]
    public sealed class RunTypes : RunTypesDataRow {

        #region Constructors -- DO NOT MODIFY
        // Do not create constructors for this class
        // Developers cannot create instances of this class with the "new" keyword
        // You should write a static Create method instead; see the "Suggested Customization" region
        // Please review the documentation to learn about entity creation and inheritance

        // This private constructor prevents accidental instance creation via 'new'
        private RunTypes() : this(null) {
        }

        // Typed DataSet constructor (needed by framework) - DO NOT REMOVE
        public RunTypes(DataRowBuilder pRowBuilder)
            : base(pRowBuilder) {
        }

        #endregion

        #region Suggested Customizations

        //    // Use this factory method template to create new instances of this class
        //    public static RunTypes Create(PersistenceManager pManager,
        //      ... your creation parameters here ...) { 
        //
        //      RunTypes aRunTypes = pManager.CreateEntity<RunTypes>();
        //
        //      // if this object type requires a unique id and you have implemented
        //      // the IIdGenerator interface implement the following line
        //      pManager.GenerateId(aRunTypes, // add id column here //);
        //
        //      // Add custom code here
        //
        //      aRunTypes.AddToManager();
        //      return aRunTypes;
        //    }

        //    // Implement this method if you want your object to sort in a specific order
        //    public override int CompareTo(Object pObject) {
        //    }

        //    // Implement this method to customize the null object version of this class
        //    protected override void UpdateNullEntity() {
        //    }

        #endregion

        // Add additional logic to your business object here...
        public static RunTypes Create(PersistenceManager pManager) {

            RunTypes oRunType = (RunTypes)pManager.CreateEntity(typeof(RunTypes));

            //pManager.GenerateId(oRunType, RunTypes.RunTypeIDEntityColumn);
            oRunType.AddToManager();
            return oRunType;
        }

        protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
            if (IsDeserializing)
                return;
            base.OnColumnChanging(pArgs);
            //DoAudit(pArgs);
        }

        private void DoAudit(DataColumnChangeEventArgs pArgs) {
            if (!this.PersistenceManager.IsClient) {
                return;
            }
            if (this.RowState == DataRowState.Added) {
                return;
            }
            if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
                AuditRecord = RunTypesAudit.Create(this.PersistenceManager, this);
            }
            //AuditRecord[pArgs.Column.ColumnName] = pArgs.ProposedValue;
        }

        protected override void AddRules(RuleList pList) {
            PropertyRequiredRule.AddToList(pList, RunTypes.RunTypeEntityColumn.ColumnName);
            pList.Add(IsRunTypeUnique, RunTypeEntityColumn.ColumnName);
        }

        private bool IsRunTypeUnique(object pTarget, RuleArgs e) {
            RunTypes oType = (RunTypes)pTarget;

            RdbQuery oQry = new RdbQuery(typeof(RunTypes), RunTypes.RunTypeEntityColumn, EntityQueryOp.EQ, oType.RunType);
            oQry.AddClause(RunTypes.RunTypeIDEntityColumn, EntityQueryOp.NE, oType.RunTypeID);
            EntityList<RunTypes> oRunTypes = this.PersistenceManager.GetEntities<RunTypes>(oQry);
            if (oRunTypes.Count > 0) {
                e.Description = "Run Type must be unique!";
                return false;
            }
            return true;
        }

        [BindingBrowsable(false)]
        public override string EntityInstanceDisplayName {
            get {
                return RunType;
            }
        }

        public static RunTypes GetRunType(PersistenceManager pPM, string pRunType, bool bAddNewRecIfMissing) {

            RdbQuery oQry = new RdbQuery(typeof(RunTypes), RunTypes.RunTypeEntityColumn, EntityQueryOp.EQ, pRunType);
            RunTypes oRunType = pPM.GetEntity<RunTypes>(oQry);
            if (oRunType.IsNullEntity) {
                oRunType = RunTypes.Create(pPM);
                oRunType.RunType = pRunType;
                oRunType.Save();
            }
            return oRunType;
        }

    }

}
