﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using TaxiPassCommon;


namespace CabRideEngineDapper {
	public partial class usp_TaxiPassReceiptData {

		public string ID { get; set; }
		public string Market { get; set; }
		public DateTime Date { get; set; }
		public decimal Fare { get; set; }
		public decimal AirportFee { get; set; }
		public decimal MiscFee { get; set; }
		public decimal Parking { get; set; }
		public decimal Stops { get; set; }
		public decimal WaitTime { get; set; }
		public decimal Tolls { get; set; }
		public decimal Discount { get; set; }
		public decimal Gratuity { get; set; }
		public decimal BookingFee { get; set; }
		public decimal DropFee { get; set; }
		public decimal CouponAmount { get; set; }
		public decimal TaxiPassFee { get; set; }
		public decimal Total { get; set; }
		public string Customer { get; set; }
		public string Type { get; set; }
		public bool RideVoucher { get; set; }
		public string Fleet { get; set; }
		public string CabNo { get; set; }
		public string SendReceiptTo { get; set; }
		public string CardHolder { get; set; }
		public string Salt { get; set; }
		public string CardNumberDisplay { get; set; }
		public bool Test { get; set; }
		public string ImageSignature { get; set; }
		public string PickUp { get; set; }
		public string DropOff { get; set; }
		public string JobID { get; set; }
		public string PassengerName { get; set; }
		public string Platform { get; set; }
		public string AccountPhone { get; set; }
		public string AffiliateEmail { get; set; }
		public bool HasSignatureImage { get; set; }
		public string DefaultPlatform { get; set; }
		public string ReceiptFooterText { get; set; }

		public static IEnumerable<usp_TaxiPassReceiptData> Execute(SqlConnection pConn, string pTransNo, string pCustomer = "", string pCardNo = "", decimal pAmount = 0, DateTime? pStart = null) {
			DynamicParameters parms = new DynamicParameters();
			parms.Add("SendReceiptTo", pCustomer);
			if (pTransNo.IsNullOrEmpty()) {
				if (pStart == null) {
					pStart = DateTime.Today;
				}
				parms.Add("Start", pStart);
				parms.Add("End", pStart.Value.AddDays(1).AddMilliseconds(-1));
				parms.Add("CardNumber", "%" + pCardNo);
			}
			parms.Add("TransNo", pTransNo);
			parms.Add("TotalCharge", pAmount);

			return pConn.Query<usp_TaxiPassReceiptData>("usp_TaxiPassReceiptData", parms, commandType: CommandType.StoredProcedure);

		}

		public static usp_TaxiPassReceiptData Execute(SqlConnection pConn, string pTransNo) {
			DynamicParameters parms = new DynamicParameters();
			parms.Add("TransNo", pTransNo);

			return pConn.Query<usp_TaxiPassReceiptData>("usp_TaxiPassReceiptDataByTransNo", parms, commandType: CommandType.StoredProcedure).FirstOrDefault();

		}

	}
}
