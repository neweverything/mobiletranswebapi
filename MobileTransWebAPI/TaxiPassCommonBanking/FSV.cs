﻿using System;
using System.Net;
using TaxiPassCommon.Banking.Enums;
using TaxiPassCommon.Banking.Interfaces;

namespace TaxiPassCommon.Banking {
    public class FSV : IPayCard {


        private com.paychekplus.FsvRemoteService mService = new com.paychekplus.FsvRemoteService();


        private Credentials mCredentials;
        private bool mProduction;

        public FSV(IPayCardCredentials pCredentials, bool pProduction = true) {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls;

            mCredentials = pCredentials as FSV.Credentials;
            mProduction = pProduction;

            string url = mCredentials.TestURL;
            if (mProduction) {
                url = mCredentials.ProdURL;
            }

            mService.Url = url;


        }


        private string UserName {
            get {
                return mProduction ? mCredentials.UserName : mCredentials.TestUserName;
            }
        }

        public string Password {
            get {
                string val = mProduction ? mCredentials.Password : mCredentials.TestPassword;
                return val.DecryptIceKey();
            }
        }

        public string Certification {
            get {
                return mCredentials.Certification.DecryptIceKey();
            }
        }

        public string PassCode {
            get {
                return mCredentials.PassCode.DecryptIceKey();
            }
        }

        public string AdjustmentCard {
            get {
                return mProduction ? mCredentials.AdjustmentCard.DecryptIceKey() : mCredentials.TestAdjustmentCard.DecryptIceKey();
            }
        }

        public string AdjustmentCode {
            get {
                return mProduction ? mCredentials.AdjustmentCode.DecryptIceKey() : mCredentials.TestAdjustmentCode.DecryptIceKey();
            }
        }

        public string FundingCardNo {
            get {
                return mCredentials.FundingCardNo.DecryptIceKey();
            }
        }


        public VerifyConnectionResponse VerifyConnection() {
            VerifyConnectionResponse resp = new VerifyConnectionResponse();
            try {
                string resultCode = mService.Transact(UserName, Password, "3801,Verify FSV SOAP Connection Services");
                string[] temp = resultCode.Split(',');

                resp.ReturnCode = temp[0];
                resp.ReturnMessage = temp[1];

            } catch (Exception ex) {
                resp.ReturnCode = "-1";
                resp.ReturnMessage = ex.Message;
            }
            if (resp.ReturnCode != "1" && mService.Url == mCredentials.ProdURL) {
                mService.Url = mCredentials.ProdURL2;
                resp = VerifyConnection();
            }
            return resp;
        }



        public CardRegistrationResponse RegisterCard(CardRegistration pRegistration) {
            CardRegistrationResponse result = new CardRegistrationResponse();

            try {
                string data = string.Format("3126,{0}", pRegistration.ToString());
                //string[] temp = mService.Transact(UserName, Password, data).Split(',');
                string temp1 = string.Format("{0},{1},{2}", AdjustmentCard, AdjustmentCode, data);
                string resultCode = mService.Transact(UserName, Password, temp1); // "4342230000321879", "1711", temp1);
                string[] temp = resultCode.Split(','); //  //string[] temp = mService.Transact("4342230000321879", "1711", temp1).Split(',');


                result.ReturnCode = temp[0];
                result.ReturnMessage = temp[1];
                if (temp.Length > 2) {
                    result.CardID = temp[2];
                    result.ATTMID = temp[3];
                    result.RoutingNumber = temp[4];
                    result.PayToNumber = temp[5];
                }
            } catch (Exception ex) {
                result.ReturnCode = "-1";
                result.ReturnMessage = ex.Message;
            }

            if (result.ReturnCode != "1" && mService.Url == mCredentials.ProdURL) {
                mService.Url = mCredentials.ProdURL2;
                result = RegisterCard(pRegistration);
            }
            //System.Console.WriteLine(JSON.JsonHelper.JsonSerialize(result));
            return result;
        }


        public FundsTransferResponse TransferToPayCard(FundsTransfer pFunds) {
            FundsTransferResponse result = new FundsTransferResponse();

            try {
                string data = string.Format("2830,{0}", pFunds.ToString());
                string temp1 = string.Format("{0},{1},{2}", FundingCardNo, PassCode, data);
                Console.WriteLine($"To PayCard: {temp1}");
                string res = mService.Transact(UserName, Password, temp1);
                string[] temp = res.Split(',');

                result.ReturnCode = temp[0];
                result.ReturnMessage = temp[1];
                if (temp.Length > 2) {
                    result.FromCardRecordNumber = temp[2];
                    result.ToCardRecordNumber = temp[3];
                    if (temp.Length > 4) {
                        result.FromCardBalance = temp[4];
                        result.ToCardBalance = temp[5];
                    }

                }
            } catch (Exception ex) {
                result.ReturnCode = "-1";
                result.ReturnMessage = ex.Message;
            }

            if (result.ReturnCode != "1" && mService.Url == mCredentials.ProdURL) {
                mService.Url = mCredentials.ProdURL2;
                result = TransferToPayCard(pFunds);
            }

            return result;
        }

        public FundsTransferResponse TransferFromPayCard(string pPayCardNo, FundsTransfer pFunds) {
            FundsTransferResponse result = new FundsTransferResponse();

            string data = string.Format("2831,{0}", pFunds.ToString());
            //string temp1 = string.Format("{0},{1},{2}", pFunds.FromCardID, pFunds.FromCardPassCode, data);

            string temp1 = string.Format("{0},{1},{2}", pPayCardNo, pFunds.FromCardPassCode, data);
            Console.WriteLine($"From PayCard: {temp1}");
            string[] temp = mService.Transact(UserName, Password, temp1).Split(',');

            result.ReturnCode = temp[0];
            result.ReturnMessage = temp[1];
            if (temp.Length > 2) {
                result.FromCardRecordNumber = temp[2];
                result.ToCardRecordNumber = temp[3];
                if (temp.Length > 4) {
                    result.FromCardBalance = temp[4];
                    result.ToCardBalance = temp[5];
                }

            }

            return result;
        }

        public decimal GetFundingBalance() {
            return 0;
        }

        public class Credentials : IPayCardCredentials {
            public string ProdURL { get; set; }
            public string ProdURL2 { get; set; }
            public string UserName { get; set; }
            public string Password { get; set; }

            public string Certification { get; set; }
            public string PassCode { get; set; }

            public string AdjustmentCard { get; set; }
            public string AdjustmentCode { get; set; }

            public string FundingCardNo { get; set; }

            public string TestURL { get; set; }
            public string TestUserName { get; set; }
            public string TestPassword { get; set; }

            public string TestCertification { get; set; }
            public string TestPassCode { get; set; }

            public string TestAdjustmentCard { get; set; }
            public string TestAdjustmentCode { get; set; }

            public string TestFundingCardNo { get; set; }

            public string UseProduction { get; set; }

            public string NewTekAdjustmentCard { get; set; }
            public string NewTekAdjustmentCode { get; set; }
            public string NewTekFundingCardNo { get; set; }
            public DateTime NewTekStartDate { get; set; }
            public string NewTekForceByDriverCellNo { get; set; }

            public string TokenURL { get; set; }


            public string PayCardType { get; set; }

            public bool UseProductionAsBoolen {
                get {
                    return UseProduction.Equals("true", StringComparison.CurrentCultureIgnoreCase);
                }
            }

            public PayCardTypes MyPayCardType() {
                return PayCardType.ToEnum<PayCardTypes>();
            }
        }

        public class CardRegistration {
            public CardRegistration(IPayCardCredentials pPayCardCredentials) {
                //Credentials cred = pPayCardCredentials as Credentials;

                //CertCardID = cred.Certification.DecryptIceKey();
                //CertPassCode = cred.PassCode.DecryptIceKey();
                try {
                    CertCardID = pPayCardCredentials.GetType().GetProperty("Certification").GetValue(pPayCardCredentials).ToString().Decrypt();
                    CertPassCode = pPayCardCredentials.GetType().GetProperty("PassCode").GetValue(pPayCardCredentials).ToString().Decrypt();
                } catch (Exception ex) {
                    Console.WriteLine(ex.Message);
                }
            }

            public string CertCardID { get; set; }
            public string CertPassCode { get; set; }
            public string Reserved { get; set; }
            public string CardHolderCardID { get; set; }
            public string ParticipantID { get; set; }
            public string NewCardPasscode { get; set; }
            public string Upgrade { get; set; }
            public string LastName { get; set; }
            public string FirstName { get; set; }
            public string MiddleName { get; set; }
            public string Title { get; set; }
            public string Address1 { get; set; }
            public string Address2 { get; set; }
            public string Address3 { get; set; }
            public string City { get; set; }
            public string State { get; set; }
            public string Country { get; set; }
            public string PostalCode { get; set; }
            public string DateOfBirth { get; set; }
            public string SocialSecNum { get; set; }
            public string Reserved100 { get; set; }
            public string Reserved101 { get; set; }
            public string Reserved102 { get; set; }
            public string Reserved103 { get; set; }
            public string Reserved104 { get; set; }
            public string USCitizen { get; set; }
            public string GovIDType { get; set; }
            public string GovIDValue { get; set; }
            public string GovLocation { get; set; }
            public string GovExpire { get; set; }
            public string GovIssued { get; set; }
            public string CustomIDType1 { get; set; }
            public string CustomIDValue1 { get; set; }
            public string CustomIDType2 { get; set; }
            public string CustomIDValue2 { get; set; }
            public string CustomIDType3 { get; set; }
            public string CustomIDValue3 { get; set; }
            public string EmailAddress { get; set; }
            public string AltEmailAddress { get; set; }
            public string HomePhone { get; set; }
            public string OfficePhone { get; set; }
            public string MobilePhone { get; set; }
            public string FaxPhone { get; set; }
            public string PhysicalAddress1 { get; set; }
            public string PhysicalAddress2 { get; set; }
            public string PhysicalAddress3 { get; set; }
            public string PhysicalCity { get; set; }
            public string PhysicalState { get; set; }
            public string PhysicalCountry { get; set; }
            public string PhysicalZip { get; set; }
            public string EmployeeID { get; set; }
            public string StoreNumber { get; set; }
            public string EmployeeHireDate { get; set; }
            public string EmployerState { get; set; }
            public string CardType { get; set; }
            public string OtherCompanyName { get; set; }
            public string FourthLineEmbossing { get; set; }
            public string Reserved1 { get; set; }
            public string Reserved2 { get; set; }
            public string Reserved3 { get; set; }
            public string Reserved4 { get; set; }
            public string Reserved5 { get; set; }
            public string Reserved6 { get; set; }
            public string Reserved7 { get; set; }
            public string Reserved8 { get; set; }
            public string Reserved9 { get; set; }
            public string Reserved10 { get; set; }
            public string Reserved11 { get; set; }
            public string Reserved12 { get; set; }
            public string Reserved13 { get; set; }
            public string Reserved14 { get; set; }
            public string Reserved15 { get; set; }
            public string Reserved16 { get; set; }
            public string Reserved17 { get; set; }
            public string Reserved18 { get; set; }
            public string Reserved19 { get; set; }
            public string Reserved20 { get; set; }
            public string Reserved21 { get; set; }
            public string Reserved22 { get; set; }
            public string Reserved23 { get; set; }
            public string Reserved24 { get; set; }
            public string Reserved25 { get; set; }
            public string Reserved26 { get; set; }
            public string Reserved27 { get; set; }
            public string Reserved28 { get; set; }
            public string Reserved29 { get; set; }
            public string Reserved30 { get; set; }
            public string Reference { get; set; }


            public override string ToString() {
                return string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21},{22},{23},{24},{25},{26},{27},{28},{29},{30},{31},{32},{33},{34},{35},{36},{37},{38},{39},{40},{41},{42},{43},{44},{45},{46},{47},{48},{49},{50},{51},{52},{53},{54},{55},{56},{57},{58},{59},{60},{61},{62},{63},{64},{65},{66},{67},{68},{69},{70},{71},{72},{73},{74},{75},{76},{77},{78},{79},{80},{81},{82},{83},{84},{85},,,,,,{86}",
                    CertCardID, CertPassCode, Reserved, CardHolderCardID, ParticipantID, NewCardPasscode, Upgrade, LastName, FirstName, MiddleName, Title, Address1, Address2, Address3, City,
                    State, Country, PostalCode, DateOfBirth, SocialSecNum, Reserved, Reserved, Reserved, Reserved, Reserved, USCitizen, GovIDType, GovIDValue, GovLocation, GovExpire, CustomIDType1,
                    CustomIDValue1, CustomIDType2, CustomIDValue2, CustomIDType3, CustomIDValue3, EmailAddress, AltEmailAddress, HomePhone, OfficePhone, MobilePhone, FaxPhone, PhysicalAddress1,
                    PhysicalAddress2, PhysicalAddress3, PhysicalCity, PhysicalState, PhysicalCountry, PhysicalZip, EmployeeID, StoreNumber, EmployeeHireDate, EmployerState, CardType,
                    OtherCompanyName, FourthLineEmbossing, Reserved1, Reserved2, Reserved3, Reserved4, Reserved5, Reserved6, Reserved7, Reserved8, Reserved9, Reserved10, Reserved11, Reserved12,
                    Reserved13, Reserved14, Reserved15, Reserved16, Reserved17, Reserved18, Reserved19, Reserved20, Reserved21, Reserved22, Reserved23, Reserved24, Reserved25, Reserved26, Reserved27,
                    Reserved28, Reserved29, Reserved30, Reference);
            }
        }

        public class CardRegistrationResponse {

            public string ReturnCode { get; set; }
            public string ReturnMessage { get; set; }
            public string CardID { get; set; }
            public string ATTMID { get; set; }
            public string RoutingNumber { get; set; }
            public string PayToNumber { get; set; }

            public string PayCardType { get; set; }

            public string ToJsonString() {
                return JSON.JsonHelper.JsonSerialize(this);
            }
        }


        public class FundsTransfer {

            public FundsTransfer(IPayCardCredentials pPayCardCredentials) {
                Credentials cred = pPayCardCredentials as Credentials;
                FromCardID = cred.AdjustmentCard.DecryptIceKey(); // "6875635432"; cred.Certification.DecryptIceKey();
                FromCardPassCode = cred.AdjustmentCode.DecryptIceKey(); // "2830"; // cred.PassCode.DecryptIceKey();
            }

            public string FromCardID { get; set; }
            public string FromCardPassCode { get; set; }
            public string ToCardID { get; set; }
            public decimal TransferAmount { get; set; }
            public string Reserved1 { get; set; }
            public string Reserved2 { get; set; }
            public string Reserved3 { get; set; }
            public string ReferenceFromCard { get; set; }
            public string ReferenceToCard { get; set; }

            public override string ToString() {
                return string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8}", FromCardID, FromCardPassCode, ToCardID, TransferAmount, Reserved1, Reserved2, Reserved3, ReferenceFromCard, ReferenceToCard);
            }
        }

        //public class FundsTransferResponse {
        //    public string ReturnCode { get; set; }
        //    public string ReturnMessage { get; set; }
        //    public string FromCardRecordNumber { get; set; }
        //    public string ToCardRecordNumber { get; set; }
        //    public string Unused1 { get; set; }
        //    public string Unused2 { get; set; }
        //    public string FromCardBalance { get; set; }
        //    public string ToCardBalance { get; set; }
        //}


        public class VerifyConnectionResponse {
            public string ReturnCode { get; set; }
            public string ReturnMessage { get; set; }
        }

        //public class RetreiveFunds {
        //    public string FromCardID { get; set; }
        //    public string FromCardPassCode { get; set; }
        //    public string ToCardID { get; set; }
        //    public string TransferAmount { get; set; }
        //    public string Reserved1 { get; set; }
        //    public string Reserved2 { get; set; }
        //    public string Reserved3 { get; set; }
        //    public string ReferenceFromCard { get; set; }
        //    public string ReferenceToCard { get; set; }
        //}
    }
}
