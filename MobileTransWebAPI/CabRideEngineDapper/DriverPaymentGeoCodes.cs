﻿using CabRideEngineDapper.Utils;
using Dapper;
using System.Collections.Generic;
using System.Linq;

namespace CabRideEngineDapper {
    public partial class DriverPaymentGeoCodes {

        public static DriverPaymentGeoCodes Create(DriverPayments pDriverPayment) {
            if (pDriverPayment.IsNewRecord) {
                pDriverPayment.Save();
            }
            DriverPaymentGeoCodes rec = DriverPaymentGeoCodes.Create();
            rec.DriverPaymentID = pDriverPayment.DriverPaymentID;
            if (pDriverPayment.DriverPaymentGeoCodeses == null) {
                pDriverPayment.DriverPaymentGeoCodeses = new List<DriverPaymentGeoCodes>();
            }
            pDriverPayment.DriverPaymentGeoCodeses.Add(rec);
            return rec;
        }

        public static DriverPaymentGeoCodes GetRecordByDriverPaymentID(long pDriverPaymentID) {
            using (var conn = SqlHelper.OpenSqlConnection()) {
                string sql = "Select * FROM DriverPaymentGeoCodes WHERE DriverPaymentID = @DriverPaymentID";
                return conn.Get<DriverPaymentGeoCodes>(sql, new { DriverPaymentID = pDriverPaymentID });
            }
        }

        public static List<DriverPaymentGeoCodes> GetRecordsByDriverPaymentID(long pDriverPaymentID) {
            using (var conn = SqlHelper.OpenSqlConnection()) {
                string sql = "Select * FROM DriverPaymentGeoCodes WHERE DriverPaymentID = @DriverPaymentID";
                return conn.Query<DriverPaymentGeoCodes>(sql, new { DriverPaymentID = pDriverPaymentID }).ToList();
            }
        }
    }
}
