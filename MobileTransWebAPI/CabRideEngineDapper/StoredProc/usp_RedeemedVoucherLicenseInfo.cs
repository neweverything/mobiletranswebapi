﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CabRideEngineDapper {
    public class usp_RedeemedVoucherLicenseInfo {
        public string TransNo { get; set; }
        public DateTime ChargeDate { get; set; }
        public decimal DriverTotal { get; set; }
        public decimal RedemptionFee { get; set; }
        public decimal DriverFee { get; set; }
        public decimal DriverTotalPaid { get; set; }
        public decimal TaxiPassFee { get; set; }
        public decimal CouponAmount { get; set; }
        public decimal Discount { get; set; }
        public decimal TotalCharge { get; set; }
        public DateTime DriverPaidDate { get; set; }
        public string LicenseNo { get; set; }
        public string CabNumber { get; set; }
        public string DriverSignature { get; set; }
        public string Name { get; set; }
        public string LicenseName { get; set; }
        public bool FraudLicense { get; set; }

        public string GridKey {
            get {
                return string.Format("{0}|{1}", TransNo, LicenseNo);
            }
        }

        public static List<usp_RedeemedVoucherLicenseInfo> Execute(SqlConnection pConn, DateTime pStart, DateTime pEnd, string pLicenseNo = "") {
            DynamicParameters param = new DynamicParameters();
            param.Add("StartDate", pStart);
            param.Add("EndDate", pEnd);
            param.Add("LicenseNo", pLicenseNo);

            return pConn.Query<usp_RedeemedVoucherLicenseInfo>("usp_RedeemedVoucherLicenseInfo", param, commandType: CommandType.StoredProcedure).ToList();

        }
    }
}
