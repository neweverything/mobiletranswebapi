﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using Csla.Validation;
using IdeaBlade.Persistence;
using IdeaBlade.Util;

using TaxiPassCommon;
using Newtonsoft.Json;

namespace CabRideEngine {
	[Serializable]
	public abstract class BaseEntity : BaseEntityDataRow, IRuleMaker, IDataErrorInfo {

		private BrokenRuleManager _BrokenRuleManager;

		private Entity AuditRecordEntity;
		private DateTime mEntityAccessDate = DateTime.Now;

		#region Constructors -- DO NOT MODIFY
		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// Typed DataSet constructor (needed by framework) - DO NOT REMOVE
		protected BaseEntity(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		// Add additional logic to your business object here...
		[JsonIgnore]
		protected bool ExecutingOnClient {
			get {
				PersistenceManager pm = this.PersistenceManager;
				return (pm != null && pm.IsClient);
			}
		}


		/// <summary>Initialize the rules for objects of this type.</summary>
		/// <remarks>Invoked by the ModelRuleSystem once for this type.</remarks>
		protected virtual void AddRules(RuleList pList) {
		}

		/// <summary>Initialize the rules for objects of this type.</summary>
		/// <remarks>Invoked by the ModelRuleSystem once for this type.
		/// Note that the interface version of this method is public but only when cast to the interface
		/// while the non-interface version is protected within the Entity type.
		/// </remarks>
		void IRuleMaker.AddRules(RuleList pList) {
			this.AddRules(pList);
		}

		/// <summary>Provides access to the broken rules functionality.</summary>
		/// <remarks>
		/// This property is used within your business logic so you can
		/// easily call the AddRule() method to associate validation
		/// rules with your object's properties.
		/// </remarks>
		[JsonIgnore]
		protected virtual BrokenRuleManager BrokenRuleManager {
			get {
				if (_BrokenRuleManager == null) {
					bool onClient = ExecutingOnClient;
					_BrokenRuleManager = // initialize rule list only if on client.
					  new BrokenRuleManager(this, EntityRuleSystem.GetInstanceRuleList(this, onClient));
					_BrokenRuleManager.Enabled = onClient;// will only run if on the client.
				}
				return _BrokenRuleManager;
			}
		}

		/// <summary>
		/// Returns <see langword="true" /> if the object is currently valid, <see langword="false" /> if the
		/// object has broken rules or is otherwise invalid.
		/// </summary>
		/// <remarks>
		/// <para>
		/// By default this property relies on the underling BrokenRuleManager
		/// object to track whether any business rules are currently broken for this object.
		/// </para><para>
		/// You can override this property to provide more sophisticated
		/// implementations of the behavior. For instance, you should always override
		/// this method if your object has child objects, since the validity of this object
		/// is affected by the validity of all child objects.
		/// </para>
		/// </remarks>
		/// <returns>A value indicating if the object is currently valid.</returns>
		[BindingBrowsable(false)]
		[JsonIgnore]
		public virtual bool IsValid {
			get {
				return BrokenRuleManager.IsValid;
			}
		}

		/// <summary>Raises OnPropertyChanged event for the pPropertyName.</summary>
		protected void OnPropertyChanged(string pPropertyName) {
			OnPropertyChanged(new PropertyChangedEventArgs(pPropertyName));
		}

		/// <summary>Raises OnPropertyChanged event.</summary>
		/// <remarks>Checks validation rules first.</remarks> 
		protected override void OnPropertyChanged(PropertyChangedEventArgs pArgs) {
			CheckRules(pArgs.PropertyName);
			base.OnPropertyChanged(pArgs);
		}

		private void CheckRules(string pPropertyName) {
			mEntityAccessDate = DateTime.Now;
			if (String.IsNullOrEmpty(pPropertyName))
				return;
			// Don't check if deleted or detached
			if (0 < (RowState & (DataRowState.Detached | DataRowState.Deleted)))
				return;
			BrokenRuleManager.CheckRules(pPropertyName);
		}


		/// <summary>Get the readonly collection of broken business rules for this object.</summary>
		[BindingBrowsable(false)]
		[JsonIgnore]
		public virtual ReadOnlyBrokenRuleCollection BrokenRules {
			get {
				return BrokenRuleManager.BrokenRules;
			}
		}

		[JsonIgnore]
		string IDataErrorInfo.Error {
			get {
				if (!IsValid)
					return BrokenRuleManager.BrokenRules.ToString();
				else
					return String.Empty;
			}
		}

		[JsonIgnore]
		string IDataErrorInfo.this[string columnName] {
			get {
				string result = string.Empty;
				if (!IsValid) {
					BrokenRule rule =
					  BrokenRuleManager.BrokenRules.GetFirstBrokenRule(columnName);
					if (rule != null)
						result = rule.Description;
				}
				return result;
			}
		}


		/// <summary>Validate() object for save to datasource.</summary>
		/// <remarks>Looks to Broken Rules first; if none, runs the traditional DevForce Validation method
		/// encapsulated in <see cref="ValidateCore()"/>.
		/// Broken Rules or exception thrown in ValidateCore return a Validation Result with Severity=Error.
		/// The method client should interpret this as an instruction to not save and report the error
		/// to the user.
		/// </remarks>
		public override EntityValidationInfo Validate(EntityValidationContext pContext) {
			EntityValidationInfo vr;
			if (IsValid) { // no known broken rules; check all anyway.
				BrokenRuleManager.CheckRules();
			}
			if (IsValid) {
				try {
					return base.Validate(pContext);
				} catch (Exception e) {
					vr = new EntityValidationInfo(false, e.Message);
					return vr;
				}
			}
			// Broken Rules pending; these trump the ValidateCore.
			vr = new EntityValidationInfo(false, BrokenRules.Text);
			return vr;
		}

		/// <summary>Traditional DevForce Validate() method.</summary>
		/// <remarks>ValidateCore() invoked if there are no broken rules; otherwise broken rules rule.</remarks>
		public virtual EntityValidationInfo ValidateCore() {
			return base.Validate(null);
		}

		/// <summary>Get the displayable name of this object's type (e.g., its class name).</summary>
		/// <remarks>Typically useful in validation messages.</remarks>
		[BindingBrowsable(false)]
		[JsonIgnore]
		public virtual string EntityTypeDisplayName {
			get {
				return this.GetType().Name;
			}
		}

		/// <summary>Get the displayable name of this object instance.</summary>
		/// <remarks>Typically useful in validation messages.</remarks>
		[BindingBrowsable(false)]
		[JsonIgnore]
		public virtual string EntityInstanceDisplayName {
			get {
				return EntityTypeDisplayName;
			}
		}
		/// <summary>Save this instance of this object to the database and override ModifiedBy with given string</summary>
		/// <remarks></remarks>
		public virtual SaveResult Save(String pModifiedBy) {
			SaveResult oResult = null;

			try {
				pModifiedBy += " | " + System.AppDomain.CurrentDomain.BaseDirectory;
			} catch (Exception) {
			}

			if (this.IsNullEntity || this.RowState == DataRowState.Unchanged) {
				oResult = new SaveResult(null, null, SaveOperation.NoOperation);
				return oResult;
			}

			if (this.Table.Columns.Contains("CreatedBy")) {
				if (this["CreatedBy"].ToString().IsNullOrEmpty()) {
					this["CreatedBy"] = pModifiedBy;
				}
			}
			if (this.Table.Columns.Contains("ModifiedBy")) {
				this["ModifiedBy"] = pModifiedBy;
			}
			if (this.Table.Columns.Contains("ModifiedDate")) {
				this["ModifiedDate"] = DateTime.Now.ToUniversalTime();
			}

			EntityValidationInfo vr = this.Validate(null);
			if (!vr.IsValid) {
				string errantEntityName = MakeEntityName(this);
				throw new Exception(String.Format("Cannot save {1}.{0}{0}{2}.", Environment.NewLine, errantEntityName, vr.Message));
			}
			List<object> saveRec = new List<object>();
			saveRec.Add(this);
			if (AuditRecord != null) {
				saveRec.Add(AuditRecord);
			}

			try {
				oResult = this.PersistenceManager.SaveChanges(saveRec);

			} catch (PersistenceManagerSaveException ex) {
				throw ex;
			}
			return oResult;
		}

		/// <summary>Save this instance of this object to the database.</summary>
		/// <remarks></remarks>
		public virtual SaveResult Save() {
			return Save(LoginManager.PrincipleUser.Identity.Name);
		}


		private string MakeEntityName(Entity anEntity) {
			BaseEntity aBaseEntity = anEntity as BaseEntity;
			string name;
			if (aBaseEntity == null) {
				name = anEntity.GetType().Name;
			} else {
				name = aBaseEntity.EntityTypeDisplayName + " \"" + aBaseEntity.EntityInstanceDisplayName + "\"";
			}
			return name;
		}

		[JsonIgnore]
		public virtual Entity AuditRecord {
			set {
				AuditRecordEntity = value;
			}

			get {
				return AuditRecordEntity;
			}
		}

		[JsonIgnore]
		public DateTime EntityLastAccessed {
			get {
				return mEntityAccessDate;
			}
		}

		public virtual bool DoUndo() {
			return DoUndo(true);
		}

		public virtual bool DoUndo(bool pShowVerifyMsg) {
			bool bOk = true;
			if (this.RowState == DataRowState.Unchanged || this.RowState == DataRowState.Detached) {
				return true;
			} else if (pShowVerifyMsg) {
				bOk = WindowUtils.Verify("Are you sure you want to revert the changes made to " + this.EntityInstanceDisplayName);
			}
			if (bOk) {
				try {
					if (this.RowState == DataRowState.Added) {
						this.Delete();
					} else {
						this.Undo();
					}

					if (AuditRecordEntity != null) {
						AuditRecordEntity.Delete();
					}
					return true;
				} catch (Exception ex) {
					WindowUtils.ShowErrorMessage(ex);
				}
			}
			return false;
		}

		public virtual bool DoDelete() {
			return DoDelete(true);
		}

		public virtual bool DoDelete(bool pShowVerifyMsg) {
			string sMsg = "You are about to delete " + this.EntityInstanceDisplayName;
			sMsg += "\n\nAre you sure you want to delete this record";
			if (pShowVerifyMsg) {
				if (!WindowUtils.Verify(sMsg)) {
					return false;
				}
			}
			this.Delete();
			return true;
		}


		public Entity[] GetChangedList() {
			return this.PersistenceManager.GetEntities(this.GetType(), DataRowState.Added | DataRowState.Modified | DataRowState.Deleted);
		}


		public static void UpdateModifedByInfo(Entity pRec, string pModifiedBy, DateTime pModifiedDate) {
			//update modified by and date

			if (!pRec.IsNullEntity) {
				try {
					if (pRec.RowState != DataRowState.Deleted) {
						if (pRec.Table.Columns.Contains("CreatedBy")) {
							if (pRec["CreatedBy"].ToString().IsNullOrEmpty()) {
								pRec["CreatedBy"] = pModifiedBy;
							}
						}
						if (pRec.Table.Columns.Contains("ModifiedBy")) {
							pRec["ModifiedBy"] = pModifiedBy;
						}
						if (pRec.Table.Columns.Contains("ModifiedDate")) {
							pRec["ModifiedDate"] = pModifiedDate;
						}
					}
				} catch (Exception) {
				}
			}
		}

	}

}
