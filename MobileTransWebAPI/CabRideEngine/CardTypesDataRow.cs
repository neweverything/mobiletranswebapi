using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Runtime.Serialization;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

namespace CabRideEngine {
 
  #region CardTypesDataTable
  /// <summary>
  /// The generated <see cref="T:IdeaBlade.Persistence.EntityTable"/> class  
  /// containing all <see cref="CardTypes"/> objects.
  /// </summary>
  /// <remarks>
  /// ****************************************************
  /// <para>** DO NOT MODIFY THIS CLASS - AutoGenerated Code</para>
  /// <para>** Any changes made to this code will be lost</para>
  /// <para>** the next time this code is regenerated.</para>
  /// <para>**</para>
  /// <para>** Generated from:   EntityBaseclass.template</para>
  /// <para>** Last modified:    2016-09-13T16:10:06.3057934-07:00</para>
  /// <para>** Template version: 3.110127</para>
  /// ****************************************************
  /// </remarks>
  [BindingBrowsable(false)]
  [Serializable]
  public partial class CardTypesDataTable : BaseEntityDataTable, 
     IdeaBlade.Persistence.Rdb.IRdbTable {
  
    /// <summary>
    /// Initializes a new instance of the CardTypesDataTable class with no arguments.
    /// </summary>
    public CardTypesDataTable() {}

    /// <summary>
    /// Initializes a new instance of the CardTypesDataTable class 
    /// with the <see cref="System.Runtime.Serialization.SerializationInfo"/> 
    /// and the <see cref="System.Runtime.Serialization.StreamingContext"/>.
    /// </summary>
    /// <param name="pInfo">The data needed to serialize or deserialize an object.</param>
    /// <param name="pContext">The source and destination of a given serialized stream.</param>
    protected CardTypesDataTable(SerializationInfo pInfo, StreamingContext pContext) :
      base(pInfo, pContext) {}

    /// <summary>
    /// Creates a new instance of the <see cref="CardTypesDataRow"/>.
    /// Constructs a row from the builder.
    /// <para>** For internal use only. **</para>
    /// </summary>
    /// <param name="pRowBuilder"></param>
    /// <returns>The <see cref="CardTypesDataRow"/>.</returns>
    protected override DataRow NewRowFromBuilder(DataRowBuilder pRowBuilder) {
      return new CardTypes(pRowBuilder);
    }

    /// <summary>Returns the <see cref="System.Type"/> of <see cref="CardTypes"/>.</summary>
    protected override Type GetRowType() {
      return typeof(CardTypes);
    }
    
    /// <summary>
    /// Gets the name of the <see cref="T:IdeaBlade.Rdb.RdbKey"/> or 
    /// <see cref="T:IdeaBlade.Persistence.WS.WsKey"/> defined in <b>IdeaBlade.ibconfig</b>. 
    /// </summary>
    /// <remarks>For this <see cref="CardTypes"/>, the DataSourceKeyName="default".</remarks>
    public override String DataSourceKeyName {
      get { return @"default"; }
    }

    #region Column properties
    //**************************************
    //* Column properties
    //**************************************
    /// <summary>Gets the CardTypeID <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn CardTypeIDColumn {
      get {
        if (mCardTypeIDColumn!=null) return mCardTypeIDColumn;
        mCardTypeIDColumn = GetColumn("CardTypeID", true);
        return mCardTypeIDColumn;
      }
    }
    private DataColumn mCardTypeIDColumn;
    
    /// <summary>Gets the CardType <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn CardTypeColumn {
      get {
        if (mCardTypeColumn!=null) return mCardTypeColumn;
        mCardTypeColumn = GetColumn("CardType", true);
        return mCardTypeColumn;
      }
    }
    private DataColumn mCardTypeColumn;
    
    /// <summary>Gets the Validation <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn ValidationColumn {
      get {
        if (mValidationColumn!=null) return mValidationColumn;
        mValidationColumn = GetColumn("Validation", true);
        return mValidationColumn;
      }
    }
    private DataColumn mValidationColumn;
    
    /// <summary>Gets the RowVersion <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn RowVersionColumn {
      get {
        if (mRowVersionColumn!=null) return mRowVersionColumn;
        mRowVersionColumn = GetColumn("RowVersion", true);
        return mRowVersionColumn;
      }
    }
    private DataColumn mRowVersionColumn;
    
    /// <summary>Gets the ModifiedBy <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn ModifiedByColumn {
      get {
        if (mModifiedByColumn!=null) return mModifiedByColumn;
        mModifiedByColumn = GetColumn("ModifiedBy", true);
        return mModifiedByColumn;
      }
    }
    private DataColumn mModifiedByColumn;
    
    /// <summary>Gets the ModifiedDate <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn ModifiedDateColumn {
      get {
        if (mModifiedDateColumn!=null) return mModifiedDateColumn;
        mModifiedDateColumn = GetColumn("ModifiedDate", true);
        return mModifiedDateColumn;
      }
    }
    private DataColumn mModifiedDateColumn;
    
    /// <summary>Gets the TaxiTronicCardTypeID <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn TaxiTronicCardTypeIDColumn {
      get {
        if (mTaxiTronicCardTypeIDColumn!=null) return mTaxiTronicCardTypeIDColumn;
        mTaxiTronicCardTypeIDColumn = GetColumn("TaxiTronicCardTypeID", true);
        return mTaxiTronicCardTypeIDColumn;
      }
    }
    private DataColumn mTaxiTronicCardTypeIDColumn;
    
    #endregion    

    /// <summary>Creates the schema mapping information for this CardTypesDataTable.</summary>
    /// <returns>
    /// The <see cref="T:IdeaBlade.Persistence.TableMappingInfo"/> for this CardTypesDataTable.
    /// </returns>
    protected override TableMappingInfo CreateTableMappingInfo() {
      RdbTableMappingInfo mappingInfo = new RdbTableMappingInfo(this);
      mappingInfo.SourceOwnerName = "dbo"; 
      mappingInfo.SourceTableName = "CardTypes"; 
      mappingInfo.ConcurrencyColumnName = "RowVersion"; 
      // ColumnSelectionOption: selectAllColumns
      mappingInfo.SourceColumnNames = "*"; 
   
      DataColumnMappingCollection columnMappings = mappingInfo.TableMapping.ColumnMappings;
      columnMappings.Clear();
      columnMappings.Add("CardTypeID", "CardTypeID");
      columnMappings.Add("CardType", "CardType");
      columnMappings.Add("Validation", "Validation");
      columnMappings.Add("RowVersion", "RowVersion");
      columnMappings.Add("ModifiedBy", "ModifiedBy");
      columnMappings.Add("ModifiedDate", "ModifiedDate");
      columnMappings.Add("TaxiTronicCardTypeID", "TaxiTronicCardTypeID");
      return mappingInfo;
    }
  
    /// <summary>** For internal use only. **</summary>
    protected override void UpdateTableMappingInfo() {
      base.UpdateTableMappingInfo();
      this.TableMappingInfo.ColumnUpdateMap["RowVersion"] = "RowVersion + 1";
    }

    /// <summary>
    /// Initializes CardTypes <see cref="System.Data.DataColumn"/> properties.
    /// </summary>
    public override void InitColumnProperties() {
      base.InitColumnProperties();

      CardTypeIDColumn.Caption = "CardTypeID";
      CardTypeColumn.Caption = "CardType";
      ValidationColumn.Caption = "Validation";
      RowVersionColumn.Caption = "RowVersion";
      ModifiedByColumn.Caption = "ModifiedBy";
      ModifiedDateColumn.Caption = "ModifiedDate";
      TaxiTronicCardTypeIDColumn.Caption = "TaxiTronicCardTypeID";
    }
  }
  #endregion
  
  #region CardTypesDataRow
  /// <summary>
  /// The generated base class for <see cref="CardTypes"/>.
  /// </summary>
  /// <remarks>
  /// <para>
  /// It represents a row of persistable data in the <see cref="CardTypesDataTable"/>
  /// and derives from <see cref="T:IdeaBlade.Persistence.Entity"/>.
	/// </para>
  /// ****************************************************
  /// <para>** DO NOT MODIFY THIS CLASS - AutoGenerated Code</para>
  /// <para>** Any changes made to this code will be lost</para>
  /// <para>** the next time this code is regenerated.</para>
  /// <para>**</para>
  /// <para>** Generated from:   EntityBaseclass.template</para>
  /// <para>** Last modified:    2016-09-13T16:10:06.3057934-07:00</para>
  /// <para>** Template version: 3.110127</para>
  /// ****************************************************
  /// </remarks>  
  [Serializable]
  public abstract partial class CardTypesDataRow : BaseEntity, 
     IdeaBlade.Persistence.Rdb.IRdbEntity {
  
    /// <summary>
    /// Initializes a new instance of the CardTypesDataRow.
    /// Constructs a row from the builder.
    /// <para>** For internal use only. **</para>
    /// </summary>
    /// <param name="pRowBuilder"></param>
    protected CardTypesDataRow(DataRowBuilder pRowBuilder)
      : base(pRowBuilder) {
    }

    /// <summary>Gets the <see cref="CardTypesDataTable"/>.</summary>
    [BindingBrowsable(false)]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public new CardTypesDataTable TypedTable {
      get { return (CardTypesDataTable) base.Table; }
    }

    /// <summary>Returns the <see cref="System.Type"/> of <see cref="CardTypesDataTable"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public static Type GetTableType() {
      return typeof(CardTypesDataTable);
    }

    /// <summary>
    /// Gets the SQL 'where' clause used to restrict which 
    /// database records are accessible via this entity. 
    /// </summary>
    /// <remarks>
    /// The default property value is defined in the Current Class 
    /// Properties tab in the <b>IdeaBlade DevForce Object Mapping Tool</b>.
    /// <para>
    /// The 'where' clause will be used, in addition to any other 
    /// query-specific criteria, in every retrieval of entities of this type.
    /// </para>
    /// </remarks>
    [BindingBrowsable(false)]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public virtual string SqlWhereClause {
      get { return ""; }
    }
    
    /// <summary>
    /// Determines whether to use the 'distinct' keyword when 
    /// generating the SQL select clause for objects of this type.
    /// </summary>
    /// <remarks>Defaults to false, but may be overridden in subclasses.</remarks>
    [BindingBrowsable(false)]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public virtual bool ForceSqlDistinct {
      get { return false; }
    }
    

    #region Enums
    //**************************************
    //* Enums
    //**************************************
    #endregion
	
    #region Relation properties
    //**************************************
    //* Relation properties
    //**************************************
	  /// <summary>Gets the CustomerCardses relation property.</summary>
    [RelationProperty("CardTypes_CustomerCards", 
    QueryDirection.ChildQuery)]
    public virtual ReadOnlyEntityList<CustomerCards> CustomerCardses {
      get { 
        ReadOnlyEntityList<CustomerCards> result_;
        if (GetInterceptor<ReadOnlyEntityList<CustomerCards>>("CustomerCardses", GetCustomerCardsesImpl, out result_)) return result_;
        return GetCustomerCardsesImpl();
      }
    }
    private ReadOnlyEntityList<CustomerCards> GetCustomerCardsesImpl() {
      return this.GetManagedChildren<CustomerCards>(EntityRelations.CardTypes_CustomerCards);
    } 
    
	  /// <summary>Gets the Reservationses relation property.</summary>
    [RelationProperty("CardTypes_Reservations", 
    QueryDirection.ChildQuery)]
    public virtual ReadOnlyEntityList<Reservations> Reservationses {
      get { 
        ReadOnlyEntityList<Reservations> result_;
        if (GetInterceptor<ReadOnlyEntityList<Reservations>>("Reservationses", GetReservationsesImpl, out result_)) return result_;
        return GetReservationsesImpl();
      }
    }
    private ReadOnlyEntityList<Reservations> GetReservationsesImpl() {
      return this.GetManagedChildren<Reservations>(EntityRelations.CardTypes_Reservations);
    } 
    
	  /// <summary>Gets the Dispatchs relation property.</summary>
    [RelationProperty("CardTypes_Dispatch", 
    QueryDirection.ChildQuery)]
    public virtual ReadOnlyEntityList<Dispatch> Dispatchs {
      get { 
        ReadOnlyEntityList<Dispatch> result_;
        if (GetInterceptor<ReadOnlyEntityList<Dispatch>>("Dispatchs", GetDispatchsImpl, out result_)) return result_;
        return GetDispatchsImpl();
      }
    }
    private ReadOnlyEntityList<Dispatch> GetDispatchsImpl() {
      return this.GetManagedChildren<Dispatch>(EntityRelations.CardTypes_Dispatch);
    } 
    
    #endregion
    
    #region EntityColumn definitions
    //**************************************
    //* Static EntityColumn definitions
    //**************************************
    /// <summary>The CardTypeID <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn CardTypeIDEntityColumn =
      new EntityColumn(typeof(CardTypes), "CardTypeID", typeof(System.Int64), false, true, true, DataSourceAccessType.ReadWrite);      
    /// <summary>The CardType <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn CardTypeEntityColumn =
      new EntityColumn(typeof(CardTypes), "CardType", typeof(System.String), false, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The Validation <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn ValidationEntityColumn =
      new EntityColumn(typeof(CardTypes), "Validation", typeof(System.String), true, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The RowVersion <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn RowVersionEntityColumn =
      new EntityColumn(typeof(CardTypes), "RowVersion", typeof(System.Int32), false, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The ModifiedBy <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn ModifiedByEntityColumn =
      new EntityColumn(typeof(CardTypes), "ModifiedBy", typeof(System.String), true, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The ModifiedDate <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn ModifiedDateEntityColumn =
      new EntityColumn(typeof(CardTypes), "ModifiedDate", typeof(System.DateTime), true, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The TaxiTronicCardTypeID <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn TaxiTronicCardTypeIDEntityColumn =
      new EntityColumn(typeof(CardTypes), "TaxiTronicCardTypeID", typeof(System.String), true, false, false, DataSourceAccessType.ReadWrite);      
    #endregion
    
    #region Properties
    //**************************************
    //* CardTypeID methods
    //**************************************
    /// <summary>Gets the CardTypeID <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn CardTypeIDColumn {
      get { return TypedTable.CardTypeIDColumn; }
    }

    /// <summary>Gets the CardTypeID.</summary>
    [DBDataType(typeof(System.Int64))]
    public virtual System.Int64 CardTypeID {
      get { 
        System.Int64 result_;
        if (GetInterceptor<System.Int64>("CardTypeID", GetCardTypeIDImpl, out result_)) return result_;
        return GetCardTypeIDImpl();
      }
    }
    private System.Int64 GetCardTypeIDImpl() {
      return (System.Int64) GetColumnValue(CardTypeIDColumn, typeof(System.Int64), false); 
    }
    
    //**************************************
    //* CardType methods
    //**************************************
    /// <summary>Gets the CardType <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn CardTypeColumn {
      get { return TypedTable.CardTypeColumn; }
    }

    /// <summary>Gets or sets the CardType.</summary>
    [MaxTextLength(20)]
    [DBDataType(typeof(System.String))]
    public virtual System.String CardType {
      get { 
        System.String result_;
        if (GetInterceptor<System.String>("CardType", GetCardTypeImpl, out result_)) return result_;
        return GetCardTypeImpl();
      }
      set { 
          if (!SetInterceptor<System.String>("CardType", value, SetCardTypeImpl)) {
            SetCardTypeImpl(value);
          }  
      }    
    }
    private System.String GetCardTypeImpl() {
      return (System.String) GetColumnValue(CardTypeColumn, typeof(System.String), false); 
    }
    private void SetCardTypeImpl(System.String value) {
      SetColumnValue(CardTypeColumn, value);
    }
    
    //**************************************
    //* Validation methods
    //**************************************
    /// <summary>Gets the Validation <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn ValidationColumn {
      get { return TypedTable.ValidationColumn; }
    }

    /// <summary>Gets or sets the Validation.</summary>
    [MaxTextLength(150)]
    [DBDataType(typeof(System.String))]
    public virtual System.String Validation {
      get { 
        System.String result_;
        if (GetInterceptor<System.String>("Validation", GetValidationImpl, out result_)) return result_;
        return GetValidationImpl();
      }
      set { 
          if (!SetInterceptor<System.String>("Validation", value, SetValidationImpl)) {
            SetValidationImpl(value);
          }  
      }    
    }
    private System.String GetValidationImpl() {
      return (System.String) GetColumnValue(ValidationColumn, typeof(System.String), true); 
    }
    private void SetValidationImpl(System.String value) {
      SetColumnValue(ValidationColumn, value);
    }
    
    //**************************************
    //* RowVersion methods
    //**************************************
    /// <summary>Gets the RowVersion <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn RowVersionColumn {
      get { return TypedTable.RowVersionColumn; }
    }

    /// <summary>Gets or sets the RowVersion.</summary>
    [DBDataType(typeof(System.Int32))]
    public virtual System.Int32 RowVersion {
      get { 
        System.Int32 result_;
        if (GetInterceptor<System.Int32>("RowVersion", GetRowVersionImpl, out result_)) return result_;
        return GetRowVersionImpl();
      }
      set { 
          if (!SetInterceptor<System.Int32>("RowVersion", value, SetRowVersionImpl)) {
            SetRowVersionImpl(value);
          }  
      }    
    }
    private System.Int32 GetRowVersionImpl() {
      return (System.Int32) GetColumnValue(RowVersionColumn, typeof(System.Int32), false); 
    }
    private void SetRowVersionImpl(System.Int32 value) {
      SetColumnValue(RowVersionColumn, value);
    }
    
    //**************************************
    //* ModifiedBy methods
    //**************************************
    /// <summary>Gets the ModifiedBy <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn ModifiedByColumn {
      get { return TypedTable.ModifiedByColumn; }
    }

    /// <summary>Gets or sets the ModifiedBy.</summary>
    [MaxTextLength(100)]
    [DBDataType(typeof(System.String))]
    public virtual System.String ModifiedBy {
      get { 
        System.String result_;
        if (GetInterceptor<System.String>("ModifiedBy", GetModifiedByImpl, out result_)) return result_;
        return GetModifiedByImpl();
      }
      set { 
          if (!SetInterceptor<System.String>("ModifiedBy", value, SetModifiedByImpl)) {
            SetModifiedByImpl(value);
          }  
      }    
    }
    private System.String GetModifiedByImpl() {
      return (System.String) GetColumnValue(ModifiedByColumn, typeof(System.String), true); 
    }
    private void SetModifiedByImpl(System.String value) {
      SetColumnValue(ModifiedByColumn, value);
    }
    
    //**************************************
    //* ModifiedDate methods
    //**************************************
    /// <summary>Gets the ModifiedDate <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn ModifiedDateColumn {
      get { return TypedTable.ModifiedDateColumn; }
    }

    /// <summary>Gets or sets the ModifiedDate.</summary>
    [DBDataType(typeof(System.DateTime))]
    public virtual Nullable<System.DateTime> ModifiedDate {
      get { 
        Nullable<System.DateTime> result_;
        if (GetInterceptor<Nullable<System.DateTime>>("ModifiedDate", GetModifiedDateImpl, out result_)) return result_;
        return GetModifiedDateImpl();
      }
      set { 
          if (!SetInterceptor<Nullable<System.DateTime>>("ModifiedDate", value, SetModifiedDateImpl)) {
            SetModifiedDateImpl(value);
          }
      }    
    }
    private Nullable<System.DateTime> GetModifiedDateImpl() {
      return (Nullable<System.DateTime>) GetColumnValue(ModifiedDateColumn, typeof(System.DateTime), true); 
    }
    private void SetModifiedDateImpl(Nullable<System.DateTime> value) {
      SetColumnValue(ModifiedDateColumn, value);
    }
    
    //**************************************
    //* TaxiTronicCardTypeID methods
    //**************************************
    /// <summary>Gets the TaxiTronicCardTypeID <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn TaxiTronicCardTypeIDColumn {
      get { return TypedTable.TaxiTronicCardTypeIDColumn; }
    }

    /// <summary>Gets or sets the TaxiTronicCardTypeID.</summary>
    [MaxTextLength(2)]
    [DBDataType(typeof(System.String))]
    public virtual System.String TaxiTronicCardTypeID {
      get { 
        System.String result_;
        if (GetInterceptor<System.String>("TaxiTronicCardTypeID", GetTaxiTronicCardTypeIDImpl, out result_)) return result_;
        return GetTaxiTronicCardTypeIDImpl();
      }
      set { 
          if (!SetInterceptor<System.String>("TaxiTronicCardTypeID", value, SetTaxiTronicCardTypeIDImpl)) {
            SetTaxiTronicCardTypeIDImpl(value);
          }  
      }    
    }
    private System.String GetTaxiTronicCardTypeIDImpl() {
      return (System.String) GetColumnValue(TaxiTronicCardTypeIDColumn, typeof(System.String), true); 
    }
    private void SetTaxiTronicCardTypeIDImpl(System.String value) {
      SetColumnValue(TaxiTronicCardTypeIDColumn, value);
    }
    
    #endregion

  }
  #endregion

  #region EntityPropertyDescriptors.CardTypesPropertyDescriptor
  public static partial class EntityPropertyDescriptors {

    /// <summary>
    /// Gets the CardTypesPropertyDescriptor for <see cref="CardTypes"/>.
    /// </summary>
    public static CardTypesPropertyDescriptor CardTypes {
      get { return CardTypesPropertyDescriptor.Instance; }
    }
    
    /// <summary>
    /// The generated class containing <see cref="CardTypes"/> PropertyDescriptors.
    /// </summary>
    public partial class CardTypesPropertyDescriptor : BaseEntityPropertyDescriptor {
	
			/// <summary>
      /// Initializes a new instance of the CardTypesPropertyDescriptor class. 
      /// </summary>
      /// <param name="pParentDescriptor">The parent <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/></param>
      /// <param name="pPropertyPath">The type of the property value</param>
			public CardTypesPropertyDescriptor(AdaptedPropertyDescriptor pParentDescriptor, String pPropertyPath)
        : base(pParentDescriptor, pPropertyPath) {}
		
      /// <summary>
      /// Initializes a new instance of the CardTypesPropertyDescriptor class. 
      /// </summary>
      /// <param name="pComponentType">The type of object to which the property is bound</param>
      protected CardTypesPropertyDescriptor(Type pComponentType)
        : base(pComponentType) {}

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for CardTypeID.</summary>
      public AdaptedPropertyDescriptor CardTypeID {
        get { return Get("CardTypeID"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for CardType.</summary>
      public AdaptedPropertyDescriptor CardType {
        get { return Get("CardType"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Validation.</summary>
      public AdaptedPropertyDescriptor Validation {
        get { return Get("Validation"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for RowVersion.</summary>
      public AdaptedPropertyDescriptor RowVersion {
        get { return Get("RowVersion"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for ModifiedBy.</summary>
      public AdaptedPropertyDescriptor ModifiedBy {
        get { return Get("ModifiedBy"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for ModifiedDate.</summary>
      public AdaptedPropertyDescriptor ModifiedDate {
        get { return Get("ModifiedDate"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for TaxiTronicCardTypeID.</summary>
      public AdaptedPropertyDescriptor TaxiTronicCardTypeID {
        get { return Get("TaxiTronicCardTypeID"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for CustomerCardses.</summary>
      public AdaptedPropertyDescriptor CustomerCardses {
        get { return Get("CustomerCardses"); }
      }
						
      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Reservationses.</summary>
      public AdaptedPropertyDescriptor Reservationses {
        get { return Get("Reservationses"); }
      }
						
      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Dispatchs.</summary>
      public AdaptedPropertyDescriptor Dispatchs {
        get { return Get("Dispatchs"); }
      }
						
     internal new static CardTypesPropertyDescriptor Instance = new CardTypesPropertyDescriptor(typeof(CardTypes));
    }
  }
  #endregion
  
}
