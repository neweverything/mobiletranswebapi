﻿using CabRideEngineDapper.Utils;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CabRideEngineDapper {
	public partial class DriverPaymentRef {

		public static DriverPaymentRef Create(DriverPayments pDriverPayment) {
			DriverPaymentRef rec = null;
			try {
				rec = Create();
				rec.DriverPaymentID = pDriverPayment.DriverPaymentID;

				pDriverPayment.DriverPaymentRefs.Add(rec);
			} catch (Exception ex) {
				throw ex;
			}
			return rec;
		}

		// **********************************************************************************************************
		// Get or Create a record
		// **********************************************************************************************************
		public static DriverPaymentRef GetCreate(DriverPayments pDriverPayment, string pGroup, string pReference) {
			if (pDriverPayment.IsNewRecord) {
				pDriverPayment.Save();
			}
			DriverPaymentRef oDriverPaymentRef = GetByGroupAndReference(pDriverPayment.DriverPaymentID, pGroup, pReference);
			if (oDriverPaymentRef.IsNullEntity) {
				try {
					oDriverPaymentRef = Create(pDriverPayment);
					oDriverPaymentRef.ReferenceGroup = pGroup;
					oDriverPaymentRef.ReferenceKey = pReference;
				} catch (Exception ex) {
					throw ex;
				}
			}
			pDriverPayment.DriverPaymentRefs.Add(oDriverPaymentRef);
			return oDriverPaymentRef;
		}



		public static DriverPaymentRef GetByReference(long pPaymentID, string pReference) {
			string sql = @"SELECT * FROM DriverPaymentRef 
							WHERE ReferenceKey = @ReferenceKey
							AND DriverPaymentID = @DriverPaymentID";

			using (var conn = SqlHelper.OpenSqlConnection()) {
				return conn.Get<DriverPaymentRef>(sql, new { ReferenceKey = pReference, DriverPaymentID = pPaymentID });
			}
		}


		public static DriverPaymentRef GetByGroupAndReference(long pPaymentID, string pGroup, string pReference) {
			string sql = @"SELECT  *
                            FROM    DriverPaymentRef
                            WHERE   ReferenceGroup = @ReferenceGroup
                                    AND ReferenceKey = @ReferenceKey
                                    AND DriverPaymentID = @DriverPaymentID";

			using (var conn = SqlHelper.OpenSqlConnection()) {
				return conn.Get<DriverPaymentRef>(sql, new { ReferenceGroup = pGroup, ReferenceKey = pReference, DriverPaymentID = pPaymentID });
			}
		}

		public static List<DriverPaymentRef> GetByDriverPaymentID(long pPaymentID) {
			string sql = @"SELECT  *
                            FROM    DriverPaymentRef
                            WHERE   DriverPaymentID = @DriverPaymentID";

			using (var conn = SqlHelper.OpenSqlConnection()) {
				return conn.Query<DriverPaymentRef>(sql, new { DriverPaymentID = pPaymentID }).ToList();
			}
		}

		public static List<DriverPaymentRef> GetByGroupAndReferenceAndValue(string pGroup, string pReference, string pValue) {
			string sql = @"SELECT  *
                            FROM    DriverPaymentRef
                            WHERE   ReferenceGroup = @ReferenceGroup
                                    AND ReferenceKey = @ReferenceKey
                                    AND ValueString = @ValueString";

			using (var conn = SqlHelper.OpenSqlConnection()) {
				return conn.Query<DriverPaymentRef>(sql, new { ReferenceGroup = pGroup, ReferenceKey = pReference, ValueString = pValue }).ToList();
			}
		}

		public static List<DriverPaymentRef> GetByGroup(long pPaymentID, string pGroup) {
			string sql = @"SELECT *
							FROM DriverPaymentRef
							WHERE DriverPaymentID = @DriverPaymentID
								  AND ReferenceGroup = @ReferenceGroup";

			using (var conn = SqlHelper.OpenSqlConnection()) {
				return conn.Query<DriverPaymentRef>(sql, new { DriverPaymentID = pPaymentID, ReferenceGroup = pGroup }).ToList();
			}
		}

		public static decimal GetRefundedAmount(string pOriginalTrans) {

			string sql = @"SELECT  SUM(Fare + AirportFee + MiscFee + ISNULL(DropFee, 0) + ISNULL(BookingFee, 0) + WaitTime + Stops + Parking + Gratuity - Discount) DriverTotal
                            FROM    dbo.DriverPaymentRef
                            LEFT JOIN dbo.DriverPayments ON DriverPayments.DriverPaymentID = DriverPaymentRef.DriverPaymentID
                            WHERE   ReferenceGroup = 'Refund'
                                    AND ReferenceKey = 'Original TransNo'
                                    AND ValueString = @TransNo
                                    AND Failed = 0";

			using (var conn = SqlHelper.OpenSqlConnection()) {
				return conn.Get<decimal?>(sql, new { TransNo = pOriginalTrans }).GetValueOrDefault(0);
			}
		}

	}
}
