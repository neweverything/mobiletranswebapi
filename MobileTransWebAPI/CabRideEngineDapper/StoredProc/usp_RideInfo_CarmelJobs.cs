﻿using System;

namespace CabRideEngineDapper.StoredProc {
    public class usp_RideInfo_CarmelJobs {
        public string JobID { get; set; }
        public string CardApprovalCode { get; set; }
        public string CardNumber { get; set; }
        public string Salt { get; set; }
        public string CardType { get; set; }
        public string Expiration { get; set; }
        public string CardHolder { get; set; }
        public string TransNo { get; set; }
        public bool Failed { get; set; }
        public double Fare { get; set; }
        public double Parking { get; set; }
        public double Stops { get; set; }
        public double Gratuity { get; set; }
        public double Tolls { get; set; }
        public double WaitTime { get; set; }
        public double Discount { get; set; }
        public double MiscFee { get; set; }
        public double DriverTotal { get; set; }
        public DateTime SwipeDate { get; set; }
        public string ServiceType { get; set; }
        public string SendReceiptTo { get; set; }
        public string SwipeResponse { get; set; }
        public string AppVersion { get; set; }


    }
}
