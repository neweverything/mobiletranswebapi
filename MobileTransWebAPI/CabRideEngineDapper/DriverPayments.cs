﻿using CabRideEngineDapper.Enums;
using CabRideEngineDapper.StoredProc;
using CabRideEngineDapper.Utils;
using CabRideEngineDapperAudit;
using Dapper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using TaxiPassCommon;
using TaxiPassCommon.Banking;
using TaxiPassCommon.Cryptography;

namespace CabRideEngineDapper {
	public partial class DriverPayments {

		public static readonly string CHARGECARDS = "ChargeCards";
		public static readonly string RECURRING_FEES = "Recurring Fee";

		public bool BypassFraudCheck = false;

		public TimeStats MyTimeStats = new TimeStats();



		#region Driver Totals
		[JsonIgnore]
		[Editable(false)]
		public decimal TotalCharge {
			get {
				if (ChargeBack) {
					return -Math.Abs(TotalChargeWithOutCoupon - RideCouponAmount);
				}
				return TotalChargeWithOutCoupon - RideCouponAmount;
			}
		}

		[Editable(false)]
		public decimal TotalChargeWithOutCoupon {
			get {
				return Fare
					+ TaxiPassFee
					+ Gratuity
					+ AirportFee
					+ MiscFee
					+ WaitTime.GetValueOrDefault(0)
					+ Tolls.GetValueOrDefault(0)
					+ Parking.GetValueOrDefault(0)
					+ Stops.GetValueOrDefault(0)
					+ DropFee.GetValueOrDefault(0)
					+ BookingFee.GetValueOrDefault(0)
					- Discount.GetValueOrDefault(0);
			}
		}

		// ********************************************************************************
		// 5/28/2013 Add RideCouponAmount back into TotalCharge
		// ********************************************************************************
		[JsonIgnore]
		[Editable(false)]
		public decimal DriverTotal {
			get {
				return TotalChargeWithOutCoupon - TaxiPassFee;
			}
		}


		// ********************************************************************************
		// Driver Pays Fee amount
		// ********************************************************************************
		[JsonIgnore]
		[Editable(false)]
		public decimal DriverTotalWithDriverFee {
			get {
				return DriverTotal - this.DriverFee.GetValueOrDefault(0);
			}
		}

		// ***************************************************
		// Calc the drivertotal with kiosk amount (if no fare)
		// ***************************************************
		[JsonIgnore]
		[Editable(false)]
		public decimal DriverTotalWithKioskAmount {
			get {
				decimal dTrueFare;
				if (Fare != 0) {
					dTrueFare = Fare;
				} else {
					dTrueFare = this.DriverPaymentKioskAmount.Amount;
				}

				return dTrueFare
					+ Gratuity
					+ AirportFee
					+ MiscFee
					+ WaitTime.GetValueOrDefault(0)
					+ Tolls.GetValueOrDefault(0)
					+ Parking.GetValueOrDefault(0)
					+ Stops.GetValueOrDefault(0)
					+ DropFee.GetValueOrDefault(0)
					+ BookingFee.GetValueOrDefault(0)
					- Discount.GetValueOrDefault(0);
			}
		}

		// ************************************************************
		// 8/30/12 CA Ride Coupon Amount to be deducted from ChargeCard
		// ************************************************************
		private decimal? mRideCouponAmount = null;
		[Editable(false)]
		public decimal RideCouponAmount {
			get {
				if (!mRideCouponAmount.HasValue) {
					DriverPaymentRef rec = DriverPaymentRef.GetByReference(this.DriverPaymentID, "Coupon Amount");
					if (rec.IsNullEntity) {
						mRideCouponAmount = 0;
					} else {
						mRideCouponAmount = decimal.Parse(rec.ValueString);
					}
				}
				return mRideCouponAmount.Value;
			}
			set {
				mRideCouponAmount = value;
			}
		}


		// ********************************************************************************
		// 
		// ********************************************************************************
		[JsonIgnore]
		[Editable(false)]
		public decimal DriverTotalPaid {
			get {
				if (this.TaxiPassRedeemers.TaxiPassRedeemerAccounts.IncludeRedeemerRedemptionFeeInACH) {
					return DriverTotal - this.DriverFee.GetValueOrDefault(0);
				}
				return DriverTotal - this.RedemptionFee.GetValueOrDefault(0) - this.DriverFee.GetValueOrDefault(0);
			}
		}

		// ********************************************************************************
		// 
		// ********************************************************************************
		[JsonIgnore]
		[Editable(false)]
		public decimal RedeemerTotalPaid {
			get {
				decimal total = DriverTotal - this.DriverFee.GetValueOrDefault(0);
				if (this.TaxiPassRedeemers.TaxiPassRedeemerAccounts.IncludeRedeemerRedemptionFeeInACH) {
					total += RedemptionFee.GetValueOrDefault(0);
				}
				return total;
			}
		}
		#endregion

		#region Custom Methods
		/// <summary>
		/// Refunds the card.
		/// </summary>
		/// <param name="pDriverPaymentsReference">The reference no from the processor.</param>
		/// <returns></returns>
		public DriverPaymentResults RefundCard(string pDriverPaymentsReference, string pOriginalTransNo) {
			DateTime elapsedStart = DateTime.Now;
			CardProcessors oProcessor = CardProcessors.Verisign;

			if (!this.CreditCardProcessor.IsNullOrEmpty()) {
				oProcessor = this.CreditCardProcessor.ToEnum<CardProcessors>();
			}

			if (this.RowState == DataRowState.Added) {
				this.Save();
			}
			DriverPaymentRef payRef = DriverPaymentRef.GetCreate(this, "Refund", "Original TransNo");
			payRef.ValueString = pOriginalTransNo;
			payRef.Save();

			Gateway gway = null;
			ProcessorResponse response = null;
			//VerisignAccounts oAcct = this.Affiliate.MyVerisignAccount;
			//Gateway gway = oAcct.GetGateway(oProcessor, VerisignTrxTypes.Credit.ToString());
			using (var conn = SqlHelper.OpenSqlConnection()) {
				if (this.Test) {
					// simulate approved for test  trans
					response = new ProcessorResponse() {
						Amount = this.TotalCharge,
						AuthCode = StringUtils.GetUniqueKey(6, true),
						CreditCardProcessor = CardProcessors.TPCard,
						Message = "Approved",
						Reference = StringUtils.GenerateRandomHex(10),
						Result = "0",
						TestCard = true,
						TransDate = ChargeDate.GetValueOrDefault(),
						TransType = VerisignTrxTypes.Credit,
						VerisignAccountID = 0
					};
				} else {
					if (VerisignAccountID.HasValue && VerisignAccountID.Value > 0) {
						List<IGatewayCredentials> acctList = Utils.CreditCardUtils.LoadGateWays(conn, VerisignAccountID.Value, oProcessor, CardType);
						gway = new Gateway(acctList.FirstOrDefault(p => p.MyGateway() == oProcessor));
					} else {
						CreditCardChargeType chargeType = CreditCardChargeType.ECommerce;
						if (Platform != Platforms.GetRide.ToString()) {
							if (CardSwiped) {
								chargeType = CreditCardChargeType.Swipe;
							} else {
								chargeType = CreditCardChargeType.Manual;
							}
						}
						gway = Affiliate.GetGateway(oProcessor, chargeType, CardType);
					}
					response = gway.Refund(new CCRequestRefNo(pDriverPaymentsReference, Math.Round(this.TotalCharge, 2), this.Test, string.Format("AD:{0} D:{1}", AffiliateDriverID, DriverID), TransNo, IsGetRideTrans, this.CardNumberDisplay, TaxiPassFee, DriverFee.GetValueOrDefault(0)));
				}
			}
			var refundResult = ProcessResponse(response, VerisignTrxTypes.Credit, this.TotalCharge, null);

			if (this.Affiliate.DoNotPay && !Failed) {
				DoNotPay = true;
				DriverPaid = false;
				DriverPaidDate = null;
				DriverPaidTransNo = null;
				DriverPaymentAux aux = this.DriverPaymentAux;
				if (aux.IsNullEntity) {
					aux = DriverPaymentAux.Create(this);
				}
				aux.DoNotPay = true;
				aux.DoNotPayReasonID = DoNotPayReason.GetCreateReason("Software Licensing").DoNotPayReasonID;
				aux.Save();
				this.Save();
			}

			var timeZoneOffest = DriverPaymentRef.GetCreate(this, "TimeZoneOffset", "Minutes");
			timeZoneOffest.ValueString = DateTime.UtcNow.Subtract(this.Affiliate.LocalTime).TotalMinutes.ToString("F0");
			timeZoneOffest.Save();

			double elapsed = DateTime.Now.Subtract(elapsedStart).TotalMilliseconds / 1000;
			var dpElapsedRef = DriverPaymentRef.GetCreate(this, "ElapsedTime", "Refund");
			dpElapsedRef.ValueString = elapsed.ToString();
			dpElapsedRef.Save();

			return refundResult;
		}


		public string DecryptCardExpiration() {
			Encryption oEncrypt = new Encryption();
			if (base.Expiration.IsNullOrEmpty()) {
				return "";
			}
			return oEncrypt.TripleDESDecrypt(base.Expiration, this.Salt);
		}

		// ********************************************************************************
		// 
		// ********************************************************************************
		public void CalcFees() {

			if (this.Affiliate.AffiliateDriverDefaults.DebitFeeProcessing.GetValueOrDefault(false) && !this.CardType.Equals("eCheck")) {
				// check if card is Debit
				using (var conn = SqlHelper.OpenSqlConnection()) {
					string binCardNo = this.DecryptCardNumber();
					//CardBin bin = CardBin.GetCardBinInfo(conn, binCardNo);
					IsDebitCard = binCardNo.IsDebitCard(); // ("" + bin.DebitCredit).Equals("DEBIT", StringComparison.CurrentCultureIgnoreCase);
				}
			}

			TaxiPassFee = CalcFee(this.Drivers.MyAffiliate);
			DriverFee = CalcDriverFee(this.Drivers.MyAffiliate);
		}


		private decimal CalcFee(Affiliate pAffiliate) {
			decimal nTotal = Math.Abs(DriverTotal);
			if (this.Affiliate.AffiliateDriverDefaults.TPFeeOnFareOnly) {
				nTotal = Math.Abs(this.Fare);
			}
			if (pAffiliate.AffiliateDriverDefaults.DebitFeeProcessing.GetValueOrDefault(false) && IsDebitCard) {
				return CalcFeeForDebitCards(this.Affiliate, nTotal);
			}
			if (pAffiliate.AffiliateDriverDefaults.CheckFeeProcessing.GetValueOrDefault(false) && ("" + this.CardType).Equals("eCheck", StringComparison.CurrentCultureIgnoreCase)) {
				return CalcFeeForChecks(this.Affiliate, nTotal);
			}

			return CalcFee(this.Affiliate, nTotal);
		}

		public static decimal CalcFeeForChecks(Affiliate pAffiliate, decimal pTotal) {
			decimal feeAmt = 0;
			bool isPct = false;

			List<AffiliateFees> affiliateFees = pAffiliate.AffiliateFeeses;

			var myFees = affiliateFees.Where(p => !p.BinType.IsNullOrEmpty() && p.BinType.Equals("eCheck", StringComparison.CurrentCultureIgnoreCase)).OrderBy(p => p.StartAmount).ToList();

			foreach (AffiliateFees fee in myFees) {
				if (Math.Abs(pTotal) >= fee.StartAmount && fee.EndAmount == 0) {
					feeAmt = fee.Fee;
					isPct = fee.IsPercent;
					break;
				} else if (Math.Abs(pTotal).IsBetween(fee.StartAmount, fee.EndAmount)) {
					feeAmt = fee.Fee;
					isPct = fee.IsPercent;
					break;
				}
			}
			FeeDesc = $"{feeAmt.ToString("F2")}{(isPct ? "%" : "")}";
			if (pTotal == 0) {
				return 0;
			}

			if (isPct) {
				feeAmt = Math.Round(pTotal * (feeAmt / 100), 2, MidpointRounding.AwayFromZero);
			}

			return feeAmt;
		}


		[Editable(false)]
		public static string FeeDesc { get; set; }


		public static decimal CalcFee(Affiliate pAffiliate, decimal pTotal) {
			return CalcFee(pAffiliate.AffiliateFeeses.ToList(), pTotal, pAffiliate.AffiliateDriverDefaults);
		}

		public static decimal CalcFee(List<AffiliateFees> pAffiliateFees, decimal pTotal, AffiliateDriverDefaults pDefaults) {


			decimal feeAmt = 0;
			bool isPct = false;

			var myFees = pAffiliateFees.Where(p => p.BinType.IsNullOrEmpty() || p.BinType.Equals("Credit")).OrderBy(p => p.StartAmount).ToList();

			foreach (AffiliateFees fee in myFees) {
				if (Math.Abs(pTotal) >= fee.StartAmount && fee.EndAmount == 0) {
					feeAmt = fee.Fee;
					isPct = fee.IsPercent;
					break;
				} else if (Math.Abs(pTotal).IsBetween(fee.StartAmount, fee.EndAmount)) {
					feeAmt = fee.Fee;
					isPct = fee.IsPercent;
					break;
				}
			}
			if (pTotal == 0) {
				return 0;
			}
			if (feeAmt == 0) {
				feeAmt = pAffiliateFees.Count == 0 ? 0 : pAffiliateFees.Max(p => p.Fee);
			}
			FeeDesc = $"{feeAmt.ToString("F2")}{(isPct ? "%" : "")}";
			if (isPct) {
				feeAmt = Math.Round(pTotal * (feeAmt / 100), 2, MidpointRounding.AwayFromZero);
			}

			decimal percFee = pDefaults.AdditionalTPPercentFee.GetValueOrDefault(0);
			percFee = Math.Round(pTotal * (percFee / 100), 2, MidpointRounding.AwayFromZero);
			feeAmt += percFee;

			return feeAmt;
		}

		public static decimal CalcFeeForDebitCards(Affiliate pAffiliate, decimal pTotal) {
			decimal feeAmt = 0;
			bool isPct = false;

			List<AffiliateFees> affiliateFees = pAffiliate.AffiliateFeeses;

			List<AffiliateFees> myFees = affiliateFees.Where(p => !p.BinType.IsNullOrEmpty() && p.BinType.Equals("Debit")).OrderBy(p => p.StartAmount).ToList();
			if (myFees.Count == 0) {
				myFees = affiliateFees.Where(p => !p.BinType.IsNullOrEmpty() && p.BinType.Equals("Credit")).OrderBy(p => p.StartAmount).ToList();
			}
			foreach (AffiliateFees fee in myFees) {
				if (Math.Abs(pTotal) >= fee.StartAmount && fee.EndAmount == 0) {
					feeAmt = fee.Fee;
					isPct = fee.IsPercent;
					break;
				} else if (Math.Abs(pTotal).IsBetween(fee.StartAmount, fee.EndAmount)) {
					feeAmt = fee.Fee;
					isPct = fee.IsPercent;
					break;
				}
			}
			FeeDesc = $"{feeAmt.ToString("F2")}{(isPct ? "%" : "")}";
			if (pTotal == 0) {
				return 0;
			}
			//if (feeAmt == 0) {
			//	feeAmt = affiliateFees.Count == 0 ? 0 : affiliateFees.Max(p => p.Fee);
			//}
			if (isPct) {
				feeAmt = Math.Round(pTotal * (feeAmt / 100), 2, MidpointRounding.AwayFromZero);
			}

			// not needed per Jason
			//AffiliateDriverDefaults defaults = pAffiliate.AffiliateDriverDefaults;
			//decimal percFee = defaults.AdditionalTPPercentFee.GetValueOrDefault(0);
			//percFee = Math.Round(pTotal * (percFee / 100), 2, MidpointRounding.AwayFromZero);
			//feeAmt += percFee;

			return feeAmt;
		}

		// ********************************************************************************
		// Do not recalculate the driverfee if there is a kiosk amount
		// ********************************************************************************
		public decimal CalcDriverFee(Affiliate pAffiliate) {


			if (!this.CreditCardProcessor.IsNullOrEmpty() && this.CreditCardProcessor.StartsWith("DriverProm", StringComparison.CurrentCultureIgnoreCase)) {
				return 0;
			}

			if (this.DriverPaymentKioskAmount.IsNullEntity) {
				decimal nTotal = Math.Abs(DriverTotal);
				if (IsDebitCard) {
					return CalcDriverFeeForDebitCards(pAffiliate, nTotal, CardSwiped, SwipeFailed, Platform);
				}
				return CalcDriverFee(pAffiliate, nTotal, CardSwiped, SwipeFailed, Platform);
			}

			decimal addFee = 0;
			if (!CardSwiped && !SwipeFailed) {
				if (!("" + Platform).Equals(Platforms.Dejavoo.ToString(), StringComparison.CurrentCultureIgnoreCase)) {
					addFee += pAffiliate.ManualTransFee;
				}
			}

			return addFee;
		}


		// ********************************************************************************
		// 
		// ********************************************************************************
		public static decimal CalcDriverFee(Affiliate pAffiliate, decimal pTotal, bool pCardSwiped, bool pSwipeFailed, string pPlatform) {

			var myFees = pAffiliate.DriverFees.Where(p => p.BinType.IsNullOrEmpty() || p.BinType.Equals("Credit")).OrderBy(p => p.StartAmount).ToList();

			decimal addFee = 0;
			if (pAffiliate.AffiliateDriverDefaults.AdditionalDriverPercentFee.GetValueOrDefault(0) > 0) {
				decimal perc = (pAffiliate.AffiliateDriverDefaults.AdditionalDriverPercentFee.GetValueOrDefault(0) / 100);
				addFee = Math.Round(pTotal * perc, 2, MidpointRounding.AwayFromZero);
			}

			if (!pCardSwiped && !pSwipeFailed) {
				if (!("" + pPlatform).Equals(Platforms.Dejavoo.ToString(), StringComparison.CurrentCultureIgnoreCase)) {
					addFee += pAffiliate.ManualTransFee;
				}
			}

			foreach (DriverFee fee in myFees) {
				if (IsBetween(fee.StartAmount, fee.EndAmount, Math.Abs(pTotal))) {
					if (fee.PercentFee) {
						decimal perc = (fee.Fee / 100);
						decimal total = Math.Round(pTotal * perc, 2, MidpointRounding.AwayFromZero);
						return total + addFee;
					} else {
						return fee.Fee + addFee;
					}
				}
			}
			return addFee;
		}

		// ********************************************************************************
		// 
		// ********************************************************************************
		public static decimal CalcDriverFeeForDebitCards(Affiliate pAffiliate, decimal pTotal, bool pCardSwiped, bool pSwipeFailed, string pPlatform) {

			var myFees = pAffiliate.DriverFees.Where(p => !p.BinType.IsNullOrEmpty() && p.BinType.Equals("Debit")).OrderBy(p => p.StartAmount).ToList();

			decimal addFee = 0;
			if (pAffiliate.AffiliateDriverDefaults.AdditionalDriverPercentFee.GetValueOrDefault(0) > 0) {
				decimal perc = (pAffiliate.AffiliateDriverDefaults.AdditionalDriverPercentFee.GetValueOrDefault(0) / 100);
				addFee = Math.Round(pTotal * perc, 2, MidpointRounding.AwayFromZero);
			}

			if (!pCardSwiped && !pSwipeFailed) {
				if (!("" + pPlatform).Equals(Platforms.Dejavoo.ToString(), StringComparison.CurrentCultureIgnoreCase)) {
					addFee += pAffiliate.ManualTransFee;
				}
			}

			foreach (DriverFee fee in myFees) {
				if (IsBetween(fee.StartAmount, fee.EndAmount, Math.Abs(pTotal))) {
					if (fee.PercentFee) {
						decimal perc = (fee.Fee / 100);
						decimal total = Math.Round(pTotal * perc, 2, MidpointRounding.AwayFromZero);
						return total + addFee;
					} else {
						return fee.Fee + addFee;
					}
				}
			}
			return addFee;
		}

		// ********************************************************************************
		// 3/26/12 CA Overload of default CalcDriverFee
		// Used to calculate fees on Chargeback vouchers for Carmel
		// ********************************************************************************
		public static decimal CalcDriverFee(Affiliate pAffiliate, decimal pTotal, bool pUseAbsoluteValue, bool pCardSwiped, bool pSwipeFailed, string pPlatform) {
			if (pUseAbsoluteValue) {
				pTotal = Math.Abs(pTotal);
			}
			return CalcDriverFee(pAffiliate, pTotal, pCardSwiped, pSwipeFailed, pPlatform);
		}


		public static string CardNumberToHash(string pCardNumber) {
			return CryptoFns.MD5HashUTF16ToString(pCardNumber);
		}


		public string DecryptCardNumber() {
			if (CardNumber.IsNullOrEmpty()) {
				return "";
			}
			try {
				Encryption oEncrypt = new Encryption();
				return oEncrypt.TripleDESDecrypt(CardNumber, Salt);
			} catch (Exception) {
			}
			return CardNumber;
		}


		public decimal ProcessRedemptionFee(TaxiPassRedeemerAccounts pRedeemerAccount) {
			if (!this.CreditCardProcessor.IsNullOrEmpty()) {
				if (!this.CreditCardProcessor.Equals("DriverProm")) {
					RedeemerFees recFee = RedeemerFees.GetRedemeerFee(pRedeemerAccount, this.Platform);
					if (recFee.Percent) {
						this.RedemptionFee = Math.Round(this.DriverTotal * (recFee.Fee / 100), 2, MidpointRounding.AwayFromZero);
					} else {
						this.RedemptionFee = recFee.Fee;
					}
				}
			}
			return this.RedemptionFee.GetValueOrDefault(0);
		}

		// **********************************************************************************************
		// 4/13/2010 - Make sure we are dealing with a 6 char (not a 12) voucher no
		// **********************************************************************************************
		public static string DetermineRealVoucherNo(string pTransNo) {
			if (pTransNo.IsNumeric() & pTransNo.ToString().Length > 6) {
				string temp = pTransNo;
				if (temp.Length == 12 && Convert.ToInt32(temp.Left(2)) >= 48) {
					temp = pTransNo.FromBarCode();
				}
				if (temp.Length == 6) {
					pTransNo = temp;
				}
			}
			if (pTransNo.Contains("_")) {
				pTransNo = pTransNo.Split('_')[3];
			}

			return pTransNo;
		}




		// ********************************************************************************
		// 
		// ********************************************************************************
		private static bool IsBetween(decimal pStart, decimal pEnd, decimal pValue) {
			if (pEnd == 0) {
				if (pValue >= pStart) {
					return true;
				}
			} else {
				if (pValue >= pStart && pValue <= pEnd) {
					return true;
				}
			}
			return false;
		}


		/// <summary>
		/// Convert Payment to TTech transaction object.
		/// </summary>
		/// <returns></returns>
		public TTechTransaction PaymentToTTechTransaction() {
			return PaymentToTTechTransaction(0);
		}


		/// <summary>
		/// Convert Payment to TTech transaction object.
		/// </summary>
		/// <param name="pLaneID">The lane ID.</param>
		/// <returns></returns>
		public TTechTransaction PaymentToTTechTransaction(short pLaneID) {
			return PaymentToTTechTransaction(pLaneID, false);
		}

		[Editable(false)]
		public bool TTechDriverFeeChargeBack { get; set; }

		/// <summary>
		/// Convert Payment to TTech transaction object.
		/// </summary>
		/// <param name="pLaneID">The p lane ID.</param>
		/// <param name="pBofA">if set to <c>true</c> [p bof A].</param>
		/// <returns></returns>
		public TTechTransaction PaymentToTTechTransaction(short pLaneID, bool pBofA) {
			DriverPayments oPayment = this;
			SystemDefaults oDefault = SystemDefaults.GetDefaults();
			TTechTransaction oTTech = new TTechTransaction();
			PayInfo oPayInfo = null;

			bool payDriver = !oPayment.TaxiPassRedeemerID.HasValue
							|| oPayment.TaxiPassRedeemerID.Value < 1
							|| oPayment.TaxiPassRedeemers.TaxiPassRedeemerAccounts.PayDriverPayCard && !oPayment.DriverPaidCash.PaidInCash;

			//check if redeemer has payment info, if not then pay driver
			if (!payDriver) {
				if (oPayment.TaxiPassRedeemerID.HasValue) {
					TaxiPassRedeemerAccounts acct = oPayment.TaxiPassRedeemers.TaxiPassRedeemerAccounts;
					if (acct.BofAAccountInfo.IsNullOrEmpty() && acct.BankAccountNumber.IsNullOrEmpty() && acct.TransCardNumber.IsNullOrEmpty()) {
						payDriver = true;
					}
				}
			}
			if (payDriver) {
				if (oPayment.AffiliateDriverID > 0) {
					oPayInfo = oPayment.AffiliateDrivers.GetPayInfo();
				}
				if (oPayInfo == null) {
					if (oPayment.Drivers.DriverBankInfos.Count > 0 && CheckAccount(oPayment.Drivers.DriverBankInfos[0])) {
						oPayInfo = oPayment.Drivers.DriverBankInfos[0].GetPayInfo();
					}
				}
			}
			if (oPayInfo == null) {
				//do we pay redeemer or affiliate?
				if (!pBofA) {
					if (oPayment.TaxiPassRedeemerID.HasValue && oPayment.TaxiPassRedeemerID.Value > 0) {
						if (!oPayment.DriverPaymentsPendingMatches.IsNullEntity && oPayment.DriverPaymentsPendingMatches.TaxiPassRedeemerID != 0) {
							oPayInfo = oPayment.DriverPaymentsPendingMatches.TaxiPassRedeemers.TaxiPassRedeemerAccounts.GetPayInfo();
						} else {
							oPayInfo = oPayment.TaxiPassRedeemers.TaxiPassRedeemerAccounts.GetPayInfo();
						}
					} else {
						if (!oPayment.AffiliateID.HasValue) {
							oPayment.AffiliateID = oPayment.Drivers.AffiliateID;
						}
						if (!oPayment.Affiliate.AffiliateBankInfo.IsNullEntity) {
							oPayInfo = oPayment.Affiliate.AffiliateBankInfo.GetPayInfo();
						}
					}
				}
			}

			if ((!pBofA && oPayInfo == null) || !oPayment.DriverPaid) {
				return null;
			}


			if (!payDriver && oPayment.TaxiPassRedeemerID.HasValue && oPayment.TaxiPassRedeemerID.Value > 0 &&
					(!oPayment.TaxiPassRedeemers.TaxiPassRedeemerAccounts.PayDriverPayCard || !oPayment.TaxiPassRedeemers.TaxiPassRedeemerAccounts.BofAAccountInfo.IsNullOrEmpty())) {
				if (oPayment.DriverPaymentsPendingMatches.IsNullEntity) {
					oTTech.Affiliate = "Redeemer: " + oPayment.TaxiPassRedeemers.TaxiPassRedeemerAccounts.CompanyName;
				} else {
					oTTech.Affiliate = "Redeemer: " + oPayment.DriverPaymentsPendingMatches.TaxiPassRedeemers.TaxiPassRedeemerAccounts.CompanyName;
				}
			} else {
				oTTech.Affiliate = oPayment.Drivers.Affiliate.Name;
			}

			//oTTech.NachaFile = oPay.NachaFile;
			oTTech.Site = oDefault.TTechSite;
			oTTech.Action = TTechAction.Normal;

			oTTech.TranID = oPayment.DriverPaymentID;

			if (oPayment.ChargeBack || TTechDriverFeeChargeBack) {
				oTTech.LaneID = pLaneID++;
			} else {
				oTTech.LaneID = pLaneID;  // oPay.MyMerchant.LaneID;
			}
			if (oTTech.Affiliate.StartsWith("Redeemer:")) {
				oTTech.Amount = oPayment.RedeemerTotalPaid;
			} else {

				// 3/7/12 CA ACHDriverFeeAsDebit is for Carmel 
				// TTechDriverFeeChargeBack is set in CabRideAutoACH after the transactions are processed
				if (oPayment.Affiliate.AffiliateDriverDefaults.ACHDriverFeeAsDebit) {
					if (TTechDriverFeeChargeBack) {
						oTTech.Amount = oPayment.DriverFee.GetValueOrDefault(0);
					} else {
						// Take whatever is set in the DB.  Carmel rolls their chargebacks into the "Credit" line
						oTTech.Amount = oPayment.DriverTotal;
					}
				} else {
					//oTTech.Amount = oPayment.DriverTotalPaid;
					oTTech.Amount = Math.Abs(oPayment.DriverTotalPaid);
				}
			}

			if (oPayment.DriverPaidDate.HasValue) {
				oTTech.TranDate = oPayment.DriverPaidDate.Value;
			} else {
				oTTech.TranDate = DateTime.Today;
			}
			oTTech.CheckNumber = oTTech.TranID;  //?
			if (TTechDriverFeeChargeBack) {
				oTTech.CheckNumber = Convert.ToInt64("999" + Math.Abs(oTTech.CheckNumber).ToString());
			}
			if (!pBofA) {
				oTTech.PaidTo = oPayInfo.PaidTo;
				oTTech.CustomerName = oPayInfo.BankAccountHolder;
				oTTech.Address = oPayInfo.BankAddress;
				oTTech.Suite = oPayInfo.BankSuite;
				oTTech.City = oPayInfo.BankCity;
				oTTech.State = oPayInfo.BankState;
				oTTech.ZIPCode = oPayInfo.BankZIPCode;
				oTTech.Phone1 = oPayment.AffiliateDrivers.Cell.IsNullOrEmpty() ? oPayment.Drivers.Cell : oPayment.AffiliateDrivers.Cell;
				oTTech.Company = oPayInfo.BankAccountPersonal ? "" : oPayment.Drivers.Affiliate.Name;
				oTTech.CheckType = oPayInfo.BankAccountPersonal ? "PERSONAL" : "BUSINESS";
				oTTech.ABANumber = oPayInfo.BankCode;
				oTTech.Account = oPayInfo.BankAccountNumber;
				oTTech.ClassCode = oPayInfo.BankAccountPersonal ? TaxiPassCommon.Banking.NachaGenerator.StandardEntryTypes.PPD.ToString() :
															  TaxiPassCommon.Banking.NachaGenerator.StandardEntryTypes.CCD.ToString();
			}

			if ((oPayment.ChargeBack && !oPayment.Affiliate.AffiliateDriverDefaults.ACHDriverFeeAsDebit) || TTechDriverFeeChargeBack) {
				oTTech.TranType = "DEBIT";
				oTTech.Application = "DDA";
				oTTech.PaymentType = "S";
			} else {
				oTTech.TranType = "CREDIT";
				oTTech.Application = "DDA";
				oTTech.PaymentType = "S";
				if (oPayInfo != null) {
					oTTech.WireTransfer = oPayInfo.WireTransfer;
				}
			}
			oTTech.TranReference = oPayment.DriverPaymentID.ToString();

			return oTTech;
		}

		private bool CheckAccount(DriverBankInfo pBankInfo) {
			if (string.IsNullOrEmpty(pBankInfo.MerchantInfo.MerchantNo)) {
				return false;
			}
			if (string.IsNullOrEmpty(pBankInfo.BankAccountNumber)) {
				return false;
			}
			if (string.IsNullOrEmpty(pBankInfo.BankCode)) {
				return false;
			}
			return true;
		}

		#endregion


		public static DriverPayments Create(Drivers pDriver, string pOriginalTransNo = "") {
			DriverPayments oPayment = DriverPaymentsTable.Create();
			try {

				oPayment.Salt = StringUtils.CreateSalt(6);
				oPayment.DriverID = pDriver.DriverID;
				oPayment.AffiliateID = pDriver.AffiliateID;
				oPayment.RequestAppTime = pDriver.Affiliate.LocalTime;

				oPayment.TransNo = StringUtils.GetUniqueKey(6, false);
				oPayment.IgnoreCardZIP = false;
				oPayment.RedemptionFee = 0;
				oPayment.Stops = 0;
				oPayment.WaitTime = 0;
				oPayment.Tolls = 0;
				oPayment.Parking = 0;
				oPayment.DriverFee = 0;
				oPayment.Discount = 0;
				oPayment.CardNumber = "";
				oPayment.Number = "";
				oPayment.Expiration = "";

				oPayment.Failed = true;
				oPayment.OriginalTransNo = pOriginalTransNo;
			} catch (Exception ex) {
				throw ex;
			}
			return oPayment;
		}

		public DriverPayments CreateAndRefund(decimal pRefundAmount, string pNote, bool pMarkDoNotPay, Platforms pPlatform) {
			DriverPayments refundRec = DriverPayments.Create(this.Drivers);
			refundRec.OriginalTransNo = this.TransNo;
			refundRec.SetFareNoTaxiPassFee = -pRefundAmount;
			//need to calc TPFee 
			decimal nTotal = this.DriverTotal + refundRec.Fare;
			decimal nTPFee = DriverPayments.CalcFee(this.Affiliate, nTotal);
			if (this.TaxiPassFee == 0) {
				refundRec.TaxiPassFee = 0;
			} else if (this.DriverTotal == Math.Abs(refundRec.DriverTotal)) {
				refundRec.TaxiPassFee = -this.TaxiPassFee;
			} else {
				refundRec.TaxiPassFee = -(this.TaxiPassFee - nTPFee);
			}
			refundRec.CardNumber = this.DecryptCardNumber();
			refundRec.Expiration = this.DecryptCardExpiration();
			refundRec.AffiliateDriverID = this.AffiliateDriverID;
			refundRec.AffiliateID = this.AffiliateID;
			refundRec.CardSwiped = false;
			if (refundRec.DriverPaymentNotes.IsNullEntity) {
				DriverPaymentNotes.Create(refundRec);
			}
			pNote += string.Format("\nRefund of Original Voucher No: {0}", this.TransNo);
			refundRec.Notes = pNote.Replace("<br />", "\n");
			refundRec.VehicleID = this.VehicleID;
			refundRec.ChargeBack = true;
			refundRec.ChargeBackInfo = refundRec.DriverPaymentNotes.Reason;
			if (refundRec.ChargeBackInfo.Length > 200) {
				refundRec.ChargeBackInfo = refundRec.ChargeBackInfo.Left(200);
			}
			refundRec.CorporateName = this.CorporateName;
			refundRec.ReferenceNo = this.ReferenceNo;

			//TaxiPassCommon.TimeZoneConverter tzConvert = new TaxiPassCommon.TimeZoneConverter();
			refundRec.ChargeDate = Affiliate.LocalTime;
			refundRec.DriverPaid = this.DriverPaid;
			refundRec.DriverPaidDate = this.DriverPaidDate;
			refundRec.DoNotPay = pMarkDoNotPay;
			refundRec.CreditCardProcessor = this.CreditCardProcessor;
			refundRec.Platform = pPlatform.ToString();
			refundRec.VerisignAccountID = this.VerisignAccountID;

			DriverPaymentResults oResult = null;
			try {
				oResult = refundRec.RefundCard(this.DriverPaymentChargeResults.Reference, this.TransNo);
				refundRec.DriverPaymentRefundResult = oResult;
			} catch (Exception ex) {
				refundRec.Notes += ex.Message;
				refundRec.Save();
				throw ex;
			}
			if (oResult.Result == "0") {
				this.DoNotPay = pMarkDoNotPay;
				this.Notes = "See Refund: Trans No: " + refundRec.TransNo + " Date: " + refundRec.ChargeDate.Value.ToString("MM/dd/yyyy") + "\n\n" + this.DriverPaymentNotes.Reason;
				try {
					this.Save();
				} catch {
				}
			}
			return refundRec;
		}


		#region Get Records
		public static DriverPayments GetPayment(SqlConnection pConn, string pTransNo) {
			pTransNo = DetermineRealVoucherNo(pTransNo);

			string sql = @"SELECT  DriverPayments.*
							FROM    dbo.DriverPayments
							WHERE   TransNo = @TransNo";

			DriverPayments rec = pConn.Get<DriverPayments>(sql, new { TransNo = pTransNo });
			if (rec.IsNullEntity) {

				sql = @"SELECT  DriverPayments.*
						FROM    dbo.DriverPayments 
						WHERE DriverPaymentID IN (SELECT   DriverPaymentID
														   FROM     DriverPaymentAux
														   WHERE    VTSRowID = @TransNo)";
				rec = pConn.Get<DriverPayments>(sql, new { TransNo = pTransNo });
			}
			return rec;
		}


		//public static DriverPayments GetPaymentByExternalVoucherID(SqlConnection pConn, string pTransNo) {
		//    //pTransNo = DetermineRealVoucherNo(pTransNo);

		//    string sql = @"SELECT  *
		//                    FROM    dbo.DriverPayments
		//                    WHERE   TransNo = @TransNo
		//                            OR DriverPaymentID IN (SELECT   DriverPaymentID
		//                                                   FROM     DriverPaymentAux
		//                                                   WHERE    VTSRowID = TransNo)";
		//    return pConn.Get<DriverPayments>(sql, new { TransNo = pTransNo });
		//}

		public static DriverPayments GetPayment(SqlConnection pConn, string pTransNo, decimal pAmount, bool pIgnoreAmount, bool pCheckWithDriverFee) {
			DriverPayments oPay = GetPayment(pConn, pTransNo);

			if (!oPay.IsNullEntity) {
				if (!pIgnoreAmount) {
					if (pCheckWithDriverFee) {
						if (oPay.Fare > 0 && oPay.DriverTotalWithDriverFee != pAmount) {
							return DriverPayments.GetNullEntity();
						}
					} else {
						if (oPay.Fare > 0 && oPay.DriverTotal != pAmount && oPay.DriverTotalWithDriverFee != pAmount && oPay.TotalCharge != pAmount) {
							return DriverPayments.GetNullEntity();
						}
					}
				}
			}
			return oPay;
		}

		public static DriverPayments GetPayment(SqlConnection pConn, string pTransNo, string pCellPhone) {

			DriverPayments oPay = GetPayment(pConn, pTransNo);
			if (!oPay.IsNullEntity) {
				if (oPay.AffiliateDrivers.Cell != pCellPhone && oPay.AffiliateDrivers.CallInCell != pCellPhone) {
					return DriverPayments.GetNullEntity();
				}
			}
			return oPay;
		}


		public static DriverPayments GetPayment(SqlConnection pConn, long pDriverPaymentID) {
			return pConn.Get<DriverPayments>(pDriverPaymentID);
		}

		public static List<DriverPayments> GetPaymentByCellPhone(SqlConnection pConn, string pCellPhone, decimal pAmount) {
			return GetPaymentByCellPhone(pConn, pCellPhone, pAmount, false);
		}


		public static List<DriverPayments> GetPayments(Drivers pDriver, DateTime pStart, DateTime pEnd, DriverPaymentStatus pStatus) {
			string sql = @"SELECT  *
							FROM    DriverPayments
							WHERE   ChargeDate >= @Start
									AND ChargeDate <= @End
									AND DriverID = @DriverID";
			switch (pStatus) {
				case DriverPaymentStatus.paid:
					sql += " AND DriverPaid = 1";
					break;
				case DriverPaymentStatus.unpaid:
					sql += " AND DriverPaid = 0";
					break;
			}
			using (var conn = SqlHelper.OpenSqlConnection()) {
				return conn.Query<DriverPayments>(sql, new { Start = pStart, End = pEnd, DriverID = pDriver.DriverID }).ToList();
			}
		}

		public static List<DriverPayments> GetPayments(AffiliateDrivers pAffDriver, DateTime pStart, DateTime pEnd, DriverPaymentStatus pStatus) {
			string sql = @"SELECT  *
							FROM    DriverPayments
							WHERE   ChargeDate >= @Start
									AND ChargeDate <= @End
									AND AffiliateDriverID = @AffiliateDriverID";
			switch (pStatus) {
				case DriverPaymentStatus.paid:
					sql += " AND DriverPaid = 1";
					break;
				case DriverPaymentStatus.unpaid:
					sql += " AND DriverPaid = 0";
					break;
			}
			using (var conn = SqlHelper.OpenSqlConnection()) {
				return conn.Query<DriverPayments>(sql, new { Start = pStart, End = pEnd, AffiliateDriverID = pAffDriver.AffiliateDriverID }).ToList();
			}

		}


		// **********************************************************************************************
		//
		// **********************************************************************************************
		public static List<DriverPayments> GetPayments(TaxiPassRedeemers pRedeemer, DateTime pStart, DateTime pEnd) {
			string sql = @"SELECT * FROM DriverPayments WHERE DriverPaidDate >= @Start
							AND DriverPaidDate <= @End
							AND DriverPaid = 1
							{0}
							ORDER BY DriverPaidDate";
			DynamicParameters parms = new DynamicParameters();
			parms.Add("Start", pStart);
			parms.Add("End", pEnd);

			List<long> redeemers = new List<long>();
			if (pRedeemer.Admin) {
				redeemers = (from o in pRedeemer.TaxiPassRedeemerAccounts.TaxiPassRedeemerses
							 select o.TaxiPassRedeemerID).ToList();
				sql = string.Format(sql, "AND TaxiPassRedeemerID IN @Redeemers");
				parms.Add("Redeemers", redeemers);
			} else {
				sql = string.Format(sql, "AND TaxiPassRedeemerID = @TaxiPassRedeemerID");
				parms.Add("TaxiPassRedeemerID", pRedeemer.TaxiPassRedeemerID);
			}

			using (var conn = SqlHelper.OpenSqlConnection()) {
				List<DriverPayments> oList = conn.Query<DriverPayments>(sql, parms).ToList();


				sql = @"SELECT  DriverPayments.*
						FROM    DriverPayments WITH (NOLOCK)
						LEFT JOIN DriverPaymentsPendingMatches ON DriverPaymentsPendingMatches.DriverPaymentID = DriverPayments.DriverPaymentID
						WHERE DriverPayments.DriverPaidDate >= @Start
							  AND DriverPayments.DriverPaidDate <= @End
							  {0}
						ORDER BY DriverPayments.DriverPaidDate";

				parms = new DynamicParameters();
				parms.Add("Start", pStart);
				parms.Add("End", pEnd);
				if (redeemers.Count == 0) {
					sql = string.Format(sql, "AND DriverPaymentsPendingMatches.TaxiPassRedeemerID = @TaxiPassRedeemerID");
					parms.Add("TaxiPassRedeemerID", pRedeemer.TaxiPassRedeemerID);

				} else {
					sql = string.Format(sql, "AND DriverPaymentsPendingMatches.TaxiPassRedeemerID IN (@Redeemers)");
					parms.Add("Redeemers", redeemers);
				}


				List<DriverPayments> matchList = conn.Query<DriverPayments>(sql, parms).ToList();
				oList.AddRange(matchList);

				oList = (from p in oList
						 orderby p.DriverPaidDate
						 select p).ToList();

				return oList;
			}
		}

		// **********************************************************************************************
		// return a list of payment recs by Trans No's
		// **********************************************************************************************
		public static List<DriverPayments> GetPayments(SqlConnection pConn, List<string> pTransNumbers) {
			string sql = "SELECT * FROM DriverPayments WHERE TransNo IN (@TransNumbers)";
			return pConn.Query<DriverPayments>(sql, new { TransNumbers = pTransNumbers }).ToList();
		}

		public static List<DriverPayments> GetPaymentByCellPhone(SqlConnection pConn, string pCellPhone, decimal pAmount, bool pIncludePaidTrans) {
			string sql = @"SELECT * FROM dbo.DriverPayments
							LEFT JOIN dbo.ACHDetail ON ACHDetail.DriverPaymentID = DriverPayments.DriverPaymentID
							WHERE AffiliateDriverID IN (SELECT AffiliateDriverID FROM dbo.AffiliateDrivers WHERE Cell = @Cell OR CallInCell = @Cell)
							AND Test = 0 AND Failed = 0
							AND  (DriverPaid = 0 OR DriverPaid = @DriverPaid)
							AND CASE WHEN @DriverPaid = 1 THEN 1
								 WHEN @DriverPaid = 0 AND ACHPaidDate IS NULL THEN 1
								 ELSE 0 END = 1";


			List<DriverPayments> payList = pConn.Query<DriverPayments>(sql, new { Cell = pCellPhone, DriverPaid = pIncludePaidTrans }).ToList();

			List<DriverPayments> goodList = new List<DriverPayments>();
			goodList = (from p in payList
						where p.DriverTotal == pAmount
						select p).ToList();


			return goodList; //[0];
		}


		public static List<ACHBatchNumbers> GetUniqueACHBatchNumbers(SqlConnection pConn, TaxiPassRedeemerAccounts pTaxiPassRedeemerAccount) {
			string sql = @"SELECT DISTINCT ACHDetail.ACHBatchNumber FROM ACHDetail WITH (NOLOCK) 
							INNER JOIN DriverPayments ON DriverPayments.DriverPaymentID = ACHDetail.DriverPaymentID  
							INNER JOIN TaxiPassRedeemers on DriverPayments.TaxiPassRedeemerID = TaxiPassRedeemers.TaxiPassRedeemerID 
							where TaxiPassRedeemers.TaxiPassRedeemerAccountID = @TaxiPassRedeemerAccountID
						union 
							SELECT DISTINCT ACHDetail.ACHBatchNumber FROM ACHDetail WITH (NOLOCK) 
							INNER JOIN DriverPayments ON DriverPayments.DriverPaymentID = ACHDetail.DriverPaymentID  
							INNER JOIN DriverPaymentsPendingMatches ON DriverPayments.DriverPaymentID = DriverPaymentsPendingMatches.DriverPaymentID  
							INNER JOIN TaxiPassRedeemers on DriverPaymentsPendingMatches.TaxiPassRedeemerID = TaxiPassRedeemers.TaxiPassRedeemerID 
							where TaxiPassRedeemers.TaxiPassRedeemerAccountID = @TaxiPassRedeemerAccountID
						ORDER BY ACHBatchNumber DESC";

			List<ACHBatchNumbers> batchList = new List<ACHBatchNumbers>();

			batchList = pConn.Query<ACHBatchNumbers>(sql, new { TaxiPassRedeemerAccountID = pTaxiPassRedeemerAccount.TaxiPassRedeemerAccountID }).ToList();
			foreach (ACHBatchNumbers rec in batchList) {
				string sTemp = rec.ACHBatchNumber;
				if (!sTemp.IsNullOrEmpty() && sTemp.Length > 10) {
					string sVal = sTemp.Substring(0, 4) + "/";
					sVal += sTemp.Substring(4, 2) + "/";
					sVal += sTemp.Substring(6, 2) + " ";
					sVal += sTemp.Substring(8, 2);
					rec.BatchNumber = sVal;
					//ACHBatchNumbers oBatch = new ACHBatchNumbers(sVal, sTemp);
					//batchList.Add(oBatch);
				}

			}


			return batchList;
		}

		// **********************************************************************************************
		//
		// **********************************************************************************************
		public static List<DriverPayments> GetPaymentByACHBatchNumber(TaxiPassRedeemerAccounts pRedeemerAccount, long pACHBatchNumber) {

			string sql = @"SELECT DriverPayments.*, Drivers.*, AffiliateDrivers.*
							FROM DriverPayments WITH (NOLOCK)
							INNER JOIN ACHDetail ON DriverPayments.DriverPaymentID = ACHDetail.DriverPaymentID
							INNER JOIN TaxiPassRedeemers ON DriverPayments.TaxiPassRedeemerID = TaxiPassRedeemers.TaxiPassRedeemerID
							LEFT JOIN Drivers ON Drivers.DriverID = DriverPayments.DriverID
							LEFT JOIN AffiliateDrivers ON AffiliateDrivers.AffiliateDriverID = DriverPayments.AffiliateDriverID
							WHERE ACHBatchNumber = @ACHBatchNumber
								AND TaxiPassRedeemers.TaxiPassRedeemerAccountID = @TaxiPassRedeemerAccountID

							UNION

							SELECT DriverPayments.*, Drivers.*, AffiliateDrivers.*
							FROM DriverPayments WITH (NOLOCK)
							INNER JOIN ACHDetail ON DriverPayments.DriverPaymentID = ACHDetail.DriverPaymentID
							INNER JOIN DriverPaymentsPendingMatches ON DriverPayments.DriverPaymentID = DriverPaymentsPendingMatches.DriverPaymentID
							INNER JOIN TaxiPassRedeemers ON DriverPaymentsPendingMatches.TaxiPassRedeemerID = TaxiPassRedeemers.TaxiPassRedeemerID
							LEFT JOIN Drivers ON Drivers.DriverID = DriverPayments.DriverID
							LEFT JOIN AffiliateDrivers ON AffiliateDrivers.AffiliateDriverID = DriverPayments.AffiliateDriverID
							WHERE ACHBatchNumber = @ACHBatchNumber
								AND TaxiPassRedeemers.TaxiPassRedeemerAccountID = @TaxiPassRedeemerAccountID";

			using (var conn = SqlHelper.OpenSqlConnection()) {

				var data = conn.Query<DriverPayments, Drivers, AffiliateDrivers, DriverPayments>(sql
						, (dp, drivers, affDrivers) => { dp.mDriver = drivers; dp.mAffiliateDriver = affDrivers; return dp; }
						, new { ACHBatchNumber = pACHBatchNumber, TaxiPassRedeemerAccountID = pRedeemerAccount.TaxiPassRedeemerAccountID }
						, splitOn: "DriverID,AffiliateDriverID");


				return data.ToList(); // conn.Query<DriverPayments>(sql, new { ACHBatchNumber = pACHBatchNumber, TaxiPassRedeemerAccountID = pRedeemerAccount.TaxiPassRedeemerAccountID }).ToList();
			}
		}


		public static List<DriverPayments> GetPaymentByACHBatchNumberSortedByRedeemerLocation(TaxiPassRedeemerAccounts pRedeemerAccount, long pACHBatchNumber) {
			string sql = @"SELECT  DriverPayments.*
								  , Drivers.*
								  , AffiliateDrivers.*
								  , TaxiPassRedeemers.*
								  , TaxiPassRedeemerLocations.*
								  , TaxiPassRedeemerAccounts.*
								  , DPResults.*
							FROM    DriverPayments WITH (NOLOCK)
							INNER JOIN ACHDetail ON DriverPayments.DriverPaymentID = ACHDetail.DriverPaymentID
							INNER JOIN TaxiPassRedeemers ON DriverPayments.TaxiPassRedeemerID = TaxiPassRedeemers.TaxiPassRedeemerID
							LEFT JOIN dbo.TaxiPassRedeemerLocations ON TaxiPassRedeemerLocations.TaxiPassRedeemerLocationID = DriverPayments.TaxiPassRedeemerLocationID
							LEFT JOIN dbo.TaxiPassRedeemerAccounts ON TaxiPassRedeemerAccounts.TaxiPassRedeemerAccountID = TaxiPassRedeemers.TaxiPassRedeemerAccountID
							LEFT JOIN Drivers ON Drivers.DriverID = DriverPayments.DriverID
							LEFT JOIN AffiliateDrivers ON AffiliateDrivers.AffiliateDriverID = DriverPayments.AffiliateDriverID
							OUTER APPLY (SELECT TOP 1
												*
										 FROM   DriverPaymentResults
										 WHERE  DriverPaymentID = DriverPayments.DriverPaymentID
												AND TransType IN ('D', 'S')
												AND Result = '0'
										 ORDER BY DriverPaymentResultID DESC
										) DPResults
							WHERE   ACHBatchNumber = @ACHBatchNumber
									AND TaxiPassRedeemers.TaxiPassRedeemerAccountID = @TaxiPassRedeemerAccountID";

			using (var conn = SqlHelper.OpenSqlConnection()) {

				var data = conn.Query<DriverPayments, Drivers, AffiliateDrivers, TaxiPassRedeemers, TaxiPassRedeemerLocations, TaxiPassRedeemerAccounts, DriverPaymentResults, DriverPayments>(sql
						, (dp, drivers, affDrivers, redeemers, locations, redAcct, dpResults) => { dp.mDriver = drivers; dp.mAffiliateDriver = affDrivers; dp.mTaxiPassRedeemers = redeemers; dp.mTaxiPassRedeemerLocations = locations; dp.mTaxiPassRedeemers.TaxiPassRedeemerAccounts = redAcct; dp.DriverPaymentChargeResults = dpResults; return dp; }
						, new { ACHBatchNumber = pACHBatchNumber, TaxiPassRedeemerAccountID = pRedeemerAccount.TaxiPassRedeemerAccountID }
						, splitOn: "DriverID,AffiliateDriverID,TaxiPassRedeemerID, TaxiPassRedeemerLocationID,TaxiPassRedeemerAccountID,DriverPaymentResultID");


				return data.ToList(); // conn.Query<DriverPayments>(sql, new { ACHBatchNumber = pACHBatchNumber, TaxiPassRedeemerAccountID = pRedeemerAccount.TaxiPassRedeemerAccountID }).ToList();
			}
		}

		public static List<DriverPayments> GetPaymentsForDriverInitiatedProcess(AffiliateDrivers pDriver) {
			string sql = @"SELECT  DriverPayments.*
							FROM    DriverPayments WITH (NOLOCK)
							LEFT JOIN ACHDetail ON ACHDetail.DriverPaymentID = DriverPayments.DriverPaymentID
							LEFT JOIN AffiliateDrivers ON AffiliateDrivers.AffiliateDriverID = DriverPayments.AffiliateDriverID
							WHERE   TaxiPassRedeemerID IS NULL
									AND DriverPayments.AffiliateDriverID = @AffiliateDriverID
									AND ACHDetailID IS NULL
									AND ((CardSwiped = 1
										  AND AutoPaySwipe = 1
										 )
										 OR (CardSwiped = 0
											 AND AutoPayManual = 1
											)
										)
									AND Test = 0
									AND Failed = 0
									AND ChargeBack = 0
							ORDER BY ChargeDate";

			using (var conn = SqlHelper.OpenSqlConnection()) {
				return conn.Query<DriverPayments>(sql, new { AffiliateDriverID = pDriver.AffiliateDriverID }).ToList();
			}
		}

		/// <summary>
		/// Gets the charges.
		/// </summary>
		/// <param name="pDriver">The driver.</param>
		/// <param name="pStartDate">The start date.</param>
		/// <param name="pIncludeFailed">if set to <c>true</c> [include failed].</param>
		/// <returns></returns>
		public static List<DriverPayments> GetCharges(Drivers pDriver, DateTime pStartDate, bool pIncludeFailed) {
			using (var conn = SqlHelper.OpenSqlConnection()) {
				string sql = @"SELECT  *
								FROM    DriverPayments
								WHERE   DriverID = @DriverID
										AND ChargeDate >= @StartDate
										AND Fare != 0
										{0}
								ORDER BY ChargeDate DESC";

				sql = string.Format(sql, !pIncludeFailed ? "AND Failed = 0" : "");
				return conn.Query<DriverPayments>(sql, new { DriverID = pDriver.DriverID, StartDate = pStartDate.Date }).ToList();
			}
		}

		/// <summary>
		/// Gets the charges.
		/// </summary>
		/// <param name="pDriver">The driver.</param>
		/// <param name="pStartDate">The start date.</param>
		/// <param name="pIncludeFailed">if set to <c>true</c> [include failed].</param>
		/// <returns></returns>
		public static List<DriverPayments> GetCharges(Drivers pDriver, DateTime pStartDate, DateTime pEndDate, bool pIncludeFailed) {
			using (var conn = SqlHelper.OpenSqlConnection()) {
				string sql = @"SELECT  *
								FROM    DriverPayments
								WHERE   DriverID = @DriverID
										AND ChargeDate >= @StartDate
										AND ChargeDate <= @EndDate
										AND Fare != 0
										{0}
								ORDER BY ChargeDate DESC";

				sql = string.Format(sql, !pIncludeFailed ? "AND Failed = 0" : "");
				return conn.Query<DriverPayments>(sql, new { DriverID = pDriver.DriverID, StartDate = pStartDate.Date, EndDate = pEndDate }).ToList();
			}
		}

		/// <summary>
		/// Gets the charges.
		/// </summary>
		/// <param name="pDriver">The driver.</param>
		/// <param name="pStartDate">The start date.</param>
		/// <param name="pIncludeFailed">if set to <c>true</c> [include failed].</param>
		/// <returns></returns>
		public static List<DriverPayments> GetCharges(AffiliateDrivers pDriver, DateTime pStartDate, bool pIncludeFailed) {
			using (var conn = SqlHelper.OpenSqlConnection()) {
				string sql = @"SELECT  *
								FROM    DriverPayments
								WHERE   AffiliateDriverID = @AffiliateDriverID
										AND ChargeDate >= @StartDate
										AND Fare != 0
										{0}
								ORDER BY ChargeDate DESC";

				sql = string.Format(sql, !pIncludeFailed ? "AND Failed = 0" : "");
				return conn.Query<DriverPayments>(sql, new { AffiliateDriverID = pDriver.AffiliateDriverID, StartDate = pStartDate.Date }).ToList();
			}
		}

		/// <summary>
		/// Gets the charges.
		/// </summary>
		/// <param name="pDriver">The driver.</param>
		/// <param name="pStartDate">The start date.</param>
		/// <param name="pIncludeFailed">if set to <c>true</c> [include failed].</param>
		/// <returns></returns>
		public static List<DriverPayments> GetCharges(AffiliateDrivers pDriver, DateTime pStartDate, DateTime pEndDate, bool pIncludeFailed) {
			using (var conn = SqlHelper.OpenSqlConnection()) {
				string sql = @"SELECT  *
								FROM    DriverPayments
								WHERE   AffiliateDriverID = @AffiliateDriverID
										AND ChargeDate >= @StartDate
										AND ChargeDate <= @EndDate
										AND Fare != 0
										{0}
								ORDER BY ChargeDate DESC";

				sql = string.Format(sql, !pIncludeFailed ? "AND Failed = 0" : "");
				return conn.Query<DriverPayments>(sql, new { AffiliateDriverID = pDriver.AffiliateDriverID, StartDate = pStartDate.Date, EndDate = pEndDate }).ToList();
			}
		}


		// **********************************************************************************************
		// check for recurring fees and deduct
		// **********************************************************************************************
		//public decimal DriverPaidProcessRecurringFee(TaxiPassRedeemers pRedeemer, AffiliateDrivers pPayeeDriver, DateTime pDriverPaidDate, string pUpdatedBy) {
		//	string transCardOwner = pPayeeDriver.Name;
		//	string transCardNumber = pPayeeDriver.TransCardNumber;
		//	decimal recurringFeeTotal = 0;

		//	DriverPayments fee = this.GetPerTransactionRecurringFee(true);
		//	if (fee.IsNullEntity) {
		//		//check if there are other fees
		//		EntityList<DriverPayments> list = this.GetRecurringFeesByDriver(true);
		//		if (list.Count > 0) {
		//			fee = list[0];
		//		}
		//	}
		//	if (!fee.IsNullEntity) {
		//		if (pRedeemer.TaxiPassRedeemerAccounts.VoucherValidationOnly && !pRedeemer.TaxiPassRedeemerAccounts.MatchVoucherOnly) {
		//			// do nothing
		//		} else {
		//			if (!pRedeemer.TaxiPassRedeemerAccounts.MatchVoucherOnly) {
		//				if (fee.ACHDetail.IsNullEntity) {
		//					ACHDetail.Create(fee);
		//				}
		//				fee.ACHDetail.ACHPaidDate = pDriverPaidDate;
		//				fee.ACHDetail.ACHPaidTo = "From " + fee.MyDriverName;
		//				fee.ACHDetail.ACHBankAccountNumber = "Less Cash";
		//			}
		//		}

		//		try {
		//			if (fee.RowState != DataRowState.Unchanged) {
		//				fee.Save();
		//				recurringFeeTotal += Math.Abs(fee.DriverTotalWithKioskAmount);
		//			}
		//		} catch {
		//		}
		//	}

		//	return recurringFeeTotal;
		//}
		public static IEnumerable<ACHBatchNumbers> GetUniqueACHBatchNumbers(Affiliate pAffiliate) {
			using (var conn = SqlHelper.OpenSqlConnection()) {
				return GetUniqueACHBatchNumbers(conn, pAffiliate);
			}
		}

		public static IEnumerable<ACHBatchNumbers> GetUniqueACHBatchNumbers(SqlConnection pConn, Affiliate pAffiliate) {
			//List<ACHBatchNumbers> 
			DateTime start = DateTime.Now;
			List<ACHBatchNumbers> batchList = new List<ACHBatchNumbers>();
			//try {
			batchList = pConn.Query<ACHBatchNumbers>("usp_AchBatchNumbersForAffiliate", new { AffiliateID = pAffiliate.AffiliateID }, commandType: CommandType.StoredProcedure, commandTimeout: 300).ToList();
			//} catch (Exception) {
			//batchList = pConn.Query<ACHBatchNumbers>("usp_AchBatchNumbersForAffiliate", new { AffiliateID = pAffiliate.AffiliateID }, commandType: CommandType.StoredProcedure).ToList();
			//}
			double elapsed = DateTime.Now.Subtract(start).TotalMilliseconds;
			Console.WriteLine(elapsed);

			foreach (ACHBatchNumbers rec in batchList) {
				string sTemp = rec.ACHBatchNumber;
				if (!sTemp.IsNullOrEmpty() && sTemp.Length > 10) {
					string sVal = sTemp.Substring(0, 4) + "/";
					sVal += sTemp.Substring(4, 2) + "/";
					sVal += sTemp.Substring(6, 2) + " ";
					sVal += sTemp.Substring(8, 2);
					rec.BatchNumber = sVal;
				}
			}
			return batchList;

		}

		// ***********************************************************************************************
		// Proc used by CabRideAutoACH
		// SQL is identical to GetPaymentsToAch(Affiliate pAffiliate... but procs provide speed
		// ***********************************************************************************************
		public static List<DriverPayments> GetCabRideAutoACH(SqlConnection pConn, Affiliate pAffiliate, DateTime pEnd) {
			return pConn.Query<DriverPayments>("usp_DriverPaymentsCabRideAutoACH", new { AffiliateID = pAffiliate.AffiliateID, End = pEnd }, commandTimeout: 600, commandType: CommandType.StoredProcedure).ToList();
		}

		public static List<DriverPayments> GetPaymentsToAchForAffiliateDriver(SqlConnection pConn, AffiliateDrivers pDriver, DateTime pStart, DateTime pEnd) {
			string sql = @"SELECT DISTINCT
									DriverPayments.*
							FROM    DriverPayments WITH (NOLOCK)
							LEFT JOIN ACHDetail ON DriverPayments.DriverPaymentID = ACHDetail.DriverPaymentID
							LEFT JOIN DriverPaymentAux ON DriverPayments.DriverPaymentID = DriverPaymentAux.DriverPaymentID
							LEFT JOIN AffiliateDrivers ON AffiliateDrivers.AffiliateDriverID = DriverPayments.AffiliateDriverID
							WHERE   DriverPayments.AffiliateDriverID = @AffiliateDriverID
									AND ChargeDate >= @Start
									AND ChargeDate <= @End
									AND ACHDetail.DriverPaymentID IS NULL
									AND Failed = 0
									AND DriverPayments.Test = 0
									AND (DriverPaymentAux.DoNotPay IS NULL
										 OR DriverPaymentAux.DoNotPay = 0
										)
									AND AffiliateDrivers.Active = 1
									AND RedeemerStopPayment = 0
									AND AffiliateDrivers.FraudSuspected = 0
									AND DriverPaid = 0
							ORDER BY ChargeDate";

			return pConn.Query<DriverPayments>(sql, new { AffiliateDriverID = pDriver.AffiliateDriverID, Start = pStart, End = pEnd }, commandTimeout: 600).ToList();

		}

		public static short GetNextBatchNumber(SqlConnection pConn, string pInput, short pMin) {
			short sNumber = 0;

			string sql = @"SELECT  MAX(CAST(SUBSTRING(CAST(achbatchnumber AS VARCHAR(20)), 9, 2) AS INT)
										+ 1) Num
							FROM    ACHDetail WITH ( NOLOCK )
							WHERE   ACHBatchNumber LIKE @ACHBatchNumber";

			List<string> oResults = pConn.Query<string>(sql, new { ACHBatchNumber = pInput + "%" }).ToList();
			if (oResults != null) {
				if (oResults.Count > 0) {
					if (!oResults[0].IsNullOrEmpty()) {
						sNumber = short.Parse(oResults[0]);
						if (sNumber < pMin) {
							sNumber = pMin;
						}
					} else {
						sNumber = pMin;
					}
				}
			}

			sNumber++;

			return sNumber;
		}


		/// <summary>
		/// Gets the affiliate driver unique ach batch numbers.
		/// </summary>
		/// <param name="pConn">The p connection.</param>
		/// <param name="pAffiliateDriverID">The p affiliate driver identifier.</param>
		/// <returns></returns>
		public static List<ACHBatchNumbers> GetAffiliateDriverUniqueACHBatchNumbers(SqlConnection pConn, long pAffiliateDriverID) {
			List<ACHBatchNumbers> achList = new List<ACHBatchNumbers>();

			AffiliateDrivers affDriver = AffiliateDrivers.GetDriver(pConn, pAffiliateDriverID);

			if (affDriver == null || affDriver.BankAccountNumber == null) {
				return achList;
			}


			string sql = @"SELECT DISTINCT ACHBatchNumber
							FROM   ACHDetail
							WHERE  ACHBankCode = @BankCode
								AND ACHBankAccountNumber = @BankAccountNumber
							ORDER BY ACHBatchNumber DESC ";

			DynamicParameters parms = new DynamicParameters();
			parms.Add("BankCode", affDriver.BankCode);
			parms.Add("BankAccountNumber", affDriver.BankAccountNumber);

			List<long> batchList = pConn.Query<long>(sql, parms).ToList();
			int i = 0;
			foreach (long batchNo in batchList) {
				string sTemp = batchNo.ToString();
				string sVal = sTemp.Substring(0, 4) + "/";
				sVal += sTemp.Substring(4, 2) + "/";
				sVal += sTemp.Substring(6, 2) + " ";
				sVal += sTemp.Substring(8, 2);
				ACHBatchNumbers oBatch = new ACHBatchNumbers(sVal, sTemp);
				achList.Add(oBatch);
				i++;

				if (i > 30) {
					break;
				}
			}


			return achList;
		}


		public static List<DriverPayments> GetPaymentsByACHBatchNumber(SqlConnection pConn, AffiliateDrivers pAffiliateDriver, long pACHBatchNumber) {
			string sql = @" SELECT DriverPayments.*
							 FROM   DriverPayments WITH ( NOLOCK )
							 INNER JOIN ACHDetail ON ACHDetail.DriverPaymentID = DriverPayments.DriverPaymentID
							 WHERE  ACHBatchNumber = @ACHBatchNumber
									AND ACHBankCode = @BankCode
									AND ACHBankAccountNumber = @BankAccountNumber
							 ORDER BY ChargeDate ";

			DynamicParameters parms = new DynamicParameters();
			parms.Add("ACHBatchNumber", pACHBatchNumber);
			parms.Add("BankCode", pAffiliateDriver.BankCode);
			parms.Add("BankAccountNumber", pAffiliateDriver.BankAccountNumber);

			return pConn.Query<DriverPayments>(sql, parms).ToList();
		}

		public static IEnumerable<DriverPayments> GetPaymentsByReferenceNo(SqlConnection pConn, string pReferenceNo) {
			string sql = @"SELECT * FROM dbo.DriverPayments
							WHERE ReferenceNo = @ReferenceNo";

			return pConn.Query<DriverPayments>(sql, new { ReferenceNo = pReferenceNo });
		}

		public static DriverPayments GetPaymentByReferenceNo(SqlConnection pConn, long pAffiliateID, string pReferenceNo) {
			string sql = @"SELECT * FROM dbo.DriverPayments
							WHERE AffiliateID = @AffiliateID AND ReferenceNo = @ReferenceNo";

			return pConn.Get<DriverPayments>(sql, new { ReferenceNo = pReferenceNo, AffiliateID = pAffiliateID });
		}

		public static DriverPayments GetPaymentByReferenceNo(SqlConnection pConn, string pReferenceNo, string pPlatform) {
			string sql = @"SELECT * FROM dbo.DriverPayments
							WHERE ReferenceNo = @ReferenceNo and Platform = @Platform";

			return pConn.Get<DriverPayments>(sql, new { ReferenceNo = pReferenceNo, Platform = pPlatform });
		}

		public static DriverPayments GetPaymentByProcessorReference(SqlConnection pConn, string pReferenceNo, DateTime pStartDate, DateTime pEndDate) {
			string sql = @"SELECT DISTINCT DriverPayments.* FROM dbo.DriverPayments
							LEFT JOIN dbo.DriverPaymentResults ON DriverPaymentResults.DriverPaymentID = DriverPayments.DriverPaymentID
							WHERE Reference = @Reference  AND (AuthDate BETWEEN @StartDate AND @EndDate OR ChargeDate BETWEEN @StartDate AND @EndDate)
							ORDER BY DriverPayments.DriverPaymentID DESC";

			return pConn.Get<DriverPayments>(sql, new { Reference = pReferenceNo, StartDate = pStartDate, EndDate = pEndDate });
		}

		public static IEnumerable<DriverPayments> GetPaymentsByCardNo(SqlConnection pConn, string pCardNumber) {
			IEnumerable<DriverPayments> paymentList;

			if (pCardNumber.Length > 14) {
				string sCardNo = CardNumberToHash(pCardNumber);

				string sql = @"SELECT  *
								FROM    DriverPayments
								WHERE   Number = @Number
										AND Failed = 0
										AND Fare >= 0";

				paymentList = pConn.Query<DriverPayments>(sql, new { Number = sCardNo });

			} else {
				string sql = @"SELECT  *
								FROM    DriverPayments
								WHERE   CardNumberDisplay LIKE @CardNumberDisplay
										AND Failed = 0
										AND Fare >= 0
								ORDER BY ChargeDate";

				paymentList = pConn.Query<DriverPayments>(sql, new { CardNumberDisplay = "%" + pCardNumber });
			}
			return paymentList;

		}


		public static DriverPayments GetLastAffDriverPayment(SqlConnection pConn, long pAffiliateDriverID) {
			string sql = @"SELECT TOP 1 *
							FROM    DriverPayments
							WHERE   AffiliateDriverID = @AffiliateDriverID
							ORDER BY ChargeDate DESC";
			return pConn.Get<DriverPayments>(sql, new { AffiliateDriverID = pAffiliateDriverID });
		}


		// ************************************************************************************************************
		// Public Get DriverPayments:  If "Tag" = true, this is a Ride Voucher 
		// ************************************************************************************************************
		public static List<DriverPayments> GetRideDriverPaymentsByCardNoAndDate(SqlConnection pConn, string pCardNumber, DateTime pDate) {
			return GetRideRelatedData(pConn, "", pCardNumber, pDate.ToShortDateString());
		}

		// ************************************************************************************************************
		// Get DriverPayments:  If "Tag" = true, this is a Ride Voucher 
		// ************************************************************************************************************
		private static List<DriverPayments> GetRideRelatedData(SqlConnection pConn, string pTransNo, string pCardNumber, string pDate) {

			DynamicParameters parms = new DynamicParameters();
			//sql.Append("usp_DriverPaymentsRideGet ");
			if (!pTransNo.IsNullOrEmpty()) {
				parms.Add("TransNo", pTransNo);
			}
			if (!pCardNumber.IsNullOrEmpty()) {
				parms.Add("CardNumber", pCardNumber);
			}
			if (!pCardNumber.IsNullOrEmpty()) {
				parms.Add("ChargeDate", pDate);
			}

			return pConn.Query<DriverPayments>("usp_DriverPaymentsRideGet", parms, commandType: CommandType.StoredProcedure).ToList();
		}


		public static List<DriverPayments> GetPendingReceiptChargeBacks(SqlConnection pPM, bool pReporting) {
			SystemDefaults oDefaults = SystemDefaults.GetDefaults(pPM);
			DateTime date = DateTime.Today.AddDays(-oDefaults.RequestReceiptChargeDays);

			if (pReporting) {
				date = date.AddDays(-1);
			}

			string sql = @"SELECT  DriverPayments.*
							FROM    DriverPayments WITH (NOLOCK)
							INNER JOIN ReceiptRequest ON DriverPayments.DriverPaymentID = ReceiptRequest.DriverPaymentID
							LEFT JOIN dbo.DriverPaymentAux ON DriverPaymentAux.DriverPaymentID = DriverPayments.DriverPaymentID
							LEFT JOIN dbo.DriverPaymentVoucherImages ON DriverPaymentVoucherImages.DriverPaymentID = DriverPayments.DriverPaymentID
							WHERE   ReceiptRequest.ReceiptRequestDue = @ReceiptRequestDue
									AND ReceiptRequest.ValidReceiptReceived IS NULL
									AND DriverPayments.ReceiptChargedBack IS NULL
									AND ReceiptRequest.InquiryFulfilled = 0
									AND ISNULL(DoNotChargeBack, 0) = 0
									AND ISNULL(SignaturePath, '') = ''
									AND DriverPaymentVoucherImageID IS NULL";

			List<DriverPayments> payList = pPM.Query<DriverPayments>(sql, new { ReceiptRequestDue = date }).ToList();

			return payList;
		}

		public static DriverPayments GetLastDriverPayment(SqlConnection pConn, long pDriverID) {
			string sql = @"SELECT TOP 1 * FROM DriverPayments
							WHERE   DriverID = @DriverID
							ORDER BY ChargeDate DESC";
			return pConn.Get<DriverPayments>(sql, new { DriverID = pDriverID });
		}


		// ********************************************************************************
		// 
		// ********************************************************************************
		public static List<DriverPayments> GetPendingTransactions(Drivers pDriver) {
			return GetPendingTransactions(pDriver, true);
		}


		// ********************************************************************************
		// 
		// ********************************************************************************
		public static List<DriverPayments> GetPendingTransactions(Drivers pDriver, bool pIncludeCorpAccounts) {
			return GetPendingTransactions(pDriver, pIncludeCorpAccounts, true);
		}

		public static List<DriverPayments> GetPendingTransactions(Drivers pDriver, bool pIncludeCorpAccounts, bool pAllDates) {
			string sql = @"SELECT  *
							FROM    DriverPayments
							WHERE   Fare = 0
									AND Failed = 0
									AND DriverID = @DriverID";

			DynamicParameters parms = new DynamicParameters();
			parms.Add("DriverID", pDriver.DriverID);
			if (!pAllDates) {
				sql += " AND AuthDate >= @Date";
				parms.Add("Date", DateTime.Today.AddDays(-30));
			}
			if (!pIncludeCorpAccounts) {
				sql += @" AND (CorporateName IS NULL
								 OR CorporateName = ''
								)";
			}
			using (var conn = SqlHelper.OpenSqlConnection()) {
				return conn.Query<DriverPayments>(sql, parms).ToList();
			}
		}

		/// <summary>
		/// Gets the pending transactions.
		/// </summary>
		/// <param name="pAffiliate">The affiliate.</param>
		/// <returns></returns>
		public static List<DriverPayments> GetPendingTransactions(Affiliate pAffiliate) {
			return GetPendingTransactions(pAffiliate, true);
		}

		/// <summary>
		/// Gets the pending transactions.
		/// </summary>
		/// <param name="pAffiliate">The affiliate.</param>
		/// <param name="pIncludeCorpAccounts">if set to <c>true</c> [include corp accounts].</param>
		/// <returns></returns>
		public static List<DriverPayments> GetPendingTransactions(Affiliate pAffiliate, bool pIncludeCorpAccounts) {
			return GetPendingTransactions(pAffiliate, pIncludeCorpAccounts, true);
		}

		public static List<DriverPayments> GetPendingTransactions(Affiliate pAffiliate, bool pIncludeCorpAccounts, bool pAllDates) {
			string sql = @"SELECT  *
							FROM    DriverPayments
							LEFT JOIN Drivers ON Drivers.DriverID = DriverPayments.DriverID
							WHERE   Fare = 0
									AND Failed = 0
									AND DriverPayments.Test = 0
									AND Drivers.AffiliateID = @AffiliateID";
			DynamicParameters parms = new DynamicParameters();
			parms.Add("AffiliateID", pAffiliate.AffiliateID);
			if (!pAllDates) {
				sql += " AND AuthDate >= @Date";
				parms.Add("Date", DateTime.Today.AddDays(-30));
			}
			if (!pIncludeCorpAccounts) {
				sql += " AND (CorporateName IS NULL OR CorporateName = '')";
			}

			using (var conn = SqlHelper.OpenSqlConnection()) {
				return conn.Query<DriverPayments>(sql, parms).ToList();
			}
		}

		#endregion


		// ************************************************************************************
		// Create a charge-back for the entire DriverTotal amount
		// ************************************************************************************
		public DriverPayments DoChargeBack(string pChargeBackReason) {
			Drivers oDriver = this.Drivers;
			DriverPayments oPayment = DriverPayments.Create(oDriver);
			oPayment.AffiliateID = this.AffiliateID;
			oPayment.AffiliateDriverID = this.AffiliateDriverID;

			//oPayment.TransNo = textBoxTransNo.Text;
			oPayment.Fare = -Math.Abs(this.DriverTotal);
			oPayment.ChargeBackDate = Affiliate.LocalTime;
			oPayment.ChargeBackInfo = "Charge Back of TransNo: " + this.TransNo + "\n\n" + pChargeBackReason;
			oPayment.ChargeBack = true;
			oPayment.ChargeDate = Affiliate.LocalTime;
			oPayment.DriverPaid = true;
			oPayment.DriverPaidDate = Affiliate.LocalTime;
			oPayment.TaxiPassRedeemerID = this.TaxiPassRedeemerID;
			oPayment.CardNumber = ""; // this.CardNumber;
			oPayment.CardNumberDisplay = ""; //this.CardNumberDisplay;
			oPayment.CardSwiped = false; //this.CardSwiped;
			oPayment.Expiration = ""; //this.Expiration;
			oPayment.CardType = ""; //this.CardType;
			oPayment.CardZIPCode = ""; //this.CardZIPCode;
			oPayment.CardHolder = ""; //this.CardHolder;
			oPayment.CardAddress = ""; //this.CardAddress;
			oPayment.Failed = false;
			oPayment.Number = ""; // this.Number;
			oPayment.Save();

			DriverPaymentAux oAux = DriverPaymentAux.Create(oPayment);
			oAux.ChargeBackDriverPaymentID = this.DriverPaymentID;
			oAux.Save();

			this.ReceiptChargedBack = oPayment.TransNo;
			this.Save();

			return oPayment;
		}

		public override int Save() {
			return Save(CabRideDapperSettings.LoggedInUser);
		}

		public override int Save(string pModifiedBy) {
			ModifiedDate = DateTime.UtcNow;
			ModifiedBy = string.Format("{0} | {1}", pModifiedBy, CabRideDapperSettings.GetAppName());
			if (IsNewRecord && DriverPaymentID == 0) {
				this.DriverPaymentID = NextID.GetNextKey(DriverPayments.TableDef.TABLE_NAME);
			} else {
				RowVersion++;
			}

			bool bFullAphabet = false;

			//ensure that the TransNo is unique
			if (IsNewRecord) {
				if (this.Platform.IsNullOrEmpty()) {
					if (this.Affiliate.DefaultPlatform.IsNullOrEmpty()) {
						this.Platform = CHARGECARDS;
					} else {
						this.Platform = this.Affiliate.DefaultPlatform;
					}
				}
				bFullAphabet = this.Platform.Equals(CHARGECARDS, StringComparison.CurrentCultureIgnoreCase) ||
									this.Platform.Equals(Platforms.ChargePassWeb.ToString(), StringComparison.CurrentCultureIgnoreCase) ||
									this.Platform.Equals(Platforms.CCKiosk.ToString(), StringComparison.CurrentCultureIgnoreCase) ||
									this.Platform.Equals(Platforms.NexStep.ToString(), StringComparison.CurrentCultureIgnoreCase);
				//string prefix = GetVoucherPrefix(bFullAphabet);

				AssignUniqueTransNo(bFullAphabet);
			}
			if (this.DriverPaid && string.IsNullOrEmpty(this.DriverPaidTransNo)) {
				this.DriverPaidTransNo = TaxiPassCommon.StringUtils.GetUniqueKey(6);
			}

			// if the card # contains 5 'x' or 5 '*' then it wasn't loaded normally so bypass this step
			if (!CardNumber.IsNullOrEmpty() && !CardNumber.ToLower().Contains("xxxxx") && !CardNumber.Contains("*****") && CardNumber.Length > 14) {
				if (!this.Test && CardUtils.IsTestCard(DecryptCardNumber())) {
					this.Test = true;
				}
			}

			if (this.ChargeBack) {
				if (this.TaxiPassFee > 0) {
					this.TaxiPassFee = -this.TaxiPassFee;
				}
			}
			if (!this.CreditCardProcessor.IsNullOrEmpty() && this.CreditCardProcessor.StartsWith("DriverProm", StringComparison.CurrentCultureIgnoreCase)) {
				this.DriverFee = 0;
			}

			if (!this.DriverPaymentGeoCodes.IsNullEntity && this.DriverPaymentGeoCodes.Address.IsNullOrEmpty() && this.DriverPaymentGeoCodes.Latitude.GetValueOrDefault(0) != 0) {
				FillAddress();
			}

			if (this.CardNumberDisplay.IsNullOrEmpty()) {
				this.CardNumberDisplay = this.DecryptCardNumber().CardNumberDisplay();
			}

			DateTime now = this.Affiliate.LocalTime;

			//try to save 5 times incase of deadlocks or timeouts
			int saveCnt = 0;
			int saved = 0;
			while (saveCnt < 3) {
				try {
					base.Save(pModifiedBy);
					if (this.DriverPaymentID > 0) {
						saved = 1;
					}
					break;
				} catch (Exception ex) {
					Thread.Sleep(1000);
					if (!this.AuthDate.HasValue) {
						if (this.DriverPaymentID < 0) {
							//try changing the voucher number
							AssignUniqueTransNo(bFullAphabet);
						}
					}

					if (saveCnt >= 5) {
						SaveError(ex);
						//throw ex;
						break;
					}
					saveCnt++;
				}
			}

			if (saved > 0) {
				if (mDriverPaymentRedeemerNotes != null && mDriverPaymentRedeemerNotes.RowState != DataRowState.Unchanged) {
					if (mDriverPaymentRedeemerNotes.DriverPaymentID == 0) {
						mDriverPaymentRedeemerNotes.DriverPaymentID = this.DriverPaymentID;
					}
					mDriverPaymentRedeemerNotes.Save(pModifiedBy);
				}
				if (mReceiptRequest != null && mReceiptRequest.RowState != DataRowState.Unchanged) {
					if (mReceiptRequest.DriverPaymentID == 0) {
						mReceiptRequest.DriverPaymentID = this.DriverPaymentID;
					}
					mReceiptRequest.Save(pModifiedBy);
				}
				if (mDriverPaymentVoucherImages != null && mDriverPaymentVoucherImages.RowState != DataRowState.Unchanged) {
					if (mDriverPaymentVoucherImages.DriverPaymentID == 0) {
						mDriverPaymentVoucherImages.DriverPaymentID = this.DriverPaymentID;
					}
					mDriverPaymentVoucherImages.Save(pModifiedBy);
				}
				if (mDriverPaymentKioskAmount != null && mDriverPaymentKioskAmount.RowState != DataRowState.Unchanged) {
					if (mDriverPaymentKioskAmount.DriverPaymentID == 0) {
						mDriverPaymentKioskAmount.DriverPaymentID = this.DriverPaymentID;
					}
					mDriverPaymentKioskAmount.Save(pModifiedBy);
				}
				if (mDriverPaymentNotes != null && mDriverPaymentNotes.RowState != DataRowState.Unchanged) {
					if (mDriverPaymentNotes.DriverPaymentID == 0) {
						mDriverPaymentNotes.DriverPaymentID = this.DriverPaymentID;
					}
					mDriverPaymentNotes.Save(pModifiedBy);
				}
				if (mACHDetail != null && mACHDetail.RowState != DataRowState.Unchanged) {
					if (mACHDetail.DriverPaymentID == 0) {
						mACHDetail.DriverPaymentID = this.DriverPaymentID;
					}
					mACHDetail.Save(pModifiedBy);
				}
				if (mDriverPaidCash != null && mDriverPaidCash.RowState != DataRowState.Unchanged) {
					if (mDriverPaidCash.DriverPaymentID == 0) {
						mDriverPaidCash.DriverPaymentID = this.DriverPaymentID;
					}
					mDriverPaidCash.Save(pModifiedBy);
				}
				if (mDriverPaymentLossGain != null && mDriverPaymentLossGain.RowState != DataRowState.Unchanged) {
					if (mDriverPaymentLossGain.DriverPaymentID == 0) {
						mDriverPaymentLossGain.DriverPaymentID = this.DriverPaymentID;
					}
					mDriverPaymentLossGain.Save(pModifiedBy);
				}
				if (mRedeemerShiftVouchers != null && mRedeemerShiftVouchers.RowState != DataRowState.Unchanged) {
					if (mRedeemerShiftVouchers.DriverPaymentID == 0) {
						mRedeemerShiftVouchers.DriverPaymentID = this.DriverPaymentID;
					}
					mRedeemerShiftVouchers.Save(pModifiedBy);
				}
				if (mDriverPaymentGeoCodes != null && mDriverPaymentGeoCodes.RowState != DataRowState.Unchanged) {
					if (mDriverPaymentGeoCodes.DriverPaymentID == 0) {
						mDriverPaymentGeoCodes.DriverPaymentID = this.DriverPaymentID;
					}
					mDriverPaymentGeoCodes.Save(pModifiedBy);
				}
				if (mDriverPaymentGeoCodesList != null) {
					foreach (DriverPaymentGeoCodes geoCode in mDriverPaymentGeoCodesList) {
						if (geoCode != null && geoCode.RowState != DataRowState.Unchanged) {
							if (geoCode.DriverPaymentID == 0) {
								geoCode.DriverPaymentID = this.DriverPaymentID;
							}
							geoCode.Save(pModifiedBy);
						}
					}
				}

				if (mDriverPaymentRefList != null) {
					foreach (DriverPaymentRef refRec in mDriverPaymentRefList) {
						if (refRec != null && refRec.RowState != DataRowState.Unchanged) {
							if (refRec.DriverPaymentID == 0) {
								refRec.DriverPaymentID = this.DriverPaymentID;
							}
							refRec.Save(pModifiedBy);
						}
					}
				}

				if (mDriverPaymentAux != null && mDriverPaymentAux.RowState != DataRowState.Unchanged) {
					if (mDriverPaymentAux.DriverPaymentID == 0) {
						mDriverPaymentAux.DriverPaymentID = this.DriverPaymentID;
					}
					mDriverPaymentAux.Save(pModifiedBy);
				}
				if (mReceiptRequest != null && mReceiptRequest.RowState != DataRowState.Unchanged) {
					if (mReceiptRequest.DriverPaymentID == 0) {
						mReceiptRequest.DriverPaymentID = this.DriverPaymentID;
					}
					mReceiptRequest.Save(pModifiedBy);
				}

				if (mDriverPaymentResultses != null) {
					foreach (DriverPaymentResults rec in mDriverPaymentResultses) {
						if (rec.RowState != DataRowState.Unchanged) {
							if (rec.DriverPaymentID == 0) {
								rec.DriverPaymentID = this.DriverPaymentID;
							}
							rec.Save(pModifiedBy);
						}
					}
				}

				if (SwipeFailed) {
					DriverPaymentRef oRef = DriverPaymentRef.GetCreate(this, "CardSwiped", "Failed");
					oRef.ValueString = SwipeFailed.ToString();
					oRef.Save();
				}

			}

			return saved;
		}

		// ************************************************************************************
		// Create a charge-back for a partial amount
		// ************************************************************************************
		public DriverPayments DoChargeBack(string pChargeBackReason, decimal pAmount) {
			Drivers oDriver = this.Drivers;
			DriverPayments oPayment = DriverPayments.Create(oDriver);
			oPayment.AffiliateID = this.AffiliateID;
			oPayment.AffiliateDriverID = this.AffiliateDriverID;

			//oPayment.TransNo = textBoxTransNo.Text;
			oPayment.Fare = pAmount;
			oPayment.ChargeBackDate = Affiliate.LocalTime;
			oPayment.ChargeBackInfo = "Charge Back of TransNo: " + this.TransNo + "\n\n" + pChargeBackReason;
			oPayment.ChargeBack = true;
			oPayment.ChargeDate = Affiliate.LocalTime;
			oPayment.DriverPaid = true;
			oPayment.DriverPaidDate = Affiliate.LocalTime;
			oPayment.TaxiPassRedeemerID = this.TaxiPassRedeemerID;
			oPayment.Platform = Platforms.CustServiceCreditCardChargeBack.ToString();
			oPayment.Save();

			DriverPaymentAux oAux = DriverPaymentAux.Create(oPayment);
			oAux.ChargeBackDriverPaymentID = oPayment.DriverPaymentID;
			oAux.Save();

			this.ReceiptChargedBack = oPayment.TransNo;
			this.Save();

			return oPayment;
		}


		// **********************************************************************************************
		//
		// **********************************************************************************************
		public static List<DriverPayments> GetReceiptRemindersDue(SqlConnection pConn) {

			DateTime dStart = DateTime.Today.AddDays(-1);

			string sql = @"SELECT  DriverPayments.*
							FROM    DriverPayments WITH (NOLOCK)
							INNER JOIN ReceiptRequest ON DriverPayments.DriverPaymentID = ReceiptRequest.DriverPaymentID
							WHERE   ReceiptRequest.ReceiptRequest >= @ReceiptRequest";

			return pConn.Query<DriverPayments>(sql, new { ReceiptRequest = dStart }).ToList();
		}



		[Editable(false)]
		public string ReceiptDetailsHTML {
			get {
				StringBuilder html = new StringBuilder();
				html.Append("Receipt Details:<br/><br/><Table border=1><tr><td>");

				html.Append("Fleet: ");
				html.Append(this.Affiliate.Name);
				html.Append("<br />");
				if (this.Affiliate.IsLimo()) {
					html.Append("Car # ");
					html.Append(this.MyVehicle);
					html.Append("<br />");
				} else {
					html.Append("Cab # ");
					html.Append(this.MyVehicle);
					html.Append("<br />");
				}
				html.Append("Driver: ");
				html.Append(this.MyDriverName);
				html.Append("<br />");
				html.Append("Date: ");
				html.Append(this.ChargeDate.Value.ToString("MM/dd/yyyy HH:mm"));
				html.Append("<br />");
				html.Append("Voucher #: ");
				html.Append(this.TransNo);
				html.Append("<br />");
				html.Append("CC #: ");
				html.Append(this.CardNumberDisplay);
				html.Append("<br />");
				if (!this.CardHolder.IsNullOrEmpty()) {
					html.Append("Card Holder: ");
					html.Append(this.CardHolder.Decrypt());
					html.Append("<br />");
				}
				html.Append("<br />");
				html.Append("Fare: ");
				html.Append(this.Fare.ToString("C"));
				html.Append("<br />");
				if (this.AirportFee > 0) {
					html.Append("Airport: ");
					html.Append(this.AirportFee.ToString("C"));
					html.Append("<br />");
				}
				if (this.MiscFee > 0) {
					html.Append("Other Fee: ");
					html.Append(this.MiscFee.ToString("C"));
					html.Append("<br />");
				}
				if (this.Gratuity > 0) {
					html.Append("Tip: ");
					html.Append(this.Gratuity.ToString("C"));
					html.Append("<br />");
				}
				if (this.Affiliate.IsLimo()) {
					html.Append("LimoPass ");
				} else {
					html.Append("TaxiPass ");
				}
				html.Append("Total: ");
				html.Append(this.DriverTotal.ToString("C"));
				html.Append("<br /><br>");

				// Added per Seth
				if (this.DriverPaidDate.HasValue) {
					html.AppendFormat("Redemption Date: {0}<br>", this.DriverPaidDate.ToString());
				}

				if (!this.MyRedeemer.IsNullOrEmpty()) {
					html.AppendFormat("Redeemer: {0}<br>", this.MyRedeemer);
				}

				if (!ReceiptRequest.ReceiptRequestTypes.IsNullEntity) {
					html.AppendFormat("Notes: {0}<br>", ReceiptRequest.ReceiptRequestTypes.Description);
				}


				html.Append("</td></tr></Table>");

				html.AppendFormat("<br><br><a href=\"mailto:{0}@taxipassreceipt.com\">{1}</a>", TransNo, CabRideEngineDapper.SystemDefaultsDict.GetByKey("ReceiptRequest", "EMailLinkText", false).ValueString);
				return html.ToString();
			}
		}


		public static List<DriverPayments> GetForRedeemerNotifications(SqlConnection pConn, TaxiPassRedeemerAccounts pRedeemerAccount, DateTime pAchDate) {
			string sql = @"SELECT  *
							FROM    DriverPayments WITH (NOLOCK)
							LEFT JOIN ACHDetail ON ACHDetail.DriverPaymentID = DriverPayments.DriverPaymentID
							LEFT JOIN DriverPaymentsPendingMatches ON DriverPayments.DriverPaymentID = DriverPaymentsPendingMatches.DriverPaymentID
							WHERE   ACHDetail.ACHPaidDate >= @ACHPaidDateStart
									AND ACHPaidDate < @ACHPaidDateEnd
									AND (DriverPayments.TaxiPassRedeemerID IN (SELECT   TaxiPassRedeemerID
																			   FROM     TaxiPassRedeemers
																			   WHERE    TaxiPassRedeemerAccountID = @TaxiPassRedeemerAccountID)
										 OR DriverPaymentsPendingMatches.TaxiPassRedeemerID IN (SELECT  TaxiPassRedeemerID
																								FROM    TaxiPassRedeemers
																								WHERE   TaxiPassRedeemerAccountID = @TaxiPassRedeemerAccountID)
										)";

			return pConn.Query<DriverPayments>(sql, new { TaxiPassRedeemerAccountID = pRedeemerAccount.TaxiPassRedeemerAccountID, ACHPaidDateStart = pAchDate, ACHPaidDateEnd = pAchDate.AddDays(1) }).ToList();
		}


		public static List<DriverPayments> GetTransHistoryCharges(long pAffiliateDriverID, DateTime pStartDate, DateTime pEndDate, bool pIncludeTest) {
			string sql = @"SELECT *
							FROM    DriverPayments
							LEFT JOIN DriverPaymentAux ON DriverPaymentAux.DriverPaymentID = DriverPayments.DriverPaymentID
							LEFT JOIN ACHDetail ON ACHDetail.DriverPaymentID = DriverPayments.DriverPaymentID
							LEFT JOIN TaxiPassRedeemers ON TaxiPassRedeemers.TaxiPassRedeemerID = DriverPayments.TaxiPassRedeemerID
							LEFT JOIN TaxiPassRedeemerAccounts ON TaxiPassRedeemerAccounts.TaxiPassRedeemerAccountID = TaxiPassRedeemers.TaxiPassRedeemerAccountID
							WHERE   AffiliateDriverID = @AffiliateDriverID
									AND ChargeDate >= @StartDate
									AND ChargeDate <= @EndDate
									AND Fare > 0
									AND Failed = 0
									AND (Test = 0 OR Test = @Test)
									AND ISNULL(DriverPayments.DoNotPay, 0) = 0 AND ISNULL(DriverPaymentAux.DoNotPay, 0) = 0
									AND DriverPayments.TransNo NOT IN (SELECT ValueString FROM DriverPaymentRef WHERE ReferenceGroup = 'Refund' AND ReferenceKey = 'Original TransNo')
							ORDER BY ChargeDate DESC";

			using (var conn = SqlHelper.OpenSqlConnection()) {
				try {
					return conn.Query<DriverPayments, DriverPaymentAux, ACHDetail, TaxiPassRedeemers, TaxiPassRedeemerAccounts, DriverPayments>(sql,
						(dp, aux, ach, rdmr, rdmrAct) => { dp.mDriverPaymentAux = aux; dp.mACHDetail = ach; dp.mTaxiPassRedeemers = rdmr; dp.mTaxiPassRedeemers.TaxiPassRedeemerAccounts = rdmrAct; return dp; },
						new { AffiliateDriverID = pAffiliateDriverID, StartDate = pStartDate, EndDate = pEndDate, Test = pIncludeTest },
						splitOn: "DriverPaymentID,TaxiPassRedeemerID,TaxiPassRedeemerAccountID").ToList();
				} catch (Exception ex) {
					Console.WriteLine(ex.Message);
				}
			}
			return new List<DriverPayments>();
		}

		public static List<DriverPayments> GetTransHistoryChargesForHandHelds(long pDriverID, DateTime pStartDate, DateTime pEndDate, bool pIncludeTest) {
			string sql = @"SELECT DISTINCT  DriverPayments.*
							FROM    DriverPayments
							LEFT JOIN DriverPaymentAux ON DriverPaymentAux.DriverPaymentID = DriverPayments.DriverPaymentID
							WHERE   DriverID = @DriverID
									AND ChargeDate >= @StartDate
									AND ChargeDate <= @EndDate
									AND Fare > 0
									AND Failed = 0
									AND (Test = 0 OR Test = @Test)
									AND ISNULL(DriverPayments.DoNotPay, 0) = 0 AND ISNULL(DriverPaymentAux.DoNotPay, 0) = 0
									AND DriverPayments.TransNo NOT IN (SELECT ValueString FROM DriverPaymentRef WHERE ReferenceGroup = 'Refund' AND ReferenceKey = 'Original TransNo')
							ORDER BY ChargeDate DESC";

			using (var conn = SqlHelper.OpenSqlConnection()) {
				return conn.Query<DriverPayments>(sql, new { DriverID = pDriverID, StartDate = pStartDate, EndDate = pEndDate, Test = pIncludeTest }).ToList();
			}

		}

		public static List<DriverPayments> GetPaymentByCardNo(Drivers pDriver, string pCardNumber, string pExcludeVoucherNo) {
			return GetPaymentByCardNo(pDriver, pCardNumber, pExcludeVoucherNo, null);
		}

		public static List<DriverPayments> GetPaymentByCardNo(Drivers pDriver, string pCardNumber, string pExcludeVoucherNo, DateTime? pStart) {
			string sCardNo = CardNumberToHash(pCardNumber);

			string sql = @"SELECT  *
							FROM    DriverPayments
							WHERE   DriverID = @DriverID
									AND Number = @Number
									AND Failed = 0
									AND Fare >= 0
									AND TransNo != @TransNo ";
			DynamicParameters parms = new DynamicParameters();
			parms.Add("DriverID", pDriver.DriverID);
			parms.Add("Number", sCardNo);
			parms.Add("TransNo", pExcludeVoucherNo);

			if (pStart.HasValue) {
				sql += "AND ChargeDate >= @Start";
				parms.Add("Start", pStart);
			}
			List<DriverPayments> paymentList = new List<DriverPayments>();
			using (var conn = SqlHelper.OpenSqlConnection()) {
				List<DriverPayments> oList = conn.Query<DriverPayments>(sql, parms).ToList();
				foreach (DriverPayments oPay in oList) {
					if (oPay.DecryptCardNumber() == pCardNumber) {
						paymentList.Add(oPay);
					}
				}
			}
			return paymentList;
		}

		public static List<DriverPayments> GetPaymentByCardNo(AffiliateDrivers pDriver, string pCardNumber, string pExcludeVoucherNo) {
			return GetPaymentByCardNo(pDriver, pCardNumber, pExcludeVoucherNo, null);
		}

		public static List<DriverPayments> GetPaymentByCardNo(AffiliateDrivers pDriver, string pCardNumber, string pExcludeVoucherNo, DateTime? pStart) {
			string sCardNo = CardNumberToHash(pCardNumber);

			string sql = @"SELECT  *
							FROM    DriverPayments
							WHERE   AffiliateDriverID = @AffiliateDriverID
									AND Number = @Number
									AND Failed = 0
									AND Fare >= 0
									AND TransNo != @TransNo ";
			DynamicParameters parms = new DynamicParameters();
			parms.Add("AffiliateDriverID", pDriver.AffiliateDriverID);
			parms.Add("Number", sCardNo);
			parms.Add("TransNo", pExcludeVoucherNo);

			if (pStart.HasValue) {
				sql += "AND ChargeDate >= @Start";
				parms.Add("Start", pStart);
			}

			List<DriverPayments> paymentList = new List<DriverPayments>();
			using (var conn = SqlHelper.OpenSqlConnection()) {
				List<DriverPayments> oList = conn.Query<DriverPayments>(sql, parms).ToList();
				foreach (DriverPayments oPay in oList) {
					if (oPay.DecryptCardNumber() == pCardNumber) {
						paymentList.Add(oPay);
					}
				}
			}
			return paymentList;
		}

		/// <summary>
		/// Gets the pre auth record.
		/// </summary>
		/// <param name="pDriver">The driver.</param>
		/// <param name="pCardNumber">The card number.</param>
		/// <returns></returns>
		public static DriverPayments GetPreAuthRecord(Drivers pDriver, string pCardNumber) {
			string sCardNo = CardNumberToHash(pCardNumber);

			string sql = @"SELECT  *
							FROM    DriverPayments
							WHERE   DriverID = @DriverID
									AND Number = @Number
									AND Failed = 0
									AND Fare = 0";

			using (var conn = SqlHelper.OpenSqlConnection()) {
				DynamicParameters parms = new DynamicParameters();
				parms.Add("DriverID", pDriver.DriverID);
				parms.Add("Number", sCardNo);

				List<DriverPayments> oList = conn.Query<DriverPayments>(sql, parms).ToList();
				foreach (DriverPayments oPay in oList) {
					if (oPay.DecryptCardNumber() == pCardNumber) {
						return oPay;
					}
				}
			}
			return GetNullEntity();
		}

		/// <summary>
		/// Gets the pre auth record.
		/// </summary>
		/// <param name="pDriver">The driver.</param>
		/// <param name="pCardNumber">The card number.</param>
		/// <returns></returns>
		public static DriverPayments GetPreAuthRecord(AffiliateDrivers pDriver, string pCardNumber) {
			string sCardNo = CardNumberToHash(pCardNumber);

			string sql = @"SELECT  *
							FROM    DriverPayments
							WHERE   AffiliateDriverID = @DriverID
									AND Number = @Number
									AND Failed = 0
									AND Fare = 0";

			using (var conn = SqlHelper.OpenSqlConnection()) {
				DynamicParameters parms = new DynamicParameters();
				parms.Add("DriverID", pDriver.AffiliateDriverID);
				parms.Add("Number", sCardNo);

				List<DriverPayments> oList = conn.Query<DriverPayments>(sql, parms).ToList();
				foreach (DriverPayments oPay in oList) {
					if (oPay.DecryptCardNumber() == pCardNumber) {
						return oPay;
					}
				}
			}
			return GetNullEntity();
		}


		private DriverPaymentResults ProcessResponse(DriverPromotions pPromo, string pTrxType) {
			return ProcessResponse(pPromo, pTrxType, 0);
		}


		// *************************************************************************************************************
		// Process the CC response for promos
		// *************************************************************************************************************
		private DriverPaymentResults ProcessResponse(DriverPromotions pPromo, string pTrxType, long pAffiliateDriverID) {
			using (var conn = SqlHelper.OpenSqlConnection()) {

				if (this.DriverPaymentNotes.IsNullEntity) {
					mDriverPaymentNotes = DriverPaymentNotes.Create(this);
				}

				try {
					DriverPaymentResults oResult = DriverPaymentResults.Create(this);
					//oResult.Number = pReserve["CardNumber"].ToString();
					oResult.Result = "0";
					oResult.Reference = "Promo";
					oResult.Message = "Promo";
					oResult.AuthCode = "Promo";
					oResult.CVV2Match = "Y";
					oResult.AVSZIP = "Y";

					if (this.Affiliate.AffiliateDriverDefaults.TaxiPayCVV && !pPromo.CardCVV.IsNullOrEmpty() && this.CVV != pPromo.CardCVV) {
						oResult.Result = "1001";
						oResult.Message = "Invalid CVV";
						oResult.CVV2Match = "N";
					}
					if (!string.IsNullOrEmpty(this.CardZIPCode)) {
						if (this.CardZIPCode != pPromo.BillingZip) {
							oResult.Result = "1002";
							oResult.Message = "Invalid ZipCode";
							oResult.AVSZIP = "N";
						}
					}
					if (!pPromo.CardExpires.IsNullOrEmpty()) {
						string promoExpMonth = pPromo.CardExpires.Left(2);
						int year = Convert.ToInt32(pPromo.CardExpires.Right(2));
						if (year < 2000) {
							year += 2000;
						}
						string promoDate = promoExpMonth + year.ToString();
						if (this.Expiration != promoDate) {
							oResult.Result = "1005";
							oResult.Message = "Invalid Expiry Date";
						}
					}
					oResult.TransType = pTrxType;
					oResult.TransDate = Affiliate.LocalTime;
					oResult.CreditCardProcessor = TaxiPassCommon.Banking.CardProcessors.DriverPromo.ToString();
					oResult.Save();

					this.CreditCardProcessor = TaxiPassCommon.Banking.CardProcessors.DriverPromo.ToString();
					if (oResult.Result != "0") {
						this.Notes = oResult.Message;
						this.Failed = true;
					} else {
						this.Failed = false;
						if (pTrxType == VerisignTrxTypes.Authorization) {
							if (!AuthDate.HasValue) {
								this.AuthDate = this.Affiliate.LocalTime;
							}

						} else {
							if (pTrxType == VerisignTrxTypes.Credit) {
								if (!this.ChargeDate.HasValue) {
									this.ChargeDate = Affiliate.LocalTime;
								}
							} else {
								this.ChargeDate = Affiliate.LocalTime;
							}

							if (this.Platform != Platforms.Redeemer.ToString()) {
								if (this.Drivers.DriverBankInfos.Count > 0) {
									DriverBankInfo oBank = this.Drivers.DriverBankInfos[0];
									//if (oBank.BankAccountNumber != "" && oBank.BankCode != "" && oBank.MerchantID != null && oBank.MerchantID > 0) {
									if (oBank.BankAccountNumber != "" && oBank.BankCode != "") {
										if (!this.Drivers.PaidByRedeemer && !this.AffiliateDrivers.PaidByRedeemer && this.AffiliateDrivers.TransCardNumber.IsNullOrEmpty()) {
											SetDriverPaid(true);
										}
									}
								}
								if (this.Drivers.MyAffiliate != null && this.Drivers.MyAffiliate.AutoPayFleet) {
									if (!this.Drivers.PaidByRedeemer && !this.AffiliateDrivers.PaidByRedeemer) {
										SetDriverPaid(false);
									}
									this.AffiliateID = this.Drivers.MyAffiliate.AffiliateID;

								}
							}
						}
					}
					this.ReferenceNo = "Promo: " + pPromo.DriverPromotionID.ToString() + (pAffiliateDriverID != 0 ? " | " + pAffiliateDriverID.ToString("F0") : "");
					//set TP fee to 0 to ensure books are correct
					this.TaxiPassFee = 0;
					this.Notes = "";
					this.Save();

					return oResult;
				} catch (Exception ex) {
					this.Notes += ex.Message;
					this.Save();
					throw ex;
				}

			}
		}


		// *************************************************************************************************************************************
		// Process charge/preauth response
		// *************************************************************************************************************************************
		public DriverPaymentResults ProcessResponse(ProcessorResponse pResponse, string pTrxType, decimal pAmount, DriverPaymentResults pChargeAuthResults) {

			DriverPaymentResults oResult = DriverPaymentResultsTable.GetNullEntity();
			try {
				using (var conn = SqlHelper.OpenSqlConnection()) {
					this.AffiliateID = this.Drivers.MyAffiliate.AffiliateID;
					oResult = DriverPaymentResults.Create(this);
					oResult.Amount = pAmount;
					oResult.Result = pResponse.Result;
					oResult.Message = pResponse.Message;


					if (("" + oResult.Message).Contains("APPROVAL (00)") || ("" + oResult.Message).Equals("Transaction Approved", StringComparison.CurrentCultureIgnoreCase)) {
						oResult.Result = "0";
						oResult.Message = "Approved";
					}

					oResult.Reference = pResponse.Reference;

					oResult.AuthCode = pResponse.AuthCode;
					oResult.AVS = pResponse.AVS;
					oResult.AVSZIP = pResponse.AVSZip;
					oResult.CVV2Match = pResponse.CVV2Match;

					// 12/5/11 Added for Carmel
					oResult.AVSResultCode = pResponse.AVSResultCode;

					// copied data from Auth for Carmel as they were complaining that we were not sending them the data
					// USAePay only sends the data once if during the preAuth
					if (pChargeAuthResults != null && !pChargeAuthResults.IsNullEntity) {
						if (oResult.AVS.IsNullOrEmpty()) {
							oResult.AVS = pChargeAuthResults.AVS;
						}
						if (oResult.AVSZIP.IsNullOrEmpty()) {
							oResult.AVSZIP = pChargeAuthResults.AVSZIP;
						}
						if (oResult.CVV2Match.IsNullOrEmpty()) {
							oResult.CVV2Match = pChargeAuthResults.CVV2Match;
						}
						if (oResult.AVSResultCode.IsNullOrEmpty()) {
							oResult.AVSResultCode = pChargeAuthResults.AVSResultCode;
						}
					}


					oResult.TransType = pTrxType;
					oResult.TransDate = Affiliate.LocalTime;

					// store processor for refunds and charge backs
					oResult.CreditCardProcessor = pResponse.CreditCardProcessor.ToString();
					this.CreditCardProcessor = oResult.CreditCardProcessor;

					if (!pResponse.BillingDescriptor.IsNullOrEmpty()) {
						var refRec = DriverPaymentRef.GetCreate(this, "BillingDescriptor", "BillingDescriptor");
						refRec.ValueString = pResponse.BillingDescriptor;
						refRec.Save();
					}

					//if (pResponse.CreditCardProcessor.ToString().StartsWith("ProPay")) {
					//    if (this.Affiliate.DefaultPlatform.Equals(Platforms.SplitPay)) {

					//    }
					//}

					bool isStoreForward = SetStoreForward();
					// store processor for refunds and charge backs
					if (pChargeAuthResults != null && pChargeAuthResults.DriverPayments.TaxiPassRedeemerID.HasValue && ("" + oResult.Message).Equals("Transactions has already been settled.", StringComparison.CurrentCultureIgnoreCase)) {
						this.Failed = false;
						oResult.Result = "0";
						oResult.Message = "Approved";
					}
					if ("" + oResult.Result != "0") {
						if (this.DriverPaymentNotes.IsNullEntity) {
							mDriverPaymentNotes = DriverPaymentNotes.Create(this);
						}
						this.Notes = oResult.Message;
						if (("" + this.DriverPaymentNotes.Reason).Contains("Requires Voice Authentication")) {
							this.Notes = "Card Declined";
						}

						if (pTrxType != "V" || (pTrxType == "V" && !oResult.Message.Equals("26: The batch has already been closed.  Please apply a credit instead."))) {
							this.Failed = true;
						}
						if (isStoreForward && Fare > 0) {
							if (this.Affiliate.AffiliateDriverDefaults.StoreForward > 0) {
								this.Failed = false;

								UpdateStoreForwardDeclined(true);
							}
						}

					} else {
						this.Failed = false;
						if (isStoreForward && Fare > 0) {
							UpdateStoreForwardDeclined(false);
						}

						DriverPaymentNotes oNotes = this.DriverPaymentNotes;
						if (oNotes.IsNullEntity) {
							oNotes = DriverPaymentNotes.Create(this);
							mDriverPaymentNotes = oNotes;
						}
						oNotes.Reason = "";

						if (pTrxType == VerisignTrxTypes.Authorization || pTrxType == VerisignTrxTypes.Inquiry) {
							if (!AuthDate.HasValue) {
								this.AuthDate = this.Affiliate.LocalTime;
							}
							if (!this.CardSwiped) {
								// 12/2 New flag for Carmel
								if (this.Affiliate.AffiliateDriverDefaults.IgnoreFailedZipOnCharge.GetValueOrDefault(false)) {
									this.IgnoreCardZIP = true;
								}

								// simulate Failed trans for test cards when CVV > 500
								if (Test && !CVV.IsNullOrEmpty() && Convert.ToInt32(CVV) > 500 && !this.CreditCardProcessor.Contains("ProPay")) {
									this.Failed = true;
									oResult.Result = "-1";
									oResult.Message = "Invalid CVV";
									oNotes.Reason = oResult.Message;
								}
								//if (!this.IgnoreCardZIP.GetValueOrDefault(false) && !string.IsNullOrEmpty(this.CardZIPCode)) {
								//    if (!oResult.AVSZIP.IsNullOrEmpty() && oResult.AVSZIP.Equals("N", StringComparison.CurrentCultureIgnoreCase)) {
								//        this.Failed = true;
								//        oResult.Result = "-1";
								//        oResult.Message = "Invalid Billing Zip Code";
								//        oNotes.Reason = oResult.Message;
								//        //oResult.Save();
								//    } else {
								//        if (this.Test && !string.IsNullOrEmpty(this.CardZIPCode)) {
								//            try {
								//                int zip = Convert.ToInt32(this.CardZIPCode);
								//                if (zip >= 50000) {
								//                    this.Failed = true;
								//                    oResult.Result = "-1";
								//                    oResult.Message = "Invalid Billing Zip Code";
								//                    oNotes.Reason = oResult.Message;
								//                    //oResult.Save();
								//                }
								//            } catch {
								//            }
								//        }
								//    }

								//}
							}
							if ("" + oResult.Result == "0") {
								this.AuthAmount = pAmount;
							}
						} else if (pTrxType == VerisignTrxTypes.Void) {
							if (oResult.Result == "0") {
								// ensure transaction is not paid out by failing voucher
								this.Failed = true;
								this.DoNotPay = true;
								DriverPaid = false;
							}

						} else {
							if (pTrxType == VerisignTrxTypes.Credit) {
								if (!this.ChargeDate.HasValue) {
									this.ChargeDate = Affiliate.LocalTime;
								}
							} else {
								this.ChargeDate = Affiliate.LocalTime;
							}
							if ("" + this.Platform != Platforms.Redeemer.ToString()) {
								if (this.Drivers.DriverBankInfos != null && this.Drivers.DriverBankInfos.Count > 0) {
									DriverBankInfo oBank = this.Drivers.DriverBankInfos[0];
									if (oBank.BankAccountNumber != "" && oBank.BankCode != "" && this.AffiliateDrivers.TransCardNumber.IsNullOrEmpty()) {
										SetDriverPaid(true);
									}
								}
								SetDriverPaid(false);
							}
						}
					}

					bool bSaved = false;
					int i = 0;
					while (!bSaved) {
						try {
							this.Save();
							bSaved = true;
						} catch (Exception ex) {
							Console.WriteLine(ex.Message);
							//force system to save
							//chances are system deadlocked
						}
						i++;
						if (i > 3) {
							break;
						}
					}
					return oResult;
				}
			} catch (Exception ex) {

				if (ex.Message.StartsWith("Thread was being aborted")) {
					using (var conn = SqlHelper.OpenSqlConnection()) {
						var resp = DriverPaymentResults.GetResults(this.DriverPaymentID);
						if (resp.Count == 0 || resp.FirstOrDefault(p => p.Reference.Equals(pChargeAuthResults.Reference)) == null) {
							Thread.Sleep(1000);
							return ProcessResponse(pResponse, pTrxType, pAmount, pChargeAuthResults);
						}
					}
				} else if (ex.Message.Contains("String or binary data would be truncated") && oResult.DriverPaymentResultID == 0) {
					if (FixDPResult(this.TransNo, JsonConvert.SerializeObject(pResponse)) > 0) {
						return oResult;
					}
				}

				StringBuilder sHtml = new StringBuilder();
				sHtml.AppendLine("<hr noshade color=#000000 size=1>");
				sHtml.AppendLine(DateTime.Now.ToString() + " CardProcessingException error<br>");
				sHtml.AppendLine("<br>** Error Info **<br>");
				sHtml.AppendLine("Method: ProcessResponse<br>");
				sHtml.AppendLine("Message: " + ex.Message + "<br>");

				sHtml.AppendLine("<br>** TransNo **<br>" + this.TransNo + "<br>");
				sHtml.AppendLine("<br>** Response **<br>" + JsonConvert.SerializeObject(pResponse) + "<br>");

				var cred = SystemDefaultsDict.GetEmailCreds(EMailReportFrom.Report);
				TaxiPassCommon.Mail.EMailMessage oMail = new TaxiPassCommon.Mail.EMailMessage(cred.From, string.Format("{0} Error", "DriverPayments Dapper"), sHtml.ToString(), cred.Password);
				oMail.UseWebServiceToSendMail = true;
				oMail.SendMail(Properties.Settings.Default.EMailErrorsTo);

				this.Notes += ex.Message;
				this.Save();
				throw ex;
			}

		}

		private long FixDPResult(string pTransNo, string pResult) {
			long resultID = 0;
			try {
				using (var conn = SqlHelper.OpenSqlConnection()) {
					CabRideEngineDapper.DriverPayments payRec = CabRideEngineDapper.DriverPayments.GetPayment(conn, pTransNo);

					var data = JsonConvert.DeserializeObject<CabRideEngineDapper.DriverPaymentResults>(pResult);

					var result = CabRideEngineDapper.DriverPaymentResults.Create(payRec);
					result.Amount = data.Amount;
					result.AuthCode = data.AuthCode;
					result.AVS = data.AVS;
					result.AVSAddr = data.AVSAddr;
					result.AVSResultCode = data.AVSResultCode;
					result.AVSZIP = data.AVSZIP;
					result.CompleteResultCode = data.CompleteResultCode;
					result.CreditCardProcessor = data.CreditCardProcessor.ToEnum<TaxiPassCommon.Banking.CardProcessors>().ToString();
					result.CVV2Match = data.CVV2Match;
					result.Message = data.Message;
					result.Reference = data.Reference;
					result.Result = data.Result;
					result.TransDate = data.TransDate;
					result.TransType = data.TransType;
					try {
						resultID = result.Save();
					} catch (Exception ex) {
						Console.WriteLine(ex.Message);
					}
				}
			} catch (Exception) {
				resultID = 0;
			}
			return resultID;
		}


		private bool SetStoreForward() {
			bool isStoreForward = ("" + this.ReferenceNo).StartsWith("StoreForward:");
			if (isStoreForward) {
				//DriverPaymentRef sfRef = DriverPaymentRef.GetCreate(this, "StoreForward", "IsStoreForward");
				//sfRef.ValueString = true.ToString();
				//sfRef.Save();
				StoreForward = true;
				try {
					List<string> data = ReferenceNo.Split('-').ToList();
					if (data.Count == 3) {
						string myDate = data[1].Trim();
						int year = Convert.ToInt32(myDate.Left(4));
						myDate = myDate.Remove(0, 4);
						int month = Convert.ToInt32(myDate.Left(2));
						myDate = myDate.Remove(0, 2);
						int day = Convert.ToInt32(myDate.Left(2));
						myDate = myDate.Remove(0, 2);
						int hour = Convert.ToInt32(myDate.Left(2));
						myDate = myDate.Remove(0, 2);
						int minute = Convert.ToInt32(myDate);

						StoreForwardDate = new DateTime(year, month, day, hour, minute, 0);
					}
				} catch (Exception) {
				}
			}
			return isStoreForward;
		}

		private void UpdateStoreForwardDeclined(bool pDeclined) {
			bool sendEMail = false;
			DriverPaymentRef sfDeclinedRef = DriverPaymentRef.GetCreate(this, "StoreForward", "Declined");
			sendEMail = sfDeclinedRef.RowState == DataRowState.Added;
			sfDeclinedRef.ValueString = pDeclined.ToString();
			sfDeclinedRef.Save();
			SetWriteOff = pDeclined;

			if (pDeclined && !this.Test) {
				string note = $"{this.Affiliate.LocalTime.ToString("MM/dd/yyyy HH:mm tt")} RedeemerStopPayment due to StoreForward Declined";
				this.AffiliateDrivers.Notes = $"{note}\r\n{this.AffiliateDrivers.Notes}";
				this.AffiliateDrivers.RedeemerStopPayment = true;
				this.AffiliateDrivers.StopPaymentDate = this.Affiliate.LocalTime;
				this.AffiliateDrivers.Save();
				using (var conn = SqlHelper.OpenSqlConnection()) {
					AffiliateDriverRef drvRef = AffiliateDriverRef.GetOrCreate(conn, this.AffiliateDriverID.Value, "StopPayment", "Note");
					drvRef.ValueString = note;
					drvRef.Save();
				}
			}

			//Send EMAIL out
			if (sendEMail && pDeclined) {
				using (var conn = SqlHelper.OpenSqlConnection()) {
					SystemDefaultsDict dict = SystemDefaultsDict.GetByKey(conn, "StoreForward", "EMailFailedTransTo", false);
					if (dict.IsNullEntity || dict.ValueString.IsNullOrEmpty()) {
						return;
					}
					StringBuilder msg = new StringBuilder();
					msg.AppendLine(string.Format("Driver's Name: {0}<br />", this.AffiliateDrivers.Name));
					msg.AppendLine(string.Format("Phone Number: {0}<br /><br />", this.AffiliateDrivers.Cell));
					msg.AppendLine(string.Format("Voucher Number: {0}<br />", this.TransNo));
					msg.AppendLine(string.Format("Driver Total: {0}<br />", this.DriverTotal.ToString("C")));

					var cred = SystemDefaultsDict.GetEmailCreds(EMailReportFrom.Report);
					TaxiPassCommon.Mail.EMailMessage email = new TaxiPassCommon.Mail.EMailMessage(cred.From, "Failed StoreForward", msg.ToString(), cred.Password);
					email.UseWebServiceToSendMail = Properties.Settings.Default.EMailViaWebService;
					email.SendMail(dict.ValueString);
				}
			}
		}




		public static string ProcessMessage(string pMsg, DriverPayments pPayment, bool pPhoneticise) {
			return ProcessMessage(pMsg, pPayment, GetNullEntity(), pPhoneticise);
		}


		public static string ProcessMessage(string pMsg, DriverPayments pPayment, DriverPayments pReferredPayment, bool pPhoneticise) {
			//If we want to do any changes in the msg ("comes from driverpromotions table") we want to send we need to put DriverPayment or Referred titles to the begining of the objects to specify where it is belong to. 
			if (pMsg.IsNullOrEmpty()) {
				return pMsg;
			}

			MatchCollection oMatch = new Regex("{{[^}}]*}}|\\[[^]]*]", RegexOptions.Multiline | RegexOptions.IgnoreCase).Matches(pMsg);
			foreach (Match match1 in oMatch) {
				string sText = match1.Value;

				if (sText.StartsWith("{")) {
					sText = sText.Replace("{", "");
					sText = sText.Replace("}", "");
					try {
						DriverPayments myRec = pPayment;
						if (sText.StartsWith("Referred")) {//this logic added if we want to pull referred information
							myRec = pReferredPayment;
						}
						object oObj = CabRideEngineDapper.Utils.EMailMessage.Eval(sText.Split('.'), myRec);
						string sValue = "";
						if (oObj != null) {
							sValue = oObj.ToString();
						}
						if (pPhoneticise) {
							if (sText.Contains("TransNo")) {
								sValue = StringUtils.Phoneticise(sValue, true);
							}
						}
						pMsg = pMsg.Replace(match1.Value, sValue);

					} catch (Exception ex) {
						throw new Exception("Error returned while parsing " + sText + ": " + ex.Message.ToString());
					}



				}
			}
			return pMsg;
		}


		public DriverPaymentResults ChargeCard(bool pForceCharge) {
			return ChargeCard("", pForceCharge);
		}

		public DriverPaymentResults ChargeCard() {
			return ChargeCard("", false);
		}

		public DriverPaymentResults ChargeCard(string pCardInfo) {
			return ChargeCard(pCardInfo, false);
		}


		// *************************************************************************
		// Charge the Card
		// *************************************************************************
		public bool JobAdjustment = false;
		public DriverPaymentResults ChargeCard(string pCardInfo, bool pForceNewCharge) {
			DateTime elapsedStart = DateTime.Now;

			if (!BankAccountNo.IsNullOrEmpty() && pCardInfo.IsNullOrEmpty() && CardNumber.IsNullOrEmpty()) {
				return ChargeCheck();
			}

			#region Validations
			// Put in to trap bogus charge card calls
			if (Platform.IsNullOrEmpty()) {
				Platform = this.Affiliate.DefaultPlatform;
			}
			if ((this.Fare == 0 || Fare < this.Affiliate.AffiliateDriverDefaults.StartingFare) && !JobAdjustment) {
				string msg = string.Format("{1} must be greater than {0}", this.Affiliate.AffiliateDriverDefaults.StartingFare.ToString("C"), Platform.Equals(Platforms.Dialie.ToString(), StringComparison.CurrentCultureIgnoreCase) || Platform.Equals(Platforms.ChargePass.ToString(), StringComparison.CurrentCultureIgnoreCase) ? "Amount" : "Fare");
				if (this.DriverPaymentID > 0) {
					this.Notes = msg;
					this.Save();
				}
				throw new Exception(msg);
			}


			// only allow this one test card for Carmel
			string cardNo = this.DecryptCardNumber();
			if (CardUtils.IsTestCard(cardNo) && this.AffiliateID == 161 && (!cardNo.Equals("4000200011112222") || this.CVV != "166" || this.DecryptCardExpiration() != "122030")) {
				string msg = "Cannot charge Test Card";
				if (this.DriverPaymentID > 0) {
					this.Notes = msg;
					this.Save();
				}
				throw new Exception(msg);
			}

			if (AffiliateDriverID.HasValue) {
				string msg = string.Format("Affiliate Driver: {0}-{1},  Account Frozen cannot charge card", AffiliateDriverID.Value, AffiliateDrivers.Name);
				if (!AffiliateDrivers.Active) {
					if (this.DriverPaymentID > 0) {
						this.Notes = msg;
						this.Save();
					}
					throw new Exception(msg);
				}
			} else {
				if (!Drivers.Active) {
					string msg = string.Format("Driver: {0}-{1},  Account Frozen cannot charge card", DriverID, Drivers.Name);
					if (this.DriverPaymentID > 0) {
						this.Notes = msg;
						this.Save();
					}
					throw new Exception(msg);
				}
			}


			//failed until card is charged
			this.Failed = true;

			// hg 2011-07/28 Force a save to prevent incorrect charges from happening
			// Always save first to avoid incorrect voucher numbers going to Credit card processor
			if (this.IsNewRecord) {
				int tries = 0;
				while (tries < 3) {
					tries++;
					try {
						this.Save("Force Saved");
						break;
					} catch (Exception ex) {
						if (ex.Message.Equals("Query timeout expired")) {
							continue;
						}
						throw new Exception(ex.Message);
					}
				}
				CreateChargeRecords();
				//var db = CabRideDrivenDB.Utils.SqlHelper.OpenCabRideDB();
				using (var conn = SqlHelper.OpenSqlConnection()) {
					//db.WriteEntities(ChargeList);
					foreach (var rec in ChargeList) {
						rec.Save(conn, CabRideDapperSettings.LoggedInUser);
					}
				}
			}

			MyTimeStats.Key = this.TransNo;




			//PreAuth card 1st if manual entry and zip code is present to ensure card is valid before charging
			//if (!this.AuthDate.HasValue && !this.CardSwiped) {
			//    if (!string.IsNullOrEmpty(this.CardZIPCode)) {
			//        if (!this.IgnoreCardZIP.GetValueOrDefault(true)) {
			//            if (this.AuthAmount == 0 || AuthAmount < DriverTotal) {
			//                this.AuthAmount = DriverTotal; // TotalCharge;  use Driver total as PreAuth up's it by the TaxiPass Fee
			//            }
			//            DriverPaymentResults preAuthResult = PreAuthCard();
			//            if (!preAuthResult.Result.Equals("0")) {
			//                return preAuthResult;
			//            }
			//        }
			//    }
			//}

			CardProcessingFailureTypes oProcess = CardProcessingFailureTypes.OKToProcess;

			if (!BypassFraudCheck) {
				BypassFraudCheck = this.Affiliate.AffiliateDriverDefaults.ByPassFraudCheck;
			}


			if (!BypassFraudCheck && !this.Test) {
				oProcess = CanProcessCard(this);
			}

			switch (oProcess) {
				case CardProcessingFailureTypes.MaxDailyAmount:
				case CardProcessingFailureTypes.MaxMonthlyAmount:
				case CardProcessingFailureTypes.MaxTransPerMonth:
				case CardProcessingFailureTypes.MaxTransPerWeek:
				case CardProcessingFailureTypes.MaxDriverTransactions:
				//case CardProcessingFailureTypes.MaxCardTransactions:
				//case CardProcessingFailureTypes.MaxTransPerMonthPerCard:
				//case CardProcessingFailureTypes.MaxTransPerWeekPerCard:
				case CardProcessingFailureTypes.MaxWeeklyAmount:
				case CardProcessingFailureTypes.MaxDailyDeclineExceeded:
				case CardProcessingFailureTypes.MaxWeeklyDeclineExceeded:
				case CardProcessingFailureTypes.MaxMonthlyDeclineExceeded:
				case CardProcessingFailureTypes.AverageChargeExceeded:
				case CardProcessingFailureTypes.OKToProcess:

					//this.Reason = "Not yet processed";
					if (!this.ChargeDate.HasValue) {
						this.ChargeDate = this.Affiliate.LocalTime;
					}
					this.FraudFailure = false;

					if (oProcess != CardProcessingFailureTypes.OKToProcess) {
						CardProcessingException ex = new CardProcessingException("Credit Card Fraud Collars Tripped", oProcess, this.Drivers, this.AffiliateDrivers, Number);
						CheckSuspendAccount(ex);
						SendOutFraudMessage(false, ex.MyMessage);
					}
					break;

				default: {
						CardProcessingException ex = new CardProcessingException("Cannot Process Credit Card", oProcess, this.Drivers, this.AffiliateDrivers, Number);
						this.Notes = ex.MyMessage;
						this.DriverPaymentNotes.Save();
						this.ChargeDate = Affiliate.LocalTime;
						if (!this.Affiliate.AffiliateDriverDefaults.ExcludeFromRedeemerStopPayment.GetValueOrDefault(false)) {
							this.FraudFailure = true;
						}

						// make sure these columns are reset
						this.DriverPaid = false;
						this.DriverPaidDate = null;

						this.Save();
						CheckSuspendAccount(ex);
						SendOutFraudMessage(true);
						throw ex;
					}
			}

			using (var conn = SqlHelper.OpenSqlConnection()) {
				//Check BlackList
				if (BlackListedCard(conn, cardNo)) {
					this.Notes = "Blacklisted Card";
					this.DriverPaymentNotes.Save();
					throw new Exception("Card not accepted 6");
				}

				//Check BlackListBIN
				if (BlackListedBIN(cardNo)) {
					this.Notes = "Blacklisted BIN";
					this.DriverPaymentNotes.Save();
					throw new Exception("Card unaccepted. Please try a different card to charge.");
				}
				#endregion Validations

				//Is it a TaxiPass Card
				TaxiPassCards tpCard = TaxiPassCards.GetCardNo(conn, cardNo);
				#region TaxiPassCards
				if (!tpCard.IsNullEntity) {
					#region TaxiPassCard
					if (!tpCard.Active) {
						this.Failed = true;
						this.Notes = "Card not accepted";
						this.ChargeDate = Affiliate.LocalTime;
						this.Save();
						throw new Exception("Card not accepted 8");
					}


					DriverPaymentResults oResult = DriverPaymentResults.Create(this);

					oResult.Reference = "TP Card";
					oResult.TransType = VerisignTrxTypes.Sale.ToString().Substring(0, 1);
					oResult.TransDate = Affiliate.LocalTime;

					if (!this.IgnoreCardZIP.GetValueOrDefault(true)) {
						if (!string.IsNullOrEmpty(this.CardZIPCode) && !string.IsNullOrEmpty(tpCard.ZIPCode)) {
							if (tpCard.ZIPCode != StringUtils.CleanPhoneNumbers(this.CardZIPCode)) {
								this.Failed = true;
								this.Notes = "Invalid Billing Zip Code";
								this.DriverPaymentNotes.Save();
								this.Save();

								oResult.Result = "-1";
								oResult.Message = "Invalid Billing Zip Code";
								oResult.AVSZIP = "N";
								oResult.Save();

								this.Save();
								return oResult;

							}
							oResult.AVSZIP = "Y";
						}
					}

					//get last transaction
					TaxiPassCardTransactions tpTrans = TaxiPassCardTransactions.GetLastTrans(tpCard);
					if (tpTrans.Balance - this.TotalCharge < -tpCard.MaxCredit) {
						this.Failed = true;
						this.Notes = "Card Declined";
						this.Save();

						oResult.Result = "-2";
						oResult.Reference = "TP Card";
						oResult.TransType = VerisignTrxTypes.Authorization;
						oResult.TransDate = Affiliate.LocalTime;
						oResult.Message = "Insufficient funds";
						oResult.Save();

						this.Save();
						return oResult;
					}

					this.Save();
					oResult.AuthCode = StringUtils.GetUniqueKey(6);
					oResult.Message = "Approved";
					oResult.Result = "0";
					oResult.Save();

					this.Failed = false;
					this.Notes = "";
					this.Save();

					TaxiPassCardTransactions trans = TaxiPassCardTransactions.AddTransaction(tpCard, -this.TotalCharge, this.DriverPaymentID);
					return oResult;
					#endregion TaxiPassCard
				}
				#endregion TaxiPassCards

				DriverPaymentResults oPayResults = null;
				CardProcessors oProcessor = CardProcessors.Verisign;


				// if store forward check if another transaction took place via other entry system
				SetStoreForward();
				if (StoreForward) {
					if (StoreForwardDate.HasValue) {

						usp_SystemDefaultsDict_StoreForward sfRec = usp_SystemDefaultsDict_StoreForward.Execute(conn);

						string sql = @"SELECT  *
										FROM    dbo.DriverPayments
										WHERE   ChargeDate >= @StartDate
												AND ChargeDate <= @EndDate
												AND DriverPaymentID != @DriverPaymentID
												AND Number = @Number";

						DynamicParameters parms = new DynamicParameters();
						parms.Add("StartDate", StoreForwardDate.Value.AddHours(-sfRec.DupeCheckInHoursAsInt));
						parms.Add("EndDate", StoreForwardDate.Value.AddHours(sfRec.DupeCheckInHoursAsInt));
						parms.Add("Number", Number);
						parms.Add("DriverPaymentID", DriverPaymentID);

						if (!sfRec.DupeCheckCardAcrossAllDriversAsBool) {
							sql = string.Format("{0} AND AffiliateDriverID = @AffiliateDriverID", sql);
							parms.Add("AffiliateDriverID", AffiliateDriverID);
						}

						if (sfRec.MatchFareAmountAsBool) {
							sql = string.Format("{0} AND Fare = @Fare", sql);
							parms.Add("Fare", Fare);
						}
						List<CabRideEngineDapper.DriverPayments> payList = conn.Query<CabRideEngineDapper.DriverPayments>(sql, parms).ToList();
						if (payList.Count > 0) {
							string myNotes = string.Format("Duplicate StoreForward of {0}", payList[0].TransNo);
							Notes = myNotes;
							var resp = CreateDummyProcessorResponse();
							resp.Message = myNotes;
							DriverPaymentResults myResults = ProcessResponse(resp, VerisignTrxTypes.Sale, TotalCharge, null);
							Failed = true;
							Notes = myNotes;
							Save();
							return myResults;
						}

					}
				}

				//Is it a promo payment card
				List<DriverPromotions> promoList = DriverPromotions.GetPromotionsByCardNo(conn, this.DecryptCardNumber());
				if (promoList.Count > 0) {
					oProcessor = CardProcessors.DriverPromo;
				} else {
					if (this.AuthAmount > 0) {
						// find auth and use this to charge the transaction
						oProcessor = CardProcessors.USAePay;
						if (!this.DriverPaymentAuthResults.CreditCardProcessor.IsNullOrEmpty()) {
							try {
								oProcessor = this.DriverPaymentAuthResults.CreditCardProcessor.ToEnum<CardProcessors>();
							} catch (Exception) {

							}
						}
					} else {
						// 4/23/2013 probably obe
						if (((CardUtils.IsTestCard(cardNo) && !this.Drivers.MyAffiliate.MyVerisignAccount.EPayTestURL.IsNullOrEmpty())
							|| (!CardUtils.IsTestCard(cardNo) && !this.Drivers.MyAffiliate.MyVerisignAccount.EPayProdURL.IsNullOrEmpty()))) {
							oProcessor = CardProcessors.USAePay;
						}
					}
				}

				DriverPayments splitFeeRec = null;
				if (oProcessor == CardProcessors.DriverPromo) {
					oPayResults = DoDriverPromoCharge(promoList, cardNo, pCardInfo);
				} else {
					string binCardNo = this.DecryptCardNumber();
					//CardBin bin = CardBin.GetCardBinInfo(conn, binCardNo);
					//if (bin.IsNullEntity) {
					//    string url = "";
					//}
					IsDebitCard = binCardNo.IsDebitCard(); //  ("" + bin.DebitCredit).Equals("DEBIT", StringComparison.CurrentCultureIgnoreCase);

					bool splitFee = false;
					VerisignAccountID = CreditCardUtils.GetDriverPaymentCardGatewayGroupsID(conn, this.TransNo, 0);
					if (UseFraudGateway) {
						bool doFraud = true;
						if (CVV.IsNullOrEmpty() && this.CardZIPCode.IsNullOrEmpty()) {
							AffiliateRef affRec = AffiliateRef.GetByGroupReference(conn, this.AffiliateID.Value, "Gateways", "No CVV and ZIP");
							if (!affRec.ValueString.IsNullOrEmpty() && affRec.ValueString.IsNumeric() && affRec.ValueString.ToInt() > 0) {
								VerisignAccountID = affRec.ValueString.ToInt();
								doFraud = false;
							}
						}
						if (doFraud) {
							AffiliateRef affRec = AffiliateRef.GetByGroupReference(conn, this.AffiliateID.Value, "Gateways", "Fraud");
							if (!affRec.ValueString.IsNullOrEmpty() && affRec.ValueString.IsNumeric() && affRec.ValueString.ToInt() > 0) {
								VerisignAccountID = affRec.ValueString.ToInt();
							} else if (this.Affiliate.FraudVerisignAccountID.GetValueOrDefault(0) > 0) {
								VerisignAccountID = this.Affiliate.FraudVerisignAccountID.Value;
							}
						}
					} else if (IsDebitCard) {
						AffiliateRef affRec = AffiliateRef.GetByGroupReference(conn, this.AffiliateID.Value, "Gateways", "Debit");
						if (!affRec.ValueString.IsNullOrEmpty() && affRec.ValueString.IsNumeric() && affRec.ValueString.ToInt() > 0) {
							VerisignAccountID = affRec.ValueString.ToInt();
						}
					} else if (("" + this.CardType).Equals("eCheck", StringComparison.CurrentCultureIgnoreCase) || CardNumberDisplay.StartsWith("CK ", StringComparison.CurrentCultureIgnoreCase)) {
						AffiliateRef affRec = AffiliateRef.GetByGroupReference(conn, this.AffiliateID.Value, "Gateways", "eCheck");
						if (!affRec.ValueString.IsNullOrEmpty() && affRec.ValueString.IsNumeric() && affRec.ValueString.ToInt() > 0) {
							VerisignAccountID = affRec.ValueString.ToInt();
						}
					} else if (("" + this.CardType).Equals("GooglePay", StringComparison.CurrentCultureIgnoreCase)) {
						AffiliateRef affRec = AffiliateRef.GetByGroupReference(conn, this.AffiliateID.Value, "Gateways", "Google Pay");
						if (!affRec.ValueString.IsNullOrEmpty() && affRec.ValueString.IsNumeric() && affRec.ValueString.ToInt() > 0) {
							VerisignAccountID = affRec.ValueString.ToInt();
						}
					}

					if (DriverFee > 0 && TaxiPassFee == 0) {
						AffiliateRef affRec = AffiliateRef.GetByGroupReference(conn, this.AffiliateID.Value, "Gateways", "Merchant Pays");
						if (!affRec.ValueString.IsNullOrEmpty() && affRec.ValueString.IsNumeric() && affRec.ValueString.ToInt() > 0) {
							VerisignAccountID = affRec.ValueString.ToInt();
						}
					} else {
						if (!CardType.Equals("eCheck", StringComparison.CurrentCultureIgnoreCase)) {
							AffiliateRef affRec = AffiliateRef.GetByGroupReference(conn, this.AffiliateID.Value, "Gateways", "SplitFee");
							if (!affRec.ValueString.IsNullOrEmpty() && affRec.ValueString.IsNumeric() && affRec.ValueString.ToInt() > 0) {
								splitFee = true;
							}
						}
					}




					if (VerisignAccountID == 0 && !Test) {
						string msg = "No Gateway assigned to Affiliate " + this.MyAffiliateName;
						this.Notes = msg;
						this.DriverPaymentNotes.Save();
						throw new Exception(msg);
					}

					CreditCardChargeType chargeType = CreditCardChargeType.ECommerce;
					if (Platform != Platforms.GetRide.ToString()) {
						if (CardSwiped) {
							chargeType = CreditCardChargeType.Swipe;
						} else {
							chargeType = CreditCardChargeType.Manual;
						}
					}
					Gateway gway = new Gateway(Utils.CreditCardUtils.LoadGateWays(conn, this.VerisignAccountID.Value, chargeType, CardType));
					usp_SystemDefaultsDialie dialieDefaults = new usp_SystemDefaultsDialie();
					//if (this.Platform.Equals(Platforms.Dialie.ToString(), StringComparison.CurrentCultureIgnoreCase)) {
					//    dialieDefaults = usp_SystemDefaultsDialie.Execute(conn, "Dialie");
					//    if (dialieDefaults.RunChargeAsManualTransaction.AsBool()) {
					//        chargeType = CreditCardChargeType.Manual;
					//        CardSwiped = false;
					//        pCardInfo = "";
					//    }
					//}


					if (!this.DriverPaymentAuthResults.IsNullEntity) {
						//card was auth'd previously, is same card being used for the charge?
						try {
							DriverPaymentsAudit audit = DriverPaymentsAudit.GetPayment(this);
							if (!audit.IsNullEntity && audit.Number != this.Number) {
								pForceNewCharge = true;
							}
						} catch (Exception ex) {
							Console.WriteLine(ex.Message);
						}
					}
					if (TotalCharge > AuthAmount || AuthDate == null) {
						pForceNewCharge = true;
					}
					string sComment = "Driver Swipe Entry";
					if (pForceNewCharge) {
						CreditCard cardData = new CreditCard();
						cardData.AffiliateDriverID = AffiliateDriverID;
						cardData.DriverID = DriverID;
						if (pCardInfo.IsNullOrEmpty()) {
							cardData.CardNo = cardNo;
							string sDate = DecryptCardExpiration();
							string sMonth = sDate.Left(2);
							string sYear = sDate.Right(2);
							cardData.CardExpiryAsMMYY = sMonth + sYear;
							sComment = "Driver Manual Entry";
						} else {
							if (Platform.Equals(Platforms.Dialie.ToString(), StringComparison.CurrentCultureIgnoreCase)) {
								if (dialieDefaults.ConvertToSimpleTrackData.AsBool()) {
									if (pCardInfo.StartsWith("%B", StringComparison.CurrentCultureIgnoreCase)) {
										string track2 = pCardInfo;
										List<string> temp = track2.Split('^').ToList();
										string track1 = string.Format(";{0}={1}", temp[0].Replace("%B", ""), temp.Last());
										if (!track1.EndsWith("?")) {
											track1 += "?";
										}
										cardData.Track2 = track1;
									}
								} else {
									cardData.Track2 = pCardInfo;
								}
							} else {
								cardData.Track2 = pCardInfo;
							}
						}
						if (CardType.Equals("GooglePay", StringComparison.CurrentCultureIgnoreCase)) {
							sComment = "Via GooglePay";
							cardData.CardType = "GooglePay";
							cardData.Track2 = pCardInfo;
							cardData.ErrorMsg = "";
							cardData.TestCard = this.Test;
						}
						cardData.CVV = CVV;
						cardData.AvsZip = this.CardZIPCode;
						if (!this.CardAddress.IsNullOrEmpty()) {
							cardData.AvsStreet = this.CardAddress.DecryptIceKey();
							cardData.AvsStreet = cardData.AvsStreet.PadRight(35, ' ').Left(35).Trim();  // Max Litle size
						}

						// Soft Descriptors
						cardData.SoftDescriptor = FillSoftDescriptor();

						if (Platform.Equals(Platforms.Dialie.ToString(), StringComparison.CurrentCultureIgnoreCase) && !cardData.Track2.IsNullOrEmpty()) {
							if (dialieDefaults.StoreTrackData.AsBool()) {
								var dpRef = CabRideEngineDapper.DriverPaymentRef.Create();
								dpRef.DriverPaymentID = this.DriverPaymentID;
								dpRef.ReferenceGroup = "Dialie";
								dpRef.ReferenceKey = "TrackData";
								dpRef.ValueString = cardData.Track2;
								if (pCardInfo != cardData.Track2) {
									dpRef.ValueString += " was " + pCardInfo;
								}
								dpRef.Save();
							}
						}


						// ride could be free based on discounts
						if (TotalCharge > 0) {
							List<ProcessorResponse> respList = new List<ProcessorResponse>();
							CardType = "" + CardType; // prevent null errors
							if (this.Test && !CardType.Equals("GooglePay")) {
								// simulate approved for test  trans
								ProcessorResponse resp = new ProcessorResponse() {
									Amount = this.TotalCharge,
									AuthCode = StringUtils.GetUniqueKey(6, true),
									CreditCardProcessor = CardProcessors.TPCard,
									Message = "Approved",
									Reference = StringUtils.GenerateRandomHex(10),
									Result = "0",
									TestCard = true,
									TransDate = ChargeDate.GetValueOrDefault(),
									TransType = "S",
									VerisignAccountID = 0
								};
								respList.Add(resp);
								oPayResults = ProcessResponse(resp, VerisignTrxTypes.Sale, TotalCharge, null);
							} else {
								if (splitFee && TaxiPassFee > 0) {
									splitFeeRec = JsonConvert.DeserializeObject<DriverPayments>(JsonConvert.SerializeObject(this));
									splitFeeRec.Fare = 0;
									splitFeeRec.Gratuity = 0;
									splitFeeRec.TaxiPassFee = this.TaxiPassFee;
									splitFeeRec.Platform = Platforms.SplitFeeMID.ToString();
									splitFeeRec.VerisignAccountID = AffiliateRef.GetByGroupReference(conn, this.AffiliateID.Value, "Gateways", "SplitFee").ValueString.ToLong();
									splitFeeRec.DriverPaymentID = 0;
									splitFeeRec.DriverFee = 0;
									splitFeeRec.DoNotPay = true;
									splitFeeRec.DriverPaid = false;
									splitFeeRec.DriverPaidDate = null;
									splitFeeRec.DriverPaidTransNo = null;
									this.TaxiPassFee = 0;
								}

								respList = gway.ChargeCard(new CCRequest(cardData, TotalCharge, sComment, TransNo, GetServiceType(), Platform, SalesClerk(), IsGetRideTrans, ("" + this.Affiliate.DefaultPlatform).Equals(Platforms.SplitPay.ToString()) ? TaxiPassFee : 0, TaxiPassFee, DriverFee.GetValueOrDefault(0)));
								if (CardType.Equals("GooglePay", StringComparison.CurrentCultureIgnoreCase)) {
									CardNumber = cardData.CardNo.Left(4);
									CardNumberDisplay = cardData.CardNo;
								}
								foreach (ProcessorResponse resp in respList) {
									oPayResults = ProcessResponse(resp, VerisignTrxTypes.Sale, TotalCharge, null);
									if (oPayResults.Result == "0") {
										break;
									}
								}
								if (splitFee && oPayResults.Result == "0" && splitFeeRec != null) {
									splitFeeRec.TransNo = $"{TransNo}-SFEE";
									splitFeeRec.RowState = DataRowState.Added;
									splitFeeRec.IsNewRecord = true;
									splitFeeRec.Save();
									Gateway gwaySplit = new Gateway(Utils.CreditCardUtils.LoadGateWays(conn, splitFeeRec.VerisignAccountID.Value, chargeType, CardType));
									respList = gwaySplit.ChargeCard(new CCRequest(cardData, splitFeeRec.TotalCharge, sComment, splitFeeRec.TransNo, GetServiceType(), splitFeeRec.Platform, SalesClerk(), IsGetRideTrans, ("" + this.Affiliate.DefaultPlatform).Equals(Platforms.SplitPay.ToString()) ? TaxiPassFee : 0, TaxiPassFee, DriverFee.GetValueOrDefault(0)));
									bool splitFeeOk = false;
									foreach (ProcessorResponse resp in respList) {
										var resp1 = splitFeeRec.ProcessResponse(resp, VerisignTrxTypes.Sale, splitFeeRec.TotalCharge, null);
										if (resp1.Result == "0") {
											//TaxiPassFee = splitFeeRec.TaxiPassFee;
											splitFeeOk = true;
											break;
										}
									}
									if (!splitFeeOk) {
										var voidResult = VoidTrans(conn);
										//voidResp = gway.Void(new CCRequestRefNo(oPayResults.Reference, TotalCharge, this.Test, SalesClerk(), TransNo, IsGetRideTrans, CardNumberDisplay, 0, DriverFee.GetValueOrDefault(0)));
										//var voidResult = ProcessResponse(voidResp, VerisignTrxTypes.Void.ToString(), TotalCharge, null);
										if (voidResult.Result == "0") {
											this.Failed = true;
											this.Notes = "Trans Voided and marked as Failed due to SplitFee failed trans\r\n" + Notes;
											splitFeeRec.Failed = true;
											oPayResults.Result = "99";
											oPayResults.Message = "Failed Split Fee";

										}
									}
								}
							}
							//if ((Platform.Equals(Platforms.Dialie.ToString(), StringComparison.CurrentCultureIgnoreCase)
							//		|| Platform.Equals(Platforms.ChargePass.ToString(), StringComparison.CurrentCultureIgnoreCase))
							//		&& chargeType == CreditCardChargeType.Swipe && respList.FirstOrDefault(p => p.Result == "0") == null) {
							//if (Platform.Equals(Platforms.Dialie.ToString(), StringComparison.CurrentCultureIgnoreCase)
							//        && chargeType == CreditCardChargeType.Swipe && respList.FirstOrDefault(p => p.Result == "0") == null) {


							// HG 2018/03/13 This will mess up SplitFee
							//if (("" + this.Affiliate.ServiceType).Equals("Taxi", StringComparison.CurrentCultureIgnoreCase) && chargeType == CreditCardChargeType.Swipe && oPayResults.Result != "0") {
							//    if (dialieDefaults.SwitchToManual.AsBool()) {
							//        chargeType = CreditCardChargeType.Manual;
							//        cardData.CardNo = cardNo;
							//        string sDate = DecryptCardExpiration();
							//        string sMonth = sDate.Left(2);
							//        string sYear = sDate.Right(2);
							//        cardData.CardExpiryAsMMYY = sMonth + sYear;
							//        sComment = "Driver Manual Entry";
							//        pCardInfo = "";
							//        cardData.Track2 = "";
							//        CardSwiped = false;
							//        gway = new Gateway(Utils.CreditCardUtils.LoadGateWays(conn, this.VerisignAccountID.Value, chargeType, CardType));
							//        respList = gway.ChargeCard(new CCRequest(cardData, TotalCharge, sComment, TransNo, GetServiceType(), Platform, SalesClerk(), IsGetRideTrans, 0, TaxiPassFee, DriverFee.GetValueOrDefault(0)));
							//        foreach (ProcessorResponse resp in respList) {
							//            Thread.Sleep(2000);
							//            oPayResults = ProcessResponse(resp, VerisignTrxTypes.Sale, TotalCharge, null);
							//        }
							//    }
							//}
						} else {
							oPayResults = ProcessResponse(CreateDummyProcessorResponse(), VerisignTrxTypes.Sale, TotalCharge, null);
						}
					} else {
						//Delayed Sale
						string sRefNo = "";
						string sTranType = VerisignTrxTypes.DelayedCapture;
						DriverPaymentResults chargeAuthResult = null;
						foreach (DriverPaymentResults oResult in this.DriverPaymentResultses) {
							if ((!oResult.IsNullEntity && (oResult.Result == "0" || oResult.Message.StartsWith("Approved", StringComparison.CurrentCultureIgnoreCase)) && oResult.TransType == VerisignTrxTypes.Authorization)) {
								sRefNo = oResult.Reference;
								chargeAuthResult = oResult;
								break;
							}
						}
						if (!CreditCardProcessor.IsNullOrEmpty()) {
							oProcessor = this.CreditCardProcessor.ToEnum<CardProcessors>();
						}

						ProcessorResponse resp = null;
						gway.SetProcessor(oProcessor);
						if (this.Test) {
							resp = new ProcessorResponse() {
								// simulate approved for test  trans
								Amount = this.TotalCharge,
								AuthCode = StringUtils.GetUniqueKey(6, true),
								CreditCardProcessor = CardProcessors.TPCard,
								Message = "Approved",
								Reference = StringUtils.GenerateRandomHex(10),
								Result = "0",
								TestCard = true,
								TransDate = ChargeDate.GetValueOrDefault(),
								TransType = "S",
								VerisignAccountID = 0
							};

						} else {

							if (splitFee) {
								splitFeeRec = JsonConvert.DeserializeObject<DriverPayments>(JsonConvert.SerializeObject(this));
								splitFeeRec.Fare = 0;
								splitFeeRec.Gratuity = 0;
								splitFeeRec.TaxiPassFee = this.TaxiPassFee;
								splitFeeRec.Platform = Platforms.SplitFeeMID.ToString();
								splitFeeRec.VerisignAccountID = AffiliateRef.GetByGroupReference(conn, this.AffiliateID.Value, "Gateways", "SplitFee").ValueString.ToLong();
								splitFeeRec.DriverPaymentID = 0;
								splitFeeRec.DriverFee = 0;
								splitFeeRec.DoNotPay = true;
								splitFeeRec.DriverPaid = false;
								splitFeeRec.DriverPaidDate = null;
								splitFeeRec.DriverPaidTransNo = null;
								this.TaxiPassFee = 0;
							}
							resp = gway.ChargeAuth(new CCRequestRefNo(sRefNo, TotalCharge, Test, SalesClerk(), TransNo, IsGetRideTrans, this.CardNumberDisplay, TaxiPassFee, DriverFee.GetValueOrDefault(0)));
						}
						oPayResults = ProcessResponse(resp, sTranType, TotalCharge, chargeAuthResult);
						if (splitFee && oPayResults.Result == "0") {
							splitFeeRec.TransNo = $"{TransNo}-SFEE";
							splitFeeRec.RowState = DataRowState.Added;
							splitFeeRec.IsNewRecord = true;
							splitFeeRec.Save();
							CreditCard cardData = new CreditCard();
							cardData.AffiliateDriverID = AffiliateDriverID;
							cardData.DriverID = DriverID;
							if (pCardInfo.IsNullOrEmpty()) {
								cardData.CardNo = this.DecryptCardNumber();
								string sDate = DecryptCardExpiration();
								string sMonth = sDate.Left(2);
								string sYear = sDate.Right(2);
								cardData.CardExpiryAsMMYY = sMonth + sYear;
								sComment = "Split Fee";
							}

							Gateway gwaySplit = new Gateway(Utils.CreditCardUtils.LoadGateWays(conn, splitFeeRec.VerisignAccountID.Value, chargeType, CardType));
							var respList = gwaySplit.ChargeCard(new CCRequest(cardData, splitFeeRec.TotalCharge, sComment, splitFeeRec.TransNo, GetServiceType(), splitFeeRec.Platform, SalesClerk(), IsGetRideTrans, ("" + this.Affiliate.DefaultPlatform).Equals(Platforms.SplitPay.ToString()) ? TaxiPassFee : 0, TaxiPassFee, DriverFee.GetValueOrDefault(0)));
							foreach (ProcessorResponse respSplitFee in respList) {
								var resp1 = splitFeeRec.ProcessResponse(respSplitFee, VerisignTrxTypes.Sale, splitFeeRec.TotalCharge, null);
								if (resp1.Result == "0") {
									TaxiPassFee = splitFeeRec.TaxiPassFee;
								}
							}

						}
					}
				}

				// hg 4/28/11 force payment if we need to mark as AutoPaid regardless of payment type and platform
				// ca 9/7/11 Only set DriverPaid if the charge was successful (oPayResults.Result.Trim() == "0")
				if (!this.DriverPaid
					&& oPayResults.Result.Trim() == "0"
					&& (this.Affiliate.AutoPayFleet
							|| (this.AffiliateDrivers.AutoPayManual && !this.CardSwiped)
							|| (this.AffiliateDrivers.AutoPaySwipe && this.CardSwiped)
						)
					) {
					if (!this.AffiliateDrivers.DriverInitiatedPayCardPayment
						&& !this.Affiliate.DriverInitiatedPayCardPayment
						&& !SystemDefaults.GetDefaults(conn).DriverInitiatedPayCardPayment) {

						if (!this.AffiliateDrivers.PaidByRedeemer) {
							SetDriverPaid(false);
						}
					}
				}


				CheckFreezeAccount();

				if (this.Affiliate.DoNotPay) {
					DoNotPay = true;
					DriverPaid = false;
					DriverPaidDate = null;
					DriverPaidTransNo = null;
					DriverPaymentAux aux = this.DriverPaymentAux;
					if (aux.IsNullEntity) {
						aux = DriverPaymentAux.Create(this);
					}
					aux.DoNotPay = true;
					aux.DoNotPayReasonID = DoNotPayReason.GetCreateReason("Software Licensing").DoNotPayReasonID;
					aux.Save();
				}

				this.Save();

				if (this.DriverPaid && !this.Failed && !this.Test && !this.DoNotPay.GetValueOrDefault(false)) {
					DoPerTransactionRecurringFee();
					// hg 03/12/12
					//moved to Redemption process, unless they are auto paid
					if (this.Affiliate.AutoPayFleet || (this.AffiliateDrivers.AutoPayManual && !this.CardSwiped)
														|| (this.AffiliateDrivers.AutoPaySwipe && this.CardSwiped)) {
						DoPromoPayments();
					}
				}

				//string batchTimeZone = SystemDefaultsDict.GetByKey("Processor", "BatchTimeZone", false).ValueString;
				//if (!batchTimeZone.IsNullOrEmpty()) {
				//    TimeZoneConverter tzConvert = new TimeZoneConverter();
				//    DateTime etTime = tzConvert.ConvertTime(ChargeDate.Value, this.Affiliate.TimeZone, batchTimeZone);
				try {
					var timeZoneOffest = DriverPaymentRef.GetCreate(this, "TimeZoneOffset", "Minutes");
					timeZoneOffest.ValueString = DateTime.UtcNow.Subtract(this.Affiliate.LocalTime).TotalMinutes.ToString("F0");
					timeZoneOffest.Save();
					//}
					double elapsed = DateTime.Now.Subtract(elapsedStart).TotalMilliseconds / 1000;
					var dpElapsedRef = DriverPaymentRef.GetCreate(this, "ElapsedTime", "ChargeCard");
					dpElapsedRef.ValueString = elapsed.ToString();
					dpElapsedRef.Save();
				} catch (Exception) {
				}
				return oPayResults;
			}

		}



		public List<DriverPaymentCharges> ChargeList = new List<DriverPaymentCharges>();
		private void CreateChargeRecords() {
			// ensure fee is calculated correctly for eChecks
			if (CardType.Equals("eCheck", StringComparison.CurrentCultureIgnoreCase)) {
				TaxiPassFee = CalcFeeForChecks(this.Affiliate, this.Fare);
			}

			ChargeList.Add(DriverPaymentCharges.Create(this.DriverPaymentID, "Fare", this.Fare));
			ChargeList.Add(DriverPaymentCharges.Create(this.DriverPaymentID, "Gratuity", this.Gratuity));
			ChargeList.Add(DriverPaymentCharges.Create(this.DriverPaymentID, "AirportFee", this.AirportFee));
			ChargeList.Add(DriverPaymentCharges.Create(this.DriverPaymentID, "MiscFee", this.MiscFee));
			ChargeList.Add(DriverPaymentCharges.Create(this.DriverPaymentID, "Tolls", this.Tolls.GetValueOrDefault(0)));
			ChargeList.Add(DriverPaymentCharges.Create(this.DriverPaymentID, "Parking", this.Parking.GetValueOrDefault(0)));
			ChargeList.Add(DriverPaymentCharges.Create(this.DriverPaymentID, "Stops", this.Stops.GetValueOrDefault(0)));
			ChargeList.Add(DriverPaymentCharges.Create(this.DriverPaymentID, "WaitTime", this.WaitTime.GetValueOrDefault(0)));
			ChargeList.Add(DriverPaymentCharges.Create(this.DriverPaymentID, "BookingFee", this.BookingFee.GetValueOrDefault(0)));
			ChargeList.Add(DriverPaymentCharges.Create(this.DriverPaymentID, "DropFee", this.DropFee.GetValueOrDefault(0)));
			ChargeList.Add(DriverPaymentCharges.Create(this.DriverPaymentID, "Discount", -Math.Abs(this.Discount.GetValueOrDefault(0))));
			ChargeList.Add(DriverPaymentCharges.Create(this.DriverPaymentID, "TaxiPassFee", this.TaxiPassFee));

			ChargeList = ChargeList.Where(p => p.Amount != 0).ToList();
		}

		List<DriverPaymentFees> FeeList = new List<DriverPaymentFees>();
		private void CreateFeeRecords() {
			FeeList.Add(DriverPaymentFees.Create(this.DriverPaymentID, "DriverFee", this.DriverFee.GetValueOrDefault(0)));
			FeeList.Add(DriverPaymentFees.Create(this.DriverPaymentID, "RedemptionFee", this.RedemptionFee.GetValueOrDefault(0)));

			FeeList = FeeList.Where(p => p.Amount != 0).ToList();

		}


		public DriverPaymentResults PreAuthCard() {
			return PreAuthCard("");
		}


		public decimal AuthServiceFee = 0;

		public DriverPaymentResults PreAuthCard(string pCardInfo) {

			DateTime elapsedStart = DateTime.Now;

			decimal dAuthAmount = 25;

			if (this.AuthAmount < 1) {
				throw new Exception("Auth amount is required");
			}

			// only allow this one test card for Carmel
			string cardNo = this.DecryptCardNumber();
			if (CardUtils.IsTestCard(cardNo) && this.AffiliateID == 161 && (!cardNo.Equals("4000200011112222") || this.CVV != "166" || this.DecryptCardExpiration() != "122030")) {
				throw new Exception("Cannot PreAuth Test Card");
			}

			if (AffiliateDriverID.HasValue) {
				if (!AffiliateDrivers.Active) {
					throw new Exception(string.Format("Affiliate Driver: {0}-{1},  Account Frozen cannot preauth card", AffiliateDriverID.Value, AffiliateDrivers.Name));
				}
			} else {
				if (!Drivers.Active && !Drivers.Name.Equals(CHARGECARDS)) {
					throw new Exception(string.Format("Driver: {0}-{1},  Account Frozen cannot preauth card", DriverID, Drivers.Name));
				}
			}


			// 12/12/2011 Added so Carmel can run a $1.00 address/CVV verification
			if (this.AuthAmount < 25) {
				if (!this.Affiliate.AffiliateDriverDefaults.AuthAmountOverride.ToString().IsNullOrEmpty()) {
					if (this.Affiliate.AffiliateDriverDefaults.AuthAmountOverride > 0) {
						dAuthAmount = Convert.ToDecimal(this.Affiliate.AffiliateDriverDefaults.AuthAmountOverride);
					}
				}
				if (this.Platform != Platforms.ChargePassWeb.ToString()) {
					this.AuthAmount = dAuthAmount;
				}
			}

			// get fee and add to preauth amount
			if (this.Platform != Platforms.ChargePassWeb.ToString()) {
				decimal tpFee = CalcFee(this.Affiliate, this.AuthAmount);
				this.AuthAmount += tpFee;
				AuthServiceFee += tpFee;
			}

			CardProcessingFailureTypes oProcess = CardProcessingFailureTypes.OKToProcess;
			if (!BypassFraudCheck) {
				BypassFraudCheck = this.Affiliate.AffiliateDriverDefaults.ByPassFraudCheck;
			}

			if (!BypassFraudCheck) {
				oProcess = CanProcessCard(this);
			}
			this.Failed = true;
			switch (oProcess) {
				case CardProcessingFailureTypes.MaxDailyAmount:
				case CardProcessingFailureTypes.MaxDriverTransactions:
				case CardProcessingFailureTypes.MaxMonthlyAmount:
				case CardProcessingFailureTypes.MaxTransPerMonth:
				case CardProcessingFailureTypes.MaxTransPerWeek:
				case CardProcessingFailureTypes.MaxWeeklyAmount:
				case CardProcessingFailureTypes.OKToProcess:

					//this.Reason = "Not yet processed";
					if (!AuthDate.HasValue) {
						this.AuthDate = this.Affiliate.LocalTime;
					}
					this.FraudFailure = false;
					//if (this.DriverPaymentID < 0) {
					//    this.Save();
					//}

					break;

				//case CardProcessingFailureTypes.MaxTransPerMonthPerCard:
				//case CardProcessingFailureTypes.MaxTransPerWeekPerCard:
				//case CardProcessingFailureTypes.MaxCardTransactions:

				default:
					CardProcessingException ex = new CardProcessingException("Cannot Process Credit Card", oProcess, this.Drivers, this.AffiliateDrivers, Number);
					this.Failed = true;

					// this.DriverPaymentNotes.save added as we were not seeing fraud failures saved - 3/2/2010
					string sError = ex.Message;
					if (sError.IsNullOrEmpty()) {
						sError = "Cannot Process Credit Card - untrapped fraud parameter";
					}
					if (Platform == Platforms.EZRide.ToString() && !ex.MyMessage.IsNullOrEmpty()) {
						sError += " - " + ex.MyMessage;
					}
					this.Notes = sError;
					this.DriverPaymentNotes.Save();

					if (!AuthDate.HasValue) {
						this.AuthDate = this.Affiliate.LocalTime;
					}
					if (!this.Affiliate.AffiliateDriverDefaults.ExcludeFromRedeemerStopPayment.GetValueOrDefault(false)) {
						this.FraudFailure = true;
					}
					this.Save();
					CheckSuspendAccount(ex);
					throw ex;
			}

			using (var conn = SqlHelper.OpenSqlConnection()) {
				//Check BlackList
				if (BlackListedCard(conn, cardNo)) {
					this.Notes = "Blacklisted Card";
					this.DriverPaymentNotes.Save();
					throw new Exception("Card not accepted 6");
				}

				//Check BlackListBIN
				if (BlackListedBIN(cardNo)) {
					this.Notes = "Blacklisted BIN";
					this.DriverPaymentNotes.Save();
					throw new Exception("Card unaccepted. Please try a different card to charge.");
				}

				// only allow this one test card for Carmel
				if (CardUtils.IsTestCard(cardNo) && this.AffiliateID == 161 && (!cardNo.Equals("4000200011112222") || this.CVV != "166" || this.DecryptCardExpiration() != "122030")) {
					this.Notes = "Cannot charge Test Card";
					this.DriverPaymentNotes.Save();
					throw new Exception("Cannot charge Test Card");
				}

				//Check BlackList
				if (BlackListedCard(conn, cardNo)) {
					this.Notes = "Blacklisted Card";
					this.DriverPaymentNotes.Save();
					throw new Exception("Card not accepted 2");
				}

				//Check BlackListBIN
				if (BlackListedBIN(cardNo)) {
					this.Notes = "Blacklisted BIN";
					this.DriverPaymentNotes.Save();
					throw new Exception("Card unaccepted. Please try a different card to charge.");
				}

				//Is it a TaxiPass Card 
				TaxiPassCards tpCard = TaxiPassCards.GetCardNo(conn, cardNo);
				#region TaxiPassCards
				if (!tpCard.IsNullEntity) {
					if (tpCard.CardNumber.IsNullOrEmpty()) {
						tpCard.CardNumber = CardNumberToHash(cardNo);
						tpCard.Save();
					}
					if (!tpCard.Active) {
						this.Failed = true;
						this.Notes = "Card not accepted 3";
						this.DriverPaymentNotes.Save();
						if (!AuthDate.HasValue) {
							this.AuthDate = this.Affiliate.LocalTime;
						}
						this.Save();
						throw new Exception("Card not accepted 4");
					} else if (tpCard.TestCard.GetValueOrDefault(false)) {
						this.Test = true;
						this.Notes = "Test TaxiPassCard used";
						this.DriverPaymentNotes.Save();
						if (!AuthDate.HasValue) {
							this.AuthDate = this.Affiliate.LocalTime;
						}
						this.Save();
					}


					DriverPaymentResults oResult = DriverPaymentResults.Create(this);
					oResult.Reference = "TP Card Check";
					oResult.TransType = VerisignTrxTypes.Authorization.ToString().Substring(0, 1);
					oResult.TransDate = Affiliate.LocalTime;


					if (!this.IgnoreCardZIP.GetValueOrDefault(true)) {
						if (!string.IsNullOrEmpty(this.CardZIPCode) && !string.IsNullOrEmpty(tpCard.ZIPCode)) {
							if (tpCard.ZIPCode != StringUtils.CleanPhoneNumbers(this.CardZIPCode)) {
								this.Failed = true;
								this.Notes = "Invalid Billing Zip Code";
								this.DriverPaymentNotes.Save();

								oResult.Result = "-1";
								oResult.Message = "Invalid Billing Zip Code";
								oResult.AVSZIP = "N";
								oResult.Save();

								if (!this.AuthDate.HasValue) {
									this.AuthDate = Affiliate.LocalTime;
								}
								this.Save();
								return oResult;

							}
							oResult.AVSZIP = "Y";
						}
					}

					//get last transaction
					TaxiPassCardTransactions tpTrans = TaxiPassCardTransactions.GetLastTrans(tpCard);
					if (tpTrans.Balance - this.AuthAmount < -tpCard.MaxCredit) {
						this.Failed = true;
						this.Notes = "Card Declined";
						this.DriverPaymentNotes.Save();

						oResult.Result = "-2";
						oResult.Reference = "TP Card";
						oResult.TransType = VerisignTrxTypes.Authorization;
						oResult.TransDate = Affiliate.LocalTime;
						oResult.Message = "Insufficient funds";
						oResult.Save();

						if (!this.AuthDate.HasValue) {
							this.AuthDate = Affiliate.LocalTime;
						}
						this.Save();
						return oResult;
					}


					oResult.AuthCode = StringUtils.GetUniqueKey(6);
					oResult.Message = "Approved";
					oResult.Result = "0";
					oResult.Save();

					this.Notes = "";
					this.DriverPaymentNotes.Save();
					if (!this.AuthDate.HasValue) {
						this.AuthDate = Affiliate.LocalTime;
					}
					this.Save();
					return oResult;
				}
				#endregion TaxiPassCards

				//Is it a promo payment card
				List<DriverPromotions> promoList = DriverPromotions.GetPromotionsByCardNo(conn, this.DecryptCardNumber());
				if (promoList.Count > 0) {
					return DoDriverPromoPreAuth(promoList, cardNo, pCardInfo);
				}

				DriverPaymentResults oPayResults = null;

				CreditCard cardData = new CreditCard();
				cardData.AffiliateDriverID = this.AffiliateDriverID;
				if (!pCardInfo.IsNullOrEmpty()) {
					cardData.Track2 = pCardInfo;
				}
				string sComment = "Driver Swipe Entry";
				if (pCardInfo.IsNullOrEmpty()) {
					cardData.CardNo = cardNo;
					string sDate = DecryptCardExpiration();
					string sMonth = sDate.Left(2);
					string sYear = sDate.Right(2);
					cardData.CardExpiryAsMMYY = sMonth + sYear;
					sComment = "Driver Manual Entry";
				}
				cardData.CVV = CVV;
				cardData.AvsZip = this.CardZIPCode;
				if (!this.CardAddress.IsNullOrEmpty()) {
					cardData.AvsStreet = this.CardAddress.DecryptIceKey();
					cardData.AvsStreet = cardData.AvsStreet.PadRight(35, ' ').Left(35).Trim();  // Max Litle size
				}

				// Soft Descriptors


				cardData.SoftDescriptor = FillSoftDescriptor();


				//Gateway gway = new Gateway(Utils.CreditCardUtils.LoadGateWays(this.Affiliate.MyVerisignAccount));
				if (this.RowState == DataRowState.Added) {
					this.Save();
				}
				this.VerisignAccountID = Utils.CreditCardUtils.GetDriverPaymentCardGatewayGroupsID(conn, this.TransNo, 0);
				if (UseFraudGateway) {
					AffiliateRef affRec = AffiliateRef.GetByGroupReference(conn, this.AffiliateID.Value, "Gateways", "Fraud");
					if (!affRec.ValueString.IsNullOrEmpty() && affRec.ValueString.IsNumeric() && affRec.ValueString.ToInt() > 0) {
						VerisignAccountID = affRec.ValueString.ToInt();
					} else if (this.Affiliate.FraudVerisignAccountID.GetValueOrDefault(0) > 0) {
						VerisignAccountID = this.Affiliate.FraudVerisignAccountID.Value;
					}
				} else if (IsDebitCard) {
					AffiliateRef affRec = AffiliateRef.GetByGroupReference(conn, this.AffiliateID.Value, "Gateways", "Debit");
					if (!affRec.ValueString.IsNullOrEmpty() && affRec.ValueString.IsNumeric() && affRec.ValueString.ToInt() > 0) {
						VerisignAccountID = affRec.ValueString.ToInt();
					}
				} else if (("" + this.CardType).Equals("eCheck", StringComparison.CurrentCultureIgnoreCase)) {
					AffiliateRef affRec = AffiliateRef.GetByGroupReference(conn, this.AffiliateID.Value, "Gateways", "eCheck");
					if (!affRec.ValueString.IsNullOrEmpty() && affRec.ValueString.IsNumeric() && affRec.ValueString.ToInt() > 0) {
						VerisignAccountID = affRec.ValueString.ToInt();
					}
				}


				CreditCardChargeType chargeType = CreditCardChargeType.ECommerce;
				if (Platform != Platforms.GetRide.ToString()) {
					if (CardSwiped) {
						chargeType = CreditCardChargeType.Swipe;
					} else {
						chargeType = CreditCardChargeType.Manual;
					}
				}

				List<ProcessorResponse> resultList = new List<ProcessorResponse>();
				if (this.Test) {
					// simulate approved for test  trans
					ProcessorResponse resp = new ProcessorResponse() {
						Amount = this.TotalCharge,
						AuthCode = StringUtils.GetUniqueKey(6, true),
						CreditCardProcessor = CardProcessors.TPCard,
						Message = "Approved",
						Reference = StringUtils.GenerateRandomHex(10),
						Result = "0",
						TestCard = true,
						TransDate = ChargeDate.GetValueOrDefault(),
						TransType = "A",
						VerisignAccountID = 0
					};

					resultList.Add(resp);
				} else {
					if (AuthAmount < TotalCharge) {
						AuthAmount = TotalCharge * 1.5M;
					}
					Gateway gway = new Gateway(Utils.CreditCardUtils.LoadGateWays(conn, this.VerisignAccountID.Value, chargeType, this.CardType));
					resultList = gway.PreAuthCard(new CCRequest(cardData, AuthAmount, sComment, TransNo, GetServiceType(), Platform, DriverID.ToString(), IsGetRideTrans, 0, TaxiPassFee, DriverFee.GetValueOrDefault(0)));
				}

				foreach (ProcessorResponse response in resultList) {
					oPayResults = ProcessResponse(response, TrxTypes.Authorization, AuthAmount, null);
				}

				double elapsed = DateTime.Now.Subtract(elapsedStart).TotalMilliseconds / 1000;
				var dpElapsedRef = DriverPaymentRef.GetCreate(this, "ElapsedTime", "Preauth");
				dpElapsedRef.ValueString = elapsed.ToString();
				dpElapsedRef.Save();

				ProcessorResponse avsRes = null;
				if (oPayResults.AVSResultCode.IsNullOrEmpty()) {
					avsRes = resultList.FirstOrDefault(p => !p.AVSResultCode.IsNullOrEmpty());
					if (avsRes != null) {
						oPayResults.AVS = avsRes.AVS;
						oPayResults.AVSAddr = avsRes.AVSAddr;
						oPayResults.AVSResultCode = avsRes.AVSResultCode;
						oPayResults.AVSZIP = avsRes.AVSZip;
					}
				}
				if (oPayResults.CVV2Match.IsNullOrEmpty()) {
					if (avsRes == null) {
						avsRes = resultList.FirstOrDefault(p => !p.CVV2Match.IsNullOrEmpty());
					}
					if (avsRes != null) {
						oPayResults.CVV2Match = avsRes.AVSZip;
					}
				}
				return oPayResults;
			}
		}

		// *************************************************************************************************************
		private DriverPaymentResults DoDriverPromoPreAuth(List<DriverPromotions> pPromoList, string pCardNo, string pCardInfo) {
			bool promoFound = false;
			DriverPromotions myPromo = null;

			foreach (DriverPromotions oPromo in pPromoList) {
				if (oPromo.AllAffiliates || oPromo.DriverPromotionAffiliateses.FirstOrDefault(p => p.AffiliateID == this.AffiliateID) != null) {
					if (DateTime.Today >= oPromo.StartDate && DateTime.Today <= oPromo.EndDate) {
						promoFound = true;
						myPromo = oPromo;
						break;
					}
				}
			}

			if (promoFound) {
				this.TaxiPassFee = 0;
				List<DriverPayments> payList = new List<DriverPayments>();
				if (this.AffiliateDrivers.IsNullEntity) {
					payList = DriverPayments.GetPaymentByCardNo(this.Drivers, pCardNo, this.TransNo);
				} else {
					payList = DriverPayments.GetPaymentByCardNo(this.AffiliateDrivers, pCardNo, this.TransNo);
				}
				if (payList.Count > 0) {
					this.Failed = true;
					this.Notes = "Card not accepted 5, promo already used";
					this.DriverPaymentNotes.Save();
					if (!AuthDate.HasValue) {
						this.AuthDate = this.Affiliate.LocalTime;
					}
					this.Save();
					throw new Exception(this.DriverPaymentNotes.Reason);
				}
				return ProcessResponse(myPromo, VerisignTrxTypes.Authorization);
			}

			this.Failed = true;
			this.Notes = "Promo Card not valid";
			this.DriverPaymentNotes.Save();
			if (!AuthDate.HasValue) {
				this.AuthDate = this.Affiliate.LocalTime;
			}
			this.Save();
			throw new Exception(this.DriverPaymentNotes.Reason);

		}

		private string GetServiceType() {
			string sServiceType = this.Affiliate.ServiceType;
			if (sServiceType.IsNullOrEmpty()) {
				sServiceType = "Taxi";
			}
			sServiceType += " Service";
			return sServiceType;
		}


		/// <summary>
		/// Fills the soft descriptor for use by Litle.
		/// </summary>
		/// <returns></returns>
		private SoftDescriptor FillSoftDescriptor() {
			SoftDescriptor desc = new SoftDescriptor();

			desc.Name = Affiliate.Markets.Market;
			if (!desc.Name.IsNullOrEmpty()) {
				desc.Name = desc.Name.PadRight(16, ' ').Left(16).Trim();
			}
			desc.StateProvince = "NEW YORK";  // Per Donna at Vantiv
			desc.AffiliateName = Affiliate.Name.PadRight(16, ' ').Left(16).Trim();
			desc.Internet = "http://contact.taxipass.com";
			desc.Phone = "8888294729";


			string affPlatform = Platform;
			if (Platform.Equals(CabRideEngineDapper.Platforms.WebTerminal.ToString(), StringComparison.CurrentCultureIgnoreCase) && !Affiliate.DefaultPlatform.IsNullOrEmpty()) {
				affPlatform = Affiliate.DefaultPlatform;
			}
			if (affPlatform.Equals(CabRideEngineDapper.Platforms.ChargePass.ToString(), StringComparison.CurrentCultureIgnoreCase)
				|| affPlatform.Equals(CabRideEngineDapper.Platforms.StorePass.ToString(), StringComparison.CurrentCultureIgnoreCase)
				|| affPlatform.Equals(CabRideEngineDapper.Platforms.MedPass.ToString(), StringComparison.CurrentCultureIgnoreCase)
				|| affPlatform.Equals(CabRideEngineDapper.Platforms.Dialie.ToString(), StringComparison.CurrentCultureIgnoreCase)) {
				desc.CompanyURL = CompanyURL.ChargePass;
				desc.Internet = "http://chargepass.co/";
			} else {
				if (IsGetRideTrans) {
					desc.CompanyURL = CompanyURL.GetRide;
				}
			}

			if (!this.Affiliate.BillingDescriptor.IsNullOrEmpty()) {
				desc.BillingDescriptor = this.Affiliate.BillingDescriptor.Replace("&", "N");
			}
			return desc;
		}


		// ********************************************************************************************************
		// 4/23/2013 Tax the max from either Drivers or AffilaiteDrivers
		// removed "bool pCheckDriver" as an input argument
		// ********************************************************************************************************
		public static CardProcessingFailureTypes CanProcessCard(DriverPayments pDriverPayments) {
			decimal dMax;
			string sCardNumber = pDriverPayments.DecryptCardNumber();
			decimal totalAmount = pDriverPayments.DriverTotal;

			Drivers oDriver = pDriverPayments.Drivers;
			AffiliateDrivers oAffDriver = pDriverPayments.AffiliateDrivers;

			string sCardNo = CardNumberToHash(sCardNumber);

			using (var conn = SqlHelper.OpenSqlConnection()) {
				SystemDefaults oDefault = SystemDefaults.GetDefaults(conn);

				if (DriverPromotions.GetPromotionsByCardNo(conn, sCardNumber).Count > 0) {
					return CardProcessingFailureTypes.OKToProcess;
				}

				if (sCardNo.IsNullOrEmpty() && !pDriverPayments.CardType.Equals("GooglePay", StringComparison.CurrentCultureIgnoreCase)) {
					return CardProcessingFailureTypes.MissingCardNumber;
				}


				TimeZoneConverter tzConvert = new TimeZoneConverter();
				DateTime now = tzConvert.ConvertTime(DateTime.Now, TimeZone.CurrentTimeZone.StandardName, pDriverPayments.Affiliate.TimeZone);
				bool bCheckDriver = true;
				if (oDriver.Name.Equals("Charge Card", StringComparison.CurrentCultureIgnoreCase) && pDriverPayments.Platform != Platforms.WebTerminal.ToString() && pDriverPayments.AffiliateDriverID.HasValue) {
					bCheckDriver = false;
				} else {
					if (!oDriver.Name.Equals("Charge Card", StringComparison.CurrentCultureIgnoreCase)) {
						bCheckDriver = true;
					}
				}
				usp_DriverPayments_FraudCheck fraudTotals = usp_DriverPayments_FraudCheck.ExecuteProc(conn, pDriverPayments, bCheckDriver, now);

				if (oDefault.DriverMinTransTime != 0 && fraudTotals.LastChargeDate.HasValue) {
					TimeSpan oTime = now.Subtract(fraudTotals.LastChargeDate.Value);
					if (Math.Abs(oTime.TotalSeconds) < oDefault.DriverMinTransTime) {
						return CardProcessingFailureTypes.CardUsedTooSoon;
					}
				}


				if (fraudTotals.DailyTransCountPerCard >= oAffDriver.GetCardChargedMaxNumber) {
					return CardProcessingFailureTypes.MaxCardTransactions;
				}


				if (fraudTotals.DailyTransCount >= oAffDriver.GetMaxTrxPerDay) {
					return CardProcessingFailureTypes.MaxDriverTransactions;
				}

				dMax = oDriver.GetMaxDailyAmount;
				if (oAffDriver.GetMaxDailyAmount > dMax) {
					dMax = oAffDriver.GetMaxDailyAmount;
				}
				if (fraudTotals.DailyTransTotal > dMax) {
					return CardProcessingFailureTypes.MaxDailyAmount;
				}


				//Check Fraud Measures for Month
				dMax = oDriver.GetMaxTransPerMonth;
				if (oAffDriver.GetMaxTransPerMonth > dMax) {
					dMax = oAffDriver.GetMaxTransPerMonth;
				}
				if (fraudTotals.MonthTransCount > dMax) {
					return CardProcessingFailureTypes.MaxTransPerMonth;
				}

				dMax = oDriver.GetMaxTransPerMonthPerCard;
				if (oAffDriver.GetMaxTransPerMonthPerCard > dMax) {
					dMax = oAffDriver.GetMaxTransPerMonthPerCard;
				}
				if (fraudTotals.MonthTransCountPerCard > dMax) {
					return CardProcessingFailureTypes.MaxTransPerMonthPerCard;
				}

				dMax = oDriver.GetMaxMonthlyAmount;
				if (oAffDriver.GetMaxMonthlyAmount > dMax) {
					dMax = oAffDriver.GetMaxMonthlyAmount;
				}
				if (fraudTotals.MonthTotal + totalAmount > dMax) {
					return CardProcessingFailureTypes.MaxMonthlyAmount;
				}


				//Check Fraud Measures for Week
				dMax = oDriver.GetMaxTransPerWeek;
				if (oAffDriver.GetMaxTransPerWeek > dMax) {
					dMax = oAffDriver.GetMaxTransPerWeek;
				}
				if (fraudTotals.WeekTransCount > dMax) {
					return CardProcessingFailureTypes.MaxTransPerWeek;
				}

				dMax = oDriver.GetMaxTransPerWeekPerCard;
				if (oAffDriver.GetMaxTransPerWeekPerCard > dMax) {
					dMax = oAffDriver.GetMaxTransPerWeekPerCard;
				}
				if (fraudTotals.WeekTransCountPerCard > dMax) {
					return CardProcessingFailureTypes.MaxTransPerWeekPerCard;
				}

				dMax = oDriver.GetMaxWeeklyAmount;
				if (oAffDriver.GetMaxWeeklyAmount > dMax) {
					dMax = oAffDriver.GetMaxWeeklyAmount;
				}
				if (fraudTotals.WeekTotal + totalAmount > dMax) {
					return CardProcessingFailureTypes.MaxWeeklyAmount;
				}

				decimal maxSwipeAmount = oDriver.GetChargeMaxSwipeAmount;
				dMax = oDriver.GetChargeMaxAmount;
				if (oAffDriver.GetChargeMaxAmount > dMax) {
					dMax = oAffDriver.GetChargeMaxAmount;
					if ((pDriverPayments.StoreForward || ("" + pDriverPayments.ReferenceNo).StartsWith("StoreForward:")) && dMax > maxSwipeAmount) {
						maxSwipeAmount = dMax;
					}

				}

				//allow exception charges to have same max charge as swipe amount
				if (maxSwipeAmount > 0 && (pDriverPayments.CardSwiped || pDriverPayments.ExceptionCharge)) {
					if (totalAmount > Math.Max(dMax, maxSwipeAmount)) {
						return CardProcessingFailureTypes.MaxChargeAmountExceeded;
					}
				} else {
					if (totalAmount > dMax) {
						return CardProcessingFailureTypes.MaxChargeAmountExceeded;
					}
				}


				usp_TotalChargesPerCardAndAffiliate cardTotals = usp_TotalChargesPerCardAndAffiliate.Execute(conn, pDriverPayments.AffiliateID.Value, now, pDriverPayments.Number);
				if (cardTotals.MaxDayAmount > 0 && totalAmount + cardTotals.TotalDay > cardTotals.MaxDayAmount) {
					return CardProcessingFailureTypes.MaxDayAmountPerCardExceeded;
				}
				if (cardTotals.MaxWeekAmount > 0 && totalAmount + cardTotals.TotalWeek > cardTotals.MaxWeekAmount) {
					return CardProcessingFailureTypes.MaxWeekAmountPerCardExceeded;
				}

				dMax = cardTotals.MaxMonthAmount;
				if (oAffDriver.AffiliateDriverFraudDefaults.MaxChargePerMonthPerCard > dMax) {
					dMax = oAffDriver.AffiliateDriverFraudDefaults.MaxChargePerMonthPerCard;
				}

				if (dMax > 0 && totalAmount + cardTotals.TotalMonth > dMax) {
					return CardProcessingFailureTypes.MaxMonthAmountPerCardExceeded;
				}

				AffiliateDriverDefaults affDef = oAffDriver.Affiliate.AffiliateDriverDefaults;
				if (affDef.MaxDeclinesTransactions.GetValueOrDefault(0) > 0 && fraudTotals.DailyDeclinedCount > affDef.MaxDeclinesTransactions.GetValueOrDefault(0)) {
					return CardProcessingFailureTypes.MaxDailyDeclineExceeded;
				}
				if (affDef.MaxDeclinesTransPerWeek.GetValueOrDefault(0) > 0 && fraudTotals.WeekDeclinedCount > affDef.MaxDeclinesTransPerWeek.GetValueOrDefault(0)) {
					return CardProcessingFailureTypes.MaxWeeklyDeclineExceeded;
				}
				if (affDef.MaxDeclinesTransPerMonth.GetValueOrDefault(0) > 0 && fraudTotals.MonthDeclinedCount > affDef.MaxDeclinesTransPerMonth.GetValueOrDefault(0)) {
					return CardProcessingFailureTypes.MaxMonthlyDeclineExceeded;
				}
				Affiliate aff = pDriverPayments.Affiliate;
				if (aff.IsNullEntity) {
					aff = oAffDriver.Affiliate;
				}
				if (aff.IsNullEntity) {
					aff = oDriver.Affiliate;
				}
				if (aff.ComputeOnLastXTrans.GetValueOrDefault(0) > 0 && aff.AverageCharge.GetValueOrDefault(0) > 0) {
					string sql = $@"SELECT   TOP {aff.ComputeOnLastXTrans.Value} Fare + AirportFee + MiscFee + ISNULL(Tolls, 0) + ISNULL(BookingFee, 0) + WaitTime + Stops + Parking + Gratuity - Discount DriverTotal
									FROM     dbo.DriverPayments
									WHERE    AffiliateDriverID = @AffiliateDriverID
											 AND Failed = 0
											 AND Test = 0
											 AND Fare > 0
									ORDER BY ChargeDate DESC";
					decimal avgTotal = conn.Query<decimal>(sql, new { AffiliateDriverID = oAffDriver.AffiliateDriverID }).Sum() / aff.ComputeOnLastXTrans.Value;
					if (avgTotal > aff.AverageCharge) {
						return CardProcessingFailureTypes.AverageChargeExceeded;
					}
				}
			}

			return CardProcessingFailureTypes.OKToProcess;
		}


		/// <summary>
		/// Opens the credit.
		/// </summary>
		/// <param name="pCardInfo">The application card information.</param>
		/// <returns></returns>
		/// <exception cref="System.Exception">
		/// Fare must not be 0
		/// </exception>
		public DriverPaymentResults OpenCredit(string pCardInfo) {
			DateTime elapsedStart = DateTime.Now;
			using (var conn = SqlHelper.OpenSqlConnection()) {
				#region Validations
				// Put in to trap bogus charge card calls
				if (this.Fare == 0) {
					throw new Exception("Fare must not be 0");
				}


				// only allow this one test card for Carmel
				string cardNo = this.DecryptCardNumber();
				if (CardUtils.IsTestCard(cardNo) && this.AffiliateID == 161 && (!cardNo.Equals("4000200011112222") || this.CVV != "166" || this.DecryptCardExpiration() != "122030")) {
					throw new Exception("Cannot charge Test Card");
				}

				if (AffiliateDriverID.HasValue) {
					if (!AffiliateDrivers.Active) {
						throw new Exception(string.Format("Affiliate Driver: {0}-{1},  Account Frozen cannot credit card", AffiliateDriverID.Value, AffiliateDrivers.Name));
					}
				} else {
					if (!Drivers.Active) {
						throw new Exception(string.Format("Driver: {0}-{1},  Account Frozen cannot credit card", DriverID, Drivers.Name));
					}
				}

				//failed until card is credited
				this.Failed = true;
				this.ChargeBack = true;

				// hg 2011-07/28 Force a save to prevent incorrect charges from happening
				// Always save first to avoid incorrect voucher numbers going to Credit card processor
				if (this.RowState == DataRowState.Added) {
					int tries = 0;
					while (tries < 3) {
						tries++;
						try {
							this.Save("Force Saved");
							break;
						} catch (Exception ex) {
							if (ex.Message.Equals("Query timeout expired")) {
								continue;
							}
							throw new Exception(ex.Message);
						}
					}
				}

				IsOpenCredit(true);



				//PreAuth card 1st if manual entry and zip code is present to ensure card is valid before charging
				//if (!this.AuthDate.HasValue && !this.CardSwiped) {
				//    if (!string.IsNullOrEmpty(this.CardZIPCode)) {
				//        if (!this.IgnoreCardZIP.GetValueOrDefault(true)) {
				//            if (this.AuthAmount == 0 || AuthAmount < DriverTotal) {
				//                this.AuthAmount = DriverTotal; // TotalCharge;  use Driver total as PreAuth up's it by the TaxiPass Fee
				//            }
				//            DriverPaymentResults preAuthResult = PreAuthCard();
				//            if (!preAuthResult.Result.Equals("0")) {
				//                return preAuthResult;
				//            }
				//        }
				//    }
				//}

				//CardProcessingFailureTypes oProcess = CardProcessingFailureTypes.OKToProcess;

				if (!this.ChargeDate.HasValue) {
					this.ChargeDate = DateTime.Now;
				}
				this.FraudFailure = false;


				//Check BlackList
				if (BlackListedCard(conn, cardNo)) {
					throw new Exception("Card not accepted 6");
				}
				MyTimeStats.Info.Add(new TimeStats.ElapsedInfo { Type = "Black Listed Card Check", Elapsed = DateTime.Now.Subtract(MyTimeStats.Start).TotalSeconds });
				#endregion Validations

				DriverPaymentResults oPayResults = null;
				//TaxiPassCommon.Banking.CardProcessors oProcessor = TaxiPassCommon.Banking.CardProcessors.Verisign;

				this.VerisignAccountID = Utils.CreditCardUtils.GetDriverPaymentCardGatewayGroupsID(conn, this.TransNo, 0);
				CreditCardChargeType chargeType = CreditCardChargeType.ECommerce;
				if (Platform != Platforms.GetRide.ToString()) {
					if (CardSwiped) {
						chargeType = CreditCardChargeType.Swipe;
					} else {
						chargeType = CreditCardChargeType.Manual;
					}
				}
				Gateway gway = new Gateway(Utils.CreditCardUtils.LoadGateWays(conn, this.VerisignAccountID.Value, chargeType, CardType));

				string sComment = "Driver Swipe Entry";
				CreditCard cardData = new CreditCard();
				cardData.AffiliateDriverID = AffiliateDriverID;
				cardData.DriverID = DriverID;
				if (pCardInfo.IsNullOrEmpty()) {
					cardData.CardNo = cardNo;
					string sDate = DecryptCardExpiration();
					string sMonth = sDate.Left(2);
					string sYear = sDate.Right(2);
					cardData.CardExpiryAsMMYY = sMonth + sYear;
					sComment = "Driver Manual Entry";
				} else {
					cardData.Track2 = pCardInfo;
				}
				cardData.CVV = CVV;
				cardData.AvsZip = this.CardZIPCode;
				cardData.AvsStreet = this.CardAddress;

				// Soft Descriptors
				cardData.SoftDescriptor = FillSoftDescriptor();


				List<ProcessorResponse> respList = gway.OpenCredit(new CCRequest(cardData, TotalCharge, sComment, TransNo, GetServiceType(), Platform, SalesClerk(), IsGetRideTrans, 0, TaxiPassFee, DriverFee.GetValueOrDefault(0)));
				foreach (ProcessorResponse resp in respList) {
					oPayResults = ProcessResponse(resp, VerisignTrxTypes.Credit, TotalCharge, null);
				}

				// hg 4/28/11 force payment if we need to mark as AutoPaid regardless of payment type and platform
				// ca 9/7/11 Only set DriverPaid if the charge was successful (oPayResults.Result.Trim() == "0")
				//if (!this.DriverPaid && oPayResults.Result.Trim() == "0") {
				//	if (!this.AffiliateDrivers.DriverInitiatedPayCardPayment
				//		&& !this.Affiliate.DriverInitiatedPayCardPayment
				//		&& !SystemDefaults.GetDefaults(this.PersistenceManager).DriverInitiatedPayCardPayment) {

				//		if (!this.AffiliateDrivers.PaidByRedeemer) {
				//			SetDriverPaid(false);
				//			MyTimeStats.Info.Add(new TimeStats.ElapsedInfo { Type = "SetDriverPaid", Elapsed = DateTime.Now.Subtract(MyTimeStats.Start).TotalSeconds });
				//		}
				//	}
				//}


				//CheckFreezeAccount();

				this.Save();

				double elapsed = DateTime.Now.Subtract(elapsedStart).TotalMilliseconds / 1000;
				var dpElapsedRef = DriverPaymentRef.GetCreate(this, "ElapsedTime", "OpenCredit");
				dpElapsedRef.ValueString = elapsed.ToString();
				dpElapsedRef.Save();

				return oPayResults;
			}
		}

		private void IsOpenCredit(bool pOpenCredit) {
			DriverPaymentRef rec = DriverPaymentRef.GetCreate(this, "OpenCredit", "IsOpenCredit");
			rec.ValueString = pOpenCredit.ToString();
			rec.Save();
		}


		//public void SetDriverPaid(bool value) {
		//    if (value) {
		//        DriverPaid = value;
		//        if (!this.DriverPaidDate.HasValue) {
		//            this.DriverPaidDate = DateTime.Now;
		//        }
		//    } else {
		//        if (this.ACHDetail.IsNullEntity) { //.DriverPaidDate.HasValue) {
		//            base.DriverPaid = value;
		//            DriverPaidDate = null;
		//            DriverPaidTransNo = null;
		//        }
		//    }
		//    if (("" + CardType).Equals("TPCard")) {
		//        base.DriverPaid = false;
		//        DriverPaidDate = null;
		//        DriverPaidTransNo = null;
		//    }
		//    if (!ChargeBack && value) {
		//        DoPerTransactionRecurringFee();
		//    }
		//    if (value) {
		//        if (TaxiPassRedeemerID != null || !DriverPaymentsPendingMatches.IsNullEntity) {
		//            long redeemerID = (this.TaxiPassRedeemerID == null ? this.DriverPaymentsPendingMatches.TaxiPassRedeemerID : this.TaxiPassRedeemerID.Value);
		//            TaxiPassRedeemers redeemer = TaxiPassRedeemers.GetRedeemer(redeemerID);
		//            if (redeemer.TaxiPassRedeemerAccounts.TrackShift) {
		//                try {
		//                    RedeemerShift shift = redeemer.CurrentShift;
		//                    if (!shift.IsNullEntity) {
		//                        RedeemerShiftVouchers shiftVoucher = RedeemerShiftVouchers.GetOrCreate(this);
		//                        shiftVoucher.RedeemerShiftID = shift.RedeemerShiftID;
		//                    }
		//                } catch (Exception) {
		//                }
		//            }
		//        }
		//    }

		//}

		//// **********************************************************************************************
		////
		//// **********************************************************************************************
		//public void DoPerTransactionRecurringFee() {
		//    StringBuilder sWork = new StringBuilder();

		//    if (this.ChargeBack || this.Drivers.DriverRecurringFeeses.Count == 0) {
		//        return;
		//    }
		//    List<DriverRecurringFees> fees = (from p in this.Drivers.DriverRecurringFeeses
		//                                      where p.ChargePerTrans && !p.Cancelled && !p.Closed
		//                                      select p).ToList();
		//    if (fees.Count == 0) {
		//        return;
		//    }
		//    foreach (DriverRecurringFees oFee in fees) {
		//        //if we already charged for this transaction, don't charge again
		//        DriverPayments recurringfee = GetPerTransactionRecurringFee(false);
		//        if (!recurringfee.IsNullEntity) {
		//            return;
		//        }
		//        StringBuilder sql = new StringBuilder();
		//        sql.Append("SELECT SUM(Fare + Gratuity + AirportFee + MiscFee - Discount) FROM DriverPayments  WITH (NOLOCK) WHERE ChargeBy = '");
		//        sql.Append(RECURRING_FEES);
		//        sql.Append("' AND ReferenceNo = 'D");
		//        sql.Append(oFee.DriverRecurringFeeID);
		//        sql.Append("'");

		//        DataTable table = SQLHelper.GetRecords(this.PersistenceManager, sql.ToString());
		//        decimal amountPaid = 0;
		//        if (table.Rows.Count > 0) {
		//            if (table.Rows[0][0].GetType() != typeof(DBNull) && table.Rows[0][0] != null) {
		//                amountPaid = Convert.ToDecimal(table.Rows[0][0]);
		//            }
		//        }
		//        amountPaid = Math.Abs(amountPaid);
		//        decimal chargeFee = oFee.Amount;
		//        decimal balance = oFee.MaxTotalCharges - amountPaid;
		//        if (oFee.MaxTotalCharges > 0) {
		//            if ((oFee.MaxTotalCharges - amountPaid) < chargeFee) {
		//                chargeFee = oFee.MaxTotalCharges - amountPaid;
		//            }
		//            if (balance == 0) {
		//                oFee.Closed = true;
		//                oFee.Save();
		//                return;
		//            }
		//        }

		//        DriverPayments oPayment = DriverPayments.Create(this.Drivers);
		//        oPayment.AffiliateDriverID = this.AffiliateDriverID;
		//        oPayment.AffiliateID = this.AffiliateID;
		//        oPayment.ChargeBack = true;
		//        oPayment.ChargeBackDate = DateTime.Now;
		//        oPayment.ChargeDate = oPayment.ChargeBackDate;

		//        sWork.Remove(0, sWork.Length);
		//        if (oFee.MaxTotalCharges > 0) {
		//            sWork.Append(oFee.ExpenseTypes.Description);
		//            sWork.Append(" (Total Charge: ");
		//            sWork.Append(oFee.MaxTotalCharges.ToString("C"));
		//            sWork.Append(" -  Total Paid: ");
		//            sWork.Append(amountPaid.ToString("C"));
		//            sWork.Append(" = ");
		//            sWork.Append(balance.ToString("C"));
		//            sWork.Append(" Payment: ");
		//            sWork.Append(chargeFee.ToString("C"));
		//            sWork.Append(" -> New Balance: ");
		//            sWork.Append((balance - chargeFee).ToString("C"));
		//            sWork.Append(")");
		//        } else {
		//            sWork.Append(oFee.ExpenseTypes.Description);
		//            sWork.Append(" (Charge: ");
		//            sWork.Append(oFee.Amount.ToString("C"));
		//            sWork.Append(")");
		//        }
		//        oPayment.ChargeBackInfo = sWork.ToString();

		//        oPayment.FareNoTaxiPassFee = -chargeFee;
		//        oPayment.ChargeBy = RECURRING_FEES;
		//        oPayment.PreAuthBy = this.TransNo;
		//        oPayment.ReferenceNo = "D" + oFee.DriverRecurringFeeID.ToString("F0");
		//        oPayment.DriverPaid = true;
		//        oPayment.DriverPaidDate = oPayment.ChargeBackDate;
		//        oPayment.Save();

		//        DriverPaymentAux oAux = DriverPaymentAux.Create(oPayment);
		//        oAux.ChargeBackDriverPaymentID = this.DriverPaymentID;
		//        oAux.Save();

		//        this.ReceiptChargedBack = oPayment.TransNo;
		//        this.Save();
		//    }
		//}


		//public DriverPayments GetPerTransactionRecurringFee(bool pNotACHd) {
		//    string sql = @"Select * FROM DriverPayments
		//                   WHERE PreAuthBy = @TransNo
		//                        AND  ChargeBy = " + RECURRING_FEES;

		//    using (var conn = SqlHelper.OpenSqlConnection()) {
		//        DriverPayments recurringfee = conn.Get<DriverPayments>(sql);

		//        if (!pNotACHd) {
		//            if (!recurringfee.ACHDetail.IsNullEntity) {
		//                recurringfee = DriverPayments.GetNullEntity();
		//            }

		//        }

		//        return recurringfee;
		//    }
		//}
	}
}