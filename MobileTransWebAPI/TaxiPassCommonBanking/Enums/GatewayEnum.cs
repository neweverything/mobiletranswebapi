﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TaxiPassCommon.Banking {
	public enum GatewayEnum {
		Verisign,
		USAePay,
		DriverPromo,
		TPCard,
		Apriva,
		IPCommerce,
		Litle
	}
}
