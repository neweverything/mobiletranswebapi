﻿using CabRideEngineDapper.StoredProc;
using System.Web.Http;

namespace MobileTransWebAPI.Controllers {

	public class AffiliateDriverTransCountsController : ApiController {

		public usp_AffiliateDriverTransCounts Get(long pAffiliateId, long pAffiliateDriverId) {
			return usp_AffiliateDriverTransCounts.Execute(pAffiliateId, pAffiliateDriverId);
		}

	}
}
