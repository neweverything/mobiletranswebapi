﻿using Dapper;
using System.Data.SqlClient;

namespace CabRideEngineDapper {
    public partial class PayCardInfo {

        public static PayCardInfo GetByType(SqlConnection pConn, string pPayCardType) {
            string sql = @"SELECT * FROM PayCardInfo
                            WHERE PayCardType = @PayCardType";

            return pConn.Get<PayCardInfo>(sql, new { PayCardType = pPayCardType });

        }
    }
}
