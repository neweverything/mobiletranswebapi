﻿
namespace CabRideEngineDapper.Enums {

    public enum ComDataCardStatus {
        Nothing = 0,
        InformationComplete,
        Exported,
        Failed,
        Approved,
        NeedsNewCard
    }

    public static class ComDataCardStatusDesc {

        public static string GetStatus(int pStatus) {
            ComDataCardStatus myStat = (ComDataCardStatus)pStatus;

            string val = "";

            switch (myStat) {
                case ComDataCardStatus.Approved:
                case ComDataCardStatus.Exported:
                case ComDataCardStatus.Failed:
                    val = myStat.ToString();
                    break;

                case ComDataCardStatus.InformationComplete:
                    val = "Information Complete";
                    break;

                case ComDataCardStatus.NeedsNewCard:
                    val = "Needs New Card";
                    break;
            }

            return val;
        }
    }
}