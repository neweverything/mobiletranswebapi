﻿using Dapper;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using TaxiPassCommon;
using TaxiPassCommon.Cryptography;

namespace CabRideEngineDapper {
    public partial class BlackListCards {

        public string GetSetCardNo {
            get {
                return base.CardNo.DecryptIceKey();
            }
            set {
                Number = CryptoFns.MD5HashUTF16ToString(value);
                base.CardNo = value.EncryptIceKey();
            }
        }

        public string CardNoEncrypted {
            get {
                if (CardNo.Length > 16) {
                    return CardNo;
                } else {
                    return CardNo.EncryptIceKey();
                }
            }
        }

        public static bool IsBlackListed(SqlConnection pConn, string pCardNo) {
            bool result = false;
            if (!pCardNo.IsNullOrEmpty()) {
                string number = CryptoFns.MD5HashUTF16ToString(pCardNo);
                dynamic cardList = pConn.Query(string.Format("usp_BlackListCardsGetByNumber '{0}'", number)).ToList();
                foreach (var rec in cardList) {
                    string cardNo = rec.CardNo;
                    if (cardNo.DecryptIceKey().Equals(pCardNo)) {
                        result = true;
                        break;
                    }
                }
            }
            return result;
        }

        public static List<BlackListCards> GetAll(SqlConnection pConn) {
            List<BlackListCards> oList = pConn.Query<BlackListCards>("SELECT * FROM BlackListCards").ToList();
            return oList;
        }



    }
}
