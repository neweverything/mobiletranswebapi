﻿using CabRideEngineDapper;
using CabRideEngineDapper.Utils;
using Dapper;
using MobileTransWebAPI.Models;
using Newtonsoft.Json;
using System;
using System.Web.Http;
using TaxiPassCommon;

namespace MobileTransWebAPI.Controllers {
    public class UpdateWorkCityController : ApiController {

        // POST api/updateaffiliateid
        public DriverLoginResult Post([FromBody]string pRequest) {

            DriverLoginResult result = new DriverLoginResult();

            string request = pRequest.DecryptIceKey();
            WebApiApplication.LogIt("UpdateWorkCity", request);

            AffDriverLoginInfo info = new AffDriverLoginInfo();
            try {
                info = JsonConvert.DeserializeObject<AffDriverLoginInfo>(request);
            } catch (Exception ex) {
                result.ErrorMsg = ex.Message;
                return result;
            }

            DateTime requestDate = Convert.ToDateTime(info.RequestTime);

            if (WebApiApplication.GetUTCServerTime().Subtract(requestDate).TotalMinutes > 1) {
                result.ErrorMsg = "Invalid Access";
                return result;
            }

            if (info.WorkCity.IsNullOrEmpty()) {
                result.ErrorMsg = "Invalid city";
                return result;
            }

            using (var conn = SqlHelper.OpenSqlConnection()) {
                AffiliateDrivers affDriver = AffiliateDrivers.GetDriverByCallInCell(conn, info.Phone);
                if (affDriver.PIN == info.PIN) {
                    long affId = 0;
                    if (info.WorkCity.Equals("Newark", StringComparison.CurrentCultureIgnoreCase)) {
                        affId = 2021; // New Jersey - EWR Taxi Newark
                    } else if (info.WorkCity.Equals("Elizabeth", StringComparison.CurrentCultureIgnoreCase)) {
                        affId = 2022;
                    }
                    if (affId != 0) {
                        string sql1 = @"UPDATE dbo.AffiliateDrivers
                                       SET AffiliateID = @AffiliateID
                                       WHERE Cell = @Cell

                                       UPDATE dbo.Drivers
                                       SET AffiliateID = @AffiliateID
                                       WHERE Cell = @Cell";

                        conn.Execute(sql1, new { Cell = info.Phone, AffiliateID = affId });
                        result = new AffDriverLoginSimpleController().Login(pRequest);
                    }

                }

            }

            return result;

        }

    }
}
