using CabRideEngine.Enums;
using Dapper;
using IdeaBlade.Persistence;
using IdeaBlade.Util;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using TaxiPassCommon;
using TaxiPassCommon.Banking;


namespace CabRideEngine.Utils {
	public static class CreditCardUtils {

		//public static List<IGatewayCredentials> LoadGateWays(VerisignAccounts pAccount, CreditCardChargeType pType) {
		//	return LoadGateWays(pAccount.PersistenceManager, pAccount.VerisignAccountID, pType);
		//}


		// ***************************************************************************************
		// 4/23/2013
		// Given a DriverPaymentID, return the correct CardGatewayGroupsID (old VerisignAccountID)
		// ***************************************************************************************
		public static long GetDriverPaymentCardGatewayGroupsID(PersistenceManager pPM, string pTransNo, long pReservationID) {
			long lCardGatewayGroupsID = 0;
			StringBuilder sql = new StringBuilder();
			sql.Append("usp_DriverPaymentsGetProcessingGroupID ");
			if (!pTransNo.IsNullOrEmpty()) {
				sql.AppendFormat(" @TransNo = '{0}'", pTransNo);
			} else {
				sql.AppendFormat(" @ReservationID = {0}", pReservationID.ToString());
			}
			Entity[] oResults = DynamicEntities.DynamicDBCall(pPM, sql.ToString());
			try {
				if (oResults != null) {
					if (oResults.Length > 0) {
						lCardGatewayGroupsID = long.Parse(oResults[0]["CardGatewayGroupsID"].ToString());
					}
				}
			} catch (Exception) {
			}

			return lCardGatewayGroupsID;
		}


		public static List<IGatewayCredentials> LoadGateWays(PersistenceManager pPM, long pID, CreditCardChargeType pType, string pCardType) {
			List<IGatewayCredentials> gatewayList = new List<IGatewayCredentials>();

			var gateways = StoredProcs.usp_CardGatewaysGet.GetGateWays(pPM, pID);

			LitleCredentials litle = CreateLitleCredentials(gateways, pType);
			if (litle != null) {
				gatewayList.Add(litle);
			}

			USAePayCredentials ePay = CreateUSAePayCredentials(gateways, pType);
			if (ePay != null) {
				gatewayList.Add(ePay);
			}

			VerisignCredentials verisign = CreateVerisignCredentials(gateways);
			if (verisign != null) {
				gatewayList.Add(verisign);
			}

			AuthorizeNetCredentials authNet = CreateAuthorizeNetCredentials(gateways);
			if (authNet != null) {
				gatewayList.Add(authNet);
			}

			StripeCredentials stripe = CreateStripeCredentials(gateways);
			if (stripe != null) {
				gatewayList.Add(stripe);
			}

			return FilterGateways(pCardType, gatewayList);
		}


		public static List<IGatewayCredentials> LoadGateWays(PersistenceManager pPM, long pID, CardProcessors pProcessor, string pCardType) {
			List<IGatewayCredentials> gatewayList = new List<IGatewayCredentials>();

			var gateways = StoredProcs.usp_CardGatewaysGet.GetGateWays(pPM, pID);

			LitleCredentials litle = CreateLitleCredentials(gateways, CreditCardChargeType.Any, pProcessor.ToString());
			if (litle != null) {
				gatewayList.Add(litle);
			}

			USAePayCredentials ePay = CreateUSAePayCredentials(gateways, CreditCardChargeType.Any, pProcessor.ToString());
			if (ePay != null) {
				gatewayList.Add(ePay);
			}

			VerisignCredentials verisign = CreateVerisignCredentials(gateways);
			if (verisign != null) {
				gatewayList.Add(verisign);
			}

			AuthorizeNetCredentials authNet = CreateAuthorizeNetCredentials(gateways);
			if (authNet != null) {
				gatewayList.Add(authNet);
			}

			StripeCredentials stripe = CreateStripeCredentials(gateways);
			if (stripe != null) {
				gatewayList.Add(stripe);
			}

			var temp = FilterGateways(pCardType, gatewayList);

			return temp.Count == 0 ? gatewayList : temp;
		}


		private static List<IGatewayCredentials> FilterGateways(string pCardType, List<IGatewayCredentials> gatewayList) {
			// Get By CardType 1st
			List<IGatewayCredentials> newList = new List<IGatewayCredentials>();
			newList = (from p in gatewayList
					   where ("" + p.GetType().GetProperty("CardType").GetValue(p)).Equals(pCardType, StringComparison.CurrentCultureIgnoreCase)
					   orderby p.ProcessingOrder
					   select p).ToList();

			newList.AddRange((from p in gatewayList
							  where p.GetType().GetProperty("CardType").GetValue(p) == null
									|| p.GetType().GetProperty("CardType").GetValue(p).ToString().Equals("all", StringComparison.CurrentCultureIgnoreCase)
							  orderby p.ProcessingOrder
							  select p).ToList());
			return newList;
		}


		/// <summary>
		/// Creates the USAePay credentials for the gateway processing.
		/// </summary>
		/// <param name="pAccount">The account record.</param>
		/// <returns></returns>
		public static USAePayCredentials CreateUSAePayCredentials(VerisignAccounts pAccount) {
			USAePayCredentials cred = new USAePayCredentials();
			cred.ProdURL = pAccount.EPayProdURL;
			cred.SourceKey = pAccount.EPaySourceKey;
			cred.Pin = pAccount.EPayPin;
			cred.TestURL = pAccount.EPayTestURL;
			cred.SourceKeyTest = pAccount.EPaySourceKeyTest;
			cred.PinTest = pAccount.EPayPinTest;
			cred.Timeout = pAccount.VerisignTimeout.GetValueOrDefault(0);
			cred.MerchantID = pAccount.EPayMerchantID;
			cred.VerisignAccountID = pAccount.VerisignAccountID;
			return cred;
		}

		public static USAePayCredentials CreateUSAePayCredentials(List<StoredProcs.usp_CardGatewaysGet> pDataList, CreditCardChargeType pType, string pProcessor = "") {
			string processor = pProcessor;
			if (processor.IsNullOrEmpty()) {
				processor = string.Format("{0}{1}", CardProcessors.USAePay.ToString(), pType.ToString());
			} else if (!processor.StartsWith(CardProcessors.USAePay.ToString())) {
				return null;
			}

			List<StoredProcs.usp_CardGatewaysGet> temp = (from p in pDataList
														  where ("" + p.Processor).Equals(processor)
														  select p).ToList();
			if (temp.Count == 0) {
				processor = CardProcessors.USAePay.ToString();
				temp = (from p in pDataList
						where p.Processor.Equals(processor)
						select p).ToList();
				if (temp.Count == 0) {
					return null;
				}
			}

			USAePayCredentials cred = new USAePayCredentials();
			cred.CreditCardProcessor = processor.ToEnum<CardProcessors>();
			string salt = "";
			foreach (StoredProcs.usp_CardGatewaysGet rec in temp) {
				switch (rec.ReferenceKey) {
					case "MerchantID":
						cred.MerchantID = rec.ValueString;
						break;

					case "Password":
						cred.Pin = rec.ValueString.DecryptIceKey();
						break;

					case "TestPassword":
						cred.PinTest = rec.ValueString.DecryptIceKey();
						break;

					case "TestURL":
						cred.TestURL = rec.ValueString;
						break;

					case "TestUserID":
						cred.SourceKeyTest = rec.ValueString;
						break;

					case "Timeout":
						cred.Timeout = Convert.ToInt32(rec.ValueString);
						break;

					case "URL":
						cred.ProdURL = rec.ValueString;
						break;

					case "UserID":
						cred.SourceKey = rec.ValueString;
						break;

					case "Validate":
						if (rec.ValueString.IsNullOrEmpty()) {
							cred.Validate = false;
						} else {
							cred.Validate = Convert.ToBoolean(rec.ValueString);
						}
						break;

					case "Salt":
						salt = rec.ValueString;
						break;

					case "TerminalType":
						cred.TerminalType = rec.ValueString;
						break;

					case "CardType":
						cred.CardType = rec.ValueString;
						break;
				}
			}

			// decrypt passwords
			//if (!salt.IsNullOrEmpty()) {
			//    Encryption oDecrypt = new Encryption();
			//    //if (!cred.PinTest.IsNullOrEmpty()) {
			//    //    cred.PinTest = oDecrypt.Decrypt(cred.PinTest, VerisignAccounts.ENCRYPT_KEY);
			//    //}

			//    if (!cred.Pin.IsNullOrEmpty()) {
			//        cred.Pin = oDecrypt.Decrypt(cred.Pin, VerisignAccounts.ENCRYPT_KEY);
			//    }

			//}

			if (temp.Count > 0) {
				cred.VerisignAccountID = temp[0].CardGatewayGroupsID;
				cred.ProcessingOrder = temp[0].ProcessorOrder;
			}
			return cred;

		}

		/// <summary>
		/// Creates the Litle credentials for the gateway processing.
		/// </summary>
		/// <param name="pAccount">The account record.</param>
		/// <returns></returns>
		public static LitleCredentials CreateLitleCredentials(List<StoredProcs.usp_CardGatewaysGet> pDataList, CreditCardChargeType pType, string pProcessor = "") {
			string processor = pProcessor;
			if (processor.IsNullOrEmpty()) {
				processor = string.Format("{0}{1}", CardProcessors.Litle.ToString(), pType.ToString());
			} else if (!processor.StartsWith(CardProcessors.Litle.ToString())) {
				return null;
			}
			var temp = (from p in pDataList
						where ("" + p.Processor).Equals(processor, StringComparison.CurrentCultureIgnoreCase)
						select p).ToList();
			if (temp.Count == 0) {
				processor = CardProcessors.Litle.ToString();
				temp = (from p in pDataList
						where p.Processor.Equals(processor)
						select p).ToList();
				if (temp.Count == 0) {
					return null;
				}
			}

			LitleCredentials cred = new LitleCredentials();

			foreach (StoredProcs.usp_CardGatewaysGet rec in temp) {
				switch (rec.ReferenceKey) {
					case "MerchantID":
						cred.MerchantID = rec.ValueString;
						break;

					case "GetRideMerchantID":
						cred.GetRideMerchantID = rec.ValueString;
						break;

					case "TestMerchantID":
						cred.TestMerchantID = rec.ValueString;
						break;

					case "Password":
						cred.Password = rec.ValueString.DecryptIceKey();
						break;

					case "TestPassword":
						cred.TestPassword = rec.ValueString.DecryptIceKey();
						break;

					case "TestURL":
						cred.TestURL = rec.ValueString;
						break;

					case "TestUserID":
						cred.TestUserName = rec.ValueString;
						break;

					case "Timeout":
						cred.Timeout = Convert.ToInt32(rec.ValueString);
						break;

					case "URL":
						cred.ProdURL = rec.ValueString;
						break;

					case "UserID":
						cred.UserName = rec.ValueString;
						break;

					case "TerminalType":
						cred.TerminalType = rec.ValueString;
						break;

					case "CardType":
						cred.CardType = rec.ValueString;
						break;

					case "BillingDescriptor":
						cred.BillingDescriptor = rec.ValueString;
						break;

					case "ExcludeBillingDescriptorPrefix":
						cred.ExcludeBillingDescriptorPrefix = Convert.ToBoolean(rec.ValueString);
						break;
				}
			}

			if (cred.UserName.IsNullOrEmpty()) {
				cred.UserName = cred.TestUserName;
			}
			if (cred.Password.IsNullOrEmpty()) {
				cred.Password = cred.TestPassword;
			}
			if (cred.MerchantID.IsNullOrEmpty()) {
				cred.MerchantID = cred.TestMerchantID;
			}
			if (cred.ProdURL.IsNullOrEmpty()) {
				cred.ProdURL = cred.TestURL;
			}
			if (temp.Count > 0) {
				cred.Validate = temp[0].Validate;
				cred.VerisignAccountID = temp[0].CardGatewayGroupsID;
				cred.ProcessingOrder = temp[0].ProcessorOrder;
			}
			cred.CreditCardProcessor = processor.ToEnum<CardProcessors>();

			return cred;
		}


		/// <summary>
		/// Creates the verisign credentials.
		/// </summary>
		/// <param name="pAccount">The Verisign Account record.</param>
		/// <returns></returns>
		public static VerisignCredentials CreateVerisignCredentials(VerisignAccounts pAccount) {
			VerisignCredentials cred = new VerisignCredentials();
			cred.ProdURL = pAccount.VerisignAddress;
			cred.TestURL = "pilot-" + pAccount.VerisignAddress;
			cred.Partner = pAccount.VerisignPartner;
			cred.Vendor = pAccount.VersignVendor;
			cred.UserName = pAccount.VerisignUserName;
			cred.Password = pAccount.VerisignPassword;
			cred.Port = pAccount.VerisignPort.GetValueOrDefault(443);
			cred.Timeout = pAccount.VerisignTimeout.GetValueOrDefault(0);
			cred.MerchantID = pAccount.VerisignMerchantID;
			cred.VerisignAccountID = pAccount.VerisignAccountID;
			return cred;
		}

		public static VerisignCredentials CreateVerisignCredentials(List<StoredProcs.usp_CardGatewaysGet> pDataList) {
			var temp = (from p in pDataList
						where ("" + p.Processor).Equals(CardProcessors.Verisign.ToString())
						select p).ToList();
			if (temp.Count == 0) {
				return null;
			}

			VerisignCredentials cred = new VerisignCredentials();
			string salt = "";

			foreach (StoredProcs.usp_CardGatewaysGet rec in temp) {
				switch (rec.ReferenceKey) {
					case "AccountName":
						cred.Vendor = rec.ValueString;
						break;

					case "MerchantID":
						cred.MerchantID = rec.ValueString;
						break;

					case "Partner":
						cred.Partner = rec.ValueString;
						break;

					case "Password":
						cred.Password = rec.ValueString.DecryptIceKey();
						break;

					case "Port":
						cred.Port = Convert.ToInt32(rec.ValueString);
						break;

					case "Salt":
						salt = rec.ValueString;
						break;

					case "TestURL":
						cred.TestURL = rec.ValueString;
						break;

					case "TestUserID":
						cred.TestUserName = rec.ValueString;
						break;

					case "TestPassword":
						cred.TestPassword = rec.ValueString.DecryptIceKey();
						break;

					case "Timeout":
						cred.Timeout = Convert.ToInt32(rec.ValueString);
						break;

					case "URL":
						cred.ProdURL = rec.ValueString;
						break;

					case "UserID":
						cred.UserName = rec.ValueString;
						break;

					case "CardType":
						cred.CardType = rec.ValueString;
						break;

				}
			}

			//// decrypt passwords
			//if (!salt.IsNullOrEmpty()) {
			//    Encryption oDecrypt = new Encryption();
			//    if (!cred.TestPassword.IsNullOrEmpty()) {
			//        cred.TestPassword = oDecrypt.Decrypt(cred.TestPassword, VerisignAccounts.ENCRYPT_KEY);
			//    }

			//    if (!cred.Password.IsNullOrEmpty()) {
			//        cred.Password = oDecrypt.Decrypt(cred.Password, VerisignAccounts.ENCRYPT_KEY);
			//    }

			//}

			if (temp.Count > 0) {
				cred.Validate = temp[0].Validate;
				cred.VerisignAccountID = temp[0].CardGatewayGroupsID;
				cred.ProcessingOrder = temp[0].ProcessorOrder;
			}

			return cred;
		}


		/// <summary>
		/// Creates the Authorize.Net credentials for the gateway processing.
		/// </summary>
		/// <param name="pAccount">The account record.</param>
		/// <returns></returns>
		public static AuthorizeNetCredentials CreateAuthorizeNetCredentials(List<StoredProcs.usp_CardGatewaysGet> pDataList) {
			string processor = CardProcessors.AuthorizeNet.ToString();

			var temp = (from p in pDataList
						where ("" + p.Processor).Equals(processor, StringComparison.CurrentCultureIgnoreCase)
						select p).ToList();
			if (temp.Count == 0) {
				processor = CardProcessors.AuthorizeNet.ToString();
				temp = (from p in pDataList
						where p.Processor.Equals(processor)
						select p).ToList();
				if (temp.Count == 0) {
					return null;
				}
			}

			AuthorizeNetCredentials cred = new AuthorizeNetCredentials();

			foreach (StoredProcs.usp_CardGatewaysGet rec in temp) {
				switch (rec.ReferenceKey) {
					case "MerchantID":
						cred.MerchantID = rec.ValueString;
						break;

					case "GetRideMerchantID":
						cred.GetRideMerchantID = rec.ValueString;
						break;

					case "TestMerchantID":
						cred.TestMerchantID = rec.ValueString;
						break;

					case "Password":
						cred.Password = rec.ValueString.DecryptIceKey();
						break;

					case "TestPassword":
						cred.TestPassword = rec.ValueString.DecryptIceKey();
						break;

					case "TestURL":
						cred.TestURL = rec.ValueString;
						break;

					case "TestUserID":
						cred.TestUserName = rec.ValueString;
						break;

					case "Timeout":
						if (rec.ValueString.IsNumeric()) {
							cred.Timeout = Convert.ToInt32(rec.ValueString);
						}
						break;

					case "URL":
						cred.ProdURL = rec.ValueString;
						break;

					case "UserID":
						cred.UserName = rec.ValueString;
						break;

					case "TerminalType":
						cred.TerminalType = rec.ValueString;
						break;

					case "CardType":
						cred.CardType = rec.ValueString;
						break;

					case "BillingDescriptor":
						cred.BillingDescriptor = rec.ValueString;
						break;

					case "ExcludeBillingDescriptorPrefix":
						cred.ExcludeBillingDescriptorPrefix = Convert.ToBoolean(rec.ValueString);
						break;
				}
			}

			if (cred.UserName.IsNullOrEmpty()) {
				cred.UserName = cred.TestUserName;
			}
			if (cred.Password.IsNullOrEmpty()) {
				cred.Password = cred.TestPassword;
			}
			if (cred.MerchantID.IsNullOrEmpty()) {
				cred.MerchantID = cred.TestMerchantID;
			}
			if (cred.ProdURL.IsNullOrEmpty()) {
				cred.ProdURL = cred.TestURL;
			}
			if (temp.Count > 0) {
				cred.Validate = temp[0].Validate;
				cred.VerisignAccountID = temp[0].CardGatewayGroupsID;
				cred.ProcessingOrder = temp[0].ProcessorOrder;
			}
			cred.CreditCardProcessor = processor.ToEnum<CardProcessors>();

			return cred;
		}


		private static StripeCredentials CreateStripeCredentials(List<StoredProcs.usp_CardGatewaysGet> pDataList) {
			string processor = CardProcessors.Stripe.ToString();

			var temp = (from p in pDataList
						where ("" + p.Processor).Equals(processor, StringComparison.CurrentCultureIgnoreCase)
						select p).ToList();
			if (temp.Count == 0) {
				processor = CardProcessors.Stripe.ToString();
				temp = (from p in pDataList
						where p.Processor.Equals(processor)
						select p).ToList();
				if (temp.Count == 0) {
					return null;
				}
			}

			StripeCredentials cred = new StripeCredentials();

			foreach (StoredProcs.usp_CardGatewaysGet rec in temp) {
				switch (rec.ReferenceKey) {
					case "MerchantID":
						cred.MerchantID = rec.ValueString;
						break;

					case "TestMerchantID":
						cred.TestMerchantID = rec.ValueString;
						break;

					case "TestUserID":
						cred.TestAPIKey = rec.ValueString;
						break;

					case "Timeout":
						if (rec.ValueString.IsNumeric()) {
							cred.Timeout = Convert.ToInt32(rec.ValueString);
						}
						break;

					case "UserID":
						cred.APIKey = rec.ValueString;
						break;


					case "CardType":
						cred.CardType = rec.ValueString;
						break;

					case "BillingDescriptor":
						cred.BillingDescriptor = rec.ValueString;
						break;

					case "ExcludeBillingDescriptorPrefix":
						cred.ExcludeBillingDescriptorPrefix = Convert.ToBoolean(rec.ValueString);
						break;
				}
			}

			if (temp.Count > 0) {
				cred.Validate = temp[0].Validate;
				cred.VerisignAccountID = temp[0].CardGatewayGroupsID;
				cred.ProcessingOrder = temp[0].ProcessorOrder;
			}
			cred.CreditCardProcessor = processor.ToEnum<CardProcessors>();

			return cred;

		}

		/// <summary>
		/// Validates the card number.
		/// </summary>
		/// <param name="pCardNumber">The p card number.</param>
		/// <param name="pCardType">Type of the p card.</param>
		/// <param name="pPM">The p PM.</param>
		/// <returns></returns>
		/// <exception cref="System.Exception">Card Number does not match card type</exception>
		[Obsolete]
		public static bool ValidateCardNumber(string pCardNumber, string pCardType, PersistenceManager pPM) {
			int length = pCardNumber.Length;
			if (length == 0) {
				return true;
			}

			if (!ValidCardType(pCardNumber, pCardType, pPM)) {
				//e.Description = "Card Number does not match card type";
				throw new Exception("Card Number does not match card type");
			}

			if (length < 9) {
				return false;
			}

			if (DriverPromotions.GetPromotionsByCardNo(pPM, pCardNumber).Count > 0) {
				return true;
			}

			if (!BlackListCards.GetCard(pPM, pCardNumber).IsNullEntity) {
				throw new Exception("Invalid Card Number!");
			}

			int sum = 0;
			int offset = length % 2;
			byte[] digits = new System.Text.ASCIIEncoding().GetBytes(pCardNumber);
			//char[] digits = cardNumber.ToCharArray();

			for (int i = 0; i < length; i++) {
				digits[i] -= 48;
				if (((i + offset) % 2) == 0) {
					digits[i] *= 2;
				}
				sum += (digits[i] > 9) ? digits[i] - 9 : digits[i];
			}
			return ((sum % 10) == 0);
		}

		public static bool ValidateCardNumber(string pCardNumber, string pCardType, SqlConnection pConn) {
			int length = pCardNumber.Length;
			if (length == 0) {
				return true;
			}

			if (!ValidCardType(pCardNumber, pCardType, pConn)) {
				throw new Exception("Card Number does not match card type");
			}

			if (length < 9) {
				return false;
			}

			if (DriverPromotions.IsPromoCard(pConn, pCardNumber)) {
				return true;
			}

			if (BlackListCards.IsBlackListed(pConn, pCardNumber)) {
				throw new Exception("Invalid Card Number!");
			}

			int sum = 0;
			int offset = length % 2;
			byte[] digits = new System.Text.ASCIIEncoding().GetBytes(pCardNumber);
			//char[] digits = cardNumber.ToCharArray();

			for (int i = 0; i < length; i++) {
				digits[i] -= 48;
				if (((i + offset) % 2) == 0) {
					digits[i] *= 2;
				}
				sum += (digits[i] > 9) ? digits[i] - 9 : digits[i];
			}
			return ((sum % 10) == 0);
		}



		/// <summary>
		/// Decrypt credit card number
		/// </summary>
		/// <param name="pCustomer"></param>
		/// <param name="pCardNumber"></param>
		/// <returns></returns>
		public static string DecryptCardNo(Customer pCustomer, string pCardNumber) {
			return DecryptCardNo(pCustomer, pCardNumber, true);
		}

		/// <summary>
		/// Decrypts the card no.
		/// </summary>
		/// <param name="pCustomer">The customer.</param>
		/// <param name="pCardNumber">The card number.</param>
		/// <param name="pHideNumber">if set to <c>true</c> [hide number].</param>
		/// <returns></returns>
		public static string DecryptCardNo(Customer pCustomer, string pCardNumber, bool pHideNumber) {
			if (String.IsNullOrEmpty(pCardNumber)) {
				return "";
			}
			string sCardNo = DoDecryption(pCardNumber, GetSalt(pCustomer));
			if (pHideNumber) {
				int len = sCardNo.Length - 5;
				string sNewCardNo = "";
				if (len > 5) {
					sNewCardNo = new string('*', len) + sCardNo.Substring(len);
				}
				return sNewCardNo;
			}
			return sCardNo;
		}


		/// <summary>
		/// Does the decryption.
		/// </summary>
		/// <param name="pCardNumber">The card number.</param>
		/// <param name="pSalt">The salt.</param>
		/// <returns></returns>
		private static string DoDecryption(string pCardNumber, string pSalt) {
			return CryptoFns.SimpleDESDecrypt(pCardNumber, pSalt);
		}


		/// <summary>
		/// Gets the salt.
		/// </summary>
		/// <param name="pCustomer">The customer.</param>
		/// <returns></returns>
		private static string GetSalt(Customer pCustomer) {
			return (pCustomer.Salt == null ? "taxipass" : "TaXi" + pCustomer.Salt + "pAsS");
		}


		/// <summary>
		/// Encrypts Credit Card Number
		/// </summary>
		/// <param name="pCustomer"></param>
		/// <param name="pCardType"></param>
		/// <param name="pCardNumber"></param>
		/// <returns>returns encrypted string</returns>
		public static string EncryptCardNo(Customer pCustomer, string pCardType, string pCardNumber) {
			pCardNumber = pCardNumber.Trim();
			using (SqlConnection oConn = SQLHelper.OpenSqlConnection(pCustomer.PersistenceManager)) {
				if (pCardType.IsNullOrEmpty()) {
					pCardType = DetermineCardType(oConn, pCardNumber);
				}
				if (ValidateCardNumber(pCardNumber, pCardType, oConn)) {
					string salt = GetSalt(pCustomer);
					return CryptoFns.SimpleDESEncrypt(pCardNumber, salt);
				} else {
					throw new Exception("Invalid Card Number!");
				}
			}
		}


		/// <summary>
		/// Validates the type of the card.
		/// </summary>
		/// <param name="pCardNumber">The card number.</param>
		/// <param name="pCardType">Type of the card.</param>
		/// <param name="pPM">The pPM.</param>
		/// <returns></returns>
		private static bool ValidCardType(String pCardNumber, string pCardType, PersistenceManager pPM) {
			SqlConnection oConn = SQLHelper.OpenSqlConnection(pPM);
			return ValidCardType(pCardNumber, pCardType, oConn);
		}

		private static bool ValidCardType(String pCardNumber, string pCardType, SqlConnection pConn) {
			bool blnValid = false;

			StringBuilder sql = new StringBuilder();
			sql.AppendFormat("usp_DetermineCardType @CardNo = '{0}'", pCardNumber);
			sql.AppendFormat(", @CardType = '{0}'", pCardType);
			dynamic cardTypeList = pConn.Query(sql.ToString()).ToList();

			foreach (var cardType in cardTypeList) {
				string specialCardType = cardType.SpecialCardType;
				if (!specialCardType.IsNullOrEmpty()) {
					blnValid = true;
				} else if (Regex.IsMatch(pCardNumber, cardType.Validation)) {
					blnValid = true;
				}
				if (blnValid) {
					break;
				}
			}


			return blnValid;
		}


		// *******************************************************************************************
		// 10/11/13 use usp_DetermineCardType to return either "SpecialCardType" (DriverPromo, TPCard)
		// or the list of card types (visa, Diners, etc) and their validation algorithms
		// *******************************************************************************************
		public static string DetermineCardType(PersistenceManager pPM, string pCardNumber) {
			SqlConnection oConn = SQLHelper.OpenSqlConnection(pPM);
			return DetermineCardType(oConn, pCardNumber);
		}

		public static string DetermineCardType(SqlConnection pConn, string pCardNumber) {
			string sCardType = "";

			if (!pCardNumber.IsNullOrEmpty()) {

				pCardNumber = pCardNumber.Replace('*', '1');

				// card must be numeric
				if (pCardNumber.IsNumeric()) {
					StringBuilder sql = new StringBuilder();
					sql.AppendFormat("usp_DetermineCardType @CardNo = '{0}'", pCardNumber);

					dynamic cardTypeList = pConn.Query(sql.ToString()).ToList();
					foreach (var cardType in cardTypeList) {
						string specialCardType = cardType.SpecialCardType;
						if (!specialCardType.IsNullOrEmpty()) {
							sCardType = cardType.SpecialCardType;
						} else if (Regex.IsMatch(pCardNumber, cardType.Validation)) {
							sCardType = cardType.CardType;
						}
						if (!sCardType.IsNullOrEmpty()) {
							break;
						}
					}
				}
			}
			return sCardType;
		}

		/// <summary>
		/// Validates that the card number is valid based on the type of card
		/// </summary>
		/// <param name="pCustomer"></param>
		/// <param name="pCardType"></param>
		/// <param name="pCardNumber"></param>
		/// <returns></returns>
		public static bool IsValidCardNumber(Customer pCustomer, string pCardType, string pCardNumber) {
			string cardNo = DoDecryption(pCardNumber, GetSalt(pCustomer));

			// Handle Null, zero length, or non-numeric input as false
			if (cardNo == null || cardNo.Trim().Length == 0) {
				throw new Exception("Card number cannot be empty");
			}

			// card must be numeric
			try {
				if (Convert.ToDecimal(cardNo) == 0) {
					throw new Exception("Card Number must be numeric");
				}
			} catch (Exception ex) {
				throw ex;
			}
			bool bOk;
			using (SqlConnection oConn = SQLHelper.OpenSqlConnection(pCustomer.PersistenceManager)) {
				bOk = ValidateCardNumber(cardNo, pCardType, oConn);
				if (!bOk) {
					throw new Exception("Invalid card number!");
				}
			}
			return bOk;
		}

		/// <summary>
		/// Determines whether [is valid card number] [the specified p PM].
		/// </summary>
		/// <param name="pPM">The Persistence Manager.</param>
		/// <param name="pCardType">Type of the card.</param>
		/// <param name="pCardNumber">The card number.</param>
		/// <returns>
		///   <c>true</c> if [is valid card number] [the specified p PM]; otherwise, <c>false</c>.
		/// </returns>
		/// <exception cref="System.Exception">Card number cannot be empty</exception>
		public static bool IsValidCardNumber(PersistenceManager pPM, string pCardType, string pCardNumber) {
			// Handle Null, zero length, or non-numeric input as false
			if (pCardNumber == null || pCardNumber.Trim().Length == 0) {
				throw new Exception("Card number cannot be empty");
			}

			// card must be numeric
			if (!pCardNumber.IsNumeric()) {
				throw new Exception("Card Number must be numeric");
			}

			bool bOk;
			using (SqlConnection oConn = SQLHelper.OpenSqlConnection(pPM)) {
				bOk = ValidateCardNumber(pCardNumber, pCardType, SQLHelper.OpenSqlConnection(pPM));
				if (!bOk) {
					throw new Exception("Invalid card number!");
				}
			}

			return bOk;
		}


		/// <summary>
		/// Authorizes the card.
		/// </summary>
		/// <param name="pReserve">The reservation.</param>
		/// <returns></returns>
		public static CardResults AuthorizeCard(Reservations pReserve) {
			CardResults oResults;
			try {
				oResults = AuthorizeCard(pReserve, false, true);
			} catch (Exception ex) {
				throw ex;
			}
			return oResults;
		}

		/// <summary>
		/// Authorizes the card.
		/// </summary>
		/// <param name="pReserve">The reservation.</param>
		/// <param name="pForceAuthorization">if set to <c>true</c> [p force authorization].</param>
		/// <param name="pSaveRecord">if set to <c>true</c> [p save record].</param>
		/// <returns></returns>
		/// <exception cref="System.Exception">Could not decrypt card number</exception>
		public static CardResults AuthorizeCard(Reservations pReserve, bool pForceAuthorization, bool pSaveRecord) {
			if (DoDecryption(pReserve["CardNumber"].ToString(), GetSalt(pReserve.Customer)).Length == 0) {
				throw new Exception("Could not decrypt card number");
			}

			if (pReserve.Cancelled) {
				throw new Exception("Can not charge card.  Reservation is cancelled");
			}
			if (pReserve.Closed) {
				throw new Exception("Can not charge card.  Reservation is closed");
			}

			PersistenceManager oPM = pReserve.PersistenceManager;
			if (!pForceAuthorization) {
				if (pReserve.IsCreditCardAuthorized) {
					throw new Exception("Credit card is already authorized");
				}
			}

			long lCardGatewayGroupsID = Utils.CreditCardUtils.GetDriverPaymentCardGatewayGroupsID(pReserve.PersistenceManager, "", pReserve.ReservationID);
			CreditCardChargeType chargeType = CreditCardChargeType.ECommerce;
			Gateway gway = new Gateway(Utils.CreditCardUtils.LoadGateWays(pReserve.PersistenceManager, lCardGatewayGroupsID, chargeType, pReserve.CardType));

			string cardNo = DoDecryption(pReserve["CardNumber"].ToString(), GetSalt(pReserve.Customer));
			string sComment = pReserve.ServiceType;

			CreditCard cardData = new CreditCard();
			cardData.CardNo = cardNo;
			cardData.CardExpiry = pReserve.CardExpiration.Value;
			cardData.CVV = pReserve.CardVerification;
			cardData.AvsStreet = pReserve.Street;
			cardData.AvsZip = pReserve.Zip;

			decimal amount = pReserve.TotalCharge.GetValueOrDefault(0);

			string sServiceType = pReserve.Affiliate.ServiceType;
			if (string.IsNullOrEmpty(sServiceType)) {
				sServiceType = "Taxi";
			}
			sServiceType += " Service";

			CCRequest req = new CCRequest(cardData, amount, sComment, pReserve.ReserveNo, sServiceType, pReserve.AccountNo, pReserve.DriverID.ToString(), true);
			List<ProcessorResponse> responseList = gway.PreAuthCard(req);

			CardResults oResult = oPM.GetNullEntity<CardResults>();
			foreach (ProcessorResponse response in responseList) {
				oResult = SaveResponse(pReserve, response, VerisignTrxTypes.Authorization);

				if (pSaveRecord) {
					oResult.Save();
				}
			}
			return oResult;
		}

		public static CardResults AuthorizeCard(PersistenceManager pPM, string pCardNo, DateTime pCardExpiration, string pCVV, Address pBillingAddress, decimal pTotal, string pPlatform, CreditCardChargeType pType, string pCardType) {

			//VerisignAccounts oAcct = VerisignAccounts.GetDefaultAccount(pPM);
			long verisignAcctID = -1;
			using (var oConn = SQLHelper.OpenSqlConnection(pPM)) {
				verisignAcctID = oConn.Query("SELECT CardGatewayGroupsId FROM [dbo].[CardGatewayGroups] WITH (NOLOCK) WHERE DefaultGroup = 1").FirstOrDefault().CardGatewayGroupsId;
			}


			CreditCard cardData = new CreditCard();
			cardData.CardNo = pCardNo;
			cardData.CardExpiry = pCardExpiration;
			cardData.CVV = pCVV;
			cardData.AvsStreet = pBillingAddress.Street;
			cardData.AvsZip = pBillingAddress.ZIP;

			string sComment = "Taxi";

			Gateway gway = new Gateway(Utils.CreditCardUtils.LoadGateWays(pPM, verisignAcctID, CreditCardChargeType.ECommerce, pCardType));

			double amount = Convert.ToDouble(pTotal);

			string sServiceType = "Taxi Service";

			List<ProcessorResponse> responseList = gway.PreAuthCard(new CCRequest(cardData, pTotal, sComment, "", sServiceType, pPlatform, "", true));
			CardResults oResult = null;
			Reservations oReserve = pPM.GetNullEntity<Reservations>();
			foreach (ProcessorResponse response in responseList) {
				oResult = SaveResponse(oReserve, response, VerisignTrxTypes.Authorization);
			}

			oResult.Number = pCardNo;
			return oResult;
		}

		public static void ChargeCard(Reservations pReserve) {
			ChargeCard(pReserve, "");
		}

		public static void ChargeCard(Reservations pReserve, string pMagStripe) {
			if (DoDecryption(pReserve["CardNumber"].ToString(), GetSalt(pReserve.Customer)).Length == 0) {
				return;
			}
			if (pReserve.Closed) {
				return;
			}

			CardResults oResult = pReserve.GetAuthorizedRecord();
			if (oResult.IsNullEntity) {
				// Do sale transaction
				DoSale(pReserve, pMagStripe, pReserve.AccountNo);
			} else {
				// Do a delayed capture transaction
				DoDelayedSale(oResult);
			}
			SavePayment(pReserve);
		}

		private static void SavePayment(Reservations pReserve) {
			PersistenceManager oPM = pReserve.PersistenceManager;
			if (!pReserve.IsCreditCardCharged) {
				return;
			}
			Deposit oDeposit = Deposit.Create(oPM);
			oDeposit.CardNumber = pReserve["CardNumber"].ToString();
			oDeposit.Total = pReserve.TotalCharge;
			oDeposit.PaymentType = pReserve.CardType;

			Payments oPayment = Payments.Create(oPM, oDeposit.DepositID);
			oPayment.ReservationID = pReserve.ReservationID;
			oPayment.CustomerID = pReserve.CustomerID;

			try {
				SaveResult oResult = oDeposit.Save();
				if (oResult.Ok) {
					oResult = oPayment.Save();
				}
				if (!oResult.Ok) {
					throw oResult.Exception;
				}
			} catch (PersistenceManagerSaveException ex) {
				throw ex;
			}
		}

		private static void DoDelayedSale(CardResults pCardResult) {
			PersistenceManager oPM = pCardResult.PersistenceManager;

			if (pCardResult.Reservations.IsCreditCardCharged) {
				return;
			}


			VerisignAccounts oAcct = VerisignAccounts.GetDefaultAccount(oPM);
			//long verisignAcctID = -1;
			//using (var oConn = SQLHelper.OpenSqlConnection(oPM)) {
			//	verisignAcctID = oConn.Query("SELECT CardGatewayGroupsId FROM [dbo].[CardGatewayGroups] WITH (NOLOCK) WHERE DefaultGroup = 1").FirstOrDefault().CardGatewayGroupsId;
			//}

			USAePay oPay = new USAePay(CreateUSAePayCredentials(oAcct));

			string cardNo = DoDecryption(pCardResult["Number"].ToString(), GetSalt(pCardResult.Reservations.Customer));
			CreditCard oCard = new CreditCard();
			oCard.CardNo = cardNo;

			string sRefNo = pCardResult.Reference;
			string sTranType = VerisignTrxTypes.DelayedCapture;

			decimal total = pCardResult.Reservations.TotalCharge.Value;
			try {
				ProcessorResponse oResponse = oPay.ChargeAuth(new CCRequestRefNo(sRefNo, total, oCard.TestCard, pCardResult.Reservations.Driver.DriverID.ToString(), pCardResult.Reservations.ReserveNo, true, ""));
				SaveResponse(pCardResult.Reservations, oResponse, sTranType);
			} catch {
				//failed so try a regular charge
				DoSale(pCardResult.Reservations);
			}
		}


		private static void DoSale(Reservations pReserve) {
			DoSale(pReserve, "", pReserve.AccountNo);
		}

		private static void DoSale(Reservations pReserve, string pMagStripe, string pPlatform) {
			PersistenceManager oPM = pReserve.PersistenceManager;

			if (pReserve.IsCreditCardCharged || pReserve.Closed) {
				return;
			}

			string cardNo = DoDecryption(pReserve["CardNumber"].ToString(), GetSalt(pReserve.Customer));

			long lCardGatewayGroupsID = Utils.CreditCardUtils.GetDriverPaymentCardGatewayGroupsID(pReserve.PersistenceManager, "", pReserve.ReservationID);
			Gateway gway = new Gateway(Utils.CreditCardUtils.LoadGateWays(pReserve.PersistenceManager, lCardGatewayGroupsID, CreditCardChargeType.ECommerce, pReserve.CardType));

			string sTranType = VerisignTrxTypes.Sale;

			CreditCard cardData = new CreditCard();
			if (pMagStripe.IsNullOrEmpty()) {
				cardData.CardNo = cardNo;
				cardData.CardExpiry = pReserve.CardExpiration.Value;
				cardData.CVV = pReserve.CardVerification;
			} else {
				cardData.Track2 = pMagStripe;
				cardData.CardPresent = true;
			}
			string sComment = pReserve.ServiceType;


			cardData.AvsStreet = pReserve.Street;
			cardData.AvsZip = pReserve.Zip;

			string sServiceType = pReserve.Affiliate.ServiceType;
			if (string.IsNullOrEmpty(sServiceType)) {
				sServiceType = "Taxi";
			}
			sServiceType += " Service";

			List<ProcessorResponse> respList = gway.ChargeCard(new CCRequest(cardData, pReserve.TotalCharge.Value, sComment, pReserve.ReserveNo, sServiceType, pPlatform, "R, " + pReserve.Driver.DriverID.ToString(), true));
			foreach (ProcessorResponse response in respList) {
				SaveResponse(pReserve, response, sTranType).Save();
			}

		}


		private static CardResults SaveResponse(Reservations pReserve, ProcessorResponse pResponse, string pTrxType) {
			PersistenceManager oPM = pReserve.PersistenceManager;

			CardResults oResult = CardResults.Create(oPM, pReserve.ReservationID);
			oResult.Number = pReserve["CardNumber"].ToString();

			oResult.Result = pResponse.Result;
			if (oResult.Result == null) {
				oResult.Result = "0";
			}
			oResult.Reference = pResponse.Reference;
			oResult.Message = pResponse.Message;
			oResult.AuthCode = pResponse.AuthCode;
			if (string.IsNullOrEmpty(pResponse.AVSResultCode)) {
				oResult.Avs = "";
				oResult.Avszip = "";
			} else {
				oResult.Avs = pResponse.AVSResultCode.Substring(0, 1);
				oResult.Avszip = pResponse.AVSResultCode.Substring(1, 1);
			}
			oResult.CVV2Match = pResponse.CVV2Match;

			oResult.TransType = pTrxType;
			oResult.TransDate = DateTime.Now;
			return oResult;
			//oResult.Save();
		}




		//public static bool IsTestCard(string pCardNo) {
		//    bool bTestCard = false;
		//    switch (pCardNo) {
		//        case "5555555555554444":
		//        case "4111111111111111":
		//        case "4012888888881881":
		//        case "5105105105105100":
		//        case "378282246310005":
		//        case "371449635398431":
		//        case "378734493671000":
		//        case "6011111111111117":
		//        case "6011000990139424":
		//        case "5551212833217698":
		//        case "4000200011112222":
		//        case "4000100011112224":
		//            bTestCard = true;
		//            break;
		//    }
		//    return bTestCard;
		//}

		//public static string TEST_CARD_WHITE = "5551212833217698";
		//public static string TEST_CARD_NEIL = "4003000123456781";
		//public static string TEST_CARD_VISA = "4111111111111111";
		//public static string USE_TEST_CARD = ";4111111111111111=12121010000047520001?";

	}
}
