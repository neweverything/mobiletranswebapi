﻿namespace TaxiPassCommon.Banking.Models.GlobalCashCard {
    public class FundBalanceResult {

        public string STATUS { get; set; }
        public string RESPONSECODE { get; set; }
        public Balance RESULTS { get; set; }

        public class Balance {
            public string BALANCE { get; set; }
        }

    }
}

