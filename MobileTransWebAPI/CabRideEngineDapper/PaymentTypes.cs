﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Dapper;

namespace CabRideEngineDapper {
    public partial class PaymentTypes {

        public static PaymentTypes GetType(SqlConnection pConn, string pType) {
            string sql = @"SELECT * FROM dbo.PaymentTypes WHERE PaymentType = @PaymentType";

            return pConn.Get<PaymentTypes>(sql, new { PaymentType = pType });
        }

    }
}
