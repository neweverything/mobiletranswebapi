﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using System.Web.Script.Serialization;

using IdeaBlade.Persistence;

using TaxiPassCommon;

using CabRideEngine;
using CabRideEngine.Utils;

using MobileTransWebAPI.Models;
using MobileTransWebAPI.Utils;

namespace MobileTransWebAPI.Controllers {

	public class PushMessageReadController : ApiController {

		private const string DRIVER_PUSH_TABLE_NAME = "DriverPushMessage";
		private const string DRIVER_PUSH_FIELD_NAME = "PushMessageID";

		public void PostPushMessageRead(PushMessageRead pRequest) {
			JavaScriptSerializer js = new JavaScriptSerializer();

			WebApiApplication.LogIt("PushMessageRead", js.Serialize(pRequest));

			PersistenceManager oPM = WebApiApplication.UserPM;
			if (oPM == null) {
				return;
			}

			PushMessageRead info = pRequest; // js.Deserialize<PushMessageRead>(pRequest.DecryptIceKey());

			DateTime requestDate = info.Key.ToDateTime();
            if (WebApiApplication.GetUTCServerTime().Subtract(requestDate).TotalMinutes > 1) {
				return;
			}

			Drivers driver = Drivers.GetDriver(oPM, info.DriverID);
			if (driver.IsNullEntity) {
				return;
			}


			PushMessage msg = PushMessage.GetMessage(oPM, info.PushMessageID);
			if (msg.OneTimeMessage) {
				EntityList<CabDriverRef> msgReadList = CabDriverRef.GetRecords(oPM, DRIVER_PUSH_TABLE_NAME, driver.DriverID, DRIVER_PUSH_FIELD_NAME);
				CabDriverRef rec = msgReadList.FirstOrDefault(p => p.ValueString.Equals(info.PushMessageID));
				if (rec == null) {
					rec = CabDriverRef.Create(oPM, DRIVER_PUSH_TABLE_NAME, driver.DriverID, DRIVER_PUSH_FIELD_NAME, info.PushMessageID.ToString());
					rec.Save();
				}
			}
		}
	}


}
