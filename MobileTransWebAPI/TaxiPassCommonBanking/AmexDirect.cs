﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Com.Americanexpress.Ips.Processor;
using Com.Americanexpress.Ips.Gcag.Properties;
using Com.Americanexpress.Ips.Messageformatter;

namespace TaxiPassCommon.Banking {
	public class AmexDirect : IProcessor {
		public ProcessorResponse PreAuthCard(CreditCard pCardData, decimal pAuthAmount, string pComments, string pVoucherNo, string pService) {
			AuthorizationProcessor objhttp = new AuthorizationProcessor();
			ResponseISOMessage responseISOMessage = new ResponseISOMessage();
			RequestISOMessage requestISOMessage = new RequestISOMessage();

			requestISOMessage = LoadData1100();

			return new ProcessorResponse(); 
		}

		public ProcessorResponse ChargeCard(CreditCard pCardData, decimal pChargeAmount, string pComments, string pVoucherNo, string pService, string pDriverID) {
			throw new NotImplementedException();
		}

		public ProcessorResponse ChargeAuth(string pRefNum, decimal pChargeAmount, bool pTestCard, string pDriverID) {
			throw new NotImplementedException();
		}

		public ProcessorResponse Refund(string pRefNum, decimal pRefundAmount, bool pTestCard) {
			throw new NotImplementedException();
		}

		public bool Void(string pRefNum, bool pTestCard) {
			throw new NotImplementedException();
		}

		public string GetTransactionHistory(DateTime pStart, DateTime pEnd) {
			throw new NotImplementedException();
		}

		private RequestISOMessage LoadData1100(CreditCard pCardData, decimal pAuthAmount) {
			RequestISOMessage oMessage = new RequestISOMessage();
			
			try {
				oMessage = new RequestISOMessage();
				oMessage.AuthorizationRequestProperties.MessageTypeIdentifier = Constants.MessageTypes.AUTH_MESSAGE_REQUEST; // ddlmsgTypId.SelectedValue; //"1100";
				oMessage.AuthorizationRequestProperties.PrimaryAccountNumber.Insert(0, pCardData.CardNo);  //"373953192351004"
				try {
					oMessage.AuthorizationRequestProperties.TransactionCurrencyCode = Constants.CurrencyCodes.USDollar;
					oMessage.AuthorizationRequestProperties.CardIndicator = Constants.cardIndicator.americanExpress;
					oMessage.AuthorizationRequestProperties.ProcessingCode = (Constants.ProcessingCodes)Enum.Parse(typeof(Constants.ProcessingCodes), ddlProcessingCodes.SelectedValue.ToString());// "004800";
					oMessage.AuthorizationRequestProperties.MerchantLocationCountryCode = (Constants.CountryCodes)Enum.Parse(typeof(Constants.CountryCodes), ddlCntrCd.SelectedValue.ToString());// "840";

				} catch (Exception) {
				}


				oMessage.AuthorizationRequestProperties.ValidateFlag = false;
				oMessage.AuthorizationRequestProperties.AmountTransaction = txtTranAmt.Text;
				oMessage.AuthorizationRequestProperties.SystemTraceAuditNumber = txtSystemTraceAN.Text;
				oMessage.AuthorizationRequestProperties.TransmissionDateAndTime = txttrandatetime.Text;
				oMessage.AuthorizationRequestProperties.CardExpirationDate = txtCardEpDt.Text;
				try {
					if (txtPOSCd.Text != string.Empty) {
						_cardDataInputCapabilityPOSDC = txtPOSCd.Text.ToString()[0].ToString();
						_codeCardHolderAuthenticationCapabilityPOSDC = txtPOSCd.Text.ToString()[1].ToString();
						_cardCaptureCapabilityPOSDC = txtPOSCd.Text.ToString()[2].ToString();
						_codeOperatingEnviornmentPOSDC = txtPOSCd.Text.ToString()[3].ToString();
						_cardHolderPresentPOSDC = txtPOSCd.Text.ToString()[4].ToString();
						_cardPresentPOSDC = txtPOSCd.Text.ToString()[5].ToString();
						_cardDataInputModePOSDC = txtPOSCd.Text.ToString()[6].ToString();
						_codeCardMemberAuthenticationPOSDC = txtPOSCd.Text.ToString()[7].ToString();
						_cardMemberAuthenticationEntityPOSDC = txtPOSCd.Text.ToString()[8].ToString();
						_cardDataOutputCapabilityPOSDC = txtPOSCd.Text.ToString()[9].ToString();
						_codeTerminalOutputCapabilityPOSDC = txtPOSCd.Text.ToString()[10].ToString();
						_codePINCaptureCapabilityPOSDC = txtPOSCd.Text.ToString()[11].ToString();
					} else {
						_cardDataInputCapabilityPOSDC = "-1";
						_codeCardHolderAuthenticationCapabilityPOSDC = "-1";
						_cardCaptureCapabilityPOSDC = "-1";
						_codeOperatingEnviornmentPOSDC = "-1";
						_cardHolderPresentPOSDC = "-1";
						_cardPresentPOSDC = "-1";
						_cardDataInputModePOSDC = "-1";
						_codeCardMemberAuthenticationPOSDC = "-1";
						_cardMemberAuthenticationEntityPOSDC = "-1";
						_cardDataOutputCapabilityPOSDC = "-1";
						_codeTerminalOutputCapabilityPOSDC = "-1";
						_codePINCaptureCapabilityPOSDC = "-1";
					}

					oMessage.AuthorizationRequestProperties.PosDataCode.CardDataInput = (Constants.CardDataInputCapability)Constants.GetCardDataInputCapability(_cardDataInputCapabilityPOSDC);
					oMessage.AuthorizationRequestProperties.PosDataCode.CardAuthentication = (Constants.CardholderAuthenticationCapability)Enum.Parse(typeof(Constants.CardholderAuthenticationCapability), _codeCardHolderAuthenticationCapabilityPOSDC);
					oMessage.AuthorizationRequestProperties.PosDataCode.CardCapture = (Constants.CardCaptureCapability)Enum.Parse(typeof(Constants.CardCaptureCapability), _cardCaptureCapabilityPOSDC);
					oMessage.AuthorizationRequestProperties.PosDataCode.OperatingEnvironment = (Constants.OperatingEnvironment)Constants.GetOperatingEnvironment(_codeOperatingEnviornmentPOSDC);
					oMessage.AuthorizationRequestProperties.PosDataCode.CardholderPresent = (Constants.CardholderPresent)Constants.GetCardholderPresent(_cardHolderPresentPOSDC);
					oMessage.AuthorizationRequestProperties.PosDataCode.CardPresent = (Constants.CardPresent)Constants.GetCardPresent(_cardPresentPOSDC);
					oMessage.AuthorizationRequestProperties.PosDataCode.CardDataInputMode = (Constants.CardDataInputMode)Constants.GetCardDataInputMode(_cardDataInputModePOSDC);
					oMessage.AuthorizationRequestProperties.PosDataCode.CardMemberIdentityMethod = (Constants.CardmemberAuthenticationMethod)Constants.GetCardmemberAuthenticationMethod(_codeCardMemberAuthenticationPOSDC);
					oMessage.AuthorizationRequestProperties.PosDataCode.CardMemberIdentityEntity = (Constants.CardmemberAuthenticationEntity)Enum.Parse(typeof(Constants.CardmemberAuthenticationEntity), _cardMemberAuthenticationEntityPOSDC);
					oMessage.AuthorizationRequestProperties.PosDataCode.CardDataOutput = (Constants.CardDataOutputCapability)Enum.Parse(typeof(Constants.CardDataOutputCapability), _cardDataOutputCapabilityPOSDC);
					oMessage.AuthorizationRequestProperties.PosDataCode.TerminalOutput = (Constants.Terminal_Output_Capability)Enum.Parse(typeof(Constants.Terminal_Output_Capability), _codeTerminalOutputCapabilityPOSDC);
					oMessage.AuthorizationRequestProperties.PosDataCode.PinCapture = (Constants.PINCaptureCapability)Constants.GetPINCaptureCapability(_codePINCaptureCapabilityPOSDC);

				} catch (Exception) {
					oMessage.AuthorizationRequestProperties.PosDataCode.CardDataInput = (Constants.CardDataInputCapability)Constants.GetCardDataInputCapability(_cardDataInputCapabilityPOSDC);
					oMessage.AuthorizationRequestProperties.PosDataCode.CardAuthentication = (Constants.CardholderAuthenticationCapability)Enum.Parse(typeof(Constants.CardholderAuthenticationCapability), _codeCardHolderAuthenticationCapabilityPOSDC);
					oMessage.AuthorizationRequestProperties.PosDataCode.CardCapture = (Constants.CardCaptureCapability)Enum.Parse(typeof(Constants.CardCaptureCapability), _cardCaptureCapabilityPOSDC);
					oMessage.AuthorizationRequestProperties.PosDataCode.OperatingEnvironment = (Constants.OperatingEnvironment)Constants.GetOperatingEnvironment(_codeOperatingEnviornmentPOSDC);
					oMessage.AuthorizationRequestProperties.PosDataCode.CardholderPresent = (Constants.CardholderPresent)Constants.GetCardholderPresent(_cardHolderPresentPOSDC);
					oMessage.AuthorizationRequestProperties.PosDataCode.CardPresent = (Constants.CardPresent)Constants.GetCardPresent(_cardPresentPOSDC);
					oMessage.AuthorizationRequestProperties.PosDataCode.CardDataInputMode = (Constants.CardDataInputMode)Constants.GetCardDataInputMode(_cardDataInputModePOSDC);
					oMessage.AuthorizationRequestProperties.PosDataCode.CardMemberIdentityMethod = (Constants.CardmemberAuthenticationMethod)Constants.GetCardmemberAuthenticationMethod(_codeCardMemberAuthenticationPOSDC);
					oMessage.AuthorizationRequestProperties.PosDataCode.CardMemberIdentityEntity = (Constants.CardmemberAuthenticationEntity)Enum.Parse(typeof(Constants.CardmemberAuthenticationEntity), _cardMemberAuthenticationEntityPOSDC);
					oMessage.AuthorizationRequestProperties.PosDataCode.CardDataOutput = (Constants.CardDataOutputCapability)Enum.Parse(typeof(Constants.CardDataOutputCapability), _cardDataOutputCapabilityPOSDC);
					oMessage.AuthorizationRequestProperties.PosDataCode.TerminalOutput = (Constants.Terminal_Output_Capability)Enum.Parse(typeof(Constants.Terminal_Output_Capability), _codeTerminalOutputCapabilityPOSDC);
					oMessage.AuthorizationRequestProperties.PosDataCode.PinCapture = (Constants.PINCaptureCapability)Constants.GetPINCaptureCapability(_codePINCaptureCapabilityPOSDC);
				}

				try {
					if (txtCardAcceptorLoc.Text != string.Empty) {
						string[] sep = new string[1];
						sep[0] = "\\";
						string[] str = txtCardAcceptorLoc.Text.Split(sep, StringSplitOptions.None);
						_nameCAD = str[0];
						_streetCAD = str[1];

						_phone = str[2];
						_email = str[3];
						_postalCodeCAD = str[4];

					} else {
						_nameCAD = null;
						_streetCAD = null;
						_cityCAD = null;
						_postalCodeCAD = null;
						_regionCAD = null;
						_countryCodeCAD = null;
						_phone = null;
						_email = null;
					}
					oMessage.AuthorizationRequestProperties.CardAcceptorNameLocation.Name = _nameCAD;
					oMessage.AuthorizationRequestProperties.CardAcceptorNameLocation.Street = _streetCAD;
					oMessage.AuthorizationRequestProperties.CardAcceptorNameLocation.Phone = _phone;
					oMessage.AuthorizationRequestProperties.CardAcceptorNameLocation.Email = _email;
					oMessage.AuthorizationRequestProperties.CardAcceptorNameLocation.PostalCode = _postalCodeCAD;
					oMessage.AuthorizationRequestProperties.CardAcceptorNameLocation.Region = _regionCAD;
					oMessage.AuthorizationRequestProperties.CardAcceptorNameLocation.CountryCode = _countryCodeCAD;
					oMessage.AuthorizationRequestProperties.CardAcceptorNameLocation.Phone = _phone;
					oMessage.AuthorizationRequestProperties.CardAcceptorNameLocation.Email = _email;
				} catch (Exception) {

				}
				try {
					if (txtVerificationInfo.Text != string.Empty) {
						_serviceIdentifierIV = txtVerificationInfo.Text.Substring(0, 2);
						_requestTypeIdentifierIV = txtVerificationInfo.Text.Substring(2, 2);
						_cardMemberBillingPostalCodeIV = txtVerificationInfo.Text.Substring(4, 9);
						_cardMemberBillingAddressIV = txtVerificationInfo.Text.Substring(13, 20);
						if (txtVerificationInfo.Text.Length > 34) {
							_cardMemberFirstNameIV = txtVerificationInfo.Text.Substring(33, 15);
							_cardMemberLastNameIV = txtVerificationInfo.Text.Substring(48, 30);
						}
						if (txtVerificationInfo.Text.Length > 79) {
							_cardMemberPhoneNumberIV = txtVerificationInfo.Text.Substring(78, 10);
							_shipToPostalCodeIV = txtVerificationInfo.Text.Substring(88, 9);
							_shipToPostalAddressIV = txtVerificationInfo.Text.Substring(97, 50);
							_shipToFirstNameIV = txtVerificationInfo.Text.Substring(147, 15);
							_shipToLastNameIV = txtVerificationInfo.Text.Substring(162, 30);
							_shipToPhoneNumberIV = txtVerificationInfo.Text.Substring(192, 10);
							_shipToCountryCodeIV = txtVerificationInfo.Text.Substring(202);
						}
					} else {
						_serviceIdentifierIV = null;
						_requestTypeIdentifierIV = null;
						_cardMemberBillingPostalCodeIV = null;
						_cardMemberBillingAddressIV = null;
						_cardMemberFirstNameIV = null;
						_cardMemberLastNameIV = null;
						_cardMemberPhoneNumberIV = null;
						_shipToPostalCodeIV = null;
						_shipToPostalAddressIV = null;
						_shipToFirstNameIV = null;
						_shipToLastNameIV = null;
						_shipToPhoneNumberIV = null;
						_shipToCountryCodeIV = null;
					}
					oMessage.AuthorizationRequestProperties.VerificationInformation.CmBillingAddress = _cardMemberBillingAddressIV;
					oMessage.AuthorizationRequestProperties.VerificationInformation.CmBillingPhoneNumber = _cardMemberPhoneNumberIV;
					oMessage.AuthorizationRequestProperties.VerificationInformation.CmBillingPostalCode = _cardMemberBillingPostalCodeIV;
					oMessage.AuthorizationRequestProperties.VerificationInformation.CmFirstName = _cardMemberFirstNameIV;
					oMessage.AuthorizationRequestProperties.VerificationInformation.CmLastName = _cardMemberLastNameIV;
					oMessage.AuthorizationRequestProperties.VerificationInformation.RequestTypeIdentifier = _requestTypeIdentifierIV;
					oMessage.AuthorizationRequestProperties.VerificationInformation.ServiceIdentifier = _serviceIdentifierIV;
					oMessage.AuthorizationRequestProperties.VerificationInformation.ShipToAddress = _shipToPostalAddressIV;
					oMessage.AuthorizationRequestProperties.VerificationInformation.ShipToCountryCode = _shipToCountryCodeIV;
					oMessage.AuthorizationRequestProperties.VerificationInformation.ShipToFirstName = _shipToFirstNameIV;
					oMessage.AuthorizationRequestProperties.VerificationInformation.ShipToLastName = _shipToLastNameIV;
					oMessage.AuthorizationRequestProperties.VerificationInformation.ShipToPhoneNumber = _shipToPhoneNumberIV;
					oMessage.AuthorizationRequestProperties.VerificationInformation.ShipToPostalCode = _shipToPostalCodeIV;
				} catch (Exception) {

					oMessage.AuthorizationRequestProperties.VerificationInformation.CmBillingAddress = _cardMemberBillingAddressIV;
					oMessage.AuthorizationRequestProperties.VerificationInformation.CmBillingPhoneNumber = _cardMemberPhoneNumberIV;
					oMessage.AuthorizationRequestProperties.VerificationInformation.CmBillingPostalCode = _cardMemberBillingPostalCodeIV;
					oMessage.AuthorizationRequestProperties.VerificationInformation.CmFirstName = _cardMemberFirstNameIV;
					oMessage.AuthorizationRequestProperties.VerificationInformation.CmLastName = _cardMemberLastNameIV;
					oMessage.AuthorizationRequestProperties.VerificationInformation.RequestTypeIdentifier = _requestTypeIdentifierIV;
					oMessage.AuthorizationRequestProperties.VerificationInformation.ServiceIdentifier = _serviceIdentifierIV;
					oMessage.AuthorizationRequestProperties.VerificationInformation.ShipToAddress = _shipToPostalAddressIV;
					oMessage.AuthorizationRequestProperties.VerificationInformation.ShipToCountryCode = _shipToCountryCodeIV;
					oMessage.AuthorizationRequestProperties.VerificationInformation.ShipToFirstName = _shipToFirstNameIV;
					oMessage.AuthorizationRequestProperties.VerificationInformation.ShipToLastName = _shipToLastNameIV;
					oMessage.AuthorizationRequestProperties.VerificationInformation.ShipToPhoneNumber = _shipToPhoneNumberIV;
					oMessage.AuthorizationRequestProperties.VerificationInformation.ShipToPostalCode = _shipToPostalCodeIV;
				}

				try {
					if (txtICCData.Text != string.Empty) {
						string[] strvalues = txtICCData.Text.Split(',');
						IccHeaderVersionName = strvalues[0];
						IccHeaderVersionNumber = strvalues[1];
						ICCApplicationCryptogram = strvalues[2];
						ICCIssuterApplicationData = strvalues[3];
						ICCUnpredicatableNumber = strvalues[4];
						ICCApplicationTransactionCounter = strvalues[5];
						ICCTerminalVerificationResult = strvalues[6];
						ICCTransactionDate = strvalues[7];
						ICCTransactionType = strvalues[8];
						ICCAmountAuthorized = strvalues[9];
						ICCTransactionCountryCode = strvalues[10];
						ICCTerminalCountryCode = strvalues[11];
						ICCApplicationstringerchangeProfile = strvalues[12];
						ICCAmountOthers = strvalues[13];
						ICCApplicationPanSequanceNumber = strvalues[14];
						ICCCryptogramInformationData = strvalues[15];
					} else {
						IccHeaderVersionName = null;
						IccHeaderVersionNumber = null;
						ICCApplicationCryptogram = null;
						ICCIssuterApplicationData = null;
						ICCUnpredicatableNumber = null;
						ICCApplicationTransactionCounter = null;
						ICCTerminalVerificationResult = null;
						ICCTransactionDate = null;
						ICCTransactionType = null;
						ICCAmountAuthorized = null;
						ICCTransactionCountryCode = null;
						ICCTerminalCountryCode = null;
						ICCApplicationstringerchangeProfile = null;
						ICCAmountOthers = null;
						ICCApplicationPanSequanceNumber = null;
						ICCCryptogramInformationData = null;
					}

					oMessage.AuthorizationRequestProperties.IccRelatedData.AmountAuthorized = this.ICCAmountAuthorized;
					oMessage.AuthorizationRequestProperties.IccRelatedData.AmountOther = this.ICCAmountOthers;
					oMessage.AuthorizationRequestProperties.IccRelatedData.AppCryptogram = this.ICCApplicationCryptogram;
					oMessage.AuthorizationRequestProperties.IccRelatedData.AppInterchangeProfile = this.ICCApplicationstringerchangeProfile;
					oMessage.AuthorizationRequestProperties.IccRelatedData.AppPanSequenceNumber = this.ICCApplicationPanSequanceNumber;
					oMessage.AuthorizationRequestProperties.IccRelatedData.AppTransactionCounter = this.ICCApplicationTransactionCounter;
					oMessage.AuthorizationRequestProperties.IccRelatedData.CryptogramInformationData = this.ICCCryptogramInformationData;
					oMessage.AuthorizationRequestProperties.IccRelatedData.IssuerAppData = this.ICCIssuterApplicationData;
					oMessage.AuthorizationRequestProperties.IccRelatedData.TerminalCountryCode = this.ICCTerminalCountryCode;
					oMessage.AuthorizationRequestProperties.IccRelatedData.TerminalVerificationResults = this.ICCTerminalVerificationResult;
					oMessage.AuthorizationRequestProperties.IccRelatedData.TransactionCurrencyCode = this.ICCTransactionCountryCode;
					oMessage.AuthorizationRequestProperties.IccRelatedData.TransactionDate = this.ICCTransactionDate;
					oMessage.AuthorizationRequestProperties.IccRelatedData.TransactionType = this.ICCTransactionType;
					oMessage.AuthorizationRequestProperties.IccRelatedData.UnpredictableNumber = this.ICCUnpredicatableNumber;
				} catch (Exception) {
					oMessage.AuthorizationRequestProperties.IccRelatedData.AmountAuthorized = this.ICCAmountAuthorized;
					oMessage.AuthorizationRequestProperties.IccRelatedData.AmountOther = this.ICCAmountOthers;
					oMessage.AuthorizationRequestProperties.IccRelatedData.AppCryptogram = this.ICCApplicationCryptogram;
					oMessage.AuthorizationRequestProperties.IccRelatedData.AppInterchangeProfile = this.ICCApplicationstringerchangeProfile;
					oMessage.AuthorizationRequestProperties.IccRelatedData.AppPanSequenceNumber = this.ICCApplicationPanSequanceNumber;
					oMessage.AuthorizationRequestProperties.IccRelatedData.AppTransactionCounter = this.ICCApplicationTransactionCounter;
					oMessage.AuthorizationRequestProperties.IccRelatedData.CryptogramInformationData = this.ICCCryptogramInformationData;
					oMessage.AuthorizationRequestProperties.IccRelatedData.IssuerAppData = this.ICCIssuterApplicationData;
					oMessage.AuthorizationRequestProperties.IccRelatedData.TerminalCountryCode = this.ICCTerminalCountryCode;
					oMessage.AuthorizationRequestProperties.IccRelatedData.TerminalVerificationResults = this.ICCTerminalVerificationResult;
					oMessage.AuthorizationRequestProperties.IccRelatedData.TransactionCurrencyCode = this.ICCTransactionCountryCode;
					oMessage.AuthorizationRequestProperties.IccRelatedData.TransactionDate = this.ICCTransactionDate;
					oMessage.AuthorizationRequestProperties.IccRelatedData.TransactionType = this.ICCTransactionType;
					oMessage.AuthorizationRequestProperties.IccRelatedData.UnpredictableNumber = this.ICCUnpredicatableNumber;
				}
				try {
					oMessage.AuthorizationRequestProperties.FunctionCode = (Constants.FunctionCodes)Enum.Parse(typeof(Constants.FunctionCodes), ddlFunctionCodes.SelectedValue.ToString()); //"181"; 
					oMessage.AuthorizationRequestProperties.MessageReasonCode = (Constants.messageReasonCode)Enum.Parse(typeof(Constants.messageReasonCode), ddlMessageReasonCode.SelectedValue.ToString()); //"1900";
					oMessage.AuthorizationRequestProperties.MessageReasonCode = (Constants.messageReasonCode)Enum.Parse(typeof(Constants.messageReasonCode), ddlMessageReasonCode.SelectedValue.ToString()); //"1900";
					oMessage.AuthorizationRequestProperties.CardAcceptorBusinessCode = (Constants.BusinessCode)Enum.Parse(typeof(Constants.BusinessCode), ddlBussCd.SelectedValue.ToString()); //txtBusscd.Text;// "4722";
				} catch (Exception) {
				}

				oMessage.AuthorizationRequestProperties.AprovalCodeLength = txtApprovalCodeLength.Text;
				oMessage.AuthorizationRequestProperties.RetrievalReferenceNumber = txtRetrivalReferenceNumber.Text;
				oMessage.AuthorizationRequestProperties.CardAcceptorTerminalId = txtTerminalId.Text;
				oMessage.AuthorizationRequestProperties.CardAcceptorIdCode = txtCardAccpetorId.Text;
				oMessage.AuthorizationRequestProperties.Track1Data = txtTrack1Data.Text;
				oMessage.AuthorizationRequestProperties.Track2Data = txtTrack2Data.Text;
				oMessage.AuthorizationRequestProperties.CardEffectiveDate = txtEffectiveDate.Text;
				oMessage.AuthorizationRequestProperties.ForwardingInstitutionIdCode = txtForwardingInstitutionIdentificationCode.Text;
				oMessage.AuthorizationRequestProperties.PinData = txtPersonalIdentificationNumberData.Text;
				oMessage.AuthorizationRequestProperties.AdditionalDataPrivate = txtPrivateAdditionalData.Text;
				oMessage.AuthorizationRequestProperties.LocalTransactionDateAndTime = txtLocalTrsDateTime.Text;
				if (txtAdditionaldata.Text == string.Empty) {
					oMessage.AuthorizationRequestProperties.AdditionalDataNational = null;
					oMessage.AuthorizationRequestProperties.AdditionalDataNationalStr = txtAdditionaldata.Text;
				} else if (IsFromPopup) {
					switch (txtAdditionaldata.Text.Substring(2, 3)) {
						case "ITD":
							oMessage.AuthorizationRequestProperties.AdditionalDataNational.SecondaryId = secondaryId.PadRight(3);
							oMessage.AuthorizationRequestProperties.AdditionalDataNational.CustomerEmailIdVli = customer_email_id_vli;
							oMessage.AuthorizationRequestProperties.AdditionalDataNational.CustomerEmail = customer_email;
							oMessage.AuthorizationRequestProperties.AdditionalDataNational.CustomerHostnameIdVli = customer_hostname_id_vli;
							oMessage.AuthorizationRequestProperties.AdditionalDataNational.CustomerHostName = customer_hostname;
							oMessage.AuthorizationRequestProperties.AdditionalDataNational.HttpBrowserTypeIdVli = http_browsertype_id_vli;
							oMessage.AuthorizationRequestProperties.AdditionalDataNational.HttpBrowserType = http_browsertype;
							oMessage.AuthorizationRequestProperties.AdditionalDataNational.ShipToCountry = ship_to_country;
							oMessage.AuthorizationRequestProperties.AdditionalDataNational.ShipToCountryIdVli = ship_to_country_id_vli;
							oMessage.AuthorizationRequestProperties.AdditionalDataNational.ShippingMethod = shipping_method;
							oMessage.AuthorizationRequestProperties.AdditionalDataNational.ShippingMethodIdVli = shipping_method_id_vli;
							oMessage.AuthorizationRequestProperties.AdditionalDataNational.MerchantProductSku = merchant_productsku;
							oMessage.AuthorizationRequestProperties.AdditionalDataNational.MerchantProductskuIdVli = merchant_productsku_id_vli;
							oMessage.AuthorizationRequestProperties.AdditionalDataNational.CustomerIp = customer_ip;
							oMessage.AuthorizationRequestProperties.AdditionalDataNational.CustomerAni = customer_ani;
							oMessage.AuthorizationRequestProperties.AdditionalDataNational.CustomerInfoIdentifier = customer_info_identifier;
							break;
						case "IAC":
							oMessage.AuthorizationRequestProperties.AdditionalDataNational.SecondaryId = secondaryId.PadRight(3);
							oMessage.AuthorizationRequestProperties.AdditionalDataNational.IacApdDepartureDate = iac_apd_departure_date;
							oMessage.AuthorizationRequestProperties.AdditionalDataNational.IacApdPassengerNameVli = iac_apd_passenger_name_vli;
							oMessage.AuthorizationRequestProperties.AdditionalDataNational.IacApdPassengerName = iac_apd_passenger_name;
							oMessage.AuthorizationRequestProperties.AdditionalDataNational.IacApdOrigin = iac_apd_origin;
							oMessage.AuthorizationRequestProperties.AdditionalDataNational.IacApdDest = iac_apd_dest;
							oMessage.AuthorizationRequestProperties.AdditionalDataNational.IacApdRoutingVli = iac_apd_routing_vli;
							oMessage.AuthorizationRequestProperties.AdditionalDataNational.IacApdNumberOfCities = iac_apd_number_of_cities;
							oMessage.AuthorizationRequestProperties.AdditionalDataNational.IacApdRoutingCities = iac_apd_routing_cities;
							oMessage.AuthorizationRequestProperties.AdditionalDataNational.IacApdCarriersVli = iac_apd_carriers_vli;
							oMessage.AuthorizationRequestProperties.AdditionalDataNational.IacApdNumberOfCarriers = iac_apd_number_of_carriers;
							oMessage.AuthorizationRequestProperties.AdditionalDataNational.IacApdCarriers = iac_apd_carriers;
							oMessage.AuthorizationRequestProperties.AdditionalDataNational.IacApdFareBasis = iac_apd_fare_basis;
							oMessage.AuthorizationRequestProperties.AdditionalDataNational.IacApdNumberOfPassengers = iac_apd_number_of_passengers;
							oMessage.AuthorizationRequestProperties.AdditionalDataNational.CustomerIp = customer_ip;
							oMessage.AuthorizationRequestProperties.AdditionalDataNational.CustomerEmailIdVli = customer_email_id_vli;
							oMessage.AuthorizationRequestProperties.AdditionalDataNational.CustomerEmail = customer_email;
							break;

						case "APD":
							oMessage.AuthorizationRequestProperties.AdditionalDataNational.SecondaryId = secondaryId.PadRight(3);
							oMessage.AuthorizationRequestProperties.AdditionalDataNational.IacApdDepartureDate = iac_apd_departure_date;
							oMessage.AuthorizationRequestProperties.AdditionalDataNational.IacApdPassengerNameVli = iac_apd_passenger_name_vli;
							oMessage.AuthorizationRequestProperties.AdditionalDataNational.IacApdPassengerName = iac_apd_passenger_name;
							oMessage.AuthorizationRequestProperties.AdditionalDataNational.ApdCardMemberNameVli = apd_cardmember_name_vli;
							oMessage.AuthorizationRequestProperties.AdditionalDataNational.ApdCardMemberName = apd_cardmember_name;
							oMessage.AuthorizationRequestProperties.AdditionalDataNational.IacApdOrigin = iac_apd_origin;
							oMessage.AuthorizationRequestProperties.AdditionalDataNational.IacApdDest = iac_apd_dest;
							oMessage.AuthorizationRequestProperties.AdditionalDataNational.IacApdRoutingVli = iac_apd_routing_vli;
							oMessage.AuthorizationRequestProperties.AdditionalDataNational.IacApdNumberOfCities = iac_apd_number_of_cities;
							oMessage.AuthorizationRequestProperties.AdditionalDataNational.IacApdRoutingCities = iac_apd_routing_cities;
							oMessage.AuthorizationRequestProperties.AdditionalDataNational.IacApdCarriersVli = iac_apd_carriers_vli;
							oMessage.AuthorizationRequestProperties.AdditionalDataNational.IacApdCarriers = iac_apd_carriers;
							oMessage.AuthorizationRequestProperties.AdditionalDataNational.IacApdNumberOfCarriers = iac_apd_number_of_carriers;
							oMessage.AuthorizationRequestProperties.AdditionalDataNational.IacApdFareBasis = iac_apd_fare_basis;
							oMessage.AuthorizationRequestProperties.AdditionalDataNational.IacApdNumberOfPassengers = iac_apd_number_of_passengers;
							oMessage.AuthorizationRequestProperties.AdditionalDataNational.ApdeTicketIndicator = apd_eticket_indicator;
							oMessage.AuthorizationRequestProperties.AdditionalDataNational.ApdReservationCodeVli = apd_reservation_code_vli;
							oMessage.AuthorizationRequestProperties.AdditionalDataNational.ApdReservationCode = apd_reservation_code;
							break;
					}

				} else {
					oMessage.AuthorizationRequestProperties.AdditionalDataNationalStr = txtAdditionaldata.Text;
				}
				oMessage.AuthorizationRequestProperties.CardIdentifierCode = txtSecurityRelatedControlInformation.Text;
				oMessage.AuthorizationRequestProperties.CardSettlementDate = txtSettlementDate.Text;
				RequestNationalUseData objNUD = new RequestNationalUseData();

				try {
					if (txtNationalUseData_2.Text != string.Empty) {
						string[] strvalues = txtNationalUseData_2.Text.Split(',');
						PRIMARY_ID = strvalues[0];
						Secondary_ID = strvalues[1];
						Electronic_Commerce_Indicator = strvalues[2];
						American_Express_Verification = strvalues[3];
						American_Express_Verification_Value = strvalues[4];
						American_Express_SafeKey_Transaction_ID = strvalues[5];
						American_Express_SafeKey_Transaction_ID_Value = strvalues[6]; ;
						//}
					} else {
						PRIMARY_ID = null;
						Secondary_ID = null;
						Electronic_Commerce_Indicator = "0";
						American_Express_Verification = null;
						American_Express_Verification_Value = null;
						American_Express_SafeKey_Transaction_ID = null;
						American_Express_SafeKey_Transaction_ID_Value = null;
					}

					objNUD.PrimaryId = PRIMARY_ID;
					objNUD.SecondaryId = Secondary_ID;
					objNUD.ElectronicCommerceIndicator = (Constants.electronicCommerceIndicator)Enum.Parse(typeof(Constants.electronicCommerceIndicator), Electronic_Commerce_Indicator); //"181"; ;
					objNUD.AmexVerificationValueId = American_Express_Verification;
					objNUD.AmexVerificationValue = American_Express_Verification_Value;
					objNUD.SafekeyTransactionIdValue = American_Express_SafeKey_Transaction_ID_Value;
					objNUD.SafekeyTransactionId = American_Express_SafeKey_Transaction_ID;
					oMessage.AuthorizationRequestProperties.NationalUseData = objNUD;
				} catch (Exception) {
					objNUD.PrimaryId = PRIMARY_ID;
					objNUD.SecondaryId = Secondary_ID;
					objNUD.ElectronicCommerceIndicator = (Constants.electronicCommerceIndicator)Enum.Parse(typeof(Constants.electronicCommerceIndicator), Electronic_Commerce_Indicator); //"181"; ;
					objNUD.AmexVerificationValueId = American_Express_Verification;
					objNUD.AmexVerificationValue = American_Express_Verification_Value;
					objNUD.SafekeyTransactionIdValue = American_Express_SafeKey_Transaction_ID_Value;
					objNUD.SafekeyTransactionId = American_Express_SafeKey_Transaction_ID;
					oMessage.AuthorizationRequestProperties.NationalUseData = objNUD;
				}


				oMessage.AuthorizationRequestProperties.ValidationInformation = txtvalidationinfo.Text;
				oMessage.AuthorizationRequestProperties.AcquiringInstitutionIdCode = txtAcqInsIdentificationCode.Text;

			} catch (Exception) {
			} finally {
			}
			return oMessage;
		}
	}
}
