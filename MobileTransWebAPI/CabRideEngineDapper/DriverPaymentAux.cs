﻿using CabRideEngineDapper.Utils;
using Dapper;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;


namespace CabRideEngineDapper {
    public partial class DriverPaymentAux {

        public static DriverPaymentAux Create(DriverPayments pDriverPayment) {
            DriverPaymentAux oDriverPaymentAux = null;
            try {
                string sql = "usp_DriverPaymentAuxInsert"; // DriverPaymentID = @DriverPaymentID, ModifiedBy = @ModifiedBy";

                using (var conn = SqlHelper.OpenSqlConnection()) {
                    oDriverPaymentAux = conn.Query<DriverPaymentAux>(sql, new { DriverPaymentID = pDriverPayment.DriverPaymentID, ModifiedBy = pDriverPayment.ModifiedBy }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
                if (oDriverPaymentAux.RowState == DataRowState.Detached) {
                    oDriverPaymentAux.RowState = DataRowState.Unchanged;
                }
                pDriverPayment.DriverPaymentAux = oDriverPaymentAux;
            } catch (Exception ex) {
                throw ex;
            }
            return oDriverPaymentAux;
        }

        public static DriverPaymentAux GetOrCreate(DriverPayments pPayment) {
            using (var conn = SqlHelper.OpenSqlConnection()) {
                DriverPaymentAux rec = GetRecord(conn, pPayment.DriverPaymentID);
                if (rec.IsNullEntity) {
                    rec = Create(pPayment);
                }
                pPayment.DriverPaymentAux = rec;
                return rec;
            }
        }

        public static DriverPaymentAux GetOrCreate(long pDriverPaymentID) {
            using (var conn = SqlHelper.OpenSqlConnection()) {
                DriverPaymentAux rec = GetRecord(conn, pDriverPaymentID);
                if (rec.IsNullEntity) {
                    rec = DriverPaymentAuxTable.Create();
                    rec.DriverPaymentID = pDriverPaymentID;
                }
                return rec;
            }
        }


        public static DriverPaymentAux GetRecord(SqlConnection pConn, long pDriverPaymentID) {
            string sql = @"SELECT  * FROM DriverPaymentAux
							WHERE DriverPaymentID = @DriverPaymentID";

            return pConn.Get<DriverPaymentAux>(sql, new { DriverPaymentID = pDriverPaymentID });
        }

        public static string GetRefundInfo(SqlConnection pConn, long pChargeBackPaymentID) {
            string sql = @"SELECT  'Source Voucher: ' + ChargeBackSource.TransNo AS RefundInfo
							FROM    DriverPaymentAux
									INNER JOIN DriverPayments AS ChargeBackSource ON DriverPaymentAux.ChargeBackDriverPaymentID = ChargeBackSource.DriverPaymentID
							WHERE   DriverPaymentAux.DriverPaymentID = @DriverPaymentID";

            return pConn.Get<string>(sql, new { DriverPaymentID = pChargeBackPaymentID });
        }

        public static DriverPaymentAux GetRecordByVTSRowID(SqlConnection pConn, string pID) {
            string sql = @"SELECT  * FROM DriverPaymentAux
							WHERE VTSRowID = @VTSRowID";

            return pConn.Get<DriverPaymentAux>(sql, new { VTSRowID = pID });
        }

    }
}
