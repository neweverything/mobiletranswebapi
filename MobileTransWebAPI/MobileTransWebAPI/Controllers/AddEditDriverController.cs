﻿using CabRideEngineDapper;
using CabRideEngineDapper.Utils;
using MobileTransWebAPI.Models;
using Newtonsoft.Json;
using System;
using System.Drawing;
using System.IO;
using System.Web.Http;
using TaxiPassCommon;

namespace MobileTransWebAPI.Controllers {
	public class AddEditDriverController : ApiController {
		// GET api/addeditdriver
		//public IEnumerable<string> Get()
		//{
		//    return new string[] { "value1", "value2" };
		//}

		//// GET api/addeditdriver/5
		//public string Get(int id)
		//{
		//    return "value";
		//}

		public DriverProfileResult Get() {
			DriverProfile driver = new DriverProfile();
			//driver.AffiliateID = 1993;
			//driver.DriverName = "Adam";
			//driver.DriverLastName = "Potash";
			//driver.PhoneNo = "2597673936";
			//driver.City = "Floral Park";
			//driver.State = "NY";
			//driver.VehicleNo = "6887";
			//driver.PIN = "3936";
			////driver.VoidedCheck = new ] => 
			//driver.BankRouting = "026013673";
			//driver.BankAcctNo = "4264378055";
			//driver.BankHolder = "Adam Adam";
			//driver.Email = "mindseyestarter @gmail.com";
			//driver.RequestKey = DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss");


			string msg = File.ReadAllText(@"c:\temp\test.json");
			driver = JsonConvert.DeserializeObject<DriverProfile>(msg);

			return Post(driver);

		}



		// POST api/addeditdriver
		public DriverProfileResult Post([FromBody]DriverProfile pProfile) {
			DriverProfileResult result = new DriverProfileResult();

			DateTime requestDate = DateTime.Today.AddDays(-1);
			if (pProfile != null) {
				requestDate = pProfile.RequestKey.DecryptIceKey().ToDateTime();
			}

			if (WebApiApplication.GetUTCServerTime().Subtract(requestDate).TotalMinutes > 1) {
				result.ErrorMsg = "Invalid Access";
				return result;
			}

			using (var conn = SqlHelper.OpenSqlConnection()) {


				Affiliate affRec = Affiliate.GetAffiliate(conn, pProfile.AffiliateID);
				//always allow Carmel Car Service drivers to auto signup, 
				if (affRec.AffiliateDriverDefaults.IVRSignUp || pProfile.AffiliateID == 161) {
				} else {
					result.ErrorMsg = "Invalid Affiliate Access";
					return result;
				}




				//driver wants to change their number, ensure no other driver has new number
				if (pProfile.PhoneNo.Length != 10) {
					result.ErrorMsg = string.Format("Phone number: {0} must be 10 digits long", pProfile.PhoneNo);
					return result;
				}
				AffiliateDrivers AffDriverNewNumber = AffiliateDrivers.GetDriverByCallInCell(conn, pProfile.PhoneNo);
				if (!AffDriverNewNumber.IsNullEntity && ((AffDriverNewNumber.AffiliateDriverID != pProfile.AffiliateDriverID && pProfile.AffiliateDriverID.GetValueOrDefault(0) > 0) || pProfile.AffiliateDriverID.GetValueOrDefault(0) == 0)) {
					result.ErrorMsg = string.Format("Phone number: {0} is assigned to another driver", pProfile.PhoneNo);
					return result;
				}

				AffiliateDrivers driver = AffiliateDrivers.Create(affRec);
				if (pProfile.AffiliateDriverID.GetValueOrDefault(0) > 0) {
					driver = AffiliateDrivers.GetDriver(conn, pProfile.AffiliateDriverID.Value);
				}


				if (driver.Cell != pProfile.PhoneNo) {
					driver.Cell = pProfile.PhoneNo;
					driver.CallInCell = pProfile.PhoneNo;
				}

				driver.Name = pProfile.DriverName;
				driver.LastName = pProfile.DriverLastName;
				if (!pProfile.City.IsNullOrEmpty()) {
					driver.City = pProfile.City;
				}
				if (!pProfile.State.IsNullOrEmpty()) {
					driver.State = pProfile.State;
				}
				if (!pProfile.PIN.IsNullOrEmpty()) {
					driver.PIN = pProfile.PIN;
				}

				if (!pProfile.VoidedCheck.IsNullOrEmpty()) {

					if (!affRec.AffiliateDriverDefaults.DriverCanSubmitCheck) {
						result.ErrorMsg = string.Format("Check cannot be submitted per fleet: {0}", affRec.Name);
						return result;
					}

					string sFullPath = Properties.Settings.Default.VoidedCheckPath;

					try {
						if (!Directory.Exists(sFullPath)) {
							Directory.CreateDirectory(sFullPath);
						}
						string storePath = DateTime.Today.ToString(@"yyyy\\MM\\dd");
						sFullPath = Path.Combine(sFullPath, storePath);
						if (!Directory.Exists(sFullPath)) {
							Directory.CreateDirectory(sFullPath);
						}
						string fileName = "";
						string sFile = "";
						while (true) {
							fileName = string.Format("{0}.png", StringUtils.GetUniqueKey(8));
							sFile = Path.Combine(sFullPath, fileName);
							if (!File.Exists(sFile)) {
								// File.Create(sFile);
								break;
							}
						}

						byte[] rawImage = Convert.FromBase64String(pProfile.VoidedCheck);
						Image img = VoidedCheckOrDriversLicenseController.byteArrayToImage(rawImage);
						img.Save(sFile);

						if (!pProfile.BankAcctNo.IsNullOrEmpty()) {
							//BankUtils.FedBankFile = Path.Combine(Properties.Settings.Default.TemplatePath, "FedACHdir.txt");
							FedACHDir info = FedACHDir.GetByRoutingNo(pProfile.BankRouting);
							if (info.Name.IsNullOrEmpty()) {
								result.ErrorMsg = "Bank Routing number not valid";
								if (!Properties.Settings.Default.SaveBankInfoOnRoutingNumberError) {
									return result;
								}
							} else {
								driver.BankName = info.Name;
								driver.BankAddress = info.Address;
								driver.BankCity = info.City;
								driver.BankState = info.State;
								driver.BankZIPCode = info.ZipCode;
							}
							driver.BankAccountHolder = pProfile.BankHolder;
							driver.BankAccountNumber = pProfile.BankAcctNo;
							driver.BankCode = pProfile.BankRouting;

							if (!driver.MerchantID.HasValue) {
								driver.MerchantID = Convert.ToInt32(SystemDefaultsDict.GetByKey(conn, "Bank Payment", "Pay From Account", false).ValueString);
							}

							driver.AutoPayManual = true;
							driver.AutoPaySwipe = true;
							driver.PaymentMethod = (int)PaymentMethod.DirectDeposit;
							driver.BankAccountPersonal = true;
						}

						driver.Save();

						DateTime start = DateTime.Today.AddDays(-30);
						//List<DriverPayments> payList = DriverPayments.GetPayments(affDriver, start, DateTime.Now, DriverPaymentStatus.unpaid);
						//foreach (DriverPayments rec in payList) {
						//    rec.DriverPaid = true;
						//}

						//oPM.SaveChanges(payList);

					} catch (Exception ex) {
						result.ErrorMsg = ex.Message;
					}
				} else {
					driver.Save();
				}

				result.DriverName = driver.Name;
				//result.FleetName = driver.Fleets.Fleet;
				result.City = driver.City;

				if (driver.Country.IsNullOrEmpty()) {
					driver.Country = "USA";
				}
				result.State = States.GetByStateCode(conn, driver.State).StateName;
				//result.VehicleNo = driver.VehicleNo;
				result.PIN = driver.PIN;

				result.AffiliateID = driver.AffiliateID;
				result.AffiliateDriverID = driver.AffiliateDriverID;
				result.SSN = driver.SSN;

			}

			result.Ok = result.ErrorMsg.IsNullOrEmpty();

			return result;

		}

	}
}
