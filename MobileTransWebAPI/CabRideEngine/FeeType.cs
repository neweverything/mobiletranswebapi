﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

using Csla.Validation;

namespace CabRideEngine {
    /// <summary>
    /// The developer class representing the FeeType business object class.
    /// </summary>
    /// <remarks>
    /// <para>Place all application-specific business logic in this class.</para>
    /// <para>
    /// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
    /// and will not overwrite changes.
    /// </para>
    /// <para>
    /// This class inherits from the generated class, its companion in the pair of classes 
    /// defining a business object. Properties in the generated class may be overridden in 
    /// order to change the behavior or add business logic.
    /// </para>
    /// <para>
    /// This class is the final class in the inheritance chain for this business object. 
    /// It should not be inherited.
    /// </para>
    /// </remarks>
    [Serializable]
    public sealed partial class FeeType : FeeTypeDataRow {

        #region Constructors -- DO NOT MODIFY

        // Do not create constructors for this class
        // Developers cannot create instances of this class with the "new" keyword
        // You should write a static Create method instead; see the "Suggested Customization" region
        // Please review the documentation to learn about entity creation and inheritance

        // This private constructor prevents accidental instance creation via 'new';
        // it is also required if the class is visible from a web service.
        // DO NOT REMOVE **
        private FeeType() : this(null) { }

        /// <summary>
        /// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
        /// </summary>
        /// <param name="pRowBuilder"></param>
        public FeeType(DataRowBuilder pRowBuilder)
            : base(pRowBuilder) {
        }

        #endregion

        #region Suggested Customizations

        //    // Use this factory method template to create new instances of this class
        //    public static FeeType Create(PersistenceManager pManager,
        //      ... your creation parameters here ...) { 
        //
        //      FeeType aFeeType = pManager.CreateEntity<FeeType>();
        //
        //      // if this object type requires a unique id and you have implemented
        //      // the IIdGenerator interface implement the following line
        //      pManager.GenerateId(aFeeType, // add id column here //);
        //
        //      // Add custom code here
        //
        //      aFeeType.AddToManager();
        //      return aFeeType;
        //    }

        //    // Implement this method if you want your object to sort in a specific order
        //    public override int CompareTo(Object pObject) {
        //    }

        //    // Implement this method to customize the null object version of this class
        //    protected override void UpdateNullEntity() {
        //    }

        #endregion

        // Add additional logic to your business object here...
        public static FeeType Create(PersistenceManager pPM) {
            FeeType rec = null;
            try {
                rec = pPM.CreateEntity<FeeType>();

                // Using the IdeaBlade Id Generation technique
                //pPM.GenerateId(rec, FeeType.FeeTypeIDEntityColumn);

                rec.AddToManager();

            } catch (Exception ex) {
                throw ex;
            }
            return rec;
        }

        protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
            if (IsDeserializing)
                return;
            base.OnColumnChanging(pArgs);
            //DoAudit(pArgs);
        }

        private void DoAudit(DataColumnChangeEventArgs pArgs) {
            if (!this.PersistenceManager.IsClient) {
                return;
            }
            if (this.RowState == DataRowState.Added) {
                return;
            }
            if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
                AuditRecord = FeeTypeAudit.Create(this.PersistenceManager, this);
            }
            //AuditRecord[pArgs.Column.ColumnName] = pArgs.ProposedValue;
        }

        protected override void AddRules(RuleList pList) {
            PropertyRequiredRule.AddToList(pList, DescriptionEntityColumn.ColumnName);
            pList.Add(IsDescriptionUnique, DescriptionEntityColumn.ColumnName);

            base.AddRules(pList);
        }


        private bool IsDescriptionUnique(object pTarget, RuleArgs e) {
            FeeType rec = pTarget as FeeType;

            RdbQuery qry = new RdbQuery(typeof(FeeType), FeeType.DescriptionEntityColumn, EntityQueryOp.EQ, rec.Description);
            qry.AddClause(FeeType.FeeTypeIDEntityColumn, EntityQueryOp.NE, rec.FeeTypeID);
            EntityList<FeeType> recList = this.PersistenceManager.GetEntities<FeeType>(qry, QueryStrategy.DataSourceThenCache);
            if (recList.Count > 0) {
                e.Description = "Fee Type Description must be unique!";
                return false;
            }
            return true;
        }

        public override bool Default {
            get {
                return base.Default;
            }
            set {
                base.Default = value;
                if (value) {
                    EntityList<FeeType> recList = this.PersistenceManager.GetEntities<FeeType>();
                    foreach (FeeType rec in recList) {
                        if (rec.FeeTypeID != this.FeeTypeID) {
                            rec.Default = false;
                        }
                    }
                }
            }
        }

        public static EntityList<FeeType> GetAll(PersistenceManager pPM, QueryStrategy pQueryStrategy) {
            RdbQuery qry = new RdbQuery(typeof(FeeType));
            qry.AddOrderBy(FeeType.DescriptionEntityColumn);
            return pPM.GetEntities<FeeType>(qry, pQueryStrategy);
        }

        public static FeeType GetDefault(PersistenceManager pPM) {
            RdbQuery qry = new RdbQuery(typeof(FeeType), FeeType.DefaultEntityColumn, EntityQueryOp.EQ, true);
            return pPM.GetEntity<FeeType>(qry);
        }

        public static FeeType GetFeeType(PersistenceManager pPM, string pFeeType) {
            RdbQuery qry = new RdbQuery(typeof(FeeType), FeeType.DescriptionEntityColumn, EntityQueryOp.EQ, pFeeType);
            return pPM.GetEntity<FeeType>(qry);
        }

        public static FeeType GetFeeType(PersistenceManager pPM, long pFeeTypeID) {
            RdbQuery qry = new RdbQuery(typeof(FeeType), FeeType.FeeTypeIDEntityColumn, EntityQueryOp.EQ, pFeeTypeID);
            return pPM.GetEntity<FeeType>(qry);
        }
    }

    #region EntityPropertyDescriptors.FeeTypePropertyDescriptor
    ////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
    ////    For example: 
    ////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
    //public static partial class EntityPropertyDescriptors {
    //	public partial class FeeTypePropertyDescriptor : BaseEntityPropertyDescriptor {
    //    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
    //    public AdaptedPropertyDescriptor Age {
    //      get { return this.Get("Age"); }
    //    }
    //  }
    //} 
    #endregion

}