using System;
using System.Collections.Generic;
using System.Text;

using IdeaBlade.Util;

namespace CabRideEngine {
    public class CellPhoneDataEntry {

        private string mCode;
        private string mDescription;

        private bool mIsNullEntity = false;

        private static BindableList<CellPhoneDataEntry> msEntities;
        private static CellPhoneDataEntry msNullEntity;


        static CellPhoneDataEntry() {

            // Create the null entity
            msNullEntity = new CellPhoneDataEntry("", "");
            msNullEntity.mIsNullEntity = true;

            // Populate list of Routing Types.
            msEntities = new BindableList<CellPhoneDataEntry>();
            msEntities.Add(new CellPhoneDataEntry(Convert.ToInt16(DataEntryTypeEnum.SwipeAndManual).ToString(), "Swipe Card and Manual Entry"));
            msEntities.Add(new CellPhoneDataEntry(Convert.ToInt16(DataEntryTypeEnum.ManualEntry).ToString(), "Manual Entry"));
            msEntities.Add(new CellPhoneDataEntry(Convert.ToInt16(DataEntryTypeEnum.SwipeCard).ToString(), "Swipe Card"));
        }

        private CellPhoneDataEntry(string pCode, string pDescription) {
            mCode = pCode;
            mDescription = pDescription;
        }

        /// <summary>Get the CellPhoneDataEntry null entity.</summary>
        public static CellPhoneDataEntry NullEntity {
            get {
                return msNullEntity;
            }
        }

        /// <summary>Get the list of all CellPhoneDataEntry.</summary>
        public static BindableList<CellPhoneDataEntry> GetAll() {
            return msEntities;
        }

        /// <summary>Get the CellPhoneDataEntry by Code.</summary>
        public static CellPhoneDataEntry GetById(string pCode) {
            foreach (CellPhoneDataEntry oRec in msEntities) {
                if (oRec.Code == pCode) {
                    return oRec;
                }
            }
            return NullEntity;
        }

        /// <summary>Return true if this is the null entity</summary>
        [BindingBrowsable(false)]
        public bool IsNullEntity {
            get {
                return mIsNullEntity;
            }
        }

        /// <summary>Get the Code</summary>
        public string Code {
            get {
                return mCode;
            }
        }

        public string Description {
            get {
                return mDescription;
            }
        }


    }
}
