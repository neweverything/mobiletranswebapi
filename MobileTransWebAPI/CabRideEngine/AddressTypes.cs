﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

using Csla.Validation;

namespace CabRideEngine {
    [Serializable]
    public sealed class AddressTypes : AddressTypesDataRow {


        #region Constructors -- DO NOT MODIFY
        // Do not create constructors for this class
        // Developers cannot create instances of this class with the "new" keyword
        // You should write a static Create method instead; see the "Suggested Customization" region
        // Please review the documentation to learn about entity creation and inheritance

        // This private constructor prevents accidental instance creation via 'new'
        private AddressTypes()
            : this(null) {
        }

        // Typed DataSet constructor (needed by framework) - DO NOT REMOVE
        public AddressTypes(DataRowBuilder pRowBuilder)
            : base(pRowBuilder) {
        }

        #endregion

        #region Suggested Customizations

        //    // Use this factory method template to create new instances of this class
        //    public static AddressTypes Create(PersistenceManager pManager,
        //      ... your creation parameters here ...) { 
        //
        //      AddressTypes aAddressTypes = pManager.CreateEntity<AddressTypes>();
        //
        //      // if this object type requires a unique id and you have implemented
        //      // the IIdGenerator interface implement the following line
        //      pManager.GenerateId(aAddressTypes, // add id column here //);
        //
        //      // Add custom code here
        //
        //      aAddressTypes.AddToManager();
        //      return aAddressTypes;
        //    }

        //    // Implement this method if you want your object to sort in a specific order
        //    public override int CompareTo(Object pObject) {
        //    }

        //    // Implement this method to customize the null object version of this class
        //    protected override void UpdateNullEntity() {
        //    }

        #endregion

        // Add additional logic to your business object here...
        public static AddressTypes Create(PersistenceManager pManager) {

            AddressTypes oAddress = (AddressTypes)pManager.CreateEntity(typeof(AddressTypes));

            //pManager.GenerateId(oAddress, CabRideEngine.AddressTypes.AddressTypeIDEntityColumn);
            oAddress.AddToManager();
            return oAddress;
        }

        protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
            if (IsDeserializing)
                return;
            base.OnColumnChanging(pArgs);
            //DoAudit(pArgs);
        }

        private void DoAudit(DataColumnChangeEventArgs pArgs) {
            if (!this.PersistenceManager.IsClient) {
                return;
            }
            if (this.RowState == DataRowState.Added) {
                return;
            }
            if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
                AuditRecord = AddressTypesAudit.Create(this.PersistenceManager, this);
            }
            //AuditRecord[pArgs.Column.ColumnName] = pArgs.ProposedValue;
        }

        public override string AddressType {
            get {
                return base.AddressType;
            }
            set {
                base.AddressType = value;
            }
        }

        protected override void AddRules(RuleList pList) {
            PropertyRequiredRule.AddToList(pList, AddressTypeEntityColumn.ColumnName);
            pList.Add(IsAddressTypeUnique, AddressTypeEntityColumn.ColumnName);

            //base.AddRules(pList);
        }

        private bool IsAddressTypeUnique(object pTarget, RuleArgs e) {
            AddressTypes oType = (AddressTypes)pTarget;

            RdbQuery oQry = new RdbQuery(typeof(AddressTypes), AddressTypes.AddressTypeEntityColumn, EntityQueryOp.EQ, oType.AddressType);
            oQry.AddClause(AddressTypes.AddressTypeIDEntityColumn, EntityQueryOp.NE, oType.AddressTypeID);
            EntityList<AddressTypes> oAddressTypes = this.PersistenceManager.GetEntities<AddressTypes>(oQry);
            if (oAddressTypes.Count > 0) {
                e.Description = "Address Type must be unique!";
                return false;
            }
            return true;
        }

        [BindingBrowsable(false)]
        public override string EntityInstanceDisplayName {
            get {
                return AddressType;
            }
        }

        public static EntityList<AddressTypes> GetAllAddressTypes(PersistenceManager pPM) {
            EntityList<AddressTypes> oTypes = pPM.GetEntities<AddressTypes>();
            oTypes.ApplySort(AddressTypes.AddressTypeEntityColumn.ColumnName, System.ComponentModel.ListSortDirection.Ascending, false);
            return oTypes;
        }

        public static AddressTypes GetAddressType(PersistenceManager pPM, string pAddressType) {
            RdbQuery oQry = new RdbQuery(typeof(AddressTypes), AddressTypes.AddressTypeEntityColumn, EntityQueryOp.EQ, pAddressType);
            AddressTypes oAddress = pPM.GetEntity<AddressTypes>(oQry);
            return oAddress;
        }
    }

}
