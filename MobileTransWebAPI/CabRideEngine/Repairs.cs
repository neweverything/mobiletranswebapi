﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;
using TaxiPassCommon;

namespace CabRideEngine {
    /// <summary>
    /// The developer class representing the Repairs business object class.
    /// </summary>
    /// <remarks>
    /// <para>Place all application-specific business logic in this class.</para>
    /// <para>
    /// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
    /// and will not overwrite changes.
    /// </para>
    /// <para>
    /// This class inherits from the generated class, its companion in the pair of classes 
    /// defining a business object. Properties in the generated class may be overridden in 
    /// order to change the behavior or add business logic.
    /// </para>
    /// <para>
    /// This class is the final class in the inheritance chain for this business object. 
    /// It should not be inherited.
    /// </para>
    /// </remarks>
    [Serializable]
    public sealed partial class Repairs : RepairsDataRow {

        #region Constructors -- DO NOT MODIFY

        // Do not create constructors for this class
        // Developers cannot create instances of this class with the "new" keyword
        // You should write a static Create method instead; see the "Suggested Customization" region
        // Please review the documentation to learn about entity creation and inheritance

        // This private constructor prevents accidental instance creation via 'new';
        // it is also required if the class is visible from a web service.
        // DO NOT REMOVE **
        private Repairs() : this(null) { }

        /// <summary>
        /// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
        /// </summary>
        /// <param name="pRowBuilder"></param>
        public Repairs(DataRowBuilder pRowBuilder)
            : base(pRowBuilder) {
        }

        #endregion

        #region Suggested Customizations

        //    // Use this factory method template to create new instances of this class
        //    public static Repairs Create(PersistenceManager pManager,
        //      ... your creation parameters here ...) { 
        //
        //      Repairs aRepairs = pManager.CreateEntity<Repairs>();
        //
        //      // if this object type requires a unique id and you have implemented
        //      // the IIdGenerator interface implement the following line
        //      pManager.GenerateId(aRepairs, // add id column here //);
        //
        //      // Add custom code here
        //
        //      aRepairs.AddToManager();
        //      return aRepairs;
        //    }

        //    // Implement this method if you want your object to sort in a specific order
        //    public override int CompareTo(Object pObject) {
        //    }

        //    // Implement this method to customize the null object version of this class
        //    protected override void UpdateNullEntity() {
        //    }

        #endregion

        // Add additional logic to your business object here...
        public static Repairs Create(AffiliateDrivers pAffiliateDrivers) {
            Repairs oRec = null;
            PersistenceManager oPM = pAffiliateDrivers.PersistenceManager;
            try {
                oRec = oPM.CreateEntity<Repairs>();

                // Using the IdeaBlade Id Generation technique
                oPM.GenerateId(oRec, Repairs.RepairIDEntityColumn);
                oRec.AffiliateDriverID = pAffiliateDrivers.AffiliateDriverID;
                TimeZoneConverter convert = new TimeZoneConverter();
                oRec.RepairRequestDate = convert.ConvertTime(DateTime.Now, TimeZone.CurrentTimeZone.StandardName, pAffiliateDrivers.Affiliate.TimeZone);

                oRec.AddToManager();

            } catch (Exception ex) {
                throw ex;
            }
            return oRec;
        }

        public string AffiliateDriverName {
            get {
                return this.AffiliateDrivers.Name;
            }
        }

        public string AffiliateDriverCellFormattedPhone {
            get {
				return AffiliateDriverCell.FormattedPhoneNumber();
            }
        }

        public string AffiliateDriverCell {
            get {
                if (this.AffiliateDrivers.Cell.Trim().Length == 10) {
                    return this.AffiliateDrivers.Cell;
                }
                return this.AffiliateDrivers.CallInCell;
            }
        }

        public string AffiliateName {
            get {
                return this.AffiliateDrivers.Affiliate.Name;
            }
        }

        public string DriverName {
            get {
                return this.Drivers.Name;
            }
        }

        public DateTime LocalRepairRequestDate {
            get {
                if (this.AffiliateDrivers.Affiliate.TimeZone == null) {
                    return this.RepairRequestDate;
                }
                TimeZoneConverter oTimeZone = new TimeZoneConverter();
                return oTimeZone.FromUniversalTime(this.AffiliateDrivers.Affiliate.TimeZone, RepairRequestDate);
            }
        }

        public DateTime? LocalRepairClosedDate {
            get {
                if (!this.ClosedDate.HasValue) {
                    return ClosedDate;
                }
                if (this.AffiliateDrivers.Affiliate.TimeZone == null) {
                    return this.ClosedDate;
                }
                TimeZoneConverter oTimeZone = new TimeZoneConverter();
                return oTimeZone.FromUniversalTime(this.AffiliateDrivers.Affiliate.TimeZone, ClosedDate.Value);
            }
        }

        public static EntityList<Repairs> GetRepairs(PersistenceManager pPM, DateTime pStart, DateTime pEnd, bool pIncludeClosed) {
            RdbQuery qry = new RdbQuery(typeof(Repairs), Repairs.RepairRequestDateEntityColumn, EntityQueryOp.GE, pStart);
            qry.AddClause(RepairRequestDateEntityColumn, EntityQueryOp.LE, pEnd);
            if (!pIncludeClosed) {
                qry.AddClause(Repairs.ClosedDateEntityColumn, EntityQueryOp.IsNull, true);
            }
            return pPM.GetEntities<Repairs>(qry);
        }

    }

    #region EntityPropertyDescriptors.RepairsPropertyDescriptor
    ////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
    ////    For example: 
    ////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
    //public static partial class EntityPropertyDescriptors {
    //	public partial class RepairsPropertyDescriptor : BaseEntityPropertyDescriptor {
    //    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
    //    public AdaptedPropertyDescriptor Age {
    //      get { return this.Get("Age"); }
    //    }
    //  }
    //} 
    #endregion

}