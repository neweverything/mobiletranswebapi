﻿using Dapper;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace CabRideEngineDapper.StoredProc {

    public class usp_FreezeDriver {
        public long AffiliateID { get; set; }
        public int NewDriverVoucherCount { get; set; }
        public int DriverFreezeVoucherCount { get; set; }
        public decimal DriverFreezeAverageVoucherAmount { get; set; }
        public int TotalVouchers { get; set; }
        public bool AccountFrozen { get; set; }
        public int BadVoucherCount { get; set; }
        public string TransNo { get; set; }

        public static usp_FreezeDriver ExecuteProc(SqlConnection pConn, long? pAffiliateDriverID, long pDriverID, DateTime pEndDate) {

            DynamicParameters parms = new DynamicParameters();
            parms.Add("EndDate", pEndDate);

            if (pAffiliateDriverID.HasValue) {
                parms.Add("AffiliateDriverID", pAffiliateDriverID);
            } else {
                parms.Add("DriverID", pDriverID);
            }

            return pConn.Query<usp_FreezeDriver>("usp_FreezeDriver", parms, commandType: CommandType.StoredProcedure).FirstOrDefault();
        }
    }
}
