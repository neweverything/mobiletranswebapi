﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.XPath;
using System.ServiceModel;

using TaxiPassCommon.Banking.com.ipcommerce.SvcInfo;

namespace TaxiPassCommon.Banking {
	public class IPCommerceCredentials : IGatewayCredentials {
		public int ProcessingOrder { get; set; }

		private string mToken;
		//private string mSessionToken;

		public string SvcInfoUrl { get; set; }
		public string SvcInfoUrlTest { get; set; }

		public string TxnUrl { get; set; }
		public string TxnUrlTest { get; set; }

		//public string TMSUrl { get; set; }
		//public string TMSUrlTest { get; set; }

		public string ServiceKey { get; set; }
		public string ApplicationProfileId { get; set; }
		public string ServiceId { get; set; }
		public string WorkflowId { get; set; }
		public string MerchantProfileId { get; set; }

		public long VerisignAccountID { get; set; }
		public string MerchantID { get; set; }

		public string Token {
			get {
				return mToken;
			}
			set {
				mToken = value;
				ServiceKey = RetrieveServiceKeyFromIdentityToken(value);
			}
		}

		private string RetrieveServiceKeyFromIdentityToken(string identityToken) {
			String clearToken = Encoding.UTF8.GetString(Convert.FromBase64String(identityToken));

			//Now try and retrieve the Service Key from the XML
			XmlDocument doc = new XmlDocument();
			doc.LoadXml(clearToken);
			XPathNavigator xnav = doc.CreateNavigator();

			XmlNamespaceManager manager = new XmlNamespaceManager(xnav.NameTable);
			manager.AddNamespace("SK", "urn:oasis:names:tc:SAML:1.0:assertion");

			XPathNavigator node = xnav.SelectSingleNode("//SK:Attribute[@AttributeName='SAK']", manager);
			return node.Value;
		}




		public CardProcessors MyGateway() {
			return CardProcessors.IPCommerce;
		}

	}
}
