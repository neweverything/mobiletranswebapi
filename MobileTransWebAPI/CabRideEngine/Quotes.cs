﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

using TaxiPassCommon;

namespace CabRideEngine {
    [Serializable]
    public sealed class Quotes : QuotesDataRow {

        #region Constructors -- DO NOT MODIFY
        // Do not create constructors for this class
        // Developers cannot create instances of this class with the "new" keyword
        // You should write a static Create method instead; see the "Suggested Customization" region
        // Please review the documentation to learn about entity creation and inheritance

        // This private constructor prevents accidental instance creation via 'new'
        private Quotes()
            : this(null) {
        }

        // Typed DataSet constructor (needed by framework) - DO NOT REMOVE
        public Quotes(DataRowBuilder pRowBuilder)
            : base(pRowBuilder) {
        }

        #endregion

        #region Suggested Customizations

        //    // Use this factory method template to create new instances of this class
        //    public static Quotes Create(PersistenceManager pManager,
        //      ... your creation parameters here ...) { 
        //
        //      Quotes aQuotes = pManager.CreateEntity<Quotes>();
        //
        //      // if this object type requires a unique id and you have implemented
        //      // the IIdGenerator interface implement the following line
        //      pManager.GenerateId(aQuotes, // add id column here //);
        //
        //      // Add custom code here
        //
        //      aQuotes.AddToManager();
        //      return aQuotes;
        //    }

        //    // Implement this method if you want your object to sort in a specific order
        //    public override int CompareTo(Object pObject) {
        //    }

        //    // Implement this method to customize the null object version of this class
        //    protected override void UpdateNullEntity() {
        //    }

        #endregion

        // Add additional logic to your business object here...
        public static Quotes Create(PersistenceManager pManager) {

            Quotes oQuote = (Quotes)pManager.CreateEntity(typeof(Quotes));
            //pManager.GenerateId(oQuote, Quotes.QuoteIDEntityColumn);
            oQuote.QuoteNo = TaxiPassCommon.StringUtils.GetUniqueKey(12);

            oQuote.AddToManager();
            return oQuote;
        }

        protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
            if (IsDeserializing)
                return;
            base.OnColumnChanging(pArgs);
            //DoAudit(pArgs);
        }

        private void DoAudit(DataColumnChangeEventArgs pArgs) {
            if (!this.PersistenceManager.IsClient) {
                return;
            }
            if (this.RowState == DataRowState.Added) {
                return;
            }
            if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
                AuditRecord = QuotesAudit.Create(this.PersistenceManager, this);
            }
            //AuditRecord[pArgs.Column.ColumnName] = pArgs.ProposedValue;
        }

        public string PickUpInfo {
            get {
                string sPickUp = "";
                if (PURoutingType == RoutingTypes.Airport) {
                    sPickUp = "Airport Pickup: " + FullAirportName(PUAirport) + " (" + PUAirport + ")";
                } else {
                    sPickUp = PUStreet;
                    sPickUp += ", " + PUCity;
                    sPickUp += ", " + PUState;
                    sPickUp += ", " + PUCountry;
                }
                return sPickUp;
            }
        }

        public string DropOffInfo {
            get {
                string sDropOff = "";
                if (DORoutingType == RoutingTypes.Airport) {
                    sDropOff = "Airport Dropoff: " + FullAirportName(DOAirport) + " (" + DOAirport + ")";
                } else {
                    sDropOff = DOStreet;
                    sDropOff += ", " + DOCity;
                    sDropOff += ", " + DOState;
                    sDropOff += ", " + DOCountry;
                }
                return sDropOff;
            }
        }

        public string GetMarket {
            get {
                if (this.AffiliateID != null) {
                    return this.Affiliate.AffiliateCoverageZoneses[0].CoverageZones.Markets.Market;
                }

                if (PURoutingType == RoutingTypes.Airport) {
                    return Airports.GetAirport(this.PersistenceManager, PUAirport).CoverageZones.Markets.Market;
                }
                Affiliate oAffiliate = null;
                try {
                    Address myPUAddress = this.PickUpAddress;
                    Address myDOAddress = this.DropOffAddress;
                    oAffiliate = Affiliate.GetAffiliateForJob(this.PersistenceManager, myPUAddress, myDOAddress);
                    if (this.PUZIPCode != myPUAddress.ZIP) {
                        this.PUZIPCode = myPUAddress.ZIP;
                    }
                    if (this.DOZIPCode != myDOAddress.ZIP) {
                        this.DOZIPCode = myDOAddress.ZIP;
                    }
                    if (!oAffiliate.IsNullEntity) {
                        this.AffiliateID = oAffiliate.AffiliateID;
                    }
                    if (this.RowState == DataRowState.Modified) {
                        //this.Save();
                        //do this so we don't override the Modified Date/User
                        EntityList<Quotes> myQuotes = new EntityList<Quotes>();
                        myQuotes.Add(this);
                        this.PersistenceManager.SaveChanges(myQuotes);
                    }
                } catch {
                    if (this.BasePrice > 0) {
                        return "Unknown";
                    } 
                }
                if (oAffiliate == null || oAffiliate.IsNullEntity) {
                    return "No Affiliate";
                } 
                return oAffiliate.AffiliateCoverageZoneses[0].CoverageZones.Markets.Market;
            }
        }

        public Address PickUpAddress {
            get {
                Address oAddress = new Address();
                if (PURoutingType == RoutingTypes.Airport) {
                    oAddress.IsAirport = true;
                    oAddress.Airport = PUAirport;
                } else {
                    oAddress.Street = this.PUStreet;
                }
                oAddress.City = this.PUCity;
                oAddress.State = this.PUState;
                oAddress.Country = this.PUCountry;
                oAddress.ZIP = this.PUZIPCode;
                if (oAddress.ZIP == "") {

                }
                if (this.PULatitude != null) {
                    oAddress.Latitude = (double)this.PULatitude;
                }
                if (this.PULongitude != null) {
                    oAddress.Longitude = (double)this.PULongitude;
                }
                return oAddress;
            }
        }

        public Address DropOffAddress {
            get {
                Address oAddress = new Address();
                if (DORoutingType == RoutingTypes.Airport) {
                    oAddress.IsAirport = true;
                    oAddress.Airport = DOAirport;
                } else {
                    oAddress.Street = this.DOStreet;
                }
                oAddress.City = this.DOCity;
                oAddress.State = this.DOState;
                oAddress.Country = this.DOCountry;
                oAddress.ZIP = this.DOZIPCode;
                if (this.DOLatitude != null) {
                    oAddress.Latitude = (double)this.DOLatitude;
                }
                if (this.DOLongitude != null) {
                    oAddress.Longitude = (double)this.DOLongitude;
                }
                return oAddress;
            }
        }

        private string FullAirportName(string pAirport) {
            return Airports.GetAirport(this.PersistenceManager, pAirport).Name;
        }


        public static Quotes GetQuote(PersistenceManager pPM, long pQuoteID) {
            RdbQuery qry = new RdbQuery(typeof(Quotes), Quotes.QuoteIDEntityColumn, EntityQueryOp.EQ, pQuoteID);
            return pPM.GetEntity<Quotes>(qry);
        }


        public string RunType {
            get {
                string sPickUp = "";
                if (PURoutingType == RoutingTypes.Airport) {
                    sPickUp = "Airport";
                } else {
                    sPickUp = "Address";
                }
                string sDropOff = "";
                if (DORoutingType == RoutingTypes.Airport) {
                    sDropOff = "Airport";
                } else {
                    sDropOff = "Address";
                }
                return sPickUp + " to " + sDropOff;
            }
        }

    }

}
