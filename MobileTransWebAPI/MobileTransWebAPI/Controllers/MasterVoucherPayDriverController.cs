﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using System.Web.Script.Serialization;

using IdeaBlade.Persistence;

using TaxiPassCommon;

using CabRideEngine;
using CabRideEngine.Utils;

using MobileTransWebAPI.Models;
using MobileTransWebAPI.Utils;

namespace MobileTransWebAPI.Controllers {

	public class MasterVoucherPayDriverController : ApiController {

		public MasterVoucherResult GetMasterVoucherPayDriver(string pTransRequest) {

			MasterVoucherResult result = new MasterVoucherResult();
			JavaScriptSerializer js = new JavaScriptSerializer();

			WebApiApplication.LogIt("DriverPaidForMasterVoucher", pTransRequest);

			PersistenceManager oPM = WebApiApplication.UserPM;
			if (oPM == null) {
				result.ErrorMsg = "Could not connect to database";
				return result;
			}

			TransListRequest info = js.Deserialize<TransListRequest>(pTransRequest.DecryptIceKey());

			if (info.VoucherNo.IsNullOrEmpty()) {
				result.ErrorMsg = "Invalid voucher";
				return result;
			}

			DateTime requestDate = info.RequestKey.DecryptIceKey().ToDateTime();
            if (WebApiApplication.GetUTCServerTime().Subtract(requestDate).TotalMinutes > 1) {
				result.ErrorMsg = "Invalid request";
				return result;
			}


			if (info.DriverID < 1) {
				result.ErrorMsg = "Invalid Access";
				return result;
			}

			AffiliateDrivers affDriver = AffiliateDrivers.GetDriver(oPM, info.AffDriverID);
			Drivers driver = Drivers.GetDriver(oPM, info.DriverID);
			if (driver.IsNullEntity) {
				result.ErrorMsg = "Invalid Driver";
				return result;
			}

			TaxiPassRedeemers redeemer = TaxiPassRedeemers.GetRedeemerByPhone(oPM, driver.Cell);
			if (redeemer.IsNullEntity) {
				result.ErrorMsg = "Invalid Access";
				return result;
			}

			MasterVoucher voucher = MasterVoucher.GetVoucher(oPM, info.VoucherNo);
			if (voucher.IsNullEntity) {
				result.ErrorMsg = "No vouchers found";
				return result;
			}

			if (voucher.DriverPaymentAuxs[0].DriverPayments.AffiliateDrivers.RedeemerStopPayment) {
				result.ErrorMsg = "Driver cannot be paid at this time.  Please have driver call TaxiPass";
				return result;
			}

			List<TransactionProcessedEntities> list = (from p in voucher.DriverPaymentAuxs
													   where p.DriverPayments.ACHDetail.IsNullEntity && !p.DriverPayments.Failed && !p.DriverPayments.Test && !p.DriverPayments.DriverPaid
													   select new TransactionProcessedEntities {
														   RedeemerId = redeemer.TaxiPassRedeemerID,
														   RedeemerAccountId = redeemer.TaxiPassRedeemerAccountID,
														   VoucherNo = p.DriverPayments.TransNo,
														   LocationID = -1,
														   Amount = p.DriverPayments.DriverTotal
													   }).ToList();
			if (list.Count < 1) {
				result.ErrorMsg = "No vouchers found";
				return result;
			}


			//TransactionWebService service = new TransactionWebService();
			//TransPaidResult[] resultList = service.TransactionProcessedByLocationBatch(list.ToArray());

			//resultList = (from p in resultList
			//              where p.Error.IsNullOrEmpty()
			//              select p).ToArray();

			//result.RedeemerName = redeemer.Name;
			//result.DriverName = voucher.DriverPaymentAuxs[0].DriverPayments.AffiliateDrivers.Name;
			//if (result.DriverName.IsNullOrEmpty()) {
			//    result.DriverName = voucher.Drivers.Name;
			//}
			//result.CellPhone = voucher.Drivers.Cell;

			//result.Count = resultList.Length;
			//result.Total = resultList.Sum(p => Convert.ToDecimal(p.VoucherAmount));

			//result.transList = (from p in resultList
			//                    select new MasterVoucherResult.TransRec {
			//                        TransNo = p.VoucherNo,
			//                        Total = Convert.ToDecimal(p.VoucherAmount),
			//                        RedemptionFee = p.RedemptionFee,
			//                        RecurringFee = p.RecurringFee,
			//                        DriverFee = p.DriverFee,
			//                    }).ToList();

			return result;
		}

		// **********************************************************
		// used for TransactionProcessedByLocation() batch processing
		// **********************************************************
		public class TransactionProcessedEntities {
			public long RedeemerAccountId { get; set; }
			public long RedeemerId { get; set; }
			public long LocationID { get; set; }
			public string VoucherNo { get; set; }
			public decimal Amount { get; set; }
			public string ImageURL { get; set; }
			public DriverLicense License { get; set; }
		}

		public class DriverLicense {
			public string Name { get; set; }
			public string LicenseNo { get; set; }
			public string Street { get; set; }
			public string City { get; set; }
			public string State { get; set; }

			public int ErrorCode { get; set; }
			public string ErrorMessage { get; set; }
		}

	}
}
