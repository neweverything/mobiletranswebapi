﻿using CabRideEngineDapper;
using CabRideEngineDapper.Utils;
using MobileTransWebAPI.Models;
using Newtonsoft.Json;
using System;
using System.Web.Http;
using TaxiPassCommon;

namespace MobileTransWebAPI.Controllers {
    public class ForgotPINController : ApiController {

        //public void Get() {
        //    ForgotPin("9F51C4A961FED61FDA0B3C71AA23C4D86F56D83B2B46E7C974D85366872A69C0594241E0CC28E13859F29D2933F8102BB2781106231D47AF027212426F9B558EB85688F0C5A211E999FF4BC8CA1C2C788D46B4F95EFB7D0F1E4A807776434721D0E7E2DBF16F2781223353EA4A00087651829E72ACFD4FFA30F4FC9BC437681DC621767DED0D6AFFE0E00E6DAFBAD28B721275023F07E6104BA421CCD22AD173C726FA6229D4CFB86AE5C4E71926466F92FCAF904578B507480B403157F78D41DE0715F56825E4F6B3C0BFD4C44FA209321CFD02E286CDCAC0B93B8C7389783D92FCAF904578B50783351DDA2B348D6C68C946D9C0954C53266304F1D77977BBA081AD97BC89C66488CF16B8DDB8AF7A1E72FCB9F65F96F1DA055FB7BDDB9BEE787B761750AA2256C44F2B0A36CF06182AC9A531A7D7B07DC621767DED0D6AFFFBF8991349CF335092FCAF904578B507A17B2110693E6AD6A4DD47FC3CAFD81841D14886818DFF393459EC90");
        //}

        [HttpGet]
        public void ForgotPin(string pRequest) {

            AffDriverLoginInfo info = new AffDriverLoginInfo();
            try {
                string request = pRequest.DecryptIceKey();
                info = JsonConvert.DeserializeObject<AffDriverLoginInfo>(request);
                if (!info.Phone.IsNullOrEmpty()) {
                    using (var conn = SqlHelper.OpenSqlConnection()) {
                        AffiliateDrivers driver = AffiliateDrivers.GetDriverByCallInCell(conn, info.Phone);
                        if (!driver.IsNullEntity) {
                            driver.SendMsgViaTwilio(conn, string.Format("Your TaxiPass PIN is {0}", driver.PIN.Length > 8 ? driver.PIN.DecryptIceKey() : driver.PIN));
                        }
                    }
                }
            } catch (Exception) {

            }
        }
    }
}