﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace CabRideEngineDapper {
    public partial class usp_AffiliateACHByBatchNo {
        public string MyAffiliateName { get; set; }
        public string MyDriverName { get; set; }
        public string MyVehicle { get; set; }
        public string CorporateName { get; set; }
        public string MyCellName { get; set; }
        public DateTime MyChargeDateTime { get; set; }
        public decimal Fare { get; set; }
        public decimal AirportFee { get; set; }
        public decimal MiscFee { get; set; }
        public decimal Tolls { get; set; }
        public decimal Parking { get; set; }
        public decimal Stops { get; set; }
        public decimal WaitTime { get; set; }
        public decimal Discount { get; set; }
        public decimal Gratuity { get; set; }
        public decimal DriverFee { get; set; }
        public decimal DebitFee { get; set; }
        public decimal DriverTotal { get; set; }
        public string CardNumberDisplay { get; set; }
        public string TransNo { get; set; }
        public string PreAuthApprovalNumber { get; set; }
        public string ChargeApprovalNumber { get; set; }
        public bool ACHDriverFeeAsDebit { get; set; }
        public string CardType { get; set; }
        public string BinType { get; set; }
        public string ACHPaidTo { get; set; }

        public decimal NetTotal {
            get {
                return Fare - DriverFee;
            }
        }

        public string CardTypeWithDebit {
            get {
                return CardType + (("" + BinType).Equals("Debit", StringComparison.CurrentCultureIgnoreCase) ? " Debit" : "");
            }
        }

        public static List<usp_AffiliateACHByBatchNo> Execute(SqlConnection pConn, long pAffiliateID, long pBatchNo) {
            return pConn.Query<usp_AffiliateACHByBatchNo>("usp_AffiliateACHByBatchNo", new { AffiliateID = pAffiliateID, ACHBatchNo = pBatchNo }, commandTimeout: 500, commandType: CommandType.StoredProcedure).ToList();
        }
    }
}
