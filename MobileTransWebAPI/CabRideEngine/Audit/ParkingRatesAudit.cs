﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Util;

namespace CabRideEngine {
    [Serializable]
    public sealed class ParkingRatesAudit : ParkingRatesAuditDataRow {

        #region Constructors -- DO NOT MODIFY
        // Do not create constructors for this class
        // Developers cannot create instances of this class with the "new" keyword
        // You should write a static Create method instead; see the "Suggested Customization" region
        // Please review the documentation to learn about entity creation and inheritance

        // This private constructor prevents accidental instance creation via 'new'
        private ParkingRatesAudit()
            : this(null) {
        }

        // Typed DataSet constructor (needed by framework) - DO NOT REMOVE
        public ParkingRatesAudit(DataRowBuilder pRowBuilder)
            : base(pRowBuilder) {
        }

        #endregion

        #region Suggested Customizations

        //    // Use this factory method template to create new instances of this class
        //    public static ParkingRatesAudit Create(PersistenceManager pManager,
        //      ... your creation parameters here ...) { 
        //
        //      ParkingRatesAudit aParkingRatesAudit = pManager.CreateEntity<ParkingRatesAudit>();
        //
        //      // if this object type requires a unique id and you have implemented
        //      // the IIdGenerator interface implement the following line
        //      pManager.GenerateId(aParkingRatesAudit, // add id column here //);
        //
        //      // Add custom code here
        //
        //      aParkingRatesAudit.AddToManager();
        //      return aParkingRatesAudit;
        //    }

        //    // Implement this method if you want your object to sort in a specific order
        //    public override int CompareTo(Object pObject) {
        //    }

        //    // Implement this method to customize the null object version of this class
        //    protected override void UpdateNullEntity() {
        //    }

        #endregion

        // Add additional logic to your business object here...
        public static ParkingRatesAudit Create(PersistenceManager pManager, ParkingRates pParkingRates) {
            ParkingRatesAudit oAudit = (ParkingRatesAudit)pManager.CreateEntity(typeof(ParkingRatesAudit));
            pManager.GenerateId(oAudit, ParkingRatesAudit.ParkingRateHistoryIDEntityColumn);

            for (int i = 0; i < pParkingRates.Table.Columns.Count; i++) {
                if (oAudit.Table.Columns.Contains(pParkingRates.Table.Columns[i].ColumnName)) {
                    oAudit[pParkingRates.Table.Columns[i].ColumnName] = pParkingRates[i];
                }
            }
            oAudit.AddToManager();

            return oAudit;
        }

    }

}
