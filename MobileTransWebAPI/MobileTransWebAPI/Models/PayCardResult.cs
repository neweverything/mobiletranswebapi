﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileTransWebAPI.Models {
	public class PayCardResult {
		public string ErrorMsg { get; set; }
		public string CardNumber { get; set; }
		public int AvailableTransfers { get; set; }
		public decimal CurrentCardBalance { get; set; }
		public decimal BalanceToTransfer { get; set; }
		public decimal AmountTransferred { get; set; }

		public int VoucherCount { get; set; }
		public decimal TransferFee { get; set; }
		public decimal VoucherTotal { get; set; }

	}
}