﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

using Dapper;
using TaxiPassCommon;

using CabRideEngineDapper.Utils;

namespace CabRideEngineDapper {
	public partial class BankHoliday {

		public static BankHoliday GetByDate(SqlConnection pPM, DateTime pDate) {
			string sql = @"SELECT * FROM dbo.BankHoliday
							WHERE Date = @Date";
			return pPM.Get<BankHoliday>(sql, new { Date = pDate });
		}

        public static List<BankHoliday> GetByDateRange(SqlConnection pConn, DateTime pStartDate, DateTime pEndDate) {
            string sql = @"SELECT  *
                            FROM    BankHoliday
                            WHERE   Date >= @StartDate
                                    AND Date < @EndDate";

            return pConn.Query<BankHoliday>(sql, new { StartDate = pStartDate, EndDate = pEndDate.AddDays(1).AddMilliseconds(-1) }).ToList();
        }

    }
}
