﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

using TaxiPassCommon;

namespace CabRideEngine {
	/// <summary>
	/// The developer class representing the DriverPaymentsPendingMatches business object class.
	/// </summary>
	/// <remarks>
	/// <para>Place all application-specific business logic in this class.</para>
	/// <para>
	/// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
	/// and will not overwrite changes.
	/// </para>
	/// <para>
	/// This class inherits from the generated class, its companion in the pair of classes 
	/// defining a business object. Properties in the generated class may be overridden in 
	/// order to change the behavior or add business logic.
	/// </para>
	/// <para>
	/// This class is the final class in the inheritance chain for this business object. 
	/// It should not be inherited.
	/// </para>
	/// </remarks>
	[Serializable]
	public sealed partial class DriverPaymentsPendingMatches : DriverPaymentsPendingMatchesDataRow {

		#region Constructors -- DO NOT MODIFY

		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new';
		// it is also required if the class is visible from a web service.
		// DO NOT REMOVE **
		private DriverPaymentsPendingMatches() : this(null) { }

		/// <summary>
		/// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
		/// </summary>
		/// <param name="pRowBuilder"></param>
		public DriverPaymentsPendingMatches(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static DriverPaymentsPendingMatches Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      DriverPaymentsPendingMatches aDriverPaymentsPendingMatches = pManager.CreateEntity<DriverPaymentsPendingMatches>();
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aDriverPaymentsPendingMatches, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aDriverPaymentsPendingMatches.AddToManager();
		//      return aDriverPaymentsPendingMatches;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		// Add additional logic to your business object here...
		public static DriverPaymentsPendingMatches Create(DriverPayments pDriverPayment) {
			PersistenceManager oPM = pDriverPayment.PersistenceManager;
			DriverPaymentsPendingMatches rec = null;
			try {
				rec = oPM.CreateEntity<DriverPaymentsPendingMatches>();

				// Using the IdeaBlade Id Generation technique
				oPM.GenerateId(rec, DriverPaymentsPendingMatches.DriverPaymentsPendingMatchIDEntityColumn);
				rec.DriverPaymentID = pDriverPayment.DriverPaymentID;

				rec.AddToManager();

				rec.Date = DateTime.Now;

			} catch (Exception ex) {
				throw ex;
			}
			return rec;
		}

		// ************************************************************************************
		// Get or create a record
		// ************************************************************************************
		public static DriverPaymentsPendingMatches GetOrCreate(DriverPayments pDriverPayment) {
			DriverPaymentsPendingMatches oDriverPaymentsPendingMatches = GetPendingMatch(pDriverPayment);
			if (oDriverPaymentsPendingMatches.IsNullEntity) {
				oDriverPaymentsPendingMatches = Create(pDriverPayment);
			}
			return oDriverPaymentsPendingMatches;
		}


		public static DriverPaymentsPendingMatches GetPendingMatch(DriverPayments pDriverPayment) {
			RdbQuery qry = new RdbQuery(typeof(DriverPaymentsPendingMatches), DriverPaymentsPendingMatches.DriverPaymentIDEntityColumn, EntityQueryOp.EQ, pDriverPayment.DriverPaymentID);
			return pDriverPayment.PersistenceManager.GetEntity<DriverPaymentsPendingMatches>(qry, QueryStrategy.DataSourceThenCache);
		}

		public override DateTime Date {
			get {
				return base.Date;
			}
			set {
				base.Date = value;
				if (this.DriverPayments.Affiliate.TimeZone.IsNullOrEmpty()) {
					base.DriverPayments.PendingMatchDate = value;
				} else {
					TimeZoneConverter convert = new TimeZoneConverter();
					base.DriverPayments.PendingMatchDate = convert.ConvertTime(value, TimeZone.CurrentTimeZone.StandardName, this.DriverPayments.Affiliate.TimeZone);
				}
				//try {
				//    this.DriverPayments.Save();
				//} catch (Exception) {
				//}
			}
		}
	}

	#region EntityPropertyDescriptors.DriverPaymentsPendingMatchesPropertyDescriptor
	////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
	////    For example: 
	////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
	//public static partial class EntityPropertyDescriptors {
	//	public partial class DriverPaymentsPendingMatchesPropertyDescriptor : BaseEntityPropertyDescriptor {
	//    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
	//    public AdaptedPropertyDescriptor Age {
	//      get { return this.Get("Age"); }
	//    }
	//  }
	//} 
	#endregion

}