﻿using Csla.Validation;
using IdeaBlade.Persistence;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;
using System;
using System.Data;
using TaxiPassCommon;
using TaxiPassCommon.Banking;

namespace CabRideEngine {
	/// <summary>
	/// The developer class representing the TaxiPassRedeemerAccounts business object class.
	/// </summary>
	/// <remarks>
	/// <para>Place all application-specific business logic in this class.</para>
	/// <para>
	/// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
	/// and will not overwrite changes.
	/// </para>
	/// <para>
	/// This class inherits from the generated class, its companion in the pair of classes 
	/// defining a business object. Properties in the generated class may be overridden in 
	/// order to change the behavior or add business logic.
	/// </para>
	/// <para>
	/// This class is the final class in the inheritance chain for this business object. 
	/// It should not be inherited.
	/// </para>
	/// </remarks>
	[Serializable]
	public sealed partial class TaxiPassRedeemerAccounts : TaxiPassRedeemerAccountsDataRow {

		#region Constructors -- DO NOT MODIFY

		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new';
		// it is also required if the class is visible from a web service.
		// DO NOT REMOVE **
		private TaxiPassRedeemerAccounts()
			: this(null) {
		}

		/// <summary>
		/// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
		/// </summary>
		/// <param name="pRowBuilder"></param>
		public TaxiPassRedeemerAccounts(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static TaxiPassRedeemerAccounts Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      TaxiPassRedeemerAccounts aTaxiPassRedeemerAccounts = pManager.CreateEntity<TaxiPassRedeemerAccounts>();
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aTaxiPassRedeemerAccounts, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aTaxiPassRedeemerAccounts.AddToManager();
		//      return aTaxiPassRedeemerAccounts;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		// Add additional logic to your business object here...
		public static TaxiPassRedeemerAccounts Create(PersistenceManager pPM) {
			TaxiPassRedeemerAccounts rec = null;

			try {
				rec = pPM.CreateEntity<TaxiPassRedeemerAccounts>();

				// Using the IdeaBlade Id Generation technique
				pPM.GenerateId(rec, TaxiPassRedeemerAccountIDEntityColumn);
				rec.Country = Countries.GetDefaultCountry(pPM).Country;
				rec.BankCountry = rec.Country;
				rec.ValidationAmount = 1;
				rec.LandingPagePayDriver = true;
				rec.PaySchedule = "0110110";
				rec.PayCardRealTimePay = false;
				rec.CardNoVerificationAmount = 75;
				rec.AddToManager();

			} catch (Exception ex) {
				throw ex;
			}
			return rec;
		}

		protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
			if (IsDeserializing) {
				return;
			}
			base.OnColumnChanging(pArgs);
			DoAudit(pArgs);
		}

		private void DoAudit(DataColumnChangeEventArgs pArgs) {
			if (!this.PersistenceManager.IsClient) {
				return;
			}
			if (this.RowState == DataRowState.Added) {
				return;
			}
			if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
				AuditRecord = TaxiPassRedeemerAccountsAudit.Create(this.PersistenceManager, this);
			}
		}

		[BindingBrowsable(false)]
		public override string EntityInstanceDisplayName {
			get {
				return this.CompanyName;
			}
		}

		protected override void AddRules(RuleList pList) {
			PropertyRequiredRule.AddToList(pList, CompanyNameEntityColumn.ColumnName);
			PropertyRequiredRule.AddToList(pList, ContactEntityColumn.ColumnName);
			pList.Add(IsCompanyNameUnique, CompanyNameEntityColumn.ColumnName);
			pList.Add(IsSubDomainUnique, SubdomainEntityColumn.ColumnName);
		}

		private bool IsCompanyNameUnique(object pTarget, RuleArgs e) {
			TaxiPassRedeemerAccounts oRecord = pTarget as TaxiPassRedeemerAccounts;

			RdbQuery qry = new RdbQuery(typeof(TaxiPassRedeemerAccounts), CompanyNameEntityColumn, EntityQueryOp.EQ, oRecord.CompanyName);
			qry.AddClause(TaxiPassRedeemerAccountIDEntityColumn, EntityQueryOp.NE, oRecord.TaxiPassRedeemerAccountID);
			EntityList<TaxiPassRedeemerAccounts> oList = this.PersistenceManager.GetEntities<TaxiPassRedeemerAccounts>(qry);
			if (oList.Count > 0) {
				e.Description = "A TaxiPass Redeemer Account already exists with the company name: " + oRecord.CompanyName;
				return false;
			}
			return true;
		}

		private bool IsSubDomainUnique(object pTarget, RuleArgs e) {
			TaxiPassRedeemerAccounts oRecord = pTarget as TaxiPassRedeemerAccounts;
			if (string.IsNullOrEmpty(oRecord.Subdomain)) {
				return true;
			}

			RdbQuery qry = new RdbQuery(typeof(TaxiPassRedeemerAccounts), SubdomainEntityColumn, EntityQueryOp.EQ, oRecord.Subdomain);
			qry.AddClause(TaxiPassRedeemerAccountIDEntityColumn, EntityQueryOp.NE, oRecord.TaxiPassRedeemerAccountID);
			EntityList<TaxiPassRedeemerAccounts> oList = this.PersistenceManager.GetEntities<TaxiPassRedeemerAccounts>(qry);
			if (oList.Count > 0) {
				e.Description = "A TaxiPass Redeemer Account already contains this Sub Domain: " + oRecord.Subdomain;
				return false;
			}

			//check if corp customer has this subdomain
			Accounts acct = Accounts.GetAccountBySubDomain(this.PersistenceManager, oRecord.Subdomain);
			if (!acct.IsNullEntity) {
				e.Description = "A Corporate Account already contains this Sub Domain: " + oRecord.Subdomain;
				return false;
			}
			return true;
		}

		public string FormattedPhone {
			get {
				return this.Phone.FormattedPhoneNumber();
			}
		}

		public string ACHPaymentScheduleText {
			get {
				return CabRideEngine.ACHPaymentSchedule.GetById(this.ACHPaymentSchedule).Description;
			}
		}

		public override string TransCardNumber {
			get {
				return base.TransCardNumber.DecryptIceKey();
			}
			set {
				base.TransCardNumber = value.EncryptIceKey();
				if (value.IsNullOrEmpty()) {
					TransCardAdminNo = "";
				}
				base.TransCardCardNumber = CustomerCardNumberDisplay(value);
			}
		}

		private static string CustomerCardNumberDisplay(string pCardNo) {
			if (pCardNo.Length > 10) {
				string stars = new string('*', 10);
				return pCardNo.Left(1) + stars + pCardNo.Right(5);
			}
			return pCardNo;
		}

		public override string TransCardID {
			get {
				return base.TransCardID.DecryptIceKey();
			}
			set {
				base.TransCardID = value.EncryptIceKey();
			}
		}

		public override DateTime? TransCardExpiration {
			get {
				return base.TransCardExpiration;
			}
			set {
				if (value.HasValue) {
					base.TransCardExpiration = new DateTime(value.Value.Year, value.Value.Month, 1);
				} else {
					base.TransCardExpiration = value;
				}
			}
		}

		public PayInfo GetPayInfo() {
			if (this.BankAccountNumber.IsNullOrEmpty()) {
				return null;
			}
			PayInfo oPay = new PayInfo();
			oPay.NachaFile = this.SendNacha;
			oPay.BankAccountHolder = this.BankAccountHolder;
			oPay.BankAccountNumber = this.BankAccountNumber;
			oPay.BankAccountPersonal = this.BankAccountPersonal;
			oPay.BankAddress = this.BankAddress;
			oPay.BankCity = this.BankCity;
			oPay.BankCode = this.BankCode;
			oPay.BankState = this.BankState;
			oPay.BankSuite = this.BankSuite;
			oPay.BankZIPCode = this.BankZIPCode;
			if (this.MerchantInfo != null) {
				oPay.MyMerchant = new Merchant();
				oPay.MyMerchant.MerchantNo = this.MerchantInfo.MerchantNo;
			}
			oPay.PaidTo = "Redeemer: " + this.CompanyName;

			oPay.WireTransfer = this.ACHProcessTransWireDepositManual.GetValueOrDefault(false);

			return oPay;
		}

		public decimal GetCardNoVerificationAmt {
			get {
				decimal amt = this.CardNoVerificationAmount;

				if (amt < 1) {
					amt = SystemDefaults.GetDefaults(this.PersistenceManager).CardNoVerificationAmount;
				}
				return amt;
			}
		}

		//public decimal GetPayCardBalance() {
		//    try {
		//        TransCardDefaults tranDef = TransCardDefaults.GetDefaults(this.PersistenceManager, this.TransCardNumber);
		//        if (tranDef == null) {
		//            tranDef = TransCardDefaults.GetDefaultTransCard(this.PersistenceManager);
		//        }
		//        TransCard oCard = new TransCard(tranDef.GetTransCardAccess());
		//        TransCard.CardInfo oCardInfo = new TransCard.CardInfo();
		//        oCardInfo.CardNumber = this.TransCardNumber;
		//        oCardInfo.ComData = tranDef.ComData;
		//        TransCard.VALIDATE_CARD_RET cardResults = oCard.ValidateCard(oCardInfo);
		//        return Convert.ToDecimal(cardResults.CURRENT_CARD_BALANCE.Replace("$", ""));
		//    } catch (Exception) {
		//    }
		//    return 0;
		//}


		public bool PaySunday {
			get {
				return PaySchedule.Left(1) == "1";
			}
			set {
				string temp = "";
				temp = value ? "1" : "0";
				temp += PaySchedule.Substring(1);
				PaySchedule = temp;
			}
		}

		public bool PayMonday {
			get {
				return PaySchedule.Substring(1, 1) == "1";
			}
			//set {
			//    string temp = "";
			//    temp = value ? "1" : "0";
			//    PaySchedule = PaySchedule.Remove(1, 1);
			//    PaySchedule = PaySchedule.Insert(1, temp);
			//}
		}

		public bool PayTuesday {
			get {
				return PaySchedule.Substring(2, 1) == "1";
			}
			set {
				string temp = "";
				temp = value ? "1" : "0";
				PaySchedule = PaySchedule.Remove(2, 1);
				PaySchedule = PaySchedule.Insert(2, temp);
			}
		}

		public bool PayWednesday {
			get {
				return PaySchedule.Substring(3, 1) == "1";
			}
			set {
				string temp = "";
				temp = value ? "1" : "0";
				PaySchedule = PaySchedule.Remove(3, 1);
				PaySchedule = PaySchedule.Insert(3, temp);
			}
		}

		public bool PayThursday {
			get {
				return PaySchedule.Substring(4, 1) == "1";
			}
			set {
				string temp = "";
				temp = value ? "1" : "0";
				PaySchedule = PaySchedule.Remove(4, 1);
				PaySchedule = PaySchedule.Insert(4, temp);
			}
		}

		public bool PayFriday {
			get {
				return PaySchedule.Substring(5, 1) == "1";
			}
			set {
				string temp = "";
				temp = value ? "1" : "0";
				PaySchedule = PaySchedule.Remove(5, 1);
				PaySchedule = PaySchedule.Insert(5, temp);
			}
		}

		public bool PaySaturday {
			get {
				return PaySchedule.Right(1) == "1";
			}
			set {
				string temp = "";
				temp = value ? "1" : "0";
				PaySchedule = PaySchedule.Left(6);
				PaySchedule += temp;
			}
		}

		public Twilio.SMSMessage SendMsg(string pMsg) {
			PersistenceManager oPM = this.PersistenceManager;
			SystemDefaults oDef = SystemDefaults.GetDefaults(oPM);

			string cell = this.Phone;

			string acctSid = oDef.TwilioAccountSID;
			string acctToken = oDef.TwilioAccountToken;
			string apiVersion = oDef.TwilioAPIVersion;

			if (this.EMailVoucherRedemption) {
				try {
					string mailTo = "";
					CabRideEngineDapper.SystemDefaultsDict dict = CabRideEngineDapper.SystemDefaultsDict.GetByKey("EMail", "Receipt", false);
					string[] cred = dict.ValueString.Decrypt().Split('|');
					TaxiPassCommon.Mail.EMailMessage mail = new TaxiPassCommon.Mail.EMailMessage(cred[0], "", pMsg, cred[1]);
					foreach (TaxiPassRedeemers rec in this.TaxiPassRedeemerses) {
						if (rec.Admin) {
							mailTo += rec.Email + ";";
						}
					}
					if (!mailTo.IsNullOrEmpty()) {
						mailTo = mailTo.Left(mailTo.Length - 1);
						mail.UseWebServiceToSendMail = Properties.Settings.Default.EMailViaWebService;
						mail.SendMail(mailTo);
					}
				} catch (Exception) {
				}
				return new Twilio.SMSMessage();
			}
			TaxiPassCommon.Mail.TwilioTexting twilio = new TaxiPassCommon.Mail.TwilioTexting(acctSid, acctToken, apiVersion, oDef.TwilioSMSNumber);
			return twilio.TwilioTextMessage(pMsg, cell);
		}


		public static EntityList<TaxiPassRedeemerAccounts> GetAll(PersistenceManager pPM) {
			EntityList<TaxiPassRedeemerAccounts> oList = pPM.GetEntities<TaxiPassRedeemerAccounts>();
			oList.ApplySort(TaxiPassRedeemerAccounts.CompanyNameEntityColumn.ColumnName, System.ComponentModel.ListSortDirection.Ascending, false);
			return oList;
		}

		public static TaxiPassRedeemerAccounts GetByPhone(PersistenceManager pPM, string pPhone) {
			RdbQuery qry = new RdbQuery(typeof(TaxiPassRedeemerAccounts), TaxiPassRedeemerAccounts.PhoneEntityColumn, EntityQueryOp.EQ, pPhone);
			return pPM.GetEntity<TaxiPassRedeemerAccounts>(qry);
		}

		public static TaxiPassRedeemerAccounts GetBySubDomain(PersistenceManager pPM, string pSubDomain) {
			RdbQuery qry = new RdbQuery(typeof(TaxiPassRedeemerAccounts), TaxiPassRedeemerAccounts.SubdomainEntityColumn, EntityQueryOp.EQ, pSubDomain);
			return pPM.GetEntity<TaxiPassRedeemerAccounts>(qry);
		}

		public static TaxiPassRedeemerAccounts GetByID(PersistenceManager pPM, long pAcctID) {
			RdbQuery qry = new RdbQuery(typeof(TaxiPassRedeemerAccounts), TaxiPassRedeemerAccounts.TaxiPassRedeemerAccountIDEntityColumn, EntityQueryOp.EQ, pAcctID);
			return pPM.GetEntity<TaxiPassRedeemerAccounts>(qry);
		}

		public static EntityList<TaxiPassRedeemerAccounts> GetAutoACHEnabled(PersistenceManager pPM) { //, bool pNachaFile) {
			RdbQuery qry = new RdbQuery(typeof(TaxiPassRedeemerAccounts), TaxiPassRedeemerAccounts.EnableAutoACHEntityColumn, EntityQueryOp.EQ, true);
			//qry.AddClause(TaxiPassRedeemerAccounts.SendNachaEntityColumn, EntityQueryOp.EQ, pNachaFile);
			qry.AddOrderBy(TaxiPassRedeemerAccounts.CompanyNameEntityColumn);
			return pPM.GetEntities<TaxiPassRedeemerAccounts>(qry);
		}

		public static TaxiPassRedeemerAccounts GetByName(PersistenceManager pPM, string pName) {
			RdbQuery qry = new RdbQuery(typeof(TaxiPassRedeemerAccounts), TaxiPassRedeemerAccounts.CompanyNameEntityColumn, EntityQueryOp.EQ, pName);
			return pPM.GetEntity<TaxiPassRedeemerAccounts>(qry);
		}

		// ***********************************************************************************************
		// TransCardCardNumber format:  5**********06318
		// ***********************************************************************************************
		public static TaxiPassRedeemerAccounts GetByPayCard(PersistenceManager pPM, string pPayCard) {
			if (string.IsNullOrEmpty(pPayCard)) {
				return pPM.GetNullEntity<TaxiPassRedeemerAccounts>();
			}

			string sCard = pPayCard.CardNumberDisplay();

			RdbQuery qry = new RdbQuery(typeof(TaxiPassRedeemerAccounts), TaxiPassRedeemerAccounts.TransCardCardNumberEntityColumn, EntityQueryOp.EQ, sCard);
			qry.AddClause(TaxiPassRedeemerAccounts.ActiveEntityColumn, EntityQueryOp.EQ, true);
			EntityList<TaxiPassRedeemerAccounts> list = pPM.GetEntities<TaxiPassRedeemerAccounts>(qry, QueryStrategy.DataSourceThenCache);
			foreach (TaxiPassRedeemerAccounts rec in list) {
				if (rec.TransCardNumber.Equals(pPayCard)) {
					return rec;
				}
			}

			return pPM.GetNullEntity<TaxiPassRedeemerAccounts>();
		}


		// ***********************************************************************************************
		// Lookup by TransCardNumber (encrypted card number)
		// ***********************************************************************************************
		public static TaxiPassRedeemerAccounts GetByPayCardNumber(PersistenceManager pPM, string pCardNumber) {
			if (string.IsNullOrEmpty(pCardNumber)) {
				return pPM.GetNullEntity<TaxiPassRedeemerAccounts>();
			}

			RdbQuery qry = new RdbQuery(typeof(TaxiPassRedeemerAccounts), TaxiPassRedeemerAccounts.TransCardNumberEntityColumn, EntityQueryOp.EQ, pCardNumber);
			qry.AddClause(TaxiPassRedeemerAccounts.ActiveEntityColumn, EntityQueryOp.EQ, true);
			return pPM.GetEntity<TaxiPassRedeemerAccounts>(qry, QueryStrategy.DataSourceThenCache);
		}

		// ***********************************************************************************************
		// Lookup by TransCardAdminNo
		// ***********************************************************************************************
		public static TaxiPassRedeemerAccounts GetByPayCardAdminNo(PersistenceManager pPM, string pPayCard) {
			if (string.IsNullOrEmpty(pPayCard)) {
				return pPM.GetNullEntity<TaxiPassRedeemerAccounts>();
			}

			RdbQuery qry = new RdbQuery(typeof(TaxiPassRedeemerAccounts), TaxiPassRedeemerAccounts.TransCardAdminNoEntityColumn, EntityQueryOp.EQ, pPayCard);
			qry.AddClause(TaxiPassRedeemerAccounts.ActiveEntityColumn, EntityQueryOp.EQ, true);
			return pPM.GetEntity<TaxiPassRedeemerAccounts>(qry, QueryStrategy.DataSourceThenCache);
		}

		public static EntityList<TaxiPassRedeemerAccounts> GetBofAAccounts(PersistenceManager pPM) {
			RdbQuery qry = new RdbQuery(typeof(TaxiPassRedeemerAccounts), TaxiPassRedeemerAccounts.BofAAccountInfoEntityColumn, EntityQueryOp.IsNotNull, true);
			qry.AddClause(TaxiPassRedeemerAccounts.BofAAccountInfoEntityColumn, EntityQueryOp.NE, "");
			return pPM.GetEntities<TaxiPassRedeemerAccounts>(qry, QueryStrategy.DataSourceThenCache);
		}

		public static TaxiPassRedeemerAccounts GetByBankInfo(PersistenceManager pPM, string pRouting, string pAccountNo) {
			RdbQuery qry = new RdbQuery(typeof(TaxiPassRedeemerAccounts), TaxiPassRedeemerAccounts.BankCodeEntityColumn, EntityQueryOp.EQ, pRouting);
			qry.AddClause(TaxiPassRedeemerAccounts.BankAccountNumberEntityColumn, EntityQueryOp.EQ, pAccountNo);
			return pPM.GetEntity<TaxiPassRedeemerAccounts>(qry);
		}


		// *************************************************************************
		// Activate the Transcard for the redeemer account
		// *************************************************************************
		//public TransCard.VALIDATE_CARD_RET ActivateTransCard() {
		//    bool blnActivate = true;
		//    TransCard.VALIDATE_CARD_RET transResult = new TransCard.VALIDATE_CARD_RET();
		//    PayCard oPayCard = new PayCard();
		//    string sWork;

		//    //update card if we have all info otherwise inactivate account until we do
		//    if (this.Contact.IsNullOrEmpty() || this.Street.IsNullOrEmpty() || this.City.IsNullOrEmpty() || this.State.IsNullOrEmpty() ||
		//        this.Country.IsNullOrEmpty() || !this.TransCardExpiration.HasValue || this.TransCardNumber.IsNullOrEmpty() ||
		//        this.LicenseNo.IsNullOrEmpty() || this.LicenseState.IsNullOrEmpty() || !this.DateOfBirth.HasValue) {
		//        if (this.Active) {
		//            this.TransCardActive = false;
		//            transResult.ERROR_MESSAGE = "Cannot activate driver until Driver's License, address and Trans Card data is populated";
		//            transResult.ERROR_FOUND = "Y";
		//            transResult.ERROR_NUMBER = "-1";
		//        }
		//    } else {
		//        string cardNumber = this.TransCardNumber;
		//        // Check for duplicate card numbers
		//        TaxiPassRedeemerAccounts tpDup = GetByPayCard(this.PersistenceManager, cardNumber);
		//        if (!tpDup.IsNullEntity && tpDup.TaxiPassRedeemerAccountID != this.TaxiPassRedeemerAccountID) {
		//            blnActivate = false;
		//            transResult.ERROR_FOUND = "Y";
		//            transResult.ERROR_NUMBER = "-2";
		//            transResult.ERROR_MESSAGE = "This TransCard Number found in the Redeemer Account belonging to: " + tpDup.CompanyName;
		//        }

		//        //TransCard.CARDHOLDER_DETAIL_RET detail = oCard.CardHolderDetail(cardNumber);

		//        if (blnActivate) {
		//            //just update changes even if it's the same
		//            CabRideEngine.com.cabpay.CellPhoneWebService.TransCardDriverIdentification oID = new CabRideEngine.com.cabpay.CellPhoneWebService.TransCardDriverIdentification();

		//            oID.EmployeeID = this.TaxiPassRedeemerAccountID.ToString();

		//            oID.AddressLine1 = this.Street;
		//            oID.AddressLine2 = this.Suite;
		//            oID.CardNumber = this.TransCardNumber;
		//            oID.City = this.City;
		//            oID.CountryCode = this.Country;
		//            //oID.Email = this.EMail;

		//            if (this.Contact.Contains(",")) {
		//                List<string> nameList = this.Contact.Split(',').ToList();
		//                oID.LastName = nameList[0];
		//                for (int i = 1; i < nameList.Count; i++) {
		//                    if (nameList.Count - 1 == i) {
		//                        oID.FirstName = nameList[i];
		//                    } else {
		//                        oID.MiddleName += nameList[i] + " ";
		//                    }
		//                }
		//            } else {
		//                List<string> nameList = this.Contact.Split(' ').ToList();
		//                oID.FirstName = nameList[0];
		//                for (int i = 1; i < nameList.Count; i++) {
		//                    if (nameList.Count - 1 == i) {
		//                        oID.LastName = nameList[i];
		//                    } else {
		//                        oID.MiddleName += nameList[i] + " ";
		//                    }
		//                }
		//            }
		//            oID.Phone = this.Phone;
		//            oID.PostalCode = this.ZIPCode;
		//            oID.StateProvince = this.State;
		//            if (this.DateOfBirth.HasValue) {
		//                oID.DateOfBirth = this.DateOfBirth.Value.ToString("yyyyMMdd");
		//            }
		//            //oID.IdCode = "STATE DRIVERS LICENSE";
		//            oID.IdCode = "S";
		//            oID.IdNumber = "123456789";
		//            oID.IdCountry = "USA";  // "California"; // States.GetStatesByCountry(mPM, "USA").Find(p=>p.State.Equals(oDriver.State, StringComparison.CurrentCultureIgnoreCase); // "USA";

		//            try {
		//                CabRideEngine.com.cabpay.CellPhoneWebService.Services oService = new CabRideEngine.com.cabpay.CellPhoneWebService.Services();
		//                CabRideEngine.com.cabpay.CellPhoneWebService.CARDHOLDER_IDENTIFICATION_RET oResult = oService.CardHolderIdentification(oID);

		//                if (!oResult.ERROR_NUMBER.IsNullOrEmpty() && oResult.ERROR_NUMBER != "0") {
		//                    transResult.ERROR_NUMBER = "-1";
		//                    transResult.ERROR_FOUND = "Y";
		//                    transResult.ERROR_MESSAGE = "Error updating Drivers Info on TransCard: " + oResult.ERROR_MESSAGE;

		//                } else {
		//                    oPayCard.SetCardNumber(this.PersistenceManager, this.TransCardNumber);
		//                    // set this as it isn't saved in the system yet.
		//                    oPayCard.TCCardInfo.CardNumber = this.TransCardNumber;
		//                    oPayCard.TCCardInfo.ComData = oResult.ComData;
		//                    oPayCard.TCCardInfo.EmployeeID = oID.EmployeeID;
		//                    oPayCard.TCCardInfo.CardHolderReferenceNumber = oResult.CardHolderReferenceNumber;
		//                    oPayCard.TCCardInfo.FirstName = oID.FirstName;
		//                    oPayCard.TCCardInfo.LastName = oID.LastName;

		//                    if (oResult.ComData) {

		//                        // 1/27/12 ComData doesn't offer us a valid "ValidateCard" yet
		//                        if (oPayCard.TCCardInfo.AdminNo != oResult.CardHolderReferenceNumber) {
		//                            oPayCard.TCCardInfo.AdminNo = oResult.CardHolderReferenceNumber;
		//                            this.TransCardAdminNo = TaxiPassCommon.Banking.TransCard.TransCardAdminNoCalc(oResult.ComData, oResult.CardHolderReferenceNumber);
		//                            this.Save();
		//                        }
		//                        transResult.ACCOUNT_NO = this.TransCardAdminNo;
		//                        transResult.CARD_STATUS = "ACTIVE";
		//                        transResult.ACTIVATION_REQUIRED = "Y";
		//                        transResult.FIRST_NAME = oID.FirstName;
		//                        transResult.LAST_NAME = oID.LastName;
		//                        transResult.CARDHOLDER_IDENTIFIED = "Y";
		//                        transResult.CURRENT_CARD_BALANCE = "0";

		//                    } else {

		//                        sWork = JsonConvert.SerializeObject(oPayCard.TCCardInfo).EncryptIceKey();
		//                        sWork = oService.ValidateCard2(sWork).DecryptIceKey();
		//                        transResult = JsonConvert.DeserializeObject<TransCard.VALIDATE_CARD_RET>(sWork);

		//                    }

		//                    if (transResult.ACTIVATION_REQUIRED.StartsWith("Y", StringComparison.CurrentCultureIgnoreCase)) {
		//                        sWork = Newtonsoft.Json.JsonConvert.SerializeObject(oPayCard.TCCardInfo).EncryptIceKey();
		//                        sWork = oService.ActivateCard2(sWork).DecryptIceKey();
		//                        TransCard.CARD_ACTIVATION_RET actResult = Newtonsoft.Json.JsonConvert.DeserializeObject<TransCard.CARD_ACTIVATION_RET>(sWork);

		//                        if (actResult.ERROR_FOUND.StartsWith("N", StringComparison.CurrentCultureIgnoreCase)) {
		//                            string sAdminNo = "";
		//                            if (!oResult.ComData) {
		//                                sWork = JsonConvert.SerializeObject(oPayCard.TCCardInfo).EncryptIceKey();
		//                                sWork = oService.ValidateCard2(sWork).DecryptIceKey();
		//                                transResult = JsonConvert.DeserializeObject<TransCard.VALIDATE_CARD_RET>(sWork);
		//                                sAdminNo = transResult.ACCOUNT_NO;
		//                            } else {

		//                                transResult.ACTIVATION_REQUIRED = "N";
		//                                sAdminNo = oResult.CardHolderReferenceNumber;
		//                            }

		//                            this.TransCardAdminNo = TaxiPassCommon.Banking.TransCard.TransCardAdminNoCalc(oResult.ComData, sAdminNo); 
		//                        }
		//                    }
		//                    this.TransCardActive = true;
		//                    this.Save();
		//                }
		//            } catch (Exception ex) {
		//                transResult.ERROR_NUMBER = "-1";
		//                transResult.ERROR_FOUND = "Y";
		//                transResult.ERROR_MESSAGE = "Error update TransCard Data " + ex.Message;
		//            }
		//        }
		//    }
		//    return transResult;
		//}


		// **********************************************************************************
		// Returns the lessor of either the Redeemer Accnt's or system MaxATMWithdrawal
		// **********************************************************************************
		public decimal GetMaxATMWithdrawal() {
			decimal dMax = MaxATMWithdrawal;
			SystemDefaults oDefaults = SystemDefaults.GetDefaults(this.PersistenceManager);
			if (dMax == 0) {
				dMax = oDefaults.MaxATMWithdrawal;
			}

			if (dMax > oDefaults.MaxATMWithdrawal) {
				dMax = oDefaults.MaxATMWithdrawal;
			}

			return dMax;
		}

	}

	#region EntityPropertyDescriptors.TaxiPassRedeemerAccountsPropertyDescriptor
	////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
	////    For example: 
	////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
	//public static partial class EntityPropertyDescriptors {
	//	public partial class TaxiPassRedeemerAccountsPropertyDescriptor : BaseEntityPropertyDescriptor {
	//    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
	//    public AdaptedPropertyDescriptor Age {
	//      get { return this.Get("Age"); }
	//    }
	//  }
	//} 
	#endregion

}
