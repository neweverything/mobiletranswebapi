﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Linq;
using System.Collections.Generic;
using System.Text;


using IdeaBlade.Persistence;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

using Csla.Validation;
using CabRideEngine.Utils;

using TaxiPassCommon;


namespace CabRideEngine {
	[Serializable]
	public sealed class Reservations : ReservationsDataRow {

		private bool mForceChargeRefetch = true;
		public static long? SetTaggedInvoiceNo;

		#region Constructors -- DO NOT MODIFY
		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new'
		private Reservations()
			: this(null) {
		}

		// Typed DataSet constructor (needed by framework) - DO NOT REMOVE
		public Reservations(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static Reservations Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      Reservations aReservations = pManager.CreateEntity<Reservations>();
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aReservations, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aReservations.AddToManager();
		//      return aReservations;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		// Add additional logic to your business object here...
		public static Reservations Create(PersistenceManager pPM, long pCustomerID) {
			Reservations oReservation = null;

			try {
				oReservation = (Reservations)pPM.CreateEntity(typeof(Reservations));

				// Using the IdeaBlade Id Generation technique
				//pPM.GenerateId(oReservation, Reservations.ReservationIDEntityColumn);
				oReservation.CustomerID = pCustomerID;
				oReservation.ReserveNo = TaxiPassCommon.StringUtils.GetUniqueKey(12);
				oReservation.AffiliateConfirmationCode = new Random().Next(100).ToString().PadLeft(2, '0');

				//turn off all notification options, only turn them on if a user books an ASAP ride
				oReservation.ASAPRide = false;
				oReservation.MaxPassengerWaitTime = -1;

				if (oReservation.Customer.BillTo > 0) {
					oReservation.BillTo = oReservation.Customer.BillTo;
				} else {
					if (oReservation.Customer.Accounts != null && oReservation.Customer.Accounts.CompanyBilled) {
						EntityList<Customer> oCustomers = Customer.GetCustomerAccountAdmins(pPM, oReservation.Customer.Accounts.AccountNo);
						if (oCustomers.Count > 0) {
							oReservation.BillTo = oCustomers[0].CustomerID;
						}
					} else {
						oReservation.BillTo = pCustomerID;
					}
				}

				oReservation.AddToManager();

			} catch (Exception ex) {
				throw ex;
			}
			return oReservation;
		}

		public void FillDefaultResInfo() {
			CustomerAddress oAddress = GetCustAddress(true);

			if (oAddress == null) {
				oAddress = GetCustAddress(false);
			}
			if (oAddress == null) {
				if (this.Customer.CustomerAddresses.Count > 0) {
					oAddress = this.Customer.CustomerAddresses[0];
				}
			}

			FillAddressInfo(oAddress);
			FillContactInfo();
			this.BillTo = Customer.BillTo;
			if (this.BillTo == 0) {
				this.BillTo = this.CustomerID;
			}

		}


		public override DateTime PickUpDate {
			get {
				return base.PickUpDate;
			}
			set {
				base.PickUpDate = value;
				if (this.Affiliate != null && Affiliate.TimeZone != null) {
					TimeZoneConverter oTimeZone = new TimeZoneConverter();
					this.UTCPUDateTime = oTimeZone.ToUniversalTime(Affiliate.TimeZone, value);
				}
			}
		}

		private void FillContactInfo() {
			this.HomePhone = this.Customer.HomePhone;
			this.WorkPhone = Customer.WorkPhone;
			this.CellPhone = Customer.CellPhone;
			this.EMail = Customer.EMail;
			this.Name = Customer.Name;
		}

		private void FillAddressInfo(CustomerAddress pAddress) {
			if (pAddress == null) {
				return;
			}
			this.Street = pAddress.Street;
			this.Suite = pAddress.Suite;
			this.City = pAddress.City;
			this.State = pAddress.State;
			this.Country = pAddress.Country;
			this.Zip = pAddress.ZIPCode;

		}

		private CustomerAddress GetCustAddress(bool pMailingAddress) {

			foreach (CustomerAddress oAddress in this.Customer.CustomerAddresses) {
				if ((pMailingAddress && Convert.ToBoolean(oAddress.IsMailingAddress)) || (!pMailingAddress && Convert.ToBoolean(oAddress.IsDefault))) {
					return oAddress;
				}
			}
			return null;
		}

		public override decimal? TotalCharge {
			get {
				return Math.Round(Convert.ToDecimal(base.TotalCharge), 2);
			}
			set {
				base.TotalCharge = Math.Round(Convert.ToDecimal(value), 2);
			}
		}

		public decimal GetTotalPrice {
			get {
				decimal total = 0.00M;
				if (mForceChargeRefetch) {
					this.Chargeses.ForceRefetch(MergeStrategy.OverwriteChanges);
				}
				foreach (Charges oCharge in this.Chargeses) {
					total += oCharge.Amount;
				}
				total = Math.Round(total, 2);

				if (total != base.TotalCharge) {
					TotalCharge = total;
				}
				return total;

			}
		}

		public decimal GetBasePrice {
			get {
				decimal basePrice = 0.00M;
				if (mForceChargeRefetch) {
					this.Chargeses.ForceRefetch(MergeStrategy.OverwriteChanges);
				}
				foreach (Charges oCharge in this.Chargeses) {
					if (oCharge.Description == ChargeTypes.BaseFee || oCharge.Description == ChargeTypes.AirportFee ||
							oCharge.Description == ChargeTypes.DropFee || oCharge.Description == ChargeTypes.OtherFee ||
							oCharge.Description == ChargeTypes.TaxiPassFee || oCharge.Description == ChargeTypes.VoucherFee) {
						basePrice += oCharge.Amount;
					}
				}
				// return baseFee;
				return Math.Round(basePrice, 2);
			}
		}

		public decimal GetServiceFee {
			get {
				decimal ServiceFee = 0.00M;
				foreach (Charges oCharge in this.Chargeses) {
					if (oCharge.Description == ChargeTypes.ServiceFee) {
						ServiceFee = oCharge.Amount;
					}
				}
				return Math.Round(ServiceFee, 2);
			}
		}

		public decimal GetDiscountFee {
			get {
				decimal Fee = 0.00M;
				foreach (Charges oCharge in this.Chargeses) {
					if (oCharge.Description == ChargeTypes.Discount) {
						Fee = oCharge.Amount;
					}
				}
				return Math.Round(Fee, 2);
			}
		}

		public decimal GetBookingFee {
			get {
				decimal BookingFee = 0.00M;
				foreach (Charges oCharge in this.Chargeses) {
					if (oCharge.Description == ChargeTypes.BookingFee) {
						BookingFee = oCharge.Amount;
					}
				}
				return Math.Round(BookingFee, 2);
			}
		}

		public string GetBasePriceText {
			get {
				return string.Format("{0:c}", GetBasePrice);
			}
		}

		public string GetServiceFeeText {
			get {
				return string.Format("{0:c}", GetServiceFee);
			}
		}

		public string GetDiscountFeeText {
			get {
				return string.Format("{0:c}", GetDiscountFee);
			}
		}

		public string AffiliateName {
			get {
				return this.Affiliate.Name;
			}
		}

		public Drivers Driver {
			get {
				if (this.DriverID.HasValue) {
					return Drivers.GetDriver(this.PersistenceManager, this.DriverID.Value);
				}
				return this.PersistenceManager.GetNullEntity<Drivers>();
			}
		}

		public string GetTotalChargeText {
			get {
				return string.Format("{0:c}", this.TotalCharge);
			}
		}

		public string GetBookingFeeText {
			get {
				decimal BookingFee = GetBookingFee;
				string sText = "$ Free";
				if (BookingFee != 0) {
					sText = string.Format("{0:c}", BookingFee);
				}
				return sText;
			}
		}

		public decimal GetOtherFees {
			get {
				return Math.Round(Convert.ToDecimal(TotalCharge) - GetBasePrice, 2);
			}
		}

		public string GetPickupInstructions {
			get {
				bool IsAirportPickup = false;
				foreach (Routing oRoute in this.Routings) {
					if (oRoute.Type == Routing_Types.PickUp) {
						if (oRoute.RoutingType == RoutingTypes.Airport) {
							IsAirportPickup = true;
						}
						break;
					}
				}
				if (IsAirportPickup && !String.IsNullOrEmpty(Affiliate.AirportNotes)) {
					return Affiliate.AirportNotes;
				} else {
					if (Affiliate.GeneralNotes == null) {
						return "";
					} else {
						return Affiliate.GeneralNotes;
					}
				}
			}
		}

		public override string CardNumber {
			get {
				return Utils.CreditCardUtils.DecryptCardNo(this.Customer, base.CardNumber);
			}

			set {
				// encrypt the data
				try {
					base.CardNumber = Utils.CreditCardUtils.EncryptCardNo(this.Customer, this.CardType, value);
				} catch (Exception ex) {
					base.CardNumber = value;
					throw ex;
				}
			}
		}

		public string FormattedCellPhone {
			get {
				return base.CellPhone.FormattedPhoneNumber();
			}
		}

		public bool IsCreditCardAuthorized {
			get {
				foreach (CardResults oResult in this.CardResultses) {
					if (oResult.TransType == VerisignTrxTypes.Authorization && oResult.Result == "0") {
						return true;
					}
				}
				return false;
			}
		}

		public bool IsCreditCardCharged {
			get {
				foreach (CardResults oResult in this.CardResultses) {
					if ((oResult.TransType == VerisignTrxTypes.DelayedCapture || oResult.TransType == VerisignTrxTypes.Sale) && oResult.Result == "0") {
						return true;
					}
				}
				return false;
			}
		}

		public string LastCreditCardAction {
			get {
				string sMsg = "";
				RdbQuery oQry = new RdbQuery(typeof(CardResults), CardResults.ReservationIDEntityColumn, EntityQueryOp.EQ, this.ReservationID);
				oQry.AddOrderBy(CardResults.CardResultIDEntityColumn, System.ComponentModel.ListSortDirection.Descending);
				CardResults oResult = this.PersistenceManager.GetEntity<CardResults>(oQry, QueryStrategy.Normal);
				if (!oResult.IsNullEntity) {
					sMsg = VerisignTrxTypes.GetTrxDescription(oResult.TransType) + " ";
					if (oResult.Result == "0") {
						if (oResult.CustomerNotified) {
							sMsg += "invoiced";
						} else {
							sMsg += "successful";
						}
					} else {
						sMsg += "failed: " + oResult.Message;
					}
				}
				return sMsg;
			}
		}

		public string LastCreditCardMessage {
			get {
				RdbQuery oQry = new RdbQuery(typeof(CardResults), CardResults.ReservationIDEntityColumn, EntityQueryOp.EQ, this.ReservationID);
				oQry.AddOrderBy(CardResults.CardResultIDEntityColumn, System.ComponentModel.ListSortDirection.Descending);
				CardResults oResult = this.PersistenceManager.GetEntity<CardResults>(oQry, QueryStrategy.Normal);
				if (!oResult.IsNullEntity) {
					return oResult.Message;
				} else {
					return "";
				}
			}
		}

		public void SetNotifiedCustomerForLastCreditCardAction(bool pCustomerNotified) {
			RdbQuery oQry = new RdbQuery(typeof(CardResults), CardResults.ReservationIDEntityColumn, EntityQueryOp.EQ, this.ReservationID);
			oQry.AddOrderBy(CardResults.CardResultIDEntityColumn, System.ComponentModel.ListSortDirection.Descending);
			CardResults oCardResult = this.PersistenceManager.GetEntity<CardResults>(oQry, QueryStrategy.Normal);
			if (!oCardResult.IsNullEntity) {
				oCardResult.CustomerNotified = pCustomerNotified;
				try {
					SaveResult oResult = oCardResult.Save();
					if (!oResult.Ok) {
						throw new Exception(oResult.Exception.Message.ToString());
					}
				} catch (Exception ex) {
					ErrorLog.ErrorRoutine(ex);
					throw new Exception("Error saving notification: " + ex.Message.ToString());
				}
			}
		}

		public CardResults GetAuthorizedRecord() {
			RdbQuery oQry = new RdbQuery(typeof(CardResults), CardResults.ReservationIDEntityColumn, EntityQueryOp.EQ, this.ReservationID);
			oQry.AddClause(CardResults.TransTypeEntityColumn, EntityQueryOp.EQ, VerisignTrxTypes.Authorization);
			oQry.AddClause(CardResults.ResultEntityColumn, EntityQueryOp.EQ, "0");
			return this.PersistenceManager.GetEntity<CardResults>(oQry);
		}

		public string PickUpInfo {
			get {
				foreach (Routing oRoute in this.Routings) {
					if (oRoute.Type == Routing_Types.PickUp) {
						return oRoute.FormattedAddress;
					}
				}
				return "";
			}
		}

		public string PickUpInfoShort {
			get {
				foreach (Routing oRoute in this.Routings) {
					if (oRoute.Type == Routing_Types.PickUp) {
						return oRoute.FormattedAddressShort;
					}
				}
				return "";
			}
		}

		public string PickUpInfoTextMsg {
			get {
				foreach (Routing oRoute in this.Routings) {
					if (oRoute.Type == Routing_Types.PickUp) {
						return oRoute.TextMsgAddress;
					}
				}
				return "";
			}
		}

		public string PickUpNote {
			get {
				foreach (Routing oRoute in this.Routings) {
					if (oRoute.Type == Routing_Types.PickUp) {
						return oRoute.Notes;
					}
				}
				return "";
			}
		}

		public string PickUpAirport {
			get {
				foreach (Routing oRoute in this.Routings) {
					if (oRoute.Type == Routing_Types.PickUp) {
						return oRoute.Airport;
					}
				}
				return "";
			}
		}

		public string PickUpFlightNumber {
			get {
				foreach (Routing oRoute in this.Routings) {
					if (oRoute.Type == Routing_Types.PickUp) {
						return oRoute.FlightNo;
					}
				}
				return "";
			}
		}

		public string DropOffInfo {
			get {
				foreach (Routing oRoute in this.Routings) {
					if (oRoute.Type == Routing_Types.DropOff) {
						return oRoute.FormattedAddress;
					}
				}
				return "";
			}
		}

		public string DropOffInfoShort {
			get {
				foreach (Routing oRoute in this.Routings) {
					if (oRoute.Type == Routing_Types.DropOff) {
						return oRoute.FormattedAddressShort;
					}
				}
				return "";
			}
		}

		public Routing DropOff {
			get {
				foreach (Routing oRoute in this.Routings) {
					if (oRoute.Type == Routing_Types.DropOff) {
						return oRoute;
					}
				}
				return this.PersistenceManager.GetNullEntity<Routing>();
			}
		}

		public Routing PickUp {
			get {
				foreach (Routing oRoute in this.Routings) {
					if (oRoute.Type == Routing_Types.PickUp) {
						return oRoute;
					}
				}
				return this.PersistenceManager.GetNullEntity<Routing>();
			}
		}


		public Address PickUpAddress {
			get {
				return PickUp.ToAddress();
			}
		}

		public string FormattedCardExpiration {
			get {
				string sMonth = this.CardExpiration.Value.Month.ToString().PadLeft(2, '0');
				string sYear = this.CardExpiration.Value.Year.ToString();
				return sMonth + "/" + sYear;
			}
		}

		public DateTime GetCreatedDate {
			get {
				DateTime createdDate = ModifiedDate.Value;
				if (RowVersion != 0) {
					ReservationsAudit oAudit = ReservationsAudit.GetFirstReserveRecord(this.PersistenceManager, this.ReservationID);
					if (!oAudit.IsNullEntity && oAudit.ModifiedDate.HasValue) {
						createdDate = oAudit.ModifiedDate.Value;
					}
				}
				return createdDate;
			}
		}

		public string GetCreatedBy {
			get {
				string createdBy;
				if (RowVersion == 0) {
					createdBy = ModifiedBy;
				} else {
					createdBy = ReservationsAudit.GetFirstReserveRecord(this.PersistenceManager, this.ReservationID).ModifiedBy;
				}
				return createdBy;
			}
		}

		public DateTime? GetLastModifiedDate {
			get {
				if (RowVersion == 0) {
					return null;
				} else {
					return ModifiedDate.Value;
				}
			}
		}

		public string GetLastModifiedBy {
			get {
				if (RowVersion == 0) {
					return "";
				} else {
					return ModifiedBy;
				}
			}
		}

		public string UsePhoneNo {
			get {
				if (!string.IsNullOrEmpty(this.WorkPhone)) {
					return this.WorkPhone;
				}
				if (!string.IsNullOrEmpty(this.CellPhone)) {
					return this.CellPhone;
				}
				if (!string.IsNullOrEmpty(this.HomePhone)) {
					return this.HomePhone;
				}
				return "";
			}
		}

		public static Reservations GetReservation(PersistenceManager pPM, long pReserveID) {
			RdbQuery qry = new RdbQuery(typeof(Reservations), Reservations.ReservationIDEntityColumn, EntityQueryOp.EQ, pReserveID);
			return pPM.GetEntity<Reservations>(qry);
		}

		public static Reservations GetReservation(PersistenceManager pPM, string pReserveNo) {
			RdbQuery qry = new RdbQuery(typeof(Reservations), Reservations.ReserveNoEntityColumn, EntityQueryOp.EQ, pReserveNo);
			return pPM.GetEntity<Reservations>(qry);
		}

		public static Reservations GetReservationByReference(PersistenceManager pPM, string pReference) {
			RdbQuery qry = new RdbQuery(typeof(Reservations), Reservations.ReferenceEntityColumn, EntityQueryOp.EQ, pReference);
			return pPM.GetEntity<Reservations>(qry);
		}

		public static Reservations GetReservation(PersistenceManager pPM, string pExternalReserveID, string pSalesSystem) {
			RdbQuery qry = new RdbQuery(typeof(Reservations), Reservations.ExternalReferenceIDEntityColumn, EntityQueryOp.EQ, pExternalReserveID);
			qry.AddClause(Reservations.SalesSystemEntityColumn, EntityQueryOp.EQ, pSalesSystem);
			return pPM.GetEntity<Reservations>(qry);
		}

		public static Reservations GetReservation(PersistenceManager pPM, string pExternalReserveID, string pSalesSystem, string pAccountNo) {
			RdbQuery qry = new RdbQuery(typeof(Reservations), Reservations.ExternalReferenceIDEntityColumn, EntityQueryOp.EQ, pExternalReserveID);
			qry.AddClause(Reservations.SalesSystemEntityColumn, EntityQueryOp.EQ, pSalesSystem);
			qry.AddClause(Reservations.AccountNoEntityColumn, EntityQueryOp.EQ, pAccountNo);
			return pPM.GetEntity<Reservations>(qry);
		}

		public static EntityList<Reservations> GetPastTrips(Customer pCustomer, bool pForceRefetch) {
			RdbQuery qry = new RdbQuery(typeof(Reservations), Reservations.PickUpDateEntityColumn, EntityQueryOp.LT, DateTime.Today);
			qry.AddClause(Reservations.CustomerIDEntityColumn, EntityQueryOp.EQ, pCustomer.CustomerID);
			qry.AddSpan(EntityRelations.Reservations_Routing);
			qry.AddSpan(EntityRelations.Reservations_Charges);
			qry.AddOrderBy(Reservations.PickUpDateEntityColumn, System.ComponentModel.ListSortDirection.Descending);
			EntityList<Reservations> oRes;
			if (pForceRefetch) {
				oRes = pCustomer.PersistenceManager.GetEntities<Reservations>(qry, QueryStrategy.DataSourceThenCache);
			} else {
				oRes = pCustomer.PersistenceManager.GetEntities<Reservations>(qry);
			}
			return oRes;

		}

		// ***************************************************************************************
		// return a list of reservations for an account within a date range
		// ***************************************************************************************
		public static EntityList<Reservations> GetReservationsByAccount(PersistenceManager pPM, string AccountNo, DateTime pStartDate, DateTime pEndDate) {
			RdbQuery qry = new RdbQuery(typeof(Reservations), Reservations.AccountNoEntityColumn, EntityQueryOp.EQ, AccountNo);
			qry.AddClause(Reservations.PickUpDateEntityColumn, EntityQueryOp.GE, pStartDate);
			qry.AddClause(Reservations.PickUpDateEntityColumn, EntityQueryOp.LE, pEndDate);
			qry.AddOrderBy(Reservations.PickUpDateEntityColumn, System.ComponentModel.ListSortDirection.Ascending);
			return pPM.GetEntities<Reservations>(qry);
		}

		public static EntityList<Reservations> GetPendingTrips(Customer pCustomer, bool pForceRefetch) {
			RdbQuery qry = new RdbQuery(typeof(Reservations), Reservations.PickUpDateEntityColumn, EntityQueryOp.GE, DateTime.Today);
			qry.AddClause(Reservations.CustomerIDEntityColumn, EntityQueryOp.EQ, pCustomer.CustomerID);
			qry.AddSpan(EntityRelations.Reservations_Routing);
			qry.AddOrderBy(Reservations.PickUpDateEntityColumn, System.ComponentModel.ListSortDirection.Ascending);
			EntityList<Reservations> oRes;
			if (pForceRefetch) {
				oRes = pCustomer.PersistenceManager.GetEntities<Reservations>(qry, QueryStrategy.DataSourceThenCache);
			} else {
				oRes = pCustomer.PersistenceManager.GetEntities<Reservations>(qry);
			}
			return oRes;
		}


		public override bool Cancelled {
			get {
				return base.Cancelled;
			}
			set {
				if (value) {
					//Status = "Cancelled";
				} else {
					//Status = "Changed";
				}
				AffiliateConfirmation = "";
				base.Cancelled = value;
			}
		}

		public bool ForceChargeRefetch {
			get {
				return mForceChargeRefetch;
			}
			set {
				mForceChargeRefetch = value;
			}
		}

		public string CustomerStatus {
			get {
				string myStatus = "Booked";
				if (this.Cancelled) {
					myStatus = "Cancelled";
				}
				//if (string.IsNullOrEmpty(this.AffiliateConfirmation)) {
				//    myStatus += " and unconfirmed";
				//} else {
				//    myStatus += " and confirmed";
				//}

				//if (IsCreditCardCharged) {
				//    myStatus = "Invoiced";
				//}
				return myStatus;
			}
		}


		public string DispatchRideStatus {
			get {
				string myStatus = Status;
				if (string.IsNullOrEmpty(this.AffiliateConfirmation) && string.IsNullOrEmpty(this.AffiliateCancelConfirmation)) {
					if (!myStatus.ToLower().Contains("confirmed")) {
						myStatus += " and unconfirmed";
					}
				} else {
					if (!myStatus.ToLower().Contains("confirmed")) {
						myStatus += " and confirmed";
					}
				}

				//if (IsCreditCardCharged) {
				//    myStatus = "Invoiced";
				//}
				return myStatus;
			}
		}


		public static EntityList<Reservations> GetReservationsByDate(PersistenceManager pPM, DateTime pStartDate, DateTime pEndDate, bool pIncludeTestTransactions) {
			return GetReservationsByDate(pPM, pStartDate, pEndDate, pIncludeTestTransactions, null);
		}

		public static EntityList<Reservations> GetReservationsByDate(PersistenceManager pPM, DateTime pStartDate, DateTime pEndDate, bool pIncludeTestTransactions, long? pMarketID) {
			RdbQuery qry = new RdbQuery(typeof(Reservations), Reservations.PickUpDateEntityColumn, EntityQueryOp.GE, pStartDate);
			qry.AddClause(Reservations.PickUpDateEntityColumn, EntityQueryOp.LT, pEndDate);
			if (!pIncludeTestTransactions) {
				Affiliate oAffiliate = Affiliate.GetTestAffiliate(pPM);
				qry.AddClause(Reservations.AffiliateIDEntityColumn, EntityQueryOp.NE, oAffiliate.AffiliateID);
			}
			qry.AddSpan(EntityRelations.Reservations_CardResults);
			if (pMarketID != null) {
				qry.AddClause(Reservations.MarketIDEntityColumn, EntityQueryOp.EQ, pMarketID);
			}

			qry.AddOrderBy(Reservations.UTCPUDateTimeEntityColumn);

			return pPM.GetEntities<Reservations>(qry, QueryStrategy.DataSourceThenCache);
		}

		public override DateTime? UTCPUDateTime {
			get {
				if (base.UTCPUDateTime == null) {
					try {
						if (this.Affiliate != null && Affiliate.TimeZone != null) {
							TimeZoneConverter oTimeZone = new TimeZoneConverter();
							this.UTCPUDateTime = oTimeZone.ToUniversalTime(Affiliate.TimeZone, PickUpDate);
							this.Save();
						}
					} catch {
					}
				}
				return base.UTCPUDateTime;
			}
			set {
				base.UTCPUDateTime = value;
			}
		}

		public string QuickCode {
			get {
				string code = this.Affiliate.CityQuickCode;
				if (RunType.ToLower().Contains("airport")) {
					code = this.Affiliate.AirportQuickCode;
				}
				return code;
			}
		}

		public ReservationsAudit PreviousRecord {
			get {
				RdbQuery qry = new RdbQuery(typeof(ReservationsAudit), ReservationsAudit.ReservationIDEntityColumn, EntityQueryOp.EQ, this.ReservationID);
				qry.AddClause(ReservationsAudit.RowVersionEntityColumn, EntityQueryOp.EQ, this.RowVersion - 1);
				qry.AddOrderBy(ReservationsAudit.ReserveHistoryIDEntityColumn, System.ComponentModel.ListSortDirection.Descending);
				return this.PersistenceManager.GetEntity<ReservationsAudit>(qry);
			}
		}

		public string GetMarket {
			get {
				string sMarket = "";
				try {
					if (MarketID == null) {
						sMarket = this.Affiliate.AffiliateCoverageZoneses[0].CoverageZones.Markets.Market;
					} else {
						sMarket = this.Markets.Market;
					}
				} catch {
				}
				return sMarket;
			}
		}

		public decimal? BaseProfit {
			get {
				if (AffiliateBaseCharge == 0) {
					return null;
				}
				return GetBasePrice - AffiliateBaseCharge;
			}
		}

		public decimal? TotalProfit {
			get {
				if (AffiliateTotal == 0) {
					return null;
				}
				return GetTotalPrice - AffiliateTotal;
			}
		}

		public bool HasInternalNotes {
			get {
				return (!string.IsNullOrEmpty(this.InternalNotes));
			}
		}

		public bool HasRatingNotes {
			get {
				return (!string.IsNullOrEmpty(this.RatingNotes));
			}
		}

		public override SaveResult Save(string sModifiedBy) {
			if (MarketID == null) {
				if (this.Affiliate.AffiliateCoverageZoneses.Count > 0) {
					this.MarketID = this.Affiliate.AffiliateCoverageZoneses[0].CoverageZones.MarketID;
				}
			}
			if (string.IsNullOrEmpty(Status)) {
				Status = "New";
			}
			if (Cancelled) {
				//Status = "Cancelled";
			}
			if (AutoCancelled.GetValueOrDefault(false)) {
				Status = "Auto Cancelled";
				AffiliateConfirmation = "Auto Confirmed";
			}

			if (this.UTCPUDateTime == null) {
				if (this.Affiliate != null && Affiliate.TimeZone != null) {
					TimeZoneConverter oTimeZone = new TimeZoneConverter();
					this.UTCPUDateTime = oTimeZone.ToUniversalTime(Affiliate.TimeZone, this.PickUpDate);
				}
			}
			if (RowState == DataRowState.Added) {
				CheckASAPRide();
			}
			return base.Save(sModifiedBy);
		}

		public string FullRunType {
			get {
				string sPURunType = "";
				string sDORunType = "";
				foreach (Routing oRoute in this.Routings) {
					if (oRoute.Type == Routing_Types.PickUp) {
						if (string.IsNullOrEmpty(sPURunType)) {
							if (oRoute.RoutingType == RoutingTypes.Airport) {
								sPURunType = "Airport";
							} else if (oRoute.RoutingType == RoutingTypes.Address) {
								sPURunType = "Address";
							} else if (oRoute.RoutingType == RoutingTypes.Hotel) {
								sPURunType = "Hotel";
							} else {
								sPURunType = "Unknown";
							}
						}
					} else {
						if (oRoute.RoutingType == RoutingTypes.Airport) {
							sDORunType = "Airport";
						} else if (oRoute.RoutingType == RoutingTypes.Address) {
							sDORunType = "Address";
						} else if (oRoute.RoutingType == RoutingTypes.Hotel) {
							sDORunType = "Hotel";
						} else {
							sDORunType = "Unknown";
						}
					}
				}
				return sPURunType + " to " + sDORunType;
			}
		}

		public static EntityList<Reservations> GetUnconfirmedReservations(PersistenceManager pPM, DateTime pStart, DateTime pEnd) {
			RdbQuery qry = new RdbQuery(typeof(Reservations), Reservations.UTCPUDateTimeEntityColumn, EntityQueryOp.GE, pStart);
			qry.AddClause(Reservations.UTCPUDateTimeEntityColumn, EntityQueryOp.LE, pEnd);
			qry.AddClause(Reservations.AffiliateConfirmationEntityColumn, EntityQueryOp.IsNull);
			qry.AddClause(Reservations.AffiliateCancelConfirmationEntityColumn, EntityQueryOp.IsNull);
			//qry.AddClause(Reservations.AffiliateConfirmationEntityColumn, EntityQueryOp.EQ, "");
			//qry.AddOperator(EntityBooleanOp.Or);
			return pPM.GetEntities<Reservations>(qry, QueryStrategy.DataSourceThenCache);
		}


		public static EntityList<Reservations> GetRideRatings(PersistenceManager pPM, DateTime pStartDate, DateTime pEndDate, ReservationRatings pRating) {
			RdbQuery oQry = new RdbQuery(typeof(Reservations), Reservations.PickUpDateEntityColumn, EntityQueryOp.GE, pStartDate);
			oQry.AddClause(Reservations.PickUpDateEntityColumn, EntityQueryOp.LE, pEndDate);
			if (pRating == ReservationRatings.RatingsWithComments) {
				oQry.AddClause(Reservations.RatingNotesEntityColumn, EntityQueryOp.IsNotNull);
			}
			oQry.AddClause(Reservations.RatingEntityColumn, EntityQueryOp.GT, 0);
			return pPM.GetEntities<Reservations>(oQry, QueryStrategy.DataSourceThenCache);
		}

		public string DueTimeText {
			get {
				TimeSpan oTime = UTCPUDateTime.Value.Subtract(DateTime.Now.ToUniversalTime());
				string sDue = "";
				if (oTime.Days > 0) {
					sDue = oTime.Days.ToString() + " days ";
				}
				sDue += oTime.Hours.ToString() + " hours ";
				if (oTime.Minutes > 0) {
					sDue += oTime.Minutes.ToString() + " minutes";
				}
				return sDue.Trim();
			}
		}

		public string PickUpTimeText {
			get {
				if (this.AffiliateEstimatedPUTime.HasValue) {
					return this.AffiliateEstimatedPUTime.Value.ToString("hh:mm tt");
				}
				return this.PickUpDate.ToString("hh:mm tt");
			}
		}



		private bool mInvoiceTag;
		/// <summary>
		/// In order to use this field, you must set the SetTaggedInvoiceNo to the correct InvoiceID value
		/// </summary>
		public bool InvoiceTag {
			get {
				if (this.InvoiceID == null) {
					return false;
				}
				return mInvoiceTag || this.InvoiceID != 0;
			}
			set {
				if (value) {
					this.InvoiceID = SetTaggedInvoiceNo;
				} else {
					this.InvoiceID = null;
				}
				mInvoiceTag = value;
			}
		}


		public void GenerateAffiliateConfirmationCode() {
			this.AffiliateConfirmationCode = new Random().Next(100).ToString().PadLeft(2, '0');
		}

		public string AffiliateConfirmationDelta {
			get {
				if (this.ConfirmationDate == null) {
					return "";
				}
				DateTime created = GetCreatedDate;
				TimeSpan oDiff = this.ConfirmationDate.Value.Subtract(created);
				string sText = "";
				if (oDiff.Days > 0) {
					sText += oDiff.Days.ToString() + "d ";
				}
				if (oDiff.Hours > 0) {
					sText += oDiff.Hours.ToString() + "h ";
				}
				if (oDiff.Minutes > 0) {
					sText += oDiff.Minutes.ToString() + "m ";
				}
				if (string.IsNullOrEmpty(sText)) {
					sText += oDiff.Seconds.ToString() + " secs";
				}
				return sText.Trim();
			}
		}

		public void CheckASAPRide() {

			SystemDefaults oDefaults = this.PersistenceManager.GetEntities<SystemDefaults>()[0];
			int minHours = oDefaults.MinPickUpTime;
			if (this.Affiliate != null && this.Affiliate.MinPUTime > 0) {
				minHours = this.Affiliate.MinPUTime;
			}

			if (TaxiPassCommon.DateUtils.IsDateTimeWithin(this.UTCPUDateTime.Value, minHours)) {
				if (DateTime.Now.ToUniversalTime().AddMinutes(oDefaults.ASAPThreshold) > this.UTCPUDateTime) {

					TimeSpan oTime = UTCPUDateTime.Value.Subtract(DateTime.Now.ToUniversalTime());
					MaxPassengerWaitTime = 0;
					if (oTime.Minutes >= 0 && oTime.Minutes <= 15) {
						MaxPassengerWaitTime = 15;
					}

				}
			}

			if (IsRideASAP()) {
				this.ASAPRide = true;
			}
		}

		private bool IsRideASAP() {

			TimeSpan span = this.UTCPUDateTime.Value.Subtract(DateTime.Now.ToUniversalTime());
			SystemDefaults oDefaults = this.PersistenceManager.GetEntities<SystemDefaults>()[0];
			int minHours = oDefaults.MinPickUpTime;
			if (this.Affiliate.MinPUTime > 0) {
				minHours = this.Affiliate.MinPUTime;
			}

			return (span.Hours < minHours && span.Days <= 0);
		}


		public static EntityList<Reservations> GetReservationsToInvoice(Invoices pInvoice, DateTime pStartDate, DateTime pEndDate) {
			PersistenceManager oPM = pInvoice.PersistenceManager;
			RdbQuery qry = new RdbQuery(typeof(Reservations), Reservations.PickUpDateEntityColumn, EntityQueryOp.GE, pStartDate);
			qry.AddClause(Reservations.PickUpDateEntityColumn, EntityQueryOp.LT, pEndDate);
			qry.AddClause(Reservations.AffiliateIDEntityColumn, EntityQueryOp.EQ, pInvoice.AffiliateID);
			qry.AddClause(Reservations.InvoiceIDEntityColumn, EntityQueryOp.EQ, pInvoice.InvoiceID);
			qry.AddClause(Reservations.InvoiceIDEntityColumn, EntityQueryOp.IsNull);
			qry.AddOperator(EntityBooleanOp.Or);

			qry.AddOrderBy(Reservations.UTCPUDateTimeEntityColumn);

			return oPM.GetEntities<Reservations>(qry, QueryStrategy.DataSourceThenCache);
		}

		public static EntityList<Reservations> GetReservationsToTxtMsgCustomers(PersistenceManager pPM, DateTime pStart, DateTime pEnd) {
			RdbQuery qry = new RdbQuery(typeof(Reservations), Reservations.UTCPUDateTimeEntityColumn, EntityQueryOp.GE, pStart);
			qry.AddClause(Reservations.UTCPUDateTimeEntityColumn, EntityQueryOp.LE, pEnd);
			qry.AddClause(Reservations.TextMsgSentEntityColumn, EntityQueryOp.IsNull);
			qry.AddClause(Reservations.CancelledEntityColumn, EntityQueryOp.EQ, false);
			//qry.AddClause(Reservations.TextMsgSentEntityColumn, EntityQueryOp.LT, new DateTime(2007,01,01));  // date known that we don't have this functionality
			//qry.AddOperator(EntityBooleanOp.Or);
			return pPM.GetEntities<Reservations>(qry, QueryStrategy.DataSourceThenCache);
		}

		public static EntityList<Reservations> GetReservationsByDateByAffiliate(PersistenceManager pPM, long pAffiliateID, DateTime pStartDate, DateTime pEndDate, AffiliateStatus pStatus, QueryStrategy pQueryStrategy, System.ComponentModel.ListSortDirection pSortDirection) {
			return GetReservationsByDateByAffiliate(pPM, pAffiliateID, "", pStartDate, pEndDate, pStatus, pQueryStrategy, pSortDirection);
		}

		public static EntityList<Reservations> GetReservationsByDateByAffiliate(PersistenceManager pPM, long pAffiliateID, string pMemberID, DateTime pStartDate, DateTime pEndDate, AffiliateStatus pStatus, QueryStrategy pQueryStrategy, System.ComponentModel.ListSortDirection pSortDirection) {
			RdbQuery qry = new RdbQuery(typeof(Reservations), Reservations.PickUpDateEntityColumn, EntityQueryOp.GE, pStartDate);
			qry.AddClause(PickUpDateEntityColumn, EntityQueryOp.LE, pEndDate);
			qry.AddClause(Dispatch.AffiliateIDEntityColumn, EntityQueryOp.EQ, pAffiliateID);

			if (!pMemberID.IsNullOrEmpty()) {
				qry.AddClause(EmployeeNumberEntityColumn, EntityQueryOp.EQ, pMemberID);
			}

			switch (pStatus) {
				case AffiliateStatus.unconfirmedRides:
					qry.AddClause(Dispatch.AffiliateConfirmationEntityColumn, EntityQueryOp.IsNull, true);
					qry.AddClause(Dispatch.AffiliateConfirmationEntityColumn, EntityQueryOp.EQ, "");
					qry.AddOperator(EntityBooleanOp.Or);
					qry.AddClause(Dispatch.AffiliateCancelConfirmationEntityColumn, EntityQueryOp.IsNull, true);
					qry.AddClause(Dispatch.AffiliateCancelConfirmationEntityColumn, EntityQueryOp.EQ, "");
					qry.AddOperator(EntityBooleanOp.Or);
					break;
				case AffiliateStatus.confirmedRides:
					qry.AddClause(CabRideEngine.Dispatch.AffiliateConfirmationEntityColumn, EntityQueryOp.NE, "");
					break;
			}
			qry.AddClause(Reservations.CancelledEntityColumn, EntityQueryOp.EQ, false);
			qry.AddOrderBy(Dispatch.UTCPUDateTimeEntityColumn, pSortDirection);
			return pPM.GetEntities<Reservations>(qry, pQueryStrategy);
		}


		public static EntityList<Reservations> GetReservationsByDateByAffiliate(Affiliate pAffiliate, DateTime pStartDate, DateTime pEndDate, AffiliateStatus pStatus, QueryStrategy pQueryStrategy, System.ComponentModel.ListSortDirection pSortDirection) {
			return GetReservationsByDateByAffiliate(pAffiliate, "", pStartDate, pEndDate, pStatus, pQueryStrategy, pSortDirection);
		}

		public static EntityList<Reservations> GetReservationsByDateByAffiliate(Affiliate pAffiliate, string pMemberID, DateTime pStartDate, DateTime pEndDate, AffiliateStatus pStatus, QueryStrategy pQueryStrategy, System.ComponentModel.ListSortDirection pSortDirection) {
			RdbQuery qry = new RdbQuery(typeof(Reservations), Reservations.PickUpDateEntityColumn, EntityQueryOp.GE, pStartDate);
			qry.AddClause(PickUpDateEntityColumn, EntityQueryOp.LE, pEndDate);
			qry.AddClause(Dispatch.AffiliateIDEntityColumn, EntityQueryOp.EQ, pAffiliate.AffiliateID);

			if (!pMemberID.IsNullOrEmpty()) {
				qry.AddClause(EmployeeNumberEntityColumn, EntityQueryOp.EQ, pMemberID);
			}

			switch (pStatus) {
				case AffiliateStatus.unconfirmedRides:
					qry.AddClause(Dispatch.AffiliateConfirmationEntityColumn, EntityQueryOp.IsNull, true);
					qry.AddClause(Dispatch.AffiliateConfirmationEntityColumn, EntityQueryOp.EQ, "");
					qry.AddOperator(EntityBooleanOp.Or);
					qry.AddClause(Dispatch.AffiliateCancelConfirmationEntityColumn, EntityQueryOp.IsNull, true);
					qry.AddClause(Dispatch.AffiliateCancelConfirmationEntityColumn, EntityQueryOp.EQ, "");
					qry.AddOperator(EntityBooleanOp.Or);
					break;
				case AffiliateStatus.confirmedRides:
					qry.AddClause(CabRideEngine.Dispatch.AffiliateConfirmationEntityColumn, EntityQueryOp.NE, "");
					break;
			}
			qry.AddClause(Reservations.CancelledEntityColumn, EntityQueryOp.EQ, false);
			qry.AddOrderBy(Dispatch.UTCPUDateTimeEntityColumn, pSortDirection);
			return pAffiliate.PersistenceManager.GetEntities<Reservations>(qry, pQueryStrategy);
		}

		public static EntityList<Reservations> GetReservationsByDateBySalesRep(SalesReps pSalesRep, DateTime pStartDate, DateTime pEndDate, AffiliateStatus pStatus, QueryStrategy pQueryStrategy, System.ComponentModel.ListSortDirection pSortDirection) {
			return GetReservationsByDateBySalesRep(pSalesRep, "", pStartDate, pEndDate, pStatus, pQueryStrategy, pSortDirection);
		}

		public static EntityList<Reservations> GetReservationsByDateBySalesRep(SalesReps pSalesRep, string pMemberID, DateTime pStartDate, DateTime pEndDate, AffiliateStatus pStatus, QueryStrategy pQueryStrategy, System.ComponentModel.ListSortDirection pSortDirection) {
			RdbQuery qry = new RdbQuery(typeof(Reservations), Reservations.PickUpDateEntityColumn, EntityQueryOp.GE, pStartDate);
			qry.AddClause(PickUpDateEntityColumn, EntityQueryOp.LE, pEndDate);

			List<long> affiliateIDs = (from p in pSalesRep.SalesRepAffiliateses
									   select p.AffiliateID).ToList();
			qry.AddClause(Reservations.AffiliateIDEntityColumn, EntityQueryOp.In, affiliateIDs);

			if (!pMemberID.IsNullOrEmpty()) {
				qry.AddClause(EmployeeNumberEntityColumn, EntityQueryOp.EQ, pMemberID);
			}

			switch (pStatus) {
				case AffiliateStatus.unconfirmedRides:
					qry.AddClause(Reservations.AffiliateConfirmationEntityColumn, EntityQueryOp.IsNull, true);
					qry.AddClause(Reservations.AffiliateConfirmationEntityColumn, EntityQueryOp.EQ, "");
					qry.AddOperator(EntityBooleanOp.Or);
					qry.AddClause(Reservations.AffiliateCancelConfirmationEntityColumn, EntityQueryOp.IsNull, true);
					qry.AddClause(Reservations.AffiliateCancelConfirmationEntityColumn, EntityQueryOp.EQ, "");
					qry.AddOperator(EntityBooleanOp.Or);
					break;
				case AffiliateStatus.confirmedRides:
					qry.AddClause(Reservations.AffiliateConfirmationEntityColumn, EntityQueryOp.NE, "");
					break;
			}
			qry.AddOrderBy(Reservations.UTCPUDateTimeEntityColumn, pSortDirection);
			return pSalesRep.PersistenceManager.GetEntities<Reservations>(qry, pQueryStrategy);
		}

		public static EntityList<Reservations> GetReservationsBySalesRep(SalesReps pSalesRep, string pReserveNo) {
			if (pReserveNo.IsNullOrEmpty()) {
				throw new Exception("Reserve No is required");
			}

			List<long> affiliateIDs = (from p in pSalesRep.SalesRepAffiliateses
									   select p.AffiliateID).ToList();
			RdbQuery qry = new RdbQuery(typeof(Reservations), Reservations.AffiliateIDEntityColumn, EntityQueryOp.In, affiliateIDs);
			qry.AddClause(Reservations.ReserveNoEntityColumn, EntityQueryOp.EQ, pReserveNo);
			qry.AddClause(Reservations.ExternalReferenceIDEntityColumn, EntityQueryOp.EQ, pReserveNo);
			qry.AddOperator(EntityBooleanOp.Or);
			qry.AddOrderBy(Dispatch.UTCPUDateTimeEntityColumn, System.ComponentModel.ListSortDirection.Ascending);
			return pSalesRep.PersistenceManager.GetEntities<Reservations>(qry, QueryStrategy.DataSourceThenCache);
		}

		public static EntityList<Reservations> GetReservationsByAffiliate(PersistenceManager pPM, long pAffiliateID, string pReserveNo) {
			if (pReserveNo.IsNullOrEmpty()) {
				throw new Exception("Reserve No is required");
			}

			RdbQuery qry = new RdbQuery(typeof(Reservations), Reservations.AffiliateIDEntityColumn, EntityQueryOp.EQ, pAffiliateID);
			qry.AddClause(Reservations.ReserveNoEntityColumn, EntityQueryOp.EQ, pReserveNo);
			qry.AddClause(Reservations.ExternalReferenceIDEntityColumn, EntityQueryOp.EQ, pReserveNo);
			qry.AddOperator(EntityBooleanOp.Or);
			qry.AddOrderBy(Dispatch.UTCPUDateTimeEntityColumn, System.ComponentModel.ListSortDirection.Ascending);
			return pPM.GetEntities<Reservations>(qry, QueryStrategy.DataSourceThenCache);
		}

		public static EntityList<Reservations> GetReservationsByAffiliate(Affiliate pAffiliate, string pReserveNo) {
			if (pReserveNo.IsNullOrEmpty()) {
				throw new Exception("Reserve No is required");
			}

			RdbQuery qry = new RdbQuery(typeof(Reservations), Reservations.AffiliateIDEntityColumn, EntityQueryOp.EQ, pAffiliate);
			qry.AddClause(Reservations.ReserveNoEntityColumn, EntityQueryOp.EQ, pReserveNo);
			qry.AddClause(Reservations.ExternalReferenceIDEntityColumn, EntityQueryOp.EQ, pReserveNo);
			qry.AddOperator(EntityBooleanOp.Or);
			qry.AddOrderBy(Dispatch.UTCPUDateTimeEntityColumn, System.ComponentModel.ListSortDirection.Ascending);
			return pAffiliate.PersistenceManager.GetEntities<Reservations>(qry, QueryStrategy.DataSourceThenCache);
		}

		public static EntityList<Reservations> GetReservationsByDriver(Drivers pDriver, DateTime pStartDate, DateTime pEndDate, QueryStrategy pQueryStrategy) {
			RdbQuery qry = new RdbQuery(typeof(Reservations), Reservations.PickUpDateEntityColumn, EntityQueryOp.GE, pStartDate);
			qry.AddClause(PickUpDateEntityColumn, EntityQueryOp.LE, pEndDate);
			qry.AddClause(Reservations.DriverIDEntityColumn, EntityQueryOp.EQ, pDriver.DriverID);
			qry.AddOrderBy(Reservations.PickUpDateEntityColumn);
			return pDriver.PersistenceManager.GetEntities<Reservations>(qry, pQueryStrategy);
		}

		// *******************************************************************************************
		// Get a list of phones, pickup/drop offs for a Customer
		// *******************************************************************************************
		public static Entity[] GetFutureReservationByCustomerPhone(PersistenceManager pPM, long pCustomerID) {
			StringBuilder sql = new StringBuilder();
			sql.Append("SELECT Reservations.PickUpDate ");
			sql.Append(" , Pickup.Street as PickupStreet ");
			sql.Append(" , DropOff.Street as DropOffStreet ");
			sql.Append(" , CustomerPhones.TaxiPassToken ");
			sql.Append(" , Reservations.ReservationID ");

			sql.Append(" FROM ReservationRef ");

			sql.Append(" INNER JOIN Reservations ON reservationref.reservationid = Reservations.ReservationID ");
			sql.Append(" INNER JOIN Routing Pickup on Reservations.ReservationID = Pickup.ReservationID ");
			sql.Append(" and Pickup.Type = 'P' ");
			sql.Append(" INNER JOIN Routing DropOff on Reservations.ReservationID = DropOff.ReservationID ");
			sql.Append(" and DropOff.Type = 'D' ");

			sql.Append(" INNER JOIN CustomerPhones ON ReservationRef.TableID = CustomerPhones.CustomerPhoneID ");
			sql.Append(" AND Tablename = 'CustomerPhones' ");

			sql.AppendFormat(" WHERE CustomerPhones.CustomerID = '{0}' ", pCustomerID);
			sql.Append(" and Reservations.PickUpDate > GETDATE() ");
			sql.Append(" and Reservations.Status <> 'Cancelled'");

			return DynamicEntities.DynamicDBCall(pPM, sql.ToString());
		}


		public static EntityList<Reservations> AutoCancelReservations(PersistenceManager pPM) {

			SystemDefaults oDefaults = SystemDefaults.GetDefaults(pPM);
			if (!oDefaults.AutoCancelRide) {
				return new EntityList<Reservations>();
			}

			//make sure we get all rides
			int duration = oDefaults.AutoCancelASAPDuration;
			if (oDefaults.AutoCancelNonASAPDuration > duration) {
				duration = oDefaults.AutoCancelNonASAPDuration;
			}
			if (oDefaults.AutoCancelMinRideDuration > duration) {
				duration = oDefaults.AutoCancelMinRideDuration;
			}

			duration = (duration / 60) + 1;

			DateTime utcDate = DateTime.Now.ToUniversalTime();
			DateTime dStartDate = utcDate.AddHours(-duration);
			DateTime dEndDate = utcDate.AddHours(duration);
			RdbQuery qry = new RdbQuery(typeof(Reservations), Reservations.UTCPUDateTimeEntityColumn, EntityQueryOp.GE, dStartDate);
			qry.AddClause(Reservations.UTCPUDateTimeEntityColumn, EntityQueryOp.LE, dEndDate);
			qry.AddClause(Reservations.CancelledEntityColumn, EntityQueryOp.EQ, false);
			qry.AddClause(Reservations.AffiliateDeniedEntityColumn, EntityQueryOp.EQ, false);
			qry.AddClause(Reservations.ConfirmationDateEntityColumn, EntityQueryOp.IsNull, true);

			EntityList<Reservations> cancelledReservations = new EntityList<Reservations>();
			EntityList<Reservations> resList = pPM.GetEntities<Reservations>(qry, QueryStrategy.DataSourceThenCache);

			foreach (Reservations res in resList) {
				if (res.UTCPUDateTime.HasValue) {
					DateTime dDate = res.UTCPUDateTime.Value.AddMinutes(res.MaxPassengerWaitTime.GetValueOrDefault(0));
					TimeSpan oTime = dDate.Subtract(utcDate);

					if (res.ASAPRide.GetValueOrDefault(false)) {
						int hours = oDefaults.AutoCancelASAPDuration / 60;
						int mins = oDefaults.AutoCancelASAPDuration % 60;
						if (oTime.Days == 0 && oTime.Hours >= 0 && oTime.Hours <= hours) {
							if (oTime.Minutes >= 0 && oTime.Minutes <= mins) {
								DoAutoCancel(res);
								cancelledReservations.Add(res);
							}
						}
						if (!res.Cancelled && oDefaults.AutoCancelMinRideDuration > 0) {
							dDate = res.GetCreatedDate;
							oTime = utcDate.Subtract(res.GetCreatedDate);
							hours = oDefaults.AutoCancelMinRideDuration / 60;
							mins = oDefaults.AutoCancelMinRideDuration % 60;
							if (oTime.Days == 0 && oTime.Hours >= 0 && oTime.Hours <= hours) {
								if (oTime.Minutes >= mins) {
									DoAutoCancel(res);
									cancelledReservations.Add(res);
								}
							}
						}
					} else {
						int hours = oDefaults.AutoCancelNonASAPDuration / 60;
						int mins = oDefaults.AutoCancelNonASAPDuration % 60;
						if (oTime.Days == 0 && oTime.Hours >= 0 && oTime.Hours <= hours) {
							if (oTime.Minutes >= 0 && oTime.Minutes <= mins) {
								DoAutoCancel(res);
								cancelledReservations.Add(res);
							}
						}
					}
				}
			}

			return cancelledReservations;

		}

		private static void DoAutoCancel(Reservations pRes) {
			pRes.Cancelled = true;
			pRes.AutoCancelled = true;
			pRes.ConfirmationAgent = "Auto Confirmed";
			pRes.ConfirmationDate = DateTime.Now.ToUniversalTime();
			try {
				pRes.Save();
			} catch {
			}
		}

		public static EntityList<Reservations> GetReservationsWithCouponCode(CouponCodes pCode, long pCustomerID) {
			RdbQuery qry = new RdbQuery(typeof(Reservations), Reservations.CustomerIDEntityColumn, EntityQueryOp.EQ, pCustomerID);
			qry.AddClause(Reservations.CouponCodeIDEntityColumn, EntityQueryOp.EQ, pCode.CouponCodeID);
			return pCode.PersistenceManager.GetEntities<Reservations>(qry);
		}

		public static EntityList<Reservations> GetCurrentReservationsForDriver(Drivers pDriver) {
			RdbQuery qry = new RdbQuery(typeof(Reservations), Reservations.DriverIDEntityColumn, EntityQueryOp.EQ, pDriver.DriverID);
			qry.AddClause(Reservations.AffiliateIDEntityColumn, EntityQueryOp.EQ, pDriver.AffiliateID);
			qry.AddClause(Reservations.CancelledEntityColumn, EntityQueryOp.EQ, false);
			qry.AddClause(Reservations.ClosedEntityColumn, EntityQueryOp.EQ, false);
			qry.AddOrderBy(Reservations.PickUpDateEntityColumn, System.ComponentModel.ListSortDirection.Ascending);
			return pDriver.PersistenceManager.GetEntities<Reservations>(qry, QueryStrategy.DataSourceThenCache);
		}

		public override DateTime? DriverResponseDate {
			get {
				if (this.Affiliate != null && Affiliate.TimeZone != null && base.DriverResponseDate.HasValue) {
					TimeZoneConverter oTimeZone = new TimeZoneConverter();
					return oTimeZone.FromUniversalTime(this.Affiliate.TimeZone, base.DriverResponseDate.Value);
				}
				return base.DriverResponseDate;
			}
			set {
				base.DriverResponseDate = value;
				if (value.HasValue) {
					base.DriverResponseDate = value.Value.ToUniversalTime();
				}
			}
		}

		public string DispatchDriverName {
			get {
				return this.Driver.Name;
			}
		}

		public string DispatchDriverStatus {
			get {
				if (this.DriverAccepted.HasValue) {
					if (this.DriverAccepted.Value) {
						return "Accepted";
					}
					return "Declined";
				}
				return "";
			}
		}

		public string DispatchPUDateTime {
			get {
				string sTimeZone = this.Affiliate.TimeZone;
				TimeZones oZone = TimeZones.GetTimeZone(sTimeZone);
				if (oZone == null) {
					sTimeZone = "";
				} else {
					sTimeZone = oZone.StandardAbbr;
					if (this.PickUpDate.IsDaylightSavingTime()) {
						sTimeZone = oZone.DayLightAbbr;
					}
				}

				string sDate = PickUpDate.ToShortTimeString() + " " + sTimeZone + "<br />";
				sDate += PickUpDate.ToString("dd/MM") + "<br />";
				sDate += PickUpDate.DayOfWeek.ToString().Substring(0, 3);

				return sDate;
			}
		}
	}


	public enum ReservationRatings {
		AllRatings,
		RatingsWithComments
	}
}
