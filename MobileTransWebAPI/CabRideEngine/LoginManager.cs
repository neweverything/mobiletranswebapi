using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Principal;
using System.Diagnostics;

using IdeaBlade.Persistence;
using TaxiPassCommon;


namespace CabRideEngine {

	[Serializable]
	public class LoginManager : IPersistenceLoginManager {

		static public IPrincipal PrincipleUser;

		public LoginManager() {
		}

		public IPrincipal Login(ILoginCredential pCredential, PersistenceManager pManager) {
			IIdentity identity;
			IPrincipal principal = null;

			if (pCredential == null) {
				return principal;
			}

			String domain = pCredential.Domain;
			String userName = pCredential.UserName;
			String password = pCredential.Password;

			//if (userName.Equals("WindowsService") && password.Equals("Windows123Service")) {
			//    identity = new GenericIdentity(userName);
			//    string sRole = "WindowsService";
			//    PrincipleUser = new GenericPrincipal(identity, new String[] { sRole });
			//    return PrincipleUser;
			//}
			
			//EntityList<CabRideEngine.User> mUsers;
			//mUsers = pManager.GetEntities<CabRideEngine.User>();
			//User oUser;
			//if (mUsers.Count == 0) {
			//	oUser = CabRideEngine.User.Create(pManager); //, "admin", "admin");
			//	oUser.UserName = "admin";
			//	oUser.UserPassword = "admin";
			//	oUser.FirstName = "Admin";
			//	oUser.LastName = "Account";
			//	oUser.IsAdministrator = true;
			//	oUser.CreatedBy = "System";
			//	oUser.CreatedDate = DateTime.Now;

			//	pManager.SaveChanges();
			//}

			if (CabRideEngineDapper.CabRideDapperSettings.SqlConnectionString.IsNullOrEmpty()) {
				CabRideEngineDapper.CabRideDapperSettings.SqlConnectionString = AppHelper.Initializer.GetConnectionString(pManager.DataSourceExtension);
			}

			CabRideEngineDapper.User oUser;
			try {
				// Database login version
				oUser = CabRideEngineDapper.User.GetUser(userName);

				if (oUser.IsNullEntity) {
					throw new AppLoginException(AppLoginExceptionType.UserName, MessageTypes.InvalidUserName, userName);
				}
				String hashedPassword = User.HashPassword(password + oUser.Salt);
				if (!hashedPassword.Equals(oUser.UserPassword)) {
					throw new AppLoginException(AppLoginExceptionType.Password, MessageTypes.InvalidPassword, userName);
				}
				identity = new GenericIdentity(userName);
				string sRole = "";
				if (oUser.IsSuperUser.GetValueOrDefault(false)) {
					sRole = "SuperUser";
				} else if (oUser.IsAdministrator) {
					sRole = "Administrator";
				}
				PrincipleUser = new GenericPrincipal(identity, new String[] { sRole });
				return PrincipleUser;
			} catch (Exception ex) {
				throw new Exception("Login Manager Error: " + ex.Message);
			}
		}

	}
}
