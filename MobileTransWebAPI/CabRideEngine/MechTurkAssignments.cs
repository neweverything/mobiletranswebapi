﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Text;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

namespace CabRideEngine {
	/// <summary>
	/// The developer class representing the MechTurkAssignments business object class.
	/// </summary>
	/// <remarks>
	/// <para>Place all application-specific business logic in this class.</para>
	/// <para>
	/// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
	/// and will not overwrite changes.
	/// </para>
	/// <para>
	/// This class inherits from the generated class, its companion in the pair of classes 
	/// defining a business object. Properties in the generated class may be overridden in 
	/// order to change the behavior or add business logic.
	/// </para>
	/// <para>
	/// This class is the final class in the inheritance chain for this business object. 
	/// It should not be inherited.
	/// </para>
	/// </remarks>
	[Serializable]
	public sealed partial class MechTurkAssignments : MechTurkAssignmentsDataRow {

		#region Constructors -- DO NOT MODIFY

		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new';
		// it is also required if the class is visible from a web service.
		// DO NOT REMOVE **
		private MechTurkAssignments() : this(null) { }

		/// <summary>
		/// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
		/// </summary>
		/// <param name="pRowBuilder"></param>
		public MechTurkAssignments(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static MechTurkAssignments Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      MechTurkAssignments aMechTurkAssignments = pManager.CreateEntity<MechTurkAssignments>();
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aMechTurkAssignments, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aMechTurkAssignments.AddToManager();
		//      return aMechTurkAssignments;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		// Add additional logic to your business object here...


		// **************************************************************************************************************
		// Lookup the assignment by the url - MT reuses preview urls
		// **************************************************************************************************************
		public static MechTurkAssignments GetAssignmentByURL(PersistenceManager pPM, string pURL) {
			MechTurkAssignments MTAssignment = null;
			StringBuilder sql = new StringBuilder();
			sql.Append("SELECT MechTurkAssignments.* FROM MechTurkAssignments with (nolock) WHERE ");
			sql.Append("MTPreviewURL = '");
			sql.Append(pURL);
			sql.Append("'");
			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(MechTurkAssignments), sql.ToString());

			EntityList<MechTurkAssignments> list = new EntityList<MechTurkAssignments>();
			try {
				list = pPM.GetEntities<MechTurkAssignments>(qry);

			} catch (Exception ex) {
				System.Console.WriteLine(ex.Message);
			}

			if (list.Count > 0) {
				MTAssignment = list[0];
			}

			return MTAssignment;
		}

		// **************************************************************************************************************
		// Lookup the assignment by the HitID - MT seems to change the hit it post assignment
		// **************************************************************************************************************
		public static MechTurkAssignments GetAssignmentByHitID(PersistenceManager pPM, string pHitID) {
			MechTurkAssignments MTAssignment = null;
			StringBuilder sql = new StringBuilder();
			sql.Append("SELECT MechTurkAssignments.* FROM MechTurkAssignments with (nolock) WHERE ");
			sql.Append("MTHitID = '");
			sql.Append(pHitID);
			sql.Append("'");
			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(MechTurkAssignments), sql.ToString());

			EntityList<MechTurkAssignments> list = new EntityList<MechTurkAssignments>();
			try {
				list = pPM.GetEntities<MechTurkAssignments>(qry);

			} catch (Exception ex) {
				System.Console.WriteLine(ex.Message);
			}

			if (list.Count > 0) {
				MTAssignment = list[0];
			}

			return MTAssignment;
		}


		// **************************************************************************************************************
		// Lookup the most recent assignment by the TransNo 
		// **************************************************************************************************************
		public static MechTurkAssignments GetAssignmentByTransNo(PersistenceManager pPM, string pTransNo) {
			MechTurkAssignments MTAssignment = null;
			StringBuilder sql = new StringBuilder();
			sql.Append("Select top 1 * from MechTurkAssignments ");
			sql.Append(" where MechTurkAssignmentsID = ( ");
				sql.Append(" SELECT  Max(MechTurkAssignmentsID) MechTurkAssignmentsID ");
				sql.Append(" FROM    MechTurkAssignments ");
				sql.Append(" inner join MatcherRowLock on MechTurkAssignments.MatcherRowLockID = MatcherRowLock.MatcherRowLockID ");
				sql.Append(" inner join DriverPaymentVoucherImages on MatcherRowLock.DriverPaymentVoucherImageID = DriverPaymentVoucherImages.driverpaymentvoucherimageid ");
				sql.Append(" inner join DriverPayments on DriverPaymentVoucherImages.DriverPaymentID = DriverPayments.DriverPaymentID ");
				sql.Append(" where DriverPayments.TransNo = '");
				sql.Append(pTransNo);
				sql.Append("') ORDER BY MechTurkAssignmentsID desc");
			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(MechTurkAssignments), sql.ToString());

			EntityList<MechTurkAssignments> list = new EntityList<MechTurkAssignments>();
			try {
				list = pPM.GetEntities<MechTurkAssignments>(qry);

			} catch (Exception ex) {
				System.Console.WriteLine(ex.Message);
			}

			if (list.Count > 0) {
				MTAssignment = list[0];
			}

			return MTAssignment;
		}

		// **************************************************************************************************************
		// Lookup the most recent assignment by the TransNo 
		// **************************************************************************************************************
		public static MechTurkAssignments GetAssignmentByDriverPaymentID(PersistenceManager pPM, long pDriverPaymentID) {
			MechTurkAssignments MTAssignment = null;
			StringBuilder sql = new StringBuilder();
			sql.Append("Select top 1 * from MechTurkAssignments ");
			sql.Append(" where MechTurkAssignmentsID = ( ");
			sql.Append(" SELECT  Max(MechTurkAssignmentsID) MechTurkAssignmentsID ");
			sql.Append(" FROM    MechTurkAssignments ");
			sql.Append(" inner join MatcherRowLock on MechTurkAssignments.MatcherRowLockID = MatcherRowLock.MatcherRowLockID ");
			sql.Append(" inner join DriverPaymentVoucherImages on MatcherRowLock.DriverPaymentVoucherImageID = DriverPaymentVoucherImages.driverpaymentvoucherimageid ");
			sql.Append(" inner join DriverPayments on DriverPaymentVoucherImages.DriverPaymentID = DriverPayments.DriverPaymentID ");
			sql.Append(" where DriverPayments.DriverPaymentID = ");
			sql.Append(pDriverPaymentID);
			sql.Append(") ORDER BY MechTurkAssignmentsID desc");
			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(MechTurkAssignments), sql.ToString());

			EntityList<MechTurkAssignments> list = new EntityList<MechTurkAssignments>();
			try {
				list = pPM.GetEntities<MechTurkAssignments>(qry);

			} catch (Exception ex) {
				System.Console.WriteLine(ex.Message);
			}

			if (list.Count > 0) {
				MTAssignment = list[0];
			}

			return MTAssignment;
		}
	}



	#region EntityPropertyDescriptors.MechTurkAssignmentsPropertyDescriptor
	////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
	////    For example: 
	////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
	//public static partial class EntityPropertyDescriptors {
	//	public partial class MechTurkAssignmentsPropertyDescriptor : BaseEntityPropertyDescriptor {
	//    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
	//    public AdaptedPropertyDescriptor Age {
	//      get { return this.Get("Age"); }
	//    }
	//  }
	//} 
	#endregion

}