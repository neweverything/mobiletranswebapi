﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

using Dapper;
using TaxiPassCommon;

using CabRideEngineDapper.Utils;

namespace CabRideEngineDapper {
	public partial class usp_Reservations_GetHotelJobs {
		public string CustomerName { get; set; }
		public string CustomerPhone { get; set; }
		public DateTime LocalPickupDate { get; set; }
		public string Status { get; set; }
		public string UserName { get; set; }
		public string Driver { get; set; }
		public string DriverCell { get; set; }
		public long ReservationID { get; set; }
		public string ReserveNo { get; set; }
		public string Location { get; set; }
		public decimal Latitude { get; set; }
		public decimal Longitude { get; set; }


		public static List<usp_Reservations_GetHotelJobs> Execute(SqlConnection pConn, long pPOIMasterID, bool pIncludeCancelled) {
			if (pIncludeCancelled) {
				return pConn.Query<usp_Reservations_GetHotelJobs>("usp_Reservations_GetHotelJobsIncludeCancelled", new { POIMasterID = pPOIMasterID }, commandType: CommandType.StoredProcedure).ToList();
			}
			return pConn.Query<usp_Reservations_GetHotelJobs>("usp_Reservations_GetHotelJobs", new { POIMasterID = pPOIMasterID }, commandType: CommandType.StoredProcedure).ToList();
		}
	}
}
