﻿using CabRideEngineDapper.Utils;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using TaxiPassCommon;

namespace CabRideEngineDapper {
	public partial class Customer {

		public static Customer Create(string pAccountNo) {
			Customer rec = CustomerTable.Create();
			rec.Salt = StringUtils.GenerateSalt();
			rec.AccountNo = pAccountNo;

			return rec;
		}


		#region Relations
		private Accounts mAccounts;
		public Accounts Accounts {
			get {
				if (mAccounts == null || (mAccounts.IsNullEntity && !this.AccountNo.IsNullOrEmpty())) {
					if (this.AccountNo.IsNullOrEmpty()) {
						mAccounts = CabRideEngineDapper.Accounts.GetNullEntity();
					} else {
						mAccounts = CabRideEngineDapper.Accounts.GetAccount(this.AccountNo);
					}
				}
				return mAccounts;
			}
		}

		private List<CustomerCards> mCustomerCards;
		public List<CustomerCards> CustomerCardses {
			get {
				if (mCustomerCards == null) {
					mCustomerCards = CustomerCards.GetCustomerCards(this.CustomerID);
				}
				return mCustomerCards;
			}
		}

		private List<CustomerAddress> mCustomerAddress;
		public List<CustomerAddress> CustomerAddresses {
			get {
				if (mCustomerAddress == null) {
					mCustomerAddress = CustomerAddress.GetCustomerAddresses(this.CustomerID);
				}
				return mCustomerAddress;
			}
		}
		#endregion

		#region Custom Properties
		[Editable(false)]
		public string GetPassword {
			get {
				Encryption encrypt = new Encryption();
				encrypt.blnReturnOrigOnError = true;
				return encrypt.Decrypt(Password, Salt);
			}
		}

		[Editable(false)]
		public string CustomerToken { get; set; }

		[Editable(false)]
		public string CouponCode { get; set; }


		[Editable(false)]
		public decimal CouponValue { get; set; }

		[Editable(false)]
		public bool CouponValid { get; set; }


		[Editable(false)]
		public string ReserveNo { get; set; }

		[Editable(false)]
		public int AlertCode { get; set; }


		[Editable(false)]
		public string FirstName {
			get {
				if (("" + base.LastName).IsNullOrEmpty()) {
					return Name.Split(' ')[0];
				}
				return Name;
			}
		}

		public override string LastName {
			get {
				if (("" + base.LastName).IsNullOrEmpty()) {
					string value = "";
					if (("" + Name).Split(' ').Length > 1) {
						List<string> data = Name.Split(' ').ToList();
						data.RemoveAt(0);
						value = string.Join(" ", data);
					}
					return value;
				}
				return base.LastName;
			}
			set {
				base.LastName = value;
			}
		}


		[Editable(false)]
		public string FullName {
			get {
				return string.Format("{0} {1}", FirstName, LastName);
			}
		}
		#endregion

		public override string Password {
			get {
				//Encryption encrypt = new Encryption();
				//encrypt.blnReturnOrigOnError = true;
				//return encrypt.Decrypt(base.Password, Salt);
				return base.Password;
			}
			set {
				if (RowState == DataRowState.Detached) {
					base.Password = value;
				} else {
					Encryption encrypt = new Encryption();
					base.Password = encrypt.Encrypt(value, Salt);
				}
			}
		}


		public static Customer GetCustomer(long pCustomerID) {
			using (var conn = SqlHelper.OpenSqlConnection()) {
				return conn.Get<Customer>(pCustomerID);
			}
		}

		public static Customer GetCustomer(string pEMail, string pAccountNo) {
			using (var conn = SqlHelper.OpenSqlConnection()) {
				if (pEMail.IsNullOrEmpty()) {
					return Customer.GetNullEntity();
				}
				StringBuilder sql = new StringBuilder();
				sql.Append("Select * from Customer Where EMail = @EMail");
				DynamicParameters parms = new DynamicParameters();
				parms.Add("EMail", pEMail);
				if (!pAccountNo.IsNullOrEmpty()) {
					sql.Append("AND AccountNo = @AccountNo");
					parms.Add("AccountNo", pAccountNo);
				}
				return conn.Get<Customer>(sql.ToString(), parms);
			}
		}


		public static Customer GetCustomer(SqlConnection pConn, long pCustID) {
			return pConn.Get<Customer>(pCustID);
		}

		// ***********************************************************************************************
		// Get a customer by his token/email
		// pSearch:  Search string
		// Optional pTokenOnly: 0 = Search by token, email, or phone.  1 = Token only
		// Optional pCouponCode: Validate coupone code has not been used by customer
		// Notes can contain:  Token:P6CNMLEY|CouponAmount:0.00
		// ***********************************************************************************************
		public static Customer GetCustomerbyTokenOrEmail(string pSearch, string pTokenOnly, string pCouponCode, string pEmailOnly) {
			using (var conn = SqlHelper.OpenSqlConnection()) {
				return GetCustomerbyTokenOrEmail(conn, pSearch, pTokenOnly, pCouponCode, pEmailOnly);
			}
		}

		public static Customer GetCustomerbyTokenOrEmail(SqlConnection pConn, string pSearch, string pTokenOnly, string pCouponCode, string pEmailOnly) {

			string sql = "usp_CustomerGetByTokenPhoneEmail";
			DynamicParameters param = new DynamicParameters();
			param.Add("Lookup", pSearch);
			if (!pTokenOnly.IsNullOrEmpty()) {
				param.Add("TokenOnly", pTokenOnly);
			}
			if (!pCouponCode.IsNullOrEmpty()) {
				param.Add("CouponCode", pCouponCode);
			}
			if (pEmailOnly == "1") {
				param.Add("EmailOnly", 1);
			}

			Customer oCustomer = pConn.Query<Customer>(sql, param, commandType: CommandType.StoredProcedure).DefaultIfEmpty(Customer.GetNullEntity()).FirstOrDefault();

			if (!oCustomer.Notes.IsNullOrEmpty()) {
				try {
					string sNotes = oCustomer.Notes;

					// Token:P6CNMLEY|
					int iPos = sNotes.ToLower().IndexOf("token:");
					if (iPos >= 0) {
						iPos = iPos + "token:".Length;
						string sToken = sNotes.Substring(iPos, sNotes.IndexOf("|") - iPos);
						oCustomer.CustomerToken = sToken;
						sNotes = sNotes.Substring(iPos + sToken.Length + 1);
					}

					// CouponAmount:5.00|
					iPos = sNotes.ToLower().IndexOf("couponamount:");
					if (iPos >= 0) {
						iPos = iPos + "CouponAmount:".Length;
						string sCouponAmount = sNotes.Substring(iPos, sNotes.IndexOf("|") - iPos);
						oCustomer.CouponCode = pCouponCode;
						oCustomer.CouponValue = decimal.Parse(sCouponAmount);

						oCustomer.CouponValid = false;
						if (oCustomer.CouponValue > 0) {
							oCustomer.CouponValid = true;
						}
						sNotes = sNotes.Substring(iPos + sCouponAmount.Length + 1);
					}

					// ReserveNo:abc123|
					iPos = sNotes.ToLower().IndexOf("reserveno:");
					if (iPos >= 0) {
						iPos = iPos + "ReserveNo:".Length;
						string sReserveNo = sNotes.Substring(iPos, sNotes.IndexOf("|") - iPos);
						oCustomer.ReserveNo = sReserveNo;
						sNotes = sNotes.Substring(iPos + sReserveNo.Length + 1);
					}

					// AlertCode:10|
					iPos = sNotes.ToLower().IndexOf("alertcode:");
					if (iPos >= 0) {
						iPos = iPos + "AlertCode:".Length;
						int iAlertCode = int.Parse(sNotes.Substring(iPos, sNotes.IndexOf("|") - iPos));
						oCustomer.AlertCode = iAlertCode;
						sNotes = sNotes.Substring(iPos + iAlertCode.ToString().Length + 1);

					}
				} catch (Exception ex) {
					Console.WriteLine(ex.Message);
				}

			}

			return oCustomer;
		}


		public static List<Customer> GetCustomerAccountAdmins(SqlConnection pConn, string pAccountNo) {
			string sql = @"SELECT * FROM Customer 
							WHERE AccountNo = @AccountNo
								AND Admin = 1";

			return pConn.Query<Customer>(sql, new { AccountNo = pAccountNo }).ToList();
		}


		public static Customer GetCustomerByCell(SqlConnection pConn, string pCell) {
			string sql = @"SELECT  *
                            FROM    dbo.Customer
                            WHERE   CellPhone = @CellPhone";

			return pConn.Get<Customer>(sql, new { CellPhone = pCell });
		}

		public static Customer GetCustomerByCell(SqlConnection pConn, string pCell, long pAffiliateID) {
			string sql = @"SELECT  *
                            FROM    dbo.Customer
                            WHERE   CellPhone = @CellPhone
                                    AND AffiliateID = @AffiliateID";

			return pConn.Get<Customer>(sql, new { CellPhone = pCell, AffiliateID = pAffiliateID });
		}

		public static Customer GetCustomerByName(string pName, long pAffiliateID) {
			string sql = @"SELECT  *
                            FROM    dbo.Customer
                            WHERE   AffiliateID = @AffiliateID
                                    AND Name = @Name";

			using (var conn = SqlHelper.OpenSqlConnection()) {
				return conn.Get<Customer>(sql, new { Name = pName, AffiliateID = pAffiliateID });
			}
		}

		public static Customer GetCustomerByNameAndPhone(string pName, long pAffiliateID, string pPhoneNo) {
			string sql = @"SELECT *
							FROM dbo.Customer
							WHERE AffiliateID = @AffiliateID
								  AND Name = @Name
								  AND CellPhone = @PhoneNo";

			if (pPhoneNo.IsNullOrEmpty()) {
				return GetCustomerByName(pName, pAffiliateID);
			}

			using (var conn = SqlHelper.OpenSqlConnection()) {
				return conn.Get<Customer>(sql, new { Name = pName, AffiliateID = pAffiliateID, PhoneNo = pPhoneNo });
			}
		}




		/// <summary>
		/// Sends the SMS.
		/// </summary>
		/// <param name="pConn">The p connection.</param>
		/// <param name="pMsg">The p MSG.</param>
		public void SendSMS(SqlConnection pConn, string pMsg) {
			SystemDefaults defaults = SystemDefaults.GetDefaults(pConn);

			string acctSid = defaults.TwilioAccountSID;
			string acctToken = defaults.TwilioAccountToken;
			string apiVersion = defaults.TwilioAPIVersion;
			string smsNumber = defaults.TwilioSMSNumber;

			//pCell = pCell.ExtractNumbers();

			if (CellPhone.Length == 10) {
				TaxiPassCommon.Mail.TwilioTexting twilio = new TaxiPassCommon.Mail.TwilioTexting(acctSid, acctToken, apiVersion, smsNumber);
				try {
					var response = twilio.TwilioTextMessage(pMsg, CellPhone);
					if (response.RestException != null && !response.RestException.Message.IsNullOrEmpty()) {

					}
				} catch (Exception) {

				}
			}

		}

		public static Customer GetCustomerByEmployeeNumber(Accounts pAccount, string pEmployeeNumber) {
			string sql = @"SELECT  *
                            FROM    Customer
                            WHERE   EmployeeNumber = @EmployeeNumber
                                    AND AccountNo = @AccountNo";
			DynamicParameters parms = new DynamicParameters();
			parms.Add("EmployeeNumber", pEmployeeNumber);
			parms.Add("AccountNo", pAccount.AccountNo);
			using (var conn = SqlHelper.OpenSqlConnection()) {
				return conn.Get<Customer>(sql, parms);
			}
		}


	}
}
