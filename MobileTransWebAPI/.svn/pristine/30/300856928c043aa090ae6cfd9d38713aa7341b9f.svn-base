﻿using CabRideEngineDapper;
using CabRideEngineDapper.Utils;
using Dapper;
using MobileTransWebAPI.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Http;
using TaxiPassCommon;
using TaxiPassCommon.Banking;
using TaxiPassCommon.Banking.Enums;
using TaxiPassCommon.Banking.Interfaces;

namespace MobileTransWebAPI.Controllers {
	public class FundPayCardSelectedVouchersController : ApiController {

		public FundsTransferResponse Get() {
			//try {
			//	List<string> lines = System.IO.File.ReadAllLines(@"d:\temp\FundPayCard_20180426").ToList();
			//	foreach (string data in lines) {
			//		string temp = data.Split(':').Last().Trim();
			//		AffDriverLoginInfo info = JsonConvert.DeserializeObject<AffDriverLoginInfo>(temp.Decrypt());
			//		if (info.Phone == "9084166165") {
			//			string js = JsonConvert.SerializeObject(info);
			//			Console.WriteLine(js);
			//			FundPayCard(temp);
			//		}
			//	}
			//} catch (Exception ex) {
			//	Console.WriteLine(ex.Message);
			//}

			string request = "0AA8FC97BF6D64C0CB8134E58EECF31DC692A5836185BF26DC1EAC5D848035C517A4C5C88345A6B244C37D21284F10222BC08163D9547F5FF9E6C2460AB4955D49556ACE06B7D0AD321E3442B796DFA7C2A4D3D72C22E481169B0688895CC344A265119A948A5521F0365E9A874FE93EA0D1D1B65A901018F0E28D5D5FFB35005CB45D10";
			return FundPayCard(request);
		}

		[HttpGet]
		public FundsTransferResponse FundPayCard(string pRequest) {
			FundsTransferResponse fundResults = new FundsTransferResponse();

			WebApiApplication.LogIt("FundPayCardSelectedVouchers:", pRequest, "FundPayCard_{0}");

			try {
				using (var conn = SqlHelper.OpenSqlConnection()) {

					FundPayCardSelected info = new FundPayCardSelected();
					string request = pRequest.DecryptIceKey();
					info = JsonConvert.DeserializeObject<FundPayCardSelected>(request);

					DateTime requestDate = Convert.ToDateTime(info.RequestTime);

					if (WebApiApplication.GetUTCServerTime().Subtract(requestDate).TotalMinutes > 1) {
						fundResults.ReturnMessage = "Invalid Access";
						return fundResults;
					}

					if (!info.Phone.IsNullOrEmpty()) {

						AffiliateDrivers pPayeeDriver = AffiliateDrivers.GetDriverByCallInCell(conn, info.Phone);

						if (pPayeeDriver.IsNullEntity || pPayeeDriver.RedeemerStopPayment || pPayeeDriver.TransCardNumber.IsNullOrEmpty()) {
							fundResults.ReturnCode = "-1";
							fundResults.ReturnMessage = "You cannot get paid at this time, please contact TaxiPass to resolve";
							return fundResults;
						}

						List<DriverPayments> payList = GetPayList(conn, pPayeeDriver.AffiliateDriverID, info.TransList);
						if (payList.Count == 0) {
							fundResults.ReturnCode = "-1";
							fundResults.ReturnMessage = "No vouchers available for funding";
							return fundResults;
						}

						bool dejavooStopPayment = payList.Where(p => p.CreditCardProcessor.Equals(Platforms.Dejavoo.ToString()) && p.DriverPaymentChargeResults.ProcessorBatchNo.IsNullOrEmpty()).ToList().Count > 0;
						if (dejavooStopPayment) {
							fundResults.ReturnCode = "-1";
							fundResults.ReturnMessage = "Please settle batch or bring credit card machine to trailer for payment";
							return fundResults;
						}

						if (pPayeeDriver.PayCardType.IsNullOrEmpty()) {
							pPayeeDriver.PayCardType = PayCardTypes.USBank.ToString();
							pPayeeDriver.Save();
						}

						List<SystemDefaultsDict> payCardDefaults = SystemDefaultsDict.GetGroup(conn, "PayCard", false);
						string payCardType = payCardDefaults.FirstOrDefault(p => p.ReferenceKey.Equals("ForceUpgradeTo", StringComparison.CurrentCultureIgnoreCase)).ValueString;
						if (!payCardType.IsNullOrEmpty() && pPayeeDriver.PayCardType != payCardType) {
							fundResults.ReturnCode = "-1";
							fundResults.ReturnMessage = payCardDefaults.FirstOrDefault(p => p.ReferenceKey.Equals("UpgradeMessage", StringComparison.CurrentCultureIgnoreCase)).ValueString;
							return fundResults;
						}


						decimal decAmount = 0;

						IPayCardCredentials cred = new FSV.Credentials();
						AffiliateDriverRef rec;


						CabRideEngineDapper.PayCardInfo fsvInfo = CabRideEngineDapper.PayCardInfo.GetByType(conn, pPayeeDriver.PayCardType);
						if (fsvInfo.PayCardType.Equals(PayCardTypes.TransCard.ToString(), StringComparison.CurrentCultureIgnoreCase)) {
							TransCardDefaults transCardDefaults = TransCardDefaults.GetDefaults(fsvInfo.UseProduction ? fsvInfo.PassCode.ToLong() : fsvInfo.TestPassCode.ToLong());
							cred = JsonConvert.DeserializeObject<TransCard.Credentials>(JsonConvert.SerializeObject(transCardDefaults));
						} else {
							cred = JsonConvert.DeserializeObject<FSV.Credentials>(JsonConvert.SerializeObject(fsvInfo)); //  conn.Query<FSV.Credentials>("usp_SystemDefaultsDict_Flattened", new { ReferenceGroup = "FSV" }, commandType: CommandType.StoredProcedure).FirstOrDefault();
						}
						rec = AffiliateDriverRef.GetOrCreate(conn, pPayeeDriver.AffiliateDriverID, "FSV", "Registration");


						PayCardGateway payCardGateWay = new PayCardGateway(cred);
						FSV.VerifyConnectionResponse verifyResponse = payCardGateWay.VerifyConnection();

						if (verifyResponse.ReturnCode != "1") {
							fundResults.ReturnCode = "-1";
							fundResults.ReturnMessage = "Could not verify connection to USBank: " + verifyResponse.ReturnMessage;
							return fundResults;
						}

						FSV.CardRegistrationResponse resp = JsonConvert.DeserializeObject<FSV.CardRegistrationResponse>(rec.ValueString);
						FSV.FundsTransfer transfer = new FSV.FundsTransfer(cred);
						transfer.ToCardID = resp.CardID;

						decAmount = ProcessPayments(fundResults, conn, info, pPayeeDriver, payList, decAmount, cred, transfer);

					}

				}
			} catch (Exception ex) {
				fundResults.ReturnMessage = ex.Message;
			}

			return fundResults;
		}

		public static decimal ProcessPayments(FundsTransferResponse fundResults, SqlConnection conn, FundPayCardSelected info, AffiliateDrivers pPayeeDriver, List<DriverPayments> payList, decimal decAmount, IPayCardCredentials cred, FSV.FundsTransfer transfer) {
			List<ACHDetail> achRecList = new List<ACHDetail>();
			long redeemerID = SystemDefaultsDict.GetByKey(conn, "SelfRedemption", "RedeemerID", false).ValueString.AsLong();
			DateTime achPaidDate = DateTime.Now;

			string transNo = "";
			foreach (DriverPayments pay in payList.Distinct().ToList()) {
				if (pay.Test || pay.DoNotPay.GetValueOrDefault(false) || pay.Failed) {
					continue;
				}
				ACHDetail achRec = ACHDetail.GetByDriverPaymentID(conn, pay.DriverPaymentID);
				if (!achRec.ACHPaidDate.HasValue) {
					if (achRec.IsNullEntity) {
						achRec = ACHDetail.Create(pay);
					}
					//if (pPayeeDriver.PayCardRealTimePay || pPayeeDriver.Affiliate.PayCardRealTimePay) {
					decAmount = pay.DriverTotalPaid;

					if (decAmount == 0) {
						decAmount = pay.DriverPaymentKioskAmount.Amount;
					}

					if (decAmount == 0) {
						achRec.ACHErrorReason = "No Fare amount found";
					} else {
						achRec.AmountPaid = decAmount;
						//transfer.TransferAmount += decAmount;
					}

					achRec.ACHErrorReason = "Paid: " + pay.DriverTotalPaid.ToString("C");
					//}
					achRec.ACHPaidDate = achPaidDate;
					achRec.ACHPaidTo = pPayeeDriver.Name;
					achRec.ACHBankAccountNumber = "PayCard: " + pPayeeDriver.TransCardNumber.DecryptIceKey().Right(5);

					achRec.TransCardAdminNo = pPayeeDriver.TransCardAdminNo;
					if (transNo.IsNullOrEmpty()) {
						transNo = pay.TransNo;
					} else {
						transNo = string.Format("{0}-{1}", transNo, pay.TransNo);
					}
					achRec.RowState = DataRowState.Unchanged;
					achRecList.Add(achRec);

					if (!pay.DriverPaid && achRec.AmountPaid.GetValueOrDefault(0) != 0) {
						pay.DriverPaid = true;
						pay.DriverPaidDate = achRec.ACHPaidDate;
						pay.TaxiPassRedeemerID = info.TaxiPassRedeemerID == 0 ? redeemerID : info.TaxiPassRedeemerID;
						pay.Save(info.UserID);

						DriverPaymentRef dpRef = DriverPaymentRefTable.Create();
						dpRef.DriverPaymentID = pay.DriverPaymentID;
						dpRef.ReferenceGroup = "ACHToPayCard";
						dpRef.ReferenceKey = "AdminNo";
						dpRef.ValueString = pPayeeDriver.TransCardAdminNo;
						dpRef.Save(info.UserID);
					}
				}
			}
			transfer.TransferAmount = achRecList.Distinct().Sum(p => p.AmountPaid.GetValueOrDefault(0));
			transfer.ReferenceFromCard = string.Format("ADriverID: {0}|{1}", pPayeeDriver.AffiliateDriverID, transNo);
			if (pPayeeDriver.PayCardType != PayCardTypes.USBank.ToString()) {
				transfer.Reserved1 = pPayeeDriver.AffiliateDriverID.ToString();
			}
			if (transfer.ReferenceFromCard.Length > 100 && pPayeeDriver.PayCardType != PayCardTypes.GlobalCashCard.ToString()) {
				// create the Red Master Voucher
				string sql = "usp_RedeemerMasterVoucherCreate";
				DynamicParameters parms = new DynamicParameters();
				parms.Add("RedeemerID", redeemerID);
				parms.Add("ModifiedBy", " (Redeemer)");
				parms.Add("ModifiedDate", DateTime.Now);

				long masterVoucherID = 0;

				masterVoucherID = conn.Query<long>(sql, parms, commandType: CommandType.StoredProcedure).DefaultIfEmpty(0).FirstOrDefault();

				//if (masterVoucherID > 0) {
				//	mRedeemerMasterVoucherID = masterVoucherID;
				//}

				foreach (var achRec in achRecList) {
					DriverPaymentAux aux = DriverPaymentAux.GetOrCreate(achRec.DriverPaymentID);
					aux.RowState = DataRowState.Unchanged;
					aux.MasterVoucherID = masterVoucherID;
					aux.Save();
				}
				transfer.ReferenceFromCard = string.Format("ADriverID: {0}|{1}", pPayeeDriver.AffiliateDriverID, masterVoucherID);
			}
			transfer.ReferenceToCard = "Driver Payment";

			PayCardGateway payCardGateway = new PayCardGateway(cred);
			var funded = payCardGateway.TransferToPayCard(transfer);
			if (funded.ReturnCode == "1") {
				fundResults.ReturnCode = funded.ReturnCode;
				fundResults.ReturnMessage = funded.ReturnMessage;
				Misc.SendTransCardPaymentSMS(payList.ToList(), pPayeeDriver, "");
				foreach (var achRec in achRecList) {
					achRec.RowState = DataRowState.Added;
					achRec.Save();
				}

				if (pPayeeDriver.PayCardType.Equals(PayCardTypes.USBank.ToString(), StringComparison.CurrentCultureIgnoreCase)) {
					SystemDefaultsDict fundBal = SystemDefaultsDict.GetOrCreate(conn, "FSV", "FundingAccountBalance");
					fundBal.ValueString = funded.FromCardBalance;
					fundBal.Save();
				}

				USBankPayCardPaid usBank = USBankPayCardPaid.Create();
				usBank.AdminNo = transfer.ToCardID;
				usBank.AffiliateDriverID = pPayeeDriver.AffiliateDriverID;
				usBank.CardEnding = pPayeeDriver.TransCardCardNumber.Right(5);
				usBank.DebitTransaction = transfer.TransferAmount;
				usBank.FundingTransDate = DateTime.Now;
				usBank.LastName = pPayeeDriver.LastName.IsNullOrEmpty() ? pPayeeDriver.Name : pPayeeDriver.LastName;
				usBank.TaxiPassRedeemerID = info.TaxiPassRedeemerID == 0 ? redeemerID : info.TaxiPassRedeemerID;
				usBank.TransactionDetail = transfer.ReferenceFromCard;
				usBank.PayCardType = pPayeeDriver.PayCardType;
				usBank.TransactionID = funded.Unused2;
				usBank.Save();


			} else {
				fundResults.ReturnCode = funded.ReturnCode;
				if (transfer.TransferAmount > 0) {
					fundResults.ReturnMessage = string.Format("Payment to be posted later, error processing payment: {0}", funded.ReturnMessage);
					pPayeeDriver.SendDriverMsg(string.Format("Payment of {0} to be posted later to your card", transfer.TransferAmount.ToString("C")), false);
				}
			}

			return decAmount;
		}

		public List<DriverPayments> GetPayList(SqlConnection pConn, long pAffiliateDriverID, List<string> pTransList) {
			string sql = @"DECLARE @LookBackDays INT = (SELECT CONVERT(INT, ValueString)
                                                         FROM   SystemDefaultsDict
                                                         WHERE  ReferenceGroup = 'PayCard'
                                                                AND ReferenceKey = 'PayDriverChargeDateMinusDays'
                                                        )

                            DECLARE @MaxAge INT = (SELECT CONVERT(INT, ValueString)
                                                         FROM   SystemDefaultsDict
                                                         WHERE  ReferenceGroup = 'PayCard'
                                                                AND ReferenceKey = 'MaxAge'
                                                        )


                            SELECT  DISTINCT
                                    DriverPayments.*
                            FROM    dbo.DriverPayments
                            LEFT JOIN ACHDetail ON ACHDetail.DriverPaymentID = DriverPayments.DriverPaymentID
                            LEFT JOIN DriverPaymentAux ON DriverPaymentAux.DriverPaymentID = DriverPayments.DriverPaymentID
                            WHERE   AffiliateDriverID = @AffiliateDriverID
                                    AND ACHPaidDate IS NULL
                                    AND Failed = 0
                                    AND Test = 0
                                    AND DATEADD(DAY, @LookBackDays, ChargeDate) < GETUTCDATE()
		                            AND ChargeDate >= '1/16/2016'
		                            AND ChargeDate > DATEADD(DAY, -@MaxAge, GETUTCDATE())
                                    AND ISNULL(DriverPayments.DoNotPay, 0) = 0
                                    AND ISNULL(DriverPaymentAux.DoNotPay, 0) = 0
                                    AND TaxiPassRedeemerID IS NULL
                            		AND TransNo IN @TransNo";

			return pConn.Query<DriverPayments>(sql, new { AffiliateDriverID = pAffiliateDriverID, TransNo = pTransList }).ToList();
		}

	}
}
