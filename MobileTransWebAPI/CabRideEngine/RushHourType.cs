﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Util;

namespace CabRideEngine {
    [Serializable]
    public sealed class RushHourType : RushHourTypeDataRow {

        #region Constructors -- DO NOT MODIFY
        // Do not create constructors for this class
        // Developers cannot create instances of this class with the "new" keyword
        // You should write a static Create method instead; see the "Suggested Customization" region
        // Please review the documentation to learn about entity creation and inheritance

        // This private constructor prevents accidental instance creation via 'new'
        private RushHourType() : this(null) {
        }

        // Typed DataSet constructor (needed by framework) - DO NOT REMOVE
        public RushHourType(DataRowBuilder pRowBuilder)
            : base(pRowBuilder) {
        }

        #endregion

        #region Suggested Customizations

        //    // Use this factory method template to create new instances of this class
        //    public static RushHourType Create(PersistenceManager pManager,
        //      ... your creation parameters here ...) { 
        //
        //      RushHourType aRushHourType = (RushHourType) pManager.CreateEntity(typeof(RushHourType));
        //
        //      // if this object type requires a unique id and you have implemented
        //      // the IIdGenerator interface implement the following line
        //      pManager.GenerateId(aRushHourType, // add id column here //);
        //
        //      // Add custom code here
        //
        //      aRushHourType.AddToManager();
        //      return aRushHourType;
        //    }

        //    // Implement this method if you want your object to sort in a specific order
        //    public override int CompareTo(Object pObject) {
        //    }

        //    // Implement this method to customize the null object version of this class
        //    protected override void UpdateNullEntity() {
        //    }

        #endregion

        // Add additional logic to your business object here...
        protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
            if (IsDeserializing)
                return;
            base.OnColumnChanging(pArgs);
            //DoAudit(pArgs);
        }

        private void DoAudit(DataColumnChangeEventArgs pArgs) {
            if (!this.PersistenceManager.IsClient) {
                return;
            }
            if (this.RowState == DataRowState.Added) {
                return;
            }
            if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
                AuditRecord = RushHourTypeAudit.Create(this.PersistenceManager, this);
            }
            //AuditRecord[pArgs.Column.ColumnName] = pArgs.ProposedValue;
        }

    }

}
