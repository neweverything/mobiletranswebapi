using System;
using System.Collections.Generic;
using System.Text;

using IdeaBlade.Persistence;
using TaxiPassCommon.Mail;

namespace CabRideEngine {
    public class DispatchNotifications {
        private string mEMailFrom;
        private string mSubject;

        public DispatchNotifications(string pEMailFrom, string pSubject) {
            mEMailFrom = pEMailFrom;
            mSubject = pSubject;
        }
        public long SendNotifications(PersistenceManager pPM) {
            SystemDefaults oDefaults = SystemDefaults.GetDefaults(pPM);

            DateTime dStart = DateTime.Now;
            DateTime dEnd = dStart.AddHours(Convert.ToDouble(oDefaults.Tier1NotificationStart));
            EntityList<Reservations> oReserves = Reservations.GetUnconfirmedReservations(pPM, dStart, dEnd);
            int iCount = 0;
            foreach (Reservations oRes in oReserves) {
                iCount++;
                //string sText = "";

                //bool bSendMessage = Utils.DateUtils.IsRideWithin(oRes.UTCPUDateTime, oDefaults.Tier2NotificationStart);

                //if  ( oRes.UTCPUDateTime.Value.Subtract(DateTime.n


                //EMailMessage oEMail = new EMailMessage(mEMailFrom, mSubject, sText);
                //oEMail.SendMail(ConfigurationManager.AppSettings["EMailErrorsTo"].ToString());
            }

            return iCount;
        }


    }
}
