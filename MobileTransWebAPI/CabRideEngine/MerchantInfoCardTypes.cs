﻿using System;
using System.Collections.Generic;
using System.Text;

using IdeaBlade.Util;

namespace CabRideEngine {
    public class MerchantInfoCardTypes {


        public string CardType { get; set; }
        public string CardCode { get; set; }

        private bool mIsNullEntity = false;

        private static BindableList<MerchantInfoCardTypes> msEntities;
        private static MerchantInfoCardTypes msNullEntity;

        /// <summary>Class Constructor (Private)</summary>
        static MerchantInfoCardTypes() {

            // Create the null entity
            msNullEntity = new MerchantInfoCardTypes("<none>", "");
            msNullEntity.mIsNullEntity = true;

            // Populate list of Charge Types.
            msEntities = new BindableList<MerchantInfoCardTypes>();
            msEntities.Add(msNullEntity);
            msEntities.Add(new MerchantInfoCardTypes("Amex", "A"));
            msEntities.Add(new MerchantInfoCardTypes("Discover", "DS"));
            msEntities.Add(new MerchantInfoCardTypes("Visa, MasterCard", "VM"));
        }

        /// <summary>Constructor (Private)</summary>
        private MerchantInfoCardTypes(string pCardType, string pCardCode) {
            CardType = pCardType;
            CardCode = pCardCode;
        }

        /// <summary>Get the Incident Types null entity.</summary>
        public static MerchantInfoCardTypes NullEntity {
            get {
                return msNullEntity;
            }
        }

        /// <summary>Get the list of all MerchantInfo Card Types.</summary>
        public static BindableList<MerchantInfoCardTypes> GetAll() {
            return msEntities;
        }


        public static MerchantInfoCardTypes GetByCode(string pCode) {
            if (String.IsNullOrEmpty(pCode)) {
                return NullEntity;
            }
            foreach (MerchantInfoCardTypes rec in msEntities) {
                if (rec.CardCode.Equals(pCode, StringComparison.CurrentCultureIgnoreCase)) {
                    return rec;
                }
            }
            return NullEntity;
        }

        /// <summary>Return true if this is the null entity</summary>
        [BindingBrowsable(false)]
        public bool IsNullEntity {
            get {
                return mIsNullEntity;
            }
        }

    }
}
