﻿
namespace TaxiPassCommon.Banking {
    public class SoftDescriptor {
        public string AffiliateName { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
        public string CountryCode { get; set; }
        public string StateProvince { get; set; }
        public string Street { get; set; }
        public string PostalCode { get; set; }
        public string Phone { get; set; }
        public string Internet { get; set; }
        public CompanyURL CompanyURL = CompanyURL.TaxiPass;
        public string BillingDescriptor { get; set; }
        public bool ExcludeBillingDescriptorPrefix { get; set; }
    }

    public enum CompanyURL {
        TaxiPass,
        GetRide,
        ChargePass,
        StorePass,
        MedPass
    }
}
