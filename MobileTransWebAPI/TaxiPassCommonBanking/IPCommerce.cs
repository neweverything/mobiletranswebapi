﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TaxiPassCommon.Banking.com.ipcommerce.SvcInfo;
using System.ServiceModel;
using System.ServiceModel.Channels;
using TaxiPassCommon.Banking.com.ipcommerce.Txn;

namespace TaxiPassCommon.Banking {

	internal class IPCommerce : IProcessor {

		private IPCommerceCredentials mCredentials;
		private CWSServiceInformationClient mService;
		private CwsTransactionProcessingClient mTransService;
		private ServiceInformation mServiceInformation;

		private string mSessionToken;

		public string ErrorMsg = "";

		public IPCommerce(IPCommerceCredentials pCredentials) {
			try {
				mCredentials = pCredentials;
			} catch (Exception ex) {
				ErrorMsg = ex.Message;
			}
		}


		/// <summary>
		/// Opens the service info and transactin channels.
		/// </summary>
		/// <param name="pServiceUrl">The Service URL.</param>
		/// <param name="pTransUrl">The Trans URL.</param>
		/// <returns></returns>
		public CWSServiceInformationClient OpenServiceInfoChannel(string pServiceUrl, string pTransUrl) {

			Uri serviceUri = new Uri(pServiceUrl);
			EndpointAddress endpointAddress = new EndpointAddress(serviceUri);

			//Create the binding here
			Binding binding = BindingFactory.CreateInstance();

			mService = new CWSServiceInformationClient(binding, endpointAddress);
			mService.Open();

			mSessionToken = mService.SignOnWithToken(mCredentials.Token);
			mServiceInformation = mService.GetServiceInformation(mSessionToken);

			Uri transUri = new Uri(pTransUrl);
			EndpointAddress endpointTransAddress = new EndpointAddress(transUri);

			mTransService = new CwsTransactionProcessingClient(binding, endpointTransAddress);
			mTransService.Open();

			return mService;
		}



		public ProcessorResponse PreAuthCard(CreditCard pCardData, decimal pAuthAmount, string pComments, string pVoucherNo, string pService) {
			string url;
			string txnUrl;
			InitURL(pCardData, out url, out txnUrl);

			//skip USAePay if we do not have a URL to post to
			if (url.IsNullOrEmpty()) {
				return null;
			}

			mService = OpenServiceInfoChannel(url, txnUrl);

			BankcardTransaction bcTransaction = FillCardInfo(pCardData);// DataGenerator.SetBankCardTxnData();
			bcTransaction.TransactionData.OrderNumber = pVoucherNo;
			bcTransaction.TransactionData.InvoiceNumber = pVoucherNo;
			bcTransaction.TransactionData.Amount = pAuthAmount;

			var ipCommResponse = RunTransaction(TrxTypes.Authorization, bcTransaction, null, null, null, false, false);

			return ProcessResponse(ipCommResponse, pAuthAmount, TrxTypes.Authorization, pCardData.TestCard);
		}

		private void InitURL(CreditCard pCardData, out string url, out string txnUrl) {
			url = mCredentials.SvcInfoUrl;
			txnUrl = mCredentials.TxnUrl;
			if (pCardData.TestCard) {
				url = mCredentials.SvcInfoUrlTest;
				txnUrl = mCredentials.TxnUrlTest;
			}
		}

		/// <summary>
		/// Fills the card info.
		/// </summary>
		/// <param name="pCardData">The card data.</param>
		/// <returns></returns>
		private BankcardTransactionPro FillCardInfo(CreditCard pCardData) {
			BankcardTransactionPro bcTransaction = new BankcardTransactionPro();
			BankcardTransactionDataPro txnData = new BankcardTransactionDataPro();

			txnData.CurrencyCode = com.ipcommerce.Txn.TypeISOCurrencyCodeA3.USD;

			txnData.TransactionDateTime = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss.fffzzz");
			bcTransaction.TenderData = new BankcardTenderData();
			bcTransaction.TenderData.CardData = new CardData();

			if (pCardData.Track2.IsNullOrEmpty()) {
				txnData.EntryMode = com.ipcommerce.Txn.EntryMode.Keyed;
				//bcTransaction.TenderData.CardData.CardType = cardType;
				bcTransaction.TenderData.CardData.Expire = pCardData.CardExpiryAsMMYY;
				bcTransaction.TenderData.CardData.PAN = pCardData.CardNo;
				bcTransaction.TenderData.CardData.CardholderName = pCardData.CardHolder;
			} else {
				txnData.EntryMode = com.ipcommerce.Txn.EntryMode.Track2DataFromMSR;
				bcTransaction.TenderData.CardData.Track2Data = pCardData.Track2;
			}

			if (!pCardData.AvsZip.IsNullOrEmpty() || !pCardData.CVV.IsNullOrEmpty()) {
				bcTransaction.TenderData.CardSecurityData = new CardSecurityData();
				if (!pCardData.CVV.IsNullOrEmpty()) {
					bcTransaction.TenderData.CardSecurityData.CVDataProvided = CVDataProvided.Provided;
					bcTransaction.TenderData.CardSecurityData.CVData = pCardData.CVV;
				}
				if (!pCardData.AvsZip.IsNullOrEmpty()) {
					bcTransaction.TenderData.CardSecurityData.AVSData = new AVSData();
					bcTransaction.TenderData.CardSecurityData.AVSData.PostalCode = pCardData.AvsZip;
				}
			}

			// soft descriptor
			if (pCardData.SoftDescriptor != null) {
				//txnData.AlternativeMerchantData = FillSoftDescriptor(pCardData.SoftDescriptor);
			}

			if (pCardData.AffiliateDriverID.HasValue) {
				txnData.EmployeeId = pCardData.AffiliateDriverID.Value.ToString();// string.Format("AD:{0}", pCardData.AffiliateDriverID.Value);
			} else {
				txnData.EmployeeId = pCardData.DriverID.ToString(); // string.Format("D:{0}", pCardData.DriverID);
			}

			SetCardType(pCardData, bcTransaction);

			bcTransaction.TransactionData = txnData;

			return bcTransaction;
		}

		/// <summary>
		/// Sets the type of the card that is being charged.
		/// </summary>
		/// <param name="pCardData">The p card data.</param>
		/// <param name="bcTransaction">The bc transaction.</param>
		private void SetCardType(CreditCard pCardData, BankcardTransactionPro bcTransaction) {
			if (pCardData.CardType.Equals("Visa", StringComparison.CurrentCultureIgnoreCase)) {
				bcTransaction.TenderData.CardData.CardType = TypeCardType.Visa;
			} else if (pCardData.CardType.Equals("MasterCard", StringComparison.CurrentCultureIgnoreCase)) {
				bcTransaction.TenderData.CardData.CardType = TypeCardType.MasterCard;
			} else if (pCardData.CardType.Equals("AMEX", StringComparison.CurrentCultureIgnoreCase)) {
				bcTransaction.TenderData.CardData.CardType = TypeCardType.AmericanExpress;
			} else if (pCardData.CardType.Equals("Discover", StringComparison.CurrentCultureIgnoreCase)) {
				bcTransaction.TenderData.CardData.CardType = TypeCardType.Discover;
			} else if (pCardData.CardType.Equals("Diners", StringComparison.CurrentCultureIgnoreCase)) {
				bcTransaction.TenderData.CardData.CardType = TypeCardType.DinersClub;
			} else if (pCardData.CardType.Equals("JCB", StringComparison.CurrentCultureIgnoreCase)) {
				bcTransaction.TenderData.CardData.CardType = TypeCardType.JCB;
			}
		}


		/// <summary>
		/// Fills the soft descriptor.
		/// </summary>
		/// <param name="pSoftDescriptor">The soft descriptor info.</param>
		/// <returns></returns>
		private AlternativeMerchantData FillSoftDescriptor(SoftDescriptor pSoftDescriptor) {
			AlternativeMerchantData amd = new AlternativeMerchantData();
			amd.Name = pSoftDescriptor.Name;
			amd.Address = new com.ipcommerce.Txn.AddressInfo();
			amd.Address.City = pSoftDescriptor.City;
			amd.Address.CountryCode = com.ipcommerce.Txn.TypeISOCountryCodeA3.USA;
			amd.Address.StateProvince = pSoftDescriptor.StateProvince;
			amd.Address.Street1 = pSoftDescriptor.Street;
			amd.Address.PostalCode = pSoftDescriptor.PostalCode;
			amd.CustomerServicePhone = pSoftDescriptor.Phone;
			amd.CustomerServiceInternet = pSoftDescriptor.Internet;

			return amd;
		}

		public ProcessorResponse ChargeCard(CreditCard pCardData, decimal pChargeAmount, string pComments, string pVoucherNo, string pService, string pDriverIDs) {
			string url;
			string txnUrl;
			InitURL(pCardData, out url, out txnUrl);

			//skip USAePay if we do not have a URL to post to
			if (url.IsNullOrEmpty()) {
				return null;
			}

			mService = OpenServiceInfoChannel(url, txnUrl);

			BankcardTransaction bcTransaction = FillCardInfo(pCardData);// DataGenerator.SetBankCardTxnData();
			bcTransaction.TransactionData.OrderNumber = pVoucherNo;
			bcTransaction.TransactionData.InvoiceNumber = pVoucherNo;
			bcTransaction.TransactionData.SignatureCaptured = pCardData.CustomerSigned;
			bcTransaction.TransactionData.Amount = pChargeAmount;

			var ipCommResponse = RunTransaction(TrxTypes.Authorization, bcTransaction, null, null, null, false, false);
			return ProcessResponse(ipCommResponse, pChargeAmount, TrxTypes.Sale, pCardData.TestCard);
		}

		public ProcessorResponse ChargeAuth(string pRefNum, decimal pChargeAmount, bool pTestCard, string pDriverID, string pVoucherNo) {
			throw new NotImplementedException();
		}

		public ProcessorResponse Refund(string pRefNum, decimal pRefundAmount, bool pTestCard, string pVoucherNo) {
			throw new NotImplementedException();
		}

		public bool Void(string pRefNum, bool pTestCard, string pVoucherNo) {
			throw new NotImplementedException();
		}

		public string GetTransactionHistory(DateTime pStart, DateTime pEnd) {
			throw new NotImplementedException();
		}


		public BankcardTransactionResponse RunTransaction(string pTransType,
								 BankcardTransaction pBCTransaction,	// Conditional : Only used for an AuthorizeAndCapture, Authorize and ReturnUnlinked. Otherwise null
								 BankcardCapture pBCDifferenceData,		// Conditional : Only used for a Capture. Otherwise null
								 Adjust pADifferenceData,				// Conditional : Only used for an Adjust. Otherwise null
								 BankcardUndo pUDifferenceData,			// Conditional : Only used for an Undo. Otherwise null
								 bool pSendAcknowledge,
								 bool pUseWorkflowId) {

			BankcardTransactionResponse response = new BankcardTransactionResponse();
			try {
				//CheckTokenExpire(); // Make sure the current token is valid

				string serviceIdOrWorkflowId = mCredentials.ServiceId;
				if (pUseWorkflowId) {
					serviceIdOrWorkflowId = mCredentials.WorkflowId;
				}

				switch (pTransType) {
					case TrxTypes.Sale:
						response = mTransService.AuthorizeAndCapture(mSessionToken, pBCTransaction, mCredentials.ApplicationProfileId, mCredentials.MerchantProfileId, serviceIdOrWorkflowId) as BankcardTransactionResponse;
						// Always Verify that the requested amount and approved amount are the same. 
						if (pBCTransaction.TransactionData.Amount != response.Amount) {

						}
						break;

					case TrxTypes.Authorization:
						//pBCTransaction.TenderData.CardData.CardType = TypeCardType.Visa;
						response = mTransService.Authorize(mSessionToken, pBCTransaction, mCredentials.ApplicationProfileId, mCredentials.MerchantProfileId, serviceIdOrWorkflowId) as BankcardTransactionResponse;
						//Always Verify that the requested amount and approved amount are the same. 
						if (pBCTransaction.TransactionData.Amount != response.Amount) {

						}
						break;

				}
				//if (pTransType == TransactionType.Capture) {
				//    response.Add(mTransService.Capture(mSessionToken, pBCDifferenceData, mCredentials.ApplicationProfileId, mCredentials.ServiceId));
				//}
				//if (pTransType == TransactionType.Return) {
				//    response.Add(Cwsbc.ReturnUnlinked(_sessionToken, pBCTransaction, _applicationProfileId, _merchantProfileId, _serviceId));
				//}
				//if (pTransType == TransactionType.Adjust)
				//    response.Add(Cwsbc.Adjust(_sessionToken, pADifferenceData, _applicationProfileId, _serviceId));
				//if (pTransType == TransactionType.Undo) {
				//    if (CredentialRequired())
				//        pUDifferenceData.Addendum = CredentialsRequired(_serviceId, _credUserName, _credPassword);
				//    response.Add(Cwsbc.Undo(_sessionToken, pUDifferenceData, _applicationProfileId, _serviceId));
				//}

				//List<ResponseDetails> RD = new List<ResponseDetails>();//Convert the response to response details so that we can report on the UI
				//if (response != null) {
				//    foreach (Response r in response) {
				//        if (pSendAcknowledge && r.TransactionId.Length > 0)
				//            Cwsbc.Acknowledge(_sessionToken, r.TransactionId, _applicationProfileId, _serviceId);

				//        ResponseDetails RDN = new ResponseDetails(0.00M, r, pTransType.ToString(), _serviceIdOrWorkflowId, _merchantProfileId, true, TypeCardType.NotSet, "");
				//        _message += ProcessResponse(ref RDN);//Pass as reference so we can extract more values from the response
				//        RD.Add(RDN);
				//    }
				//}

				return response;
			} catch (EndpointNotFoundException) {
				//In this case the SvcEndpoint was not available. Try the same logic again with the alternate Endpoint
				//    try {
				//        SetTxnEndpoint();//Change the endpoint to use the backup.

				//        //TODO : Add a copy of the code above once fully tested out.

				//        return null;

				//    } catch (EndpointNotFoundException) {
				//        _message += "Neither the primary or secondary endpoints are available. Unable to process.";
				//    } catch (Exception ex) {
				//        _message += "Unable to AuthorizeAndCapture\r\nError Message : " + ex.Message;
				//    }
				//} catch (System.TimeoutException te) {
				//    //A timeout has occured. Prompt the user if they'd like to query for the last transaction submitted
				//    if (pBCTransaction != null) {
				//        _message += "A timeout has occured. A call to 'RequestTransaction' was made to obtain the transactions that should have been returned. Your code will need to reconcile transactions.";
				//        RequestTransaction(pBCTransaction.TenderData);
				//    } else { throw te; }
			} catch (Exception ex) {
				Console.WriteLine(ex.Message);
				string strErrorId;
				string strErrorMessage;
				IPCommerceFaultHandler faultHandler = new IPCommerceFaultHandler();
				if (faultHandler.handleTxnFault(ex, out strErrorId, out strErrorMessage)) {
					throw new Exception(strErrorMessage, ex);
				} else {
					Console.WriteLine(ex.Message);
				}
			}

			return null;
		}




		private ProcessorResponse ProcessResponse(BankcardTransactionResponse pResponse, decimal pAuthAmount, string pTrxType, bool pTestCard) {
			ProcessorResponse oResult = new ProcessorResponse();

			oResult.TestCard = pTestCard;
			oResult.VerisignAccountID = mCredentials.VerisignAccountID;
			oResult.MerchantID = mCredentials.MerchantID;
			oResult.CreditCardProcessor = mCredentials.MyGateway();

			oResult.Result = pResponse.StatusCode;
			oResult.Reference = pResponse.RetrievalReferenceNumber;
			oResult.Message = pResponse.StatusMessage;
			oResult.Amount = pResponse.Amount;
			if (oResult.Amount == 0) {
				oResult.Amount = pAuthAmount;
			}
			if (oResult.Result.IsNullOrEmpty()) {
				oResult.Result = "0";
			}
			//fixed for Chase results
			if (oResult.Result != "0" && oResult.Message.Contains("Approve")) {
				oResult.Result = "0";
			}

			oResult.AuthCode = pResponse.ApprovalCode;
			if (pResponse.AVSResult == null) {
				oResult.AVS = "";
				oResult.AVSZip = "";
			} else {
				oResult.AVS = pResponse.AVSResult.ActualResult.Substring(0, 1);
				try {
					//oResult.AVSZip = pResponse.AVSResult;
				} catch (Exception) {
					oResult.AVSZip = oResult.AVS;
				}
			}
			oResult.CVV2Match = pResponse.CVResult.ToString();

			oResult.TransType = pTrxType;
			oResult.TransDate = DateTime.Now;

			if (pResponse.AVSResult != null) {
				oResult.AVSResultCode = pResponse.AVSResult.ActualResult;
			}

			return oResult;

		}


		private class BindingFactory {
			internal static Binding CreateInstance() {
				BasicHttpBinding binding = new BasicHttpBinding();
				binding.Security.Mode = BasicHttpSecurityMode.Transport; //.TransportCredentialOnly;
				binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.None;
				binding.UseDefaultWebProxy = true;
				return binding;
			}
		}

		public class ResponseDetails {
			public decimal TxnAmount;
			public Response Response;
			public string TransactionType;
			public string WorkflowId;
			public string MerchantProfileId;
			public bool Verbose;
			public TypeCardType CardType;
			public string MaskedPan;

			public ResponseDetails(decimal txnAmount, Response response, string transactionType, string workflowId, string merchantProfileId, bool verbose,
									TypeCardType cardType, string maskedPan) {
				TxnAmount = txnAmount;
				Response = response;
				TransactionType = transactionType;
				WorkflowId = workflowId;
				MerchantProfileId = merchantProfileId;
				Verbose = verbose;
				CardType = TypeCardType.NotSet;
				MaskedPan = "";
			}
		}


	}
}
