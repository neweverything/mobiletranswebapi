﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace CabRideEngineDapper {
    public class usp_DriverPaymentsPossibleDups {
        public int DifferenceInMinutes { get; set; }
        public string Affiliate { get; set; }
        public string Driver { get; set; }
        public string Handheld { get; set; }
        public string Platform { get; set; }
        public string CardNumberDisplay { get; set; }
        public string TransNo { get; set; }
        public string DupVoucher { get; set; }
        public decimal Fare { get; set; }
        public decimal? DriverTotal { get; set; }
        public DateTime AuthDate { get; set; }
        public DateTime ChargeDate { get; set; }
        public string CardType { get; set; }

        public static List<usp_DriverPaymentsPossibleDups> Execute(SqlConnection pConn, Int64 pMinutesBack, Int64 pMinutesApart) {
            return pConn.Query<usp_DriverPaymentsPossibleDups>("usp_DriverPaymentsPossibleDups", new { MinutesBack = pMinutesBack, MinuteSpan = pMinutesApart }, commandTimeout: 900, commandType: CommandType.StoredProcedure).ToList();
        }
    }
}
