﻿
namespace TaxiPassCommon.Banking {
    public class VerisignCredentials : IGatewayCredentials {

        public VerisignCredentials() {
            CreditCardProcessor = CardProcessors.Verisign;
        }

        public string UserName { get; set; }
        public string Vendor { get; set; }
        public string Partner { get; set; }
        public string Password { get; set; }
        public string ProdURL { get; set; }

        public string TestURL { get; set; }
        public string TestPassword { get; set; }
        public string TestUserName { get; set; }

        public int Port { get; set; }
        public int Timeout { get; set; }

        public long VerisignAccountID { get; set; }
        public string MerchantID { get; set; }

        public bool Validate { get; set; }

        public string TerminalType { get; set; }
        public string CardType { get; set; }

        public int ProcessingOrder { get; set; }
        public CardProcessors CreditCardProcessor { get; set; }

        public CardProcessors MyGateway() {
            return CreditCardProcessor;
        }

    }
}
