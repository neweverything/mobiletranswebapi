﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Util;
using IdeaBlade.Persistence.Rdb;

using CabRideEngine.Utils;
using TaxiPassCommon;
using TaxiPassCommon.Banking;

using Csla.Validation;
using System.Text;

namespace CabRideEngine {
	/// <summary>
	/// The developer class representing the AffiliateBankInfo business object class.
	/// </summary>
	/// <remarks>
	/// <para>Place all application-specific business logic in this class.</para>
	/// <para>
	/// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
	/// and will not overwrite changes.
	/// </para>
	/// <para>
	/// This class inherits from the generated class, its companion in the pair of classes 
	/// defining a business object. Properties in the generated class may be overridden in 
	/// order to change the behavior or add business logic.
	/// </para>
	/// <para>
	/// This class is the final class in the inheritance chain for this business object. 
	/// It should not be inherited.
	/// </para>
	/// </remarks>
	[Serializable]
	public sealed partial class AffiliateBankInfo : AffiliateBankInfoDataRow {

		#region Constructors -- DO NOT MODIFY

		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new';
		// it is also required if the class is visible from a web service.
		// DO NOT REMOVE **
		private AffiliateBankInfo()
			: this(null) {
		}

		/// <summary>
		/// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
		/// </summary>
		/// <param name="pRowBuilder"></param>
		public AffiliateBankInfo(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static AffiliateBankInfo Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      AffiliateBankInfo aAffiliateBankInfo = pManager.CreateEntity<AffiliateBankInfo>();
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aAffiliateBankInfo, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aAffiliateBankInfo.AddToManager();
		//      return aAffiliateBankInfo;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		// Add additional logic to your business object here...
		public static AffiliateBankInfo Create(Affiliate pAffiliate) {
			AffiliateBankInfo oBank = null;
			if (pAffiliate.AffiliateBankInfos.Count > 0) {
				return pAffiliate.AffiliateBankInfos[0];
			}

			try {
				PersistenceManager oPM = pAffiliate.PersistenceManager;

				oBank = oPM.CreateEntity<AffiliateBankInfo>();

				// Using the IdeaBlade Id Generation technique
				oPM.GenerateId(oBank, AffiliateBankInfo.AffiliateBankIDEntityColumn);
				oBank.AffiliateID = pAffiliate.AffiliateID;
				oBank.Country = pAffiliate.Country;
				oBank.State = pAffiliate.State;
				oBank.MerchantID = 1;

				oBank.AddToManager();

			} catch (Exception ex) {
				throw ex;
			}
			return oBank;
		}

		//protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
		//	if (IsDeserializing) {
		//		return;
		//	}
		//	base.OnColumnChanging(pArgs);
		//	DoAudit(pArgs);
		//}

		//private void DoAudit(DataColumnChangeEventArgs pArgs) {
		//	if (!this.PersistenceManager.IsClient) {
		//		return;
		//	}
		//	if (this.RowState == DataRowState.Added) {
		//		return;
		//	}
		//	if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
		//		AuditRecord = AffiliateBankInfoAudit.Create(this.PersistenceManager, this);
		//	}
		//}

		[BindingBrowsable(false)]
		public override string EntityInstanceDisplayName {
			get {
				return this.Name;
			}
		}

		public static AffiliateBankInfo GetByBankInfo(PersistenceManager pPM, string pRouting, string pAccountNo) {
			//RdbQuery qry = new RdbQuery(typeof(AffiliateBankInfo), AffiliateBankInfo.CodeEntityColumn, EntityQueryOp.EQ, pRouting);
			//qry.AddClause(AffiliateBankInfo.AccountNumberEntityColumn, EntityQueryOp.EQ, pAccountNo);
			EntityList<AffiliateBankInfo> oAffiliateBankInfo = GetData(pPM, "", pRouting, pAccountNo);
			if (oAffiliateBankInfo.Count == 0) {
				return pPM.GetNullEntity<AffiliateBankInfo>();
			} else {
				return oAffiliateBankInfo[0];
			}
		}

		//public static AffiliateBankInfo GetForAffiliate(PersistenceManager pPM, long pAffiliateID) {
		//	EntityList<AffiliateBankInfo> oAffiliateBankInfo = GetData(pPM, pAffiliateID.ToString(), "", "");
		//	if (oAffiliateBankInfo.Count == 0) {
		//		return pPM.GetNullEntity<AffiliateBankInfo>();
		//	} else {
		//		return oAffiliateBankInfo[0];
		//	}
		//}

		//// ************************************************************************************************************
		//// Get the data
		//// ************************************************************************************************************
		private static EntityList<AffiliateBankInfo> GetData(PersistenceManager pPM, string pAffiliateID, string pRouting, string pAccountNo) {
			string sDelimiter = "";
			int iArgCount = 0;

			StringBuilder sql = new StringBuilder();
			sql.Append("usp_AffiliateBankInfoGet ");
			if (!pAffiliateID.IsNullOrEmpty()) {
				sql.AppendFormat(" @AffiliateID = {0}", pAffiliateID);
				iArgCount++;
			}
			if (!pRouting.IsNullOrEmpty()) {
				if (iArgCount > 0) {
					sDelimiter = ", ";
				}
				sql.AppendFormat("{0}@Routing = '{1}'", sDelimiter, pRouting);
				iArgCount++;
			}
			if (!pAccountNo.IsNullOrEmpty()) {
				if (iArgCount > 0) {
					sDelimiter = ", ";
				}
				sql.AppendFormat("{0}@AccountNo = '{1}'", sDelimiter, pAccountNo);
				iArgCount++;
			}
			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(AffiliateBankInfo), sql.ToString());
			EntityList<AffiliateBankInfo> oData = pPM.GetEntities<AffiliateBankInfo>(qry);
			//if (oData.Count == 0) {
			//	oData.Add(new AffiliateBankInfo());
			//}
			return oData;
		}


		public PayInfo GetPayInfo() {
			PayInfo oPay = new PayInfo();
			oPay.NachaFile = this.Affiliate.SendNacha;
			oPay.BankAccountHolder = this.AccountHolder;
			oPay.BankAccountNumber = this.AccountNumber;
			oPay.BankAccountPersonal = this.AccountPersonal;
			oPay.BankAddress = this.Address;
			oPay.BankCity = this.City;
			oPay.BankCode = this.Code;
			oPay.BankState = this.State;
			oPay.BankSuite = this.Suite;
			oPay.BankZIPCode = this.ZIPCode;
			if (oPay.MyMerchant == null) {
				oPay.MyMerchant = new Merchant();
			}
			oPay.MyMerchant.MerchantNo = this.MerchantInfo.MerchantNo;
			oPay.PaidTo = "Affiliate: " + this.Affiliate.Name;

			oPay.WireTransfer = this.Affiliate.ACHWireTransfer;
			return oPay;
		}


		public string ValidatedByAndDate {
			get {
				if (ValidatedBy.IsNullOrEmpty() || !ValidatedDate.HasValue) {
					return "";
				}
				return string.Format("{0} on {1}", ValidatedBy, ValidatedDate.Value.ToString("MM/dd/yyyy"));
			}
		}

		public bool BankInfoChanged { get; set; }
		public bool ValidateAccountNumbers { get; set; }
		public string ConfirmAccountNumber { get; set; }

		public override string AccountNumber {
			get {
				return base.AccountNumber;
			}
			set {
				if (!IsDeserializing) {
					ValidateAccountNumbers = (value != base.AccountNumber);
					if (ValidateAccountNumbers) {
						ClearValidateByInfo();
					}
				}
				base.AccountNumber = value;
			}
		}

		public override string Code {
			get {
				return base.Code;
			}
			set {
				if (!IsDeserializing) {
					if(value != base.Code) {
						ClearValidateByInfo();
					}
				}
				base.Code = value;
			}
		}

		public override string AccountHolder {
			get {
				return base.AccountHolder;
			}
			set {
				if (value != base.AccountHolder) {
					ClearValidateByInfo();
				}
				base.AccountHolder = value;
			}
		}

		public override string VoidedCheck {
			get {
				return base.VoidedCheck;
			}
			set {
				if (value != base.VoidedCheck) {
					ClearValidateByInfo();
				}
				base.VoidedCheck = value;
			}
		}
		private void ClearValidateByInfo() {
			ValidatedBy = "";
			ValidatedDate = null;
		}


	}

	#region EntityPropertyDescriptors.AffiliateBankInfoPropertyDescriptor
	////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
	////    For example: 
	////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
	//public static partial class EntityPropertyDescriptors {
	//	public partial class AffiliateBankInfoPropertyDescriptor : BaseEntityPropertyDescriptor {
	//    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
	//    public AdaptedPropertyDescriptor Age {
	//      get { return this.Get("Age"); }
	//    }
	//  }
	//} 
	#endregion

}
