using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Runtime.Serialization;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

namespace CabRideEngine {
 
  #region DriverPaymentChargesDataTable
  /// <summary>
  /// The generated <see cref="T:IdeaBlade.Persistence.EntityTable"/> class  
  /// containing all <see cref="DriverPaymentCharges"/> objects.
  /// </summary>
  /// <remarks>
  /// ****************************************************
  /// <para>** DO NOT MODIFY THIS CLASS - AutoGenerated Code</para>
  /// <para>** Any changes made to this code will be lost</para>
  /// <para>** the next time this code is regenerated.</para>
  /// <para>**</para>
  /// <para>** Generated from:   EntityBaseclass.template</para>
  /// <para>** Last modified:    2014-04-16T09:38:18.7938718-07:00</para>
  /// <para>** Template version: 3.110127</para>
  /// ****************************************************
  /// </remarks>
  [BindingBrowsable(false)]
  [Serializable]
  public partial class DriverPaymentChargesDataTable : BaseEntityDataTable, 
     IdeaBlade.Persistence.Rdb.IRdbTable {
  
    /// <summary>
    /// Initializes a new instance of the DriverPaymentChargesDataTable class with no arguments.
    /// </summary>
    public DriverPaymentChargesDataTable() {}

    /// <summary>
    /// Initializes a new instance of the DriverPaymentChargesDataTable class 
    /// with the <see cref="System.Runtime.Serialization.SerializationInfo"/> 
    /// and the <see cref="System.Runtime.Serialization.StreamingContext"/>.
    /// </summary>
    /// <param name="pInfo">The data needed to serialize or deserialize an object.</param>
    /// <param name="pContext">The source and destination of a given serialized stream.</param>
    protected DriverPaymentChargesDataTable(SerializationInfo pInfo, StreamingContext pContext) :
      base(pInfo, pContext) {}

    /// <summary>
    /// Creates a new instance of the <see cref="DriverPaymentChargesDataRow"/>.
    /// Constructs a row from the builder.
    /// <para>** For internal use only. **</para>
    /// </summary>
    /// <param name="pRowBuilder"></param>
    /// <returns>The <see cref="DriverPaymentChargesDataRow"/>.</returns>
    protected override DataRow NewRowFromBuilder(DataRowBuilder pRowBuilder) {
      return new DriverPaymentCharges(pRowBuilder);
    }

    /// <summary>Returns the <see cref="System.Type"/> of <see cref="DriverPaymentCharges"/>.</summary>
    protected override Type GetRowType() {
      return typeof(DriverPaymentCharges);
    }
    
    /// <summary>
    /// Gets the name of the <see cref="T:IdeaBlade.Rdb.RdbKey"/> or 
    /// <see cref="T:IdeaBlade.Persistence.WS.WsKey"/> defined in <b>IdeaBlade.ibconfig</b>. 
    /// </summary>
    /// <remarks>For this <see cref="DriverPaymentCharges"/>, the DataSourceKeyName="default".</remarks>
    public override String DataSourceKeyName {
      get { return @"default"; }
    }

    #region Column properties
    //**************************************
    //* Column properties
    //**************************************
    /// <summary>Gets the DriverPaymentChargeID <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn DriverPaymentChargeIDColumn {
      get {
        if (mDriverPaymentChargeIDColumn!=null) return mDriverPaymentChargeIDColumn;
        mDriverPaymentChargeIDColumn = GetColumn("DriverPaymentChargeID", true);
        return mDriverPaymentChargeIDColumn;
      }
    }
    private DataColumn mDriverPaymentChargeIDColumn;
    
    /// <summary>Gets the DriverPaymentID <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn DriverPaymentIDColumn {
      get {
        if (mDriverPaymentIDColumn!=null) return mDriverPaymentIDColumn;
        mDriverPaymentIDColumn = GetColumn("DriverPaymentID", true);
        return mDriverPaymentIDColumn;
      }
    }
    private DataColumn mDriverPaymentIDColumn;
    
    /// <summary>Gets the ChargeType <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn ChargeTypeColumn {
      get {
        if (mChargeTypeColumn!=null) return mChargeTypeColumn;
        mChargeTypeColumn = GetColumn("ChargeType", true);
        return mChargeTypeColumn;
      }
    }
    private DataColumn mChargeTypeColumn;
    
    /// <summary>Gets the Amount <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn AmountColumn {
      get {
        if (mAmountColumn!=null) return mAmountColumn;
        mAmountColumn = GetColumn("Amount", true);
        return mAmountColumn;
      }
    }
    private DataColumn mAmountColumn;
    
    /// <summary>Gets the RowVersion <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn RowVersionColumn {
      get {
        if (mRowVersionColumn!=null) return mRowVersionColumn;
        mRowVersionColumn = GetColumn("RowVersion", true);
        return mRowVersionColumn;
      }
    }
    private DataColumn mRowVersionColumn;
    
    /// <summary>Gets the ModifiedBy <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn ModifiedByColumn {
      get {
        if (mModifiedByColumn!=null) return mModifiedByColumn;
        mModifiedByColumn = GetColumn("ModifiedBy", true);
        return mModifiedByColumn;
      }
    }
    private DataColumn mModifiedByColumn;
    
    /// <summary>Gets the ModifiedDate <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn ModifiedDateColumn {
      get {
        if (mModifiedDateColumn!=null) return mModifiedDateColumn;
        mModifiedDateColumn = GetColumn("ModifiedDate", true);
        return mModifiedDateColumn;
      }
    }
    private DataColumn mModifiedDateColumn;
    
    #endregion    

    /// <summary>Creates the schema mapping information for this DriverPaymentChargesDataTable.</summary>
    /// <returns>
    /// The <see cref="T:IdeaBlade.Persistence.TableMappingInfo"/> for this DriverPaymentChargesDataTable.
    /// </returns>
    protected override TableMappingInfo CreateTableMappingInfo() {
      RdbTableMappingInfo mappingInfo = new RdbTableMappingInfo(this);
      mappingInfo.SourceOwnerName = "dbo"; 
      mappingInfo.SourceTableName = "DriverPaymentCharges"; 
      mappingInfo.ConcurrencyColumnName = "RowVersion"; 
      // ColumnSelectionOption: selectAllColumns
      mappingInfo.SourceColumnNames = "*"; 
   
      DataColumnMappingCollection columnMappings = mappingInfo.TableMapping.ColumnMappings;
      columnMappings.Clear();
      columnMappings.Add("DriverPaymentChargeID", "DriverPaymentChargeID");
      columnMappings.Add("DriverPaymentID", "DriverPaymentID");
      columnMappings.Add("ChargeType", "ChargeType");
      columnMappings.Add("Amount", "Amount");
      columnMappings.Add("RowVersion", "RowVersion");
      columnMappings.Add("ModifiedBy", "ModifiedBy");
      columnMappings.Add("ModifiedDate", "ModifiedDate");
      return mappingInfo;
    }
  
    /// <summary>** For internal use only. **</summary>
    protected override void UpdateTableMappingInfo() {
      base.UpdateTableMappingInfo();
      this.TableMappingInfo.ColumnUpdateMap["RowVersion"] = "RowVersion + 1";
    }

    /// <summary>
    /// Initializes DriverPaymentCharges <see cref="System.Data.DataColumn"/> properties.
    /// </summary>
    public override void InitColumnProperties() {
      base.InitColumnProperties();

      DriverPaymentChargeIDColumn.Caption = "DriverPaymentChargeID";
      DriverPaymentIDColumn.Caption = "DriverPaymentID";
      ChargeTypeColumn.Caption = "ChargeType";
      AmountColumn.Caption = "Amount";
      RowVersionColumn.Caption = "RowVersion";
      ModifiedByColumn.Caption = "ModifiedBy";
      ModifiedDateColumn.Caption = "ModifiedDate";
    }
  }
  #endregion
  
  #region DriverPaymentChargesDataRow
  /// <summary>
  /// The generated base class for <see cref="DriverPaymentCharges"/>.
  /// </summary>
  /// <remarks>
  /// <para>
  /// It represents a row of persistable data in the <see cref="DriverPaymentChargesDataTable"/>
  /// and derives from <see cref="T:IdeaBlade.Persistence.Entity"/>.
	/// </para>
  /// ****************************************************
  /// <para>** DO NOT MODIFY THIS CLASS - AutoGenerated Code</para>
  /// <para>** Any changes made to this code will be lost</para>
  /// <para>** the next time this code is regenerated.</para>
  /// <para>**</para>
  /// <para>** Generated from:   EntityBaseclass.template</para>
  /// <para>** Last modified:    2014-04-16T09:38:18.7938718-07:00</para>
  /// <para>** Template version: 3.110127</para>
  /// ****************************************************
  /// </remarks>  
  [Serializable]
  public abstract partial class DriverPaymentChargesDataRow : BaseEntity, 
     IdeaBlade.Persistence.Rdb.IRdbEntity {
  
    /// <summary>
    /// Initializes a new instance of the DriverPaymentChargesDataRow.
    /// Constructs a row from the builder.
    /// <para>** For internal use only. **</para>
    /// </summary>
    /// <param name="pRowBuilder"></param>
    protected DriverPaymentChargesDataRow(DataRowBuilder pRowBuilder)
      : base(pRowBuilder) {
    }

    /// <summary>Gets the <see cref="DriverPaymentChargesDataTable"/>.</summary>
    [BindingBrowsable(false)]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public new DriverPaymentChargesDataTable TypedTable {
      get { return (DriverPaymentChargesDataTable) base.Table; }
    }

    /// <summary>Returns the <see cref="System.Type"/> of <see cref="DriverPaymentChargesDataTable"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public static Type GetTableType() {
      return typeof(DriverPaymentChargesDataTable);
    }

    /// <summary>
    /// Gets the SQL 'where' clause used to restrict which 
    /// database records are accessible via this entity. 
    /// </summary>
    /// <remarks>
    /// The default property value is defined in the Current Class 
    /// Properties tab in the <b>IdeaBlade DevForce Object Mapping Tool</b>.
    /// <para>
    /// The 'where' clause will be used, in addition to any other 
    /// query-specific criteria, in every retrieval of entities of this type.
    /// </para>
    /// </remarks>
    [BindingBrowsable(false)]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public virtual string SqlWhereClause {
      get { return ""; }
    }
    
    /// <summary>
    /// Determines whether to use the 'distinct' keyword when 
    /// generating the SQL select clause for objects of this type.
    /// </summary>
    /// <remarks>Defaults to false, but may be overridden in subclasses.</remarks>
    [BindingBrowsable(false)]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public virtual bool ForceSqlDistinct {
      get { return false; }
    }
    

    #region Enums
    //**************************************
    //* Enums
    //**************************************
    #endregion
	
    #region Relation properties
    //**************************************
    //* Relation properties
    //**************************************
    #endregion
    
    #region EntityColumn definitions
    //**************************************
    //* Static EntityColumn definitions
    //**************************************
    /// <summary>The DriverPaymentChargeID <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn DriverPaymentChargeIDEntityColumn =
      new EntityColumn(typeof(DriverPaymentCharges), "DriverPaymentChargeID", typeof(System.Int64), false, true, true, DataSourceAccessType.ReadWrite);      
    /// <summary>The DriverPaymentID <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn DriverPaymentIDEntityColumn =
      new EntityColumn(typeof(DriverPaymentCharges), "DriverPaymentID", typeof(System.Int64), false, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The ChargeType <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn ChargeTypeEntityColumn =
      new EntityColumn(typeof(DriverPaymentCharges), "ChargeType", typeof(System.String), false, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The Amount <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn AmountEntityColumn =
      new EntityColumn(typeof(DriverPaymentCharges), "Amount", typeof(System.Decimal), false, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The RowVersion <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn RowVersionEntityColumn =
      new EntityColumn(typeof(DriverPaymentCharges), "RowVersion", typeof(System.Int32), false, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The ModifiedBy <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn ModifiedByEntityColumn =
      new EntityColumn(typeof(DriverPaymentCharges), "ModifiedBy", typeof(System.String), true, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The ModifiedDate <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn ModifiedDateEntityColumn =
      new EntityColumn(typeof(DriverPaymentCharges), "ModifiedDate", typeof(System.DateTime), true, false, false, DataSourceAccessType.ReadWrite);      
    #endregion
    
    #region Properties
    //**************************************
    //* DriverPaymentChargeID methods
    //**************************************
    /// <summary>Gets the DriverPaymentChargeID <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn DriverPaymentChargeIDColumn {
      get { return TypedTable.DriverPaymentChargeIDColumn; }
    }

    /// <summary>Gets or sets the DriverPaymentChargeID.</summary>
    [DBDataType(typeof(System.Int64))]
    public virtual System.Int64 DriverPaymentChargeID {
      get { 
        System.Int64 result_;
        if (GetInterceptor<System.Int64>("DriverPaymentChargeID", GetDriverPaymentChargeIDImpl, out result_)) return result_;
        return GetDriverPaymentChargeIDImpl();
      }
      set { 
          if (!SetInterceptor<System.Int64>("DriverPaymentChargeID", value, SetDriverPaymentChargeIDImpl)) {
            SetDriverPaymentChargeIDImpl(value);
          }  
      }    
    }
    private System.Int64 GetDriverPaymentChargeIDImpl() {
      return (System.Int64) GetColumnValue(DriverPaymentChargeIDColumn, typeof(System.Int64), false); 
    }
    private void SetDriverPaymentChargeIDImpl(System.Int64 value) {
      SetColumnValue(DriverPaymentChargeIDColumn, value);
    }
    
    //**************************************
    //* DriverPaymentID methods
    //**************************************
    /// <summary>Gets the DriverPaymentID <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn DriverPaymentIDColumn {
      get { return TypedTable.DriverPaymentIDColumn; }
    }

    /// <summary>Gets or sets the DriverPaymentID.</summary>
    [DBDataType(typeof(System.Int64))]
    public virtual System.Int64 DriverPaymentID {
      get { 
        System.Int64 result_;
        if (GetInterceptor<System.Int64>("DriverPaymentID", GetDriverPaymentIDImpl, out result_)) return result_;
        return GetDriverPaymentIDImpl();
      }
      set { 
          if (!SetInterceptor<System.Int64>("DriverPaymentID", value, SetDriverPaymentIDImpl)) {
            SetDriverPaymentIDImpl(value);
          }  
      }    
    }
    private System.Int64 GetDriverPaymentIDImpl() {
      return (System.Int64) GetColumnValue(DriverPaymentIDColumn, typeof(System.Int64), false); 
    }
    private void SetDriverPaymentIDImpl(System.Int64 value) {
      SetColumnValue(DriverPaymentIDColumn, value);
    }
    
    //**************************************
    //* ChargeType methods
    //**************************************
    /// <summary>Gets the ChargeType <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn ChargeTypeColumn {
      get { return TypedTable.ChargeTypeColumn; }
    }

    /// <summary>Gets or sets the ChargeType.</summary>
    [MaxTextLength(50)]
    [DBDataType(typeof(System.String))]
    public virtual System.String ChargeType {
      get { 
        System.String result_;
        if (GetInterceptor<System.String>("ChargeType", GetChargeTypeImpl, out result_)) return result_;
        return GetChargeTypeImpl();
      }
      set { 
          if (!SetInterceptor<System.String>("ChargeType", value, SetChargeTypeImpl)) {
            SetChargeTypeImpl(value);
          }  
      }    
    }
    private System.String GetChargeTypeImpl() {
      return (System.String) GetColumnValue(ChargeTypeColumn, typeof(System.String), false); 
    }
    private void SetChargeTypeImpl(System.String value) {
      SetColumnValue(ChargeTypeColumn, value);
    }
    
    //**************************************
    //* Amount methods
    //**************************************
    /// <summary>Gets the Amount <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn AmountColumn {
      get { return TypedTable.AmountColumn; }
    }

    /// <summary>Gets or sets the Amount.</summary>
    [DBDataType(typeof(System.Decimal))]
    public virtual System.Decimal Amount {
      get { 
        System.Decimal result_;
        if (GetInterceptor<System.Decimal>("Amount", GetAmountImpl, out result_)) return result_;
        return GetAmountImpl();
      }
      set { 
          if (!SetInterceptor<System.Decimal>("Amount", value, SetAmountImpl)) {
            SetAmountImpl(value);
          }  
      }    
    }
    private System.Decimal GetAmountImpl() {
      return (System.Decimal) GetColumnValue(AmountColumn, typeof(System.Decimal), false); 
    }
    private void SetAmountImpl(System.Decimal value) {
      SetColumnValue(AmountColumn, value);
    }
    
    //**************************************
    //* RowVersion methods
    //**************************************
    /// <summary>Gets the RowVersion <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn RowVersionColumn {
      get { return TypedTable.RowVersionColumn; }
    }

    /// <summary>Gets or sets the RowVersion.</summary>
    [DBDataType(typeof(System.Int32))]
    public virtual System.Int32 RowVersion {
      get { 
        System.Int32 result_;
        if (GetInterceptor<System.Int32>("RowVersion", GetRowVersionImpl, out result_)) return result_;
        return GetRowVersionImpl();
      }
      set { 
          if (!SetInterceptor<System.Int32>("RowVersion", value, SetRowVersionImpl)) {
            SetRowVersionImpl(value);
          }  
      }    
    }
    private System.Int32 GetRowVersionImpl() {
      return (System.Int32) GetColumnValue(RowVersionColumn, typeof(System.Int32), false); 
    }
    private void SetRowVersionImpl(System.Int32 value) {
      SetColumnValue(RowVersionColumn, value);
    }
    
    //**************************************
    //* ModifiedBy methods
    //**************************************
    /// <summary>Gets the ModifiedBy <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn ModifiedByColumn {
      get { return TypedTable.ModifiedByColumn; }
    }

    /// <summary>Gets or sets the ModifiedBy.</summary>
    [MaxTextLength(200)]
    [DBDataType(typeof(System.String))]
    public virtual System.String ModifiedBy {
      get { 
        System.String result_;
        if (GetInterceptor<System.String>("ModifiedBy", GetModifiedByImpl, out result_)) return result_;
        return GetModifiedByImpl();
      }
      set { 
          if (!SetInterceptor<System.String>("ModifiedBy", value, SetModifiedByImpl)) {
            SetModifiedByImpl(value);
          }  
      }    
    }
    private System.String GetModifiedByImpl() {
      return (System.String) GetColumnValue(ModifiedByColumn, typeof(System.String), true); 
    }
    private void SetModifiedByImpl(System.String value) {
      SetColumnValue(ModifiedByColumn, value);
    }
    
    //**************************************
    //* ModifiedDate methods
    //**************************************
    /// <summary>Gets the ModifiedDate <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn ModifiedDateColumn {
      get { return TypedTable.ModifiedDateColumn; }
    }

    /// <summary>Gets or sets the ModifiedDate.</summary>
    [DBDataType(typeof(System.DateTime))]
    public virtual Nullable<System.DateTime> ModifiedDate {
      get { 
        Nullable<System.DateTime> result_;
        if (GetInterceptor<Nullable<System.DateTime>>("ModifiedDate", GetModifiedDateImpl, out result_)) return result_;
        return GetModifiedDateImpl();
      }
      set { 
          if (!SetInterceptor<Nullable<System.DateTime>>("ModifiedDate", value, SetModifiedDateImpl)) {
            SetModifiedDateImpl(value);
          }
      }    
    }
    private Nullable<System.DateTime> GetModifiedDateImpl() {
      return (Nullable<System.DateTime>) GetColumnValue(ModifiedDateColumn, typeof(System.DateTime), true); 
    }
    private void SetModifiedDateImpl(Nullable<System.DateTime> value) {
      SetColumnValue(ModifiedDateColumn, value);
    }
    
    #endregion

  }
  #endregion

  #region EntityPropertyDescriptors.DriverPaymentChargesPropertyDescriptor
  public static partial class EntityPropertyDescriptors {

    /// <summary>
    /// Gets the DriverPaymentChargesPropertyDescriptor for <see cref="DriverPaymentCharges"/>.
    /// </summary>
    public static DriverPaymentChargesPropertyDescriptor DriverPaymentCharges {
      get { return DriverPaymentChargesPropertyDescriptor.Instance; }
    }
    
    /// <summary>
    /// The generated class containing <see cref="DriverPaymentCharges"/> PropertyDescriptors.
    /// </summary>
    public partial class DriverPaymentChargesPropertyDescriptor : BaseEntityPropertyDescriptor {
	
			/// <summary>
      /// Initializes a new instance of the DriverPaymentChargesPropertyDescriptor class. 
      /// </summary>
      /// <param name="pParentDescriptor">The parent <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/></param>
      /// <param name="pPropertyPath">The type of the property value</param>
			public DriverPaymentChargesPropertyDescriptor(AdaptedPropertyDescriptor pParentDescriptor, String pPropertyPath)
        : base(pParentDescriptor, pPropertyPath) {}
		
      /// <summary>
      /// Initializes a new instance of the DriverPaymentChargesPropertyDescriptor class. 
      /// </summary>
      /// <param name="pComponentType">The type of object to which the property is bound</param>
      protected DriverPaymentChargesPropertyDescriptor(Type pComponentType)
        : base(pComponentType) {}

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for DriverPaymentChargeID.</summary>
      public AdaptedPropertyDescriptor DriverPaymentChargeID {
        get { return Get("DriverPaymentChargeID"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for DriverPaymentID.</summary>
      public AdaptedPropertyDescriptor DriverPaymentID {
        get { return Get("DriverPaymentID"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for ChargeType.</summary>
      public AdaptedPropertyDescriptor ChargeType {
        get { return Get("ChargeType"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Amount.</summary>
      public AdaptedPropertyDescriptor Amount {
        get { return Get("Amount"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for RowVersion.</summary>
      public AdaptedPropertyDescriptor RowVersion {
        get { return Get("RowVersion"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for ModifiedBy.</summary>
      public AdaptedPropertyDescriptor ModifiedBy {
        get { return Get("ModifiedBy"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for ModifiedDate.</summary>
      public AdaptedPropertyDescriptor ModifiedDate {
        get { return Get("ModifiedDate"); }
      }

     internal new static DriverPaymentChargesPropertyDescriptor Instance = new DriverPaymentChargesPropertyDescriptor(typeof(DriverPaymentCharges));
    }
  }
  #endregion
  
}
