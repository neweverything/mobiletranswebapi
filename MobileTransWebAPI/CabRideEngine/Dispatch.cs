﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

using TaxiPassCommon;

namespace CabRideEngine {
	/// <summary>
	/// The developer class representing the Dispatch business object class.
	/// </summary>
	/// <remarks>
	/// <para>Place all application-specific business logic in this class.</para>
	/// <para>
	/// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
	/// and will not overwrite changes.
	/// </para>
	/// <para>
	/// This class inherits from the generated class, its companion in the pair of classes 
	/// defining a business object. Properties in the generated class may be overridden in 
	/// order to change the behavior or add business logic.
	/// </para>
	/// <para>
	/// This class is the final class in the inheritance chain for this business object. 
	/// It should not be inherited.
	/// </para>
	/// </remarks>
	[Serializable]
	public sealed partial class Dispatch : DispatchDataRow {

		#region Constructors -- DO NOT MODIFY

		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new';
		// it is also required if the class is visible from a web service.
		// DO NOT REMOVE **
		private Dispatch()
			: this(null) {
		}

		/// <summary>
		/// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
		/// </summary>
		/// <param name="pRowBuilder"></param>
		public Dispatch(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static Dispatch Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      Dispatch aDispatch = pManager.CreateEntity<Dispatch>();
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aDispatch, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aDispatch.AddToManager();
		//      return aDispatch;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		// Add additional logic to your business object here...
		public static Dispatch GetReservation(PersistenceManager pPM, long pReserveID) {
			RdbQuery qry = new RdbQuery(typeof(Dispatch), Dispatch.ReservationIDEntityColumn, EntityQueryOp.EQ, pReserveID);
			return pPM.GetEntity<Dispatch>(qry);
		}

		public static EntityList<Dispatch> GetReservationsByDate(PersistenceManager pPM, DateTime pStartDate, DateTime pEndDate, AffiliateStatus pStatus, QueryStrategy pQueryStrategy) {
			RdbQuery qry = new RdbQuery(typeof(Dispatch), Dispatch.PickUpDateEntityColumn, EntityQueryOp.GE, pStartDate);
			qry.AddClause(PickUpDateEntityColumn, EntityQueryOp.LE, pEndDate);
			switch (pStatus) {
				case AffiliateStatus.unconfirmedRides:
					//qry.AddClause(Dispatch.AffiliateConfirmationEntityColumn, EntityQueryOp.IsNull, true);
					//qry.AddClause(Dispatch.AffiliateConfirmationEntityColumn, EntityQueryOp.EQ, "");
					//qry.AddOperator(EntityBooleanOp.Or);
					//qry.AddClause(Dispatch.AffiliateCancelConfirmationEntityColumn, EntityQueryOp.IsNull, true);
					//qry.AddClause(Dispatch.AffiliateCancelConfirmationEntityColumn, EntityQueryOp.EQ, "");
					//qry.AddOperator(EntityBooleanOp.Or);
					qry.AddClause(Dispatch.ConfirmationDateEntityColumn, EntityQueryOp.IsNull, true);
					qry.AddClause(Dispatch.CancelledEntityColumn, EntityQueryOp.EQ, false);
					qry.AddClause(Dispatch.AffiliateDeniedEntityColumn, EntityQueryOp.EQ, true);
					qry.AddOperator(EntityBooleanOp.And);
					qry.AddOperator(EntityBooleanOp.Or);
					break;
				case AffiliateStatus.confirmedRides:
					qry.AddClause(CabRideEngine.Dispatch.AffiliateConfirmationEntityColumn, EntityQueryOp.NE, "");
					break;
			}
			qry.AddOrderBy(Dispatch.UTCPUDateTimeEntityColumn, System.ComponentModel.ListSortDirection.Ascending);
			return pPM.GetEntities<Dispatch>(qry, pQueryStrategy);
		}

		//needed as UltraWebGrid does not pick up Customer.AccountNo
		public string AffiliateName {
			get {
				return this.Affiliate.Name;
			}
		}

		public string AffiliateNumber {
			get {
				return this.Affiliate.ReservePhone.FormattedPhoneNumber();
			}
		}

		public string AffiliateConfirmationStatus {
			get {
				string confNo = this.AffiliateConfirmation;
				if (this.Cancelled) {
					confNo = this.AffiliateCancelConfirmation;
				}
				return confNo;
			}
		}

		//public DateTime UniversalPickUpDate {
		//    get {
		//        TimeZoneConverter oTime = new TimeZoneConverter();
		//        return oTime.oUniversalTime(this.Affiliate.TimeZone, this.PickUpDate);
		//    }
		//}

		//needed as UltraWebGrid does not pick up Customer.AccountNo
		public string CustomerAccountNo {
			get {
				return this.Customer.AccountNo;
			}
		}


		public string AffiliateInfoWeb {
			get {
				string sText = AffiliateNumber + "<br />";
				sText += this.Affiliate.AccountNo + "<br />";
				sText += this.AffiliateName;
				return sText;
			}
		}

		public string PickUpInfoShort {
			get {
				foreach (Routing oRoute in this.Routings) {
					if (oRoute.Type == Routing_Types.PickUp) {
						return oRoute.FormattedAddressShort;
					}
				}
				return "";
			}
		}

		public string DropOffInfoShort {
			get {
				foreach (Routing oRoute in this.Routings) {
					if (oRoute.Type == Routing_Types.DropOff) {
						return oRoute.FormattedAddressShort;
					}
				}
				return "";
			}
		}

		public string DispatchPUDateTime {
			get {
				string sTimeZone = this.Affiliate.TimeZone;
				TimeZones oZone = TimeZones.GetTimeZone(sTimeZone);
				if (oZone == null) {
					sTimeZone = "";
				} else {
					sTimeZone = oZone.StandardAbbr;
					if (this.PickUpDate.IsDaylightSavingTime()) {
						sTimeZone = oZone.DayLightAbbr;
					}
				}

				string sDate = PickUpDate.ToShortTimeString() + " " + sTimeZone + "<br />";
				sDate += PickUpDate.ToString("dd/MM") + "<br />";
				sDate += PickUpDate.DayOfWeek.ToString().Substring(0, 3);

				return sDate;
			}
		}

		public string DispatchRideStatus {
			get {
				string myStatus = Status;
				if (string.IsNullOrEmpty(this.AffiliateConfirmation)) {
					myStatus += " and unconfirmed";
				} else {
					myStatus += " and confirmed";
				}

				//if (IsCreditCardCharged) {
				//    myStatus = "Invoiced";
				//}
				return myStatus;
			}
		}


		public static EntityList<Dispatch> GetReservationsByDateByAffiliate(Affiliate pAffiliate, DateTime pStartDate, DateTime pEndDate, AffiliateStatus pStatus, QueryStrategy pQueryStrategy) {
			RdbQuery qry = new RdbQuery(typeof(Dispatch), Dispatch.PickUpDateEntityColumn, EntityQueryOp.GE, pStartDate);
			qry.AddClause(PickUpDateEntityColumn, EntityQueryOp.LE, pEndDate);
			qry.AddClause(Dispatch.AffiliateIDEntityColumn, EntityQueryOp.EQ, pAffiliate.AffiliateID);
			switch (pStatus) {
				case AffiliateStatus.unconfirmedRides:
					qry.AddClause(Dispatch.AffiliateConfirmationEntityColumn, EntityQueryOp.IsNull, true);
					qry.AddClause(Dispatch.AffiliateConfirmationEntityColumn, EntityQueryOp.EQ, "");
					qry.AddOperator(EntityBooleanOp.Or);
					qry.AddClause(Dispatch.AffiliateCancelConfirmationEntityColumn, EntityQueryOp.IsNull, true);
					qry.AddClause(Dispatch.AffiliateCancelConfirmationEntityColumn, EntityQueryOp.EQ, "");
					qry.AddOperator(EntityBooleanOp.Or);
					break;
				case AffiliateStatus.confirmedRides:
					qry.AddClause(CabRideEngine.Dispatch.AffiliateConfirmationEntityColumn, EntityQueryOp.NE, "");
					break;
			}
			qry.AddClause(Dispatch.CancelledEntityColumn, EntityQueryOp.EQ, false);
			qry.AddOrderBy(Dispatch.UTCPUDateTimeEntityColumn, System.ComponentModel.ListSortDirection.Ascending);
			return pAffiliate.PersistenceManager.GetEntities<Dispatch>(qry, pQueryStrategy);
		}

		public static EntityList<Dispatch> GetReservationsByDateByAccount(PersistenceManager pPM, long pAccountID, DateTime pStartDate, DateTime pEndDate, AffiliateStatus pStatus, QueryStrategy pQueryStrategy) {
			string sql = @"SELECT * FROM Reservations
							WHERE PickUpDate >= '{0}'
								AND PickUpDate <= '{1}'
								AND AccountID = {2}
								AND CASE WHEN {3} = 0
											  AND ISNULL(AffiliateConfirmation, '') = ''
											  AND ISNULL(AffiliateCancelConfirmation, '') = '' THEN 1
										 WHEN {3} = 1
											  AND ISNULL(AffiliateConfirmation, '') != '' THEN 1
										 WHEN {3} = 2 THEN 1
									END = 1
								AND Cancelled = 0
							ORDER BY UTCPUDateTime";
			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(Dispatch), string.Format(sql, pStartDate, pEndDate, pAccountID, (int)pStatus));
			return pPM.GetEntities<Dispatch>(qry, pQueryStrategy);
		}


	}

	public enum AffiliateStatus {
		unconfirmedRides,
		confirmedRides,
		allRides
	}


	#region EntityPropertyDescriptors.DispatchPropertyDescriptor
	////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
	////    For example: 
	////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
	//public static partial class EntityPropertyDescriptors {
	//	public partial class DispatchPropertyDescriptor : BaseEntityPropertyDescriptor {
	//    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
	//    public AdaptedPropertyDescriptor Age {
	//      get { return this.Get("Age"); }
	//    }
	//  }
	//} 
	#endregion

}
