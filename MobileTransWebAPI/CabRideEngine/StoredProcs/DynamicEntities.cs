﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Collections;

using IdeaBlade.Persistence;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Rdb;
using System.Data;

namespace CabRideEngine {
	public static class DynamicEntities {

		// *****************************************************************************************************
		// Generic query caller
		// *****************************************************************************************************
		private static int iLoop = 0;

		public static Entity[] DynamicDBCall(PersistenceManager pPM, string pName, string pSQL) {
			Entity[] oRet = null;

			while (true) {
				try {
					Type oGenericType;
					var oDynamics = EntityTypeInfo.GetAllDynamicTypes();
					var x = (from p in oDynamics
							where p.EntityType.Name == pName
							select p).FirstOrDefault();
					if (x == null) {
						oGenericType = DynamicEntityTypeBuilder.CreateType(pName, "Default");
					} else {
						oGenericType = x.EntityType;
					}

					IEntityQuery qry = new PassthruRdbQuery(oGenericType, pSQL);
					qry.CommandTimeout = 300;
					oRet = pPM.GetEntities(qry);
					oGenericType = null;
					break;
				} catch (Exception ex) {
					if (ex.Message.ToLower().Contains("dynamic type:") && ex.Message.ToLower().Contains(" already exists")) {
						iLoop++;
					} else {
						break;
					}
				}
			}

			return oRet;
		}

		public static Entity[] DynamicDBCall(PersistenceManager pPM, string pSQL) {
			Entity[] oRet = null;
			// multi threaded can cause issues here so process a while loop
			while (true) {
				try {
					EntityTypeInfo.GetAllDynamicTypes();
					var oGenericType = DynamicEntityTypeBuilder.CreateType("GenericType" + iLoop.ToString(), "Default");
					
					IEntityQuery qry = new PassthruRdbQuery(oGenericType, pSQL);
					qry.CommandTimeout = 300;
					oRet = pPM.GetEntities(qry);
					oGenericType = null;
					iLoop++;
					break;
				} catch (Exception ex) {
					if (ex.Message.ToLower().Contains("dynamic type:") && ex.Message.ToLower().Contains(" already exists")) {
						iLoop++;
					} else {
						break;
					}
				}
			}

			return oRet;
		}


		private static Type mGetRealTimeTransCardPaymentsForDoW = null;
		public static Entity[] GetRealTimeTransCardPaymentsForDoW(PersistenceManager pPM, DateTime pStart, DateTime pEnd, DayOfWeek pDayOfWeek) {
			if (mGetRealTimeTransCardPaymentsForDoW == null) {
				mGetRealTimeTransCardPaymentsForDoW = DynamicEntityTypeBuilder.CreateType("GetRealTimeTransCardPaymentsForDoW", "Default");
			}

			StoredProcRdbQuery aQuery = new StoredProcRdbQuery("dbo", "usp_TransCardPaymentsForDoW", mGetRealTimeTransCardPaymentsForDoW);

			RdbParameter aParameter = new RdbParameter("@StartDate", DbType.DateTime);
			aParameter.Value = pStart;
			aQuery.Parameters.Add(aParameter);

			aParameter = new RdbParameter("@EndDate", DbType.DateTime);
			aParameter.Value = pEnd;
			aQuery.Parameters.Add(aParameter);

			aParameter = new RdbParameter("@DofW", DbType.Int32);
			aParameter.Value = (int)pDayOfWeek + 1;
			aQuery.Parameters.Add(aParameter);

			return pPM.GetEntities(aQuery);
		}


		private static Type CashierTransfers = null;
		public static Entity[] RedeemerCashAccounting_CashierTrans(TaxiPassRedeemers pRedeemer, long pCashierID, DateTime pStart, DateTime pEnd) {
			if (CashierTransfers == null) {
				CashierTransfers = DynamicEntityTypeBuilder.CreateType("CashierTransfers", "Default");
			}
			StoredProcRdbQuery aQuery = new StoredProcRdbQuery("dbo", "usp_RedeemerCashAccounting_CashierTrans", CashierTransfers);

			RdbParameter aParameter = new RdbParameter("@StartDate", DbType.DateTime);
			aParameter.Value = pStart;
			aQuery.Parameters.Add(aParameter);

			aParameter = new RdbParameter("@EndDate", DbType.DateTime);
			aParameter.Value = pEnd.AddDays(1);
			aQuery.Parameters.Add(aParameter);

			aParameter = new RdbParameter("@RedeemerAccountID", DbType.Int64);
			aParameter.Value = pRedeemer.TaxiPassRedeemerAccountID;
			aQuery.Parameters.Add(aParameter);

			aParameter = new RdbParameter("@RedeemerID", DbType.Int64);
			aParameter.Value = pCashierID;
			aQuery.Parameters.Add(aParameter);

			return pRedeemer.PersistenceManager.GetEntities(aQuery);
		}

		private static Type typeGetDistinctAdminNoForMissedTransactions = DynamicEntityTypeBuilder.CreateType("GetDistinctAdminNoForMissedTransactions", "Default");

		public static List<string> GetDistinctAdminNoForMissedTransactions(PersistenceManager pPM) {
			if (typeGetDistinctAdminNoForMissedTransactions == null) {
				typeGetDistinctAdminNoForMissedTransactions = DynamicEntityTypeBuilder.CreateType("typeGetDistinctAdminNoForMissedTransactions", "Default");
			}
			StringBuilder sql = new StringBuilder();
			sql.AppendLine("SELECT distinct ACHDetail.TransCardAdminNo FROM ACHDetail");
			sql.AppendLine("inner join DriverPayments on DriverPayments.DriverPaymentID = ACHDetail.DriverPaymentID");
			sql.AppendLine("where TransCardAdminNo is not null and Validated = 0 and ACHPaidDate >= '1/1/2011' and ACHErrorReason is null");
			sql.AppendLine("and AffiliateID != 81");

			IEntityQuery qry = new PassthruRdbQuery(typeGetDistinctAdminNoForMissedTransactions, sql.ToString());
			
			Entity[] list = pPM.GetEntities(qry);
			List<string> adminNoList = (from p in list
										select p[0].ToString()).ToList();
			return adminNoList;
		}

	}
}