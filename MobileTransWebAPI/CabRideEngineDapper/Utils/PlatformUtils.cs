﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CabRideEngineDapper.Utils {
    public static class PlatformUtils {
        public static IEnumerable<string> GetForDropdown() {
            Platforms[] values = (Platforms[])Enum.GetValues(typeof(Platforms));

            var sorted = values.OrderBy(v => v.ToString());

            List<string> list = new List<string>();
            list.Add("");
            foreach (Platforms rec in sorted) {
                list.Add(rec.ToString());
            }

            return list;
        }
    }
}
