using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;
using TaxiPassCommon;
using System.Text;

namespace CabRideEngine {
	/// <summary>
	/// The developer class representing the AffiliateRef business object class.
	/// </summary>
	/// <remarks>
	/// <para>Place all application-specific business logic in this class.</para>
	/// <para>
	/// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
	/// and will not overwrite changes.
	/// </para>
	/// <para>
	/// This class inherits from the generated class, its companion in the pair of classes 
	/// defining a business object. Properties in the generated class may be overridden in 
	/// order to change the behavior or add business logic.
	/// </para>
	/// <para>
	/// This class is the final class in the inheritance chain for this business object. 
	/// It should not be inherited.
	/// </para>
	/// </remarks>
	[Serializable]
	public sealed partial class AffiliateRef : AffiliateRefDataRow {

		#region Constructors -- DO NOT MODIFY

		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new';
		// it is also required if the class is visible from a web service.
		// DO NOT REMOVE **
		private AffiliateRef() : this(null) { }

		/// <summary>
		/// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
		/// </summary>
		/// <param name="pRowBuilder"></param>
		public AffiliateRef(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static AffiliateRef Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      AffiliateRef aAffiliateRef = pManager.CreateEntity<AffiliateRef>();
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aAffiliateRef, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aAffiliateRef.AddToManager();
		//      return aAffiliateRef;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		// Add additional logic to your business object here...
		#region Standard Entity logic to create/audit/validate the entity

		// Add additional logic to your business object here...
		public static AffiliateRef CreateUpdate(Affiliate pAffiliate, long? pAffiliateRefID, string pGroup, string pReference, string pValue, string pModifiedBy) {
			StringBuilder sql = new StringBuilder("usp_AffiliateRefUpdate ");
			if (pAffiliateRefID.GetValueOrDefault(0) > 0) {
				sql.AppendFormat("@AffiliateRefID = {0}", pAffiliateRefID);
				sql.AppendFormat(", @AffiliateID = {0}", pAffiliate.AffiliateID);
			} else {
				sql.AppendFormat("@AffiliateID = {0}", pAffiliate.AffiliateID);
			}
			sql.AppendFormat(", @ReferenceGroup = '{0}'", pGroup);
			sql.AppendFormat(", @ReferenceKey = '{0}'", pReference);
			sql.AppendFormat(", @ValueString = '{0}'", pValue);
			sql.AppendFormat(", @ModifiedBy = '{0}'", pModifiedBy);

			PersistenceManager oPM = pAffiliate.PersistenceManager;

			AffiliateRef rec = null;

			try {
				PassthruRdbQuery qry = new PassthruRdbQuery(typeof(AffiliateRef), sql.ToString());
				rec = oPM.GetEntity<AffiliateRef>(qry);

			} catch (Exception ex) {
				throw ex;
			}
			return rec;
		}

		#endregion



		// **********************************************************************************************************
		// Get or Create a record
		// **********************************************************************************************************
		public static AffiliateRef GetCreate(Affiliate pAffiliate, string pGroup, string pReference, string pDefaultValue) {

			//AffiliateRef rec = SelectRecord(pAffiliate, pGroup, pReference);
			if (pAffiliate.PersistenceManager.DefaultQueryStrategy != QueryStrategy.Normal) {
				pAffiliate.PersistenceManager.DefaultQueryStrategy = QueryStrategy.Normal;
			}
			AffiliateRef rec = GetByGroupReference(pAffiliate, pGroup, pReference);
			if (rec.IsNullEntity) {
				try {
					rec = CreateUpdate(pAffiliate, null, pGroup, pReference, pDefaultValue, LoginManager.PrincipleUser.Identity.Name);
					// done this way, as edit was not working correctly
					if (rec.IsNullEntity) {
						rec = GetByGroupReference(pAffiliate, pGroup, pReference);
					}
				} catch (Exception ex) {
					throw ex;
				}
			}
			return rec;
		}

		public static AffiliateRef SelectRecord(Affiliate pAffiliate, string pGroup, string pReference) {
			StringBuilder sql = new StringBuilder("usp_AffiliateRefSelect ");
			sql.AppendFormat("@AffiliateID = {0}", pAffiliate.AffiliateID);
			sql.AppendFormat(", @ReferenceGroup = '{0}'", pGroup);
			sql.AppendFormat(", @ReferenceKey = '{0}'", pReference);

			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(AffiliateRef), sql.ToString());
			AffiliateRef rec = pAffiliate.PersistenceManager.GetEntity<AffiliateRef>(qry);
			return rec;
		}


		public static AffiliateRef GetByGroupReference(Affiliate pAffiliate, string pGroup, string pReference) {
			return GetByGroupReference(pAffiliate.PersistenceManager, pAffiliate.AffiliateID, pGroup, pReference);
		}

		public static AffiliateRef GetByGroupReference(PersistenceManager pPM, long pAffiliateID, string pGroup, string pReference) {
			RdbQuery qry = new RdbQuery(typeof(AffiliateRef), ReferenceKeyEntityColumn, EntityQueryOp.EQ, pReference);
			qry.AddClause(AffiliateRef.ReferenceGroupEntityColumn, EntityQueryOp.EQ, pGroup);
			qry.AddClause(AffiliateRef.AffiliateIDEntityColumn, EntityQueryOp.EQ, pAffiliateID);
			AffiliateRef rec = pPM.GetEntity<AffiliateRef>(qry);
			return rec;
		}


		public static EntityList<AffiliateRef> GetByReference(Affiliate pAffiliate, string pReference) {
			RdbQuery qry = new RdbQuery(typeof(AffiliateRef), ReferenceKeyEntityColumn, EntityQueryOp.EQ, pReference);
			qry.AddClause(AffiliateRef.AffiliateIDEntityColumn, EntityQueryOp.EQ, pAffiliate.AffiliateID);
			return pAffiliate.PersistenceManager.GetEntities<AffiliateRef>(qry);
		}

		public static AffiliateRef GetByReferenceAndValue(Affiliate pAffiliate, string pReference, string pValue) {
			RdbQuery qry = new RdbQuery(typeof(AffiliateRef), ReferenceKeyEntityColumn, EntityQueryOp.EQ, pReference);
			qry.AddClause(ValueStringEntityColumn, EntityQueryOp.EQ, pValue);
			qry.AddClause(AffiliateRef.AffiliateIDEntityColumn, EntityQueryOp.EQ, pAffiliate.AffiliateID);
			return pAffiliate.PersistenceManager.GetEntity<AffiliateRef>(qry);
		}

		// ********************************************************************************************
		// Return all rows for a Group
		// ********************************************************************************************
		public static EntityList<AffiliateRef> GetGroup(Affiliate pAffiliate, string pGroup) {
			StringBuilder sql = new StringBuilder();
			sql.AppendFormat("usp_AffiliateRefGroupGet @ReferenceGroup='{0}'", pGroup);
			sql.AppendFormat(", @AffiliateID = {0}", pAffiliate.AffiliateID);

			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(SystemDefaultsDict), sql.ToString());
			EntityList<AffiliateRef> recList = pAffiliate.PersistenceManager.GetEntities<AffiliateRef>(qry);
			return recList;
		}


	}

	#region EntityPropertyDescriptors.AffiliateRefPropertyDescriptor
	////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
	////    For example: 
	////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
	//public static partial class EntityPropertyDescriptors {
	//	public partial class AffiliateRefPropertyDescriptor : AdaptedPropertyDescriptor {
	//    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
	//    public AdaptedPropertyDescriptor Age {
	//      get { return this.Get("Age"); }
	//    }
	//  }
	//} 
	#endregion

}