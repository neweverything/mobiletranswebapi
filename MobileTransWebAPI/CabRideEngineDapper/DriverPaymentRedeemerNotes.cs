﻿using Dapper;
using System;
using System.Data.SqlClient;

namespace CabRideEngineDapper {
    public partial class DriverPaymentRedeemerNotes {

        public static DriverPaymentRedeemerNotes Create(DriverPayments pDriverPayment) {
            DriverPaymentRedeemerNotes rec = null;
            try {
                rec = Create();

                // Using the IdeaBlade Id Generation technique
                //pDriverPayment.PersistenceManager.GenerateId(rec, DriverPaymentRedeemerNotes.DriverPaymentRedeemerNoteIDEntityColumn);
                rec.DriverPaymentID = pDriverPayment.DriverPaymentID;


            } catch (Exception ex) {
                throw ex;
            }
            pDriverPayment.MyDriverPaymentRedeemerNotes = rec;
            return rec;
        }

        public override int Save() {
            if (IsNewRecord) {
                this.DriverPaymentRedeemerNoteID = NextID.GetNextKey(DriverPaymentRedeemerNotes.TableDef.TABLE_NAME);
            }
            return base.Save();
        }

        public override int Save(SqlConnection pConn, string pModifiedBy) {
            if (IsNewRecord) {
                this.DriverPaymentRedeemerNoteID = NextID.GetNextKey(DriverPaymentRedeemerNotes.TableDef.TABLE_NAME);
            }

            return base.Save(pConn, pModifiedBy);
        }



        public static DriverPaymentRedeemerNotes GetNote(SqlConnection pConn, long pDriverPaymentID) {
            string sql = @"SELECT  *
                            FROM    DriverPaymentRedeemerNotes
                            WHERE   DriverPaymentID = @DriverPaymentID";

            return pConn.Get<DriverPaymentRedeemerNotes>(sql, new { DriverPaymentID = pDriverPaymentID });
        }
    }
}
