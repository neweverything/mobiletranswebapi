﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Util;

namespace CabRideEngine {
    [Serializable]
    public sealed class PaymentsAudit : PaymentsAuditDataRow {

        #region Constructors -- DO NOT MODIFY
        // Do not create constructors for this class
        // Developers cannot create instances of this class with the "new" keyword
        // You should write a static Create method instead; see the "Suggested Customization" region
        // Please review the documentation to learn about entity creation and inheritance

        // This private constructor prevents accidental instance creation via 'new'
        private PaymentsAudit() : this(null) { }

        // Typed DataSet constructor (needed by framework) - DO NOT REMOVE
        public PaymentsAudit(DataRowBuilder pRowBuilder)
            : base(pRowBuilder) {
        }

        #endregion

        #region Suggested Customizations

        //    // Use this factory method template to create new instances of this class
        //    public static PaymentsAudit Create(PersistenceManager pManager,
        //      ... your creation parameters here ...) { 
        //
        //      PaymentsAudit aPaymentsAudit = pManager.CreateEntity<PaymentsAudit>();
        //
        //      // if this object type requires a unique id and you have implemented
        //      // the IIdGenerator interface implement the following line
        //      pManager.GenerateId(aPaymentsAudit, // add id column here //);
        //
        //      // Add custom code here
        //
        //      aPaymentsAudit.AddToManager();
        //      return aPaymentsAudit;
        //    }

        //    // Implement this method if you want your object to sort in a specific order
        //    public override int CompareTo(Object pObject) {
        //    }

        //    // Implement this method to customize the null object version of this class
        //    protected override void UpdateNullEntity() {
        //    }

        #endregion


        public static PaymentsAudit Create(PersistenceManager pPM, Payments pPayments) {
            PaymentsAudit oAudit = pPM.CreateEntity<PaymentsAudit>();
            pPM.GenerateId(oAudit, PaymentsAudit.PaymentHistoryIDEntityColumn);

            for (int i = 0; i < pPayments.Table.Columns.Count; i++) {
                if (oAudit.Table.Columns.Contains(pPayments.Table.Columns[i].ColumnName)) {
                    oAudit[pPayments.Table.Columns[i].ColumnName] = pPayments[i];
                }
            }
            oAudit.AddToManager();

            return oAudit;
        }

    }

}
