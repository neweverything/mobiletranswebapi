﻿using CabRideEngineDapper.Utils;
using Dapper;

namespace CabRideEngineDapper {
	public partial class EmailServer {

		public static EmailServer GetEmailServer(long pID) {
			string sql = "SELECT * FROM EmailServer WHERE AffiliateID = @AffiliateID ";

			using (var conn = SqlHelper.OpenSqlConnection()) {
				return conn.Get<EmailServer>(sql, new { AffiliateID = pID });
			}
		}
	}
}
