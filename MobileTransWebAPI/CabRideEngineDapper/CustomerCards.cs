﻿using CabRideEngineDapper.Utils;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using TaxiPassCommon;

namespace CabRideEngineDapper {
	public partial class CustomerCards {

		private string mSalt;

		public static List<CustomerCards> GetCustomerCards(long pCustomerID) {
			using (var conn = SqlHelper.OpenSqlConnection()) {
				return GetCustomerCards(conn, pCustomerID);
			}
		}

		public static List<CustomerCards> GetCustomerCards(SqlConnection pConn, long pCustomerID) {
			string sql = "SELECT * FROM CustomerCards WITH (NOLOCK) WHERE CustomerID = @CustomerID";
			return pConn.Query<CustomerCards>(sql, new { CustomerID = pCustomerID }).ToList();
		}


		public static CustomerCards GetCardByToken(SqlConnection pConn, string pToken, long pCustomerID) {
			CustomerCards card = CustomerCardsTable.GetNullEntity();
			if (pCustomerID == -1) {
				string sql = @"SELECT  @Token Token
									  , CustomerCards.*
								FROM    CustomerCards WITH (NOLOCK)
								WHERE   CustCardID IN (SELECT   ID
													   FROM     Tokens
													   WHERE    Tokens.TableName = 'CustomerCards'
																AND Token = @Token)";
				card = pConn.Get<CustomerCards>(sql, new { Token = pToken });
			} else {
				DynamicParameters param = new DynamicParameters();
				param.Add("CustomerID", pCustomerID);
				param.Add("Token", pToken);
				card = pConn.Query<CustomerCards>("usp_CustomerCardsGetTokens", param, commandType: CommandType.StoredProcedure).DefaultIfEmpty(CustomerCards.GetNullEntity()).FirstOrDefault();
			}
			return card;
		}


		public string DecryptCard(string pSalt = "") {
			if (pSalt.IsNullOrEmpty()) {
				if (mSalt.IsNullOrEmpty()) {
					using (var conn = SqlHelper.OpenSqlConnection()) {
						mSalt = conn.Query<string>("SELECT Salt FROM dbo.Customer WHERE CustomerID = @CustomerID", new { CustomerID = this.CustomerID }).FirstOrDefault();
					}
				}
			} else {
				mSalt = pSalt;
			}
			return Utils.CreditCardUtils.DecryptCardNo(mSalt, Number, false);
		}

		public static CustomerCards GetCard(SqlConnection pConn, long pCustCardID) {
			return pConn.Get<CustomerCards>(pCustCardID);
		}


		public static CustomerCards GetCard(long pCustomerID, string pCardNo) {
			string sql = @"SELECT * FROM CustomerCards
							WHERE CustomerID = @CustomerID AND Number = @CardNumber";

			string number = EncryptCardNo(pCustomerID, pCardNo);
			using (var conn = SqlHelper.OpenSqlConnection()) {
				return conn.Get<CustomerCards>(sql, new { CustomerID = pCustomerID, CardNumber = number });
			}
		}


		public static CustomerCards GetECheck(long pCustomerID, string pAccountNo) {
			string sql = @"SELECT * FROM CustomerCards
							WHERE CustomerID = @CustomerID AND Number = @CardNumber and CardType = 'eCheck'";

			string number = EncryptECheck(pCustomerID, pAccountNo);
			using (var conn = SqlHelper.OpenSqlConnection()) {
				return conn.Get<CustomerCards>(sql, new { CustomerID = pCustomerID, CardNumber = number });
			}
		}


		public static CustomerCards GetCardByCardHolder(long pAffiliateID, string pCardHolder, string pCardNumber) {
			string sql = @"SELECT * FROM CustomerCards
							LEFT JOIN Customer ON Customer.CustomerID = CustomerCards.CustomerID
							WHERE CustomerCards.Name = @Name AND AffiliateID = @AffiliateID";


			CustomerCards card = CustomerCards.GetNullEntity();
			using (var conn = SqlHelper.OpenSqlConnection()) {
				List<CustomerCards> cardList = conn.Query<CustomerCards>(sql, new { Name = pCardHolder, AffiliateID = pAffiliateID }).ToList();
				foreach (CustomerCards rec in cardList) {
					if (rec.DecryptCard() == pCardNumber) {
						card = rec;
						break;
					}
				}
				if (card.IsNullEntity && cardList.Count > 0) {
					card.CustomerID = cardList[0].CustomerID;
				}
			}

			return card;
		}

		[Editable(false)]
		public string CardExpiryAsMMYY {
			get {
				if (Expiration.HasValue) {
					return Expiration.Value.ToString("MM /yy");
				}
				return "";
			}
			set {
				Expiration = Convert.ToDateTime(string.Format("{0}/01/{1}", value.Left(2), value.Right(2))).AddMonths(1).AddSeconds(-1);
			}
		}

		[Editable(false)]
		public string NameDecrypted {
			get {
				return Name.DecryptIceKey();
			}
			set {
				Name = value.EncryptIceKey();
			}
		}

		[Editable(false)]
		public string CVVForGrid {
			get {
				return "";
			}
			set {
				Verification = value.EncryptIceKey();
			}
		}


		public void EncryptCard(string pCardNo, bool pSkipCardValidation = false) {
			using (var conn = SqlHelper.OpenSqlConnection()) {
				if (mSalt.IsNullOrEmpty()) {
					mSalt = conn.Query<string>("SELECT Salt FROM dbo.Customer WHERE CustomerID = @CustomerID", new { CustomerID = this.CustomerID }).FirstOrDefault();
				}
				Number = CreditCardUtils.EncryptCardNo(conn, mSalt, pCardNo, pSkipCardValidation);
				CardNumberDisplay = pCardNo.CardNumberDisplay();
				CardType = CardUtils.DetermineCardType(pCardNo);
			}
		}

		public static string EncryptCardNo(long pCustomerID, string pCardNo) {
			using (var conn = SqlHelper.OpenSqlConnection()) {
				string salt = conn.Query<string>("SELECT Salt FROM dbo.Customer WHERE CustomerID = @CustomerID", new { CustomerID = pCustomerID }).FirstOrDefault();
				return CreditCardUtils.EncryptCardNo(conn, salt, pCardNo);
			}
		}

		public static string EncryptECheck(long pCustomerID, string pAccountNo) {
			using (var conn = SqlHelper.OpenSqlConnection()) {
				string salt = conn.Query<string>("SELECT Salt FROM dbo.Customer WHERE CustomerID = @CustomerID", new { CustomerID = pCustomerID }).FirstOrDefault();
				return CreditCardUtils.EncryptCardNo(conn, salt, pAccountNo, true);
			}
		}
	}
}
