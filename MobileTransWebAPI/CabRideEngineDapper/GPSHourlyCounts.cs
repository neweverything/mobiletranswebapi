﻿using CabRideEngineDapper.Utils;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CabRideEngineDapper {
	public partial class GPSHourlyCounts {

		public static List<cr_GetGPSHourlyCounts_Result> GetRecords(long pAffiliateID, DateTime pStart, DateTime pEnd) {

			List<cr_GetGPSHourlyCounts_Result> myResults = new List<cr_GetGPSHourlyCounts_Result>();

			using (var ctx = SqlHelper.OpenSqlConnection()) {
				//CabRideDataClassesDataContext ctx = new CabRideDataClassesDataContext(GlobalSettings.DecryptedConnectionString);
				//ctx.CommandTimeout = 300;

				var p = new DynamicParameters();
				p.Add("@StartDate", pStart);
				p.Add("@@EndDate", pEnd);

				List<cr_GetGPSHourlyCounts_Result> list = ctx.Query<cr_GetGPSHourlyCounts_Result>("cr_GetGPSHourlyCounts", param: p, commandType: CommandType.StoredProcedure).ToList();
				List<Drivers> driverList = ctx.Query<Drivers>("Select * From Drivers Where AffiliateID = @AffiliateID", new { AffiliateID = pAffiliateID }).ToList();

				foreach (Drivers rec in driverList) {
					var x = from gps in list
							where gps.DriverID == rec.DriverID
							orderby gps.MyGPSTime
							select gps;
					myResults.AddRange(x);
				}
			}

			return myResults;
		}

	}


	public partial class cr_GetGPSHourlyCounts_Result {
		public long DriverID { get; set; }
		public Nullable<System.DateTime> MyGPSTime { get; set; }
		public Nullable<int> GPSCount { get; set; }
	}
}
