﻿using CabRideEngineDapper.Utils;
using Dapper;
using System;
using System.Data.SqlClient;
using System.Linq;

namespace CabRideEngineDapper {
    public partial class Routing {

        public static Routing Create(long pReservationID) {
            Routing oRouting = null;

            try {
                oRouting = RoutingTable.Create();

                if (!oRouting.IsValidReservationID(pReservationID)) {
                    throw new Exception("Unknown Reservation ID");
                }

                oRouting.ReservationID = pReservationID;

            } catch (Exception ex) {
                throw ex;
            }
            return oRouting;
        }

        private bool IsValidReservationID(long pReservationID) {
            using (var conn = SqlHelper.OpenSqlConnection()) {
                Reservations oRes = Reservations.GetReservation(conn, pReservationID);
                return !oRes.IsNullEntity;
            }
        }

        public static Routing GetRouting(SqlConnection pConn, long pReservationID, string pType) {
            string sql = @"SELECT  *
                            FROM    dbo.Routing
                            WHERE   ReservationID = @ReservationID
                                    AND Type = @Type";

            return pConn.Query<Routing>(sql, new { ReservationID = pReservationID, Type = pType }).FirstOrDefault();
        }


        public static Routing GetRouting(Reservations pReserve, short pRoutingOrder) {
            string sql = @"SELECT  *
                            FROM    Routing
                            WHERE   ReservationID = @ReservationID
                                    AND RoutingOrder = @RoutingOrder";

            DynamicParameters parms = new DynamicParameters();
            parms.Add("ReservationID", pReserve.ReservationID);
            parms.Add("RoutingOrder", pRoutingOrder);

            using (var conn = SqlHelper.OpenSqlConnection()) {
                return conn.Get<Routing>(sql, parms);
            }
        }

        [Editable(false)]
        public bool ValidateAddress { get; set; }



    }
}
