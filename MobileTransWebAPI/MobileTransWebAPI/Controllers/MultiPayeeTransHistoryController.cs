﻿using CabRideEngineDapper;
using CabRideEngineDapper.Utils;
using Dapper;
using MobileTransWebAPI.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web.Http;
using TaxiPassCommon;

namespace MobileTransWebAPI.Controllers {
	public class MultiPayeeTransHistoryController : ApiController {

		//public MultiPayeeTransHistoryResponse Get() {
		//	MultiPayeeTransHistoryResponse result = new MultiPayeeTransHistoryResponse();
		//	try {
		//		MultiPayeeTransHistoryRequest req = new MultiPayeeTransHistoryRequest {
		//			AffiliateID = 2595,
		//			AffiliateCode = "MWFR",
		//			RequestTime = DateTime.UtcNow.ToString("s"),
		//			StartDate = new DateTime(2020, 2, 5),
		//			EndDate = new DateTime(2020, 2, 6),
		//			IncludeTestTrans = false,
		//			IncludeFailedTrans = false
		//		};
		//		string js = JsonConvert.SerializeObject(req);
		//		Console.WriteLine(js);
		//		//req.CustomFieldFilters = new List<MultiPayeeTransHistoryRequest.CustomFieldFilter>();
		//		//req.CustomFieldFilters.Add(new MultiPayeeTransHistoryRequest.CustomFieldFilter { FieldName = "id_user", Value = "usr_1awQLKZsD" });

		//		//System.IO.File.WriteAllText(@"c:\temp\MultiPayeeTransHistoryRequest.json", JsonConvert.SerializeObject(req));


		//		result = Index(req);
		//	} catch (Exception ex) {
		//		result.ErrorMsg = ex.Message;
		//	}
		//	return result;
		//}

		public MultiPayeeTransHistoryResponse Index(MultiPayeeTransHistoryRequest pRequest) {
			MultiPayeeTransHistoryResponse result = new MultiPayeeTransHistoryResponse();

			if (pRequest == null) {
				result.ErrorCode = 1;
				result.ErrorMsg = "Request cannot be null";
				return result;
			}

			if (pRequest.AffiliateID == 0 || pRequest.AffiliateCode.IsNullOrEmpty()) {
				result.ErrorCode = 2;
				result.ErrorMsg = "Request object is not valid";
				return result;
			}

			Affiliate aff = Affiliate.GetAffiliate(pRequest.AffiliateID);
			if (aff.IsNullEntity || !aff.AffiliateCode.Equals(pRequest.AffiliateCode)) {
				result.ErrorCode = 3;
				result.ErrorMsg = "Invalid Request";
				return result;
			}

			DateTime requestDate = Convert.ToDateTime(pRequest.RequestTime);
			if (WebApiApplication.GetUTCServerTime().Subtract(requestDate).TotalMinutes > 1) {
				result.ErrorCode = -2;
				result.ErrorMsg = "Invalid Access";
				return result;
			}


			StringBuilder sql = new StringBuilder(GetSQLString());
			List<KeyValue> parms = new List<KeyValue>();
			try {
				using (var conn = SqlHelper.OpenSqlConnection()) {


					if (!pRequest.VoucherNo.IsNullOrEmpty()) {
						sql.AppendLine("AND TransNo = @VoucherNo");
						parms.Add(new KeyValue("VoucherNo", pRequest.VoucherNo));
					} else {
						if (pRequest.StartDate.Year > 2000) {
							sql.AppendLine("AND ChargeDate >= @StartDate");
							parms.Add(new KeyValue("StartDate", pRequest.StartDate));
						}

						if (pRequest.EndDate > pRequest.StartDate) {
							sql.AppendLine("AND ChargeDate <= @EndDate");
							parms.Add(new KeyValue("EndDate", pRequest.EndDate));
						}

						if (pRequest.PayeeID != 0) {
							sql.AppendLine("AND DriverPayments.AffiliateDriverID = @PayeeID");
							parms.Add(new KeyValue("PayeeID", pRequest.PayeeID));
						}

						if (!pRequest.VoucherNo.IsNullOrEmpty()) {
							sql.AppendLine("AND DriverPayments.AffiliateDriverID = @PayeeID");
							parms.Add(new KeyValue("PayeeID", pRequest.PayeeID));
						}

						if (pRequest.CustomFieldFilters != null && pRequest.CustomFieldFilters.Count > 0) {
							string custSQL = @"SELECT DriverPaymentRef.DriverPaymentID
											FROM dbo.DriverPaymentRef
											LEFT JOIN DriverPayments ON DriverPayments.DriverPaymentID = DriverPaymentRef.DriverPaymentID
											WHERE ReferenceGroup = 'CustomFields'
												  AND AffiliateID = @AffiliateID
												  AND ReferenceKey = @Key
												  AND ValueString = @Value
											ORDER BY DriverPaymentRef.DriverPaymentID";

							List<long> idList = new List<long>();
							foreach (var filter in pRequest.CustomFieldFilters) {
								idList.AddRange(conn.Query<long>(custSQL, new { AffiliateID = aff.AffiliateID, Key = filter.FieldName, Value = filter.Value }));
							}
							if (idList.Count > 0) {
								sql.AppendLine("AND DriverPayments.DriverPaymentID IN (@IDList)");
								parms.Add(new KeyValue("IDList", idList));
							}
						}

						if (!pRequest.IncludeTestTrans) {
							sql.AppendLine("AND Test = @Test");
							parms.Add(new KeyValue("Test", 0));
						}

						if (!pRequest.IncludeFailedTrans) {
							sql.AppendLine("AND Failed = @Failed");
							parms.Add(new KeyValue("Failed", 0));
						}
					}

					string tmp = sql.ToString();

					SqlCommand command = new SqlCommand(sql.ToString(), conn);
					command.Parameters.AddWithValue("AffiliateID", aff.AffiliateID);
					foreach (KeyValue param in parms) {
						if (param.Value.GetType() == typeof(List<long>)) {
							command.AddArrayParameters(param.Key, param.Value as List<long>);
						} else {
							command.Parameters.AddWithValue(param.Key, param.Value);
						}
					}
					SqlDataAdapter adapter = new SqlDataAdapter(command);

					DataSet ds = new DataSet();
					adapter.Fill(ds);

					long acctID = conn.Get<long>("SELECT AccountID FROM Accounts WHERE AffiliateID = @AffiliateID", new { AffiliateID = aff.AffiliateID });
					var customFieldList = new List<AccountCustomField>();
					if (acctID > 0) {
						customFieldList = AccountCustomField.GetList(acctID);
					}

					foreach (var field in customFieldList) {
						DataColumn col = new DataColumn(field.FieldName);
						col.Caption = field.Description;
						ds.Tables[0].Columns.Add(col);
					}


					for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
						string[] tempCustomFieldList = ("" + ds.Tables[0].Rows[i]["CustomFields"]).ToString().Split('|');
						foreach (var cfRec in tempCustomFieldList) {
							if (cfRec.IsNullOrEmpty()) {
								continue;
							}
							try {
								string[] valList = cfRec.Split(':');
								string colName = customFieldList.FirstOrDefault(p => p.FieldName.Equals(valList[0], StringComparison.CurrentCultureIgnoreCase) || p.Description.Equals(valList[0])).FieldName;
								ds.Tables[0].Rows[i][colName] = ("" + valList[1]).Trim();
							} catch (Exception ex) {
								Console.WriteLine(ex.Message);
							}
						}
					}
					ds.Tables[0].Columns.Remove("CustomFields");

					result.ErrorCode = 0;
					result.ErrorMsg = "OK";
					result.TransList = JsonConvert.SerializeObject(ds);
					string temp = result.TransList.Substring(9);
					temp = temp.Left(temp.Length - 1);
					result.TransList = temp;
				}
			} catch (Exception ex) {
				result.ErrorCode = ex.HResult;
				result.ErrorMsg = ex.Message;
			}


			return result;
		}


		private string GetSQLString() {
			string sql = @"SELECT DriverPayments.AffiliateID
							 , DriverPayments.AffiliateDriverID CashierID
							 , DriverPayments.DriverID			RegisterID
							 , TimeZone
							 , AffiliateDrivers.Name			CashierName
							 , Affiliate.Name					MerchantName
							 , ChargeDate
							 , Fare								Amount
							 , TransNo							VoucherNo
							 , DriverPayments.CardHolder
							 , CardNumberDisplay
							 , CardAddress
							 , ReferenceNo
							 , CustPhone
							 , EmailAddress
							 , Test
							 , ChargeBy
							 , PreAuthBy
							 , CorporateName
							 , Reason
							 , ChargeBackInfo
							 , DriverPayments.DriverFee
							 , DriverPayments.CardType
							 , TaxiPassFee						ServiceFee
							 , BinType
							 , SoftwareLicensing
							 , AirportFee
							 , MiscFee
							 , AuthCode
							 , Message							ChargeStatus
							 , (   SELECT ReferenceKey + ': ' + ValueString + '|'
								   FROM DriverPaymentRef WITH (NOLOCK)
								   WHERE DriverPaymentID = DriverPayments.DriverPaymentID
										 AND ReferenceGroup = 'CustomFields'
										 AND IsNull(ValueString, '') <> ''
								   ORDER BY ReferenceKey
								   FOR XML PATH(''))			CustomFields
							 , SendReceiptTo
							 , Failed
						FROM DriverPayments WITH (NOLOCK)
						LEFT JOIN Affiliate WITH (NOLOCK) ON Affiliate.AffiliateID = DriverPayments.AffiliateID
						LEFT JOIN AffiliateDrivers WITH (NOLOCK) ON AffiliateDrivers.AffiliateDriverID = DriverPayments.AffiliateDriverID
						LEFT JOIN ACHDetail WITH (NOLOCK) ON ACHDetail.DriverPaymentID = DriverPayments.DriverPaymentID
						LEFT JOIN DriverPaymentNotes WITH (NOLOCK) ON DriverPaymentNotes.DriverPaymentID = DriverPayments.DriverPaymentID
						OUTER APPLY (	SELECT TOP 1 *
										FROM DriverPaymentResults WITH (NOLOCK)
										WHERE DriverPaymentResults.DriverPaymentID = DriverPayments.DriverPaymentID
											  AND Result = '0'
											  AND TransType = 's'
										ORDER BY DriverPaymentResultID DESC) DriverPaymentResults
						OUTER APPLY (	SELECT ValueString CustPhone
										FROM DriverPaymentRef WITH (NOLOCK)
										WHERE ReferenceGroup = 'Customer'
											  AND ReferenceKey = 'Phone'
											  AND DriverPaymentID = DriverPayments.DriverPaymentID) CustPhone
						OUTER APPLY (	SELECT CASE
												   WHEN ValueString = 'True' THEN
													   1
												   ELSE
													   0
											   END SoftwareLicensing
										FROM AffiliateRef WITH (NOLOCK)
										WHERE ReferenceGroup = 'Affiliate'
											  AND ReferenceKey = 'SoftwareLicensing'
											  AND AffiliateID = DriverPayments.AffiliateID) SoftwareLicensing
						LEFT JOIN DriverPaymentGeoCodes ON DriverPaymentGeoCodes.DriverPaymentID = DriverPayments.DriverPaymentID
						WHERE DriverPayments.AffiliateID = @AffiliateID ";
			return sql;
		}
	}
}