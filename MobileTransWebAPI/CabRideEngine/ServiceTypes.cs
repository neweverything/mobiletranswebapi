﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

using Csla.Validation;

namespace CabRideEngine {
    [Serializable]
    public sealed class ServiceTypes : ServiceTypesDataRow {

        #region Constructors -- DO NOT MODIFY
        // Do not create constructors for this class
        // Developers cannot create instances of this class with the "new" keyword
        // You should write a static Create method instead; see the "Suggested Customization" region
        // Please review the documentation to learn about entity creation and inheritance

        // This private constructor prevents accidental instance creation via 'new'
        private ServiceTypes()
            : this(null) {
        }

        // Typed DataSet constructor (needed by framework) - DO NOT REMOVE
        public ServiceTypes(DataRowBuilder pRowBuilder)
            : base(pRowBuilder) {
        }

        #endregion

        #region Suggested Customizations

        //    // Use this factory method template to create new instances of this class
        //    public static ServiceTypes Create(PersistenceManager pManager,
        //      ... your creation parameters here ...) { 
        //
        //      ServiceTypes aServiceTypes = pManager.CreateEntity<ServiceTypes>();
        //
        //      // if this object type requires a unique id and you have implemented
        //      // the IIdGenerator interface implement the following line
        //      pManager.GenerateId(aServiceTypes, // add id column here //);
        //
        //      // Add custom code here
        //
        //      aServiceTypes.AddToManager();
        //      return aServiceTypes;
        //    }

        //    // Implement this method if you want your object to sort in a specific order
        //    public override int CompareTo(Object pObject) {
        //    }

        //    // Implement this method to customize the null object version of this class
        //    protected override void UpdateNullEntity() {
        //    }

        #endregion

        // Add additional logic to your business object here...
        public static ServiceTypes Create(PersistenceManager pManager) {

            ServiceTypes oServiceType = (ServiceTypes)pManager.CreateEntity(typeof(ServiceTypes));

            //pManager.GenerateId(oServiceType, ServiceTypes.ServiceTypeIDEntityColumn);
            oServiceType.AddToManager();
            return oServiceType;
        }

        protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
            if (IsDeserializing)
                return;
            base.OnColumnChanging(pArgs);
            //DoAudit(pArgs);
        }

        private void DoAudit(DataColumnChangeEventArgs pArgs) {
            if (!this.PersistenceManager.IsClient) {
                return;
            }
            if (this.RowState == DataRowState.Added) {
                return;
            }
            if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
                AuditRecord = ServiceTypesAudit.Create(this.PersistenceManager, this);
            }
            //AuditRecord[pArgs.Column.ColumnName] = pArgs.ProposedValue;
        }


        protected override void AddRules(RuleList pList) {
            PropertyRequiredRule.AddToList(pList, ServiceTypes.ServiceTypeEntityColumn.ColumnName);
            pList.Add(IsServiceTypeUnique, ServiceTypeEntityColumn.ColumnName);
        }

        private bool IsServiceTypeUnique(object pTarget, RuleArgs e) {
            ServiceTypes oType = (ServiceTypes)pTarget;

            RdbQuery oQry = new RdbQuery(typeof(ServiceTypes), ServiceTypes.ServiceTypeEntityColumn, EntityQueryOp.EQ, oType.ServiceType);
            oQry.AddClause(ServiceTypes.ServiceTypeIDEntityColumn, EntityQueryOp.NE, oType.ServiceTypeID);
            EntityList<ServiceTypes> oServiceTypes = this.PersistenceManager.GetEntities<ServiceTypes>(oQry);
            if (oServiceTypes.Count > 0) {
                e.Description = "Service Type must be unique!";
                return false;
            }
            return true;
        }

        [BindingBrowsable(false)]
        public override string EntityInstanceDisplayName {
            get {
                return ServiceType;
            }
        }

        public static ServiceTypes GetServiceType(PersistenceManager pPM, string pServiceType, bool bAddNewRecIfMissing) {

            RdbQuery oQry = new RdbQuery(typeof(ServiceTypes), ServiceTypes.ServiceTypeEntityColumn, EntityQueryOp.EQ, pServiceType);
            ServiceTypes oServiceType = pPM.GetEntity<ServiceTypes>(oQry);
            if (oServiceType.IsNullEntity) {
                oServiceType = ServiceTypes.Create(pPM);
                oServiceType.ServiceType = pServiceType;
                oServiceType.Save();
            }
            return oServiceType;
        }

    }
}
