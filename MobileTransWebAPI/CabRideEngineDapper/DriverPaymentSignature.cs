﻿using Dapper;
using System.Data.SqlClient;

namespace CabRideEngineDapper {
	public partial class DriverPaymentSignature {

		public static DriverPaymentSignature Get(SqlConnection pConn, long pDriverPaymentID) {
			string sql = @"SELECT * FROM dbo.DriverPaymentSignature
							WHERE DriverPaymentID = @DriverPaymentID";

			return pConn.Get<DriverPaymentSignature>(sql, new { DriverPaymentID = pDriverPaymentID });
		}

		public static DriverPaymentSignature Get(SqlConnection pConn, string pTransNo) {
			string sql = @"SELECT  *
							FROM    DriverPaymentSignature
							WHERE   DriverPaymentID IN ( SELECT DriverPaymentID
														 FROM   DriverPayments
														 WHERE  TransNo = @TransNo )";

			return pConn.Get<DriverPaymentSignature>(sql, new { TransNo = pTransNo });
		}
	}
}
