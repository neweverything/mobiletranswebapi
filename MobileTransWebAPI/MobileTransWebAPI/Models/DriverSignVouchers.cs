﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MobileTransWebAPI.Models {
	public class DriverSignVouchers {
		public long DriverPaymentID { get; set; }
		public string TransNo { get; set; }
		public decimal Amount { get; set; }
		public decimal DriverFee { get; set; }
		public decimal RedemptionFee { get; set; }
	}
}