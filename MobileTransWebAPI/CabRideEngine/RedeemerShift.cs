﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

using Csla.Validation;
using TaxiPassCommon;
using System.Text;

namespace CabRideEngine {
	/// <summary>
	/// The developer class representing the RedeemerShift business object class.
	/// </summary>
	/// <remarks>
	/// <para>Place all application-specific business logic in this class.</para>
	/// <para>
	/// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
	/// and will not overwrite changes.
	/// </para>
	/// <para>
	/// This class inherits from the generated class, its companion in the pair of classes 
	/// defining a business object. Properties in the generated class may be overridden in 
	/// order to change the behavior or add business logic.
	/// </para>
	/// <para>
	/// This class is the final class in the inheritance chain for this business object. 
	/// It should not be inherited.
	/// </para>
	/// </remarks>
	[Serializable]
	public sealed partial class RedeemerShift : RedeemerShiftDataRow {

		#region Constructors -- DO NOT MODIFY

		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new';
		// it is also required if the class is visible from a web service.
		// DO NOT REMOVE **
		private RedeemerShift() : this(null) { }

		/// <summary>
		/// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
		/// </summary>
		/// <param name="pRowBuilder"></param>
		public RedeemerShift(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static RedeemerShift Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      RedeemerShift aRedeemerShift = pManager.CreateEntity<RedeemerShift>();
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aRedeemerShift, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aRedeemerShift.AddToManager();
		//      return aRedeemerShift;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		// Add additional logic to your business object here...
		public static RedeemerShift Create(TaxiPassRedeemers pRedeemer) {
			RedeemerShift rec = null;

			try {
				rec = GetLastShiftByRedeemer(pRedeemer);
				if (rec.StartTime.Date != DateTime.Now.Date) {
					TimeZoneConverter tzConvert = new TimeZoneConverter();
					DateTime now = tzConvert.ConvertTime(DateTime.Now, TimeZone.CurrentTimeZone.StandardName, pRedeemer.TaxiPassRedeemerAccounts.Markets.TimeZone);

					rec = pRedeemer.PersistenceManager.CreateEntity<RedeemerShift>();
					rec.TaxiPassRedeemerID = pRedeemer.TaxiPassRedeemerID;
					rec.AddToManager();
					rec.StartTime = DateTime.Now;
				}

			} catch (Exception ex) {
				throw ex;
			}
			return rec;
		}


		protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
			if (IsDeserializing) {
				return;
			}
			base.OnColumnChanging(pArgs);
			DoAudit(pArgs);
		}

		private void DoAudit(DataColumnChangeEventArgs pArgs) {
			if (!this.PersistenceManager.IsClient) {
				return;
			}
			if (this.RowState == DataRowState.Added) {
				return;
			}
			if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
				AuditRecord = RedeemerShiftAudit.Create(this.PersistenceManager, this);
			}
		}

		public override bool Closed {
			get {
				return base.Closed;
			}
			set {
				base.Closed = value;
				if (value) {
					ClosedTime = DateTime.Now;
				}
			}
		}

		public override DateTime? ClosedTime {
			get {
				return base.ClosedTime;
			}
			set {
				try {
					if (value.HasValue) {
						TimeZoneConverter tzConvert = new TimeZoneConverter();
						base.ClosedTime = tzConvert.ConvertTime(value.Value, TimeZone.CurrentTimeZone.StandardName, this.TaxiPassRedeemers.TaxiPassRedeemerAccounts.Markets.TimeZone);
					} else {
						base.ClosedTime = value;
					}
				} catch (Exception) {
					base.ClosedTime = value;
				}
			}
		}

		public override DateTime StartTime {
			get {
				return base.StartTime;
			}
			set {
				try {
					TimeZoneConverter tzConvert = new TimeZoneConverter();
					base.StartTime = tzConvert.ConvertTime(value, TimeZone.CurrentTimeZone.StandardName, this.TaxiPassRedeemers.TaxiPassRedeemerAccounts.Markets.TimeZone);
				} catch (Exception) {
					base.StartTime = value;
				}

			}
		}

		public override DateTime? EndTime {
			get {
				return base.EndTime;
			}
			set {
				try {
					if (value.HasValue) {
						TimeZoneConverter tzConvert = new TimeZoneConverter();
						base.EndTime = tzConvert.ConvertTime(value.Value, TimeZone.CurrentTimeZone.StandardName, this.TaxiPassRedeemers.TaxiPassRedeemerAccounts.Markets.TimeZone);
					} else {
						base.EndTime = value;
					}
				} catch (Exception) {
					base.EndTime = value;
				}
			}
		}


		public static RedeemerShift GetShift(PersistenceManager pPM, long pShiftID) {
			return pPM.GetEntity<RedeemerShift>(new PrimaryKey(typeof(RedeemerShift), pShiftID));
		}

		public static RedeemerShift GetLastShiftByRedeemer(TaxiPassRedeemers pRedeemer) {
			RdbQuery qry = new RdbQuery(typeof(RedeemerShift), RedeemerShift.TaxiPassRedeemerIDEntityColumn, EntityQueryOp.EQ, pRedeemer.TaxiPassRedeemerID);
			qry.AddClause(RedeemerShift.ClosedEntityColumn, EntityQueryOp.EQ, false);
			qry.AddClause(RedeemerShift.StartTimeEntityColumn, EntityQueryOp.GE, DateTime.Today.AddDays(-1));
			qry.AddOrderBy(RedeemerShift.StartTimeEntityColumn, System.ComponentModel.ListSortDirection.Descending);
			return pRedeemer.PersistenceManager.GetEntity<RedeemerShift>(qry);
		}

		public static RedeemerShift GetCurrrentShiftManager(TaxiPassRedeemers pRedeemer) {
			StringBuilder sql = new StringBuilder();
			sql.Append("select * from RedeemerShift ");
			sql.Append("where TaxiPassRedeemerID in (select TaxiPassRedeemerID from TaxiPassRedeemers where ShiftManager = 1 and TaxiPassRedeemerAccountID = ");
			sql.Append(pRedeemer.TaxiPassRedeemerAccountID);
			sql.Append(") and Closed = 0");

			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(RedeemerShift), sql.ToString());
			return pRedeemer.PersistenceManager.GetEntity<RedeemerShift>(qry);
		}

		public static EntityList<RedeemerShift> GetCurrentShiftCashiers(PersistenceManager pPM) {
			RdbQuery qry = new RdbQuery(typeof(RedeemerShift), ClosedEntityColumn, EntityQueryOp.EQ, false);
			return pPM.GetEntities<RedeemerShift>(qry);
		}

		
		// **************************************************************************************************************
		// need to check both redeemershifts and time-related ATMAccess to get all available shifts for a redeemer
		// **************************************************************************************************************
		public static EntityList<RedeemerShift> GetShiftsForRedeemerByDate(TaxiPassRedeemers pRedeemer, DateTime pDate) {
			StringBuilder sql = new StringBuilder();
			sql.Append("SELECT TOP 1 redeemershift.*");
			sql.Append(" FROM ATMAccess ");
				sql.Append(" LEFT OUTER JOIN redeemershift		ON ATMAccess.taxipassredeemerid = redeemershift.taxipassredeemerid ");
					sql.Append(" AND ATMAccess.TransactionDate 		between redeemershift.StartTime AND	redeemershift.EndTime ");
				sql.Append(" LEFT OUTER JOIN shiftname			ON shiftname.shiftnameid				= redeemershift.shifttypeid ");

			sql.Append(" WHERE ATMAccess.taxipassredeemerid = ");
			sql.Append(pRedeemer.TaxiPassRedeemerID);
				sql.Append(" and ATMAccess.TransactionDate between '");
					sql.Append(pDate.ToString());
					sql.Append("' and Dateadd(d, 1, '");
					sql.Append(pDate.ToString());
					sql.Append("') ");

			sql.Append(" UNION ");

			sql.Append(" SELECT  redeemershift.* ");
			sql.Append(" FROM redeemershift");
			sql.Append(" WHERE redeemershift.TaxiPassRedeemerID = ");
				sql.Append(pRedeemer.TaxiPassRedeemerID);

			sql.Append(" AND redeemershift.StartTime between '");
				sql.Append(pDate.ToString());
				sql.Append("' and Dateadd(d, 1, '");
				sql.Append(pDate.ToString());
				sql.Append("') ");

			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(RedeemerShift), sql.ToString());
			return pRedeemer.PersistenceManager.GetEntities<RedeemerShift>(qry);
		}


		public static EntityList<RedeemerShift> GetShiftsByDate(PersistenceManager pPM, DateTime pDate) {
			RdbQuery qry = new RdbQuery(typeof(RedeemerShift), RedeemerShift.StartTimeEntityColumn, EntityQueryOp.GE, pDate.Date);
			qry.AddClause(RedeemerShift.StartTimeEntityColumn, EntityQueryOp.LT, pDate.AddDays(1).Date);
			return pPM.GetEntities<RedeemerShift>(qry);
		}
	}

	#region EntityPropertyDescriptors.RedeemerShiftPropertyDescriptor
	////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
	////    For example: 
	////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
	//public static partial class EntityPropertyDescriptors {
	//	public partial class RedeemerShiftPropertyDescriptor : BaseEntityPropertyDescriptor {
	//    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
	//    public AdaptedPropertyDescriptor Age {
	//      get { return this.Get("Age"); }
	//    }
	//  }
	//} 
	#endregion

}