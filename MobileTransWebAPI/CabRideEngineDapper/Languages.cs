﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CabRideEngineDapper {
    public partial class Languages {

        public static List<Languages> GetAll(SqlConnection pConn) {
            return pConn.Query<Languages>("usp_LanguagesGet", commandType: CommandType.StoredProcedure).ToList();
        }

    }
}
