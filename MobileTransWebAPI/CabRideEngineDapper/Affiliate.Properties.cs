﻿using CabRideEngineDapper.Utils;
using Dapper;
using System;
using TaxiPassCommon;

namespace CabRideEngineDapper {
	public partial class Affiliate {

		[Editable(false)]
		public DateTime LocalTime {
			get {
				if (TimeZone.IsNullOrEmpty() || System.TimeZone.CurrentTimeZone.StandardName.Equals(TimeZone, StringComparison.CurrentCultureIgnoreCase)) {
					return DateTime.Now;
				}
				try {
					TimeZoneConverter tzConvert = new TimeZoneConverter();
					return tzConvert.ConvertTime(DateTime.Now, System.TimeZone.CurrentTimeZone.StandardName, TimeZone);
				} catch {
					return DateTime.Now;
				}

			}
		}

		public DateTime UtcToLocalTime(DateTime pDate) {
			if (TimeZone.IsNullOrEmpty()) {
				return pDate;
			}
			TimeZoneConverter tzConvert = new TimeZoneConverter();
			return tzConvert.ConvertTime(pDate, System.TimeZone.CurrentTimeZone.StandardName, TimeZone);
		}

		public DateTime UtcToLocalTimeActual(DateTime pDate) {
			if (TimeZone.IsNullOrEmpty()) {
				return pDate;
			}
			TimeZoneConverter tzConvert = new TimeZoneConverter();
			return tzConvert.ConvertTime(pDate, "(UTC) Coordinated Universal Time", TimeZone);
		}

		private AffiliateRef mAffiliateLogo;
		[Editable(false)]
		public string AffiliateLogo {
			get {
				CreateAffiliateLogo();
				return mAffiliateLogo.ValueString;
			}
			set {
				CreateAffiliateLogo();
				mAffiliateLogo.ValueString = value;
			}
		}

		private void CreateAffiliateLogo() {
			if (mAffiliateLogo == null) {
				mAffiliateLogo = AffiliateRef.GetCreate(AffiliateID, "Affiliate", "Logo", "");
			}
		}

		private AffiliateRef mDejavooMerchantID;
		[Editable(false)]
		public string DejavooMerchantID {
			get {
				CreateDejavooMerchantID();
				return mDejavooMerchantID.ValueString;
			}
			set {
				CreateDejavooMerchantID();
				mDejavooMerchantID.ValueString = value;
			}
		}

		private void CreateDejavooMerchantID() {
			if (mDejavooMerchantID == null) {
				mDejavooMerchantID = AffiliateRef.GetCreate(AffiliateID, "Dejavoo", "MerchantID", "");
			}
		}

		private AffiliateRef mDejavooAmexMerchantID;
		[Editable(false)]
		public string DejavooAmexMerchantID {
			get {
				CreateDejavooAmexMerchantID();
				return mDejavooAmexMerchantID.ValueString;
			}
			set {
				CreateDejavooAmexMerchantID();
				mDejavooAmexMerchantID.ValueString = value;
			}
		}

		private void CreateDejavooAmexMerchantID() {
			if (mDejavooAmexMerchantID == null) {
				mDejavooAmexMerchantID = AffiliateRef.GetCreate(AffiliateID, "Dejavoo", "AmexMerchantID", "");
			}
		}

		private AffiliateRef mDejavooMerchantInfo;
		[Editable(false)]
		public string DejavooMerchantInfo {
			get {
				CreateDejavooMerchantInfo();
				return mDejavooMerchantInfo.ValueString;
			}
			set {
				CreateDejavooMerchantInfo();
				mDejavooMerchantInfo.ValueString = value;
			}
		}

		private void CreateDejavooMerchantInfo() {
			if (mDejavooMerchantInfo == null) {
				mDejavooMerchantInfo = AffiliateRef.GetCreate(AffiliateID, "Dejavoo", "MerchantInfo", "");
			}
		}


		private AffiliateRef mReceiptTemplate;
		[Editable(false)]
		public string ReceiptTemplate {
			get {
				CreateReceiptTemplate();
				return mReceiptTemplate.ValueString;
			}
			set {
				CreateReceiptTemplate();
				mReceiptTemplate.ValueString = value;
			}
		}

		private void CreateReceiptTemplate() {
			if (mReceiptTemplate == null) {
				mReceiptTemplate = AffiliateRef.GetCreate(AffiliateID, "ChargePass", "ReceiptTemplate", "");
			}
		}

		private AffiliateRef mDoNotPay;
		[Editable(false)]
		public bool DoNotPay {
			get {
				CreateDoNotPay();
				return mDoNotPay.ValueString.AsBool();
			}
			set {
				CreateDoNotPay();
				mDoNotPay.ValueString = value.ToString();
			}
		}

		private void CreateDoNotPay() {
			if (mDoNotPay == null) {
				mDoNotPay = AffiliateRef.GetCreate(AffiliateID, "Affiliate", "DoNotPay", false.ToString());
			}
		}

		private AffiliateRef mSoftwareLicensing;
		[Editable(false)]
		public bool SoftwareLicensing {
			get {
				CreateSoftwareLicensing();
				return mSoftwareLicensing.ValueString.AsBool();
			}
			set {
				CreateSoftwareLicensing();
				mSoftwareLicensing.ValueString = value.ToString();
			}
		}

		private void CreateSoftwareLicensing() {
			if (mSoftwareLicensing == null) {
				mSoftwareLicensing = AffiliateRef.GetCreate(AffiliateID, "Affiliate", "SoftwareLicensing", false.ToString());
			}
		}

		private AffiliateRef mReceiptPrinterName;
		[Editable(false)]
		public string ReceiptPrinterName {
			get {
				CreateReceiptPrinterName();
				return mReceiptPrinterName.ValueString;
			}
			set {
				CreateReceiptPrinterName();
				mReceiptPrinterName.ValueString = value;
			}
		}

		private void CreateReceiptPrinterName() {
			if (mReceiptPrinterName == null) {
				mReceiptPrinterName = AffiliateRef.GetCreate(AffiliateID, "Affiliate", "ReceiptPrinterName", "");
			}
		}

		private AffiliateRef mReceiptFooterText;
		[Editable(false)]
		public string ReceiptFooterText {
			get {
				CreateReceiptFooterText();
				return mReceiptFooterText.ValueString;
			}
			set {
				CreateReceiptFooterText();
				mReceiptFooterText.ValueString = value;
			}
		}

		private void CreateReceiptFooterText() {
			if (mReceiptFooterText == null) {
				mReceiptFooterText = AffiliateRef.GetCreate(AffiliateID, "Affiliate", "ReceiptFooterText", "");
			}
		}

		private AffiliateRef mRecurringBilling;
		[Editable(false)]
		public bool RecurringBilling {
			get {
				CreateRecurringBilling();
				return Convert.ToBoolean(mRecurringBilling.ValueString);
			}
			set {
				CreateRecurringBilling();
				mRecurringBilling.ValueString = value.ToString();
			}
		}

		private void CreateRecurringBilling() {
			if (mRecurringBilling == null) {
				mRecurringBilling = AffiliateRef.GetCreate(AffiliateID, "Affiliate", "RecurringBilling", false.ToString());
			}
		}


		private AffiliateRef mChargeBackFee;
		[Editable(false)]
		public decimal ChargeBackFee {
			get {
				CreateChargeBackFee();
				return mChargeBackFee.ValueString.AsDecimal();
			}
			set {
				CreateChargeBackFee();
				mChargeBackFee.ValueString = value.ToString();
			}
		}

		private void CreateChargeBackFee() {
			if (mChargeBackFee == null) {
				mChargeBackFee = AffiliateRef.GetCreate(AffiliateID, "Affiliate", "ChargeBackFee", "0");
			}
		}

		private AffiliateRef mDiscountRate;
		[Editable(false)]
		public decimal DiscountRate {
			get {
				CreateDiscountRate();
				return mDiscountRate.ValueString.AsDecimal();
			}
			set {
				CreateDiscountRate();
				mDiscountRate.ValueString = value.ToString();
			}
		}

		private void CreateDiscountRate() {
			if (mDiscountRate == null) {
				mDiscountRate = AffiliateRef.GetCreate(AffiliateID, "Affiliate", "DiscountRate", "0");
			}
		}

		private AffiliateRef mCustomServiceFeeText;
		[Editable(false)]
		public string CustomServiceFeeText {
			get {
				CreateCustomServiceFeeText();
				return mCustomServiceFeeText.ValueString;
			}
			set {
				CreateCustomServiceFeeText();
				mCustomServiceFeeText.ValueString = value;
			}
		}

		private void CreateCustomServiceFeeText() {
			if (mCustomServiceFeeText == null) {
				mCustomServiceFeeText = AffiliateRef.GetCreate(AffiliateID, "Affiliate", "CustomServiceFeeText", "");
			}
		}

		//private AffiliateRef mCardGatewayGroupsID;
		//[Editable(false)]
		//public long CardGatewayGroupsID {
		//    get {
		//        CreateCardGatewayGroupsID();
		//        return mCardGatewayGroupsID.ValueString.AsLong();
		//    }
		//    set {
		//        CreateCardGatewayGroupsID();
		//        mCardGatewayGroupsID.ValueString = value.ToString();
		//    }
		//}

		//private void CreateCardGatewayGroupsID() {
		//    if (mCardGatewayGroupsID == null) {
		//        mCardGatewayGroupsID = AffiliateRef.GetCreate(this.AffiliateID, "Affiliate", "CardGatewayGroupsID", "0");
		//    }
		//}

		[Editable(false)]
		public int AffiliateDriversCount {
			get {
				string sql = @"SELECT COUNT(*) FROM dbo.AffiliateDrivers
                                WHERE AffiliateID = @AffiliateID";
				using (var conn = SqlHelper.OpenSqlConnection()) {
					return conn.Get<int>(sql, new { AffiliateID = this.AffiliateID });
				}

			}
		}

		[Editable(false)]
		public int VehiclesCount {
			get {
				string sql = @"SELECT COUNT(*) FROM dbo.Vehicles
                                WHERE AffiliateID = @AffiliateID";
				using (var conn = SqlHelper.OpenSqlConnection()) {
					return conn.Get<int>(sql, new { AffiliateID = this.AffiliateID });
				}

			}
		}


		//private AffiliateRef mEnableECheck;
		//[Editable(false)]
		//public bool EnableECheck {
		//    get {
		//        CreateEnableECheck();
		//        return mEnableECheck.ValueString.AsBool();
		//    }
		//    set {
		//        CreateEnableECheck();
		//        mEnableECheck.ValueString = value.ToString();
		//    }
		//}

		//private void CreateEnableECheck() {
		//    if (mEnableECheck == null) {
		//        mEnableECheck = AffiliateRef.GetCreate(AffiliateID, "Affiliate", "EnableECheck", false.ToString());
		//    }
		//}

		private AffiliateRef mInvoicing;
		[Editable(false)]
		public bool Invoicing {
			get {
				CreateInvoicing();
				return mInvoicing.ValueString.ToBool();
			}
			set {
				CreateInvoicing();
				mCustomServiceFeeText.ValueString = value.ToString();
			}
		}

		private void CreateInvoicing() {
			if (mInvoicing == null) {
				mInvoicing = AffiliateRef.GetCreate(this.AffiliateID, "Affiliate", "Invoicing", false.ToString());
			}
		}

		private AffiliateRef mECheckReceiptFooterText;
		[Editable(false)]
		public string ECheckReceiptFooterText {
			get {
				CreateECheckReceiptFooterText();
				return mECheckReceiptFooterText.ValueString;
			}
			set {
				CreateECheckReceiptFooterText();
				mECheckReceiptFooterText.ValueString = value;
			}
		}

		private void CreateECheckReceiptFooterText() {
			if (mECheckReceiptFooterText == null) {
				mECheckReceiptFooterText = AffiliateRef.GetCreate(AffiliateID, "Affiliate", "ECheckReceiptFooterText", "");
			}
		}

		private AffiliateRef mInvoiceAllowPartialPayments;
		[Editable(false)]
		public bool InvoiceAllowPartialPayments {
			get {
				CreateInvoiceAllowPartialPayments();
				return mInvoiceAllowPartialPayments.ValueString.ToBool();
			}
			set {
				CreateInvoiceAllowPartialPayments();
				mInvoiceAllowPartialPayments.ValueString = value.ToString();
			}
		}

		private void CreateInvoiceAllowPartialPayments() {
			if (mInvoiceAllowPartialPayments == null) {
				mInvoiceAllowPartialPayments = AffiliateRef.GetCreate(this.AffiliateID, "Affiliate", "InvoiceAllowPartialPayments", false.ToString());
			}
		}

		private AffiliateRef mInvoiceAllowRecurringPayments;
		[Editable(false)]
		public bool InvoiceAllowRecurringPayments {
			get {
				CreateInvoiceAllowRecurringPayments();
				return mInvoiceAllowRecurringPayments.ValueString.ToBool();
			}
			set {
				CreateInvoiceAllowRecurringPayments();
				mInvoiceAllowRecurringPayments.ValueString = value.ToString();
			}
		}

		private void CreateInvoiceAllowRecurringPayments() {
			if (mInvoiceAllowRecurringPayments == null) {
				mInvoiceAllowRecurringPayments = AffiliateRef.GetCreate(this.AffiliateID, "Affiliate", "InvoiceAllowRecurringPayments", false.ToString());
			}
		}

		// Bill page title block 
		private AffiliateRef mBillPaymentPageTitle;
		[Editable(false)]
		public string BillPaymentPageTitle {
			get {
				CreateBillPaymentPageTitle();
				return mBillPaymentPageTitle.ValueString;
			}
			set {
				CreateBillPaymentPageTitle();
				mBillPaymentPageTitle.ValueString = value;
			}
		}

		private void CreateBillPaymentPageTitle() {
			if (mBillPaymentPageTitle == null) {
				mBillPaymentPageTitle = AffiliateRef.GetCreate(this.AffiliateID, "Affiliate", "BillPaymentPageTitle", " ");
			}
		}

		// Bill page sub title block 
		private AffiliateRef mBillPaymentPageSubTitle;
		[Editable(false)]
		public string BillPaymentPageSubTitle {
			get {
				CreateBillPaymentPageSubTitle();
				return mBillPaymentPageSubTitle.ValueString;
			}
			set {
				CreateBillPaymentPageSubTitle();
				mBillPaymentPageSubTitle.ValueString = value;
			}
		}

		private void CreateBillPaymentPageSubTitle() {
			if (mBillPaymentPageSubTitle == null) {
				mBillPaymentPageSubTitle = AffiliateRef.GetCreate(this.AffiliateID, "Affiliate", "BillPaymentPageSubTitle", " ");
			}
		}

		// Customer Email Required 
		private AffiliateRef mCustomerEmailRequired;
		[Editable(false)]
		public bool CustomerEmailRequired {
			get {
				CreateCustomerEmailRequired();
				return mCustomerEmailRequired.ValueString.AsBool();
			}
			set {
				CreateCustomerEmailRequired();
				mCustomerEmailRequired.ValueString = value.ToString();
			}
		}

		private void CreateCustomerEmailRequired() {
			if (mCustomerEmailRequired == null) {
				mCustomerEmailRequired = AffiliateRef.GetCreate(this.AffiliateID, "Affiliate", "CustomerEmailRequired", false.ToString());
			}
		}

		// Customer Ref Invoice No Required
		private AffiliateRef mCustomerRefInvoiceNoRequired;
		[Editable(false)]
		public bool CustomerRefInvoiceNoRequired {
			get {
				CreateCustomerRefInvoiceNoRequired();
				return mCustomerRefInvoiceNoRequired.ValueString.AsBool();
			}
			set {
				CreateCustomerRefInvoiceNoRequired();
				mCustomerRefInvoiceNoRequired.ValueString = value.ToString();
			}
		}

		private void CreateCustomerRefInvoiceNoRequired() {
			if (mCustomerRefInvoiceNoRequired == null) {
				mCustomerRefInvoiceNoRequired = AffiliateRef.GetCreate(this.AffiliateID, "Affiliate", "CustomerRefInvoiceNoRequired", false.ToString());
			}
		}

		[Editable(false)]
		public string TotalText {
			get {
				string text = "Total";
				SystemDefaultsDict rec = SystemDefaultsDict.GetByKey($"Affiliate:{this.AffiliateID}", "Total", false);
				if (!rec.ValueString.IsNullOrEmpty()) {
					text = rec.ValueString;
				}
				return text;
			}
		}

	}
}