using System;
using System.Collections.Generic;
using System.Text;

using IdeaBlade.Util;

namespace CabRideEngine {
	public class IncidentTypes {

		public static string Affiliates {
			get {
				return "Affiliates";
			}
		}

		public static string Reservations {
			get {
				return "Reservations";
			}
		}

		private string mIncidentType;
		private bool mIsNullEntity = false;

		private static BindableList<IncidentTypes> msEntities;
		private static IncidentTypes msNullEntity;

		/// <summary>Class Constructor (Private)</summary>
		static IncidentTypes() {

			// Create the null entity
			msNullEntity = new IncidentTypes("<none>");
			msNullEntity.mIsNullEntity = true;

			// Populate list of Incident Types.
			msEntities = new BindableList<IncidentTypes>();
			msEntities.Add(new IncidentTypes(Affiliates));
			msEntities.Add(new IncidentTypes(Reservations));
		}

		/// <summary>Constructor (Private)</summary>
		private IncidentTypes(string pIncidentType) {
			mIncidentType = pIncidentType;
		}

		/// <summary>Get the Incident Types null entity.</summary>
		public static IncidentTypes NullEntity {
			get {
				return msNullEntity;
			}
		}

		/// <summary>Get the list of all Incident Types.</summary>
		public static BindableList<IncidentTypes> GetAll() {
			return msEntities;
		}

		/// <summary>Get the list of all Incident Types.</summary>
		public static IncidentTypes GetById(string pId) {
			if (String.IsNullOrEmpty(pId))
				return NullEntity;
			foreach (IncidentTypes oIncidentType in msEntities) {
				if (oIncidentType.IncidentType == pId)
					return oIncidentType;
			}
			return NullEntity;
		}

		/// <summary>Return true if this is the null entity</summary>
		[BindingBrowsable(false)]
		public bool IsNullEntity {
			get {
				return mIsNullEntity;
			}
		}

		/// <summary>Get the Incident Type</summary>
		public string IncidentType {
			get {
				return mIncidentType;
			}
		}


	}
}
