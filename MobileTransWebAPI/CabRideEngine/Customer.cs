﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Text;

using CabRideEngine.Utils;

using TaxiPassCommon;
using TaxiPassCommon.Entities;

using IdeaBlade.Persistence;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

using Csla.Validation;
using System.Threading;

namespace CabRideEngine {
	[Serializable]
	public sealed class Customer : CustomerDataRow {

		#region Constructors -- DO NOT MODIFY
		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new'
		private Customer()
			: this(null) {
		}

		// Typed DataSet constructor (needed by framework) - DO NOT REMOVE
		public Customer(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static Customer Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      Customer aCustomer = pManager.CreateEntity<Customer>();
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aCustomer, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aCustomer.AddToManager();
		//      return aCustomer;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		// Add additional logic to your business object here...
		
		// **********************************************************
		// Create a blank customer recore
		// **********************************************************
		public static Customer Create(PersistenceManager pManager) {
			Customer oCustomer = null;
			try {
				oCustomer = (Customer)pManager.CreateEntity(typeof(Customer));

				oCustomer.Salt = StringUtils.GenerateSalt();
				oCustomer.AddToManager();

			} catch (Exception ex) {
				throw ex;
			}
			return oCustomer;
		}


		protected override void AddRules(RuleList pList) {
			PropertyRequiredRule.AddToList(pList, NameColumn.ColumnName);
			//hg 11/14/06 don't think we need this as a number could be repeated but for different accounts
			//PropertyRequiredRule.AddToList(pList, HomePhoneColumn.ColumnName);
			//pList.Add(IsHomePhoneUnique, HomePhoneColumn.ColumnName);
			PropertyRequiredRule.AddToList(pList, Customer.AccountNoEntityColumn.ColumnName);
		}

		private bool IsHomePhoneUnique(object pTarget, RuleArgs e) {
			Customer oCustomer = (Customer)pTarget;

			RdbQuery oQry = new RdbQuery(typeof(Customer), Customer.HomePhoneEntityColumn, EntityQueryOp.EQ, oCustomer.HomePhone);
			oQry.AddClause(Customer.CustomerIDEntityColumn, EntityQueryOp.NE, oCustomer.CustomerID);
			EntityList<Customer> oCustomers = this.PersistenceManager.GetEntities<Customer>(oQry);
			if (oCustomers.Count > 0) {
				e.Description = "A customer already exists with this Home Phone number!";
				return false;
			}
			return true;
		}

		[BindingBrowsable(false)]
		public override string EntityInstanceDisplayName {
			get {
				return Name;
			}
		}


		public static Customer GetCustomer(PersistenceManager pPM, long pCustomerID) {
			RdbQuery oQry = new RdbQuery(typeof(Customer), Customer.CustomerIDEntityColumn, EntityQueryOp.EQ, pCustomerID);
			return pPM.GetEntity<Customer>(oQry);
		}

		public static EntityList<Customer> GetCustomerByPhone(PersistenceManager pPM, string pPhone) {
			RdbQuery oQry = new RdbQuery(typeof(Customer), Customer.HomePhoneEntityColumn, EntityQueryOp.EQ, pPhone);
			oQry.AddClause(Customer.CellPhoneEntityColumn, EntityQueryOp.EQ, pPhone);
			oQry.AddOperator(EntityBooleanOp.Or);
			return pPM.GetEntities<Customer>(oQry);
		}

		public static Customer GetCustomer(PersistenceManager pPM, string pEMail, string pAccountNo) {
			if (string.IsNullOrEmpty(pEMail)) {
				return pPM.GetNullEntity<Customer>();
			}
			RdbQuery oQry = new RdbQuery(typeof(Customer), Customer.EMailEntityColumn, EntityQueryOp.EQ, pEMail);
			if (!string.IsNullOrEmpty(pAccountNo)) {
				oQry.AddClause(Customer.AccountNoEntityColumn, EntityQueryOp.EQ, pAccountNo);
			}
			return pPM.GetEntity<Customer>(oQry);
		}

		public static Customer GetCustomerByCell(PersistenceManager pPM, string pCell, string pAccountNo) {
			if (pCell.IsNullOrEmpty()) {
				return pPM.GetNullEntity<Customer>();
			}
			RdbQuery oQry = new RdbQuery(typeof(Customer), Customer.CellPhoneEntityColumn, EntityQueryOp.EQ, pCell);
			if (!string.IsNullOrEmpty(pAccountNo)) {
				oQry.AddClause(Customer.AccountNoEntityColumn, EntityQueryOp.EQ, pAccountNo);
			}
			return pPM.GetEntity<Customer>(oQry);
		}

		public static EntityList<Customer> GetCustomersByAccount(PersistenceManager pPM, string pAccountNo) {
			RdbQuery qry = new RdbQuery(typeof(Customer), Customer.AccountNoEntityColumn, EntityQueryOp.EQ, pAccountNo);
			qry.AddOrderBy(Customer.NameEntityColumn);
			return pPM.GetEntities<Customer>(qry);
		}

		public static Customer GetCustomerByEmployeeNumber(Accounts pAccount, string pEmployeeNumber) {
			RdbQuery qry = new RdbQuery(typeof(Customer), Customer.EmployeeNumberEntityColumn, EntityQueryOp.EQ, pEmployeeNumber);
			qry.AddClause(Customer.AccountNoEntityColumn, EntityQueryOp.EQ, pAccount.AccountNo);
			return pAccount.PersistenceManager.GetEntity<Customer>(qry);
		}

		public override string Password {
			get {
				return base.Password;
			}
			set {
				Encryption encrypt = new Encryption();
				base.Password = encrypt.Encrypt(value, Salt);
			}
		}

		public string GetPassword {
			get {
				Encryption encrypt = new Encryption();
				return encrypt.Decrypt(Password, Salt);
			}
		}

		public override string AccountNo {
			get {
				return base.AccountNo;
			}
			set {
				base.AccountNo = value;
				if (this.Accounts.CompanyBilled) {
					EntityList<Customer> oCustomers = GetCustomerAccountAdmins(this.PersistenceManager, value);
					if (oCustomers.Count > 0) {
						//use 1st account found
						BillTo = oCustomers[0].CustomerID;
					}
				}
			}
		}

		public static EntityList<Customer> GetCustomerAccountAdmins(PersistenceManager pPM, string pAccountNo) {
			RdbQuery qry = new RdbQuery(typeof(Customer), Customer.AccountNoEntityColumn, EntityQueryOp.EQ, pAccountNo);
			qry.AddClause(Customer.AdminEntityColumn, EntityQueryOp.EQ, true);
			return pPM.GetEntities<Customer>(qry);
		}

		
		private string mCustomerToken;
		public string CustomerToken {
			get {
				return mCustomerToken;
			}
			set {
				mCustomerToken = value;
			}
		}

		private string mCouponCode;
		public string CouponCode {
			get {
				return mCouponCode;
			}
			set {
				mCouponCode = value;
			}
		}


		private decimal mCouponValue;
		public decimal CouponValue {
			get {
				return mCouponValue;
			}
			set {
				mCouponValue = value;
			}
		}

		private bool mCouponValid;
		public bool CouponValid {
			get {
				return mCouponValid;
			}
			set {
				mCouponValid = value;
			}
		}


		private string mReserveNo;
		public string ReserveNo {
			get {
				return mReserveNo;
			}
			set {
				mReserveNo = value;
			}
		}

		private int mAlertCode;
		public int AlertCode {
			get {
				return mAlertCode;
			}
			set {
				mAlertCode = value;
			}
		}


		// ***********************************************************************************************
		// Get a customer by his token
		// ***********************************************************************************************
		public static Customer GetCustomerbyToken(PersistenceManager pPM, string pSearch) {
			return GetCustomerbyTokenOrEmail(pPM, pSearch, "1", "", "");
		}

		// ***********************************************************************************************
		// Get a customer by his email
		// ***********************************************************************************************
		public static Customer GetCustomerbyEmailOnly(PersistenceManager pPM, string pSearch) {
			return GetCustomerbyTokenOrEmail(pPM, pSearch, "0", "", "1");
		}

		// ***********************************************************************************************
		// Get a customer by his token/email
		// ***********************************************************************************************
		public static Customer GetCustomerbyTokenOrEmail(PersistenceManager pPM, string pSearch) {
			return GetCustomerbyTokenOrEmail(pPM, pSearch, "0", "", "");
		}


		// ***********************************************************************************************
		// Get a customer by his token/email
		// pSearch:  Search string
		// Optional pTokenOnly: 0 = Search by token, email, or phone.  1 = Token only
		// Optional pCouponCode: Validate coupone code has not been used by customer
		// Notes can contain:  Token:P6CNMLEY|CouponAmount:0.00
		// ***********************************************************************************************
		public static Customer GetCustomerbyTokenOrEmail(PersistenceManager pPM, string pSearch, string pTokenOnly, string pCouponCode, string pEmailOnly) {
			StringBuilder sql = new StringBuilder();
			sql.AppendFormat("usp_CustomerGetByTokenPhoneEmail @Lookup= '{0}'", pSearch);
			if (!pTokenOnly.IsNullOrEmpty()) {
				sql.AppendFormat(", @TokenOnly = {0}", pTokenOnly);
			}
			if (!pCouponCode.IsNullOrEmpty()) {
				sql.AppendFormat(", @CouponCode = '{0}'", pCouponCode);
			}
			if (pEmailOnly == "1") {
				sql.Append(", @EmailOnly = 1");
			}

			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(Customer), sql.ToString());
			Entity[] x = DynamicEntities.DynamicDBCall(pPM, sql.ToString());
			Console.WriteLine(x.Length);


			Customer oCustomer = pPM.GetEntity<Customer>(qry);
			if (!oCustomer.Notes.IsNullOrEmpty()) {
				try {
					string sNotes = oCustomer.Notes;

					// Token:P6CNMLEY|
					int iPos = sNotes.ToLower().IndexOf("token:");
					if (iPos >= 0) {
						iPos = iPos + "token:".Length;
						string sToken = sNotes.Substring(iPos, sNotes.IndexOf("|") - iPos);
						oCustomer.CustomerToken = sToken;
						sNotes = sNotes.Substring(iPos + sToken.Length + 1);
					}

					// CouponAmount:5.00|
					iPos = sNotes.ToLower().IndexOf("couponamount:");
					if (iPos >= 0) {
						iPos = iPos + "CouponAmount:".Length;
						string sCouponAmount = sNotes.Substring(iPos, sNotes.IndexOf("|") - iPos);
						oCustomer.CouponCode = pCouponCode;
						oCustomer.mCouponValue = decimal.Parse(sCouponAmount);

						oCustomer.CouponValid = false;
						if (oCustomer.mCouponValue > 0) {
							oCustomer.CouponValid = true;
						}
						sNotes = sNotes.Substring(iPos + sCouponAmount.Length + 1);
					}

					// ReserveNo:abc123|
					iPos = sNotes.ToLower().IndexOf("reserveno:");
					if (iPos >= 0) {
						iPos = iPos + "ReserveNo:".Length;
						string sReserveNo = sNotes.Substring(iPos, sNotes.IndexOf("|") - iPos);
						oCustomer.ReserveNo = sReserveNo;
						sNotes = sNotes.Substring(iPos + sReserveNo.Length + 1);
					}

					// AlertCode:10|
					iPos = sNotes.ToLower().IndexOf("alertcode:");
					if (iPos >= 0) {
						iPos = iPos + "AlertCode:".Length;
						int iAlertCode = int.Parse(sNotes.Substring(iPos, sNotes.IndexOf("|") - iPos));
						oCustomer.AlertCode = iAlertCode;
						sNotes = sNotes.Substring(iPos + iAlertCode.ToString().Length + 1);

					}
				} catch (Exception ex) {
					Console.WriteLine(ex.Message);
				}

			}
			
			return oCustomer;
		}


		public bool IsVIP {
			get {
				bool bVIP = this.Vip.GetValueOrDefault();
				if (!bVIP) {
					EntityList<VIPList> oList = VIPList.GetAllVIPList(this.PersistenceManager);
					foreach (VIPList oVIP in oList) {
						if (this[oVIP.SearchField].ToString().ToLower().StartsWith(oVIP.Value.ToLower())) {
							bVIP = true;
							break;
						}
					}
				}
				return bVIP;
			}
		}

		public string VIPText {
			get {
				string sVIP = "";
				if (this.IsVIP) {
					sVIP = "VIP";
				}
				return sVIP;
			}
		}
	}

}
