﻿using Dapper;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace CabRideEngineDapper {
    public class usp_FundPayCardInfo {

        public int AchPaidCount { get; set; }
        public int PayCardLoadsPerMonth { get; set; }
        public int VoucherCount { get; set; }
        public decimal DriverTotal { get; set; }
        public decimal FundingAcctAmt { get; set; }
        public decimal DisplayFundingMsgAt { get; set; }
        public int MissingDejavooBatchCount { get; set; }
        public decimal MissingBatchDriverTotal { get; set; }

        public static usp_FundPayCardInfo GetVoucherCountForPayCard(SqlConnection pConn, string pTransCardAdminNo) {
            return pConn.Query<usp_FundPayCardInfo>("usp_FundPayCardInfo", new { TransCardAdminNo = pTransCardAdminNo }, commandType: CommandType.StoredProcedure).FirstOrDefault();
        }

    }
}
