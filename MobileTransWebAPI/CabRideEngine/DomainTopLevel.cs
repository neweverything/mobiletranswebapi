using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

using Csla.Validation;

namespace CabRideEngine {
	/// <summary>
	/// The developer class representing the DomainTopLevel business object class.
	/// </summary>
	/// <remarks>
	/// <para>Place all application-specific business logic in this class.</para>
	/// <para>
	/// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
	/// and will not overwrite changes.
	/// </para>
	/// <para>
	/// This class inherits from the generated class, its companion in the pair of classes 
	/// defining a business object. Properties in the generated class may be overridden in 
	/// order to change the behavior or add business logic.
	/// </para>
	/// <para>
	/// This class is the final class in the inheritance chain for this business object. 
	/// It should not be inherited.
	/// </para>
	/// </remarks>
	[Serializable]
	public sealed partial class DomainTopLevel : DomainTopLevelDataRow {

		#region Constructors -- DO NOT MODIFY

		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new';
		// it is also required if the class is visible from a web service.
		// DO NOT REMOVE **
		private DomainTopLevel() : this(null) { }

		/// <summary>
		/// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
		/// </summary>
		/// <param name="pRowBuilder"></param>
		public DomainTopLevel(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static DomainTopLevel Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      DomainTopLevel aDomainTopLevel = pManager.CreateEntity<DomainTopLevel>();
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aDomainTopLevel, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aDomainTopLevel.AddToManager();
		//      return aDomainTopLevel;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		// Add additional logic to your business object here...
		public static DomainTopLevel Create(PersistenceManager pPM) {
			DomainTopLevel rec = null;
			try {
				rec = pPM.CreateEntity<DomainTopLevel>();
				rec.AddToManager();

			} catch (Exception ex) {
				throw ex;
			}
			return rec;
		}

		protected override void AddRules(RuleList pList) {
			PropertyRequiredRule.AddToList(pList, TopLevelEntityColumn.ColumnName);
			pList.Add(IsTopLevelUnique, TopLevelEntityColumn.ColumnName);
		}

		private bool IsTopLevelUnique(object pTarget, RuleArgs e) {
			DomainTopLevel rec = (DomainTopLevel)pTarget;

			RdbQuery qry = new RdbQuery(typeof(DomainTopLevel), DomainTopLevel.TopLevelEntityColumn, EntityQueryOp.EQ, rec.TopLevel);
			qry.AddClause(DomainTopLevel.DomainTopLevelIDEntityColumn, EntityQueryOp.NE, rec.DomainTopLevelID);
			EntityList<DomainTopLevel> list = this.PersistenceManager.GetEntities<DomainTopLevel>(qry);
			if (list.Count > 0) {
				e.Description = "Domain Top Level must be unique!";
				return false;
			}
			return true;
		}

		[BindingBrowsable(false)]
		public override string EntityInstanceDisplayName {
			get {
				return TopLevel;
			}
		}

		protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
			if (IsDeserializing)
				return;
			base.OnColumnChanging(pArgs);
			DoAudit(pArgs);
		}

		private void DoAudit(DataColumnChangeEventArgs pArgs) {
			if (!this.PersistenceManager.IsClient) {
				return;
			}
			if (this.RowState == DataRowState.Added) {
				return;
			}
			if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
				AuditRecord = DomainTopLevelAudit.Create(this);
			}
		}

		public static EntityList<DomainTopLevel> GetAll(PersistenceManager pPM) {
			RdbQuery qry = new RdbQuery(typeof(DomainTopLevel));
			qry.AddOrderBy(TopLevelEntityColumn);
			return pPM.GetEntities<DomainTopLevel>(qry);
		}

		public static DateTime GetLastUpdated(PersistenceManager pPM) {
			RdbQuery qry = new RdbQuery(typeof(DomainTopLevel));
			qry.AddOrderBy(DomainName.ModifiedDateEntityColumn, System.ComponentModel.ListSortDirection.Descending);
			qry.Top = 1;

			return pPM.GetEntity<DomainTopLevel>(qry).ModifiedDate;
		}
	}

	#region EntityPropertyDescriptors.DomainTopLevelPropertyDescriptor
	////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
	////    For example: 
	////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
	//public static partial class EntityPropertyDescriptors {
	//	public partial class DomainTopLevelPropertyDescriptor : BaseEntityPropertyDescriptor {
	//    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
	//    public AdaptedPropertyDescriptor Age {
	//      get { return this.Get("Age"); }
	//    }
	//  }
	//} 
	#endregion

}