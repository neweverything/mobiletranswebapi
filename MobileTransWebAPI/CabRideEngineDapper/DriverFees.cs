﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Dapper;
using TaxiPassCommon;
using CabRideEngineDapper.Utils;

namespace CabRideEngineDapper {
	public partial class DriverFee {

        [Editable(false)]
        public bool PercentFee {
            get {
                return IsPercent;
            }
        }

		public static List<DriverFee> GetByAffiliate(long pAffiliateID) {
			string sql = @"SELECT * FROM dbo.DriverFee
							WHERE AffiliateID = @AffiliateID
							ORDER BY StartAmount";
			using (var conn = SqlHelper.OpenSqlConnection()) {
				return conn.Query<DriverFee>(sql, new { AffiliateID = pAffiliateID }).ToList();
			}
		}

    }
}
