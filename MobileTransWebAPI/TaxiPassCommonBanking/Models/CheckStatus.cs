﻿namespace TaxiPassCommon.Banking.Models {

	public class CheckStatus {
		public string RefNum { get; set; }
		public string Date { get; set; }
		public string Customer { get; set; }
		public string Invoice { get; set; }
		public string OrderNum { get; set; }
		public string Type { get; set; }
		public string Account { get; set; }
		public string Routing { get; set; }
		public string AuthCode { get; set; }
		public string Status { get; set; }
		public decimal Amount { get; set; }
		public string Source { get; set; }
		public string EffectiveDate { get; set; }
		public string ProcessedDate { get; set; }
		public string SettledDate { get; set; }
		public string ReturnedDate { get; set; }
		public string StatusMessage { get; set; }
	}

}
