﻿
namespace MobileTransWebAPI.Models {
	public partial class ChargeResult {
		public string ErrorMsg { get; set; }
		public string AVSResultCode { get; set; }
		public string CVVResultCode { get; set; }
		public string ErrorCode { get; set; }

		public string AuthCode { get; set; }
		public string VoucherNo { get; set; }
		public bool IsTestCard { get; set; }
		public decimal TPFee { get; set; }
		public string AuthDate { get; set; }
		public string ChargeDate { get; set; }
		public bool Saved { get; set; }
		public string CardNo { get; set; }
		public decimal Fare { get; set; }
		public decimal Tolls { get; set; }
		public decimal Tip { get; set; }
		public decimal DriverFee { get; set; }
		public decimal CashierFee {
			get {
				return DriverFee;
			}
			set {
			}
		}
		public decimal DispatchFee { get; set; }
		public decimal DriverTotal { get; set; }
		public decimal CashierTotal {
			get {
				return DriverTotal;
			}
			set {
			}
		}
		public string SendReceiptToEMail { get; set; }
		public string SendReceiptToPhone { get; set; }
		public string SoftDescriptor { get; set; }
		public string CardType { get; set; }
		public string AffiliateEmail { get; set; }
		public long DriverPaymentID { get; set; }
		public string TransType { get; set; }
		public string CardHolder { get; set; }
	}
}