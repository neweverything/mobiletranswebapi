﻿using CabRideEngineDapper.Utils;
using Dapper;
using System;
using System.Data.SqlClient;
using System.Linq;
using TaxiPassCommon;

namespace CabRideEngineDapper {
    public partial class DriverPaymentsPendingMatches {

        #region Related Tables

        private TaxiPassRedeemers mTaxiPassRedeemers;
        public TaxiPassRedeemers TaxiPassRedeemers {
            get {
                if (mTaxiPassRedeemers == null) {
                    mTaxiPassRedeemers = TaxiPassRedeemers.GetRedeemer(this.TaxiPassRedeemerID);
                }
                return mTaxiPassRedeemers;
            }
        }

        private DriverPayments mDriverPayments;
        public DriverPayments DriverPayments {
            get {
                if (mDriverPayments == null) {
                    using (var conn = SqlHelper.OpenSqlConnection()) {
                        mDriverPayments = DriverPayments.GetPayment(conn, this.DriverPaymentID);
                    }
                }
                return mDriverPayments;
            }
        }

        #endregion

        #region Custom Properties

        [Editable(false)]
        public DateTime SetDate {
            set {
                base.Date = value;
                if (DriverPayments.Affiliate.TimeZone.IsNullOrEmpty()) {
                    DriverPayments.PendingMatchDate = value;
                } else {
                    TimeZoneConverter convert = new TimeZoneConverter();
                    DriverPayments.PendingMatchDate = convert.ConvertTime(value, TimeZone.CurrentTimeZone.StandardName, this.DriverPayments.Affiliate.TimeZone);
                }
            }
        }

        #endregion

        public static DriverPaymentsPendingMatches Create(DriverPayments pDriverPayment) {
            DriverPaymentsPendingMatches rec = null;
            try {
                rec = Create();

                //oPM.GenerateId(rec, DriverPaymentsPendingMatches.DriverPaymentsPendingMatchIDEntityColumn);
                rec.DriverPaymentID = pDriverPayment.DriverPaymentID;
                rec.SetDate = DateTime.Now;

            } catch (Exception ex) {
                throw ex;
            }
            return rec;
        }

        public override int Save() {
            if (IsNewRecord) {
                this.DriverPaymentsPendingMatchID = NextID.GetNextKey(DriverPaymentsPendingMatches.TableDef.TABLE_NAME);
            }
            return base.Save();
        }

        public override int Save(SqlConnection pConn, string pModifiedBy) {
            if (IsNewRecord) {
                this.DriverPaymentsPendingMatchID = NextID.GetNextKey(DriverPaymentsPendingMatches.TableDef.TABLE_NAME);
            }

            return base.Save(pConn, pModifiedBy);
        }

        // ************************************************************************************
        // Get or create a record
        // ************************************************************************************
        public static DriverPaymentsPendingMatches GetOrCreate(DriverPayments pDriverPayment) {
            DriverPaymentsPendingMatches oDriverPaymentsPendingMatches = GetPendingMatch(pDriverPayment);
            if (oDriverPaymentsPendingMatches.IsNullEntity) {
                oDriverPaymentsPendingMatches = Create(pDriverPayment);
            }
            return oDriverPaymentsPendingMatches;
        }



        public static DriverPaymentsPendingMatches GetPendingMatch(DriverPayments pDriverPayment) {
            return GetPendingMatch(pDriverPayment.DriverPaymentID);
        }

        public static DriverPaymentsPendingMatches GetPendingMatch(long pDriverPaymentID) {
            string sql = "Select * FROM DriverPaymentsPendingMatches WHERE DriverPaymentID = @DriverPaymentID";
            using (var conn = SqlHelper.OpenSqlConnection()) {
                return conn.Query<DriverPaymentsPendingMatches>(sql, new { DriverPaymentID = pDriverPaymentID }).DefaultIfEmpty(DriverPaymentsPendingMatches.GetNullEntity()).FirstOrDefault();
            }
        }
    }
}
