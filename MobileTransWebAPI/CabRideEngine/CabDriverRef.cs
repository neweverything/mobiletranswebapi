using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Linq;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;
using System.Text;

namespace CabRideEngine {
	/// <summary>
	/// The developer class representing the CabDriverRef business object class.
	/// </summary>
	/// <remarks>
	/// <para>Place all application-specific business logic in this class.</para>
	/// <para>
	/// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
	/// and will not overwrite changes.
	/// </para>
	/// <para>
	/// This class inherits from the generated class, its companion in the pair of classes 
	/// defining a business object. Properties in the generated class may be overridden in 
	/// order to change the behavior or add business logic.
	/// </para>
	/// <para>
	/// This class is the final class in the inheritance chain for this business object. 
	/// It should not be inherited.
	/// </para>
	/// </remarks>
	[Serializable]
	public sealed partial class CabDriverRef : CabDriverRefDataRow {

		#region Constructors -- DO NOT MODIFY

		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new';
		// it is also required if the class is visible from a web service.
		// DO NOT REMOVE **
		private CabDriverRef() : this(null) { }

		/// <summary>
		/// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
		/// </summary>
		/// <param name="pRowBuilder"></param>
		public CabDriverRef(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static CabDriverRef Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      CabDriverRef aCabDriverRef = pManager.CreateEntity<CabDriverRef>();
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aCabDriverRef, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aCabDriverRef.AddToManager();
		//      return aCabDriverRef;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		// Add additional logic to your business object here...
		public static CabDriverRef Create(PersistenceManager pPM, string pTableName, long pSourceID, string pFieldName, string pValue) {
			CabDriverRef oCabDriverRef = null;
			StringBuilder sql = new StringBuilder();
			sql.AppendFormat("usp_CabDriverRefInsert @SourceID = {0}", pSourceID.ToString());
			sql.AppendFormat(", @SourceTable = '{0}'", pTableName);
			sql.AppendFormat(", @ReferenceKey = '{0}'", pFieldName);
			sql.AppendFormat(", @ValueString = '{0}'", pValue);

			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(CabDriverRef), sql.ToString());
			oCabDriverRef = pPM.GetEntity<CabDriverRef>(qry);

			return oCabDriverRef;
		}

		public static CabDriverRef GetOrCreateRecord(PersistenceManager pPM, string pTableName, long pSourceID, string pFieldName) {
			pPM.DefaultQueryStrategy = QueryStrategy.DataSourceOnly;
			RdbQuery qry = new RdbQuery(typeof(CabDriverRef), SourceTableEntityColumn, EntityQueryOp.EQ, pTableName);
			qry.AddClause(SourceIDEntityColumn, EntityQueryOp.EQ, pSourceID);
			qry.AddClause(ReferenceKeyEntityColumn, EntityQueryOp.EQ, pFieldName);
			CabDriverRef rec = pPM.GetEntity<CabDriverRef>(qry);

			if (rec.IsNullEntity) {
				rec = Create(pPM, pTableName, pSourceID, pFieldName, "");
			}
			
			return rec;
		}



		public static CabDriverRef GetRecord(PersistenceManager pPM, string pTableName, long pSourceID, string pFieldName) {
			pPM.DefaultQueryStrategy = QueryStrategy.DataSourceOnly;
			RdbQuery qry = new RdbQuery(typeof(CabDriverRef), SourceTableEntityColumn, EntityQueryOp.EQ, pTableName);
			qry.AddClause(SourceIDEntityColumn, EntityQueryOp.EQ, pSourceID);
			qry.AddClause(ReferenceKeyEntityColumn, EntityQueryOp.EQ, pFieldName);
			return pPM.GetEntity<CabDriverRef>(qry);
		}

		public static EntityList<CabDriverRef> GetRecords(PersistenceManager pPM, string pTableName, long pSourceID) {
			pPM.DefaultQueryStrategy = QueryStrategy.DataSourceOnly;
			RdbQuery qry = new RdbQuery(typeof(CabDriverRef), SourceTableEntityColumn, EntityQueryOp.EQ, pTableName);
			qry.AddClause(SourceIDEntityColumn, EntityQueryOp.EQ, pSourceID);
			return pPM.GetEntities<CabDriverRef>(qry);
		}

		public static EntityList<CabDriverRef> GetRecords(PersistenceManager pPM, string pTableName, long pSourceID, string pFieldName) {
			pPM.DefaultQueryStrategy = QueryStrategy.DataSourceOnly;
			RdbQuery qry = new RdbQuery(typeof(CabDriverRef), SourceTableEntityColumn, EntityQueryOp.EQ, pTableName);
			qry.AddClause(SourceIDEntityColumn, EntityQueryOp.EQ, pSourceID);
			qry.AddClause(ReferenceKeyEntityColumn, EntityQueryOp.EQ, pFieldName);
			return pPM.GetEntities<CabDriverRef>(qry);
		}


		
	}

	#region EntityPropertyDescriptors.CabDriverRefPropertyDescriptor
	////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
	////    For example: 
	////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
	//public static partial class EntityPropertyDescriptors {
	//	public partial class CabDriverRefPropertyDescriptor : BaseEntityPropertyDescriptor {
	//    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
	//    public AdaptedPropertyDescriptor Age {
	//      get { return this.Get("Age"); }
	//    }
	//  }
	//} 
	#endregion

}