using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Runtime.Serialization;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

namespace CabRideEngine {
 
  #region SystemImportDataTable
  /// <summary>
  /// The generated <see cref="T:IdeaBlade.Persistence.EntityTable"/> class  
  /// containing all <see cref="SystemImport"/> objects.
  /// </summary>
  /// <remarks>
  /// ****************************************************
  /// <para>** DO NOT MODIFY THIS CLASS - AutoGenerated Code</para>
  /// <para>** Any changes made to this code will be lost</para>
  /// <para>** the next time this code is regenerated.</para>
  /// <para>**</para>
  /// <para>** Generated from:   EntityBaseclass.template</para>
  /// <para>** Last modified:    2011-06-29T13:00:28.3777579-07:00</para>
  /// <para>** Template version: 3.110127</para>
  /// ****************************************************
  /// </remarks>
  [BindingBrowsable(false)]
  [Serializable]
  public partial class SystemImportDataTable : BaseEntityDataTable, 
     IdeaBlade.Persistence.Rdb.IRdbTable {
  
    /// <summary>
    /// Initializes a new instance of the SystemImportDataTable class with no arguments.
    /// </summary>
    public SystemImportDataTable() {}

    /// <summary>
    /// Initializes a new instance of the SystemImportDataTable class 
    /// with the <see cref="System.Runtime.Serialization.SerializationInfo"/> 
    /// and the <see cref="System.Runtime.Serialization.StreamingContext"/>.
    /// </summary>
    /// <param name="pInfo">The data needed to serialize or deserialize an object.</param>
    /// <param name="pContext">The source and destination of a given serialized stream.</param>
    protected SystemImportDataTable(SerializationInfo pInfo, StreamingContext pContext) :
      base(pInfo, pContext) {}

    /// <summary>
    /// Creates a new instance of the <see cref="SystemImportDataRow"/>.
    /// Constructs a row from the builder.
    /// <para>** For internal use only. **</para>
    /// </summary>
    /// <param name="pRowBuilder"></param>
    /// <returns>The <see cref="SystemImportDataRow"/>.</returns>
    protected override DataRow NewRowFromBuilder(DataRowBuilder pRowBuilder) {
      return new SystemImport(pRowBuilder);
    }

    /// <summary>Returns the <see cref="System.Type"/> of <see cref="SystemImport"/>.</summary>
    protected override Type GetRowType() {
      return typeof(SystemImport);
    }
    
    /// <summary>
    /// Gets the name of the <see cref="T:IdeaBlade.Rdb.RdbKey"/> or 
    /// <see cref="T:IdeaBlade.Persistence.WS.WsKey"/> defined in <b>IdeaBlade.ibconfig</b>. 
    /// </summary>
    /// <remarks>For this <see cref="SystemImport"/>, the DataSourceKeyName="default".</remarks>
    public override String DataSourceKeyName {
      get { return @"default"; }
    }

    #region Column properties
    //**************************************
    //* Column properties
    //**************************************
    /// <summary>Gets the Url <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn UrlColumn {
      get {
        if (mUrlColumn!=null) return mUrlColumn;
        mUrlColumn = GetColumn("Url", true);
        return mUrlColumn;
      }
    }
    private DataColumn mUrlColumn;
    
    /// <summary>Gets the DateProcessed <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn DateProcessedColumn {
      get {
        if (mDateProcessedColumn!=null) return mDateProcessedColumn;
        mDateProcessedColumn = GetColumn("DateProcessed", true);
        return mDateProcessedColumn;
      }
    }
    private DataColumn mDateProcessedColumn;
    
    /// <summary>Gets the Issues <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn IssuesColumn {
      get {
        if (mIssuesColumn!=null) return mIssuesColumn;
        mIssuesColumn = GetColumn("Issues", true);
        return mIssuesColumn;
      }
    }
    private DataColumn mIssuesColumn;
    
    /// <summary>Gets the RecordsProcessed <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn RecordsProcessedColumn {
      get {
        if (mRecordsProcessedColumn!=null) return mRecordsProcessedColumn;
        mRecordsProcessedColumn = GetColumn("RecordsProcessed", true);
        return mRecordsProcessedColumn;
      }
    }
    private DataColumn mRecordsProcessedColumn;
    
    #endregion    

    /// <summary>Creates the schema mapping information for this SystemImportDataTable.</summary>
    /// <returns>
    /// The <see cref="T:IdeaBlade.Persistence.TableMappingInfo"/> for this SystemImportDataTable.
    /// </returns>
    protected override TableMappingInfo CreateTableMappingInfo() {
      RdbTableMappingInfo mappingInfo = new RdbTableMappingInfo(this);
      mappingInfo.SourceOwnerName = "dbo"; 
      mappingInfo.SourceTableName = "SystemImport"; 
      mappingInfo.ConcurrencyColumnName = ""; 
      // ColumnSelectionOption: selectAllColumns
      mappingInfo.SourceColumnNames = "*"; 
   
      DataColumnMappingCollection columnMappings = mappingInfo.TableMapping.ColumnMappings;
      columnMappings.Clear();
      columnMappings.Add("URL", "Url");
      columnMappings.Add("DateProcessed", "DateProcessed");
      columnMappings.Add("Issues", "Issues");
      columnMappings.Add("RecordsProcessed", "RecordsProcessed");
      return mappingInfo;
    }
  
    /// <summary>** For internal use only. **</summary>
    protected override void UpdateTableMappingInfo() {
      base.UpdateTableMappingInfo();
    }

    /// <summary>
    /// Initializes SystemImport <see cref="System.Data.DataColumn"/> properties.
    /// </summary>
    public override void InitColumnProperties() {
      base.InitColumnProperties();

      UrlColumn.Caption = "URL";
      DateProcessedColumn.Caption = "DateProcessed";
      IssuesColumn.Caption = "Issues";
      RecordsProcessedColumn.Caption = "RecordsProcessed";
    }
  }
  #endregion
  
  #region SystemImportDataRow
  /// <summary>
  /// The generated base class for <see cref="SystemImport"/>.
  /// </summary>
  /// <remarks>
  /// <para>
  /// It represents a row of persistable data in the <see cref="SystemImportDataTable"/>
  /// and derives from <see cref="T:IdeaBlade.Persistence.Entity"/>.
	/// </para>
  /// ****************************************************
  /// <para>** DO NOT MODIFY THIS CLASS - AutoGenerated Code</para>
  /// <para>** Any changes made to this code will be lost</para>
  /// <para>** the next time this code is regenerated.</para>
  /// <para>**</para>
  /// <para>** Generated from:   EntityBaseclass.template</para>
  /// <para>** Last modified:    2011-06-29T13:00:28.3777579-07:00</para>
  /// <para>** Template version: 3.110127</para>
  /// ****************************************************
  /// </remarks>  
  [Serializable]
  public abstract partial class SystemImportDataRow : BaseEntity, 
     IdeaBlade.Persistence.Rdb.IRdbEntity {
  
    /// <summary>
    /// Initializes a new instance of the SystemImportDataRow.
    /// Constructs a row from the builder.
    /// <para>** For internal use only. **</para>
    /// </summary>
    /// <param name="pRowBuilder"></param>
    protected SystemImportDataRow(DataRowBuilder pRowBuilder)
      : base(pRowBuilder) {
    }

    /// <summary>Gets the <see cref="SystemImportDataTable"/>.</summary>
    [BindingBrowsable(false)]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public new SystemImportDataTable TypedTable {
      get { return (SystemImportDataTable) base.Table; }
    }

    /// <summary>Returns the <see cref="System.Type"/> of <see cref="SystemImportDataTable"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public static Type GetTableType() {
      return typeof(SystemImportDataTable);
    }

    /// <summary>
    /// Gets the SQL 'where' clause used to restrict which 
    /// database records are accessible via this entity. 
    /// </summary>
    /// <remarks>
    /// The default property value is defined in the Current Class 
    /// Properties tab in the <b>IdeaBlade DevForce Object Mapping Tool</b>.
    /// <para>
    /// The 'where' clause will be used, in addition to any other 
    /// query-specific criteria, in every retrieval of entities of this type.
    /// </para>
    /// </remarks>
    [BindingBrowsable(false)]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public virtual string SqlWhereClause {
      get { return ""; }
    }
    
    /// <summary>
    /// Determines whether to use the 'distinct' keyword when 
    /// generating the SQL select clause for objects of this type.
    /// </summary>
    /// <remarks>Defaults to false, but may be overridden in subclasses.</remarks>
    [BindingBrowsable(false)]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public virtual bool ForceSqlDistinct {
      get { return false; }
    }
    

    #region Enums
    //**************************************
    //* Enums
    //**************************************
    #endregion
	
    #region Relation properties
    //**************************************
    //* Relation properties
    //**************************************
    #endregion
    
    #region EntityColumn definitions
    //**************************************
    //* Static EntityColumn definitions
    //**************************************
    /// <summary>The Url <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn UrlEntityColumn =
      new EntityColumn(typeof(SystemImport), "Url", typeof(System.String), false, true, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The DateProcessed <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn DateProcessedEntityColumn =
      new EntityColumn(typeof(SystemImport), "DateProcessed", typeof(System.DateTime), false, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The Issues <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn IssuesEntityColumn =
      new EntityColumn(typeof(SystemImport), "Issues", typeof(System.String), true, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The RecordsProcessed <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn RecordsProcessedEntityColumn =
      new EntityColumn(typeof(SystemImport), "RecordsProcessed", typeof(System.Int32), false, false, false, DataSourceAccessType.ReadWrite);      
    #endregion
    
    #region Properties
    //**************************************
    //* Url methods
    //**************************************
    /// <summary>Gets the Url <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn UrlColumn {
      get { return TypedTable.UrlColumn; }
    }

    /// <summary>Gets the Url.</summary>
    [MaxTextLength(250)]
    [DBDataType(typeof(System.String))]
    public virtual System.String Url {
      get { 
        System.String result_;
        if (GetInterceptor<System.String>("Url", GetUrlImpl, out result_)) return result_;
        return GetUrlImpl();
      }
    }
    private System.String GetUrlImpl() {
      return (System.String) GetColumnValue(UrlColumn, typeof(System.String), false); 
    }
    
    //**************************************
    //* DateProcessed methods
    //**************************************
    /// <summary>Gets the DateProcessed <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn DateProcessedColumn {
      get { return TypedTable.DateProcessedColumn; }
    }

    /// <summary>Gets or sets the DateProcessed.</summary>
    [DBDataType(typeof(System.DateTime))]
    public virtual System.DateTime DateProcessed {
      get { 
        System.DateTime result_;
        if (GetInterceptor<System.DateTime>("DateProcessed", GetDateProcessedImpl, out result_)) return result_;
        return GetDateProcessedImpl();
      }
      set { 
          if (!SetInterceptor<System.DateTime>("DateProcessed", value, SetDateProcessedImpl)) {
            SetDateProcessedImpl(value);
          }  
      }    
    }
    private System.DateTime GetDateProcessedImpl() {
      return (System.DateTime) GetColumnValue(DateProcessedColumn, typeof(System.DateTime), false); 
    }
    private void SetDateProcessedImpl(System.DateTime value) {
      SetColumnValue(DateProcessedColumn, value);
    }
    
    //**************************************
    //* Issues methods
    //**************************************
    /// <summary>Gets the Issues <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn IssuesColumn {
      get { return TypedTable.IssuesColumn; }
    }

    /// <summary>Gets or sets the Issues.</summary>
    [MaxTextLength(1073741823)]
    [DBDataType(typeof(System.String))]
    public virtual System.String Issues {
      get { 
        System.String result_;
        if (GetInterceptor<System.String>("Issues", GetIssuesImpl, out result_)) return result_;
        return GetIssuesImpl();
      }
      set { 
          if (!SetInterceptor<System.String>("Issues", value, SetIssuesImpl)) {
            SetIssuesImpl(value);
          }  
      }    
    }
    private System.String GetIssuesImpl() {
      return (System.String) GetColumnValue(IssuesColumn, typeof(System.String), true); 
    }
    private void SetIssuesImpl(System.String value) {
      SetColumnValue(IssuesColumn, value);
    }
    
    //**************************************
    //* RecordsProcessed methods
    //**************************************
    /// <summary>Gets the RecordsProcessed <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn RecordsProcessedColumn {
      get { return TypedTable.RecordsProcessedColumn; }
    }

    /// <summary>Gets or sets the RecordsProcessed.</summary>
    [DBDataType(typeof(System.Int32))]
    public virtual System.Int32 RecordsProcessed {
      get { 
        System.Int32 result_;
        if (GetInterceptor<System.Int32>("RecordsProcessed", GetRecordsProcessedImpl, out result_)) return result_;
        return GetRecordsProcessedImpl();
      }
      set { 
          if (!SetInterceptor<System.Int32>("RecordsProcessed", value, SetRecordsProcessedImpl)) {
            SetRecordsProcessedImpl(value);
          }  
      }    
    }
    private System.Int32 GetRecordsProcessedImpl() {
      return (System.Int32) GetColumnValue(RecordsProcessedColumn, typeof(System.Int32), false); 
    }
    private void SetRecordsProcessedImpl(System.Int32 value) {
      SetColumnValue(RecordsProcessedColumn, value);
    }
    
    #endregion

  }
  #endregion

  #region EntityPropertyDescriptors.SystemImportPropertyDescriptor
  public static partial class EntityPropertyDescriptors {

    /// <summary>
    /// Gets the SystemImportPropertyDescriptor for <see cref="SystemImport"/>.
    /// </summary>
    public static SystemImportPropertyDescriptor SystemImport {
      get { return SystemImportPropertyDescriptor.Instance; }
    }
    
    /// <summary>
    /// The generated class containing <see cref="SystemImport"/> PropertyDescriptors.
    /// </summary>
    public partial class SystemImportPropertyDescriptor : BaseEntityPropertyDescriptor {
	
			/// <summary>
      /// Initializes a new instance of the SystemImportPropertyDescriptor class. 
      /// </summary>
      /// <param name="pParentDescriptor">The parent <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/></param>
      /// <param name="pPropertyPath">The type of the property value</param>
			public SystemImportPropertyDescriptor(AdaptedPropertyDescriptor pParentDescriptor, String pPropertyPath)
        : base(pParentDescriptor, pPropertyPath) {}
		
      /// <summary>
      /// Initializes a new instance of the SystemImportPropertyDescriptor class. 
      /// </summary>
      /// <param name="pComponentType">The type of object to which the property is bound</param>
      protected SystemImportPropertyDescriptor(Type pComponentType)
        : base(pComponentType) {}

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Url.</summary>
      public AdaptedPropertyDescriptor Url {
        get { return Get("Url"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for DateProcessed.</summary>
      public AdaptedPropertyDescriptor DateProcessed {
        get { return Get("DateProcessed"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Issues.</summary>
      public AdaptedPropertyDescriptor Issues {
        get { return Get("Issues"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for RecordsProcessed.</summary>
      public AdaptedPropertyDescriptor RecordsProcessed {
        get { return Get("RecordsProcessed"); }
      }

     internal new static SystemImportPropertyDescriptor Instance = new SystemImportPropertyDescriptor(typeof(SystemImport));
    }
  }
  #endregion
  
}
