﻿using Dapper;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace CabRideEngineDapper {
    public partial class CardTypes {

        public static List<CardTypes> GetAllCardTypes(SqlConnection pConn) {
            string sql = @"SELECT * FROM CardTypes
                            ORDER BY CardType";
            return pConn.Query<CardTypes>(sql).ToList();
        }

    }
}
