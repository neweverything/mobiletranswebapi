﻿using Dapper;
using System.Data.SqlClient;
using System.Linq;

namespace CabRideEngineDapper {
    public partial class CustomFeeType {

        public static CustomFeeType GetOrCreate(SqlConnection pConn, long pAffiliateID) {
            string sql = @"SELECT * FROM dbo.CustomFeeType
                            WHERE AffiliateID = @AffiliateID";

            CustomFeeType rec = pConn.Get<CustomFeeType>(sql, new { AffiliateID = pAffiliateID });
            if (rec.IsNullEntity) {
                rec = CustomFeeType.Create();
                rec.AffiliateID = pAffiliateID;
                rec.Save();
            }

            return rec;
        }
    }
}
