﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CabRideEngine.Properties {
    
    
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "16.7.0.0")]
    internal sealed partial class Settings : global::System.Configuration.ApplicationSettingsBase {
        
        private static Settings defaultInstance = ((Settings)(global::System.Configuration.ApplicationSettingsBase.Synchronized(new Settings())));
        
        public static Settings Default {
            get {
                return defaultInstance;
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://findv3.staging.mappoint.net/Find-30/Common.asmx")]
        public string CabRideEngine_MapPointWebService_CommonService {
            get {
                return ((string)(this["CabRideEngine_MapPointWebService_CommonService"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://www.travelnow.com/services/CarServices200436")]
        public string CabRideEngine_CarRentalWebService_CarFunctions {
            get {
                return ((string)(this["CabRideEngine_CarRentalWebService_CarFunctions"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://www.cabpay.com/GeoCodedAddressesWebService/GeoCodeAddressService.asmx")]
        public string CabRideEngine_com_TaxiPass_GeoCodeWS_GeoCodeService {
            get {
                return ((string)(this["CabRideEngine_com_TaxiPass_GeoCodeWS_GeoCodeService"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://services-staging.cmtnyc.com/operator/reportingservices.asmx")]
        public string CabRideEngine_com_cmt_reporting_CMT_x0020_Reporting_x0020_Web_x0020_Service {
            get {
                return ((string)(this["CabRideEngine_com_cmt_reporting_CMT_x0020_Reporting_x0020_Web_x0020_Service"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("https://www.taxitronic.org/TAXITRONIC_SERVER/taxi_service.asmx")]
        public string CabRideEngine_org_taxitronic_taxiService_Taxi_Service {
            get {
                return ((string)(this["CabRideEngine_org_taxitronic_taxiService_Taxi_Service"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("https://www.taxitronic.org/VTS_SERVICE/Taxi_Service.asmx")]
        public string CabRideEngine_org_taxitronic_vtsService_Service {
            get {
                return ((string)(this["CabRideEngine_org_taxitronic_vtsService_Service"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("304985941671A6204F7A59730F")]
        public string TaxiPassInternalWebServiceSecurityKey {
            get {
                return ((string)(this["TaxiPassInternalWebServiceSecurityKey"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("True")]
        public bool EMailViaWebService {
            get {
                return ((bool)(this["EMailViaWebService"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("https://cabpay.com/CellPhoneWebServices/Services.asmx")]
        public string CabRideEngine_com_cabpay_CellPhoneWebService_Services {
            get {
                return ((string)(this["CabRideEngine_com_cabpay_CellPhoneWebService_Services"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("email@taxipass.com")]
        public string EMailFrom {
            get {
                return ((string)(this["EMailFrom"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("hgobran@newtekone.com")]
        public string EMailErrorsTo {
            get {
                return ((string)(this["EMailErrorsTo"]));
            }
        }
    }
}
