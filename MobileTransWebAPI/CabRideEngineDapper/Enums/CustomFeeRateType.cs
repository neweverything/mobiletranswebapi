﻿
namespace CabRideEngineDapper.Enums {
    public enum CustomFeeRateType {
        Flat,
        PerMile,
        PerMinute
    }
}
