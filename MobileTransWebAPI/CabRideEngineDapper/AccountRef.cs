﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace CabRideEngineDapper {
	public partial class AccountRef {

		#region Save
		public new void Save(SqlConnection pConn, string pModifiedBy) {
			if (IsNewRecord) {
				CreatedBy = pModifiedBy;
				CreatedDate = DateTime.UtcNow;
				ModifiedDate = CreatedDate;
				ModifiedBy = CreatedBy;

				int? id = pConn.Insert(this);
				this.AccountRefID = id.Value;
			} else {
				ModifiedBy = pModifiedBy;
				ModifiedDate = DateTime.UtcNow;
				pConn.Update(this);
			}
		}
		#endregion

		public static AccountRef GetByGroupReference(SqlConnection pConn, long pAccountID, string pGroup, string pReference) {
			var builder = new SqlBuilder();
			//var count = builder.AddTemplate("select count() from table WITH (NoLock) /where/");

			var selector = builder.AddTemplate("select * from AccountRef /**where**/ /**orderby**/");
			builder.Where("ReferenceGroup = @ReferenceGroup", new { ReferenceGroup = pGroup });
			builder.Where("ReferenceKey = @ReferenceKey", new { ReferenceKey = pReference });
			builder.Where("AccountID = @AccountID", new { AccountID = pAccountID });
			return pConn.Query<AccountRef>(selector.RawSql, selector.Parameters).DefaultIfEmpty(AccountRef.GetNullEntity()).FirstOrDefault();


			//string sql = string.Format("Select * FROM {0} WITH (NoLock) WHERE {1} = @{1} AND {2} = @{2} AND {3} = @{3}", AccountRefTableDef.TABLE_NAME, 
			//	AccountRefTableDef.ReferenceGroup, AccountRefTableDef.ReferenceKey, AccountRefTableDef.AccountID);
			//DynamicParameters param = new DynamicParameters();
			//param.Add(AccountRefTableDef.ReferenceGroup, pGroup);
			//param.Add(AccountRefTableDef.ReferenceKey, pReference);
			//param.Add(AccountRefTableDef.AccountID, pAccountID);
			//return pConn.Query<AccountRef>(sql, param).FirstOrDefault();
		}


		public static List<AccountRef> GetByGroup(SqlConnection pConn, string pAccountNo, string pGroup) {
			string sql = @"SELECT  ReferenceKey
                                  , ValueString
                            FROM    AccountRef
                            WHERE   ReferenceGroup = @ReferenceGroup
                                    AND AccountID IN (SELECT    AccountID
                                                      FROM      dbo.Accounts
                                                      WHERE     AccountNo = @AccountNo)";
			return pConn.Query<AccountRef>(sql, new { ReferenceGroup = pGroup, AccountNo = pAccountNo }).ToList();

		}

		public static AccountRef GetCreate(SqlConnection pConn, long pAccountID, string pGroup, string pKey, string pDefaultValue) {

			if (pAccountID == 0) {
				return AccountRef.GetNullEntity();
			}

			AccountRef rec = GetByGroupReference(pConn, pAccountID, pGroup, pKey);
			if (rec == null || rec.IsNullEntity) {
				try {
					rec = Create();
					rec.AccountID = pAccountID;
					rec.ReferenceGroup = pGroup;
					rec.ReferenceKey = pKey;
					rec.ValueString = pDefaultValue;
					// done this way, as edit was not working correctly
					rec.Save(pConn, CabRideDapperSettings.LoggedInUser);
				} catch (Exception ex) {
					throw ex;
				}
			}
			return rec;
		}

	}
}
