﻿using System;
using System.Collections.Generic;

namespace MobileTransWebAPI.Models {
	public class MultiPayeeTransHistoryRequest {

		public long AffiliateID { get; set; }
		public long PayeeID { get; set; }
		public string AffiliateCode { get; set; }

		public bool IncludeTestTrans { get; set; }
		public bool IncludeFailedTrans { get; set; }

		public string RequestTime { get; set; }

		public DateTime StartDate { get; set; }
		public DateTime EndDate { get; set; }

		public string VoucherNo { get; set; }

		public List<CustomFieldFilter> CustomFieldFilters { get; set; }

		public class CustomFieldFilter {
			public string FieldName { get; set; }
			public string Value { get; set; }
		}
	}
}