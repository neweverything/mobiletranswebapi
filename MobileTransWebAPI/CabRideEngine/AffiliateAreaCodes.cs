﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

using CabRideEngine.Utils;
using TaxiPassCommon;

using Csla.Validation;

namespace CabRideEngine {
    /// <summary>
    /// The developer class representing the AffiliateAreaCodes business object class.
    /// </summary>
    /// <remarks>
    /// <para>Place all application-specific business logic in this class.</para>
    /// <para>
    /// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
    /// and will not overwrite changes.
    /// </para>
    /// <para>
    /// This class inherits from the generated class, its companion in the pair of classes 
    /// defining a business object. Properties in the generated class may be overridden in 
    /// order to change the behavior or add business logic.
    /// </para>
    /// <para>
    /// This class is the final class in the inheritance chain for this business object. 
    /// It should not be inherited.
    /// </para>
    /// </remarks>
    [Serializable]
    public sealed partial class AffiliateAreaCodes : AffiliateAreaCodesDataRow {

        #region Constructors -- DO NOT MODIFY

        // Do not create constructors for this class
        // Developers cannot create instances of this class with the "new" keyword
        // You should write a static Create method instead; see the "Suggested Customization" region
        // Please review the documentation to learn about entity creation and inheritance

        // This private constructor prevents accidental instance creation via 'new';
        // it is also required if the class is visible from a web service.
        // DO NOT REMOVE **
        private AffiliateAreaCodes() : this(null) { }

        /// <summary>
        /// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
        /// </summary>
        /// <param name="pRowBuilder"></param>
        public AffiliateAreaCodes(DataRowBuilder pRowBuilder)
            : base(pRowBuilder) {
        }

        #endregion

        #region Suggested Customizations

        //    // Use this factory method template to create new instances of this class
        //    public static AffiliateAreaCodes Create(PersistenceManager pManager,
        //      ... your creation parameters here ...) { 
        //
        //      AffiliateAreaCodes aAffiliateAreaCodes = pManager.CreateEntity<AffiliateAreaCodes>();
        //
        //      // if this object type requires a unique id and you have implemented
        //      // the IIdGenerator interface implement the following line
        //      pManager.GenerateId(aAffiliateAreaCodes, // add id column here //);
        //
        //      // Add custom code here
        //
        //      aAffiliateAreaCodes.AddToManager();
        //      return aAffiliateAreaCodes;
        //    }

        //    // Implement this method if you want your object to sort in a specific order
        //    public override int CompareTo(Object pObject) {
        //    }

        //    // Implement this method to customize the null object version of this class
        //    protected override void UpdateNullEntity() {
        //    }

        #endregion

        // Add additional logic to your business object here...
        #region Standard Entity logic to create/audit/validate the entity
        public static AffiliateAreaCodes Create(PersistenceManager pPM) {
            AffiliateAreaCodes rec = null;

            try {
                rec = pPM.CreateEntity<AffiliateAreaCodes>();

                // Using the IdeaBlade Id Generation technique
                pPM.GenerateId(rec, AffiliateAreaCodes.AffiliateAreaCodeIDEntityColumn);
                rec.AddToManager();

            } catch (Exception ex) {
                throw ex;
            }
            return rec;
        }

        protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
            if (IsDeserializing) {
                return;
            }
            base.OnColumnChanging(pArgs);
            DoAudit(pArgs);
        }

        private void DoAudit(DataColumnChangeEventArgs pArgs) {
            if (!this.PersistenceManager.IsClient) {
                return;
            }
            if (this.RowState == DataRowState.Added) {
                return;
            }
            if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
                AuditRecord = AffiliateAreaCodesAudit.Create(this.PersistenceManager, this);
            }
        }

        [BindingBrowsable(false)]
        public override string EntityInstanceDisplayName {
            get {
                return this.AreaCode;
            }
        }

        protected override void AddRules(RuleList pList) {
            PropertyRequiredRule.AddToList(pList, AreaCodeColumn.ColumnName);
            //pList.Add(IsAreaCodeUnique, AreaCodeColumn.ColumnName);
        }

        private bool IsAreaCodeUnique(object pTarget, RuleArgs e) {
            AffiliateAreaCodes rec = pTarget as AffiliateAreaCodes;
            if (string.IsNullOrEmpty(rec.AreaCode)) {
                return false;
            }
            RdbQuery qry = new RdbQuery(typeof(AffiliateAreaCodes), AreaCodeEntityColumn, EntityQueryOp.EQ, rec.AreaCode);
            qry.AddClause(AffiliateAreaCodeIDEntityColumn, EntityQueryOp.NE, rec.AffiliateAreaCodeID);
            EntityList<AffiliateAreaCodes> oList = this.PersistenceManager.GetEntities<AffiliateAreaCodes>(qry);
            if (oList.Count > 0) {
                e.Description = "Area Code already exists for affiliate: " + oList[0].Affiliate.Name;
                return false;
            }
            return true;
        }

        #endregion

        #region Retrieve Records

        public static EntityList<AffiliateAreaCodes> GetAll(PersistenceManager pPM) {
            EntityList<AffiliateAreaCodes> oList = pPM.GetEntities<AffiliateAreaCodes>();
            oList.ApplySort(AffiliateAreaCodes.AreaCodeEntityColumn.ColumnName, System.ComponentModel.ListSortDirection.Ascending, false);
            return oList;
        }

        public static AffiliateAreaCodes GetAreaCode(PersistenceManager pPM, string pAreaCode) {
            RdbQuery qry = new RdbQuery(typeof(AffiliateAreaCodes), AreaCodeEntityColumn, EntityQueryOp.EQ, pAreaCode);
            return pPM.GetEntity<AffiliateAreaCodes>(qry, QueryStrategy.Normal);
        }

        public static EntityList<AffiliateAreaCodes> GetAreaCodes(PersistenceManager pPM, string pAreaCode) {
            RdbQuery qry = new RdbQuery(typeof(AffiliateAreaCodes), AreaCodeEntityColumn, EntityQueryOp.EQ, pAreaCode);
            return pPM.GetEntities<AffiliateAreaCodes>(qry, QueryStrategy.Normal);
        }

        #endregion

    }

    #region EntityPropertyDescriptors.AffiliateAreaCodesPropertyDescriptor
    ////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
    ////    For example: 
    ////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
    //public static partial class EntityPropertyDescriptors {
    //	public partial class AffiliateAreaCodesPropertyDescriptor : BaseEntityPropertyDescriptor {
    //    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
    //    public AdaptedPropertyDescriptor Age {
    //      get { return this.Get("Age"); }
    //    }
    //  }
    //} 
    #endregion

}