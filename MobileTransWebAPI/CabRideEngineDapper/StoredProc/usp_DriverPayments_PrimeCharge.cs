﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using TaxiPassCommon;

namespace CabRideEngineDapper.StoredProc {

    public class usp_DriverPayments_PrimeCharge {

        public string RetMsg { get; set; }
        public long DriverPaymentID { get; set; }
        public DateTime ChargeDate { get; set; }
        public string CardNumberDisplay { get; set; }
        public string TransNo { get; set; }
        public long AffiliateID { get; set; }
        public string TimeZone { get; set; }
        public string ServiceType { get; set; }
        public string DefaultPlatform { get; set; }
        public long DriverID { get; set; }
        public string DriverCell { get; set; }
        public long VehicleID { get; set; }
        public long? AffiliateDriverID { get; set; }
        public decimal ChargeMaxAmount { get; set; }
        public decimal ChargeMaxSwipeAmount { get; set; }
        public decimal DriverPaysSwipeFee { get; set; }
        public decimal DriverPaysSwipeFeePerAmount { get; set; }
        public string TreatAsTestCards { get; set; }
        public bool IsFrozen { get; set; }
        public bool FraudSuspected { get; set; }

        public static List<usp_DriverPayments_PrimeCharge> Execute(SqlConnection pConn, long pDriverID, long pAffDriverID, string pVoucherNo, string pVehicleNo) {
            DynamicParameters parms = new DynamicParameters();
            parms.Add("DriverID", pDriverID);
            parms.Add("AffiliateDriverID", pAffDriverID);
            if (!pVoucherNo.IsNullOrEmpty()) {
                parms.Add("TransNo", pVoucherNo);
            }
            if (!pVehicleNo.IsNullOrEmpty()) {
                parms.Add("VehicleNo", pVehicleNo);
            }
            return pConn.Query<usp_DriverPayments_PrimeCharge>("usp_DriverPayments_PrimeCharge", parms, commandType: CommandType.StoredProcedure).ToList();
        }

    }
}
