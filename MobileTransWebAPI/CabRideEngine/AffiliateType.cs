﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Util;
using System.Text;

using TaxiPassCommon;
using IdeaBlade.Persistence.Rdb;

namespace CabRideEngine {
    [Serializable]
    public sealed class AffiliateType:AffiliateTypeDataRow {

        #region Constructors -- DO NOT MODIFY
        // Do not create constructors for this class
        // Developers cannot create instances of this class with the "new" keyword
        // You should write a static Create method instead; see the "Suggested Customization" region
        // Please review the documentation to learn about entity creation and inheritance

        // This private constructor prevents accidental instance creation via 'new'
        private AffiliateType() : this(null) {
        }

        // Typed DataSet constructor (needed by framework) - DO NOT REMOVE
        public AffiliateType(DataRowBuilder pRowBuilder)
            : base(pRowBuilder) {
        }

        #endregion

        #region Suggested Customizations

        //    // Use this factory method template to create new instances of this class
        //    public static AffiliateType Create(PersistenceManager pManager,
        //      ... your creation parameters here ...) { 
        //
        //      AffiliateType aAffiliateType = (AffiliateType) pManager.CreateEntity(typeof(AffiliateType));
        //
        //      // if this object type requires a unique id and you have implemented
        //      // the IIdGenerator interface implement the following line
        //      pManager.GenerateId(aAffiliateType, // add id column here //);
        //
        //      // Add custom code here
        //
        //      aAffiliateType.AddToManager();
        //      return aAffiliateType;
        //    }

        //    // Implement this method if you want your object to sort in a specific order
        //    public override int CompareTo(Object pObject) {
        //    }

        //    // Implement this method to customize the null object version of this class
        //    protected override void UpdateNullEntity() {
        //    }

        #endregion

        // Add additional logic to your business object here...
        public static AffiliateType Create(PersistenceManager pPM) {
            AffiliateType oType = null;

            try {
                // Creates the State but it is not yet accessible to the application
                oType = (AffiliateType)pPM.CreateEntity(typeof(AffiliateType));

                // Using the IdeaBlade Id Generation technique
                //pPM.GenerateId(emp, Employee.IdEntityColumn);

                // CRITICAL: must tell emp to add itself to its PM
                oType.AddToManager();


            } catch (Exception ex) {
                throw ex;
            }
            return oType;
        }

		protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
			if (IsDeserializing)
				return;
			base.OnColumnChanging(pArgs);
			//DoAudit(pArgs);
		}

		private void DoAudit(DataColumnChangeEventArgs pArgs) {
			if (!this.PersistenceManager.IsClient) {
				return;
			}
            if (this.RowState == DataRowState.Added) {
                return;
            }
			if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
				AuditRecord = AffiliateTypeAudit.Create(this.PersistenceManager, this);
			}
			//AuditRecord[pArgs.Column.ColumnName] = pArgs.ProposedValue;
		}


		// ******************************************************************************
		// Return all AffiliateTypes
		// ******************************************************************************
		public static EntityList<AffiliateType> GetAffiliateTypes(PersistenceManager pPM) {
			return GetData(pPM);
		}

		// ******************************************************************************
		// Call the proc
		// ******************************************************************************
		private static EntityList<AffiliateType> GetData(PersistenceManager pPM) {
			StringBuilder sql = new StringBuilder();
			sql.Append("usp_AffiliateTypeGet");


			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(AffiliateType), sql.ToString());
			EntityList<AffiliateType> oAffiliateTypes = pPM.GetEntities<AffiliateType>(qry);
			if (oAffiliateTypes.Count == 0) {
				oAffiliateTypes.Add(new AffiliateType());
			}
			return oAffiliateTypes;
		}

    }

}
