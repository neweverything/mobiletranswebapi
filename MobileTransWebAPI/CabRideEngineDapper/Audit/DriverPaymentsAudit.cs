﻿using CabRideEngineDapper;
using CabRideEngineDapper.Utils;
using Dapper;

namespace CabRideEngineDapperAudit {
    public partial class DriverPaymentsAudit {

        public static DriverPaymentsAudit GetPayment(DriverPayments pDriverPayment) {
            using (var conn = SqlHelper.OpenSqlAuditConnection()) {
                string sql = @"SELECT TOP 1 *
                                FROM    DriverPaymentsAudit
                                WHERE   DriverPaymentID = @DriverPaymentID
                                        AND RowVersion <= @RowVersion
                                ORDER BY RowVersion DESC";
                return conn.Get<DriverPaymentsAudit>(sql, new { DriverPaymentID = pDriverPayment.DriverPaymentID, RowVersion = pDriverPayment.RowVersion });
            }
        }
    }
}
