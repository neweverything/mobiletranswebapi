﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;
using Csla.Validation;

namespace CabRideEngine {
	/// <summary>
	/// The developer class representing the CashAccountType business object class.
	/// </summary>
	/// <remarks>
	/// <para>Place all application-specific business logic in this class.</para>
	/// <para>
	/// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
	/// and will not overwrite changes.
	/// </para>
	/// <para>
	/// This class inherits from the generated class, its companion in the pair of classes 
	/// defining a business object. Properties in the generated class may be overridden in 
	/// order to change the behavior or add business logic.
	/// </para>
	/// <para>
	/// This class is the final class in the inheritance chain for this business object. 
	/// It should not be inherited.
	/// </para>
	/// </remarks>
	[Serializable]
	public sealed partial class CashAccountType : CashAccountTypeDataRow {

		#region Constructors -- DO NOT MODIFY

		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new';
		// it is also required if the class is visible from a web service.
		// DO NOT REMOVE **
		private CashAccountType() : this(null) { }

		/// <summary>
		/// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
		/// </summary>
		/// <param name="pRowBuilder"></param>
		public CashAccountType(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static CashAccountType Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      CashAccountType aCashAccountType = pManager.CreateEntity<CashAccountType>();
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aCashAccountType, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aCashAccountType.AddToManager();
		//      return aCashAccountType;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		// Add additional logic to your business object here...
		public static CashAccountType Create(PersistenceManager pPM) {
			CashAccountType rec = null;
			try {
				rec = pPM.CreateEntity<CashAccountType>();
				rec.AddToManager();

			} catch (Exception ex) {
				throw ex;
			}
			return rec;
		}

		protected override void AddRules(RuleList pList) {
			PropertyRequiredRule.AddToList(pList, TypeEntityColumn.ColumnName);
			pList.Add(IsTypeUnique, TypeEntityColumn.ColumnName);
		}

		private bool IsTypeUnique(object pTarget, RuleArgs e) {
			CashAccountType oType = (CashAccountType)pTarget;

			RdbQuery qry = new RdbQuery(typeof(CashAccountType), CashAccountType.TypeEntityColumn, EntityQueryOp.EQ, oType.Type);
			qry.AddClause(CashAccountType.CashAccountTypeIDEntityColumn, EntityQueryOp.NE, oType.CashAccountTypeID);
			EntityList<CashAccountType> typeList = this.PersistenceManager.GetEntities<CashAccountType>(qry);
			if (typeList.Count > 0) {
				e.Description = "Account Type must be unique!";
				return false;
			}
			return true;
		}

		[BindingBrowsable(false)]
		public override string EntityInstanceDisplayName {
			get {
				return Type;
			}
		}

		protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
			if (IsDeserializing)
				return;
			base.OnColumnChanging(pArgs);
			//DoAudit(pArgs);
		}

		private void DoAudit(DataColumnChangeEventArgs pArgs) {
			if (!this.PersistenceManager.IsClient) {
				return;
			}
			if (this.RowState == DataRowState.Added) {
				return;
			}
			if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
				AuditRecord = CashAccountTypeAudit.Create(this);
			}
		}

		public static EntityList<CashAccountType> GetAllTypes(PersistenceManager pPM) {
			EntityList<CashAccountType> typeList = pPM.GetEntities<CashAccountType>();
			typeList.ApplySort(CashAccountType.TypeEntityColumn.ColumnName, System.ComponentModel.ListSortDirection.Ascending, false);
			return typeList;
		}

		public static CashAccountType GetByID(PersistenceManager pPM, long pCashAccountTypeID) {
			return pPM.GetEntity<CashAccountType>(new PrimaryKey(typeof(CashAccountType), pCashAccountTypeID));
		}

		public static EntityList<CashAccountType> GetAllWithTrackBalance(PersistenceManager pPM) {
			RdbQuery qry = new RdbQuery(typeof(CashAccountType), TrackBalanceEntityColumn, EntityQueryOp.EQ, true);
			qry.AddOrderBy(CashAccountType.TypeEntityColumn, System.ComponentModel.ListSortDirection.Ascending);
			return pPM.GetEntities<CashAccountType>(qry);
		}
	}

	#region EntityPropertyDescriptors.CashAccountTypePropertyDescriptor
	////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
	////    For example: 
	////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
	//public static partial class EntityPropertyDescriptors {
	//	public partial class CashAccountTypePropertyDescriptor : BaseEntityPropertyDescriptor {
	//    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
	//    public AdaptedPropertyDescriptor Age {
	//      get { return this.Get("Age"); }
	//    }
	//  }
	//} 
	#endregion

}