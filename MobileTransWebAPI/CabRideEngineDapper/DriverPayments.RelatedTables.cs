﻿using CabRideEngineDapper.Utils;
using Dapper;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace CabRideEngineDapper {
    public partial class DriverPayments {

        private List<DriverPaymentResults> mDriverPaymentResultses;
        [JsonIgnore]
        public List<DriverPaymentResults> DriverPaymentResultses {
            get {
                if (mDriverPaymentResultses == null || mDriverPaymentResultses.Count == 0) {
                    using (var conn = SqlHelper.OpenSqlConnection()) {
                        mDriverPaymentResultses = DriverPaymentResults.GetResults(conn, this.DriverPaymentID);
                    }
                }
                return mDriverPaymentResultses;
            }
            set {
                mDriverPaymentResultses = value;
            }
        }

        private DriverPaymentLossGain mDriverPaymentLossGain;
        [JsonIgnore]
        public DriverPaymentLossGain MyDriverPaymentLossGain {
            get {
                if (mDriverPaymentLossGain == null || mDriverPaymentLossGain.IsNullEntity) {
                    using (var conn = SqlHelper.OpenSqlConnection()) {
                        mDriverPaymentLossGain = DriverPaymentLossGain.GetLossGain(conn, this.DriverPaymentID);
                    }
                }
                return mDriverPaymentLossGain;
            }
        }

        private DriverPaymentRedeemerNotes mDriverPaymentRedeemerNotes;
        [JsonIgnore]
        public DriverPaymentRedeemerNotes MyDriverPaymentRedeemerNotes {
            get {
                if (mDriverPaymentRedeemerNotes == null || mDriverPaymentRedeemerNotes.IsNullEntity) {
                    using (var conn = SqlHelper.OpenSqlConnection()) {
                        mDriverPaymentRedeemerNotes = DriverPaymentRedeemerNotes.GetNote(conn, this.DriverPaymentID);
                    }
                }
                return mDriverPaymentRedeemerNotes;
            }
            set {
                mDriverPaymentRedeemerNotes = value;
            }
        }

        private RedeemerShiftVouchers mRedeemerShiftVouchers;
        [JsonIgnore]
        public RedeemerShiftVouchers MyRedeemerShiftVouchers {
            get {
                if (mRedeemerShiftVouchers == null || mRedeemerShiftVouchers.IsNullEntity) {
                    using (var conn = SqlHelper.OpenSqlConnection()) {
                        mRedeemerShiftVouchers = RedeemerShiftVouchers.GetShiftVoucher(conn, this.DriverPaymentID);
                    }
                }
                return mRedeemerShiftVouchers;
            }
        }

        private ReceiptRequest mReceiptRequest;
        [JsonIgnore]
        public ReceiptRequest ReceiptRequest {
            get {
                if (mReceiptRequest == null) {
                    mReceiptRequest = ReceiptRequest.GetRecord(this.DriverPaymentID);
                }
                return mReceiptRequest;
            }
        }


        private DriverPaymentVoucherImages mDriverPaymentVoucherImages;
        [JsonIgnore]
        public DriverPaymentVoucherImages DriverPaymentVoucherImages {
            get {
                if (mDriverPaymentVoucherImages == null) {
                    mDriverPaymentVoucherImages = DriverPaymentVoucherImages.GetRecord(this.DriverPaymentID);
                }
                return mDriverPaymentVoucherImages;
            }
        }

        private DriverPaymentKioskAmount mDriverPaymentKioskAmount;
        [JsonIgnore]
        public DriverPaymentKioskAmount DriverPaymentKioskAmount {
            get {
                if (mDriverPaymentKioskAmount == null) {
                    mDriverPaymentKioskAmount = DriverPaymentKioskAmount.GetKioskAmount(this.DriverPaymentID);
                }
                return mDriverPaymentKioskAmount;
            }
        }

        private List<FraudQueue> mFraudQueues;
        [JsonIgnore]
        public List<FraudQueue> FraudQueues {
            get {
                if (mFraudQueues == null) {
                    using (var conn = SqlHelper.OpenSqlConnection()) {
                        mFraudQueues = conn.GetList<FraudQueue>(new { DriverPaymentID = this.DriverPaymentID }).ToList();
                    }
                }
                return mFraudQueues;
            }
        }

        private Drivers mDriver;
        [JsonIgnore]
        public Drivers Drivers {
            get {
                if (mDriver == null) {
                    mDriver = Drivers.GetDriver(this.DriverID);
                }
                return mDriver;
            }
        }

        private AffiliateDrivers mAffiliateDriver;
        [JsonIgnore]
        public AffiliateDrivers AffiliateDrivers {
            get {
                if (!AffiliateDriverID.HasValue) {
                    return AffiliateDrivers.GetNullEntity();
                }
                if (mAffiliateDriver == null) {
                    mAffiliateDriver = AffiliateDrivers.GetDriver(this.AffiliateDriverID.Value);
                }
                return mAffiliateDriver;
            }
        }

        private Vehicles mVehicles;
        [JsonIgnore]
        public Vehicles Vehicles {
            get {
                if (mVehicles == null) {
                    using (var conn = SqlHelper.OpenSqlConnection()) {
                        mVehicles = Vehicles.GetVehicle(conn, this.VehicleID);
                    }
                }
                return mVehicles;
            }
        }

        private TaxiPassRedeemers mTaxiPassRedeemers;
        [JsonIgnore]
        public TaxiPassRedeemers TaxiPassRedeemers {
            get {
                if (mTaxiPassRedeemers == null) {
                    mTaxiPassRedeemers = TaxiPassRedeemers.GetRedeemer(this.TaxiPassRedeemerID);
                }
                return mTaxiPassRedeemers;
            }
        }

        private DriverPaymentsPendingMatches mDriverPaymentsPendingMatches;
        [JsonIgnore]
        public DriverPaymentsPendingMatches DriverPaymentsPendingMatches {
            get {
                if (mDriverPaymentsPendingMatches == null) {
                    mDriverPaymentsPendingMatches = CabRideEngineDapper.DriverPaymentsPendingMatches.GetPendingMatch(this.DriverPaymentID);
                }
                return mDriverPaymentsPendingMatches;
            }
        }

        private ACHDetail mACHDetail;
        [JsonIgnore]
        public ACHDetail ACHDetail {
            get {
                if (mACHDetail == null) {
                    mACHDetail = ACHDetail.GetByDriverPaymentID(this.DriverPaymentID);
                }
                return mACHDetail;
            }
            set {
                mACHDetail = value;
            }
        }

        private TaxiPassRedeemerLocations mTaxiPassRedeemerLocations;
        [JsonIgnore]
        public TaxiPassRedeemerLocations TaxiPassRedeemerLocations {
            get {
                if (!TaxiPassRedeemerLocationID.HasValue) {
                    return CabRideEngineDapper.TaxiPassRedeemerLocations.GetNullEntity();
                }
                if (mTaxiPassRedeemerLocations == null) {
                    mTaxiPassRedeemerLocations = TaxiPassRedeemerLocations.GetLocation(this.TaxiPassRedeemerLocationID.Value);
                }
                return mTaxiPassRedeemerLocations;
            }
        }

        private AffiliateDriverReferrals mAffiliateDriverReferrals;
        [JsonIgnore]
        public AffiliateDriverReferrals AffiliateDriverReferrals {
            get {
                if (!AffiliateDriverID.HasValue) {
                    return AffiliateDriverReferrals.GetNullEntity();
                }
                if (mAffiliateDriverReferrals == null) {
                    mAffiliateDriverReferrals = AffiliateDriverReferrals.GetReferredDriver(this.AffiliateDriverID.Value);
                }
                return mAffiliateDriverReferrals;
            }
        }

        private Affiliate mAffiliate;
        [JsonIgnore]
        public Affiliate Affiliate {
            get {
                if (!AffiliateID.HasValue) {
                    AffiliateID = Drivers.AffiliateID;
                }
                if (mAffiliate == null) {
                    mAffiliate = Affiliate.GetAffiliate(this.AffiliateID.Value);
                }
                return mAffiliate;
            }
        }

        private DriverPaymentNotes mDriverPaymentNotes;
        [JsonIgnore]
        public DriverPaymentNotes DriverPaymentNotes {
            get {
                if (mDriverPaymentNotes == null) {
                    mDriverPaymentNotes = DriverPaymentNotes.GetNote(this.DriverPaymentID);
                }
                return mDriverPaymentNotes;
            }
            set {
                mDriverPaymentNotes = value;
            }
        }

        private DriverPaymentResults mDriverPaymentAuthResults;
        [JsonIgnore]
        public DriverPaymentResults DriverPaymentAuthResults {
            get {
                if (mDriverPaymentAuthResults == null) {
                    mDriverPaymentAuthResults = DriverPaymentResults.GetAuthApprovedResult(this);
                    if (mDriverPaymentResultses == null) {
                        mDriverPaymentResultses = new List<DriverPaymentResults>();
                    }
                    mDriverPaymentResultses.Add(mDriverPaymentAuthResults);
                }
                return mDriverPaymentAuthResults;
            }
        }

        private DriverPaymentResults mDriverPaymentChargeResults;
        [JsonIgnore]
        public DriverPaymentResults DriverPaymentChargeResults {
            get {
                if (mDriverPaymentChargeResults == null) {
                    mDriverPaymentChargeResults = DriverPaymentResults.GetSaledApprovedResult(this);
                    if (mDriverPaymentResultses == null) {
                        mDriverPaymentResultses = new List<DriverPaymentResults>();
                    }
                    mDriverPaymentResultses.Add(mDriverPaymentChargeResults);
                }
                return mDriverPaymentChargeResults;
            }
            set {
                mDriverPaymentChargeResults = value;
            }
        }

        private DriverPaymentResults mDriverPaymentRefundResult;
        [JsonIgnore]
        public DriverPaymentResults DriverPaymentRefundResult {
            get {
                if (mDriverPaymentRefundResult == null) {
                    mDriverPaymentRefundResult = DriverPaymentResults.GetRefundApprovedResult(this);
                    if (mDriverPaymentResultses == null) {
                        mDriverPaymentResultses = new List<DriverPaymentResults>();
                    }
                    mDriverPaymentResultses.Add(mDriverPaymentRefundResult);
                }
                return mDriverPaymentRefundResult;
            }
            set {
                mDriverPaymentRefundResult = value;
            }
        }


        private DriverPaidCash mDriverPaidCash;
        [JsonIgnore]
        public DriverPaidCash DriverPaidCash {
            get {
                if (mDriverPaidCash == null) {
                    mDriverPaidCash = DriverPaidCash.GetPaidCash(this.DriverPaymentID);
                }
                return mDriverPaidCash;
            }
        }


        private DriverPaymentAux mDriverPaymentAux;
        [JsonIgnore]
        public DriverPaymentAux DriverPaymentAux {
            get {
                if (mDriverPaymentAux == null) {
                    using (var conn = SqlHelper.OpenSqlConnection()) {
                        mDriverPaymentAux = DriverPaymentAux.GetRecord(conn, this.DriverPaymentID);
                    }
                }
                return mDriverPaymentAux;
            }
            set {
                mDriverPaymentAux = value;
            }
        }

        private DriverPaymentGeoCodes mDriverPaymentGeoCodes;
        [JsonIgnore]
        public DriverPaymentGeoCodes DriverPaymentGeoCodes {
            get {
                if (mDriverPaymentGeoCodes == null) {
                    mDriverPaymentGeoCodes = DriverPaymentGeoCodes.GetRecordByDriverPaymentID(this.DriverPaymentID);
                }
                return mDriverPaymentGeoCodes;
            }
            set {
                mDriverPaymentGeoCodes = value;
            }
        }

        private List<DriverPaymentGeoCodes> mDriverPaymentGeoCodesList;
        [JsonIgnore]
        public List<DriverPaymentGeoCodes> DriverPaymentGeoCodeses {
            get {
                if (mDriverPaymentGeoCodesList == null) {
                    mDriverPaymentGeoCodesList = DriverPaymentGeoCodes.GetRecordsByDriverPaymentID(this.DriverPaymentID);
                }
                return mDriverPaymentGeoCodesList;
            }
            set {
                mDriverPaymentGeoCodesList = value;
            }
        }

        private List<DriverPaymentRef> mDriverPaymentRefList;
        [JsonIgnore]
        public List<DriverPaymentRef> DriverPaymentRefs {
            get {
                if (mDriverPaymentRefList == null) {
                    mDriverPaymentRefList = DriverPaymentRef.GetByDriverPaymentID(this.DriverPaymentID);
                }
                return mDriverPaymentRefList;
            }
            set {
                mDriverPaymentRefList = value;
            }
        }


        private RideInfo mRideInfo;
        [JsonIgnore]
        public RideInfo RideInfo {
            get {
                if (mRideInfo == null) {
                    mRideInfo = RideInfo.GetByDriverPaymentID(this.DriverPaymentID);
                }
                return mRideInfo;
            }
            set {
                mRideInfo = value;
            }
        }


        private ReservationRef mResRef;
        [JsonIgnore]
        public ReservationRef ReserveRef {
            get {
                if (mResRef == null) {
                    mResRef = ReservationRef.GetResByDriverPaymentID(this.DriverPaymentID);
                }
                return mResRef;
            }
        }

    }
}
