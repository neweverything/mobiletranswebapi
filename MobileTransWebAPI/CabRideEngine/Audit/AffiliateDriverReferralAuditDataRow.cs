using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Runtime.Serialization;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

namespace CabRideEngine {
 
  #region AffiliateDriverReferralAuditDataTable
  /// <summary>
  /// The generated <see cref="T:IdeaBlade.Persistence.EntityTable"/> class  
  /// containing all <see cref="AffiliateDriverReferralAudit"/> objects.
  /// </summary>
  /// <remarks>
  /// ****************************************************
  /// <para>** DO NOT MODIFY THIS CLASS - AutoGenerated Code</para>
  /// <para>** Any changes made to this code will be lost</para>
  /// <para>** the next time this code is regenerated.</para>
  /// <para>**</para>
  /// <para>** Generated from:   EntityBaseclass.template</para>
  /// <para>** Last modified:    2011-11-01T16:39:28.123689-07:00</para>
  /// <para>** Template version: 3.110127</para>
  /// ****************************************************
  /// </remarks>
  [BindingBrowsable(false)]
  [Serializable]
  public partial class AffiliateDriverReferralAuditDataTable : IdeaBlade.Persistence.EntityTable, 
     IdeaBlade.Persistence.Rdb.IRdbTable {
  
    /// <summary>
    /// Initializes a new instance of the AffiliateDriverReferralAuditDataTable class with no arguments.
    /// </summary>
    public AffiliateDriverReferralAuditDataTable() {}

    /// <summary>
    /// Initializes a new instance of the AffiliateDriverReferralAuditDataTable class 
    /// with the <see cref="System.Runtime.Serialization.SerializationInfo"/> 
    /// and the <see cref="System.Runtime.Serialization.StreamingContext"/>.
    /// </summary>
    /// <param name="pInfo">The data needed to serialize or deserialize an object.</param>
    /// <param name="pContext">The source and destination of a given serialized stream.</param>
    protected AffiliateDriverReferralAuditDataTable(SerializationInfo pInfo, StreamingContext pContext) :
      base(pInfo, pContext) {}

    /// <summary>
    /// Creates a new instance of the <see cref="AffiliateDriverReferralAuditDataRow"/>.
    /// Constructs a row from the builder.
    /// <para>** For internal use only. **</para>
    /// </summary>
    /// <param name="pRowBuilder"></param>
    /// <returns>The <see cref="AffiliateDriverReferralAuditDataRow"/>.</returns>
    protected override DataRow NewRowFromBuilder(DataRowBuilder pRowBuilder) {
      return new AffiliateDriverReferralAudit(pRowBuilder);
    }

    /// <summary>Returns the <see cref="System.Type"/> of <see cref="AffiliateDriverReferralAudit"/>.</summary>
    protected override Type GetRowType() {
      return typeof(AffiliateDriverReferralAudit);
    }
    
    /// <summary>
    /// Gets the name of the <see cref="T:IdeaBlade.Rdb.RdbKey"/> or 
    /// <see cref="T:IdeaBlade.Persistence.WS.WsKey"/> defined in <b>IdeaBlade.ibconfig</b>. 
    /// </summary>
    /// <remarks>For this <see cref="AffiliateDriverReferralAudit"/>, the DataSourceKeyName="Audit".</remarks>
    public override String DataSourceKeyName {
      get { return @"Audit"; }
    }

    #region Column properties
    //**************************************
    //* Column properties
    //**************************************
    /// <summary>Gets the AffiliateDriverReferralHistoryID <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn AffiliateDriverReferralHistoryIDColumn {
      get {
        if (mAffiliateDriverReferralHistoryIDColumn!=null) return mAffiliateDriverReferralHistoryIDColumn;
        mAffiliateDriverReferralHistoryIDColumn = GetColumn("AffiliateDriverReferralHistoryID", true);
        return mAffiliateDriverReferralHistoryIDColumn;
      }
    }
    private DataColumn mAffiliateDriverReferralHistoryIDColumn;
    
    /// <summary>Gets the AffiliateDriverReferralID <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn AffiliateDriverReferralIDColumn {
      get {
        if (mAffiliateDriverReferralIDColumn!=null) return mAffiliateDriverReferralIDColumn;
        mAffiliateDriverReferralIDColumn = GetColumn("AffiliateDriverReferralID", true);
        return mAffiliateDriverReferralIDColumn;
      }
    }
    private DataColumn mAffiliateDriverReferralIDColumn;
    
    /// <summary>Gets the AffiliateDriverID <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn AffiliateDriverIDColumn {
      get {
        if (mAffiliateDriverIDColumn!=null) return mAffiliateDriverIDColumn;
        mAffiliateDriverIDColumn = GetColumn("AffiliateDriverID", true);
        return mAffiliateDriverIDColumn;
      }
    }
    private DataColumn mAffiliateDriverIDColumn;
    
    /// <summary>Gets the ReferredDriverID <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn ReferredDriverIDColumn {
      get {
        if (mReferredDriverIDColumn!=null) return mReferredDriverIDColumn;
        mReferredDriverIDColumn = GetColumn("ReferredDriverID", true);
        return mReferredDriverIDColumn;
      }
    }
    private DataColumn mReferredDriverIDColumn;
    
    /// <summary>Gets the ReferralDate <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn ReferralDateColumn {
      get {
        if (mReferralDateColumn!=null) return mReferralDateColumn;
        mReferralDateColumn = GetColumn("ReferralDate", true);
        return mReferralDateColumn;
      }
    }
    private DataColumn mReferralDateColumn;
    
    /// <summary>Gets the RowVersion <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn RowVersionColumn {
      get {
        if (mRowVersionColumn!=null) return mRowVersionColumn;
        mRowVersionColumn = GetColumn("RowVersion", true);
        return mRowVersionColumn;
      }
    }
    private DataColumn mRowVersionColumn;
    
    /// <summary>Gets the ModifiedBy <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn ModifiedByColumn {
      get {
        if (mModifiedByColumn!=null) return mModifiedByColumn;
        mModifiedByColumn = GetColumn("ModifiedBy", true);
        return mModifiedByColumn;
      }
    }
    private DataColumn mModifiedByColumn;
    
    /// <summary>Gets the ModifiedDate <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn ModifiedDateColumn {
      get {
        if (mModifiedDateColumn!=null) return mModifiedDateColumn;
        mModifiedDateColumn = GetColumn("ModifiedDate", true);
        return mModifiedDateColumn;
      }
    }
    private DataColumn mModifiedDateColumn;
    
    /// <summary>Gets the Platform <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn PlatformColumn {
      get {
        if (mPlatformColumn!=null) return mPlatformColumn;
        mPlatformColumn = GetColumn("Platform", true);
        return mPlatformColumn;
      }
    }
    private DataColumn mPlatformColumn;
    
    #endregion    

    /// <summary>Creates the schema mapping information for this AffiliateDriverReferralAuditDataTable.</summary>
    /// <returns>
    /// The <see cref="T:IdeaBlade.Persistence.TableMappingInfo"/> for this AffiliateDriverReferralAuditDataTable.
    /// </returns>
    protected override TableMappingInfo CreateTableMappingInfo() {
      RdbTableMappingInfo mappingInfo = new RdbTableMappingInfo(this);
      mappingInfo.SourceOwnerName = "dbo"; 
      mappingInfo.SourceTableName = "AffiliateDriverReferralAudit"; 
      mappingInfo.ConcurrencyColumnName = ""; 
      // ColumnSelectionOption: selectAllColumns
      mappingInfo.SourceColumnNames = "*"; 
   
      DataColumnMappingCollection columnMappings = mappingInfo.TableMapping.ColumnMappings;
      columnMappings.Clear();
      columnMappings.Add("AffiliateDriverReferralHistoryID", "AffiliateDriverReferralHistoryID");
      columnMappings.Add("AffiliateDriverReferralID", "AffiliateDriverReferralID");
      columnMappings.Add("AffiliateDriverID", "AffiliateDriverID");
      columnMappings.Add("ReferredDriverID", "ReferredDriverID");
      columnMappings.Add("ReferralDate", "ReferralDate");
      columnMappings.Add("RowVersion", "RowVersion");
      columnMappings.Add("ModifiedBy", "ModifiedBy");
      columnMappings.Add("ModifiedDate", "ModifiedDate");
      columnMappings.Add("Platform", "Platform");
      return mappingInfo;
    }
  
    /// <summary>** For internal use only. **</summary>
    protected override void UpdateTableMappingInfo() {
      base.UpdateTableMappingInfo();
    }

    /// <summary>
    /// Initializes AffiliateDriverReferralAudit <see cref="System.Data.DataColumn"/> properties.
    /// </summary>
    public override void InitColumnProperties() {
      base.InitColumnProperties();

      AffiliateDriverReferralHistoryIDColumn.Caption = "AffiliateDriverReferralHistoryID";
      AffiliateDriverReferralIDColumn.Caption = "AffiliateDriverReferralID";
      AffiliateDriverIDColumn.Caption = "AffiliateDriverID";
      ReferredDriverIDColumn.Caption = "ReferredDriverID";
      ReferralDateColumn.Caption = "ReferralDate";
      RowVersionColumn.Caption = "RowVersion";
      ModifiedByColumn.Caption = "ModifiedBy";
      ModifiedDateColumn.Caption = "ModifiedDate";
      PlatformColumn.Caption = "Platform";
    }
  }
  #endregion
  
  #region AffiliateDriverReferralAuditDataRow
  /// <summary>
  /// The generated base class for <see cref="AffiliateDriverReferralAudit"/>.
  /// </summary>
  /// <remarks>
  /// <para>
  /// It represents a row of persistable data in the <see cref="AffiliateDriverReferralAuditDataTable"/>
  /// and derives from <see cref="T:IdeaBlade.Persistence.Entity"/>.
	/// </para>
  /// ****************************************************
  /// <para>** DO NOT MODIFY THIS CLASS - AutoGenerated Code</para>
  /// <para>** Any changes made to this code will be lost</para>
  /// <para>** the next time this code is regenerated.</para>
  /// <para>**</para>
  /// <para>** Generated from:   EntityBaseclass.template</para>
  /// <para>** Last modified:    2011-11-01T16:39:28.123689-07:00</para>
  /// <para>** Template version: 3.110127</para>
  /// ****************************************************
  /// </remarks>  
  [Serializable]
  public abstract partial class AffiliateDriverReferralAuditDataRow : IdeaBlade.Persistence.Entity, 
     IdeaBlade.Persistence.Rdb.IRdbEntity {
  
    /// <summary>
    /// Initializes a new instance of the AffiliateDriverReferralAuditDataRow.
    /// Constructs a row from the builder.
    /// <para>** For internal use only. **</para>
    /// </summary>
    /// <param name="pRowBuilder"></param>
    protected AffiliateDriverReferralAuditDataRow(DataRowBuilder pRowBuilder)
      : base(pRowBuilder) {
    }

    /// <summary>Gets the <see cref="AffiliateDriverReferralAuditDataTable"/>.</summary>
    [BindingBrowsable(false)]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public new AffiliateDriverReferralAuditDataTable TypedTable {
      get { return (AffiliateDriverReferralAuditDataTable) base.Table; }
    }

    /// <summary>Returns the <see cref="System.Type"/> of <see cref="AffiliateDriverReferralAuditDataTable"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public static Type GetTableType() {
      return typeof(AffiliateDriverReferralAuditDataTable);
    }

    /// <summary>
    /// Gets the SQL 'where' clause used to restrict which 
    /// database records are accessible via this entity. 
    /// </summary>
    /// <remarks>
    /// The default property value is defined in the Current Class 
    /// Properties tab in the <b>IdeaBlade DevForce Object Mapping Tool</b>.
    /// <para>
    /// The 'where' clause will be used, in addition to any other 
    /// query-specific criteria, in every retrieval of entities of this type.
    /// </para>
    /// </remarks>
    [BindingBrowsable(false)]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public virtual string SqlWhereClause {
      get { return ""; }
    }
    
    /// <summary>
    /// Determines whether to use the 'distinct' keyword when 
    /// generating the SQL select clause for objects of this type.
    /// </summary>
    /// <remarks>Defaults to false, but may be overridden in subclasses.</remarks>
    [BindingBrowsable(false)]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public virtual bool ForceSqlDistinct {
      get { return false; }
    }
    

    #region Enums
    //**************************************
    //* Enums
    //**************************************
    #endregion
	
    #region Relation properties
    //**************************************
    //* Relation properties
    //**************************************
    #endregion
    
    #region EntityColumn definitions
    //**************************************
    //* Static EntityColumn definitions
    //**************************************
    /// <summary>The AffiliateDriverReferralHistoryID <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn AffiliateDriverReferralHistoryIDEntityColumn =
      new EntityColumn(typeof(AffiliateDriverReferralAudit), "AffiliateDriverReferralHistoryID", typeof(System.Int64), false, true, true, DataSourceAccessType.ReadWrite);      
    /// <summary>The AffiliateDriverReferralID <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn AffiliateDriverReferralIDEntityColumn =
      new EntityColumn(typeof(AffiliateDriverReferralAudit), "AffiliateDriverReferralID", typeof(System.Int64), false, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The AffiliateDriverID <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn AffiliateDriverIDEntityColumn =
      new EntityColumn(typeof(AffiliateDriverReferralAudit), "AffiliateDriverID", typeof(System.Int64), false, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The ReferredDriverID <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn ReferredDriverIDEntityColumn =
      new EntityColumn(typeof(AffiliateDriverReferralAudit), "ReferredDriverID", typeof(System.Int64), false, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The ReferralDate <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn ReferralDateEntityColumn =
      new EntityColumn(typeof(AffiliateDriverReferralAudit), "ReferralDate", typeof(System.DateTime), false, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The RowVersion <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn RowVersionEntityColumn =
      new EntityColumn(typeof(AffiliateDriverReferralAudit), "RowVersion", typeof(System.Int32), false, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The ModifiedBy <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn ModifiedByEntityColumn =
      new EntityColumn(typeof(AffiliateDriverReferralAudit), "ModifiedBy", typeof(System.String), true, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The ModifiedDate <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn ModifiedDateEntityColumn =
      new EntityColumn(typeof(AffiliateDriverReferralAudit), "ModifiedDate", typeof(System.DateTime), true, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The Platform <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn PlatformEntityColumn =
      new EntityColumn(typeof(AffiliateDriverReferralAudit), "Platform", typeof(System.String), true, false, false, DataSourceAccessType.ReadWrite);      
    #endregion
    
    #region Properties
    //**************************************
    //* AffiliateDriverReferralHistoryID methods
    //**************************************
    /// <summary>Gets the AffiliateDriverReferralHistoryID <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn AffiliateDriverReferralHistoryIDColumn {
      get { return TypedTable.AffiliateDriverReferralHistoryIDColumn; }
    }

    /// <summary>Gets the AffiliateDriverReferralHistoryID.</summary>
    [DBDataType(typeof(System.Int64))]
    public virtual System.Int64 AffiliateDriverReferralHistoryID {
      get { 
        System.Int64 result_;
        if (GetInterceptor<System.Int64>("AffiliateDriverReferralHistoryID", GetAffiliateDriverReferralHistoryIDImpl, out result_)) return result_;
        return GetAffiliateDriverReferralHistoryIDImpl();
      }
    }
    private System.Int64 GetAffiliateDriverReferralHistoryIDImpl() {
      return (System.Int64) GetColumnValue(AffiliateDriverReferralHistoryIDColumn, typeof(System.Int64), false); 
    }
    
    //**************************************
    //* AffiliateDriverReferralID methods
    //**************************************
    /// <summary>Gets the AffiliateDriverReferralID <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn AffiliateDriverReferralIDColumn {
      get { return TypedTable.AffiliateDriverReferralIDColumn; }
    }

    /// <summary>Gets or sets the AffiliateDriverReferralID.</summary>
    [DBDataType(typeof(System.Int64))]
    public virtual System.Int64 AffiliateDriverReferralID {
      get { 
        System.Int64 result_;
        if (GetInterceptor<System.Int64>("AffiliateDriverReferralID", GetAffiliateDriverReferralIDImpl, out result_)) return result_;
        return GetAffiliateDriverReferralIDImpl();
      }
      set { 
          if (!SetInterceptor<System.Int64>("AffiliateDriverReferralID", value, SetAffiliateDriverReferralIDImpl)) {
            SetAffiliateDriverReferralIDImpl(value);
          }  
      }    
    }
    private System.Int64 GetAffiliateDriverReferralIDImpl() {
      return (System.Int64) GetColumnValue(AffiliateDriverReferralIDColumn, typeof(System.Int64), false); 
    }
    private void SetAffiliateDriverReferralIDImpl(System.Int64 value) {
      SetColumnValue(AffiliateDriverReferralIDColumn, value);
    }
    
    //**************************************
    //* AffiliateDriverID methods
    //**************************************
    /// <summary>Gets the AffiliateDriverID <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn AffiliateDriverIDColumn {
      get { return TypedTable.AffiliateDriverIDColumn; }
    }

    /// <summary>Gets or sets the AffiliateDriverID.</summary>
    [DBDataType(typeof(System.Int64))]
    public virtual System.Int64 AffiliateDriverID {
      get { 
        System.Int64 result_;
        if (GetInterceptor<System.Int64>("AffiliateDriverID", GetAffiliateDriverIDImpl, out result_)) return result_;
        return GetAffiliateDriverIDImpl();
      }
      set { 
          if (!SetInterceptor<System.Int64>("AffiliateDriverID", value, SetAffiliateDriverIDImpl)) {
            SetAffiliateDriverIDImpl(value);
          }  
      }    
    }
    private System.Int64 GetAffiliateDriverIDImpl() {
      return (System.Int64) GetColumnValue(AffiliateDriverIDColumn, typeof(System.Int64), false); 
    }
    private void SetAffiliateDriverIDImpl(System.Int64 value) {
      SetColumnValue(AffiliateDriverIDColumn, value);
    }
    
    //**************************************
    //* ReferredDriverID methods
    //**************************************
    /// <summary>Gets the ReferredDriverID <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn ReferredDriverIDColumn {
      get { return TypedTable.ReferredDriverIDColumn; }
    }

    /// <summary>Gets or sets the ReferredDriverID.</summary>
    [DBDataType(typeof(System.Int64))]
    public virtual System.Int64 ReferredDriverID {
      get { 
        System.Int64 result_;
        if (GetInterceptor<System.Int64>("ReferredDriverID", GetReferredDriverIDImpl, out result_)) return result_;
        return GetReferredDriverIDImpl();
      }
      set { 
          if (!SetInterceptor<System.Int64>("ReferredDriverID", value, SetReferredDriverIDImpl)) {
            SetReferredDriverIDImpl(value);
          }  
      }    
    }
    private System.Int64 GetReferredDriverIDImpl() {
      return (System.Int64) GetColumnValue(ReferredDriverIDColumn, typeof(System.Int64), false); 
    }
    private void SetReferredDriverIDImpl(System.Int64 value) {
      SetColumnValue(ReferredDriverIDColumn, value);
    }
    
    //**************************************
    //* ReferralDate methods
    //**************************************
    /// <summary>Gets the ReferralDate <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn ReferralDateColumn {
      get { return TypedTable.ReferralDateColumn; }
    }

    /// <summary>Gets or sets the ReferralDate.</summary>
    [DBDataType(typeof(System.DateTime))]
    public virtual System.DateTime ReferralDate {
      get { 
        System.DateTime result_;
        if (GetInterceptor<System.DateTime>("ReferralDate", GetReferralDateImpl, out result_)) return result_;
        return GetReferralDateImpl();
      }
      set { 
          if (!SetInterceptor<System.DateTime>("ReferralDate", value, SetReferralDateImpl)) {
            SetReferralDateImpl(value);
          }  
      }    
    }
    private System.DateTime GetReferralDateImpl() {
      return (System.DateTime) GetColumnValue(ReferralDateColumn, typeof(System.DateTime), false); 
    }
    private void SetReferralDateImpl(System.DateTime value) {
      SetColumnValue(ReferralDateColumn, value);
    }
    
    //**************************************
    //* RowVersion methods
    //**************************************
    /// <summary>Gets the RowVersion <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn RowVersionColumn {
      get { return TypedTable.RowVersionColumn; }
    }

    /// <summary>Gets or sets the RowVersion.</summary>
    [DBDataType(typeof(System.Int32))]
    public virtual System.Int32 RowVersion {
      get { 
        System.Int32 result_;
        if (GetInterceptor<System.Int32>("RowVersion", GetRowVersionImpl, out result_)) return result_;
        return GetRowVersionImpl();
      }
      set { 
          if (!SetInterceptor<System.Int32>("RowVersion", value, SetRowVersionImpl)) {
            SetRowVersionImpl(value);
          }  
      }    
    }
    private System.Int32 GetRowVersionImpl() {
      return (System.Int32) GetColumnValue(RowVersionColumn, typeof(System.Int32), false); 
    }
    private void SetRowVersionImpl(System.Int32 value) {
      SetColumnValue(RowVersionColumn, value);
    }
    
    //**************************************
    //* ModifiedBy methods
    //**************************************
    /// <summary>Gets the ModifiedBy <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn ModifiedByColumn {
      get { return TypedTable.ModifiedByColumn; }
    }

    /// <summary>Gets or sets the ModifiedBy.</summary>
    [MaxTextLength(100)]
    [DBDataType(typeof(System.String))]
    public virtual System.String ModifiedBy {
      get { 
        System.String result_;
        if (GetInterceptor<System.String>("ModifiedBy", GetModifiedByImpl, out result_)) return result_;
        return GetModifiedByImpl();
      }
      set { 
          if (!SetInterceptor<System.String>("ModifiedBy", value, SetModifiedByImpl)) {
            SetModifiedByImpl(value);
          }  
      }    
    }
    private System.String GetModifiedByImpl() {
      return (System.String) GetColumnValue(ModifiedByColumn, typeof(System.String), true); 
    }
    private void SetModifiedByImpl(System.String value) {
      SetColumnValue(ModifiedByColumn, value);
    }
    
    //**************************************
    //* ModifiedDate methods
    //**************************************
    /// <summary>Gets the ModifiedDate <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn ModifiedDateColumn {
      get { return TypedTable.ModifiedDateColumn; }
    }

    /// <summary>Gets or sets the ModifiedDate.</summary>
    [DBDataType(typeof(System.DateTime))]
    public virtual Nullable<System.DateTime> ModifiedDate {
      get { 
        Nullable<System.DateTime> result_;
        if (GetInterceptor<Nullable<System.DateTime>>("ModifiedDate", GetModifiedDateImpl, out result_)) return result_;
        return GetModifiedDateImpl();
      }
      set { 
          if (!SetInterceptor<Nullable<System.DateTime>>("ModifiedDate", value, SetModifiedDateImpl)) {
            SetModifiedDateImpl(value);
          }
      }    
    }
    private Nullable<System.DateTime> GetModifiedDateImpl() {
      return (Nullable<System.DateTime>) GetColumnValue(ModifiedDateColumn, typeof(System.DateTime), true); 
    }
    private void SetModifiedDateImpl(Nullable<System.DateTime> value) {
      SetColumnValue(ModifiedDateColumn, value);
    }
    
    //**************************************
    //* Platform methods
    //**************************************
    /// <summary>Gets the Platform <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn PlatformColumn {
      get { return TypedTable.PlatformColumn; }
    }

    /// <summary>Gets or sets the Platform.</summary>
    [MaxTextLength(50)]
    [DBDataType(typeof(System.String))]
    public virtual System.String Platform {
      get { 
        System.String result_;
        if (GetInterceptor<System.String>("Platform", GetPlatformImpl, out result_)) return result_;
        return GetPlatformImpl();
      }
      set { 
          if (!SetInterceptor<System.String>("Platform", value, SetPlatformImpl)) {
            SetPlatformImpl(value);
          }  
      }    
    }
    private System.String GetPlatformImpl() {
      return (System.String) GetColumnValue(PlatformColumn, typeof(System.String), true); 
    }
    private void SetPlatformImpl(System.String value) {
      SetColumnValue(PlatformColumn, value);
    }
    
    #endregion

  }
  #endregion

  #region EntityPropertyDescriptors.AffiliateDriverReferralAuditPropertyDescriptor
  public static partial class EntityPropertyDescriptors {

    /// <summary>
    /// Gets the AffiliateDriverReferralAuditPropertyDescriptor for <see cref="AffiliateDriverReferralAudit"/>.
    /// </summary>
    public static AffiliateDriverReferralAuditPropertyDescriptor AffiliateDriverReferralAudit {
      get { return AffiliateDriverReferralAuditPropertyDescriptor.Instance; }
    }
    
    /// <summary>
    /// The generated class containing <see cref="AffiliateDriverReferralAudit"/> PropertyDescriptors.
    /// </summary>
    public partial class AffiliateDriverReferralAuditPropertyDescriptor : AdaptedPropertyDescriptor {
	
			/// <summary>
      /// Initializes a new instance of the AffiliateDriverReferralAuditPropertyDescriptor class. 
      /// </summary>
      /// <param name="pParentDescriptor">The parent <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/></param>
      /// <param name="pPropertyPath">The type of the property value</param>
			public AffiliateDriverReferralAuditPropertyDescriptor(AdaptedPropertyDescriptor pParentDescriptor, String pPropertyPath)
        : base(pParentDescriptor, pPropertyPath) {}
		
      /// <summary>
      /// Initializes a new instance of the AffiliateDriverReferralAuditPropertyDescriptor class. 
      /// </summary>
      /// <param name="pComponentType">The type of object to which the property is bound</param>
      protected AffiliateDriverReferralAuditPropertyDescriptor(Type pComponentType)
        : base(pComponentType) {}

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for AffiliateDriverReferralHistoryID.</summary>
      public AdaptedPropertyDescriptor AffiliateDriverReferralHistoryID {
        get { return Get("AffiliateDriverReferralHistoryID"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for AffiliateDriverReferralID.</summary>
      public AdaptedPropertyDescriptor AffiliateDriverReferralID {
        get { return Get("AffiliateDriverReferralID"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for AffiliateDriverID.</summary>
      public AdaptedPropertyDescriptor AffiliateDriverID {
        get { return Get("AffiliateDriverID"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for ReferredDriverID.</summary>
      public AdaptedPropertyDescriptor ReferredDriverID {
        get { return Get("ReferredDriverID"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for ReferralDate.</summary>
      public AdaptedPropertyDescriptor ReferralDate {
        get { return Get("ReferralDate"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for RowVersion.</summary>
      public AdaptedPropertyDescriptor RowVersion {
        get { return Get("RowVersion"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for ModifiedBy.</summary>
      public AdaptedPropertyDescriptor ModifiedBy {
        get { return Get("ModifiedBy"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for ModifiedDate.</summary>
      public AdaptedPropertyDescriptor ModifiedDate {
        get { return Get("ModifiedDate"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Platform.</summary>
      public AdaptedPropertyDescriptor Platform {
        get { return Get("Platform"); }
      }

     internal static AffiliateDriverReferralAuditPropertyDescriptor Instance = new AffiliateDriverReferralAuditPropertyDescriptor(typeof(AffiliateDriverReferralAudit));
    }
  }
  #endregion
  
}
