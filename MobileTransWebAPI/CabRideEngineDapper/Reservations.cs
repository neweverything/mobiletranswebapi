﻿using CabRideEngineDapper.Enums;
using CabRideEngineDapper.Utils;
using Dapper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using TaxiPassCommon;

namespace CabRideEngineDapper {
    public partial class Reservations {

        public static Reservations Create(SqlConnection pConn, Customer pCustomer) {
            //using (var conn = SqlHelper.OpenSqlConnection()) {
            Reservations rec = ReservationsTable.Create();
            rec.mCustomer = pCustomer;

            rec.CustomerID = pCustomer.CustomerID;
            rec.ReserveNo = TaxiPassCommon.StringUtils.GetUniqueKey(12);
            rec.AffiliateConfirmationCode = new Random().Next(100).ToString().PadLeft(2, '0');

            //turn off all notification options, only turn them on if a user books an ASAP ride
            rec.ASAPRide = false;
            rec.MaxPassengerWaitTime = -1;
            rec.Notes = "";
            rec.SalesSystem = "";

            if (rec.Customer.BillTo > 0) {
                rec.BillTo = rec.Customer.BillTo;
            } else {
                if (rec.Customer.Accounts != null && rec.Customer.Accounts.CompanyBilled) {
                    List<Customer> oCustomers = Customer.GetCustomerAccountAdmins(pConn, rec.Customer.Accounts.AccountNo);
                    if (oCustomers.Count > 0) {
                        rec.BillTo = oCustomers[0].CustomerID;
                    }
                } else {
                    rec.BillTo = pCustomer.CustomerID;
                }
            }
            return rec;
            //}
        }


        #region Related Tables

        private Customer mCustomer;
        public Customer Customer {
            get {
                if (mCustomer == null) {
                    mCustomer = Customer.GetCustomer(this.CustomerID);
                }
                return mCustomer;
            }
        }

        private Drivers mDriver;
        public Drivers Driver {
            get {
                if (mDriver == null) {
                    mDriver = Drivers.GetDriver(this.DriverID.GetValueOrDefault(0));
                }
                return mDriver;
            }
        }

        private ReserveMTAUserVerified mReserveMTAUserVerified;
        public ReserveMTAUserVerified ReserveMTAUserVerified {
            get {
                if (mReserveMTAUserVerified == null) {
                    mReserveMTAUserVerified = CabRideEngineDapper.ReserveMTAUserVerified.GetRecord(this.ReservationID);
                }
                return mReserveMTAUserVerified;
            }
        }

        #endregion


        public static Reservations GetReservation(SqlConnection pConn, string pReserveNo) {
            return pConn.Query<Reservations>("SELECT * FROM Reservations Where ReserveNo = @ReserveNo", new { ReserveNo = pReserveNo }).DefaultIfEmpty(Reservations.GetNullEntity()).FirstOrDefault();
        }

        public static Reservations GetReservation(SqlConnection pConn, long pReserveID) {
            return pConn.Query<Reservations>("SELECT * FROM Reservations Where ReservationID = @ReservationID", new { ReservationID = pReserveID }).FirstOrDefault();
        }


        public static List<Reservations> GetReservationsBySalesRep(SalesReps pSalesRep, string pReserveNo) {
            if (pReserveNo.IsNullOrEmpty()) {
                throw new Exception("Reserve No is required");
            }

            List<long> affiliateIDs = (from p in pSalesRep.SalesRepAffiliateses
                                       select p.AffiliateID).ToList();

            string sql = @"SELECT * FROM Reservations WITH (NOLOCK)
							WHERE AffiliateID IN @AffiliateID
								AND (ReserveNo = @ReserveNo
								OR ExternalReferenceID = @ReserveNo)
							ORDER BY UTCPUDateTime";
            DynamicParameters param = new DynamicParameters();
            param.Add("AffiliateID", affiliateIDs);
            param.Add("ReserveNo", pReserveNo);

            using (var conn = SqlHelper.OpenSqlConnection()) {
                return conn.Query<Reservations>(sql, param).ToList();
            }
        }

        public static List<Reservations> GetReservationsByDateBySalesRep(SalesReps pSalesRep, DateTime pStartDate, DateTime pEndDate, AffiliateStatus pStatus, System.ComponentModel.ListSortDirection pSortDirection) {
            return GetReservationsByDateBySalesRep(pSalesRep, "", pStartDate, pEndDate, pStatus, pSortDirection);
        }

        public static List<Reservations> GetReservationsByDateBySalesRep(SalesReps pSalesRep, string pMemberID, DateTime pStartDate, DateTime pEndDate, AffiliateStatus pStatus, System.ComponentModel.ListSortDirection pSortDirection) {
            List<long> affiliateIDs = (from p in pSalesRep.SalesRepAffiliateses
                                       select p.AffiliateID).ToList();

            StringBuilder sql = new StringBuilder(@"SELECT * FROM Reservations WITH (NOLOCK)
													WHERE PickUpDate >= @StartDate
														AND PickUpDate <= @EndDate
														AND AffiliateID IN @AffiliateIDs ");

            DynamicParameters param = new DynamicParameters();
            param.Add("StartDate", pStartDate);
            param.Add("EndDate", pEndDate);
            param.Add("AffiliateIDs", affiliateIDs);


            if (!pMemberID.IsNullOrEmpty()) {
                param.Add("EmployeeNumber", pMemberID);
                sql.Append("AND EmployeeNumber = @EmployeeNumber ");
            }

            switch (pStatus) {
                case AffiliateStatus.unconfirmedRides:
                    sql.Append(@"AND ((AffiliateConfirmation is null or AffiliateConfirmation = '') 
									AND (AffiliateCancelConfirmation is null OR AffiliateCancelConfirmation= '')) ");
                    break;
                case AffiliateStatus.confirmedRides:
                    sql.Append("AND AffiliateConfirmation != '' ");
                    break;
            }
            sql.Append("ORDER BY UTCPUDateTime " + (pSortDirection == ListSortDirection.Descending ? "Desc" : ""));
            using (var conn = SqlHelper.OpenSqlConnection()) {
                return conn.Query<Reservations>(sql.ToString(), param).ToList();
            }
        }

        public static List<Reservations> GetReservationsByDateByAffiliate(long pAffiliateID, DateTime pStartDate, DateTime pEndDate, AffiliateStatus pStatus, ListSortDirection pSortDirection) {
            return GetReservationsByDateByAffiliate(pAffiliateID, "", pStartDate, pEndDate, pStatus, pSortDirection);
        }

        public static List<Reservations> GetReservationsByDateByAffiliate(long pAffiliateID, string pMemberID, DateTime pStartDate, DateTime pEndDate, AffiliateStatus pStatus, System.ComponentModel.ListSortDirection pSortDirection) {
            StringBuilder sql = new StringBuilder(@"SELECT * FROM Reservations 
													WHERE PickUpDate >= @StartDate
															AND PickUpDate <= @EndDate
															AND AffiliateID = @AffiliateID ");

            DynamicParameters param = new DynamicParameters();
            param.Add("StartDate", pStartDate);
            param.Add("EndDate", pEndDate);
            param.Add("AffiliateID", pAffiliateID);

            if (!pMemberID.IsNullOrEmpty()) {
                sql.Append("AND EmployeeNumber = @EmployeeNumber ");
                param.Add("EmployeeNumber", pMemberID);
            }

            switch (pStatus) {
                case AffiliateStatus.unconfirmedRides:
                    sql.Append(@"AND ((AffiliateConfirmation is null or AffiliateConfirmation = '') 
									AND (AffiliateCancelConfirmation is null OR AffiliateCancelConfirmation= '')) ");
                    break;
                case AffiliateStatus.confirmedRides:
                    sql.Append("AND AffiliateConfirmation != '' ");
                    break;
            }
            sql.Append("AND Cancelled = 0 ");
            sql.Append("ORDER BY UTCPUDateTime " + (pSortDirection == ListSortDirection.Descending ? "Desc" : ""));
            using (var conn = SqlHelper.OpenSqlConnection()) {
                return conn.Query<Reservations>(sql.ToString(), param).ToList();
            }
        }


        public static List<Reservations> GetReservationsByDateByAffiliate(Affiliate pAffiliate, DateTime pStartDate, DateTime pEndDate, AffiliateStatus pStatus, ListSortDirection pSortDirection) {
            return GetReservationsByDateByAffiliate(pAffiliate, "", pStartDate, pEndDate, pStatus, pSortDirection);
        }

        public static List<Reservations> GetReservationsByDateByAffiliate(Affiliate pAffiliate, string pMemberID, DateTime pStartDate, DateTime pEndDate, AffiliateStatus pStatus, ListSortDirection pSortDirection) {
            StringBuilder sql = new StringBuilder(@"SELECT * FROM Reservations 
													WHERE PickUpDate >= @StartDate
															AND PickUpDate <= @EndDate
															AND AffiliateID = @AffiliateID ");

            DynamicParameters param = new DynamicParameters();
            param.Add("StartDate", pStartDate);
            param.Add("EndDate", pEndDate);
            param.Add("AffiliateID", pAffiliate.AffiliateID);

            if (!pMemberID.IsNullOrEmpty()) {
                sql.Append("AND EmployeeNumber = @EmployeeNumber ");
                param.Add("EmployeeNumber", pMemberID);
            }

            switch (pStatus) {
                case AffiliateStatus.unconfirmedRides:
                    sql.Append(@"AND ((AffiliateConfirmation is null or AffiliateConfirmation = '') 
									AND (AffiliateCancelConfirmation is null OR AffiliateCancelConfirmation= '')) ");
                    break;
                case AffiliateStatus.confirmedRides:
                    sql.Append("AND AffiliateConfirmation != '' ");
                    break;
            }
            sql.Append("AND Cancelled = 0 ");
            sql.Append("ORDER BY UTCPUDateTime " + (pSortDirection == ListSortDirection.Descending ? "Desc" : ""));
            using (var conn = SqlHelper.OpenSqlConnection()) {
                return conn.Query<Reservations>(sql.ToString(), param).ToList();
            }
        }

        public static List<Reservations> GetReservationsByAffiliate(long pAffiliateID, string pReserveNo) {
            if (pReserveNo.IsNullOrEmpty()) {
                throw new Exception("Reserve No is required");
            }

            string sql = @"SELECT * FROM Reservations 
							WHERE AffiliateID = @AffiliateID AND (ReserveNo = @ReserveNo OR ExternalReferenceID = @ReserveNo)
						   Order By UTCPUDateTime";
            using (var conn = SqlHelper.OpenSqlConnection()) {
                return conn.Query<Reservations>(sql, new { AffiliateID = pAffiliateID, ReserveNo = pReserveNo }).ToList();
            }

        }

        public static Reservations GetResByDriverPaymentID(long pDriverPaymentID) {
            string sql = @"SELECT  *
                            FROM    Reservations
                            WHERE   ReservationID IN (SELECT    ReservationID
                                                      FROM      ReservationRef
                                                      WHERE     TableName = 'DriverPayments'
                                                                AND TableID = @TableID)";

            using (var conn = SqlHelper.OpenSqlConnection()) {
                return conn.Get<Reservations>(sql, new { TableID = pDriverPaymentID });
            }
        }

        public static Reservations GetReservationByReference(SqlConnection pConn, string pReference) {
            string sql = @"SELECT * FROM Reservations
                            WHERE Reference = @Reference";

            return pConn.Get<Reservations>(sql, new { Reference = pReference });
        }

        public static List<Reservations> GetReservationsByDriver(Drivers pDriver, DateTime pStartDate, DateTime pEndDate) {
            string sql = @"SELECT  *
                            FROM    Reservations
                            WHERE   PickUpDate >= @StartDate
                                    AND PickUpDate <= @EndDate
                                    AND DriverID = @DriverID
                            ORDER BY PickUpDate";
            DynamicParameters parms = new DynamicParameters();
            parms.Add("StartDate", pStartDate);
            parms.Add("EndDate", pEndDate);
            parms.Add("DriverID", pDriver.DriverID);

            using (var conn = SqlHelper.OpenSqlConnection()) {
                return conn.Query<Reservations>(sql, parms).ToList();
            }
        }

        public static Reservations GetReservation(SqlConnection pConn, string pExternalReserveID, string pSalesSystem) {
            string sql = @"SELECT  *
                            FROM    Reservations
                            WHERE   ExternalReferenceID = @ExternalReserveID
                                    AND SalesSystem = @SalesSystem";
            DynamicParameters parms = new DynamicParameters();
            parms.Add("ExternalReserveID", pExternalReserveID);
            parms.Add("SalesSystem", pSalesSystem);
            return pConn.Get<Reservations>(sql, parms);
        }

        public static Reservations GetReservation(SqlConnection pConn, string pExternalReserveID, string pSalesSystem, string pAccountNo) {
            string sql = @"SELECT  *
                            FROM    Reservations
                            WHERE   ExternalReferenceID = @ExternalReserveID
                                    AND SalesSystem = @SalesSystem
                                    AND AccountNo = @AccountNo";
            DynamicParameters parms = new DynamicParameters();
            parms.Add("ExternalReserveID", pExternalReserveID);
            parms.Add("SalesSystem", pSalesSystem);
            parms.Add("AccountNo", pAccountNo);
            return pConn.Get<Reservations>(sql, parms);
        }

    }
}
