﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web;
using System.Net;
using System.IO;

using IdeaBlade.Persistence;

namespace CabRideEngine.Utils {

    public class InnoportFax {

        private SystemDefaults mDefault;
        private string mToken;


        public InnoportFax(SystemDefaults pDefaultTableEntity) {
            mDefault = pDefaultTableEntity;
            if (string.IsNullOrEmpty(mDefault.InnoportSAR)) {
                throw new Exception("System Defaults Innoport SAR must be specified!");
            }
        }

        private string GetToken() {
            string tokenURL = mDefault.InnoportFaxTokenURL.Trim();
            if (tokenURL == "") {
                throw new Exception("System Defaults Fax Token URL must be specified!");
            }

            try {
                string sParam = "SAR=" + mDefault.InnoportSAR;

                HttpWebRequest oRequest = (HttpWebRequest)WebRequest.Create(tokenURL);
                oRequest.Method = "POST";
                oRequest.CookieContainer = new CookieContainer();
                oRequest.ContentLength = sParam.Length;
                oRequest.ContentType = "application/x-www-form-urlencoded";

                TextWriter oWriter = (TextWriter)new StreamWriter(oRequest.GetRequestStream());
                oWriter.Write(sParam);
                oWriter.Close();

                HttpWebResponse oResponse = (HttpWebResponse)oRequest.GetResponse();
                TextReader oReader = (TextReader)new StreamReader(oResponse.GetResponseStream());
                mToken = oReader.ReadToEnd();
                oReader.Close();
                oResponse.Close();

            } catch (Exception ex) {
                throw ex;
            }
            return mToken;
        }

        public string FaxSubmit(string pFaxTo, string pFaxNumber, string pFaxMessage) {

            if (string.IsNullOrEmpty(pFaxTo) || string.IsNullOrEmpty(pFaxNumber) || string.IsNullOrEmpty(pFaxMessage)) {
                throw new Exception("All parameters must have a value");
            }

            string sResponse = "";

            try {

                GetToken();
                if (string.IsNullOrEmpty(mToken)) {
                    throw new Exception("Could not initiate request with Innoport");
                }

                string sBoundary = "---==innoport_HTTPS_outbound_fax_api_boundary";

                MemoryStream strmParam = new MemoryStream();

                // param SAR
                string sParam = "\r\n" + sBoundary + "\r\n" + "Content-Disposition: form-data; name=\"SAR\"\r\n\r\n";
                sParam += mDefault.InnoportSAR;

                // param token
                sParam += "\r\n" + sBoundary + "\r\n" + "Content-Disposition: form-data; name=\"token\"\r\n\r\n";
                sParam += mToken;
                strmParam.Write(Encoding.Default.GetBytes(sParam), 0, sParam.Length);

                // param DocumentFile1
                sParam = "\r\n" + sBoundary + "\r\n" + "Content-Disposition: form-data; name=\"DocumentFile1\"; filename=\"Fax.htm\"\r\nContent-Type: application/octet_stream\r\n\r\n";
                strmParam.Write(Encoding.Default.GetBytes(sParam), 0, sParam.Length);

                strmParam.Write(Encoding.Default.GetBytes(pFaxMessage), 0, pFaxMessage.Length);

                // param RecipientFaxNumber
                sParam = "\r\n" + sBoundary + "\r\n" + "Content-Disposition: form-data; name=\"Recipient1FaxNumber\"\r\n\r\n";
                sParam += "1" + pFaxNumber;

                // param RecipientName
                sParam += "\r\n" + sBoundary + "\r\n" + "Content-Disposition: form-data; name=\"Recipient1Name\"\r\n\r\n";
                sParam += pFaxTo;
                strmParam.Write(Encoding.Default.GetBytes(sParam), 0, sParam.Length);

                // end of params
                sParam = "\r\n" + sBoundary + "--";
                strmParam.Write(Encoding.Default.GetBytes(sParam), 0, sParam.Length);
                byte[] bParam = strmParam.GetBuffer();


                HttpWebRequest oRequest = (HttpWebRequest)WebRequest.Create(mDefault.InnoportFaxSubmitURL);
                oRequest.Method = "POST";
                oRequest.CookieContainer = new CookieContainer();
                oRequest.ContentLength = bParam.Length;
                oRequest.ContentType = "multipart/form-data; boundary=" + sBoundary;
                oRequest.Timeout = 60000;  // Set the  'Timeout' property of the HttpWebRequest to 60000 milli seconds
                oRequest.GetRequestStream().Write(bParam, 0, bParam.Length);
                oRequest.GetRequestStream().Close();

                HttpWebResponse oResponse = (HttpWebResponse)oRequest.GetResponse();
                TextReader oReader = (TextReader)new StreamReader(oResponse.GetResponseStream());
                sResponse = oReader.ReadToEnd();
                oReader.Close();
                oResponse.Close();

            } catch (Exception ex) {
                // Console.Write(e.Message);
                throw ex;
            }
            return sResponse;
        }

        public string CheckStatus() {

            if (string.IsNullOrEmpty(mDefault.InnoportFaxStatusURL)) {
                throw new Exception("System Defaults Fax Status URL must be specified!");
            }

            string sResponse = "";
            try {
                string sParam = "SAR=" + mDefault.InnoportSAR;
                sParam = "&TOKEN=" + mToken;

                HttpWebRequest oRequest = (HttpWebRequest)WebRequest.Create(mDefault.InnoportFaxStatusURL);
                oRequest.Method = "POST";
                oRequest.CookieContainer = new CookieContainer();
                oRequest.ContentLength = sParam.Length;
                oRequest.ContentType = "application/x-www-form-urlencoded";

                TextWriter oWriter = (TextWriter)new StreamWriter(oRequest.GetRequestStream());
                oWriter.Write(sParam);
                oWriter.Close();

                HttpWebResponse oResponse = (HttpWebResponse)oRequest.GetResponse();
                TextReader oReader = (TextReader)new StreamReader(oResponse.GetResponseStream());
                sResponse = oReader.ReadToEnd();
                oReader.Close();
                oResponse.Close();

            } catch (Exception ex) {
                throw ex;
            }
            return sResponse;
        }
    }
}
