using Csla.Validation;
using IdeaBlade.Persistence;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;
using System;
using System.Data;
using System.Text;

namespace CabRideEngine {
	/// <summary>
	/// The developer class representing the PushMessage business object class.
	/// </summary>
	/// <remarks>
	/// <para>Place all application-specific business logic in this class.</para>
	/// <para>
	/// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
	/// and will not overwrite changes.
	/// </para>
	/// <para>
	/// This class inherits from the generated class, its companion in the pair of classes 
	/// defining a business object. Properties in the generated class may be overridden in 
	/// order to change the behavior or add business logic.
	/// </para>
	/// <para>
	/// This class is the final class in the inheritance chain for this business object. 
	/// It should not be inherited.
	/// </para>
	/// </remarks>
	[Serializable]
	public sealed partial class PushMessage : PushMessageDataRow {

		#region Constructors -- DO NOT MODIFY

		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new';
		// it is also required if the class is visible from a web service.
		// DO NOT REMOVE **
		private PushMessage() : this(null) { }

		/// <summary>
		/// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
		/// </summary>
		/// <param name="pRowBuilder"></param>
		public PushMessage(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static PushMessage Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      PushMessage aPushMessage = pManager.CreateEntity<PushMessage>();
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aPushMessage, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aPushMessage.AddToManager();
		//      return aPushMessage;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		// Add additional logic to your business object here...
		public static PushMessage Create(PersistenceManager pPM) {

			PushMessage rec = pPM.CreateEntity<PushMessage>();
			rec.AddToManager();
			return rec;
		}


		protected override void AddRules(RuleList pList) {
			PropertyRequiredRule.AddToList(pList, PushMessage.TitleEntityColumn.ColumnName);
			PropertyRequiredRule.AddToList(pList, PushMessage.MessageEntityColumn.ColumnName);
			pList.Add(IsTitleUnique, PushMessage.TitleEntityColumn.ColumnName);
		}

		private bool IsTitleUnique(object pTarget, RuleArgs e) {
			PushMessage rec = pTarget as PushMessage;

			RdbQuery oQry = new RdbQuery(typeof(PushMessage), PushMessage.TitleEntityColumn, EntityQueryOp.EQ, rec.Title);
			oQry.AddClause(PushMessage.PushMessageIDEntityColumn, EntityQueryOp.NE, rec.PushMessageID);
			EntityList<PushMessage> list = this.PersistenceManager.GetEntities<PushMessage>(oQry);
			if (list.Count > 0) {
				e.Description = "Title must be unique!";
				return false;
			}
			return true;
		}

		[BindingBrowsable(false)]
		public override string EntityInstanceDisplayName {
			get {
				return Title;
			}
		}

		protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
			if (IsDeserializing) {
				return;
			}
			base.OnColumnChanging(pArgs);
			DoAudit(pArgs);
		}

		private void DoAudit(DataColumnChangeEventArgs pArgs) {
			if (!this.PersistenceManager.IsClient) {
				return;
			}
			if (this.RowState == DataRowState.Added) {
				return;
			}
			if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
				AuditRecord = PushMessageAudit.Create(this);
			}
		}

		public EntityList<CabDriverRef> PushMessageDrivers {
			get {
				return CabDriverRef.GetRecords(this.PersistenceManager, "PushMessageDriver", this.PushMessageID);
			}
		}

		public EntityList<CabDriverRef> PushMessageDevices {
			get {
				return CabDriverRef.GetRecords(this.PersistenceManager, "PushMessageDevice", this.PushMessageID);
			}
		}


		public EntityList<CabDriverRef> PushMessageAffiliates {
			get {
				return CabDriverRef.GetRecords(this.PersistenceManager, "PushMessageAffiliate", this.PushMessageID);
			}
		}


		public static EntityList<PushMessage> GetAll(PersistenceManager pPM) {
			EntityList<PushMessage> oList = pPM.GetEntities<PushMessage>();
			oList.ApplySort(PushMessage.TitleEntityColumn.ColumnName, System.ComponentModel.ListSortDirection.Ascending, false);
			return oList;
		}

		public static EntityList<PushMessage> GetMessages(Drivers pDriver, bool pOnBoarding) {
			StringBuilder sql = new StringBuilder("SELECT PushMessage.* FROM PushMessage ");
			sql.AppendLine("LEFT JOIN CabDriverRef ON PushMessage.PushMessageID = CabDriverRef.SourceID AND SourceTable = 'PushMessageAffiliate' ");
			sql.AppendFormat("WHERE Active = 1 AND OnBoardingMsg = {0} and (AllAffiliates = 1 OR CabDriverRef.ValueString = '{1}')", pOnBoarding ? 1 : 0, pDriver.AffiliateID.GetValueOrDefault(0));

			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(PushMessage), sql.ToString());

			return pDriver.PersistenceManager.GetEntities<PushMessage>(qry);
		}

		public static EntityList<PushMessage> GetMessages(Drivers pDriver) {
			StringBuilder sql = new StringBuilder("SELECT Distinct PushMessage.* FROM PushMessage ");
			sql.AppendLine("LEFT JOIN CabDriverRef ON PushMessage.PushMessageID = CabDriverRef.SourceID AND SourceTable = 'PushMessageAffiliate' ");
			sql.AppendFormat("WHERE Active = 1 AND (AllAffiliates = 1 OR CabDriverRef.ValueString = '{0}')", pDriver.AffiliateID.GetValueOrDefault(0));

			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(PushMessage), sql.ToString());

			return pDriver.PersistenceManager.GetEntities<PushMessage>(qry);
		}

		public static PushMessage GetMessage(PersistenceManager pPM, int pMsgID) {
			RdbQuery qry = new RdbQuery(typeof(PushMessage), PushMessage.PushMessageIDEntityColumn, EntityQueryOp.EQ, pMsgID);
			return pPM.GetEntity<PushMessage>(qry);
		}
	}

	#region EntityPropertyDescriptors.PushMessagePropertyDescriptor
	////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
	////    For example: 
	////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
	//public static partial class EntityPropertyDescriptors {
	//	public partial class PushMessagePropertyDescriptor : BaseEntityPropertyDescriptor {
	//    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
	//    public AdaptedPropertyDescriptor Age {
	//      get { return this.Get("Age"); }
	//    }
	//  }
	//} 
	#endregion

}