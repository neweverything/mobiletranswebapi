﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using System.Linq;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

using Csla.Validation;

using TaxiPassCommon;
using System.Collections.Generic;
using TaxiPassCommon.Banking;

using Newtonsoft.Json;

namespace CabRideEngine {
	/// <summary>
	/// The developer class representing the RedeemerPayCards business object class.
	/// </summary>
	/// <remarks>
	/// <para>Place all application-specific business logic in this class.</para>
	/// <para>
	/// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
	/// and will not overwrite changes.
	/// </para>
	/// <para>
	/// This class inherits from the generated class, its companion in the pair of classes 
	/// defining a business object. Properties in the generated class may be overridden in 
	/// order to change the behavior or add business logic.
	/// </para>
	/// <para>
	/// This class is the final class in the inheritance chain for this business object. 
	/// It should not be inherited.
	/// </para>
	/// </remarks>
	[Serializable]
	public sealed partial class RedeemerPayCards : RedeemerPayCardsDataRow {

		#region Constructors -- DO NOT MODIFY

		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new';
		// it is also required if the class is visible from a web service.
		// DO NOT REMOVE **
		private RedeemerPayCards() : this(null) { }

		/// <summary>
		/// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
		/// </summary>
		/// <param name="pRowBuilder"></param>
		public RedeemerPayCards(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static RedeemerPayCards Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      RedeemerPayCards aRedeemerPayCards = pManager.CreateEntity<RedeemerPayCards>();
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aRedeemerPayCards, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aRedeemerPayCards.AddToManager();
		//      return aRedeemerPayCards;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		// Add additional logic to your business object here...
		public static RedeemerPayCards Create(TaxiPassRedeemerAccounts pRedeemerAccount) {
			RedeemerPayCards rec = null;
			try {
				rec = pRedeemerAccount.PersistenceManager.CreateEntity<RedeemerPayCards>();

				// Using the IdeaBlade Id Generation technique
				//pRedeemerAccount.PersistenceManager.GenerateId(rec, RedeemerPayCards.RedeemerPayCardIDEntityColumn);
				rec.TaxiPassRedeemerAccountID = pRedeemerAccount.TaxiPassRedeemerAccountID;
				short pos = 0;
				try {
					pos = pRedeemerAccount.RedeemerPayCardses.Max(p => p.CardUseOrder);
				} catch (Exception) {
					pos = 0;
				}
				rec.CardUseOrder = ++pos;
				rec.AddToManager();

			} catch (Exception ex) {
				throw ex;
			}
			return rec;
		}

		protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
			if (IsDeserializing) {
				return;
			}
			base.OnColumnChanging(pArgs);
			//DoAudit(pArgs);
		}

		private void DoAudit(DataColumnChangeEventArgs pArgs) {
			if (!this.PersistenceManager.IsClient) {
				return;
			}
			if (this.RowState == DataRowState.Added) {
				return;
			}
			if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
				AuditRecord = RedeemerPayCardsAudit.Create(this.PersistenceManager, this);
			}
		}

		protected override void AddRules(RuleList pList) {
			pList.Add(IsCardNumberUnique, TransCardCardNumberEntityColumn.ColumnName);
		}

		private bool IsCardNumberUnique(object pTarget, RuleArgs e) {
			RedeemerPayCards rec = pTarget as RedeemerPayCards;

			if (rec.TransCardNumber.IsNullOrEmpty()) {
				return false;
			}

			RdbQuery qry = new RdbQuery(typeof(RedeemerPayCards), RedeemerPayCards.TransCardCardNumberEntityColumn, EntityQueryOp.EQ, rec.TransCardCardNumber);
			qry.AddClause(RedeemerPayCards.TaxiPassRedeemerAccountIDEntityColumn, EntityQueryOp.EQ, rec.TaxiPassRedeemerAccountID);
			qry.AddClause(RedeemerPayCards.RedeemerPayCardIDEntityColumn, EntityQueryOp.NE, rec.RedeemerPayCardID);

			EntityList<RedeemerPayCards> oList = this.PersistenceManager.GetEntities<RedeemerPayCards>(qry);
			foreach (RedeemerPayCards myRec in oList) {
				if (myRec.TransCardNumber.Equals(rec.TransCardNumber)) {
					e.Description = "This card number already exists: " + rec.TransCardNumber;
					return false;
				}
			}
			if (rec.TransCardNumber.Equals(rec.TaxiPassRedeemerAccounts.TransCardNumber)) {
				e.Description = "This card number is assigned as the main number: " + rec.TransCardNumber;
				return false;
			}

			return true;
		}

		public override string TransCardNumber {
			get {
				return base.TransCardNumber.DecryptIceKey();
			}
			set {
				base.TransCardNumber = value.EncryptIceKey();
				base.TransCardCardNumber = CustomerCardNumberDisplay(value);
			}
		}

		private static string CustomerCardNumberDisplay(string pCardNo) {
			if (pCardNo.Length > 10) {
				string stars = new string('*', 10);
				return pCardNo.Left(1) + stars + pCardNo.Right(5);
			}
			return pCardNo;
		}

		public override string TransCardID {
			get {
				return base.TransCardID.DecryptIceKey();
			}
			set {
				base.TransCardID = value.EncryptIceKey();
			}
		}

		public override string TransCardNewID {
			get {
				return base.TransCardNewID.DecryptIceKey();
			}
			set {
				base.TransCardNewID = value.EncryptIceKey();
			}
		}
		public override DateTime TransCardExpiration {
			get {
				return base.TransCardExpiration;
			}
			set {
				//if (value.HasValue) {
				base.TransCardExpiration = new DateTime(value.Year, value.Month, 1);
				//} else {
				base.TransCardExpiration = value;
				//}
			}
		}

		// *******************************************************************************************
		// Validate and activate the PayCard
		// *******************************************************************************************
//        public TransCard.VALIDATE_CARD_RET ActivateTransCard() {
//            TransCard.VALIDATE_CARD_RET transResult = new TransCard.VALIDATE_CARD_RET();

//            //update card if we have all info otherwise inactivate account until we do
//            TaxiPassRedeemerAccounts acct = this.TaxiPassRedeemerAccounts;
//            if (acct.Contact.IsNullOrEmpty() 
//                || acct.Street.IsNullOrEmpty() 
//                || acct.City.IsNullOrEmpty() 
//                || acct.State.IsNullOrEmpty() 
//                || acct.Country.IsNullOrEmpty() 
//                || this.TransCardNumber.IsNullOrEmpty() 
//                || acct.LicenseNo.IsNullOrEmpty() 
//                || acct.LicenseState.IsNullOrEmpty() 
//                || !acct.DateOfBirth.HasValue) {

//            } else {

//                string cardNumber = this.TransCardNumber;
//                //TransCard.CARDHOLDER_DETAIL_RET detail = oCard.CardHolderDetail(cardNumber);

//                //just update changes even if it's the same
//                CabRideEngine.com.cabpay.CellPhoneWebService.TransCardDriverIdentification oID = new CabRideEngine.com.cabpay.CellPhoneWebService.TransCardDriverIdentification();
				
//                oID.EmployeeID = acct.TaxiPassRedeemerAccountID.ToString();

//                oID.AddressLine1 = acct.Street;
//                oID.AddressLine2 = acct.Suite;
//                oID.CardNumber = this.TransCardNumber;
//                oID.City = acct.City;
//                oID.CountryCode = acct.Country;
//                //oID.Email = this.EMail;

//                if (acct.Contact.Contains(",")) {
//                    List<string> nameList = acct.Contact.Split(',').ToList();
//                    oID.LastName = nameList[0];
//                    for (int i = 1; i < nameList.Count; i++) {
//                        if (nameList.Count - 1 == i) {
//                            oID.FirstName = nameList[i];
//                        } else {
//                            oID.MiddleName += nameList[i] + " ";
//                        }
//                    }
//                } else {
//                    List<string> nameList = acct.Contact.Split(' ').ToList();
//                    oID.FirstName = nameList[0];
//                    for (int i = 1; i < nameList.Count; i++) {
//                        if (nameList.Count - 1 == i) {
//                            oID.LastName = nameList[i];
//                        } else {
//                            oID.MiddleName += nameList[i] + " ";
//                        }
//                    }
//                }
//                oID.Phone = acct.Phone;
//                oID.PostalCode = acct.ZIPCode;
//                oID.StateProvince = acct.State;
//                if (acct.DateOfBirth.HasValue) {
//                    oID.DateOfBirth = acct.DateOfBirth.Value.ToString("yyyyMMdd");
//                }
//                //oID.IdCode = "STATE DRIVERS LICENSE";
//                oID.IdCode = "S";
//                oID.IdNumber = "123456789";
//                oID.IdCountry = "USA";  // "California"; // States.GetStatesByCountry(mPM, "USA").Find(p=>p.State.Equals(oDriver.State, StringComparison.CurrentCultureIgnoreCase); // "USA";

//                try {
//                    PayCard oPayCard = new PayCard();
//                    CabRideEngine.com.cabpay.CellPhoneWebService.Services oService = new CabRideEngine.com.cabpay.CellPhoneWebService.Services();
//                    CabRideEngine.com.cabpay.CellPhoneWebService.CARDHOLDER_IDENTIFICATION_RET oResult = oService.CardHolderIdentification(oID);

//                    if (!oResult.ERROR_NUMBER.IsNullOrEmpty() && oResult.ERROR_NUMBER != "0") {
//                        transResult.ERROR_NUMBER = "-1";
//                        transResult.ERROR_FOUND = "Y";
//                        transResult.ERROR_MESSAGE = "Error updating Redeemer Info on PayCard: " + oResult.ERROR_MESSAGE;

//                    } else {
//                        oPayCard.SetCardNumber(this.PersistenceManager, this.TransCardNumber);
//                        // set this as it isn't saved in the system yet.
//                        oPayCard.TCCardInfo.CardNumber = this.TransCardNumber;
//                        oPayCard.TCCardInfo.ComData = oResult.ComData;
//                        oPayCard.TCCardInfo.EmployeeID = oID.EmployeeID;
//                        oPayCard.TCCardInfo.CardHolderReferenceNumber = oResult.CardHolderReferenceNumber;
//                        oPayCard.TCCardInfo.FirstName = oID.FirstName;
//                        oPayCard.TCCardInfo.LastName = oID.LastName;

//                        if (oResult.ComData){

//                            // 1/27/12 ComData doesn't offer us a valid "ValidateCard" yet
//                            if (oPayCard.TCCardInfo.AdminNo != oResult.CardHolderReferenceNumber) {
//                                oPayCard.TCCardInfo.AdminNo = oResult.CardHolderReferenceNumber;
//                                this.TransCardAdminNo = TaxiPassCommon.Banking.TransCard.TransCardAdminNoCalc(oResult.ComData, oResult.CardHolderReferenceNumber);
//                                this.Save();
//                            }
//                            transResult.ACCOUNT_NO = this.TransCardAdminNo;
//                            transResult.CARD_STATUS = "ACTIVE";
//                            transResult.ACTIVATION_REQUIRED = "Y";
//                            transResult.FIRST_NAME = oID.FirstName;
//                            transResult.LAST_NAME = oID.LastName;
//                            transResult.CARDHOLDER_IDENTIFIED = "Y";
//                            transResult.CURRENT_CARD_BALANCE = "0";		
				

//                        } else {
							
////							oPayCard.SetCardNumber(this.PersistenceManager, this.TransCardNumber);

//                            string sWork;
//                            // Validate the card
//                            sWork = JsonConvert.SerializeObject(oPayCard.TCCardInfo).EncryptIceKey();
//                            sWork = oService.ValidateCard2(sWork).DecryptIceKey();
//                            transResult = JsonConvert.DeserializeObject<TransCard.VALIDATE_CARD_RET>(sWork);
//                        }

//                        if (transResult.ACTIVATION_REQUIRED.StartsWith("Y", StringComparison.CurrentCultureIgnoreCase)) {

//                            string sWork = Newtonsoft.Json.JsonConvert.SerializeObject(oPayCard.TCCardInfo).EncryptIceKey();
//                            sWork = oService.ActivateCard2(sWork).DecryptIceKey();
//                            TransCard.CARD_ACTIVATION_RET actResult = Newtonsoft.Json.JsonConvert.DeserializeObject<TransCard.CARD_ACTIVATION_RET>(sWork);

//                            if (actResult.ERROR_FOUND.StartsWith("N", StringComparison.CurrentCultureIgnoreCase)) {
//                                string sAdminNo = "";

//                                // revalidate the card
//                                if (!oResult.ComData){
//                                    sWork = JsonConvert.SerializeObject(oPayCard.TCCardInfo).EncryptIceKey();
//                                    sWork = oService.ValidateCard2(sWork).DecryptIceKey();
//                                    transResult = JsonConvert.DeserializeObject<TransCard.VALIDATE_CARD_RET>(sWork);

//                                    sAdminNo = transResult.ACCOUNT_NO;
//                                } else {

//                                    transResult.ACTIVATION_REQUIRED = "N";
//                                    sAdminNo = oResult.CardHolderReferenceNumber;
//                                }

//                                this.TransCardAdminNo = TaxiPassCommon.Banking.TransCard.TransCardAdminNoCalc(oResult.ComData, sAdminNo); 
//                            }
//                        }
//                        this.TransCardActive = true;
//                        this.Save();
						
//                    }
//                } catch (Exception ex) {
//                    transResult.ERROR_NUMBER = "-1";
//                    transResult.ERROR_FOUND = "Y";
//                    transResult.ERROR_MESSAGE = "Error update PayCard Data " + ex.Message;
//                }
//            }
//            return transResult;
//        }

//        public decimal GetPayCardBalance() {
//            try {
//                TransCardDefaults tranDef = TransCardDefaults.GetDefaults(this.PersistenceManager, this.TransCardNumber);
//                if (tranDef == null) {
//                    tranDef = TransCardDefaults.GetDefaultTransCard(this.PersistenceManager);
//                }
//                TransCard oCard = new TransCard(tranDef.GetTransCardAccess());
//                TransCard.CardInfo oCardInfo = new TransCard.CardInfo();
//                oCardInfo.CardNumber = this.TransCardNumber;
//                oCardInfo.ComData = tranDef.ComData;
//                TransCard.VALIDATE_CARD_RET cardResults = oCard.ValidateCard(oCardInfo);
//                return Convert.ToDecimal(cardResults.CURRENT_CARD_BALANCE.Replace("$", ""));
//            } catch (Exception) {
//            }
//            return 0;
//        }

		public static RedeemerPayCards GetByPayCard(PersistenceManager pPM, string pPayCard) {
			if (string.IsNullOrEmpty(pPayCard)) {
				return pPM.GetNullEntity<RedeemerPayCards>();
			}

			string sCard = pPayCard.CardNumberDisplay();

			RdbQuery qry = new RdbQuery(typeof(RedeemerPayCards), RedeemerPayCards.TransCardCardNumberEntityColumn, EntityQueryOp.EQ, sCard);
			EntityList<RedeemerPayCards> list = pPM.GetEntities<RedeemerPayCards>(qry, QueryStrategy.DataSourceThenCache);
			foreach (RedeemerPayCards rec in list) {
				if (rec.TransCardNumber.Equals(pPayCard)) {
					return rec;
				}
			}

			return pPM.GetNullEntity<RedeemerPayCards>();
		}

		public static RedeemerPayCards GetByPayCardAdminNo(PersistenceManager pPM, string pPayCard) {
			if (string.IsNullOrEmpty(pPayCard)) {
				return pPM.GetNullEntity<RedeemerPayCards>();
			}

			RdbQuery qry = new RdbQuery(typeof(RedeemerPayCards), RedeemerPayCards.TransCardAdminNoEntityColumn, EntityQueryOp.EQ, pPayCard);
			return pPM.GetEntity<RedeemerPayCards>(qry, QueryStrategy.DataSourceThenCache);
		}
	}

	#region EntityPropertyDescriptors.RedeemerPayCardsPropertyDescriptor
	////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
	////    For example: 
	////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
	//public static partial class EntityPropertyDescriptors {
	//	public partial class RedeemerPayCardsPropertyDescriptor : BaseEntityPropertyDescriptor {
	//    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
	//    public AdaptedPropertyDescriptor Age {
	//      get { return this.Get("Age"); }
	//    }
	//  }
	//} 
	#endregion

}