﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Dapper;

namespace CabRideEngineDapper {
	public partial class FraudDriverLicenses {

		public static FraudDriverLicenses GetLicense(SqlConnection pPM, string pLicenseNo) {
			string sql = @"SELECT TOP 1 * FROM FraudDriverLicenses 
							WHERE LicenseNo = @LicenseNo";

			return pPM.Get<FraudDriverLicenses>(sql, new { LicenseNo = pLicenseNo });
		}

		public static FraudDriverLicenses GetLicense(SqlConnection pPM, string pLicenseNo, string pState) {
			string sql = @"SELECT TOP 1 * FROM FraudDriverLicenses 
							WHERE LicenseNo = @LicenseNo AND LicenseState = @State";

			return pPM.Get<FraudDriverLicenses>(sql, new { LicenseNo = pLicenseNo, State = pState });
		}
	}
}
