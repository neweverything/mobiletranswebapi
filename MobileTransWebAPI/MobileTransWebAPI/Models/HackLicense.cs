using System;

namespace MobileTransWebAPI.Models {

	public partial class HackLicense {

		//public CabRideEngineEF.HackLicenseInfo HackLicenseRec { get; set; }

		public string LicenseClass { get; set; }

		public bool LicenseValidated { get; set; }
		public string LicenseNo { get; set; }
		//public int LicenseExpiryMonth { get; set; }
		//public int LicenseExpiryYear { get; set; }
		public DateTime LicenseExpiry { get; set; }

		public string LicenseImageURL { get; set; }

		public string State { get; set; }

		// allows admin user to upload image and validate on a single post
		public bool AdminOverride { get; set; }

		public string LicenseImageBase64 { get; set; }
		public long DriverID { get; set; }
		public long AffiliateDriverID { get; set; }
		public long AffiliateID { get; set; }

		public string ErrorMsg { get; set; }
		public int ErrorNo { get; set; }

		public string RequestKey { get; set; }


	}
}