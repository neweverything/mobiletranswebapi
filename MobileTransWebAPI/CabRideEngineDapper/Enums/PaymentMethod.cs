﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CabRideEngineDapper.Utils {
	public enum PaymentMethod {
		Fleet,
		Redeemer,
		PayCard,
		DirectDeposit
	}
}
