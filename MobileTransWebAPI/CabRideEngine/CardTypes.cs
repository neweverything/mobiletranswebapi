﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

using System.Text.RegularExpressions;

using Csla.Validation;

namespace CabRideEngine {
	[Serializable]
	public sealed class CardTypes : CardTypesDataRow {

		#region Constructors -- DO NOT MODIFY
		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new'
		private CardTypes()
			: this(null) {
		}

		// Typed DataSet constructor (needed by framework) - DO NOT REMOVE
		public CardTypes(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static CardTypes Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      CardTypes aCardTypes = pManager.CreateEntity<CardTypes>();
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aCardTypes, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aCardTypes.AddToManager();
		//      return aCardTypes;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		// Add additional logic to your business object here...
		public static CardTypes Create(PersistenceManager pManager) {

			CardTypes oType = (CardTypes)pManager.CreateEntity(typeof(CardTypes));

			//pManager.GenerateId(oType, CardTypes.CardTypeIDEntityColumn);
			oType.AddToManager();
			return oType;
		}

		public override string CardType {
			get {
				return base.CardType;
			}
			set {
				base.CardType = value;
			}
		}

		protected override void AddRules(RuleList pList) {
			PropertyRequiredRule.AddToList(pList, CardTypeEntityColumn.ColumnName);
			pList.Add(IsCardTypeUnique, CardTypeEntityColumn.ColumnName);
		}

		private bool IsCardTypeUnique(object pTarget, RuleArgs e) {
			CardTypes oType = (CardTypes)pTarget;

			RdbQuery oQry = new RdbQuery(typeof(CardTypes), CardTypes.CardTypeEntityColumn, EntityQueryOp.EQ, oType.CardType);
			oQry.AddClause(CardTypes.CardTypeIDEntityColumn, EntityQueryOp.NE, oType.CardTypeID);
			EntityList<CardTypes> oCardTypes = this.PersistenceManager.GetEntities<CardTypes>(oQry);
			if (oCardTypes.Count > 0) {
				e.Description = "Card Type must be unique!";
				return false;
			}
			return true;
		}

		[BindingBrowsable(false)]
		public override string EntityInstanceDisplayName {
			get {
				return CardType;
			}
		}

		protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
			if (IsDeserializing)
				return;
			base.OnColumnChanging(pArgs);
			//DoAudit(pArgs);
		}

		private void DoAudit(DataColumnChangeEventArgs pArgs) {
			if (!this.PersistenceManager.IsClient) {
				return;
			}
			if (this.RowState == DataRowState.Added) {
				return;
			}
			if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
				AuditRecord = CardTypesAudit.Create(this.PersistenceManager, this);
			}
			//AuditRecord[pArgs.Column.ColumnName] = pArgs.ProposedValue;
		}

		public static String GetCardType(PersistenceManager pPM, string pCardNo) {
			EntityList<CardTypes> oTypes = pPM.GetEntities<CardTypes>();

			foreach (CardTypes oType in oTypes) {
				if (Regex.IsMatch(pCardNo, oType.Validation)) {
					return oType.CardType;
				}
			}
			return "";
		}

		public static CardTypes GetCardTypeByVtsID(PersistenceManager pPM, string pVtsID) {
			RdbQuery qry = new RdbQuery(typeof(CardTypes), TaxiTronicCardTypeIDEntityColumn, EntityQueryOp.EQ, pVtsID);
			return pPM.GetEntity<CardTypes>(qry);
		}

		// snelson 7/28/2006; added -->
		public static EntityList<CardTypes> GetAllCardTypes(PersistenceManager pPM) {
			EntityList<CardTypes> oTypes = pPM.GetEntities<CardTypes>();
			oTypes.ApplySort(CardTypes.CardTypeEntityColumn.ColumnName, System.ComponentModel.ListSortDirection.Ascending, false);
			return oTypes;
		}
		// <-- snelson 7/28/2006

		public static long GetCardTypeId(PersistenceManager pPM, string pCardTypeName) {
			EntityList<CabRideEngine.CardTypes> oTypes = pPM.GetEntities<CabRideEngine.CardTypes>();
			foreach (CabRideEngine.CardTypes oType in oTypes) {
				if (oType.CardType.ToUpper() == pCardTypeName.ToUpper()) {
					return oType.CardTypeID;
				}
			}
			return 0;
		}

	}

}
