﻿using System;

namespace TaxiPassCommon.Banking {
    public class ProcessorResponse {

        public long VerisignAccountID { get; set; }
        public string MerchantID { get; set; }

        public string Result { get; set; }
        public string Reference { get; set; }
        public string Message { get; set; }
        public string TransType { get; set; }
        public DateTime TransDate { get; set; }
        public string AuthCode { get; set; }

        public string AVS { get; set; }
        public string AVSAddr { get; set; }
        public string AVSZip { get; set; }
        public string AVSResultCode { get; set; }

        public string CVV2Match { get; set; }
        public CardProcessors CreditCardProcessor { get; set; }

        public decimal Amount { get; set; }
        public bool TestCard { get; set; }

        public string BillingDescriptor { get; set; }
        //public string GatewayResponse { get; set; }
    }
}
