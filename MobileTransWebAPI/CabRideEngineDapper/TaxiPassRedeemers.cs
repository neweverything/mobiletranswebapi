﻿using CabRideEngineDapper.Utils;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using TaxiPassCommon;

namespace CabRideEngineDapper {
    public partial class TaxiPassRedeemers {

        #region Related Tables
        private TaxiPassRedeemerAccounts mTaxiPassRedeemerAccounts;
        public TaxiPassRedeemerAccounts TaxiPassRedeemerAccounts {
            get {
                if (mTaxiPassRedeemerAccounts == null) {
                    mTaxiPassRedeemerAccounts = CabRideEngineDapper.TaxiPassRedeemerAccounts.GetAccount(this.TaxiPassRedeemerAccountID);
                }
                return mTaxiPassRedeemerAccounts;
            }
            set {
                mTaxiPassRedeemerAccounts = value;
            }
        }

        #endregion

        #region Custom Fields
        [Editable(false)]
        public string PasswordDecrypted {
            get {
                Encryption oEncrypt = new Encryption();
                string sPwd = oEncrypt.Decrypt(base.Password, this.Salt);
                return sPwd;
            }
        }

        [Editable(false)]
        public string FormattedPhone {
            get {
                return this.Phone.FormattedPhoneNumber();
            }
        }

        [Editable(false)]
        public string SmartWAPLink {
            get {
                return "http://driverpay.1800cabride.com/default.aspx?z=" + this.Phone.EncryptIceKey();
            }
        }

        [Editable(false)]
        public string i335WAPLinkCash {
            get {
                return "http://creditcardtaxi.com/cellphonewebservice1/WAPRedemptionCash.aspx?z=" + this.Phone.EncryptIceKey();
            }
        }


        [Editable(false)]
        public RedeemerShift CurrentShift {
            get {
                return RedeemerShift.GetLastShiftByRedeemer(this);
            }
        }

        #endregion


        public static TaxiPassRedeemers GetRedeemer(long? pRedeemerID) {
            if (!pRedeemerID.HasValue) {
                return TaxiPassRedeemers.GetNullEntity();
            }
            string sql = "SELECT * FROM TaxiPassRedeemers WHERE TaxiPassRedeemerID = @TaxiPassRedeemerID";
            using (var conn = SqlHelper.OpenSqlConnection()) {
                return conn.Query<TaxiPassRedeemers>(sql, new { TaxiPassRedeemerID = pRedeemerID }).DefaultIfEmpty(TaxiPassRedeemers.GetNullEntity()).FirstOrDefault();
            }
        }

        public static TaxiPassRedeemers GetRedeemer(string pEMail) {
            using (var conn = SqlHelper.OpenSqlConnection()) {
                return GetRedeemer(conn, pEMail);
            }
        }

        public static TaxiPassRedeemers GetRedeemer(SqlConnection pConn, string pEMail) {
            string sql = "SELECT * FROM TaxiPassRedeemers WHERE Email = @Email";
            return pConn.Query<TaxiPassRedeemers>(sql, new { Email = pEMail }).DefaultIfEmpty(TaxiPassRedeemers.GetNullEntity()).FirstOrDefault();
        }

        public static TaxiPassRedeemers GetRedeemer(SqlConnection pConn, long pTaxiPassRedeemerID) {
            string sql = "SELECT * FROM TaxiPassRedeemers WHERE TaxiPassRedeemerID = @TaxiPassRedeemerID";
            return pConn.Get<TaxiPassRedeemers>(sql, new { TaxiPassRedeemerID = pTaxiPassRedeemerID });
        }


        public static TaxiPassRedeemers GetRedeemerByPhone(string pPhone) {
            using (var conn = SqlHelper.OpenSqlConnection()) {
                return GetRedeemerByPhone(conn, pPhone);
            }
        }

        public static TaxiPassRedeemers GetRedeemerByPhone(SqlConnection pConn, string pPhone) {
            string sql = "SELECT * FROM TaxiPassRedeemers WHERE Phone = @Phone";
            return pConn.Query<TaxiPassRedeemers>(sql, new { Phone = pPhone }).DefaultIfEmpty(TaxiPassRedeemers.GetNullEntity()).FirstOrDefault();
        }

        public static List<TaxiPassRedeemers> GetRedeemersByPhone(SqlConnection pConn, string pPhone) {
            string sql = "SELECT * FROM TaxiPassRedeemers WHERE Phone = @Phone";
            using (var conn = SqlHelper.OpenSqlConnection()) {
                return conn.Query<TaxiPassRedeemers>(sql, new { Phone = pPhone }).ToList();
            }

        }

        public static TaxiPassRedeemers GetRedeemerByIDOrPhone(SqlConnection pConn, long pTaxiPassRedeemerAccountID, string pID) {
            string sql = @"SELECT * FROM TaxiPassRedeemers 
							WHERE TaxiPassRedeemerAccountID = @TaxiPassRedeemerAccountID
									AND ActiveEntity = 1
									AND (Phone = @ID OR EmployeeNo = @ID)";

            return pConn.Query<TaxiPassRedeemers>(sql, new { TaxiPassRedeemerAccountID = pTaxiPassRedeemerAccountID, ID = pID }).DefaultIfEmpty(TaxiPassRedeemers.GetNullEntity()).FirstOrDefault();
        }

        public static TaxiPassRedeemers GetRedeemer(SqlConnection pConn, TaxiPassRedeemerAccounts pTaxiPassRedeemerAccount, string pEMail) {
            string sql = "SELECT * FROM TaxiPassRedeemers WHERE Email = @EMail AND TaxiPassRedeemerAccountID = @TaxiPassRedeemerAccountID";
            return pConn.Query<TaxiPassRedeemers>(sql, new { EMail = pEMail, TaxiPassRedeemerAccountID = pTaxiPassRedeemerAccount }).DefaultIfEmpty(TaxiPassRedeemers.GetNullEntity()).FirstOrDefault();
        }

        // *****************************************************************************
        // returns true if either the Redeemer or his Account is set to PayBeforeMatch
        // *****************************************************************************
        public bool CanUseRedemptionApp() {
            return (this.UseRedemptionApp || this.TaxiPassRedeemerAccounts.UseRedemptionApp);
        }

        public static List<TaxiPassRedeemers> GetRedeemers(CabRideEngineDapper.TaxiPassRedeemerAccounts pTaxiPassRedeemerAccounts) {
            string sql = @"SELECT * FROM TaxiPassRedeemers 
							WHERE TaxiPassRedeemerAccountID = @TaxiPassRedeemerAccountID
							Order By Name";
            using (var conn = SqlHelper.OpenSqlConnection()) {
                return conn.Query<TaxiPassRedeemers>(sql, new { TaxiPassRedeemerAccountID = pTaxiPassRedeemerAccounts.TaxiPassRedeemerAccountID }).ToList();
            }
        }

        public static List<TaxiPassRedeemers> GetActiveRedeemers(TaxiPassRedeemerAccounts pTaxiPassRedeemerAccount) {
            string sql = @"SELECT * FROM TaxiPassRedeemers 
							WHERE TaxiPassRedeemerAccountID = @TaxiPassRedeemerAccountID
									AND Active = 1
							Order By Name";
            using (var conn = SqlHelper.OpenSqlConnection()) {
                return conn.Query<TaxiPassRedeemers>(sql, new { TaxiPassRedeemerAccountID = pTaxiPassRedeemerAccount.TaxiPassRedeemerAccountID }).ToList();
            }
        }


        public static List<TaxiPassRedeemers> GetActiveRedeemers(long pTaxiPassRedeemerAccountID) {
            string sql = @"SELECT * FROM TaxiPassRedeemers 
							WHERE TaxiPassRedeemerAccountID = @TaxiPassRedeemerAccountID
									AND Active = 1
							Order By Name";
            using (var conn = SqlHelper.OpenSqlConnection()) {
                return conn.Query<TaxiPassRedeemers>(sql, new { TaxiPassRedeemerAccountID = pTaxiPassRedeemerAccountID }).ToList();
            }
        }

        public static List<TaxiPassRedeemers> GetRedeemersThatRedeemedVouchers(TaxiPassRedeemerAccounts pRedeemerAccount, DateTime pDate) {
            //StringBuilder sql = new StringBuilder();
            //sql.Append("execute usp_TaxiPassRedeemers_GetRedeemersThatRedeemedVouchers ");
            //sql.Append(pRedeemerAccount.TaxiPassRedeemerAccountID.ToString("F0"));
            //sql.Append(", '");
            //sql.Append(pDate.ToString("MM/dd/yyyy"));
            //sql.Append("'");

            DynamicParameters param = new DynamicParameters();
            param.Add("TaxiPassRedeemerAccountID", pRedeemerAccount.TaxiPassRedeemerAccountID);
            param.Add("ScanDate", pDate.Date);

            //PassthruRdbQuery qry = new PassthruRdbQuery(typeof(TaxiPassRedeemers), sql.ToString());
            //return pRedeemerAccount.PersistenceManager.GetEntities<TaxiPassRedeemers>(qry);
            using (var conn = SqlHelper.OpenSqlConnection()) {
                return conn.Query<TaxiPassRedeemers>("usp_TaxiPassRedeemers_GetRedeemersThatRedeemedVouchers", param, commandType: CommandType.StoredProcedure).ToList();
            }
        }

        public static List<TaxiPassRedeemers> GetShiftManagers(CabRideEngineDapper.TaxiPassRedeemerAccounts pTaxiPassRedeemerAccounts) {
            string sql = @"SELECT * FROM TaxiPassRedeemers 
						   WHERE TaxiPassRedeemerAccountID = @TaxiPassRedeemerAccountID
								AND ShiftManager = 1
							Order By Name";
            using (var conn = SqlHelper.OpenSqlConnection()) {
                return conn.Query<TaxiPassRedeemers>(sql, new { TaxiPassRedeemerAccountID = pTaxiPassRedeemerAccounts.TaxiPassRedeemerAccountID }).ToList();
            }
        }



    }
}
