using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using IdeaBlade.Util;

namespace CabRideEngine {
    public class RoutingTypes {
        private string mCode;
        private string mDescription;

        private bool mIsNullEntity = false;

        private static BindableList<RoutingTypes> msEntities;
        private static RoutingTypes msNullEntity;

        public static string Airport {
            get {
                return "AA";
            }
        }

        public static string Address {
            get {
                return "A";
            }
        }

        public static string Hotel {
            get {
                return "H";
            }
        }


        /// <summary>Class Constructor (Private)</summary>
        static RoutingTypes() {

            // Create the null entity
            msNullEntity = new RoutingTypes("", "");
            msNullEntity.mIsNullEntity = true;

            // Populate list of Routing Types.
            msEntities = new BindableList<RoutingTypes>();
            msEntities.Add(new RoutingTypes(Address, "Address"));
            msEntities.Add(new RoutingTypes(Airport, "Airport"));
            msEntities.Add(new RoutingTypes(Hotel, "Hotel"));
        }

        /// <summary>Constructor (Private)</summary>
        private RoutingTypes(string pCode, string pDescription) {
            mCode = pCode;
            mDescription = pDescription;
        }

        /// <summary>Get the Routing Types null entity.</summary>
        public static RoutingTypes NullEntity {
            get {
                return msNullEntity;
            }
        }

        /// <summary>Get the list of all Routing Types.</summary>
        public static BindableList<RoutingTypes> GetAll() {
            return msEntities;
        }

        /// <summary>Get the list of all Routing Types.</summary>
        public static RoutingTypes GetById(string pId) {
            if (String.IsNullOrEmpty(pId))
                return NullEntity;
            foreach (RoutingTypes oRoutingType in msEntities) {
                if (oRoutingType.Code == pId)
                    return oRoutingType;
            }
            return NullEntity;
        }

        /// <summary>Return true if this is the null entity</summary>
        [BindingBrowsable(false)]
        public bool IsNullEntity {
            get {
                return mIsNullEntity;
            }
        }

        /// <summary>Get the Routing Type</summary>
        public string Code {
            get {
                return mCode;
            }
        }

        public string Description {
            get {
                return mDescription;
            }
        }

        //public static DataTable GetRoutingTypes() {
        //    DataTable oTable = new DataTable("Routing_Types");
        //    oTable.Columns.Add("Code", typeof(string));
        //    oTable.Columns.Add("Name", typeof(string));

        //    DataRow oRow = oTable.NewRow();
        //    oRow["Code"] = "";
        //    oRow["Name"] = "";
        //    oTable.Rows.Add(oRow);

        //    oRow = oTable.NewRow();
        //    oRow["Code"] = "A";
        //    oRow["Name"] = "Address";
        //    oTable.Rows.Add(oRow);

        //    oRow = oTable.NewRow();
        //    oRow["Code"] = "AA";
        //    oRow["Name"] = "Airport Arrival";
        //    oTable.Rows.Add(oRow);

        //    oRow = oTable.NewRow();
        //    oRow["Code"] = "AD";
        //    oRow["Name"] = "Airport Departure";
        //    oTable.Rows.Add(oRow);

        //    return oTable;
        //}


    }
}
