using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Runtime.Serialization;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

namespace CabRideEngine {
 
  #region HitsDataTable
  /// <summary>
  /// The generated <see cref="T:IdeaBlade.Persistence.EntityTable"/> class  
  /// containing all <see cref="Hits"/> objects.
  /// </summary>
  /// <remarks>
  /// ****************************************************
  /// <para>** DO NOT MODIFY THIS CLASS - AutoGenerated Code</para>
  /// <para>** Any changes made to this code will be lost</para>
  /// <para>** the next time this code is regenerated.</para>
  /// <para>**</para>
  /// <para>** Generated from:   EntityBaseclass.template</para>
  /// <para>** Last modified:    2012-03-15T13:21:08.4965138-07:00</para>
  /// <para>** Template version: 3.110127</para>
  /// ****************************************************
  /// </remarks>
  [BindingBrowsable(false)]
  [Serializable]
  public partial class HitsDataTable : BaseEntityDataTable, 
     IdeaBlade.Persistence.Rdb.IRdbTable {
  
    /// <summary>
    /// Initializes a new instance of the HitsDataTable class with no arguments.
    /// </summary>
    public HitsDataTable() {}

    /// <summary>
    /// Initializes a new instance of the HitsDataTable class 
    /// with the <see cref="System.Runtime.Serialization.SerializationInfo"/> 
    /// and the <see cref="System.Runtime.Serialization.StreamingContext"/>.
    /// </summary>
    /// <param name="pInfo">The data needed to serialize or deserialize an object.</param>
    /// <param name="pContext">The source and destination of a given serialized stream.</param>
    protected HitsDataTable(SerializationInfo pInfo, StreamingContext pContext) :
      base(pInfo, pContext) {}

    /// <summary>
    /// Creates a new instance of the <see cref="HitsDataRow"/>.
    /// Constructs a row from the builder.
    /// <para>** For internal use only. **</para>
    /// </summary>
    /// <param name="pRowBuilder"></param>
    /// <returns>The <see cref="HitsDataRow"/>.</returns>
    protected override DataRow NewRowFromBuilder(DataRowBuilder pRowBuilder) {
      return new Hits(pRowBuilder);
    }

    /// <summary>Returns the <see cref="System.Type"/> of <see cref="Hits"/>.</summary>
    protected override Type GetRowType() {
      return typeof(Hits);
    }
    
    /// <summary>
    /// Gets the name of the <see cref="T:IdeaBlade.Rdb.RdbKey"/> or 
    /// <see cref="T:IdeaBlade.Persistence.WS.WsKey"/> defined in <b>IdeaBlade.ibconfig</b>. 
    /// </summary>
    /// <remarks>For this <see cref="Hits"/>, the DataSourceKeyName="default".</remarks>
    public override String DataSourceKeyName {
      get { return @"default"; }
    }

    #region Column properties
    //**************************************
    //* Column properties
    //**************************************
    /// <summary>Gets the HitID <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn HitIDColumn {
      get {
        if (mHitIDColumn!=null) return mHitIDColumn;
        mHitIDColumn = GetColumn("HitID", true);
        return mHitIDColumn;
      }
    }
    private DataColumn mHitIDColumn;
    
    /// <summary>Gets the Page <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn PageColumn {
      get {
        if (mPageColumn!=null) return mPageColumn;
        mPageColumn = GetColumn("Page", true);
        return mPageColumn;
      }
    }
    private DataColumn mPageColumn;
    
    /// <summary>Gets the Count <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn CountColumn {
      get {
        if (mCountColumn!=null) return mCountColumn;
        mCountColumn = GetColumn("Count", true);
        return mCountColumn;
      }
    }
    private DataColumn mCountColumn;
    
    /// <summary>Gets the Referral <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn ReferralColumn {
      get {
        if (mReferralColumn!=null) return mReferralColumn;
        mReferralColumn = GetColumn("Referral", true);
        return mReferralColumn;
      }
    }
    private DataColumn mReferralColumn;
    
    /// <summary>Gets the Market <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn MarketColumn {
      get {
        if (mMarketColumn!=null) return mMarketColumn;
        mMarketColumn = GetColumn("Market", true);
        return mMarketColumn;
      }
    }
    private DataColumn mMarketColumn;
    
    /// <summary>Gets the ClickThru <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn ClickThruColumn {
      get {
        if (mClickThruColumn!=null) return mClickThruColumn;
        mClickThruColumn = GetColumn("ClickThru", true);
        return mClickThruColumn;
      }
    }
    private DataColumn mClickThruColumn;
    
    #endregion    

    /// <summary>Creates the schema mapping information for this HitsDataTable.</summary>
    /// <returns>
    /// The <see cref="T:IdeaBlade.Persistence.TableMappingInfo"/> for this HitsDataTable.
    /// </returns>
    protected override TableMappingInfo CreateTableMappingInfo() {
      RdbTableMappingInfo mappingInfo = new RdbTableMappingInfo(this);
      mappingInfo.SourceOwnerName = "dbo"; 
      mappingInfo.SourceTableName = "Hits"; 
      mappingInfo.ConcurrencyColumnName = ""; 
      // ColumnSelectionOption: selectAllColumns
      mappingInfo.SourceColumnNames = "*"; 
   
      DataColumnMappingCollection columnMappings = mappingInfo.TableMapping.ColumnMappings;
      columnMappings.Clear();
      columnMappings.Add("HitID", "HitID");
      columnMappings.Add("Page", "Page");
      columnMappings.Add("Count", "Count");
      columnMappings.Add("Referral", "Referral");
      columnMappings.Add("Market", "Market");
      columnMappings.Add("ClickThru", "ClickThru");
      return mappingInfo;
    }
  
    /// <summary>** For internal use only. **</summary>
    protected override void UpdateTableMappingInfo() {
      base.UpdateTableMappingInfo();
    }

    /// <summary>
    /// Initializes Hits <see cref="System.Data.DataColumn"/> properties.
    /// </summary>
    public override void InitColumnProperties() {
      base.InitColumnProperties();

      HitIDColumn.Caption = "HitID";
      PageColumn.Caption = "Page";
      CountColumn.Caption = "Count";
      ReferralColumn.Caption = "Referral";
      MarketColumn.Caption = "Market";
      ClickThruColumn.Caption = "ClickThru";
    }
  }
  #endregion
  
  #region HitsDataRow
  /// <summary>
  /// The generated base class for <see cref="Hits"/>.
  /// </summary>
  /// <remarks>
  /// <para>
  /// It represents a row of persistable data in the <see cref="HitsDataTable"/>
  /// and derives from <see cref="T:IdeaBlade.Persistence.Entity"/>.
	/// </para>
  /// ****************************************************
  /// <para>** DO NOT MODIFY THIS CLASS - AutoGenerated Code</para>
  /// <para>** Any changes made to this code will be lost</para>
  /// <para>** the next time this code is regenerated.</para>
  /// <para>**</para>
  /// <para>** Generated from:   EntityBaseclass.template</para>
  /// <para>** Last modified:    2012-03-15T13:21:08.4965138-07:00</para>
  /// <para>** Template version: 3.110127</para>
  /// ****************************************************
  /// </remarks>  
  [Serializable]
  public abstract partial class HitsDataRow : BaseEntity, 
     IdeaBlade.Persistence.Rdb.IRdbEntity {
  
    /// <summary>
    /// Initializes a new instance of the HitsDataRow.
    /// Constructs a row from the builder.
    /// <para>** For internal use only. **</para>
    /// </summary>
    /// <param name="pRowBuilder"></param>
    protected HitsDataRow(DataRowBuilder pRowBuilder)
      : base(pRowBuilder) {
    }

    /// <summary>Gets the <see cref="HitsDataTable"/>.</summary>
    [BindingBrowsable(false)]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public new HitsDataTable TypedTable {
      get { return (HitsDataTable) base.Table; }
    }

    /// <summary>Returns the <see cref="System.Type"/> of <see cref="HitsDataTable"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public static Type GetTableType() {
      return typeof(HitsDataTable);
    }

    /// <summary>
    /// Gets the SQL 'where' clause used to restrict which 
    /// database records are accessible via this entity. 
    /// </summary>
    /// <remarks>
    /// The default property value is defined in the Current Class 
    /// Properties tab in the <b>IdeaBlade DevForce Object Mapping Tool</b>.
    /// <para>
    /// The 'where' clause will be used, in addition to any other 
    /// query-specific criteria, in every retrieval of entities of this type.
    /// </para>
    /// </remarks>
    [BindingBrowsable(false)]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public virtual string SqlWhereClause {
      get { return ""; }
    }
    
    /// <summary>
    /// Determines whether to use the 'distinct' keyword when 
    /// generating the SQL select clause for objects of this type.
    /// </summary>
    /// <remarks>Defaults to false, but may be overridden in subclasses.</remarks>
    [BindingBrowsable(false)]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public virtual bool ForceSqlDistinct {
      get { return false; }
    }
    

    #region Enums
    //**************************************
    //* Enums
    //**************************************
    #endregion
	
    #region Relation properties
    //**************************************
    //* Relation properties
    //**************************************
    #endregion
    
    #region EntityColumn definitions
    //**************************************
    //* Static EntityColumn definitions
    //**************************************
    /// <summary>The HitID <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn HitIDEntityColumn =
      new EntityColumn(typeof(Hits), "HitID", typeof(System.Int64), false, true, true, DataSourceAccessType.ReadWrite);      
    /// <summary>The Page <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn PageEntityColumn =
      new EntityColumn(typeof(Hits), "Page", typeof(System.String), false, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The Count <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn CountEntityColumn =
      new EntityColumn(typeof(Hits), "Count", typeof(System.Int64), false, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The Referral <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn ReferralEntityColumn =
      new EntityColumn(typeof(Hits), "Referral", typeof(System.String), true, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The Market <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn MarketEntityColumn =
      new EntityColumn(typeof(Hits), "Market", typeof(System.String), true, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The ClickThru <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn ClickThruEntityColumn =
      new EntityColumn(typeof(Hits), "ClickThru", typeof(System.String), true, false, false, DataSourceAccessType.ReadWrite);      
    #endregion
    
    #region Properties
    //**************************************
    //* HitID methods
    //**************************************
    /// <summary>Gets the HitID <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn HitIDColumn {
      get { return TypedTable.HitIDColumn; }
    }

    /// <summary>Gets the HitID.</summary>
    [DBDataType(typeof(System.Int64))]
    public virtual System.Int64 HitID {
      get { 
        System.Int64 result_;
        if (GetInterceptor<System.Int64>("HitID", GetHitIDImpl, out result_)) return result_;
        return GetHitIDImpl();
      }
    }
    private System.Int64 GetHitIDImpl() {
      return (System.Int64) GetColumnValue(HitIDColumn, typeof(System.Int64), false); 
    }
    
    //**************************************
    //* Page methods
    //**************************************
    /// <summary>Gets the Page <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn PageColumn {
      get { return TypedTable.PageColumn; }
    }

    /// <summary>Gets or sets the Page.</summary>
    [MaxTextLength(20)]
    [DBDataType(typeof(System.String))]
    public virtual System.String Page {
      get { 
        System.String result_;
        if (GetInterceptor<System.String>("Page", GetPageImpl, out result_)) return result_;
        return GetPageImpl();
      }
      set { 
          if (!SetInterceptor<System.String>("Page", value, SetPageImpl)) {
            SetPageImpl(value);
          }  
      }    
    }
    private System.String GetPageImpl() {
      return (System.String) GetColumnValue(PageColumn, typeof(System.String), false); 
    }
    private void SetPageImpl(System.String value) {
      SetColumnValue(PageColumn, value);
    }
    
    //**************************************
    //* Count methods
    //**************************************
    /// <summary>Gets the Count <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn CountColumn {
      get { return TypedTable.CountColumn; }
    }

    /// <summary>Gets or sets the Count.</summary>
    [DBDataType(typeof(System.Int64))]
    public virtual System.Int64 Count {
      get { 
        System.Int64 result_;
        if (GetInterceptor<System.Int64>("Count", GetCountImpl, out result_)) return result_;
        return GetCountImpl();
      }
      set { 
          if (!SetInterceptor<System.Int64>("Count", value, SetCountImpl)) {
            SetCountImpl(value);
          }  
      }    
    }
    private System.Int64 GetCountImpl() {
      return (System.Int64) GetColumnValue(CountColumn, typeof(System.Int64), false); 
    }
    private void SetCountImpl(System.Int64 value) {
      SetColumnValue(CountColumn, value);
    }
    
    //**************************************
    //* Referral methods
    //**************************************
    /// <summary>Gets the Referral <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn ReferralColumn {
      get { return TypedTable.ReferralColumn; }
    }

    /// <summary>Gets or sets the Referral.</summary>
    [MaxTextLength(20)]
    [DBDataType(typeof(System.String))]
    public virtual System.String Referral {
      get { 
        System.String result_;
        if (GetInterceptor<System.String>("Referral", GetReferralImpl, out result_)) return result_;
        return GetReferralImpl();
      }
      set { 
          if (!SetInterceptor<System.String>("Referral", value, SetReferralImpl)) {
            SetReferralImpl(value);
          }  
      }    
    }
    private System.String GetReferralImpl() {
      return (System.String) GetColumnValue(ReferralColumn, typeof(System.String), true); 
    }
    private void SetReferralImpl(System.String value) {
      SetColumnValue(ReferralColumn, value);
    }
    
    //**************************************
    //* Market methods
    //**************************************
    /// <summary>Gets the Market <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn MarketColumn {
      get { return TypedTable.MarketColumn; }
    }

    /// <summary>Gets or sets the Market.</summary>
    [MaxTextLength(20)]
    [DBDataType(typeof(System.String))]
    public virtual System.String Market {
      get { 
        System.String result_;
        if (GetInterceptor<System.String>("Market", GetMarketImpl, out result_)) return result_;
        return GetMarketImpl();
      }
      set { 
          if (!SetInterceptor<System.String>("Market", value, SetMarketImpl)) {
            SetMarketImpl(value);
          }  
      }    
    }
    private System.String GetMarketImpl() {
      return (System.String) GetColumnValue(MarketColumn, typeof(System.String), true); 
    }
    private void SetMarketImpl(System.String value) {
      SetColumnValue(MarketColumn, value);
    }
    
    //**************************************
    //* ClickThru methods
    //**************************************
    /// <summary>Gets the ClickThru <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn ClickThruColumn {
      get { return TypedTable.ClickThruColumn; }
    }

    /// <summary>Gets or sets the ClickThru.</summary>
    [MaxTextLength(50)]
    [DBDataType(typeof(System.String))]
    public virtual System.String ClickThru {
      get { 
        System.String result_;
        if (GetInterceptor<System.String>("ClickThru", GetClickThruImpl, out result_)) return result_;
        return GetClickThruImpl();
      }
      set { 
          if (!SetInterceptor<System.String>("ClickThru", value, SetClickThruImpl)) {
            SetClickThruImpl(value);
          }  
      }    
    }
    private System.String GetClickThruImpl() {
      return (System.String) GetColumnValue(ClickThruColumn, typeof(System.String), true); 
    }
    private void SetClickThruImpl(System.String value) {
      SetColumnValue(ClickThruColumn, value);
    }
    
    #endregion

  }
  #endregion

  #region EntityPropertyDescriptors.HitsPropertyDescriptor
  public static partial class EntityPropertyDescriptors {

    /// <summary>
    /// Gets the HitsPropertyDescriptor for <see cref="Hits"/>.
    /// </summary>
    public static HitsPropertyDescriptor Hits {
      get { return HitsPropertyDescriptor.Instance; }
    }
    
    /// <summary>
    /// The generated class containing <see cref="Hits"/> PropertyDescriptors.
    /// </summary>
    public partial class HitsPropertyDescriptor : BaseEntityPropertyDescriptor {
	
			/// <summary>
      /// Initializes a new instance of the HitsPropertyDescriptor class. 
      /// </summary>
      /// <param name="pParentDescriptor">The parent <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/></param>
      /// <param name="pPropertyPath">The type of the property value</param>
			public HitsPropertyDescriptor(AdaptedPropertyDescriptor pParentDescriptor, String pPropertyPath)
        : base(pParentDescriptor, pPropertyPath) {}
		
      /// <summary>
      /// Initializes a new instance of the HitsPropertyDescriptor class. 
      /// </summary>
      /// <param name="pComponentType">The type of object to which the property is bound</param>
      protected HitsPropertyDescriptor(Type pComponentType)
        : base(pComponentType) {}

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for HitID.</summary>
      public AdaptedPropertyDescriptor HitID {
        get { return Get("HitID"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Page.</summary>
      public AdaptedPropertyDescriptor Page {
        get { return Get("Page"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Count.</summary>
      public AdaptedPropertyDescriptor Count {
        get { return Get("Count"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Referral.</summary>
      public AdaptedPropertyDescriptor Referral {
        get { return Get("Referral"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Market.</summary>
      public AdaptedPropertyDescriptor Market {
        get { return Get("Market"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for ClickThru.</summary>
      public AdaptedPropertyDescriptor ClickThru {
        get { return Get("ClickThru"); }
      }

     internal new static HitsPropertyDescriptor Instance = new HitsPropertyDescriptor(typeof(Hits));
    }
  }
  #endregion
  
}
