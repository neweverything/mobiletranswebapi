﻿using MobileTransWebAPI.Models;
using Newtonsoft.Json;
using System;
using System.Web.Http;
using TaxiPassCommon;

namespace MobileTransWebAPI.Controllers {
    public class RedeemerLoginController : ApiController {


        public RedeemerLoginResult Get() {
            string data = "8F64F682671F144E8C5DE675BF430CC7CDB42A5646BB647628BBEB380B835FF3410B24D6DC8B29F203B5D5A1BCA034410C6A0A1B2FCD007DD3FF9DEB2C9F21EF9B386DC1196276FC27040507E9E9E3EBC603CD4260E1C7F92EE1AF2290EC06B33574038192FC1F2670BC00A9D7C36AB2EFB8D86976F8DD5B8C68DA673ADD98BB9A03D149E62BE3E9099C1CE06D9D167BF885AB919EBF39AD34B952A625053DE054460E58";
            return Login(data);
        }

        [HttpGet]
        public RedeemerLoginResult Login(string pRequest) {
            RedeemerLoginResult result = new RedeemerLoginResult();

            try {
                RedeemerLogin login = JsonConvert.DeserializeObject<RedeemerLogin>(pRequest.Decrypt());


                DateTime requestDate = Convert.ToDateTime(login.RequestTime);

                if (WebApiApplication.GetUTCServerTime().Subtract(requestDate).TotalMinutes > 1) {
                    result.ErrorMsg = "Invalid Access";
                    return result;
                }

                result = RedeemerLoginResult.GetRedeemerInfo(login.EMail);
                if (result == null) {
                    result.ErrorMsg = "Invalid password or username";
                    return result;
                }

                Encryption oEncrypt = new Encryption();
                string pwd = oEncrypt.Decrypt(login.Password, result.Salt);

                if (result.PasswordDecrypted != pwd) {
                    result.ErrorMsg = "Invalid password or username";
                    return result;
                }
                if (!result.RedeemerAccountActive) {
                    result.ErrorMsg = "Redeemer account is not active";
                    return result;
                }

                if (!result.RedeemerActive) {
                    result.ErrorMsg = "Redeemer is not active";
                    return result;
                }

                if (!result.PayDriverPayCard) {
                    result.ErrorMsg = "Redeemer cannot fund PayCard";
                    return result;
                }
            } catch (Exception ex) {
                result.ErrorMsg = ex.Message;
            }

            return result;
        }
    }
}
