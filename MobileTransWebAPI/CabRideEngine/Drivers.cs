﻿using CabRideEngine.Utils;
using Csla.Validation;
using IdeaBlade.Persistence;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using TaxiPassCommon;
using TPTaxiTronicService;
using AppHelper;
using System;

namespace CabRideEngine {
	/// <summary>
	/// The developer class representing the Drivers business object class.
	/// </summary>
	/// <remarks>
	/// <para>Place all application-specific business logic in this class.</para>
	/// <para>
	/// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
	/// and will not overwrite changes.
	/// </para>
	/// <para>
	/// This class inherits from the generated class, its companion in the pair of classes 
	/// defining a business object. Properties in the generated class may be overridden in 
	/// order to change the behavior or add business logic.
	/// </para>
	/// <para>
	/// This class is the final class in the inheritance chain for this business object. 
	/// It should not be inherited.
	/// </para>
	/// </remarks>
	[Serializable]
	public sealed partial class Drivers : DriversDataRow {

		#region Constructors -- DO NOT MODIFY

		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new';
		// it is also required if the class is visible from a web service.
		// DO NOT REMOVE **
		private Drivers()
			: this(null) {
		}

		/// <summary>
		/// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
		/// </summary>
		/// <param name="pRowBuilder"></param>
		public Drivers(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static Drivers Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      Drivers aDrivers = pManager.CreateEntity<Drivers>();
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aDrivers, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aDrivers.AddToManager();
		//      return aDrivers;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		public const string CHARGE_CARD = "Charge Card";

		#region Standard Entity logic to create/audit/validate the entity
		/// <summary>
		/// Mind
		/// </summary>
		/// <param name="pManager"></param>
		/// <returns></returns>
		// Add additional logic to your business object here...
		public static Drivers Create(PersistenceManager pManager) {
			Drivers oDriver = null;
			try {
				oDriver = pManager.CreateEntity<Drivers>();
				SystemDefaults oDefaults = SystemDefaults.GetDefaults(pManager);

				// Using the IdeaBlade Id Generation technique
				pManager.GenerateId(oDriver, Drivers.DriverIDEntityColumn);
				//oDriver["Salt"] = StringUtils.GenerateSalt();
				oDriver.Country = Countries.GetDefaultCountry(pManager).Country;
				//oDriver.BankCountry = oDriver.Country;
				//oDriver.MaxTrxPerDay = oDefaults.DriverMaxTransactions;
				oDriver.MinPUTime = oDefaults.MinPickUpTime;
				oDriver.AddToManager();

			} catch (Exception ex) {
				throw ex;
			}
			return oDriver;
		}

		public static Drivers CreateAndroidBackSeat(AffiliateDrivers pAffDriver, string pAndroidCellNo) {
			PersistenceManager oPM = pAffDriver.PersistenceManager;
			Drivers driver = pAffDriver.PersistenceManager.GetNullEntity<Drivers>();
			if (!pAndroidCellNo.Equals("CELL")) {
				driver = Drivers.GetDriverByBoostPhone(oPM, pAndroidCellNo);
			}
			if (driver.IsNullEntity) {
				driver = Create(pAffDriver.PersistenceManager);
			}
			driver.ByPassValidation = true;

			if (pAffDriver.AffiliateID > 0) {
				driver.AffiliateID = pAffDriver.AffiliateID;
			}
			driver.Name = pAffDriver.Name;
			driver.LastName = pAffDriver.LastName;
			driver.Street = pAffDriver.Street;
			driver.Suite = pAffDriver.Suite;
			driver.City = pAffDriver.City;
			driver.State = pAffDriver.State;
			driver.Country = pAffDriver.Country;
			driver.ZIPCode = pAffDriver.ZIPCode;
			driver.Cell = pAffDriver.Cell;
			driver.BoostPhone = pAndroidCellNo;
			driver.EMail = pAffDriver.EMail;
			if (driver.EMail.IsNullOrEmpty()) {
				driver.EMail = driver.Cell;
			}

			driver.Active = pAffDriver.Active;
			driver.SerialNo = "Android Signup";
			driver.InBackSeat = false;
			driver.HHFareFirst = true;
			driver.HHTripLog = false;
			driver.TrackSwipeCounts = true;

			driver.CardDataEntryType = ((int)DataEntryTypeEnum.SwipeAndManual).ToString();
			driver.TaxiMeter = ((int)TaxiMeterUserEntryType.UserEntryTypeEnum.VoucherMenuFare).ToString();
			driver.PrinterType = ((int)HandHeldPrinterTypes.HandHeldPrinterTypesEnum.HomeATM).ToString();
			try {
				driver.Save();
			} catch (Exception) {

			}

			return driver;
		}

		public static Drivers CreateAndroidDriver(AffiliateDrivers pAffDriver, string pAndroidCellNo) {
			Drivers driver = CreateAndroidBackSeat(pAffDriver, pAndroidCellNo);
			driver.InBackSeat = false;
			return driver;
		}

		protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
			if (IsDeserializing) {
				return;
			}
			base.OnColumnChanging(pArgs);
			DoAudit(pArgs);
		}

		private void DoAudit(DataColumnChangeEventArgs pArgs) {
			if (!this.PersistenceManager.IsClient) {
				return;
			}
			if (this.RowState == DataRowState.Added) {
				return;
			}
			if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
				AuditRecord = DriversAudit.Create(this.PersistenceManager, this);
			}
		}

		public DateTime SignUpDate {
			get {
				DateTime signUpDate = this.ModifiedDate.Value;
				try {

					EntityList<DriversAudit> auditList = DriversAudit.GetDriverHistory(this);
					if (auditList.Count > 0) {
						DateTime history = DriversAudit.GetDriverHistory(this).Min(p => p.ModifiedDate).Value;

						if (history < signUpDate) {
							signUpDate = history;
						}
					}
				} catch (Exception ex) {
					Console.WriteLine(ex.Message);
				}
				return signUpDate;
			}
		}

		public bool ByPassValidation { get; set; }

		protected override void AddRules(RuleList pList) {
			PropertyRequiredRule.AddToList(pList, NameColumn.ColumnName);
			//PropertyRequiredRule.AddToList(pList, Drivers.EMailEntityColumn.ColumnName);
			PropertyRequiredRule.AddToList(pList, CellEntityColumn.ColumnName);
			//PropertyRequiredRule.AddToList(pList, BoostPhoneEntityColumn.ColumnName);
			//PropertyRequiredRule.AddToList(pList, LicenseNoEntityColumn.ColumnName);
			//pList.Add(IsCellUnique, CellEntityColumn.ColumnName);
			pList.Add(IsNextelPhoneUnique, BoostPhoneEntityColumn.ColumnName);
			pList.Add(IsLicenseNoUnique, LicenseNoEntityColumn.ColumnName);
			pList.Add(IsSerialNoUnique, SerialNoEntityColumn.ColumnName);
			//pList.Add(IsSubNoValid, SubNoEntityColumn.ColumnName);
		}


		private bool IsCellUnique(object pTarget, RuleArgs e) {
			if (this.TaxiTronic || ByPassValidation) {
				return true;
			}

			Drivers oDriver = pTarget as Drivers;

			if (oDriver.Name.Contains("Chargeback") || oDriver.Name.Equals(CHARGE_CARD, StringComparison.CurrentCultureIgnoreCase)) {
				return true;
			}

			if (string.IsNullOrEmpty(oDriver.Cell)) {
				return false;
			}
			if (oDriver.Cell.ToUpper() == "CELL") {
				return true;
			}
			RdbQuery qry = new RdbQuery(typeof(Drivers), Drivers.CellEntityColumn, EntityQueryOp.EQ, oDriver.Cell);
			qry.AddClause(Drivers.DriverIDEntityColumn, EntityQueryOp.NE, oDriver.DriverID);
			EntityList<Drivers> oList = this.PersistenceManager.GetEntities<Drivers>(qry);
			if (oList.Count > 0) {
				e.Description = "A driver already exists with the Cell Phone: " + oDriver.Cell.FormattedPhoneNumber() + " with DriverID: " + oDriver.DriverID;
				return false;
			}
			return true;
		}

		private bool IsNextelPhoneUnique(object pTarget, RuleArgs e) {
			if (this.TaxiTronic || ByPassValidation) {
				return true;
			}

			Drivers oDriver = pTarget as Drivers;

			if (oDriver.Name.Contains("Chargeback") || oDriver.Name.Equals(CHARGE_CARD, StringComparison.CurrentCultureIgnoreCase)) {
				return true;
			}

			if (string.IsNullOrEmpty(oDriver.BoostPhone)) {
				return false;
			}
			if (oDriver.BoostPhone.ToUpper() == "CELL") {
				return true;
			}
			RdbQuery qry = new RdbQuery(typeof(Drivers), Drivers.BoostPhoneEntityColumn, EntityQueryOp.EQ, oDriver.BoostPhone);
			qry.AddClause(Drivers.DriverIDEntityColumn, EntityQueryOp.NE, oDriver.DriverID);
			EntityList<Drivers> oList = this.PersistenceManager.GetEntities<Drivers>(qry);
			if (oList.Count > 0) {
				e.Description = "A record already exists with the Nextel Phone: " + oDriver.BoostPhone.FormattedPhoneNumber();
				return false;
			}
			return true;
		}

		private bool IsLicenseNoUnique(object pTarget, RuleArgs e) {
			if (this.TaxiTronic || ByPassValidation) {
				return true;
			}

			Drivers oDriver = pTarget as Drivers;
			if (string.IsNullOrEmpty(oDriver.LicenseNo)) {
				return true;
			}
			if (oDriver.LicenseNo.ToUpper() == "AUTODRIVER") {
				return true;
			}
			RdbQuery qry = new RdbQuery(typeof(Drivers), Drivers.LicenseNoEntityColumn, EntityQueryOp.EQ, oDriver.LicenseNo);
			qry.AddClause(Drivers.DriverIDEntityColumn, EntityQueryOp.NE, oDriver.DriverID);
			EntityList<Drivers> oList = this.PersistenceManager.GetEntities<Drivers>(qry);
			if (oList.Count > 0) {
				e.Description = "A driver already exists with this License No: " + oDriver.LicenseNo;
				return false;
			}
			return true;
		}

		private bool IsSerialNoUnique(object pTarget, RuleArgs e) {
			string serialNo = "" + this.SerialNo;
			if (this.TaxiTronic || ByPassValidation || serialNo.Equals("Android Signup", StringComparison.CurrentCultureIgnoreCase) || serialNo.Equals("Registered", StringComparison.CurrentCultureIgnoreCase)) {
				return true;
			}

			// allow Android and iPhone app too have more than 1 driver account for the serial no
			string version = "" + this.AppVersion;
			if (version.StartsWith("i", StringComparison.CurrentCultureIgnoreCase) || version.StartsWith("a", StringComparison.CurrentCultureIgnoreCase)) {
				version = version.Substring(1);
			}
			try {
				decimal vers = Convert.ToDecimal(version);

				if (!this.HardwareManufacturer.IsNullOrEmpty() && vers >= 2.5M) {
					return true;
				}
			} catch (Exception) {
			}

			Drivers oDriver = pTarget as Drivers;
			if (string.IsNullOrEmpty(oDriver.SerialNo)) {
				return true;
			}
			RdbQuery qry = new RdbQuery(typeof(Drivers), Drivers.SerialNoEntityColumn, EntityQueryOp.EQ, oDriver.SerialNo);
			qry.AddClause(Drivers.DriverIDEntityColumn, EntityQueryOp.NE, oDriver.DriverID);
			qry.AddClause(Drivers.SerialNoEntityColumn, EntityQueryOp.NE, "Android Signup");
			qry.AddClause(Drivers.SerialNoEntityColumn, EntityQueryOp.NE, "Registered");
			EntityList<Drivers> oList = this.PersistenceManager.GetEntities<Drivers>(qry);

			if (oList.Count > 0) {
				e.Description = "A cell phone already exists with this Serial Number: " + oDriver.SerialNo;
				return false;
			}
			return true;
		}

		//private bool IsSubNoValid(object pTarget, RuleArgs e) {
		//    if (this.TaxiTronic || ByPassValidation || !this.HardwareOS.IsNullOrEmpty()) {
		//        return true;
		//    }
		//    Drivers oDriver = pTarget as Drivers;
		//    if (oDriver.Name.Contains("Chargeback") || oDriver.Name.Equals(CHARGE_CARD, StringComparison.CurrentCultureIgnoreCase)) {
		//        return true;
		//    }
		//    if (string.IsNullOrEmpty(oDriver.SubNo) && oDriver.Name.Equals("New Phone")) {
		//        return true;
		//    } else if (string.IsNullOrEmpty(oDriver.SubNo)) {
		//        //e.Description = "Please enter the Sub No from the phone";
		//        return true;
		//    }

		//    if (oDriver.SubNo.Length < 19 || !oDriver.SubNo.Contains("-") || oDriver.SubNo.IndexOf("-") != 10) {
		//        e.Description = "Not a valid Sub No";
		//        return false;
		//    }

		//    RdbQuery qry = new RdbQuery(typeof(Drivers), Drivers.SubNoEntityColumn, EntityQueryOp.EQ, oDriver.SubNo);
		//    qry.AddClause(Drivers.DriverIDEntityColumn, EntityQueryOp.NE, oDriver.DriverID);
		//    EntityList<Drivers> oList = this.PersistenceManager.GetEntities<Drivers>(qry);
		//    if (oList.Count > 0) {
		//        e.Description = "A cell phone already exists with this Sub Number: " + oDriver.SubNo;
		//        return false;
		//    }

		//    return true;
		//}

		#endregion

		//***************************************************************************************************
		#region My Relations

		public Affiliate MyAffiliate {
			get {
				if (!AffiliateID.HasValue) {
					return this.PersistenceManager.GetNullEntity<Affiliate>();
				}
				return Affiliate.GetAffiliate(this.PersistenceManager, AffiliateID.Value);
			}
		}


		#endregion

		//***************************************************************************************************
		#region Custom Fields not persistable

		[BindingBrowsable(false)]
		public override string EntityInstanceDisplayName {
			get {
				return Name;
			}
		}


		public string GetPassword {
			get {
				Encryption encrypt = new Encryption();
				return encrypt.Decrypt(Password, Salt);
			}
		}


		public bool HasPendingAuth {
			get {
				EntityList<DriverPayments> oPayments = DriverPayments.GetPendingTransactions(this);
				return oPayments.Count > 0;
			}
		}

		public string FormattedCellPhone {
			get {
				return this.Cell.FormattedPhoneNumber();
			}
		}

		public string FormattedBoostPhone {
			get {
				return this.BoostPhone.FormattedPhoneNumber();
			}
		}

		public int GetMaxTrxPerDay {
			get {
				int maxTrx = base.MaxTrxPerDay;
				if (maxTrx < 1) {
					//if (!this.MyAffiliate.IsNullEntity && this.MyAffiliate.AffiliateDriverDefaultses.Count > 0) {
					maxTrx = this.MyAffiliate.AffiliateDriverDefaults.MaxTransactions;
					//}
					if (maxTrx < 1) {
						maxTrx = SystemDefaults.GetDefaults(this.PersistenceManager).DriverMaxTransactions;
					}
				}
				//if no max defined set to a high number that probably will never occur
				if (maxTrx == 0) {
					maxTrx = 100000;
				}
				return maxTrx;
			}
		}

		public decimal GetChargeMaxAmount {
			get {
				decimal nAmount = base.ChargeMaxAmount;
				if (nAmount < 1) {
					//if (!this.MyAffiliate.IsNullEntity && this.MyAffiliate.AffiliateDriverDefaultses.Count > 0) {
					nAmount = this.MyAffiliate.AffiliateDriverDefaults.ChargeMaxAmount;
					//}
					if (nAmount < 1) {
						nAmount = SystemDefaults.GetDefaults(this.PersistenceManager).DriverChargeMaxAmount;
					}
				}
				//if no max defined set to a high number that probably will never occur
				if (nAmount == 0) {
					nAmount = 100000;
				}
				return nAmount;
			}
		}

		public decimal GetChargeMaxSwipeAmount {
			get {
				decimal nAmount = base.ChargeMaxSwipeAmount;
				if (nAmount < 1) {
					nAmount = this.MyAffiliate.AffiliateDriverDefaults.ChargeMaxSwipeAmount;
					if (nAmount < 1) {
						nAmount = SystemDefaults.GetDefaults(this.PersistenceManager).DriverChargeMaxSwipeAmount;
					}
				}
				//if no max defined set to a high number that probably will never occur
				if (nAmount == 0) {
					nAmount = 100000;
				}
				return nAmount;
			}
		}

		public decimal DisplayDriverFeeMsgAmount {
			get {
				decimal amount = HHDisplayDriverFeeMsgAmount;
				if (amount < 1) {
					amount = this.MyAffiliate.AffiliateDriverDefaults.HHDisplayDriverFeeMsgAmount;
					if (amount < 1) {
						amount = SystemDefaults.GetDefaults(this.PersistenceManager).HHDisplayDriverFeeMsgAmount;
					}
				}

				return amount;
			}
		}

		public int GetCardChargedMaxNumber {
			get {
				int maxNumber = base.CardChargedMaxNumber;
				if (maxNumber < 1) {
					//if (!this.MyAffiliate.IsNullEntity && this.MyAffiliate.AffiliateDriverDefaultses.Count > 0) {
					maxNumber = this.MyAffiliate.AffiliateDriverDefaults.CardChargedMaxNumber;
					//}
					if (maxNumber < 1) {
						maxNumber = SystemDefaults.GetDefaults(this.PersistenceManager).DriverCardChargedMaxNumber;
					}
				}
				//if no max defined set to a high number that probably will never occur
				if (maxNumber == 0) {
					maxNumber = 100000;
				}
				return maxNumber;
			}
		}

		public int GetMinTransTime {
			get {
				return SystemDefaults.GetDefaults(this.PersistenceManager).DriverMinTransTime;
			}
		}


		public int GetMaxTransPerWeek {
			get {
				int max = this.MaxTransPerWeek;
				if (max < 1) {
					//if (!this.MyAffiliate.IsNullEntity && this.MyAffiliate.AffiliateDriverDefaultses.Count > 0) {
					max = this.MyAffiliate.AffiliateDriverDefaults.MaxTransPerWeek;
					//}
					if (max < 1) {
						max = SystemDefaults.GetDefaults(this.PersistenceManager).DriverMaxTransPerWeek;
					}
				}
				//if no max defined set to a high number that probably will never occur
				if (max == 0) {
					max = 100000;
				}
				return max;
			}
		}

		public int GetMaxTransPerWeekPerCard {
			get {
				int max = this.MaxTransPerWeekPerCard;
				if (max < 1) {
					if (!this.MyAffiliate.IsNullEntity) { // && this.MyAffiliate.AffiliateDriverDefaultses.Count > 0) {
						max = this.MyAffiliate.AffiliateDriverDefaults.MaxTransPerWeekPerCard;
					}
					if (max < 1) {
						max = SystemDefaults.GetDefaults(this.PersistenceManager).DriverMaxTransPerWeekPerCard;
					}
				}
				//if no max defined set to a high number that probably will never occur
				if (max == 0) {
					max = 100000;
				}
				return max;
			}
		}

		public int GetMaxTransPerMonth {
			get {
				int max = this.MaxTransPerMonth;
				if (max < 1) {
					if (!this.MyAffiliate.IsNullEntity) { // && this.MyAffiliate.AffiliateDriverDefaultses.Count > 0) {
						max = this.MyAffiliate.AffiliateDriverDefaults.MaxTransPerMonth;
					}
					if (max < 1) {
						max = SystemDefaults.GetDefaults(this.PersistenceManager).DriverMaxTransPerMonth;
					}
				}
				//if no max defined set to a high number that probably will never occur
				if (max == 0) {
					max = 100000;
				}
				return max;
			}
		}

		public int GetMaxTransPerMonthPerCard {
			get {
				int max = this.MaxTransPerMonthPerCard;
				if (max < 1) {
					if (!this.MyAffiliate.IsNullEntity) { //&& this.MyAffiliate.AffiliateDriverDefaultses.Count > 0) {
						max = this.MyAffiliate.AffiliateDriverDefaults.MaxTransPerMonthPerCard;
					}
					if (max < 1) {
						max = SystemDefaults.GetDefaults(this.PersistenceManager).DriverMaxTransPerMonthPerCard;
					}
				}
				//if no max defined set to a high number that probably will never occur
				if (max == 0) {
					max = 100000;
				}
				return max;
			}
		}

		public int GetMaxDailyAmount {
			get {
				int maxAmount = this.MaxDailyAmount;
				if (maxAmount < 1) {
					if (!this.MyAffiliate.IsNullEntity) { // && this.MyAffiliate.AffiliateDriverDefaultses.Count > 0) {
						maxAmount = this.MyAffiliate.AffiliateDriverDefaults.MaxDailyAmount;
					}
					if (maxAmount < 1) {
						maxAmount = SystemDefaults.GetDefaults(this.PersistenceManager).DriverMaxDailyAmount;
					}
				}
				//if no max defined set to a high number that probably will never occur
				if (maxAmount == 0) {
					maxAmount = 100000;
				}
				return maxAmount;
			}
		}

		public int GetMaxWeeklyAmount {
			get {
				int maxAmount = this.MaxWeeklyAmount;
				if (maxAmount < 1) {
					if (!this.MyAffiliate.IsNullEntity) { // && this.MyAffiliate.AffiliateDriverDefaultses.Count > 0) {
						maxAmount = this.MyAffiliate.AffiliateDriverDefaults.MaxWeeklyAmount;
					}
					if (maxAmount < 1) {
						maxAmount = SystemDefaults.GetDefaults(this.PersistenceManager).DriverMaxWeeklyAmount;
					}
				}
				//if no max defined set to a high number that probably will never occur
				if (maxAmount == 0) {
					maxAmount = 100000;
				}
				return maxAmount;
			}
		}

		public int GetMaxMonthlyAmount {
			get {
				int maxAmount = this.MaxMonthlyAmount;
				if (maxAmount < 1) {
					if (!this.MyAffiliate.IsNullEntity) { // && this.MyAffiliate.AffiliateDriverDefaultses.Count > 0) {
						maxAmount = this.MyAffiliate.AffiliateDriverDefaults.MaxMonthlyAmount;
					}
					if (maxAmount < 1) {
						maxAmount = SystemDefaults.GetDefaults(this.PersistenceManager).DriverMaxMonthlyAmount;
					}
				}
				//if no max defined set to a high number that probably will never occur
				if (maxAmount == 0) {
					maxAmount = 1000000;
				}
				return maxAmount;
			}
		}

		public int GetAuthLimit1() {
			if (this.MyAffiliate != null) {
				if (this.MyAffiliate.AffiliateDriverDefaults != null) {
					//if (this.MyAffiliate.AffiliateDriverDefaultses.Count > 0) {
					return this.MyAffiliate.AffiliateDriverDefaults.GetAuthLimit1();
					//}
				}
			}
			return 75;
		}

		public int GetAuthLimit2() {
			if (this.MyAffiliate != null) {
				if (this.MyAffiliate.AffiliateDriverDefaults != null) {
					//if (this.MyAffiliate.AffiliateDriverDefaultses.Count > 0) {
					return this.MyAffiliate.AffiliateDriverDefaults.GetAuthLimit2();
					//}
				}
			}
			return 100;
		}

		public int GetAuthLimit3() {
			if (ChargeMaxAmount != 0) {
				return Convert.ToInt32(ChargeMaxAmount);
			}
			if (this.MyAffiliate != null) {
				if (this.MyAffiliate.AffiliateDriverDefaults != null) {
					//if (this.MyAffiliate.AffiliateDriverDefaultses.Count > 0) {
					return this.MyAffiliate.AffiliateDriverDefaults.GetAuthLimit3();
					//}
				}
			}
			return 150;
		}


		public DateTime? LastGPSTime {
			get {
				return this.DriversGPSLocation.GPSTime;
			}
		}

		private DateTime? mLastTransactionTime = null;
		public DateTime? LastTransactionTime {
			get {
				if (!mLastTransactionTime.HasValue) {
					DateTime? date = DriverPayments.GetLastDriverPayment(this.PersistenceManager, this.DriverID).ChargeDate;
					TimeZoneConverter converter = new TimeZoneConverter();
					if (date.HasValue && !this.MyAffiliate.TimeZone.IsNullOrEmpty()) {
						date = converter.FromUniversalTime(this.MyAffiliate.TimeZone, date.Value);
					}
					mLastTransactionTime = date;
				}
				return mLastTransactionTime;
			}
		}

		public bool TrackTrips {
			get {
				return this.HHTripLog || this.MyAffiliate.AffiliateDriverDefaults.HHTripLog;
			}
		}

		public bool FareFirst {
			get {
				return this.HHFareFirst || this.MyAffiliate.AffiliateDriverDefaults.HHFareFirst;
			}
		}

		public string MiddleName { get; set; }
		public string LastName { get; set; }

		public string FirstName {
			get {
				string firstName = "";

				if (this.Name.Contains(",")) {
					List<string> nameList = this.Name.Split(',').ToList();
					LastName = nameList[0];
					for (int i = 1; i < nameList.Count; i++) {
						if (nameList.Count - 1 == i) {
							firstName = nameList[i];
						} else {
							MiddleName += nameList[i] + " ";
						}
					}
				} else {
					List<string> nameList = this.Name.Split(' ').ToList();
					firstName = nameList[0];
					for (int i = 1; i < nameList.Count; i++) {
						if (nameList.Count - 1 == i) {
							LastName = nameList[i];
						} else {
							MiddleName += nameList[i] + " ";
						}
					}
				}
				return firstName;
			}
		}

		public bool GetRideDispatch {
			get {
				bool value = false;
				CabDriverRef oCDRef = CabDriverRef.GetRecord(this.PersistenceManager, "CabRideEngine:Drivers", DriverID, "RideQ Dispatch Disabled");
				if (!oCDRef.IsNullEntity) {
					if (oCDRef.ValueString == "1") {
						value = true;
					}
				}
				return value;
			}
		}


		public bool GetRideVIPDispatch {
			get {
				bool value = false;
				CabDriverRef oCDRef = CabDriverRef.GetRecord(this.PersistenceManager, "CabRideEngine:Drivers", DriverID, "RideQ VIP Driver");
				if (!oCDRef.IsNullEntity) {
					if (oCDRef.ValueString == "1") {
						value = true;
					}
				}
				return value;
			}
		}

		#endregion

		//***************************************************************************************************
		#region Override properties
		public override long? DriverStatusID {
			get {
				return base.DriverStatusID;
			}
			set {
				if (base.DriverStatusID != value) {
					DriverStatusUpdated = DateTime.Now;
					DriverStatusUpdatedBy = LoginManager.PrincipleUser.Identity.Name;
				}

				base.DriverStatusID = value;
			}
		}


		public override DateTime? DriverStatusUpdated {
			get {
				return base.DriverStatusUpdated;
			}
			set {
				if (this.MyAffiliate.TimeZone.IsNullOrEmpty() || !value.HasValue) {
					base.DriverStatusUpdated = value;
				} else {
					TimeZoneConverter convert = new TimeZoneConverter();
					if (value.HasValue) {
						base.DriverStatusUpdated = convert.ConvertTime(value.Value, TimeZone.CurrentTimeZone.StandardName, this.MyAffiliate.TimeZone);
					}
				}
			}
		}

		public override string Password {
			get {
				return base.Password;
			}
			set {
				Encryption encrypt = new Encryption();
				base.Password = encrypt.Encrypt(value, Salt);
			}
		}

		public override string SerialNo {
			get {
				return base.SerialNo;
			}
			set {
				base.SerialNo = value.ToUpper();
			}
		}
		#endregion

		//***************************************************************************************************
		#region Retrieve Records

		public static Drivers GetDriver(PersistenceManager pPM, long pDriverID) {
			StringBuilder sql = new StringBuilder();
			sql.AppendFormat("usp_DriversGet @DriverID = {0}", pDriverID);
			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(Drivers), sql.ToString());
			EntityList<Drivers> oDrivers = pPM.GetEntities<Drivers>(qry);
			if (oDrivers.Count == 0) {
				return pPM.GetNullEntity<Drivers>();
			} else {
				return oDrivers[0];
			}
		}

		public static Drivers GetDriver(PersistenceManager pPM, string pEMail) {
			if (string.IsNullOrEmpty(pEMail)) {
				return pPM.GetNullEntity<Drivers>();
			}
			RdbQuery qry = new RdbQuery(typeof(Drivers), Drivers.EMailEntityColumn, EntityQueryOp.EQ, pEMail);
			return pPM.GetEntity<Drivers>(qry);
		}

		public static Drivers GetDriver(PersistenceManager pPM, string pSubNo, string pCallingLine) {
			if (string.IsNullOrEmpty(pSubNo) && string.IsNullOrEmpty(pCallingLine)) {
				return pPM.GetNullEntity<Drivers>();
			}
			RdbQuery qry = new RdbQuery(typeof(Drivers));
			if (!pCallingLine.IsNullOrEmpty()) {
				qry.AddClause(Drivers.CallingLineEntityColumn, EntityQueryOp.EQ, pCallingLine);
			}
			if (!pSubNo.IsNullOrEmpty()) {
				qry.AddClause(Drivers.SubNoEntityColumn, EntityQueryOp.EQ, pSubNo);
			}

			return pPM.GetEntity<Drivers>(qry);
		}

		public static Drivers GetChargeCard(Affiliate pAffiliate) {
			PersistenceManager oPM = pAffiliate.PersistenceManager;
			RdbQuery qry = new RdbQuery(typeof(Drivers), Drivers.NameEntityColumn, EntityQueryOp.EQ, CHARGE_CARD);
			qry.AddClause(Drivers.AffiliateIDEntityColumn, EntityQueryOp.EQ, pAffiliate.AffiliateID);
			Drivers oDriver = oPM.GetEntity<Drivers>(qry);
			if (oDriver.IsNullEntity) {
				oDriver = Create(oPM);
				oDriver.Name = CHARGE_CARD;
				oDriver.City = pAffiliate.City;
				oDriver.State = pAffiliate.State;
				oDriver.Country = pAffiliate.Country;
				oDriver.ZIPCode = pAffiliate.ZIPCode;
				oDriver.EMail = pAffiliate.Email;
				oDriver.Cell = "CELL";
				oDriver.BoostPhone = "CELL";
				oDriver.AffiliateID = pAffiliate.AffiliateID;
				if (oDriver.EMail.IsNullOrEmpty()) {
					oDriver.EMail = "email@taxipass.com";
				}

				try {
					oDriver.Save();
				} catch (PersistenceManagerSaveException ex) {
					throw ex;
				}
			}
			return oDriver;
		}

		public static Drivers GetChargeCard(PersistenceManager pPM, long pAffiliateID) {
			Affiliate aff = Affiliate.GetAffiliate(pPM, pAffiliateID);
			return GetChargeCard(aff);
		}

		public static Drivers GetDriverByIPAddress(PersistenceManager pPM, string pIPAddress) {
			if (string.IsNullOrEmpty(pIPAddress)) {
				return pPM.GetNullEntity<Drivers>();
			}
			RdbQuery qry = new RdbQuery(typeof(Drivers), Drivers.IPAddressEntityColumn, EntityQueryOp.EQ, pIPAddress);
			return pPM.GetEntity<Drivers>(qry);
		}

		public static Drivers GetDriverByCellPhone(PersistenceManager pPM, long pAffiliateID, string pCellPhone) {
			if (string.IsNullOrEmpty(pCellPhone)) {
				return pPM.GetNullEntity<Drivers>();
			}
			RdbQuery qry = new RdbQuery(typeof(Drivers), Drivers.CellEntityColumn, EntityQueryOp.EQ, pCellPhone);
			qry.AddClause(Drivers.AffiliateIDEntityColumn, EntityQueryOp.EQ, pAffiliateID);
			return pPM.GetEntity<Drivers>(qry);
		}

		public static Drivers GetDriverByCellPhone(Affiliate pAffiliate, string pCellPhone) {
			if (string.IsNullOrEmpty(pCellPhone)) {
				return pAffiliate.PersistenceManager.GetNullEntity<Drivers>();
			}
			RdbQuery qry = new RdbQuery(typeof(Drivers), Drivers.CellEntityColumn, EntityQueryOp.EQ, pCellPhone);
			qry.AddClause(Drivers.AffiliateIDEntityColumn, EntityQueryOp.EQ, pAffiliate.AffiliateID);
			return pAffiliate.PersistenceManager.GetEntity<Drivers>(qry);
		}

		public static Drivers GetDriverBySerialNo(PersistenceManager pPM, string pSerialNo) {
			if (string.IsNullOrEmpty(pSerialNo)) {
				return pPM.GetNullEntity<Drivers>();
			}
			RdbQuery qry = new RdbQuery(typeof(Drivers), Drivers.SerialNoEntityColumn, EntityQueryOp.EQ, pSerialNo);
			return pPM.GetEntity<Drivers>(qry);
		}


		public static EntityList<Drivers> GetAll(PersistenceManager pPM) {
			return GetData(pPM, 0);

		}


		// ******************************************************************************
		// Get all Drivers for an Affiliate
		// ******************************************************************************
		public static EntityList<Drivers> GetDrivers(Affiliate pAffiliate) {
			return GetData(pAffiliate.PersistenceManager, pAffiliate.AffiliateID);
		}

		public static EntityList<Drivers> GetDrivers(PersistenceManager pPM, long pAffiliateID) {
			return GetData(pPM, pAffiliateID);
		}

		// ******************************************************************************
		// Call the proc
		// ******************************************************************************
		private static EntityList<Drivers> GetData(PersistenceManager pPM, long pAffiliateID) {
			int iArgCount = 0;
			StringBuilder sql = new StringBuilder();
			sql.Append("usp_DriversGet ");
			if (pAffiliateID != 0) {
				sql.AppendFormat(" @AffiliateID = {0}", pAffiliateID);
				iArgCount++;
			}

			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(Drivers), sql.ToString());
			EntityList<Drivers> oData = pPM.GetEntities<Drivers>(qry);

			return oData;
		}

		public static EntityList<Drivers> GetCellPhones(Affiliate pAffiliate) {
			RdbQuery qry = new RdbQuery(typeof(Drivers), Drivers.AffiliateIDEntityColumn, EntityQueryOp.EQ, pAffiliate.AffiliateID);
			qry.AddClause(BoostPhoneEntityColumn, EntityQueryOp.IsNotNull);
			qry.AddOrderBy(Drivers.BoostPhoneEntityColumn);
			return pAffiliate.PersistenceManager.GetEntities<Drivers>(qry, QueryStrategy.DataSourceThenCache);
		}


		public static Drivers GetDriver(Affiliate pAffiliate, string pName) {
			PersistenceManager oPM = pAffiliate.PersistenceManager;
			if (string.IsNullOrEmpty(pName)) {
				return oPM.GetNullEntity<Drivers>();
			}
			RdbQuery qry = new RdbQuery(typeof(Drivers), Drivers.NameEntityColumn, EntityQueryOp.EQ, pName);
			qry.AddClause(Drivers.AffiliateIDEntityColumn, EntityQueryOp.EQ, pAffiliate.AffiliateID);
			return oPM.GetEntity<Drivers>(qry);
		}

		public static Drivers GetDriverByTaxiNumber(Affiliate pAffiliate, string pTaxiNumber) {
			PersistenceManager oPM = pAffiliate.PersistenceManager;
			if (string.IsNullOrEmpty(pTaxiNumber)) {
				return oPM.GetNullEntity<Drivers>();
			}
			RdbQuery qry = new RdbQuery(typeof(Drivers), Drivers.VehicleNoEntityColumn, EntityQueryOp.EQ, pTaxiNumber);
			qry.AddClause(Drivers.AffiliateIDEntityColumn, EntityQueryOp.EQ, pAffiliate.AffiliateID);
			return oPM.GetEntity<Drivers>(qry);
		}

		public static EntityList<Drivers> GetDriversSpanLocations(PersistenceManager pPM) {
			RdbQuery qry = new RdbQuery(typeof(Drivers));
			qry.AddSpan(EntityRelations.Drivers_DriversGPSLocation);
			qry.AddOrderBy(Drivers.NameEntityColumn);
			return pPM.GetEntities<Drivers>(qry);
		}

		// ******************************************************************************************************
		// 11/14/13 Lookup Driver by cell, name, email, or DriverID
		// used in CabRide.DriversForm
		// ******************************************************************************************************
		public static EntityList<Drivers> GetDriversSearch(PersistenceManager pPM, string pSearch) {
			StringBuilder sql = new StringBuilder();
			sql.AppendFormat("usp_Drivers_Search @Search = '{0}'", pSearch);
			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(Drivers), sql.ToString());
			EntityList<Drivers> oData = pPM.GetEntities<Drivers>(qry);

			return oData;

		}

		public static EntityList<Drivers> GetInactiveDrivers(Affiliate pAffiliate) {

			EntityList<DriversGPSLocation> oGPSList = DriversGPSLocation.GetInactiveDriverLocations(pAffiliate);
			EntityList<Drivers> driverList = new EntityList<Drivers>();

			foreach (DriversGPSLocation oRec in oGPSList) {
				driverList.Add(oRec.Drivers);
			}
			return driverList;
		}

		public static EntityList<Drivers> GetDriversByName(PersistenceManager pPM, long pAffiliateID, string pName) {
			RdbQuery qry = new RdbQuery(typeof(Drivers), Drivers.AffiliateIDEntityColumn, EntityQueryOp.EQ, pAffiliateID);
			qry.AddClause(NameEntityColumn, EntityQueryOp.EQ, pName);
			qry.AddOrderBy(Drivers.DriverIDEntityColumn);

			return pPM.GetEntities<Drivers>(qry);
		}

		public static Drivers GetDriverByCell(PersistenceManager pPM, string pCellPhone) {
			RdbQuery qry = new RdbQuery(typeof(Drivers), CellEntityColumn, EntityQueryOp.EQ, pCellPhone);
			return pPM.GetEntity<Drivers>(qry);
		}


		public static Drivers GetDriverByCell(PersistenceManager pPM, string pCellPhone, bool pActive) {
			RdbQuery qry = new RdbQuery(typeof(Drivers), CellEntityColumn, EntityQueryOp.EQ, pCellPhone);
			qry.AddClause(ActiveEntityColumn, EntityQueryOp.EQ, pActive);
			return pPM.GetEntity<Drivers>(qry);
		}

		public static Drivers GetDriverByBoostPhone(PersistenceManager pPM, string pPhoneNo) {
			RdbQuery qry = new RdbQuery(typeof(Drivers), BoostPhoneEntityColumn, EntityQueryOp.EQ, pPhoneNo);
			return pPM.GetEntity<Drivers>(qry);
		}

		public static Drivers GetDriverByPhone(PersistenceManager pPM, string pCellPhone) {
			RdbQuery qry = new RdbQuery(typeof(Drivers), CellEntityColumn, EntityQueryOp.EQ, pCellPhone);
			qry.AddClause(BoostPhoneEntityColumn, EntityQueryOp.EQ, pCellPhone);
			qry.AddOperator(EntityBooleanOp.Or);
			return pPM.GetEntity<Drivers>(qry);
		}

		// **************************************************************************************
		// Look at phone #, SubNo, Serial No
		// **************************************************************************************
		public static Drivers GetDriverPhoneSubNoOrSerial(PersistenceManager pPM, long pAffiliateID, string pText) {

			RdbQuery qry = new RdbQuery(typeof(Drivers), AffiliateIDEntityColumn, EntityQueryOp.EQ, pAffiliateID);
			// look by Name
			qry.AddClause(NameEntityColumn, EntityQueryOp.EQ, pText);
			qry.AddOperator(EntityBooleanOp.Or);

			// look by Phone
			qry.AddClause(CellEntityColumn, EntityQueryOp.EQ, pText);
			qry.AddClause(BoostPhoneEntityColumn, EntityQueryOp.EQ, pText);
			qry.AddOperator(EntityBooleanOp.Or);

			// by SubNo (device ID)
			qry.AddClause(Drivers.SubNoEntityColumn, EntityQueryOp.EQ, pText);
			qry.AddOperator(EntityBooleanOp.Or);

			// Serial no
			qry.AddClause(Drivers.SerialNoEntityColumn, EntityQueryOp.EQ, pText);
			qry.AddOperator(EntityBooleanOp.Or);
			return pPM.GetEntity<Drivers>(qry);
		}


		public static EntityList<Drivers> GetDriversByCell(PersistenceManager pPM, string pCellPhone) {
			RdbQuery qry = new RdbQuery(typeof(Drivers), CellEntityColumn, EntityQueryOp.EQ, pCellPhone);
			return pPM.GetEntities<Drivers>(qry);
		}

		public static bool DriversHaveGPS(Affiliate pAffiliate) {
			string sql = "SELECT TOP(1) * FROM Drivers INNER JOIN DriversGPSLocation ON Drivers.DriverID = DriversGPSLocation.DriverID " +
						 "WHERE Drivers.AffiliateID = " + pAffiliate.AffiliateID;

			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(Drivers), sql);
			Drivers oDriver = pAffiliate.PersistenceManager.GetEntity<Drivers>(qry);
			return !oDriver.IsNullEntity;
		}

		public static int GetActiveDriverCount(Affiliate pAffiliate, DateTime pDate) {
			string myDate = "'" + pDate.ToString("MM/dd/yyyy") + "'";
			string sql = "SELECT Count(Drivers.DriverID) FROM Drivers left outer join CabRideAudit.dbo.DriversAudit " +
						 "on Drivers.DriverID = CabRideAudit.dbo.DriversAudit.DriverID and CabRideAudit.dbo.DriversAudit.RowVersion = 0 " +
						 "where Drivers.AffiliateID = " + pAffiliate.AffiliateID + " AND Drivers.Name != '" + CHARGE_CARD + "' AND Drivers.Active = 1 " +
						 "AND Drivers.Test = 0 AND (Drivers.ModifiedDate <= " + myDate + " or CabRideAudit.dbo.DriversAudit.ModifiedDate <= " + myDate + ")";

			DataTable table = Utils.SQLHelper.GetRecords(pAffiliate.PersistenceManager, sql);
			int recs = Convert.ToInt32(table.Rows[0][0]);
			return recs;
		}

		public static EntityList<Drivers> GetAndroidEnabled(Affiliate pAffiliate) {
			RdbQuery qry = new RdbQuery(typeof(Drivers), Drivers.AffiliateIDEntityColumn, EntityQueryOp.EQ, pAffiliate.AffiliateID);
			qry.AddClause(Drivers.AppVersionEntityColumn, EntityQueryOp.Like, "a%");
			qry.AddClause(Drivers.ActiveEntityColumn, EntityQueryOp.EQ, true);
			return pAffiliate.PersistenceManager.GetEntities<Drivers>(qry);
		}

		public static EntityList<Drivers> GetSmartPhoneEnabled(Affiliate pAffiliate) {
			RdbQuery qry = new RdbQuery(typeof(Drivers), Drivers.AffiliateIDEntityColumn, EntityQueryOp.EQ, pAffiliate.AffiliateID);
			qry.AddClause(Drivers.ActiveEntityColumn, EntityQueryOp.EQ, true);
			qry.AddClause(Drivers.AppVersionEntityColumn, EntityQueryOp.Like, "a%");
			qry.AddClause(Drivers.AppVersionEntityColumn, EntityQueryOp.Like, "i%");
			qry.AddOperator(EntityBooleanOp.Or);
			return pAffiliate.PersistenceManager.GetEntities<Drivers>(qry);
		}

		#endregion

		//***************************************************************************************************
		#region GPS Methods
		public void PingGPS() {
			PingGPS(false);
		}

		public void PingGPS(bool pPauseAfterPing) {
			if (UsesTaxiTronicGPS()) {
				AffiliateDriverDefaults oDefault = this.MyAffiliate.AffiliateDriverDefaults;
				GPSInfo oGPSInfo = new GPSInfo(oDefault.TaxiTronicMerchant, oDefault.TaxiTronicUserName, oDefault.TaxiTronicPassword);
				string[] aName = Name.Split(' ');
				string sVehicleNo = aName[aName.Length - 1];
				DriversGPSLocation currentGPS = DriversGPSLocation.GetRecord(this);

				int i = 0;
				bool bKeepLooping = true;
				while (bKeepLooping) {
					oGPSInfo.PingGPS(sVehicleNo);
					if (pPauseAfterPing) {
						System.Threading.Thread.Sleep(1000);
						i++;
						if (i > 10) {
							bKeepLooping = false;
						}
					}
					string sXML = oGPSInfo.GetGPSInfo(sVehicleNo);
					if (sXML.StartsWith("<")) {
						TaxiPassCommon.XMLConverter oXML = new TaxiPassCommon.XMLConverter();
						DataSet oDS = oXML.ConvertXMLToDataSet(sXML);
						foreach (DataRow oRow in oDS.Tables[0].Rows) {
							DriversGPSLocation oGPS = DriversGPSLocation.GetRecord(this);
							oGPS.GPSTime = Convert.ToDateTime(oRow["DateTime"]);
							oGPS.Latitude = Convert.ToDouble(oRow["LA"]);
							oGPS.Longitude = Convert.ToDouble(oRow["LO"]);
							oGPS.Direction = oRow["Direct"].ToString();
							oGPS.Speed = Convert.ToDouble(oRow["Speed"]);
							oGPS.Save();
							if (oGPS.GPSTime != currentGPS.GPSTime) {
								bKeepLooping = false;
							}
						}

					} else {
						bKeepLooping = false;
					}
				}
			}
		}

		public void RetrieveGPSHistory(DateTime pStartDate, DateTime pEndDate) {

			if (UsesTaxiTronicGPS()) {
				AffiliateDriverDefaults oDefault = this.MyAffiliate.AffiliateDriverDefaults;
				GPSInfo oGPSInfo = new GPSInfo(oDefault.TaxiTronicMerchant, oDefault.TaxiTronicUserName, oDefault.TaxiTronicPassword);
				string[] aName = Name.Split(' ');
				string sVehicleNo = aName[aName.Length - 1];

				string sXML = oGPSInfo.GetGPSHistory(sVehicleNo, pStartDate.ToString(), pEndDate.ToString());
				if (sXML.StartsWith("<")) {
					TaxiPassCommon.XMLConverter oXML = new TaxiPassCommon.XMLConverter();
					DataSet oDS = oXML.ConvertXMLToDataSet(sXML);
					foreach (DataRow oRow in oDS.Tables[0].Rows) {
						DateTime dDate = Convert.ToDateTime(oRow["DateTime"]);
						DriversGPSHistory oGPS = DriversGPSHistory.GetRecordByDateTime(this, dDate);
						if (oGPS.IsNullEntity) {
							oGPS = DriversGPSHistory.Create(this, dDate);
						}
						oGPS.Latitude = Convert.ToDouble(oRow["LA"]);
						oGPS.Longitude = Convert.ToDouble(oRow["LO"]);
						oGPS.Direction = oRow["Direct"].ToString();
						oGPS.Speed = Convert.ToDouble(oRow["Speed"]);
						oGPS.Save();

					}

				}
			}

		}

		public bool UsesTaxiTronicGPS() {
			//if (this.MyAffiliate.AffiliateDriverDefaultses.Count > 0) {
			AffiliateDriverDefaults oDefault = this.MyAffiliate.AffiliateDriverDefaults;
			return oDefault.TaxiTronicGPS;
			//}
			//return false;
		}
		#endregion

		//***************************************************************************************************
		public void SendMsgToPhone(string pMsg) {
			PersistenceManager oPM = this.PersistenceManager;
			SystemDefaults oDef = SystemDefaults.GetDefaults(oPM);

			string cell = this.BoostPhone;

            TaxiPassCommon.Mail.EZTexting ezText = new TaxiPassCommon.Mail.EZTexting(oDef.EZTextingUserName, oDef.EZTextingPassword);
            bool bUseEzTexting = true;

            string acctSid = oDef.TwilioAccountSID;
            string acctToken = oDef.TwilioAccountToken;
            string apiVersion = oDef.TwilioAPIVersion;
            string smsNumber = oDef.TwilioSMSNumber;

            TaxiPassCommon.Mail.TwilioTexting twilio = new TaxiPassCommon.Mail.TwilioTexting(acctSid, acctToken, apiVersion, smsNumber);
            var response = twilio.TwilioTextMessage(pMsg, cell);
            if (response.RestException == null || response.RestException.Message.IsNullOrEmpty())
            {
                bUseEzTexting = false;
            }

            if (bUseEzTexting)
            {
                ezText.Send(cell, pMsg);
            }

        }

		/// <summary>
		/// Mind Change
		/// </summary>
		/// <param name="pMsg"></param>
		/// <returns></returns>
		public string SendMsgViaTwilio(string pMsg) {
			PersistenceManager oPM = this.PersistenceManager;
			SystemDefaults oDef = SystemDefaults.GetDefaults(oPM);

			string cell = this.Cell;
			string acctSid = oDef.TwilioAccountSID;
			string acctToken = oDef.TwilioAccountToken;
			string apiVersion = oDef.TwilioAPIVersion;
			string smsNumber = oDef.TwilioSMSNumber;

            TaxiPassCommon.Mail.TwilioTexting twilio = new TaxiPassCommon.Mail.TwilioTexting(acctSid, acctToken, apiVersion, smsNumber);
            return twilio.TwilioTextMessage(pMsg, cell).Sid;
		}


		public bool IsRegistered {
			get {
				CabDriverRef onBoard = CabDriverRef.GetRecord(this.PersistenceManager, this.Table.TableName, this.DriverID, "IsRegistered");
				return (!onBoard.IsNullEntity && Convert.ToBoolean(onBoard.ValueString));
			}

			set {
				if (this.DriverID < 0) {
					this.Save();
				}

				CabDriverRef rec = CabDriverRef.GetOrCreateRecord(this.PersistenceManager, this.Table.TableName, this.DriverID, "IsRegistered");
				rec.ValueString = Convert.ToString(value);
				rec.Save();
			}
		}

		public bool IsDriverAuthenticated {
			get {
				CabDriverRef rec = CabDriverRef.GetRecord(this.PersistenceManager, this.Table.TableName, this.DriverID, "DriverAuthenticated");
				return (!rec.IsNullEntity && Convert.ToBoolean(rec.ValueString));
			}

			set {
				if (this.DriverID < 0) {
					this.Save();
				}

				CabDriverRef rec = CabDriverRef.GetOrCreateRecord(this.PersistenceManager, this.Table.TableName, this.DriverID, "DriverAuthenticated");
				rec.ValueString = Convert.ToString(value);
				rec.Save();
			}
		}



		// ************************************************
		// return the base.FraudSuspected
		// ************************************************
		public bool GetFraudSuspected() {
			return base.FraudSuspected;
		}


		// ***************************************************
		// 8/13/2012 Fraud checking got a bit more complicated
		// 11/27/2012 Uncomplicated 
		// ***************************************************
		/*
		public override bool FraudSuspected {
			get {
				return Utils.FraudCheck.DriverFraudCheck(this.PersistenceManager, null, this);
			}
			set {
				base.FraudSuspected = value;
			}
		}
		*/

		// ----------------------------------------------------------------
		// 8/17/2012 Mark driver as frozen if his device is black listed
		// ----------------------------------------------------------------
		public bool IsFrozen() {
			BlackListDevice device = BlackListDevice.GetDevice(this.PersistenceManager, this.SerialNo);
			return !device.IsNullEntity;
		}


		// -------------------------------------------------------------------
		// Is this driver considered as a new driver for freezing the account
		// -------------------------------------------------------------------
		public bool IsNewDriver {
			get {
				StringBuilder sql = new StringBuilder("SELECT count(*) FROM DriverPayments ");
				sql.AppendFormat("WHERE DriverID = {0}", DriverID.ToString());

				DataTable table = SQLHelper.GetRecords(this.PersistenceManager, sql.ToString());
				return MyAffiliate.NewDriverVoucherCount >= Convert.ToInt32(table.Rows[0][0]);
			}
		}

		public bool GetRideDriver {
			get {
				bool getRide = false;
				string version = "" + this.AppVersion;
				if (version.StartsWith("i", StringComparison.CurrentCultureIgnoreCase) || version.StartsWith("a", StringComparison.CurrentCultureIgnoreCase)) {
					version = version.Substring(1);
				}
				try {
					decimal vers = Convert.ToDecimal(version);
					getRide = vers >= 3.5M;
				} catch (Exception) {
				}
				return getRide;
			}
		}


		//public bool PreAuthCardOnly {
		//	get {
		//		CabDriverRef rec = CabDriverRef.GetRecord(this.PersistenceManager, this.Table.TableName, this.DriverID, "PreAuthCardOnly");
		//		return (!rec.IsNullEntity && Convert.ToBoolean(rec.ValueString));
		//	}

		//	set {
		//		if (this.DriverID < 0) {
		//			this.Save();
		//		}

		//		CabDriverRef rec = CabDriverRef.GetOrCreateRecord(this.PersistenceManager, this.Table.TableName, this.DriverID, "PreAuthCardOnly");
		//		rec.ValueString = value.ToString();
		//		rec.Save();
		//	}
		//}

	}

	#region EntityPropertyDescriptors.DriversPropertyDescriptor
	////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
	////    For example: 
	////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
	//public static partial class EntityPropertyDescriptors {
	//	public partial class DriversPropertyDescriptor : BaseEntityPropertyDescriptor {
	//    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
	//    public AdaptedPropertyDescriptor Age {
	//      get { return this.Get("Age"); }
	//    }
	//  }
	//} 
	#endregion

}
