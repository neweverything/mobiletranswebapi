﻿namespace MobileTransWebAPI.Models {
    public partial class MaxRefund {
        public long AffiliateID { get; set; }
        public long AffiliateDriverID { get; set; }
        public string Number { get; set; }
    }
}