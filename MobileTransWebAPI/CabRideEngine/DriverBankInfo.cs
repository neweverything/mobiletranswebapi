﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Util;
using IdeaBlade.Persistence.Rdb;

using CabRideEngine.Utils;
using TaxiPassCommon;
using TaxiPassCommon.Banking;

using Csla.Validation;

namespace CabRideEngine {
    /// <summary>
    /// The developer class representing the DriverBankInfo business object class.
    /// </summary>
    /// <remarks>
    /// <para>Place all application-specific business logic in this class.</para>
    /// <para>
    /// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
    /// and will not overwrite changes.
    /// </para>
    /// <para>
    /// This class inherits from the generated class, its companion in the pair of classes 
    /// defining a business object. Properties in the generated class may be overridden in 
    /// order to change the behavior or add business logic.
    /// </para>
    /// <para>
    /// This class is the final class in the inheritance chain for this business object. 
    /// It should not be inherited.
    /// </para>
    /// </remarks>
    [Serializable]
    public sealed partial class DriverBankInfo : DriverBankInfoDataRow {

        #region Constructors -- DO NOT MODIFY

        // Do not create constructors for this class
        // Developers cannot create instances of this class with the "new" keyword
        // You should write a static Create method instead; see the "Suggested Customization" region
        // Please review the documentation to learn about entity creation and inheritance

        // This private constructor prevents accidental instance creation via 'new';
        // it is also required if the class is visible from a web service.
        // DO NOT REMOVE **
        private DriverBankInfo() : this(null) {
        }

        /// <summary>
        /// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
        /// </summary>
        /// <param name="pRowBuilder"></param>
        public DriverBankInfo(DataRowBuilder pRowBuilder)
            : base(pRowBuilder) {
        }

        #endregion

        #region Suggested Customizations

        //    // Use this factory method template to create new instances of this class
        //    public static DriverBankInfo Create(PersistenceManager pManager,
        //      ... your creation parameters here ...) { 
        //
        //      DriverBankInfo aDriverBankInfo = pManager.CreateEntity<DriverBankInfo>();
        //
        //      // if this object type requires a unique id and you have implemented
        //      // the IIdGenerator interface implement the following line
        //      pManager.GenerateId(aDriverBankInfo, // add id column here //);
        //
        //      // Add custom code here
        //
        //      aDriverBankInfo.AddToManager();
        //      return aDriverBankInfo;
        //    }

        //    // Implement this method if you want your object to sort in a specific order
        //    public override int CompareTo(Object pObject) {
        //    }

        //    // Implement this method to customize the null object version of this class
        //    protected override void UpdateNullEntity() {
        //    }

        #endregion

        // Add additional logic to your business object here...
        public static DriverBankInfo Create(Drivers pDriver) {
            DriverBankInfo oBank = null;
            if (pDriver.DriverBankInfos.Count > 0) {
                return pDriver.DriverBankInfos[0];
            }

            try {
                PersistenceManager oPM = pDriver.PersistenceManager;
                
                oBank = oPM.CreateEntity<DriverBankInfo>();

                // Using the IdeaBlade Id Generation technique
                oPM.GenerateId(oBank, DriverBankInfo.DriverBankInfoIDEntityColumn);
                oBank.DriverID = pDriver.DriverID;
                oBank.BankCountry = pDriver.Country;
                oBank.BankState = pDriver.State;

                oBank.AddToManager();

            } catch (Exception ex) {
                throw ex;
            }
            return oBank;
        }

        protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
            if (IsDeserializing) {
                return;
            }
            base.OnColumnChanging(pArgs);
            DoAudit(pArgs);
        }

        private void DoAudit(DataColumnChangeEventArgs pArgs) {
            if (!this.PersistenceManager.IsClient) {
                return;
            }
            if (this.RowState == DataRowState.Added) {
                return;
            }
            if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
                AuditRecord = DriverBankInfoAudit.Create(this);
            }
        }

        [BindingBrowsable(false)]
        public override string EntityInstanceDisplayName {
            get {
                return this.BankName;
            }
        }

        public PayInfo GetPayInfo() {
            PayInfo oPay = new PayInfo();
            oPay.NachaFile = this.Drivers.MyAffiliate.SendNacha;
            oPay.BankAccountHolder = this.BankAccountHolder;
            oPay.BankAccountNumber = this.BankAccountNumber;
            oPay.BankAccountPersonal = this.BankAccountPersonal;
            oPay.BankAddress = this.BankAddress;
            oPay.BankCity = this.BankCity;
            oPay.BankCode = this.BankCode;
            oPay.BankState = this.BankState;
            oPay.BankSuite = this.BankSuite;
            oPay.BankZIPCode = this.BankZIPCode;
            if (oPay.MyMerchant == null) {
                oPay.MyMerchant = new Merchant();
            }
            oPay.MyMerchant.MerchantNo = this.MerchantInfo.MerchantNo;
            oPay.PaidTo = "Cell: " + this.Drivers.Name;
            return oPay;
        }

        public static DriverBankInfo GetByBankInfo(PersistenceManager pPM, string pRouting, string pAccountNo) {
            RdbQuery qry = new RdbQuery(typeof(DriverBankInfo), DriverBankInfo.BankCodeEntityColumn, EntityQueryOp.EQ, pRouting);
            qry.AddClause(DriverBankInfo.BankAccountNumberEntityColumn, EntityQueryOp.EQ, pAccountNo);
            return pPM.GetEntity<DriverBankInfo>(qry);
        }
    }

    #region EntityPropertyDescriptors.DriverBankInfoPropertyDescriptor
    ////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
    ////    For example: 
    ////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
    //public static partial class EntityPropertyDescriptors {
    //	public partial class DriverBankInfoPropertyDescriptor : BaseEntityPropertyDescriptor {
    //    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
    //    public AdaptedPropertyDescriptor Age {
    //      get { return this.Get("Age"); }
    //    }
    //  }
    //} 
    #endregion

}
