﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace CabRideEngineDapper {
    public class usp_ReservationGetCardProcessingResults {

        public string Result { get; set; }
        public string Reference { get; set; }
        public string Message { get; set; }
        public string TransType { get; set; }
        public DateTime? TransDate { get; set; }
        public string AuthCode { get; set; }
        public string CVV2Match { get; set; }
        public string CreditCardProcessor { get; set; }
        public decimal? Amount { get; set; }
        public string MerchantID { get; set; }
        public string AVS { get; set; }
        public string AVSAddr { get; set; }
        public string AVSZIP { get; set; }
        public string AVSResultCode { get; set; }
        public string ProcessingNotes { get; set; }
        public decimal ServiceTipPerc { get; set; }
        public string CouponCode { get; set; }
        public decimal CouponAmount { get; set; }
        public long CustomerID { get; set; }
        public string CardType { get; set; }
        public string CardNumber { get; set; }
        public string CardHolder { get; set; }
        public DateTime CardExpiration { get; set; }
        public bool TestCustomer { get; set; }
        public int CardGatewayGroupsID { get; set; }

        public static List<usp_ReservationGetCardProcessingResults> Execute(SqlConnection pConn, string pReserveNo) {
            return pConn.Query<usp_ReservationGetCardProcessingResults>("usp_ReservationGetCardProcessingResults", new { ReserveNo = pReserveNo }, commandType: CommandType.StoredProcedure).ToList();
            }
    }
}
