﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CabRideEngineDapper {
	public class SysDefaults_RideCardProcessing {
		public string CardGatewayGroupsID { get; set; }
		public string CardValidationAmount { get; set; }
		public string CardPreAuthAmount { get; set; }
		public string CardFailedMessage { get; set; }


		public long CardGatewayGroupsIDAsLong {
			get {
				return Convert.ToInt64(CardGatewayGroupsID);
			}
		}


		public Decimal CardValidationAmountAsDecimal {
			get {
				return Convert.ToDecimal(CardValidationAmount);
			}
		}


		public decimal CardPreAuthAmountAsDecimal {
			get {
				return Convert.ToDecimal(CardPreAuthAmount);
			}
		}
	}
}
