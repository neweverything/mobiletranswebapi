﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Util;
using IdeaBlade.Persistence.Rdb;

using CabRideEngine.Utils;
using TaxiPassCommon;

using Csla.Validation;
using System.Text;
using System.Threading;

namespace CabRideEngine {
	/// <summary>
	/// The developer class representing the DriversGPSLocation business object class.
	/// </summary>
	/// <remarks>
	/// <para>Place all application-specific business logic in this class.</para>
	/// <para>
	/// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
	/// and will not overwrite changes.
	/// </para>
	/// <para>
	/// This class inherits from the generated class, its companion in the pair of classes 
	/// defining a business object. Properties in the generated class may be overridden in 
	/// order to change the behavior or add business logic.
	/// </para>
	/// <para>
	/// This class is the final class in the inheritance chain for this business object. 
	/// It should not be inherited.
	/// </para>
	/// </remarks> 
	[Serializable]
	public sealed partial class DriversGPSLocation : DriversGPSLocationDataRow {

		#region Constructors -- DO NOT MODIFY

		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new';
		// it is also required if the class is visible from a web service.
		// DO NOT REMOVE **
		private DriversGPSLocation()
			: this(null) {
		}

		/// <summary>
		/// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
		/// </summary>
		/// <param name="pRowBuilder"></param>
		public DriversGPSLocation(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static DriversGPSLocation Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      DriversGPSLocation aDriversGPSLocation = pManager.CreateEntity<DriversGPSLocation>();
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aDriversGPSLocation, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aDriversGPSLocation.AddToManager();
		//      return aDriversGPSLocation;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		// Add additional logic to your business object here...
		#region Standard Entity logic to create/audit/validate the entity

		// Add additional logic to your business object here...
		private static DriversGPSLocation Create(Drivers pDriver) {
			PersistenceManager oPM = pDriver.PersistenceManager;
			DriversGPSLocation oDriversGPSLocation = pDriver.DriversGPSLocation;
			try {

				if (oDriversGPSLocation.IsNullEntity) {
					oDriversGPSLocation = oPM.CreateEntity<DriversGPSLocation>();
				}
				// Using the IdeaBlade Id Generation technique
				oPM.GenerateId(oDriversGPSLocation, DriversGPSLocation.DriverGPSLocationIDEntityColumn);
				oDriversGPSLocation.DriverID = pDriver.DriverID;
				oDriversGPSLocation.AddToManager();

			} catch (Exception ex) {
				throw ex;
			}
			return oDriversGPSLocation;
		}

		#endregion

		public static DriversGPSLocation GetRecord(Drivers pDriver) {
			RdbQuery qry = new RdbQuery(typeof(DriversGPSLocation), DriversGPSLocation.DriverIDEntityColumn, EntityQueryOp.EQ, pDriver.DriverID);
			DriversGPSLocation oRec = pDriver.PersistenceManager.GetEntity<DriversGPSLocation>(qry, QueryStrategy.DataSourceThenCache);
			if (oRec.IsNullEntity) {
				oRec = Create(pDriver);
				oRec.GoodSwipes = 0;
				oRec.BadSwipes = 0;
				oRec.Save();
			}
			return oRec;
		}

		// ******************************************************************************************
		// Pass in a DriverID, return the gps record
		// ******************************************************************************************
		public static DriversGPSLocation GetByDriverID(PersistenceManager pPM, long pDriverID) {
			RdbQuery qry = new RdbQuery(typeof(DriversGPSLocation), DriversGPSLocation.DriverIDEntityColumn, EntityQueryOp.EQ, pDriverID);
			DriversGPSLocation oRec = pPM.GetEntity<DriversGPSLocation>(qry, QueryStrategy.DataSourceThenCache);
			if (oRec.IsNullEntity) {
				Drivers oDriver = Drivers.GetDriver(pPM, pDriverID);
				oRec = Create(oDriver);
				oRec.GoodSwipes = 0;
				oRec.BadSwipes = 0;
				oRec.Save();
			}
			return oRec;
		}

		public static DriversGPSLocation GetRecord(PersistenceManager pPM, long pDriverGPSLocationID) {
			return GetRecord(pPM, pDriverGPSLocationID, QueryStrategy.DataSourceThenCache);
		}

		public static DriversGPSLocation GetRecord(PersistenceManager pPM, long pDriverGPSLocationID, QueryStrategy pQueryStrategy) {
			RdbQuery qry = new RdbQuery(typeof(DriversGPSLocation), DriversGPSLocation.DriverGPSLocationIDEntityColumn, EntityQueryOp.EQ, pDriverGPSLocationID);
			DriversGPSLocation oRec = pPM.GetEntity<DriversGPSLocation>(qry, pQueryStrategy);
			return oRec;
		}


		// *********************************************************************************************
		// Get all GPSLocs for an affiliate
		// *********************************************************************************************
		public static EntityList<DriversGPSLocation> GetAffiliateDriverLocations(Affiliate pAffiliate) {
			StringBuilder sql = new StringBuilder();
			sql.AppendFormat("usp_DriversGPSLocation_GetByAffiliate @AffiliateID = {0}", pAffiliate.AffiliateID);
			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(DriversGPSLocation), sql.ToString());
			return pAffiliate.PersistenceManager.GetEntities<DriversGPSLocation>(qry);
		}

		public static EntityList<DriversGPSLocation> GetInactiveDriverLocations(Affiliate pAffiliate) {
			if (pAffiliate.AffiliateDriverDefaults.IsNullEntity) {
				return new EntityList<DriversGPSLocation>();
			}
			if (pAffiliate.AffiliateDriverDefaults.EmailInactiveDriversDuration < 1) {
				return new EntityList<DriversGPSLocation>();
			}
			DateTime dDate = DateTime.Now.AddHours(-pAffiliate.AffiliateDriverDefaults.EmailInactiveDriversDuration);

			RdbQuery qry = new RdbQuery(typeof(DriversGPSLocation), DriversGPSLocation.GPSTimeEntityColumn, EntityQueryOp.LE, dDate);
			//qry.AddClause(DriversGPSLocation.dr
			return pAffiliate.PersistenceManager.GetEntities<DriversGPSLocation>(qry);
		}

		public static DriversGPSLocation GetLastAffiliateDriverLocation(AffiliateDrivers pDriver) {
			RdbQuery qry = new RdbQuery(typeof(DriversGPSLocation), DriversGPSLocation.AffiliateDriverIDEntityColumn, EntityQueryOp.EQ, pDriver.AffiliateDriverID);
			qry.AddOrderBy(DriversGPSLocation.GPSTimeEntityColumn, System.ComponentModel.ListSortDirection.Descending);
			return pDriver.PersistenceManager.GetEntity<DriversGPSLocation>(qry);
		}


		public DateTime? GetSetLocalGPSTime {
			get {
				return this.GPSTime;
			}
			set {
				if (value.HasValue) {
					if (this.Drivers.MyAffiliate != null && !this.Drivers.MyAffiliate.TimeZone.IsNullOrEmpty()) {
						TimeZoneConverter oTime = new TimeZoneConverter();
						base.GPSTime = oTime.ConvertTime(value.Value, TimeZone.CurrentTimeZone.StandardName, this.Drivers.MyAffiliate.TimeZone);
					} else {
						base.GPSTime = value.Value.ToUniversalTime();
					}
				} else {
					base.GPSTime = value;
				}
			}
		}

		public DateTime? GetSetLocalMeterStatusTime {
			get {
				return base.MeterStatusTime;
			}
			set {
				if (value.HasValue) {
					if (this.Drivers.MyAffiliate != null && !this.Drivers.MyAffiliate.TimeZone.IsNullOrEmpty()) {
						TimeZoneConverter oTime = new TimeZoneConverter();
						base.MeterStatusTime = oTime.ConvertTime(value.Value, TimeZone.CurrentTimeZone.StandardName, this.Drivers.MyAffiliate.TimeZone);
					} else {
						base.MeterStatusTime = value.Value.ToUniversalTime();
					}
				} else {
					base.MeterStatusTime = value;
				}
			}
		}


		public string LocalGPSDateTimeString {
			get {
				string sTime = "";
				if (this.GPSTime.HasValue) {
					if (this.Drivers.MyAffiliate != null && !string.IsNullOrEmpty(this.Drivers.MyAffiliate.TimeZone)) {
						TimeZoneConverter oTime = new TimeZoneConverter();
						sTime = oTime.FromUniversalTime(this.Drivers.MyAffiliate.TimeZone, this.GPSTime.Value).ToString("MM/dd/yyyy hh:mm tt");
					} else {
						sTime = this.GPSTime.Value.ToLocalTime().ToString("MM/dd/yyyy hh:mm tt");
					}
				}
				return sTime;
			}
		}

		//public DateTime LocalGPSAcquiredDateTime {
		//    get {
		//        if (!this.GPSAcquiredTime.HasValue) {
		//            return LocalGPSDateTime.Value;
		//        }
		//        if (this.Drivers.MyAffiliate != null && !string.IsNullOrEmpty(this.Drivers.MyAffiliate.TimeZone)) {
		//            TimeZoneConverter oTime = new TimeZoneConverter();
		//            return oTime.FromUniversalTime(this.Drivers.MyAffiliate.TimeZone, this.GPSAcquiredTime.Value);
		//        }
		//        return this.GPSAcquiredTime.Value.ToLocalTime();
		//    }
		//}

		//public DateTime? LocalGPSDateTime {
		//    get {
		//        if (this.GPSTime.HasValue) {
		//            if (this.Drivers.MyAffiliate != null && !string.IsNullOrEmpty(this.Drivers.MyAffiliate.TimeZone)) {
		//                TimeZoneConverter oTime = new TimeZoneConverter();
		//                return oTime.FromUniversalTime(this.Drivers.MyAffiliate.TimeZone, this.GPSTime.Value);
		//            } else {
		//                return this.GPSTime.Value.ToLocalTime();
		//            }
		//        }
		//        return this.GPSTime;
		//    }
		//}

		//public string LocalMeterStatusDateTimeString {
		//    get {
		//        string sTime = "";
		//        if (this.MeterStatusTime.HasValue) {
		//            if (this.Drivers.MyAffiliate != null && !string.IsNullOrEmpty(this.Drivers.MyAffiliate.TimeZone)) {
		//                TimeZoneConverter oTime = new TimeZoneConverter();
		//                sTime = oTime.FromUniversalTime(this.Drivers.MyAffiliate.TimeZone, this.MeterStatusTime.Value).ToString("MM/dd/yyyy hh:mm tt");
		//            } else {
		//                sTime = this.MeterStatusTime.Value.ToLocalTime().ToString("MM/dd/yyyy hh:mm tt");
		//            }
		//        }
		//        return sTime;
		//    }
		//}

		//public DateTime? LocalMeterStatusDateTime {
		//    get {
		//        if (this.MeterStatusTime.HasValue) {
		//            if (this.Drivers.MyAffiliate != null && !string.IsNullOrEmpty(this.Drivers.MyAffiliate.TimeZone)) {
		//                TimeZoneConverter oTime = new TimeZoneConverter();
		//                return oTime.FromUniversalTime(this.Drivers.MyAffiliate.TimeZone, this.MeterStatusTime.Value);
		//            } else {
		//                return this.MeterStatusTime.Value.ToLocalTime();
		//            }
		//        }
		//        return this.GPSTime;
		//    }
		//}

		public bool IsCellTowerReading() {
			if (this.LocationMethod == null) {
				return false;
			}
			return (!this.LocationMethod.StartsWith("Sat", StringComparison.CurrentCultureIgnoreCase) && !this.LocationMethod.Equals("327681"));
		}


		public void FillAddress() {

			if (this.Latitude.GetValueOrDefault(0) != 0 && this.Longitude.GetValueOrDefault(0) != 0) {
				SystemDefaultsDict dict = SystemDefaultsDict.GetByKey(this.PersistenceManager, "GeoCodes", "PopulateAddress", false);
				if (Convert.ToBoolean(dict.ValueString)) {
					//SystemDefaults oDefaults = SystemDefaults.GetDefaults(this.PersistenceManager);
					//TaxiPassCommon.GeoCoding.MapPointService oService = new TaxiPassCommon.GeoCoding.MapPointService(oDefaults.MapPointUserName, oDefaults.MapPointPassword);
					//this.Address = oService.GetAddressFromGeoCode(this.Latitude.Value, this.Longitude.Value);
					try {
						new Thread(new ThreadStart(() => {
							try {
								com.TaxiPass.GeoCodeWS.GeoCodeService oService = new CabRideEngine.com.TaxiPass.GeoCodeWS.GeoCodeService();
								com.TaxiPass.GeoCodeWS.Address oAddr = oService.GetAddressByGeoCode(this.Latitude.Value.ToString(), this.Longitude.Value.ToString());
								this.Address = oAddr.Street;
								if (oAddr.Distance > 0) {
									this.Address += " within " + oAddr.Distance + " feet";
								}
								this.Save();
							} catch {
							}
						})).Start();
					} catch {
					}
				}
			}
		}

		public override SaveResult Save(string sModifiedBy) {

			try {
				if (string.IsNullOrEmpty(this.Address)) {
					FillAddress();
				}
				DriversGPSHistory oRec = null;
				TimeZoneConverter convert = new TimeZoneConverter();
				try {
					if (this.GPSTime.HasValue && !this.Drivers.MyAffiliate.ExcludeGPSHistory) {
						oRec = DriversGPSHistory.Create(this.Drivers, this.GPSTime.Value);
						oRec.Address = this.Address;
						oRec.Direction = this.Direction;
						oRec.Distance = this.Distance;
						oRec.Latitude = this.Latitude;
						oRec.LocationMethod = this.LocationMethod;
						oRec.Longitude = this.Longitude;
						oRec.Message = this.Message;
						oRec.MeterStatus = this.MeterStatus;
						if (this.MeterStatusTime.HasValue) {
							oRec.MeterStatusTime = this.MeterStatusTime.Value;
						}
						oRec.ModifiedBy = this.ModifiedBy;
						oRec.ModifiedDate = convert.ConvertTime(DateTime.Now, TimeZone.CurrentTimeZone.StandardName, this.Drivers.MyAffiliate.TimeZone); // DateTime.Now.oUniversalTime();
						oRec.Result = this.Result;
						oRec.Speed = this.Speed;
						if (this.GPSAcquiredTime.HasValue) {
							oRec.GPSAcquiredTime = this.GPSAcquiredTime.Value;
						}
						oRec.AffiliateDriverID = this.AffiliateDriverID;

						oRec.Save(sModifiedBy);
					}
				} catch {
					if (oRec != null) {
						oRec.RemoveFromManager();
					}
				}

			} catch {
			}
			return base.Save(sModifiedBy);
		}
	}

	#region EntityPropertyDescriptors.DriversGPSLocationPropertyDescriptor
	////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
	////    For example: 
	////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
	//public static partial class EntityPropertyDescriptors {
	//	public partial class DriversGPSLocationPropertyDescriptor : BaseEntityPropertyDescriptor {
	//    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
	//    public AdaptedPropertyDescriptor Age {
	//      get { return this.Get("Age"); }
	//    }
	//  }
	//} 
	#endregion

}
