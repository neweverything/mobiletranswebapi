﻿using CabRideEngineDapper.Utils;
using MobileTransWebAPI.Models;
using System;
using System.Web.Http;
using System.Web.Script.Serialization;
using TaxiPassCommon;

namespace MobileTransWebAPI.Controllers {
	public class LatestDriverInfoController : ApiController {

		public DriverInfo Get() {
			DriverProfile prof = Newtonsoft.Json.JsonConvert.DeserializeObject<DriverProfile>(System.IO.File.ReadAllText(@"d:\temp\t.json"));
			prof.RequestKey = DateTime.UtcNow.ToString();

			return GetLatestDriverInfo(Newtonsoft.Json.JsonConvert.SerializeObject(prof).EncryptIceKey());
		}

		public DriverInfo GetLatestDriverInfo(string pRequest) {
			DriverInfo driverInfo = new DriverInfo();
			try {
				JavaScriptSerializer js = new JavaScriptSerializer();

				pRequest = pRequest.DecryptIceKey();
				WebApiApplication.LogIt("GetLatestDriverInfo", pRequest);

				DriverProfile info = js.Deserialize<DriverProfile>(pRequest);

				DateTime requestDate = DateTime.Today.AddDays(-1);
				if (info != null) {
					requestDate = info.RequestKey.DecryptIceKey().ToDateTime();
				}

				if (WebApiApplication.GetUTCServerTime().Subtract(requestDate).TotalMinutes > 1) {
					driverInfo.ErrorMsg = "Invalid Access";
					driverInfo.DriverPin = StringUtils.GetUniqueKey(4);
					driverInfo.TaxiPayPin = driverInfo.DriverPin;
					return driverInfo;
				}

				//PersistenceManager oPM = WebApiApplication.UserPM;
				//if (oPM == null) {
				//	driverInfo.ErrorMsg = "Could not connect to database";
				//	return driverInfo;

				//}

				using (var conn = SqlHelper.OpenSqlConnection()) {
					CabRideEngineDapper.Drivers driver = CabRideEngineDapper.Drivers.GetDriver(conn, info.DriverID);
					CabRideEngineDapper.AffiliateDrivers affDriver = CabRideEngineDapper.AffiliateDrivers.GetDriver(conn, info.AffiliateDriverID.GetValueOrDefault(0));

					//if (driver.IsNullEntity || affDriver.Cell != driver.Cell) {
					//    driverInfo.ErrorMsg = "Invalid Access";
					//    driverInfo.DriverPin = StringUtils.GetUniqueKey(4);
					//    driverInfo.TaxiPayPin = driverInfo.DriverPin;
					//    return driverInfo;
					//}


					driverInfo = Utils.Common.GetDriverInfo(conn, driverInfo, driver, affDriver, info.AppName);

				}
			} catch (Exception ex) {
				WebApiApplication.LogIt("GetLatestDriverInfo error", ex.Message);
			}
			return driverInfo;
		}

	}
}
