﻿using CabRideEngineDapper;
using CabRideEngineDapper.Utils;
using Dapper;
//using IdeaBlade.Persistence;
//using IdeaBlade.Persistence.Rdb;
using MobileTransWebAPI.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Http;
using TaxiPassCommon;
using TaxiPassCommon.Banking;
using TaxiPassCommon.Banking.Enums;
using TaxiPassCommon.Banking.Interfaces;

namespace MobileTransWebAPI.Controllers {

	public class FundPayCardController : ApiController {

		public FundsTransferResponse Get() {
			//try {
			//	List<string> lines = System.IO.File.ReadAllLines(@"d:\temp\FundPayCard_20180426").ToList();
			//	foreach (string data in lines) {
			//		string temp = data.Split(':').Last().Trim();
			//		AffDriverLoginInfo info = JsonConvert.DeserializeObject<AffDriverLoginInfo>(temp.Decrypt());
			//		if (info.Phone == "9084166165") {
			//			string js = JsonConvert.SerializeObject(info);
			//			Console.WriteLine(js);
			//			FundPayCard(temp);
			//		}
			//	}
			//} catch (Exception ex) {
			//	Console.WriteLine(ex.Message);
			//}

			return FundPayCard("0970A8836BE4AA7A94B01EDB0D967289EB49A6E732F909F6328B98D6A413543E691F40F4CF2717D2DCD4336C6A06056570BB9B4E011BB1CD5E6C2339185E61EA1B8294B84F5A22BAB98879D21E0971DE1360AC7D8F4F04CAC57F700C0B68DA510164E691951BCE5169B0B63F6140F5344D33EABB5F85E8095E855D780DA95DCA304125262D6653098125891352CA43C8ECA92CD429CDC67C6385C6D8B5D8CEB84E9919D73F65F0650E034C395E6C271B98611EBD1BDDA235DF52F9435E65DC294C371F4FFF6E7EF87468DEDCB1112082AFAE61D4E6B7BE36C268F54FD7D4F322B63E22ACAA6E1AB5DF52F9435E65DC29BFA8027684C4363AC2D117D164F9D48F7EDBCE89D721B398134597D787EA6FFD070B3A237C764B2F8399A869C04F15E5361B6A5FCB8BF0E2C4D585721336FAA90486B1401FF1FD2BBFCEA838CE35DB0F2152C8168FFA1BD58EC01BC6816969ADCA5F32BFF6BA85A31E040F051653A2F8964127E3CD434E6B9752DE30AD4478A413127F9D93244A7EF0203278433AA23CCDEE30524BF8A77D9E356CE74DD7F8AC91104F22D3FB73B97F24E5C81A7AE8AE4E9919D73F65F065BBFB3D9714427E5DB635C3EFF66E390C20873AF0");
		}

		[HttpGet]
		public FundsTransferResponse FundPayCard(string pRequest) {
			FundsTransferResponse fundResults = new FundsTransferResponse();

			if (!Utils.ActionValidator.IsValid(Utils.ActionValidator.ActionTypeEnum.None)) {
				fundResults.ReturnCode = "-10";
				fundResults.ReturnMessage = "Pay card funding in progress";
				return fundResults;
			}

			WebApiApplication.LogIt("FundPayCard:", pRequest, "FundPayCard_{0}");

			AffDriverLoginInfo info = new AffDriverLoginInfo();
			try {
				using (var conn = SqlHelper.OpenSqlConnection()) {
					string request = pRequest.DecryptIceKey();
					info = JsonConvert.DeserializeObject<AffDriverLoginInfo>(request);

					DateTime requestDate = Convert.ToDateTime(info.RequestTime);

					if (WebApiApplication.GetUTCServerTime().Subtract(requestDate).TotalMinutes > 1) {
						fundResults.ReturnMessage = "Invalid Access";
						return fundResults;
					}

					if (!info.Phone.IsNullOrEmpty()) {

						AffiliateDrivers pPayeeDriver = AffiliateDrivers.GetDriverByCallInCell(conn, info.Phone);

						if (pPayeeDriver.IsNullEntity || pPayeeDriver.RedeemerStopPayment || pPayeeDriver.TransCardNumber.IsNullOrEmpty()) {
							fundResults.ReturnCode = "-1";
							fundResults.ReturnMessage = "You cannot get paid at this time, please contact TaxiPass to resolve";
							return fundResults;
						}

						int missedDejavoo = FundingPayCardInfo.GetMissedDejavooBatchCount(pPayeeDriver.AffiliateDriverID);
						if (missedDejavoo > 0) {
							fundResults.ReturnCode = "-1";
							fundResults.ReturnMessage = "Please settle batch or bring credit card machine to trailer for payment";
							return fundResults;
						}

						List<DriverPayments> payList = GetPayList(conn, pPayeeDriver.AffiliateDriverID);
						if (payList.Count == 0) {
							fundResults.ReturnCode = "-1";
							fundResults.ReturnMessage = "No vouchers available for funding";
							return fundResults;
						}

						if (pPayeeDriver.PayCardType.IsNullOrEmpty()) {
							pPayeeDriver.PayCardType = PayCardTypes.USBank.ToString();
							pPayeeDriver.Save();
						}

						List<SystemDefaultsDict> payCardDefaults = SystemDefaultsDict.GetGroup(conn, "PayCard", false);
						string payCardType = payCardDefaults.FirstOrDefault(p => p.ReferenceKey.Equals("ForceUpgradeTo", StringComparison.CurrentCultureIgnoreCase)).ValueString;
						if (!payCardType.IsNullOrEmpty() && payCardType != pPayeeDriver.PayCardType) {
							fundResults.ReturnCode = "-1";
							fundResults.ReturnMessage = payCardDefaults.FirstOrDefault(p => p.ReferenceKey.Equals("UpgradeMessage", StringComparison.CurrentCultureIgnoreCase)).ValueString;
							return fundResults;
						}


						bool passAppTest = true;
						bool checkAppVersion = SystemDefaultsDict.GetByKey(conn, "PayCard", "CheckAppVersion", false).ValueString.AsBool();
						if (checkAppVersion) {
							if (("" + info.AppName).Equals("TaxiPass EWR")) {
								if (info.VersionNo.StartsWith("1.")) {
									decimal vers = info.VersionNo.Substring(2).AsDecimal();
									if (vers < 3.60M) {
										passAppTest = false;
									}
								}
							} else if (("" + info.AppName).Equals("GetRidePayments")) {
								if (info.VersionNo.StartsWith("3.")) {
									decimal vers = info.VersionNo.Substring(2).AsDecimal();
									if (info.HardwareOS == "Android") {
										if (vers < 10.66M) {
											passAppTest = false;
										}
									} else {
										if (vers < 10.60M) {
											passAppTest = false;
										}
									}
								} else {
									int verStart = info.VersionNo.Split('.').First().AsInt();
									if (verStart < 3) {
										passAppTest = false;
									}
								}
							}
							if (!passAppTest) {
								fundResults.ReturnCode = "99";
								fundResults.ReturnMessage = "Please update app in order to fund your pay card";
								return fundResults;
							}
						}


						// Ensure only 1 funding request goes through 
						PayCardProcessing payCardProcessing = PayCardProcessing.GetNullEntity();
						try {
							payCardProcessing = PayCardProcessing.AddTransCardAdminNo(conn, pPayeeDriver.TransCardAdminNo);
							if (payCardProcessing.PayCardProcessingID <= 0) {
								fundResults.ReturnCode = "99";
								fundResults.ReturnMessage = "Error Adding Card Info";
								return fundResults;
							}
						} catch (Exception) {
							fundResults.ReturnCode = "99";
							fundResults.ReturnMessage = "Too soon to fund your pay card, try again in an hour";
							return fundResults;
						}

						var lastProcessed = PayCardProcessing.GetLastFunding(conn, pPayeeDriver.TransCardAdminNo, payCardProcessing.PayCardProcessingID);
						if (!lastProcessed.IsNullEntity && payCardProcessing.CreatedDate.Subtract(lastProcessed.CreatedDate).TotalMinutes < 30) {
							fundResults.ReturnCode = "99";
							fundResults.ReturnMessage = "Too soon to fund your pay card, try again in an hour";
							return fundResults;
						}


						decimal decAmount = 0;

						IPayCardCredentials cred = new FSV.Credentials();
						AffiliateDriverRef rec;
						if (pPayeeDriver.PayCardType.IsNullOrEmpty()) {
							pPayeeDriver.PayCardType = PayCardTypes.USBank.ToString();
						}
						CabRideEngineDapper.PayCardInfo fsvInfo = CabRideEngineDapper.PayCardInfo.GetByType(conn, pPayeeDriver.PayCardType);
						if (fsvInfo.PayCardType.Equals(PayCardTypes.TransCard.ToString(), StringComparison.CurrentCultureIgnoreCase)) {
							TransCardDefaults transCardDefaults = TransCardDefaults.GetDefaults(fsvInfo.UseProduction ? fsvInfo.PassCode.ToLong() : fsvInfo.TestPassCode.ToLong());
							cred = JsonConvert.DeserializeObject<TransCard.Credentials>(JsonConvert.SerializeObject(transCardDefaults));
						} else {
							cred = JsonConvert.DeserializeObject<FSV.Credentials>(JsonConvert.SerializeObject(fsvInfo)); //  conn.Query<FSV.Credentials>("usp_SystemDefaultsDict_Flattened", new { ReferenceGroup = "FSV" }, commandType: CommandType.StoredProcedure).FirstOrDefault();
						}
						rec = AffiliateDriverRef.GetOrCreate(conn, pPayeeDriver.AffiliateDriverID, "FSV", "Registration");


						PayCardGateway payCardGateWay = new PayCardGateway(cred);
						FSV.VerifyConnectionResponse verifyResponse = payCardGateWay.VerifyConnection();
						if (verifyResponse.ReturnCode != "1") {
							fundResults.ReturnCode = "-1";
							fundResults.ReturnMessage = "Could not verify connection to USBank: " + verifyResponse.ReturnMessage;
							return fundResults;
						}


						FundPayCardSelected fundInfo = new FundPayCardSelected {
							Phone = info.Phone,
							TaxiPassRedeemerID = 0, //taxipassRedeemerID,
							UserID = info.UserID
						};


						FSV.CardRegistrationResponse resp = JsonConvert.DeserializeObject<FSV.CardRegistrationResponse>(rec.ValueString);
						FSV.FundsTransfer transfer = new FSV.FundsTransfer(cred);
						transfer.ToCardID = resp.CardID;

						decAmount = FundPayCardSelectedVouchersController.ProcessPayments(fundResults, conn, fundInfo, pPayeeDriver, payList, decAmount, cred, transfer);

					}
				}
			} catch (Exception ex) {
				fundResults.ReturnCode = "-1";
				fundResults.ReturnMessage = ex.Message;
			}

			return fundResults;
		}

		public List<DriverPayments> GetPayList(SqlConnection pConn, long pAffiliateDriverID) {
			string sql = @"DECLARE @LookBackDays INT = (SELECT CONVERT(INT, ValueString)
														 FROM   SystemDefaultsDict
														 WHERE  ReferenceGroup = 'PayCard'
																AND ReferenceKey = 'PayDriverChargeDateMinusDays'
														)

                          DECLARE @MaxAge INT = (SELECT CONVERT(INT, ValueString)
                                                         FROM   SystemDefaultsDict
                                                         WHERE  ReferenceGroup = 'PayCard'
                                                                AND ReferenceKey = 'MaxAge'
                                                        )
							SELECT  DISTINCT
									DriverPayments.*
							FROM    dbo.DriverPayments
							LEFT JOIN ACHDetail ON ACHDetail.DriverPaymentID = DriverPayments.DriverPaymentID
							LEFT JOIN DriverPaymentAux ON DriverPaymentAux.DriverPaymentID = DriverPayments.DriverPaymentID
							WHERE   AffiliateDriverID = @AffiliateDriverID
									AND ACHPaidDate IS NULL
									AND Failed = 0
									AND Test = 0
									AND DATEADD(DAY, @LookBackDays, ChargeDate) < GETUTCDATE()
									AND ChargeDate >= '1/16/2016'
                                    AND ChargeDate > DATEADD(DAY, -@MaxAge, GETUTCDATE())
									AND ISNULL(DriverPayments.DoNotPay, 0) = 0 
									AND ISNULL(DriverPaymentAux.DoNotPay, 0) = 0
									AND TaxiPassRedeemerID IS NULL";

			//PassthruRdbQuery qry = new PassthruRdbQuery(typeof(DriverPayments), string.Format(sql, pAffiliateDriverID));
			//return pPM.GetEntities<DriverPayments>(qry);
			return pConn.Query<DriverPayments>(sql, new { AffiliateDriverID = pAffiliateDriverID }).ToList();
		}
	}
}