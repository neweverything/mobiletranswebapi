using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

using Csla.Validation;

namespace CabRideEngine {
	/// <summary>
	/// The developer class representing the DomainName business object class.
	/// </summary>
	/// <remarks>
	/// <para>Place all application-specific business logic in this class.</para>
	/// <para>
	/// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
	/// and will not overwrite changes.
	/// </para>
	/// <para>
	/// This class inherits from the generated class, its companion in the pair of classes 
	/// defining a business object. Properties in the generated class may be overridden in 
	/// order to change the behavior or add business logic.
	/// </para>
	/// <para>
	/// This class is the final class in the inheritance chain for this business object. 
	/// It should not be inherited.
	/// </para>
	/// </remarks>
	[Serializable]
	public sealed partial class DomainName : DomainNameDataRow {

		#region Constructors -- DO NOT MODIFY

		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new';
		// it is also required if the class is visible from a web service.
		// DO NOT REMOVE **
		private DomainName() : this(null) { }

		/// <summary>
		/// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
		/// </summary>
		/// <param name="pRowBuilder"></param>
		public DomainName(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static DomainName Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      DomainName aDomainName = pManager.CreateEntity<DomainName>();
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aDomainName, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aDomainName.AddToManager();
		//      return aDomainName;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		// Add additional logic to your business object here...
		public static DomainName Create(PersistenceManager pPM) {
			DomainName rec = null;
			try {
				rec = pPM.CreateEntity<DomainName>();
				rec.AddToManager();

			} catch (Exception ex) {
				throw ex;
			}
			return rec;
		}

		protected override void AddRules(RuleList pList) {
			PropertyRequiredRule.AddToList(pList, NameEntityColumn.ColumnName);
			pList.Add(IsNameUnique, NameEntityColumn.ColumnName);
		}

		private bool IsNameUnique(object pTarget, RuleArgs e) {
			DomainName domain = (DomainName)pTarget;

			RdbQuery qry = new RdbQuery(typeof(DomainName), DomainName.NameEntityColumn, EntityQueryOp.EQ, domain.Name);
			qry.AddClause(DomainName.DomainNameIDEntityColumn, EntityQueryOp.NE, domain.DomainNameID);
			EntityList<DomainName> typeList = this.PersistenceManager.GetEntities<DomainName>(qry);
			if (typeList.Count > 0) {
				e.Description = "Domain Name must be unique!";
				return false;
			}
			return true;
		}

		[BindingBrowsable(false)]
		public override string EntityInstanceDisplayName {
			get {
				return Name;
			}
		}

		protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
			if (IsDeserializing)
				return;
			base.OnColumnChanging(pArgs);
			DoAudit(pArgs);
		}

		private void DoAudit(DataColumnChangeEventArgs pArgs) {
			if (!this.PersistenceManager.IsClient) {
				return;
			}
			if (this.RowState == DataRowState.Added) {
				return;
			}
			if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
				AuditRecord = DomainNameAudit.Create(this);
			}
		}

		public static EntityList<DomainName> GetAll(PersistenceManager pPM) {
			RdbQuery qry = new RdbQuery(typeof(DomainName));
			qry.AddOrderBy(NameEntityColumn);
			return pPM.GetEntities<DomainName>(qry);
		}

		public static DateTime GetLastUpdated(PersistenceManager pPM) {
			RdbQuery qry = new RdbQuery(typeof(DomainName));
			qry.AddOrderBy(DomainName.ModifiedDateEntityColumn, System.ComponentModel.ListSortDirection.Descending);
			qry.Top = 1;

			return pPM.GetEntity<DomainName>(qry).ModifiedDate;
		}

	}

	#region EntityPropertyDescriptors.DomainNamePropertyDescriptor
	////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
	////    For example: 
	////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
	//public static partial class EntityPropertyDescriptors {
	//	public partial class DomainNamePropertyDescriptor : BaseEntityPropertyDescriptor {
	//    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
	//    public AdaptedPropertyDescriptor Age {
	//      get { return this.Get("Age"); }
	//    }
	//  }
	//} 
	#endregion

}