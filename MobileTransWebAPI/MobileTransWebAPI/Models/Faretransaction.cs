﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MobileTransWebAPI.Models {

	public class Faretransaction {

		[DataType(DataType.Text)]
		[StringLength(500)]
		public string Token { get; set; }

		[Required]
		public int MerchantID { get; set; }

		[Required]
		[DataType(DataType.Text)]
		[StringLength(45)]
		public string MerchantName { get; set; }

		public decimal? FareAmount { get; set; }

		public int? DistanceTravelled { get; set; }

		public decimal? TollAmount { get; set; }

		public decimal? TipAmount { get; set; }

		public decimal? DefaultTipPercentage { get; set; }

		public decimal TotalAmount { get; set; }

		public DateTime? TransactionDate { get; set; }
		public decimal? ServiceFee { get; set; }
		public int StatusID { get; set; }
		public DateTime? LastUpdatedStatusDate { get; set; }
		public string VendorToken { get; set; }
		public string CardNumber { get; set; }
	}

	public class FaretransactionResponse {
		public decimal? TipAmount { get; set; }
		public decimal? ServiceFee { get; set; }
		public decimal? TotalAmount { get; set; }
		public string StatusName { get; set; }
		public string VendorToken { get; set; }
		public string CardNumber { get; set; }
		public DateTime LastUpdatedDate { get; set; }
	}
}