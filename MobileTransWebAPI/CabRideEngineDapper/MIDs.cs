﻿using Dapper;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace CabRideEngineDapper {
    public partial class MIDs {

        public static List<MIDs> GetMIDs(SqlConnection pConn) {
            string sql = @"SELECT * FROM MIDs
                            ORDER BY Description";

            return pConn.Query<MIDs>(sql).ToList();
        }

        public static MIDs GetOrCreate(SqlConnection pConn, string pMID) {

            MIDs rec = GetMID(pConn, pMID);
            if (rec.IsNullEntity) {
                rec = Create();
            }
            return rec;
        }

        public static MIDs GetMID(SqlConnection pConn, string pMID) {
            string sql = @"SELECT * FROM MIDs
                            WHERE MID = @MID";

            MIDs rec = pConn.Get<MIDs>(sql, new { MID = pMID });
            return rec;
        }

        public static MIDs GetByID(SqlConnection pConn, int pID) {
            string sql = @"SELECT *
                            FROM   MIDs
                            WHERE  MidID = @ID";

            return pConn.Get<MIDs>(sql, new { ID = pID });
        }


    }
}

