﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Util;
using IdeaBlade.Persistence.Rdb;

using TaxiPassCommon;

using Csla.Validation;
using Newtonsoft.Json;
using TaxiPassCommon.Banking;

namespace CabRideEngine {
	/// <summary>
	/// The developer class representing the VerisignAccounts business object class.
	/// </summary>
	/// <remarks>
	/// <para>Place all application-specific business logic in this class.</para>
	/// <para>
	/// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
	/// and will not overwrite changes.
	/// </para>
	/// <para>
	/// This class inherits from the generated class, its companion in the pair of classes 
	/// defining a business object. Properties in the generated class may be overridden in 
	/// order to change the behavior or add business logic.
	/// </para>
	/// <para>
	/// This class is the final class in the inheritance chain for this business object. 
	/// It should not be inherited.
	/// </para>
	/// </remarks>
	[Serializable]
	[JsonObject(IsReference = true)]
	public sealed partial class VerisignAccounts : VerisignAccountsDataRow {

		#region Constructors -- DO NOT MODIFY

		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new';
		// it is also required if the class is visible from a web service.
		// DO NOT REMOVE **
		private VerisignAccounts() : this(null) {
		}

		/// <summary>
		/// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
		/// </summary>
		/// <param name="pRowBuilder"></param>
		public VerisignAccounts(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static VerisignAccounts Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      VerisignAccounts aVerisignAccounts = pManager.CreateEntity<VerisignAccounts>();
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aVerisignAccounts, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aVerisignAccounts.AddToManager();
		//      return aVerisignAccounts;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		public const string ENCRYPT_KEY = "VeRiSiGn";

		// Add additional logic to your business object here...
		public static VerisignAccounts Create(PersistenceManager pPM) {

			VerisignAccounts rec = pPM.CreateEntity<VerisignAccounts>();

			pPM.GenerateId(rec, VerisignAccounts.VerisignAccountIDEntityColumn);
			rec.Salt = StringUtils.GenerateSalt();
			rec.ValidateUSAePay = true;
			rec.ValidateVerisign = false;
			rec.AddToManager();
			return rec;
		}

		protected override void AddRules(RuleList pList) {
			PropertyRequiredRule.AddToList(pList, VerisignAccounts.AccountEntityColumn.ColumnName);
			PropertyRequiredRule.AddToList(pList, VerisignAccounts.VerisignPartnerEntityColumn.ColumnName);
			PropertyRequiredRule.AddToList(pList, VerisignAccounts.VerisignPasswordEntityColumn.ColumnName);
			PropertyRequiredRule.AddToList(pList, VerisignAccounts.VerisignUserNameEntityColumn.ColumnName);
			PropertyRequiredRule.AddToList(pList, VerisignAccounts.VersignVendorEntityColumn.ColumnName);
			pList.Add(IsAccountUnique, AccountEntityColumn.ColumnName);
		}

		private bool IsAccountUnique(object pTarget, RuleArgs e) {
			VerisignAccounts anEntity = pTarget as VerisignAccounts;

			RdbQuery qry = new RdbQuery(typeof(VerisignAccounts), VerisignAccounts.AccountEntityColumn, EntityQueryOp.EQ, anEntity.Account);
			qry.AddClause(VerisignAccounts.VerisignAccountIDEntityColumn, EntityQueryOp.NE, anEntity.VerisignAccountID);
			EntityList<VerisignAccounts> oList = this.PersistenceManager.GetEntities<VerisignAccounts>(qry);
			if (oList.Count > 0) {
				e.Description = "Account must be unique!";
				return false;
			}
			return true;
		}

		[BindingBrowsable(false)]
		public override string EntityInstanceDisplayName {
			get {
				return Account;
			}
		}

		[JsonIgnore]
		public override string VerisignPassword {
			get {
				Encryption oDecrypt = new Encryption();
				string sPassword = "";
				if (!string.IsNullOrEmpty(base.VerisignPassword)) {
					sPassword = oDecrypt.Decrypt(base.VerisignPassword, ENCRYPT_KEY);
				}
				return sPassword;
			}
			set {
				Encryption oEncrypt = new Encryption();
				string sValue = oEncrypt.Encrypt(value, ENCRYPT_KEY);
				base.VerisignPassword = sValue;
			}
		}

		[JsonIgnore]
		public override string EPayPin {
			get {
				Encryption oDecrypt = new Encryption();
				string sPassword = "";
				if (!string.IsNullOrEmpty(base.EPayPin)) {
					sPassword = oDecrypt.Decrypt(base.EPayPin, ENCRYPT_KEY);
				}
				return sPassword;
			}
			set {
				Encryption oEncrypt = new Encryption();
				string sValue = oEncrypt.Encrypt(value, ENCRYPT_KEY);
				base.EPayPin = sValue;
			}
		}

		//public override string AprivaPin {
		//	get {
		//		return base.AprivaPin.DecryptIceKey();
		//	}
		//	set {
		//		base.AprivaPin = value.EncryptIceKey();
		//	}
		//}

		public static EntityList<VerisignAccounts> GetList(PersistenceManager pPM, bool bAddBlank) {
			EntityList<VerisignAccounts> oList = pPM.GetEntities<VerisignAccounts>();
			oList.ApplySort(VerisignAccounts.AccountEntityColumn.ColumnName, System.ComponentModel.ListSortDirection.Ascending,true);
			oList.Insert(0, pPM.GetNullEntity<VerisignAccounts>());
			return oList;
		}

		public static EntityList<VerisignAccounts> GetUSAePayAccountsToValidate(PersistenceManager pPM) {
			RdbQuery qry = new RdbQuery(typeof(VerisignAccounts), ValidateUSAePayEntityColumn, EntityQueryOp.EQ, true);
			qry.AddOrderBy(VerisignAccounts.VerisignAccountIDEntityColumn);
			return pPM.GetEntities<VerisignAccounts>(qry);
		}

		public static VerisignAccounts GetAccount(PersistenceManager pPM, long pVerisignAccountID) {
			RdbQuery qry = new RdbQuery(typeof(VerisignAccounts), VerisignAccountIDEntityColumn, EntityQueryOp.EQ, pVerisignAccountID);
			return pPM.GetEntity<VerisignAccounts>(qry);
		}

		public static VerisignAccounts GetDefaultAccount(PersistenceManager pPM) {
			RdbQuery qry = new RdbQuery(typeof(VerisignAccounts), DefaultAccountEntityColumn, EntityQueryOp.EQ, true);
			return pPM.GetEntity<VerisignAccounts>(qry);
		}


		//public Gateway GetGateway(TaxiPassCommon.Banking.CardProcessors pProcessor, string pType) {
		//    Gateway gway;
		//    switch (pProcessor) {
		//        case CardProcessors.DriverPromo:
		//            throw new Exception("Promo Card cannot be refunded");

		//        case CardProcessors.Verisign:
		//            gway = new Gateway(Utils.CreditCardUtils.CreateVerisignCredentials(this));
		//            break;

		//        case CardProcessors.USAePay:
		//            gway = new Gateway(Utils.CreditCardUtils.CreateUSAePayCredentials(this));
		//            break;

		//        case CardProcessors.Litle:

		//        default:
		//            throw new Exception(string.Format("{0} not implemented for {1}", pType, pProcessor.ToString()));
		//    }

		//    return gway;
		//}
	}

	#region EntityPropertyDescriptors.VerisignAccountsPropertyDescriptor
	////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
	////    For example: 
	////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
	//public static partial class EntityPropertyDescriptors {
	//	public partial class VerisignAccountsPropertyDescriptor : BaseEntityPropertyDescriptor {
	//    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
	//    public AdaptedPropertyDescriptor Age {
	//      get { return this.Get("Age"); }
	//    }
	//  }
	//} 
	#endregion

}
