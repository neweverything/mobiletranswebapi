﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileTransWebAPI.Models {
	public class TransListResult {
		public string ErrorMsg { get; set; }

		public string PaidTrans { get; set; }
		public string Trans { get; set; }
		public string PendingTrans { get; set; }

		public string UnPaidSwipe { get; set; }
		public string UnPaidManual { get; set; }

		public string MasterVoucher { get; set; }
	}
}