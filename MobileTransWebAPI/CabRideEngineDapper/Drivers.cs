﻿using CabRideEngineDapper.Utils;
using CabRideEngineDapperAudit;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using TaxiPassCommon;
//using System.ComponentModel.DataAnnotations;

namespace CabRideEngineDapper {
    public partial class Drivers {

        public const string CHARGE_CARD = "Charge Card";



        [Editable(false)]
        public DateTime SignUpDate {
            get {
                DateTime signUpDate = this.ModifiedDate.Value;
                try {

                    List<DriversAudit> auditList = DriversAudit.GetDriverHistory(this);
                    if (auditList.Count > 0) {
                        DateTime history = DriversAudit.GetDriverHistory(this).Min(p => p.ModifiedDate).Value;

                        if (history < signUpDate) {
                            signUpDate = history;
                        }
                    }
                } catch (Exception ex) {
                    Console.WriteLine(ex.Message);
                }
                return signUpDate;
            }
        }

        [Editable(false)]
        public string PasswordEncryptDecrypt {
            get {
                Encryption encrypt = new Encryption();
                return encrypt.Decrypt(Password, Salt);
            }
            set {
                Encryption encrypt = new Encryption();
                Password = encrypt.Encrypt(value, Salt);
            }
        }

        [Editable(false)]
        public decimal GetChargeMaxAmount {
            get {
                decimal nAmount = base.ChargeMaxAmount;
                if (nAmount < 1) {
                    nAmount = this.Affiliate.AffiliateDriverDefaults.ChargeMaxAmount;
                    if (nAmount < 1) {
                        nAmount = SystemDefaults.GetDefaults().DriverChargeMaxAmount;
                    }
                }
                //if no max defined set to a high number that probably will never occur
                if (nAmount == 0) {
                    nAmount = 100000;
                }
                return nAmount;
            }
        }

        [Editable(false)]
        public decimal GetChargeMaxSwipeAmount {
            get {
                decimal nAmount = base.ChargeMaxSwipeAmount;
                if (nAmount < 1) {
                    nAmount = this.Affiliate.AffiliateDriverDefaults.ChargeMaxSwipeAmount;
                    if (nAmount < 1) {
                        nAmount = SystemDefaults.GetDefaults().DriverChargeMaxSwipeAmount;
                    }
                }
                //if no max defined set to a high number that probably will never occur
                if (nAmount == 0) {
                    nAmount = 100000;
                }
                return nAmount;
            }
        }

        [Editable(false)]
        public bool TrackTrips {
            get {
                return this.HHTripLog || this.Affiliate.AffiliateDriverDefaults.HHTripLog;
            }
        }

        [Editable(false)]
        public bool FareFirst {
            get {
                return this.HHFareFirst || this.Affiliate.AffiliateDriverDefaults.HHFareFirst;
            }
        }


        [Editable(false)]
        public decimal DisplayDriverFeeMsgAmount {
            get {
                decimal amount = HHDisplayDriverFeeMsgAmount;
                if (amount < 1) {
                    amount = this.Affiliate.AffiliateDriverDefaults.HHDisplayDriverFeeMsgAmount;
                    if (amount < 1) {
                        amount = SystemDefaults.GetDefaults().HHDisplayDriverFeeMsgAmount;
                    }
                }

                return amount;
            }
        }

        [Editable(false)]
        public bool IsRegistered {
            get {
                CabDriverRef onBoard = CabDriverRef.GetRecord("CabRideEngine:Drivers", this.DriverID, "IsRegistered");
                return (!onBoard.IsNullEntity && Convert.ToBoolean(onBoard.ValueString));
            }

            set {
                if (this.DriverID < 0) {
                    this.Save(CabRideDapperSettings.LoggedInUser);
                }

                using (var conn = SqlHelper.OpenSqlConnection()) {
                    CabDriverRef rec = CabDriverRef.GetOrCreate(conn, "CabRideEngine:Drivers", this.DriverID, "IsRegistered");
                    rec.ValueString = Convert.ToString(value);
                    rec.Save(conn, CabRideDapperSettings.LoggedInUser);
                }
            }
        }

        [Editable(false)]
        public bool IsDriverAuthenticated {
            get {
                CabDriverRef rec = CabDriverRef.GetRecord("CabRideEngine:Drivers", this.DriverID, "DriverAuthenticated");
                return (!rec.IsNullEntity && Convert.ToBoolean(rec.ValueString));
            }

            set {
                if (this.DriverID < 0) {
                    this.Save(CabRideDapperSettings.LoggedInUser);
                }

                using (var conn = SqlHelper.OpenSqlConnection()) {
                    CabDriverRef rec = CabDriverRef.GetOrCreate(conn, "CabRideEngine:Drivers", this.DriverID, "DriverAuthenticated");
                    rec.ValueString = Convert.ToString(value);
                    rec.Save(conn, CabRideDapperSettings.LoggedInUser);
                }
            }
        }

        [Editable(false)]
        public string GetPassword {
            get {
                Encryption encrypt = new Encryption();
                return encrypt.Decrypt(Password, Salt);
            }
        }

        [Editable(false)]
        public bool HasPendingAuth {
            get {
                List<DriverPayments> oPayments = DriverPayments.GetPendingTransactions(this);
                return oPayments.Count > 0;
            }
        }

        [Editable(false)]
        public string FormattedCellPhone {
            get {
                return this.Cell.FormattedPhoneNumber();
            }
        }

        [Editable(false)]
        public string FormattedBoostPhone {
            get {
                return this.BoostPhone.FormattedPhoneNumber();
            }
        }

        [Editable(false)]
        public int GetMaxTrxPerDay {
            get {
                int maxTrx = base.MaxTrxPerDay;
                if (maxTrx < 1) {
                    //if (!this.MyAffiliate.IsNullEntity && this.MyAffiliate.AffiliateDriverDefaultses.Count > 0) {
                    maxTrx = this.MyAffiliate.AffiliateDriverDefaults.MaxTransactions;
                    //}
                    if (maxTrx < 1) {
                        maxTrx = SystemDefaults.GetDefaults().DriverMaxTransactions;
                    }
                }
                //if no max defined set to a high number that probably will never occur
                if (maxTrx == 0) {
                    maxTrx = 100000;
                }
                return maxTrx;
            }
        }

        [Editable(false)]
        public int GetCardChargedMaxNumber {
            get {
                int maxNumber = base.CardChargedMaxNumber;
                if (maxNumber < 1) {
                    //if (!this.MyAffiliate.IsNullEntity && this.MyAffiliate.AffiliateDriverDefaultses.Count > 0) {
                    maxNumber = this.MyAffiliate.AffiliateDriverDefaults.CardChargedMaxNumber;
                    //}
                    if (maxNumber < 1) {
                        maxNumber = SystemDefaults.GetDefaults().DriverCardChargedMaxNumber;
                    }
                }
                //if no max defined set to a high number that probably will never occur
                if (maxNumber == 0) {
                    maxNumber = 100000;
                }
                return maxNumber;
            }
        }

        [Editable(false)]
        public int GetMinTransTime {
            get {
                return SystemDefaults.GetDefaults().DriverMinTransTime;
            }
        }

        [Editable(false)]
        public int GetMaxTransPerWeek {
            get {
                int max = this.MaxTransPerWeek;
                if (max < 1) {
                    //if (!this.MyAffiliate.IsNullEntity && this.MyAffiliate.AffiliateDriverDefaultses.Count > 0) {
                    max = this.MyAffiliate.AffiliateDriverDefaults.MaxTransPerWeek;
                    //}
                    if (max < 1) {
                        max = SystemDefaults.GetDefaults().DriverMaxTransPerWeek;
                    }
                }
                //if no max defined set to a high number that probably will never occur
                if (max == 0) {
                    max = 100000;
                }
                return max;
            }
        }

        [Editable(false)]
        public int GetMaxTransPerWeekPerCard {
            get {
                int max = this.MaxTransPerWeekPerCard;
                if (max < 1) {
                    if (!this.MyAffiliate.IsNullEntity) { // && this.MyAffiliate.AffiliateDriverDefaultses.Count > 0) {
                        max = this.MyAffiliate.AffiliateDriverDefaults.MaxTransPerWeekPerCard;
                    }
                    if (max < 1) {
                        max = SystemDefaults.GetDefaults().DriverMaxTransPerWeekPerCard;
                    }
                }
                //if no max defined set to a high number that probably will never occur
                if (max == 0) {
                    max = 100000;
                }
                return max;
            }
        }

        [Editable(false)]
        public int GetMaxTransPerMonth {
            get {
                int max = this.MaxTransPerMonth;
                if (max < 1) {
                    if (!this.MyAffiliate.IsNullEntity) { // && this.MyAffiliate.AffiliateDriverDefaultses.Count > 0) {
                        max = this.MyAffiliate.AffiliateDriverDefaults.MaxTransPerMonth;
                    }
                    if (max < 1) {
                        max = SystemDefaults.GetDefaults().DriverMaxTransPerMonth;
                    }
                }
                //if no max defined set to a high number that probably will never occur
                if (max == 0) {
                    max = 100000;
                }
                return max;
            }
        }

        [Editable(false)]
        public int GetMaxTransPerMonthPerCard {
            get {
                int max = this.MaxTransPerMonthPerCard;
                if (max < 1) {
                    if (!this.MyAffiliate.IsNullEntity) { //&& this.MyAffiliate.AffiliateDriverDefaultses.Count > 0) {
                        max = this.MyAffiliate.AffiliateDriverDefaults.MaxTransPerMonthPerCard;
                    }
                    if (max < 1) {
                        max = SystemDefaults.GetDefaults().DriverMaxTransPerMonthPerCard;
                    }
                }
                //if no max defined set to a high number that probably will never occur
                if (max == 0) {
                    max = 100000;
                }
                return max;
            }
        }

        [Editable(false)]
        public int GetMaxDailyAmount {
            get {
                int maxAmount = this.MaxDailyAmount;
                if (maxAmount < 1) {
                    if (!this.MyAffiliate.IsNullEntity) { // && this.MyAffiliate.AffiliateDriverDefaultses.Count > 0) {
                        maxAmount = this.MyAffiliate.AffiliateDriverDefaults.MaxDailyAmount;
                    }
                    if (maxAmount < 1) {
                        maxAmount = SystemDefaults.GetDefaults().DriverMaxDailyAmount;
                    }
                }
                //if no max defined set to a high number that probably will never occur
                if (maxAmount == 0) {
                    maxAmount = 100000;
                }
                return maxAmount;
            }
        }

        [Editable(false)]
        public int GetMaxWeeklyAmount {
            get {
                int maxAmount = this.MaxWeeklyAmount;
                if (maxAmount < 1) {
                    if (!this.MyAffiliate.IsNullEntity) { // && this.MyAffiliate.AffiliateDriverDefaultses.Count > 0) {
                        maxAmount = this.MyAffiliate.AffiliateDriverDefaults.MaxWeeklyAmount;
                    }
                    if (maxAmount < 1) {
                        maxAmount = SystemDefaults.GetDefaults().DriverMaxWeeklyAmount;
                    }
                }
                //if no max defined set to a high number that probably will never occur
                if (maxAmount == 0) {
                    maxAmount = 100000;
                }
                return maxAmount;
            }
        }

        [Editable(false)]
        public int GetMaxMonthlyAmount {
            get {
                int maxAmount = this.MaxMonthlyAmount;
                if (maxAmount < 1) {
                    if (!this.MyAffiliate.IsNullEntity) { // && this.MyAffiliate.AffiliateDriverDefaultses.Count > 0) {
                        maxAmount = this.MyAffiliate.AffiliateDriverDefaults.MaxMonthlyAmount;
                    }
                    if (maxAmount < 1) {
                        maxAmount = SystemDefaults.GetDefaults().DriverMaxMonthlyAmount;
                    }
                }
                //if no max defined set to a high number that probably will never occur
                if (maxAmount == 0) {
                    maxAmount = 1000000;
                }
                return maxAmount;
            }
        }


        public int GetAuthLimit1() {
            if (this.MyAffiliate != null) {
                if (this.MyAffiliate.AffiliateDriverDefaults != null) {
                    //if (this.MyAffiliate.AffiliateDriverDefaultses.Count > 0) {
                    return this.MyAffiliate.AffiliateDriverDefaults.GetAuthLimit1();
                    //}
                }
            }
            return 75;
        }

        public int GetAuthLimit2() {
            if (this.MyAffiliate != null) {
                if (this.MyAffiliate.AffiliateDriverDefaults != null) {
                    //if (this.MyAffiliate.AffiliateDriverDefaultses.Count > 0) {
                    return this.MyAffiliate.AffiliateDriverDefaults.GetAuthLimit2();
                    //}
                }
            }
            return 100;
        }

        public int GetAuthLimit3() {
            if (ChargeMaxAmount != 0) {
                return Convert.ToInt32(ChargeMaxAmount);
            }
            if (this.MyAffiliate != null) {
                if (this.MyAffiliate.AffiliateDriverDefaults != null) {
                    //if (this.MyAffiliate.AffiliateDriverDefaultses.Count > 0) {
                    return this.MyAffiliate.AffiliateDriverDefaults.GetAuthLimit3();
                    //}
                }
            }
            return 150;
        }

        [Editable(false)]
        public DateTime? LastGPSTime {
            get {
                return this.DriversGPSLocation.GPSTime;
            }
        }

        private DateTime? mLastTransactionTime = null;
        [Editable(false)]
        public DateTime? LastTransactionTime {
            get {
                if (!mLastTransactionTime.HasValue) {
                    using (var conn = SqlHelper.OpenSqlConnection()) {
                        DateTime? date = DriverPayments.GetLastDriverPayment(conn, this.DriverID).ChargeDate;
                        TimeZoneConverter converter = new TimeZoneConverter();
                        if (date.HasValue && !this.MyAffiliate.TimeZone.IsNullOrEmpty()) {
                            date = converter.FromUniversalTime(this.MyAffiliate.TimeZone, date.Value);
                        }
                        mLastTransactionTime = date;
                    }
                }
                return mLastTransactionTime;
            }
        }

        [Editable(false)]
        public string MiddleName { get; set; }
        [Editable(false)]
        public string LastName { get; set; }

        [Editable(false)]
        public string FirstName {
            get {
                string firstName = "";

                if (this.Name.Contains(",")) {
                    List<string> nameList = this.Name.Split(',').ToList();
                    LastName = nameList[0];
                    for (int i = 1; i < nameList.Count; i++) {
                        if (nameList.Count - 1 == i) {
                            firstName = nameList[i];
                        } else {
                            MiddleName += nameList[i] + " ";
                        }
                    }
                } else {
                    List<string> nameList = this.Name.Split(' ').ToList();
                    firstName = nameList[0];
                    for (int i = 1; i < nameList.Count; i++) {
                        if (nameList.Count - 1 == i) {
                            LastName = nameList[i];
                        } else {
                            MiddleName += nameList[i] + " ";
                        }
                    }
                }
                return firstName;
            }
        }

        [Editable(false)]
        public bool GetRideDispatch {
            get {
                bool value = false;
                CabDriverRef oCDRef = CabDriverRef.GetRecord("CabRideEngine:Drivers", DriverID, "RideQ Dispatch Disabled");
                if (!oCDRef.IsNullEntity) {
                    if (oCDRef.ValueString == "1") {
                        value = true;
                    }
                }
                return value;
            }
        }

        [Editable(false)]
        public bool GetRideVIPDispatch {
            get {
                bool value = false;
                CabDriverRef oCDRef = CabDriverRef.GetRecord("CabRideEngine:Drivers", DriverID, "RideQ VIP Driver");
                if (!oCDRef.IsNullEntity) {
                    if (oCDRef.ValueString == "1") {
                        value = true;
                    }
                }
                return value;
            }
        }






        //public override long DriverID {
        //	get {
        //		return base.DriverID;
        //	}
        //	set {
        //		base.DriverID = value;
        //	}
        //}

        public static new Drivers Create() {
            SystemDefaults oDefaults = SystemDefaults.GetDefaults();

            Drivers rec = DriversTable.Create();

            rec.Salt = StringUtils.GenerateSalt();
            rec.Country = CabRideEngineDapper.Countries.GetDefaultCountry().Country;
            rec.MinPUTime = oDefaults.MinPickUpTime;

            return rec;
        }

        public static Drivers CreateAndroidDriver(SqlConnection pConn, AffiliateDrivers pAffDriver, string pAndroidCellNo) {
            Drivers driver = CreateAndroidBackSeat(pConn, pAffDriver, pAndroidCellNo);
            driver.InBackSeat = false;
            return driver;
        }

        public static Drivers CreateAndroidBackSeat(SqlConnection pConn, AffiliateDrivers pAffDriver, string pAndroidCellNo) {
            Drivers driver = Drivers.GetNullEntity();
            if (!pAndroidCellNo.Equals("CELL", StringComparison.CurrentCultureIgnoreCase)) {
                driver = Drivers.GetDriverByBoostPhone(pConn, pAndroidCellNo);
            }
            if (driver.IsNullEntity) {
                driver = Drivers.Create();
            }
            driver.ByPassValidation = true;

            if (pAffDriver.AffiliateID > 0) {
                driver.AffiliateID = pAffDriver.AffiliateID;
            }
            if (pAffDriver.IsNullEntity) {
                driver.Active = true;
            } else {
                driver.Name = pAffDriver.Name;
                driver.Street = pAffDriver.Street;
                driver.Suite = pAffDriver.Suite;
                driver.City = pAffDriver.City;
                driver.State = pAffDriver.State;
                driver.Country = pAffDriver.Country;
                driver.ZIPCode = pAffDriver.ZIPCode;
                driver.Cell = pAffDriver.Cell;
                driver.BoostPhone = pAndroidCellNo;
                driver.EMail = pAffDriver.EMail;
                if (driver.EMail.IsNullOrEmpty()) {
                    driver.EMail = driver.Cell;
                }
                driver.Active = pAffDriver.Active;
            }
            driver.SerialNo = "Android Signup";
            driver.InBackSeat = false;
            driver.HHFareFirst = true;
            driver.HHTripLog = false;
            driver.TrackSwipeCounts = true;

            driver.CardDataEntryType = ((int)DataEntryTypeEnum.SwipeAndManual).ToString();
            driver.TaxiMeter = ((int)TaxiMeterUserEntryType.UserEntryTypeEnum.VoucherMenuFare).ToString();
            driver.PrinterType = ((int)HandHeldPrinterTypes.HandHeldPrinterTypesEnum.HomeATM).ToString();
            try {
                driver.Save(CabRideDapperSettings.LoggedInUser);
            } catch (Exception) {

            }

            return driver;
        }

        public new bool Save(string pModifiedBy) {
            bool result = false;
            using (var conn = SqlHelper.OpenSqlConnection()) {
                if (BoostPhone.IsNullOrEmpty()) {
                    BoostPhone = Cell;
                }
                if (!Validate(conn)) {
                    return false;
                }
                ModifiedDate = DateTime.UtcNow;
                ModifiedBy = pModifiedBy;
                if (IsNewRecord) {
                    this.DriverID = NextID.GetNextKey(Drivers.TableDef.TABLE_NAME);
                    int? id = conn.Insert(this);

                    result = true;
                } else {
                    RowVersion++;
                    result = conn.Update(this, additionalWhere: "RowVersion = " + (RowVersion - 1)) > 0;
                }
            }
            return result;
        }

        [Editable(false)]
        public bool ByPassValidation { get; set; }

        [Editable(false)]
        public string ErrorMessage { get; set; }

        public bool Validate(SqlConnection pConn) {
            if (ByPassValidation) {
                return true;
            }

            ErrorMessage = "";

            if (Name.IsNullOrEmpty()) {
                ErrorMessage += "Name is Required\n";
            }
            if (Cell.IsNullOrEmpty()) {
                ErrorMessage += "Cell is Required\n";
            }
            if (BoostPhone.IsNullOrEmpty()) {
                ErrorMessage += "BoostPhone is Required\n";
            }

            IsCellUnique(pConn);
            IsNextelPhoneUnique(pConn);
            IsLicenseNoUnique(pConn);
            IsSerialNoUnique(pConn);

            return ErrorMessage.IsNullOrEmpty();

        }

        private bool IsCellUnique(SqlConnection pConn) {
            if (this.TaxiTronic || ByPassValidation) {
                return true;
            }


            if (Name.Contains("Chargeback") || Name.Equals(CHARGE_CARD, StringComparison.CurrentCultureIgnoreCase)) {
                return true;
            }

            if (Cell.IsNullOrEmpty()) {
                return false;
            }
            if (Cell.ToUpper() == "CELL") {
                return true;
            }
            string sql = "SELECT * FROM Drivers WITH (NOLOCK) WHERE Cell = @Cell AND DriverID != @DriverID";
            Drivers rec = pConn.Query<Drivers>(sql, new { Cell = this.Cell, DriverID = this.DriverID }).DefaultIfEmpty(Drivers.GetNullEntity()).FirstOrDefault();
            if (!rec.IsNullEntity) {
                ErrorMessage += "\nA driver already exists with the Cell Phone: " + rec.Cell.FormattedPhoneNumber() + " with DriverID: " + rec.DriverID;
                return false;
            }
            return true;
        }

        private bool IsNextelPhoneUnique(SqlConnection pConn) {
            if (this.TaxiTronic || ByPassValidation) {
                return true;
            }

            if (Name.Contains("Chargeback") || Name.Equals(CHARGE_CARD, StringComparison.CurrentCultureIgnoreCase)) {
                return true;
            }

            if (BoostPhone.IsNullOrEmpty()) {
                ErrorMessage += "\nBoostPhone number required";
                return false;
            }
            if (BoostPhone.ToUpper() == "CELL") {
                return true;
            }

            string sql = "SELECT * FROM Drivers WITH (NOLOCK) WHERE BoostPhone = @BoostPhone AND DriverID != @DriverID";
            Drivers rec = pConn.Query<Drivers>(sql, new { BoostPhone = this.BoostPhone, DriverID = this.DriverID }).DefaultIfEmpty(Drivers.GetNullEntity()).FirstOrDefault();
            if (!rec.IsNullEntity) {
                ErrorMessage += "\nA record already exists with the Nextel Phone: " + rec.BoostPhone.FormattedPhoneNumber();
                return false;
            }
            return true;
        }

        private bool IsLicenseNoUnique(SqlConnection pConn) {
            if (this.TaxiTronic || ByPassValidation) {
                return true;
            }

            Drivers oDriver = this;
            if (string.IsNullOrEmpty(oDriver.LicenseNo)) {
                return true;
            }
            if (oDriver.LicenseNo.ToUpper() == "AUTODRIVER") {
                return true;
            }
            string sql = "SELECT * FROM Drivers WITH (NOLOCK) WHERE LicenseNo = @LicenseNo AND DriverID != @DriverID";
            Drivers rec = pConn.Query<Drivers>(sql, new { LicenseNo = this.LicenseNo, DriverID = this.DriverID }).DefaultIfEmpty(Drivers.GetNullEntity()).FirstOrDefault();
            if (!rec.IsNullEntity) {
                ErrorMessage = "A driver already exists with this License No: " + oDriver.LicenseNo;
                return false;
            }
            return true;
        }

        private bool IsSerialNoUnique(SqlConnection pConn) {
            string serialNo = "" + this.SerialNo;
            if (this.TaxiTronic || ByPassValidation || serialNo.Equals("Android Signup", StringComparison.CurrentCultureIgnoreCase) || serialNo.Equals("Registered", StringComparison.CurrentCultureIgnoreCase)) {
                return true;
            }

            // allow Android and iPhone app too have more than 1 driver account for the serial no
            string version = "" + this.AppVersion;
            if (version.StartsWith("i", StringComparison.CurrentCultureIgnoreCase) || version.StartsWith("a", StringComparison.CurrentCultureIgnoreCase)) {
                version = version.Substring(1);
            }
            try {
                decimal vers = Convert.ToDecimal(version);

                if (!this.HardwareManufacturer.IsNullOrEmpty() && vers >= 2.5M) {
                    return true;
                }
            } catch (Exception) {
            }

            Drivers oDriver = this;
            if (string.IsNullOrEmpty(oDriver.SerialNo)) {
                return true;
            }
            string sql = @"select * from Drivers with (nolock)
							where  SerialNo = @SerialNo
								AND DriverID != @DriverID
								AND SerialNo != 'Android Signup'
								AND SerialNo != 'Registered'";
            Drivers rec = pConn.Query<Drivers>(sql, new { SerialNo = this.SerialNo, DriverID = this.DriverID }).DefaultIfEmpty(Drivers.GetNullEntity()).FirstOrDefault();
            if (!rec.IsNullEntity) {
                ErrorMessage = "A cell phone already exists with this Serial Number: " + oDriver.SerialNo;
                return false;
            }
            return true;
        }


        #region Get Records
        // **************************************************************************************
        // Look at phone #, SubNo, Serial No
        // **************************************************************************************
        public static Drivers GetDriverPhoneSubNoOrSerial(SqlConnection pConn, long pAffiliateID, string pText) {

            string sql = @"SELECT  *
                            FROM Drivers
                            WHERE AffiliateID = @AffiliateID
                                    AND Name = @Text
                                    OR Cell = @Text
                                    OR BoostPhone = @Text
                                    OR SubNo = @Text
                                    OR SerialNo = @Text";

            return pConn.Get<Drivers>(sql, new { AffiliateID = pAffiliateID, Text = pText });
        }


        public static Drivers GetDriverByCell(SqlConnection pConn, string pCellPhone) {
            string sql = "SELECT * FROM Drivers WHERE Cell = @CellPhone";
            return pConn.Get<Drivers>(sql, new { CellPhone = pCellPhone });
        }


        public static Drivers GetDriverByCell(SqlConnection pConn, string pCellPhone, bool pActive) {
            string sql = @"SELECT  *
                            FROM    Drivers
                            WHERE   Cell = @CellPhone
                                    AND Active = @Active";
            return pConn.Get<Drivers>(sql, new { CellPhone = pCellPhone, Active = pActive });
        }


        public static Drivers GetDriverByPhone(SqlConnection pConn, string pCellPhone) {
            string sql = @"SELECT  *
                            FROM    Drivers
                            WHERE   Cell = @Phone
                                    OR BoostPhone = @Phone";
            return pConn.Get<Drivers>(sql, new { Phone = pCellPhone });
        }

        private static Drivers GetDriverByBoostPhone(SqlConnection pConn, string pAndroidCellNo) {
            string sql = @"SELECT * FROM dbo.Drivers
							WHERE BoostPhone = @Phone";

            return pConn.Get<Drivers>(sql, new { Phone = pAndroidCellNo });
        }


        public static List<Drivers> GetDriversByName(SqlConnection pConn, string pName) {
            string sql = @"SELECT * FROM dbo.Drivers
							WHERE Name like @Name";

            return pConn.Query<Drivers>(sql, new { Name = pName + "%" }).ToList();
        }

        public static Drivers GetDriver(long pDriverID) {
            using (var conn = SqlHelper.OpenSqlConnection()) {
                return GetDriver(conn, pDriverID);
            }
        }

        public static Drivers GetDriver(SqlConnection pConn, long pDriverID) {
            return pConn.Get<Drivers>(pDriverID);
        }

        public static Drivers GetDriverIncludeAffiliate(SqlConnection pConn, long pDriverID) {
            string sql = @"SELECT * FROM dbo.Drivers
							LEFT JOIN dbo.Affiliate ON Affiliate.AffiliateID = Drivers.AffiliateID
							LEFT JOIN dbo.AffiliateDriverDefaults ON AffiliateDriverDefaults.AffiliateID = Affiliate.AffiliateID
							WHERE DriverID = @DriverID";

            var rec = pConn.Query<Drivers, Affiliate, AffiliateDriverDefaults, Drivers>(sql
                        , (drivers, aff, def) => { drivers.mAffiliate = aff; drivers.Affiliate.AffiliateDriverDefaults = def; return drivers; }
                        , new { DriverID = pDriverID }
                        , splitOn: "AffiliateID,AffiliateDriverDefaultID").DefaultIfEmpty(Drivers.GetNullEntity()).FirstOrDefault();



            rec.Affiliate.RowState = System.Data.DataRowState.Unchanged;
            rec.Affiliate.AffiliateDriverDefaults.RowState = System.Data.DataRowState.Unchanged;

            return rec;
        }

        public static List<Drivers> GetDrivers(long pAffiliateID) {
            string sql = "SELECT * FROM Drivers WHERE AffiliateID = @AffiliateID ORDER BY Name";
            using (var conn = SqlHelper.OpenSqlConnection()) {
                return conn.Query<Drivers>(sql, new { AffiliateID = pAffiliateID }).ToList();
            }
        }

        public static List<Drivers> GetDriversForDropDown(long pAffiliateID, bool pAddNullRecord = false, string pNullRecordText = "") {
            //            string sql = @"SELECT Name, DriverID FROM dbo.Drivers WITH (NOLOCK)
            //							ORDER BY Name";
            string sql = @"SELECT  DriverID
								  , Name
							FROM    dbo.Drivers
							WHERE   AffiliateID = @AffiliateID
									AND Name != 'Charge Card'
							ORDER BY Name";

            List<Drivers> recList = new List<Drivers>();
            using (var conn = SqlHelper.OpenSqlConnection()) {
                recList = conn.Query<Drivers>(sql, new { AffiliateID = pAffiliateID }).ToList();
            }
            if (pAddNullRecord) {
                Drivers nullRec = Drivers.GetNullEntity();
                nullRec.Name = pNullRecordText;
                nullRec.DriverID = 0;
                recList.Insert(0, nullRec);
            }
            return recList;
        }

        public static bool DriversHaveGPS(SqlConnection pConn, long pAffiliateID) {
            string sql = @"SELECT TOP (1) Drivers.DriverID FROM Drivers
							INNER JOIN DriversGPSLocation ON Drivers.DriverID = DriversGPSLocation.DriverID
							WHERE Drivers.AffiliateID = @AffiliateID"; // + pAffiliate.AffiliateID;

            return !pConn.Query<Drivers>(sql, new { AffiliateID = pAffiliateID }).DefaultIfEmpty(Drivers.GetNullEntity()).FirstOrDefault().IsNullEntity;
        }

        public static List<Drivers> GetCellPhones(Affiliate mAffiliate) {
            return GetCellPhones(mAffiliate.AffiliateID);
        }

        public static List<Drivers> GetCellPhones(long mAffiliateID) {
            string sql = "SELECT * FROM Drivers WHERE AffiliateID = @AffiliateID AND BoostPhone is not null ORDER BY BoostPhone";
            using (var conn = SqlHelper.OpenSqlConnection()) {
                return conn.Query<Drivers>(sql, new { AffiliateID = mAffiliateID }).ToList();
            }
        }

        public static List<Drivers> GetDriversByCell(SqlConnection pConn, string pCellPhone) {
            string sql = @"SELECT * FROM dbo.Drivers
							LEFT JOIN dbo.Affiliate ON Affiliate.AffiliateID = Drivers.AffiliateID
							LEFT JOIN dbo.AffiliateDriverDefaults ON AffiliateDriverDefaults.AffiliateID = Affiliate.AffiliateID
							WHERE Drivers.Cell = @Cell";

            var data = pConn.Query<Drivers, Affiliate, AffiliateDriverDefaults, Drivers>(sql
                        , (drivers, aff, def) => { drivers.mAffiliate = aff; drivers.Affiliate.AffiliateDriverDefaults = def; return drivers; }
                        , new { Cell = pCellPhone }
                        , splitOn: "AffiliateID,AffiliateDriverDefaultID").ToList();


            foreach (var rec in data) {
                rec.Affiliate.RowState = System.Data.DataRowState.Unchanged;
                rec.Affiliate.AffiliateDriverDefaults.RowState = System.Data.DataRowState.Unchanged;
            }
            return data;
        }

        public static Drivers GetDriverBySerialNo(SqlConnection pConn, string pSerialNo) {
            if (pSerialNo.IsNullOrEmpty()) {
                return Drivers.GetNullEntity();
            }
            string sql = @"SELECT * FROM Drivers 
							WHERE SerialNo = @SerialNo";
            return pConn.Get<Drivers>(sql, new { SerialNo = pSerialNo });
        }


        public static Drivers GetChargeCard(Affiliate pAffiliate) {
            return GetChargeCard(pAffiliate.AffiliateID);
        }

        public static Drivers GetChargeCard(long pAffiliateID) {
            using (var conn = SqlHelper.OpenSqlConnection()) {
                return GetChargeCard(conn, pAffiliateID);
            }
        }

        public static Drivers GetChargeCard(SqlConnection pConn, long pAffiliateID) {

            string sql = "SELECT * FROM drivers WHERE name = 'Charge Card' AND AffiliateID = @AffiliateID";

            Drivers driver = pConn.Get<Drivers>(sql, new { AffiliateID = pAffiliateID });
            if (driver.IsNullEntity) {
                driver = Drivers.Create();
                Affiliate aff = Affiliate.GetAffiliate(pConn, pAffiliateID);
                driver.Name = CHARGE_CARD;
                driver.City = aff.City;
                driver.State = aff.State;
                driver.Country = aff.Country;
                driver.ZIPCode = aff.ZIPCode;
                driver.EMail = aff.email;
                driver.Cell = "CELL";
                driver.BoostPhone = "CELL";
                driver.AffiliateID = aff.AffiliateID;
                driver.CardDataEntryType = ((int)DataEntryTypeEnum.SwipeAndManual).ToString();
                driver.TaxiMeter = ((int)TaxiMeterUserEntryType.UserEntryTypeEnum.VoucherMenuFare).ToString();
                driver.PrinterType = ((int)HandHeldPrinterTypes.HandHeldPrinterTypesEnum.HomeATM).ToString();
                driver.Active = true;
                if (driver.EMail.IsNullOrEmpty()) {
                    driver.EMail = "email@taxipass.com";
                }

                try {
                    driver.Save(CabRideDapperSettings.LoggedInUser);
                } catch (Exception ex) {
                    throw ex;
                }
            }

            if (!driver.Active) {
                driver.Active = true;
                driver.Save(CabRideDapperSettings.LoggedInUser);
            }
            return driver;
        }


        #endregion

        public Twilio.SMSMessage SendMsgViaTwilio(SqlConnection pConn, string pMsg, string pTwilioNumber = "") {
            if (this.Affiliate.DoNotTextDrivers) {
                return new Twilio.SMSMessage();
            }

            SystemDefaults oDef = SystemDefaults.GetDefaults(pConn);

            string cell = this.Cell;
            string acctSid = oDef.TwilioAccountSID;
            string acctToken = oDef.TwilioAccountToken;
            string apiVersion = oDef.TwilioAPIVersion;

            if (pTwilioNumber.IsNullOrEmpty()) {
                pTwilioNumber = oDef.TwilioSMSNumber;
            }

            TaxiPassCommon.Mail.TwilioTexting twilio = new TaxiPassCommon.Mail.TwilioTexting(acctSid, acctToken, apiVersion, pTwilioNumber);
            return twilio.TwilioTextMessage(pMsg, cell);
        }

        //***************************************************************************************************
        public void SendMsgToPhone(string pMsg) {
            SystemDefaults oDef = SystemDefaults.GetDefaults();

            string cell = this.BoostPhone;

            TaxiPassCommon.Mail.EZTexting ezText = new TaxiPassCommon.Mail.EZTexting(oDef.EZTextingUserName, oDef.EZTextingPassword);
            bool bUseEzTexting = true;

            string acctSid = oDef.TwilioAccountSID;
            string acctToken = oDef.TwilioAccountToken;
            string apiVersion = oDef.TwilioAPIVersion;
            string smsNumber = oDef.TwilioSMSNumber;

            TaxiPassCommon.Mail.TwilioTexting twilio = new TaxiPassCommon.Mail.TwilioTexting(acctSid, acctToken, apiVersion, smsNumber);
            var response = twilio.TwilioTextMessage(pMsg, cell);
            if (response.RestException == null || response.RestException.Message.IsNullOrEmpty()) {
                bUseEzTexting = false;
            }

            if (bUseEzTexting) {
                ezText.Send(cell, pMsg);
            }

        }

        public string SendMsgViaTwilio(string pMsg) {
            if (this.Affiliate.DoNotTextDrivers) {
                return "";
            }
            SystemDefaults oDef = SystemDefaults.GetDefaults();

            string cell = this.Cell;
            string acctSid = oDef.TwilioAccountSID;
            string acctToken = oDef.TwilioAccountToken;
            string apiVersion = oDef.TwilioAPIVersion;
            string smsNumber = oDef.TwilioSMSNumber;

            TaxiPassCommon.Mail.TwilioTexting twilio = new TaxiPassCommon.Mail.TwilioTexting(acctSid, acctToken, apiVersion, smsNumber);
            return twilio.TwilioTextMessage(pMsg, cell).Sid;
        }


        public static Drivers GetDriverByCellPhone(Affiliate pAffiliate, string pCellPhone) {
            if (pCellPhone.IsNullOrEmpty()) {
                return Drivers.GetNullEntity();
            }
            string sql = @"SELECT  *
                            FROM    Drivers
                            WHERE   Cell = @CellPhone
                                    AND AffiliateID = @AffiliateID";
            DynamicParameters parms = new DynamicParameters();
            parms.Add("Cell", pCellPhone);
            parms.Add("AffiliateID", pAffiliate.AffiliateID);
            using (var conn = SqlHelper.OpenSqlConnection()) {
                return conn.Get<Drivers>(sql, parms);
            }
        }

    }
}
