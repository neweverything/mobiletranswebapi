﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TaxiPassCommon.Banking {
	public enum TTechAction {
		Normal,
		Void,
		Represent,
		OverRide,
		BatchOut
	}
}
