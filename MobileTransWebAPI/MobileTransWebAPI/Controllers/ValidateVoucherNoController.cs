﻿using CabRideEngine;
using CabRideEngineDapper.Utils;
using IdeaBlade.Persistence;
using MobileTransWebAPI.Models;
using System;
using System.Web.Http;
using System.Web.Script.Serialization;
using TaxiPassCommon;

namespace MobileTransWebAPI.Controllers {
    public class ValidateVoucherNoController : ApiController {
        //
        // GET: /ValidateVoucherNo/

        public ValidateVoucherResponse Get() {
            return GetValidateVoucher("8FEC063CC10AB1CF63DBBB44FE78BE238E94237585439D81BEC161A041757915EC38A19C9DF1CC0F9FFE406465A19242BB04B7F85BF0351970CF8FBA5444FC581A961D99B3B485BFC248993769780F212A05B3CB2AF04A29124EA6B1");
        }

        public ValidateVoucherResponse GetValidateVoucher(string pRequest) {
            ValidateVoucherResponse result = new ValidateVoucherResponse();
            result.Valid = false;

            if (pRequest.IsNullOrEmpty()) {
                result.ErrorMsg = "Invalid Access";
                return result;
            }
            JavaScriptSerializer js = new JavaScriptSerializer();

            ValidateVoucherRequest info = js.Deserialize<ValidateVoucherRequest>(pRequest.DecryptIceKey());
            WebApiApplication.LogIt("GetValidateVoucher", pRequest);

            DateTime requestDate = DateTime.Today.AddDays(-1);
            if (info != null) {
                requestDate = info.RequestKey.DecryptIceKey().ToDateTime();
            }

            if (WebApiApplication.GetUTCServerTime().Subtract(requestDate).TotalMinutes > 1) {
                result.ErrorMsg = "Invalid Access";
                return result;
            }

            PersistenceManager oPM = WebApiApplication.UserPM;
            if (oPM == null) {
                result.ErrorMsg = "Could not connect to database";
                return result;
            }

            int maxDays = 10;
            using (var conn = SqlHelper.OpenSqlConnection()) {
                var temp = CabRideEngineDapper.SystemDefaultsDict.GetByKey(conn, "DriverPay", "SelfRedemptionMaxVoucherAge", false);
                if (!temp.IsNullEntity && temp.ValueString.IsNumeric()) {
                    maxDays = Convert.ToInt32(temp.ValueString);
                }
            }


            DriverPayments pay = DriverPayments.GetPayment(oPM, info.VoucherNo);


            if (pay.IsNullEntity || pay.CardNumberDisplay.Right(4) != info.CardNo) {
                result.ErrorMsg = "Not a Kiosk Voucher";
            } else if (pay.Fare > 0) {
                result.ErrorMsg = "Voucher already processed";
            } else if (DateTime.Today.Subtract(pay.AuthDate.Value).TotalDays >= maxDays) {
                result.ErrorMsg = "Voucher has expired, please see manager";
            } else {
                //if (pay.Test || pay.Failed) {
                if (pay.Test) {
                    result.ErrorMsg = "Test Voucher";
                } else if (pay.Failed) {
                    result.ErrorMsg = "Failed Transaction";
                } else {
                    result.Valid = true;
                }
            }

            return result;
        }


    }
}
