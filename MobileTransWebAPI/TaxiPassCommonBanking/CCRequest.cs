﻿
namespace TaxiPassCommon.Banking {
    public class CCRequest {

        public CCRequest() {
        }

        public CCRequest(CreditCard pCardData, decimal pAmount, string pComments, string pVoucherNo, string pService, string pPlatform, string pDriverID, bool pGetRideTrans, decimal pSplitPayAmount = 0, decimal pTaxiPassFee = 0, decimal pDriverFee = 0) {
            CardData = pCardData;
            Amount = pAmount;
            Comments = pComments;
            VoucherNo = pVoucherNo;
            Service = pService;
            Platform = pPlatform;
            DriverID = pDriverID;
            GetRideTrans = pGetRideTrans;
            SplitPayAmount = pSplitPayAmount;
            TaxiPassFee = pTaxiPassFee;
            DriverFee = pDriverFee;
        }

        public CreditCard CardData = new CreditCard();
        public decimal Amount { get; set; }
        public decimal SplitPayAmount { get; set; }
        public string Comments { get; set; }
        public string VoucherNo { get; set; }
        public string Service { get; set; }
        public string Platform { get; set; }
        public string DriverID { get; set; }

        public bool GetRideTrans { get; set; }
        public string AffiliateBillingDescriptor { get; set; }

        public decimal DriverFee { get; set; }
        public decimal TaxiPassFee { get; set; }
    }
}
