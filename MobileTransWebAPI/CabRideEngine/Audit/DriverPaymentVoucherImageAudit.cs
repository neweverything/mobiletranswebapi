﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

using TaxiPassCommon;

namespace CabRideEngine {
    /// <summary>
    /// The developer class representing the DriverPaymentVoucherImageAudit business object class.
    /// </summary>
    /// <remarks>
    /// <para>Place all application-specific business logic in this class.</para>
    /// <para>
    /// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
    /// and will not overwrite changes.
    /// </para>
    /// <para>
    /// This class inherits from the generated class, its companion in the pair of classes 
    /// defining a business object. Properties in the generated class may be overridden in 
    /// order to change the behavior or add business logic.
    /// </para>
    /// <para>
    /// This class is the final class in the inheritance chain for this business object. 
    /// It should not be inherited.
    /// </para>
    /// </remarks>
    [Serializable]
    public sealed partial class DriverPaymentVoucherImageAudit : DriverPaymentVoucherImageAuditDataRow {

        #region Constructors -- DO NOT MODIFY

        // Do not create constructors for this class
        // Developers cannot create instances of this class with the "new" keyword
        // You should write a static Create method instead; see the "Suggested Customization" region
        // Please review the documentation to learn about entity creation and inheritance

        // This private constructor prevents accidental instance creation via 'new';
        // it is also required if the class is visible from a web service.
        // DO NOT REMOVE **
        private DriverPaymentVoucherImageAudit() : this(null) { }

        /// <summary>
        /// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
        /// </summary>
        /// <param name="pRowBuilder"></param>
        public DriverPaymentVoucherImageAudit(DataRowBuilder pRowBuilder)
            : base(pRowBuilder) {
        }

        #endregion

        #region Suggested Customizations

        //    // Use this factory method template to create new instances of this class
        //    public static DriverPaymentVoucherImageAudit Create(PersistenceManager pManager,
        //      ... your creation parameters here ...) { 
        //
        //      DriverPaymentVoucherImageAudit aDriverPaymentVoucherImageAudit = pManager.CreateEntity<DriverPaymentVoucherImageAudit>();
        //
        //      // if this object type requires a unique id and you have implemented
        //      // the IIdGenerator interface implement the following line
        //      pManager.GenerateId(aDriverPaymentVoucherImageAudit, // add id column here //);
        //
        //      // Add custom code here
        //
        //      aDriverPaymentVoucherImageAudit.AddToManager();
        //      return aDriverPaymentVoucherImageAudit;
        //    }

        //    // Implement this method if you want your object to sort in a specific order
        //    public override int CompareTo(Object pObject) {
        //    }

        //    // Implement this method to customize the null object version of this class
        //    protected override void UpdateNullEntity() {
        //    }

        #endregion

        // Add additional logic to your business object here...
        public static DriverPaymentVoucherImageAudit Create(DriverPaymentVoucherImages pDriverPaymentVoucherImages) {
            PersistenceManager oPM = pDriverPaymentVoucherImages.PersistenceManager;
            DriverPaymentVoucherImageAudit oAudit = oPM.CreateEntity<DriverPaymentVoucherImageAudit>();
            oPM.GenerateId(oAudit, DriverPaymentVoucherImageAudit.DriverPaymentVoucherImageHistoryIDEntityColumn);

            for (int i = 0; i < pDriverPaymentVoucherImages.Table.Columns.Count; i++) {
                if (oAudit.Table.Columns.Contains(pDriverPaymentVoucherImages.Table.Columns[i].ColumnName)) {
                    oAudit[pDriverPaymentVoucherImages.Table.Columns[i].ColumnName] = pDriverPaymentVoucherImages[i];
                }
            }
            oAudit.AddToManager();

            return oAudit;
        }

        public DateTime ModifiedDateLocalTime {
            get {
                DriverPayments rec = DriverPayments.GetPayment(this.PersistenceManager, this.DriverPaymentID);
                if (!rec.Affiliate.TimeZone.IsNullOrEmpty()) {
                    TimeZoneConverter timeZone = new TimeZoneConverter();
                    return timeZone.FromUniversalTime(rec.Affiliate.TimeZone, this.ModifiedDate.Value);
                }
                return TimeZone.CurrentTimeZone.ToLocalTime(this.ModifiedDate.Value);
            }
        }

        public DriverPayments DriverPayment {
            get {
                return DriverPayments.GetPayment(this.PersistenceManager, this.DriverPaymentID);
            }
        }

        public string MyImageURL {
            get {
                SystemDefaults def = SystemDefaults.GetDefaults(this.PersistenceManager);
                string url = def.VoucherImageURL;
                if (this.DriverPayment.Platform == Platforms.CCKiosk.ToString()) {
                    url = def.CCKioskVoucherImageURL;
                }
                string imageURL = this.ImageURL.Replace('\\', '/');
                if (!url.EndsWith("/") && !this.ImageURL.StartsWith("/")) {
                    url += "/";
                }
                url += imageURL;
                return url;
            }
        }


        public static DriverPaymentVoucherImageAudit GetFirstVoucherImage(DriverPaymentVoucherImages pDriverPaymentVoucherImages) {
            PersistenceManager oPM = pDriverPaymentVoucherImages.PersistenceManager;
            RdbQuery qry = new RdbQuery(typeof(DriverPaymentVoucherImageAudit), DriverPaymentVoucherImageAudit.DriverPaymentVoucherImageIDEntityColumn, EntityQueryOp.EQ, pDriverPaymentVoucherImages.DriverPaymentVoucherImageID);
            qry.AddOrderBy(DriverPaymentVoucherImageAudit.RowVersionEntityColumn);
            qry.Top = 1;
            return oPM.GetEntity<DriverPaymentVoucherImageAudit>(qry);
        }

        public static EntityList<DriverPaymentVoucherImageAudit> GetVoucherImages(DriverPaymentVoucherImages pDriverPaymentVoucherImages) {
            PersistenceManager oPM = pDriverPaymentVoucherImages.PersistenceManager;
            RdbQuery qry = new RdbQuery(typeof(DriverPaymentVoucherImageAudit), DriverPaymentVoucherImageAudit.DriverPaymentVoucherImageIDEntityColumn, EntityQueryOp.EQ, pDriverPaymentVoucherImages.DriverPaymentVoucherImageID);
            qry.AddOrderBy(DriverPaymentVoucherImageAudit.RowVersionEntityColumn);
            return oPM.GetEntities<DriverPaymentVoucherImageAudit>(qry);
        }

    }

    #region EntityPropertyDescriptors.DriverPaymentVoucherImageAuditPropertyDescriptor
    ////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
    ////    For example: 
    ////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
    //public static partial class EntityPropertyDescriptors {
    //	public partial class DriverPaymentVoucherImageAuditPropertyDescriptor : AdaptedPropertyDescriptor {
    //    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
    //    public AdaptedPropertyDescriptor Age {
    //      get { return this.Get("Age"); }
    //    }
    //  }
    //} 
    #endregion

}