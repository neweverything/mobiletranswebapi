﻿using static TaxiPassCommon.Banking.FSV;

namespace TaxiPassCommon.Banking.Interfaces {

    public interface IPayCard {

        VerifyConnectionResponse VerifyConnection();
        CardRegistrationResponse RegisterCard(CardRegistration pRegistration);
        FundsTransferResponse TransferToPayCard(FundsTransfer pFunds);
        FundsTransferResponse TransferFromPayCard(string pPayCardNo, FundsTransfer pFunds);
        decimal GetFundingBalance();

    }
}
