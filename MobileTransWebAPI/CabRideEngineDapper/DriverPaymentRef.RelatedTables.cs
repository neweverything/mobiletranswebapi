﻿using CabRideEngineDapper.Utils;
using Dapper;

namespace CabRideEngineDapper {
    public partial class DriverPaymentRef {

        private DriverPayments mDriverPayment;
        public DriverPayments DriverPayment {
            get {
                if (mDriverPayment == null || mDriverPayment.IsNullEntity) {
                    using (var conn = SqlHelper.OpenSqlConnection()) {
                        mDriverPayment = conn.Get<DriverPayments>(this.DriverPaymentID);
                    }
                }
                return mDriverPayment;
            }
            set {
                mDriverPayment = value;
            }
        }
    }
}
