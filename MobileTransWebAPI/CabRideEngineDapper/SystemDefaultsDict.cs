﻿using CabRideEngineDapper.Enums;
using CabRideEngineDapper.Utils;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using TaxiPassCommon;
using TaxiPassCommon.Mail;

namespace CabRideEngineDapper {

	public partial class SystemDefaultsDict {


		[Editable(false)]
		public decimal ValueAsDecimal {
			get {
				decimal val = 0;
				if (!ValueString.IsNullOrEmpty() && ValueString.IsNumeric()) {
					val = Convert.ToDecimal(ValueString);
				}
				return val;
			}
		}

		[Editable(false)]
		public long ValueAsLong {
			get {
				long val = 0;
				if (!ValueString.IsNullOrEmpty() && ValueString.IsNumeric()) {
					val = Convert.ToInt64(ValueString);
				}
				return val;
			}
		}

		[Editable(false)]
		public bool ValueAsBool {
			get {
				bool val = false;
				if (!ValueString.IsNullOrEmpty() && ValueString.Equals("true", StringComparison.CurrentCultureIgnoreCase)) {
					val = true;
				}
				return val;
			}
		}


		public new static SystemDefaultsDict Create() {
			SystemDefaultsDict rec = SystemDefaultsDictTable.Create();
			rec.InActive = false;
			return rec;
		}

		// ********************************************************************************************
		// Get Or Create record
		// ********************************************************************************************
		public static SystemDefaultsDict GetOrCreate(SqlConnection pConn, string pKey) {
			SystemDefaultsDict rec = GetByKey(pConn, pKey);

			if (rec.IsNullEntity) {
				rec = Create();
				rec.ReferenceKey = pKey;

			}

			return rec;
		}

		public static SystemDefaultsDict GetOrCreate(SqlConnection pConn, string pGroup, string pKey) {
			SystemDefaultsDict rec = GetByKey(pConn, pGroup, pKey, false);

			if (rec.IsNullEntity) {
				rec = Create();
				rec.ReferenceKey = pKey;
				rec.ReferenceGroup = pGroup;
			}

			return rec;
		}


		// ********************************************************************************************
		// Given a key and active status, return the row
		// ********************************************************************************************
		public static SystemDefaultsDict GetByKey(string pGroup, string pKey, bool pInactive) {
			using (var conn = SqlHelper.OpenSqlConnection()) {
				return GetByKey(conn, pGroup, pKey, pInactive);
			}
		}

		public static SystemDefaultsDict GetByKey(SqlConnection pConn, string pGroup, string pKey, bool pInactive) {
			string sql = "SELECT * FROM SystemDefaultsDict WHERE ReferenceKey = @ReferenceKey AND ReferenceGroup = @ReferenceGroup AND InActive = @InActive";
			DynamicParameters param = new DynamicParameters();
			param.Add("ReferenceKey", pKey);
			param.Add("ReferenceGroup", pGroup);
			param.Add("InActive", pInactive);

			return GetRecord(pConn, sql, param);
		}


		// ********************************************************************************************
		// Given a key, return the row regardless of active status
		// ********************************************************************************************
		public static SystemDefaultsDict GetByKey(string pKey) {
			string sql = "SELECT * FROM SystemDefaultsDict WHERE ReferenceKey = @ReferenceKey ";
			DynamicParameters param = new DynamicParameters();
			param.Add("ReferenceKey", pKey);

			return GetRecord(sql, param);
		}

		public static SystemDefaultsDict GetByKey(SqlConnection pConn, string pKey) {
			string sql = "SELECT * FROM SystemDefaultsDict WHERE ReferenceKey = @ReferenceKey ";
			DynamicParameters param = new DynamicParameters();
			param.Add("ReferenceKey", pKey);

			return GetRecord(pConn, sql, param);
		}


		private static SystemDefaultsDict GetRecord(string sql, DynamicParameters param) {
			using (var conn = SqlHelper.OpenSqlConnection()) {
				return GetRecord(conn, sql, param);
			}
		}


		private static SystemDefaultsDict GetRecord(SqlConnection pConn, string sql, DynamicParameters param) {
			return pConn.Query<SystemDefaultsDict>(sql, param).DefaultIfEmpty(SystemDefaultsDict.GetNullEntity()).FirstOrDefault();
		}


		public static List<SystemDefaultsDict> GetGroup(SqlConnection pConn, string pGroup, bool pInActive = false) {
			string sql = @"SELECT * FROM SystemDefaultsDict 
							WHERE ReferenceGroup = @ReferenceGroup and InActive = @InActive";


			return pConn.Query<SystemDefaultsDict>(sql, new { ReferenceGroup = pGroup, InActive = pInActive }).ToList();
		}

		public static string BlackListedAreaCodes(SqlConnection pConn) {
			SystemDefaultsDict rec = SystemDefaultsDict.GetByKey(pConn, "BlackListedAreaCodes");
			return rec.ValueString;
		}

		public static string BlackListedAreaCodes(SqlConnection pConn, string pAreaCodes) {
			SystemDefaultsDict rec = SystemDefaultsDict.GetOrCreate(pConn, "BlackListedAreaCodes");
			rec.ValueString = pAreaCodes;

			return rec.ValueString;
		}

		public static EmailCredentials GetEmailCreds(EMailReportFrom pEmailFor) {
			SystemDefaultsDict dict = SystemDefaultsDict.GetByKey("EMail", pEmailFor.ToString(), false);
			string[] cred = dict.ValueString.Decrypt().Split('|');
			return new EmailCredentials() {
				From = cred[0],
				Password = cred[1]
			};

		}
	}
}
