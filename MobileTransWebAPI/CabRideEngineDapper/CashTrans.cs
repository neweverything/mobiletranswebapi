﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace CabRideEngineDapper {
	public partial class CashTrans {


		private Affiliate mAffiliate;
		public Affiliate Affiliate {
			get {
				if (mAffiliate == null) {
					mAffiliate = Affiliate.GetAffiliate(this.AffiliateID);
				}
				return mAffiliate;
			}
		}


		private Invoices mInvoice;
		public Invoices Invoice {
			get {
				if (!this.InvoiceID.HasValue) {
					return Invoices.GetNullEntity();
				}

				if (mInvoice == null) {
					mInvoice = Invoices.GetInvoice(this.InvoiceID.GetValueOrDefault(0));
				}
				return mInvoice;
			}
		}


		public static CashTrans Create(Drivers pDriver) {
			CashTrans rec = CashTransTable.Create();
			rec.DriverID = pDriver.DriverID;
			rec.AffiliateID = pDriver.AffiliateID.Value;
			return rec;
		}


		public static IEnumerable<CashTrans> GetTransByID(SqlConnection pConn, long pCashTransID, string pCustomer = "") {
			string sql = @"SELECT  *
                            FROM    CashTrans
                            WHERE   CashTransID = @CashTransID
                                    AND  (@Customer = '' OR SendReceiptTo = @Customer)";


			DynamicParameters parms = new DynamicParameters();
			parms.Add("CashTransID", pCashTransID);
			parms.Add("Customer", pCustomer);

			return pConn.Query<CashTrans>(sql, parms);
		}

		public static List<CashTrans> GetByInvoiceID(SqlConnection pConn, long pInvoiceID) {
			string sql = @"SELECT *
							FROM CashTrans
							WHERE InvoiceID = @InvoiceID
							ORDER BY ChargeDate";

			return pConn.Query<CashTrans>(sql, new { InvoiceID = pInvoiceID }).ToList();
		}

		// ********************************************************************************
		// return CashTrans for a customer within a date range
		// ********************************************************************************
		public static IEnumerable<CashTrans> GetTransBySendReceiptTo(SqlConnection pConn, string pTo, DateTime pStart, DateTime pEnd) {
			string sql = @"SELECT  *
                            FROM    CashTrans
                            WHERE   SendReceiptTo = @To
                                    AND ChargeDate >= @Start
                                    AND ChargeDate <= @End
                            ORDER BY ChargeDate DESC";

			DynamicParameters parms = new DynamicParameters();
			parms.Add("To", pTo);
			parms.Add("Start", pStart);
			parms.Add("End", pEnd);

			return pConn.Query<CashTrans>(sql, parms);
		}

	}
}
