﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

using TaxiPassCommon;

namespace CabRideEngine {
	/// <summary>
	/// The developer class representing the ShiftCloseValidateVoucher business object class.
	/// </summary>
	/// <remarks>
	/// <para>Place all application-specific business logic in this class.</para>
	/// <para>
	/// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
	/// and will not overwrite changes.
	/// </para>
	/// <para>
	/// This class inherits from the generated class, its companion in the pair of classes 
	/// defining a business object. Properties in the generated class may be overridden in 
	/// order to change the behavior or add business logic.
	/// </para>
	/// <para>
	/// This class is the final class in the inheritance chain for this business object. 
	/// It should not be inherited.
	/// </para>
	/// </remarks>
	[Serializable]
	public sealed partial class ShiftCloseValidateVoucher : ShiftCloseValidateVoucherDataRow {

		#region Constructors -- DO NOT MODIFY

		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new';
		// it is also required if the class is visible from a web service.
		// DO NOT REMOVE **
		private ShiftCloseValidateVoucher() : this(null) { }

		/// <summary>
		/// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
		/// </summary>
		/// <param name="pRowBuilder"></param>
		public ShiftCloseValidateVoucher(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static ShiftCloseValidateVoucher Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      ShiftCloseValidateVoucher aShiftCloseValidateVoucher = pManager.CreateEntity<ShiftCloseValidateVoucher>();
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aShiftCloseValidateVoucher, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aShiftCloseValidateVoucher.AddToManager();
		//      return aShiftCloseValidateVoucher;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion


		private bool mTag;
		public bool Tag {
			get {
				return mTag;
			}
			set {
				mTag = value;
			}
		}

		// Add additional logic to your business object here...
		public static ShiftCloseValidateVoucher Create(TaxiPassRedeemers pRedeemer) {
			ShiftCloseValidateVoucher rec = null;
			try {
				rec = pRedeemer.PersistenceManager.CreateEntity<ShiftCloseValidateVoucher>();
				rec.RedeemerID = pRedeemer.TaxiPassRedeemerID;
				//TimeZoneConverter tzConvert = new TimeZoneConverter();
				//rec.ValidateDate = tzConvert.ConvertTime(DateTime.Now, TimeZone.CurrentTimeZone.StandardName, pRedeemer.TaxiPassRedeemerAccounts.Markets.TimeZone);
				rec.AddToManager();

			} catch (Exception ex) {
				throw ex;
			}
			return rec;
		}

		public static EntityList<ShiftCloseValidateVoucher> GetMissedList(TaxiPassRedeemers pRedeemer) {
			RdbQuery qry = new RdbQuery(typeof(ShiftCloseValidateVoucher), ShiftCloseValidateVoucher.RedeemerIDEntityColumn, EntityQueryOp.EQ, pRedeemer.TaxiPassRedeemerID);
			qry.AddClause(ShiftCloseValidateVoucher.ValidatedEntityColumn, EntityQueryOp.EQ, false);
			qry.AddOrderBy(ShiftCloseValidateVoucher.ValidateDateEntityColumn);
			return pRedeemer.PersistenceManager.GetEntities<ShiftCloseValidateVoucher>(qry);
		}

		public static EntityList<ShiftCloseValidateVoucher> GetMissedList(TaxiPassRedeemers pRedeemer, DateTime pDate) {
			RdbQuery qry = new RdbQuery(typeof(ShiftCloseValidateVoucher), ShiftCloseValidateVoucher.RedeemerIDEntityColumn, EntityQueryOp.EQ, pRedeemer.TaxiPassRedeemerID);
			qry.AddClause(ShiftCloseValidateVoucher.ValidateDateEntityColumn, EntityQueryOp.GE, pDate.Date);
			qry.AddClause(ShiftCloseValidateVoucher.ValidateDateEntityColumn, EntityQueryOp.LT, pDate.Date.AddDays(1));
			qry.AddClause(ShiftCloseValidateVoucher.ValidatedEntityColumn, EntityQueryOp.EQ, false);
			qry.AddOrderBy(ShiftCloseValidateVoucher.ValidateDateEntityColumn);
			return pRedeemer.PersistenceManager.GetEntities<ShiftCloseValidateVoucher>(qry);
		}

		public static EntityList<ShiftCloseValidateVoucher> GetValidatedVouchers(TaxiPassRedeemers pRedeemer, DateTime pStart, DateTime pEnd) {
			RdbQuery qry = new RdbQuery(typeof(ShiftCloseValidateVoucher), ShiftCloseValidateVoucher.RedeemerIDEntityColumn, EntityQueryOp.EQ, pRedeemer.TaxiPassRedeemerID);
			qry.AddClause(ShiftCloseValidateVoucher.ValidateDateEntityColumn, EntityQueryOp.GE, pStart.Date);
			qry.AddClause(ShiftCloseValidateVoucher.ValidateDateEntityColumn, EntityQueryOp.LT, pEnd.Date.AddDays(1));
			qry.AddClause(ShiftCloseValidateVoucher.ValidatedEntityColumn, EntityQueryOp.EQ, true);
			qry.AddOrderBy(ShiftCloseValidateVoucher.ValidateDateEntityColumn);
			qry.AddSpan(EntityRelations.ShiftCloseValidateVoucher_DriverPayments);
			qry.AddSpan(EntityRelations.TaxiPassRedeemers_ShiftCloseValidateVoucher);
			return pRedeemer.PersistenceManager.GetEntities<ShiftCloseValidateVoucher>(qry);

		}
	}

	#region EntityPropertyDescriptors.ShiftCloseValidateVoucherPropertyDescriptor
	////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
	////    For example: 
	////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
	//public static partial class EntityPropertyDescriptors {
	//	public partial class ShiftCloseValidateVoucherPropertyDescriptor : BaseEntityPropertyDescriptor {
	//    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
	//    public AdaptedPropertyDescriptor Age {
	//      get { return this.Get("Age"); }
	//    }
	//  }
	//} 
	#endregion

}