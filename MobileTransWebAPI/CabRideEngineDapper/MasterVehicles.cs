﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Dapper;

using CabRideEngineDapper.Utils;

namespace CabRideEngineDapper {
	public partial class MasterVehicles {

		public static MasterVehicles GetVehicleByLicensePlate(Affiliate pAffiliate, string pLicensePlate) {
			using (var conn = SqlHelper.OpenSqlConnection()) {
				string sql = "SELECT * FROM MasterVehicles WHERE DMVLicensePlate = @DMVLicensePlate AND AffiliatedBaseLicenseNumber = @LicenseNumber";
				DynamicParameters param = new DynamicParameters();
				param.Add("LicenseNumber", pAffiliate.LicenseNumber);
				param.Add("LicensePlate", pLicensePlate);
				return conn.Query<MasterVehicles>(sql, param).DefaultIfEmpty(MasterVehicles.GetNullEntity()).FirstOrDefault();
			}
		}
	}
}
