﻿using Dapper;
using System.Data.SqlClient;

namespace CabRideEngineDapper {
    public partial class Devices {

        public static Devices GetDevice(SqlConnection pConn, long pDeviceID) {
            return pConn.Get<Devices>(pDeviceID);
        }
    }
}
