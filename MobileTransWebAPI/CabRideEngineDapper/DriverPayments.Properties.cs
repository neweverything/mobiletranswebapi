﻿using CabRideEngineDapper.Utils;
using Dapper;
using Newtonsoft.Json;
using System;
using System.Data;
using TaxiPassCommon;
using TaxiPassCommon.Cryptography;
using TaxiPassCommon.Enums;

namespace CabRideEngineDapper {
	public partial class DriverPayments {

		[Editable(false)]
		public bool SwipeFailed { get; set; }

		//private bool mSwipeFailed = false;
		//[Editable(false)]
		//public bool SwipeFailed {
		//    get {
		//        return mSwipeFailed;
		//    }
		//    set {
		//        mSwipeFailed = value;
		//        if (mSwipeFailed) {
		//            var rec = DriverPaymentRef.GetCreate(this, "CardSwiped", "Failed");
		//            rec.ValueString = mSwipeFailed.ToString();
		//        }
		//    }
		//}

		[Editable(false)]
		public string OriginalTransNo { get; set; }

		//public List<BaseEntity> SaveRecords = new List<BaseEntity>();

		//----------------------------------------------------------------------------------------
		// Exception Charge, used by Twilio to identify trans that we let driver to process even if
		// they do not have a CVV
		//----------------------------------------------------------------------------------------
		[Editable(false)]
		public bool UseFraudGateway { get; set; }

		[Editable(false)]
		public bool ExceptionCharge { get; set; }

		[Editable(false)]
		public string CVV { get; set; }

		[Editable(false)]
		[JsonIgnore]
		public bool SetWriteOff {
			get {
				return this.DriverPaymentAux.WriteOff.GetValueOrDefault(false);
			}
			set {
				if (this.RowState == DataRowState.Added) {
					this.Save();
				}
				if (this.DriverPaymentAux.IsNullEntity) {
					DriverPaymentAux.Create(this);
				}
				this.DriverPaymentAux.WriteOff = value;
				this.Save();
			}
		}


		[Editable(false)]
		[JsonIgnore]
		public bool StoreForward {
			get {
				return this.DriverPaymentAux.StoreForward;
			}
			set {
				if (this.DriverPaymentAux.IsNullEntity) {
					DriverPaymentAux.Create(this);
				}
				this.DriverPaymentAux.StoreForward = value;
			}
		}

		private DateTime? mStoreForwardDate;
		[Editable(false)]
		[JsonIgnore]
		public DateTime? StoreForwardDate {
			get {
				return mStoreForwardDate;
			}
			set {
				mStoreForwardDate = value;
			}
		}

		[Editable(false)]
		[JsonIgnore]
		public int? Passengers {
			get {
				return this.DriverPaymentAux.Passengers;
			}
			set {
				if (this.DriverPaymentAux.IsNullEntity) {
					DriverPaymentAux.Create(this);
				}
				this.DriverPaymentAux.Passengers = value;
			}
		}

		// ********************************************************************************
		// 
		// ********************************************************************************
		[Editable(false)]
		[JsonIgnore]
		public bool CancelTaxiPassFee {
			get {
				return !this.CorporateName.IsNullOrEmpty();
			}
		}

		/// <summary>
		/// Gets a value indicating whether this trans should be run as GetRide.
		/// </summary>
		/// <value>
		///   <c>true</c> if [is get ride trans]; otherwise, <c>false</c>.
		/// </value>
		[Editable(false)]
		[JsonIgnore]
		public bool IsGetRideTrans {
			get {
				bool val = false;
				//if (Platform == Platforms.GetRide.ToString() || (this.Drivers.GetRideDriver && Platform != Platforms.TaxiPay.ToString() && Platform != Platforms.NexStep.ToString())) {
				if (Platform == Platforms.GetRide.ToString() || (Platform != Platforms.TaxiPay.ToString() && Platform != Platforms.NexStep.ToString())) {
					SystemDefaultsDict dict = SystemDefaultsDict.GetByKey("SoftDescriptor", "ShowGetRideUrlOnStatement", false);
					val = (dict.ValueString.Equals("true", StringComparison.CurrentCultureIgnoreCase));
				}
				return val;
			}
		}


		[Editable(false)]
		[JsonIgnore]
		public decimal SetFareNoTaxiPassFee {
			set {
				Fare = value;
				TaxiPassFee = 0;
			}
		}

		// ********************************************************************************
		// 
		// ********************************************************************************
		[Editable(false)]
		[JsonIgnore]
		public decimal SetFare {
			set {
				Fare = value;
				TaxiPassFee = CalcFee(this.Drivers.Affiliate);
				DriverFee = CalcDriverFee(this.Drivers.Affiliate);
			}
		}

		// ********************************************************************************
		// 
		// ********************************************************************************
		[Editable(false)]
		[JsonIgnore]
		public decimal SetGratuity {
			set {
				Gratuity = value;
				TaxiPassFee = CalcFee(this.Drivers.MyAffiliate);
				DriverFee = CalcDriverFee(this.Drivers.MyAffiliate);
			}
		}

		// ********************************************************************************
		// 
		// ********************************************************************************
		[Editable(false)]
		[JsonIgnore]
		public decimal SetAirportFee {
			set {
				AirportFee = value;
				TaxiPassFee = CalcFee(this.Drivers.MyAffiliate);
				DriverFee = CalcDriverFee(this.Drivers.MyAffiliate);
			}
		}

		// ********************************************************************************
		// 
		// ********************************************************************************
		[Editable(false)]
		[JsonIgnore]
		public decimal SetMiscFee {
			set {
				MiscFee = value;
				TaxiPassFee = CalcFee(this.Drivers.MyAffiliate);
				DriverFee = CalcDriverFee(this.Drivers.MyAffiliate);
			}
		}

		[Editable(false)]
		[JsonIgnore]
		public decimal SetBookingFee {
			set {
				BookingFee = value;
				TaxiPassFee = CalcFee(this.Drivers.MyAffiliate);
				DriverFee = CalcDriverFee(this.Drivers.MyAffiliate);
			}
		}

		[Editable(false)]
		[JsonIgnore]
		public decimal SetDropFee {
			set {
				DropFee = value;
				TaxiPassFee = CalcFee(this.Drivers.MyAffiliate);
				DriverFee = CalcDriverFee(this.Drivers.MyAffiliate);
			}
		}

		[Editable(false)]
		[JsonIgnore]
		public decimal SetTolls {
			set {
				Tolls = value;
				TaxiPassFee = CalcFee(this.Drivers.MyAffiliate);
				DriverFee = CalcDriverFee(this.Drivers.MyAffiliate);
			}
		}

		[Editable(false)]
		[JsonIgnore]
		public decimal SetWaitTime {
			set {
				WaitTime = value;
				TaxiPassFee = CalcFee(this.Drivers.MyAffiliate);
				DriverFee = CalcDriverFee(this.Drivers.MyAffiliate);
			}
		}

		[Editable(false)]
		[JsonIgnore]
		public decimal SetStops {
			set {
				Stops = value;
				TaxiPassFee = CalcFee(this.Drivers.MyAffiliate);
				DriverFee = CalcDriverFee(this.Drivers.MyAffiliate);
			}
		}

		[Editable(false)]
		[JsonIgnore]
		public decimal SetParking {
			set {
				Parking = value;
				TaxiPassFee = CalcFee(this.Drivers.MyAffiliate);
				DriverFee = CalcDriverFee(this.Drivers.MyAffiliate);
			}
		}

		// ********************************************************************************
		// 
		// ********************************************************************************
		[Editable(false)]
		[JsonIgnore]
		public decimal SetDiscount {
			set {
				Discount = Math.Abs(value);
				TaxiPassFee = CalcFee(this.Drivers.MyAffiliate);
				DriverFee = CalcDriverFee(this.Drivers.MyAffiliate);
			}
		}


		[Editable(false)]
		[JsonIgnore]
		public string SortByRedeemerLocation {
			get {
				return this.TaxiPassRedeemerLocations.Location + " " + this.TaxiPassRedeemers.Name + " " + this.MyDriverName;
			}
		}

		[Editable(false)]
		public bool IsPromo {
			get {
				return ("" + this.CreditCardProcessor).Contains("Prom");
			}
		}

		[Editable(false)]
		public bool HasVoucherImage {
			get {
				return !this.DriverPaymentVoucherImages.IsNullEntity;
			}
		}

		[Editable(false)]
		public string ReceiptRequestDateAsString {
			get {
				if (this.ReceiptRequest.IsNullEntity) {
					return "";
				}
				return this.ReceiptRequest.ReceiptRequest.ToShortDateString();
			}
		}

		[Editable(false)]
		public string RefundInfo {
			get {
				if (!this.ReceiptChargedBack.IsNullOrEmpty()) {
					return "ChargeBack Voucher: " + this.ReceiptChargedBack;
				} else {
					using (var conn = SqlHelper.OpenSqlConnection()) {
						return DriverPaymentAux.GetRefundInfo(conn, this.DriverPaymentID);
					}
				}
			}
		}

		[Editable(false)]
		public string MyAffiliateName {
			get {
				if (!this.AffiliateDriverID.HasValue || this.AffiliateDriverID.Value < 1) {
					return this.Drivers.Affiliate.Name;
				}

				return AffiliateDrivers.Affiliate.Name;
			}
		}

		[Editable(false)]
		public string MyDriverName {
			get {
				if (!this.AffiliateDriverID.HasValue || this.AffiliateDriverID.Value < 1) {
					return this.Drivers.Name;
				}
				return AffiliateDrivers.Name;
			}
		}

		[Editable(false)]
		public string MyDriverPhone {
			get {
				if (!this.AffiliateDriverID.HasValue || this.AffiliateDriverID.Value < 1) {
					return this.Drivers.Cell;
				}
				if (this.AffiliateDrivers.Cell.Length < 10) {
					return this.AffiliateDrivers.CallInCell;
				}
				return this.AffiliateDrivers.Cell;
			}
		}

		[Editable(false)]
		public string MyCellName {
			get {
				return this.Drivers.Name;
			}
		}

		[Editable(false)]
		public string MyVehicle {
			get {
				if (!this.VehicleID.HasValue || this.VehicleID.Value < 1) {
					return "";
				}
				return this.Vehicles.VehicleNo;
			}
		}

		[Editable(false)]
		public string MyRedeemer {
			get {
				if (!this.TaxiPassRedeemerID.HasValue || this.TaxiPassRedeemerID.Value < 1) {
					return "";
				}
				return this.TaxiPassRedeemers.Name;
			}
		}

		[Editable(false)]
		public string MyRedeemerAccount {
			get {
				if (!this.DriverPaymentsPendingMatches.IsNullEntity) {
					return this.DriverPaymentsPendingMatches.TaxiPassRedeemers.TaxiPassRedeemerAccounts.CompanyName;
				}
				return this.TaxiPassRedeemers.TaxiPassRedeemerAccounts.CompanyName;
			}
		}

		[Editable(false)]
		public string MyRedeemerAccountPhone {
			get {
				if (!this.DriverPaymentsPendingMatches.IsNullEntity) {
					return this.DriverPaymentsPendingMatches.TaxiPassRedeemers.TaxiPassRedeemerAccounts.Phone.FormattedPhoneNumber();
				}
				return this.TaxiPassRedeemers.TaxiPassRedeemerAccounts.Phone.FormattedPhoneNumber();
			}
		}

		[Editable(false)]
		public string RedemptionPlatform {
			get {
				if (this.TaxiPassRedeemerID.HasValue) {
					if (this.ACHDetail.ACHBankAccountNumber.IsNullOrEmpty()) {
						return "Redeemer";
					}
					if (this.ACHDetail.ACHBankAccountNumber.StartsWith("TransCard:", StringComparison.CurrentCultureIgnoreCase)) {
						return "Pay Card";
					}
					return "Redeemer";
				} else {
					if (this.ACHDetail.ACHPaidTo.IsNullOrEmpty()) {
						return "";
					}
					if (this.ACHDetail.ACHPaidTo.StartsWith("Redeemer:", StringComparison.CurrentCultureIgnoreCase)) {
						return "Redeemer";
					}
					if (this.ACHDetail.ACHPaidTo.StartsWith("Affiliate:", StringComparison.CurrentCultureIgnoreCase)) {
						return "Affiliate";
					}
					if (this.ACHDetail.ACHPaidTo.StartsWith("Cell:", StringComparison.CurrentCultureIgnoreCase)) {
						return "Owner";
					}
					if (this.ACHDetail.ACHPaidTo.StartsWith("Driver:", StringComparison.CurrentCultureIgnoreCase)) {
						return "Driver";
					}
					if (!this.ACHDetail.ACHPaidTo.IsNullOrEmpty()) {
						return "unknown";
					}
				}
				return "";
			}
		}

		[Editable(false)]
		public string MyRedeemerLocation {
			get {
				if (!this.TaxiPassRedeemerLocationID.HasValue || this.TaxiPassRedeemerLocationID.Value < 1) {
					return "";
				}
				return this.TaxiPassRedeemerLocations.Location;
			}
		}

		[Editable(false)]
		public string MyReferrer {
			get {
				return this.AffiliateDriverReferrals.AffiliateDrivers.Name;
			}
		}

		[Editable(false)]
		public string MyEMail {
			get {
				if (!this.AffiliateDriverID.HasValue || this.AffiliateDriverID.Value < 1) {
					return this.Drivers.EMail;
				}

				return AffiliateDrivers.EMail;
			}
		}

		[Editable(false)]
		public string MyPlatform {
			get {
				if (("" + this.Platform).Equals("TaxiPay", StringComparison.CurrentCultureIgnoreCase) && this.Affiliate.IsLimo()) {
					return "LimoPay";
				}
				return this.Platform;
			}
		}

		[Editable(false)]
		public string PreAuthApprovalNumber {
			get {
				string sText = "";
				if (this.DriverPaymentAuthResults != null) {
					sText += this.DriverPaymentAuthResults.AuthCode;
				}
				return sText;
			}
		}

		[Editable(false)]
		public string ChargeApprovalNumber {
			get {
				string sText = "";
				if (this.DriverPaymentChargeResults != null) {
					sText = this.DriverPaymentChargeResults.AuthCode;
				}
				return sText;
			}
		}

		[Editable(false)]
		[JsonIgnoreAttribute]
		public string Notes {
			get {
				string note = this.DriverPaymentNotes.Reason;
				if (!note.IsNullOrEmpty()) {
					note += "\n\n";
				}
				note += this.ChargeBackInfo;
				return note;
			}
			set {
				if (this.DriverPaymentNotes.IsNullEntity) {
					DriverPaymentNotes.Create(this);
				}
				if (!this.DriverPaymentNotes.Reason.IsNullOrEmpty()) {
					this.DriverPaymentNotes.Reason += "\n\n";
				}
				this.DriverPaymentNotes.Reason += value;
			}
		}

		[Editable(false)]
		public string DriverPaidByRedeemer {
			get {
				if (this.TaxiPassRedeemerID != null && this.DriverPaymentsPendingMatches.IsNullEntity) {
					if (this.TaxiPassRedeemers.EmployeeNo.IsNullOrEmpty()) {
						return this.TaxiPassRedeemers.Name;
					} else {
						return this.TaxiPassRedeemers.EmployeeNo;
					}
				}
				if (DriverPaid) {
					if (!this.DriverPaymentsPendingMatches.IsNullEntity) {
						return this.DriverPaymentsPendingMatches.TaxiPassRedeemers.Name;
					}
					return "Direct ACH Payment";
				}
				return "";
			}
		}

		[Editable(false)]
		public DateTime? MyDriverPaidTime {
			get {
				return this.DriverPaidDate;
			}
		}

		[JsonIgnore]
		[Editable(false)]
		public DateTime? MyACHPaidDateTime {
			get {
				return this.ACHDetail.ACHPaidDate;
			}
		}

		// ********************************************************************************
		// 
		// ********************************************************************************
		[JsonIgnore]
		[Editable(false)]
		public string ACHPaidTo {
			get {
				return this.ACHDetail.ACHPaidTo;
			}
		}

		// ********************************************************************************
		// 
		// ********************************************************************************
		[JsonIgnore]
		[Editable(false)]
		public string ACHBankAccountNumber {
			get {
				return this.ACHDetail.ACHBankAccountNumber;
			}
		}

		[Editable(false)]
		public string ACHProcessError { get; set; }

		[Editable(false)]
		public string ExpirationDecrypted {
			get {
				if (Expiration.IsNullOrEmpty()) {
					return Expiration;
				}
				Encryption oEncrypt = new Encryption();
				return oEncrypt.TripleDESDecrypt(Expiration, this.Salt);
			}

		}

		// ********************************************************************************
		// 
		// ********************************************************************************
		[Editable(false)]
		public string ExpirationAsDelimitedString {
			get {
				string exp = ExpirationDecrypted;
				if (exp.IsNullOrEmpty()) {
					return "";
				}
				return exp.Insert(2, "/");
			}
		}


		[JsonIgnore]
		[Editable(false)]
		public bool GetSetDoNotPay {
			get {
				return this.DriverPaymentAux.DoNotPay.GetValueOrDefault(false);
			}
			set {
				if (this.DriverPaymentAux.IsNullEntity) {
					if (this.RowState == DataRowState.Added) {
						this.Save();
					}
					DriverPaymentAux rec = DriverPaymentAux.Create(this);
					rec.Save();
				}
				this.DriverPaymentAux.DoNotPay = value;
				this.DoNotPay = value;
			}
		}

		[JsonIgnore]
		[Editable(false)]
		public long? DoNotPayReasonID {
			get {
				return this.DriverPaymentAux.DoNotPayReasonID;
			}
			set {
				if (this.DriverPaymentAux.IsNullEntity) {
					DriverPaymentAux.Create(this);
				}
				this.DriverPaymentAux.DoNotPayReasonID = value;
			}
		}

		// ********************************************************************************
		// Return N****NNNN for a card number
		// ********************************************************************************
		[JsonIgnore]
		[Editable(false)]
		public string GetSetCardNumber {
			get {
				try {
					if (!CardNumber.IsNullOrEmpty()) {
						string sCardNo = DecryptCardNumber();
						if (sCardNo.IsNullOrEmpty()) {
							return sCardNo;
						}
						sCardNo = sCardNo.Left(1) + "******" + sCardNo.Right(5);
						return sCardNo;
					}
					return "";
				} catch (Exception ex) {
					return ex.Message;
				}
			}
			set {
				value = value.ToUpper().Replace("%B", "").Replace(";", "").Replace(" ", "");
				if (!value.IsNumeric()) {
					try {
						value = value.DecryptIceKey();
					} catch (Exception) {

					}
				}
				Encryption oEncrypt = new Encryption();
				string sValue = "";
				while (true) {
					sValue = oEncrypt.TripleDESEncrypt(value, this.Salt);
					if (oEncrypt.TripleDESDecrypt(sValue, this.Salt) == value) {
						break;
					}
					//encryption/decryption didn't work, try different salt
					this.Salt = StringUtils.CreateSalt(6);
				}
				CardNumber = sValue;
				Number = CryptoFns.MD5HashUTF16ToString(value);
				if (CardNumber.IsNullOrEmpty()) {
					this.CardType = "";
				} else {
					this.CardType = CardUtils.DetermineCardType(DecryptCardNumber());
				}
				this.CardNumberDisplay = value.CardNumberDisplay();
				if (this.Affiliate.AffiliateDriverDefaults.DebitFeeProcessing.GetValueOrDefault(false)) {
					if (CardType.Equals("AMEX", StringComparison.CurrentCultureIgnoreCase)) {
						IsDebitCard = false;
					} else {
						IsDebitCard = value.IsDebitCard();
					}
				}
			}
		}

		// ********************************************************************************
		// 
		// ********************************************************************************
		[JsonIgnore]
		[Editable(false)]
		public string GetSetExpiration {
			get {
				if (base.Expiration.IsNullOrEmpty()) {
					return Expiration;
				}
				Encryption oEncrypt = new Encryption();
				return oEncrypt.TripleDESDecrypt(Expiration, this.Salt);
			}

			set {
				Encryption oEncrypt = new Encryption();
				string sValue = oEncrypt.TripleDESEncrypt(value, this.Salt);
				Expiration = sValue;
			}
		}

		// ********************************************************************************
		// Encrypt Card Holder Name
		// ********************************************************************************
		[JsonIgnore]
		[Editable(false)]
		public string GetSetCardHolder {
			get {
				return CardHolder.DecryptIceKey();
			}
			set {
				if (value.IsNullOrEmpty()) {
					CardHolder = value;
				} else {
					CardHolder = value.EncryptIceKey();
				}
			}
		}

		[Editable(false)]
		public string UseSoftDescriptor { get; set; }

		[JsonIgnore]
		[Editable(false)]
		public string SoftDescriptor {
			get {
				if (!UseSoftDescriptor.IsNullOrEmpty()) {
					return UseSoftDescriptor;
				}

				string softDesc = this.Affiliate.DefaultPlatform;
				DriverPaymentRef desc = DriverPaymentRef.GetByGroupAndReference(DriverPaymentID, "BillingDescriptor", "BillingDescriptor");
				if (!desc.ValueString.IsNullOrEmpty()) {
					softDesc = desc.ValueString;
				}
				return softDesc;
			}
		}


		[Editable(false)]
		public bool IsDebitCard { get; set; }

		public override bool? DoNotPay {
			get {
				return base.DoNotPay.GetValueOrDefault(false) || this.DriverPaymentAux.DoNotPay.GetValueOrDefault(false);
			}

			set {
				try {
					if (this.RowState != DataRowState.Detached) {
						if (this.DriverPaymentAux.IsNullEntity) {
							if (this.RowState == DataRowState.Added) {
								this.Save();
							}
							DriverPaymentAux.Create(this);
						}
						this.DriverPaymentAux.DoNotPay = value;
					}
				} catch (Exception) {
				}
				base.DoNotPay = value;

			}
		}


		//[JsonIgnore]
		//[Editable(false)]
		//public bool SwipeFailed {
		//    get {
		//        DriverPaymentRef rec = DriverPaymentRef.GetByGroupAndReference(DriverPaymentID, "CardSwiped", "Failed");
		//        if (!rec.ValueString.IsNullOrEmpty()) {
		//            return false;
		//        }
		//        return rec.ValueString.AsBool();
		//    }
		//}

		[Editable(false)]
		public string BankAccountNo { get; set; }

		[Editable(false)]
		public string BankRoutingNo { get; set; }

		[Editable(false)]
		public CheckAccountTypes BankAccountType { get; set; }


		[Editable(false)]
		public string CheckNo { get; set; }


		[Editable(false)]
		public string CustPhone {
			get {
				return DriverPaymentRef.GetByGroupAndReference(this.DriverPaymentID, "Customer", "Phone").ValueString;
			}

		}

		public override string CardType {
			get {
				if (base.CardType.IsNullOrEmpty()) {
					using (var conn = SqlHelper.OpenSqlConnection()) {
						base.CardType = CreditCardUtils.DetermineCardType(conn, this.DecryptCardNumber());
						if (this.RowState == DataRowState.Modified) {
							if (base.CardType != "") {
								this.Save();
							}
						}
					}
				}
				return base.CardType;
			}
			set => base.CardType = value;
		}


		//[JsonIgnore]
		//[Editable(false)]
		//public string IPAddress {
		//	get {
		//		string value = "";
		//		DriverPaymentRef desc = DriverPaymentRef.GetByGroupAndReference(DriverPaymentID, "Customer", "IPAddress");
		//		if (!desc.ValueString.IsNullOrEmpty()) {
		//			value = desc.ValueString;
		//		}
		//		return value;
		//	}
		//	set {
		//		DriverPaymentRef desc = DriverPaymentRef.GetByGroupAndReference(DriverPaymentID, "Customer", "IPAddress");
		//		desc
		//	}
		//}



	}
}
