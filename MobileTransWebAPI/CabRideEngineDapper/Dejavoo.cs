﻿using CabRideEngineDapper.Utils;
using Dapper;
using System.Data.SqlClient;

namespace CabRideEngineDapper {
    public partial class Dejavoo {

        public static Dejavoo GetRecord(string pID) {
            using (var conn = SqlHelper.OpenSqlConnection()) {
                return GetRecord(conn, pID);
            }
        }

        public static Dejavoo GetRecord(SqlConnection pConn, string pID) {
            string sql = @"SELECT * FROM Dejavoo
                            WHERE ExternalVoucherID = @ID";

            return pConn.Get<Dejavoo>(sql, new { ID = pID });
        }
    }
}
