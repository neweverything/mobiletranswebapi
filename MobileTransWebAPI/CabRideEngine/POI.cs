﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

namespace CabRideEngine {
    [Serializable]
    public sealed class POI : POIDataRow {

        #region Constructors -- DO NOT MODIFY
        // Do not create constructors for this class
        // Developers cannot create instances of this class with the "new" keyword
        // You should write a static Create method instead; see the "Suggested Customization" region
        // Please review the documentation to learn about entity creation and inheritance

        // This private constructor prevents accidental instance creation via 'new'
        private POI()
            : this(null) {
        }

        // Typed DataSet constructor (needed by framework) - DO NOT REMOVE
        public POI(DataRowBuilder pRowBuilder)
            : base(pRowBuilder) {
        }

        #endregion

        #region Suggested Customizations

        //    // Use this factory method template to create new instances of this class
        //    public static POI Create(PersistenceManager pManager,
        //      ... your creation parameters here ...) { 
        //
        //      POI aPOI = (POI) pManager.CreateEntity(typeof(POI));
        //
        //      // if this object type requires a unique id and you have implemented
        //      // the IIdGenerator interface implement the following line
        //      pManager.GenerateId(aPOI, // add id column here //);
        //
        //      // Add custom code here
        //
        //      aPOI.AddToManager();
        //      return aPOI;
        //    }

        //    // Implement this method if you want your object to sort in a specific order
        //    public override int CompareTo(Object pObject) {
        //    }

        //    // Implement this method to customize the null object version of this class
        //    protected override void UpdateNullEntity() {
        //    }

        #endregion

        // Add additional logic to your business object here...

		//protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
		//	if (IsDeserializing)
		//		return;
		//	base.OnColumnChanging(pArgs);
		//	DoAudit(pArgs);
		//}

		//private void DoAudit(DataColumnChangeEventArgs pArgs) {
		//	if (!this.PersistenceManager.IsClient) {
		//		return;
		//	}
		//	if (this.RowState == DataRowState.Added) {
		//		return;
		//	}
		//	if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
		//		AuditRecord = POIAudit.Create(this.PersistenceManager, this);
		//	}

		//}

        public static POI Create(PersistenceManager pPM, string POIName) {
            POI oPOI = null;

            try {
                // Creates the State but it is not yet accessible to the application
                oPOI = (POI)pPM.CreateEntity(typeof(POI));
				oPOI.POIMasterID = 1;
				oPOI.Name = POIName;
                // CRITICAL: must tell emp to add itself to its PM
                oPOI.AddToManager();

                // Add custom code here
                //oType.POIType = sPOIType;

            } catch (Exception ex) {
                throw ex;
            }
            return oPOI;
        }
    }

}
