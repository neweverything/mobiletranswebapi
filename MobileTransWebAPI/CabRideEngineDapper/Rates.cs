﻿using CabRideEngineDapper.Utils;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace CabRideEngineDapper {

    public partial class Rates {


        private CoverageZones mPUCoverage;
        public CoverageZones PUCoverage {
            get {
                if (mPUCoverage == null) {
                    using (var conn = SqlHelper.OpenSqlConnection()) {
                        mPUCoverage = conn.Get<CoverageZones>(PUCoverageZoneID);
                    }
                }
                return mPUCoverage;
            }
            set {
                mPUCoverage = value;
            }
        }

        private CoverageZones mDOCoverage;
        public CoverageZones DOCoverage {
            get {
                if (mDOCoverage == null) {
                    using (var conn = SqlHelper.OpenSqlConnection()) {
                        mDOCoverage = conn.Get<CoverageZones>(DOCoverageZoneID);
                    }
                }
                return mDOCoverage;
            }
            set {
                mDOCoverage = value;
            }
        }

        private VehicleTypes mVehicleType;
        public VehicleTypes VehicleType {
            get {
                if (mVehicleType == null) {
                    using (var conn = SqlHelper.OpenSqlConnection()) {
                        mVehicleType = conn.Get<VehicleTypes>(VehicleTypeID);
                    }
                }
                return mVehicleType;
            }
            set {
                mVehicleType = value;
            }
        }

        private Accounts mAccount;
        public Accounts Account {
            get {
                if (mAccount == null) {
                    using (var conn = SqlHelper.OpenSqlConnection()) {
                        mAccount = conn.Get<Accounts>(AccountID);
                    }
                }
                return mAccount;
            }
            set {
                mAccount = value;
            }
        }


        public static IEnumerable<Rates> GetRatesForAccount(long pAccountID) {
            try {

                using (var conn = SqlHelper.OpenSqlConnection()) {
                    string sql = @"SELECT r.*, puc.*, doc.*, vt.* FROM Rates r WITH (nolock)
							LEFT JOIN CoverageZones puc on puc.CoverageZoneID = r.PUCoverageZoneID
							LEFT JOIN CoverageZones doc on doc.CoverageZoneID = r.DOCoverageZoneID
							LEFT JOIN VehicleTypes vt on vt.VehicleTypeID = r.VehicleTypeID
							WHERE AccountId = @AccountID
							ORDER BY vt.VehicleType, RateType";

                    var data = conn.Query<Rates, CoverageZones, CoverageZones, VehicleTypes, Rates>(sql
                        , (rate, puCoverage, doCoverage, vehicleType) => { rate.PUCoverage = puCoverage; rate.DOCoverage = doCoverage; rate.VehicleType = vehicleType; return rate; }
                        , new { AccountID = pAccountID }, splitOn: "CoverageZoneID,CoverageZoneID,VehicleTypeID");

                    return data;
                }
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
            return null;

        }

        public static IEnumerable<Rates> GetRates() {
            try {

                using (var conn = SqlHelper.OpenSqlConnection()) {
                    string sql = @"SELECT r.*
										, puc.*
										, doc.*
										, vt.*
										, acct.*
									FROM Rates r WITH (NOLOCK)
									LEFT JOIN CoverageZones puc ON puc.CoverageZoneID = r.PUCoverageZoneID
									LEFT JOIN CoverageZones doc ON doc.CoverageZoneID = r.DOCoverageZoneID
									LEFT JOIN VehicleTypes vt ON vt.VehicleTypeID = r.VehicleTypeID
									LEFT JOIN Accounts acct ON acct.AccountID = r.AccountID
									ORDER BY acct.NAME
										, vt.VehicleType
										, RateType";

                    var data = conn.Query<Rates, CoverageZones, CoverageZones, VehicleTypes, Accounts, Rates>(sql
                        , (rate, puCoverage, doCoverage, vehicleType, account) => { rate.PUCoverage = puCoverage; rate.DOCoverage = doCoverage; rate.VehicleType = vehicleType; rate.Account = account; return rate; }
                        , splitOn: "CoverageZoneID,CoverageZoneID,VehicleTypeID,AccountID");

                    return data;
                }
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
            return null;

        }

        public static List<Rates> RatesForPricing(long pAccountID, string pPUZipCode, string pDOZipCode, string pVehicleType = "Taxi") {
            string sql = @"SELECT distinct r.* FROM Rates r WITH (nolock)
							LEFT JOIN CoverageZones puc on puc.CoverageZoneID = r.PUCoverageZoneID
							LEFT JOIN CoverageZones doc on doc.CoverageZoneID = r.DOCoverageZoneID
							LEFT JOIN VehicleTypes vt on vt.VehicleTypeID = r.VehicleTypeID
							left join CoverageArea pua on pua.CoverageZoneID = puc.CoverageZoneID
							left join CoverageArea doa on doa.CoverageZoneID = doc.CoverageZoneID
							WHERE AccountId = @AccountId and pua.ZipCode = @PUZipCode and doa.ZipCode = @DOZipCode and vt.VehicleType = @VehicleType";

            DynamicParameters param = new DynamicParameters();
            param.Add("AccountId", pAccountID);
            param.Add("PUZipCode", pPUZipCode);
            param.Add("DOZipCode", pDOZipCode);
            param.Add("VehicleType", pVehicleType);

            using (var db = SqlHelper.OpenSqlConnection()) {
                return db.Query<Rates>(sql, param).ToList();
            }
        }

        public static IEnumerable<Rates> GetRatesForAffiliate(System.Data.SqlClient.SqlConnection conn, long pAffiliateID) {
            try {

                string sql = @"SELECT  r.*
                                      , puc.*
                                      , doc.*
                                      , vt.*
                                FROM    Rates r WITH (NOLOCK)
                                LEFT JOIN CoverageZones puc ON puc.CoverageZoneID = r.PUCoverageZoneID
                                LEFT JOIN CoverageZones doc ON doc.CoverageZoneID = r.DOCoverageZoneID
                                LEFT JOIN VehicleTypes vt ON vt.VehicleTypeID = r.VehicleTypeID
                                WHERE   r.AffiliateID = @AffiliateID
                                ORDER BY vt.VehicleType
                                      , RateType";

                var data = conn.Query<Rates, CoverageZones, CoverageZones, VehicleTypes, Rates>(sql
                    , (rate, puCoverage, doCoverage, vehicleType) => { rate.PUCoverage = puCoverage; rate.DOCoverage = doCoverage; rate.VehicleType = vehicleType; return rate; }
                    , new { AffiliateID = pAffiliateID }, splitOn: "CoverageZoneID,CoverageZoneID,VehicleTypeID");

                return data;
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
            return null;

        }

        public static Rates GetRate(SqlConnection pConn, long pRateID) {
            return pConn.Get<Rates>(pRateID);
        }

        //        internal static List<Rates> RatesForAffiliatePricing(long AffiliateID, TaxiPassCommon.Address pickUpAddress, TaxiPassCommon.Address dropOffAddress, string pVehicleType = "Taxi") {
        //            string sql = @"SELECT  *
        //                            FROM    dbo.Rates
        //                            WHERE   Rates.AffiliateID = 1737
        //                                    AND VehicleTypeID = 8
        //                                    AND (PickUpLocation = '92596'
        //                                         OR PickUpLocation = 'Winchester'
        //                                         OR PickUpLocation = 'All Zip Codes'
        //                                        )
        //                                    AND (DropOffLocation = '92591'
        //                                         OR DropOffLocation = 'Temecula'
        //                                         OR DropOffLocation = 'All Zip Codes'
        //                                        ) 
        //
        //                            SELECT  *
        //                            FROM    dbo.CustomFeeType
        //                            WHERE   AffiliateID = AffiliateID";

        //            throw new NotImplementedException();
        //        }
    }
}
