﻿using CabRideEngineDapper.Utils;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using TaxiPassCommon;

namespace CabRideEngineDapper {
    public partial class DriverPayments {

        public DriverPayments GetPerTransactionRecurringFee(bool pNotACHd) {
            string sql = @"SELECT  *
                            FROM    DriverPayments
                            WHERE   TransNo = @TransNo
                                    AND ChargeBy = 'Recurring Fee'";

            using (var conn = SqlHelper.OpenSqlConnection()) {
                DriverPayments recurringfee = conn.Get<DriverPayments>(sql, new { TransNo = this.TransNo });

                if (!pNotACHd) {
                    if (!recurringfee.ACHDetail.IsNullEntity) {
                        recurringfee = GetNullEntity();
                    }
                }
                return recurringfee;
            }
        }

        public List<DriverPayments> GetRecurringFeesByDriver(bool pNotACHd) {
            string sql = @"SELECT  *
                            FROM    DriverPayments
                            WHERE   DriverID = @DriverID
                                    AND ChargeBy = 'Recurring Fee'
                            ORDER BY ChargeBackDate";

            using (var conn = SqlHelper.OpenSqlConnection()) {
                List<DriverPayments> list = conn.Query<DriverPayments>(sql, new { DriverID = this.DriverID }).ToList();

                if (pNotACHd) {
                    var x = (from p in list where p.ACHDetail.IsNullEntity select p).ToList();
                }

                // CA 3/18:  Fund test DriverPayments with no AffiliateID
                foreach (DriverPayments oDP in list) {
                    if (oDP.AffiliateID.ToString().IsNullOrEmpty()) {
                        oDP.AffiliateID = this.Affiliate.AffiliateID;
                    }
                }

                return list;
            }
        }

        public static List<DriverPayments> GetPaymentByReferenceNo(SqlConnection pConn, long[] pAffiliateIDs, string pReferenceNo) {
            string sql = @"SELECT  *
                            FROM    DriverPayments
                            WHERE   ReferenceNo = @ReferenceNo
                                    AND AffiliateID IN (@AffiliateIDs)";
            return pConn.Query<DriverPayments>(sql, new { ReferenceNo = pReferenceNo, AffiliateIDs = pAffiliateIDs }).ToList();
        }

        public static List<DriverPayments> GetPaymentsByReferenceNo(AffiliateDrivers pAffiliateDriver, string pReferenceNo) {
            string sql = @"SELECT  *
                            FROM    DriverPayments
                            WHERE   ReferenceNo = @ReferenceNo
                                    AND AffiliateDriverID = @AffiliateDriverID";
            using (var conn = SqlHelper.OpenSqlConnection()) {
                return conn.Query<DriverPayments>(sql, new { ReferenceNo = pReferenceNo, AffiliateDriverID = pAffiliateDriver.AffiliateDriverID }).ToList();
            }
        }

        public static List<DriverPayments> GetPaymentsByReferenceNo(Drivers pDriver, string pReferenceNo) {
            string sql = @"SELECT  *
                            FROM    DriverPayments
                            WHERE   ReferenceNo = @ReferenceNo
                                    AND DriverID = @DriverID";
            using (var conn = SqlHelper.OpenSqlConnection()) {
                return conn.Query<DriverPayments>(sql, new { ReferenceNo = pReferenceNo, DriverID = pDriver.DriverID }).ToList();
            }
        }

        //public static List<DriverPayments> GetPaymentsByReferenceNo(SqlConnection pConn, string pReferenceNo) {
        //    string sql = @"SELECT  *
        //                    FROM    DriverPayments
        //                    WHERE   ReferenceNo = @ReferenceNo";
        //    return pConn.Query<DriverPayments>(sql, new { ReferenceNo = pReferenceNo }).ToList();

        //}

        // **********************************************************************************************
        // Charge Count for Promos by AffiliateDriver
        // **********************************************************************************************
        public static long GetChargeCountForPromos(AffiliateDrivers pDriver, DateTime pStartDate) {
            return GetChargeCountForPromos(pDriver, pStartDate, false);
        }

        /// <summary>
        /// Gets the charge count for promos.
        /// </summary>
        /// <param name="pDriver">The driver.</param>
        /// <param name="pStartDate">The start date.</param>
        /// <param name="pSwipeOnly">if set to <c>true</c> [swipe only].</param>
        /// <returns></returns>
        public static long GetChargeCountForPromos(AffiliateDrivers pDriver, DateTime pStartDate, bool pSwipeOnly) {
            string sql = @"SELECT  COUNT(DriverPaymentID)
                            FROM    DriverPayments WITH (NOLOCK)
                            WHERE   AffiliateDriverID = @AffiliateDriverID
                                    AND ChargeDate >= @StartDate
                                    AND Failed = 0
                                    AND Test = 0
                                    AND Fare > 0
                                    AND (CreditCardProcessor != 'DriverProm'
                                         OR CreditCardProcessor IS NULL
                                        ) ";

            if (pSwipeOnly) {
                sql += "AND CardSwiped = 1";
            }


            using (var conn = SqlHelper.OpenSqlConnection()) {
                return conn.Query<long>(sql, new { AffiliateDriverID = pDriver.AffiliateDriverID, StartDate = pStartDate.Date }).FirstOrDefault();
            }
        }

        // **********************************************************************************************
        // Charge Count for Promos by Driver
        // **********************************************************************************************
        public static long GetChargeCountForPromos(Drivers pDriver, DateTime pStartDate) {
            return GetChargeCountForPromos(pDriver, pStartDate, false);
        }

        /// <summary>
        /// Gets the charge count for promos.
        /// </summary>
        /// <param name="pDriver">The driver.</param>
        /// <param name="pStartDate">The start date.</param>
        /// <param name="pSwipeOnly">if set to <c>true</c> [swipe only].</param>
        /// <returns></returns>
        public static long GetChargeCountForPromos(Drivers pDriver, DateTime pStartDate, bool pSwipeOnly) {
            string sql = @"SELECT  COUNT(DriverPaymentID)
                            FROM    DriverPayments WITH (NOLOCK)
                            WHERE   DriverID = @DriverID
                                    AND AffiliateDriverID IS NULL
                                    AND ChargeDate >= @StartDate
                                    AND Failed = 0
                                    AND Test = 0
                                    AND Fare > 0
                                    AND (CreditCardProcessor != 'DriverProm'
                                         OR CreditCardProcessor IS NULL
                                        ) ";

            if (pSwipeOnly) {
                sql += "AND CardSwiped = 1";
            }
            using (var conn = SqlHelper.OpenSqlConnection()) {
                return conn.Query<long>(sql, new { DriverID = pDriver.DriverID, StartDate = pStartDate.Date }).FirstOrDefault();
            }

        }
    }
}