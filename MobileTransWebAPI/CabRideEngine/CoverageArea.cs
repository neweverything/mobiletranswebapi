﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

using TaxiPassCommon;

namespace CabRideEngine {
    [Serializable]
    public sealed class CoverageArea : CoverageAreaDataRow {

        #region Constructors -- DO NOT MODIFY
        // Do not create constructors for this class
        // Developers cannot create instances of this class with the "new" keyword
        // You should write a static Create method instead; see the "Suggested Customization" region
        // Please review the documentation to learn about entity creation and inheritance

        // This private constructor prevents accidental instance creation via 'new'
        private CoverageArea()
            : this(null) {
        }

        // Typed DataSet constructor (needed by framework) - DO NOT REMOVE
        public CoverageArea(DataRowBuilder pRowBuilder)
            : base(pRowBuilder) {
        }

        #endregion

        #region Suggested Customizations

        //    // Use this factory method template to create new instances of this class
        //    public static CoverageArea Create(PersistenceManager pManager,
        //      ... your creation parameters here ...) { 
        //
        //      CoverageArea aCoverageArea = (CoverageArea) pManager.CreateEntity(typeof(CoverageArea));
        //
        //      // if this object type requires a unique id and you have implemented
        //      // the IIdGenerator interface implement the following line
        //      pManager.GenerateId(aCoverageArea, // add id column here //);
        //
        //      // Add custom code here
        //
        //      aCoverageArea.AddToManager();
        //      return aCoverageArea;
        //    }

        //    // Implement this method if you want your object to sort in a specific order
        //    public override int CompareTo(Object pObject) {
        //    }

        //    // Implement this method to customize the null object version of this class
        //    protected override void UpdateNullEntity() {
        //    }

        #endregion

        // Add additional logic to your business object here...
        //public static CoverageArea Create(PersistenceManager pPM, string coverageZone) {
        //    CoverageArea oArea = null;

        //    try {
        //        // Creates the Country but it is not yet accessible to the application
        //        oArea = (CoverageArea)pPM.CreateEntity(typeof(CoverageArea));
        //        oArea["CoverageZone"] = coverageZone;
        //        oArea.AddToManager();

        //    } catch (Exception ex) {
        //        throw ex;
        //    }
        //    return oArea;
        //}

        public static CoverageArea Create(PersistenceManager pPM, long coverageZoneId) {
            CoverageArea oArea = null;

            try {
                // Creates the Country but it is not yet accessible to the application
                oArea = (CoverageArea)pPM.CreateEntity(typeof(CoverageArea));
                oArea["CoverageZoneID"] = coverageZoneId;
                //pPM.GetEntity<CoverageZones>();
                oArea.AddToManager();

            } catch (Exception ex) {
                throw ex;
            }
            return oArea;
        }

        protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
            if (IsDeserializing)
                return;
            base.OnColumnChanging(pArgs);
            DoAudit(pArgs);
        }

        private void DoAudit(DataColumnChangeEventArgs pArgs) {
            if (!this.PersistenceManager.IsClient) {
                return;
            }
            if (this.RowState == DataRowState.Added) {
                return;
            }
            if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
                AuditRecord = CoverageAreaAudit.Create(this.PersistenceManager, this);
            }
            //AuditRecord[pArgs.Column.ColumnName] = pArgs.ProposedValue;
        }


        public static EntityList<CoverageArea> GetCoverageAreas(PersistenceManager pPM, Address pAddress) {
            return GetCoverageAreas(pPM, pAddress, false);
        }

        public static EntityList<CoverageArea> GetCoverageAreas(PersistenceManager pPM, Address pAddress, bool pUse1stAddress) {
            if (string.IsNullOrEmpty(pAddress.ZIP)) {
                if (pAddress.IsAirport) {
                    Airports oAirport = Airports.GetAirport(pPM, pAddress.Airport);
                    pAddress.ZIP = oAirport.ZIPCode;
                    pAddress.Latitude = oAirport.Latitude;
                    pAddress.Longitude = oAirport.Longitude;
                }
                try {
                    SystemDefaults oDefaults = SystemDefaults.GetDefaults(pPM);
                    pAddress.Validate(oDefaults.MapPointUserName, oDefaults.MapPointPassword);
                } catch (TaxiPassCommon.GeoCoding.MapPointServiceAddressException ex) {
                    if (pUse1stAddress) {
                        pAddress.ZIP = ex.AddressList[0].ZIP;
                    } else {
                        throw ex;
                    }
                }
            }
            RdbQuery oQry = new RdbQuery(typeof(CoverageArea), CoverageArea.ZIPCodeEntityColumn, EntityQueryOp.EQ, pAddress.ZIP.Trim());
            oQry.AddClause(CoverageArea.CountryEntityColumn, EntityQueryOp.EQ, pAddress.Country.Trim());

            return pPM.GetEntities<CoverageArea>(oQry);
        }

    }


}
