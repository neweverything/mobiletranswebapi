﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;
using System.Text;

namespace CabRideEngine {
    /// <summary>
    /// The developer class representing the RideInfo business object class.
    /// </summary>
    /// <remarks>
    /// <para>Place all application-specific business logic in this class.</para>
    /// <para>
    /// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
    /// and will not overwrite changes.
    /// </para>
    /// <para>
    /// This class inherits from the generated class, its companion in the pair of classes 
    /// defining a business object. Properties in the generated class may be overridden in 
    /// order to change the behavior or add business logic.
    /// </para>
    /// <para>
    /// This class is the final class in the inheritance chain for this business object. 
    /// It should not be inherited.
    /// </para>
    /// </remarks>
    [Serializable]
    public sealed partial class RideInfo : RideInfoDataRow {

        #region Constructors -- DO NOT MODIFY

        // Do not create constructors for this class
        // Developers cannot create instances of this class with the "new" keyword
        // You should write a static Create method instead; see the "Suggested Customization" region
        // Please review the documentation to learn about entity creation and inheritance

        // This private constructor prevents accidental instance creation via 'new';
        // it is also required if the class is visible from a web service.
        // DO NOT REMOVE **
        private RideInfo() : this(null) { }

        /// <summary>
        /// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
        /// </summary>
        /// <param name="pRowBuilder"></param>
        public RideInfo(DataRowBuilder pRowBuilder)
            : base(pRowBuilder) {
        }

        #endregion

        #region Suggested Customizations

        //    // Use this factory method template to create new instances of this class
        //    public static RideInfo Create(PersistenceManager pManager,
        //      ... your creation parameters here ...) { 
        //
        //      RideInfo aRideInfo = pManager.CreateEntity<RideInfo>();
        //
        //      // if this object type requires a unique id and you have implemented
        //      // the IIdGenerator interface implement the following line
        //      pManager.GenerateId(aRideInfo, // add id column here //);
        //
        //      // Add custom code here
        //
        //      aRideInfo.AddToManager();
        //      return aRideInfo;
        //    }

        //    // Implement this method if you want your object to sort in a specific order
        //    public override int CompareTo(Object pObject) {
        //    }

        //    // Implement this method to customize the null object version of this class
        //    protected override void UpdateNullEntity() {
        //    }

        #endregion

        // Add additional logic to your business object here...
        public static RideInfo Create(DriverPayments pPayment) {
            RideInfo rec = null;

            try {
                rec = pPayment.PersistenceManager.CreateEntity<RideInfo>();

                pPayment.PersistenceManager.GenerateId(rec, RideInfo.RideInfoIDEntityColumn);
                rec.DriverPaymentID = pPayment.DriverPaymentID;
                rec.AddToManager();

            } catch (Exception ex) {
                throw ex;
            }
            return rec;
        }

        protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
            if (IsDeserializing) {
                return;
            }
            base.OnColumnChanging(pArgs);
            DoAudit(pArgs);
        }

        private void DoAudit(DataColumnChangeEventArgs pArgs) {
            if (!this.PersistenceManager.IsClient) {
                return;
            }
            if (this.RowState == DataRowState.Added) {
                return;
            }
            if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
                AuditRecord = RideInfoAudit.Create(this);
            }
        }

        public static EntityList<RideInfo> GetJobsByDate(PersistenceManager pPM, DateTime pStart, DateTime pEnd) {
            RdbQuery qry = new RdbQuery(typeof(RideInfo), RideInfo.PUTimeEntityColumn, EntityQueryOp.GE, pStart);
            qry.AddClause(RideInfo.PUTimeEntityColumn, EntityQueryOp.LT, pEnd);
            qry.AddSpan(EntityRelations.DriverPayments_RideInfo);
            return pPM.GetEntities<RideInfo>(qry, QueryStrategy.DataSourceThenCache);
        }

		public static EntityList<RideInfo> GetJobsByAccountAndDate(PersistenceManager pPM, long pAccountID, DateTime pStart, DateTime pEnd) {
			RdbQuery qry = new RdbQuery(typeof(RideInfo), RideInfo.PUTimeEntityColumn, EntityQueryOp.GE, pStart);
			qry.AddClause(RideInfo.PUTimeEntityColumn, EntityQueryOp.LT, pEnd);
			qry.AddClause(RideInfo.AccountIDEntityColumn, EntityQueryOp.EQ, pAccountID);
			qry.AddSpan(EntityRelations.DriverPayments_RideInfo);
			return pPM.GetEntities<RideInfo>(qry, QueryStrategy.DataSourceThenCache);
		}

		public static EntityList<RideInfo> GetSmartPhoneJobsByAccountAndDate(PersistenceManager pPM, long pAccountID, DateTime pStart, DateTime pEnd) {
			StringBuilder sql = new StringBuilder();
			sql.AppendFormat("select * from RideInfo where AccountID = {0} and ", pAccountID);
			sql.Append("DriverPaymentID in (");
			sql.Append("SELECT  DriverPaymentID FROM DriverPayments ");
			sql.Append("where DriverID in (select DriverID from Drivers where AppVersion is Not null) ");
			sql.AppendFormat("and ChargeDate >= '{0}' and ChargeDate <= '{1}' )", pStart.Date, pEnd);
			sql.Append("order by DriverPaymentID");


			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(RideInfo), sql.ToString());
			return pPM.GetEntities<RideInfo>(qry);
		}


        public static RideInfo GetByJobID(PersistenceManager pPM, string pJobID) {
            RdbQuery qry = new RdbQuery(typeof(RideInfo), RideInfo.JobIDEntityColumn, EntityQueryOp.EQ, pJobID);
            return pPM.GetEntity<RideInfo>(qry);
        }


		// ******************************************************************************************************
		// return all RideInfo entries for a given Account/Job ID
		// ******************************************************************************************************
		public static EntityList<RideInfo> GetJobsByID(PersistenceManager pPM, long pAccountID, string pJobID) {
			RdbQuery qry = new RdbQuery(typeof(RideInfo), RideInfo.JobIDEntityColumn, EntityQueryOp.EQ, pJobID);
			qry.AddClause(RideInfo.AccountIDEntityColumn, EntityQueryOp.EQ, pAccountID);
			qry.AddSpan(EntityRelations.DriverPayments_RideInfo);
			return pPM.GetEntities<RideInfo>(qry, QueryStrategy.DataSourceThenCache);
		}
    }

    #region EntityPropertyDescriptors.RideInfoPropertyDescriptor
    ////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
    ////    For example: 
    ////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
    //public static partial class EntityPropertyDescriptors {
    //	public partial class RideInfoPropertyDescriptor : BaseEntityPropertyDescriptor {
    //    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
    //    public AdaptedPropertyDescriptor Age {
    //      get { return this.Get("Age"); }
    //    }
    //  }
    //} 
    #endregion

}