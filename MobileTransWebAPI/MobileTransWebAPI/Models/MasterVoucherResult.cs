﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileTransWebAPI.Models {
	public class MasterVoucherResult {
		public string ErrorMsg { get; set; }

		public string CreatedDate { get; set; }
		public string TransNo { get; set; }
		public int Count { get; set; }
		public decimal Total { get; set; }

		public string DriverName { get; set; }
		public string CellPhone { get; set; }
		public string RedeemerName { get; set; }

		public decimal RecurringFees { get; set; }

		public List<TransRec> transList = new List<TransRec>();

		public class TransRec {
			public TransRec() {

			}

			public TransRec(string pDate, string pTransNo, string pCardNo, decimal pTotal) {
				Date = pDate;
				TransNo = pTransNo;
				Total = pTotal;
				CardNo = pCardNo;
				DriverFee = 0;
				RedemptionFee = 0;
			}

			public TransRec(string pDate, string pTransNo, string pCardNo, decimal pTotal, decimal pDriverFee, decimal pRedemptionFee) {
				Date = pDate;
				TransNo = pTransNo;
				Total = pTotal;
				CardNo = pCardNo;
				DriverFee = pDriverFee;
				RedemptionFee = pRedemptionFee;
			}

			public string Date { get; set; }
			public decimal Total { get; set; }
			public string TransNo { get; set; }
			public string CardNo { get; set; }
			public decimal DriverFee { get; set; }
			public decimal RedemptionFee { get; set; }
			public decimal RecurringFee { get; set; }
		}
	}

	
}