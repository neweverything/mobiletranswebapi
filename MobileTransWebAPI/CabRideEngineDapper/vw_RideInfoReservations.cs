﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace CabRideEngineDapper {
    public partial class vw_RideInfoReservations {

        public static List<vw_RideInfoReservations> GetJobsByAccountAndDate(SqlConnection pConn, long pAccountID, DateTime pStart, DateTime pEnd) {
            string sql = @"SELECT  *
                            FROM    vw_RideInfoReservations
                            WHERE   PUTime >= @Start
                                    AND PUTime <= @End
                                    AND AccountID = @AccountID";
            DynamicParameters parms = new DynamicParameters();
            parms.Add("Start", pStart);
            parms.Add("End", pEnd);
            parms.Add("AccountID", pAccountID);

            return pConn.Query<vw_RideInfoReservations>(sql, parms).ToList();
        }
    }
}
