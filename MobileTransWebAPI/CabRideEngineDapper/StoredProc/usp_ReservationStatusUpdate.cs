﻿using Dapper;
using System.Data;
using System.Data.SqlClient;

namespace CabRideEngineDapper {
    public class usp_ReservationStatusUpdate {

        public static void Execute(SqlConnection pConn, string pReserveNo, long pReservationID, string pStatus, string pModifiedBy) {
            pConn.Execute("usp_ReservationStatusUpdate", new { ReserveNo = pReserveNo, ReservationID = pReservationID, Status = pStatus, ModifiedBy = pModifiedBy }, commandType: CommandType.StoredProcedure);
        }

    }
}
