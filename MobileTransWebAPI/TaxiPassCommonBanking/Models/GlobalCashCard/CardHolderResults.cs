﻿using System.Collections.Generic;

namespace TaxiPassCommon.Banking.Models.GlobalCashCard {
	public class CardHolderResults {

		public string RESPONSECODE { get; set; }
		public List<RESULT> RESULTS { get; set; }
		public string STATUS { get; set; }
		public int COUNT { get; set; }

		public class RESULT {
			public bool PINSET { get; set; }
			public string GOVID { get; set; }
			public string GOVIDTYPE { get; set; }
			public int ZIPCODE { get; set; }
			public string STATE { get; set; }
			public string FIRSTNAME { get; set; }
			public string STATUS { get; set; }
			public string CITY { get; set; }
			public string COUNTRY { get; set; }
			public long CARDNUMBER { get; set; }
			public string LASTNAME { get; set; }
			public string DOB { get; set; }
			public string ADDRESS { get; set; }
			public int KEYFIELD { get; set; }
			public string GOVIDISSUER { get; set; }
		}


	}
}
