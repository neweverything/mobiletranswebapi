﻿using MobileTransWebAPI.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace MobileTransWebAPI.Controllers {
	public class TestShareController : ApiController {

		public DriverProfileResult Get() {
			JavaScriptSerializer js = new JavaScriptSerializer();
			string text = File.ReadAllText(@"c:\temp\temp.txt");

			DriverProfile profile = js.Deserialize<DriverProfile>(text);
			return SaveImage(profile);
		}

		private DriverProfileResult SaveImage(DriverProfile pProfile) {
			DriverProfileResult result = new DriverProfileResult();

			try {
				TaxiPassCommon.Images.ImageHelper oImgHelper = new TaxiPassCommon.Images.ImageHelper();
				System.Drawing.Image oImg = oImgHelper.Base64ToImage(pProfile.HackLicense.LicenseImageBase64);

				if (!Directory.Exists(Properties.Settings.Default.HackLicenseImagePath)) {
					Directory.CreateDirectory(Properties.Settings.Default.HackLicenseImagePath);
				}
				string path = Path.Combine(Properties.Settings.Default.HackLicenseImagePath, "test");
				if (!Directory.Exists(path)) {
					Directory.CreateDirectory(path);
				}

				string fileName = string.Format("{0}.png", DateTime.Now.ToString("yyyyMMddhhmmss"));
				path = Path.Combine(path, fileName);
				oImg.Save(path, System.Drawing.Imaging.ImageFormat.Png);
				result.ErrorMsg = "Saved Image to " + path;
			} catch (Exception ex) {
				result.ErrorMsg = "Error: " + ex.Message;
			}

			return result;
		}


	}
}
