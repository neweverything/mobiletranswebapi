﻿using TaxiPassCommon;

namespace MobileTransWebAPI.Models {
	public class CardInfo {
		public string AccountNo { get; set; }
		public string AffiliateCode { get; set; }
		public long CustomerID { get; set; }
		public long AffiliateID { get; set; }
		public string PhoneNo { get; set; }

		public string CardHolder { get; set; }
		public string CardType { get; set; }
		public string CardNumber { get; set; }
		public string CVV { get; set; }
		public int ExpMonth { get; set; }
		public int ExpYear { get; set; }

		public string BankRoutingNo { get; set; }
		public string BankAccountNo { get; set; }
		public string BankAccountType { get; set; }
		public string AccountHolderName { get; set; }


		public string Street { get; set; }
		public string Suite { get; set; }
		public string City { get; set; }
		public string State { get; set; }
		public string Zip { get; set; }


		public string Name {
			get {
				return CardHolder.IsNullOrEmpty() ? AccountHolderName : CardHolder;
			}
		}

		public string Number {
			get {
				return CardNumber.IsNullOrEmpty() ? BankAccountNo : CardNumber;
			}
		}


	}
}