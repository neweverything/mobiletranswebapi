﻿using CabRideEngineDapper;
using CabRideEngineDapper.Utils;
using Dapper;
using MobileTransWebAPI.Models;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using TaxiPassCommon;

namespace MobileTransWebAPI.Controllers {
    public class FundingPayCardUnpaidDriverVouchersController : ApiController {

        public FundingPayCardInfo Get() {
            string data = "821AE8EF7F9D6113B2DB334DDCAF93F396A632CA9F2E1D46BC9C063D18102DCB75A2F00DB4A98BC2DCF5CC61748D8D3A034942DA91967BCCD21C8D7A6381639C454AACF2012F7EDFD2035BB94AF3835DA501FF12E1EF4BF12824BD6052D308D95ECFDC4E";
            return Get(data);
        }


        // GET api/fundingpaycardunpaiddrivervouchers
        public FundingPayCardInfo Get(string pRequest) {
            FundingPayCardInfo result = new FundingPayCardInfo();

            DriverInfo driver = JsonConvert.DeserializeObject<DriverInfo>(pRequest.Decrypt());

            using (var conn = SqlHelper.OpenSqlConnection()) {
                AffiliateDrivers affDriver = AffiliateDrivers.GetDriver(conn, driver.AffiliateDriverID);
                if (affDriver.Cell != driver.Cell) {
                    result.ErrorMessage = "Invalid access";
                    return result;
                }

                List<string> affList = SystemDefaultsDict.GetByKey("EWRSelfRedemption", "Affiliates", false).ValueString.Split('|').ToList();
                if (affList.Count > 0) {
                    if (!affList.Contains(affDriver.AffiliateID.ToString())) {
                        result.ErrorMessage = "Account not authorized to self redeem vouchers";
                        return result;
                    }
                }

                AffDriverLoginInfo info = new AffDriverLoginInfo();
                info.Phone = driver.Cell;

                FundingPayCardInfoController cntr = new FundingPayCardInfoController();
                result = cntr.GetFundingInfo(JsonConvert.SerializeObject(info));
                result.VoucherList = new List<Vouchers>();


                List<DriverPayments> voucherList = GetVoucherList(driver.AffiliateDriverID);
                foreach (DriverPayments rec in voucherList) {
                    result.VoucherList.Add(new Vouchers() {
                        ChargeDate = rec.ChargeDate.Value,
                        TransNo = rec.TransNo,
                        DriverTotal = rec.DriverTotal
                    });
                }

            }

            return result;
        }

        private List<DriverPayments> GetVoucherList(long pAffiliateDriverID) {
            string sql = @"DECLARE @LookBackDays INT = (SELECT CONVERT(INT, ValueString)
														 FROM   SystemDefaultsDict
														 WHERE  ReferenceGroup = 'PayCard'
																AND ReferenceKey = 'PayDriverChargeDateMinusDays'
														)

							DECLARE @MaxAge INT = (SELECT CONVERT(INT, ValueString)
                                                         FROM   SystemDefaultsDict
                                                         WHERE  ReferenceGroup = 'PayCard'
                                                                AND ReferenceKey = 'MaxAge'
                                                        )

                            SELECT  DISTINCT
									DriverPayments.*
							FROM    dbo.DriverPayments
							LEFT JOIN ACHDetail ON ACHDetail.DriverPaymentID = DriverPayments.DriverPaymentID
							LEFT JOIN DriverPaymentAux ON DriverPaymentAux.DriverPaymentID = DriverPayments.DriverPaymentID
							WHERE   AffiliateDriverID = @AffiliateDriverID
									AND ACHPaidDate IS NULL
									AND Failed = 0
									AND Test = 0
									AND DATEADD(DAY, @LookBackDays, ChargeDate) < GETUTCDATE()
									AND ChargeDate >= '1/16/2016'
                                    AND ChargeDate > DATEADD(DAY, -@MaxAge, GETUTCDATE())
									AND ISNULL(DriverPayments.DoNotPay, 0) = 0 
									AND ISNULL(DriverPaymentAux.DoNotPay, 0) = 0
									AND TaxiPassRedeemerID IS NULL";

            using (var conn = SqlHelper.OpenSqlConnection()) {
                return conn.Query<DriverPayments>(sql, new { AffiliateDriverID = pAffiliateDriverID }).ToList();
            }
        }

    }
}
