using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Runtime.Serialization;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

namespace CabRideEngine {
 
  #region usp_MasterVoucherGetUnredeemedDataTable
  /// <summary>
  /// The generated <see cref="T:IdeaBlade.Persistence.EntityTable"/> class  
  /// containing all <see cref="usp_MasterVoucherGetUnredeemed"/> objects.
  /// </summary>
  /// <remarks>
  /// ****************************************************
  /// <para>** DO NOT MODIFY THIS CLASS - AutoGenerated Code</para>
  /// <para>** Any changes made to this code will be lost</para>
  /// <para>** the next time this code is regenerated.</para>
  /// <para>**</para>
  /// <para>** Generated from:   EntityBaseclass.template</para>
  /// <para>** Last modified:    2012-03-08T14:06:20.7414687-08:00</para>
  /// <para>** Template version: 3.110127</para>
  /// ****************************************************
  /// </remarks>
  [BindingBrowsable(false)]
  [Serializable]
  public partial class usp_MasterVoucherGetUnredeemedDataTable : IdeaBlade.Persistence.EntityTable, 
     IdeaBlade.Persistence.Rdb.IRdbTable {
  
    /// <summary>
    /// Initializes a new instance of the usp_MasterVoucherGetUnredeemedDataTable class with no arguments.
    /// </summary>
    public usp_MasterVoucherGetUnredeemedDataTable() {}

    /// <summary>
    /// Initializes a new instance of the usp_MasterVoucherGetUnredeemedDataTable class 
    /// with the <see cref="System.Runtime.Serialization.SerializationInfo"/> 
    /// and the <see cref="System.Runtime.Serialization.StreamingContext"/>.
    /// </summary>
    /// <param name="pInfo">The data needed to serialize or deserialize an object.</param>
    /// <param name="pContext">The source and destination of a given serialized stream.</param>
    protected usp_MasterVoucherGetUnredeemedDataTable(SerializationInfo pInfo, StreamingContext pContext) :
      base(pInfo, pContext) {}

    /// <summary>
    /// Creates a new instance of the <see cref="usp_MasterVoucherGetUnredeemedDataRow"/>.
    /// Constructs a row from the builder.
    /// <para>** For internal use only. **</para>
    /// </summary>
    /// <param name="pRowBuilder"></param>
    /// <returns>The <see cref="usp_MasterVoucherGetUnredeemedDataRow"/>.</returns>
    protected override DataRow NewRowFromBuilder(DataRowBuilder pRowBuilder) {
      return new usp_MasterVoucherGetUnredeemed(pRowBuilder);
    }

    /// <summary>Returns the <see cref="System.Type"/> of <see cref="usp_MasterVoucherGetUnredeemed"/>.</summary>
    protected override Type GetRowType() {
      return typeof(usp_MasterVoucherGetUnredeemed);
    }
    
    /// <summary>
    /// Gets the name of the <see cref="T:IdeaBlade.Rdb.RdbKey"/> or 
    /// <see cref="T:IdeaBlade.Persistence.WS.WsKey"/> defined in <b>IdeaBlade.ibconfig</b>. 
    /// </summary>
    /// <remarks>For this <see cref="usp_MasterVoucherGetUnredeemed"/>, the DataSourceKeyName="default".</remarks>
    public override String DataSourceKeyName {
      get { return @"default"; }
    }

    #region Column properties
    //**************************************
    //* Column properties
    //**************************************
    /// <summary>Gets the TransNo <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn TransNoColumn {
      get {
        if (mTransNoColumn!=null) return mTransNoColumn;
        mTransNoColumn = GetColumn("TransNo", true);
        return mTransNoColumn;
      }
    }
    private DataColumn mTransNoColumn;
    
    /// <summary>Gets the CreatedDate <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn CreatedDateColumn {
      get {
        if (mCreatedDateColumn!=null) return mCreatedDateColumn;
        mCreatedDateColumn = GetColumn("CreatedDate", true);
        return mCreatedDateColumn;
      }
    }
    private DataColumn mCreatedDateColumn;
    
    /// <summary>Gets the VoucherCount <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn VoucherCountColumn {
      get {
        if (mVoucherCountColumn!=null) return mVoucherCountColumn;
        mVoucherCountColumn = GetColumn("VoucherCount", true);
        return mVoucherCountColumn;
      }
    }
    private DataColumn mVoucherCountColumn;
    
    /// <summary>Gets the DriverTotal <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn DriverTotalColumn {
      get {
        if (mDriverTotalColumn!=null) return mDriverTotalColumn;
        mDriverTotalColumn = GetColumn("DriverTotal", true);
        return mDriverTotalColumn;
      }
    }
    private DataColumn mDriverTotalColumn;
    
    #endregion    

    /// <summary>Creates the schema mapping information for this usp_MasterVoucherGetUnredeemedDataTable.</summary>
    /// <returns>
    /// The <see cref="T:IdeaBlade.Persistence.TableMappingInfo"/> for this usp_MasterVoucherGetUnredeemedDataTable.
    /// </returns>
    protected override TableMappingInfo CreateTableMappingInfo() {
      RdbTableMappingInfo mappingInfo = new RdbTableMappingInfo(this);
      mappingInfo.SourceOwnerName = "dbo"; 
      mappingInfo.SourceTableName = "usp_MasterVoucherGetUnredeemed"; 
   
      DataColumnMappingCollection columnMappings = mappingInfo.TableMapping.ColumnMappings;
      columnMappings.Clear();
      columnMappings.Add("TransNo", "TransNo");
      columnMappings.Add("CreatedDate", "CreatedDate");
      columnMappings.Add("VoucherCount", "VoucherCount");
      columnMappings.Add("DriverTotal", "DriverTotal");
      return mappingInfo;
    }
  
    /// <summary>** For internal use only. **</summary>
    protected override void UpdateTableMappingInfo() {
      base.UpdateTableMappingInfo();
    }

    /// <summary>
    /// Initializes usp_MasterVoucherGetUnredeemed <see cref="System.Data.DataColumn"/> properties.
    /// </summary>
    public override void InitColumnProperties() {
      base.InitColumnProperties();

      TransNoColumn.Caption = "TransNo";
      CreatedDateColumn.Caption = "CreatedDate";
      VoucherCountColumn.Caption = "VoucherCount";
      DriverTotalColumn.Caption = "DriverTotal";
    }
  }
  #endregion
  
  #region usp_MasterVoucherGetUnredeemedDataRow
  /// <summary>
  /// The generated base class for <see cref="usp_MasterVoucherGetUnredeemed"/>.
  /// </summary>
  /// <remarks>
  /// <para>
  /// It represents a row of persistable data in the <see cref="usp_MasterVoucherGetUnredeemedDataTable"/>
  /// and derives from <see cref="T:IdeaBlade.Persistence.Entity"/>.
	/// </para>
  /// ****************************************************
  /// <para>** DO NOT MODIFY THIS CLASS - AutoGenerated Code</para>
  /// <para>** Any changes made to this code will be lost</para>
  /// <para>** the next time this code is regenerated.</para>
  /// <para>**</para>
  /// <para>** Generated from:   EntityBaseclass.template</para>
  /// <para>** Last modified:    2012-03-08T14:06:20.7414687-08:00</para>
  /// <para>** Template version: 3.110127</para>
  /// ****************************************************
  /// </remarks>  
  [Serializable]
  public abstract partial class usp_MasterVoucherGetUnredeemedDataRow : IdeaBlade.Persistence.Entity, 
     IdeaBlade.Persistence.Rdb.IRdbEntity {
  
    /// <summary>
    /// Initializes a new instance of the usp_MasterVoucherGetUnredeemedDataRow.
    /// Constructs a row from the builder.
    /// <para>** For internal use only. **</para>
    /// </summary>
    /// <param name="pRowBuilder"></param>
    protected usp_MasterVoucherGetUnredeemedDataRow(DataRowBuilder pRowBuilder)
      : base(pRowBuilder) {
    }

    /// <summary>Gets the <see cref="usp_MasterVoucherGetUnredeemedDataTable"/>.</summary>
    [BindingBrowsable(false)]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public new usp_MasterVoucherGetUnredeemedDataTable TypedTable {
      get { return (usp_MasterVoucherGetUnredeemedDataTable) base.Table; }
    }

    /// <summary>Returns the <see cref="System.Type"/> of <see cref="usp_MasterVoucherGetUnredeemedDataTable"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public static Type GetTableType() {
      return typeof(usp_MasterVoucherGetUnredeemedDataTable);
    }

    
    /// <summary>
    /// Gets the SQL 'where' clause used to restrict which 
    /// database records are accessible via this entity. 
    /// </summary>
    /// <remarks>
    /// The default property value is defined in the Current Class 
    /// Properties tab in the <b>IdeaBlade DevForce Object Mapping Tool</b>.
    /// <para>
    /// The 'where' clause will be used, in addition to any other 
    /// query-specific criteria, in every retrieval of entities of this type.
    /// </para>
    /// </remarks>
    [BindingBrowsable(false)]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public string SqlWhereClause {
      get { return ""; }
    }
    
    /// <summary>
    /// Determines whether to use the 'distinct' keyword when 
    /// generating the SQL select clause for objects of this type.
    /// </summary>
    /// <remarks>Defaults to false, but may be overridden in subclasses.</remarks>
    [BindingBrowsable(false)]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public bool ForceSqlDistinct {
      get { return false; }
    }

    #region Enums
    //**************************************
    //* Enums
    //**************************************
    #endregion
	
    #region Relation properties
    //**************************************
    //* Relation properties
    //**************************************
    #endregion
    
    #region EntityColumn definitions
    //**************************************
    //* Static EntityColumn definitions
    //**************************************
    /// <summary>The TransNo <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn TransNoEntityColumn =
      new EntityColumn(typeof(usp_MasterVoucherGetUnredeemed), "TransNo", typeof(System.String), false, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The CreatedDate <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn CreatedDateEntityColumn =
      new EntityColumn(typeof(usp_MasterVoucherGetUnredeemed), "CreatedDate", typeof(System.DateTime), false, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The VoucherCount <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn VoucherCountEntityColumn =
      new EntityColumn(typeof(usp_MasterVoucherGetUnredeemed), "VoucherCount", typeof(System.Int32), true, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The DriverTotal <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn DriverTotalEntityColumn =
      new EntityColumn(typeof(usp_MasterVoucherGetUnredeemed), "DriverTotal", typeof(System.Decimal), true, false, false, DataSourceAccessType.ReadWrite);      
    #endregion
    
    #region Properties
    //**************************************
    //* TransNo methods
    //**************************************
    /// <summary>Gets the TransNo <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn TransNoColumn {
      get { return TypedTable.TransNoColumn; }
    }

    /// <summary>Gets or sets the TransNo.</summary>
    [MaxTextLength(9)]
    [DBDataType(typeof(System.String))]
    public virtual System.String TransNo {
      get { 
        System.String result_;
        if (GetInterceptor<System.String>("TransNo", GetTransNoImpl, out result_)) return result_;
        return GetTransNoImpl();
      }
      set { 
          if (!SetInterceptor<System.String>("TransNo", value, SetTransNoImpl)) {
            SetTransNoImpl(value);
          }  
      }    
    }
    private System.String GetTransNoImpl() {
      return (System.String) GetColumnValue(TransNoColumn, typeof(System.String), false); 
    }
    private void SetTransNoImpl(System.String value) {
      SetColumnValue(TransNoColumn, value);
    }
    
    //**************************************
    //* CreatedDate methods
    //**************************************
    /// <summary>Gets the CreatedDate <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn CreatedDateColumn {
      get { return TypedTable.CreatedDateColumn; }
    }

    /// <summary>Gets or sets the CreatedDate.</summary>
    [DBDataType(typeof(System.DateTime))]
    public virtual System.DateTime CreatedDate {
      get { 
        System.DateTime result_;
        if (GetInterceptor<System.DateTime>("CreatedDate", GetCreatedDateImpl, out result_)) return result_;
        return GetCreatedDateImpl();
      }
      set { 
          if (!SetInterceptor<System.DateTime>("CreatedDate", value, SetCreatedDateImpl)) {
            SetCreatedDateImpl(value);
          }  
      }    
    }
    private System.DateTime GetCreatedDateImpl() {
      return (System.DateTime) GetColumnValue(CreatedDateColumn, typeof(System.DateTime), false); 
    }
    private void SetCreatedDateImpl(System.DateTime value) {
      SetColumnValue(CreatedDateColumn, value);
    }
    
    //**************************************
    //* VoucherCount methods
    //**************************************
    /// <summary>Gets the VoucherCount <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn VoucherCountColumn {
      get { return TypedTable.VoucherCountColumn; }
    }

    /// <summary>Gets or sets the VoucherCount.</summary>
    [DBDataType(typeof(System.Int32))]
    public virtual Nullable<System.Int32> VoucherCount {
      get { 
        Nullable<System.Int32> result_;
        if (GetInterceptor<Nullable<System.Int32>>("VoucherCount", GetVoucherCountImpl, out result_)) return result_;
        return GetVoucherCountImpl();
      }
      set { 
          if (!SetInterceptor<Nullable<System.Int32>>("VoucherCount", value, SetVoucherCountImpl)) {
            SetVoucherCountImpl(value);
          }
      }    
    }
    private Nullable<System.Int32> GetVoucherCountImpl() {
      return (Nullable<System.Int32>) GetColumnValue(VoucherCountColumn, typeof(System.Int32), true); 
    }
    private void SetVoucherCountImpl(Nullable<System.Int32> value) {
      SetColumnValue(VoucherCountColumn, value);
    }
    
    //**************************************
    //* DriverTotal methods
    //**************************************
    /// <summary>Gets the DriverTotal <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn DriverTotalColumn {
      get { return TypedTable.DriverTotalColumn; }
    }

    /// <summary>Gets or sets the DriverTotal.</summary>
    [DBDataType(typeof(System.Decimal))]
    public virtual Nullable<System.Decimal> DriverTotal {
      get { 
        Nullable<System.Decimal> result_;
        if (GetInterceptor<Nullable<System.Decimal>>("DriverTotal", GetDriverTotalImpl, out result_)) return result_;
        return GetDriverTotalImpl();
      }
      set { 
          if (!SetInterceptor<Nullable<System.Decimal>>("DriverTotal", value, SetDriverTotalImpl)) {
            SetDriverTotalImpl(value);
          }
      }    
    }
    private Nullable<System.Decimal> GetDriverTotalImpl() {
      return (Nullable<System.Decimal>) GetColumnValue(DriverTotalColumn, typeof(System.Decimal), true); 
    }
    private void SetDriverTotalImpl(Nullable<System.Decimal> value) {
      SetColumnValue(DriverTotalColumn, value);
    }
    
    #endregion

    #region usp_MasterVoucherGetUnredeemed.StoredProcRdbQuery
    //**************************************
    //* usp_MasterVoucherGetUnredeemed.StoredProcRdbQuery
    //**************************************    
    /// <summary>
    /// Subclassed <see cref="T:IdeaBlade.Persistence.Rdb.StoredProcRdbQuery"/> which retrieves usp_MasterVoucherGetUnredeemed entities.
    /// </summary>
    [Serializable]
    public class StoredProcRdbQuery : IdeaBlade.Persistence.Rdb.StoredProcRdbQuery {
      /// <summary>Initializes a new instance of the StoredProcRdbQuery class.</summary>
      public StoredProcRdbQuery(
        Object pDriverID
        )
        : base("dbo", "usp_MasterVoucherGetUnredeemed", typeof(usp_MasterVoucherGetUnredeemed)) {
        RdbParameter aParameter;

        aParameter = new RdbParameter("@RETURN_VALUE", DbType.Int32);
        aParameter.Direction = ParameterDirection.ReturnValue;
        aParameter.Size = 0;
        this.Parameters.Add(aParameter);

        aParameter = new RdbParameter("@DriverID", DbType.Int64);
        aParameter.Direction = ParameterDirection.Input;
        aParameter.Size = 0;
        aParameter.Value = pDriverID;
        this.Parameters.Add(aParameter);

      }
    }
    #endregion
  }
  #endregion

  #region EntityPropertyDescriptors.usp_MasterVoucherGetUnredeemedPropertyDescriptor
  public static partial class EntityPropertyDescriptors {

    /// <summary>
    /// Gets the usp_MasterVoucherGetUnredeemedPropertyDescriptor for <see cref="usp_MasterVoucherGetUnredeemed"/>.
    /// </summary>
    public static usp_MasterVoucherGetUnredeemedPropertyDescriptor usp_MasterVoucherGetUnredeemed {
      get { return usp_MasterVoucherGetUnredeemedPropertyDescriptor.Instance; }
    }
    
    /// <summary>
    /// The generated class containing <see cref="usp_MasterVoucherGetUnredeemed"/> PropertyDescriptors.
    /// </summary>
    public partial class usp_MasterVoucherGetUnredeemedPropertyDescriptor : AdaptedPropertyDescriptor {
	
			/// <summary>
      /// Initializes a new instance of the usp_MasterVoucherGetUnredeemedPropertyDescriptor class. 
      /// </summary>
      /// <param name="pParentDescriptor">The parent <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/></param>
      /// <param name="pPropertyPath">The type of the property value</param>
			public usp_MasterVoucherGetUnredeemedPropertyDescriptor(AdaptedPropertyDescriptor pParentDescriptor, String pPropertyPath)
        : base(pParentDescriptor, pPropertyPath) {}
		
      /// <summary>
      /// Initializes a new instance of the usp_MasterVoucherGetUnredeemedPropertyDescriptor class. 
      /// </summary>
      /// <param name="pComponentType">The type of object to which the property is bound</param>
      protected usp_MasterVoucherGetUnredeemedPropertyDescriptor(Type pComponentType)
        : base(pComponentType) {}

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for TransNo.</summary>
      public AdaptedPropertyDescriptor TransNo {
        get { return Get("TransNo"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for CreatedDate.</summary>
      public AdaptedPropertyDescriptor CreatedDate {
        get { return Get("CreatedDate"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for VoucherCount.</summary>
      public AdaptedPropertyDescriptor VoucherCount {
        get { return Get("VoucherCount"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for DriverTotal.</summary>
      public AdaptedPropertyDescriptor DriverTotal {
        get { return Get("DriverTotal"); }
      }

     internal static usp_MasterVoucherGetUnredeemedPropertyDescriptor Instance = new usp_MasterVoucherGetUnredeemedPropertyDescriptor(typeof(usp_MasterVoucherGetUnredeemed));
    }
  }
  #endregion
  
}
