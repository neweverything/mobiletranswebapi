﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

using Dapper;
using TaxiPassCommon;

using CabRideEngineDapper;
using CabRideEngineDapper.Utils;

namespace CabRideEngineDapper {
	public partial class MerchantInfo {


		public static List<MerchantInfo> GetMerchantsForDropDowns(SqlConnection pConn) {
			string sql = @"SELECT MerchantID, MerchantNo + ' - ' + Description MerchantNo FROM dbo.MerchantInfo WITH (NOLOCK)
							ORDER BY MerchantNo";

			return pConn.Query<MerchantInfo>(sql).ToList();
		}

		public static MerchantInfo GetMerchant(long pMerchantID) {
			using (var conn = SqlHelper.OpenSqlConnection()) {
				return conn.Get<MerchantInfo>(pMerchantID);
			}
		}
	}
}
