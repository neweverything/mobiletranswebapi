﻿#if !RideService
using RideService;
#endif

using System;
using System.Collections.Generic;
using TaxiPassCommon;
using TaxiPassCommon.Cryptography;

namespace MobileTransWebAPI.Models {

    public class ChargeInfo {

        public enum WhoPaysFee {
            Passenger,
            Driver,
            Split
        }

        public String ErrorMsg { get; set; }
        public String Platform { get; set; }
        public string AccountNo { get; set; }

        public long AffiliateID { get; set; }

        public long DriverID { get; set; }
        public string DeviceID { get; set; }

        public long AffDriverID { get; set; }
        public string VehicleNo { get; set; }
        public String RequestTime { get; set; }

        public decimal Fare { get; set; }
        public decimal Tolls { get; set; }
        public decimal WaitTime { get; set; }
        public decimal Gratuity { get; set; }
        public decimal TPFee { get; set; }

        // Calculated Pricing Extras
        public decimal BookingFee { get; set; }
        public decimal AirportFee { get; set; }
        public decimal DropFee { get; set; }
        public decimal MiscFee { get; set; }


        public String CardHolder { get; set; }
        public String CardNo { get; set; }
        public String CardType { get; set; }
        public String SwipeData { get; set; }
        public string ZipCode { get; set; }

        public String KSN { get; set; }
        public String SwipeFormat { get; set; }
        public bool homeATMEncrypted { get; set; }

        public int? ExpMonth { get; set; }
        public int? ExpYear { get; set; }
        public String CVV { get; set; }
        public String BillingZipCode { get; set; }

        public string VoucherNo { get; set; }
        public string ExternalVoucherID { get; set; }

        public bool payCash { get; set; }
        public int WhoPaysTPFee { get; set; }

        [Obsolete]
        public bool passengerPaysTPFee { get; set; }


        public GPSInfo GpsStart { get; set; }
        public GPSInfo GpsEnd { get; set; }

        public String TextMsgCell { get; set; }
        public string Signature { get; set; }
        public string PassengerName { get; set; }
        public string TripID { get; set; }
        public string ReferenceNo { get; set; }

        public bool StoreForward { get; set; }
        public string ChargeDate { get; set; }
        public bool PayDriverPayCard { get; set; }

        // Generic placeholder, used by GetRide to denote a "Ride" charge (I.E.  Ride ResNo, ABC123)
        public List<ClassSettings> SettingsDict { get; set; }


        public JobDetail JobDetail;


        // Dejavoo


        public int Passengers { get; set; }


        private string[] mKeys = new string[] { "042666B49184CF5C68DE9628D0397B36", "0123456789ABCDEFFEDCBA9876543210", "B91AD3C2FC821B2FCEBB3EFB5BD66167" };
        private const string BDK = "739040FABB9131C764A8BD6D05593474";

        public string CardExpires {
            get {
                return StringUtils.PadLeft(this.ExpMonth.ToString(), 2, "0") + StringUtils.PadLeft(this.ExpYear.ToString(), 2, "0");
            }
        }

        // Used by Dejavoo
        public string HardwareId { get; set; }
        public string HardwarePhoneNumber { get; set; }
        public string DriverPhoneNumber { get; set; }
        public string CardNumberDisplay { get; set; }
        public string CardToken { get; set; }
        public string CardProcessingResult { get; set; }
        public string BatchNumber { get; set; }


        public string DecryptHomeATMSwipe() {
            // My swipe returns format: 38, Marty's Swipe returns 36
            try {
                if (SwipeFormat.Equals("38")) {
                    Decryptor decrypt = new Decryptor(BDK);
                    int iResult = decrypt.Decrypt2(KSN, SwipeData);
                    if (iResult == 0) {
                        return decrypt.GetTrack2();
                    }
                }
                if (!SwipeFormat.Equals("36")) {
                    foreach (string key in mKeys) {
                        Decryptor decrypt = new Decryptor(key);
                        int iResult = decrypt.Decrypt(KSN, SwipeData);

                        if (iResult == 0) {
                            return decrypt.GetTrack2();
                        }

                        iResult = decrypt.Decrypt2(KSN, SwipeData);
                        if (iResult == 0) {
                            return decrypt.GetTrack2();
                        }
                    }
                }
                //return "";


                //string swipeData = "8a47788c6c418e53b2fa3ead8d6bbfd088fceb60b1254d28eb1c86f47e78f93e66cdf3956e86ad25924701ce4beb43ff8445c3fd760c076de1ffc8c86dc85c15febd8769d5bec9f86dc23d4d371d330928a1467173d9015ab83e30da7de494e77ccd495781f30a8d10e687cafa68d0ef4415f2ac98243832";
                //new Swipes 
                //SwipeData = "fec6fd359aa13525622d6eea9034752174af570fef8a23850203f648e5d87424ccbc29f4ba0ae255b4c9ffa3ed44e2eb04bd89e7a4bfa4f9c6dd1e65cbae05000a2e43dbe59d4e90fcf83ae82df5e5cabe2f34f69086861ad222544b5bb4c18709daf08b3db44c15c796af286a8f4b57a0ecfc103cf42475";

                //Decrypt data

                string result = "";

                foreach (string key in mKeys) {
                    result = HomeATMCrypto.TDES.DecryptCBC(SwipeData, key);
                    if (!result.IsNullOrEmpty()) {
                        break;
                    }
                }

                if (result.IsNullOrEmpty()) {
                    foreach (string key in mKeys) {
                        Decryptor decrypt = new Decryptor(key);
                        result = decrypt.Decrypt_CBC(KSN, SwipeData);
                        if (!result.IsNullOrEmpty()) {
                            break;
                        }
                    }
                }
                return result;
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
            return "";
        }
    }
}
