﻿namespace MobileTransWebAPI.Models {
    public partial class DailyTotals {

        public long AffiliateID { get; set; }
        public long AffDriverID { get; set; }

        public string StartDate { get; set; }
        public string EndDate { get; set; }
    }
}