﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using System.Collections.Generic;
using System.Linq;

using IdeaBlade.Persistence;
using IdeaBlade.Util;
using IdeaBlade.Persistence.Rdb;

using CabRideEngine.Utils;
using TaxiPassCommon;

using Csla.Validation;
using System.Text;

namespace CabRideEngine {
	/// <summary>
	/// The developer class representing the Vehicles business object class.
	/// </summary>
	/// <remarks>
	/// <para>Place all application-specific business logic in this class.</para>
	/// <para>
	/// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
	/// and will not overwrite changes.
	/// </para>
	/// <para>
	/// This class inherits from the generated class, its companion in the pair of classes 
	/// defining a business object. Properties in the generated class may be overridden in 
	/// order to change the behavior or add business logic.
	/// </para>
	/// <para>
	/// This class is the final class in the inheritance chain for this business object. 
	/// It should not be inherited.
	/// </para>
	/// </remarks>
	[Serializable]
	public sealed partial class Vehicles : VehiclesDataRow {

		#region Constructors -- DO NOT MODIFY

		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new';
		// it is also required if the class is visible from a web service.
		// DO NOT REMOVE **
		private Vehicles()
			: this(null) {
		}

		/// <summary>
		/// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
		/// </summary>
		/// <param name="pRowBuilder"></param>
		public Vehicles(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static Vehicles Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      Vehicles aVehicles = pManager.CreateEntity<Vehicles>();
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aVehicles, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aVehicles.AddToManager();
		//      return aVehicles;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		// Add additional logic to your business object here...
		#region Standard Entity logic to create/audit/validate the entity

		// Add additional logic to your business object here...
		public static Vehicles Create(Affiliate pAffiliate) {
			PersistenceManager oPM = pAffiliate.PersistenceManager;

			Vehicles oVehicle = null;

			try {
				oVehicle = oPM.CreateEntity<Vehicles>();

				// Using the IdeaBlade Id Generation technique
				//oPM.GenerateId(oVehicle, Vehicles.VehicleIDEntityColumn);
				oVehicle.AffiliateID = pAffiliate.AffiliateID;
				oVehicle.LicenseState = pAffiliate.State;
				oVehicle.AddToManager();

			} catch (Exception ex) {
				throw ex;
			}
			return oVehicle;
		}


		public static Vehicles Create(PersistenceManager pPM, long pAffiliateID) {
			Affiliate aff = Affiliate.GetAffiliate(pPM, pAffiliateID);
			return Create(aff);
		}

		//protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
		//	if (IsDeserializing) {
		//		return;
		//	}
		//	base.OnColumnChanging(pArgs);
		//	DoAudit(pArgs);
		//}

		//private void DoAudit(DataColumnChangeEventArgs pArgs) {
		//	if (!this.PersistenceManager.IsClient) {
		//		return;
		//	}
		//	if (this.RowState == DataRowState.Added) {
		//		return;
		//	}
		//	if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
		//		AuditRecord = VehiclesAudit.Create(this);
		//	}
		//}

		protected override void AddRules(RuleList pList) {
			PropertyRequiredRule.AddToList(pList, VehicleNoEntityColumn.ColumnName);
			PropertyRequiredRule.AddToList(pList, VehicleTypeIDEntityColumn.ColumnName);
			pList.Add(IsVehicleNoUnique, VehicleNoEntityColumn.ColumnName);
		}

		private bool IsVehicleNoUnique(object pTarget, RuleArgs e) {
			Vehicles oVehicle = pTarget as Vehicles;
			if (string.IsNullOrEmpty(oVehicle.VehicleNo)) {
				return false;
			}
			RdbQuery qry = new RdbQuery(typeof(Vehicles), Vehicles.VehicleNoEntityColumn, EntityQueryOp.EQ, oVehicle.VehicleNo);
			qry.AddClause(Vehicles.AffiliateIDEntityColumn, EntityQueryOp.EQ, oVehicle.AffiliateID);
			qry.AddClause(Vehicles.VehicleIDEntityColumn, EntityQueryOp.NE, oVehicle.VehicleID);
			EntityList<Vehicles> oList = this.PersistenceManager.GetEntities<Vehicles>(qry, QueryStrategy.DataSourceOnly);
			if (oList.Count > 0) {
				e.Description = "A vehicle already exists with the Vehicle No: " + oVehicle.VehicleNo;
				return false;
			}
			return true;
		}

		#endregion

		#region Custom Fields

		[BindingBrowsable(false)]
		public override string EntityInstanceDisplayName {
			get {
				return this.VehicleNo;
			}
		}

		public string MyAffiliateName {
			get {
				return this.Affiliate.Name;
			}
		}

		public string OutOfServiceGridView {
			get {
				if (OutOfService) {
					return "Yes";
				}
				return "";
			}
		}

		public string SuspendedGridView {
			get {
				return Suspended ? "Yes" : "";
			}
		}

		public bool Suspended {
			get {
				if (string.IsNullOrEmpty(this.Affiliate.LicenseNumber)) {
					return false;
				}
				MasterVehicles oVehicle = MasterVehicles.GetVehicleByLicensePlate(this.Affiliate, this.LicensePlate);
				return oVehicle.IsNullEntity;
			}
		}


		#endregion

		#region Retrieve Records

		public static EntityList<Vehicles> GetVehicles(EntityList<Affiliate> pAffiliateList) {
			PersistenceManager oPM = pAffiliateList[0].PersistenceManager;
			List<long> affIdList = (from p in pAffiliateList
									select p.AffiliateID).ToList();
			RdbQuery qry = new RdbQuery(typeof(Vehicles), AffiliateIDEntityColumn, EntityQueryOp.In, affIdList);
			qry.AddOrderBy(VehicleNoEntityColumn, System.ComponentModel.ListSortDirection.Ascending);
			return oPM.GetEntities<Vehicles>(qry);
		}


		// ************************************************************************************************************
		// return an Affiliate's vehicles
		// ************************************************************************************************************
		public static EntityList<Vehicles> GetVehicles(Affiliate pAffiliate) {
			return GetData(pAffiliate.PersistenceManager, pAffiliate.AffiliateID.ToString(), "", "", "");
		}


		// ************************************************************************************************************
		// return a vehicle
		// ************************************************************************************************************
		public static Vehicles GetVehicle(PersistenceManager pPM, long pVehicleID) {
			EntityList<Vehicles> oVehicles = GetData(pPM, "", "", "", pVehicleID.ToString());
			if (oVehicles.Count == 0) {
				return pPM.GetNullEntity<Vehicles>();
			} else {
				return oVehicles[0];
			}
		}

		// ************************************************************************************************************
		// return an Affiliate's active vehicles
		// ************************************************************************************************************
		public static EntityList<Vehicles> GetActiveVehicles(Affiliate pAffiliate) {
			return GetData(pAffiliate.PersistenceManager, pAffiliate.AffiliateID.ToString(), "0", "", "");
		}

		// ************************************************************************************************************
		// return an Affiliate's vehicle by VehicleNo
		// ************************************************************************************************************
		public static Vehicles GetVehicle(Affiliate pAffiliate, string pVehicleNo) {
			EntityList <Vehicles> oVehicles = GetData(pAffiliate.PersistenceManager, pAffiliate.AffiliateID.ToString(), "0", pVehicleNo, "");
			if (oVehicles.Count == 0) {
				return pAffiliate.PersistenceManager.GetNullEntity<Vehicles>();
			} else {
				return oVehicles[0];
			}
		}


		public static Vehicles GetVehicle(PersistenceManager pPM, long pAffiliateID, string pVehicleNo) {
			EntityList<Vehicles> oVehicles = GetData(pPM, pAffiliateID.ToString(), "0", pVehicleNo, "");
			if (oVehicles.Count == 0) {
				return pPM.GetNullEntity<Vehicles>();
			} else {
				return oVehicles[0];
			}
		}

		// ************************************************************************************************************
		// pOutOfService is either "", "1" for true, "0" for false
		// ************************************************************************************************************
		private static EntityList<Vehicles> GetData(PersistenceManager pPM, string pAffiliateid, string pOutOfService, string pVehicleNo, string pID) {
			string sDelimiter = "";
			int iArgCount = 0;

			StringBuilder sql = new StringBuilder();
			sql.Append("usp_VehiclesGet ");
			if (!pAffiliateid.IsNullOrEmpty()) {
				sql.AppendFormat(" @AffiliateID = {0}", pAffiliateid);
				iArgCount++;
			}
			if (!pOutOfService.IsNullOrEmpty()) {
				if (iArgCount > 0) {
					sDelimiter = ", ";
				}
				sql.AppendFormat("{0}@OutOfService = {1}", sDelimiter, pOutOfService);
				iArgCount++;
			}
			if (!pVehicleNo.IsNullOrEmpty()) {
				if (iArgCount > 0) {
					sDelimiter = ", ";
				}
				sql.AppendFormat("{0} @VehicleNo = '{1}'", sDelimiter, pVehicleNo);
				iArgCount++;
			}
			if (!pID.IsNullOrEmpty()) {
				if (iArgCount > 0) {
					sDelimiter = ", ";
				}
				sql.AppendFormat("{0} @VehicleID = {1}", sDelimiter, pID);
				iArgCount++;
			}

			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(Vehicles), sql.ToString());
			EntityList<Vehicles> oVehicles = pPM.GetEntities<Vehicles>(qry);
			return oVehicles;
		}



		public static List<string> GetVehicleNumbers(Affiliate pAffiliate) {
			List<string> vehiList = new List<string>();
			EntityList<Vehicles> list = GetVehicles(pAffiliate);
			vehiList = (from p in list
						select p.VehicleNo).ToList();

			return vehiList;
		}
		#endregion
	}

	#region EntityPropertyDescriptors.VehiclesPropertyDescriptor
	////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
	////    For example: 
	////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
	//public static partial class EntityPropertyDescriptors {
	//	public partial class VehiclesPropertyDescriptor : BaseEntityPropertyDescriptor {
	//    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
	//    public AdaptedPropertyDescriptor Age {
	//      get { return this.Get("Age"); }
	//    }
	//  }
	//} 
	#endregion

}
