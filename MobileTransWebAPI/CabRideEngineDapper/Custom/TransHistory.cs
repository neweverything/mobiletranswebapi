﻿using CabRideEngineDapper.Utils;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CabRideEngineDapper.Custom {
    public class TransHistory {

        public DateTime ChargeDate { get; set; }
        public decimal? DriverFee { get; set; }
        public decimal DriverTotalPaid { get; set; }
        public bool CarmelDispatch { get; set; }
        public bool Test { get; set; }
        public string TransNo { get; set; }
        public string CardNumberDisplay { get; set; }
        public DateTime? DriverPaidDate { get; set; }
        public DateTime? ACHPaidDate { get; set; }
        public long? TaxiPassRedeemerID { get; set; }
        public bool DriverPaid { get; set; }

        public static List<TransHistory> GetTransHistory(long pDriverID, DateTime pStartDate, DateTime pEndDate, bool pIncludeTest) {
            using (var conn = SqlHelper.OpenSqlConnection()) {
                string sql = @"SELECT  ChargeDate
                                      , DriverPayments.DriverFee
                                      , CASE WHEN IncludeRedeemerRedemptionFeeInACH = 1 THEN Fare + Gratuity + AirportFee + MiscFee + ISNULL(WaitTime, 0) + ISNULL(Tolls, 0) + ISNULL(Parking, 0) + ISNULL(Stops, 0) + ISNULL(DropFee, 0) + ISNULL(BookingFee, 0) - ISNULL(Discount, 0) - ISNULL(DriverPayments.DriverFee, 0)
                                             ELSE Fare + Gratuity + AirportFee + MiscFee + ISNULL(WaitTime, 0) + ISNULL(Tolls, 0) + ISNULL(Parking, 0) + ISNULL(Stops, 0) + ISNULL(DropFee, 0) + ISNULL(BookingFee, 0) - ISNULL(Discount, 0) - ISNULL(DriverPayments.DriverFee, 0) - ISNULL(RedemptionFee, 0)
                                        END DriverTotalPaid
                                      , CarmelDispatch
                                      , Test
                                      , TransNo
                                      , CardNumberDisplay
                                      , DriverPaidDate
                                      , ACHPaidDate
                                      , DriverPayments.TaxiPassRedeemerID
                                      , DriverPaid
                                FROM    DriverPayments
                                LEFT JOIN DriverPaymentAux ON DriverPaymentAux.DriverPaymentID = DriverPayments.DriverPaymentID
                                LEFT JOIN ACHDetail ON ACHDetail.DriverPaymentID = DriverPayments.DriverPaymentID
                                LEFT JOIN TaxiPassRedeemers ON TaxiPassRedeemers.TaxiPassRedeemerID = DriverPayments.TaxiPassRedeemerID
                                LEFT JOIN TaxiPassRedeemerAccounts ON TaxiPassRedeemerAccounts.TaxiPassRedeemerAccountID = TaxiPassRedeemers.TaxiPassRedeemerAccountID
                                LEFT JOIN Affiliate ON Affiliate.AffiliateID = DriverPayments.AffiliateID
                                LEFT JOIN AffiliateDriverDefaults ON AffiliateDriverDefaults.AffiliateID = Affiliate.AffiliateID
                                WHERE   AffiliateDriverID = @AffiliateDriverID
                                        AND ChargeDate >= @StartDate
                                        AND ChargeDate <= @EndDate
                                        AND Fare > 0
                                        AND Failed = 0
                                        AND (Test = 0
                                             OR Test = @Test
                                            )
                                        AND ISNULL(DriverPayments.DoNotPay, 0) = 0
                                        AND ISNULL(DriverPaymentAux.DoNotPay, 0) = 0
                                        AND DriverPayments.TransNo NOT IN (SELECT   ValueString
                                                                           FROM     DriverPaymentRef
                                                                           WHERE    ReferenceGroup = 'Refund'
                                                                                    AND ReferenceKey = 'Original TransNo')
                                ORDER BY ChargeDate DESC";
                return conn.Query<TransHistory>(sql, new { AffiliateDriverID = pDriverID, StartDate = pStartDate, EndDate = pEndDate, Test = pIncludeTest }).ToList();
            }
        }

        public static List<TransHistory> GetTransHistoryForHandHelds(long pDriverID, DateTime pStartDate, DateTime pEndDate, bool pIncludeTest) {
            using (var conn = SqlHelper.OpenSqlConnection()) {
                string sql = @"SELECT  ChargeDate
                                      , DriverPayments.DriverFee
                                      , CASE WHEN IncludeRedeemerRedemptionFeeInACH = 1 THEN Fare + TaxiPassFee + Gratuity + AirportFee + MiscFee + ISNULL(WaitTime, 0) + ISNULL(Tolls, 0) + ISNULL(Parking, 0) + ISNULL(Stops, 0) + ISNULL(DropFee, 0) + ISNULL(BookingFee, 0) - ISNULL(Discount, 0) - ISNULL(DriverPayments.DriverFee, 0)
                                             ELSE Fare + TaxiPassFee + Gratuity + AirportFee + MiscFee + ISNULL(WaitTime, 0) + ISNULL(Tolls, 0) + ISNULL(Parking, 0) + ISNULL(Stops, 0) + ISNULL(DropFee, 0) + ISNULL(BookingFee, 0) - ISNULL(Discount, 0) - ISNULL(DriverPayments.DriverFee, 0) - ISNULL(RedemptionFee, 0)
                                        END DriverTotalPaid
                                      , CarmelDispatch
                                      , Test
                                      , TransNo
                                      , CardNumberDisplay
                                      , DriverPaidDate
                                      , ACHPaidDate
                                      , DriverPayments.TaxiPassRedeemerID
                                      , DriverPaid
                                FROM    DriverPayments
                                LEFT JOIN DriverPaymentAux ON DriverPaymentAux.DriverPaymentID = DriverPayments.DriverPaymentID
                                LEFT JOIN ACHDetail ON ACHDetail.DriverPaymentID = DriverPayments.DriverPaymentID
                                LEFT JOIN TaxiPassRedeemers ON TaxiPassRedeemers.TaxiPassRedeemerID = DriverPayments.TaxiPassRedeemerID
                                LEFT JOIN TaxiPassRedeemerAccounts ON TaxiPassRedeemerAccounts.TaxiPassRedeemerAccountID = TaxiPassRedeemers.TaxiPassRedeemerAccountID
                                LEFT JOIN Affiliate ON Affiliate.AffiliateID = DriverPayments.AffiliateID
                                LEFT JOIN AffiliateDriverDefaults ON AffiliateDriverDefaults.AffiliateID = Affiliate.AffiliateID
                                WHERE   DriverID = @DriverID
                                        AND ChargeDate >= @StartDate
                                        AND ChargeDate <= @EndDate
                                        AND Fare > 0
                                        AND Failed = 0
                                        AND (Test = 0
                                             OR Test = @Test
                                            )
                                        AND ISNULL(DriverPayments.DoNotPay, 0) = 0
                                        AND ISNULL(DriverPaymentAux.DoNotPay, 0) = 0
                                        AND DriverPayments.TransNo NOT IN (SELECT   ValueString
                                                                           FROM     DriverPaymentRef
                                                                           WHERE    ReferenceGroup = 'Refund'
                                                                                    AND ReferenceKey = 'Original TransNo')
                                ORDER BY ChargeDate DESC";
                return conn.Query<TransHistory>(sql, new { DriverID = pDriverID, StartDate = pStartDate, EndDate = pEndDate, Test = pIncludeTest }).ToList();
            }
        }

    }
}
