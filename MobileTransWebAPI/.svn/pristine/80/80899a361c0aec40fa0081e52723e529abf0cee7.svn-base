﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using System.Web.Script.Serialization;
using System.IO;
using System.Drawing;
using System.Web.Hosting;
using System.Text;

using IdeaBlade.Persistence;

using MobileTransWebAPI.Models;

using CabRideEngine;

using TaxiPassCommon;
using System.Web;

namespace MobileTransWebAPI.Controllers {
	public class CreditCardReceiptController : ApiController {


		// *************************************************************
		// Receipt Request
		// *************************************************************
		public bool PostReceiptRequest(ReceiptRequestInfo pRequest) {
			JavaScriptSerializer js = new JavaScriptSerializer();

			WebApiApplication.LogIt("CreditCardReceiptRequest", js.Serialize(pRequest));

			PersistenceManager oPM = WebApiApplication.UserPM;
			if (oPM == null) {
				return false;
			}

			ReceiptRequestInfo info = pRequest;  // js.Deserialize<ReceiptRequestInfo>(pRequest.DecryptIceKey());
			if (info.TransNo.IsNullOrEmpty() || info.ReceiptTo.IsNullOrEmpty()) {
				return false;
			}

			DriverPayments oDriverPayments = DriverPayments.GetPayment(oPM, info.TransNo);
			if (oDriverPayments.IsNullEntity) {
				return false;
			}

			//DateTime requestDate = Convert.ToDateTime(info.RequestTime.DecryptIceKey());

			DriverPaymentGeoCodes geo = oDriverPayments.DriverPaymentGeoCodes;
			if (geo.IsNullEntity) {
				geo = DriverPaymentGeoCodes.Create(oDriverPayments);
			}
			geo.SendReceiptTo = info.ReceiptTo;
			geo.Save();

			string id = String.Format("dp={0}", oDriverPayments.TransNo).EncryptIceKey();

			string receiptURL = String.Format("{0}{1}", Properties.Settings.Default.TaxiPassReceiptURL, id);
			if (!oDriverPayments.Affiliate.ServiceType.ToLower().Contains("taxi")) {
				receiptURL = String.Format("{0}{1}", Properties.Settings.Default.LimoPassReceiptURL, id);
			}

			if (info.ReceiptTo.Contains("@")) {
				SendReceiptViaEMail(info.ReceiptTo, BuildHTMLReceipt(oPM, oDriverPayments, oDriverPayments.Affiliate.ServiceType));
			} else {
				SendReceiptViaSMS(oPM, info.ReceiptTo, BuildSMSReceipt(oDriverPayments, id));
			}

			return true;
		}


		// *****************************************************************
		// Send the recept via sms
		// *****************************************************************
		public void SendReceiptViaSMS(PersistenceManager pPM, string pCellNumber, string pMessage) {
			MobileTransWebAPI.Utils.Common.SendSMS(SystemDefaults.GetDefaults(pPM), pCellNumber, pMessage);
		}


		// *****************************************************************
		// Email the receipt
		// *****************************************************************
		public void SendReceiptViaEMail(string pEMailTo, string pMessage) {
			string subject = "LimoPass Receipt";
			string sFrom = Properties.Settings.Default.EMailFrom;
			if (pMessage.Contains("GetRide")) {
				subject = "GetRide Receipt";
				sFrom = Properties.Settings.Default.EMailFromGetRide;
			} else if (pMessage.Contains("TaxiPass")) {
				subject = "TaxiPass Receipt";				
			}
			TaxiPassCommon.Mail.EMailMessage email = new TaxiPassCommon.Mail.EMailMessage(sFrom, subject, pMessage);
			email.UseWebServiceToSendMail = true;
			email.SendMail(pEMailTo, true);
		}


		// **************************************************************
		// Build a text only receipt for phone messaging
		// **************************************************************
		private string BuildSMSReceipt(DriverPayments pDriverPayments, string pID) {
			StringBuilder msg = new StringBuilder();
			//msg.Append(String.Format("\n\n{0}{1}", Properties.Settings.Default.TaxiPassReceiptURL, id));
			if (pDriverPayments.Affiliate.ServiceType.ToLower().Contains("taxi")) {
				msg.Append(String.Format("Receipt for TaxiPass Voucher Number {0}", pDriverPayments.Test ? "Test" : pDriverPayments.TransNo));
				msg.Append(String.Format("\n\n{0}{1}", Properties.Settings.Default.TaxiPassReceiptURL, pID));
			} else {
				msg.Append(String.Format("Receipt for LimoPass Voucher Number {0}", pDriverPayments.Test ? "Test" : pDriverPayments.TransNo));
				msg.Append(String.Format("\n\n{0}{1}", Properties.Settings.Default.LimoPassReceiptURL, pID));
			}
			return msg.ToString();
		}


		// *************************************************************
		// Build a voucher receipt to email
		// Get Ride.TaxiPassFee is calculated:
		//		Calc total ServiceTipPerc
		//			'Half' to Gratuity
		//			Remainder goes to TPFee
		//		If a Ride coupon was used 
		//			Deduct the amt from TPFee
		// *************************************************************
		public string BuildHTMLReceipt(PersistenceManager pPM, DriverPayments pDriverPayments, string pLogo) {
			
			decimal dGratuity = pDriverPayments.Gratuity;
			decimal dDiscount = pDriverPayments.Discount;
			bool blnIsRide	 = false;
			string sReceipt = "starting";
			string sMarketing = "";
			StringBuilder sWork = new StringBuilder();
			int iStart;
			int iEnd;

			try {
				// sPath = @"C:\My Projects\VS2010 TaxiPass\MobileTransWebAPI\MobileTransWebAPI\HTMLTemplates\VoucherReceipt.htm";
				string sPath = Path.Combine(Properties.Settings.Default.TemplatePath, "VoucherReceipt.htm");
				if (HttpContext.Current.Request.Url.ToString().ToLower().Contains("localhost")) {
					sPath = @"C:\My Projects\VS2010 TaxiPass\MobileTransWebAPI\MobileTransWebAPI\HTMLTemplates\VoucherReceipt.htm";
					pLogo = "ride";
				}
				WebApiApplication.LogIt("Voucher Receipt Path", sPath);

				sReceipt = System.IO.File.ReadAllText(sPath);

				// Logo
				string sLogo = "https://driverpay.1800cabride.com/images/taxipass-logo.GIF";

				//string sCompany =  "TaxiPass";
				switch (pLogo.ToLower()) { 
					case "taxi":
						sLogo = "https://driverpay.1800cabride.com/images/taxipass-logo.GIF";
						sMarketing = "Save $5.00 on your first taxi when booked thru <a href='http://www.getride.com/App'>GetRide</a><span style='font-size: small'>&#0153;</span> iPhone/Android booking app";
						break;

					case "ride":
						blnIsRide = true;
						dGratuity = pDriverPayments.Gratuity + pDriverPayments.TaxiPassFee;
						dDiscount = pDriverPayments.RideCouponAmount;
						sLogo = "http://getride.com/images/logoBlackText.png";
						sMarketing = "GetRide Receipt";
						break;

					default:
						sLogo = "https://driverpay.1800cabride.com/images/limopasslogo.JPG";
						sMarketing = "LimoPass  Receipt";
						break;
				}
				sReceipt = sReceipt.Replace("|logo", sLogo);

				// sMarketing
				sReceipt = sReceipt.Replace("|Marketing", sMarketing);


				// Fleet
				string sFleet = pDriverPayments.Affiliate.Name;
				if (pDriverPayments.Drivers.FleetID.HasValue) {
					sFleet = pDriverPayments.Drivers.Fleets.Fleet;
				}
				sReceipt = sReceipt.Replace("|fleet", sFleet);

				// Cab #
				if (pDriverPayments.VehicleID.HasValue) {
					sReceipt = sReceipt.Replace("|taxinumber", pDriverPayments.Vehicles.VehicleNo);
				} else {
					iStart = sReceipt.IndexOf("<tr id='trCab'>");
					iEnd = sReceipt.IndexOf("</tr>", iStart) + 5;
					sReceipt = sReceipt.Substring(0, iStart - 1) + sReceipt.Substring(iEnd);
				}

				// Date
				sReceipt = sReceipt.Replace("|date", String.Format("{0:f}", pDriverPayments.ChargeDate));

				// TransNo
				sReceipt = sReceipt.Replace("|transno", pDriverPayments.TransNo);

				// Card
				sReceipt = sReceipt.Replace("|card", pDriverPayments.CardNumberDisplay);

				// Fare
				sReceipt = sReceipt.Replace("|fare", pDriverPayments.Fare.ToString("C"));
				

				// Tolls
				if (pDriverPayments.Tolls.GetValueOrDefault(0) == 0) {
					iStart = sReceipt.IndexOf("<tr id='trTolls'>");
					iEnd = sReceipt.IndexOf("</tr>", iStart) + 5;
					sReceipt = sReceipt.Substring(0, iStart - 1) + sReceipt.Substring(iEnd);
				} else {
					sReceipt = sReceipt.Replace("|tolls", pDriverPayments.Tolls.GetValueOrDefault(0).ToString("C"));
					
				}


				// Airport Fee
				if (pDriverPayments.AirportFee == 0) {
					iStart = sReceipt.IndexOf("<tr id='trAirport'>");
					iEnd = sReceipt.IndexOf("</tr>", iStart) + 5;
					sReceipt = sReceipt.Substring(0, iStart - 1) + sReceipt.Substring(iEnd);
				} else {
					sReceipt = sReceipt.Replace("|airport", pDriverPayments.AirportFee.ToString("C"));
					
				}

				// Misc Fee
				if (pDriverPayments.MiscFee == 0) {
					iStart = sReceipt.IndexOf("<tr id='trMisc'>");
					iEnd = sReceipt.IndexOf("</tr>", iStart) + 5;
					sReceipt = sReceipt.Substring(0, iStart - 1) + sReceipt.Substring(iEnd);
				} else {
					sReceipt = sReceipt.Replace("|misc", pDriverPayments.MiscFee.ToString("C"));
					
				}

				// Gratuity
				if (blnIsRide) {
					sReceipt = sReceipt.Replace("+ Gratuity:", "+ Service:");
					sReceipt = sReceipt.Replace("|gratuity", dGratuity.ToString("C"));
				} else {
					sReceipt = sReceipt.Replace("|gratuity", dGratuity.ToString("C"));
				}

				// Discount
				if (dDiscount == 0) {
					iStart = sReceipt.IndexOf("<tr id='trDiscount'>");
					iEnd = sReceipt.IndexOf("</tr>", iStart) + 5;
					sReceipt = sReceipt.Substring(0, iStart - 1) + sReceipt.Substring(iEnd);
				} else {
					sReceipt = sReceipt.Replace("|discount", dDiscount.ToString("C"));
				}

				// Total
				sReceipt = sReceipt.Replace("|total", pDriverPayments.TotalCharge.ToString("C"));

				// TaxiPass Fee
				if (pDriverPayments.TaxiPassFee == 0 || blnIsRide) {
					iStart = sReceipt.IndexOf("<tr id='trTaxiPassFee'>");
					iEnd = sReceipt.IndexOf("</tr>", iStart) + 5;
					sReceipt = sReceipt.Substring(0, iStart - 1) + sReceipt.Substring(iEnd);
				} else {
					sReceipt = sReceipt.Replace("|fee", pDriverPayments.TaxiPassFee.ToString("C") + " voucher fee");
				}

				// Signature image
				if (pDriverPayments.DriverPaymentAux.SignaturePath.IsNullOrEmpty()) {
					iStart = sReceipt.IndexOf("<tr id='trSignature'>");
					iEnd = sReceipt.IndexOf("</tr>", iStart) + 5;
					sReceipt = sReceipt.Substring(0, iStart - 1) + sReceipt.Substring(iEnd);
				} else {
					string ImgPath = ResizeSignatureImage(pPM, pDriverPayments.DriverPaymentAux.SignaturePath.Replace(@"\", "/"));
					sReceipt = sReceipt.Replace("|signature", ImgPath);
				}

				// Receipt.com hyperlink
				if (blnIsRide) {
					sWork.AppendFormat("<a href='{0}{1}'>", Properties.Settings.Default.GetRideReceiptURL, ("DP=" + pDriverPayments.TransNo).EncryptIceKey());
				} else {
					sWork.AppendFormat("<a href='{0}{1}'>", Properties.Settings.Default.TaxiPassReceiptURL, ("DP=" + pDriverPayments.TransNo).EncryptIceKey());
				}
				sReceipt = sReceipt.Replace("|HeaderLink", sWork.ToString());

				sWork.Append("Get Receipt</A>");
				sReceipt = sReceipt.Replace("|ReceiptLink", sWork.ToString());
			} catch (Exception ex) {
				sReceipt = ex.Message + Environment.NewLine + Environment.NewLine + sReceipt;
			}
			return sReceipt;
		}

		// **************************************************************
		// Voucher signatures are too big for Receipts, shrink em down
		// **************************************************************
		private string ResizeSignatureImage(PersistenceManager pPM, string pImage) {
			SystemDefaults oSysDefaults = SystemDefaults.GetDefaults(pPM);

			string pImgRoot = Properties.Settings.Default.SignaturePath;
			if (!pImgRoot.EndsWith(@"\")) {
				pImgRoot += @"\";
			}

			string sNewPath = "";
			try {
				sNewPath = pImgRoot;

				Image oImg = Image.FromFile(sNewPath + pImage.Replace("/", @"\"));
				TaxiPassCommon.Images.ImageHelper oImgHelper = new TaxiPassCommon.Images.ImageHelper();
				Size oSize = new Size(340, 128);

				oImg = oImgHelper.ResizeImage(oImg, oSize);

				sNewPath += Path.GetDirectoryName(pImage) + @"\Resized\";
				if (!Directory.Exists(sNewPath)) {
					Directory.CreateDirectory(sNewPath);
				}
				if (!File.Exists(sNewPath + Path.GetFileName(pImage))) {
					oImg.Save(sNewPath + Path.GetFileName(pImage));
				}

				sNewPath = oSysDefaults.VoucherImageURL.ToString().ToLower().Replace("vouchers", "Signatures")
					+ Path.GetDirectoryName(pImage).Replace(@"\", "/")
					+ @"/Resized/" + Path.GetFileName(pImage);
			} catch (Exception ex) {
				sNewPath = "Error: " + ex.Message;
			}
			return sNewPath;
		}

	}
}
