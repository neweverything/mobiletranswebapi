﻿using CabRideEngineDapper.Utils;

namespace CabRideEngineDapper {
    public partial class TaxiPassCardTransactions {

        private DriverPayments mDriverPayments;
        public DriverPayments DriverPayments {
            get {
                if (!DriverPaymentID.HasValue) {
                    return DriverPaymentsTable.GetNullEntity();
                }
                if (mDriverPayments == null || mDriverPayments.IsNullEntity) {
                    using (var conn = SqlHelper.OpenSqlConnection()) {
                        mDriverPayments = CabRideEngineDapper.DriverPayments.GetPayment(conn, this.DriverPaymentID.Value);
                    }
                }
                return mDriverPayments;
            }
        }

        private TaxiPassCards mTaxiPassCards;
        public TaxiPassCards TaxiPassCards {
            get {
                if (TaxiPassCardID < 1) {
                    return TaxiPassCardsTable.GetNullEntity();
                }
                if (mTaxiPassCards == null || mTaxiPassCards.IsNullEntity) {
                    using (var conn = SqlHelper.OpenSqlConnection()) {
                        mTaxiPassCards = CabRideEngineDapper.TaxiPassCards.GetCard(conn, this.TaxiPassCardID);
                    }
                }
                return mTaxiPassCards;
            }
        }
    }
}
