using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

namespace CabRideEngine {
	/// <summary>
	/// The developer class representing the MasterVoucherAudit business object class.
	/// </summary>
	/// <remarks>
	/// <para>Place all application-specific business logic in this class.</para>
	/// <para>
	/// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
	/// and will not overwrite changes.
	/// </para>
	/// <para>
	/// This class inherits from the generated class, its companion in the pair of classes 
	/// defining a business object. Properties in the generated class may be overridden in 
	/// order to change the behavior or add business logic.
	/// </para>
	/// <para>
	/// This class is the final class in the inheritance chain for this business object. 
	/// It should not be inherited.
	/// </para>
	/// </remarks>
	[Serializable]
	public sealed partial class MasterVoucherAudit : MasterVoucherAuditDataRow {

		#region Constructors -- DO NOT MODIFY

		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new';
		// it is also required if the class is visible from a web service.
		// DO NOT REMOVE **
		private MasterVoucherAudit() : this(null) { }

		/// <summary>
		/// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
		/// </summary>
		/// <param name="pRowBuilder"></param>
		public MasterVoucherAudit(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static MasterVoucherAudit Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      MasterVoucherAudit aMasterVoucherAudit = pManager.CreateEntity<MasterVoucherAudit>();
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aMasterVoucherAudit, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aMasterVoucherAudit.AddToManager();
		//      return aMasterVoucherAudit;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		// Add additional logic to your business object here...
		public static MasterVoucherAudit Create(MasterVoucher pMasterVoucher) {
			PersistenceManager oPM = pMasterVoucher.PersistenceManager;
			MasterVoucherAudit oAudit = oPM.CreateEntity<MasterVoucherAudit>();

			for (int i = 0; i < pMasterVoucher.Table.Columns.Count; i++) {
				if (oAudit.Table.Columns.Contains(pMasterVoucher.Table.Columns[i].ColumnName)) {
					oAudit[pMasterVoucher.Table.Columns[i].ColumnName] = pMasterVoucher[i];
				}
			}
			oAudit.AddToManager();

			return oAudit;
		}

	}

	#region EntityPropertyDescriptors.MasterVoucherAuditPropertyDescriptor
	////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
	////    For example: 
	////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
	//public static partial class EntityPropertyDescriptors {
	//	public partial class MasterVoucherAuditPropertyDescriptor : AdaptedPropertyDescriptor {
	//    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
	//    public AdaptedPropertyDescriptor Age {
	//      get { return this.Get("Age"); }
	//    }
	//  }
	//} 
	#endregion

}