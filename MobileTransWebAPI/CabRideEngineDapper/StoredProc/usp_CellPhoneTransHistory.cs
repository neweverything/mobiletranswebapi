﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

using Dapper;
using TaxiPassCommon;
using Newtonsoft.Json;

namespace CabRideEngineDapper {
	public partial class usp_CellPhoneTransHistory {
		public long DriverPaymentID { get; set; }
		public string AffiliateName { get; set; }
		public string MyDriverName { get; set; }
		public string MyVehicle { get; set; }
		public string CellNumber { get; set; }
		public string CorporateName { get; set; }
		public DateTime? MyAuthDateTime { get; set; }
		public decimal AuthAmount { get; set; }
		public DateTime MyChargeDateTime { get; set; }
		public string TransNo { get; set; }
		public string ReferenceNo { get; set; }
		public decimal Fare { get; set; }
		public decimal AirportFee { get; set; }
		public decimal MiscFee { get; set; }
		public decimal DropFee { get; set; }
		public decimal BookingFee { get; set; }
		public decimal WaitTime { get; set; }
		public decimal Stops { get; set; }
		public decimal Parking { get; set; }
		public decimal Tolls { get; set; }
		public decimal Gratuity { get; set; }
		public decimal Discount { get; set; }
		public decimal RideCouponAmount { get; set; }
		public decimal TaxiPassFee { get; set; }
		public decimal TotalCharge { get; set; }
		public decimal DriverTotal { get; set; }
		public decimal DriverTotalPaid { get; set; }
		public decimal DriverFee { get; set; }
		public decimal RedemptionFee { get; set; }
		public bool Failed { get; set; }
		public bool Test { get; set; }
		public string CardSwiped { get; set; }
		public string CardType { get; set; }
		public string CardNumberDisplay { get; set; }
		public string CardNumber { get; set; }
		public string Salt { get; set; }
		public string Expiration { get; set; }
		public DateTime? ImageDate { get; set; }
		public DateTime? MyACHPaidDateTime { get; set; }
		public string ACHPaidTo { get; set; }
		public string ACHErrorReason { get; set; }
		public string Reason { get; set; }
		public bool FraudFailure { get; set; }
		public string CardZIPCode { get; set; }
		public bool IgnoreCardZIP { get; set; }
		public string CreditCardProcessor { get; set; }
		public string Platform { get; set; }
		public string DriverPaymentValidation_LicenseNo { get; set; }
		public string DriverPaymentValidation_Name { get; set; }
		public string DriverPaymentValidation_Street { get; set; }
		public string DriverPaymentValidation_City { get; set; }
		public string DriverPaymentValidation_State { get; set; }
		public string CompanyName { get; set; }
		public string RedeemerName { get; set; }
		public string RedeemerEmail { get; set; }
		public string PendingRedeemerName { get; set; }
		public string PendingRedeemerEmail { get; set; }
		public DateTime? PendingMatchDate { get; set; }
		public decimal? KioskAmount { get; set; }


		[Editable(false)]
		[JsonIgnore]
		public string CellNumberFormatted {
			get {
				return CellNumber.FormattedPhoneNumber();
			}
		}

		public string DecryptCardNumber() {
			if (CardNumber.IsNullOrEmpty()) {
				return "";
			}
			Encryption oEncrypt = new Encryption();
			return oEncrypt.TripleDESDecrypt(CardNumber, Salt);
		}

		[Editable(false)]
		[JsonIgnore]
		public string ExpirationDecrypted {
			get {
				if (Expiration.IsNullOrEmpty()) {
					return Expiration;
				}
				Encryption oEncrypt = new Encryption();
				return oEncrypt.TripleDESDecrypt(Expiration, this.Salt);
			}
		}

		[Editable(false)]
		[JsonIgnore]
		public string ReferenceNoDisplay {
			get {
				if (CreditCardProcessor.IsNullOrEmpty()) {
					return ReferenceNo;
				}
				if (this.CreditCardProcessor.Equals("DriverProm", StringComparison.CurrentCultureIgnoreCase)) {
					//"Promo: " + oPromo.DriverPromotionID.ToString() + " | " + this.AffiliateDriverID.Value.ToString("F0");
					if (ReferenceNo.StartsWith("Promo: ")) {
						string temp = ReferenceNo.Replace("Promo: ", "");
						string[] values = temp.Split('|');
						if (values[0].Trim().IsNumeric()) {
							long promoID = Convert.ToInt64(values[0].Trim());
							DriverPromotions promo = DriverPromotions.GetRecord(promoID);
							temp = ReferenceNo.Replace(": " + promoID, promo.Title);
							return temp;
						}
					}
					return ReferenceNo;
				}
				return ReferenceNo;
			}
		}


		public static List<usp_CellPhoneTransHistory> Execute(SqlConnection pConn, long pAffiliateID, DateTime pStart, DateTime pEnd, bool pIncludeTest, bool pIncludeFailed) {
			DynamicParameters param = new DynamicParameters();
			param.Add("AffiliateID", pAffiliateID);
			param.Add("Start", pStart);
			param.Add("End", pEnd);
			param.Add("IncludeTest", pIncludeTest);
			param.Add("IncludeFailed", pIncludeFailed);
			return pConn.Query<usp_CellPhoneTransHistory>("usp_CellPhoneTransHistory", param, commandType: CommandType.StoredProcedure).ToList();
		}

		public static usp_CellPhoneTransHistory GetPayment(SqlConnection pConn, string pTransNo) {
			string sql = mSql + " WHERE TransNo = @TransNo";
			return pConn.Query<usp_CellPhoneTransHistory>(sql, new { TransNo = pTransNo }).FirstOrDefault();
		}

		private static string mSql = @"	SELECT DriverPayments.DriverPaymentID
											, Affiliate.NAME AffiliateName
											, CASE 
												WHEN AffiliateDrivers.NAME IS NULL
													THEN Drivers.NAME
												ELSE AffiliateDrivers.NAME
												END MyDriverName
											, Vehicles.VehicleNo MyVehicle
											, CASE WHEN ISNUMERIC(AffiliateDrivers.Cell) = 1 THEN AffiliateDrivers.Cell ELSE Drivers.Cell END CellNumber
											, CorporateName
											, AuthDate MyAuthDateTime
											, AuthAmount
											, ChargeDate MyChargeDateTime
											, TransNo
											, ISNULL(ReferenceNo, '') ReferenceNo
											, Fare
											, AirportFee
											, MiscFee
											, ISNULL(DropFee, 0) DropFee
											, ISNULL(BookingFee, 0) BookingFee
											, ISNULL(Tolls, 0) Tolls		
											, WaitTime
											, Stops
											, Parking
											, Gratuity
											, Discount
											, ISNULL(CAST(RideCouponAmount.ValueString AS SMALLMONEY), 0) RideCouponAmount
											, TaxiPassFee
											, Fare + AirportFee + MiscFee + ISNULL(Tolls, 0) + ISNULL(DropFee, 0) + ISNULL(BookingFee, 0) + WaitTime + Stops + Parking + Gratuity - Discount + TaxiPassFee TotalCharge
											, Fare + AirportFee + MiscFee + ISNULL(Tolls, 0) + ISNULL(DropFee, 0) + ISNULL(BookingFee, 0) + WaitTime + Stops + Parking + Gratuity - Discount DriverTotal
											, Fare + AirportFee + MiscFee + ISNULL(Tolls, 0) + ISNULL(DropFee, 0) + ISNULL(BookingFee, 0) + WaitTime + Stops + Parking + Gratuity - Discount - ISNULL(RedemptionFee, 0) - ISNULL(AffiliateDrivers.DriverFee, 0) DriverTotalPaid
											, DriverPayments.DriverFee
											, RedemptionFee
											, Failed
											, DriverPayments.Test
											, CardSwiped
											, CardType
											, CardNumberDisplay
											, CardNumber
											, DriverPayments.Salt
											, Expiration
											, DriverPaymentVoucherImages.ModifiedDate ImageDate
											, ACHPaidDate MyACHPaidDateTime
											, ACHPaidTo
											, ACHErrorReason
											, Reason
											, FraudFailure
											, CardZIPCode
											, IgnoreCardZIP
											, CreditCardProcessor
											, Platform
											, DriverPaymentValidation.LicenseNo DriverPaymentValidation_LicenseNo
											, DriverPaymentValidation.Name DriverPaymentValidation_Name
											, DriverPaymentValidation.Street DriverPaymentValidation_Street
											, DriverPaymentValidation.City DriverPaymentValidation_City
											, DriverPaymentValidation.State DriverPaymentValidation_State
											, CompanyName
											, TaxiPassRedeemers.NAME RedeemerName
											, TaxiPassRedeemers.email RedeemerEmail
											, PendingRedeemer.NAME PendingRedeemerName
											, PendingRedeemer.email PendingRedeemerEmail
											, DriverPaymentsPendingMatches.Date PendingMatchDate
											, DriverPaymentKioskAmount.Amount KioskAmount
										FROM DriverPayments WITH (NOLOCK)
										LEFT JOIN Affiliate ON Affiliate.AffiliateID = DriverPayments.AffiliateID
										LEFT JOIN Drivers ON Drivers.DriverID = DriverPayments.DriverID
										LEFT JOIN AffiliateDrivers ON AffiliateDrivers.AffiliateDriverID = DriverPayments.AffiliateDriverID
										LEFT JOIN Vehicles ON Vehicles.VehicleID = DriverPayments.VehicleID
										LEFT JOIN DriverPaymentVoucherImages ON DriverPaymentVoucherImages.DriverPaymentID = DriverPayments.DriverPaymentID
										LEFT JOIN ACHDetail ON ACHDetail.DriverPaymentID = DriverPayments.DriverPaymentID
										LEFT JOIN DriverPaymentNotes ON DriverPaymentNotes.DriverPaymentID = DriverPayments.DriverPaymentID
										LEFT JOIN DriverPaymentValidation ON DriverPaymentValidation.DriverPaymentID = DriverPayments.DriverPaymentID
										LEFT JOIN TaxiPassRedeemers ON TaxiPassRedeemers.TaxiPassRedeemerID = DriverPayments.TaxiPassRedeemerID
										LEFT JOIN TaxiPassRedeemerAccounts ON TaxiPassRedeemerAccounts.TaxiPassRedeemerAccountID = TaxiPassRedeemers.TaxiPassRedeemerAccountID
										LEFT JOIN DriverPaymentsPendingMatches ON DriverPaymentsPendingMatches.DriverPaymentID = DriverPayments.DriverPaymentID
										LEFT JOIN TaxiPassRedeemers PendingRedeemer ON PendingRedeemer.TaxiPassRedeemerID = DriverPaymentsPendingMatches.TaxiPassRedeemerID
										LEFT JOIN DriverPaymentKioskAmount ON DriverPaymentKioskAmount.DriverPaymentID = DriverPayments.DriverPaymentID
										OUTER APPLY (
											SELECT DriverPaymentID
												, ValueString
											FROM DriverPaymentRef
											WHERE DriverPaymentID = DriverPayments.DriverPaymentID
												AND ReferenceKey = 'Coupon Amount'
											) RideCouponAmount
										";

	}
}
