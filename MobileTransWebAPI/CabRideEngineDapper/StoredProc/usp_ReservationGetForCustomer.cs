﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace CabRideEngineDapper {
    public partial class usp_ReservationGetForCustomer {
        public long ReservationID { get; set; }
        public string ReserveNo { get; set; }
        public long CustomerID { get; set; }
        public long? DriverID { get; set; }
        public string Name { get; set; }
        public string CellPhone { get; set; }
        public string WorkPhone { get; set; }
        public string EMail { get; set; }
        public DateTime PickUpDate { get; set; }
        public DateTime UTCPUDateTime { get; set; }
        public DateTime? DropOffTime { get; set; }
        public string Status { get; set; }
        public bool Cancelled { get; set; }
        public string CardType { get; set; }
        public string CardNumber { get; set; }
        public decimal ExtraTip { get; set; }
        public decimal TotalCharge { get; set; }
        public string Notes { get; set; }
        public string ExternalReferenceID { get; set; }
        public string DriverName { get; set; }
        public string Fleet { get; set; }
        public string DriverCell { get; set; }
        public string VehicleNo { get; set; }
        public string VehicleColor { get; set; }
        public string DriverPic { get; set; }
        public DateTime? PicModDate { get; set; }
        public double? DriverLatitude { get; set; }
        public double? DriverLongitude { get; set; }
        public string DriverAddress { get; set; }
        public DateTime? DriverGpsTime { get; set; }

        public string PickupType { get; set; }
        public string PickupPlaceName { get; set; }
        public string PickupStreet { get; set; }
        public string PickupSuite { get; set; }
        public string PickupCity { get; set; }
        public string PickupState { get; set; }
        public string PickupCountry { get; set; }
        public string PickupRoutingType { get; set; }
        public string PickupAirport { get; set; }
        public string PickupAirline { get; set; }
        public string PickupFlightNo { get; set; }
        public string PickupTerminal { get; set; }
        public DateTime? PickupFlightTime { get; set; }
        public double PickupLatitude { get; set; }
        public double PickupLongitude { get; set; }

        public string DropOffType { get; set; }
        public string DropOffPlaceName { get; set; }
        public string DropOffStreet { get; set; }
        public string DropOffSuite { get; set; }
        public string DropOffCity { get; set; }
        public string DropOffState { get; set; }
        public string DropOffCountry { get; set; }
        public string DropOffRoutingType { get; set; }
        public string DropOffAirport { get; set; }
        public string DropOffAirline { get; set; }
        public string DropOffFlightNo { get; set; }
        public string DropOffTerminal { get; set; }
        public DateTime? DropOffFlightTime { get; set; }
        public double? DropOffLatitude { get; set; }
        public double? DropOffLongitude { get; set; }

        public int PushAlertCode { get; set; }
        public string PushAlert { get; set; }
        public string ProfileThumbURL { get; set; }
        public DateTime? ThumbModDate { get; set; }
        public decimal LikePercent { get; set; }
        public decimal Rating { get; set; }
        public bool Favorite { get; set; }

        public decimal Fare { get; set; }
        public decimal Gratuity { get; set; }
        public decimal DriverFee { get; set; }
        public decimal TaxiPassFee { get; set; }
        public decimal Discount { get; set; }
        public decimal AirportFee { get; set; }
        public decimal BookingFee { get; set; }
        public decimal DropFee { get; set; }
        public decimal WaitTime { get; set; }
        public decimal Stops { get; set; }
        public decimal Parking { get; set; }
        public decimal Tolls { get; set; }
        public decimal MiscFee { get; set; }

        public bool HasGetRide { get; set; }


        public static List<usp_ReservationGetForCustomer> Execute(SqlConnection pConn, DynamicParameters param) {
            return pConn.Query<usp_ReservationGetForCustomer>("usp_ReservationGetForCustomer", param, commandType: CommandType.StoredProcedure).ToList();
        }

        public static List<usp_ReservationGetForCustomer> ExecuteNoToken(SqlConnection pConn, string pReserveNo) {
            string sql = @"DECLARE @CustID BIGINT = (SELECT CustomerID FROM dbo.Reservations WHERE ReserveNo = @ReserveNo)
                            DECLARE @MyToken VARCHAR(10) = (SELECT Token FROM dbo.Tokens WHERE TableName = 'Customer' AND ID = @CustID)

                            EXEC usp_ReservationGetForCustomer @PassengerToken = @MyToken, @ReservationNo = @ReserveNo";

            return pConn.Query<usp_ReservationGetForCustomer>(sql, new { ReserveNo = pReserveNo }).ToList();
        }

    }
}
