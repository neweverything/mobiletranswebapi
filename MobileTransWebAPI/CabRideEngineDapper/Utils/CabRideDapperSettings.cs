﻿using System;

using TaxiPassCommon;


namespace CabRideEngineDapper {
	public static class CabRideDapperSettings {

		//public static Context DB;
		private static string mDataSource = "";
		public static string DataSource {
			get {
				return mDataSource;
			}
			set {
				mDataSource = value;
				SqlConnectionString = AppHelper.Initializer.GetConnectionString(mDataSource);
			}
		}

		private static string mAuditDataSource = "audit";
		public static string AuditDataSource {
			get {
				return mAuditDataSource;
			}
			set {
				mAuditDataSource = value;
				SqlAuditConnectionString = AppHelper.Initializer.GetConnectionString(mAuditDataSource);
			}
		}


		private static string mSqlConnectionString;
		public static string SqlConnectionString {
			get {
				return mSqlConnectionString;
			}
			set {
				mSqlConnectionString = value.Replace("Provider=SQLOLEDB.1;", "").Replace("Encrypt=True", "");
			}
		}

		private static string mSqlAuditConnectionString;
		public static string SqlAuditConnectionString {
			get {
				return mSqlAuditConnectionString;
			}
			set {
				mSqlAuditConnectionString = value.Replace("Provider=SQLOLEDB.1;", "").Replace("Encrypt=True", "");
			}
		}

		// ***************************************************************
		// Return the name of the current application
		// ***************************************************************
		public static string GetAppName() {
			string sAppName = "";

			if (System.Web.HttpContext.Current != null) {
				try {
					sAppName = System.Web.HttpContext.Current.Request.Url.AbsoluteUri.Split('?')[0];
				} catch (Exception ex) {
					Console.WriteLine(ex.Message);
					try {
						sAppName = System.Web.HttpContext.Current.Request.Url.AbsolutePath;
					} catch (Exception) {

					}
				}
				sAppName = sAppName.Replace("http://", "");

			} else {
				try {
					sAppName = System.AppDomain.CurrentDomain.FriendlyName.Replace("vshost.", "");
				} catch (Exception ex) {
					Console.WriteLine(ex.Message);
				}
			}

			if (sAppName.Length > 128) {
				sAppName = sAppName.Right(128);
			}

			return sAppName;
		}


		private static string mLoggedInUser = "";
		public static string LoggedInUser {
			get {
				return mLoggedInUser;
			}
			set {
				mLoggedInUser = value;
			}
		}
	}
}
