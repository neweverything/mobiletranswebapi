﻿namespace TaxiPassCommon.Banking.Models.GlobalCashCard {
    public class LoadCardResults {
        public string RESPONSECODE { get; set; }
        public string STATUS { get; set; }
        public string DESCRIPTION { get; set; }
        public decimal FromCardBalance { get; set; }

        public LoadResults RESULTS { get; set; }

        public class LoadResults {
            public string AUDITNO { get; set; }
            public string CARDNUMBER { get; set; }
            public string CUSTID { get; set; }
            public string DESCRIPTION { get; set; }
            public string KEYFIELD { get; set; }
            public string NET { get; set; }
            public string PAYROLLID { get; set; }
            public string TRANSID { get; set; }
        }
    }
}
