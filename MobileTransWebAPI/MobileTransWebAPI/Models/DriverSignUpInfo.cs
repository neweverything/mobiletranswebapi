﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileTransWebAPI.Models {
	public class DriverSignUpInfo {
		public String NewDriverCellNo { get; set; }
		public long ReferringAffiliateDriverID { get; set; }
		public String ReferringAffiliateDriverCell { get; set; }
		public String CabNo { get; set; }
		public Boolean ATT { get; set; }
		public Boolean iPhone { get; set; }
		public String RequestKey { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string Email { get; set; }
	}
}