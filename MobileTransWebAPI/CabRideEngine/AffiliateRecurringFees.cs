﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Collections;

using IdeaBlade.Persistence;
using IdeaBlade.Util;
using IdeaBlade.Persistence.Rdb;

using CabRideEngine.Utils;
using TaxiPassCommon;
using TaxiPassCommon.Banking;

using Csla.Validation;

namespace CabRideEngine {
    /// <summary>
    /// The developer class representing the AffiliateRecurringFees business object class.
    /// </summary>
    /// <remarks>
    /// <para>Place all application-specific business logic in this class.</para>
    /// <para>
    /// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
    /// and will not overwrite changes.
    /// </para>
    /// <para>
    /// This class inherits from the generated class, its companion in the pair of classes 
    /// defining a business object. Properties in the generated class may be overridden in 
    /// order to change the behavior or add business logic.
    /// </para>
    /// <para>
    /// This class is the final class in the inheritance chain for this business object. 
    /// It should not be inherited.
    /// </para>
    /// </remarks>
    [Serializable]
    public sealed partial class AffiliateRecurringFees : AffiliateRecurringFeesDataRow {

        #region Constructors -- DO NOT MODIFY

        // Do not create constructors for this class
        // Developers cannot create instances of this class with the "new" keyword
        // You should write a static Create method instead; see the "Suggested Customization" region
        // Please review the documentation to learn about entity creation and inheritance

        // This private constructor prevents accidental instance creation via 'new';
        // it is also required if the class is visible from a web service.
        // DO NOT REMOVE **
        private AffiliateRecurringFees() : this(null) { }

        /// <summary>
        /// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
        /// </summary>
        /// <param name="pRowBuilder"></param>
        public AffiliateRecurringFees(DataRowBuilder pRowBuilder)
            : base(pRowBuilder) {
        }

        #endregion

        #region Suggested Customizations

        //    // Use this factory method template to create new instances of this class
        //    public static AffiliateRecurringFees Create(PersistenceManager pManager,
        //      ... your creation parameters here ...) { 
        //
        //      AffiliateRecurringFees aAffiliateRecurringFees = pManager.CreateEntity<AffiliateRecurringFees>();
        //
        //      // if this object type requires a unique id and you have implemented
        //      // the IIdGenerator interface implement the following line
        //      pManager.GenerateId(aAffiliateRecurringFees, // add id column here //);
        //
        //      // Add custom code here
        //
        //      aAffiliateRecurringFees.AddToManager();
        //      return aAffiliateRecurringFees;
        //    }

        //    // Implement this method if you want your object to sort in a specific order
        //    public override int CompareTo(Object pObject) {
        //    }

        //    // Implement this method to customize the null object version of this class
        //    protected override void UpdateNullEntity() {
        //    }

        #endregion

        // Add additional logic to your business object here...
        // Add additional logic to your business object here...
        #region Standard Entity logic to create/audit/validate the entity

        // Add additional logic to your business object here...
        public static AffiliateRecurringFees Create(Affiliate pAffiliate) {
            PersistenceManager oPM = pAffiliate.PersistenceManager;

            AffiliateRecurringFees rec = null;

            try {
                rec = oPM.CreateEntity<AffiliateRecurringFees>();

                // Using the IdeaBlade Id Generation technique
                //oPM.GenerateId(rec, AffiliateRecurringFees.AffiliateRecurringFeeIDEntityColumn);
                rec.AffiliateID = pAffiliate.AffiliateID;
                TimeZoneConverter convert = new TimeZoneConverter();
                rec.StartDate = convert.ConvertTime(DateTime.Now, TimeZone.CurrentTimeZone.StandardName, pAffiliate.TimeZone).Date;
                rec.AddToManager();

            } catch (Exception ex) {
                throw ex;
            }
            return rec;
        }

        protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
            if (IsDeserializing) {
                return;
            }
            base.OnColumnChanging(pArgs);
            //DoAudit(pArgs);
        }

        private void DoAudit(DataColumnChangeEventArgs pArgs) {
            if (!this.PersistenceManager.IsClient) {
                return;
            }
            if (this.RowState == DataRowState.Added) {
                return;
            }
            if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
                AuditRecord = AffiliateRecurringFeesAudit.Create(this.PersistenceManager, this);
            }
        }

        protected override void AddRules(RuleList pList) {
            pList.Add(IsExpenseTypeUnique, ExpenseTypeIDEntityColumn.ColumnName);
        }

        private bool IsExpenseTypeUnique(object pTarget, RuleArgs e) {
            AffiliateRecurringFees rec = pTarget as AffiliateRecurringFees;

            RdbQuery qry = new RdbQuery(typeof(AffiliateRecurringFees), AffiliateRecurringFees.ExpenseTypeIDEntityColumn, EntityQueryOp.EQ, rec.ExpenseTypeID);
            qry.AddClause(AffiliateRecurringFees.AffiliateIDEntityColumn, EntityQueryOp.EQ, rec.AffiliateID);
            qry.AddClause(AffiliateRecurringFees.AffiliateRecurringFeeIDEntityColumn, EntityQueryOp.NE, rec.AffiliateRecurringFeeID);
            EntityList<AffiliateRecurringFees> oList = this.PersistenceManager.GetEntities<AffiliateRecurringFees>(qry);
            if (oList.Count > 0) {
                e.Description = "A recurring fee already exists for " + rec.ExpenseTypes.Description;
                return false;
            }

            return true;
        }
        #endregion

        public static EntityList<AffiliateRecurringFees> GetActiveFees(PersistenceManager pPM) {
            RdbQuery qry = new RdbQuery(typeof(AffiliateRecurringFees), AffiliateRecurringFees.CancelledEntityColumn, EntityQueryOp.EQ, false);
            qry.AddClause(AffiliateRecurringFees.StartDateEntityColumn, EntityQueryOp.LE, DateTime.Now);
            qry.AddOrderBy(AffiliateRecurringFees.AffiliateIDEntityColumn);
            return pPM.GetEntities<AffiliateRecurringFees>(qry, QueryStrategy.DataSourceThenCache);
        }

        public DateTime? GetLastRecurringFeeDate() {
            RdbQuery qry = new RdbQuery(typeof(DriverPayments), DriverPayments.ChargeByEntityColumn, EntityQueryOp.EQ, DriverPayments.RECURRING_FEES);
            qry.AddClause(DriverPayments.ReferenceNoEntityColumn, EntityQueryOp.EQ, this.AffiliateRecurringFeeID.ToString("F0"));
            qry.AddClause(DriverPayments.AffiliateIDEntityColumn, EntityQueryOp.EQ, this.AffiliateID);
            qry.AddOrderBy(DriverPayments.ChargeDateEntityColumn, System.ComponentModel.ListSortDirection.Descending);

            qry.Top = 1;
            return this.PersistenceManager.GetEntity<DriverPayments>(qry, QueryStrategy.DataSourceThenCache).ChargeBackDate;
        }
    }

    #region EntityPropertyDescriptors.AffiliateRecurringFeesPropertyDescriptor
    ////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
    ////    For example: 
    ////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
    //public static partial class EntityPropertyDescriptors {
    //	public partial class AffiliateRecurringFeesPropertyDescriptor : BaseEntityPropertyDescriptor {
    //    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
    //    public AdaptedPropertyDescriptor Age {
    //      get { return this.Get("Age"); }
    //    }
    //  }
    //} 
    #endregion

}