﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileTransWebAPI.Models {
	public class PayCardInfo {
		public long AffiliateDriverID { get; set; }
		public long DriverID { get; set; }

		public decimal TransferAmount { get; set; }
		public String RequestKey { get; set; }
	}
}