﻿namespace TaxiPassCommon.Banking {
    public class ProPayCredentials : IGatewayCredentials {

        public ProPayCredentials() {
            CreditCardProcessor = CardProcessors.ProPay;
        }

        public string ProdURL { get; set; }
        public string TestURL { get; set; }

        public string CertKey { get; set; }
        public string AccountNo { get; set; }
        public string MerchantID { get; set; }

        public string TestCertKey { get; set; }
        public string TestAccountNo { get; set; }
        public string TestMerchantID { get; set; }

        public string GetRideMerchantID { get; set; }

        public int Timeout { get; set; }

        public long VerisignAccountID { get; set; }

        public bool Validate { get; set; }

        public int ProcessingOrder { get; set; }

        public CardProcessors CreditCardProcessor { get; set; }

        public string TerminalType { get; set; }
        public string CardType { get; set; }

        public string BillingDescriptor { get; set; }

        public bool ExcludeBillingDescriptorPrefix { get; set; }

        public string SplitPayAcctNum { get; set; }

        public CardProcessors MyGateway() {
            return CreditCardProcessor;
        }

    }
}
