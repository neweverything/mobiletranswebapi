﻿namespace MobileTransWebAPI.Models {
	public class ServiceFee {
		public long AffiliateID { get; set; }
		public long AffiliateDriverID { get; set; }
		public decimal Total { get; set; }
		public string CardNumber { get; set; }
		public bool eCheck { get; set; }

		public long CashierID {
			get {
				return AffiliateDriverID;
			}
			set {
				AffiliateDriverID = value;
			}
		}
	}
}