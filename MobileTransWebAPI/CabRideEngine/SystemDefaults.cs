﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Linq;

using IdeaBlade.Persistence;
using IdeaBlade.Util;

using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Rdb;

using TaxiPassCommon;
using TaxiPassCommon.Entities;
using System.Collections.Generic;

namespace CabRideEngine {
	[Serializable]
	public sealed class SystemDefaults : SystemDefaultsDataRow {

		private static AdoHelper mAdoHelper = new AdoHelper();
		private const int MaxTrys = 3;

		#region Constructors -- DO NOT MODIFY
		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new'
		private SystemDefaults()
			: this(null) {
		}

		// Typed DataSet constructor (needed by framework) - DO NOT REMOVE
		public SystemDefaults(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static SystemDefaults Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      SystemDefaults aSystemDefaults = pManager.CreateEntity<SystemDefaults>();
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aSystemDefaults, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aSystemDefaults.AddToManager();
		//      return aSystemDefaults;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		// Add additional logic to your business object here...

		public static long GetNextKey(String sTable) {
			string sTableName = "SystemDefaults";

			String sqlSelect = "SELECT " + sTable + " FROM [" + sTableName + "]";
			String sqlUpdate = "UPDATE [" + sTableName + "] SET " + sTable + "={0} WHERE " + sTable + "={1}";

			IDbConnection aConnection = mAdoHelper.CreateDbConnection(true);
			aConnection.Open();
			using (aConnection) {
				IDbCommand aCommand = mAdoHelper.CreateDbCommand(aConnection);

				for (int trys = 0; trys <= MaxTrys; trys++) {

					DataSet oDS = new DataSet();
					DataTable oDT = mAdoHelper.SelectIntoDataSet(oDS, "SystemDefaults", sqlSelect);
					if (oDS.Tables["SystemDefaults"].Rows.Count == 0) {
						DataRow oRow = oDT.NewRow();
						oDT.Rows.Add(oRow);
						sqlUpdate = "INSERT INTO [" + sTableName + "] (" + sTable + ") VALUES ({0})";
					}

					long nextId = (long)oDT.Rows[0][sTable];
					long newNextId = nextId + 1;

					// do the update;
					aCommand.CommandText = String.Format(sqlUpdate, newNextId, nextId);

					// if only one record was affected - we're ok; otherwise try again.
					if (aCommand.ExecuteNonQuery() == 1) {
						return newNextId;
					}
				}
			}
			throw new IdeaBladeException("Unable to generate a new id");

			//return "";

		}

		protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
			if (IsDeserializing) {
				return;
			}
			base.OnColumnChanging(pArgs);
			//DoAudit(pArgs);
		}

		private void DoAudit(DataColumnChangeEventArgs pArgs) {
			if (!this.PersistenceManager.IsClient) {
				return;
			}
			if (this.RowState == DataRowState.Added) {
				return;
			}
			if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
				AuditRecord = SystemDefaultsAudit.Create(this);
			}
		}

		public static SystemDefaults GetDefaults(PersistenceManager pPM) {
			return pPM.GetEntities<SystemDefaults>()[0];
		}

		public override double AndroidAppVersion {
			get {
				return base.AndroidAppVersion;
			}
			set {
				base.AndroidAppVersion = value;
				AndroidUpdated = DateTime.Now;
			}
		}

		public override DateTime? AndroidUpdated {
			get {
				return base.AndroidUpdated;
			}
			set {
				DateTime? newTime = value;
				if (newTime.HasValue) {
					TimeZoneConverter tzConvert = new TimeZoneConverter();
					newTime = tzConvert.ConvertTime(newTime.Value, TimeZone.CurrentTimeZone.StandardName, "Eastern");
				}
				base.AndroidUpdated = newTime;

			}
		}

		public int SmartPhoneUpdateDriverProfileDuration {
			get {
				return SystemDefaultsDict.SmartPhoneUpdateDriverProfileDuration(this.PersistenceManager);
			}
			set {
				SystemDefaultsDict.SmartPhoneUpdateDriverProfileDuration(this.PersistenceManager, value);
			}
		}

		public string BlackListedAreaCodes {
			get {
				return SystemDefaultsDict.BlackListedAreaCodes(this.PersistenceManager);
			}
			set {
				SystemDefaultsDict.BlackListedAreaCodes(this.PersistenceManager, value);
			}
		}


		public List<string> TestCardsAsList {
			get {
				List<string> cardList = this.TreatAsTestCards.Split('|').ToList();
				cardList.Sort();
				return cardList;
			}
		}

		public override string TreatAsTestCards {
			get {
				return base.TreatAsTestCards.DecryptIceKey();
			}
			set {
				base.TreatAsTestCards = value.EncryptIceKey();
			}
		}


		public override SaveResult Save() {
			//return base.Save();

			PersistenceManager oPM = this.PersistenceManager;
			EntityList<Entity> saveList = new EntityList<Entity>();
			saveList.ShouldRemoveDeletedEntities = false;

			saveList.Add(this);
			saveList.AddRange(oPM.GetEntities<SystemDefaultsDict>(DataRowState.Added | DataRowState.Deleted | DataRowState.Modified).ToArray());

			//update modified by and date
			DateTime now = DateTime.Now.ToUniversalTime();

			saveList.UpdateModifedByInfo(LoginManager.PrincipleUser.Identity.Name, now);

			return this.PersistenceManager.SaveChanges(saveList);
		}
	}

}
