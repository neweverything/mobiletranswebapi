using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

namespace CabRideEngine {
	/// <summary>
	/// The developer class representing the POIMasters business object class.
	/// </summary>
	/// <remarks>
	/// <para>Place all application-specific business logic in this class.</para>
	/// <para>
	/// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
	/// and will not overwrite changes.
	/// </para>
	/// <para>
	/// This class inherits from the generated class, its companion in the pair of classes 
	/// defining a business object. Properties in the generated class may be overridden in 
	/// order to change the behavior or add business logic.
	/// </para>
	/// <para>
	/// This class is the final class in the inheritance chain for this business object. 
	/// It should not be inherited.
	/// </para>
	/// </remarks>
	[Serializable]
	public sealed partial class POIMasters : POIMastersDataRow {

		#region Constructors -- DO NOT MODIFY

		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new';
		// it is also required if the class is visible from a web service.
		// DO NOT REMOVE **
		private POIMasters() : this(null) { }

		/// <summary>
		/// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
		/// </summary>
		/// <param name="pRowBuilder"></param>
		public POIMasters(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static POIMasters Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      POIMasters aPOIMasters = pManager.CreateEntity<POIMasters>();
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aPOIMasters, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aPOIMasters.AddToManager();
		//      return aPOIMasters;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		// Add additional logic to your business object here...


		public static POIMasters Create(POI pPOI) {
			POIMasters rec = pPOI.PersistenceManager.CreateEntity<POIMasters>();
			rec.POIName = pPOI.Name;
			rec.AddToManager();
			rec.Save();
			return rec;
		}

		public static POIMasters Create(PersistenceManager pPM, string  pPOIName) {
			POIMasters rec = pPM.CreateEntity<POIMasters>();
			rec.POIName = pPOIName;
			rec.AddToManager();
			rec.Save();
			return rec;
		}


		public static POIMasters GetMaster(PersistenceManager pPM, string pName) {
			RdbQuery qry = new RdbQuery(typeof(POIMasters), POIMasters.POINameEntityColumn, EntityQueryOp.EQ, pName);
			return pPM.GetEntity<POIMasters>(qry);
		}

		public static POIMasters GetOrCreate(PersistenceManager pPM, string pPOIName) {
			POIMasters rec = GetMaster(pPM, pPOIName);

			if (rec.IsNullEntity) {
				rec = POIMasters.Create(pPM,pPOIName);
				rec.Save();
			}

			return rec;
		}

	}

	#region EntityPropertyDescriptors.POIMastersPropertyDescriptor
	////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
	////    For example: 
	////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
	//public static partial class EntityPropertyDescriptors {
	//	public partial class POIMastersPropertyDescriptor : BaseEntityPropertyDescriptor {
	//    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
	//    public AdaptedPropertyDescriptor Age {
	//      get { return this.Get("Age"); }
	//    }
	//  }
	//} 
	#endregion

}