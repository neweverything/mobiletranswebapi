﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using TaxiPassCommon;
using TaxiPassCommon.Banking;

namespace CabRideEngine.Utils {
	public class KioskCharge {

		public string CardName = "";
		public string CardNo = "";
		public string CVV = "";
		public string AuthAmount = "";
		public string ServiceCode = "";
		public bool ManualEntry = false;
		public bool ByPassFraudCheck = false;
		private string mSwipeInfo = "";
		private string mCardType = "";
		public double Fare = 0;
		public double Gratuity = 0;
		public double AirportFee = 0;
		public double MiscFee = 0;
		public double FeeAmount = 0;
		public bool PreAuthCard = false;
		public bool ChargeAuth = false;
		public bool CashReceipt = false;
		public bool CardApproved = false;
		public bool DupeReceipt = false;
		public string CardDeclinedReason = "";
		public string PreApprovalCode = "";
		public string ApprovalCode = "";
		public string TransID = "";
		public string VoucherNo = "";
		public double TaxiPassFeeRate = 0;
		public double TaxiPassFeePerAmount = 0;
		private int mExpiryYear = 0;
		private int mExpiryMonth = 0;
		public string ChargeDate = "";
		public string AuthDate = "";
		public string VehcleNo = "";
		public string ZipCode = "";
		public string Latitude = "0";
		public string Longitude = "0";
		public bool TestCard = false;

		public KioskCharge() {
		}

		public String getSwipedCard() {
			return mSwipeInfo;
		}

		public bool setSwipedCard(string value) {
			try {
				value = value.Trim();

				if (value.Equals("exit", StringComparison.CurrentCultureIgnoreCase)) {
					return true;
				}

				if (value.StartsWith("%B") && value.Contains("^")) {
					if (value.IndexOf(CardUtils.TEST_CARD_WHITE) > 0 || value.IndexOf(CardUtils.TEST_CARD_NEIL) > 0) {
						value = CardUtils.USE_TEST_CARD;
					}
				}

				if (value.StartsWith(";%" + CardUtils.TEST_CARD_WHITE)) {
					value = CardUtils.USE_TEST_CARD;
					return setSwipedCard(value);

				} else if ((value.StartsWith(";%") || value.StartsWith("%B")) && value.EndsWith("?") && value.IndexOf("=") > 0) {
					String workData = value;
					int startPos = value.IndexOf("?") + 1;
					int endPos = value.IndexOf("=");
					CardNo = value.Substring(startPos, endPos - startPos);
					if (CardNo.StartsWith(";")) {
						CardNo = CardNo.Substring(1);
					}
					String dateData = workData.Substring(endPos + 1);
					setExpiryYear(Int32.Parse(dateData.Substring(0, 2)));
					setExpiryMonth(Int32.Parse(dateData.Substring(2, 2)));

					startPos = value.IndexOf("^") + 1;
					if (startPos >= 0) {
						workData = value.Substring(startPos);
						endPos = workData.IndexOf("^");
						if (endPos > 0) {
							CardName = workData.Substring(0, endPos).Trim();
						}
					}

					setTrack1(value.Substring(0, value.IndexOf(";")));

				} else if ((value.StartsWith(";") || value.StartsWith("';")) && value.EndsWith("?") && value.IndexOf("=") > 0) {
					if (value.IndexOf(CardUtils.TEST_CARD_WHITE) > 0) {
						value = CardUtils.USE_TEST_CARD;
					}
					mSwipeInfo = value;
					String sText = value.Substring(1);
					sText = sText.Substring(0, sText.Length - 1);
					CardNo = sText.Split('=')[0];
					sText = sText.Split('=')[1];
					mExpiryYear = Int32.Parse(sText.Substring(0, 2));
					sText = sText.Substring(2);
					mExpiryMonth = Int32.Parse(sText.Substring(0, 2));
					sText = sText.Substring(2);
					ServiceCode = sText.Substring(0, 3);
					sText = sText.Substring(3);
				} else if (value.StartsWith(";0") && value.EndsWith("?")) {
					if (value.IndexOf(CardUtils.TEST_CARD_WHITE) > 0) {
						value = CardUtils.USE_TEST_CARD;
					}
					mSwipeInfo = value;
					String sText = value.Substring(1);
					sText = sText.Substring(1, sText.Length - 1);
					CardNo = sText;
					mExpiryYear = 9999;
					mExpiryMonth = 12;
					ServiceCode = "101";
				} else if (value.StartsWith("%B") && value.Contains("^")) {
					mSwipeInfo = value;
					string[] data = mSwipeInfo.Substring(2).Split('^');
					CardNo = data[0];
					mExpiryYear = Int32.Parse(data[2].Substring(0, 2));
					mExpiryMonth = Int32.Parse(data[2].Substring(2, 2));
				} else {
					mErrorMsg = "Card Swipe Read Error";
					return false;
				}

			} catch (Exception ex) {
				throw new Exception("Error Validating card, Please try again " + ex.Message);
			}
			return true;
		}

		public void setTrack1(String pTrack1) {
			mSwipeInfo = pTrack1;
			ManualEntry = false;
		}

		public String getExpiration() {
			String month = mExpiryMonth.ToString();
			String year = mExpiryYear.ToString();
			return month.PadLeft(2, '0') + year.PadLeft(2, '0');
		}

		public int getExpiryMonth() {
			return mExpiryMonth;
		}

		public void setExpiryMonth(int value) {
			mExpiryMonth = value;
		}

		public int getExpiryYear() {

			return mExpiryYear;
		}

		public void setExpiryYear(int value) {
			if (value < 1000) {
				value += 2000;
			}
			mExpiryYear = value;
		}

		public double getTotal() {
			return Fare + FeeAmount + Gratuity + AirportFee + MiscFee;
		}

		public double getDriverTotal() {
			return getTotal() - FeeAmount;
		}

		public String getCardType() {
			return mCardType;
		}

		public void setCardType(String value) {
			mCardType = value;
		}

		public bool IsValidCard() {

			bool bValid = true;
			try {
				mErrorMsg = "";

				if (ServiceCode.Length == 0) {
					return true;
				}

				String sChar = ServiceCode.Substring(ServiceCode.Length - 1);
				int iChar = Int32.Parse(sChar);

				if (((mCardType.Length > 0) && mCardType.ToUpper() == "AMEX") || (mCardType.Length == 0 && CardNo.StartsWith("3"))) {
					return bValid;
				}


				switch (iChar) {
					case 0:
						bValid = false;
						mErrorMsg = "PIN required";
						break;
					case 3:
						bValid = false;
						mErrorMsg = "ATM only and PIN required";
						break;
					case 4:
						bValid = false;
						mErrorMsg = "Cash only.";
						break;
					case 5:
						bValid = false;
						mErrorMsg = "Goods and services only (no cash) and PIN required.";
						break;
				}
			} catch (Exception ex) {
				//.SendErrorMsg("IsValidCard", ex.getMessage());
				mErrorMsg = ex.Message;
			}
			return bValid;
		}

		private String mErrorMsg = "";
		public String getErrorMsg() {
			return mErrorMsg;
		}
	}
}

