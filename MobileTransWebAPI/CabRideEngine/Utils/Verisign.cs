﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PayPal.Payments.Common;
using PayPal.Payments.Common.Utility;
using PayPal.Payments.DataObjects;
using PayPal.Payments.Transactions;

using TaxiPassCommon;

namespace CabRideEngine.Utils {
	public class Verisign {

		private UserInfo mUserInfo;
		private VerisignHost mHost;
		private VerisignProxy mProxy;

		private Response mResponse;

		public Verisign(VerisignAccounts pAccount, SystemDefaults pDefault) {

			if (pAccount == null || pAccount.IsNullEntity) {
				pAccount = VerisignAccounts.GetDefaultAccount(pDefault.PersistenceManager);
			}

			mUserInfo = new UserInfo(pAccount.VerisignUserName, pAccount.VersignVendor, pAccount.VerisignPartner, pAccount.VerisignPassword);
			VerisignHost oHost = new VerisignHost();
			oHost.HostAddress = pAccount.VerisignAddress;
			oHost.TimeOut = pAccount.VerisignTimeout.GetValueOrDefault(30);
			oHost.VerisignPort = pAccount.VerisignPort.GetValueOrDefault(443);
			mHost = oHost;

			VerisignProxy oProxy = new VerisignProxy();
			oProxy.ProxyAddress = pDefault.ProxyAddress;
			oProxy.ProxyLogon = pDefault.ProxyLogon;
			oProxy.ProxyPassword = pDefault.ProxyPassword;
			oProxy.ProxyPort = pDefault.ProxyPort.GetValueOrDefault(0).ToString();
			mProxy = oProxy;

		}

		//public void PreAuth(CreditCardData pCreditCardData) {

		//    if (CreditCardUtils.IsTestCard(pCreditCardData.CardNo) && !mPayFlo.HostAddress.ToLower().Contains("test")) {
		//        mPayFlo.HostAddress = "test-" + mPayFlo.HostAddress;
		//    }

		//    mPayFlo.AddParm("TRXTYPE", pCreditCardData.TrxType);
		//    mPayFlo.AddParm("TENDER", VerisignTenderTypes.CreditCard);     //Credit Card
		//    mPayFlo.AddParm("COMMENT1", pCreditCardData.Comment1);
		//    if (string.IsNullOrEmpty(pCreditCardData.Swipe)) {
		//        mPayFlo.AddParm("ACCT", pCreditCardData.CardNo);
		//        mPayFlo.AddParm("EXPDATE", pCreditCardData.ExpDate);
		//        if (!string.IsNullOrEmpty(pCreditCardData.CVV)) {
		//            mPayFlo.AddParm("CVV2", pCreditCardData.CVV);
		//        }
		//    } else {
		//        mPayFlo.AddParm("SWIPE", pCreditCardData.Swipe);
		//    }
		//    mPayFlo.AddParm("AMT", pCreditCardData.Amount);
		//    if (!string.IsNullOrEmpty(pCreditCardData.ZipCode)) {
		//        mPayFlo.AddParm("ZIP", pCreditCardData.ZipCode);
		//    }

		//    try {
		//        mPayFlo.ProcessTransaction();
		//    } catch (Exception ex) {
		//        throw ex;
		//    }

		//}

		private Invoice CreatePaymentInvoice(CreditCardData pCreditCardData) {

			decimal amount = pCreditCardData.Amount;
			Invoice oInvoice = new Invoice();
			oInvoice.TaxAmt = new Currency(0);
			oInvoice.Amt = new Currency(amount);
			oInvoice.TaxExempt = "Y";
			oInvoice.Comment1 = pCreditCardData.Comment1;
			oInvoice.Comment2 = pCreditCardData.VoucherNo;
			oInvoice.OrderDate = DateTime.Today.ToString("MMddyy");
			oInvoice.InvNum = pCreditCardData.DriverIDs;
			return oInvoice;
		}

		public Response PreAuth(CreditCardData pCreditCardData) {
			//mHost.HostAddress = "payflowpro.paypal.com";
			if (CreditCardUtils.IsTestCard(pCreditCardData.CardNo) && !mHost.HostAddress.ToLower().Contains("test")) {
				mHost.HostAddress = "pilot-" + mHost.HostAddress;
			}

			PayflowConnectionData connection = new PayflowConnectionData(mHost.HostAddress, mHost.VerisignPort, mHost.TimeOut);

			CardTender card = null;
			if (pCreditCardData.Swipe.IsNullOrEmpty()) {
				CreditCard CC = new CreditCard(pCreditCardData.CardNo, pCreditCardData.ExpDate);
				CC.Cvv2 = pCreditCardData.CVV;

				// Create a new Tender - Card Tender data object.
				card = new CardTender(CC);
			} else {
				SwipeCard oSwipe = new SwipeCard(pCreditCardData.Swipe);
				card = new CardTender(oSwipe);
			}

			Invoice oInvoice = CreatePaymentInvoice(pCreditCardData);

			// Create a new Auth Transaction.
			AuthorizationTransaction trans = new AuthorizationTransaction(mUserInfo, connection, oInvoice, card, PayflowUtility.RequestId);
			// Submit the Transaction
			mResponse = trans.SubmitTransaction();
			return mResponse;
		}

		//public void Sale(CreditCardData pCreditCardData) {

		//    if (CreditCardUtils.IsTestCard(pCreditCardData.CardNo) && !mPayFlo.HostAddress.ToLower().Contains("test")) {
		//        mPayFlo.HostAddress = "test-" + mPayFlo.HostAddress;
		//    }

		//    mPayFlo.AddParm("TENDER", VerisignTenderTypes.CreditCard);
		//    mPayFlo.AddParm("AMT", pCreditCardData.Amount);
		//    mPayFlo.AddParm("TRXTYPE", pCreditCardData.TrxType);
		//    mPayFlo.AddParm("COMMENT1", pCreditCardData.Comment1);
		//    if (string.IsNullOrEmpty(pCreditCardData.Swipe)) {
		//        mPayFlo.AddParm("ACCT", pCreditCardData.CardNo);
		//        mPayFlo.AddParm("EXPDATE", pCreditCardData.ExpDate);
		//        if (!string.IsNullOrEmpty(pCreditCardData.CVV)) {
		//            mPayFlo.AddParm("CVV2", pCreditCardData.CVV);
		//        }
		//        if (!string.IsNullOrEmpty(pCreditCardData.ZipCode)) {
		//            mPayFlo.AddParm("ZIP", pCreditCardData.ZipCode);
		//        }
		//    } else {
		//        mPayFlo.AddParm("SWIPE", pCreditCardData.Swipe);
		//    }

		//    try {
		//        mPayFlo.ProcessTransaction();
		//    } catch (Exception ex) {
		//        throw ex;
		//    }
		//}

		public Response Sale(CreditCardData pCreditCardData) {
			//mHost.HostAddress = "payflowpro.paypal.com";
			if (CreditCardUtils.IsTestCard(pCreditCardData.CardNo) && !mHost.HostAddress.ToLower().Contains("test")) {
				mHost.HostAddress = "pilot-" + mHost.HostAddress;
			}

			PayflowConnectionData connection = new PayflowConnectionData(mHost.HostAddress, mHost.VerisignPort, mHost.TimeOut);

			CardTender card = null;
			if (pCreditCardData.Swipe.IsNullOrEmpty()) {
				CreditCard CC = new CreditCard(pCreditCardData.CardNo, pCreditCardData.ExpDate);
				CC.Cvv2 = pCreditCardData.CVV;

				// Create a new Tender - Card Tender data object.
				card = new CardTender(CC);
			} else {
				SwipeCard oSwipe = new SwipeCard(pCreditCardData.Swipe);
				card = new CardTender(oSwipe);
			}

			Invoice oInvoice = CreatePaymentInvoice(pCreditCardData);

			// Create a new Sale Transaction.
			SaleTransaction Trans = new SaleTransaction(mUserInfo, connection, oInvoice, card, PayflowUtility.RequestId);

			// Submit the Transaction
			mResponse = Trans.SubmitTransaction();
			return mResponse;
		}

		//public void DelayedSale(CreditCardData pCreditCardData) {

		//    if (CreditCardUtils.IsTestCard(pCreditCardData.CardNo) && !mPayFlo.HostAddress.ToLower().Contains("test")) {
		//        mPayFlo.HostAddress = "test-" + mPayFlo.HostAddress;
		//    }

		//    mPayFlo.AddParm("TENDER", VerisignTenderTypes.CreditCard);
		//    mPayFlo.AddParm("AMT", pCreditCardData.Amount);
		//    mPayFlo.AddParm("TRXTYPE", pCreditCardData.TrxType);
		//    mPayFlo.AddParm("ORIGID", pCreditCardData.ReferenceNo);

		//    try {
		//        mPayFlo.ProcessTransaction();
		//    } catch (Exception ex) {
		//        throw ex;
		//    }
		//}


		public Response DelayedSale(CreditCardData pCreditCardData) {
			//mHost.HostAddress = "payflowpro.paypal.com";
			if (CreditCardUtils.IsTestCard(pCreditCardData.CardNo) && !mHost.HostAddress.ToLower().Contains("test")) {
				mHost.HostAddress = "pilot-" + mHost.HostAddress;
			}

			PayflowConnectionData connection = new PayflowConnectionData(mHost.HostAddress, mHost.VerisignPort, mHost.TimeOut);

			Invoice oInvoice = CreatePaymentInvoice(pCreditCardData);
			mResponse = new CaptureTransaction(pCreditCardData.ReferenceNo, mUserInfo, connection, oInvoice, PayflowUtility.RequestId).SubmitTransaction();
			return mResponse;
		}

		public Response Void(CreditCardData pCreditCardData) {
			//mHost.HostAddress = "payflowpro.paypal.com";
			if (CreditCardUtils.IsTestCard(pCreditCardData.CardNo) && !mHost.HostAddress.ToLower().Contains("test")) {
				mHost.HostAddress = "pilot-" + mHost.HostAddress;
			}
			PayflowConnectionData connection = new PayflowConnectionData(mHost.HostAddress, mHost.VerisignPort, mHost.TimeOut);

			VoidTransaction trans = new VoidTransaction(pCreditCardData.ReferenceNo, mUserInfo, PayflowUtility.RequestId);
			mResponse = trans.SubmitTransaction();
			return mResponse;
		}

		public Response Refund(CreditCardData pCreditCardData) {
			//mHost.HostAddress = "payflowpro.paypal.com";
			if (CreditCardUtils.IsTestCard(pCreditCardData.CardNo) && !mHost.HostAddress.ToLower().Contains("test")) {
				mHost.HostAddress = "pilot-" + mHost.HostAddress;
			}
			PayflowConnectionData connection = new PayflowConnectionData(mHost.HostAddress, mHost.VerisignPort, mHost.TimeOut);

			Invoice inv = CreatePaymentInvoice(pCreditCardData);
			
			PayPal.Payments.Transactions.CreditTransaction trans = new CreditTransaction(pCreditCardData.ReferenceNo, mUserInfo, inv, PayflowUtility.RequestId);
			mResponse = trans.SubmitTransaction();
			return mResponse;
		}

		//public void CleanUp() {
		//    mPayFlo.PNCleanup();
		//}

		//public void Report() {
		//    PayPal.Payments.Communication.PayflowNETAPI api = new PayPal.Payments.Communication.PayflowNETAPI();
		//    PayPal.Payments.Transactions.InquiryTransaction x = new InquiryTransaction("", new UserInfo(), "");
		//    PayPal.Payments.DataObjects.
		//}

		public string Response {
			get {
				return mResponse.ResponseString;
			}
		}


		public class CreditCardData {
			public string DriverIDs { get; set; }

			public string VoucherNo { get; set; }
			public string TrxType { get; set; }
			public string Tender { get; set; }
			public string Comment1 { get; set; }
			public string CardNo { get; set; }
			public string ExpDate { get; set; }
			public string CVV { get; set; }
			public string Swipe { get; set; }
			public decimal Amount { get; set; }
			public string ZipCode { get; set; }
			public string ReferenceNo { get; set; }
		}

	}

	public class VerisignLogin {
		public string User { get; set; }
		public string Vendor { get; set; }
		public string Partner { get; set; }
		public string Password { get; set; }
	}

	public class VerisignProxy {
		public string ProxyAddress { get; set; }
		public string ProxyLogon { get; set; }
		public string ProxyPassword { get; set; }
		public string ProxyPort { get; set; }
	}

	public class VerisignHost {
		public string HostAddress { get; set; }
		public int VerisignPort { get; set; }
		public int TimeOut { get; set; }
	}

}
