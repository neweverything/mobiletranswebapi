﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CabRideEngineDapper {
	public class DriverPricing {
		public decimal BaseFare { get; set; }
		public decimal MinimumFare { get; set; }
		public decimal PerMileCharge { get; set; }
		public decimal PerMinCharge { get; set; }
		public int PerMinMinimumMPH { get; set; }
		public decimal PerMinMinimumMPHRate { get; set; }
		public decimal Extras { get; set; }
		public decimal FlatRate { get; set; }
		public string FlatRates { get; set; }
		//public void Save(Driver pDriver)
	}
}
