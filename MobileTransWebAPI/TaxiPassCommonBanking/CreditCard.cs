﻿using System;

namespace TaxiPassCommon.Banking {
	public class CreditCard {

		public long? AffiliateDriverID { get; set; }
		public long DriverID { get; set; }

		public string ErrorMsg;

		private string mTrack2;
		private string mCardNo;

		public bool TestCard { get; set; }

		public DateTime CardExpiry { get; set; }
		public string CVV { get; set; }
		public string CardHolder { get; set; }
		public string CardType { get; set; }

		public bool CustomerSigned { get; set; }
		public bool CardPresent { get; set; }

		public string CardNo {
			get {
				return mCardNo;
			}
			set {
				mCardNo = value;
				SetTestCard();
			}
		}


		public string AvsStreet { get; set; }
		public string AvsZip { get; set; }
		public string ReferenceNo { get; set; }
		public SoftDescriptor SoftDescriptor { get; set; }


		public string Track2 {
			get {
				return mTrack2;
			}
			set {
				if (("" + CardType).Equals("GooglePay")) {
					mTrack2 = value;
				} else {
					SetSwipedCard(value);
				}
			}
		}

		public string CardExpiryMonth {
			get {
				return CardExpiry.Month.ToString().PadLeft(2, '0');
			}
		}

		public string CardExpiryYear {
			get {
				return CardExpiry.Year.ToString().Right(2);
			}
		}

		public string CardExpiryAsMMYY {
			get {
				return CardExpiryMonth + CardExpiryYear;
			}
			set {
				CardExpiry = Convert.ToDateTime(value.Insert(2, "/01/"));
			}
		}

		private void SetTestCard() {
			TestCard = CardUtils.IsTestCard(CardNo);
		}

		private bool SetSwipedCard(string value) {
			try {
				value = value.Trim();

				if (value.StartsWith("%B") && value.Contains("^")) {
					if (value.IndexOf(CardUtils.TEST_CARD_WHITE) > 0 || value.IndexOf(CardUtils.TEST_CARD_NEIL) > 0) {
						value = CardUtils.USE_TEST_CARD;
					}
				}

				if (value.StartsWith(";%" + CardUtils.TEST_CARD_WHITE)) {
					value = CardUtils.USE_TEST_CARD;
					return SetSwipedCard(value);

				} else if ((value.StartsWith(";%") || value.StartsWith("%B")) && value.EndsWith("?") && value.IndexOf("=") > 0) {
					String workData = value;
					int startPos = value.IndexOf("?") + 1;
					int endPos = value.IndexOf("=");
					CardNo = value.Substring(startPos, endPos - startPos);
					if (CardNo.StartsWith(";")) {
						CardNo = CardNo.Substring(1);
					}
					String dateData = workData.Substring(endPos + 1);
					int expiryYear = Int32.Parse(dateData.Substring(0, 2));
					int expiryMonth = Int32.Parse(dateData.Substring(2, 2));
					CardExpiry = Convert.ToDateTime(string.Format("{0}/1/{1}", expiryMonth, expiryYear));

					startPos = value.IndexOf("^") + 1;
					if (startPos >= 0) {
						workData = value.Substring(startPos);
						endPos = workData.IndexOf("^");
						if (endPos > 0) {
							CardHolder = workData.Substring(0, endPos).Trim();
						}
					}

					mTrack2 = value.Substring(0, value.IndexOf(";"));

				} else if ((value.StartsWith(";") || value.StartsWith("';")) && value.EndsWith("?") && value.IndexOf("=") > 0) {
					if (value.IndexOf(CardUtils.TEST_CARD_WHITE) > 0) {
						value = CardUtils.USE_TEST_CARD;
					}
					mTrack2 = value;
					String sText = value.Substring(1);
					sText = sText.Substring(0, sText.Length - 1);
					CardNo = sText.Split('=')[0];
					sText = sText.Split('=')[1];
					int expiryYear = Int32.Parse(sText.Substring(0, 2));
					sText = sText.Substring(2);
					int expiryMonth = Int32.Parse(sText.Substring(0, 2));
					CardExpiry = Convert.ToDateTime(string.Format("{0}/1/{1}", expiryMonth, expiryYear));
					sText = sText.Substring(2);
					//ServiceCode = sText.Substring(0, 3);
					sText = sText.Substring(3);
				} else if (value.StartsWith(";0") && value.EndsWith("?")) {
					if (value.IndexOf(CardUtils.TEST_CARD_WHITE) > 0) {
						value = CardUtils.USE_TEST_CARD;
					}
					mTrack2 = value;
					String sText = value.Substring(1);
					sText = sText.Substring(1, sText.Length - 1);
					CardNo = sText;
					CardExpiry = new DateTime(9999, 12, 1);
					//ServiceCode = "101";
				} else if (value.StartsWith("%B") && value.Contains("^")) {
					mTrack2 = value;
					string[] data = mTrack2.Substring(2).Split('^');
					CardNo = data[0];
					int expiryYear = Int32.Parse(data[2].Substring(0, 2));
					int expiryMonth = Int32.Parse(data[2].Substring(2, 2));
					CardExpiry = Convert.ToDateTime(string.Format("{0}/1/{1}", expiryMonth, expiryYear));
				} else {
					ErrorMsg = "Card Swipe Read Error";
					return false;
				}

			} catch (Exception ex) {
				throw new Exception("Error Validating card, Please try again " + ex.Message);
			}
			return true;
		}

		public string SalesPerson {
			get {
				string value = string.Format("AD: {0} D:{1}", AffiliateDriverID, DriverID);
				if (!AffiliateDriverID.HasValue && DriverID == 0) {
					value = "";
				}
				return value;
			}
		}
	}
}
