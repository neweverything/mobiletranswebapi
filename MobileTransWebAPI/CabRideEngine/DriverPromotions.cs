﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using System.Data.Linq;
using System.Linq;

using System.Collections.Generic;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

using Csla.Validation;
using System.Data.SqlClient;
using Dapper;

namespace CabRideEngine {
	/// <summary>
	/// The developer class representing the DriverPromotions business object class.
	/// </summary>
	/// <remarks>
	/// <para>Place all application-specific business logic in this class.</para>
	/// <para>
	/// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
	/// and will not overwrite changes.
	/// </para>
	/// <para>
	/// This class inherits from the generated class, its companion in the pair of classes 
	/// defining a business object. Properties in the generated class may be overridden in 
	/// order to change the behavior or add business logic.
	/// </para>
	/// <para>
	/// This class is the final class in the inheritance chain for this business object. 
	/// It should not be inherited.
	/// </para>
	/// </remarks>
	[Serializable]
	public sealed partial class DriverPromotions : DriverPromotionsDataRow {

		#region Constructors -- DO NOT MODIFY

		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new';
		// it is also required if the class is visible from a web service.
		// DO NOT REMOVE **
		private DriverPromotions() : this(null) { }

		/// <summary>
		/// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
		/// </summary>
		/// <param name="pRowBuilder"></param>
		public DriverPromotions(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static DriverPromotions Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      DriverPromotions aDriverPromotions = pManager.CreateEntity<DriverPromotions>();
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aDriverPromotions, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aDriverPromotions.AddToManager();
		//      return aDriverPromotions;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		// Add additional logic to your business object here...
		#region Standard Entity logic to create/audit/validate the entity
		public static DriverPromotions Create(PersistenceManager pPM) {
			DriverPromotions rec = null;

			try {
				rec = pPM.CreateEntity<DriverPromotions>();
				// Using the IdeaBlade Id Generation technique
				//pPM.GenerateId(rec, DriverPromotions.DriverPromotionIDEntityColumn);
				rec.Platform = Platforms.TaxiPay.ToString();
				rec.AddToManager();

			} catch (Exception ex) {
				throw ex;
			}
			return rec;
		}

		protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
			if (IsDeserializing) {
				return;
			}
			base.OnColumnChanging(pArgs);
			//DoAudit(pArgs);
		}

		private void DoAudit(DataColumnChangeEventArgs pArgs) {
			if (!this.PersistenceManager.IsClient) {
				return;
			}
			if (this.RowState == DataRowState.Added) {
				return;
			}
			if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
				AuditRecord = DriverPromotionsAudit.Create(this.PersistenceManager, this);
			}
		}

		[BindingBrowsable(false)]
		public override string EntityInstanceDisplayName {
			get {
				return this.Title;
			}
		}

		protected override void AddRules(RuleList pList) {
			PropertyRequiredRule.AddToList(pList, TitleEntityColumn.ColumnName);
			pList.Add(IsTitleUnique, TitleEntityColumn.ColumnName);
		}

		private bool IsTitleUnique(object pTarget, RuleArgs e) {
			DriverPromotions rec = pTarget as DriverPromotions;
			if (string.IsNullOrEmpty(rec.Title)) {
				return false;
			}
			RdbQuery qry = new RdbQuery(typeof(DriverPromotions), DriverPromotions.TitleEntityColumn, EntityQueryOp.EQ, rec.Title);
			qry.AddClause(DriverPromotions.DriverPromotionIDEntityColumn, EntityQueryOp.NE, rec.DriverPromotionID);
			EntityList<DriverPromotions> oList = this.PersistenceManager.GetEntities<DriverPromotions>(qry);
			if (oList.Count > 0) {
				e.Description = "Title already exists: " + rec.Title;
				return false;
			}
			return true;
		}

		public override string ToString() {
			return EntityInstanceDisplayName;
		}

		#endregion

		#region Retrieve Records

		public static EntityList<DriverPromotions> GetAll(PersistenceManager pPM) {
			EntityList<DriverPromotions> oList = pPM.GetEntities<DriverPromotions>();
			oList.ApplySort(DriverPromotions.TitleEntityColumn.ColumnName, System.ComponentModel.ListSortDirection.Ascending, false);
			return oList;
		}

		public static DriverPromotions GetRecord(PersistenceManager pPM, long pDriverPromotionID) {
			DriverPromotions rec = pPM.GetEntity<DriverPromotions>(new PrimaryKey(typeof(DriverPromotions), pDriverPromotionID));
			return rec;
		}

		public static EntityList<DriverPromotions> GetPromotionsForAffiliate(PersistenceManager pPM, long pAffiliateID) {
			string sDate = "'" + DateTime.Today.ToString("MM/dd/yyyy") + "'";
			string sSQL = "SELECT DriverPromotions.* FROM DriverPromotions WITH (NoLock) WHERE (AllAffiliates = 1 AND EndDate >= " + sDate + " AND StartDate <= " + sDate + ") OR " +
							"(DriverPromotionID IN (SELECT DISTINCT DriverPromotionID FROM DriverPromotionAffiliates " +
								"WHERE AffiliateID = " + pAffiliateID + ") AND EndDate >= " + sDate + " AND StartDate <= " + sDate + ")";

			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(DriverPromotions), sSQL);

			EntityList<DriverPromotions> list = new EntityList<DriverPromotions>();
			try {
				list = pPM.GetEntities<DriverPromotions>(qry);

			} catch (Exception ex) {
				System.Console.WriteLine(ex.Message);
			}


			return list;

		}

		public static EntityList<DriverPromotions> GetPromotionsForAffiliate(Affiliate pAffiliate) {
			return GetPromotionsForAffiliate(pAffiliate.PersistenceManager, pAffiliate.AffiliateID);
		}


		public static EntityList<DriverPromotions> GetPromotionsByCardNo(PersistenceManager pPM, string pCardNo) {
			RdbQuery qry = new RdbQuery(typeof(DriverPromotions), CardNumberEntityColumn, EntityQueryOp.EQ, pCardNo);
			return pPM.GetEntities<DriverPromotions>(qry);
		}


		public static bool IsPromoCard(SqlConnection pConn, string pCardNo) {
			var promoList = pConn.Query(string.Format("Select * FROM DriverPromotions WITH (NoLock) WHERE CardNumber = '{0}'", pCardNo)).ToList();

			return promoList.Count > 0;
		}
		#endregion

		//private EntityList<Affiliate> mAffiliateList;
		//public EntityList<Affiliate> Affiliates {
		//    get {
		//        if (mAffiliateList == null) {
		//            if (string.IsNullOrEmpty(this.AffiliateList)) {
		//                mAffiliateList = new EntityList<Affiliate>();
		//            } else {
		//                string[] affiliateIDs = this.AffiliateList.Split('|');
		//                RdbQuery qry = new RdbQuery(typeof(Affiliate), Affiliate.AffiliateIDEntityColumn, EntityQueryOp.In, affiliateIDs);
		//                mAffiliateList = this.PersistenceManager.GetEntities<Affiliate>(qry);
		//            }
		//        }
		//        return mAffiliateList;
		//    }
		//}
	}

	#region EntityPropertyDescriptors.DriverPromotionsPropertyDescriptor
	////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
	////    For example: 
	////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
	//public static partial class EntityPropertyDescriptors {
	//	public partial class DriverPromotionsPropertyDescriptor : AdaptedPropertyDescriptor {
	//    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
	//    public AdaptedPropertyDescriptor Age {
	//      get { return this.Get("Age"); }
	//    }
	//  }
	//} 
	#endregion

}