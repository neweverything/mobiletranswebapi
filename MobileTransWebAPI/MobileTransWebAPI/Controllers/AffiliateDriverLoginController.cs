﻿using CabRideEngineDapper;
using CabRideEngineDapper.Utils;
using Dapper;
using MobileTransWebAPI.Models;
using Newtonsoft.Json;
using System;
using System.Data;
using System.Linq;
using System.Web.Http;
using TaxiPassCommon;

namespace MobileTransWebAPI.Controllers {
	public class AffiliateDriverLoginController : ApiController {

		public DriverLoginResult Get() {
			string js = "4186B329149808536F07E71CAD0ACF41E938FC0E903A9D4F974DCE61D962BA96E560E832836C3263F4389ACAE597C4C585FA3E891AAFEF5956E7EFFAFB771ABAD716912CA6EBEC4C5D5AF10CDA97CC7A395E53A794B0E669FE6AB13A35728F8BB632AC6897B3B7B29516D2A4237AAAB60E038BB8163FEE7CB8E30FE01E1E44A8FA4D209989452136BD644B87BD18A33F91075E9735632055469C0644F23CE9D83068F30F31DEB2775BA67B9B0787F2FCC77DF7C7BCE92983E4234C51F311D89ECDA47990D08DFCF0465AE04185306AC59BBF570E22446457E5413F4FF53E4A327B6F87A3CB1BE7B451D5D245C51855052271A0E06D1043F234FE76DDBEF5C4B4E8BAFA1869253AF26BA2CA899DC186801C9FBC50D4BD853C04E8C6610BD5902F32CA14C4A37086C86DBB95FC149B66E9E94B2B75074ECE1F127A1E2225DECAFFE4E4330531B91B92B00B1C440396F519C1F2E17ABAE140477F84BE9DEBC465BB05E24CC973F59B0A60E598F46176B7552796AE92A748F6DC689C3A3B9626CC08201D92DCE872DBD5F2B3B84E4EFC81429E1CBE6F43B2A45F6F6E5168869E46D748B02B40";
			return Login(js);
		}


		[HttpGet]
		public DriverLoginResult Login(string pRequest) {
			DriverLoginResult result = new DriverLoginResult();

			string request = pRequest.DecryptIceKey();
			WebApiApplication.LogIt("AffiliateDriverLogin", request);

			AffDriverLoginInfo info = JsonConvert.DeserializeObject<AffDriverLoginInfo>(request);

			DateTime requestDate = Convert.ToDateTime(info.RequestTime);

			if (WebApiApplication.GetUTCServerTime().Subtract(requestDate).TotalMinutes > 1) {
				result.ErrorMsg = "Invalid Access";
				return result;
			}

			using (var conn = SqlHelper.OpenSqlConnection()) {
				Drivers oDriver = Drivers.GetDriverIncludeAffiliate(conn, info.DriverID);
				Affiliate myAffiliate = oDriver.Affiliate;

				AffiliateDrivers myDriver = AffiliateDrivers.GetDriverByCell(conn, myAffiliate.AffiliateID, info.Phone);
				if (myDriver.IsNullEntity || myDriver.PIN != info.PIN) {
					result.ErrorMsg = "Invalid Phone or PIN";
					return result;
				}
				if (!myDriver.Active || myDriver.FraudSuspected) {
					result.ErrorMsg = "Error: Your account is currently disabled.";
					return result;
				}
				result.AffiliateID = myDriver.AffiliateID;
				result.AskForTolls = myDriver.Affiliate.HHShowTolls;
				result.AffiliateDriverID = myDriver.AffiliateDriverID;
				result.ServiceType = myAffiliate.ServiceType;

				result.DriverName = myDriver.Name;
				result.AffiliateName = myAffiliate.Name;
				result.AffiliateCity = myAffiliate.City;
				result.AffiliatePhone = myAffiliate.Phone;
				result.AffiliateState = myAffiliate.State;
				result.AffiliateStreet = myAffiliate.Line1;
				result.AffiliateSuite = myAffiliate.Line2;
				result.DriverID = info.DriverID;
				result.StartingFare = myAffiliate.AffiliateDriverDefaults.StartingFare;
				result.ChargeMaxAmount = myAffiliate.AffiliateDriverDefaults.ChargeMaxAmount;
				result.ChargeMaxSwipeAmount = myAffiliate.AffiliateDriverDefaults.ChargeMaxSwipeAmount;


				result.PaidByRedeemer = myDriver.PaidByRedeemer;
				result.SSN = myDriver.SSN;
				result.AskForTip = myAffiliate.AffiliateDriverDefaults.TaxiPayGratuity;

				if (string.IsNullOrEmpty(myDriver.CallInCell)) {
					result.TaxiPayCell = myDriver.Cell;
				} else {
					result.TaxiPayCell = myDriver.CallInCell;
				}
				result.TaxiPayPin = info.PIN;



				AssignVehicleNo(info.Vehicle, myDriver.AffiliateDriverID);
				//if (!info.Vehicle.IsNullOrEmpty()) {
				//	Vehicles oVehicle = Vehicles.GetVehicle(conn, myAffiliate.AffiliateID, info.Vehicle);
				//	if (oVehicle.IsNullEntity) {
				//		if (myAffiliate.AffiliateDriverDefaults.AffiliateControlsCabNumber) {
				//			result.ErrorMsg = "Vehicle not found. Please contact your office to setup this vehicle.";
				//			return result;
				//		} else {
				//			oVehicle = Vehicles.Create(myAffiliate.AffiliateID);
				//			oVehicle.VehicleNo = info.Vehicle;
				//			try {
				//				oVehicle.VehicleTypeID = VehicleTypes.GetLikeVehicleTypes(conn, myAffiliate.ServiceType).DefaultIfEmpty(VehicleTypes.GetNullEntity()).FirstOrDefault().VehicleTypeID;
				//			} catch (Exception) {

				//			}
				//			if (oVehicle.VehicleTypeID == 0) {
				//				oVehicle.VehicleTypeID = VehicleTypes.GetLikeVehicleTypes(conn, "Taxi").DefaultIfEmpty(VehicleTypes.GetNullEntity()).FirstOrDefault().VehicleTypeID;
				//			}
				//			oVehicle.Save(conn, CabRideDapperSettings.LoggedInUser);
				//		}
				//	}
				//	result.VehicleID = oVehicle.VehicleID;
				//	result.VehicleNo = oVehicle.VehicleNo;

				//	string sql = @"UPDATE dbo.Drivers
				//                                SET VehicleNo = @VehicleNo
				//                                WHERE DriverID = @DriverID";
				//	try {
				//		conn.Execute(sql, new { VehicleNo = oVehicle.VehicleNo, DriverID = oDriver.DriverID });
				//	} catch (Exception ex) {
				//		Console.WriteLine(ex.Message);
				//	}
				//}

				if (result.ErrorMsg.IsNullOrEmpty()) {
					// mark driver as available

					DynamicParameters param = new DynamicParameters();
					//StringBuilder sql = new StringBuilder();
					//sql.AppendFormat("usp_DriversSetAvailability @DriverID = {0}", pDriver.DriverID);
					param.Add("DriverID", oDriver.DriverID);
					param.Add("Status", "Available");
					param.Add("ModifiedBy", "MobileTransWebAPI.AffiliateDriverLogin");
					try {
						conn.Execute("usp_DriversSetAvailability", param, commandType: CommandType.StoredProcedure);
						result.DispatchAvailable = true;
					} catch (Exception ex) {
						result.ErrorMsg = ex.Message;
					}
				}

			}
			return result;
		}

		public static void AssignVehicleNo(string pVehicleNo, long pAffiliateDriverID) {
			using (var conn = SqlHelper.OpenSqlConnection()) {

				if (!pVehicleNo.IsNullOrEmpty()) {
					AffiliateDrivers affiliateDriver = AffiliateDrivers.GetDriver(conn, pAffiliateDriverID);
					Affiliate affiliate = affiliateDriver.Affiliate;
					Vehicles oVehicle = Vehicles.GetVehicle(conn, affiliateDriver.AffiliateID, pVehicleNo);
					if (oVehicle.IsNullEntity) {
						if (affiliate.AffiliateDriverDefaults.AffiliateControlsCabNumber) {
							//result.ErrorMsg = "Vehicle not found. Please contact your office to setup this vehicle.";
							return; // result;
						} else {
							oVehicle = Vehicles.Create(affiliate.AffiliateID);
							oVehicle.VehicleNo = pVehicleNo;
							try {
								oVehicle.VehicleTypeID = VehicleTypes.GetLikeVehicleTypes(conn, affiliate.ServiceType).DefaultIfEmpty(VehicleTypes.GetNullEntity()).FirstOrDefault().VehicleTypeID;
							} catch (Exception) {

							}
							if (oVehicle.VehicleTypeID == 0) {
								oVehicle.VehicleTypeID = VehicleTypes.GetLikeVehicleTypes(conn, "Taxi").DefaultIfEmpty(VehicleTypes.GetNullEntity()).FirstOrDefault().VehicleTypeID;
							}
							oVehicle.Save(conn, CabRideDapperSettings.LoggedInUser);
						}
					}
					affiliateDriver.VehicleID = oVehicle.VehicleID;
					affiliateDriver.Save(conn, CabRideDapperSettings.LoggedInUser);
					//result.VehicleID = oVehicle.VehicleID;
					//result.VehicleNo = oVehicle.VehicleNo;

					//string sql = @"UPDATE dbo.Drivers
					//SET VehicleNo = @VehicleNo
					//WHERE DriverID = @DriverID";
					//try {
					//	conn.Execute(sql, new { VehicleNo = oVehicle.VehicleNo, DriverID = oDriver.DriverID });

					//} catch (Exception ex) {
					//	Console.WriteLine(ex.Message);
					//}

					Drivers driver = Drivers.GetDriverByCell(conn, affiliateDriver.Cell);

					if (!driver.IsNullEntity) {
						driver.VehicleNo = oVehicle.VehicleNo;
						driver.Save(conn, CabRideDapperSettings.LoggedInUser);
					}
				}
			}
		}
	}
}
