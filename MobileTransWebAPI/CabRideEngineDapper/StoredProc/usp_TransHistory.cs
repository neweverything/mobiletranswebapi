﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CabRideEngineDapper {
	public partial class usp_TransHistory {
		public string TransNo { get; set; }
		public string ReferenceNo { get; set; }
		public string CardNumberDisplay { get; set; }
		public string CardType { get; set; }
		public decimal Fare { get; set; }
		public decimal Gratuity { get; set; }
		public decimal AirportFee { get; set; }
		public decimal MiscFee { get; set; }
		public decimal Tolls { get; set; }
		public decimal Parking { get; set; }
		public decimal Stops { get; set; }
		public decimal WaitTime { get; set; }
		public decimal Discount { get; set; }
		public decimal TaxiPassFee { get; set; }
		public decimal TotalCharge { get; set; }
		public decimal DriverTotal { get; set; }
		public DateTime AuthDate { get; set; }
		public DateTime ChargeDate { get; set; }
		public bool Test { get; set; }
		public bool Failed { get; set; }
		public string Platform { get; set; }
		public long DriverPaymentID { get; set; }
		public long AffiliateID { get; set; }
		public string Market { get; set; }
		public decimal CouponAmount { get; set; }
	}
}
