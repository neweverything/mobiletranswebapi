﻿using CabRideEngineDapper.Utils;
using Dapper;
using System.Data.SqlClient;

namespace CabRideEngineDapper {
    public partial class GateWayTransaction {

        public static GateWayTransaction Create(GateWayBatchInfo pInfo) {
            GateWayTransaction rec = GateWayTransactionTable.Create();
            rec.GateWayBatchInfoID = pInfo.GateWayBatchInfoID;
            return rec;
        }

        public static GateWayTransaction GetOrCreate(GateWayBatchInfo pInfo, string pOrderID, string pRefNum) {
            string sql = @"SELECT *
                            FROM   GateWayTransaction
                            WHERE  RefNum = @RefNum
                                   AND GateWayBatchInfoID = @GateWayBatchInfoID
                                   AND OrderID = @OrderID";
            using (var conn = SqlHelper.OpenSqlConnection()) {
                GateWayTransaction rec = conn.Get<GateWayTransaction>(sql, new { RefNum = pRefNum, GateWayBatchInfoID = pInfo.GateWayBatchInfoID, OrderID = pOrderID });
                if (rec.IsNullEntity) {
                    rec = Create(pInfo);
                }
                return rec;
            }
        }

        public static GateWayTransaction GetTransByTransNo(SqlConnection pConn, string pTransNo) {
            string sql = @"SELECT *
                            FROM   GateWayTransaction
                            WHERE  OrderID = @OrderID";
            return pConn.Get<GateWayTransaction>(sql, new { OrderID = pTransNo });
        }

        public static GateWayTransaction GetTransaction(SqlConnection pConn, long pTransID) {
            string sql = @"SELECT *
                            FROM   GateWayTransaction
                            WHERE  GateWayTransactionID = @GateWayTransactionID";
            return pConn.Get<GateWayTransaction>(sql, new { GateWayTransactionID = pTransID });
        }

    }
}
