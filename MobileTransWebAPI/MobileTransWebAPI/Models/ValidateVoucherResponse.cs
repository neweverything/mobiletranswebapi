using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace MobileTransWebAPI.Models {

	public class ValidateVoucherResponse {
		public string ErrorMsg { get; set; }

		public string VoucherNo { get; set; }
		public string TransNo { get; set; }
		public bool Valid { get; set; }
	}
}