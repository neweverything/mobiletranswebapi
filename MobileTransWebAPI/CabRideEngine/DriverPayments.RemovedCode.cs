﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CabRideEngine {
	public partial class DriverPayments {
		//private DriverPaymentResults DoEPayChargeMulti(PersistenceManager pPM, string pCardNo, string pCardInfo, decimal pMaxFee) {

		//    VerisignAccounts oAcct = this.Affiliate.MyVerisignAccount;
		//    string sURL = oAcct.EPayProdURL;
		//    string sSourceKey = oAcct.EPaySourceKey;
		//    string sPin = oAcct.EPayPin;
		//    if (CreditCardUtils.IsTestCard(pCardNo)) {
		//        sURL = oAcct.EPayTestURL;
		//        sSourceKey = oAcct.EPaySourceKeyTest;
		//        sPin = oAcct.EPayPinTest;
		//        this.Test = true;
		//    }

		//    //skip USAePay if we do not have a URL to post to
		//    if (sURL.IsNullOrEmpty()) {
		//        return pPM.GetNullEntity<DriverPaymentResults>();
		//    }

		//    this.VerisignAccountID = oAcct.VerisignAccountID;

		//    TaxiPassCommon.Banking.USAePay oUSAePay = new TaxiPassCommon.Banking.USAePay(sSourceKey, sPin, sURL, oAcct.VerisignTimeout.GetValueOrDefault(0));

		//    string sTranType = VerisignTrxTypes.Sale;
		//    double chargeAmount = Convert.ToDouble(this.TotalCharge);
		//    TaxiPassCommon.com.USAePay.TransactionResponse oResponse = null;

		//    decimal driverTotal = this.DriverTotal;
		//    EntityList<DriverPaymentResults> resultList = new EntityList<DriverPaymentResults>();
		//    int cnt = 0;
		//    double myAmount = 0;
		//    while (driverTotal > 0) {
		//        if (AuthDate == null) {
		//            //New Charge
		//            TaxiPassCommon.com.USAePay.CreditCardData cardData = new TaxiPassCommon.com.USAePay.CreditCardData();

		//            string sComment = "Driver Swipe Entry";
		//            if (string.IsNullOrEmpty(pCardInfo)) {
		//                cardData.CardNumber = pCardNo;
		//                string sDate = DecryptCardExpiration();
		//                string sMonth = StringUtils.Left(sDate, 2);
		//                string sYear = StringUtils.Right(sDate, 2);
		//                cardData.CardExpiration = sMonth + sYear;
		//                sComment = "Driver Manual Entry";
		//            } else {
		//                cardData.MagStripe = pCardInfo;
		//            }
		//            cardData.CardCode = mCVV;
		//            cardData.AvsZip = this.CardZIPCode;

		//            string sServiceType = this.Affiliate.ServiceType;
		//            if (string.IsNullOrEmpty(sServiceType)) {
		//                sServiceType = "Taxi";
		//            }
		//            sServiceType += " Service";

		//            myAmount = Convert.ToDouble(driverTotal);
		//            if (myAmount > Convert.ToDouble(pMaxFee)) {
		//                myAmount = Convert.ToDouble(pMaxFee);
		//            }
		//            driverTotal -= Convert.ToDecimal(myAmount);
		//            myAmount += Convert.ToDouble(CalcFee(this.Affiliate, Convert.ToDecimal(myAmount)));

		//            cnt++;
		//            oResponse = oUSAePay.ChargeCard(cardData, myAmount, sComment, this.TransNo + "-" + cnt, sServiceType, "DP, " + this.DriverID + ", " + this.AffiliateDriverID.GetValueOrDefault(0));
		//            if (oResponse.ErrorCode != "0" && cnt == 1) {
		//                //do verisign
		//                return ProcessResponse(oResponse, sTranType, Convert.ToDecimal(myAmount));
		//            }

		//        } else {
		//            //Delayed Sale
		//            string sRefNo = "";
		//            sTranType = VerisignTrxTypes.DelayedCapture;
		//            foreach (DriverPaymentResults oResult in this.DriverPaymentResultses) {
		//                if (oResult.Result == "0" && oResult.TransType == VerisignTrxTypes.Authorization) {
		//                    sRefNo = oResult.Reference;
		//                    break;
		//                }
		//            }
		//            return ProcessResponse(oUSAePay.ChargeAuth(sRefNo, chargeAmount), sTranType, Convert.ToDecimal(chargeAmount));
		//        }
		//        resultList.Add(ProcessResponse(oResponse, sTranType, Convert.ToDecimal(myAmount)));
		//    }
		//    //check if all results were successful if not roll back and send decline message
		//    bool bOk = true;
		//    DriverPaymentResults returnResult = resultList[0];
		//    foreach (DriverPaymentResults rec in resultList) {
		//        if (rec.Result != "0") {
		//            bOk = false;
		//            returnResult = rec;
		//        }
		//    }
		//    if (!bOk) {
		//        foreach (DriverPaymentResults rec in resultList) {
		//            if (rec.Result == "0") {
		//                oUSAePay.Void(rec.Reference);
		//            }
		//        }
		//    }
		//    return returnResult;

		//}

		// ********************************************************************************
		// 
		// ********************************************************************************
		//private DriverPaymentResults DoVerisignChargeMulti(PersistenceManager pPM, string pCardNo, string pCardInfo, decimal pMaxFee) {

		//    //skip Verisign if we do not have a URL to post to
		//    if (this.Drivers.MyAffiliate.MyVerisignAccount.VerisignAddress.IsNullOrEmpty()) {
		//        return pPM.GetNullEntity<DriverPaymentResults>();
		//    }

		//    VerisignAccounts oAcct = this.Drivers.MyAffiliate.MyVerisignAccount;
		//    Verisign oVerisign = new Verisign(oAcct, SystemDefaults.GetDefaults(pPM));
		//    DriverPaymentResults oPayResults = null;

		//    this.VerisignAccountID = oAcct.VerisignAccountID;

		//    Verisign.CreditCardData oData = new Verisign.CreditCardData();
		//    oData.TrxType = VerisignTrxTypes.Sale;
		//    oData.Tender = VerisignTenderTypes.CreditCard;
		//    oData.Amount = this.TotalCharge;
		//    oData.VoucherNo = this.TransNo;

		//    if (TotalCharge > AuthAmount || AuthDate == null || this.CreditCardProcessor != TaxiPassCommon.Banking.CardProcessors.Verisign.ToString()) {
		//        decimal driverTotal = this.DriverTotal;
		//        EntityList<DriverPaymentResults> resultList = new EntityList<DriverPaymentResults>();
		//        int cnt = 0;
		//        while (driverTotal > 0) {
		//            oVerisign = new Verisign(oAcct, SystemDefaults.GetDefaults(pPM));
		//            oData = new Verisign.CreditCardData();
		//            oData.TrxType = VerisignTrxTypes.Sale;
		//            oData.Tender = VerisignTenderTypes.CreditCard;

		//            //New Charge
		//            if (string.IsNullOrEmpty(pCardInfo)) {
		//                oData.Comment1 = "Driver Manual Entry";
		//                oData.CardNo = pCardNo;
		//                string sDate = DecryptCardExpiration();
		//                string sMonth = StringUtils.Left(sDate, 2);
		//                string sYear = StringUtils.Right(sDate, 2);
		//                oData.ExpDate = sMonth + sYear;
		//                oData.CVV = mCVV;

		//                if (!this.IgnoreCardZIP.GetValueOrDefault(true)) {
		//                    if (!string.IsNullOrEmpty(this.CardZIPCode)) {
		//                        oData.ZipCode = StringUtils.CleanPhoneNumbers(this.CardZIPCode);
		//                    }
		//                }
		//            } else {
		//                oData.Comment1 = "Driver Swipe Entry";
		//                oData.Swipe = pCardInfo;
		//            }


		//            decimal myAmount = driverTotal;
		//            if (myAmount > pMaxFee) {
		//                myAmount = pMaxFee;
		//            }
		//            driverTotal -= myAmount;
		//            myAmount += CalcFee(this.Affiliate, Convert.ToDecimal(myAmount));
		//            cnt++;
		//            oData.Amount = myAmount;
		//            oData.VoucherNo = this.TransNo + "-" + cnt;
		//            oData.DriverIDs = "DP, " + this.DriverID + ", " + this.AffiliateDriverID.GetValueOrDefault(0);

		//            oVerisign.Sale(oData);
		//            oPayResults = ProcessResponse(oVerisign.Response, oData.TrxType, myAmount);
		//            if (oPayResults.Result != "0" && cnt == 1) {
		//                //do verisign
		//                return oPayResults;
		//            }
		//            resultList.Add(oPayResults);
		//        }

		//        //check if all results were successful if not roll back and send decline message
		//        bool bOk = true;
		//        DriverPaymentResults returnResult = resultList[0];
		//        foreach (DriverPaymentResults rec in resultList) {
		//            if (rec.Result != "0") {
		//                bOk = false;
		//                returnResult = rec;
		//            }
		//        }
		//        if (!bOk) {
		//            foreach (DriverPaymentResults rec in resultList) {
		//                if (rec.Result == "0") {
		//                    oData.ReferenceNo = rec.Reference;
		//                    oVerisign.Void(oData);
		//                }
		//            }
		//        }

		//    } else {
		//        //Delayed Sale
		//        oData.TrxType = VerisignTrxTypes.DelayedCapture;
		//        oData.CardNo = pCardNo;
		//        foreach (DriverPaymentResults oResult in this.DriverPaymentResultses) {
		//            if (oResult.Result == "0" && oResult.TransType == VerisignTrxTypes.Authorization) {
		//                oData.ReferenceNo = oResult.Reference;
		//                break;
		//            }
		//        }
		//        oVerisign.DelayedSale(oData);
		//    }

		//    try {
		//        oPayResults = ProcessResponse(oVerisign.Response, oData.TrxType, oData.Amount);
		//    } catch (Exception ex) {
		//        throw ex;
		//    }
		//    //oVerisign.CleanUp();

		//    return oPayResults;
		//}

		// **********************************************************************************************************
		// Adjust ACH payments if voucher has been redeemed & paid before matching
		// **********************************************************************************************************
		// ********************************************************************************************
		// called if the voucher was paid prior to matching and there is a discrepancy between what was 
		// paid and the voucher amount, if an adjustment is needed, call AdjustPayBeforeMatchAmount()
		// ********************************************************************************************
		//public string CheckPayBeforeMatchAdjustment(decimal pMatchAmount) {
		//    string sRetMsg = "";
		//    string sAdminNo = this.ACHDetail.TransCardAdminNo;

		//    DateTime dtStart = Convert.ToDateTime(this.ACHDetail.ACHPaidDate.GetValueOrDefault().AddDays(-1).ToShortDateString()).Date;
		//    DateTime dtEnd = dtStart.AddDays(2).AddMilliseconds(-1);


		//    decimal decPaid = ACHDetail.AmountPaid.GetValueOrDefault(this.Fare);
		//    bool blnNeedToAdjust = false;

		//    com.cabpay.CellPhoneWebService.Services oService = new com.cabpay.CellPhoneWebService.Services();
		//    com.cabpay.CellPhoneWebService.TRANSACTION_HISTORY_RET oHistory = oService.GetTransactionHistory(sAdminNo, dtStart, dtEnd);

		//    if (oHistory.ERROR_FOUND.StartsWith("Y", StringComparison.CurrentCultureIgnoreCase)) {

		//        blnNeedToAdjust = true;

		//    } else {
		//        var oTranRecs = (from p in oHistory.TransactionList
		//                         where p.DESCRIPTION1.Equals(TransNo, StringComparison.CurrentCultureIgnoreCase)
		//                         select new {
		//                             p.AMOUNT,
		//                             p.DATE,
		//                             p.DESCRIPTION1,
		//                             p.END_TRAN_BALANCE,
		//                             p.TIME,
		//                             p.STATUS,
		//                             p.TRANSACTION_TYPE,
		//                             p.TRANSACTION_NUMBER
		//                         }).ToList();
		//        if (oTranRecs.Count != 0) {
		//            blnNeedToAdjust = true;
		//            decPaid = oTranRecs.Sum(p => decimal.Parse(p.AMOUNT.Replace("$", "").Replace(",", "")));
		//        }
		//    }


		//    if (blnNeedToAdjust) {
		//        sRetMsg = AdjustPayBeforeMatchAmount(sAdminNo, decPaid, pMatchAmount);

		//    }

		//    // If fare = 0 then kiosk voucher so collect the $$ from the customer
		//    if (Fare == 0) {
		//        Fare = pMatchAmount;
		//        Save();
		//        ChargeCard();
		//    }


		//    return sRetMsg;
		//}

		// ********************************************************************************************
		// Performs the actual adjustment to a PayBeforeMatch voucher
		// ********************************************************************************************
		//        public string AdjustPayBeforeMatchAmount(string sPayeeAdminNo, decimal decPaid, decimal pMatchAmount) {
		//            StringBuilder CallingInfo = new StringBuilder();
		//            string sRetMsg = "";
		//            com.cabpay.CellPhoneWebService.Services oService = new com.cabpay.CellPhoneWebService.Services();

		//            StringBuilder sRequest = new StringBuilder();
		//            StringBuilder sMsg = new StringBuilder();

		//            decimal dDiff;

		//            //PayCard oPayCard = new PayCard();
		//            //oPayCard.SetCardNumber(this.PersistenceManager, sPayeeAdminNo);
		//            //oPayCard.TCCardInfo.CardNumber = oPayCard.TCDefaults.AcctNo;

		//            //string sWork = Newtonsoft.Json.JsonConvert.SerializeObject(oPayCard.TCCardInfo).EncryptIceKey();
		//            //sWork = oService.ValidateCard2(sWork).DecryptIceKey();
		//            //sWork = sWork.DecryptIceKey();
		//            //TransCard.VALIDATE_CARD_RET oTPCard = Newtonsoft.Json.JsonConvert.DeserializeObject<TransCard.VALIDATE_CARD_RET>(sWork);

		//            //oPayCard.TCCardInfo.CardNumber = oPayCard.TCCardInfo.CardNumber;

		//            //sWork = Newtonsoft.Json.JsonConvert.SerializeObject(oPayCard.TCCardInfo).EncryptIceKey();
		//            //sWork = oService.ValidateCard2(sWork).DecryptIceKey();
		//            //sWork = sWork.DecryptIceKey();
		//            //TransCard.VALIDATE_CARD_RET oPayeeCard = Newtonsoft.Json.JsonConvert.DeserializeObject<TransCard.VALIDATE_CARD_RET>(sWork);

		////			PayCardTransfer oPayTrans = PayCardTransfer.Create(this.PersistenceManager);

		//            if (decPaid < pMatchAmount) {

		//                // under paid the pay-before-match voucher
		//                #region underpaid
		//                dDiff = pMatchAmount - this.ACHDetail.AmountPaid.GetValueOrDefault(0);

		//                oPayTrans.FromHolder = oTPCard.FIRST_NAME + " " + oTPCard.LAST_NAME;
		//                oPayTrans.ToHolder = oPayeeCard.FIRST_NAME + " " + oPayeeCard.LAST_NAME;

		//                oPayTrans.TransCardFromNumber = oPayCard.TCDefaults.FundingAccount;
		//                oPayTrans.TransCardToNumber = sPayeeAdminNo;

		//                oPayTrans.TransCardFromAdminNo = oTPCard.ACCOUNT_NO;
		//                oPayTrans.TransCardToAdminNo = oPayeeCard.ACCOUNT_NO;

		//                sRequest.Append("from=");
		//                sRequest.Append(oPayCard.TCDefaults.FundingAccount.DecryptIceKey());
		//                sRequest.Append("&to=");
		//                sRequest.Append(this.ACHDetail.TransCardAdminNo);
		//                sRequest.Append("&date=");
		//                sRequest.Append(DateTime.Now.ToUniversalTime());
		//                sRequest.Append("&voucherNo=");

		//                oPayCard = new PayCard();
		//                oPayCard.SetCardNumber(this.PersistenceManager, this.ACHDetail.TransCardAdminNo);

		//                sRequest.Append("&CardNo=");
		//                sRequest.Append(oPayCard.TCCardInfo.CardNumber.DecryptIceKey());

		//                sRequest.Append("&ToEmployeeID=");
		//                sRequest.Append(oPayCard.TCCardInfo.EmployeeID);
		//                sRequest.Append("&RemoveFundsFromCard=False");

		//                CallingInfo.Append("DriverPayments.AdjustPayBeforeMatchAmount.underpaid: ");
		//                CallingInfo.Append(" | ");
		//                try {
		//                    CallingInfo.Append(System.AppDomain.CurrentDomain.BaseDirectory);
		//                } catch (Exception) {
		//                }
		//                sRequest.AppendFormat("&CallingInfo={0}", CallingInfo.ToString());

		//                sMsg.Append("UndrPd ");
		//                sMsg.Append(this.TransNo);
		//                sMsg.Append(" ");
		//                sMsg.Append(dDiff.ToString("C"));

		//                sRequest.Append(sMsg.ToString());
		//                #endregion underpaid

		//            } else {

		//                // over paid the pay-before-match voucher
		//                #region overpaid
		//                dDiff = this.ACHDetail.AmountPaid.GetValueOrDefault(0) - pMatchAmount;

		//                oPayTrans.FromHolder = oPayeeCard.FIRST_NAME + " " + oPayeeCard.LAST_NAME;
		//                oPayTrans.ToHolder = oTPCard.FIRST_NAME + " " + oTPCard.LAST_NAME;

		//                oPayTrans.TransCardFromNumber = sPayeeAdminNo;
		//                oPayTrans.TransCardToNumber = oPayCard.TCDefaults.FundingAccount;

		//                oPayTrans.TransCardFromAdminNo = oPayeeCard.ACCOUNT_NO;
		//                oPayTrans.TransCardToAdminNo = oTPCard.ACCOUNT_NO;

		//                sRequest.Append("from=");
		//                sRequest.Append(this.ACHDetail.TransCardAdminNo);
		//                sRequest.Append("&to=");
		//                sRequest.Append(oPayCard.TCDefaults.FundingAccount.DecryptIceKey());
		//                sRequest.Append("&date=");
		//                sRequest.Append(DateTime.Now.ToUniversalTime());
		//                sRequest.Append("&voucherNo=");

		//                // how to handle Comdata?
		//                PayCard oToPayCard = new PayCard();
		//                oToPayCard.SetCardNumber(this.PersistenceManager, this.ACHDetail.TransCardAdminNo);
		//                sRequest.Append("&CardNo=");
		//                sRequest.Append(oToPayCard.TCCardInfo.CardNumber);

		//                sRequest.Append("&ToEmployeeID=");
		//                sRequest.Append(oPayCard.TCCardInfo.EmployeeID);

		//                sRequest.Append("&TransCardDefaultID=");
		//                sRequest.Append(oPayCard.TCDefaults.TransCardDefaultID);

		//                sRequest.Append("&RemoveFundsFromCard=True");

		//                CallingInfo.Append("DriverPayments.AdjustPayBeforeMatchAmount.overpaid: ");
		//                CallingInfo.Append(" | ");
		//                try {
		//                    CallingInfo.Append(System.AppDomain.CurrentDomain.BaseDirectory);
		//                } catch (Exception) {
		//                }
		//                sRequest.AppendFormat("&CallingInfo={0}", CallingInfo.ToString());


		//                sMsg.Append("OvrPd ");
		//                sMsg.Append(this.TransNo);
		//                sMsg.Append(" ");
		//                sMsg.Append(dDiff.ToString("C"));

		//                sRequest.Append(sMsg.ToString());
		//                #endregion overpaid
		//            }

		//            oPayTrans.Amount = dDiff;
		//            oPayTrans.Notes = sMsg.ToString();

		//            CabRideEngine.com.cabpay.CellPhoneWebService.LOAD_CARD_FUNDED_RET fundResults = oService.TransferFunds2(sRequest.ToString().EncryptIceKey(), dDiff);
		//            if (fundResults.ERROR_FOUND.StartsWith("Y")) {
		//                oPayTrans.Failed = true;
		//                oPayTrans.FailedReason = fundResults.ERROR_MESSAGE;
		//            }

		//            oPayTrans.Save();
		//            return sRetMsg;

		//        }



		// **********************************************************************************
		// pull funds from a paycard
		// **********************************************************************************
		//public TransCard.LOAD_CARD_FUNDED_RET ChargePayCard(AffiliateDrivers pPayeeDriver) {
		//    TransCardDefaults tranDef = TransCardDefaults.GetDefaults(this.PersistenceManager, pPayeeDriver.TransCardNumber);
		//    if (tranDef == null) {
		//        tranDef = TransCardDefaults.GetDefaultTransCard(this.PersistenceManager);
		//    }

		//    TransCard oCard = new TransCard(tranDef.GetTransCardAccess());

		//    if (pPayeeDriver.TransCardNumber.IsNullOrEmpty()) {
		//        return new TransCard.LOAD_CARD_FUNDED_RET();
		//    }

		//    TransCard.CardInfo oCardInfoTo = new TransCard.CardInfo();
		//    oCardInfoTo.CardNumber = tranDef.FundingAccount;
		//    oCardInfoTo.ComData = tranDef.ComData;
		//    oCardInfoTo.RemoveFundsFromCard = true;
		//    oCardInfoTo.EmployeeID = pPayeeDriver.AffiliateDriverID.ToString();

		//    PayCard oPayCardFunding = new PayCard();
		//    oPayCardFunding.SetCardNumber(this.PersistenceManager, pPayeeDriver.TransCardNumber);
		//    oPayCardFunding.RemoveFundsFromCard = true;

		//    TransCard.LOAD_CARD_FUNDED_RET fundResults = oCard.LoadCardFunds(oCardInfoTo, oPayCardFunding.TCCardInfo, Math.Abs(this.DriverTotalPaid), this.TransNo);
		//    if (fundResults.ERROR_FOUND.Equals("Yes", StringComparison.CurrentCultureIgnoreCase)) {
		//        this.Notes = "Error charging paycard: " + fundResults.ERROR_MESSAGE;
		//        this.Save();

		//    } else {

		//        if (this.ACHDetail.IsNullEntity) {
		//            ACHDetail.Create(this);
		//        }
		//        this.ACHDetail.ACHPaidDate = DateTime.Now;
		//        this.ACHDetail.ACHPaidTo = pPayeeDriver.Name;
		//        this.ACHDetail.ACHBankAccountNumber = "TransCard: " + pPayeeDriver.TransCardNumber.Right(5);
		//        int i = 0;
		//        while (i < 5) {
		//            try {
		//                this.Save();
		//                try {
		//                    CabRideEngine.Utils.Misc.SendTransCardPaymentSMS(this, pPayeeDriver, fundResults.CURRENT_CARD_BALANCE);
		//                } catch (Exception) {
		//                }
		//                break;
		//            } catch (Exception) {
		//            }
		//            i++;
		//        }
		//    }
		//    return fundResults;
		//}






		//private PayAccountResult PayRedeemerAccount(TaxiPassRedeemers pRedeemer, decimal pTotalPay, DateTime pDriverPaidDate) {
		//    PersistenceManager oPM = this.PersistenceManager;

		//    PayAccountResult result = new PayAccountResult();
		//    //check to make sure we have not already paid redeemer
		//    ACHDetail rec = ACHDetail.GetTransactionByDriverPaymentID(pRedeemer.PersistenceManager, this.DriverPaymentID, QueryStrategy.DataSourceThenCache);
		//    if (!rec.IsNullEntity) {
		//        return result;
		//    }


		//    TaxiPassRedeemerAccounts acct = pRedeemer.TaxiPassRedeemerAccounts;
		//    TransCardDefaults tranDef = TransCardDefaults.GetDefaults(oPM, acct.TransCardNumber);
		//    if (tranDef == null) {
		//        tranDef = TransCardDefaults.GetDefaultTransCard(this.PersistenceManager);
		//    }
		//    TransCard oCard = new TransCard(tranDef.GetTransCardAccess());

		//    if (acct.MarkAsACHd) {
		//        if (this.ACHDetail.IsNullEntity) {
		//            ACHDetail.Create(this);
		//        }
		//        this.ACHDetail.ACHPaidDate = DateTime.Now;
		//        this.ACHDetail.ACHPaidTo = pRedeemer.TaxiPassRedeemerAccounts.CompanyName;
		//        this.ACHDetail.Save();
		//        return result;
		//    }
		//    TransCard.CardInfo oCardInfo = new TransCard.CardInfo();
		//    oCardInfo.CardNumber = tranDef.FundingAccount;
		//    oCardInfo.ComData = tranDef.ComData;
		//    TransCard.VALIDATE_CARD_RET cardResults = oCard.ValidateCard(oCardInfo);

		//    if (cardResults.ERROR_FOUND.Equals("Yes", StringComparison.CurrentCultureIgnoreCase)) {
		//        result.Error = true;
		//        result.ErrorMsg = "System Error redeemers cannot be paid at this time: " + cardResults.ERROR_FOUND;
		//        return result;
		//    }
		//    if (Convert.ToDecimal(cardResults.CURRENT_CARD_BALANCE.Replace("$", "")) < pTotalPay) {
		//        result.Error = true;
		//        result.ErrorMsg = "System Error redeemers cannot be paid at this time.  Current account balance: " + cardResults.CURRENT_CARD_BALANCE;
		//        return result;
		//    }
		//    TransCard.LOAD_CARD_FUNDED_RET fundResults = FundPayCard(acct);

		//    if (fundResults.ERROR_FOUND.Equals("Yes", StringComparison.CurrentCultureIgnoreCase)) {
		//        result.Error = true;
		//        result.ErrorMsg = "Error funding card for voucher " + this.TransNo + ": " + fundResults.ERROR_MESSAGE + "<br />";
		//    }

		//    return result;
		//}

		// **********************************************************************************************
		// Transfers the money to the Affiliate Driver
		// **********************************************************************************************
		//public TransCard.LOAD_CARD_FUNDED_RET FundPayCard(AffiliateDrivers pPayeeDriver) {

		//    decimal decAmount = 0;
		//    TransCard.LOAD_CARD_FUNDED_RET fundResults = new TransCard.LOAD_CARD_FUNDED_RET();
		//    fundResults.ERROR_FOUND = "Yes";
		//    fundResults.CURRENT_CARD_BALANCE = "";

		//    if (pPayeeDriver.IsNullEntity
		//        || pPayeeDriver.TransCardNumber.IsNullOrEmpty()
		//        || this.Affiliate.DriverInitiatedPayCardPayment
		//        || this.Test
		//        || this.DoNotPay
		//        || this.Failed
		//        || pPayeeDriver.RedeemerStopPayment) {

		//        fundResults.ERROR_FOUND = "No";
		//        fundResults.CURRENT_CARD_BALANCE = "";
		//        return fundResults;
		//    }

		//    ACHDetail achRec = ACHDetail.GetTransactionByDriverPaymentID(this.PersistenceManager, this.DriverPaymentID, QueryStrategy.DataSourceThenCache);
		//    if (!achRec.ACHPaidDate.HasValue) {
		//        if (achRec.IsNullEntity) {
		//            achRec = ACHDetail.Create(this);
		//        }
		//        try {
		//            if (pPayeeDriver.PayCardRealTimePay || pPayeeDriver.Affiliate.PayCardRealTimePay) {
		//                decAmount = this.DriverTotalPaid;

		//                if (decAmount == 0) {
		//                    decAmount = DriverPaymentKioskAmount.Amount;
		//                }

		//                if (decAmount == 0) {
		//                    fundResults.ERROR_FOUND = "Yes";
		//                    achRec.ACHErrorReason = "No Fare amount found";

		//                } else {

		//                    PayCard oPayCardTo = new PayCard();
		//                    oPayCardTo.SetCardNumber(this.PersistenceManager, pPayeeDriver.TransCardNumber);
		//                    oPayCardTo.RemoveFundsFromCard = false;

		//                    TransCard oCard = new TransCard(oPayCardTo.TCDefaults.GetTransCardAccess());

		//                    achRec.AmountPaid = decAmount;

		//                    TransCard.CardInfo oFundingInfo = new TransCard.CardInfo();
		//                    oFundingInfo.CardNumber = oPayCardTo.TCDefaults.FundingAccount;
		//                    oFundingInfo.ComData = oPayCardTo.TCDefaults.ComData;

		//                    fundResults = oCard.LoadCardFunds(oPayCardTo.TCCardInfo, oFundingInfo, decAmount, this.TransNo);
		//                }
		//            }

		//        } catch (Exception ex) {
		//            fundResults.ERROR_FOUND = "Yes";
		//            fundResults.CURRENT_CARD_BALANCE = "";
		//            string msg = ex.Message;
		//        }

		//        if (fundResults.ACCOUNT_NO.IsNullOrEmpty() || fundResults.ERROR_FOUND.StartsWith("y", StringComparison.CurrentCultureIgnoreCase)) {
		//            if (pPayeeDriver.PayCardRealTimePay || pPayeeDriver.Affiliate.PayCardRealTimePay) {
		//                try {
		//                    TransCardPaymentFailures failedRec = TransCardPaymentFailures.Create(this);
		//                    if (fundResults.ERROR_MESSAGE.IsNullOrEmpty()) {
		//                        failedRec.ErrorMessage = "TransCard communication failure";
		//                    } else {
		//                        failedRec.ErrorMessage = fundResults.ERROR_MESSAGE;
		//                    }
		//                    failedRec.Save();
		//                } catch (Exception) {
		//                    //don't blow up due to not being able to save this record
		//                }

		//            } else {
		//                achRec.ACHErrorReason = "Batch Required";
		//                achRec.BatchTime = DateTime.Now;
		//            }
		//        } else {
		//            achRec.ACHErrorReason = "Paid: " + this.DriverTotalPaid.ToString("C");
		//            achRec.AmountPaid = this.DriverTotalPaid;
		//        }
		//        achRec.ACHPaidDate = DateTime.Now;
		//        achRec.ACHPaidTo = pPayeeDriver.Name;
		//        achRec.ACHBankAccountNumber = "TransCard: " + pPayeeDriver.TransCardNumber.Right(5);

		//        if (pPayeeDriver.TransCardAdminNo.IsNullOrEmpty()) {
		//            pPayeeDriver.TransCardAdminNo = fundResults.ACCOUNT_NO;
		//            try {
		//                pPayeeDriver.Save();
		//            } catch (Exception) {
		//            }
		//        }
		//        achRec.TransCardAdminNo = pPayeeDriver.TransCardAdminNo;
		//        achRec.ExternalConfNo = fundResults.REFERENCE_NUMBER;
		//        //}
		//        if (!this.DriverPaid) {
		//            DriverPaid = true;
		//            DriverPaidDate = achRec.ACHPaidDate;
		//        }
		//    }
		//    int i = 0;

		//    // try to save 5 times
		//    while (i < 5) {
		//        try {
		//            this.Save();
		//            try {
		//                // Only send affirmative message if no errors
		//                if (fundResults.ERROR_FOUND.StartsWith("n", StringComparison.CurrentCultureIgnoreCase)) {
		//                    CabRideEngine.Utils.Misc.SendTransCardPaymentSMS(this, pPayeeDriver, fundResults.CURRENT_CARD_BALANCE);
		//                } else {
		//                    StringBuilder msg = new StringBuilder("Voucher ");
		//                    msg.Append(this.TransNo);
		//                    msg.Append(" received. ");
		//                    msg.Append(this.DriverTotalPaid.ToString("C"));
		//                    if (this.RedemptionFee.GetValueOrDefault(0) > 0) {
		//                        msg.Append(" (" + this.DriverTotal.ToString("C") + " - " + this.RedemptionFee.Value.ToString("C") + " redemption fee)");
		//                    }
		//                    if (this.DriverFee.GetValueOrDefault(0) > 0) {
		//                        msg.Append(" - (" + this.DriverFee.Value.ToString("C") + " driver fee)");
		//                    }
		//                    msg.Append(" will be summed with other vouchers and added to card *");
		//                    msg.Append(pPayeeDriver.TransCardNumber.Right(5));

		//                    msg.Append(". Another text will be sent when we add funds to your card.");
		//                    pPayeeDriver.SendDriverMsg(msg.ToString(), false);
		//                }
		//            } catch (Exception) {
		//            }
		//            break;
		//        } catch (Exception) {
		//        }
		//        i++;
		//    }
		//    return fundResults;
		//}


		// **********************************************************************************************
		// Transfers the money to the Redeemer Account
		// **********************************************************************************************
		//public TransCard.LOAD_CARD_FUNDED_RET FundPayCard(TaxiPassRedeemerAccounts pRedeemerAcct) {
		//    return FundPayCard(pRedeemerAcct, 0);
		//}


		// **********************************************************************************************
		// Transfers the money to the Redeemer Account, if pAmount is 0 then use this.RedeemerTotalPaid
		// **********************************************************************************************
		//public TransCard.LOAD_CARD_FUNDED_RET FundPayCard(TaxiPassRedeemerAccounts pRedeemerAcct, decimal pAmount) {

		//    string sCardNumber = pRedeemerAcct.TransCardNumber;
		//    string adminNo = pRedeemerAcct.TransCardAdminNo;
		//    bool bDefaultPayCard = true;
		//    RedeemerPayCards oRedeemerPayCard = null;
		//    TransCard.LOAD_CARD_FUNDED_RET fundResults = new TransCard.LOAD_CARD_FUNDED_RET();

		//    TransCardDefaults tranDef = TransCardDefaults.GetDefaults(this.PersistenceManager, sCardNumber);
		//    if (tranDef == null) {
		//        tranDef = TransCardDefaults.GetDefaultTransCard(this.PersistenceManager);
		//    }
		//    //determine card to use if more than 1 exists
		//    int i = 0;

		//    if (pRedeemerAcct.RedeemerPayCardses != null && pRedeemerAcct.RedeemerPayCardses.Count > 0) {
		//        decimal amtPaid = GetTodaysAchPaidAmount(pRedeemerAcct, sCardNumber);
		//        //tranDef = TransCardDefaults.GetDefaults(this.PersistenceManager, sCardNumber);  - duplicate line
		//        i = 0;
		//        decimal maxFunding = tranDef.MaxFundPerCardPerDay;
		//        if (maxFunding == 0) {
		//            maxFunding = 5000;
		//        }

		//        while (amtPaid > tranDef.MaxFundPerCardPerDay && i < pRedeemerAcct.RedeemerPayCardses.Count) {
		//            bDefaultPayCard = false;
		//            oRedeemerPayCard = pRedeemerAcct.RedeemerPayCardses[i];
		//            sCardNumber = oRedeemerPayCard.TransCardNumber;
		//            adminNo = oRedeemerPayCard.TransCardAdminNo;
		//            amtPaid = GetTodaysAchPaidAmount(pRedeemerAcct, sCardNumber);
		//            tranDef = TransCardDefaults.GetDefaults(this.PersistenceManager, sCardNumber);
		//            i++;
		//        }
		//    }
		//    TransCard oCard = new TransCard(tranDef.GetTransCardAccess());


		//    if (pRedeemerAcct.PayCardRealTimePay) {
		//        PayCard oToPayCard = new PayCard();
		//        oToPayCard.SetCardNumber(this.PersistenceManager, sCardNumber);
		//        oToPayCard.RemoveFundsFromCard = false;

		//        TransCard.VALIDATE_CARD_RET valResult = oCard.ValidateCard(oToPayCard.TCCardInfo);
		//        if (!valResult.ERROR_FOUND.Equals("Yes", StringComparison.CurrentCultureIgnoreCase)) {
		//            if (!valResult.ACCOUNT_NO.IsNullOrEmpty()) {
		//                adminNo = valResult.ACCOUNT_NO;
		//            }

		//            if (!adminNo.IsNullOrEmpty()) {
		//                if (bDefaultPayCard) {
		//                    if (adminNo != pRedeemerAcct.TransCardAdminNo) {
		//                        pRedeemerAcct.TransCardAdminNo = adminNo;
		//                        pRedeemerAcct.Save("FundPayCard - Redeemer");
		//                    }
		//                } else {
		//                    if (adminNo != oRedeemerPayCard.TransCardAdminNo) {
		//                        oRedeemerPayCard.TransCardAdminNo = adminNo;
		//                        oRedeemerPayCard.Save("FundPayCard - Redeemer");
		//                    }
		//                }
		//            }
		//        }

		//        ACHDetail achRec = ACHDetail.GetTransactionByDriverPaymentID(this.PersistenceManager, this.DriverPaymentID, QueryStrategy.DataSourceThenCache);
		//        if (!achRec.ACHPaidDate.HasValue) {
		//            try {
		//                //pAmount will be > 0 on PayBeforeMatch redeemed vouchers
		//                if (pAmount == 0) {
		//                    pAmount = this.RedeemerTotalPaid;
		//                }

		//                TransCard.CardInfo oFundingInfo = new TransCard.CardInfo();
		//                oFundingInfo.CardNumber = tranDef.FundingAccount;
		//                oFundingInfo.ComData = tranDef.ComData;

		//                fundResults = oCard.LoadCardFunds(oToPayCard.TCCardInfo, oFundingInfo, pAmount, this.TransNo);

		//            } catch (Exception ex) {
		//                fundResults.ERROR_FOUND = "Yes";
		//                fundResults.CURRENT_CARD_BALANCE = "";
		//                string s = ex.Message;
		//                Console.WriteLine(s);
		//            }
		//        }

		//    } else {
		//        // Batch mode
		//        fundResults.ERROR_FOUND = "No";
		//        fundResults.CURRENT_CARD_BALANCE = "";
		//    }

		//    if (this.ACHDetail.IsNullEntity) {
		//        ACHDetail.Create(this);
		//    }
		//    if (fundResults.ACCOUNT_NO.IsNullOrEmpty() || fundResults.ERROR_FOUND.StartsWith("Y", StringComparison.CurrentCultureIgnoreCase)) {
		//        if (pRedeemerAcct.PayCardRealTimePay) {
		//            try {
		//                TransCardPaymentFailures failedRec = TransCardPaymentFailures.Create(this);
		//                if (fundResults.ERROR_MESSAGE.IsNullOrEmpty()) {
		//                    failedRec.ErrorMessage = "TransCard communication failure";
		//                } else {
		//                    failedRec.ErrorMessage = fundResults.ERROR_MESSAGE;
		//                }
		//                failedRec.Save();
		//            } catch (Exception) {
		//                //don't blow up due to not being able to save this record
		//            }
		//        } else {
		//            this.ACHDetail.ACHErrorReason = "Batch Required";
		//        }
		//    } else {
		//        this.ACHDetail.ACHErrorReason = "Paid: " + pAmount.ToString("C");
		//    }

		//    this.ACHDetail.ACHPaidDate = DateTime.Now;
		//    this.ACHDetail.ACHPaidTo = "Redeemer: " + pRedeemerAcct.CompanyName;
		//    this.ACHDetail.ACHBankAccountNumber = "TransCard: " + sCardNumber.Right(5);
		//    this.ACHDetail.TransCardAdminNo = adminNo;
		//    this.ACHDetail.ExternalConfNo = fundResults.REFERENCE_NUMBER;

		//    if (!this.DriverPaid) {
		//        DriverPaid = true;
		//        DriverPaidDate = this.ACHDetail.ACHPaidDate;
		//    }

		//    i = 0;
		//    while (i < 5) {
		//        try {
		//            this.Save();
		//            try {
		//                if (!fundResults.CURRENT_CARD_BALANCE.IsNullOrEmpty()) {
		//                    CabRideEngine.Utils.Misc.SendTransCardPaymentSMS(this, pRedeemerAcct, pAmount, fundResults.CURRENT_CARD_BALANCE);

		//                } else {
		//                    StringBuilder msg = new StringBuilder("Voucher ");
		//                    msg.Append(this.TransNo);
		//                    msg.Append(" received. ");
		//                    msg.Append(pAmount.ToString("C"));

		//                    if (this.DriverFee.GetValueOrDefault(0) > 0) {
		//                        msg.Append(" - (" + this.DriverFee.Value.ToString("C") + " driver fee)");
		//                    }
		//                    msg.Append(" will be summed with other vouchers and added to card *");
		//                    msg.Append(sCardNumber.Right(5));

		//                    msg.Append(". Another text will be sent when we add funds to your card.");
		//                    pRedeemerAcct.SendMsg(msg.ToString());
		//                }

		//                //pay out promos if any
		//                DoPromoPayments();
		//            } catch (Exception) {
		//            }
		//            break;
		//        } catch (Exception) {
		//        }
		//        i++;
		//    }
		//    return fundResults;
		//}


		// **********************************************************************************************
		// Transfers the money to the Redeemer's shift Transcard
		// **********************************************************************************************
		//public TransCard.LOAD_CARD_FUNDED_RET FundPayCard(long pRedeemerShiftID, decimal pAmount) {

		//    StringBuilder sql = new StringBuilder();
		//    sql.Append(" usp_RedeemersGetShiftCard @RedeemerShiftID = ");
		//    sql.Append(pRedeemerShiftID);

		//    DataTable dtShift = CabRideEngine.Utils.SQLHelper.GetRecords(this.PersistenceManager, sql.ToString());
		//    TaxiPassRedeemerAccounts pRedeemerAcct = TaxiPassRedeemerAccounts.GetByID(this.PersistenceManager, long.Parse(dtShift.Rows[0]["TaxiPassRedeemerAccountID"].ToString()));

		//    string sCardNumber = dtShift.Rows[0]["TransCardNumber"].ToString();
		//    string adminNo = dtShift.Rows[0]["TransCardAdminNo"].ToString();

		//    bool bPayCardRealTimePay = bool.Parse(dtShift.Rows[0]["PayCardRealTimePay"].ToString());

		//    TransCard.LOAD_CARD_FUNDED_RET fundResults = new TransCard.LOAD_CARD_FUNDED_RET();

		//    if (this.Test || this.Failed) {
		//        return fundResults;
		//    }

		//    TransCardDefaults tranDef = TransCardDefaults.GetDefaults(this.PersistenceManager, sCardNumber);
		//    if (tranDef == null) {
		//        tranDef = TransCardDefaults.GetDefaultTransCard(this.PersistenceManager);
		//    }
		//    //determine card to use if more than 1 exists
		//    int i = 0;
		//    TransCard oCard = new TransCard(tranDef.GetTransCardAccess());


		//    if (bPayCardRealTimePay) {
		//        PayCard oPayCardTo = new PayCard();
		//        oPayCardTo.SetCardNumber(this.PersistenceManager, sCardNumber);
		//        oPayCardTo.RemoveFundsFromCard = false;

		//        TransCard.VALIDATE_CARD_RET valResult = oCard.ValidateCard(oPayCardTo.TCCardInfo);

		//        if (!valResult.ERROR_FOUND.Equals("Yes", StringComparison.CurrentCultureIgnoreCase)) {
		//            if (!valResult.ACCOUNT_NO.IsNullOrEmpty()) {
		//                adminNo = valResult.ACCOUNT_NO;
		//            }

		//            if (!adminNo.IsNullOrEmpty()) {

		//                if (adminNo != dtShift.Rows[0]["TransCardAdminNo"].ToString()) {
		//                    // need to update the admin no in the proper table
		//                    sql.Remove(0, sql.Length);
		//                    sql.Append("usp_RedeemerShiftUpdateTCAdminNo @RedeemerShiftID =");
		//                    sql.Append(pRedeemerShiftID);
		//                    sql.Append(", @TransCardAdminNo = '");
		//                    sql.Append(adminNo);
		//                    sql.Append("'");
		//                    CabRideEngine.Utils.SQLHelper.GetRecords(this.PersistenceManager, sql.ToString());
		//                }
		//            }
		//        }

		//        ACHDetail achRec = ACHDetail.GetTransactionByDriverPaymentID(this.PersistenceManager, this.DriverPaymentID, QueryStrategy.DataSourceThenCache);
		//        if (!achRec.ACHPaidDate.HasValue) {
		//            try {

		//                //pAmount will be > 0 on PayBeforeMatch redeemed vouchers
		//                if (pAmount == 0) {
		//                    pAmount = this.RedeemerTotalPaid;
		//                }

		//                TransCard.CardInfo oFundingInfo = new TransCard.CardInfo();
		//                oFundingInfo.CardNumber = tranDef.FundingAccount;
		//                oFundingInfo.ComData = tranDef.ComData;

		//                fundResults = oCard.LoadCardFunds(oPayCardTo.TCCardInfo, oFundingInfo, pAmount, this.TransNo);

		//            } catch (Exception ex) {
		//                fundResults.ERROR_FOUND = "Yes";
		//                fundResults.CURRENT_CARD_BALANCE = "";
		//                string s = ex.Message;
		//                Console.WriteLine(s);
		//            }
		//        }

		//    } else {
		//        // Batch mode
		//        fundResults.ERROR_FOUND = "No";
		//        fundResults.CURRENT_CARD_BALANCE = "";
		//    }

		//    if (this.ACHDetail.IsNullEntity) {
		//        ACHDetail.Create(this);
		//    }
		//    if (fundResults.ACCOUNT_NO.IsNullOrEmpty() || fundResults.ERROR_FOUND.StartsWith("Y", StringComparison.CurrentCultureIgnoreCase)) {
		//        if (bPayCardRealTimePay) {
		//            try {
		//                TransCardPaymentFailures failedRec = TransCardPaymentFailures.Create(this);
		//                if (fundResults.ERROR_MESSAGE.IsNullOrEmpty()) {
		//                    failedRec.ErrorMessage = "TransCard communication failure";
		//                } else {
		//                    failedRec.ErrorMessage = fundResults.ERROR_MESSAGE;
		//                }
		//                failedRec.Save();
		//            } catch (Exception) {
		//                //don't blow up due to not being able to save this record
		//            }
		//        } else {
		//            this.ACHDetail.ACHErrorReason = "Batch Required";
		//        }
		//    } else {
		//        this.ACHDetail.ACHErrorReason = "Paid: " + pAmount.ToString("C");
		//    }

		//    this.ACHDetail.ACHPaidDate = DateTime.Now;
		//    this.ACHDetail.ACHPaidTo = "Redeemer: " + dtShift.Rows[0]["CompanyName"].ToString();
		//    this.ACHDetail.ACHBankAccountNumber = "TransCard: " + sCardNumber.Right(5);
		//    this.ACHDetail.TransCardAdminNo = adminNo;
		//    this.ACHDetail.ExternalConfNo = fundResults.REFERENCE_NUMBER;

		//    if (!this.DriverPaid) {
		//        DriverPaid = true;
		//        DriverPaidDate = this.ACHDetail.ACHPaidDate;
		//    }

		//    i = 0;
		//    while (i < 5) {
		//        try {
		//            this.Save();
		//            try {
		//                if (!fundResults.CURRENT_CARD_BALANCE.IsNullOrEmpty()) {
		//                    CabRideEngine.Utils.Misc.SendTransCardPaymentSMS(this, pRedeemerAcct, pAmount, fundResults.CURRENT_CARD_BALANCE);

		//                } else {
		//                    StringBuilder msg = new StringBuilder("Voucher ");
		//                    msg.Append(this.TransNo);
		//                    msg.Append(" received. ");
		//                    msg.Append(pAmount.ToString("C"));

		//                    if (this.DriverFee.GetValueOrDefault(0) > 0) {
		//                        msg.Append(" - (" + this.DriverFee.Value.ToString("C") + " driver fee)");
		//                    }
		//                    msg.Append(" will be summed with other vouchers and added to card *");
		//                    msg.Append(sCardNumber.Right(5));

		//                    msg.Append(". Another text will be sent when we add funds to your card.");
		//                    pRedeemerAcct.SendMsg(msg.ToString());
		//                }

		//                //pay out promos if any
		//                DoPromoPayments();
		//            } catch (Exception) {
		//            }
		//            break;
		//        } catch (Exception) {
		//        }
		//        i++;
		//    }
		//    return fundResults;
		//}

		// *****************************************************************************************************
		// Process the response for Verisign
		// AVSADDR and AVSZIP responses: 
		//		Y = match 
		//		N = no match
		//		X = cardholder’s bank does not support address verification service
		// *****************************************************************************************************
		//private DriverPaymentResults ProcessResponse(string pPayFloResponse, string pTrxType, decimal pAmount) {
		//    PersistenceManager oPM = this.PersistenceManager;

		//    if (this.DriverPaymentNotes.IsNullEntity) {
		//        DriverPaymentNotes.Create(this);
		//    }

		//    string[] aResponse = new string[] { };

		//    if (!string.IsNullOrEmpty(pPayFloResponse)) {
		//        aResponse = pPayFloResponse.Split('&');
		//    }

		//    try {
		//        DriverPaymentResults oResult = DriverPaymentResults.Create(this);
		//        //oResult.Number = pReserve["CardNumber"].ToString();
		//        foreach (string sType in aResponse) {
		//            string sField = sType.Split('=')[0].ToUpper();
		//            string sValue = sType.Split('=')[1];
		//            switch (sField) {
		//                case "RESULT":
		//                    oResult.Result = sValue;
		//                    break;
		//                case "PNREF":
		//                    oResult.Reference = sValue;
		//                    break;
		//                case "RESPMSG":
		//                    oResult.Message = sValue;
		//                    break;
		//                case "AUTHCODE":
		//                    oResult.AuthCode = sValue;
		//                    break;
		//                case "IAVS":
		//                    oResult.Avs = sValue;
		//                    break;
		//                case "AVSADDR":
		//                    oResult.AVSAddr = sValue;
		//                    break;
		//                case "AVSZIP":
		//                    oResult.Avszip = sValue;
		//                    // 12/5/11 Added for Carmel
		//                    oResult.AVSResultCode = sValue;
		//                    break;
		//                case "CVV2MATCH":
		//                    oResult.CVV2Match = sValue;
		//                    break;
		//            }
		//        }
		//        if (aResponse.Length == 0) {
		//            oResult.Result = "-1001";
		//            oResult.Message = "Verisign Processor did not respond";
		//        }
		//        oResult.TransType = pTrxType;
		//        oResult.TransDate = DateTime.Now;
		//        oResult.CreditCardProcessor = TaxiPassCommon.Banking.CardProcessors.Verisign.ToString();
		//        oResult.Amount = pAmount;
		//        //oResult.Save();

		//        this.CreditCardProcessor = TaxiPassCommon.Banking.CardProcessors.Verisign.ToString();
		//        if (oResult.Result == "111") {
		//            if (oResult.Message.Contains("has already been captured")) {
		//                //locate previously captured result
		//                foreach (DriverPaymentResults resultRec in this.DriverPaymentResultses) {
		//                    if ((resultRec.TransType == "D" || resultRec.TransType == "S") && resultRec.Result != "111") {
		//                        oResult = resultRec;
		//                        break;
		//                    }
		//                }
		//            } else {
		//                while (true) {
		//                    oResult = DriverPaymentResults.GetSaledPaymentResult(this.PersistenceManager, this.DriverPaymentID);
		//                    if (!oResult.IsNullEntity) {
		//                        break;
		//                    }
		//                }
		//            }
		//        }

		//        if (oResult.Result != "0") {
		//            this.DriverPaymentNotes.Reason = oResult.Message;
		//            this.Failed = true;
		//        } else {
		//            this.Failed = false;
		//            if (this.DriverPaymentNotes.IsNullEntity) {
		//                DriverPaymentNotes.Create(this);
		//            }
		//            this.DriverPaymentNotes.Reason = "";
		//            if (pTrxType == VerisignTrxTypes.Authorization) {
		//                this.AuthDate = DateTime.Now;
		//                if (!this.CardSwiped) {

		//                    // 12/2 New flag for Carmel
		//                    if (this.Affiliate.AffiliateDriverDefaults.IgnoreFailedZipOnCharge.GetValueOrDefault(false)) {
		//                        this.IgnoreCardZIP = true;
		//                    }
		//                    if (!this.IgnoreCardZIP.GetValueOrDefault(false) && !string.IsNullOrEmpty(this.CardZIPCode)) {
		//                        if (("" + oResult.Avszip).Equals("N", StringComparison.CurrentCultureIgnoreCase)) {
		//                            this.Failed = true;
		//                            this.DriverPaymentNotes.Reason = "Invalid Billing Zip Code";
		//                            oResult.Result = "-1";
		//                            oResult.Message = this.DriverPaymentNotes.Reason;
		//                            //oResult.Save();
		//                        } else {
		//                            if (this.Test && !string.IsNullOrEmpty(this.CardZIPCode)) {
		//                                try {
		//                                    int zip = Convert.ToInt32(this.CardZIPCode);
		//                                    if (zip >= 50000) {
		//                                        this.Failed = true;
		//                                        this.DriverPaymentNotes.Reason = "Invalid Billing Zip Code";
		//                                        oResult.Result = "-1";
		//                                        oResult.Message = this.DriverPaymentNotes.Reason;
		//                                        //oResult.Save();
		//                                    }
		//                                } catch {
		//                                }
		//                            }
		//                        }

		//                    }
		//                }
		//            } else {
		//                //this.ChargeApprovalCode = oResult.AuthCode;
		//                if (pTrxType == VerisignTrxTypes.Credit) {
		//                    if (!this.ChargeDate.HasValue) {
		//                        this.ChargeDate = DateTime.Now;
		//                    }
		//                } else {
		//                    this.ChargeDate = DateTime.Now;
		//                }
		//                if (this.Platform != Platforms.Redeemer.ToString()) {
		//                    if (this.Drivers.DriverBankInfos.Count > 0) {
		//                        DriverBankInfo oBank = this.Drivers.DriverBankInfos[0];
		//                        //if (oBank.BankAccountNumber != "" && oBank.BankCode != "" && oBank.MerchantID != null && oBank.MerchantID > 0) {
		//                        if (oBank.BankAccountNumber != "" && oBank.BankCode != "" && this.AffiliateDrivers.TransCardNumber.IsNullOrEmpty()) {
		//                            SetDriverPaid(true);
		//                        }
		//                    }
		//                    if (this.Drivers.MyAffiliate.AutoPayFleet) {
		//                        SetDriverPaid(true);
		//                        this.AffiliateID = this.Drivers.MyAffiliate.AffiliateID;
		//                    }
		//                }
		//            }
		//        }
		//        this.Save();

		//        return oResult;
		//    } catch (Exception ex) {
		//        throw ex;
		//    }

		//}


		//private DriverPaymentResults DoEPayRefund(PersistenceManager pPM, string pOriginalReferenceNo) {

		//    VerisignAccounts oAcct = this.Affiliate.MyVerisignAccount;
		//    string sURL = oAcct.EPayProdURL;
		//    string sSourceKey = oAcct.EPaySourceKey;
		//    string sPin = oAcct.EPayPin;
		//    if (CreditCardUtils.IsTestCard(this.DecryptCardNumber())) {
		//        sURL = oAcct.EPayTestURL;
		//        sSourceKey = oAcct.EPaySourceKeyTest;
		//        sPin = oAcct.EPayPinTest;
		//        this.Test = true;
		//    }

		//    this.VerisignAccountID = oAcct.VerisignAccountID;

		//    TaxiPassCommon.Banking.USAePay oPay = new TaxiPassCommon.Banking.USAePay(sSourceKey, sPin, sURL, oAcct.VerisignTimeout.GetValueOrDefault(0));

		//    string sTranType = VerisignTrxTypes.Credit;
		//    double chargeAmount = Convert.ToDouble(this.TotalCharge);
		//    TaxiPassCommon.com.USAePay.TransactionResponse oResponse = null;

		//    oResponse = oPay.Refund(pOriginalReferenceNo, Math.Abs(chargeAmount)); ;
		//    return ProcessResponse(oResponse, sTranType, Convert.ToDecimal(chargeAmount));

		//}


		//private DriverPaymentResults DoVerisignRefund(string pOriginalReferenceNo) {
		//    //skip Verisign if we do not have a URL to post to
		//    PersistenceManager oPM = this.PersistenceManager;
		//    VerisignAccounts oAcct = this.Drivers.MyAffiliate.MyVerisignAccount;
		//    if (oAcct.VerisignAddress.IsNullOrEmpty()) {
		//        return oPM.GetNullEntity<DriverPaymentResults>();
		//    }

		//    Verisign oVerisign = new Verisign(oAcct, SystemDefaults.GetDefaults(oPM));
		//    this.VerisignAccountID = oAcct.VerisignAccountID;

		//    Verisign.CreditCardData oData = new Verisign.CreditCardData();
		//    oData.TrxType = VerisignTrxTypes.Credit;
		//    oData.Tender = VerisignTenderTypes.CreditCard;
		//    oData.Amount = Math.Abs(this.TotalCharge);
		//    oData.VoucherNo = this.TransNo;

		//    oData.TrxType = VerisignTrxTypes.Credit;
		//    oData.CardNo = this.DecryptCardNumber();
		//    oData.ReferenceNo = pOriginalReferenceNo;

		//    oVerisign.Refund(oData);

		//    DriverPaymentResults result = null;
		//    try {
		//        result = ProcessResponse(oVerisign.Response, oData.TrxType, oData.Amount);
		//    } catch (Exception ex) {
		//        throw ex;
		//    }
		//    //oVerisign.CleanUp();

		//    return result;
		//}

		//private DriverPaymentResults DoVerisignCharge(PersistenceManager pPM, string pCardNo, string pCardInfo) {

		//    //skip Verisign if we do not have a URL to post to
		//    VerisignAccounts oAcct = this.Drivers.MyAffiliate.MyVerisignAccount;
		//    if (oAcct.VerisignAddress.IsNullOrEmpty()) {
		//        return pPM.GetNullEntity<DriverPaymentResults>();
		//    }

		//    Verisign oVerisign = new Verisign(oAcct, SystemDefaults.GetDefaults(pPM));
		//    this.VerisignAccountID = oAcct.VerisignAccountID;

		//    DriverPaymentResults oPayResults = null;

		//    Verisign.CreditCardData oData = new Verisign.CreditCardData();
		//    oData.TrxType = VerisignTrxTypes.Sale;
		//    oData.Tender = VerisignTenderTypes.CreditCard;
		//    oData.Amount = this.TotalCharge;
		//    oData.VoucherNo = this.TransNo;

		//    if (TotalCharge > AuthAmount || AuthDate == null || this.CreditCardProcessor != TaxiPassCommon.Banking.CardProcessors.Verisign.ToString()) {
		//        //New Charge
		//        if (string.IsNullOrEmpty(pCardInfo)) {
		//            oData.Comment1 = "Driver Manual Entry";
		//            oData.CardNo = pCardNo;
		//            string sDate = DecryptCardExpiration();
		//            string sMonth = StringUtils.Left(sDate, 2);
		//            string sYear = StringUtils.Right(sDate, 2);
		//            oData.ExpDate = sMonth + sYear;
		//            oData.CVV = mCVV;

		//            if (!this.IgnoreCardZIP.GetValueOrDefault(true)) {
		//                if (!string.IsNullOrEmpty(this.CardZIPCode)) {
		//                    oData.ZipCode = StringUtils.CleanPhoneNumbers(this.CardZIPCode);
		//                }
		//            }
		//        } else {
		//            oData.Comment1 = "Driver Swipe Entry";
		//            oData.Swipe = pCardInfo;
		//        }
		//        oData.DriverIDs = "DP, " + this.DriverID + ", " + this.AffiliateDriverID.GetValueOrDefault(0);
		//        oVerisign.Sale(oData);
		//    } else {
		//        //Delayed Sale
		//        oData.TrxType = VerisignTrxTypes.DelayedCapture;
		//        oData.CardNo = pCardNo;
		//        foreach (DriverPaymentResults oResult in this.DriverPaymentResultses) {
		//            if (oResult.Result == "0" && oResult.TransType == VerisignTrxTypes.Authorization) {

		//                // Don't use another processor's reference number
		//                if (oResult.CreditCardProcessor == TaxiPassCommon.Banking.CardProcessors.Verisign.ToString()) {
		//                    oData.ReferenceNo = oResult.Reference;
		//                }
		//                break;
		//            }
		//        }
		//        oVerisign.DelayedSale(oData);
		//    }

		//    try {
		//        oPayResults = ProcessResponse(oVerisign.Response, oData.TrxType, oData.Amount);
		//    } catch (Exception ex) {
		//        throw ex;
		//    }
		//    //oVerisign.CleanUp();

		//    return oPayResults;
		//}

		//private DriverPaymentResults DoEPayCharge(PersistenceManager pPM, string pCardNo, string pCardInfo) {
		//    return DoEPayCharge(pPM, pCardNo, pCardInfo, false);
		//}


		// **************************************************************************************************************************
		// Main chargecard routine
		// **************************************************************************************************************************
		//private DriverPaymentResults DoEPayCharge(PersistenceManager pPM, string pCardNo, string pCardInfo, bool pForceNewCharge) {

		//    VerisignAccounts oAcct = this.Affiliate.MyVerisignAccount;
		//    string sURL = oAcct.EPayProdURL;
		//    string sSourceKey = oAcct.EPaySourceKey;
		//    string sPin = oAcct.EPayPin;
		//    if (CreditCardUtils.IsTestCard(pCardNo)) {
		//        sURL = oAcct.EPayTestURL;
		//        sSourceKey = oAcct.EPaySourceKeyTest;
		//        sPin = oAcct.EPayPinTest;
		//        this.Test = true;
		//    }

		//    //skip USAePay if we do not have a URL to post to
		//    if (sURL.IsNullOrEmpty()) {
		//        return pPM.GetNullEntity<DriverPaymentResults>();
		//    }

		//    this.VerisignAccountID = oAcct.VerisignAccountID;

		//    TaxiPassCommon.Banking.USAePay oUSAePay = new TaxiPassCommon.Banking.USAePay(sSourceKey, sPin, sURL, oAcct.VerisignTimeout.GetValueOrDefault(0));

		//    string sTranType = VerisignTrxTypes.Sale;
		//    double chargeAmount = Convert.ToDouble(this.TotalCharge);
		//    TaxiPassCommon.com.USAePay.TransactionResponse oResponse = null;

		//    // 1/5/11 CA If the charge is greater than the auth or no auth, create a new charge
		//    if (TotalCharge > AuthAmount || AuthDate == null) {
		//        pForceNewCharge = true;
		//    }

		//    // chargeAmount could be zero if we have a coupon equal to the total fare
		//    if (chargeAmount > 0) {
		//        if (pForceNewCharge) {
		//            //New Charge
		//            TaxiPassCommon.com.USAePay.CreditCardData cardData = new TaxiPassCommon.com.USAePay.CreditCardData();

		//            string sComment = "Driver Swipe Entry";
		//            if (string.IsNullOrEmpty(pCardInfo)) {
		//                cardData.CardNumber = pCardNo;
		//                string sDate = DecryptCardExpiration();
		//                string sMonth = StringUtils.Left(sDate, 2);
		//                string sYear = StringUtils.Right(sDate, 2);
		//                cardData.CardExpiration = sMonth + sYear;
		//                sComment = "Driver Manual Entry";
		//            } else {
		//                cardData.MagStripe = pCardInfo;
		//            }
		//            cardData.CardCode = mCVV;
		//            cardData.AvsZip = this.CardZIPCode;

		//            string sServiceType = this.Affiliate.ServiceType;
		//            if (string.IsNullOrEmpty(sServiceType)) {
		//                sServiceType = "Taxi";
		//            }
		//            sServiceType += " Service";

		//            oResponse = oUSAePay.ChargeCard(cardData, chargeAmount, sComment, this.TransNo, sServiceType, "DP, " + this.DriverID + ", " + this.AffiliateDriverID.GetValueOrDefault(0));

		//        } else {

		//            //Delayed Sale
		//            string sRefNo = "";
		//            sTranType = VerisignTrxTypes.DelayedCapture;
		//            foreach (DriverPaymentResults oResult in this.DriverPaymentResultses) {
		//                if ((oResult.Result == "0" || oResult.Message.StartsWith("Approved", StringComparison.CurrentCultureIgnoreCase)) && oResult.TransType == VerisignTrxTypes.Authorization) {
		//                    sRefNo = oResult.Reference;
		//                    break;
		//                }
		//            }
		//            oResponse = oUSAePay.ChargeAuth(sRefNo, chargeAmount);
		//        }

		//    } else {
		//        oResponse = CreateDummyUSAePayResponse();
		//    }
		//    return ProcessResponse(oResponse, sTranType, Convert.ToDecimal(chargeAmount));

		//}

		//private DriverPaymentResults DoAprivaPreAuth(PersistenceManager pPM, string pCardNo, string pCardInfo) {
		//    VerisignAccounts oAcct = this.Affiliate.MyVerisignAccount;
		//    string deviceID = oAcct.AprivaDeviceID;
		//    string sURL = oAcct.AprivaProdURL;
		//    string sPin = oAcct.AprivaPin;
		//    int port = 80;

		//    if (CreditCardUtils.IsTestCard(pCardNo)) {
		//        sURL = oAcct.AprivaTestURL;
		//        deviceID = oAcct.AprivaDeviceID;
		//        sPin = oAcct.AprivaPin;
		//    }

		//    //skip Apriva if we do not have a URL to post to
		//    if (sURL.IsNullOrEmpty()) {
		//        return pPM.GetNullEntity<DriverPaymentResults>();
		//    }

		//    if (sURL.Contains(":")) {
		//        string[] val = sURL.Split(':');
		//        sURL = val[0];
		//        port = Convert.ToInt32(val[1]);
		//    }

		//    AprivaRequest request = new AprivaRequest();
		//    string sComment = "Driver Swipe Entry";
		//    if (string.IsNullOrEmpty(pCardInfo)) {
		//        request.CardNumber = pCardNo;
		//        request.CardExpires = DecryptCardExpiration();
		//        sComment = "Driver Manual Entry";
		//    } else {
		//        request.Track2 = pCardInfo;
		//    }
		//    request.CVV = mCVV;
		//    request.AvsZip = this.CardZIPCode;

		//    MyTimeStats.Info.Add(new TimeStats.ElapsedInfo { Type = "Submit Apriva", Elapsed = DateTime.Now.Subtract(MyTimeStats.Start).TotalSeconds });
		//    Apriva oPay = new Apriva(Apriva.SourceType.XMLSSLPOSInterface, deviceID, @"c:\Apriva\Cert.p12", sPin, sURL, port, oAcct.VerisignTimeout.GetValueOrDefault(0));
		//    TaxiPassCommon.com.USAePay.TransactionResponse oResponse = oPay.PreAuthCard(request, AuthAmount, sComment, TransNo, GetServiceType(), DriverPaymentID);
		//    MyTimeStats.Info.Add(new TimeStats.ElapsedInfo { Type = "Apriva Results", Elapsed = DateTime.Now.Subtract(MyTimeStats.Start).TotalSeconds });

		//    return ProcessResponse(oResponse, VerisignTrxTypes.Authorization, AuthAmount);

		//}


		//private DriverPaymentResults DoEPayPreAuth(PersistenceManager pPM, string pCardNo, string pCardInfo) {
		//    VerisignAccounts oAcct = this.Affiliate.MyVerisignAccount;
		//    string sURL = oAcct.EPayProdURL;
		//    string sSourceKey = oAcct.EPaySourceKey;
		//    string sPin = oAcct.EPayPin;
		//    if (TaxiPassCommon.Banking.CreditCardUtils.IsTestCard(pCardNo)) {
		//        sURL = oAcct.EPayTestURL;
		//        sSourceKey = oAcct.EPaySourceKeyTest;
		//        sPin = oAcct.EPayPinTest;
		//    }

		//    //skip USAePay if we do not have a URL to post to
		//    if (sURL.IsNullOrEmpty()) {
		//        return pPM.GetNullEntity<DriverPaymentResults>();
		//    }

		//    this.VerisignAccountID = oAcct.VerisignAccountID;

		//    TaxiPassCommon.Banking.com.USAePay.CreditCardData cardData = new TaxiPassCommon.Banking.com.USAePay.CreditCardData();

		//    string sComment = "Driver Swipe Entry";
		//    if (string.IsNullOrEmpty(pCardInfo)) {
		//        cardData.CardNumber = pCardNo;
		//        string sDate = DecryptCardExpiration();
		//        string sMonth = StringUtils.Left(sDate, 2);
		//        string sYear = StringUtils.Right(sDate, 2);
		//        cardData.CardExpiration = sMonth + sYear;
		//        sComment = "Driver Manual Entry";
		//    } else {
		//        cardData.MagStripe = pCardInfo;
		//    }
		//    cardData.CardCode = mCVV;
		//    cardData.AvsZip = this.CardZIPCode;

		//    double amount = Convert.ToDouble(this.AuthAmount);
		//    MyTimeStats.Info.Add(new TimeStats.ElapsedInfo { Type = "Submit USAePay", Elapsed = DateTime.Now.Subtract(MyTimeStats.Start).TotalSeconds });
		//    TaxiPassCommon.Banking.USAePay oPay = new TaxiPassCommon.Banking.USAePay(sSourceKey, sPin, sURL, oAcct.VerisignTimeout.GetValueOrDefault(0));
		//    MyTimeStats.Info.Add(new TimeStats.ElapsedInfo { Type = "USAePay Results", Elapsed = DateTime.Now.Subtract(MyTimeStats.Start).TotalSeconds });

		//    string sServiceType = GetServiceType();

		//    // Call USAePay's processor
		//    TaxiPassCommon.com.USAePay.TransactionResponse oResponse = oPay.PreAuthCard(cardData, amount, sComment, this.TransNo, sServiceType);
		//    return ProcessResponse(oResponse, VerisignTrxTypes.Authorization, Convert.ToDecimal(amount));

		//}

		//public DateTime? ChargeDateLocalTime() {
		//    if (!ChargeDate.HasValue) {
		//        return ChargeDate;
		//    }
		//    if (this.Drivers.MyAffiliate != null && this.Drivers.MyAffiliate.TimeZone != null) {
		//        TimeZoneConverter oTimeZone = new TimeZoneConverter();
		//        return oTimeZone.FromUniversalTime(this.Drivers.MyAffiliate.TimeZone, ChargeDate.Value);
		//    }
		//    return ChargeDate;
		//}

		//public string PendingMatchDate {
		//    get {
		//        if (this.DriverPaymentsPendingMatches.IsNullEntity) {
		//            return "";
		//        }
		//        return this.DriverPaymentsPendingMatches.Date.ToShortDateString();
		//    }
		//}

		//public override decimal? ScanAmount {
		//    get {
		//        return base.ScanAmount;
		//    }
		//    set {
		//        // hg 2011/06/03 only allow the value to be set once, Jason does not want this to change
		//        if (base.ScanAmount.GetValueOrDefault(0) == 0) {
		//            base.ScanAmount = value;
		//        }
		//    }
		//}

		//private DriverPaymentResults DoVerisignPreAuth(PersistenceManager pPM, string pCardNo, string pCardInfo) {

		//    DriverPaymentResults oPayResults = null;
		//    //skip Verisign if we do not have a URL to post to
		//    VerisignAccounts oAcct = this.Drivers.MyAffiliate.MyVerisignAccount;
		//    if (oAcct.VerisignAddress.IsNullOrEmpty()) {
		//        return pPM.GetNullEntity<DriverPaymentResults>();
		//    }

		//    Verisign oVerisign = new Verisign(this.Drivers.MyAffiliate.MyVerisignAccount, SystemDefaults.GetDefaults(pPM));
		//    Verisign.CreditCardData oData = new Verisign.CreditCardData();

		//    this.VerisignAccountID = oAcct.VerisignAccountID;
		//    oData.TrxType = VerisignTrxTypes.Authorization;
		//    oData.Tender = VerisignTrxTypes.Credit;
		//    oData.VoucherNo = this.TransNo;

		//    if (string.IsNullOrEmpty(pCardInfo)) {
		//        oData.Comment1 = "Driver Manual Entry";
		//        oData.CardNo = pCardNo;
		//        string sDate = DecryptCardExpiration();
		//        string sMonth = StringUtils.Left(sDate, 2);
		//        string sYear = StringUtils.Right(sDate, 2);
		//        oData.ExpDate = sMonth + sYear;
		//        if (!string.IsNullOrEmpty(mCVV)) {
		//            oData.CVV = mCVV;
		//        }
		//    } else {
		//        oData.Comment1 = "Driver Swipe Entry";
		//        oData.Swipe = pCardInfo;
		//    }
		//    oData.Amount = this.AuthAmount;
		//    if (!this.IgnoreCardZIP.GetValueOrDefault(true)) {
		//        if (!string.IsNullOrEmpty(this.CardZIPCode)) {
		//            oData.ZipCode = StringUtils.CleanPhoneNumbers(this.CardZIPCode);
		//        }
		//    }

		//    try {
		//        oVerisign.PreAuth(oData);
		//    } catch (Exception ex) {
		//        throw ex;
		//    }

		//    try {
		//        oPayResults = ProcessResponse(oVerisign.Response, VerisignTrxTypes.Authorization, oData.Amount);
		//    } catch (Exception ex) {
		//        throw ex;
		//    }

		//    //oVerisign.CleanUp();
		//    return oPayResults;

		//}


		//public string AsNacha() {
		//    string nacha = "";


		//    return nacha;
		//}
	}
}
