﻿using Dapper;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using TaxiPassCommon;

namespace CabRideEngineDapper {

    [Serializable]
    public class usp_AccountRef_Flattened {

        public string AccountEmail { get; set; }
        public string AccountHomePage { get; set; }
        public string AccountPhone { get; set; }
        public string CalculateFare { get; set; }
        public string DoNotSendNewRideTextMsg { get; set; }
        public string DropOffRequired { get; set; }
        public string ExclusiveDispatch { get; set; }
        public string MinPickUpTime { get; set; }
        public string MinServicePercent { get; set; }
        public string NoCreditCardOnFile { get; set; }
        public string PassengerAndroid { get; set; }
        public string PassengeriPhone { get; set; }
        public string SkipCreditCard { get; set; }
        public string DriverChangeFare { get; set; }
        public string DisplayExtraFees { get; set; }
        public string AllowFutureBookings { get; set; }
        public string GuaranteedResTime { get; set; }

        public int GuaranteedResTimeAsInt {
            get {
                return Convert.ToInt32("0" + GuaranteedResTime);
            }
        }

        public bool AllowFutureBookingsAsBoolean {
            get {
                if (AllowFutureBookings.IsNullOrEmpty()) {
                    return false;
                }
                if (AllowFutureBookings == "1") {
                    return true;
                } else if (AllowFutureBookings == "0") {
                    return false;
                }
                return Convert.ToBoolean(AllowFutureBookings);
            }
        }

        public bool DisplayExtraFeesAsBoolean {
            get {
                if (DisplayExtraFees.IsNullOrEmpty()) {
                    return false;
                }
                return Convert.ToBoolean(DisplayExtraFees);
            }
        }

        public bool DriverChangeFareAsBoolean {
            get {
                if (DriverChangeFare.IsNullOrEmpty()) {
                    return true;
                }
                return Convert.ToBoolean(DriverChangeFare);
            }
        }

        public bool CalculateFareAsBoolean {
            get {
                if (CalculateFare.IsNullOrEmpty()) {
                    return false;
                }
                return Convert.ToBoolean(CalculateFare);
            }
        }

        public static usp_AccountRef_Flattened Execute(SqlConnection pConn, long pAccountID) {
            return pConn.Query<usp_AccountRef_Flattened>("usp_AccountRef_Flattened", new { AccountID = pAccountID }, commandType: CommandType.StoredProcedure).FirstOrDefault();
        }
    }
}
