using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Runtime.Serialization;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

namespace CabRideEngine {
 
  #region ReservationDriverLogDataTable
  /// <summary>
  /// The generated <see cref="T:IdeaBlade.Persistence.EntityTable"/> class  
  /// containing all <see cref="ReservationDriverLog"/> objects.
  /// </summary>
  /// <remarks>
  /// ****************************************************
  /// <para>** DO NOT MODIFY THIS CLASS - AutoGenerated Code</para>
  /// <para>** Any changes made to this code will be lost</para>
  /// <para>** the next time this code is regenerated.</para>
  /// <para>**</para>
  /// <para>** Generated from:   EntityBaseclass.template</para>
  /// <para>** Last modified:    2012-04-18T12:17:43.9030153-07:00</para>
  /// <para>** Template version: 3.110127</para>
  /// ****************************************************
  /// </remarks>
  [BindingBrowsable(false)]
  [Serializable]
  public partial class ReservationDriverLogDataTable : BaseEntityDataTable, 
     IdeaBlade.Persistence.Rdb.IRdbTable {
  
    /// <summary>
    /// Initializes a new instance of the ReservationDriverLogDataTable class with no arguments.
    /// </summary>
    public ReservationDriverLogDataTable() {}

    /// <summary>
    /// Initializes a new instance of the ReservationDriverLogDataTable class 
    /// with the <see cref="System.Runtime.Serialization.SerializationInfo"/> 
    /// and the <see cref="System.Runtime.Serialization.StreamingContext"/>.
    /// </summary>
    /// <param name="pInfo">The data needed to serialize or deserialize an object.</param>
    /// <param name="pContext">The source and destination of a given serialized stream.</param>
    protected ReservationDriverLogDataTable(SerializationInfo pInfo, StreamingContext pContext) :
      base(pInfo, pContext) {}

    /// <summary>
    /// Creates a new instance of the <see cref="ReservationDriverLogDataRow"/>.
    /// Constructs a row from the builder.
    /// <para>** For internal use only. **</para>
    /// </summary>
    /// <param name="pRowBuilder"></param>
    /// <returns>The <see cref="ReservationDriverLogDataRow"/>.</returns>
    protected override DataRow NewRowFromBuilder(DataRowBuilder pRowBuilder) {
      return new ReservationDriverLog(pRowBuilder);
    }

    /// <summary>Returns the <see cref="System.Type"/> of <see cref="ReservationDriverLog"/>.</summary>
    protected override Type GetRowType() {
      return typeof(ReservationDriverLog);
    }
    
    /// <summary>
    /// Gets the name of the <see cref="T:IdeaBlade.Rdb.RdbKey"/> or 
    /// <see cref="T:IdeaBlade.Persistence.WS.WsKey"/> defined in <b>IdeaBlade.ibconfig</b>. 
    /// </summary>
    /// <remarks>For this <see cref="ReservationDriverLog"/>, the DataSourceKeyName="default".</remarks>
    public override String DataSourceKeyName {
      get { return @"default"; }
    }

    #region Column properties
    //**************************************
    //* Column properties
    //**************************************
    /// <summary>Gets the ReservationDriverLogID <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn ReservationDriverLogIDColumn {
      get {
        if (mReservationDriverLogIDColumn!=null) return mReservationDriverLogIDColumn;
        mReservationDriverLogIDColumn = GetColumn("ReservationDriverLogID", true);
        return mReservationDriverLogIDColumn;
      }
    }
    private DataColumn mReservationDriverLogIDColumn;
    
    /// <summary>Gets the ReservationDriverID <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn ReservationDriverIDColumn {
      get {
        if (mReservationDriverIDColumn!=null) return mReservationDriverIDColumn;
        mReservationDriverIDColumn = GetColumn("ReservationDriverID", true);
        return mReservationDriverIDColumn;
      }
    }
    private DataColumn mReservationDriverIDColumn;
    
    /// <summary>Gets the Action <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn ActionColumn {
      get {
        if (mActionColumn!=null) return mActionColumn;
        mActionColumn = GetColumn("Action", true);
        return mActionColumn;
      }
    }
    private DataColumn mActionColumn;
    
    /// <summary>Gets the Comment <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn CommentColumn {
      get {
        if (mCommentColumn!=null) return mCommentColumn;
        mCommentColumn = GetColumn("Comment", true);
        return mCommentColumn;
      }
    }
    private DataColumn mCommentColumn;
    
    /// <summary>Gets the CreatedDate <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn CreatedDateColumn {
      get {
        if (mCreatedDateColumn!=null) return mCreatedDateColumn;
        mCreatedDateColumn = GetColumn("CreatedDate", true);
        return mCreatedDateColumn;
      }
    }
    private DataColumn mCreatedDateColumn;
    
    /// <summary>Gets the CreatedBy <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn CreatedByColumn {
      get {
        if (mCreatedByColumn!=null) return mCreatedByColumn;
        mCreatedByColumn = GetColumn("CreatedBy", true);
        return mCreatedByColumn;
      }
    }
    private DataColumn mCreatedByColumn;
    
    #endregion    

    /// <summary>Creates the schema mapping information for this ReservationDriverLogDataTable.</summary>
    /// <returns>
    /// The <see cref="T:IdeaBlade.Persistence.TableMappingInfo"/> for this ReservationDriverLogDataTable.
    /// </returns>
    protected override TableMappingInfo CreateTableMappingInfo() {
      RdbTableMappingInfo mappingInfo = new RdbTableMappingInfo(this);
      mappingInfo.SourceOwnerName = "dbo"; 
      mappingInfo.SourceTableName = "ReservationDriverLog"; 
      mappingInfo.ConcurrencyColumnName = ""; 
      // ColumnSelectionOption: selectAllColumns
      mappingInfo.SourceColumnNames = "*"; 
   
      DataColumnMappingCollection columnMappings = mappingInfo.TableMapping.ColumnMappings;
      columnMappings.Clear();
      columnMappings.Add("ReservationDriverLogID", "ReservationDriverLogID");
      columnMappings.Add("ReservationDriverID", "ReservationDriverID");
      columnMappings.Add("Action", "Action");
      columnMappings.Add("Comment", "Comment");
      columnMappings.Add("CreatedDate", "CreatedDate");
      columnMappings.Add("CreatedBy", "CreatedBy");
      return mappingInfo;
    }
  
    /// <summary>** For internal use only. **</summary>
    protected override void UpdateTableMappingInfo() {
      base.UpdateTableMappingInfo();
    }

    /// <summary>
    /// Initializes ReservationDriverLog <see cref="System.Data.DataColumn"/> properties.
    /// </summary>
    public override void InitColumnProperties() {
      base.InitColumnProperties();

      ReservationDriverLogIDColumn.Caption = "ReservationDriverLogID";
      ReservationDriverIDColumn.Caption = "ReservationDriverID";
      ActionColumn.Caption = "Action";
      CommentColumn.Caption = "Comment";
      CreatedDateColumn.Caption = "CreatedDate";
      CreatedByColumn.Caption = "CreatedBy";
    }
  }
  #endregion
  
  #region ReservationDriverLogDataRow
  /// <summary>
  /// The generated base class for <see cref="ReservationDriverLog"/>.
  /// </summary>
  /// <remarks>
  /// <para>
  /// It represents a row of persistable data in the <see cref="ReservationDriverLogDataTable"/>
  /// and derives from <see cref="T:IdeaBlade.Persistence.Entity"/>.
	/// </para>
  /// ****************************************************
  /// <para>** DO NOT MODIFY THIS CLASS - AutoGenerated Code</para>
  /// <para>** Any changes made to this code will be lost</para>
  /// <para>** the next time this code is regenerated.</para>
  /// <para>**</para>
  /// <para>** Generated from:   EntityBaseclass.template</para>
  /// <para>** Last modified:    2012-04-18T12:17:43.9030153-07:00</para>
  /// <para>** Template version: 3.110127</para>
  /// ****************************************************
  /// </remarks>  
  [Serializable]
  public abstract partial class ReservationDriverLogDataRow : BaseEntity, 
     IdeaBlade.Persistence.Rdb.IRdbEntity {
  
    /// <summary>
    /// Initializes a new instance of the ReservationDriverLogDataRow.
    /// Constructs a row from the builder.
    /// <para>** For internal use only. **</para>
    /// </summary>
    /// <param name="pRowBuilder"></param>
    protected ReservationDriverLogDataRow(DataRowBuilder pRowBuilder)
      : base(pRowBuilder) {
    }

    /// <summary>Gets the <see cref="ReservationDriverLogDataTable"/>.</summary>
    [BindingBrowsable(false)]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public new ReservationDriverLogDataTable TypedTable {
      get { return (ReservationDriverLogDataTable) base.Table; }
    }

    /// <summary>Returns the <see cref="System.Type"/> of <see cref="ReservationDriverLogDataTable"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public static Type GetTableType() {
      return typeof(ReservationDriverLogDataTable);
    }

    /// <summary>
    /// Gets the SQL 'where' clause used to restrict which 
    /// database records are accessible via this entity. 
    /// </summary>
    /// <remarks>
    /// The default property value is defined in the Current Class 
    /// Properties tab in the <b>IdeaBlade DevForce Object Mapping Tool</b>.
    /// <para>
    /// The 'where' clause will be used, in addition to any other 
    /// query-specific criteria, in every retrieval of entities of this type.
    /// </para>
    /// </remarks>
    [BindingBrowsable(false)]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public virtual string SqlWhereClause {
      get { return ""; }
    }
    
    /// <summary>
    /// Determines whether to use the 'distinct' keyword when 
    /// generating the SQL select clause for objects of this type.
    /// </summary>
    /// <remarks>Defaults to false, but may be overridden in subclasses.</remarks>
    [BindingBrowsable(false)]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public virtual bool ForceSqlDistinct {
      get { return false; }
    }
    

    #region Enums
    //**************************************
    //* Enums
    //**************************************
    #endregion
	
    #region Relation properties
    //**************************************
    //* Relation properties
    //**************************************
	  /// <summary>Gets or sets the ReservationDrivers relation property.</summary>
    [RelationProperty("ReservationDrivers_ReservationDriverLog", 
    QueryDirection.ParentQuery)]
    public virtual ReservationDrivers ReservationDrivers {
      get { 
        ReservationDrivers result_;
        if (GetInterceptor<ReservationDrivers>("ReservationDrivers", GetReservationDriversImpl, out result_)) return result_;
        return GetReservationDriversImpl();
      }
      set { 
          if (!SetInterceptor<ReservationDrivers>("ReservationDrivers", value, SetReservationDriversImpl)) {
					  SetReservationDriversImpl(value);
          }
      }
    }
    private ReservationDrivers GetReservationDriversImpl() {
      return GetParent<ReservationDrivers>(EntityRelations.ReservationDrivers_ReservationDriverLog, this.PersistenceManager.DefaultQueryStrategy);
    }
    private void SetReservationDriversImpl(ReservationDrivers value) {
      if (value == null) {
        SetNull(this.ReservationDriverIDColumn);
      } else {
        SetColumnValue(this.ReservationDriverIDColumn, value, value.ReservationDriverIDColumn);
      }
      OnPropertyChanged(new PropertyChangedEventArgs("ReservationDrivers"));
    }
    
    #endregion
    
    #region EntityColumn definitions
    //**************************************
    //* Static EntityColumn definitions
    //**************************************
    /// <summary>The ReservationDriverLogID <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn ReservationDriverLogIDEntityColumn =
      new EntityColumn(typeof(ReservationDriverLog), "ReservationDriverLogID", typeof(System.Int64), false, true, true, DataSourceAccessType.ReadWrite);      
    /// <summary>The ReservationDriverID <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn ReservationDriverIDEntityColumn =
      new EntityColumn(typeof(ReservationDriverLog), "ReservationDriverID", typeof(System.Int64), false, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The Action <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn ActionEntityColumn =
      new EntityColumn(typeof(ReservationDriverLog), "Action", typeof(System.String), false, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The Comment <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn CommentEntityColumn =
      new EntityColumn(typeof(ReservationDriverLog), "Comment", typeof(System.String), true, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The CreatedDate <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn CreatedDateEntityColumn =
      new EntityColumn(typeof(ReservationDriverLog), "CreatedDate", typeof(System.DateTime), false, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The CreatedBy <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn CreatedByEntityColumn =
      new EntityColumn(typeof(ReservationDriverLog), "CreatedBy", typeof(System.String), false, false, false, DataSourceAccessType.ReadWrite);      
    #endregion
    
    #region Properties
    //**************************************
    //* ReservationDriverLogID methods
    //**************************************
    /// <summary>Gets the ReservationDriverLogID <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn ReservationDriverLogIDColumn {
      get { return TypedTable.ReservationDriverLogIDColumn; }
    }

    /// <summary>Gets the ReservationDriverLogID.</summary>
    [DBDataType(typeof(System.Int64))]
    public virtual System.Int64 ReservationDriverLogID {
      get { 
        System.Int64 result_;
        if (GetInterceptor<System.Int64>("ReservationDriverLogID", GetReservationDriverLogIDImpl, out result_)) return result_;
        return GetReservationDriverLogIDImpl();
      }
    }
    private System.Int64 GetReservationDriverLogIDImpl() {
      return (System.Int64) GetColumnValue(ReservationDriverLogIDColumn, typeof(System.Int64), false); 
    }
    
    //**************************************
    //* ReservationDriverID methods
    //**************************************
    /// <summary>Gets the ReservationDriverID <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn ReservationDriverIDColumn {
      get { return TypedTable.ReservationDriverIDColumn; }
    }

    /// <summary>Gets or sets the ReservationDriverID.</summary>
    [DBDataType(typeof(System.Int64))]
    public virtual System.Int64 ReservationDriverID {
      get { 
        System.Int64 result_;
        if (GetInterceptor<System.Int64>("ReservationDriverID", GetReservationDriverIDImpl, out result_)) return result_;
        return GetReservationDriverIDImpl();
      }
      set { 
          if (!SetInterceptor<System.Int64>("ReservationDriverID", value, SetReservationDriverIDImpl)) {
            SetReservationDriverIDImpl(value);
          }  
      }    
    }
    private System.Int64 GetReservationDriverIDImpl() {
      return (System.Int64) GetColumnValue(ReservationDriverIDColumn, typeof(System.Int64), false); 
    }
    private void SetReservationDriverIDImpl(System.Int64 value) {
      SetColumnValue(ReservationDriverIDColumn, value);
    }
    
    //**************************************
    //* Action methods
    //**************************************
    /// <summary>Gets the Action <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn ActionColumn {
      get { return TypedTable.ActionColumn; }
    }

    /// <summary>Gets or sets the Action.</summary>
    [MaxTextLength(100)]
    [DBDataType(typeof(System.String))]
    public virtual System.String Action {
      get { 
        System.String result_;
        if (GetInterceptor<System.String>("Action", GetActionImpl, out result_)) return result_;
        return GetActionImpl();
      }
      set { 
          if (!SetInterceptor<System.String>("Action", value, SetActionImpl)) {
            SetActionImpl(value);
          }  
      }    
    }
    private System.String GetActionImpl() {
      return (System.String) GetColumnValue(ActionColumn, typeof(System.String), false); 
    }
    private void SetActionImpl(System.String value) {
      SetColumnValue(ActionColumn, value);
    }
    
    //**************************************
    //* Comment methods
    //**************************************
    /// <summary>Gets the Comment <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn CommentColumn {
      get { return TypedTable.CommentColumn; }
    }

    /// <summary>Gets or sets the Comment.</summary>
    [MaxTextLength(2000)]
    [DBDataType(typeof(System.String))]
    public virtual System.String Comment {
      get { 
        System.String result_;
        if (GetInterceptor<System.String>("Comment", GetCommentImpl, out result_)) return result_;
        return GetCommentImpl();
      }
      set { 
          if (!SetInterceptor<System.String>("Comment", value, SetCommentImpl)) {
            SetCommentImpl(value);
          }  
      }    
    }
    private System.String GetCommentImpl() {
      return (System.String) GetColumnValue(CommentColumn, typeof(System.String), true); 
    }
    private void SetCommentImpl(System.String value) {
      SetColumnValue(CommentColumn, value);
    }
    
    //**************************************
    //* CreatedDate methods
    //**************************************
    /// <summary>Gets the CreatedDate <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn CreatedDateColumn {
      get { return TypedTable.CreatedDateColumn; }
    }

    /// <summary>Gets or sets the CreatedDate.</summary>
    [DBDataType(typeof(System.DateTime))]
    public virtual System.DateTime CreatedDate {
      get { 
        System.DateTime result_;
        if (GetInterceptor<System.DateTime>("CreatedDate", GetCreatedDateImpl, out result_)) return result_;
        return GetCreatedDateImpl();
      }
      set { 
          if (!SetInterceptor<System.DateTime>("CreatedDate", value, SetCreatedDateImpl)) {
            SetCreatedDateImpl(value);
          }  
      }    
    }
    private System.DateTime GetCreatedDateImpl() {
      return (System.DateTime) GetColumnValue(CreatedDateColumn, typeof(System.DateTime), false); 
    }
    private void SetCreatedDateImpl(System.DateTime value) {
      SetColumnValue(CreatedDateColumn, value);
    }
    
    //**************************************
    //* CreatedBy methods
    //**************************************
    /// <summary>Gets the CreatedBy <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn CreatedByColumn {
      get { return TypedTable.CreatedByColumn; }
    }

    /// <summary>Gets or sets the CreatedBy.</summary>
    [MaxTextLength(100)]
    [DBDataType(typeof(System.String))]
    public virtual System.String CreatedBy {
      get { 
        System.String result_;
        if (GetInterceptor<System.String>("CreatedBy", GetCreatedByImpl, out result_)) return result_;
        return GetCreatedByImpl();
      }
      set { 
          if (!SetInterceptor<System.String>("CreatedBy", value, SetCreatedByImpl)) {
            SetCreatedByImpl(value);
          }  
      }    
    }
    private System.String GetCreatedByImpl() {
      return (System.String) GetColumnValue(CreatedByColumn, typeof(System.String), false); 
    }
    private void SetCreatedByImpl(System.String value) {
      SetColumnValue(CreatedByColumn, value);
    }
    
    #endregion

  }
  #endregion

  #region EntityPropertyDescriptors.ReservationDriverLogPropertyDescriptor
  public static partial class EntityPropertyDescriptors {

    /// <summary>
    /// Gets the ReservationDriverLogPropertyDescriptor for <see cref="ReservationDriverLog"/>.
    /// </summary>
    public static ReservationDriverLogPropertyDescriptor ReservationDriverLog {
      get { return ReservationDriverLogPropertyDescriptor.Instance; }
    }
    
    /// <summary>
    /// The generated class containing <see cref="ReservationDriverLog"/> PropertyDescriptors.
    /// </summary>
    public partial class ReservationDriverLogPropertyDescriptor : BaseEntityPropertyDescriptor {
	
			/// <summary>
      /// Initializes a new instance of the ReservationDriverLogPropertyDescriptor class. 
      /// </summary>
      /// <param name="pParentDescriptor">The parent <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/></param>
      /// <param name="pPropertyPath">The type of the property value</param>
			public ReservationDriverLogPropertyDescriptor(AdaptedPropertyDescriptor pParentDescriptor, String pPropertyPath)
        : base(pParentDescriptor, pPropertyPath) {}
		
      /// <summary>
      /// Initializes a new instance of the ReservationDriverLogPropertyDescriptor class. 
      /// </summary>
      /// <param name="pComponentType">The type of object to which the property is bound</param>
      protected ReservationDriverLogPropertyDescriptor(Type pComponentType)
        : base(pComponentType) {}

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for ReservationDriverLogID.</summary>
      public AdaptedPropertyDescriptor ReservationDriverLogID {
        get { return Get("ReservationDriverLogID"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for ReservationDriverID.</summary>
      public AdaptedPropertyDescriptor ReservationDriverID {
        get { return Get("ReservationDriverID"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Action.</summary>
      public AdaptedPropertyDescriptor Action {
        get { return Get("Action"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Comment.</summary>
      public AdaptedPropertyDescriptor Comment {
        get { return Get("Comment"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for CreatedDate.</summary>
      public AdaptedPropertyDescriptor CreatedDate {
        get { return Get("CreatedDate"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for CreatedBy.</summary>
      public AdaptedPropertyDescriptor CreatedBy {
        get { return Get("CreatedBy"); }
      }

      /// <summary>Gets the <see cref="ReservationDriversPropertyDescriptor"/> for ReservationDrivers.</summary>
      public ReservationDriversPropertyDescriptor ReservationDrivers {
        get { return Get<ReservationDriversPropertyDescriptor>("ReservationDrivers"); }
      }
						
     internal new static ReservationDriverLogPropertyDescriptor Instance = new ReservationDriverLogPropertyDescriptor(typeof(ReservationDriverLog));
    }
  }
  #endregion
  
}
