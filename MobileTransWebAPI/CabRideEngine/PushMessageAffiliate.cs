﻿using IdeaBlade.Persistence;
using IdeaBlade.Persistence.Rdb;
using System.Data;
using System.Linq;

namespace CabRideEngine {
	public static class PushMessageAffiliate {

		private enum FieldNames {
			AffiliateID
		}

		private const string PUSH_MESSAGE_AFFILIATE = "PushMessageAffiliate";
		public static CabDriverRef CreatePushMessageAffiliate(PushMessage pPushMessage) {
			CabDriverRef rec = pPushMessage.PersistenceManager.CreateEntity<CabDriverRef>();
			rec.SourceID = pPushMessage.PushMessageID;
			rec.SourceTable = PUSH_MESSAGE_AFFILIATE;

			rec.AddToManager();
			return rec;
		}

		public static CabDriverRef GetAffiliate(PushMessage pPushMessage, long pAffiliateID) {
			RdbQuery qry = new RdbQuery(typeof(CabDriverRef), CabDriverRef.SourceTableEntityColumn, EntityQueryOp.EQ, PUSH_MESSAGE_AFFILIATE);
			qry.AddClause(CabDriverRef.SourceIDEntityColumn, EntityQueryOp.EQ, pPushMessage.PushMessageID);
			qry.AddClause(CabDriverRef.ReferenceKeyEntityColumn, EntityQueryOp.EQ, FieldNames.AffiliateID.ToString());
			qry.AddClause(CabDriverRef.ValueStringEntityColumn, EntityQueryOp.EQ, pAffiliateID.ToString());
			return pPushMessage.PersistenceManager.GetEntity<CabDriverRef>(qry);
		}

		public static EntityList<CabDriverRef> GetChangedAffiliateList(PersistenceManager pPM) {
			EntityList<CabDriverRef> list = pPM.GetEntities<CabDriverRef>(DataRowState.Added | DataRowState.Modified | DataRowState.Deleted);

			list = new EntityList<CabDriverRef>((from p in list
												 where p.SourceTable.Equals(PUSH_MESSAGE_AFFILIATE)
												 select p).ToArray());

			return list;
		}

		public static CabDriverRef GetOrCreateAffiliateRec(PushMessage pPushMessage, long pAffiliateID) {
			CabDriverRef rec = GetAffiliate(pPushMessage, pAffiliateID);

			if (rec.IsNullEntity) {
				rec = CabDriverRef.Create(pPushMessage.PersistenceManager, PUSH_MESSAGE_AFFILIATE, pPushMessage.PushMessageID, FieldNames.AffiliateID.ToString(), pAffiliateID.ToString());
				rec.Save();
			}

			return rec;
		}
	}
}
