﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

using CabRideEngine.Utils;
using TaxiPassCommon;
using TaxiPassCommon.Banking;

using Csla.Validation;

namespace CabRideEngine {
    /// <summary>
    /// The developer class representing the TaxiPassCards business object class.
    /// </summary>
    /// <remarks>
    /// <para>Place all application-specific business logic in this class.</para>
    /// <para>
    /// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
    /// and will not overwrite changes.
    /// </para>
    /// <para>
    /// This class inherits from the generated class, its companion in the pair of classes 
    /// defining a business object. Properties in the generated class may be overridden in 
    /// order to change the behavior or add business logic.
    /// </para>
    /// <para>
    /// This class is the final class in the inheritance chain for this business object. 
    /// It should not be inherited.
    /// </para>
    /// </remarks>
    [Serializable]
    public sealed partial class TaxiPassCards : TaxiPassCardsDataRow {

        #region Constructors -- DO NOT MODIFY

        // Do not create constructors for this class
        // Developers cannot create instances of this class with the "new" keyword
        // You should write a static Create method instead; see the "Suggested Customization" region
        // Please review the documentation to learn about entity creation and inheritance

        // This private constructor prevents accidental instance creation via 'new';
        // it is also required if the class is visible from a web service.
        // DO NOT REMOVE **
        private TaxiPassCards() : this(null) { }

        /// <summary>
        /// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
        /// </summary>
        /// <param name="pRowBuilder"></param>
        public TaxiPassCards(DataRowBuilder pRowBuilder)
            : base(pRowBuilder) {
        }

        #endregion

        #region Suggested Customizations

        //    // Use this factory method template to create new instances of this class
        //    public static TaxiPassCards Create(PersistenceManager pManager,
        //      ... your creation parameters here ...) { 
        //
        //      TaxiPassCards aTaxiPassCards = pManager.CreateEntity<TaxiPassCards>();
        //
        //      // if this object type requires a unique id and you have implemented
        //      // the IIdGenerator interface implement the following line
        //      pManager.GenerateId(aTaxiPassCards, // add id column here //);
        //
        //      // Add custom code here
        //
        //      aTaxiPassCards.AddToManager();
        //      return aTaxiPassCards;
        //    }

        //    // Implement this method if you want your object to sort in a specific order
        //    public override int CompareTo(Object pObject) {
        //    }

        //    // Implement this method to customize the null object version of this class
        //    protected override void UpdateNullEntity() {
        //    }

        #endregion

        // Add additional logic to your business object here...
		public override string TaxiPassCardNo {
			get {
				return base.TaxiPassCardNo;
			}
			set {
				base.TaxiPassCardNo = value;
				base.CardNumber = CryptoFns.MD5HashUTF16ToString(base.TaxiPassCardNo);
			}
		}

        #region Standard Entity logic to create/audit/validate the entity
        public static TaxiPassCards Create(PersistenceManager pPM) {
            TaxiPassCards rec = null;

            try {
                rec = pPM.CreateEntity<TaxiPassCards>();
                rec.Country = Countries.GetDefaultCountry(pPM).Country;
                // Using the IdeaBlade Id Generation technique
                pPM.GenerateId(rec, TaxiPassCards.TaxiPassCardIDEntityColumn);
                rec.AddToManager();

            } catch (Exception ex) {
                throw ex;
            }
            return rec;
        }

        protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
            if (IsDeserializing) {
                return;
            }
            base.OnColumnChanging(pArgs);
            DoAudit(pArgs);
        }

        private void DoAudit(DataColumnChangeEventArgs pArgs) {
            if (!this.PersistenceManager.IsClient) {
                return;
            }
            if (this.RowState == DataRowState.Added) {
                return;
            }
            if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
                AuditRecord = TaxiPassCardsAudit.Create(this.PersistenceManager, this);
            }
        }

        [BindingBrowsable(false)]
        public override string EntityInstanceDisplayName {
            get {
                return this.TaxiPassCardNo;
            }
        }

        protected override void AddRules(RuleList pList) {
            PropertyRequiredRule.AddToList(pList, TaxiPassCardNoEntityColumn.ColumnName);
            pList.Add(IsTaxiPassCardNoUnique, TaxiPassCardNoEntityColumn.ColumnName);
        }

        private bool IsTaxiPassCardNoUnique(object pTarget, RuleArgs e) {
            TaxiPassCards rec = pTarget as TaxiPassCards;
            if (string.IsNullOrEmpty(rec.TaxiPassCardNo)) {
                return false;
            }
            RdbQuery qry = new RdbQuery(typeof(TaxiPassCards), TaxiPassCards.TaxiPassCardNoEntityColumn, EntityQueryOp.EQ, rec.TaxiPassCardNo);
            qry.AddClause(TaxiPassCards.TaxiPassCardIDEntityColumn, EntityQueryOp.NE, rec.TaxiPassCardID);
            EntityList<TaxiPassCards> oList = this.PersistenceManager.GetEntities<TaxiPassCards>(qry);
            if (oList.Count > 0) {
                e.Description = "TaxiPass Card Number already exists: " + rec.TaxiPassCardNo;
                return false;
            }
            return true;
        }

        #endregion

        #region Retrieve Records

        public static EntityList<TaxiPassCards> GetAll(PersistenceManager pPM) {
            EntityList<TaxiPassCards> oList = pPM.GetEntities<TaxiPassCards>();
            oList.ApplySort(TaxiPassCards.TaxiPassCardNoEntityColumn.ColumnName, System.ComponentModel.ListSortDirection.Ascending, false);
            return oList;
        }

        public static TaxiPassCards GetCardNo(PersistenceManager pPM, string pCardNo) {
            RdbQuery qry = new RdbQuery(typeof(TaxiPassCards), TaxiPassCardNoEntityColumn, EntityQueryOp.EQ, pCardNo);
            return pPM.GetEntity<TaxiPassCards>(qry, QueryStrategy.DataSourceThenCache);
        }

        #endregion

    }

    #region EntityPropertyDescriptors.TaxiPassCardsPropertyDescriptor
    ////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
    ////    For example: 
    ////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
    //public static partial class EntityPropertyDescriptors {
    //	public partial class TaxiPassCardsPropertyDescriptor : BaseEntityPropertyDescriptor {
    //    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
    //    public AdaptedPropertyDescriptor Age {
    //      get { return this.Get("Age"); }
    //    }
    //  }
    //} 
    #endregion

}