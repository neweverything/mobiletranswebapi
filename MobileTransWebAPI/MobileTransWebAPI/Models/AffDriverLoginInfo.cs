namespace MobileTransWebAPI.Models {
    public partial class AffDriverLoginInfo {

        public long DriverID { get; set; }
        public string UserID { get; set; }
        public string Phone { get; set; }
        public string PIN { get; set; }
        public string Vehicle { get; set; }
        public string RequestTime { get; set; }
        public bool PhoneNumberOnly { get; set; }
        public string SimSerialNumber { get; set; }
        public string DevicePhoneNumber { get; set; }
        public string SubscriberId { get; set; }
        public string HardwareManufacturer { get; set; }
        public string HardwareModel { get; set; }
        public string HardwareOS { get; set; }
        public string Carrier { get; set; }
        public string AppName { get; set; }
        public bool InfoUpdate { get; set; }

        public string WorkCity { get; set; }
        public string VersionNo { get; set; }
        public bool DisplayLogout { get; set; }
        public string WidgetKey { get; set; }
        public string AskTolls { get; set; }
    }
}