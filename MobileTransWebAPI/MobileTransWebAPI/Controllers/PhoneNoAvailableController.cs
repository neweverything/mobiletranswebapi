﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using System.Web.Script.Serialization;

using IdeaBlade.Persistence;

using TaxiPassCommon;

using CabRideEngine;
using CabRideEngine.Utils;

using MobileTransWebAPI.Models;

namespace MobileTransWebAPI.Controllers
{
    public class PhoneNoAvailableController : ApiController
    {
		public PhoneNoAvailResult GetIsPhoneNoAvailable(string pRequest) {
			PhoneNoAvailResult result = new PhoneNoAvailResult();

			JavaScriptSerializer js = new JavaScriptSerializer();

			pRequest = pRequest.DecryptIceKey();

			WebApiApplication.LogIt("IsPhoneNoAvailable", pRequest);

			PhoneNoAvailRequest info = js.Deserialize<PhoneNoAvailRequest>(pRequest);

			try {
				PersistenceManager oPM = WebApiApplication.UserPM;
				if (oPM == null) {
					result.ErrorMsg = "Could not connect to database";
					return result;
				}

				SystemDefaults defaults = SystemDefaults.GetDefaults(oPM);

				Drivers driver = Drivers.GetDriver(oPM, info.DriverID);
				if (driver.BoostPhone != info.CurrentNumber) {
					result.ErrorMsg = "Invalid Access";
					return result;
				}

				Drivers newPhone = Drivers.GetDriverByPhone(oPM, info.NewNumber);
				if (!newPhone.IsNullEntity && driver.IsNullEntity) {
					driver = newPhone;
				}

				if (!newPhone.IsNullEntity) {
					if (newPhone.DriverID != driver.DriverID) {
						result.ErrorMsg = "Phone number is assigned to another phone, if this is your number then log out and login with new number";
						return result;
					}
				}


				SystemDefaultsDict oDict = SystemDefaultsDict.GetByKey(oPM, "Black Listed Area Codes", "BlackListedAreaCodes", false);
				if (!oDict.IsNullEntity) {
					List<string> areaCodes = oDict.ValueString.Split('|').ToList();
					if (areaCodes.Contains(info.NewNumber.Left(3))) {
						result.ErrorMsg = "Phone number is not valid";
						return result;
					}
				}

				AffiliateDrivers affDriver = oPM.GetNullEntity<AffiliateDrivers>();
				if (driver.AffiliateID.GetValueOrDefault(0) > 0) {
					affDriver = AffiliateDrivers.GetDriverByCallInCell(oPM, info.NewNumber);
					if (!affDriver.IsNullEntity) {
						if (driver.AffiliateID > 0 && affDriver.AffiliateID != driver.AffiliateID) {
							result.ErrorMsg = "Phone number is assigned to another phone, if this is your number call TaxiPass to resolve";
							return result;
						}
					}
				}

				if (driver.AffiliateID.GetValueOrDefault(0) > 0 || (affDriver.IsNullEntity && !driver.IsNullEntity)) {
					driver.BoostPhone = info.NewNumber;
					driver.Cell = info.NewNumber;
					try {
						if (driver.RowState != System.Data.DataRowState.Unchanged) {
							driver.Save();
							result.PhoneNoUpdated = true;
						}
					} catch (Exception) {
					}
				}

				result.DriverID = driver.DriverID;
				result.IsAvailable = true;
			} catch (Exception ex) {
				result.ErrorMsg = ex.Message;
			}

			return result;
		}
    }
}
