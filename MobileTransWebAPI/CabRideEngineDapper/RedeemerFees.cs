﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Dapper;
using TaxiPassCommon;

using CabRideEngineDapper.Utils;

namespace CabRideEngineDapper {
	public partial class RedeemerFees {

		public static IEnumerable<RedeemerFees> GetFees(TaxiPassRedeemerAccounts pTaxiPassRedeemerAccounts) {
			string sql = "SELECT * FROM RedeemerFees WHERE TaxiPassRedeemerAccountID = @TaxiPassRedeemerAccountID";
			using (var conn = SqlHelper.OpenSqlConnection()) {
				return conn.Query<RedeemerFees>(sql, new { TaxiPassRedeemerAccountID = pTaxiPassRedeemerAccounts.TaxiPassRedeemerAccountID });
			}
		}

		public static RedeemerFees GetRedemeerFee(TaxiPassRedeemerAccounts pRedeemerAccount, string pPlatform) {
			string sql = @"SELECT * FROM RedeemerFees
							WHERE TaxiPassRedeemerAccountID = @TaxiPassRedeemerAccountID
								  AND Platform = @Platform";
			using (var conn = SqlHelper.OpenSqlConnection()) {
				return conn.Get<RedeemerFees>(sql, new { TaxiPassRedeemerAccountID = pRedeemerAccount.TaxiPassRedeemerAccountID, Platform = pPlatform });
			}
		}
	}
}
