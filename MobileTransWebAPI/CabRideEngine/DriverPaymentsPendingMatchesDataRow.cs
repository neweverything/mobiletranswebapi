using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Runtime.Serialization;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

namespace CabRideEngine {
 
  #region DriverPaymentsPendingMatchesDataTable
  /// <summary>
  /// The generated <see cref="T:IdeaBlade.Persistence.EntityTable"/> class  
  /// containing all <see cref="DriverPaymentsPendingMatches"/> objects.
  /// </summary>
  /// <remarks>
  /// ****************************************************
  /// <para>** DO NOT MODIFY THIS CLASS - AutoGenerated Code</para>
  /// <para>** Any changes made to this code will be lost</para>
  /// <para>** the next time this code is regenerated.</para>
  /// <para>**</para>
  /// <para>** Generated from:   EntityBaseclass.template</para>
  /// <para>** Last modified:    2011-06-29T13:00:28.3777579-07:00</para>
  /// <para>** Template version: 3.110127</para>
  /// ****************************************************
  /// </remarks>
  [BindingBrowsable(false)]
  [Serializable]
  public partial class DriverPaymentsPendingMatchesDataTable : BaseEntityDataTable, 
     IdeaBlade.Persistence.Rdb.IRdbTable {
  
    /// <summary>
    /// Initializes a new instance of the DriverPaymentsPendingMatchesDataTable class with no arguments.
    /// </summary>
    public DriverPaymentsPendingMatchesDataTable() {}

    /// <summary>
    /// Initializes a new instance of the DriverPaymentsPendingMatchesDataTable class 
    /// with the <see cref="System.Runtime.Serialization.SerializationInfo"/> 
    /// and the <see cref="System.Runtime.Serialization.StreamingContext"/>.
    /// </summary>
    /// <param name="pInfo">The data needed to serialize or deserialize an object.</param>
    /// <param name="pContext">The source and destination of a given serialized stream.</param>
    protected DriverPaymentsPendingMatchesDataTable(SerializationInfo pInfo, StreamingContext pContext) :
      base(pInfo, pContext) {}

    /// <summary>
    /// Creates a new instance of the <see cref="DriverPaymentsPendingMatchesDataRow"/>.
    /// Constructs a row from the builder.
    /// <para>** For internal use only. **</para>
    /// </summary>
    /// <param name="pRowBuilder"></param>
    /// <returns>The <see cref="DriverPaymentsPendingMatchesDataRow"/>.</returns>
    protected override DataRow NewRowFromBuilder(DataRowBuilder pRowBuilder) {
      return new DriverPaymentsPendingMatches(pRowBuilder);
    }

    /// <summary>Returns the <see cref="System.Type"/> of <see cref="DriverPaymentsPendingMatches"/>.</summary>
    protected override Type GetRowType() {
      return typeof(DriverPaymentsPendingMatches);
    }
    
    /// <summary>
    /// Gets the name of the <see cref="T:IdeaBlade.Rdb.RdbKey"/> or 
    /// <see cref="T:IdeaBlade.Persistence.WS.WsKey"/> defined in <b>IdeaBlade.ibconfig</b>. 
    /// </summary>
    /// <remarks>For this <see cref="DriverPaymentsPendingMatches"/>, the DataSourceKeyName="default".</remarks>
    public override String DataSourceKeyName {
      get { return @"default"; }
    }

    #region Column properties
    //**************************************
    //* Column properties
    //**************************************
    /// <summary>Gets the DriverPaymentsPendingMatchID <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn DriverPaymentsPendingMatchIDColumn {
      get {
        if (mDriverPaymentsPendingMatchIDColumn!=null) return mDriverPaymentsPendingMatchIDColumn;
        mDriverPaymentsPendingMatchIDColumn = GetColumn("DriverPaymentsPendingMatchID", true);
        return mDriverPaymentsPendingMatchIDColumn;
      }
    }
    private DataColumn mDriverPaymentsPendingMatchIDColumn;
    
    /// <summary>Gets the DriverPaymentID <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn DriverPaymentIDColumn {
      get {
        if (mDriverPaymentIDColumn!=null) return mDriverPaymentIDColumn;
        mDriverPaymentIDColumn = GetColumn("DriverPaymentID", true);
        return mDriverPaymentIDColumn;
      }
    }
    private DataColumn mDriverPaymentIDColumn;
    
    /// <summary>Gets the TaxiPassRedeemerID <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn TaxiPassRedeemerIDColumn {
      get {
        if (mTaxiPassRedeemerIDColumn!=null) return mTaxiPassRedeemerIDColumn;
        mTaxiPassRedeemerIDColumn = GetColumn("TaxiPassRedeemerID", true);
        return mTaxiPassRedeemerIDColumn;
      }
    }
    private DataColumn mTaxiPassRedeemerIDColumn;
    
    /// <summary>Gets the Date <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn DateColumn {
      get {
        if (mDateColumn!=null) return mDateColumn;
        mDateColumn = GetColumn("Date", true);
        return mDateColumn;
      }
    }
    private DataColumn mDateColumn;
    
    /// <summary>Gets the PayCardOwner <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn PayCardOwnerColumn {
      get {
        if (mPayCardOwnerColumn!=null) return mPayCardOwnerColumn;
        mPayCardOwnerColumn = GetColumn("PayCardOwner", true);
        return mPayCardOwnerColumn;
      }
    }
    private DataColumn mPayCardOwnerColumn;
    
    #endregion    

    /// <summary>Creates the schema mapping information for this DriverPaymentsPendingMatchesDataTable.</summary>
    /// <returns>
    /// The <see cref="T:IdeaBlade.Persistence.TableMappingInfo"/> for this DriverPaymentsPendingMatchesDataTable.
    /// </returns>
    protected override TableMappingInfo CreateTableMappingInfo() {
      RdbTableMappingInfo mappingInfo = new RdbTableMappingInfo(this);
      mappingInfo.SourceOwnerName = "dbo"; 
      mappingInfo.SourceTableName = "DriverPaymentsPendingMatches"; 
      mappingInfo.ConcurrencyColumnName = ""; 
      // ColumnSelectionOption: selectAllColumns
      mappingInfo.SourceColumnNames = "*"; 
   
      DataColumnMappingCollection columnMappings = mappingInfo.TableMapping.ColumnMappings;
      columnMappings.Clear();
      columnMappings.Add("DriverPaymentsPendingMatchID", "DriverPaymentsPendingMatchID");
      columnMappings.Add("DriverPaymentID", "DriverPaymentID");
      columnMappings.Add("TaxiPassRedeemerID", "TaxiPassRedeemerID");
      columnMappings.Add("Date", "Date");
      columnMappings.Add("PayCardOwner", "PayCardOwner");
      return mappingInfo;
    }
  
    /// <summary>** For internal use only. **</summary>
    protected override void UpdateTableMappingInfo() {
      base.UpdateTableMappingInfo();
    }

    /// <summary>
    /// Initializes DriverPaymentsPendingMatches <see cref="System.Data.DataColumn"/> properties.
    /// </summary>
    public override void InitColumnProperties() {
      base.InitColumnProperties();

      DriverPaymentsPendingMatchIDColumn.Caption = "DriverPaymentsPendingMatchID";
      DriverPaymentIDColumn.Caption = "DriverPaymentID";
      TaxiPassRedeemerIDColumn.Caption = "TaxiPassRedeemerID";
      DateColumn.Caption = "Date";
      PayCardOwnerColumn.Caption = "PayCardOwner";
    }
  }
  #endregion
  
  #region DriverPaymentsPendingMatchesDataRow
  /// <summary>
  /// The generated base class for <see cref="DriverPaymentsPendingMatches"/>.
  /// </summary>
  /// <remarks>
  /// <para>
  /// It represents a row of persistable data in the <see cref="DriverPaymentsPendingMatchesDataTable"/>
  /// and derives from <see cref="T:IdeaBlade.Persistence.Entity"/>.
	/// </para>
  /// ****************************************************
  /// <para>** DO NOT MODIFY THIS CLASS - AutoGenerated Code</para>
  /// <para>** Any changes made to this code will be lost</para>
  /// <para>** the next time this code is regenerated.</para>
  /// <para>**</para>
  /// <para>** Generated from:   EntityBaseclass.template</para>
  /// <para>** Last modified:    2011-06-29T13:00:28.3777579-07:00</para>
  /// <para>** Template version: 3.110127</para>
  /// ****************************************************
  /// </remarks>  
  [Serializable]
  public abstract partial class DriverPaymentsPendingMatchesDataRow : BaseEntity, 
     IdeaBlade.Persistence.Rdb.IRdbEntity {
  
    /// <summary>
    /// Initializes a new instance of the DriverPaymentsPendingMatchesDataRow.
    /// Constructs a row from the builder.
    /// <para>** For internal use only. **</para>
    /// </summary>
    /// <param name="pRowBuilder"></param>
    protected DriverPaymentsPendingMatchesDataRow(DataRowBuilder pRowBuilder)
      : base(pRowBuilder) {
    }

    /// <summary>Gets the <see cref="DriverPaymentsPendingMatchesDataTable"/>.</summary>
    [BindingBrowsable(false)]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public new DriverPaymentsPendingMatchesDataTable TypedTable {
      get { return (DriverPaymentsPendingMatchesDataTable) base.Table; }
    }

    /// <summary>Returns the <see cref="System.Type"/> of <see cref="DriverPaymentsPendingMatchesDataTable"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public static Type GetTableType() {
      return typeof(DriverPaymentsPendingMatchesDataTable);
    }

    /// <summary>
    /// Gets the SQL 'where' clause used to restrict which 
    /// database records are accessible via this entity. 
    /// </summary>
    /// <remarks>
    /// The default property value is defined in the Current Class 
    /// Properties tab in the <b>IdeaBlade DevForce Object Mapping Tool</b>.
    /// <para>
    /// The 'where' clause will be used, in addition to any other 
    /// query-specific criteria, in every retrieval of entities of this type.
    /// </para>
    /// </remarks>
    [BindingBrowsable(false)]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public virtual string SqlWhereClause {
      get { return ""; }
    }
    
    /// <summary>
    /// Determines whether to use the 'distinct' keyword when 
    /// generating the SQL select clause for objects of this type.
    /// </summary>
    /// <remarks>Defaults to false, but may be overridden in subclasses.</remarks>
    [BindingBrowsable(false)]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public virtual bool ForceSqlDistinct {
      get { return false; }
    }
    

    #region Enums
    //**************************************
    //* Enums
    //**************************************
    #endregion
	
    #region Relation properties
    //**************************************
    //* Relation properties
    //**************************************
	  /// <summary>Gets or sets the DriverPayments relation property.</summary>
    [RelationProperty("DriverPayments_DriverPaymentsPendingMatches", 
    QueryDirection.ParentQuery)]
    public virtual DriverPayments DriverPayments {
      get { 
        DriverPayments result_;
        if (GetInterceptor<DriverPayments>("DriverPayments", GetDriverPaymentsImpl, out result_)) return result_;
        return GetDriverPaymentsImpl();
      }
      set { 
          if (!SetInterceptor<DriverPayments>("DriverPayments", value, SetDriverPaymentsImpl)) {
					  SetDriverPaymentsImpl(value);
          }
      }
    }
    private DriverPayments GetDriverPaymentsImpl() {
      return GetParent<DriverPayments>(EntityRelations.DriverPayments_DriverPaymentsPendingMatches, this.PersistenceManager.DefaultQueryStrategy);
    }
    private void SetDriverPaymentsImpl(DriverPayments value) {
      if (value == null) {
        SetNull(this.DriverPaymentIDColumn);
      } else {
        SetColumnValue(this.DriverPaymentIDColumn, value, value.DriverPaymentIDColumn);
      }
      OnPropertyChanged(new PropertyChangedEventArgs("DriverPayments"));
    }
    
	  /// <summary>Gets or sets the TaxiPassRedeemers relation property.</summary>
    [RelationProperty("TaxiPassRedeemers_DriverPaymentsPendingMatches", 
    QueryDirection.ParentQuery)]
    public virtual TaxiPassRedeemers TaxiPassRedeemers {
      get { 
        TaxiPassRedeemers result_;
        if (GetInterceptor<TaxiPassRedeemers>("TaxiPassRedeemers", GetTaxiPassRedeemersImpl, out result_)) return result_;
        return GetTaxiPassRedeemersImpl();
      }
      set { 
          if (!SetInterceptor<TaxiPassRedeemers>("TaxiPassRedeemers", value, SetTaxiPassRedeemersImpl)) {
					  SetTaxiPassRedeemersImpl(value);
          }
      }
    }
    private TaxiPassRedeemers GetTaxiPassRedeemersImpl() {
      return GetParent<TaxiPassRedeemers>(EntityRelations.TaxiPassRedeemers_DriverPaymentsPendingMatches, this.PersistenceManager.DefaultQueryStrategy);
    }
    private void SetTaxiPassRedeemersImpl(TaxiPassRedeemers value) {
      if (value == null) {
        SetNull(this.TaxiPassRedeemerIDColumn);
      } else {
        SetColumnValue(this.TaxiPassRedeemerIDColumn, value, value.TaxiPassRedeemerIDColumn);
      }
      OnPropertyChanged(new PropertyChangedEventArgs("TaxiPassRedeemers"));
    }
    
    #endregion
    
    #region EntityColumn definitions
    //**************************************
    //* Static EntityColumn definitions
    //**************************************
    /// <summary>The DriverPaymentsPendingMatchID <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn DriverPaymentsPendingMatchIDEntityColumn =
      new EntityColumn(typeof(DriverPaymentsPendingMatches), "DriverPaymentsPendingMatchID", typeof(System.Int64), false, true, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The DriverPaymentID <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn DriverPaymentIDEntityColumn =
      new EntityColumn(typeof(DriverPaymentsPendingMatches), "DriverPaymentID", typeof(System.Int64), false, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The TaxiPassRedeemerID <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn TaxiPassRedeemerIDEntityColumn =
      new EntityColumn(typeof(DriverPaymentsPendingMatches), "TaxiPassRedeemerID", typeof(System.Int64), false, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The Date <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn DateEntityColumn =
      new EntityColumn(typeof(DriverPaymentsPendingMatches), "Date", typeof(System.DateTime), false, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The PayCardOwner <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn PayCardOwnerEntityColumn =
      new EntityColumn(typeof(DriverPaymentsPendingMatches), "PayCardOwner", typeof(System.Int64), true, false, false, DataSourceAccessType.ReadWrite);      
    #endregion
    
    #region Properties
    //**************************************
    //* DriverPaymentsPendingMatchID methods
    //**************************************
    /// <summary>Gets the DriverPaymentsPendingMatchID <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn DriverPaymentsPendingMatchIDColumn {
      get { return TypedTable.DriverPaymentsPendingMatchIDColumn; }
    }

    /// <summary>Gets the DriverPaymentsPendingMatchID.</summary>
    [DBDataType(typeof(System.Int64))]
    public virtual System.Int64 DriverPaymentsPendingMatchID {
      get { 
        System.Int64 result_;
        if (GetInterceptor<System.Int64>("DriverPaymentsPendingMatchID", GetDriverPaymentsPendingMatchIDImpl, out result_)) return result_;
        return GetDriverPaymentsPendingMatchIDImpl();
      }
    }
    private System.Int64 GetDriverPaymentsPendingMatchIDImpl() {
      return (System.Int64) GetColumnValue(DriverPaymentsPendingMatchIDColumn, typeof(System.Int64), false); 
    }
    
    //**************************************
    //* DriverPaymentID methods
    //**************************************
    /// <summary>Gets the DriverPaymentID <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn DriverPaymentIDColumn {
      get { return TypedTable.DriverPaymentIDColumn; }
    }

    /// <summary>Gets or sets the DriverPaymentID.</summary>
    [DBDataType(typeof(System.Int64))]
    public virtual System.Int64 DriverPaymentID {
      get { 
        System.Int64 result_;
        if (GetInterceptor<System.Int64>("DriverPaymentID", GetDriverPaymentIDImpl, out result_)) return result_;
        return GetDriverPaymentIDImpl();
      }
      set { 
          if (!SetInterceptor<System.Int64>("DriverPaymentID", value, SetDriverPaymentIDImpl)) {
            SetDriverPaymentIDImpl(value);
          }  
      }    
    }
    private System.Int64 GetDriverPaymentIDImpl() {
      return (System.Int64) GetColumnValue(DriverPaymentIDColumn, typeof(System.Int64), false); 
    }
    private void SetDriverPaymentIDImpl(System.Int64 value) {
      SetColumnValue(DriverPaymentIDColumn, value);
    }
    
    //**************************************
    //* TaxiPassRedeemerID methods
    //**************************************
    /// <summary>Gets the TaxiPassRedeemerID <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn TaxiPassRedeemerIDColumn {
      get { return TypedTable.TaxiPassRedeemerIDColumn; }
    }

    /// <summary>Gets or sets the TaxiPassRedeemerID.</summary>
    [DBDataType(typeof(System.Int64))]
    public virtual System.Int64 TaxiPassRedeemerID {
      get { 
        System.Int64 result_;
        if (GetInterceptor<System.Int64>("TaxiPassRedeemerID", GetTaxiPassRedeemerIDImpl, out result_)) return result_;
        return GetTaxiPassRedeemerIDImpl();
      }
      set { 
          if (!SetInterceptor<System.Int64>("TaxiPassRedeemerID", value, SetTaxiPassRedeemerIDImpl)) {
            SetTaxiPassRedeemerIDImpl(value);
          }  
      }    
    }
    private System.Int64 GetTaxiPassRedeemerIDImpl() {
      return (System.Int64) GetColumnValue(TaxiPassRedeemerIDColumn, typeof(System.Int64), false); 
    }
    private void SetTaxiPassRedeemerIDImpl(System.Int64 value) {
      SetColumnValue(TaxiPassRedeemerIDColumn, value);
    }
    
    //**************************************
    //* Date methods
    //**************************************
    /// <summary>Gets the Date <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn DateColumn {
      get { return TypedTable.DateColumn; }
    }

    /// <summary>Gets or sets the Date.</summary>
    [DBDataType(typeof(System.DateTime))]
    public virtual System.DateTime Date {
      get { 
        System.DateTime result_;
        if (GetInterceptor<System.DateTime>("Date", GetDateImpl, out result_)) return result_;
        return GetDateImpl();
      }
      set { 
          if (!SetInterceptor<System.DateTime>("Date", value, SetDateImpl)) {
            SetDateImpl(value);
          }  
      }    
    }
    private System.DateTime GetDateImpl() {
      return (System.DateTime) GetColumnValue(DateColumn, typeof(System.DateTime), false); 
    }
    private void SetDateImpl(System.DateTime value) {
      SetColumnValue(DateColumn, value);
    }
    
    //**************************************
    //* PayCardOwner methods
    //**************************************
    /// <summary>Gets the PayCardOwner <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn PayCardOwnerColumn {
      get { return TypedTable.PayCardOwnerColumn; }
    }

    /// <summary>Gets or sets the PayCardOwner.</summary>
    [DBDataType(typeof(System.Int64))]
    public virtual Nullable<System.Int64> PayCardOwner {
      get { 
        Nullable<System.Int64> result_;
        if (GetInterceptor<Nullable<System.Int64>>("PayCardOwner", GetPayCardOwnerImpl, out result_)) return result_;
        return GetPayCardOwnerImpl();
      }
      set { 
          if (!SetInterceptor<Nullable<System.Int64>>("PayCardOwner", value, SetPayCardOwnerImpl)) {
            SetPayCardOwnerImpl(value);
          }
      }    
    }
    private Nullable<System.Int64> GetPayCardOwnerImpl() {
      return (Nullable<System.Int64>) GetColumnValue(PayCardOwnerColumn, typeof(System.Int64), true); 
    }
    private void SetPayCardOwnerImpl(Nullable<System.Int64> value) {
      SetColumnValue(PayCardOwnerColumn, value);
    }
    
    #endregion

  }
  #endregion

  #region EntityPropertyDescriptors.DriverPaymentsPendingMatchesPropertyDescriptor
  public static partial class EntityPropertyDescriptors {

    /// <summary>
    /// Gets the DriverPaymentsPendingMatchesPropertyDescriptor for <see cref="DriverPaymentsPendingMatches"/>.
    /// </summary>
    public static DriverPaymentsPendingMatchesPropertyDescriptor DriverPaymentsPendingMatches {
      get { return DriverPaymentsPendingMatchesPropertyDescriptor.Instance; }
    }
    
    /// <summary>
    /// The generated class containing <see cref="DriverPaymentsPendingMatches"/> PropertyDescriptors.
    /// </summary>
    public partial class DriverPaymentsPendingMatchesPropertyDescriptor : BaseEntityPropertyDescriptor {
	
			/// <summary>
      /// Initializes a new instance of the DriverPaymentsPendingMatchesPropertyDescriptor class. 
      /// </summary>
      /// <param name="pParentDescriptor">The parent <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/></param>
      /// <param name="pPropertyPath">The type of the property value</param>
			public DriverPaymentsPendingMatchesPropertyDescriptor(AdaptedPropertyDescriptor pParentDescriptor, String pPropertyPath)
        : base(pParentDescriptor, pPropertyPath) {}
		
      /// <summary>
      /// Initializes a new instance of the DriverPaymentsPendingMatchesPropertyDescriptor class. 
      /// </summary>
      /// <param name="pComponentType">The type of object to which the property is bound</param>
      protected DriverPaymentsPendingMatchesPropertyDescriptor(Type pComponentType)
        : base(pComponentType) {}

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for DriverPaymentsPendingMatchID.</summary>
      public AdaptedPropertyDescriptor DriverPaymentsPendingMatchID {
        get { return Get("DriverPaymentsPendingMatchID"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for DriverPaymentID.</summary>
      public AdaptedPropertyDescriptor DriverPaymentID {
        get { return Get("DriverPaymentID"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for TaxiPassRedeemerID.</summary>
      public AdaptedPropertyDescriptor TaxiPassRedeemerID {
        get { return Get("TaxiPassRedeemerID"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Date.</summary>
      public AdaptedPropertyDescriptor Date {
        get { return Get("Date"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for PayCardOwner.</summary>
      public AdaptedPropertyDescriptor PayCardOwner {
        get { return Get("PayCardOwner"); }
      }

      /// <summary>Gets the <see cref="DriverPaymentsPropertyDescriptor"/> for DriverPayments.</summary>
      public DriverPaymentsPropertyDescriptor DriverPayments {
        get { return Get<DriverPaymentsPropertyDescriptor>("DriverPayments"); }
      }
						
      /// <summary>Gets the <see cref="TaxiPassRedeemersPropertyDescriptor"/> for TaxiPassRedeemers.</summary>
      public TaxiPassRedeemersPropertyDescriptor TaxiPassRedeemers {
        get { return Get<TaxiPassRedeemersPropertyDescriptor>("TaxiPassRedeemers"); }
      }
						
     internal new static DriverPaymentsPendingMatchesPropertyDescriptor Instance = new DriverPaymentsPendingMatchesPropertyDescriptor(typeof(DriverPaymentsPendingMatches));
    }
  }
  #endregion
  
}
