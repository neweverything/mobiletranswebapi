using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using IdeaBlade.Util;

namespace CabRideEngine {
    public class Routing_Types {

        private string mCode;
        private string mDescription;

        private bool mIsNullEntity = false;

        private static BindableList<Routing_Types> msEntities;
        private static Routing_Types msNullEntity;

        public static string PickUp {
            get {
                return "P";
            }
        }

        public static string DropOff {
            get {
                return "D";
            }
        }

        public static string Stop {
            get {
                return "S";
            }
        }


        /// <summary>Class Constructor (Private)</summary>
        static Routing_Types() {

            // Create the null entity
            msNullEntity = new Routing_Types("", "");
            msNullEntity.mIsNullEntity = true;

            // Populate list of Routing Types.
            msEntities = new BindableList<Routing_Types>();
            msEntities.Add(new Routing_Types(PickUp, "Pick Up"));
            msEntities.Add(new Routing_Types(DropOff, "Drop Off"));
            msEntities.Add(new Routing_Types(Stop, "Stop"));
        }

        /// <summary>Constructor (Private)</summary>
        private Routing_Types(string pCode, string pDescription) {
            mCode = pCode;
            mDescription = pDescription;
        }

        /// <summary>Get the Routing Types null entity.</summary>
        public static Routing_Types NullEntity {
            get {
                return msNullEntity;
            }
        }

        /// <summary>Get the list of all Routing Types.</summary>
        public static BindableList<Routing_Types> GetAll() {
            return msEntities;
        }

        /// <summary>Get the list of all Routing Types.</summary>
        public static Routing_Types GetById(string pId) {
            if (String.IsNullOrEmpty(pId))
                return NullEntity;
            foreach (Routing_Types oRoutingType in msEntities) {
                if (oRoutingType.Code == pId)
                    return oRoutingType;
            }
            return NullEntity;
        }

        /// <summary>Return true if this is the null entity</summary>
        [BindingBrowsable(false)]
        public bool IsNullEntity {
            get {
                return mIsNullEntity;
            }
        }

        /// <summary>Get the Routing Type</summary>
        public string Code {
            get {
                return mCode;
            }
        }

        public string Description {
            get {
                return mDescription;
            }
        }

        //public static DataTable GetRouting_Types() {
        //    DataTable oTable = new DataTable("Routing_Types");
        //    oTable.Columns.Add("Code", typeof(string));
        //    oTable.Columns.Add("Name", typeof(string));

        //    Datar oRow = oTable.NewRow();
        //    oRow["Code"] = "";
        //    oRow["Name"] = "";
        //    oTable.Rows.Add(oRow);

        //    oRow = oTable.NewRow();
        //    oRow["Code"] = "P";
        //    oRow["Name"] = "Pick Up";
        //    oTable.Rows.Add(oRow);

        //    oRow = oTable.NewRow();
        //    oRow["Code"] = "D";
        //    oRow["Name"] = "Drop Off";
        //    oTable.Rows.Add(oRow);

        //    return oTable;
        //}

    }
}
