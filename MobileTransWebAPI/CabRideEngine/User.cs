﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using CabRideEngine.Utils;

using IdeaBlade.Persistence;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

using TaxiPassCommon;

namespace CabRideEngine {
    [Serializable]
    public sealed class User : UserDataRow {

        #region Constructors -- DO NOT MODIFY
        // Do not create constructors for this class
        // Developers cannot create instances of this class with the "new" keyword
        // You should write a static Create method instead; see the "Suggested Customization" region
        // Please review the documentation to learn about entity creation and inheritance

        // This private constructor prevents accidental instance creation via 'new'
        private User()
            : this(null) {
        }

        // Typed DataSet constructor (needed by framework) - DO NOT REMOVE
        public User(DataRowBuilder pRowBuilder)
            : base(pRowBuilder) {
        }

        #endregion

        #region Suggested Customizations

        //    // Use this factory method template to create new instances of this class
        //    public static User Create(PersistenceManager pManager,
        //      ... your creation parameters here ...) { 
        //
        //      User aUser = (User) pManager.CreateEntity(typeof(User));
        //
        //      // if this object type requires a unique id and you have implemented
        //      // the IIdGenerator interface implement the following line
        //      pManager.GenerateId(aUser, // add id column here //);
        //
        //      // Add custom code here
        //
        //      aUser.AddToManager();
        //      return aUser;
        //    }

        //    // Implement this method if you want your object to sort in a specific order
        //    public override int CompareTo(Object pObject) {
        //    }

        //    // Implement this method to customize the null object version of this class
        //    protected override void UpdateNullEntity() {
        //    }

        #endregion

        // Add additional logic to your business object here...
        public static User Create(PersistenceManager pPM, string sUser, string sPassword) {
            byte[] bBuffer = new byte[10];
            (new Random()).NextBytes(bBuffer);
            return Create(pPM, sUser, sPassword, "Pepper");
        }

        public static User Create(PersistenceManager pPM, string sUser, string sPassword, string sSalt) {
            User oUser = null;

            try {
                // Creates the State but it is not yet accessible to the application
                oUser = (User)pPM.CreateEntity(typeof(User));

                // Using the IdeaBlade Id Generation technique
                //pPM.GenerateId(emp, Employee.IdEntityColumn);

                // CRITICAL: must tell emp to add itself to its PM
                oUser.AddToManager();

                // Add custom code here
                oUser.UserName = sUser;
                oUser.Salt = sSalt;
                oUser.UserPassword = HashPassword(sPassword + sSalt);

            } catch (Exception ex) {
                throw ex;
            }
            return oUser;
        }

       
        public static User Create(PersistenceManager pPM) {
            User oUser = null;

            try {
                oUser = (User)pPM.CreateEntity<User>();
                //pPM.GenerateId(oUser, User.UserIdEntityColumn);
                //oUser["UserId"] = SystemDefaults.GetNextKey(SystemDefaults.UserTableEntityColumn.ColumnName);
                oUser.Salt = StringUtils.GenerateSalt();
                oUser.CreatedBy = System.Threading.Thread.CurrentPrincipal.Identity.Name;
                oUser.CreatedDate = DateTime.Now;

                oUser.AddToManager();

                return oUser;
            } catch (Exception ex) {
                throw ex;
            }
        }

        /// <summary>
        /// Changes the user supplied password to the hash encoded password
        /// </summary>
        public override string UserPassword {
            get {
                return base.UserPassword;
            }
            set {
                if (value.Length > 3) {
                    base.UserPassword = HashPassword(value + Salt);
                } else {
                    EntityValidationInfo oResult = new EntityValidationInfo(false, "Password must be at least 4 characters");
                    throw new Exception(oResult.Message);

                }
            }
        }

        public override EntityValidationInfo Validate(EntityValidationContext pContext) {
            EntityValidationInfo oResult = base.Validate(pContext);
            if (oResult.IsValid) {
                oResult = ValidateUser(UserName);
                if (oResult.IsValid) {
                    oResult = ValidateUserPassword(UserPassword);
                }
            }
            return oResult;
        }

        public static User GetUser(PersistenceManager pManager, String pUserName) {
            EntityQuery oQuery = new EntityQuery(typeof(User), User.UserNameEntityColumn, EntityQueryOp.EQ, pUserName);
            oQuery.AddClause(User.IsLockedEntityColumn, EntityQueryOp.EQ, false);
            User oUser = (User)pManager.GetEntity(oQuery);
            return oUser;
        }

        public static string HashPassword(String sPassword) {
            return CryptoFns.MD5HashUTF16ToString(sPassword);
        }

        //public static string[] GetUserRoles(string pUserName) {
        //    EntityQuery oQuery = new EntityQuery(typeof(User), User.UserNameEntityColumn, EntityQueryOp.EQ, pUserName);
        //    User oUser = (User) .GetEntity(oQuery);
        //    Array oRoles = new ArraySegment<string>();
        //    return oRoles;
        //}

        public EntityValidationInfo ValidateUser(string pNewUserName) {
            if (pNewUserName.Length < 3) {
                return new EntityValidationInfo(false, "UserName must be at least 3 characters long");
            }

            RdbQuery oQry = new RdbQuery(typeof(User), UserNameEntityColumn, EntityQueryOp.EQ, pNewUserName);
            oQry.AddClause(UserIdEntityColumn, EntityQueryOp.NE, UserId);
            EntityList<User> oUsers = PersistenceManager.GetEntities<User>(oQry);
            if (oUsers.Count > 0) {
                return new EntityValidationInfo(false, "UserName must be unique");
            }
            return new EntityValidationInfo(true, "");
        }

        public EntityValidationInfo ValidateUserPassword(string pNewPassword) {
            if (pNewPassword.Length < 4) {
                return new EntityValidationInfo(false, "UserPassword must be at least 4 characters long");
            }

            return new EntityValidationInfo(true, "");
        }

        public static bool ChangePassword(PersistenceManager pPM, string pUser, string pOldPassword, string pNewPassword) {

            RdbQuery oQry = new RdbQuery(typeof(User), UserNameEntityColumn, EntityQueryOp.EQ, pUser);

            User oUser = pPM.GetEntity<User>(oQry);
            if (oUser.IsNullEntity) {
                EntityValidationInfo oResult = new EntityValidationInfo(false, "Connection Error or you have been removed from the system!");
                throw new Exception(oResult.Message);
            }
            if (oUser.UserPassword != HashPassword(pOldPassword + oUser.Salt)) {
                EntityValidationInfo oResult = new EntityValidationInfo(false, "Old Password is incorrect or user has been removed from the system!");
                throw new Exception(oResult.Message);
            }

            oUser.UserPassword = pNewPassword;
            oUser.ModifiedBy = pUser;
            oUser.ModifiedDate = DateTime.Now;
            EntityList<User> changedUsers = pPM.GetEntities<User>(DataRowState.Modified);
            pPM.SaveChanges(changedUsers);

            return true;
        }

        public string FullName {
            get {
                return FirstName + " " + LastName;
            }
        }

        public string GetPager() {
            if (string.IsNullOrEmpty(this.PagerNumber) || this.PagerTypes.IsNullEntity) {
                return "";
            }
            string sPager = this.PagerNumber.Trim();
            if (!this.PagerTypes.Url.StartsWith("@")) {
                sPager += "@";
            }
            sPager += PagerTypes.Url;
            return sPager;
        }

    }

}
