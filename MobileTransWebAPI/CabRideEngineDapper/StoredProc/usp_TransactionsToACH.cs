﻿using CabRideEngineDapper.Enums;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using TaxiPassCommon;
using TaxiPassCommon.Banking;

namespace CabRideEngineDapper {

    public partial class usp_TransactionsToACH : BaseEntity {
        public static TimeZoneConverter tzConvert = new TimeZoneConverter();


        public long DriverPaymentID { get; set; }
        public long? AffiliateDriverID { get; set; }
        public long AffiliateID { get; set; }
        public string ACHBatchNumber { get; set; }
        public string ACHPaidTo { get; set; }
        public string ACHErrorReason { get; set; }
        public string AffiliateName { get; set; }
        public DateTime? SignUpDate { get; set; }
        public string CardNumber { get; set; }
        public string CardNumberDisplay { get; set; }
        public string Salt { get; set; }
        public string CardZIPCode { get; set; }
        public bool ChargeBack { get; set; }
        public string ChargeBackInfo { get; set; }
        public string CorporateName { get; set; }
        public string CreditCardProcessor { get; set; }
        public string Platform { get; set; }

        private bool mDoNotPay;
        public bool DoNotPay {
            get {
                return mDoNotPay;
            }
            set {
                if (mDoNotPay != value) {
                    OnChanged();
                }
                mDoNotPay = value;
            }
        }
        public long? mDoNotPayReasonID;
        public long? DoNotPayReasonID {
            get {
                return mDoNotPayReasonID;
            }
            set {
                if (mDoNotPayReasonID != value) {
                    OnChanged();
                }
                mDoNotPayReasonID = value;
            }
        }

        public decimal DriverFee { get; set; }
        public bool DriverPaid { get; set; }
        public DateTime? DriverPaidDate { get; set; }

        private string mReason;
        public string Reason {
            get {
                return mReason;
            }
            set {
                if (mReason != value) {
                    OnChanged();
                }
                mReason = value;
            }
        }
        public string DriverPaymentValidation_City { get; set; }
        public string DriverPaymentValidation_LicenseNo { get; set; }
        public string DriverPaymentValidation_Name { get; set; }
        public string DriverPaymentValidation_State { get; set; }
        public string DriverPaymentValidation_Street { get; set; }
        public string DriverName { get; set; }
        public string CellName { get; set; }
        public string Drivers_Cell { get; set; }
        public string TransNo { get; set; }

        private bool mTest;
        public bool Test {
            get { return mTest; }
            set {
                if (mTest != value) {
                    OnChanged();
                }
                mTest = value;
            }
        }

        public decimal CouponAmount { get; set; }
        public decimal TaxiPassFee { get; set; }
        public decimal RedemptionFee { get; set; }
        public decimal TotalCharge { get; set; }
        public decimal DriverTotal { get; set; }
        public decimal DriverTotalPaid { get; set; }
        public decimal RedeemerTotalPaid { get; set; }
        public bool Failed { get; set; }
        public bool IgnoreCardZIP { get; set; }
        public DateTime? ACHPaidDate { get; set; }
        public DateTime ChargeDate { get; set; }
        public string VehicleNo { get; set; }
        public string ReferenceNo { get; set; }
        public bool? PaidInCash { get; set; }
        public bool ACHDriverFeeAsDebit { get; set; }
        public bool? PayDriverPayCard { get; set; }
        public long? TaxiPassRedeemerAccountID { get; set; }
        public string RedeemerAcct_CompanyName { get; set; }
        public long? TaxiPassRedeemerID { get; set; }

        public string Redeemer_Name { get; set; }
        public string Redeemer_BofAAccountInfo { get; set; }
        public string Redeemer_TransCardNumber { get; set; }
        public string Redeemer_BankAccountHolder { get; set; }
        public string Redeemer_BankAccountNumber { get; set; }
        public bool? Redeemer_BankAccountPersonal { get; set; }
        public string Redeemer_BankAddress { get; set; }
        public string Redeemer_BankCity { get; set; }
        public string Redeemer_BankCode { get; set; }
        public string Redeemer_BankState { get; set; }
        public string Redeemer_BankSuite { get; set; }
        public string Redeemer_BankZIPCode { get; set; }
        public string Redeemer_MerchantNo { get; set; }
        public bool? Redeemer_SendNacha { get; set; }
        public bool? Redeemer_PayDriverPayCard { get; set; }

        public string AffiliateDrivers_Name { get; set; }
        public bool? Affiliate_SendNacha { get; set; }
        public string AffiliateDrivers_BankAccountHolder { get; set; }
        public string AffiliateDrivers_BankAccountNumber { get; set; }
        public bool? AffiliateDrivers_BankAccountPersonal { get; set; }
        public string AffiliateDrivers_BankAddress { get; set; }
        public string AffiliateDrivers_BankCity { get; set; }
        public string AffiliateDrivers_BankCode { get; set; }
        public string AffiliateDrivers_BankState { get; set; }
        public string AffiliateDrivers_BankSuite { get; set; }
        public string AffiliateDrivers_BankZIPCode { get; set; }
        public string AffiliateDrivers_MerchantNo { get; set; }

        public string Drivers_BankAccountHolder { get; set; }
        public string Drivers_BankAccountNumber { get; set; }
        public bool? Drivers_BankAccountPersonal { get; set; }
        public string Drivers_BankAddress { get; set; }
        public string Drivers_BankCity { get; set; }
        public string Drivers_BankCode { get; set; }
        public string Drivers_BankState { get; set; }
        public string Drivers_BankSuite { get; set; }
        public string Drivers_BankZIPCode { get; set; }
        public string Drivers_MerchantNo { get; set; }

        public string PendingMatchRedeemerAcct_BankAccountHolder { get; set; }
        public string PendingMatchRedeemerAcct_BankAccountNumber { get; set; }
        public bool? PendingMatchRedeemerAcct_BankAccountPersonal { get; set; }
        public string PendingMatchRedeemerAcct_BankAddress { get; set; }
        public string PendingMatchRedeemerAcct_BankCity { get; set; }
        public string PendingMatchRedeemerAcct_BankCode { get; set; }
        public string PendingMatchRedeemerAcct_BankState { get; set; }
        public string PendingMatchRedeemerAcct_BankSuite { get; set; }
        public string PendingMatchRedeemerAcct_BankZIPCode { get; set; }
        public string PendingMatchRedeemerAcct_MerchantNo { get; set; }
        public bool? PendingMatchRedeemerAcct_SendNacha { get; set; }
        public string PendingMatchRedeemerAcct_CompanyName { get; set; }
        public long? PendingMatchRedeemerAcct_TaxiPassRedeemerAccountID { get; set; }

        public string Affiliate_BankAccountHolder { get; set; }
        public string Affiliate_BankAccountNumber { get; set; }
        public bool? Affiliate_BankAccountPersonal { get; set; }
        public string Affiliate_BankAddress { get; set; }
        public string Affiliate_BankCity { get; set; }
        public string Affiliate_BankCode { get; set; }
        public string Affiliate_BankState { get; set; }
        public string Affiliate_BankSuite { get; set; }
        public string Affiliate_BankZIPCode { get; set; }
        public string Affiliate_MerchantNo { get; set; }
        public string TimeZone { get; set; }
        public string DriverPaidByCompany { get; set; }
        public string DriverPaidByRedeemer { get; set; }

        public bool Tag { get; set; }
        public string ACHProcessError { get; set; }


        private bool mIsNullEntity = false;
        public bool IsNullEntity {
            get {
                return mIsNullEntity;
            }
        }


        public DateTime? MyACHPaidDateTime {
            get {
                DateTime? value = ACHPaidDate;
                if (ACHPaidDate.HasValue) {
                    if (!TimeZone.IsNullOrEmpty()) {
                        value = tzConvert.ConvertTime(ACHPaidDate.Value, System.TimeZone.CurrentTimeZone.StandardName, TimeZone);
                    }
                }
                return value;
            }
        }

        public DateTime MyChargeDateTime {
            get {
                DateTime value = ChargeDate;
                if (!TimeZone.IsNullOrEmpty()) {
                    value = tzConvert.ConvertTime(value, System.TimeZone.CurrentTimeZone.StandardName, TimeZone);
                }

                return value;
            }
        }

        public DateTime? MyDriverPaidTime {
            get {
                DateTime? value = DriverPaidDate;
                if (value.HasValue && !TimeZone.IsNullOrEmpty()) {
                    value = tzConvert.ConvertTime(value.Value, System.TimeZone.CurrentTimeZone.StandardName, TimeZone);
                }

                return value;
            }
        }


        public string MyDriverName {
            get {
                if (CellName.IsNullOrEmpty()) {
                    return DriverName;
                }
                return CellName;
            }
        }

        public string DecryptCardNumber() {
            if (CardNumber.IsNullOrEmpty()) {
                return "";
            }
            Encryption oEncrypt = new Encryption();
            return oEncrypt.TripleDESDecrypt(CardNumber, Salt);
        }


        public static usp_TransactionsToACH GetNullEntity() {
            usp_TransactionsToACH rec = new usp_TransactionsToACH();
            rec.mIsNullEntity = true;
            return rec;
        }

        public static List<usp_TransactionsToACH> Execute(SqlConnection pConn, DateTime pStart, DateTime pEnd, DriverPaymentStatus pStatus, bool pIncludeTest, bool pIncludeFailed, long? pAffiliateID = null, long? pRedeemerAccountID = null) {
            DynamicParameters param = new DynamicParameters();
            param.Add("StartDate", pStart);
            param.Add("EndDate", pEnd);
            if (pStatus != DriverPaymentStatus.paid) {
                param.Add("Test", pIncludeTest);
                param.Add("Failed", pIncludeFailed);
                param.Add("Status", (int)pStatus);
            }
            if (pAffiliateID.HasValue) {
                param.Add("AffiliateID", pAffiliateID.Value);
            }
            if (pRedeemerAccountID.HasValue) {
                param.Add("RedeemerAccountID", pRedeemerAccountID.Value);
            }

            List<usp_TransactionsToACH> data;
            if (pStatus == DriverPaymentStatus.paid) {
                data = pConn.Query<usp_TransactionsToACH>("usp_TransactionsACHd", param, commandType: CommandType.StoredProcedure).ToList();
            } else {
                data = pConn.Query<usp_TransactionsToACH>("usp_TransactionsToACH", param, commandType: CommandType.StoredProcedure).ToList();
            }

            data.ForEach(p => p.RowState = DataRowState.Unchanged);

            return data;
        }


        public static List<usp_TransactionsToACH> GetByDriverPaidDate(SqlConnection pConn, DateTime pStart, DateTime pEnd, DriverPaymentStatus pStatus, bool pIncludeTest, bool pIncludeFailed, long? pAffiliateID = null, long? pRedeemerAccountID = null) {
            DynamicParameters param = new DynamicParameters();
            param.Add("StartDate", pStart);
            param.Add("EndDate", pEnd);
            if (pStatus != DriverPaymentStatus.paid) {
                param.Add("Test", pIncludeTest);
                param.Add("Failed", pIncludeFailed);
                param.Add("Status", (int)pStatus);
            }
            if (pAffiliateID.HasValue) {
                param.Add("AffiliateID", pAffiliateID.Value);
            }
            if (pRedeemerAccountID.HasValue) {
                param.Add("RedeemerAccountID", pRedeemerAccountID.Value);
            }

            List<usp_TransactionsToACH> data = null;
            if (pStatus == DriverPaymentStatus.paid) {
                data = pConn.Query<usp_TransactionsToACH>("usp_TransactionsACHd", param, commandType: CommandType.StoredProcedure).ToList();
            } else {
                data = pConn.Query<usp_TransactionsToACH>("usp_TransactionsToACHByDriverPaidDate", param, commandType: CommandType.StoredProcedure).ToList();
            }
            //List<string> transNoList = data.OrderBy(p => p.TransNo).Select(p => p.TransNo).ToList();
            //string temp = Newtonsoft.Json.JsonConvert.SerializeObject(transNoList);
            //Console.WriteLine(temp);

            data.ForEach(p => p.RowState = DataRowState.Unchanged);
            return data;
        }

        public static List<usp_TransactionsToACH> GetByPlatform(SqlConnection pConn, DateTime pStart, DateTime pEnd, DriverPaymentStatus pStatus, bool pIncludeTest, bool pIncludeFailed, string pPlatform) {
            DynamicParameters param = new DynamicParameters();
            param.Add("StartDate", pStart);
            param.Add("EndDate", pEnd);
            if (pStatus != DriverPaymentStatus.paid) {
                param.Add("Test", pIncludeTest);
                param.Add("Failed", pIncludeFailed);
            }
            param.Add("Status", (int)pStatus);
            param.Add("Platform", pPlatform);

            var data = pConn.Query<usp_TransactionsToACH>("usp_TransactionsToACHByPlatform", param, commandType: CommandType.StoredProcedure).ToList();
            data.ForEach(p => p.RowState = DataRowState.Unchanged);
            return data;
        }


        public static usp_TransactionsToACH Execute(SqlConnection pConn, string pTransNo) {
            DynamicParameters param = new DynamicParameters();
            param.Add("TransNo", pTransNo);
            var data = pConn.Query<usp_TransactionsToACH>("usp_TransactionsToACHByTransNo", param, commandType: CommandType.StoredProcedure).DefaultIfEmpty(GetNullEntity()).FirstOrDefault();
            data.RowState = DataRowState.Unchanged;
            return data;
        }


        /// <summary>
        /// Convert Payment to TTech transaction object.
        /// </summary>
        /// <returns></returns>
        public TTechTransaction PaymentToTTechTransaction(SqlConnection pConn) {
            return PaymentToTTechTransaction(pConn, 0);
        }


        /// <summary>
        /// Convert Payment to TTech transaction object.
        /// </summary>
        /// <param name="pLaneID">The lane ID.</param>
        /// <returns></returns>
        public TTechTransaction PaymentToTTechTransaction(SqlConnection pConn, short pLaneID) {
            return PaymentToTTechTransaction(pConn, pLaneID, false);
        }


        /// <summary>
        /// Convert Payment to TTech transaction object.
        /// </summary>
        /// <param name="pLaneID">The p lane ID.</param>
        /// <param name="pBofA">if set to <c>true</c> [p bof A].</param>
        /// <returns></returns>
        public bool TTechDriverFeeChargeBack { get; set; }
        public TTechTransaction PaymentToTTechTransaction(SqlConnection pConn, short pLaneID, bool pBofA) {
            usp_TransactionsToACH oPayment = this;
            SystemDefaults oDefault = SystemDefaults.GetDefaults(pConn);
            TTechTransaction oTTech = new TTechTransaction();
            PayInfo oPayInfo = null;

            bool payDriver = !oPayment.TaxiPassRedeemerID.HasValue
                            || oPayment.TaxiPassRedeemerID.Value < 1
                            || oPayment.PayDriverPayCard.GetValueOrDefault(false) && !oPayment.PaidInCash.GetValueOrDefault(false);

            //check if redeemer has payment info, if not then pay driver
            if (!payDriver) {
                if (oPayment.TaxiPassRedeemerID.HasValue) {
                    //TaxiPassRedeemerAccounts acct = oPayment.TaxiPassRedeemers.TaxiPassRedeemerAccounts;
                    if (oPayment.Redeemer_BofAAccountInfo.IsNullOrEmpty() && oPayment.Redeemer_BankAccountNumber.IsNullOrEmpty() && oPayment.Redeemer_TransCardNumber.IsNullOrEmpty()) {
                        payDriver = true;
                    }
                }
            }
            if (payDriver) {
                if (oPayment.AffiliateDriverID > 0) {
                    oPayInfo = oPayment.AffiliateDrivers_GetPayInfo();
                }
                if (oPayInfo == null) {
                    if (!oPayment.Drivers_BankAccountNumber.IsNullOrEmpty() && !oPayment.Drivers_BankCode.IsNullOrEmpty() && !oPayment.Drivers_MerchantNo.IsNullOrEmpty()) {
                        oPayInfo = oPayment.Drivers_GetPayInfo();
                    }
                }
            }
            if (oPayInfo == null) {
                //do we pay redeemer or affiliate?
                if (!pBofA) {
                    if (oPayment.TaxiPassRedeemerID.HasValue && oPayment.TaxiPassRedeemerID.Value > 0) {
                        if (!oPayment.PendingMatchRedeemerAcct_BankAccountNumber.IsNullOrEmpty()) {
                            oPayInfo = oPayment.PendingMatch_GetPayInfo();
                        } else {
                            oPayInfo = oPayment.TaxiPassRedeemers_GetPayInfo();
                        }
                    } else {
                        if (!oPayment.Affiliate_BankAccountNumber.IsNullOrEmpty()) {
                            oPayInfo = oPayment.Affiliate_GetPayInfo();
                        }
                    }
                }
            }

            if ((!pBofA && oPayInfo == null) || !oPayment.DriverPaid) {
                return null;
            }


            if (!payDriver && oPayment.TaxiPassRedeemerID.HasValue && oPayment.TaxiPassRedeemerID.Value > 0 &&
                    (!oPayment.Redeemer_PayDriverPayCard.GetValueOrDefault(false) || !oPayment.Redeemer_BofAAccountInfo.IsNullOrEmpty())) {
                if (oPayment.PendingMatchRedeemerAcct_TaxiPassRedeemerAccountID.HasValue) {
                    oTTech.Affiliate = "Redeemer: " + oPayment.PendingMatchRedeemerAcct_CompanyName;
                } else {
                    oTTech.Affiliate = "Redeemer: " + oPayment.RedeemerAcct_CompanyName;
                }
            } else {
                oTTech.Affiliate = oPayment.AffiliateName;
            }

            //oTTech.NachaFile = oPay.NachaFile;
            oTTech.Site = oDefault.TTechSite;
            oTTech.Action = TTechAction.Normal;

            oTTech.TranID = oPayment.DriverPaymentID;

            if (oPayment.ChargeBack || TTechDriverFeeChargeBack) {
                oTTech.LaneID = pLaneID++;
            } else {
                oTTech.LaneID = pLaneID;  // oPay.MyMerchant.LaneID;
            }
            if (oTTech.Affiliate.StartsWith("Redeemer:")) {
                oTTech.Amount = oPayment.RedeemerTotalPaid;
            } else {

                // 3/7/12 CA ACHDriverFeeAsDebit is for Carmel 
                // TTechDriverFeeChargeBack is set in CabRideAutoACH after the transactions are processed
                if (oPayment.ACHDriverFeeAsDebit) {
                    if (TTechDriverFeeChargeBack) {
                        oTTech.Amount = oPayment.DriverFee;
                    } else {
                        // Take whatever is set in the DB.  Carmel rolls their chargebacks into the "Credit" line
                        oTTech.Amount = oPayment.DriverTotal;
                    }
                } else {
                    //oTTech.Amount = oPayment.DriverTotalPaid;
                    oTTech.Amount = Math.Abs(oPayment.DriverTotalPaid);
                }
            }

            if (oPayment.DriverPaidDate.HasValue) {
                oTTech.TranDate = oPayment.DriverPaidDate.Value;
            } else {
                oTTech.TranDate = DateTime.Today;
            }
            oTTech.CheckNumber = oTTech.TranID;  //?
            if (TTechDriverFeeChargeBack) {
                oTTech.CheckNumber = Convert.ToInt64("999" + Math.Abs(oTTech.CheckNumber).ToString());
            }
            if (!pBofA) {
                oTTech.PaidTo = oPayInfo.PaidTo;
                oTTech.CustomerName = oPayInfo.BankAccountHolder;
                oTTech.Address = oPayInfo.BankAddress;
                oTTech.Suite = oPayInfo.BankSuite;
                oTTech.City = oPayInfo.BankCity;
                oTTech.State = oPayInfo.BankState;
                oTTech.ZIPCode = oPayInfo.BankZIPCode;
                oTTech.Phone1 = oPayment.Drivers_Cell;
                oTTech.Company = oPayInfo.BankAccountPersonal ? "" : oPayment.AffiliateName;
                oTTech.CheckType = oPayInfo.BankAccountPersonal ? "PERSONAL" : "BUSINESS";
                oTTech.ABANumber = oPayInfo.BankCode;
                oTTech.Account = oPayInfo.BankAccountNumber;
                oTTech.ClassCode = oPayInfo.BankAccountPersonal ? TaxiPassCommon.Banking.NachaGenerator.StandardEntryTypes.PPD.ToString() :
                                                              TaxiPassCommon.Banking.NachaGenerator.StandardEntryTypes.CCD.ToString();
            }

            if ((oPayment.ChargeBack && !oPayment.ACHDriverFeeAsDebit) || TTechDriverFeeChargeBack) {
                oTTech.TranType = "DEBIT";
                oTTech.Application = "DDA";
                oTTech.PaymentType = "S";
            } else {
                oTTech.TranType = "CREDIT";
                oTTech.Application = "DDA";
                oTTech.PaymentType = "S";
                if (oPayInfo != null) {
                    oTTech.WireTransfer = oPayInfo.WireTransfer;
                }
            }
            oTTech.TranReference = oPayment.DriverPaymentID.ToString();

            return oTTech;
        }

        private PayInfo Drivers_GetPayInfo() {
            if (Drivers_BankAccountNumber.IsNullOrEmpty()) {
                return null;
            }
            PayInfo oPay = new PayInfo();
            oPay.NachaFile = Affiliate_SendNacha.GetValueOrDefault(false);
            oPay.BankAccountHolder = Drivers_BankAccountHolder;
            oPay.BankAccountNumber = Drivers_BankAccountNumber;
            oPay.BankAccountPersonal = Drivers_BankAccountPersonal.GetValueOrDefault(false);
            oPay.BankAddress = Drivers_BankAddress;
            oPay.BankCity = Drivers_BankCity;
            oPay.BankCode = Drivers_BankCode;
            oPay.BankState = Drivers_BankState;
            oPay.BankSuite = Drivers_BankSuite;
            oPay.BankZIPCode = Drivers_BankZIPCode;
            if (!Drivers_MerchantNo.IsNullOrEmpty()) {
                oPay.MyMerchant = new Merchant();
                oPay.MyMerchant.MerchantNo = Drivers_MerchantNo;
            }
            oPay.PaidTo = "Driver: " + this.DriverName;

            return oPay;

        }

        private PayInfo Affiliate_GetPayInfo() {
            if (Affiliate_BankAccountNumber.IsNullOrEmpty()) {
                return null;
            }
            PayInfo oPay = new PayInfo();
            oPay.NachaFile = Affiliate_SendNacha.GetValueOrDefault(false);
            oPay.BankAccountHolder = Affiliate_BankAccountHolder;
            oPay.BankAccountNumber = Affiliate_BankAccountNumber;
            oPay.BankAccountPersonal = Affiliate_BankAccountPersonal.GetValueOrDefault(false);
            oPay.BankAddress = Affiliate_BankAddress;
            oPay.BankCity = Affiliate_BankCity;
            oPay.BankCode = Affiliate_BankCode;
            oPay.BankState = Affiliate_BankState;
            oPay.BankSuite = Affiliate_BankSuite;
            oPay.BankZIPCode = Affiliate_BankZIPCode;
            if (!Affiliate_MerchantNo.IsNullOrEmpty()) {
                oPay.MyMerchant = new Merchant();
                oPay.MyMerchant.MerchantNo = Affiliate_MerchantNo;
            }
            oPay.PaidTo = "Affiliate: " + this.AffiliateName;

            return oPay;

        }

        private PayInfo AffiliateDrivers_GetPayInfo() {
            if (AffiliateDrivers_BankAccountNumber.IsNullOrEmpty()) {
                return null;
            }
            PayInfo oPay = new PayInfo();
            oPay.NachaFile = Affiliate_SendNacha.GetValueOrDefault(false);
            oPay.BankAccountHolder = AffiliateDrivers_BankAccountHolder;
            oPay.BankAccountNumber = AffiliateDrivers_BankAccountNumber;
            oPay.BankAccountPersonal = AffiliateDrivers_BankAccountPersonal.GetValueOrDefault(false);
            oPay.BankAddress = AffiliateDrivers_BankAddress;
            oPay.BankCity = AffiliateDrivers_BankCity;
            oPay.BankCode = AffiliateDrivers_BankCode;
            oPay.BankState = AffiliateDrivers_BankState;
            oPay.BankSuite = AffiliateDrivers_BankSuite;
            oPay.BankZIPCode = AffiliateDrivers_BankZIPCode;
            if (!AffiliateDrivers_MerchantNo.IsNullOrEmpty()) {
                oPay.MyMerchant = new Merchant();
                oPay.MyMerchant.MerchantNo = AffiliateDrivers_MerchantNo;
            }
            oPay.PaidTo = "Driver: " + this.AffiliateDrivers_Name;

            return oPay;

        }

        private PayInfo PendingMatch_GetPayInfo() {
            if (PendingMatchRedeemerAcct_BankAccountNumber.IsNullOrEmpty()) {
                return null;
            }
            PayInfo oPay = new PayInfo();
            oPay.NachaFile = PendingMatchRedeemerAcct_SendNacha.GetValueOrDefault(false);
            oPay.BankAccountHolder = PendingMatchRedeemerAcct_BankAccountHolder;
            oPay.BankAccountNumber = PendingMatchRedeemerAcct_BankAccountNumber;
            oPay.BankAccountPersonal = PendingMatchRedeemerAcct_BankAccountPersonal.GetValueOrDefault(false);
            oPay.BankAddress = PendingMatchRedeemerAcct_BankAddress;
            oPay.BankCity = PendingMatchRedeemerAcct_BankCity;
            oPay.BankCode = PendingMatchRedeemerAcct_BankCode;
            oPay.BankState = PendingMatchRedeemerAcct_BankState;
            oPay.BankSuite = PendingMatchRedeemerAcct_BankSuite;
            oPay.BankZIPCode = PendingMatchRedeemerAcct_BankZIPCode;
            if (!PendingMatchRedeemerAcct_MerchantNo.IsNullOrEmpty()) {
                oPay.MyMerchant = new Merchant();
                oPay.MyMerchant.MerchantNo = PendingMatchRedeemerAcct_MerchantNo;
            }
            oPay.PaidTo = "Redeemer: " + this.PendingMatchRedeemerAcct_CompanyName;

            return oPay;

        }

        private PayInfo TaxiPassRedeemers_GetPayInfo() {
            if (Redeemer_BankAccountNumber.IsNullOrEmpty()) {
                return null;
            }
            PayInfo oPay = new PayInfo();
            oPay.NachaFile = Redeemer_SendNacha.GetValueOrDefault(false);
            oPay.BankAccountHolder = Redeemer_BankAccountHolder;
            oPay.BankAccountNumber = Redeemer_BankAccountNumber;
            oPay.BankAccountPersonal = Redeemer_BankAccountPersonal.GetValueOrDefault(false);
            oPay.BankAddress = Redeemer_BankAddress;
            oPay.BankCity = Redeemer_BankCity;
            oPay.BankCode = Redeemer_BankCode;
            oPay.BankState = Redeemer_BankState;
            oPay.BankSuite = Redeemer_BankSuite;
            oPay.BankZIPCode = Redeemer_BankZIPCode;
            if (!Redeemer_MerchantNo.IsNullOrEmpty()) {
                oPay.MyMerchant = new Merchant();
                oPay.MyMerchant.MerchantNo = Redeemer_MerchantNo;
            }
            oPay.PaidTo = "Redeemer: " + this.RedeemerAcct_CompanyName;

            return oPay;

        }

        //private bool CheckAccount(DriverBankInfo pBankInfo) {
        //	if (string.IsNullOrEmpty(pBankInfo.MerchantInfo.MerchantNo)) {
        //		return false;
        //	}
        //	if (string.IsNullOrEmpty(pBankInfo.BankAccountNumber)) {
        //		return false;
        //	}
        //	if (string.IsNullOrEmpty(pBankInfo.BankCode)) {
        //		return false;
        //	}
        //	return true;
        //}

    }
}
