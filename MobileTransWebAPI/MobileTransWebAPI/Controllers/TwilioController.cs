﻿using CabRideEngineDapper;
using CabRideEngineDapper.Utils;
using System.Collections.Generic;
using System.Web.Http;
using Twilio;

namespace MobileTransWebAPI.Controllers {
	public class TwilioController : ApiController {

		[HttpGet]
		public SMSMessage SendMesssage(long pAffiliateID, long pDriverID, string pMessage) {
			SMSMessage msg = new SMSMessage();
			msg.Status = "Invalid Request";

			using (var conn = SqlHelper.OpenSqlConnection()) {
				Drivers driver = Drivers.GetDriver(pDriverID);
				if (driver.AffiliateID.GetValueOrDefault(0) == pAffiliateID) {
					return driver.SendMsgViaTwilio(conn, pMessage);
				}
			}

			return msg;
		}

		public static string GetToken(string pAccountSID, string pAccountToken, long pDriverID) {
			TwilioCapability capability = new TwilioCapability(pAccountSID, pAccountToken);
			string clientName = string.Format("TaxiPass_{0}", pDriverID);
			capability.AllowClientIncoming(clientName);
			List<string> appParms = new List<string>();
			appParms.Add(clientName);
			capability.AllowClientOutgoing(Properties.Settings.Default.TwilioAppSID); //, appParms);
			return capability.GenerateToken(86400);
		}
	}


}
