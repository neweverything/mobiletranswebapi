using System;
using System.Collections.Generic;
using System.Text;

namespace TaxiPassCommon.Banking {
    public class BankInfo {
        public string BankName = "";
        public string AccountHolder = "";
        public string Address = "";
        public string Suite = "";
        public string City = "";
        public string State = "";
        public string ZIPCode = "";
        public string Phone = "";
        public bool IsAccountPersonal = false;
        public string RoutingABA = "";
        public string AccountNumber = "";
        public Merchant MyMerchant;

        public string TransactionKey {
            get {
                return this.MyMerchant.MerchantNo + "_" + this.RoutingABA + "_" + this.AccountNumber;
            }
        }

    }
}
