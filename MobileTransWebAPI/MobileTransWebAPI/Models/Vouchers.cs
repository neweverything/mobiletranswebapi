﻿using System;

namespace MobileTransWebAPI.Models {
    public partial class Vouchers {
        public DateTime ChargeDate { get; set; }
        public string TransNo { get; set; }
        public decimal DriverTotal { get; set; }
        public bool Selected { get; set; }
    }
}