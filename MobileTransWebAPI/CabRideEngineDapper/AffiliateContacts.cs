﻿using CabRideEngineDapper.Utils;
using Dapper;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using TaxiPassCommon;

namespace CabRideEngineDapper {
	public partial class AffiliateContacts {

		private Affiliate mAffiliate;
		[Editable(false)]
		public Affiliate Affiliate {
			get {
				if (mAffiliate == null) {
					mAffiliate = CabRideEngineDapper.Affiliate.GetAffiliate(this.AffiliateID);
				}
				return mAffiliate;
			}
		}


		private string mNewPassword;
		[Editable(false)]
		public string NewPassword {
			get {
				return mNewPassword;
			}
			set {
				if (!value.IsNullOrEmpty()) {
					mNewPassword = value;
					Encryption oEncrypt = new Encryption();
					Password = oEncrypt.Encrypt(value, this.Affiliate.Salt);
				}
			}
		}

		[Editable(false)]
		public string PasswordDecrypted {
			get {
				Encryption oEncrypt = new Encryption();
				return oEncrypt.Decrypt(Password, this.Affiliate.Salt);
			}
		}

		[Editable(false)]
		public string PhoneFormatted {
			get {
				return Phone.FormattedPhoneNumber();
			}
		}

		public static AffiliateContacts GetContact(string pEMail) {
			using (var conn = SqlHelper.OpenSqlConnection()) {
				return GetContact(conn, pEMail);
			}
		}

		public static AffiliateContacts GetContact(SqlConnection pConn, string pEMail) {
			string sql = "SELECT * FROM AffiliateContacts WITH (NOLOCK) WHERE Email = @Email";
			return pConn.Query<AffiliateContacts>(sql, new { Email = pEMail }).DefaultIfEmpty(AffiliateContacts.GetNullEntity()).FirstOrDefault();
		}


		public static AffiliateContacts GetContact(long pContactID) {
			using (var conn = SqlHelper.OpenSqlConnection()) {
				return GetContact(conn, pContactID);
			}
		}

		public static AffiliateContacts GetContactByPhone(long pAffiliateID, string pPhone) {
			string sql = @"SELECT *
							FROM AffiliateContacts
							WHERE AffiliateID = @AffiliateID
								  AND Cell = @Cell";
			using (var conn = SqlHelper.OpenSqlConnection()) {
				return conn.Get<AffiliateContacts>(sql, new { AffiliateID = pAffiliateID, Cell = pPhone });
			}
		}

		public static IEnumerable<AffiliateContacts> GetContacts(SqlConnection pConn, long pAffiliateID) {
			string sql = "SELECT * FROM AffiliateContacts WHERE AffiliateID = @AffiliateID ORDER BY Employee";
			return pConn.Query<AffiliateContacts>(sql, new { AffiliateID = pAffiliateID });
		}

		// ************************************************************************************
		// Return a list of contacts who are set to receive the ACHTransRpt from CAbRideAutoACH
		// ************************************************************************************
		public static List<AffiliateContacts> GetACHTransReportRecipients(SqlConnection pConn, long pAffiliateID) {

			string sql = @"SELECT  * FROM    AffiliateContacts
							WHERE   AffiliateID = @AffiliateID
									AND ACHTransReportRecipient = 1";

			return pConn.Query<AffiliateContacts>(sql, new { AffiliateID = pAffiliateID }).ToList();
		}


		public static AffiliateContacts GetContact(SqlConnection pConn, long pContactID) {
			string sql = "SELECT * FROM AffiliateContacts WHERE AffiliateContactID = @AffiliateContactID";
			return pConn.Get<AffiliateContacts>(sql, new { AffiliateContactID = pContactID });
		}
	}
}
