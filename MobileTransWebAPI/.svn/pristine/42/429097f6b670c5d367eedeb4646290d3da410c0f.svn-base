﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using System.Text;
using System.Web.Script.Serialization;

using MobileTransWebAPI.Models;
using MobileTransWebAPI.Utils;

using IdeaBlade.Persistence;

using TaxiPassCommon;
using CabRideEngine;

namespace MobileTransWebAPI.Controllers {

	public class TransHistoryController : ApiController {

		public TransListResult Get() {
            return GetTransHistoryWithBankPending("92435AC55948F8CFCA9DE7676084B1DB16E767F73C081F5448315C622F6839148C50488799A669A64ED3A2EFB376576FB30A56367BE29ABE3AA6E7428851131885A93D759A8014F4B81A505BDCF58D8FFF78C3B34C6DD40CB19034C6325EADED7238BAC61B84B9A1BAC70BB8F1EBFB441C40D379ED92E224BD4990AB97300107FC22D74C3B91D5F317A584016AB9D83CF0773DFA");
		}



		public TransListResult GetTransHistory(string pTransRequest) {
			TransListResult result = new TransListResult();
			JavaScriptSerializer js = new JavaScriptSerializer();

			pTransRequest = pTransRequest.DecryptIceKey();
			WebApiApplication.LogIt("GetTransHistory", pTransRequest);

			PersistenceManager oPM = WebApiApplication.UserPM;
			if (oPM == null) {
				result.ErrorMsg = "Could not connect to database";
				return result;
			}

			TransListRequest info = js.Deserialize<TransListRequest>(pTransRequest);

			if (info.DriverID < 1) {
				result.ErrorMsg = "Invalid Access";
				return result;
			}

			Drivers oDriver = Drivers.GetDriver(oPM, info.DriverID);
			if (oDriver.IsNullEntity) {
				result.ErrorMsg = "Invalid Driver";
				return result;
			}

			try {
				DateTime requestDate = DateTime.Today.AddDays(-1);
				if (info != null) {
					requestDate = info.RequestKey.DecryptIceKey().ToDateTime();
				}

                if (Math.Abs(WebApiApplication.GetUTCServerTime().Subtract(requestDate).TotalMinutes) > 1) {
					result.ErrorMsg = "Invalid Access";
					return result;
				}
			} catch (Exception) {
				result.ErrorMsg = "Invalid Access";
				return result;
			}


			AffiliateDrivers affDriver = AffiliateDrivers.GetDriver(oPM, info.AffDriverID);
			EntityList<DriverPayments> oList = new EntityList<DriverPayments>();
			int month = DateTime.Now.Month;
			int year = DateTime.Now.Year;
			DateTime dStart = new DateTime(year, month, 1);
			DateTime dEnd = dStart.AddMonths(1).AddSeconds(-1); // dStart.AddMonths(1).AddSeconds(-1);
			if (affDriver.IsNullEntity) {
				if (!info.CurrentMonth) {
					dStart = dStart.AddMonths(-1);
					dEnd = dStart.AddMonths(1).AddSeconds(-1);
				}
				oList.AddRange(DriverPayments.GetCharges(oDriver, dStart, dEnd, false));
			} else {
				if (!info.CurrentMonth) {
					dStart = dStart.AddMonths(-1);
					dEnd = dStart.AddMonths(1).AddSeconds(-1);
				}

				oList.AddRange(DriverPayments.GetCharges(affDriver, dStart, dEnd, false));

			}

			oList = new EntityList<DriverPayments>((from p in oList
													where !p.Test && !p.Failed
													select p).ToList());

			StringBuilder sPaid = new StringBuilder();
			StringBuilder sTrans = new StringBuilder();
			bool addDriverFee = oDriver.MyAffiliate.AffiliateDriverDefaults.CarmelDispatch;

			foreach (DriverPayments oPay in oList) {
				decimal nTotal = oPay.DriverTotalPaid; //		oPay.TotalCharge - oPay.TaxiPassFee - oPay.DriverFee.GetValueOrDefault(0);
				if (addDriverFee) {
					nTotal += oPay.DriverFee.GetValueOrDefault(0);
				}
				string sText = String.Format("{0},{1},{2},{3},{4},{5}|", oPay.ChargeDate.Value.ToString("MM/dd"), nTotal.ToString("C"), oPay.Test ? "Test" : oPay.TransNo, oPay.CardNumberDisplay.IsNullOrEmpty() ? "N/A" : oPay.CardNumberDisplay.Right(4), oPay.DriverPaidDate.HasValue ? oPay.DriverPaidDate.Value.ToString("MM/dd") : "", oPay.ACHDetail.ACHPaidDate.HasValue ? oPay.ACHDetail.ACHPaidDate.Value.ToString("MM/dd") : "");
				//sOutput.Append(StringUtils.XMLTag("Rec" + i.ToString(), sText));

				if (oPay.ACHDetail.ACHPaidDate.HasValue || oPay.TaxiPassRedeemerID.HasValue || oPay.DriverPaid) {
					sPaid.Append(sText);
				} else {
					sTrans.Append(sText);
				}
			}
			if (sPaid.Length > 2) {
				result.PaidTrans = sPaid.Remove(sPaid.Length - 1, 1).ToString();
			}
			if (sTrans.Length > 2) {
				result.Trans = sTrans.Remove(sTrans.Length - 1, 1).ToString();
			}

			return result;
		}


		public TransListResult GetTransHistoryWithBankPending(string pRequest) {
			TransListResult result = new TransListResult();
			JavaScriptSerializer js = new JavaScriptSerializer();

			pRequest = pRequest.DecryptIceKey();
			WebApiApplication.LogIt("GetTransHistoryWithBankPending", pRequest);

			PersistenceManager oPM = WebApiApplication.UserPM;
			if (oPM == null) {
				result.ErrorMsg = "Could not connect to database";
				return result;
			}

			Drivers oDriver = oPM.GetNullEntity<Drivers>();
			TransListRequest info = new TransListRequest();
			try {
				info = js.Deserialize<TransListRequest>(pRequest);

				if (info.DriverID < 1) {
					result.ErrorMsg = "Invalid Access";
					return result;
				}

				oDriver = Drivers.GetDriver(oPM, info.DriverID);
				if (oDriver.IsNullEntity) {
					result.ErrorMsg = "Invalid Driver";
					return result;
				}

				DateTime requestDate = DateTime.Today.AddDays(-1);
				if (info != null) {
					requestDate = info.RequestKey.DecryptIceKey().ToDateTime();
				}

                if (Math.Abs(WebApiApplication.GetUTCServerTime().Subtract(requestDate).TotalMinutes) > 1) {
					result.ErrorMsg = "Invalid Access";
					return result;
				}
			} catch (Exception) {
				result.ErrorMsg = "Invalid Access";
				return result;
			}


			AffiliateDrivers affDriver = AffiliateDrivers.GetDriver(oPM, info.AffDriverID);
			EntityList<DriverPayments> oList = new EntityList<DriverPayments>();
			int month = DateTime.Now.Month;
			int year = DateTime.Now.Year;
			DateTime dStart = new DateTime(year, month, 1);
			DateTime dEnd = dStart.AddMonths(1).AddSeconds(-1); // dStart.AddMonths(1).AddSeconds(-1);
			if (affDriver.IsNullEntity) {
				if (!info.CurrentMonth) {
					dStart = dStart.AddMonths(-1);
					dEnd = dStart.AddMonths(1).AddSeconds(-1);
				}
				oList.AddRange(DriverPayments.GetCharges(oDriver, dStart, dEnd, false));
			} else {
				if (!info.CurrentMonth) {
					dStart = dStart.AddMonths(-1);
					dEnd = dStart.AddMonths(1).AddSeconds(-1);
				}

				oList.AddRange(DriverPayments.GetCharges(affDriver, dStart, dEnd, false));

			}

			oList = new EntityList<DriverPayments>((from p in oList
													where (!p.Test || (Properties.Settings.Default.AllowTestTrans.Split('|').Contains(affDriver.Cell))) && !p.Failed
													select p).ToList());

			StringBuilder sPaid = new StringBuilder();
			StringBuilder sTrans = new StringBuilder();
			StringBuilder sPending = new StringBuilder();

			bool addDriverFee = oDriver.MyAffiliate.AffiliateDriverDefaults.CarmelDispatch;

			string sAppVers = "0";
			if (!oDriver.AppVersion.IsNullOrEmpty()) {
				List<string> temp = oDriver.AppVersion.Substring(1).Split('.').ToList();
				while (temp.Count > 2) {
					temp.RemoveAt(temp.Count - 1);
				}
				sAppVers = string.Join(".", temp.ToArray());
			}
			decimal appVers = Convert.ToDecimal(sAppVers);


			foreach (DriverPayments oPay in oList) {
				decimal nTotal = oPay.DriverTotalPaid; //		oPay.TotalCharge - oPay.TaxiPassFee - oPay.DriverFee.GetValueOrDefault(0);
				if (addDriverFee) {
					nTotal += oPay.DriverFee.GetValueOrDefault(0);
				}

				string sText;

				// added logic below to ensure we don't mess up existing drivers
				if ((affDriver.BankAccountNumber.IsNullOrEmpty() && affDriver.TransCardAdminNo.IsNullOrEmpty()) || appVers < 2.998M) {
					sText = String.Format("{0},{1},{2},{3}|", oPay.ChargeDate.Value.ToString("MM/dd"), nTotal.ToString("C"), oPay.Test ? "Test" : oPay.TransNo, oPay.CardNumberDisplay.IsNullOrEmpty() ? "N/A" : oPay.CardNumberDisplay.Right(4));
				} else {
					sText = String.Format("{0},{1},{2},{3},{4},{5}|", oPay.ChargeDate.Value.ToString("MM/dd"), nTotal.ToString("C"), oPay.Test ? "Test" : oPay.TransNo, oPay.CardNumberDisplay.IsNullOrEmpty() ? "N/A" : oPay.CardNumberDisplay.Right(4), oPay.DriverPaidDate.HasValue ? oPay.DriverPaidDate.Value.ToString("MM/dd") : "", oPay.ACHDetail.ACHPaidDate.HasValue ? oPay.ACHDetail.ACHPaidDate.Value.ToString("MM/dd") : "");
				}
				//sOutput.Append(StringUtils.XMLTag("Rec" + i.ToString(), sText));

				if (!oPay.TaxiPassRedeemerID.HasValue && oPay.DriverPaid && (!oPay.ACHDetail.ACHPaidDate.HasValue || oPay.ACHDetail.ACHPaidDate.GetValueOrDefault(DateTime.Today.AddMonths(-1)) == DateTime.Today)) {
					sPending.Append(sText);
				} else if (oPay.ACHDetail.ACHPaidDate.HasValue || oPay.TaxiPassRedeemerID.HasValue || oPay.DriverPaid) {
					sPaid.Append(sText);
				} else {
					sTrans.Append(sText);
				}
			}
			if (sPaid.Length > 2) {
				result.PaidTrans = sPaid.Remove(sPaid.Length - 1, 1).ToString();
			}
			if (sTrans.Length > 2) {
				result.Trans = sTrans.Remove(sTrans.Length - 1, 1).ToString();
			}
			if (sPending.Length > 2) {
				result.PendingTrans = sPending.Remove(sPending.Length - 1, 1).ToString();
			}

			return result;
		}

	}
}
