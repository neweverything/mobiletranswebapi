﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

using Dapper;

namespace CabRideEngineDapper {
    public partial class usp_ReservationGetGPSHistory {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string Address { get; set; }
        public DateTime GPSTime { get; set; }

        public static IEnumerable<usp_ReservationGetGPSHistory> Execute(SqlConnection pConn, string pTransNo) {
            return pConn.Query<usp_ReservationGetGPSHistory>("usp_ReservationGetGPSHistory", new { TransNo = pTransNo }, commandType: CommandType.StoredProcedure);
        }

    }
}
