﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using TaxiPassCommon;

namespace CabRideEngineDapper.StoredProc {
    public class ReceiptRequestByPlatform {
        public string TransNo { get; set; }
        public string ReceiptRequestID { get; set; }
        public string CaseNumber { get; set; }
        public string AffiliateID { get; set; }
        public string AffiliateName { get; set; }
        public string CardNumberDisplay { get; set; }
        public string CardHolder { get; set; }
        public string ChargeDate { get; set; }
        public string TotalCharge { get; set; }
        public string TaxiPassFee { get; set; }
        public string ReceiptRequest { get; set; }
        public string ReceiptRequestDue { get; set; }
        public string ChargeBackDueDate { get; set; }
        public string CBStatus { get; set; }
        public string DateOfWinLoss { get; set; }
        public string OutCome { get; set; }
        public string ContestedAmount { get; set; }
        public string ChargeBackReason { get; set; }

        private string mReceiptURL;
        public string ReceiptURL {
            get {
                return mReceiptURL;
            }
            set {
                if (value.EndsWith("z=")) {
                    value = value + TransNo.EncryptIceKey();
                }
                mReceiptURL = value;
            }
        }

        

        public static List<ReceiptRequestByPlatform> Execute(SqlConnection pConn, DateTime pStartDate, DateTime pEndDate, long pAffiliateID, string pTransNo, string pPlatform, bool pAllRequests = false) {
            DynamicParameters parms = new DynamicParameters();
            parms.Add("StartDate", pStartDate);
            parms.Add("EndDate", pEndDate);
            parms.Add("AffiliateID", pAffiliateID);
            parms.Add("TransNo", pTransNo);
            parms.Add("Platform", pPlatform);
            parms.Add("AllRequests", pAllRequests);


            return pConn.Query<ReceiptRequestByPlatform>("usp_ReceiptRequestByPlatform", parms, commandType: System.Data.CommandType.StoredProcedure).ToList();
        }


    }
}
