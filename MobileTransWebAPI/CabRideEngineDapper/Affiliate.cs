﻿using CabRideEngineDapper.Enums;
using CabRideEngineDapper.Utils;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using TaxiPassCommon;
using TaxiPassCommon.Banking;

namespace CabRideEngineDapper {

	public partial class Affiliate {

		#region Related Tables
		private AffiliateDriverDefaults mAffiliateDriverDefaults;
		public AffiliateDriverDefaults AffiliateDriverDefaults {
			get {
				if (mAffiliateDriverDefaults == null) {
					mAffiliateDriverDefaults = CabRideEngineDapper.AffiliateDriverDefaults.GetDefault(this);
				}

				return mAffiliateDriverDefaults;
			}
			set {
				mAffiliateDriverDefaults = value;
			}
		}

		private List<Drivers> mDrivers;
		public List<Drivers> Drivers {
			get {
				if (mDrivers == null) {
					mDrivers = CabRideEngineDapper.Drivers.GetDrivers(this.AffiliateID);
				}
				return mDrivers;
			}
		}

		private List<AffiliateDrivers> mAffiliateDrivers;
		public List<AffiliateDrivers> AffiliateDrivers {
			get {
				if (mAffiliateDrivers == null) {
					mAffiliateDrivers = CabRideEngineDapper.AffiliateDrivers.GetDrivers(this.AffiliateID);
				}
				return mAffiliateDrivers;
			}
		}

		private List<AffiliateFees> mAffiliateFees;
		public List<AffiliateFees> AffiliateFeeses {
			get {
				if (mAffiliateFees == null) {
					mAffiliateFees = CabRideEngineDapper.AffiliateFees.GetFees(this.AffiliateID);
				}
				return mAffiliateFees;
			}
			set {
				mAffiliateFees = value;
			}
		}

		private List<AffiliateDispatchStatus> mAffiliateDispatchStatuses;
		public List<AffiliateDispatchStatus> AffiliateDispatchStatuses {
			get {
				if (mAffiliateDispatchStatuses == null) {
					mAffiliateDispatchStatuses = AffiliateDispatchStatus.GetByAffiliate(this.AffiliateID);
				}
				return mAffiliateDispatchStatuses;
			}
			set {
				mAffiliateDispatchStatuses = value;
			}
		}

		private List<DriverFee> mDriverFees;
		public List<DriverFee> DriverFees {
			get {
				if (mDriverFees == null) {
					mDriverFees = DriverFee.GetByAffiliate(this.AffiliateID);
				}
				return mDriverFees;
			}
		}


		private AffiliateBankInfo mAffiliateBankInfo;
		public AffiliateBankInfo AffiliateBankInfo {
			get {
				if (mAffiliateBankInfo == null || mAffiliateBankInfo.IsNullEntity) {
					mAffiliateBankInfo = AffiliateBankInfo.GetByAffiliateID(this.AffiliateID);
				}
				return mAffiliateBankInfo;
			}
		}

		private Markets mMarkets;
		public Markets Markets {
			get {
				if (!this.MarketID.HasValue) {
					return Markets.GetNullEntity();
				}

				if (mMarkets == null) {
					mMarkets = CabRideEngineDapper.Markets.GetMarket(this.MarketID.Value);
				}

				return mMarkets;
			}
			set {
				mMarkets = value;
			}
		}

		// Email Server Details
		private EmailServer mEmailServer;
		public EmailServer EmailServer {
			get {

				if (mEmailServer == null) {
					mEmailServer = CabRideEngineDapper.EmailServer.GetEmailServer(this.AffiliateID);
				}

				return mEmailServer;
			}
			set {
				mEmailServer = value;
			}
		}

		// Account Table 
		private Accounts mAccount;
		public Accounts Account {
			get {

				if (mAccount == null) {
					using (var conn = SqlHelper.OpenSqlConnection()) {
						mAccount = CabRideEngineDapper.Accounts.GetAccountByAffiliate(conn, this.AffiliateID);
					}
				}

				return mAccount;
			}
			set {
				mAccount = value;
			}
		}

		#endregion


		public static Affiliate Create(SqlConnection pConn) {
			Affiliate oAffiliate = null;
			try {
				oAffiliate = AffiliateTable.Create();
				oAffiliate.FleetTypeID = 1;
				oAffiliate.ServiceType = "Taxi";
				oAffiliate.PaySchedule = "0110110";
				oAffiliate.ACHPaymentSchedule = (int)ACHPaymentScheduleEnum.Custom;
				// Using the IdeaBlade Id Generation technique
				//pManager.GenerateId(oAffiliate, Affiliate.AffiliateIDEntityColumn);


				oAffiliate.AffiliateDriverDefaults = AffiliateDriverDefaults.Create(oAffiliate);

				//DriverFee fee = DriverFee.Create(oAffiliate);
				//fee.StartAmount = 100;
				//fee.EndAmount = 250;
				//fee.Fee = 5;

				//Affiliate
				oAffiliate.AffiliateFeeses = new List<AffiliateFees>();
				AffiliateFees fee = AffiliateFees.Create(oAffiliate);
				fee.StartAmount = 0.01M;
				fee.EndAmount = 20;
				fee.Fee = 2;
				oAffiliate.AffiliateFeeses.Add(fee);

				AffiliateFees fee1 = AffiliateFees.Create(oAffiliate);
				fee1.StartAmount = 20.01M;
				fee1.EndAmount = 40;
				fee1.Fee = 3;
				oAffiliate.AffiliateFeeses.Add(fee1);

				AffiliateFees fee2 = AffiliateFees.Create(oAffiliate);
				fee2.StartAmount = 40.01M;
				fee2.EndAmount = 100;
				fee2.Fee = 4.50M;
				oAffiliate.AffiliateFeeses.Add(fee2);

				AffiliateFees fee3 = AffiliateFees.Create(oAffiliate);
				fee3.StartAmount = 100.01M;
				fee3.EndAmount = 200;
				fee3.Fee = 9;
				oAffiliate.AffiliateFeeses.Add(fee3);

				AffiliateFees fee4 = AffiliateFees.Create(oAffiliate);
				fee4.StartAmount = 200.01M;
				fee4.EndAmount = 300;
				fee4.Fee = 13.50M;
				oAffiliate.AffiliateFeeses.Add(fee4);

				AffiliateFees fee5 = AffiliateFees.Create(oAffiliate);
				fee5.StartAmount = 300.01M;
				fee5.EndAmount = 400;
				fee5.Fee = 18.00M;
				oAffiliate.AffiliateFeeses.Add(fee5);

				AffiliateFees fee6 = AffiliateFees.Create(oAffiliate);
				fee6.StartAmount = 400.01M;
				fee6.EndAmount = 500;
				fee6.Fee = 22.50M;
				oAffiliate.AffiliateFeeses.Add(fee6);

				AffiliateFees fee7 = AffiliateFees.Create(oAffiliate);
				fee7.StartAmount = 500.01M;
				fee7.EndAmount = 600;
				fee7.Fee = 27.00M;
				oAffiliate.AffiliateFeeses.Add(fee7);

				AffiliateFees fee8 = AffiliateFees.Create(oAffiliate);
				fee8.StartAmount = 600.01M;
				fee8.EndAmount = 700;
				fee8.Fee = 31.50M;
				oAffiliate.AffiliateFeeses.Add(fee8);

				AffiliateFees fee9 = AffiliateFees.Create(oAffiliate);
				fee9.StartAmount = 700.01M;
				fee9.EndAmount = 800;
				fee9.Fee = 36.00M;
				oAffiliate.AffiliateFeeses.Add(fee9);

				AffiliateFees fee10 = AffiliateFees.Create(oAffiliate);
				fee10.StartAmount = 800.01M;
				fee10.EndAmount = 900;
				fee10.Fee = 40.50M;
				oAffiliate.AffiliateFeeses.Add(fee10);

				AffiliateFees fee11 = AffiliateFees.Create(oAffiliate);
				fee11.StartAmount = 900.01M;
				fee11.EndAmount = 1000;
				fee11.Fee = 45.00M;
				oAffiliate.AffiliateFeeses.Add(fee11);

				AffiliateFees fee12 = AffiliateFees.Create(oAffiliate);
				fee12.StartAmount = 1000.01M;
				fee12.EndAmount = 0;
				fee12.Fee = 50.00M;
				oAffiliate.AffiliateFeeses.Add(fee12);

				// get default country
				Countries oCountry = Countries.GetDefaultCountry(pConn);
				if (!oCountry.IsNullEntity) {
					oAffiliate.Country = oCountry.Country;
				}

			} catch (Exception ex) {
				throw ex;
			}
			return oAffiliate;
		}



		public static long GetDriverCountForAffiliate(SqlConnection pConn, long pAffiliateID) {
			if (pAffiliateID < 1) {
				return 0;
			}
			string sql = @"SELECT COUNT(*) Cnt FROM AffiliateDrivers
                            WHERE AffiliateID = @AffiliateID";
			long count = 0;
			try {
				count = pConn.Query<long>(sql, new { AffiliateID = pAffiliateID }).FirstOrDefault();
			} catch (Exception) {
			}
			return count;
		}

		public static Affiliate GetAffiliate(long pAffiliateID) {
			using (var conn = Utils.SqlHelper.OpenSqlConnection()) {
				string sql = string.Format("Select * FROM {0} WITH (NoLock) WHERE {1} = @{1}", Affiliate.TableDef.TABLE_NAME, Affiliate.TableDef.AffiliateID);
				DynamicParameters param = new DynamicParameters();
				param.Add(Affiliate.TableDef.AffiliateID, pAffiliateID);
				return conn.Query<Affiliate>(sql, param).DefaultIfEmpty(Affiliate.GetNullEntity()).FirstOrDefault();
			}
		}

		public static Affiliate GetAffiliate(SqlConnection pConn, long pAffiliateID) {
			string sql = string.Format("Select * FROM {0} WITH (NoLock) WHERE {1} = @{1}", Affiliate.TableDef.TABLE_NAME, Affiliate.TableDef.AffiliateID);
			DynamicParameters param = new DynamicParameters();
			param.Add(Affiliate.TableDef.AffiliateID, pAffiliateID);
			return pConn.Query<Affiliate>(sql, param).DefaultIfEmpty(Affiliate.GetNullEntity()).FirstOrDefault();
		}

		public static Affiliate GetAffiliateByName(SqlConnection pConn, string pName) {
			string sql = string.Format("Select * FROM {0} WITH (NoLock) WHERE {1} = @{1}", Affiliate.TableDef.TABLE_NAME, Affiliate.TableDef.Name);
			DynamicParameters param = new DynamicParameters();
			param.Add(Affiliate.TableDef.Name, pName);
			return pConn.Query<Affiliate>(sql, param).DefaultIfEmpty(Affiliate.GetNullEntity()).FirstOrDefault();
		}


		public static string GetTimeZone(SqlConnection pConn, long pAffiliateID) {
			string sql = @"SELECT TimeZone FROM dbo.Affiliate
							WHERE AffiliateID = @AffiliateID";

			return pConn.Query<string>(sql, new { AffiliateID = pAffiliateID }).FirstOrDefault();
		}


		public static Affiliate GetByAccountCode(SqlConnection pConn, string pCode) {
			string sql = @"SELECT * FROM Affiliate
							WHERE AffiliateCode = @AffiliateCode";

			return pConn.Get<Affiliate>(sql, new { AffiliateCode = pCode });
		}

		public static List<Affiliate> GetData(string pSql, DynamicParameters pParam = null) {
			using (var conn = SqlHelper.OpenSqlConnection()) {
				return GetData(conn, pSql, pParam);
			}
		}

		public static List<Affiliate> GetData(SqlConnection pConn, string pSql, DynamicParameters pParam = null) {
			return pConn.Query<Affiliate>(pSql, pParam).ToList();
		}



		//public static List<Affiliate> GetSubFleets(SqlConnection pConn, Affiliate pAffiliate) {
		//	string sql = "SELECT * FROM Affiliate WHERE ParentAffiliateID = @ParentAffiliateID";
		//	return pConn.Query<Affiliate>(sql, new { ParentAffiliateID = pAffiliate.AffiliateID }).ToList();
		//}

		public static List<Affiliate> GetSubFleets(SqlConnection pConn, long pAffiliateID) {
			string sql = "SELECT * FROM Affiliate WHERE ParentAffiliateID = @ParentAffiliateID";
			return pConn.Query<Affiliate>(sql, new { ParentAffiliateID = pAffiliateID }).ToList();
		}

		public bool IsTaxiTronicAffiliate() {
			bool bTaxiTronic = false;
			if (!this.AffiliateDriverDefaults.IsNullEntity) {
				bTaxiTronic = (!string.IsNullOrEmpty(this.AffiliateDriverDefaults.TaxiTronicMerchant) || this.TaxiTronicAffiliateID.HasValue);
			}
			return bTaxiTronic;
		}

		public bool IsTaxiTronicGPS() {
			bool bTaxiTronicGPS = false;
			if (!this.AffiliateDriverDefaults.IsNullEntity) {
				bTaxiTronicGPS = this.AffiliateDriverDefaults.TaxiTronicGPS;
			}
			return bTaxiTronicGPS;
		}

		public bool IsLimo() {
			if (this.ServiceType.IsNullOrEmpty()) {
				return false;
			}
			return this.ServiceType.Equals("limo", StringComparison.CurrentCultureIgnoreCase);
		}

		[Editable(false)]
		public string LimoTaxiPassText {
			get {
				//string sLimoTaxiPassText = "TaxiPass";
				//if (IsLimo()) {
				//    sLimoTaxiPassText = "LimoPass";
				//}
				//return sLimoTaxiPassText;
				string sLimoTaxiPassText = "TaxiPass";
				if (("" + DefaultPlatform).Equals(Platforms.ChargePass.ToString())) {
					sLimoTaxiPassText = Platforms.ChargePass.ToString();
				} else {
					if (IsLimo()) {
						sLimoTaxiPassText = "LimoPass";
					}
				}
				return sLimoTaxiPassText;
			}
		}



		public static List<Affiliate> GetAffiliatesForDropDown(SqlConnection pConn, bool pAddNullRecord = false) {
			string sql = @"SELECT Name, AffiliateID FROM dbo.Affiliate WITH (NOLOCK)
							ORDER BY Name";

			List<Affiliate> recList = GetData(pConn, sql).ToList();
			if (pAddNullRecord) {
				recList.Insert(0, Affiliate.GetNullEntity());
			}
			return recList;
		}

		public static List<Affiliate> GetAffiliatesForDropDownForNewtekAdmin(SqlConnection pConn, bool pAddNullRecord = false) {
			string sql = @"SELECT Name
								 , AffiliateID
							FROM dbo.Affiliate WITH (NOLOCK)
							WHERE ServiceType = 'Taxi' OR ServiceType LIKE 'ewr%'
							ORDER BY Name";

			List<Affiliate> recList = GetData(pConn, sql).ToList();
			if (pAddNullRecord) {
				recList.Insert(0, Affiliate.GetNullEntity());
			}
			return recList;
		}

		public static List<Affiliate> GetAffiliatesForDropDownBySalesRep(SqlConnection pConn, SalesReps pSalesRep, bool pAddNullRecord = false) {
			string sql = @"SELECT  Name
                                  , Affiliate.AffiliateID
                            FROM    Affiliate
                            LEFT JOIN SalesRepAffiliates ON SalesRepAffiliates.AffiliateID = Affiliate.AffiliateID
                            WHERE   SalesRepID = @SalesRepID
                            ORDER BY Name";

			DynamicParameters parms = new DynamicParameters();
			parms.Add("SalesRepID", pSalesRep.SalesRepID);
			List<Affiliate> recList = GetData(pConn, sql, parms).ToList();
			if (pAddNullRecord) {
				recList.Insert(0, Affiliate.GetNullEntity());
			}
			return recList;
		}


		public static Affiliate GetDefaultAffiliateForState(SqlConnection pConn, string pState) {
			States state = States.GetStateForCountry(pConn, "USA", pState);

			if (!state.StateName.IsNullOrEmpty()) {
				pState = state.StateName;
			}

			string affName = string.Format("{0} General", pState);
			string sql = @"SELECT * FROM Affiliate 
							WHERE State = @State
							AND Name = @AffName";


			Affiliate rec = pConn.Get<CabRideEngineDapper.Affiliate>(sql, new { State = pState, AffName = affName });
			if (rec.IsNullEntity) {
				rec = Affiliate.Create();
				rec.Name = affName;
				rec.State = state.State;
				if (!state.IsNullEntity) {
					Markets market = Markets.GetMarket(pConn, pState);
					if (market.IsNullEntity) {
						market = Markets.Create();
						market.Market = pState;
						market.State = state.State;
						market.Country = state.Country;
						market.TimeZone = state.TimeZone;
						market.MatchingPriority = 999;
						market.AndroidEnabled = true;
						market.Save();
					}
					rec.MarketID = market.MarketID;
				}
				string affCode = string.Format("{0}GE", state.State);
				if (affCode.Trim().Length < 4) {
					affCode = StringUtils.GetUniqueKey(4, true);
				}

				while (true) {
					sql = @"SELECT * FROM Affiliate 
							WHERE AffiliateCode = @AffCode";
					Affiliate code = pConn.Get<Affiliate>(sql, new { AffCode = affCode });
					if (code.IsNullEntity) {
						break;
					}
					affCode = StringUtils.GetUniqueKey(4);
				}

				rec.AffiliateCode = affCode;

				rec.TimeZone = state.TimeZone;
				if (rec.TimeZone.IsNullOrEmpty()) {
					rec.TimeZone = "(GMT-05:00) Eastern Time (US & Canada)";
				}
				rec.VerisignAccountID = VerisignAccounts.GetDefaultAccount(pConn).VerisignAccountID;
				rec.DefaultPlatform = Platforms.TaxiPay.ToString();

				rec.AffiliateDriverDefaults.TrackSwipeCounts = true;
				rec.AffiliateDriverDefaults.HHFareFirst = true;
				rec.Save();

				foreach (var fee in rec.AffiliateFeeses) {
					fee.AffiliateID = rec.AffiliateID;
					fee.Save();
				}
				rec.AffiliateDriverDefaults.Save();
			}

			return rec;
		}


		public static List<Affiliate> GetAutoACHEnabled(SqlConnection pConn) {
			string sql = @"SELECT * FROM dbo.Affiliate
							WHERE EnableAutoACH = 1
							ORDER BY Name";

			return pConn.Query<Affiliate>(sql).ToList();
		}


		public bool SetGeoCodes(object pTarget) {
			Affiliate oAffiliate = (Affiliate)pTarget;
			bool bOk = false;

			//validate address with Mappoint
			try {
				Address oAddress = new Address();
				if (!oAffiliate.Line1.IsNullOrEmpty()) {
					oAddress.Street = oAffiliate.Line1;
				}
				if (!oAffiliate.City.IsNullOrEmpty()) {
					oAddress.City = oAffiliate.City;
				}
				if (!oAffiliate.State.IsNullOrEmpty()) {
					oAddress.State = oAffiliate.State;
				}
				TaxiPassCommon.GeoCoding.Google.GeoResponse oGeoResponse = TaxiPassCommon.GeoCoding.Google.GetGeoCode(oAddress);
				if (oGeoResponse.Status == "OK") {
					Latitude = oGeoResponse.Results[0].Geometry.Location.Lat;
					Longitude = oGeoResponse.Results[0].Geometry.Location.Lng;
					bOk = true;
				}
			} catch (Exception ex) {
				if (ex.Message.Contains("The FindAddress method cannot find Post Office Box addresses")) {
					bOk = true;
				} else {
					// hg Dec 7, 2011 address changed need to resign up
					//e.Description = ex.Message;
					bOk = true; // false;
				}
			}
			return bOk;
		}


		public override string TimeZone {
			get {
				string myTimeZone = base.TimeZone;
				if (myTimeZone == null) {
					myTimeZone = "";
				}
				if (TimeZones.GetAll()[0].DisplayName.Contains("GMT")) {
					if (!myTimeZone.IsNullOrEmpty()) {
						myTimeZone = myTimeZone.Replace("UTC", "GMT");
					}
				} else {
					if (!myTimeZone.IsNullOrEmpty()) {
						myTimeZone = myTimeZone.Replace("GMT", "UTC");
					}
				}
				return myTimeZone;
			}
			set {
				base.TimeZone = value;
			}
		}


		[Editable(false)]
		public string ServiceSubType {
			get {
				using (var conn = SqlHelper.OpenSqlConnection()) {
					return AffiliateRef.GetByGroupReference(conn, AffiliateID, "Account", "SubType").ValueString;
				}
			}
			set {
				using (var conn = SqlHelper.OpenSqlConnection()) {
					AffiliateRef rec = AffiliateRef.GetCreate(conn, AffiliateID, "Account", "SubType", "");
					rec.ValueString = value;
				}
			}
		}

		private AffiliateRef mBankInfoOnWebSite;
		[Editable(false)]
		public bool BankInfoOnWebSite {
			get {
				CreateBankInfoOnWebSite();
				return Convert.ToBoolean(mBankInfoOnWebSite.ValueString);
			}
			set {
				CreateBankInfoOnWebSite();
				mBankInfoOnWebSite.ValueString = value.ToString();
			}
		}

		private void CreateBankInfoOnWebSite() {
			if (mBankInfoOnWebSite == null) {
				using (var conn = SqlHelper.OpenSqlConnection()) {
					mBankInfoOnWebSite = AffiliateRef.GetCreate(conn, AffiliateID, "Affiliate", "BankInfoOnWebSite", false.ToString());
				}
			}
		}


		private AffiliateRef mBillingDescriptor;
		[Editable(false)]
		public string BillingDescriptor {
			get {
				CreateBillingDescriptor();
				return mBillingDescriptor.ValueString;
			}
			set {
				CreateBillingDescriptor();
				mBillingDescriptor.ValueString = value;
			}
		}

		private void CreateBillingDescriptor() {
			if (mBillingDescriptor == null) {
				using (var conn = SqlHelper.OpenSqlConnection()) {
					mBillingDescriptor = AffiliateRef.GetCreate(conn, AffiliateID, "Affiliate", "BillingDescriptor", "");
				}
			}
		}


		private AffiliateRef mDisplayReadOnlyHideDontPayRefundFlags;
		[Editable(false)]
		public bool DisplayReadOnlyHideDontPayRefundFlags {
			get {
				CreateDisplayReadOnlyHideDontPayRefundFlags();
				return Convert.ToBoolean(mDisplayReadOnlyHideDontPayRefundFlags.ValueString);
			}
			set {
				CreateDisplayReadOnlyHideDontPayRefundFlags();
				mDisplayReadOnlyHideDontPayRefundFlags.ValueString = value.ToString();
			}
		}

		private void CreateDisplayReadOnlyHideDontPayRefundFlags() {
			if (mDisplayReadOnlyHideDontPayRefundFlags == null) {
				using (var conn = SqlHelper.OpenSqlConnection()) {
					mDisplayReadOnlyHideDontPayRefundFlags = AffiliateRef.GetCreate(conn, this.AffiliateID, "Affiliate", "DisplayReadOnlyHideDontPayRefundFlags", false.ToString());
				}
			}
		}


		private AffiliateRef mFundPayCardDelayed;
		[Editable(false)]
		public bool FundPayCardDelayed {
			get {
				CreateFundPayCardDelayed();
				return Convert.ToBoolean(mFundPayCardDelayed.ValueString);
			}
			set {
				CreateFundPayCardDelayed();
				mFundPayCardDelayed.ValueString = value.ToString();
			}
		}

		private void CreateFundPayCardDelayed() {
			if (mFundPayCardDelayed == null) {
				using (var conn = SqlHelper.OpenSqlConnection()) {
					mFundPayCardDelayed = AffiliateRef.GetCreate(conn, AffiliateID, "Affiliate", "FundPayCardDelayed", false.ToString());
				}
			}
		}


		//--
		private AffiliateRef mFundPayCardDelayedMinusHours;
		[Editable(false)]
		public int FundPayCardDelayedMinusHours {
			get {
				CreateFundPayCardDelayedMinusHours();
				return Convert.ToInt32(mFundPayCardDelayedMinusHours.ValueString);
			}
			set {
				CreateFundPayCardDelayedMinusHours();
				mFundPayCardDelayedMinusHours.ValueString = value.ToString();
			}
		}

		private void CreateFundPayCardDelayedMinusHours() {
			if (mFundPayCardDelayedMinusHours == null) {
				using (var conn = SqlHelper.OpenSqlConnection()) {
					mFundPayCardDelayedMinusHours = AffiliateRef.GetCreate(conn, AffiliateID, "Affiliate", "FundPayCardDelayedMinusHours", 0.ToString());
				}
			}
		}


		private AffiliateRef mDoNotTextDrivers;
		[Editable(false)]
		public bool DoNotTextDrivers {
			get {
				CreateDoNotTextDrivers();
				return Convert.ToBoolean(mDoNotTextDrivers.ValueString);
			}
			set {
				CreateDoNotTextDrivers();
				mDoNotTextDrivers.ValueString = value.ToString();
			}
		}

		private void CreateDoNotTextDrivers() {
			if (mDoNotTextDrivers == null) {
				using (var conn = SqlHelper.OpenSqlConnection()) {
					mDoNotTextDrivers = AffiliateRef.GetCreate(conn, AffiliateID, "Affiliate", "DoNotTextDrivers", false.ToString());
				}
			}
		}


		private AffiliateRef mManualTransFee;
		[Editable(false)]
		public decimal ManualTransFee {
			get {
				CreateManualTransFee();
				return mManualTransFee.ValueString.AsDecimal();
			}
			set {
				CreateManualTransFee();
				mManualTransFee.ValueString = value.ToString();
			}
		}

		private void CreateManualTransFee() {
			if (mManualTransFee == null) {
				using (var conn = SqlHelper.OpenSqlConnection()) {
					mManualTransFee = AffiliateRef.GetCreate(conn, AffiliateID, "Affiliate", "ManualTransFee", "0.00");
				}
			}
		}

		public Gateway GetGateway(CardProcessors pProcessor, CreditCardChargeType pType, string pCardType) {
			using (var conn = SqlHelper.OpenSqlConnection()) {
				List<IGatewayCredentials> acctList = CreditCardUtils.LoadGateWays(conn, VerisignAccountID.Value, pType, pCardType);
				if (acctList.Count == 1) {
					return new Gateway(acctList[0]);
				}
				return new Gateway(acctList.FirstOrDefault(p => p.MyGateway() == pProcessor));
			}
		}

		public List<Gateway> GetGateways(CreditCardChargeType pType, string pCardType) {
			using (var conn = SqlHelper.OpenSqlConnection()) {
				List<IGatewayCredentials> acctList = CreditCardUtils.LoadGateWays(conn, VerisignAccountID.Value, pType, pCardType);
				List<Gateway> gateWayList = new List<Gateway>();

				foreach (IGatewayCredentials rec in acctList) {
					gateWayList.Add(new Gateway(rec));
				}

				return gateWayList;
			}
		}


		public VerisignAccounts MyVerisignAccount {
			get {
				using (var conn = SqlHelper.OpenSqlConnection()) {
					if (VerisignAccountID.HasValue) {
						VerisignAccounts acct = VerisignAccounts.GetAccount(conn, VerisignAccountID.Value);
						if (acct.IsNullEntity) {
							return VerisignAccounts.GetDefaultAccount(conn);
						}
						return VerisignAccounts.GetAccount(conn, VerisignAccountID.Value);
					}
					return VerisignAccounts.GetDefaultAccount(conn);
				}
			}
		}

		private AffiliateRef mHHShowTolls;
		[Editable(false)]
		public bool HHShowTolls {
			get {
				CreateHHShowTolls();
				return mHHShowTolls.ValueString.AsBool();
			}
			set {
				CreateHHShowTolls();
				mHHShowTolls.ValueString = value.ToString();
			}
		}

		private void CreateHHShowTolls() {
			if (mHHShowTolls == null) {
				using (var conn = SqlHelper.OpenSqlConnection()) {
					mHHShowTolls = AffiliateRef.GetCreate(conn, this.AffiliateID, "AffiliateDriverDefaults", "HHShowTolls", false.ToString());
				}
			}
		}

		private AffiliateRef mEChecksOnly;
		[Editable(false)]
		public bool EChecksOnly {
			get {
				CreateEChecksOnly();
				return mEChecksOnly.ValueString.AsBool();
			}
			set {
				CreateEChecksOnly();
				mEChecksOnly.ValueString = value.ToString();
			}
		}

		private void CreateEChecksOnly() {
			if (mEChecksOnly == null) {
				using (var conn = SqlHelper.OpenSqlConnection()) {
					mEChecksOnly = AffiliateRef.GetCreate(conn, this.AffiliateID, "Affiliate", "EChecksOnly", false.ToString());
				}
			}
		}

		private AffiliateRef mDoNotRefundTPFee;
		[Editable(false)]
		public bool DoNotRefundTPFee {
			get {
				CreateDoNotRefundTPFee();
				return mDoNotRefundTPFee.ValueString.AsBool();
			}
			set {
				CreateDoNotRefundTPFee();
				mDoNotRefundTPFee.ValueString = value.ToString();
			}
		}

		private void CreateDoNotRefundTPFee() {
			if (mDoNotRefundTPFee == null) {
				using (var conn = SqlHelper.OpenSqlConnection()) {
					mDoNotRefundTPFee = AffiliateRef.GetCreate(conn, this.AffiliateID, "Affiliate", "DoNotRefundTPFee", false.ToString());
				}
			}
		}

		private AffiliateRef mBillPayRequireApproval;
		[Editable(false)]
		public bool BillPayRequirePaymentApproval {
			get {
				CreateBillPayRequireApproval();
				return mBillPayRequireApproval.ValueString.AsBool();
			}
			set {
				CreateBillPayRequireApproval();
				mBillPayRequireApproval.ValueString = value.ToString();
			}
		}

		private void CreateBillPayRequireApproval() {
			if (mBillPayRequireApproval == null) {
				using (var conn = SqlHelper.OpenSqlConnection()) {
					mBillPayRequireApproval = AffiliateRef.GetCreate(conn, this.AffiliateID, "Affiliate", "BillPayRequireApproval", false.ToString());
				}
			}
		}
	}
}
