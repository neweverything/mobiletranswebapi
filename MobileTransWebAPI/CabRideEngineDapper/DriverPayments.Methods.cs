﻿using CabRideEngineDapper.Enums;
using CabRideEngineDapper.StoredProc;
using CabRideEngineDapper.Utils;
using Dapper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using TaxiPassCommon;
using TaxiPassCommon.Banking;

namespace CabRideEngineDapper {
	public partial class DriverPayments {


		// ********************************************************************************
		public void CheckSuspendAccount(CardProcessingException ex) {
			try {
				if (ex.ErrorNo != 3) {
					if (!this.Affiliate.AffiliateDriverDefaults.ExcludeFromRedeemerStopPayment.GetValueOrDefault(false)) {

						if (!this.AffiliateDrivers.IsNullEntity) {
							if (!this.AffiliateDrivers.RedeemerStopPayment) {
								DateTime localTime = this.Affiliate.LocalTime;
								string note = $"{localTime.ToString("MM/dd/yyyy HH:mm tt")} -> Auto Fraud Collars tripped {ex.MyMessage}";

								this.AffiliateDrivers.RedeemerStopPayment = true;

								//TimeZoneConverter timeZone = new TimeZoneConverter();
								//DateTime myTime = timeZone.ConvertTime(DateTime.Now, TimeZone.CurrentTimeZone.StandardName, this.Affiliate.TimeZone);
								//if (this.AffiliateDrivers.Notes.IsNullOrEmpty()) {
								//    this.AffiliateDrivers.Notes += "\n";
								//}
								this.AffiliateDrivers.Notes = $"{note}\r\n{this.AffiliateDrivers.Notes}";
								this.AffiliateDrivers.StopPaymentDate = localTime;
								this.AffiliateDrivers.Save();
								using (var conn = SqlHelper.OpenSqlConnection()) {
									AffiliateDriverRef drvRef = AffiliateDriverRef.GetOrCreate(conn, this.AffiliateDriverID.Value, "StopPayment", "Note");
									drvRef.ValueString = note;
									drvRef.Save();
								}
							}
						} else {
							//this.Drivers.FraudSuspected = true;
							//this.Drivers.Save();
						}
					}
				}
			} catch (Exception) {
			}
		}

		/// <summary>
		/// Sends the out fraud message.
		/// </summary>
		/// <param name="pTransBlocked">if set to <c>true</c> [trans blocked].</param>
		public void SendOutFraudMessage(bool pTransBlocked) {
			SendOutFraudMessage(pTransBlocked, "");
		}

		// **********************************************************************************************
		//
		// **********************************************************************************************
		public void SendOutFraudMessage(bool pTransBlocked, string pMsg) {

			if (!this.Notes.IsNullOrEmpty()) {
				this.Notes += " - ";
			}
			this.Notes = pMsg;
			try {
				this.DriverPaymentNotes.Save();
			} catch (Exception) {
			}

			StringBuilder body = new StringBuilder();

			using (var conn = SqlHelper.OpenSqlConnection()) {

				SystemDefaults oDefaults = SystemDefaults.GetDefaults(conn);
				if (!oDefaults.FraudAlertEMail.IsNullOrEmpty()) {
					try {
						if (!this.AffiliateDrivers.IsNullEntity) {
							body.Append("Affiliate: ");
							body.Append(this.AffiliateDrivers.Affiliate.Name);
							body.Append("<br />");
							body.Append("Affiliate Driver ID: ");
							body.Append(this.AffiliateDrivers.AffiliateDriverID);
							body.Append("<br />");
							body.Append("Affiliate Driver: ");
							body.Append(this.AffiliateDrivers.Name);
							body.Append("<br />");
							body.Append("Cell: ");
							body.Append(this.AffiliateDrivers.Cell);
							body.Append("<br /><br />");
						}

						body.Append("Driver ID: ");
						body.Append(this.Drivers.DriverID);
						body.Append("<br />");
						body.Append("Driver: ");
						body.Append(this.Drivers.Name);
						body.Append("<br />");
						body.Append("Cell: ");
						body.Append(this.Drivers.Cell.FormattedPhoneNumber());
						body.Append("<br /><br />");
						body.Append("Reason: ");
						body.Append((this.DriverPaymentNotes.Reason.IsNullOrEmpty() ? pMsg : this.DriverPaymentNotes.Reason));
						body.Append("<br /><br />");

						body.Append("Trans No: ");
						body.Append(this.TransNo);
						body.Append("<br />");
						body.Append("Card No: ");
						body.Append(this.CardNumberDisplay);
						body.Append("<br />");

						var cred = SystemDefaultsDict.GetEmailCreds(EMailReportFrom.Report);
						TaxiPassCommon.Mail.EMailMessage mail = new TaxiPassCommon.Mail.EMailMessage(cred.From, pTransBlocked ? "Fraud Alert" : "Stop Payment", body.ToString(), cred.Password);
						mail.UseWebServiceToSendMail = Properties.Settings.Default.EMailViaWebService;
						SystemDefaultsDict oSysDict = SystemDefaultsDict.GetByKey(conn, "Stop Payment", "EmailRecipients", false);
						mail.SendMail(oSysDict.ValueString);
					} catch (Exception) {

					}
				}
			}
		}


		// *********************************************
		// Check for blacklisted cards
		// *********************************************
		public bool BlackListedCard(SqlConnection pConn, string pCardNo) {
			bool Blacklisted = false;

			List<BlackListCards> oBlackList = usp_BlackListCardsGetByNumber.Execute(pConn, TaxiPassCommon.Cryptography.CryptoFns.MD5HashUTF16ToString(pCardNo));

			foreach (var rec in oBlackList) {
				string cardNo = rec.CardNo; // need to do it this way as dynamic string can't process DecryptIceKey
				if (cardNo.DecryptIceKey().Equals(pCardNo)) {
					this.Failed = true;
					this.Notes = "Card not accepted 1";
					this.AuthDate = DateTime.Now;
					this.Save();
					Blacklisted = true;
					break;
				}
			}


			return Blacklisted;
		}

		public bool BlackListedBIN(string pCardNo) {
			bool blackListed = false;
			using (var conn = SqlHelper.OpenSqlConnection()) {
				BlackListBIN bin = BlackListBIN.GetByBIN(conn, pCardNo.Left(6));
				blackListed = !bin.IsNullEntity;
				if (blackListed) {
					this.Failed = true;
					this.Notes = "Sorry, We do not accept charges from this Card's Institution";
					this.AuthDate = DateTime.Now;
					this.Save();
				}
			}
			return blackListed;
		}

		/// <summary>
		/// Creates the dummy processor response.
		/// </summary>
		/// <returns></returns>
		private ProcessorResponse CreateDummyProcessorResponse() {
			ProcessorResponse response = new ProcessorResponse();
			response.Result = "0";
			response.Message = "Approved";
			response.Reference = "Zero charge";
			response.AuthCode = "-999";
			return response;
		}


		/// <summary>
		/// Does the driver promo charge.
		/// </summary>
		/// <param name="pPromoList">The promo list.</param>
		/// <param name="pCardNo">The card no.</param>
		/// <param name="pCardInfo">The card info (swipe).</param>
		/// <returns></returns>
		/// <exception cref="System.Exception"></exception>
		private DriverPaymentResults DoDriverPromoCharge(List<DriverPromotions> pPromoList, string pCardNo, string pCardInfo) {

			bool promoFound = false;
			DriverPromotions myPromo = null;

			foreach (DriverPromotions oPromo in pPromoList) {
				if (oPromo.AllAffiliates || oPromo.DriverPromotionAffiliateses.FirstOrDefault(p => p.AffiliateID == this.AffiliateID) != null) {
					if (DateTime.Today >= oPromo.StartDate && DateTime.Today <= oPromo.EndDate) {
						if (pCardNo.Equals(oPromo.CardNumber) && this.CVV == oPromo.CardCVV && oPromo.CardExpires.Equals(this.Expiration.Left(2) + this.Expiration.Right(2))) {
							promoFound = true;
							myPromo = oPromo;
							break;
						}
					}
				}
			}

			if (promoFound) {
				List<DriverPayments> payList = new List<DriverPayments>();
				if (this.AffiliateDrivers.IsNullEntity) {
					payList = DriverPayments.GetPaymentByCardNo(this.Drivers, pCardNo, this.TransNo);
				} else {
					payList = DriverPayments.GetPaymentByCardNo(this.AffiliateDrivers, pCardNo, this.TransNo);
				}

				if (payList.Count > 0) {
					this.Failed = true;
					this.Notes = "Card not accepted 9, promo already used";
					this.AuthDate = DateTime.Now;
					this.Save();
					throw new Exception(this.DriverPaymentNotes.Reason);
				}
			} else {
				this.Failed = true;
				this.Notes = "Promo not valid";
				this.AuthDate = DateTime.Now;
				this.Save();
			}

			this.SetFareNoTaxiPassFee = myPromo.DriverPayout;
			string sTranType = VerisignTrxTypes.Sale;

			if (TotalCharge > AuthAmount || AuthDate == null) {
				sTranType = VerisignTrxTypes.Sale;
			} else {
				//Delayed Sale
				sTranType = VerisignTrxTypes.DelayedCapture;
			}
			return ProcessResponse(myPromo, sTranType);
		}

		private string SalesClerk() {
			return "DP, " + this.DriverID + ", " + this.AffiliateDriverID.GetValueOrDefault(0);
		}


		// *********************************************
		// Do we Freeze the account
		// *********************************************
		private void CheckFreezeAccount() {
			// Check if we need to freeze the account

			if (AffiliateID == 161) {
				// Don't check Carmel drivers
				return;
			}

			try {

				using (var conn = SqlHelper.OpenSqlConnection()) {
					var rec = usp_FreezeDriver.ExecuteProc(conn, AffiliateDriverID, DriverID, DateTime.Now);
					if (rec != null && rec.AccountFrozen) {
						SystemDefaultsDict dict = SystemDefaultsDict.GetByKey(conn, "Freeze Check", "TextMsg", false);
						if (AffiliateDriverID.HasValue) {
							AffiliateDrivers.SendDriverMsg(dict.ValueString, false, true);
						}
					}
				}
			} catch (Exception) {
			}
		}

		[Editable(false)]
		public string AddPrefixToVoucherVoucherNo { get; set; }

		public void AssignUniqueTransNo(bool bFullAphabet) {
			string[] invalidVoucherNoStart = new string[] { "5", "W", "S", "M", "P", "X", "Q", "CR", "E", "Y", "Z" };
			string startVoucherNo = GetVoucherPrefix(bFullAphabet);
			int voucherNoSize = 6;

			if ((!startVoucherNo.IsNullOrEmpty() && !TransNo.StartsWith(startVoucherNo, StringComparison.CurrentCultureIgnoreCase)) || TransNo.IsNullOrEmpty()) {
				TransNo = StringUtils.GetUniqueKey(voucherNoSize, bFullAphabet, startVoucherNo);
			}

			MyTimeStats.Info.Add(new TimeStats.ElapsedInfo { Type = "Start UniqueTransNo", Elapsed = DateTime.Now.Subtract(MyTimeStats.Start).TotalSeconds });
			using (var conn = SqlHelper.OpenSqlConnection()) {
				while (true) {
					MyTimeStats.GetUniqueTransNoCount++;

					if (startVoucherNo.IsNullOrEmpty()) {
						if (invalidVoucherNoStart.FirstOrDefault(p => TransNo.StartsWith(p, StringComparison.CurrentCultureIgnoreCase)) != null) {
							TransNo = StringUtils.GetUniqueKey(voucherNoSize, bFullAphabet, startVoucherNo);
							continue;
						}
					}
					if (!OriginalTransNo.IsNullOrEmpty()) {
						TransNo = string.Format("{0}-{1}", OriginalTransNo, TransNo.Left(3));
					}

					TransNo = AddPrefixToVoucherVoucherNo + TransNo;

					string sql = @"SELECT  DriverPaymentID
                                FROM    DriverPayments
                                WHERE   TransNo = @TransNo
                                        AND DriverPaymentID != @DriverPaymentID";

					List<long> oList = conn.Query<long>(sql, new { TransNo = this.TransNo, DriverPaymentID = this.DriverPaymentID }).ToList();
					if (oList.Count < 1) {
						break;
					}
					TransNo = StringUtils.GetUniqueKey(voucherNoSize, bFullAphabet, startVoucherNo);

				}
			}
			MyTimeStats.Info.Add(new TimeStats.ElapsedInfo { Type = "End UniqueTransNo", Elapsed = DateTime.Now.Subtract(MyTimeStats.Start).TotalSeconds });
		}


		private string GetVoucherPrefix(bool bFullAphabet) {
			string prefix = "";
			if (this.Platform.Equals(Platforms.CCKiosk.ToString(), StringComparison.CurrentCultureIgnoreCase)
					|| this.Platform.Equals(Platforms.NexStep.ToString(), StringComparison.CurrentCultureIgnoreCase)) {
				prefix = "K";
			} else {

				if (this.CardSwiped && (this.Platform.Equals(Platforms.Android.ToString()) || this.Platform.Equals(Platforms.iPhone.ToString()))) {
					prefix = "S";
				} else if (!this.CardSwiped && (this.Platform.Equals(Platforms.Android.ToString()) || this.Platform.Equals(Platforms.iPhone.ToString()))) {
					prefix = "M";
				} else if (this.Platform.Equals(Platforms.WebTerminal.ToString())) {
					prefix = "W";
				} else if (this.Platform.Equals(Platforms.TaxiPay.ToString())) {
					prefix = "P";
				} else if (Platform.Equals(Platforms.GetRide.ToString())) {
					prefix = "Q";
				} else if (Platform.Equals("KioskHuman", StringComparison.CurrentCultureIgnoreCase)) {
					prefix = "H";
					Platform = Platforms.Kiosk.ToString();
				} else if (Platform.Equals(Platforms.Credit.ToString())) {
					prefix = "CR";
				}

				if (Platform.Equals(Platforms.EWRA.ToString()) || ("" + this.Affiliate.DefaultPlatform).Equals(Platforms.EWRA.ToString())) {
					prefix = "E";
					//} else if (Platform.Equals(Platforms.EWRBC.ToString()) || ("" + this.Affiliate.DefaultPlatform).Equals(Platforms.EWRBC.ToString())) {
					//    prefix = "Y";
				}
				if (SwipeFailed) {
					prefix = "Z";
					if (Platform.Equals(Platforms.EWRA.ToString()) || ("" + this.Affiliate.DefaultPlatform).Equals(Platforms.EWRA.ToString())) {
						prefix = "Y";
					}
				}
				if (ExceptionCharge) {
					prefix = "X";
				}
			}
			return prefix;
		}

		public static List<DriverPayments> GetByPlatform(SqlConnection pConn, string pPlatform) {
			string sql = @"SELECT * FROM dbo.DriverPayments
                            WHERE  Platform = @Platform";

			return pConn.Query<DriverPayments>(sql, new { Platform = pPlatform }).ToList();
		}

		// ****************************************************************************************************************
		// Save all the data to disk first 
		// Notify us that there was a problem saving
		// ****************************************************************************************************************
		public void SaveError(Exception ex) {
			//string work;
			var drives = System.IO.DriveInfo.GetDrives();
			var drive = drives.FirstOrDefault(p => p.DriveType != System.IO.DriveType.CDRom && !p.RootDirectory.Name.Equals(@"C:\"));
			string driveName = @"C:\";
			if (drive != null) {
				driveName = drive.RootDirectory.Name;
			}
			string sPath = string.Format(@"{0}DriverPaymentsSaveError\", driveName);

			if (!System.IO.Directory.Exists(sPath)) {
				System.IO.Directory.CreateDirectory(sPath);
			}

			StringBuilder sFileName = new StringBuilder("DriverPayments_");
			sFileName.AppendFormat("{0}_", this.TransNo);
			sFileName.AppendFormat("{0}.txt", DateTime.Now.ToString("yyyyMMdd"));

			StringBuilder sObjects = new StringBuilder();
			string work = ObjectDisplayer.ObjectToString(this);
			string work2 = ObjectDisplayer.ObjectToXMLString(this, "");

			StringBuilder sHtml = new StringBuilder();
			sHtml.AppendLine("<hr>");
			sHtml.AppendLine(String.Format("{0} DriverPayments Save Error ({1})<br>", DateTime.Now.ToString(), this.TransNo));
			sHtml.AppendLine(String.Format("File Created: {0}", sPath + sFileName));
			sHtml.AppendLine(String.Format("Executing App: {0}", System.AppDomain.CurrentDomain.BaseDirectory));

			sHtml.AppendLine("<br><br>** Error Info **<br>");
			sHtml.AppendLine(String.Format("<br>TransNo: {0}", this.TransNo));
			sHtml.AppendLine("Message: " + ex.Message + "<br>");

			if (ex.InnerException != null) {
				sHtml.AppendLine("<br>");
				sHtml.AppendLine("Inner Message: " + ex.InnerException.Message + "<br>");
			}
			sHtml.AppendLine("<br><br>Stack Trace: " + ex.StackTrace + "<br>");

			sHtml.AppendLine("Entities with Errors<br><br>");
			sHtml.AppendLine(ObjectDisplayer.ObjectToXMLString(this, "").EncryptIceKey());
			sHtml.AppendLine(string.Format("{0} - {1}", this.GetType().Name, JsonConvert.SerializeObject(this).EncryptIceKey()));
			sHtml.AppendLine("<br><br><br>");


			try {
				System.IO.File.WriteAllText(sFileName.ToString(), sHtml.ToString());
			} catch {
			}

			try {
				var cred = SystemDefaultsDict.GetEmailCreds(EMailReportFrom.Report);
				TaxiPassCommon.Mail.EMailMessage oMail = new TaxiPassCommon.Mail.EMailMessage(cred.From, String.Format("DriverPayments Save Error ({0})", this.TransNo), sHtml.ToString(), cred.Password);
				oMail.UseWebServiceToSendMail = Properties.Settings.Default.EMailViaWebService;
				oMail.SendMail(Properties.Settings.Default.EMailErrorsTo);
			} catch (Exception) {
			}
		}


		// ********************************************************************************
		// 
		// ********************************************************************************
		public void FillAddress() {

			// do nothing for now, GeoNames is causing a slow down in processing

			////new Thread(new ThreadStart(() => {
			//	if (this.DriverPaymentGeoCodes.Latitude.GetValueOrDefault(0) != 0 && this.DriverPaymentGeoCodes.Longitude.GetValueOrDefault(0) != 0 && this.DriverPaymentGeoCodes.Address.IsNullOrEmpty()) {
			//		try {

			//			try {
			//				com.TaxiPass.GeoCodeWS.GeoCodeService oService = new CabRideEngine.com.TaxiPass.GeoCodeWS.GeoCodeService();
			//				com.TaxiPass.GeoCodeWS.Address oAddr = oService.GetAddressByGeoCode(this.DriverPaymentGeoCodes.Latitude.Value.ToString(), this.DriverPaymentGeoCodes.Longitude.Value.ToString());
			//				this.DriverPaymentGeoCodes.Address = oAddr.Street;
			//				if (!DriverPaymentGeoCodes.Address.IsNullOrEmpty() && oAddr.Distance > 0) {
			//					this.DriverPaymentGeoCodes.Address += " within " + oAddr.Distance + " feet";
			//				}
			//				this.DriverPaymentGeoCodes.Save();
			//			} catch {
			//			}
			//		} catch {
			//		}
			//	}

			//	if (this.DriverPaymentGeoCodes.StartLatitude.GetValueOrDefault(0) != 0 && this.DriverPaymentGeoCodes.StartLongitude.GetValueOrDefault(0) != 0 && this.DriverPaymentGeoCodes.StartAddress.IsNullOrEmpty()) {
			//		try {
			//			if (this.DriverPaymentGeoCodes.StartLatitude != this.DriverPaymentGeoCodes.Latitude && this.DriverPaymentGeoCodes.StartLongitude.GetValueOrDefault(0) != this.DriverPaymentGeoCodes.Longitude) {
			//				if (DriverPaymentGeoCodes.Address.IsNullOrEmpty()) {
			//					try {
			//						com.TaxiPass.GeoCodeWS.GeoCodeService oService = new CabRideEngine.com.TaxiPass.GeoCodeWS.GeoCodeService();
			//						com.TaxiPass.GeoCodeWS.Address oAddr = oService.GetAddressByGeoCode(this.DriverPaymentGeoCodes.StartLatitude.Value.ToString(), this.DriverPaymentGeoCodes.StartLongitude.Value.ToString());
			//						this.DriverPaymentGeoCodes.StartAddress = oAddr.Street;
			//						if (!DriverPaymentGeoCodes.StartAddress.IsNullOrEmpty() && oAddr.Distance > 0) {
			//							this.DriverPaymentGeoCodes.StartAddress += " within " + oAddr.Distance + " feet";
			//						}
			//						this.DriverPaymentGeoCodes.Save();
			//					} catch {
			//					}
			//				} else {
			//					this.DriverPaymentGeoCodes.StartAddress = this.DriverPaymentGeoCodes.Address;
			//				}
			//			}
			//		} catch {
			//		}
			//	}
			////})).Start();
		}

		// **********************************************************************************************
		//
		// **********************************************************************************************
		public void DoPerTransactionRecurringFee() {
			StringBuilder sWork = new StringBuilder();

			if (this.ChargeBack || this.Drivers.DriverRecurringFeeses.Count == 0) {
				return;
			}
			List<DriverRecurringFees> fees = (from p in this.Drivers.DriverRecurringFeeses
											  where p.ChargePerTrans && !p.Cancelled && !p.Closed
											  select p).ToList();
			if (fees.Count == 0) {
				return;
			}
			using (var conn = SqlHelper.OpenSqlConnection()) {
				foreach (DriverRecurringFees oFee in fees) {
					//if we already charged for this transaction, don't charge again
					DriverPayments recurringfee = GetPerTransactionRecurringFee(false);
					if (!recurringfee.IsNullEntity) {
						return;
					}

					string sql = @"SELECT  SUM(Fare + Gratuity + AirportFee + MiscFee - Discount)
                                FROM    DriverPayments WITH (NOLOCK)
                                WHERE   ChargeBy = @ChargeBy
                                        AND ReferenceNo = @ReferenceNo";


					decimal amountPaid = conn.Query<decimal>(sql, new { ChargeBy = RECURRING_FEES, ReferenceNo = "D" + oFee.DriverRecurringFeeID }).FirstOrDefault();
					amountPaid = Math.Abs(amountPaid);
					decimal chargeFee = oFee.Amount;
					decimal balance = oFee.MaxTotalCharges - amountPaid;
					if (oFee.MaxTotalCharges > 0) {
						if ((oFee.MaxTotalCharges - amountPaid) < chargeFee) {
							chargeFee = oFee.MaxTotalCharges - amountPaid;
						}
						if (balance == 0) {
							oFee.Closed = true;
							oFee.Save();
							return;
						}
					}

					DriverPayments oPayment = DriverPayments.Create(this.Drivers);
					oPayment.AffiliateDriverID = this.AffiliateDriverID;
					oPayment.AffiliateID = this.AffiliateID;
					oPayment.ChargeBack = true;
					oPayment.ChargeBackDate = DateTime.Now;
					oPayment.ChargeDate = oPayment.ChargeBackDate;

					sWork.Remove(0, sWork.Length);
					if (oFee.MaxTotalCharges > 0) {
						sWork.Append(oFee.ExpenseTypes.Description);
						sWork.Append(" (Total Charge: ");
						sWork.Append(oFee.MaxTotalCharges.ToString("C"));
						sWork.Append(" -  Total Paid: ");
						sWork.Append(amountPaid.ToString("C"));
						sWork.Append(" = ");
						sWork.Append(balance.ToString("C"));
						sWork.Append(" Payment: ");
						sWork.Append(chargeFee.ToString("C"));
						sWork.Append(" -> New Balance: ");
						sWork.Append((balance - chargeFee).ToString("C"));
						sWork.Append(")");
					} else {
						sWork.Append(oFee.ExpenseTypes.Description);
						sWork.Append(" (Charge: ");
						sWork.Append(oFee.Amount.ToString("C"));
						sWork.Append(")");
					}
					oPayment.ChargeBackInfo = sWork.ToString();

					oPayment.SetFareNoTaxiPassFee = -chargeFee;
					oPayment.ChargeBy = RECURRING_FEES;
					oPayment.PreAuthBy = this.TransNo;
					oPayment.ReferenceNo = "D" + oFee.DriverRecurringFeeID.ToString("F0");
					oPayment.DriverPaid = true;
					oPayment.DriverPaidDate = oPayment.ChargeBackDate;
					oPayment.Save();

					DriverPaymentAux oAux = DriverPaymentAux.Create(oPayment);
					oAux.ChargeBackDriverPaymentID = this.DriverPaymentID;
					oAux.Save();

					this.ReceiptChargedBack = oPayment.TransNo;
					this.Save();
				}
			}
		}


		/// <summary>
		/// Does the promo payments.
		/// </summary>
		public void DoPromoPayments() {


			if (this.CreditCardProcessor.IsNullOrEmpty()
				|| this.CreditCardProcessor.Equals("DriverProm", StringComparison.CurrentCultureIgnoreCase)
				|| this.Test
				|| this.Failed
				|| this.GetSetDoNotPay) {
				return;
			}

			if (!this.AffiliateDriverID.HasValue && this.Drivers.Name.Equals(CHARGECARDS, StringComparison.CurrentCultureIgnoreCase)) {
				return;
			}

			List<DriverPromotions> promoList = DriverPromotions.GetPromotionsForAffiliate(this.Affiliate);
			var x = (from p in promoList
					 where p.VoucherCount > 0 && (p.DriverPayout > 0 || p.ReferrerPayout > 0)
					 select p).ToList();
			if (x.Count == 0) {
				return;
			}

			long fleetDriverID = this.AffiliateDriverID.GetValueOrDefault(0);  //.Value;

			promoList = new List<DriverPromotions>(x);

			foreach (DriverPromotions oPromo in promoList) {
				if (oPromo.NewDriversOnly) {
					if (this.AffiliateDrivers.IsNullEntity) {
						if (this.Drivers.SignUpDate < oPromo.StartDate) {
							string appVers = "" + this.Drivers.AppVersion;
							if (!appVers.StartsWith("a", StringComparison.CurrentCultureIgnoreCase) && appVers.StartsWith("i", StringComparison.CurrentCultureIgnoreCase)) {
								continue;
							}
						}
					} else {
						if (this.AffiliateDrivers.SignUpDate < oPromo.StartDate) {
							string appVers = "" + this.Drivers.AppVersion;
							if (!appVers.StartsWith("a", StringComparison.CurrentCultureIgnoreCase) && appVers.StartsWith("i", StringComparison.CurrentCultureIgnoreCase)) {
								continue;
							}
						}
					}
				}
				if (oPromo.Platform != this.Platform) {
					continue;
				}
				if (oPromo.SwipeOnly && !this.CardSwiped) {
					continue;
				}


				List<DriverPayments> driverPaidPromos = DriverPayments.GetPaymentsByReferenceNo(this.AffiliateDrivers, "Promo: " + oPromo.DriverPromotionID.ToString());
				if (driverPaidPromos.Count == 0) {
					driverPaidPromos = DriverPayments.GetPaymentsByReferenceNo(this.AffiliateDrivers, "Promo: " + oPromo.Title.Trim());
				}
				if (driverPaidPromos.Count == 0 && !this.Drivers.Name.Equals(CHARGECARDS, StringComparison.CurrentCultureIgnoreCase)) {
					driverPaidPromos = DriverPayments.GetPaymentsByReferenceNo(this.Drivers, "Promo: " + oPromo.DriverPromotionID.ToString());
				}
				if (driverPaidPromos.Count == 0 && !this.Drivers.Name.Equals(CHARGECARDS, StringComparison.CurrentCultureIgnoreCase)) {
					driverPaidPromos = DriverPayments.GetPaymentsByReferenceNo(this.Drivers, "Promo: " + oPromo.Title.Trim());
				}

				// 7/26/2011 had to change from Title as the string length exceeded DriverPayments.ReferenceNo
				//string temp = "Promo: " + oPromo.Title.Trim() + " | " + this.AffiliateDriverID.Value.ToString("F0");
				string temp = "Promo: " + oPromo.DriverPromotionID.ToString() + " | " + this.AffiliateDriverID.GetValueOrDefault(0).ToString("F0");

				using (var conn = SqlHelper.OpenSqlConnection()) {
					List<DriverPayments> referrerPaidPromos = DriverPayments.GetPaymentsByReferenceNo(conn, temp).ToList();
					if (referrerPaidPromos.Count == 0) {
						temp = "Promo: " + oPromo.Title.Trim() + " | " + this.AffiliateDriverID.GetValueOrDefault(0).ToString("F0");
						referrerPaidPromos = DriverPayments.GetPaymentsByReferenceNo(conn, temp).ToList();
					}

					long transCount = DriverPayments.GetChargeCountForPromos(this.AffiliateDrivers, oPromo.StartDate);
					transCount += GetChargeCountForPromos(this.Drivers, oPromo.StartDate);

					if (oPromo.OneTimeUse) {
						if (transCount >= oPromo.VoucherCount) {

							if (driverPaidPromos.Count == 0 && oPromo.DriverPayout > 0) {
								PayPromoToAffiliateDriver(oPromo);

								if (referrerPaidPromos.Count == 0) {
									PayPromoToReferrer(oPromo);
								} else {
									AffiliateDriverReferrals referral = AffiliateDriverReferrals.GetByReferredDriver(conn, this.AffiliateDriverID.Value);
									if (!referral.IsNullEntity) {
										var canPayAgain = (from p in referrerPaidPromos
														   where p.ChargeDate >= referral.ReferralDate
														   select p).ToList();
										if (canPayAgain.Count == 0) {
											PayPromoToReferrer(oPromo);
										}
									}
								}
							} else if (referrerPaidPromos.Count == 0) {
								PayPromoToReferrer(oPromo);
							} else {
								AffiliateDriverReferrals referral = AffiliateDriverReferrals.GetByReferredDriver(conn, this.AffiliateDriverID.Value);
								if (!referral.IsNullEntity) {
									var canPayAgain = (from p in referrerPaidPromos
													   where p.ChargeDate >= referral.ReferralDate
													   select p).ToList();
									if (canPayAgain.Count == 0) {
										PayPromoToReferrer(oPromo);
									}
								}
							}
						}
					} else {
						//check if driver needs another promo payment
						long paymentsProcessed = transCount / oPromo.VoucherCount;
						if (paymentsProcessed > driverPaidPromos.Count || paymentsProcessed > referrerPaidPromos.Count) {
							PayPromoToAffiliateDriver(oPromo);
							PayPromoToReferrer(oPromo);
						}
					}
				}
			}
		}


		// **************************************************************************************
		// Pay the Aff Driver's promo
		// **************************************************************************************
		private void PayPromoToAffiliateDriver(DriverPromotions pDriverPromotions) {
			if (!this.AffiliateDriverID.HasValue && this.Drivers.Name.Equals(CHARGECARDS, StringComparison.CurrentCultureIgnoreCase)) {
				return;
			}
			if (pDriverPromotions.DriverPayout == 0) {
				return;
			}

			DriverPayments newPromo = DriverPayments.Create(this.Drivers);
			if (this.AffiliateDriverID.GetValueOrDefault(0) > 0) {
				newPromo.AffiliateDriverID = this.AffiliateDriverID;
			}
			newPromo.DriverID = this.DriverID;
			newPromo.Fare = pDriverPromotions.DriverPayout;
			newPromo.Failed = false;
			newPromo.ChargeDate = DateTime.Now;
			newPromo.FraudFailure = false;
			newPromo.Platform = pDriverPromotions.Platform;
			newPromo.ChargeBy = this.TransNo;
			newPromo.CardNumber = pDriverPromotions.CardNumber;
			newPromo.CVV = pDriverPromotions.CardCVV;
			string promoExpMonth = pDriverPromotions.CardExpires.Left(2);
			int year = Convert.ToInt32(pDriverPromotions.CardExpires.Right(2));
			if (year < 2000) {
				year += 2000;
			}
			string promoDate = promoExpMonth + year.ToString();
			newPromo.Expiration = promoDate;


			if (newPromo.AffiliateDrivers.AutoPayManual || newPromo.AffiliateDrivers.AutoPaySwipe) {
				if (!newPromo.AffiliateDrivers.BankAccountNumber.IsNullOrEmpty() || !newPromo.AffiliateDrivers.TransCardAdminNo.IsNullOrEmpty()) {
					newPromo = SetDriverPaid(newPromo);
				}
			}

			newPromo.Save();
			newPromo.ProcessResponse(pDriverPromotions, VerisignTrxTypes.Sale);
			if (pDriverPromotions.Platform == Platforms.Android.ToString() || pDriverPromotions.Platform == Platforms.iPhone.ToString()) {
				string msg = ProcessMessage(pDriverPromotions.DriverPayoutMessage, newPromo, this, false);
				if (!msg.IsNullOrEmpty()) {
					if (newPromo.AffiliateDrivers.IsNullEntity) {
						newPromo.Drivers.SendMsgViaTwilio(msg);
					} else {
						newPromo.AffiliateDrivers.SendDriverMsg(msg, false, true);
					}
				}
			} else {
				string url = "http://cabpay.com/Twilio/TwilioSayPromo.aspx?z=";
				string param = "promo=" + pDriverPromotions.DriverPromotionID.ToString("F0");
				param += "&driver=" + this.AffiliateDriverID.Value.ToString("F0");
				param += "&referrer=0";
				param += "&payment=" + newPromo.DriverPaymentID.ToString("F0");
				AffiliateDrivers.CallDriver(url + param.EncryptIceKey());
			}

		}

		// ***************************************************************************
		// 11/17 centralized/standarized the call to DriverPaid = true
		// mark as paid:
		//		Fleet set to autopay
		//		Manually entered (IVR) and not swiped
		//		Auto pay for swiped and the card was swiped
		// ***************************************************************************
		public void SetDriverPaid(bool pForcePaid) {
			if (this.Test || this.Failed || ("" + this.CardType).Equals("TPCard", StringComparison.CurrentCultureIgnoreCase) || Fare + Gratuity == 0) {
				return;
			}

			if (this.Drivers.MyAffiliate != null
				&& (this.Drivers.MyAffiliate.AutoPayFleet
				|| (this.AffiliateDrivers.AutoPayManual && !this.CardSwiped)
				|| (this.AffiliateDrivers.AutoPaySwipe && this.CardSwiped))) {

				bool driverInitiated = this.Affiliate.DriverInitiatedPayCardPayment && !this.AffiliateDrivers.TransCardNumber.IsNullOrEmpty();

				if ((!this.Drivers.PaidByRedeemer && !driverInitiated) || pForcePaid) {
					// 
					if (!this.AffiliateDrivers.RedeemerStopPayment) {
						DriverPaid = true;
						DriverPaidDate = DateTime.Now;
						RedemptionFee += this.Affiliate.AffiliateDriverDefaults.DriverInitiatedRedemptionFee;
					}
				}
			}

			if (this.Affiliate.DoNotPay && !Failed) {
				DoNotPay = true;
				DriverPaid = false;
				DriverPaidDate = null;
				DriverPaidTransNo = null;
				DriverPaymentAux aux = this.DriverPaymentAux;
				if (aux.IsNullEntity) {
					aux = DriverPaymentAux.Create(this);
				}
				aux.DoNotPay = true;
				aux.DoNotPayReasonID = DoNotPayReason.GetCreateReason("Software Licensing").DoNotPayReasonID;
			}
		}

		// **************************************************************************
		// Overload of default SetDriverPaid()
		// **************************************************************************
		public DriverPayments SetDriverPaid(DriverPayments pDriverPayments) {
			if (pDriverPayments.Test || pDriverPayments.Failed || ("" + this.CardType).Equals("TPCard", StringComparison.CurrentCultureIgnoreCase)) {
				return pDriverPayments;
			}
			if (pDriverPayments.Drivers.MyAffiliate != null
				&& (pDriverPayments.Drivers.MyAffiliate.AutoPayFleet
				|| (pDriverPayments.AffiliateDrivers.AutoPayManual && !pDriverPayments.CardSwiped)
				|| (pDriverPayments.AffiliateDrivers.AutoPaySwipe && pDriverPayments.CardSwiped))) {

				if (!pDriverPayments.Drivers.PaidByRedeemer && !pDriverPayments.Affiliate.DriverInitiatedPayCardPayment) {
					if (!pDriverPayments.AffiliateDrivers.RedeemerStopPayment) {
						DriverPaid = true;
						DriverPaidDate = DateTime.Now;
						RedemptionFee += pDriverPayments.Affiliate.AffiliateDriverDefaults.DriverInitiatedRedemptionFee;
					}
				}
			}
			return pDriverPayments;
		}


		/// <summary>
		/// Pays the promo to referrer.
		/// </summary>
		/// <param name="pDriverPromotions">The driver promotions.</param>
		private void PayPromoToReferrer(DriverPromotions pDriverPromotions) {
			//check if driver was referred
			if (!this.AffiliateDriverID.HasValue) {
				return;
			}
			if (pDriverPromotions.ReferrerPayout == 0) {
				return;
			}

			using (var conn = SqlHelper.OpenSqlConnection()) {
				AffiliateDriverReferrals referral = AffiliateDriverReferrals.GetByReferredDriver(conn, this.AffiliateDriverID.Value);
				if (referral.IsNullEntity) {
					return;
				}

				//if android only pay when driver swipes card
				if (("" + referral.Platform).StartsWith(Platforms.Android.ToString(), StringComparison.CurrentCultureIgnoreCase)) {
					if (GetChargeCountForPromos(this.AffiliateDrivers, referral.ReferralDate, true) == 0) {
						return;
					}
				}

				// get the driver account of the sales rep
				Drivers salesRepDriver = Drivers.GetDriverByCell(conn, referral.AffiliateDrivers.Cell);
				if (salesRepDriver.IsNullEntity) {
					salesRepDriver = Drivers.GetChargeCard(conn, referral.AffiliateDrivers.AffiliateID);
				}

				DriverPayments newPromo = DriverPayments.Create(salesRepDriver);
				newPromo.AffiliateDriverID = referral.AffiliateDriverID;
				newPromo.Fare = pDriverPromotions.ReferrerPayout;
				newPromo.Failed = false;
				newPromo.ChargeDate = DateTime.Now;
				newPromo.FraudFailure = false;
				newPromo.Platform = pDriverPromotions.Platform;
				newPromo.ChargeBy = this.TransNo;
				newPromo.CardNumber = pDriverPromotions.CardNumber;
				newPromo.CVV = pDriverPromotions.CardCVV;
				string promoExpMonth = pDriverPromotions.CardExpires.Left(2);
				int year = Convert.ToInt32(pDriverPromotions.CardExpires.Right(2));
				if (year < 2000) {
					year += 2000;
				}
				string promoDate = promoExpMonth + year.ToString();
				newPromo.Expiration = promoDate;

				if (newPromo.AffiliateDrivers.AutoPayManual || newPromo.AffiliateDrivers.AutoPaySwipe) {
					if (!newPromo.AffiliateDrivers.BankAccountNumber.IsNullOrEmpty() || !newPromo.AffiliateDrivers.TransCardAdminNo.IsNullOrEmpty()) {
						newPromo = SetDriverPaid(newPromo);
					}
				}

				newPromo.Save();
				newPromo.ProcessResponse(pDriverPromotions, VerisignTrxTypes.Sale, this.AffiliateDriverID.Value);

				// Only send a message if there is one to send
				if (!pDriverPromotions.ReferrerPayoutMessage.IsNullOrEmpty()) {
					string msg = DriverPayments.ProcessMessage(pDriverPromotions.ReferrerPayoutMessage, newPromo, this, false);
					//We are sending 2 driverpayments because we need referred(this) driver information and referrer(newPromo) driver information
					newPromo.AffiliateDrivers.SendDriverMsg(msg, false, true);
				}
			}
		}


		public DateTime? SetChargeDate(DateTime? pDate) {

			try {
				if (pDate != null && pDate.HasValue) {
					ChargeDate = pDate.Value;
					if (this.Drivers.MyAffiliate != null && !this.Drivers.MyAffiliate.TimeZone.IsNullOrEmpty()) {
						TimeZoneConverter oTimeZone = new TimeZoneConverter();
						ChargeDate = oTimeZone.ConvertTime(pDate.Value, TimeZone.CurrentTimeZone.StandardName, this.Drivers.MyAffiliate.TimeZone);
					}
				} else {
					ChargeDate = pDate;
				}
			} catch (Exception) {
				ChargeDate = pDate;
			}
			return ChargeDate;
		}

		public DateTime? SetAuthDate(DateTime? pDate) {

			try {
				if (pDate != null && pDate.HasValue) {
					AuthDate = pDate.Value;
					if (this.Drivers.MyAffiliate != null && !this.Drivers.MyAffiliate.TimeZone.IsNullOrEmpty()) {
						TimeZoneConverter oTimeZone = new TimeZoneConverter();
						AuthDate = oTimeZone.ConvertTime(pDate.Value, TimeZone.CurrentTimeZone.StandardName, this.Drivers.MyAffiliate.TimeZone);
					}
				} else {
					AuthDate = pDate;
				}
			} catch (Exception) {
				AuthDate = pDate;
			}
			return AuthDate;
		}

		/// <summary>
		/// Voids a voucher.
		/// </summary>
		/// <param name="pDriverPaymentsReference">The reference no from the processor.</param>
		/// <returns></returns>
		public DriverPaymentResults VoidTrans(SqlConnection pConn, DriverPaymentResults pDriverPaymentResults = null) {
			TaxiPassCommon.Banking.CardProcessors oProcessor = TaxiPassCommon.Banking.CardProcessors.Verisign;

			if (!string.IsNullOrEmpty(this.CreditCardProcessor)) {
				oProcessor = StringUtils.StringToEnum<TaxiPassCommon.Banking.CardProcessors>(this.CreditCardProcessor);
			}
			CreditCardChargeType chargeType = CreditCardChargeType.ECommerce;
			if (Platform != Platforms.GetRide.ToString()) {
				if (CardSwiped) {
					chargeType = CreditCardChargeType.Swipe;
				} else {
					chargeType = CreditCardChargeType.Manual;
				}
			}


			Gateway gway = null;
			//VerisignAccounts oAcct = this.Affiliate.MyVerisignAccount;
			//Gateway gway = oAcct.GetGateway(oProcessor, VerisignTrxTypes.Credit.ToString());
			if (VerisignAccountID.HasValue && VerisignAccountID.Value > 0) {
				List<IGatewayCredentials> acctList = Utils.CreditCardUtils.LoadGateWays(pConn, VerisignAccountID.Value, chargeType, CardType);
				gway = new Gateway(acctList.FirstOrDefault(p => p.MyGateway() == oProcessor));
			} else {
				gway = Affiliate.GetGateway(oProcessor, chargeType, CardType);
			}
			DriverPaymentResults approved = pDriverPaymentResults;
			if (approved == null) {
				approved = DriverPaymentResults.GetSaledApprovedResult(this);
			}
			ProcessorResponse response = gway.Void(new CCRequestRefNo(approved.Reference, Math.Round(this.TotalCharge, 2), this.Test, string.Format("AD:{0} D:{1}", AffiliateDriverID, DriverID), TransNo, IsGetRideTrans, this.CardNumberDisplay, 0, 0));
			return ProcessResponse(response, VerisignTrxTypes.Void, this.TotalCharge, null);
		}


		public static List<DriverPayments> GetPaymentAndRefunds(SqlConnection pConn, string pTransNo) {
			//string sql = @"SELECT vw_CCTransactions.*
			//                     , VTSRowID
			//                FROM   vw_CCTransactions
			//                LEFT JOIN DriverPaymentAux ON DriverPaymentAux.DriverPaymentID = vw_CCTransactions.DriverPaymentID
			//                OUTER APPLY (   SELECT TOP 1 Reference + ' - ' + Message AS Message
			//                                FROM   DriverPaymentResults
			//                                WHERE  DriverPaymentID = vw_CCTransactions.DriverPaymentID
			//                                       AND Reference = 'Zero Charge'
			//                            ) ZeroCharge
			//                WHERE  TransNo LIKE @TransNo
			//                UNION
			//                SELECT vw_CCTransactions.*
			//                     , VTSRowID
			//                FROM   vw_CCTransactions
			//                LEFT JOIN DriverPaymentAux ON DriverPaymentAux.DriverPaymentID = vw_CCTransactions.DriverPaymentID
			//                OUTER APPLY (   SELECT TOP 1 Reference + ' - ' + Message AS Message
			//                                FROM   DriverPaymentResults
			//                                WHERE  DriverPaymentID = vw_CCTransactions.DriverPaymentID
			//                                       AND Reference = 'Zero Charge'
			//                            ) ZeroCharge
			//                WHERE  vw_CCTransactions.DriverPaymentID IN (   SELECT DriverPaymentID
			//                                                                FROM   DriverPaymentRef WITH (NOLOCK)
			//                                                                WHERE  ReferenceGroup = 'Refund'
			//                                                                       AND ReferenceKey = 'Original TransNo'
			//                                                                       AND ValueString LIKE @TransNo
			//                                                            )
			//                UNION
			//                SELECT vw_CCTransactions.*
			//                     , VTSRowID
			//                FROM   vw_CCTransactions
			//                LEFT JOIN DriverPaymentAux ON DriverPaymentAux.DriverPaymentID = vw_CCTransactions.DriverPaymentID
			//                WHERE TransNo IN (SELECT ReceiptChargedBack FROM dbo.DriverPayments
			//                                  WHERE TransNo LIKE @TransNo)
			//                ORDER BY ChargeDate";

			string sql = @"SELECT *
                        FROM   dbo.DriverPayments
                        WHERE  TransNo LIKE @TransNo
                        UNION
                        SELECT *
                        FROM   dbo.DriverPayments
                        WHERE  DriverPaymentID IN (   SELECT DriverPaymentID
                                                      FROM   DriverPaymentRef WITH (NOLOCK)
                                                      WHERE  ReferenceGroup = 'Refund'
                                                             AND ReferenceKey = 'Original TransNo'
                                                             AND ValueString LIKE @TransNo
                                                  )
                        UNION
                        SELECT *
                        FROM   dbo.DriverPayments
                        WHERE  TransNo IN (   SELECT ReceiptChargedBack
                                              FROM   dbo.DriverPayments
                                              WHERE  TransNo LIKE @TransNo
                                          )";

			return pConn.Query<DriverPayments>(sql, new { TransNo = pTransNo + "%" }).ToList();

		}

		public static DriverPayments CreateChargeBackFee(Drivers pDriver, DriverPayments pPayment, string pChargeBackTransNo) {
			DriverPayments chargeBackFee = null;
			if (pPayment.Affiliate.ChargeBackFee > 0) {
				chargeBackFee = DriverPayments.Create(pDriver);
				chargeBackFee.OriginalTransNo = pPayment.TransNo;
				chargeBackFee.AffiliateID = pPayment.AffiliateID;
				chargeBackFee.AffiliateDriverID = pPayment.AffiliateDriverID;

				//oPayment.TransNo = textBoxTransNo.Text;
				chargeBackFee.DriverFee = pPayment.Affiliate.ChargeBackFee;
				chargeBackFee.ChargeBackDate = DateTime.Now;
				chargeBackFee.ChargeBackInfo = $"Fee for Chargeback of Voucher: {pChargeBackTransNo} with original voucher {pPayment.TransNo}";
				chargeBackFee.ChargeBack = true;
				chargeBackFee.ChargeDate = DateTime.Now;
				chargeBackFee.DriverPaid = true;
				chargeBackFee.DriverPaidDate = DateTime.Now;
				chargeBackFee.TaxiPassRedeemerID = pPayment.TaxiPassRedeemerID;
				chargeBackFee.Failed = false;
				chargeBackFee.Platform = Platforms.ChargeBackFeeRevenue.ToString();
				chargeBackFee.Save();

				DriverPaymentAux chargeBackFeeAux = DriverPaymentAux.Create(chargeBackFee);
				chargeBackFeeAux.ChargeBackDriverPaymentID = pPayment.DriverPaymentID;
				chargeBackFeeAux.Save();
			}
			return chargeBackFee;
		}

		public FundsTransferResponse CollectFromPayCard(TaxiPassRedeemers pTaxiPassRedeemer) {

			using (var conn = SqlHelper.OpenSqlConnection()) {
				decimal decAmount = 0;
				FundsTransferResponse fundResults = new FundsTransferResponse();

				AffiliateDrivers driver = AffiliateDrivers.GetDriver(AffiliateDriverID.Value);
				if (driver.TransCardNumber.IsNullOrEmpty()) {
					fundResults.ReturnCode = "-1";
					fundResults.ReturnMessage = "Driver does not have a pay card";
					return fundResults;
				}

				FSV.Credentials cred = new FSV.Credentials();
				AffiliateDriverRef rec;
				cred = conn.Query<FSV.Credentials>("usp_SystemDefaultsDict_Flattened", new { ReferenceGroup = "FSV" }, commandType: CommandType.StoredProcedure).FirstOrDefault();
				rec = AffiliateDriverRef.GetOrCreate(conn, driver.AffiliateDriverID, "FSV", "Registration");


				FSV fsv = new FSV(cred, cred.UseProductionAsBoolen);
				FSV.VerifyConnectionResponse verifyResponse = new FSV.VerifyConnectionResponse();
				verifyResponse = fsv.VerifyConnection();
				if (verifyResponse.ReturnCode != "1") {
					fundResults.ReturnCode = "-1";
					fundResults.ReturnMessage = "Could not verify connection to USBank: " + verifyResponse.ReturnMessage;
					return fundResults;
				}

				FSV.CardRegistrationResponse resp = JsonConvert.DeserializeObject<FSV.CardRegistrationResponse>(rec.ValueString);
				FSV.FundsTransfer transfer = new FSV.FundsTransfer(cred);

				AffiliateDriverRef recPassCode = AffiliateDriverRef.GetOrCreate(conn, driver.AffiliateDriverID, "PayCard", "PassCode");

				transfer.FromCardID = driver.TransCardAdminNo;
				transfer.FromCardPassCode = recPassCode.ValueString;
				transfer.ToCardID = cred.AdjustmentCard.DecryptIceKey(); // "6875635432"; cred.Certification.DecryptIceKey();

				string transNo = "";
				ACHDetail achRec = ACHDetail.GetByDriverPaymentID(DriverPaymentID);
				if (!achRec.ACHPaidDate.HasValue) {
					if (achRec.IsNullEntity) {
						achRec = ACHDetail.Create(this);
					}
					//if (pPayeeDriver.PayCardRealTimePay || pPayeeDriver.Affiliate.PayCardRealTimePay) {
					decAmount = DriverTotalPaid;

					if (decAmount == 0) {
						decAmount = DriverPaymentKioskAmount.Amount;
					}

					if (decAmount == 0) {
						achRec.ACHErrorReason = "No Fare amount found";
					} else {
						achRec.AmountPaid = decAmount;
						//transfer.TransferAmount += decAmount;
					}

					achRec.ACHErrorReason = "Collected: " + DriverTotalPaid.ToString("C");
					//}
					achRec.ACHPaidDate = Affiliate.LocalTime;
					achRec.ACHPaidTo = driver.Name;
					achRec.ACHBankAccountNumber = "PayCard: " + driver.TransCardNumber.DecryptIceKey().Right(5);

					achRec.TransCardAdminNo = driver.TransCardAdminNo;
					//achRec.Save(SessionHelper.EMail(Session));
					if (transNo.IsNullOrEmpty()) {
						transNo = TransNo;
					} else {
						transNo = string.Format("{0}-{1}", transNo, TransNo);
					}

					if (!DriverPaid && achRec.AmountPaid.GetValueOrDefault(0) != 0) {
						DriverPaid = true;
						DriverPaidDate = achRec.ACHPaidDate;
						TaxiPassRedeemerID = pTaxiPassRedeemer.TaxiPassRedeemerID;
					}
				}

				transfer.TransferAmount = -Math.Abs(achRec.AmountPaid.GetValueOrDefault(0));
				transfer.ReferenceFromCard = string.Format("ADriverID: {0}|{1}", driver.AffiliateDriverID, transNo);
				transfer.ReferenceToCard = "Payment Collected from Driver";

				var funded = fsv.TransferFromPayCard(driver.TransCardNumber.DecryptIceKey(), transfer);
				if (funded.ReturnCode == "1") {
					fundResults.ReturnCode = funded.ReturnCode;
					fundResults.ReturnMessage = funded.ReturnMessage;
					Utils.Misc.SendTransCardPaymentSMS(this, driver, "");
					achRec.Save();
					SystemDefaultsDict fundBal = SystemDefaultsDict.GetOrCreate(conn, "FSV", "FundingAccountBalance");
					fundBal.ValueString = funded.FromCardBalance;
					fundBal.Save();

					USBankPayCardPaid usBank = USBankPayCardPaid.Create();
					usBank.AdminNo = transfer.ToCardID;
					usBank.AffiliateDriverID = driver.AffiliateDriverID;
					usBank.CardEnding = driver.TransCardCardNumber.Right(5);
					usBank.DebitTransaction = transfer.TransferAmount;
					usBank.FundingTransDate = DateTime.Now;
					usBank.LastName = driver.LastName.IsNullOrEmpty() ? driver.Name : driver.LastName;
					usBank.TaxiPassRedeemerID = pTaxiPassRedeemer.TaxiPassRedeemerID;
					usBank.TransactionDetail = transfer.ReferenceFromCard;
					usBank.Save();

				} else {
					//string mailFrom = SystemDefaultsDict.GetOrCreate(conn, "Mail", "From").ValueString;

					string mailTo = SystemDefaultsDict.GetOrCreate(conn, "Mail", "ErrorsTo").ValueString;
					var emailCred = SystemDefaultsDict.GetEmailCreds(EMailReportFrom.Report);
					TaxiPassCommon.Mail.EMailMessage mail = new TaxiPassCommon.Mail.EMailMessage(emailCred, "Pay Card Collect Error", $"Error: {JsonConvert.SerializeObject(funded)}\r\n{JsonConvert.SerializeObject(transfer)}");
					mail.UseWebServiceToSendMail = true;
					mail.SendMail(mailTo);
					//fundResults.ReturnCode = funded.ReturnCode;
					//fundResults.ReturnMessage = string.Format("Payment to be posted later, error processing payment: {0}", funded.ReturnMessage);
					//driver.SendDriverMsg(string.Format("Payment of {0} to be posted later to your card", transfer.TransferAmount.ToString("C")), false);
				}

				return fundResults;
			}
		}


		public Gateway GetGateway() {
			Gateway gway = null;
			CardProcessors oProcessor = CardProcessors.Verisign;

			if (!this.CreditCardProcessor.IsNullOrEmpty()) {
				oProcessor = this.CreditCardProcessor.ToEnum<CardProcessors>();
			}

			//VerisignAccounts oAcct = this.Affiliate.MyVerisignAccount;
			//Gateway gway = oAcct.GetGateway(oProcessor, VerisignTrxTypes.Credit.ToString());
			using (var conn = SqlHelper.OpenSqlConnection()) {
				if (VerisignAccountID.HasValue && VerisignAccountID.Value > 0) {
					List<IGatewayCredentials> acctList = Utils.CreditCardUtils.LoadGateWays(conn, VerisignAccountID.Value, oProcessor, CardType);
					gway = new Gateway(acctList.FirstOrDefault(p => p.MyGateway() == oProcessor));
				} else {
					CreditCardChargeType chargeType = CreditCardChargeType.ECommerce;
					if (Platform != Platforms.GetRide.ToString()) {
						if (CardSwiped) {
							chargeType = CreditCardChargeType.Swipe;
						} else {
							chargeType = CreditCardChargeType.Manual;
						}
					}
					gway = Affiliate.GetGateway(oProcessor, chargeType, CardType);
				}
			}
			return gway;
		}


		//------------------------------------------------------------------------------------------------------------------------------------------------------------------
		// Charge eCheck
		//------------------------------------------------------------------------------------------------------------------------------------------------------------------
		//public DriverPaymentResults ChargeCheck() {
		//    DateTime elapsedStart = DateTime.Now;

		//    #region Validations
		//    // Put in to trap bogus charge card calls
		//    if ((this.Fare == 0 || Fare < this.Affiliate.AffiliateDriverDefaults.StartingFare) && !JobAdjustment) {
		//        string msg = $"Amount must be greater than {this.Affiliate.AffiliateDriverDefaults.StartingFare.ToString("C")}";
		//        if (this.DriverPaymentID > 0) {
		//            this.Notes = msg;
		//            this.Save();
		//        }
		//        throw new Exception(msg);
		//    }


		//    if (AffiliateDriverID.HasValue) {
		//        string msg = $"Affiliate Driver: {AffiliateDriverID.Value}-{AffiliateDrivers.Name},  Account Frozen cannot charge eCheck";
		//        if (!AffiliateDrivers.Active) {
		//            if (this.DriverPaymentID > 0) {
		//                this.Notes = msg;
		//                this.Save();
		//            }
		//            throw new Exception(msg);
		//        }
		//    } else {
		//        if (!Drivers.Active) {
		//            string msg = string.Format("Driver: {0}-{1},  Account Frozen cannot charge eCheck", DriverID, Drivers.Name);
		//            if (this.DriverPaymentID > 0) {
		//                this.Notes = msg;
		//                this.Save();
		//            }
		//            throw new Exception(msg);
		//        }
		//    }


		//    //failed until card is charged
		//    this.Failed = true;

		//    // hg 2011-07/28 Force a save to prevent incorrect charges from happening
		//    // Always save first to avoid incorrect voucher numbers going to Credit card processor
		//    if (this.IsNewRecord) {
		//        int tries = 0;
		//        while (tries < 3) {
		//            tries++;
		//            try {
		//                this.Save("Force Saved");
		//                break;
		//            } catch (Exception ex) {
		//                if (ex.Message.Equals("Query timeout expired")) {
		//                    continue;
		//                }
		//                throw new Exception(ex.Message);
		//            }
		//        }
		//        CreateChargeRecords();
		//        //var db = CabRideDrivenDB.Utils.SqlHelper.OpenCabRideDB();
		//        using (var conn = SqlHelper.OpenSqlConnection()) {
		//            //db.WriteEntities(ChargeList);
		//            foreach (var rec in ChargeList) {
		//                rec.Save(conn, CabRideDapperSettings.LoggedInUser);
		//            }
		//        }
		//    }

		//    MyTimeStats.Key = this.TransNo;


		//    CardProcessingFailureTypes oProcess = CardProcessingFailureTypes.OKToProcess;

		//    if (!BypassFraudCheck) {
		//        BypassFraudCheck = this.Affiliate.AffiliateDriverDefaults.ByPassFraudCheck;
		//    }


		//    if (!BypassFraudCheck && !this.Test) {
		//        oProcess = CanProcessCard(this);
		//    }

		//    switch (oProcess) {
		//        case CardProcessingFailureTypes.MaxDailyAmount:
		//        case CardProcessingFailureTypes.MaxMonthlyAmount:
		//        case CardProcessingFailureTypes.MaxTransPerMonth:
		//        case CardProcessingFailureTypes.MaxTransPerWeek:
		//        case CardProcessingFailureTypes.MaxDriverTransactions:
		//        //case CardProcessingFailureTypes.MaxCardTransactions:
		//        //case CardProcessingFailureTypes.MaxTransPerMonthPerCard:
		//        //case CardProcessingFailureTypes.MaxTransPerWeekPerCard:
		//        case CardProcessingFailureTypes.MaxWeeklyAmount:
		//        case CardProcessingFailureTypes.OKToProcess:

		//            //this.Reason = "Not yet processed";
		//            if (!this.ChargeDate.HasValue) {
		//                this.ChargeDate = this.Affiliate.LocalTime;
		//            }
		//            this.FraudFailure = false;

		//            if (oProcess != CardProcessingFailureTypes.OKToProcess) {
		//                CardProcessingException ex = new CardProcessingException("Credit Card Fraud Collars Tripped", oProcess, this.Drivers, this.AffiliateDrivers, Number);
		//                CheckSuspendAccount(ex);
		//                SendOutFraudMessage(false, ex.MyMessage);
		//            }
		//            break;

		//        default: {
		//                CardProcessingException ex = new CardProcessingException("Cannot Process Credit Card", oProcess, this.Drivers, this.AffiliateDrivers, Number);
		//                this.Notes = ex.MyMessage;
		//                this.DriverPaymentNotes.Save();
		//                this.ChargeDate = Affiliate.LocalTime;
		//                if (!this.Affiliate.AffiliateDriverDefaults.ExcludeFromRedeemerStopPayment.GetValueOrDefault(false)) {
		//                    this.FraudFailure = true;
		//                }

		//                // make sure these columns are reset
		//                this.DriverPaid = false;
		//                this.DriverPaidDate = null;

		//                this.Save();
		//                CheckSuspendAccount(ex);
		//                SendOutFraudMessage(true);
		//                throw ex;
		//            }
		//    }

		//    using (var conn = SqlHelper.OpenSqlConnection()) {
		//        //Check BlackList
		//        //if (BlackListedCard(conn, cardNo)) {
		//        //    this.Notes = "Blacklisted Card";
		//        //    this.DriverPaymentNotes.Save();
		//        //    throw new Exception("Card not accepted 6");
		//        //}

		//        ////Check BlackListBIN
		//        //if (BlackListedBIN(cardNo)) {
		//        //    this.Notes = "Blacklisted BIN";
		//        //    this.DriverPaymentNotes.Save();
		//        //    throw new Exception("Card unaccepted. Please try a different card to charge.");
		//        //}
		//        #endregion Validations

		//        DriverPaymentResults oPayResults = null;
		//        CardProcessors oProcessor = CardProcessors.Verisign;


		//        // 4/23/2013 probably obe
		//        if (((CardUtils.IsTestCard(cardNo) && !this.Drivers.MyAffiliate.MyVerisignAccount.EPayTestURL.IsNullOrEmpty())
		//            || (!CardUtils.IsTestCard(cardNo) && !this.Drivers.MyAffiliate.MyVerisignAccount.EPayProdURL.IsNullOrEmpty()))) {
		//            oProcessor = CardProcessors.USAePay;
		//        }



		//        VerisignAccountID = CreditCardUtils.GetDriverPaymentCardGatewayGroupsID(conn, this.TransNo, 0);
		//        if (UseFraudGateway && this.Affiliate.FraudVerisignAccountID.GetValueOrDefault(0) > 0 && !this.CardZIPCode.IsNullOrEmpty() && !this.CVV.IsNullOrEmpty()) {
		//            VerisignAccountID = this.Affiliate.FraudVerisignAccountID.Value;
		//        }
		//        if (VerisignAccountID == 0 && !Test) {
		//            string msg = "No Gateway assigned to Affiliate " + this.MyAffiliateName;
		//            this.Notes = msg;
		//            this.DriverPaymentNotes.Save();
		//            throw new Exception(msg);
		//        }

		//        CreditCardChargeType chargeType = CreditCardChargeType.ECommerce;
		//        if (Platform != Platforms.GetRide.ToString()) {
		//            if (CardSwiped) {
		//                chargeType = CreditCardChargeType.Swipe;
		//            } else {
		//                chargeType = CreditCardChargeType.Manual;
		//            }
		//        }
		//        Gateway gway = new Gateway(Utils.CreditCardUtils.LoadGateWays(conn, this.VerisignAccountID.Value, chargeType, CardType));
		//        usp_SystemDefaultsDialie dialieDefaults = new usp_SystemDefaultsDialie();
		//        if (this.Platform.Equals(Platforms.Dialie.ToString(), StringComparison.CurrentCultureIgnoreCase)) {
		//            dialieDefaults = usp_SystemDefaultsDialie.Execute(conn, "Dialie");
		//            if (dialieDefaults.RunChargeAsManualTransaction.AsBool()) {
		//                chargeType = CreditCardChargeType.Manual;
		//                CardSwiped = false;
		//                pCardInfo = "";
		//            }
		//        }


		//        if (!this.DriverPaymentAuthResults.IsNullEntity) {
		//            //card was auth'd previously, is same card being used for the charge?
		//            try {
		//                DriverPaymentsAudit audit = DriverPaymentsAudit.GetPayment(this);
		//                if (!audit.IsNullEntity && audit.Number != this.Number) {
		//                    pForceNewCharge = true;
		//                }
		//            } catch (Exception ex) {
		//                Console.WriteLine(ex.Message);
		//            }
		//        }
		//        if (TotalCharge > AuthAmount || AuthDate == null) {
		//            pForceNewCharge = true;
		//        }
		//        string sComment = "Driver Swipe Entry";
		//        if (pForceNewCharge) {
		//            CreditCard cardData = new CreditCard();
		//            cardData.AffiliateDriverID = AffiliateDriverID;
		//            cardData.DriverID = DriverID;
		//            if (pCardInfo.IsNullOrEmpty()) {
		//                cardData.CardNo = cardNo;
		//                string sDate = DecryptCardExpiration();
		//                string sMonth = sDate.Left(2);
		//                string sYear = sDate.Right(2);
		//                cardData.CardExpiryAsMMYY = sMonth + sYear;
		//                sComment = "Driver Manual Entry";
		//            } else {
		//                if (Platform.Equals(Platforms.Dialie.ToString(), StringComparison.CurrentCultureIgnoreCase)) {
		//                    if (dialieDefaults.ConvertToSimpleTrackData.AsBool()) {
		//                        if (pCardInfo.StartsWith("%B", StringComparison.CurrentCultureIgnoreCase)) {
		//                            string track2 = pCardInfo;
		//                            List<string> temp = track2.Split('^').ToList();
		//                            string track1 = string.Format(";{0}={1}", temp[0].Replace("%B", ""), temp.Last());
		//                            if (!track1.EndsWith("?")) {
		//                                track1 += "?";
		//                            }
		//                            cardData.Track2 = track1;
		//                        }
		//                    } else {
		//                        cardData.Track2 = pCardInfo;
		//                    }
		//                } else {
		//                    cardData.Track2 = pCardInfo;
		//                }
		//            }
		//            cardData.CVV = CVV;
		//            cardData.AvsZip = this.CardZIPCode;
		//            if (!this.CardAddress.IsNullOrEmpty()) {
		//                cardData.AvsStreet = this.CardAddress.DecryptIceKey();
		//                cardData.AvsStreet = cardData.AvsStreet.PadRight(35, ' ').Left(35).Trim();  // Max Litle size
		//            }

		//            // Soft Descriptors
		//            cardData.SoftDescriptor = FillSoftDescriptor();

		//            if (Platform.Equals(Platforms.Dialie.ToString(), StringComparison.CurrentCultureIgnoreCase) && !cardData.Track2.IsNullOrEmpty()) {
		//                if (dialieDefaults.StoreTrackData.AsBool()) {
		//                    var dpRef = CabRideEngineDapper.DriverPaymentRef.Create();
		//                    dpRef.DriverPaymentID = this.DriverPaymentID;
		//                    dpRef.ReferenceGroup = "Dialie";
		//                    dpRef.ReferenceKey = "TrackData";
		//                    dpRef.ValueString = cardData.Track2;
		//                    if (pCardInfo != cardData.Track2) {
		//                        dpRef.ValueString += " was " + pCardInfo;
		//                    }
		//                    dpRef.Save();
		//                }
		//            }


		//            // ride could be free based on discounts
		//            if (TotalCharge > 0) {
		//                List<ProcessorResponse> respList = new List<ProcessorResponse>();
		//                if (this.Test) {
		//                    // simulate approved for test  trans
		//                    ProcessorResponse resp = new ProcessorResponse() {
		//                        Amount = this.TotalCharge,
		//                        AuthCode = StringUtils.GetUniqueKey(6, true),
		//                        CreditCardProcessor = CardProcessors.TPCard,
		//                        Message = "Approved",
		//                        Reference = StringUtils.GenerateRandomHex(10),
		//                        Result = "0",
		//                        TestCard = true,
		//                        TransDate = ChargeDate.GetValueOrDefault(),
		//                        TransType = "S",
		//                        VerisignAccountID = 0
		//                    };
		//                    respList.Add(resp);
		//                    oPayResults = ProcessResponse(resp, VerisignTrxTypes.Sale, TotalCharge, null);
		//                } else {
		//                    respList = gway.ChargeCard(new CCRequest(cardData, TotalCharge, sComment, TransNo, GetServiceType(), Platform, SalesClerk(), IsGetRideTrans, ("" + this.Affiliate.DefaultPlatform).Equals(Platforms.SplitPay.ToString()) ? TaxiPassFee : 0));
		//                    foreach (ProcessorResponse resp in respList) {
		//                        oPayResults = ProcessResponse(resp, VerisignTrxTypes.Sale, TotalCharge, null);
		//                    }
		//                }
		//                //if ((Platform.Equals(Platforms.Dialie.ToString(), StringComparison.CurrentCultureIgnoreCase)
		//                //		|| Platform.Equals(Platforms.ChargePass.ToString(), StringComparison.CurrentCultureIgnoreCase))
		//                //		&& chargeType == CreditCardChargeType.Swipe && respList.FirstOrDefault(p => p.Result == "0") == null) {
		//                if (Platform.Equals(Platforms.Dialie.ToString(), StringComparison.CurrentCultureIgnoreCase)
		//                        && chargeType == CreditCardChargeType.Swipe && respList.FirstOrDefault(p => p.Result == "0") == null) {
		//                    if (dialieDefaults.SwitchToManual.AsBool()) {
		//                        chargeType = CreditCardChargeType.Manual;
		//                        cardData.CardNo = cardNo;
		//                        string sDate = DecryptCardExpiration();
		//                        string sMonth = sDate.Left(2);
		//                        string sYear = sDate.Right(2);
		//                        cardData.CardExpiryAsMMYY = sMonth + sYear;
		//                        sComment = "Driver Manual Entry";
		//                        pCardInfo = "";
		//                        cardData.Track2 = "";
		//                        CardSwiped = false;
		//                        gway = new Gateway(Utils.CreditCardUtils.LoadGateWays(conn, this.VerisignAccountID.Value, chargeType, CardType));
		//                        respList = gway.ChargeCard(new CCRequest(cardData, TotalCharge, sComment, TransNo, GetServiceType(), Platform, SalesClerk(), IsGetRideTrans));
		//                        foreach (ProcessorResponse resp in respList) {
		//                            System.Threading.Thread.Sleep(2000);
		//                            oPayResults = ProcessResponse(resp, VerisignTrxTypes.Sale, TotalCharge, null);
		//                        }
		//                    }
		//                }
		//            } else {
		//                oPayResults = ProcessResponse(CreateDummyProcessorResponse(), VerisignTrxTypes.Sale, TotalCharge, null);
		//            }
		//        } else {
		//            //Delayed Sale
		//            string sRefNo = "";
		//            string sTranType = VerisignTrxTypes.DelayedCapture;
		//            DriverPaymentResults chargeAuthResult = null;
		//            foreach (DriverPaymentResults oResult in this.DriverPaymentResultses) {
		//                if ((!oResult.IsNullEntity && (oResult.Result == "0" || oResult.Message.StartsWith("Approved", StringComparison.CurrentCultureIgnoreCase)) && oResult.TransType == VerisignTrxTypes.Authorization)) {
		//                    sRefNo = oResult.Reference;
		//                    chargeAuthResult = oResult;
		//                    break;
		//                }
		//            }
		//            if (!CreditCardProcessor.IsNullOrEmpty()) {
		//                oProcessor = this.CreditCardProcessor.ToEnum<CardProcessors>();
		//            }

		//            ProcessorResponse resp = null;
		//            gway.SetProcessor(oProcessor);
		//            if (this.Test) {
		//                resp = new ProcessorResponse() {
		//                    // simulate approved for test  trans
		//                    Amount = this.TotalCharge,
		//                    AuthCode = StringUtils.GetUniqueKey(6, true),
		//                    CreditCardProcessor = CardProcessors.TPCard,
		//                    Message = "Approved",
		//                    Reference = StringUtils.GenerateRandomHex(10),
		//                    Result = "0",
		//                    TestCard = true,
		//                    TransDate = ChargeDate.GetValueOrDefault(),
		//                    TransType = "S",
		//                    VerisignAccountID = 0
		//                };

		//            } else {
		//                resp = gway.ChargeAuth(new CCRequestRefNo(sRefNo, TotalCharge, Test, SalesClerk(), TransNo, IsGetRideTrans, this.CardNumberDisplay));
		//            }
		//            oPayResults = ProcessResponse(resp, sTranType, TotalCharge, chargeAuthResult);
		//        }

		//        // hg 4/28/11 force payment if we need to mark as AutoPaid regardless of payment type and platform
		//        // ca 9/7/11 Only set DriverPaid if the charge was successful (oPayResults.Result.Trim() == "0")
		//        if (!this.DriverPaid
		//            && oPayResults.Result.Trim() == "0"
		//            && (this.Affiliate.AutoPayFleet
		//                    || (this.AffiliateDrivers.AutoPayManual && !this.CardSwiped)
		//                    || (this.AffiliateDrivers.AutoPaySwipe && this.CardSwiped)
		//                )
		//            ) {
		//            if (!this.AffiliateDrivers.DriverInitiatedPayCardPayment
		//                && !this.Affiliate.DriverInitiatedPayCardPayment
		//                && !SystemDefaults.GetDefaults(conn).DriverInitiatedPayCardPayment) {

		//                if (!this.AffiliateDrivers.PaidByRedeemer) {
		//                    SetDriverPaid(false);
		//                }
		//            }
		//        }


		//        CheckFreezeAccount();

		//        if (this.Affiliate.DoNotPay) {
		//            DoNotPay = true;
		//            DriverPaid = false;
		//            DriverPaidDate = null;
		//            DriverPaidTransNo = null;
		//            DriverPaymentAux aux = this.DriverPaymentAux;
		//            if (aux.IsNullEntity) {
		//                aux = DriverPaymentAux.Create(this);
		//            }
		//            aux.DoNotPay = true;
		//            aux.DoNotPayReasonID = DoNotPayReason.GetCreateReason("Software Licensing").DoNotPayReasonID;
		//            aux.Save();
		//        }

		//        this.Save();

		//        if (this.DriverPaid && !this.Failed && !this.Test && !this.DoNotPay.GetValueOrDefault(false)) {
		//            DoPerTransactionRecurringFee();
		//            // hg 03/12/12
		//            //moved to Redemption process, unless they are auto paid
		//            if (this.Affiliate.AutoPayFleet || (this.AffiliateDrivers.AutoPayManual && !this.CardSwiped)
		//                                                || (this.AffiliateDrivers.AutoPaySwipe && this.CardSwiped)) {
		//                DoPromoPayments();
		//            }
		//        }

		//        //string batchTimeZone = SystemDefaultsDict.GetByKey("Processor", "BatchTimeZone", false).ValueString;
		//        //if (!batchTimeZone.IsNullOrEmpty()) {
		//        //    TimeZoneConverter tzConvert = new TimeZoneConverter();
		//        //    DateTime etTime = tzConvert.ConvertTime(ChargeDate.Value, this.Affiliate.TimeZone, batchTimeZone);
		//        var timeZoneOffest = DriverPaymentRef.GetCreate(this, "TimeZoneOffset", "Minutes");
		//        timeZoneOffest.ValueString = DateTime.UtcNow.Subtract(this.Affiliate.LocalTime).TotalMinutes.ToString("F0");
		//        timeZoneOffest.Save();
		//        //}
		//        double elapsed = DateTime.Now.Subtract(elapsedStart).TotalMilliseconds / 1000;
		//        var dpElapsedRef = DriverPaymentRef.GetCreate(this, "ElapsedTime", "ChargeCard");
		//        dpElapsedRef.ValueString = elapsed.ToString();
		//        dpElapsedRef.Save();

		//        return oPayResults;
		//    }

		//}



		// ********************************************************************************************************
		// 4/23/2013 Tax the max from either Drivers or AffilaiteDrivers
		// removed "bool pCheckDriver" as an input argument
		// ********************************************************************************************************

		public DriverPaymentResults ChargeCheck() {
			DateTime elapsedStart = DateTime.Now;

			#region Validations
			// Put in to trap bogus charge card calls
			if ((this.Fare == 0 || Fare < this.Affiliate.AffiliateDriverDefaults.StartingFare) && !JobAdjustment) {
				if (Platform.IsNullOrEmpty()) {
					Platform = "" + this.Affiliate.DefaultPlatform;
				}
				string msg = $"{(Platform.Equals(Platforms.ChargePass.ToString(), StringComparison.CurrentCultureIgnoreCase) ? "Amount" : "Fare")} must be greater than {this.Affiliate.AffiliateDriverDefaults.StartingFare.ToString("C")}";
				if (this.DriverPaymentID > 0) {
					this.Notes = msg;
					this.Save();
				}
				throw new Exception(msg);
			}

			if (BankAccountNo.IsNullOrEmpty() || BankRoutingNo.IsNullOrEmpty()) {
				string msg = "Bank Routing Number and Account Number are required";
				if (this.DriverPaymentID > 0) {
					this.Notes = msg;
					this.Save();
				}
				throw new Exception(msg);
			}

			GetSetCardNumber = $"{BankRoutingNo}-{BankAccountNo}";

			// only allow this one test card for Carmel
			string cardNo = this.DecryptCardNumber();
			if (CardUtils.IsTestCard(cardNo) && this.AffiliateID == 161) {
				string msg = "Cannot charge Test Card";
				if (this.DriverPaymentID > 0) {
					this.Notes = msg;
					this.Save();
				}
				throw new Exception(msg);
			}

			if (AffiliateDriverID.HasValue) {
				string msg = string.Format("Affiliate Driver: {0}-{1},  Account Frozen cannot process check", AffiliateDriverID.Value, AffiliateDrivers.Name);
				if (!AffiliateDrivers.Active) {
					if (this.DriverPaymentID > 0) {
						this.Notes = msg;
						this.Save();
					}
					throw new Exception(msg);
				}
			} else {
				if (!Drivers.Active) {
					string msg = string.Format("Driver: {0}-{1},  Account Frozen cannot process check", DriverID, Drivers.Name);
					if (this.DriverPaymentID > 0) {
						this.Notes = msg;
						this.Save();
					}
					throw new Exception(msg);
				}
			}


			//failed until card is charged
			this.Failed = true;

			// hg 2011-07/28 Force a save to prevent incorrect charges from happening
			// Always save first to avoid incorrect voucher numbers going to Credit card processor
			if (this.IsNewRecord) {
				int tries = 0;
				while (tries < 3) {
					tries++;
					try {
						this.Save("Force Saved");
						break;
					} catch (Exception ex) {
						if (ex.Message.Equals("Query timeout expired")) {
							continue;
						}
						throw new Exception(ex.Message);
					}
				}
				CreateChargeRecords();
				//var db = CabRideDrivenDB.Utils.SqlHelper.OpenCabRideDB();
				using (var conn = SqlHelper.OpenSqlConnection()) {
					//db.WriteEntities(ChargeList);
					foreach (var rec in ChargeList) {
						rec.Save(conn, CabRideDapperSettings.LoggedInUser);
					}
				}
			}

			MyTimeStats.Key = this.TransNo;




			//PreAuth card 1st if manual entry and zip code is present to ensure card is valid before charging
			//if (!this.AuthDate.HasValue && !this.CardSwiped) {
			//    if (!string.IsNullOrEmpty(this.CardZIPCode)) {
			//        if (!this.IgnoreCardZIP.GetValueOrDefault(true)) {
			//            if (this.AuthAmount == 0 || AuthAmount < DriverTotal) {
			//                this.AuthAmount = DriverTotal; // TotalCharge;  use Driver total as PreAuth up's it by the TaxiPass Fee
			//            }
			//            DriverPaymentResults preAuthResult = PreAuthCard();
			//            if (!preAuthResult.Result.Equals("0")) {
			//                return preAuthResult;
			//            }
			//        }
			//    }
			//}

			CardProcessingFailureTypes oProcess = CardProcessingFailureTypes.OKToProcess;

			if (!BypassFraudCheck) {
				BypassFraudCheck = this.Affiliate.AffiliateDriverDefaults.ByPassFraudCheck;
			}


			if (!BypassFraudCheck && !this.Test) {
				oProcess = CanProcessCard(this);
			}

			switch (oProcess) {
				case CardProcessingFailureTypes.MaxDailyAmount:
				case CardProcessingFailureTypes.MaxMonthlyAmount:
				case CardProcessingFailureTypes.MaxTransPerMonth:
				case CardProcessingFailureTypes.MaxTransPerWeek:
				case CardProcessingFailureTypes.MaxDriverTransactions:
				//case CardProcessingFailureTypes.MaxCardTransactions:
				//case CardProcessingFailureTypes.MaxTransPerMonthPerCard:
				//case CardProcessingFailureTypes.MaxTransPerWeekPerCard:
				case CardProcessingFailureTypes.MaxWeeklyAmount:
				case CardProcessingFailureTypes.OKToProcess:

					//this.Reason = "Not yet processed";
					if (!this.ChargeDate.HasValue) {
						this.ChargeDate = this.Affiliate.LocalTime;
					}
					this.FraudFailure = false;

					if (oProcess != CardProcessingFailureTypes.OKToProcess) {
						CardProcessingException ex = new CardProcessingException("Check Fraud Collars Tripped", oProcess, this.Drivers, this.AffiliateDrivers, Number);
						CheckSuspendAccount(ex);
						SendOutFraudMessage(false, ex.MyMessage);
					}
					break;

				default: {
						CardProcessingException ex = new CardProcessingException("Cannot Process Check", oProcess, this.Drivers, this.AffiliateDrivers, Number);
						this.Notes = ex.MyMessage;
						this.DriverPaymentNotes.Save();
						this.ChargeDate = Affiliate.LocalTime;
						if (!this.Affiliate.AffiliateDriverDefaults.ExcludeFromRedeemerStopPayment.GetValueOrDefault(false)) {
							this.FraudFailure = true;
						}

						// make sure these columns are reset
						this.DriverPaid = false;
						this.DriverPaidDate = null;

						this.Save();
						CheckSuspendAccount(ex);
						SendOutFraudMessage(true);
						throw ex;
					}
			}

			using (var conn = SqlHelper.OpenSqlConnection()) {
				//Check BlackList
				if (BlackListedCard(conn, cardNo)) {
					this.Notes = "Blacklisted Check";
					this.DriverPaymentNotes.Save();
					throw new Exception("Check not accepted");
				}

				#endregion Validations

				DriverPaymentResults oPayResults = null;
				//CardProcessors oProcessor = CardProcessors.Verisign;

				//// 4/23/2013 probably obe
				//if (((CardUtils.IsTestCard(cardNo) && !this.Drivers.MyAffiliate.MyVerisignAccount.EPayTestURL.IsNullOrEmpty())
				//    || (!CardUtils.IsTestCard(cardNo) && !this.Drivers.MyAffiliate.MyVerisignAccount.EPayProdURL.IsNullOrEmpty()))) {
				//    oProcessor = CardProcessors.USAePay;
				//}

				VerisignAccountID = CreditCardUtils.GetDriverPaymentCardGatewayGroupsID(conn, this.TransNo, 0);
				if (UseFraudGateway && this.Affiliate.FraudVerisignAccountID.GetValueOrDefault(0) > 0 && !this.CardZIPCode.IsNullOrEmpty() && !this.CVV.IsNullOrEmpty()) {
					VerisignAccountID = this.Affiliate.FraudVerisignAccountID.Value;
				}
				AffiliateRef affRec = AffiliateRef.GetByGroupReference(conn, this.AffiliateID.Value, "Gateways", "eCheck");
				if (!affRec.ValueString.IsNullOrEmpty() && affRec.ValueString.IsNumeric() && affRec.ValueString.ToInt() > 0) {
					VerisignAccountID = affRec.ValueString.ToInt();
				}


				if (VerisignAccountID == 0 && !Test) {
					string msg = "No Gateway assigned to Affiliate " + this.MyAffiliateName;
					this.Notes = msg;
					this.DriverPaymentNotes.Save();
					throw new Exception(msg);
				}

				CreditCardChargeType chargeType = CreditCardChargeType.ECommerce;
				if (Platform != Platforms.GetRide.ToString()) {
					chargeType = CreditCardChargeType.Manual;
				}
				Gateway gway = new Gateway(Utils.CreditCardUtils.LoadGateWays(conn, this.VerisignAccountID.Value, chargeType, CardType));

				string sComment = "";

				BankCheck checkData = new BankCheck();
				checkData.AffiliateDriverID = AffiliateDriverID.GetValueOrDefault(0);
				checkData.DriverID = DriverID;

				checkData.RoutingNo = BankRoutingNo;
				checkData.AccountNo = BankAccountNo;
				checkData.AccountType = BankAccountType;

				checkData.CheckNo = CheckNo;
				checkData.CustomerName = GetSetCardHolder;
				sComment = "Manual Entry";

				// ride could be free based on discounts
				if (TotalCharge > 0) {
					List<ProcessorResponse> respList = new List<ProcessorResponse>();
					if (this.Test) {
						// simulate approved for test  trans
						ProcessorResponse resp = new ProcessorResponse() {
							Amount = this.TotalCharge,
							AuthCode = StringUtils.GetUniqueKey(6, true),
							CreditCardProcessor = CardProcessors.TPCard,
							Message = "Approved",
							Reference = StringUtils.GenerateRandomHex(10),
							Result = "0",
							TestCard = true,
							TransDate = ChargeDate.GetValueOrDefault(),
							TransType = "S",
							VerisignAccountID = 0
						};
						respList.Add(resp);
						oPayResults = ProcessResponse(resp, VerisignTrxTypes.Sale, TotalCharge, null);
					} else {
						respList = gway.ProcessCheck(new CheckRequest(checkData, TotalCharge, sComment, TransNo, GetServiceType(), Platform, SalesClerk(), ("" + this.Affiliate.DefaultPlatform).Equals(Platforms.SplitPay.ToString()) ? TaxiPassFee : 0));
						foreach (ProcessorResponse resp in respList) {
							oPayResults = ProcessResponse(resp, VerisignTrxTypes.Sale, TotalCharge, null);
						}
					}
				} else {
					oPayResults = ProcessResponse(CreateDummyProcessorResponse(), VerisignTrxTypes.Sale, TotalCharge, null);
				}



				// hg 4/28/11 force payment if we need to mark as AutoPaid regardless of payment type and platform
				// ca 9/7/11 Only set DriverPaid if the charge was successful (oPayResults.Result.Trim() == "0")
				if (!this.DriverPaid
					&& oPayResults.Result.Trim() == "0"
					&& (this.Affiliate.AutoPayFleet
							|| (this.AffiliateDrivers.AutoPayManual && !this.CardSwiped)
							|| (this.AffiliateDrivers.AutoPaySwipe && this.CardSwiped)
						)
					) {
					if (!this.AffiliateDrivers.DriverInitiatedPayCardPayment
						&& !this.Affiliate.DriverInitiatedPayCardPayment
						&& !SystemDefaults.GetDefaults(conn).DriverInitiatedPayCardPayment) {

						if (!this.AffiliateDrivers.PaidByRedeemer) {
							SetDriverPaid(false);
						}
					}
				}


				CheckFreezeAccount();

				if (this.Affiliate.DoNotPay) {
					DoNotPay = true;
					DriverPaid = false;
					DriverPaidDate = null;
					DriverPaidTransNo = null;
					DriverPaymentAux aux = this.DriverPaymentAux;
					if (aux.IsNullEntity) {
						aux = DriverPaymentAux.Create(this);
					}
					aux.DoNotPay = true;
					aux.DoNotPayReasonID = DoNotPayReason.GetCreateReason("Software Licensing").DoNotPayReasonID;
					aux.Save();
				}

				this.Save();

				if (this.DriverPaid && !this.Failed && !this.Test && !this.DoNotPay.GetValueOrDefault(false)) {
					DoPerTransactionRecurringFee();
					// hg 03/12/12
					//moved to Redemption process, unless they are auto paid
					if (this.Affiliate.AutoPayFleet || (this.AffiliateDrivers.AutoPayManual && !this.CardSwiped)
														|| (this.AffiliateDrivers.AutoPaySwipe && this.CardSwiped)) {
						DoPromoPayments();
					}
				}

				//string batchTimeZone = SystemDefaultsDict.GetByKey("Processor", "BatchTimeZone", false).ValueString;
				//if (!batchTimeZone.IsNullOrEmpty()) {
				//    TimeZoneConverter tzConvert = new TimeZoneConverter();
				//    DateTime etTime = tzConvert.ConvertTime(ChargeDate.Value, this.Affiliate.TimeZone, batchTimeZone);
				var timeZoneOffest = DriverPaymentRef.GetCreate(this, "TimeZoneOffset", "Minutes");
				timeZoneOffest.ValueString = DateTime.UtcNow.Subtract(this.Affiliate.LocalTime).TotalMinutes.ToString("F0");
				timeZoneOffest.Save();
				//}
				double elapsed = DateTime.Now.Subtract(elapsedStart).TotalMilliseconds / 1000;
				var dpElapsedRef = DriverPaymentRef.GetCreate(this, "ElapsedTime", "ChargeCard");
				dpElapsedRef.ValueString = elapsed.ToString();
				dpElapsedRef.Save();

				return oPayResults;
			}

		}


		public DriverPaymentResults RefundCheck() {
			DateTime elapsedStart = DateTime.Now;

			#region Validations
			// Put in to trap bogus charge card calls
			if (this.Fare == 0) {
				string msg = $"{(Platform.Equals(Platforms.ChargePass.ToString(), StringComparison.CurrentCultureIgnoreCase) ? "Amount" : "Fare")} must be greater than {this.Affiliate.AffiliateDriverDefaults.StartingFare.ToString("C")}";
				if (this.DriverPaymentID > 0) {
					this.Notes = msg;
					this.Save();
				}
				throw new Exception(msg);
			}

			if (BankAccountNo.IsNullOrEmpty() || BankRoutingNo.IsNullOrEmpty()) {
				string msg = "Bank Routing Number and Account Number are required";
				if (this.DriverPaymentID > 0) {
					this.Notes = msg;
					this.Save();
				}
				throw new Exception(msg);
			}

			GetSetCardNumber = $"{BankRoutingNo}-{BankAccountNo}";

			// only allow this one test card for Carmel
			string cardNo = this.DecryptCardNumber();

			if (AffiliateDriverID.HasValue) {
				string msg = string.Format("Affiliate Driver: {0}-{1},  Account Frozen cannot process check", AffiliateDriverID.Value, AffiliateDrivers.Name);
				if (!AffiliateDrivers.Active) {
					if (this.DriverPaymentID > 0) {
						this.Notes = msg;
						this.Save();
					}
					throw new Exception(msg);
				}
			} else {
				if (!Drivers.Active) {
					string msg = string.Format("Driver: {0}-{1},  Account Frozen cannot process check", DriverID, Drivers.Name);
					if (this.DriverPaymentID > 0) {
						this.Notes = msg;
						this.Save();
					}
					throw new Exception(msg);
				}
			}


			//failed until card is charged
			this.Failed = true;

			// hg 2011-07/28 Force a save to prevent incorrect charges from happening
			// Always save first to avoid incorrect voucher numbers going to Credit card processor
			if (this.IsNewRecord) {
				int tries = 0;
				while (tries < 3) {
					tries++;
					try {
						this.Save("Force Saved");
						break;
					} catch (Exception ex) {
						if (ex.Message.Equals("Query timeout expired")) {
							continue;
						}
						throw new Exception(ex.Message);
					}
				}
				CreateChargeRecords();
				//var db = CabRideDrivenDB.Utils.SqlHelper.OpenCabRideDB();
				using (var conn = SqlHelper.OpenSqlConnection()) {
					//db.WriteEntities(ChargeList);
					foreach (var rec in ChargeList) {
						rec.Save(conn, CabRideDapperSettings.LoggedInUser);
					}
				}
			}

			MyTimeStats.Key = this.TransNo;

			using (var conn = SqlHelper.OpenSqlConnection()) {
				//Check BlackList
				if (BlackListedCard(conn, cardNo)) {
					this.Notes = "Blacklisted Check";
					this.DriverPaymentNotes.Save();
					throw new Exception("Check not accepted");
				}

				#endregion Validations

				DriverPaymentResults oPayResults = null;
				AffiliateRef affRec = AffiliateRef.GetByGroupReference(conn, this.AffiliateID.Value, "Gateways", "eCheck");

				if (VerisignAccountID == 0 && !Test) {
					string msg = "No Gateway assigned to Affiliate " + this.MyAffiliateName;
					this.Notes = msg;
					this.DriverPaymentNotes.Save();
					throw new Exception(msg);
				}

				CreditCardChargeType chargeType = CreditCardChargeType.ECommerce;
				if (Platform != Platforms.GetRide.ToString()) {
					chargeType = CreditCardChargeType.Manual;
				}
				Gateway gway = new Gateway(Utils.CreditCardUtils.LoadGateWays(conn, this.VerisignAccountID.Value, chargeType, CardType));

				string sComment = "";

				BankCheck checkData = new BankCheck();
				checkData.AffiliateDriverID = AffiliateDriverID.GetValueOrDefault(0);
				checkData.DriverID = DriverID;

				checkData.RoutingNo = BankRoutingNo;
				checkData.AccountNo = BankAccountNo;
				checkData.AccountType = BankAccountType;

				checkData.CheckNo = CheckNo;
				checkData.CustomerName = GetSetCardHolder;
				sComment = "Manual Entry";

				if (TotalCharge != 0) {
					List<ProcessorResponse> respList = new List<ProcessorResponse>();
					if (this.Test) {
						// simulate approved for test  trans
						ProcessorResponse resp = new ProcessorResponse() {
							Amount = this.TotalCharge,
							AuthCode = StringUtils.GetUniqueKey(6, true),
							CreditCardProcessor = CardProcessors.TPCard,
							Message = "Approved",
							Reference = StringUtils.GenerateRandomHex(10),
							Result = "0",
							TestCard = true,
							TransDate = ChargeDate.GetValueOrDefault(),
							TransType = "S",
							VerisignAccountID = 0
						};
						respList.Add(resp);
						oPayResults = ProcessResponse(resp, VerisignTrxTypes.Sale, TotalCharge, null);
					} else {
						respList = gway.RefundCheck(new CheckRequest(checkData, TotalCharge, sComment, TransNo, GetServiceType(), Platform, SalesClerk(), ("" + this.Affiliate.DefaultPlatform).Equals(Platforms.SplitPay.ToString()) ? TaxiPassFee : 0));
						foreach (ProcessorResponse resp in respList) {
							oPayResults = ProcessResponse(resp, VerisignTrxTypes.Sale, TotalCharge, null);
						}
					}
				} else {
					oPayResults = ProcessResponse(CreateDummyProcessorResponse(), VerisignTrxTypes.Sale, TotalCharge, null);
				}



				// hg 4/28/11 force payment if we need to mark as AutoPaid regardless of payment type and platform
				// ca 9/7/11 Only set DriverPaid if the charge was successful (oPayResults.Result.Trim() == "0")
				if (!this.DriverPaid
					&& oPayResults.Result.Trim() == "0"
					&& (this.Affiliate.AutoPayFleet
							|| (this.AffiliateDrivers.AutoPayManual && !this.CardSwiped)
							|| (this.AffiliateDrivers.AutoPaySwipe && this.CardSwiped)
						)
					) {
					if (!this.AffiliateDrivers.DriverInitiatedPayCardPayment
						&& !this.Affiliate.DriverInitiatedPayCardPayment
						&& !SystemDefaults.GetDefaults(conn).DriverInitiatedPayCardPayment) {

						if (!this.AffiliateDrivers.PaidByRedeemer) {
							SetDriverPaid(false);
						}
					}
				}


				CheckFreezeAccount();

				if (this.Affiliate.DoNotPay) {
					DoNotPay = true;
					DriverPaid = false;
					DriverPaidDate = null;
					DriverPaidTransNo = null;
					DriverPaymentAux aux = this.DriverPaymentAux;
					if (aux.IsNullEntity) {
						aux = DriverPaymentAux.Create(this);
					}
					aux.DoNotPay = true;
					aux.DoNotPayReasonID = DoNotPayReason.GetCreateReason("Software Licensing").DoNotPayReasonID;
					aux.Save();
				}

				this.Save();

				var timeZoneOffest = DriverPaymentRef.GetCreate(this, "TimeZoneOffset", "Minutes");
				timeZoneOffest.ValueString = DateTime.UtcNow.Subtract(this.Affiliate.LocalTime).TotalMinutes.ToString("F0");
				timeZoneOffest.Save();
				//}
				double elapsed = DateTime.Now.Subtract(elapsedStart).TotalMilliseconds / 1000;
				var dpElapsedRef = DriverPaymentRef.GetCreate(this, "ElapsedTime", "ChargeCard");
				dpElapsedRef.ValueString = elapsed.ToString();
				dpElapsedRef.Save();

				return oPayResults;
			}

		}

		public static CardProcessingFailureTypes CanProcessCheck(DriverPayments pDriverPayments) {
			//decimal dMax;
			//string sCardNumber = pDriverPayments.DecryptCardNumber();
			//decimal totalAmount = pDriverPayments.DriverTotal;

			//Drivers oDriver = pDriverPayments.Drivers;
			//AffiliateDrivers oAffDriver = pDriverPayments.AffiliateDrivers;

			//string sCardNo = CardNumberToHash(sCardNumber);

			//using (var conn = SqlHelper.OpenSqlConnection()) {
			//    SystemDefaults oDefault = SystemDefaults.GetDefaults(conn);

			//    if (DriverPromotions.GetPromotionsByCardNo(conn, sCardNumber).Count > 0) {
			//        return CardProcessingFailureTypes.OKToProcess;
			//    }

			//    if (sCardNo.IsNullOrEmpty()) {
			//        return CardProcessingFailureTypes.MissingCardNumber;
			//    }


			//    TimeZoneConverter tzConvert = new TimeZoneConverter();
			//    DateTime now = tzConvert.ConvertTime(DateTime.Now, TimeZone.CurrentTimeZone.StandardName, pDriverPayments.Affiliate.TimeZone);
			//    bool bCheckDriver = true;
			//    if (oDriver.Name.Equals("Charge Card", StringComparison.CurrentCultureIgnoreCase) && pDriverPayments.Platform != Platforms.WebTerminal.ToString() && pDriverPayments.AffiliateDriverID.HasValue) {
			//        bCheckDriver = false;
			//    } else {
			//        if (!oDriver.Name.Equals("Charge Card", StringComparison.CurrentCultureIgnoreCase)) {
			//            bCheckDriver = true;
			//        }
			//    }
			//    usp_DriverPayments_FraudCheck fraudTotals = usp_DriverPayments_FraudCheck.ExecuteProc(conn, pDriverPayments, bCheckDriver, now);

			//    if (oDefault.DriverMinTransTime != 0 && fraudTotals.LastChargeDate.HasValue) {
			//        TimeSpan oTime = now.Subtract(fraudTotals.LastChargeDate.Value);
			//        if (Math.Abs(oTime.TotalSeconds) < oDefault.DriverMinTransTime) {
			//            return CardProcessingFailureTypes.CardUsedTooSoon;
			//        }
			//    }


			//    if (fraudTotals.DailyTransCountPerCard >= oAffDriver.GetCardChargedMaxNumber) {
			//        return CardProcessingFailureTypes.MaxCardTransactions;
			//    }


			//    if (fraudTotals.DailyTransCount >= oAffDriver.GetMaxTrxPerDay) {
			//        return CardProcessingFailureTypes.MaxDriverTransactions;
			//    }

			//    dMax = oDriver.GetMaxDailyAmount;
			//    if (oAffDriver.GetMaxDailyAmount > dMax) {
			//        dMax = oAffDriver.GetMaxDailyAmount;
			//    }
			//    if (fraudTotals.DailyTransTotal > dMax) {
			//        return CardProcessingFailureTypes.MaxDailyAmount;
			//    }


			//    //Check Fraud Measures for Month
			//    dMax = oDriver.GetMaxTransPerMonth;
			//    if (oAffDriver.GetMaxTransPerMonth > dMax) {
			//        dMax = oAffDriver.GetMaxTransPerMonth;
			//    }
			//    if (fraudTotals.MonthTransCount > dMax) {
			//        return CardProcessingFailureTypes.MaxTransPerMonth;
			//    }

			//    dMax = oDriver.GetMaxTransPerMonthPerCard;
			//    if (oAffDriver.GetMaxTransPerMonthPerCard > dMax) {
			//        dMax = oAffDriver.GetMaxTransPerMonthPerCard;
			//    }
			//    if (fraudTotals.MonthTransCountPerCard > dMax) {
			//        return CardProcessingFailureTypes.MaxTransPerMonthPerCard;
			//    }

			//    dMax = oDriver.GetMaxMonthlyAmount;
			//    if (oAffDriver.GetMaxMonthlyAmount > dMax) {
			//        dMax = oAffDriver.GetMaxMonthlyAmount;
			//    }
			//    if (fraudTotals.MonthTotal + totalAmount > dMax) {
			//        return CardProcessingFailureTypes.MaxMonthlyAmount;
			//    }


			//    //Check Fraud Measures for Week
			//    dMax = oDriver.GetMaxTransPerWeek;
			//    if (oAffDriver.GetMaxTransPerWeek > dMax) {
			//        dMax = oAffDriver.GetMaxTransPerWeek;
			//    }
			//    if (fraudTotals.WeekTransCount > dMax) {
			//        return CardProcessingFailureTypes.MaxTransPerWeek;
			//    }

			//    dMax = oDriver.GetMaxTransPerWeekPerCard;
			//    if (oAffDriver.GetMaxTransPerWeekPerCard > dMax) {
			//        dMax = oAffDriver.GetMaxTransPerWeekPerCard;
			//    }
			//    if (fraudTotals.WeekTransCountPerCard > dMax) {
			//        return CardProcessingFailureTypes.MaxTransPerWeekPerCard;
			//    }

			//    dMax = oDriver.GetMaxWeeklyAmount;
			//    if (oAffDriver.GetMaxWeeklyAmount > dMax) {
			//        dMax = oAffDriver.GetMaxWeeklyAmount;
			//    }
			//    if (fraudTotals.WeekTotal + totalAmount > dMax) {
			//        return CardProcessingFailureTypes.MaxWeeklyAmount;
			//    }

			//    decimal maxSwipeAmount = oDriver.GetChargeMaxSwipeAmount;
			//    dMax = oDriver.GetChargeMaxAmount;
			//    if (oAffDriver.GetChargeMaxAmount > dMax) {
			//        dMax = oAffDriver.GetChargeMaxAmount;
			//        if ((pDriverPayments.StoreForward || ("" + pDriverPayments.ReferenceNo).StartsWith("StoreForward:")) && dMax > maxSwipeAmount) {
			//            maxSwipeAmount = dMax;
			//        }

			//    }

			//    //allow exception charges to have same max charge as swipe amount
			//    if (maxSwipeAmount > 0 && (pDriverPayments.CardSwiped || pDriverPayments.ExceptionCharge)) {
			//        if (totalAmount > Math.Max(dMax, maxSwipeAmount)) {
			//            return CardProcessingFailureTypes.MaxChargeAmountExceeded;
			//        }
			//    } else {
			//        if (totalAmount > dMax) {
			//            return CardProcessingFailureTypes.MaxChargeAmountExceeded;
			//        }
			//    }


			//    usp_TotalChargesPerCardAndAffiliate cardTotals = usp_TotalChargesPerCardAndAffiliate.Execute(conn, pDriverPayments.AffiliateID.Value, now, pDriverPayments.Number);
			//    if (cardTotals.MaxDayAmount > 0 && totalAmount + cardTotals.TotalDay > cardTotals.MaxDayAmount) {
			//        return CardProcessingFailureTypes.MaxDayAmountPerCardExceeded;
			//    }
			//    if (cardTotals.MaxWeekAmount > 0 && totalAmount + cardTotals.TotalWeek > cardTotals.MaxWeekAmount) {
			//        return CardProcessingFailureTypes.MaxWeekAmountPerCardExceeded;
			//    }

			//    dMax = cardTotals.MaxMonthAmount;
			//    if (oAffDriver.AffiliateDriverFraudDefaults.MaxChargePerMonthPerCard > dMax) {
			//        dMax = oAffDriver.AffiliateDriverFraudDefaults.MaxChargePerMonthPerCard;
			//    }

			//    if (dMax > 0 && totalAmount + cardTotals.TotalMonth > dMax) {
			//        return CardProcessingFailureTypes.MaxMonthAmountPerCardExceeded;
			//    }
			//}

			return CardProcessingFailureTypes.OKToProcess;
		}



	}
}