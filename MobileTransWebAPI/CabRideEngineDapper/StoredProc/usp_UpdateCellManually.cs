﻿using CabRideEngineDapper.Utils;
using Dapper;
using System.Linq;

namespace CabRideEngineDapper.StoredProc
{
	public class usp_UpdateCellManually
	{
		public string OldCellNumber { get; set; }
		public string NewCellNumber { get; set; }
		public static usp_UpdateCellManually Execute(long pAffiliateId, long pAffiliateDriverId, string pNewCellNumber, string pOldCellNumber)
		{

			using (var conn = SqlHelper.OpenSqlConnection())
			{
				return conn.Query<usp_UpdateCellManually>("usp_UpdateCellManually", new { AffiliateId = pAffiliateId, AffiliateDriverId = pAffiliateDriverId,NewCellNumber=pNewCellNumber,OldCellNumber=pOldCellNumber  }, commandType: System.Data.CommandType.StoredProcedure).FirstOrDefault();
			}
		}
	}
}
