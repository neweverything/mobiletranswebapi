﻿using CabRideEngineDapper.Utils;
using Dapper;
using System;
using TaxiPassCommon;

namespace CabRideEngineDapper {
    public partial class ReceiptRequest {


        private DriverPayments mDriverPayment;
        [Editable(false)]
        public DriverPayments DriverPayment {
            get {
                if (mDriverPayment == null) {
                    using (var conn = SqlHelper.OpenSqlConnection()) {
                        mDriverPayment = DriverPayments.GetPayment(conn, this.DriverPaymentID);
                    }
                }
                return mDriverPayment;
            }
            set {
                mDriverPayment = value;
            }
        }

        private ReceiptRequestTypes mReceiptRequestType;
        [Editable(false)]
        public ReceiptRequestTypes ReceiptRequestTypes {
            get {
                if (mReceiptRequestType == null) {
                    if (ReceiptRequestTypeID.HasValue) {
                        using (var conn = SqlHelper.OpenSqlConnection()) {
                            mReceiptRequestType = ReceiptRequestTypes.GetbyID(conn, this.ReceiptRequestTypeID.Value);
                        }
                    } else {
                        mReceiptRequestType = ReceiptRequestTypes.GetNullEntity();
                    }
                }
                return mReceiptRequestType;
            }
            set {
                mReceiptRequestType = value;
            }
        }


        public static ReceiptRequest GetRecord(long pDriverPaymentID) {
            string sql = @"SELECT * FROM dbo.ReceiptRequest
							WHERE DriverPaymentID = @DriverPaymentID";
            using (var conn = SqlHelper.OpenSqlConnection()) {
                return conn.Get<ReceiptRequest>(sql, new { DriverPaymentID = pDriverPaymentID });
            }
        }

        public static ReceiptRequest GetOrCreate(DriverPayments pDriverPayment) {
            SystemDefaults defs = SystemDefaults.GetDefaults();

            ReceiptRequest rec = GetRecord(pDriverPayment.DriverPaymentID);
            if (rec.IsNullEntity) {
                rec = ReceiptRequestTable.Create();
                rec.DriverPaymentID = pDriverPayment.DriverPaymentID;
                rec.ReceiptRequest = DateTime.Today;
                rec.ReceiptRequestDue = DateTime.Today.AddDays(defs.RequestReceiptDueDays);
                if (defs.RequestReceiptChargeDays != 0) {
                    rec.ChargeBackDueDate = DateTime.Today.AddDays(defs.RequestReceiptChargeDays);
                }
                if (!pDriverPayment.DriverPaymentVoucherImages.IsNullEntity || !pDriverPayment.DriverPaymentAux.SignaturePath.IsNullOrEmpty()) {
                    rec.ValidReceiptReceived = DateTime.Today;
                }
            }
            return rec;
        }

        public override int Save(string pModifiedBy) {
            if (this.RowState == System.Data.DataRowState.Unchanged) {
                return 1;
            }
            ModifiedDate = DateTime.UtcNow;
            ModifiedBy = string.Format("{0} | {1}", pModifiedBy, CabRideDapperSettings.GetAppName());
            if (IsNewRecord) {
                this.ReceiptRequestID = NextID.GetNextKey(CabRideEngineDapper.ReceiptRequest.TableDef.TABLE_NAME);
            } else {
                RowVersion++;
            }

            return base.Save(pModifiedBy);
        }

    }
}
