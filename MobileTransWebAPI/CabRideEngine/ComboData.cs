﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CabRideEngine {
    public class ComboData {
        public ComboData(string pName, string pValue) {
            Name = pName;
            Value = pValue;
        }
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
