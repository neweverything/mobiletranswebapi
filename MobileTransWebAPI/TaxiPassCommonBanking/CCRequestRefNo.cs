﻿namespace TaxiPassCommon.Banking {

    public class CCRequestRefNo {

        public CCRequestRefNo() {

        }

        public CCRequestRefNo(CCRequest pRequest) {
            RefNum = pRequest.CardData.ReferenceNo;
            Amount = pRequest.Amount;
            TestCard = pRequest.CardData.TestCard;
            DriverID = pRequest.DriverID;
            VoucherNo = pRequest.VoucherNo;
            if (!pRequest.CardData.CardNo.IsNullOrEmpty() && pRequest.CardData.CardNo.Length > 3) {
                CardNoLast4 = pRequest.CardData.CardNo.Right(4);
            }
        }

        public CCRequestRefNo(string pRefNum, decimal pChargeAmount, bool pTestCard, string pDriverID, string pVoucherNo, bool pGetRideTrans, string pCardNoDisplay, decimal pTaxiPassFee = 0, decimal pDriverFee = 0) {
            RefNum = pRefNum;
            Amount = pChargeAmount;
            TestCard = pTestCard;
            DriverID = pDriverID;
            VoucherNo = pVoucherNo;
            GetRideTrans = pGetRideTrans;
            CardNoLast4 = pCardNoDisplay.Right(4);
            TaxiPassFee = pTaxiPassFee;
            DriverFee = pDriverFee;
        }

        public string RefNum { get; set; }
        public decimal Amount { get; set; }
        public bool TestCard { get; set; }
        public string DriverID { get; set; }
        public string VoucherNo { get; set; }

        public bool GetRideTrans { get; set; }
        public string CardNoLast4 { get; set; }

        public decimal DriverFee { get; set; }
        public decimal TaxiPassFee { get; set; }
    }
}
