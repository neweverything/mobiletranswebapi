﻿using Dapper;
using System.Data.SqlClient;
using System.Linq;

namespace CabRideEngineDapper {
    public partial class PayCardProcessing {

        public static PayCardProcessing GetLastFunding(SqlConnection pConn, string pAdminNo, long pNotPayCardProcessingID) {
            string sql = @"SELECT   TOP 1 *
                            FROM     PayCardProcessing
                            WHERE    PayCardAdminNo = @PayCardAdminNo
                                     AND PayCardProcessingID != @PayCardProcessingID
                            ORDER BY CreatedDate DESC";

            return pConn.Get<PayCardProcessing>(sql, new { PayCardAdminNo = pAdminNo, PayCardProcessingID = pNotPayCardProcessingID });
        }


        public static PayCardProcessing AddTransCardAdminNo(SqlConnection pConn, string pAdminNo) {
            return pConn.Query<PayCardProcessing>("usp_PayCardProcessingAddRec", new { AdminNo = pAdminNo }, commandType: System.Data.CommandType.StoredProcedure).FirstOrDefault();
        }
    }


}
