﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CabRideEngineDapper {
    public partial class DriverPaymentFees {

        public static DriverPaymentFees Create(long pDriverPaymentID, string pFeeType, decimal pAmount, string pFeeOwner = "TaxiPass", long pFeeOwnerID = 0) {
            DriverPaymentFees rec = Create();
            rec.DriverPaymentID = pDriverPaymentID;
            rec.FeeType = pFeeType;
            rec.Amount = pAmount;
            rec.FeeOwner = pFeeOwner;
            rec.FeeOwnerID = pFeeOwnerID;
            return rec;
        }
    }
}
