﻿using System;
using System.Collections.Generic;

using System.Data.SqlClient;

using System.Linq;
using System.Text;

using System.Data;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using TaxiPassCommon;

namespace CabRideEngine.Utils {
	public static class SQLHelper {

		public static DataTable GetRecords(PersistenceManager pPM, string pSQL) {
			return GetRecords(pPM, pSQL, "MyTable");
		}

		public static DataTable GetRecords(PersistenceManager pPM, string pSQL, string pTableName) {
			DataTable oData = null;
			try {
				AdoHelper oHelper = new AdoHelper();
				RdbKey aRdbKey = pPM.DataSourceResolver.GetDataSourceKey("Default") as RdbKey;
				oHelper = aRdbKey.AdoHelper;
                
				DataSet oDS = new DataSet();
				oData = oHelper.SelectIntoDataSet(oDS, pTableName, pSQL);
			} catch (Exception ex) {
				string x = ex.Message;
				Console.Write(x);
			}
			return oData;
		}

		//hg Code not needed
		//public static DataSet GetCabRideDataDSNLess(string pSQL, bool? pUseTest) {
		//    DataSet ds = new DataSet();
		//    SqlDataAdapter oDataAdapter;
		//    string sConnString;


		//    if (pUseTest.GetValueOrDefault(false)) {
		//        sConnString = IdeaBlade.Util.IdeaBladeConfig.Instance.RdbKeys["default_TEST"].Connection.Replace("Provider=SQLOLEDB.1;", "");
		//    } else {
		//        sConnString = IdeaBlade.Util.IdeaBladeConfig.Instance.RdbKeys["default"].Connection.Replace("Provider=SQLOLEDB.1;", "");
		//    }
		//    sConnString = sConnString.Replace(";Encrypt=True", "");

		//    SqlConnection oConn = new SqlConnection(sConnString);
		//    oConn.Open();
		//    oDataAdapter = new SqlDataAdapter(pSQL, oConn);
		//    oDataAdapter.Fill(ds);
		//    oConn.Close();

		//    return ds;
		//}


		public static SqlConnection OpenSqlConnection(PersistenceManager pPM) {
			SqlConnection connection = null;
			try {
				string connString = AppHelper.Initializer.GetConnectionString(pPM.DataSourceExtension).Replace("Provider=SQLOLEDB.1;", "").Replace("Encrypt=True", "");
				connection = new SqlConnection(connString);
				connection.Open();
			} catch (Exception ex){
				Console.WriteLine(ex.Message);
			}
			return connection;
		}
	}
}
