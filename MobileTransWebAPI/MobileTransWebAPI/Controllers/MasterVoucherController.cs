﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using System.Web.Script.Serialization;

using IdeaBlade.Persistence;

using TaxiPassCommon;

using CabRideEngine;
using CabRideEngine.Utils;

using MobileTransWebAPI.Models;
using MobileTransWebAPI.Utils;

namespace MobileTransWebAPI.Controllers {

	public class MasterVoucherController : ApiController {

		public MasterVoucherResult GetTransForMasterVoucher(string pTransRequest) {

			MasterVoucherResult result = new MasterVoucherResult();
			JavaScriptSerializer js = new JavaScriptSerializer();

			WebApiApplication.LogIt("GetTransForMasterVoucher", pTransRequest);

			PersistenceManager oPM = WebApiApplication.UserPM;
			if (oPM == null) {
				result.ErrorMsg = "Could not connect to database";
				return result;
			}

			TransListRequest info = js.Deserialize<TransListRequest>(pTransRequest.DecryptIceKey());

			if (info.VoucherNo.IsNullOrEmpty()) {
				result.ErrorMsg = "Invalid voucher";
				return result;
			}

			DateTime requestDate = info.RequestKey.DecryptIceKey().ToDateTime();
            if (WebApiApplication.GetUTCServerTime().Subtract(requestDate).TotalMinutes > 1) {
				result.ErrorMsg = "Invalid request";
				return result;
			}


			if (info.DriverID < 1) {
				result.ErrorMsg = "Invalid Access";
				return result;
			}

			AffiliateDrivers affDriver = AffiliateDrivers.GetDriver(oPM, info.AffDriverID);
			Drivers driver = Drivers.GetDriver(oPM, info.DriverID);
			if (driver.IsNullEntity) {
				result.ErrorMsg = "Invalid Driver";
				return result;
			}

			TaxiPassRedeemers redeemer = TaxiPassRedeemers.GetRedeemerByPhone(oPM, driver.Cell);
			if (redeemer.IsNullEntity) {
				result.ErrorMsg = "Invalid Access";
				return result;
			}

			MasterVoucher voucher = MasterVoucher.GetVoucher(oPM, info.VoucherNo);
			if (voucher.IsNullEntity) {
				result.ErrorMsg = "No vouchers found";
				return result;
			}


			List<DriverPayments> list = (from p in voucher.DriverPaymentAuxs
										 where p.DriverPayments.ACHDetail.IsNullEntity && !p.DriverPayments.Test && !p.DriverPayments.Failed && !p.DriverPayments.DriverPaid
										 select p.DriverPayments).ToList();

			if (list.Count < 1) {
				result.ErrorMsg = "No vouchers found";
				return result;
			}

			if (list[0].AffiliateDrivers.RedeemerStopPayment) {
				result.ErrorMsg = SystemDefaults.GetDefaults(oPM).DriverStopPaymentMsg;
				return result;
			}

			foreach (DriverPayments rec in list) {
				if (rec.RedemptionFee.GetValueOrDefault(0) == 0) {
					rec.RedemptionFee = rec.ProcessRedemptionFee(redeemer.TaxiPassRedeemerAccounts);
				}
			}

			result.TransNo = info.VoucherNo;
			result.Count = list.Count;
			result.CreatedDate = voucher.CreatedDate.ToString("MM/dd/yyyy");
			result.Total = list.Sum(p => p.DriverTotalPaid);

			if (list[0].AffiliateDrivers.IsNullEntity) {
				result.DriverName = list[0].Drivers.Name;
				result.CellPhone = list[0].Drivers.Cell;
			} else {
				result.DriverName = list[0].AffiliateDrivers.Name;
				result.CellPhone = list[0].AffiliateDrivers.Cell;
			}

			foreach (DriverPayments rec in list) {
				result.transList.Add(new MasterVoucherResult.TransRec(rec.ChargeDate.Value.ToString("MM/dd"), rec.TransNo, rec.CardNumberDisplay.Left(4), rec.DriverTotal, rec.DriverFee.GetValueOrDefault(0), rec.RedemptionFee.GetValueOrDefault(0)));
			}

			return result;
		}

		public MasterVoucherResult Post(string pTransRequest) {

			MasterVoucherResult result = new MasterVoucherResult();
			JavaScriptSerializer js = new JavaScriptSerializer();

			WebApiApplication.LogIt("CreateMasterVoucher", pTransRequest);

			PersistenceManager oPM = WebApiApplication.UserPM;
			if (oPM == null) {
				result.ErrorMsg = "Could not connect to database";
				return result;
			}

			TransListRequest info = js.Deserialize<TransListRequest>(pTransRequest.DecryptIceKey());

			DateTime requestDate = info.RequestKey.DecryptIceKey().ToDateTime();
            if (WebApiApplication.GetUTCServerTime().Subtract(requestDate).TotalMinutes > 1) {
				result.ErrorMsg = "Invalid Access";
				return result;
			}

			if (info.DriverID < 1) {
				result.ErrorMsg = "Invalid Driver";
				return result;
			}

			AffiliateDrivers affDriver = AffiliateDrivers.GetDriver(oPM, info.AffDriverID);
			Drivers driver = Drivers.GetDriver(oPM, info.DriverID);
			if (driver.IsNullEntity) {
				result.ErrorMsg = "Invalid Driver";
				return result;
			}

			if (affDriver.IsNullEntity || affDriver.AffiliateID != driver.AffiliateID) {
				result.ErrorMsg = "Invalid Fleet Access";
				return result;
			}

			EntityList<DriverPayments> payList = DriverPayments.GetSwipePaymentsForMasterVoucher(driver, affDriver);
			if (payList.Count < 1) {
				result.ErrorMsg = "No vouchers found to create Master Voucher";
				return result;
			}

			MasterVoucher voucher = MasterVoucher.Create(driver);
			voucher.Save(driver.EMail);

			foreach (DriverPayments rec in payList) {
				if (rec.DriverPaymentAux.IsNullEntity) {
					DriverPaymentAux.Create(rec);
				}
				rec.DriverPaymentAux.MasterVoucherID = voucher.MasterVoucherID;
				rec.DriverPaymentAux.Save(driver.EMail);
			}

			result.CreatedDate = voucher.CreatedDate.ToString("MM/dd/yyyy");
			result.TransNo = voucher.TransNo;

			foreach (DriverPaymentAux rec in voucher.DriverPaymentAuxs) {
				if (rec.DriverPayments.ACHDetail.IsNullEntity) {
					result.transList.Add(new MasterVoucherResult.TransRec(rec.DriverPayments.ChargeDate.Value.ToString("MM/dd"), rec.DriverPayments.TransNo, rec.DriverPayments.CardNumberDisplay.Left(4), rec.DriverPayments.DriverTotal));
				}
			}
			result.Count = result.transList.Count;
			result.Total = result.transList.Sum(p => p.Total);

			return result;
		}

	}

}
