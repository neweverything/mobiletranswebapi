using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using System.Collections.Generic;
using System.Linq;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

using TaxiPassCommon;

namespace CabRideEngine {	
	/// <summary>
	/// The developer class representing the vw_AffiliateDriverListForWebSite business object class.
	/// </summary>
	/// <remarks>
	/// <para>Place all application-specific business logic in this class.</para>
	/// <para>
	/// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
	/// and will not overwrite changes.
	/// </para>
	/// <para>
	/// This class inherits from the generated class, its companion in the pair of classes 
	/// defining a business object. Properties in the generated class may be overridden in 
	/// order to change the behavior or add business logic.
	/// </para>
	/// <para>
	/// This class is the final class in the inheritance chain for this business object. 
	/// It should not be inherited.
	/// </para>
	/// </remarks>
	[Serializable]
	public sealed partial class vw_AffiliateDriverListForWebSite : vw_AffiliateDriverListForWebSiteDataRow {

		#region Constructors -- DO NOT MODIFY

		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new';
		// it is also required if the class is visible from a web service.
		// DO NOT REMOVE **
		private vw_AffiliateDriverListForWebSite() : this(null) { }

		/// <summary>
		/// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
		/// </summary>
		/// <param name="pRowBuilder"></param>
		public vw_AffiliateDriverListForWebSite(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static vw_AffiliateDriverListForWebSite Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      vw_AffiliateDriverListForWebSite avw_AffiliateDriverListForWebSite = pManager.CreateEntity<vw_AffiliateDriverListForWebSite>();
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(avw_AffiliateDriverListForWebSite, // add id column here //);
		//
		//      // Add custom code here
		//
		//      avw_AffiliateDriverListForWebSite.AddToManager();
		//      return avw_AffiliateDriverListForWebSite;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		// Add additional logic to your business object here...

		// ***********************************************************************************************************
		// Return all Drivers for an affiliate
		// ***********************************************************************************************************
		public static EntityList<vw_AffiliateDriverListForWebSite> GetDrivers(Affiliate pAffiliate, bool? pInActive) {
			RdbQuery qry = new RdbQuery(typeof(vw_AffiliateDriverListForWebSite), vw_AffiliateDriverListForWebSite.AffiliateIDEntityColumn, EntityQueryOp.EQ, pAffiliate.AffiliateID);
			if (pInActive.HasValue) {
				qry.AddClause(ActiveEntityColumn, EntityQueryOp.EQ, !pInActive);
			}
			qry.AddOrderBy(vw_AffiliateDriverListForWebSite.AffiliateNameEntityColumn);
			qry.AddOrderBy(vw_AffiliateDriverListForWebSite.NameEntityColumn);
			qry.AddOrderBy(vw_AffiliateDriverListForWebSite.CellEntityColumn);
			return pAffiliate.PersistenceManager.GetEntities<vw_AffiliateDriverListForWebSite>(qry);
		}

		// ***********************************************************************************************************
		// Return all Drivers for a (optional) Market
		// ***********************************************************************************************************
		public static EntityList<vw_AffiliateDriverListForWebSite> GetDriverInMarket(PersistenceManager pPM, string Market, bool? pInActive) {

			RdbQuery qry = new RdbQuery(typeof(vw_AffiliateDriverListForWebSite));
			qry.CommandTimeout = 180;

			if (!Market.IsNullOrEmpty()) {
				qry.AddClause(vw_AffiliateDriverListForWebSite.MarketEntityColumn, EntityQueryOp.EQ, Market);
			}

			if (pInActive.HasValue) {
				qry.AddClause(ActiveEntityColumn, EntityQueryOp.EQ, !pInActive);
			}
			qry.AddOrderBy(vw_AffiliateDriverListForWebSite.AffiliateNameEntityColumn);
			qry.AddOrderBy(vw_AffiliateDriverListForWebSite.NameEntityColumn);
			qry.AddOrderBy(vw_AffiliateDriverListForWebSite.CellEntityColumn);
			return pPM.GetEntities<vw_AffiliateDriverListForWebSite>(qry);
		}


		// ******************************************************************************************************************
		// Given a list of affiliates, return the drivers
		// ******************************************************************************************************************
		public static EntityList<vw_AffiliateDriverListForWebSite> GetDrivers(PersistenceManager pPM, long pSalesRepID, bool? pInActive) {
			RdbQuery qry = new RdbQuery(typeof(vw_AffiliateDriverListForWebSite), vw_AffiliateDriverListForWebSite.SalesRepIDEntityColumn, EntityQueryOp.EQ, pSalesRepID);

			if (pInActive.HasValue) {
				qry.AddClause(ActiveEntityColumn, EntityQueryOp.EQ, !pInActive);
			}
			qry.AddOrderBy(vw_AffiliateDriverListForWebSite.NameEntityColumn);
			return pPM.GetEntities<vw_AffiliateDriverListForWebSite>(qry);
		}


		// ******************************************************************************************************************
		// Given a list of affiliates, return the drivers
		// ******************************************************************************************************************
		public static EntityList<vw_AffiliateDriverListForWebSite> GetDrivers(EntityList<Affiliate> pAffiliateList, bool? pInActive) {
			PersistenceManager oPM = pAffiliateList[0].PersistenceManager;
			List<long> idList = (from p in pAffiliateList
								 select p.AffiliateID).ToList();
			RdbQuery qry = new RdbQuery(typeof(vw_AffiliateDriverListForWebSite), vw_AffiliateDriverListForWebSite.AffiliateIDEntityColumn, EntityQueryOp.In, idList);
			if (pInActive.HasValue) {
				qry.AddClause(ActiveEntityColumn, EntityQueryOp.EQ, !pInActive);
			}
			qry.AddOrderBy(vw_AffiliateDriverListForWebSite.NameEntityColumn);
			return oPM.GetEntities<vw_AffiliateDriverListForWebSite>(qry);
		}


		// ******************************************************************************************************************
		// Return any driver with a name/cell match
		// ******************************************************************************************************************
		public static EntityList<vw_AffiliateDriverListForWebSite> GetDriversByCellOrName(PersistenceManager pPM, string pCellOrName) {
			if (string.IsNullOrEmpty(pCellOrName)) {
				return new EntityList<vw_AffiliateDriverListForWebSite>();
			}
			RdbQuery qry = new RdbQuery(typeof(vw_AffiliateDriverListForWebSite), vw_AffiliateDriverListForWebSite.CallInCellEntityColumn, EntityQueryOp.EQ, pCellOrName);
			qry.AddClause(vw_AffiliateDriverListForWebSite.CellEntityColumn, EntityQueryOp.EQ, pCellOrName);
			qry.AddClause(vw_AffiliateDriverListForWebSite.NameEntityColumn, EntityQueryOp.Contains, pCellOrName);
			qry.AddOperator(EntityBooleanOp.Or);
			qry.AddOperator(EntityBooleanOp.Or);
			qry.AddOrderBy(NameEntityColumn);
			return pPM.GetEntities<vw_AffiliateDriverListForWebSite>(qry, QueryStrategy.DataSourceThenCache);
		}


		// ******************************************************************************************************************
		// Return any driver in an Affiliate with a name/cell match
		// ******************************************************************************************************************
		public static EntityList<vw_AffiliateDriverListForWebSite> GetDriversByCellOrName(Affiliate pAffiliate, string pCellOrName) {
			if (string.IsNullOrEmpty(pCellOrName)) {
				return new EntityList<vw_AffiliateDriverListForWebSite>();
			}
			RdbQuery qry = new RdbQuery(typeof(vw_AffiliateDriverListForWebSite), vw_AffiliateDriverListForWebSite.CallInCellEntityColumn, EntityQueryOp.EQ, pCellOrName);
			qry.AddClause(vw_AffiliateDriverListForWebSite.CellEntityColumn, EntityQueryOp.EQ, pCellOrName);
			qry.AddClause(vw_AffiliateDriverListForWebSite.NameEntityColumn, EntityQueryOp.Contains, pCellOrName);
			qry.AddOperator(EntityBooleanOp.Or);
			qry.AddOperator(EntityBooleanOp.Or);
			qry.AddClause(vw_AffiliateDriverListForWebSite.AffiliateIDEntityColumn, EntityQueryOp.EQ, pAffiliate.AffiliateID);
			qry.AddOrderBy(NameEntityColumn);
			return pAffiliate.PersistenceManager.GetEntities<vw_AffiliateDriverListForWebSite>(qry, QueryStrategy.DataSourceThenCache);
		}


		// ******************************************************************************************************************
		// Return any driver for a sales rep
		// ******************************************************************************************************************
		public static EntityList<vw_AffiliateDriverListForWebSite> GetDriversByCellOrName(PersistenceManager pPM, long pSalesRepID, string pCellOrName) {

			RdbQuery qry = new RdbQuery(typeof(vw_AffiliateDriverListForWebSite), vw_AffiliateDriverListForWebSite.SalesRepIDEntityColumn, EntityQueryOp.EQ, pSalesRepID);
			qry.AddClause(vw_AffiliateDriverListForWebSite.CellEntityColumn, EntityQueryOp.EQ, pCellOrName);
			qry.AddClause(vw_AffiliateDriverListForWebSite.NameEntityColumn, EntityQueryOp.Contains, pCellOrName);
			qry.AddOperator(EntityBooleanOp.Or);
			qry.AddOperator(EntityBooleanOp.Or);
			qry.AddOrderBy(NameEntityColumn);
			return pPM.GetEntities<vw_AffiliateDriverListForWebSite>(qry, QueryStrategy.DataSourceThenCache);
		}


		public string FormattedCellPhone {
			get {
				return this.Cell.FormattedPhoneNumber();
			}
		}

		public string FormattedCallInCell {
			get {
				return this.CallInCell.FormattedPhoneNumber();
			}
		}

		public string SuspendedGridView {
			get {
				return Suspended.GetValueOrDefault(false) ? "Yes" : "";
			}
		}

		public string DriverLicenseHTMLImageTag {
			get {
				if (this.LicenseImage.IsNullOrEmpty()) {
					return "";
				}
				return "<a href='" + this.MyImageURL + "' target='_blank'>Driver License</a>";
			}
		}

		public string MyImageURL {
			get {
				SystemDefaults def = SystemDefaults.GetDefaults(this.PersistenceManager);
				string url = def.LicenseImageURL;
				if (this.LicenseImage.IsNullOrEmpty()) {
					return "";
				}
				string imageURL = this.LicenseImage.Replace('\\', '/');
				if (!url.EndsWith("/") && !imageURL.StartsWith("/")) {
					url += "/";
				}
				url += imageURL;
				return url;
			}
		}

		public string SignedUpBy {
			get {
				string value = this.MySigner.Split('|')[0].Trim();
				if (value.IsNumeric()) {
					return value.FormattedPhoneNumber();
				}
				return value;
			}
		}
	}

	#region EntityPropertyDescriptors.vw_AffiliateDriverListForWebSitePropertyDescriptor
	////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
	////    For example: 
	////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
	//public static partial class EntityPropertyDescriptors {
	//	public partial class vw_AffiliateDriverListForWebSitePropertyDescriptor : AdaptedPropertyDescriptor {
	//    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
	//    public AdaptedPropertyDescriptor Age {
	//      get { return this.Get("Age"); }
	//    }
	//  }
	//} 
	#endregion

}