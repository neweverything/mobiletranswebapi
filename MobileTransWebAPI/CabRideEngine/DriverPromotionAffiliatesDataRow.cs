using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Runtime.Serialization;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

namespace CabRideEngine {
 
  #region DriverPromotionAffiliatesDataTable
  /// <summary>
  /// The generated <see cref="T:IdeaBlade.Persistence.EntityTable"/> class  
  /// containing all <see cref="DriverPromotionAffiliates"/> objects.
  /// </summary>
  /// <remarks>
  /// ****************************************************
  /// <para>** DO NOT MODIFY THIS CLASS - AutoGenerated Code</para>
  /// <para>** Any changes made to this code will be lost</para>
  /// <para>** the next time this code is regenerated.</para>
  /// <para>**</para>
  /// <para>** Generated from:   EntityBaseclass.template</para>
  /// <para>** Last modified:    2011-06-29T13:00:28.3777579-07:00</para>
  /// <para>** Template version: 3.110127</para>
  /// ****************************************************
  /// </remarks>
  [BindingBrowsable(false)]
  [Serializable]
  public partial class DriverPromotionAffiliatesDataTable : BaseEntityDataTable, 
     IdeaBlade.Persistence.Rdb.IRdbTable {
  
    /// <summary>
    /// Initializes a new instance of the DriverPromotionAffiliatesDataTable class with no arguments.
    /// </summary>
    public DriverPromotionAffiliatesDataTable() {}

    /// <summary>
    /// Initializes a new instance of the DriverPromotionAffiliatesDataTable class 
    /// with the <see cref="System.Runtime.Serialization.SerializationInfo"/> 
    /// and the <see cref="System.Runtime.Serialization.StreamingContext"/>.
    /// </summary>
    /// <param name="pInfo">The data needed to serialize or deserialize an object.</param>
    /// <param name="pContext">The source and destination of a given serialized stream.</param>
    protected DriverPromotionAffiliatesDataTable(SerializationInfo pInfo, StreamingContext pContext) :
      base(pInfo, pContext) {}

    /// <summary>
    /// Creates a new instance of the <see cref="DriverPromotionAffiliatesDataRow"/>.
    /// Constructs a row from the builder.
    /// <para>** For internal use only. **</para>
    /// </summary>
    /// <param name="pRowBuilder"></param>
    /// <returns>The <see cref="DriverPromotionAffiliatesDataRow"/>.</returns>
    protected override DataRow NewRowFromBuilder(DataRowBuilder pRowBuilder) {
      return new DriverPromotionAffiliates(pRowBuilder);
    }

    /// <summary>Returns the <see cref="System.Type"/> of <see cref="DriverPromotionAffiliates"/>.</summary>
    protected override Type GetRowType() {
      return typeof(DriverPromotionAffiliates);
    }
    
    /// <summary>
    /// Gets the name of the <see cref="T:IdeaBlade.Rdb.RdbKey"/> or 
    /// <see cref="T:IdeaBlade.Persistence.WS.WsKey"/> defined in <b>IdeaBlade.ibconfig</b>. 
    /// </summary>
    /// <remarks>For this <see cref="DriverPromotionAffiliates"/>, the DataSourceKeyName="default".</remarks>
    public override String DataSourceKeyName {
      get { return @"default"; }
    }

    #region Column properties
    //**************************************
    //* Column properties
    //**************************************
    /// <summary>Gets the DriverPromotionAffiliateID <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn DriverPromotionAffiliateIDColumn {
      get {
        if (mDriverPromotionAffiliateIDColumn!=null) return mDriverPromotionAffiliateIDColumn;
        mDriverPromotionAffiliateIDColumn = GetColumn("DriverPromotionAffiliateID", true);
        return mDriverPromotionAffiliateIDColumn;
      }
    }
    private DataColumn mDriverPromotionAffiliateIDColumn;
    
    /// <summary>Gets the DriverPromotionID <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn DriverPromotionIDColumn {
      get {
        if (mDriverPromotionIDColumn!=null) return mDriverPromotionIDColumn;
        mDriverPromotionIDColumn = GetColumn("DriverPromotionID", true);
        return mDriverPromotionIDColumn;
      }
    }
    private DataColumn mDriverPromotionIDColumn;
    
    /// <summary>Gets the AffiliateID <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn AffiliateIDColumn {
      get {
        if (mAffiliateIDColumn!=null) return mAffiliateIDColumn;
        mAffiliateIDColumn = GetColumn("AffiliateID", true);
        return mAffiliateIDColumn;
      }
    }
    private DataColumn mAffiliateIDColumn;
    
    /// <summary>Gets the RowVersion <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn RowVersionColumn {
      get {
        if (mRowVersionColumn!=null) return mRowVersionColumn;
        mRowVersionColumn = GetColumn("RowVersion", true);
        return mRowVersionColumn;
      }
    }
    private DataColumn mRowVersionColumn;
    
    /// <summary>Gets the ModifiedBy <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn ModifiedByColumn {
      get {
        if (mModifiedByColumn!=null) return mModifiedByColumn;
        mModifiedByColumn = GetColumn("ModifiedBy", true);
        return mModifiedByColumn;
      }
    }
    private DataColumn mModifiedByColumn;
    
    /// <summary>Gets the ModifiedDate <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn ModifiedDateColumn {
      get {
        if (mModifiedDateColumn!=null) return mModifiedDateColumn;
        mModifiedDateColumn = GetColumn("ModifiedDate", true);
        return mModifiedDateColumn;
      }
    }
    private DataColumn mModifiedDateColumn;
    
    #endregion    

    /// <summary>Creates the schema mapping information for this DriverPromotionAffiliatesDataTable.</summary>
    /// <returns>
    /// The <see cref="T:IdeaBlade.Persistence.TableMappingInfo"/> for this DriverPromotionAffiliatesDataTable.
    /// </returns>
    protected override TableMappingInfo CreateTableMappingInfo() {
      RdbTableMappingInfo mappingInfo = new RdbTableMappingInfo(this);
      mappingInfo.SourceOwnerName = "dbo"; 
      mappingInfo.SourceTableName = "DriverPromotionAffiliates"; 
      mappingInfo.ConcurrencyColumnName = "RowVersion"; 
      // ColumnSelectionOption: selectAllColumns
      mappingInfo.SourceColumnNames = "*"; 
   
      DataColumnMappingCollection columnMappings = mappingInfo.TableMapping.ColumnMappings;
      columnMappings.Clear();
      columnMappings.Add("DriverPromotionAffiliateID", "DriverPromotionAffiliateID");
      columnMappings.Add("DriverPromotionID", "DriverPromotionID");
      columnMappings.Add("AffiliateID", "AffiliateID");
      columnMappings.Add("RowVersion", "RowVersion");
      columnMappings.Add("ModifiedBy", "ModifiedBy");
      columnMappings.Add("ModifiedDate", "ModifiedDate");
      return mappingInfo;
    }
  
    /// <summary>** For internal use only. **</summary>
    protected override void UpdateTableMappingInfo() {
      base.UpdateTableMappingInfo();
      this.TableMappingInfo.ColumnUpdateMap["RowVersion"] = "RowVersion + 1";
    }

    /// <summary>
    /// Initializes DriverPromotionAffiliates <see cref="System.Data.DataColumn"/> properties.
    /// </summary>
    public override void InitColumnProperties() {
      base.InitColumnProperties();

      DriverPromotionAffiliateIDColumn.Caption = "DriverPromotionAffiliateID";
      DriverPromotionIDColumn.Caption = "DriverPromotionID";
      AffiliateIDColumn.Caption = "AffiliateID";
      RowVersionColumn.Caption = "RowVersion";
      ModifiedByColumn.Caption = "ModifiedBy";
      ModifiedDateColumn.Caption = "ModifiedDate";
    }
  }
  #endregion
  
  #region DriverPromotionAffiliatesDataRow
  /// <summary>
  /// The generated base class for <see cref="DriverPromotionAffiliates"/>.
  /// </summary>
  /// <remarks>
  /// <para>
  /// It represents a row of persistable data in the <see cref="DriverPromotionAffiliatesDataTable"/>
  /// and derives from <see cref="T:IdeaBlade.Persistence.Entity"/>.
	/// </para>
  /// ****************************************************
  /// <para>** DO NOT MODIFY THIS CLASS - AutoGenerated Code</para>
  /// <para>** Any changes made to this code will be lost</para>
  /// <para>** the next time this code is regenerated.</para>
  /// <para>**</para>
  /// <para>** Generated from:   EntityBaseclass.template</para>
  /// <para>** Last modified:    2011-06-29T13:00:28.3777579-07:00</para>
  /// <para>** Template version: 3.110127</para>
  /// ****************************************************
  /// </remarks>  
  [Serializable]
  public abstract partial class DriverPromotionAffiliatesDataRow : BaseEntity, 
     IdeaBlade.Persistence.Rdb.IRdbEntity {
  
    /// <summary>
    /// Initializes a new instance of the DriverPromotionAffiliatesDataRow.
    /// Constructs a row from the builder.
    /// <para>** For internal use only. **</para>
    /// </summary>
    /// <param name="pRowBuilder"></param>
    protected DriverPromotionAffiliatesDataRow(DataRowBuilder pRowBuilder)
      : base(pRowBuilder) {
    }

    /// <summary>Gets the <see cref="DriverPromotionAffiliatesDataTable"/>.</summary>
    [BindingBrowsable(false)]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public new DriverPromotionAffiliatesDataTable TypedTable {
      get { return (DriverPromotionAffiliatesDataTable) base.Table; }
    }

    /// <summary>Returns the <see cref="System.Type"/> of <see cref="DriverPromotionAffiliatesDataTable"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public static Type GetTableType() {
      return typeof(DriverPromotionAffiliatesDataTable);
    }

    /// <summary>
    /// Gets the SQL 'where' clause used to restrict which 
    /// database records are accessible via this entity. 
    /// </summary>
    /// <remarks>
    /// The default property value is defined in the Current Class 
    /// Properties tab in the <b>IdeaBlade DevForce Object Mapping Tool</b>.
    /// <para>
    /// The 'where' clause will be used, in addition to any other 
    /// query-specific criteria, in every retrieval of entities of this type.
    /// </para>
    /// </remarks>
    [BindingBrowsable(false)]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public virtual string SqlWhereClause {
      get { return ""; }
    }
    
    /// <summary>
    /// Determines whether to use the 'distinct' keyword when 
    /// generating the SQL select clause for objects of this type.
    /// </summary>
    /// <remarks>Defaults to false, but may be overridden in subclasses.</remarks>
    [BindingBrowsable(false)]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public virtual bool ForceSqlDistinct {
      get { return false; }
    }
    

    #region Enums
    //**************************************
    //* Enums
    //**************************************
    #endregion
	
    #region Relation properties
    //**************************************
    //* Relation properties
    //**************************************
	  /// <summary>Gets or sets the DriverPromotions relation property.</summary>
    [RelationProperty("DriverPromotions_DriverPromotionAffiliates", 
    QueryDirection.ParentQuery)]
    public virtual DriverPromotions DriverPromotions {
      get { 
        DriverPromotions result_;
        if (GetInterceptor<DriverPromotions>("DriverPromotions", GetDriverPromotionsImpl, out result_)) return result_;
        return GetDriverPromotionsImpl();
      }
      set { 
          if (!SetInterceptor<DriverPromotions>("DriverPromotions", value, SetDriverPromotionsImpl)) {
					  SetDriverPromotionsImpl(value);
          }
      }
    }
    private DriverPromotions GetDriverPromotionsImpl() {
      return GetParent<DriverPromotions>(EntityRelations.DriverPromotions_DriverPromotionAffiliates, this.PersistenceManager.DefaultQueryStrategy);
    }
    private void SetDriverPromotionsImpl(DriverPromotions value) {
      if (value == null) {
        SetNull(this.DriverPromotionIDColumn);
      } else {
        SetColumnValue(this.DriverPromotionIDColumn, value, value.DriverPromotionIDColumn);
      }
      OnPropertyChanged(new PropertyChangedEventArgs("DriverPromotions"));
    }
    
    #endregion
    
    #region EntityColumn definitions
    //**************************************
    //* Static EntityColumn definitions
    //**************************************
    /// <summary>The DriverPromotionAffiliateID <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn DriverPromotionAffiliateIDEntityColumn =
      new EntityColumn(typeof(DriverPromotionAffiliates), "DriverPromotionAffiliateID", typeof(System.Int64), false, true, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The DriverPromotionID <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn DriverPromotionIDEntityColumn =
      new EntityColumn(typeof(DriverPromotionAffiliates), "DriverPromotionID", typeof(System.Int64), false, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The AffiliateID <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn AffiliateIDEntityColumn =
      new EntityColumn(typeof(DriverPromotionAffiliates), "AffiliateID", typeof(System.Int64), false, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The RowVersion <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn RowVersionEntityColumn =
      new EntityColumn(typeof(DriverPromotionAffiliates), "RowVersion", typeof(System.Int32), false, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The ModifiedBy <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn ModifiedByEntityColumn =
      new EntityColumn(typeof(DriverPromotionAffiliates), "ModifiedBy", typeof(System.String), true, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The ModifiedDate <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn ModifiedDateEntityColumn =
      new EntityColumn(typeof(DriverPromotionAffiliates), "ModifiedDate", typeof(System.DateTime), true, false, false, DataSourceAccessType.ReadWrite);      
    #endregion
    
    #region Properties
    //**************************************
    //* DriverPromotionAffiliateID methods
    //**************************************
    /// <summary>Gets the DriverPromotionAffiliateID <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn DriverPromotionAffiliateIDColumn {
      get { return TypedTable.DriverPromotionAffiliateIDColumn; }
    }

    /// <summary>Gets the DriverPromotionAffiliateID.</summary>
    [DBDataType(typeof(System.Int64))]
    public virtual System.Int64 DriverPromotionAffiliateID {
      get { 
        System.Int64 result_;
        if (GetInterceptor<System.Int64>("DriverPromotionAffiliateID", GetDriverPromotionAffiliateIDImpl, out result_)) return result_;
        return GetDriverPromotionAffiliateIDImpl();
      }
    }
    private System.Int64 GetDriverPromotionAffiliateIDImpl() {
      return (System.Int64) GetColumnValue(DriverPromotionAffiliateIDColumn, typeof(System.Int64), false); 
    }
    
    //**************************************
    //* DriverPromotionID methods
    //**************************************
    /// <summary>Gets the DriverPromotionID <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn DriverPromotionIDColumn {
      get { return TypedTable.DriverPromotionIDColumn; }
    }

    /// <summary>Gets or sets the DriverPromotionID.</summary>
    [DBDataType(typeof(System.Int64))]
    public virtual System.Int64 DriverPromotionID {
      get { 
        System.Int64 result_;
        if (GetInterceptor<System.Int64>("DriverPromotionID", GetDriverPromotionIDImpl, out result_)) return result_;
        return GetDriverPromotionIDImpl();
      }
      set { 
          if (!SetInterceptor<System.Int64>("DriverPromotionID", value, SetDriverPromotionIDImpl)) {
            SetDriverPromotionIDImpl(value);
          }  
      }    
    }
    private System.Int64 GetDriverPromotionIDImpl() {
      return (System.Int64) GetColumnValue(DriverPromotionIDColumn, typeof(System.Int64), false); 
    }
    private void SetDriverPromotionIDImpl(System.Int64 value) {
      SetColumnValue(DriverPromotionIDColumn, value);
    }
    
    //**************************************
    //* AffiliateID methods
    //**************************************
    /// <summary>Gets the AffiliateID <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn AffiliateIDColumn {
      get { return TypedTable.AffiliateIDColumn; }
    }

    /// <summary>Gets or sets the AffiliateID.</summary>
    [DBDataType(typeof(System.Int64))]
    public virtual System.Int64 AffiliateID {
      get { 
        System.Int64 result_;
        if (GetInterceptor<System.Int64>("AffiliateID", GetAffiliateIDImpl, out result_)) return result_;
        return GetAffiliateIDImpl();
      }
      set { 
          if (!SetInterceptor<System.Int64>("AffiliateID", value, SetAffiliateIDImpl)) {
            SetAffiliateIDImpl(value);
          }  
      }    
    }
    private System.Int64 GetAffiliateIDImpl() {
      return (System.Int64) GetColumnValue(AffiliateIDColumn, typeof(System.Int64), false); 
    }
    private void SetAffiliateIDImpl(System.Int64 value) {
      SetColumnValue(AffiliateIDColumn, value);
    }
    
    //**************************************
    //* RowVersion methods
    //**************************************
    /// <summary>Gets the RowVersion <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn RowVersionColumn {
      get { return TypedTable.RowVersionColumn; }
    }

    /// <summary>Gets or sets the RowVersion.</summary>
    [DBDataType(typeof(System.Int32))]
    public virtual System.Int32 RowVersion {
      get { 
        System.Int32 result_;
        if (GetInterceptor<System.Int32>("RowVersion", GetRowVersionImpl, out result_)) return result_;
        return GetRowVersionImpl();
      }
      set { 
          if (!SetInterceptor<System.Int32>("RowVersion", value, SetRowVersionImpl)) {
            SetRowVersionImpl(value);
          }  
      }    
    }
    private System.Int32 GetRowVersionImpl() {
      return (System.Int32) GetColumnValue(RowVersionColumn, typeof(System.Int32), false); 
    }
    private void SetRowVersionImpl(System.Int32 value) {
      SetColumnValue(RowVersionColumn, value);
    }
    
    //**************************************
    //* ModifiedBy methods
    //**************************************
    /// <summary>Gets the ModifiedBy <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn ModifiedByColumn {
      get { return TypedTable.ModifiedByColumn; }
    }

    /// <summary>Gets or sets the ModifiedBy.</summary>
    [MaxTextLength(100)]
    [DBDataType(typeof(System.String))]
    public virtual System.String ModifiedBy {
      get { 
        System.String result_;
        if (GetInterceptor<System.String>("ModifiedBy", GetModifiedByImpl, out result_)) return result_;
        return GetModifiedByImpl();
      }
      set { 
          if (!SetInterceptor<System.String>("ModifiedBy", value, SetModifiedByImpl)) {
            SetModifiedByImpl(value);
          }  
      }    
    }
    private System.String GetModifiedByImpl() {
      return (System.String) GetColumnValue(ModifiedByColumn, typeof(System.String), true); 
    }
    private void SetModifiedByImpl(System.String value) {
      SetColumnValue(ModifiedByColumn, value);
    }
    
    //**************************************
    //* ModifiedDate methods
    //**************************************
    /// <summary>Gets the ModifiedDate <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn ModifiedDateColumn {
      get { return TypedTable.ModifiedDateColumn; }
    }

    /// <summary>Gets or sets the ModifiedDate.</summary>
    [DBDataType(typeof(System.DateTime))]
    public virtual Nullable<System.DateTime> ModifiedDate {
      get { 
        Nullable<System.DateTime> result_;
        if (GetInterceptor<Nullable<System.DateTime>>("ModifiedDate", GetModifiedDateImpl, out result_)) return result_;
        return GetModifiedDateImpl();
      }
      set { 
          if (!SetInterceptor<Nullable<System.DateTime>>("ModifiedDate", value, SetModifiedDateImpl)) {
            SetModifiedDateImpl(value);
          }
      }    
    }
    private Nullable<System.DateTime> GetModifiedDateImpl() {
      return (Nullable<System.DateTime>) GetColumnValue(ModifiedDateColumn, typeof(System.DateTime), true); 
    }
    private void SetModifiedDateImpl(Nullable<System.DateTime> value) {
      SetColumnValue(ModifiedDateColumn, value);
    }
    
    #endregion

  }
  #endregion

  #region EntityPropertyDescriptors.DriverPromotionAffiliatesPropertyDescriptor
  public static partial class EntityPropertyDescriptors {

    /// <summary>
    /// Gets the DriverPromotionAffiliatesPropertyDescriptor for <see cref="DriverPromotionAffiliates"/>.
    /// </summary>
    public static DriverPromotionAffiliatesPropertyDescriptor DriverPromotionAffiliates {
      get { return DriverPromotionAffiliatesPropertyDescriptor.Instance; }
    }
    
    /// <summary>
    /// The generated class containing <see cref="DriverPromotionAffiliates"/> PropertyDescriptors.
    /// </summary>
    public partial class DriverPromotionAffiliatesPropertyDescriptor : BaseEntityPropertyDescriptor {
	
			/// <summary>
      /// Initializes a new instance of the DriverPromotionAffiliatesPropertyDescriptor class. 
      /// </summary>
      /// <param name="pParentDescriptor">The parent <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/></param>
      /// <param name="pPropertyPath">The type of the property value</param>
			public DriverPromotionAffiliatesPropertyDescriptor(AdaptedPropertyDescriptor pParentDescriptor, String pPropertyPath)
        : base(pParentDescriptor, pPropertyPath) {}
		
      /// <summary>
      /// Initializes a new instance of the DriverPromotionAffiliatesPropertyDescriptor class. 
      /// </summary>
      /// <param name="pComponentType">The type of object to which the property is bound</param>
      protected DriverPromotionAffiliatesPropertyDescriptor(Type pComponentType)
        : base(pComponentType) {}

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for DriverPromotionAffiliateID.</summary>
      public AdaptedPropertyDescriptor DriverPromotionAffiliateID {
        get { return Get("DriverPromotionAffiliateID"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for DriverPromotionID.</summary>
      public AdaptedPropertyDescriptor DriverPromotionID {
        get { return Get("DriverPromotionID"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for AffiliateID.</summary>
      public AdaptedPropertyDescriptor AffiliateID {
        get { return Get("AffiliateID"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for RowVersion.</summary>
      public AdaptedPropertyDescriptor RowVersion {
        get { return Get("RowVersion"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for ModifiedBy.</summary>
      public AdaptedPropertyDescriptor ModifiedBy {
        get { return Get("ModifiedBy"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for ModifiedDate.</summary>
      public AdaptedPropertyDescriptor ModifiedDate {
        get { return Get("ModifiedDate"); }
      }

      /// <summary>Gets the <see cref="DriverPromotionsPropertyDescriptor"/> for DriverPromotions.</summary>
      public DriverPromotionsPropertyDescriptor DriverPromotions {
        get { return Get<DriverPromotionsPropertyDescriptor>("DriverPromotions"); }
      }
						
     internal new static DriverPromotionAffiliatesPropertyDescriptor Instance = new DriverPromotionAffiliatesPropertyDescriptor(typeof(DriverPromotionAffiliates));
    }
  }
  #endregion
  
}
