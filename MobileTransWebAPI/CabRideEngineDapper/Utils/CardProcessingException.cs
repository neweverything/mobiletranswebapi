﻿using System;
using System.Runtime.Serialization;
using TaxiPassCommon;

namespace CabRideEngineDapper.Utils {
    public class CardProcessingException : ApplicationException {
        private CardProcessingFailureTypes mFailure = CardProcessingFailureTypes.OKToProcess;
        public string MyMessage { get; set; }
        public int ErrorNo { get; set; }

        public CardProcessingException() {
        }

        public CardProcessingException(string pMessage, CardProcessingFailureTypes pFailure, Drivers pDriver, AffiliateDrivers pAffDriver, string pCardNumberHashed)
            : base(pMessage) {

            //usp_TotalChargesPerCardAndAffiliate cardTotals =new usp_TotalChargesPerCardAndAffiliate();
            //using (var conn = CabRideEngineDapper.Utils.SqlHelper.OpenSqlConnection()) {
            //	cardTotals = usp_TotalChargesPerCardAndAffiliate.Execute(conn, pDriver.AffiliateID.Value, DateTime.UtcNow, pCardNumberHashed);
            //}

            mFailure = pFailure;
            switch (pFailure) {
                case CardProcessingFailureTypes.MaxCardTransactions:
                    //MyMessage = "Card exceeded number of times for taxi payment";
                    if (pAffDriver.IsNullEntity) {
                        MyMessage = "Max number of " + pDriver.GetCardChargedMaxNumber.ToString() + " transaction/Card/24 hour period has been reached";
                    } else {
                        MyMessage = "Max number of " + pAffDriver.GetCardChargedMaxNumber.ToString() + " transaction/Card/24 hour period has been reached";
                    }
                    ErrorNo = 1;
                    break;

                case CardProcessingFailureTypes.MaxDriverTransactions:
                    if (pAffDriver.IsNullEntity) {
                        MyMessage = "Your " + pDriver.GetMaxTrxPerDay.ToString() + " Transactions/24 hour period has been reached";

                    } else {
                        MyMessage = "Your " + pAffDriver.GetMaxTrxPerDay.ToString() + " Transactions/24 hour period has been reached";
                    }
                    ErrorNo = 2;
                    break;

                case CardProcessingFailureTypes.CardUsedTooSoon:
                    if (pAffDriver.IsNullEntity) {
                        MyMessage = "Same Card cannot be used within " + pDriver.GetMinTransTime.ToString() + " seconds of last transaction";
                    } else {
                        MyMessage = "Same Card cannot be used within " + pAffDriver.GetMinTransTime.ToString() + " seconds of last transaction";
                    }
                    ErrorNo = 3;
                    break;

                case CardProcessingFailureTypes.MaxChargeAmountExceeded:
                    if (pAffDriver.IsNullEntity) {
                        MyMessage = "You cannot charge more than " + pDriver.GetChargeMaxAmount;
                    } else {
                        MyMessage = "You cannot charge more than " + pAffDriver.GetChargeMaxAmount;
                    }
                    ErrorNo = 4;
                    break;

                case CardProcessingFailureTypes.MaxTransPerMonth:
                    if (pAffDriver.IsNullEntity) {
                        MyMessage = "Max number of " + pDriver.GetMaxTransPerMonth.ToString() + " transaction/30 Day period has been reached";
                    } else {
                        MyMessage = "Max number of " + pAffDriver.GetMaxTransPerMonth.ToString() + " transaction/30 Day period has been reached";
                    }
                    ErrorNo = 5;
                    break;

                case CardProcessingFailureTypes.MaxTransPerMonthPerCard:
                    if (pAffDriver.IsNullEntity) {
                        MyMessage = "Max number of " + pDriver.GetMaxTransPerMonthPerCard.ToString() + " transaction/Card/30 Day period has been reached";
                    } else {
                        MyMessage = "Max number of " + pAffDriver.GetMaxTransPerMonthPerCard.ToString() + " transaction/Card/30 Day period has been reached";
                    }
                    ErrorNo = 6;
                    break;

                case CardProcessingFailureTypes.MaxTransPerWeek:
                    if (pAffDriver.IsNullEntity) {
                        MyMessage = "Max number of " + pDriver.GetMaxTransPerWeek.ToString() + " transaction/7 Day period has been reached";
                    } else {
                        MyMessage = "Max number of " + pAffDriver.GetMaxTransPerWeek.ToString() + " transaction/7 Day period has been reached";
                    }
                    ErrorNo = 7;
                    break;

                case CardProcessingFailureTypes.MaxTransPerWeekPerCard:
                    if (pAffDriver.IsNullEntity) {
                        MyMessage = "Max number of " + pDriver.GetMaxTransPerWeekPerCard.ToString() + " transaction/Card/7 Day period has been reached";
                    } else {
                        MyMessage = "Max number of " + pAffDriver.GetMaxTransPerWeekPerCard.ToString() + " transaction/Card/7 Day period has been reached";
                    }
                    ErrorNo = 8;
                    break;

                case CardProcessingFailureTypes.AuthorizedByAnotherDriver:
                    MyMessage = "Card is already PreAuthed by another driver";
                    ErrorNo = 9;
                    break;

                case CardProcessingFailureTypes.Authorized:
                    MyMessage = "Card is already PreAuthed by you";
                    ErrorNo = 10;
                    break;

                case CardProcessingFailureTypes.MissingCardNumber:
                    MyMessage = "Missing credit card number";
                    ErrorNo = 12;
                    break;


                case CardProcessingFailureTypes.MaxDayAmountPerCardExceeded:
                    MyMessage = "Card will exceed daily charge limit allowed";
                    ErrorNo = 14;
                    break;

                case CardProcessingFailureTypes.MaxWeekAmountPerCardExceeded:
                    MyMessage = "Card will exceed weekly charge limit allowed";
                    ErrorNo = 14;
                    break;

                case CardProcessingFailureTypes.MaxMonthAmountPerCardExceeded:
                    MyMessage = "Card will exceed monthly charge limit allowed";
                    ErrorNo = 16;
                    break;

                case CardProcessingFailureTypes.MaxDailyDeclineExceeded:
                    MyMessage = "Max number of daily declines exceeded";
                    ErrorNo = 17;
                    break;

                case CardProcessingFailureTypes.MaxWeeklyDeclineExceeded:
                    MyMessage = "Max number of weekly declines exceeded";
                    ErrorNo = 18;
                    break;

                case CardProcessingFailureTypes.MaxMonthlyDeclineExceeded:
                    MyMessage = "Max number of monthly declines exceeded";
                    ErrorNo = 19;
                    break;

                case CardProcessingFailureTypes.AverageChargeExceeded:
                    MyMessage = "Average Charge Amount exceeded";
                    ErrorNo = 20;
                    break;

                default:
                    MyMessage = pFailure.ToString();
                    if (MyMessage.IsNullOrEmpty()) {
                        MyMessage = pMessage;
                    }
                    ErrorNo = 11;
                    break;
            }
        }

        public CardProcessingException(string pMessage, Exception pInner)
            : base(pMessage, pInner) {
        }

        public CardProcessingException(SerializationInfo pInfo, StreamingContext pContext)
            : base(pInfo, pContext) {

        }

        public CardProcessingFailureTypes FailureType {
            get {
                return mFailure;
            }
        }

    }
}
