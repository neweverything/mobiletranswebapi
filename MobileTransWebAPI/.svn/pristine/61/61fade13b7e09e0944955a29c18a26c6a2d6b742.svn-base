﻿using CabRideEngineDapper;
using CabRideEngineDapper.Utils;
using MobileTransWebAPI.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Http;
using TaxiPassCommon;
using TaxiPassCommon.Banking;

namespace MobileTransWebAPI.Controllers {

    public class AvsCheckController : ApiController {
        public AvsCheckResponse Get() {
            string data = File.ReadAllText(@"C:\Temp\test.json");

            ChargeInfo info = JsonConvert.DeserializeObject<ChargeInfo>(data);
            return Post(info);
        }

        // POST: api/PreAuthCard
        public AvsCheckResponse Post([FromBody]ChargeInfo pInfo) {
            AvsCheckResponse result = new AvsCheckResponse();
            //ChargeResult avsResult = new ChargeResult();
            //PersistenceManager oPM = Global.UserPM;
            try {
                Accounts oAcct = null;
                if (pInfo.AccountNo.IsNullOrEmpty()) {
                    result.ErrorMessage = "Account No is required";
                    return result;
                } else {
                    oAcct = Accounts.GetAccount(pInfo.AccountNo);
                    if (oAcct.IsNullEntity) {
                        result.ErrorMessage = "Unknown account";
                        return result;
                    }
                }

                string req = JsonConvert.SerializeObject(pInfo);
                WebApiApplication.LogIt(string.Format("{0} MobileTrans Web Service Data", pInfo.Platform), JsonConvert.SerializeObject(pInfo), "avsCheck" + pInfo.Platform + "_{0}.txt");

                using (var conn = SqlHelper.OpenSqlConnection()) {
                    Gateway gway = null;
                    Affiliate aff = Affiliate.GetNullEntity();
                    if (pInfo.AffiliateID != 0) {
                        aff = Affiliate.GetAffiliate(conn, pInfo.AffiliateID);
                    } else if (pInfo.AffDriverID != 0) {
                        aff = AffiliateDrivers.GetDriver(conn, pInfo.AffDriverID).Affiliate;
                    } else if (pInfo.DriverID != 0) {
                        aff = Drivers.GetDriver(conn, pInfo.DriverID).Affiliate;
                    }
                    if (aff.VerisignAccountID.GetValueOrDefault(0) > 0) {
                        List<IGatewayCredentials> acctList = CreditCardUtils.LoadGateWays(conn, aff.VerisignAccountID.Value, CardProcessors.CardConnectManual, pInfo.CardType);
                        if (acctList != null) {
                            acctList = acctList.Where(p => p.MyGateway().ToString().StartsWith(CardProcessors.CardConnect.ToString())).ToList();
                        }
                        if (acctList.Count > 0) {
                            gway = new Gateway(acctList[0]);
                        }
                    }
                    if (gway == null) {
                        List<IGatewayCredentials> acctList = CreditCardUtils.LoadGateWays(conn, (int)SystemDefaultsDict.GetByKey(conn, "AVSCheck", "DefaultGateway", false).ValueAsDecimal, CardProcessors.CardConnectManual, pInfo.CardType);
                        gway = new Gateway(acctList[0]);
                    }

                    CreditCard cardData = new CreditCard();
                    cardData.AffiliateDriverID = pInfo.AffDriverID;
                    if (!pInfo.SwipeData.IsNullOrEmpty()) {
                        cardData.Track2 = pInfo.SwipeData;
                    }
                    string sComment = "Driver Swipe Entry";
                    if (pInfo.SwipeData.IsNullOrEmpty()) {
                        cardData.CardNo = pInfo.CardNo.Decrypt();

                        string sMonth = pInfo.ExpMonth.ToString().PadLeft(2, '0');
                        string sYear = pInfo.ExpYear.ToString().Right(2);
                        cardData.CardExpiryAsMMYY = sMonth + sYear;
                        sComment = "Driver Manual Entry";
                    }
                    cardData.CVV = pInfo.CVV;
                    cardData.AvsZip = pInfo.BillingZipCode.IsNullOrEmpty() ? pInfo.ZipCode : pInfo.BillingZipCode;

                    if (!pInfo.BillingStreet.IsNullOrEmpty()) {
                        cardData.AvsStreet = pInfo.BillingStreet.Decrypt();
                        cardData.AvsStreet = cardData.AvsStreet.PadRight(35, ' ').Left(35).Trim();  // Max Litle size
                    }

                    var resultList = gway.AvsCheck(new CCRequest(cardData, 0, sComment, pInfo.ReferenceNo, "AVSCheck", aff.DefaultPlatform, pInfo.DriverID.ToString(), false));

                    DriverPayments dp;
                    SystemDefaultsDict avsCheckStoreRec = SystemDefaultsDict.GetByKey("AvsCheck", "StoreAVSCheckResults", false);
                    if (aff.AffiliateID == 161 && avsCheckStoreRec.ValueAsBool) {       //Only store for Carmel
                        dp = CreateDPRecord(oAcct.AccountID, pInfo, "AVSCheck");
                        bool doPreAuth = true;
                        foreach (ProcessorResponse response in resultList) {
                            var dpResults = dp.ProcessResponse(response, TrxTypes.Authorization, 0.01M, null);
                            if (dpResults.Result == "0") {
                                doPreAuth = false;
                            }
                        }
                        if (doPreAuth) {
                            var preAuthResult = dp.PreAuthCard(cardData.Track2);
                            result.Amount = preAuthResult.Amount.GetValueOrDefault(0);
                            result.AuthCode = preAuthResult.AuthCode;
                            result.AVS = preAuthResult.AVS;
                            result.AVSResultCode = preAuthResult.AVSResultCode;
                            result.CVV2Match = preAuthResult.CVV2Match;
                            result.Message = preAuthResult.Message;
                            result.Reference = preAuthResult.Reference;
                            result.Result = preAuthResult.Result;
                            result.TestCard = cardData.TestCard;
                            result.TransDate = preAuthResult.TransDate.HasValue ? preAuthResult.TransDate.Value : DateTime.Now;
                        }
                    }

                    if (resultList.Count == 0) {
                        result.ErrorMessage = "No response from Processor";
                    } else {
                        string js = JsonConvert.SerializeObject(resultList[0]);
                        result = JsonConvert.DeserializeObject<AvsCheckResponse>(js);
                        //js = JsonConvert.SerializeObject(result);
                    }
                }
            } catch (Exception ex) {
                result.ErrorMessage = ex.Message;
            }
            if (result.Result != "0") {
                result.ErrorMessage = result.Message;
            }
            return result;
        }

        private DriverPayments CreateDPRecord(long pAccountID, ChargeInfo oChargeInfo, string pModifiedBy) {

            using (var conn = SqlHelper.OpenSqlConnection()) {
                // Driver/Device lookup
                Drivers oDriver;
                if (oChargeInfo.DriverID > 0) {
                    oDriver = Drivers.GetDriver(conn, oChargeInfo.DriverID);
                } else if (oChargeInfo.DeviceID.IsNullOrEmpty()) {
                    Affiliate oAff = Affiliate.GetAffiliate(conn, oChargeInfo.AffiliateID);
                    oDriver = Drivers.GetChargeCard(conn, oAff.AffiliateID);
                } else {
                    oDriver = Drivers.GetDriverPhoneSubNoOrSerial(conn, oChargeInfo.AffiliateID, oChargeInfo.DeviceID);
                }

                AffiliateDrivers actualDriver = AffiliateDrivers.GetNullEntity();
                if (oChargeInfo.AffDriverID > 0) {
                    actualDriver = AffiliateDrivers.GetDriver(conn, oChargeInfo.AffDriverID);
                }

                DriverPayments oPayment = DriverPayments.Create(oDriver);
                //oPayment.MyTimeStats = stats;

                oPayment.AuthAmount = oChargeInfo.Fare;
                oPayment.GetSetCardNumber = oChargeInfo.CardNo;
                oPayment.GetSetExpiration = oChargeInfo.CardExpires;
                oPayment.ReferenceNo = oChargeInfo.ReferenceNo;

                if (!oChargeInfo.CVV.IsNullOrEmpty()) {
                    oPayment.CVV = oChargeInfo.CVV;
                }
                if (!oChargeInfo.BillingZipCode.IsNullOrEmpty()) {
                    oPayment.CardZIPCode = oChargeInfo.BillingZipCode;
                } else if (!oChargeInfo.ZipCode.IsNullOrEmpty()) {
                    oPayment.CardZIPCode = oChargeInfo.ZipCode;
                }
                oPayment.CardAddress = oChargeInfo.BillingStreet;

                oPayment.AffiliateID = oDriver.AffiliateID;
                //if (oPayment.AffiliateID == 161) {
                //	ravenDB = "Carmel";
                //}

                if (!actualDriver.IsNullEntity) {
                    oPayment.AffiliateDriverID = actualDriver.AffiliateDriverID;
                }
                if (!oChargeInfo.VehicleNo.IsNullOrEmpty()) {
                    Vehicles oVehicle = Vehicles.GetVehicle(conn, long.Parse(oChargeInfo.VehicleNo));
                    if (!oVehicle.IsNullEntity) {
                        oPayment.VehicleID = oVehicle.VehicleID;
                    }
                }

                if (oPayment.Affiliate.AffiliateDriverDefaults.TPFeeOnFareOnly) {
                    oPayment.TaxiPassFee = DriverPayments.CalcFee(oPayment.Affiliate, oChargeInfo.Fare);
                }

                oPayment.Platform = oChargeInfo.Platform;
                if (!oPayment.Platform.IsNullOrEmpty()) {
                    oPayment.Platform = oPayment.Affiliate.DefaultPlatform;
                }

                oPayment.CardSwiped = false;
                if (!oChargeInfo.SwipeData.IsNullOrEmpty()) {
                    oPayment.CardSwiped = (oChargeInfo.SwipeData.Trim().Length > 0);
                }

                oPayment.AddPrefixToVoucherVoucherNo = "AVS-";
                oPayment.Save(pModifiedBy);
                //stats.Key = oPayment.TransNo;

                if (!oChargeInfo.TripID.IsNullOrEmpty()) {
                    // Create the RideInfo record
                    Reservations oReservation = Reservations.GetReservationByReference(conn, oChargeInfo.TripID);
                    RideInfo ride = oPayment.RideInfo;
                    if (ride.IsNullEntity) {
                        ride = RideInfo.Create(oPayment);
                    }
                    ride.AccountID = pAccountID;
                    ride.JobID = oChargeInfo.TripID;
                    ride.Name = oChargeInfo.PassengerName;
                    if (oChargeInfo.GpsStart != null) {
                        ride.PickUp = oChargeInfo.GpsStart.Address;
                    } else {
                        ride.PickUp = "";
                    }
                    if (oChargeInfo.GpsEnd != null) {
                        ride.DropOff = oChargeInfo.GpsEnd.Address;
                    } else {
                        ride.DropOff = "";
                    }
                    ride.CardNo = oPayment.CardNumberDisplay.Right(5);
                    if (!oReservation.IsNullEntity) {
                        ride.PUTime = oReservation.PickUpDate;
                    } else {
                        TimeZoneConverter convert = new TimeZoneConverter();
                        ride.PUTime = convert.ConvertTime(DateTime.Now, TimeZone.CurrentTimeZone.StandardName, ride.DriverPayments.Affiliate.TimeZone);
                    }
                    ride.Save(pModifiedBy);
                    //stats.Info.Add(new TimeStats.ElapsedInfo { Type = "Saved Ride Info", Elapsed = Math.Round(DateTime.Now.Subtract(stats.Start).TotalSeconds, 2) });
                }
                return oPayment;
            }
        }

        public class AvsCheckResponse {
            public string ErrorMessage { get; set; }
            public decimal Amount { get; set; }
            public string CVV2Match { get; set; }
            public string AVSResultCode { get; set; }
            //public string AVSZip { get; set; }
            //public string AVSAddr { get; set; }
            public string AVS { get; set; }
            public string AuthCode { get; set; }
            public DateTime TransDate { get; set; }
            public string Message { get; set; }
            public string Reference { get; set; }
            public string Result { get; set; }
            public bool TestCard { get; set; }
        }
    }
}
