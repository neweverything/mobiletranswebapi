﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Util;
using IdeaBlade.Persistence.Rdb;

using Csla.Validation;

namespace CabRideEngine {
    [Serializable]
    public sealed class CancelReasonTypes : CancelReasonTypesDataRow {

        #region Constructors -- DO NOT MODIFY
        // Do not create constructors for this class
        // Developers cannot create instances of this class with the "new" keyword
        // You should write a static Create method instead; see the "Suggested Customization" region
        // Please review the documentation to learn about entity creation and inheritance

        // This private constructor prevents accidental instance creation via 'new'
        private CancelReasonTypes() : this(null) {
        }

        // Typed DataSet constructor (needed by framework) - DO NOT REMOVE
        public CancelReasonTypes(DataRowBuilder pRowBuilder)
            : base(pRowBuilder) {
        }

        #endregion

        #region Suggested Customizations

        //    // Use this factory method template to create new instances of this class
        //    public static CancelReasonTypes Create(PersistenceManager pManager,
        //      ... your creation parameters here ...) { 
        //
        //      CancelReasonTypes aCancelReasonTypes = pManager.CreateEntity<CancelReasonTypes>();
        //
        //      // if this object type requires a unique id and you have implemented
        //      // the IIdGenerator interface implement the following line
        //      pManager.GenerateId(aCancelReasonTypes, // add id column here //);
        //
        //      // Add custom code here
        //
        //      aCancelReasonTypes.AddToManager();
        //      return aCancelReasonTypes;
        //    }

        //    // Implement this method if you want your object to sort in a specific order
        //    public override int CompareTo(Object pObject) {
        //    }

        //    // Implement this method to customize the null object version of this class
        //    protected override void UpdateNullEntity() {
        //    }

        #endregion

        // Add additional logic to your business object here...

        // Add additional logic to your business object here...
        public static CancelReasonTypes Create(PersistenceManager pManager) {

            CancelReasonTypes oType = (CancelReasonTypes)pManager.CreateEntity(typeof(CancelReasonTypes));

            //pManager.GenerateId(oType, CancelReasonTypes.CancelReasonTypeIDEntityColumn);
            oType.AddToManager();
            return oType;
        }

        protected override void AddRules(RuleList pList) {
            PropertyRequiredRule.AddToList(pList, CancelReasonTypes.CancelReasonTypeEntityColumn.ColumnName);
            pList.Add(IsCancelReasonTypeUnique, CancelReasonTypes.CancelReasonTypeEntityColumn.ColumnName);
        }

        private bool IsCancelReasonTypeUnique(object pTarget, RuleArgs e) {
            CancelReasonTypes oType = (CancelReasonTypes)pTarget;

            RdbQuery oQry = new RdbQuery(typeof(CancelReasonTypes), CancelReasonTypes.CancelReasonTypeEntityColumn, EntityQueryOp.EQ, oType.CancelReasonType);
            oQry.AddClause(CancelReasonTypes.CancelReasonTypeIDEntityColumn, EntityQueryOp.NE, oType.CancelReasonTypeID);
            EntityList<CancelReasonTypes> oCancelReasonTypes = this.PersistenceManager.GetEntities<CancelReasonTypes>(oQry);
            if (oCancelReasonTypes.Count > 0) {
                e.Description = "Cancelled Reason Type must be unique!";
                return false;
            }
            return true;
        }

        [BindingBrowsable(false)]
        public override string EntityInstanceDisplayName {
            get {
                return CancelReasonType;
            }
        }

        protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
            if (IsDeserializing)
                return;
            base.OnColumnChanging(pArgs);
            //DoAudit(pArgs);
        }

        private void DoAudit(DataColumnChangeEventArgs pArgs) {
            if (!this.PersistenceManager.IsClient) {
                return;
            }
            if (this.RowState == DataRowState.Added) {
                return;
            }
            if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
                AuditRecord = CancelReasonTypesAudit.Create(this.PersistenceManager, this);
            }
            //AuditRecord[pArgs.Column.ColumnName] = pArgs.ProposedValue;
        }
    }
}
