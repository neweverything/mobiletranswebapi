﻿using CabRideEngineDapper.Utils;
using Dapper;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using TaxiPassCommon;

namespace CabRideEngineDapper {
    public partial class Vehicles {

        public List<string> ExceptionErrors = new List<string>();

        public static Vehicles Create(long pAffiliateID) {
            Vehicles rec = VehiclesTable.Create();
            rec.AffiliateID = pAffiliateID;
            return rec;
        }

        public override int Save(string pModifiedBy) {
            using (var conn = SqlHelper.OpenSqlConnection()) {
                return Save(conn, pModifiedBy);
            }
        }

        public override int Save(SqlConnection pConn, string pModifiedBy) {
            ExceptionErrors = new List<string>();

            if (VehicleNo.IsNullOrEmpty()) {
                ExceptionErrors.Add("Vehicle No is required");
            }
            if (VehicleTypeID < 1) {
                ExceptionErrors.Add("Vehicle Type is required");
            }
            IsVehicleNoUnique(pConn, this);

            if (ExceptionErrors.Count > 0) {
                return -1;
            }

            return base.Save(pConn, pModifiedBy);
        }

        private static bool IsVehicleNoUnique(SqlConnection pConn, Vehicles pVehicle) {
            if (pVehicle.VehicleNo.IsNullOrEmpty()) {
                return false;
            }

            string sql = @"SELECT * FROM Vehicles
							WHERE   VehicleNo = @VehicleNo
									AND AffiliateID = @AffiliateID
									AND VehicleID != @VehicleID";

            DynamicParameters parms = new DynamicParameters();
            parms.Add("VehicleNo", pVehicle.VehicleNo);
            parms.Add("AffiliateID", pVehicle.AffiliateID);
            parms.Add("VehicleID", pVehicle.VehicleID);
            List<Vehicles> oList = pConn.Query<Vehicles>(sql, parms).ToList();
            if (oList.Count > 0) {
                pVehicle.ExceptionErrors.Add("A vehicle already exists with the Vehicle No: " + pVehicle.VehicleNo);
                return false;
            }
            return true;
        }


        #region Relations
        private Affiliate mAffiliate;
        public Affiliate Affiliate {
            get {
                if (mAffiliate == null) {
                    mAffiliate = Affiliate.GetAffiliate(this.AffiliateID);
                }
                return mAffiliate;
            }
        }

        private MasterVehicles mMasterVehicles;
        public MasterVehicles MasterVehicles {
            get {
                if (mMasterVehicles == null) {
                    mMasterVehicles = MasterVehicles.GetVehicleByLicensePlate(this.Affiliate, this.LicensePlate);
                }
                return mMasterVehicles;
            }
        }

        private VehicleTypes mVehicleType;
        public VehicleTypes VehicleType {
            get {
                if (mVehicleType == null) {
                    mVehicleType = VehicleTypes.GetVehicleType(this.VehicleTypeID);
                }
                return mVehicleType;
            }
        }
        #endregion




        #region Custom Properties
        [Editable(false)]
        public bool Suspended {
            get {
                if (string.IsNullOrEmpty(this.Affiliate.LicenseNumber)) {
                    return false;
                }
                return !MasterVehicles.IsNullEntity && !MasterVehicles.DMVLicensePlate.IsNullOrEmpty();
            }
        }


        [Editable(false)]
        public int PaymentCount {
            get {
                string sql = @"SELECT COUNT(*) PaymentCount FROM dbo.DriverPayments
								WHERE  VehicleID = @VehicleID";
                using (var conn = SqlHelper.OpenSqlConnection()) {
                    return conn.Query<int>(sql, new { VehicleID = this.VehicleID }).DefaultIfEmpty(0).FirstOrDefault();
                }
            }
        }
        #endregion


        #region Retrieve Records
        public static Vehicles GetVehicle(SqlConnection pConn, long? pVehicleID) {
            if (!pVehicleID.HasValue) {
                return Vehicles.GetNullEntity();
            }

            return pConn.Get<Vehicles>(pVehicleID);
        }

        public static List<Vehicles> GetByAffiliate(SqlConnection pConn, long pAffiliateID) {
            string sql = @"SELECT  Vehicles.*
								  , 'Aff' MyAffiliate
								  , Affiliate.*
								  , 'MV' MasterVehicles
								  , MasterVehicles.*
								  , dbo.VehicleTypes.*
							FROM    dbo.Vehicles
									LEFT JOIN dbo.Affiliate ON Affiliate.AffiliateID = Vehicles.AffiliateID
									LEFT JOIN dbo.VehicleTypes ON VehicleTypes.VehicleTypeID = Vehicles.VehicleTypeID
									OUTER APPLY ( SELECT    *
												  FROM      dbo.MasterVehicles
												  WHERE     DMVLicensePlate = Vehicles.LicensePlate
															AND AffiliatedBaseLicenseNumber = LicenseNumber
												) MasterVehicles
							WHERE   Affiliate.AffiliateID = @AffiliateID";

            return pConn.Query<Vehicles, Affiliate, MasterVehicles, VehicleTypes, Vehicles>(sql
                , (vehi, aff, mv, vt) => { vehi.mAffiliate = aff; vehi.mMasterVehicles = mv; vehi.mVehicleType = vt; return vehi; }
                , new { AffiliateID = pAffiliateID }
                , splitOn: "MyAffiliate,MasterVehicles,VehicleTypeID").DefaultIfEmpty(Vehicles.GetNullEntity()).ToList();

        }

        public static List<Vehicles> GetBySalesRep(SqlConnection pConn, long pSalesRepID) {
            string sql = @"SELECT  Vehicles.*
								  , 'Aff' MyAffiliate
								  , Affiliate.*
								  , 'MV' MasterVehicles
								  , MasterVehicles.*
							FROM    dbo.Vehicles 
									LEFT JOIN dbo.Affiliate ON Affiliate.AffiliateID = Vehicles.AffiliateID
									OUTER APPLY ( SELECT    *
												  FROM      dbo.MasterVehicles
												  WHERE     DMVLicensePlate = Vehicles.LicensePlate
															AND AffiliatedBaseLicenseNumber = LicenseNumber
												) MasterVehicles
							WHERE   Affiliate.AffiliateID IN (
									SELECT  AffiliateID
									FROM    SalesRepAffiliates
									WHERE   SalesRepId IN ( SELECT  SalesRepId
															FROM    SalesReps
															WHERE   SalesRepId = @SalesRepId))";

            return pConn.Query<Vehicles, Affiliate, MasterVehicles, Vehicles>(sql
                , (vehi, aff, mv) => { vehi.mAffiliate = aff; vehi.mMasterVehicles = mv; return vehi; }
                , new { SalesRepId = pSalesRepID }
                , splitOn: "MyAffiliate,MasterVehicles").DefaultIfEmpty(Vehicles.GetNullEntity()).ToList();
        }

        public static Vehicles GetVehicle(SqlConnection pConn, long pAffiliateID, string pVehicleNo) {
            string sql = @"SELECT  Vehicles.*
								  , 'Aff' MyAffiliate
								  , Affiliate.*
								  , 'MV' MasterVehicles
								  , MasterVehicles.*
								  , dbo.VehicleTypes.*
							FROM    dbo.Vehicles
									LEFT JOIN dbo.Affiliate ON Affiliate.AffiliateID = Vehicles.AffiliateID
									LEFT JOIN dbo.VehicleTypes ON VehicleTypes.VehicleTypeID = Vehicles.VehicleTypeID
									OUTER APPLY ( SELECT    *
												  FROM      dbo.MasterVehicles
												  WHERE     DMVLicensePlate = Vehicles.LicensePlate
															AND AffiliatedBaseLicenseNumber = LicenseNumber
												) MasterVehicles
							WHERE   Affiliate.AffiliateID = @AffiliateID AND VehicleNo = @VehicleNo";

            var data = pConn.Query<Vehicles, Affiliate, MasterVehicles, VehicleTypes, Vehicles>(sql
                , (vehi, aff, mv, vt) => { vehi.mAffiliate = aff; vehi.mMasterVehicles = mv; vehi.mVehicleType = vt; return vehi; }
                , new { AffiliateID = pAffiliateID, VehicleNo = pVehicleNo }
                , splitOn: "MyAffiliate,MasterVehicles, VehicleTypeID").DefaultIfEmpty(Vehicles.GetNullEntity()).FirstOrDefault();

            data.Affiliate.RowState = System.Data.DataRowState.Unchanged;
            //data.MasterVehicles.RowState = System.Data.DataRowState.Unchanged;
            data.VehicleType.RowState = System.Data.DataRowState.Unchanged;

            return data;
        }


        public static List<string> GetVehicleNumbers(SqlConnection pConn, long pAffiliateID) {
            string sql = @"SELECT VehicleNo FROM dbo.Vehicles
							WHERE AffiliateID = @AffiliateID
							ORDER BY VehicleNo";

            return pConn.Query<string>(sql, new { AffiliateID = pAffiliateID }).ToList();
        }


        public static List<Vehicles> GetVehiclesForDropDown(long pAffiliateID, bool pAddNullRecord = false) {
            using (var conn = SqlHelper.OpenSqlConnection()) {
                return GetVehiclesForDropDown(conn, pAffiliateID, pAddNullRecord);
            }
        }

        public static List<Vehicles> GetVehiclesForDropDown(SqlConnection pConn, long pAffiliateID, bool pAddNullRecord = false) {
            string sql = @"SELECT  VehicleID
                                  , VehicleNo
                            FROM    dbo.Vehicles
                            WHERE   AffiliateID = @AffiliateID AND OutOfService = 0
                            ORDER BY VehicleNo";


            List<Vehicles> data = pConn.Query<Vehicles>(sql, new { AffiliateID = pAffiliateID }).ToList();
            if (pAddNullRecord) {
                data.Insert(0, Vehicles.GetNullEntity());
            }
            return data;
        }

        // ************************************************************************************************************
        // return an Affiliate's active vehicles
        // ************************************************************************************************************
        public static List<Vehicles> GetActiveVehicles(Affiliate pAffiliate) {
            return GetData(pAffiliate.AffiliateID.ToString(), "0", "", "");
        }

        // ************************************************************************************************************
        // pOutOfService is either "", "1" for true, "0" for false
        // ************************************************************************************************************
        private static List<Vehicles> GetData(string pAffiliateid, string pOutOfService, string pVehicleNo, string pID) {


            DynamicParameters parms = new DynamicParameters();
            if (!pAffiliateid.IsNullOrEmpty()) {
                parms.Add("AffiliateID", pAffiliateid);
            }
            if (!pOutOfService.IsNullOrEmpty()) {
                parms.Add("OutOfService", pOutOfService);
            }
            if (!pVehicleNo.IsNullOrEmpty()) {
                parms.Add("VehicleNo", pVehicleNo);
            }
            if (!pID.IsNullOrEmpty()) {
                parms.Add("VehicleID", pID);
            }
            using (var conn = SqlHelper.OpenSqlConnection()) {
                return conn.Query<Vehicles>("usp_VehiclesGet", parms, commandType: System.Data.CommandType.StoredProcedure).ToList();
            }
        }



        #endregion



    }
}
