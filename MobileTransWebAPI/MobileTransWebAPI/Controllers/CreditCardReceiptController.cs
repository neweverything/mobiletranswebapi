﻿using CabRideEngine;
using IdeaBlade.Persistence;
using MobileTransWebAPI.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;
using System.Web.Hosting;
using System.Web.Http;
using System.Web.Script.Serialization;
using TaxiPassCommon;

namespace MobileTransWebAPI.Controllers {
	public partial class CreditCardReceiptController : ApiController {

		private string mAccountName = "";

		[HttpGet]
		public bool Get() {
			JavaScriptSerializer js = new JavaScriptSerializer();
			string text = "32C5AF29655D99EBE020BE72DAE2368EEB82A6E8C83C919E8ECF1F6ADF218C5E2B8A27EC0DBE6DEE20024EA69B3D71962C1D12CAF27D203B6DC20A8B329197A867674D3604A6158D7CBAC4063263DF216094AD8D93C0C593A09115EDABF59079113AFFDA095F2F2A4E2E6B6CD25F0DD05173E4E7FCF4183A3F49D4A4D758FA908AC996BACD89A8403F83EA1308CB6E271AF0A075".DecryptIceKey();

			//string[] emailList = File.ReadAllLines(@"d:\temp\resendemails.txt");
			bool sent = false;
			//foreach (string text in emailList) {
			try {
				ReceiptRequestInfo req = js.Deserialize<ReceiptRequestInfo>(text);
				req.RequestTime = DateTime.UtcNow.ToString();
				//req.TransNo = "E19BCJ";
				//req.ReceiptTo = Properties.Settings.Default.EMailErrorsTo;
				//if (req.ReceiptTo.Contains("@")) {
				sent = PostReceiptRequest(req);
				//}
			} catch (Exception ex) {
				Console.WriteLine(ex.Message);
			}
			//WebApiApplication.LogIt("Testing", "Email Sent: " + sent, @"d:\temp\emailtest.snt");
			//}
			return sent;
		}

		[HttpGet]
		public bool Get(string pRequest) {
			ReceiptRequestInfo req = JsonConvert.DeserializeObject<ReceiptRequestInfo>(pRequest.DecryptIceKey());
			return PostReceiptRequest(req);
		}


		// *************************************************************
		// Receipt Request
		// *************************************************************
		[HttpPost]
		public bool PostReceiptRequest(ReceiptRequestInfo pRequest) {
			JavaScriptSerializer js = new JavaScriptSerializer();

			WebApiApplication.LogIt("CreditCardReceiptRequest", js.Serialize(pRequest), "CreditCardReceipt_{0}.txt");


			if (pRequest.ReceiptTo.Equals(Properties.Settings.Default.EMailErrorsTo)) {
				WebApiApplication.LogIt("CreditCardReceiptRequest", js.Serialize(pRequest), "hani.txt");
			}

			PersistenceManager oPM = WebApiApplication.UserPM;
			if (oPM == null) {
				return false;
			}

			ReceiptRequestInfo info = pRequest;  // js.Deserialize<ReceiptRequestInfo>(pRequest.DecryptIceKey());
			if (info.TransNo.IsNullOrEmpty() || info.ReceiptTo.IsNullOrEmpty()) {
				return false;
			}

			DriverPayments oDriverPayments = DriverPayments.GetPayment(oPM, info.TransNo);
			if (oDriverPayments.IsNullEntity) {
				return false;
			}

			DriverPaymentGeoCodes geo = oDriverPayments.DriverPaymentGeoCodes;
			if (geo.IsNullEntity) {
				geo = DriverPaymentGeoCodes.Create(oDriverPayments);
			}
			geo.SendReceiptTo = info.ReceiptTo;
			geo.Save();

			if (info.ReceiptTo.Contains("@")) {
				if (oDriverPayments.Platform.StartsWith(Platforms.ChargePass.ToString(), StringComparison.CurrentCultureIgnoreCase)) {
					return EMailChargePassReceipt(oDriverPayments.TransNo, info.ReceiptTo);
				} else if (oDriverPayments.Affiliate.DefaultPlatform.StartsWith("EWR", StringComparison.CurrentCultureIgnoreCase) || oDriverPayments.Affiliate.DefaultPlatform.StartsWith("Taxi", StringComparison.CurrentCultureIgnoreCase) || oDriverPayments.Platform.Equals(Platforms.TaxiCharge.ToString(), StringComparison.CurrentCultureIgnoreCase)) {
					return EMailTaxiChargeReceipt(oDriverPayments.TransNo, info.ReceiptTo);
				}
			}
			//DateTime requestDate = Convert.ToDateTime(info.RequestTime.DecryptIceKey());



			string id = String.Format("dp={0}", oDriverPayments.TransNo).EncryptIceKey();
			string receiptURL = "";
			if (oDriverPayments.Affiliate.DefaultPlatform.Equals(CabRideEngineDapper.Platforms.StorePass.ToString())) {
				receiptURL = String.Format("{0}{1}", Properties.Settings.Default.StorePassReceiptURL, id);
			} else if (oDriverPayments.Affiliate.DefaultPlatform.Equals(CabRideEngineDapper.Platforms.ChargePass.ToString())) {
				receiptURL = String.Format("{0}{1}", Properties.Settings.Default.ChargePassReceiptURL, id);
			} else if (IsTaxiPassReceipt(oDriverPayments)) {
				receiptURL = String.Format("{0}{1}", Properties.Settings.Default.TaxiPassReceiptURL, id);
			} else {
				receiptURL = String.Format("{0}{1}", Properties.Settings.Default.GetRideReceiptURL, id);
				if (!oDriverPayments.Affiliate.ServiceType.ToLower().Contains("taxi")) {
					receiptURL = String.Format("{0}{1}", Properties.Settings.Default.LimoPassReceiptURL, id);
				}
			}

			if (info.ReceiptTo.Contains("@")) {
				string subject = GetSubjectLine(oDriverPayments, mAccountName);
				SendReceiptViaEMail(info.ReceiptTo, subject, BuildHTMLReceipt(oPM, oDriverPayments, oDriverPayments.Affiliate.ServiceType), oDriverPayments.Platform);
			} else {
				SendReceiptViaSMS(oPM, info.ReceiptTo, BuildSMSReceipt(oDriverPayments, id));
			}

			return true;
		}

		public static string GetSubjectLine(DriverPayments pDriverPayments, string pAccountName) {
			string subject = "NewTaxiPass Receipt";
			if (pDriverPayments.Platform.Equals(CabRideEngineDapper.Platforms.StorePass.ToString())) {
				subject = "StorePass Receipt";
			} else if (pDriverPayments.Platform.Equals(CabRideEngineDapper.Platforms.ChargePass.ToString())) {
				subject = "ChargePass Receipt";
			} else if (IsTaxiPassReceipt(pDriverPayments)) {
				subject = "TaxiPass Receipt";
			} else {
				pAccountName = GetAccountName(pDriverPayments, pAccountName);
				if (!pAccountName.IsNullOrEmpty()) {
					subject = pAccountName + " Receipt";
				}
			}
			return subject;
		}


		// *****************************************************************
		// Send the recept via sms
		// *****************************************************************
		[HttpPatch]
		public void SendReceiptViaSMS(PersistenceManager pPM, string pCellNumber, string pMessage) {
			MobileTransWebAPI.Utils.Common.SendSMS(SystemDefaults.GetDefaults(pPM), pCellNumber, pMessage);
		}


		// *****************************************************************
		// Email the receipt
		// 8/28/2013 make everything GetRide
		// *****************************************************************
		[HttpPatch]
		public void SendReceiptViaEMail(string pEMailTo, string pSubject, string pMessage, string pPlatform) {

			var cred = CabRideEngineDapper.SystemDefaultsDict.GetEmailCreds(CabRideEngineDapper.Enums.EMailReportFrom.Receipt);
			TaxiPassCommon.Mail.EMailMessage email = new TaxiPassCommon.Mail.EMailMessage(cred, pSubject, pMessage);
			email.UseWebServiceToSendMail = true;
			email.SendMail(pEMailTo, true);
		}


		// **************************************************************
		// Build a text only receipt for phone messaging
		// 8/28/2013 make everything GetRide
		// **************************************************************
		private string BuildSMSReceipt(DriverPayments pDriverPayments, string pID) {
			StringBuilder msg = new StringBuilder();

			if (IsTaxiPassReceipt(pDriverPayments)) {
				msg.Append(String.Format("Receipt for TaxiPass Voucher Number {0}", pDriverPayments.Test ? "Test" : pDriverPayments.TransNo));
				msg.Append(String.Format("\n\n{0}{1}", CabRideEngineDapper.SystemDefaultsDict.GetByKey("ReceiptURL", "TaxiPass", false).ValueString, pID));
			} else {
				string accountName = GetAccountName(pDriverPayments, "");
				string url = CabRideEngineDapper.SystemDefaultsDict.GetByKey("ReceiptURL", "GetRide", false).ValueString;
				if (accountName.IsNullOrEmpty()) {
					if (pDriverPayments.Platform.Equals(CabRideEngineDapper.Platforms.StorePass.ToString())) {
						accountName = pDriverPayments.Platform.ToString();
						url = CabRideEngineDapper.SystemDefaultsDict.GetByKey("ReceiptURL", "StorePass", false).ValueString; // Properties.Settings.Default.StorePassReceiptURL;
					} else if (pDriverPayments.Platform.Equals(CabRideEngineDapper.Platforms.ChargePass.ToString())) {
						accountName = pDriverPayments.Platform.ToString();
						accountName = "NewtekZero";
						url = CabRideEngineDapper.SystemDefaultsDict.GetByKey("ReceiptURL", "ChargePass", false).ValueString; //Properties.Settings.Default.ChargePassReceiptURL;
					} else {
						accountName = "NewtekZero";
					}
				}
				msg.Append(String.Format("Receipt for {1} Voucher Number {0}", pDriverPayments.Test ? "Test" : pDriverPayments.TransNo, accountName));
				msg.Append(String.Format("\n\n{0}{1}", url, pID));
				//} else {
				//	msg.Append(String.Format("Receipt for LimoPass Voucher Number {0}", pDriverPayments.Test ? "Test" : pDriverPayments.TransNo));
				//	msg.Append(String.Format("\n\n{0}{1}", Properties.Settings.Default.LimoPassReceiptURL, pID));
			}
			return msg.ToString();
		}


		// *************************************************************
		// Build a voucher receipt to email
		// Get Ride.TaxiPassFee is calculated:
		//		Calc total ServiceTipPerc
		//			'Half' to Gratuity
		//			Remainder goes to TPFee
		//		If a Ride coupon was used 
		//			Deduct the amt from TPFee
		// *************************************************************
		[HttpPatch]
		public string BuildHTMLReceipt(PersistenceManager pPM, DriverPayments pDriverPayments, string pLogo) {
			string sStage = "0";

			decimal dGratuity = pDriverPayments.Gratuity;
			decimal dDiscount = pDriverPayments.Discount;
			bool blnIsRide = false;
			string sReceipt = "starting";
			string sMarketing = "";
			StringBuilder sWork = new StringBuilder();
			int iStart;
			int iEnd;
			string sAccountNo = "";
			try {
				string sPath = HostingEnvironment.MapPath("~/HTMLTemplates/VoucherReceipt.htm");
				// sPath = @"C:\My Projects\VS2010 TaxiPass\MobileTransWebAPI\MobileTransWebAPI\HTMLTemplates\VoucherReceipt.htm";
				//string sPath = Path.Combine(Properties.Settings.Default.TemplatePath, "VoucherReceipt.htm");
				//if (HttpContext.Current.Request.Url.ToString().ToLower().Contains("localhost")) {
				//	sPath = @"C:\My Projects\VS2010 TaxiPass\MobileTransWebAPI\MobileTransWebAPI\HTMLTemplates\VoucherReceipt.htm";
				//	pLogo = "ride";
				//}
				WebApiApplication.LogIt("Voucher Receipt Path", sPath);

				sReceipt = System.IO.File.ReadAllText(sPath);

				// Logo  
				// 8/28/2013 make everything GetRide
				string sLogo = "http://GetRide.Com/images/Logo250WBlack.png";
				if (IsTaxiPassReceipt(pDriverPayments)) {
					sLogo = "http://GetRide.Com/images/taxipass.png";
				}
				if (pDriverPayments.Platform.Equals(CabRideEngineDapper.Platforms.StorePass.ToString())) {
					sLogo = "http://receipt.storepass.co/images/store-pass.png";
				} else if (pDriverPayments.Platform.Equals(CabRideEngineDapper.Platforms.ChargePass.ToString())) {
					sLogo = "http://receipt.chargepass.co/images/charge-pass.png";
				}
				string sLogoWidth = "280px";

				//string sCompany =  "TaxiPass";
				switch (pLogo.ToLower()) {
					case "taxi":
						sStage = "1";
						if (!IsTaxiPassReceipt(pDriverPayments)) {
							//sMarketing = "Save $5.00 on your first taxi when booked thru <a href='http://www.getride.com/App'>GetRide</a><span style='font-size: small'>&#0153;</span> iPhone/Android booking app";
						}
						break;

					case "ride":
						sStage = "2";

						blnIsRide = true;
						dGratuity = pDriverPayments.Gratuity + pDriverPayments.TaxiPassFee;
						dDiscount = pDriverPayments.RideCouponAmount;
						sMarketing = "Receipt";

						StringBuilder sql = new StringBuilder();
						sql.AppendFormat("usp_Reservation_GetAccount_From_DP @TransNo ='{0}'", pDriverPayments.TransNo);
						Entity[] oRideAccounts = DynamicEntities.DynamicDBCall(pPM, sql.ToString());
						sStage = "3";

						if (oRideAccounts != null) {
							if (oRideAccounts.Length > 0) {
								sAccountNo = oRideAccounts[0]["AccountNo"].ToString();
								mAccountName = oRideAccounts[0]["AccountName"].ToString();

								sStage = "4";
								sWork.AppendFormat("<Div class='Rounded' style='background-color: #A4E1FF;'><span style='font-size: 24pt'>{0} Receipt</span><br>", mAccountName);
								if (oRideAccounts[0]["AccountPhone"].ToString().Length > 0) {
									// AccountPhone is already formatted (nnn) nnn-nnnn
									sWork.Append(oRideAccounts[0]["AccountPhone"]);
								}
								sWork.Append("</div>");
								sStage = "5";

								sMarketing = sWork.ToString();
								sLogoWidth = "0px";
								sLogo = "http://GetRide.Com/images/Spacer.png";
								sStage = "6";
							}
						}


						break;

					default:
						sStage = "7";
						sMarketing = "LimoPass  Receipt";
						break;
				}

				sStage = "8";
				sReceipt = sReceipt.Replace("|logo", sLogo);
				sStage = "9";
				sReceipt = sReceipt.Replace("|sLogoWidth", sLogoWidth);
				sStage = "10";

				// sMarketing
				if (("" + pDriverPayments.Affiliate.DefaultPlatform).Equals(Platforms.ChargePass.ToString(), StringComparison.CurrentCultureIgnoreCase)) {
					sMarketing = "";
				}
				sReceipt = sReceipt.Replace("|Marketing", sMarketing);


				// Fleet
				string sFleet = pDriverPayments.Affiliate.Name;
				if (pDriverPayments.Drivers.FleetID.HasValue) {
					if (!IsTaxiPassReceipt(pDriverPayments)) {
						sFleet = pDriverPayments.Drivers.Fleets.Fleet;
					}
				}
				sReceipt = sReceipt.Replace("|fleet", sFleet);

				// Cab #
				if (pDriverPayments.VehicleID.HasValue) {
					sReceipt = sReceipt.Replace("|taxinumber", pDriverPayments.Vehicles.VehicleNo);
				} else {
					iStart = sReceipt.IndexOf("<tr id='trCab'>");
					iEnd = sReceipt.IndexOf("</tr>", iStart) + 5;
					sReceipt = sReceipt.Substring(0, iStart - 1) + sReceipt.Substring(iEnd);
				}

				// Date
				sReceipt = sReceipt.Replace("|date", String.Format("{0:f}", pDriverPayments.ChargeDate));

				// TransNo
				sReceipt = sReceipt.Replace("|transno", pDriverPayments.TransNo);

				// Card
				sReceipt = sReceipt.Replace("|card", pDriverPayments.CardNumberDisplay);

				// Fare
				string[] valList = CabRideEngineDapper.SystemDefaultsDict.GetByKey("Receipt", "EWRAffiliates", false).ValueString.Split('|');
				List<long> affIDList = new List<long>();
				foreach (string id in valList) {
					affIDList.Add(Convert.ToInt64(id));
				}

				string note = "";
				sReceipt = sReceipt.Replace("|fare", pDriverPayments.Fare.ToString("C"));
				if (pDriverPayments.Platform.Equals(Platforms.ChargePass.ToString())) {
					sReceipt = sReceipt.Replace("Fare:", "Total:");
				} else if (affIDList.Contains(pDriverPayments.AffiliateID.GetValueOrDefault(0))) {
					if (pDriverPayments.Tolls.GetValueOrDefault(0) == 0) {
						sReceipt = sReceipt.Replace("Fare:", "Fare plus Round Trip Tolls:");
					}
					note = "*Please note round trip tolls to New York average $20+";
				}


				// Tolls
				if (pDriverPayments.Tolls.GetValueOrDefault(0) == 0) {
					iStart = sReceipt.IndexOf("<tr id='trTolls'>");
					iEnd = sReceipt.IndexOf("</tr>", iStart) + 5;
					sReceipt = sReceipt.Substring(0, iStart - 1) + sReceipt.Substring(iEnd);
				} else {
					sReceipt = sReceipt.Replace("|tolls", pDriverPayments.Tolls.GetValueOrDefault(0).ToString("C"));

				}


				// Airport Fee
				if (pDriverPayments.AirportFee == 0) {
					iStart = sReceipt.IndexOf("<tr id='trAirport'>");
					iEnd = sReceipt.IndexOf("</tr>", iStart) + 5;
					sReceipt = sReceipt.Substring(0, iStart - 1) + sReceipt.Substring(iEnd);
				} else {
					sReceipt = sReceipt.Replace("|airport", pDriverPayments.AirportFee.ToString("C"));

				}

				// Misc Fee
				if (pDriverPayments.MiscFee == 0) {
					iStart = sReceipt.IndexOf("<tr id='trMisc'>");
					iEnd = sReceipt.IndexOf("</tr>", iStart) + 5;
					sReceipt = sReceipt.Substring(0, iStart - 1) + sReceipt.Substring(iEnd);
				} else {
					sReceipt = sReceipt.Replace("|misc", pDriverPayments.MiscFee.ToString("C"));
				}

				// Gratuity
				if (blnIsRide) {
					sReceipt = sReceipt.Replace("+ Gratuity:", "+ Service:");
					sReceipt = sReceipt.Replace("|gratuity", dGratuity.ToString("C"));
				} else {
					if (dGratuity == 0) {
						iStart = sReceipt.IndexOf("<tr id='trGratuity'>");
						iEnd = sReceipt.IndexOf("</tr>", iStart) + 5;
						sReceipt = sReceipt.Substring(0, iStart - 1) + sReceipt.Substring(iEnd);
					} else {
						sReceipt = sReceipt.Replace("|gratuity", dGratuity.ToString("C"));
					}
				}

				// TaxiPass Fee
				if (pDriverPayments.TaxiPassFee == 0 || blnIsRide) {
					iStart = sReceipt.IndexOf("<tr id='trTaxiPassFee'>");
					iEnd = sReceipt.IndexOf("</tr>", iStart) + 5;
					sReceipt = sReceipt.Substring(0, iStart - 1) + sReceipt.Substring(iEnd);
				} else {
					if (pDriverPayments.Platform.Equals(CabRideEngineDapper.Platforms.StorePass.ToString()) ||
						pDriverPayments.Platform.Equals(CabRideEngineDapper.Platforms.MedPass.ToString()) ||
						pDriverPayments.Platform.Equals(CabRideEngineDapper.Platforms.ZPass.ToString()) ||
						pDriverPayments.Platform.Equals(CabRideEngineDapper.Platforms.ChargePass.ToString())) {
						sReceipt = sReceipt.Replace("+ Extras", "+ Processing");
					} else if (sLogo.Equals("http://GetRide.Com/images/Logo250WBlack.png")) {
						sReceipt = sReceipt.Replace("+ Extras", "+ Tech Fee");
					} else if (pDriverPayments.Platform.Equals(CabRideEngineDapper.Platforms.EWRTablet.ToString())) {
						sReceipt = sReceipt.Replace("+ Extras", "+ Tech Fee");
					}
					sReceipt = sReceipt.Replace("|extra", pDriverPayments.TaxiPassFee.ToString("C"));
				}


				// Wait Time
				if (pDriverPayments.WaitTime == 0) {
					iStart = sReceipt.IndexOf("<tr id='trWaitTime'>");
					iEnd = sReceipt.IndexOf("</tr>", iStart) + 5;
					sReceipt = sReceipt.Substring(0, iStart - 1) + sReceipt.Substring(iEnd);
				} else {
					sReceipt = sReceipt.Replace("|waittime", pDriverPayments.WaitTime.Value.ToString("C"));
				}

				// Parking
				if (pDriverPayments.Parking == 0) {
					iStart = sReceipt.IndexOf("<tr id='trParking'>");
					iEnd = sReceipt.IndexOf("</tr>", iStart) + 5;
					sReceipt = sReceipt.Substring(0, iStart - 1) + sReceipt.Substring(iEnd);
				} else {
					sReceipt = sReceipt.Replace("|parking", pDriverPayments.Parking.Value.ToString("C"));
				}

				// Stops
				if (pDriverPayments.Stops == 0) {
					iStart = sReceipt.IndexOf("<tr id='trStops'>");
					iEnd = sReceipt.IndexOf("</tr>", iStart) + 5;
					sReceipt = sReceipt.Substring(0, iStart - 1) + sReceipt.Substring(iEnd);
				} else {
					sReceipt = sReceipt.Replace("|stops", pDriverPayments.Stops.Value.ToString("C"));
				}

				// Discount
				if (dDiscount == 0) {
					iStart = sReceipt.IndexOf("<tr id='trDiscount'>");
					iEnd = sReceipt.IndexOf("</tr>", iStart) + 5;
					sReceipt = sReceipt.Substring(0, iStart - 1) + sReceipt.Substring(iEnd);
				} else {
					sReceipt = sReceipt.Replace("|discount", dDiscount.ToString("C"));
				}

				// Total
				sReceipt = sReceipt.Replace("|total", pDriverPayments.TotalCharge.ToString("C"));

				// Signature image
				if (pDriverPayments.DriverPaymentAux.SignaturePath.IsNullOrEmpty()) {
					iStart = sReceipt.IndexOf("<tr id='trSignature'>");
					iEnd = sReceipt.IndexOf("</tr>", iStart) + 5;
					sReceipt = sReceipt.Substring(0, iStart - 1) + sReceipt.Substring(iEnd);
				} else {
					string ImgPath = ResizeSignatureImage(pPM, pDriverPayments.DriverPaymentAux.SignaturePath.Replace(@"\", "/"));
					sReceipt = sReceipt.Replace("|signature", ImgPath);
				}

				// Receipt.com hyperlink
				sWork.Remove(0, sWork.Length);
				if (IsTaxiPassReceipt(pDriverPayments)) {
					string sURL = "http://contact.taxipass.com";
					sWork.AppendFormat("<a href='{0}'>", sURL);
					sReceipt = sReceipt.Replace("|HeaderLink", sWork.ToString());

					sWork.Append("Click here for customer service</A>");
				} else {
					if (pDriverPayments.Platform.Equals(CabRideEngineDapper.Platforms.ChargePass.ToString()) ||
						pDriverPayments.Platform.Equals(CabRideEngineDapper.Platforms.StorePass.ToString()) ||
						pDriverPayments.Platform.Equals(CabRideEngineDapper.Platforms.ZPass.ToString()) ||
						pDriverPayments.Platform.Equals(CabRideEngineDapper.Platforms.MedPass.ToString())) {
						sWork = new StringBuilder();
						sReceipt = sReceipt.Replace("|HeaderLink", sWork.ToString());
					} else {
						//string sURL = Properties.Settings.Default.GetRideReceiptURL;
						//if (!sAccountNo.IsNullOrEmpty()) {
						//    sURL = sURL.Replace("http://", "http://" + sAccountNo + ".");
						//}
						//sWork.AppendFormat("<a href='{0}{1}'>", sURL, ("DP=" + pDriverPayments.TransNo).EncryptIceKey());
						//sReceipt = sReceipt.Replace("|HeaderLink", sWork.ToString());

						//sWork.Append("Get Receipt</A>");
						string inquiries = CabRideEngineDapper.SystemDefaultsDict.GetByKey("Receipt", "Inquiries", false).ValueString;
						sWork.Append(inquiries);
					}
				}
				if (!sWork.ToString().IsNullOrEmpty()) {
					if (!note.IsNullOrEmpty()) {
						note = "<br><br><br>" + note;
					}
					note = sWork.ToString() + note;
				}
				sReceipt = sReceipt.Replace("|ReceiptLink", note);
			} catch (Exception ex) {
				sReceipt = ex.Message + Environment.NewLine + "Stage: " + sStage + Environment.NewLine + sReceipt;
			}
			string footerMsg = pDriverPayments.Affiliate.ReceiptFooterText;
			if (!footerMsg.IsNullOrEmpty()) {
				footerMsg = $"{footerMsg}<br /><br />";
			}
			sReceipt = sReceipt.Replace("{footerMessage}", footerMsg);
			sReceipt = sReceipt.Replace("|HeaderLink", "");

			return sReceipt;
		}

		// **************************************************************
		// Voucher signatures are too big for Receipts, shrink em down
		// **************************************************************
		private string ResizeSignatureImage(PersistenceManager pPM, string pImage) {
			SystemDefaults oSysDefaults = SystemDefaults.GetDefaults(pPM);

			string pImgRoot = Properties.Settings.Default.SignaturePath;
			if (!pImgRoot.EndsWith(@"\")) {
				pImgRoot += @"\";
			}

			string sNewPath = "";
			try {
				sNewPath = pImgRoot;

				Image oImg = Image.FromFile(sNewPath + pImage.Replace("/", @"\"));
				TaxiPassCommon.Images.ImageHelper oImgHelper = new TaxiPassCommon.Images.ImageHelper();
				Size oSize = new Size(340, 128);

				oImg = oImgHelper.ResizeImage(oImg, oSize);

				sNewPath += Path.GetDirectoryName(pImage) + @"\Resized\";
				if (!Directory.Exists(sNewPath)) {
					Directory.CreateDirectory(sNewPath);
				}
				if (!File.Exists(sNewPath + Path.GetFileName(pImage))) {
					oImg.Save(sNewPath + Path.GetFileName(pImage));
				}

				sNewPath = oSysDefaults.VoucherImageURL.ToString().ToLower().Replace("vouchers", "Signatures")
					+ Path.GetDirectoryName(pImage).Replace(@"\", "/")
					+ @"/Resized/" + Path.GetFileName(pImage);
			} catch (Exception ex) {
				sNewPath = "Error: " + ex.Message;
			}
			return sNewPath;
		}

		[HttpPatch]
		public static bool IsTaxiPassReceipt(DriverPayments pDriverPayments) {
			return pDriverPayments.Platform.Equals(Platforms.Western.ToString())
				|| pDriverPayments.Platform.Equals(Platforms.TPPos.ToString())
				|| pDriverPayments.Platform.Equals(Platforms.NexStep.ToString())
				|| pDriverPayments.Platform.Equals(Platforms.Kiosk.ToString())
				|| pDriverPayments.Platform.Equals(Platforms.EWRTablet.ToString())
				|| pDriverPayments.Affiliate.DefaultPlatform.StartsWith("EWR");
		}

		[HttpPatch]
		public static string GetReceiptURL(DriverPayments pDriverPayments) {
			string url = Properties.Settings.Default.GetRideReceiptURL;
			if (IsTaxiPassReceipt(pDriverPayments)) {
				url = Properties.Settings.Default.TaxiPassReceiptURL;
			}
			return url;
		}

		[HttpPatch]
		public static string GetAccountName(DriverPayments pDriverPayments, string pAccountName) {
			if (pAccountName.IsNullOrEmpty()) {
				string sql = string.Format("usp_Reservation_GetAccount_From_DP @TransNo ='{0}'", pDriverPayments.TransNo);
				Entity[] oRideAccounts = DynamicEntities.DynamicDBCall(pDriverPayments.PersistenceManager, sql);
				if (oRideAccounts != null) {
					if (oRideAccounts.Length > 0) {
						pAccountName = oRideAccounts[0]["AccountName"].ToString();
					}
				}
			}
			return pAccountName;
		}



	}
}
