﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dapper {

	[Serializable]
	public class BaseEntity {

		//public bool IsDeserializing { get; set; }

		private DataRowState mRowState = DataRowState.Detached;
		[Editable(false)]
		public DataRowState RowState {
			get {
				return mRowState;
			}
			set {
				mRowState = value;
			}
		}

		public virtual void OnChanged() {
			if (RowState == DataRowState.Unchanged) {
				RowState = DataRowState.Modified;
			}
		}



	}
}
