﻿using CabRideEngineDapper.Utils;
using Dapper;
using System.Collections.Generic;
using System.Linq;

namespace CabRideEngineDapper {
    public partial class AffiliateDispatchStatus {

        public static List<AffiliateDispatchStatus> GetByAffiliate(long pAffiliateID) {
            string sql = @"SELECT * FROM dbo.AffiliateDispatchStatus
                            WHERE AffiliateID = @AffiliateID";

            using (var conn = SqlHelper.OpenSqlConnection()) {
                return conn.Query<AffiliateDispatchStatus>(sql, new { AffiliateID = pAffiliateID }).ToList();
            }
        }

    }
}