﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Dapper;

namespace CabRideEngineDapper {
    public partial class usp_DriverPaymentsGetCouponValue {
        public decimal TaxiPassFee { get; set; }
        public string CouponAmount { get; set; }
        public string CouponCode { get; set; }

        public static usp_DriverPaymentsGetCouponValue Execute(SqlConnection pConn, string pTransNo) {
            return pConn.Query<usp_DriverPaymentsGetCouponValue>("usp_DriverPaymentsGetCouponValue", new { TransNo = pTransNo }, commandType: CommandType.StoredProcedure).FirstOrDefault();
        }

        public static usp_DriverPaymentsGetCouponValue Execute(SqlConnection pConn, long pDriverPaymentID) {
            return pConn.Query<usp_DriverPaymentsGetCouponValue>("usp_DriverPaymentsGetCouponValue", new { DriverPaymentID = pDriverPaymentID }, commandType: CommandType.StoredProcedure).FirstOrDefault();
        }

    }
}
