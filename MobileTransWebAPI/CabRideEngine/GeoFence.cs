﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;
using Csla.Validation;

namespace CabRideEngine {
    /// <summary>
    /// The developer class representing the GeoFence business object class.
    /// </summary>
    /// <remarks>
    /// <para>Place all application-specific business logic in this class.</para>
    /// <para>
    /// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
    /// and will not overwrite changes.
    /// </para>
    /// <para>
    /// This class inherits from the generated class, its companion in the pair of classes 
    /// defining a business object. Properties in the generated class may be overridden in 
    /// order to change the behavior or add business logic.
    /// </para>
    /// <para>
    /// This class is the final class in the inheritance chain for this business object. 
    /// It should not be inherited.
    /// </para>
    /// </remarks>
    [Serializable]
    public sealed partial class GeoFence : GeoFenceDataRow {

        #region Constructors -- DO NOT MODIFY

        // Do not create constructors for this class
        // Developers cannot create instances of this class with the "new" keyword
        // You should write a static Create method instead; see the "Suggested Customization" region
        // Please review the documentation to learn about entity creation and inheritance

        // This private constructor prevents accidental instance creation via 'new';
        // it is also required if the class is visible from a web service.
        // DO NOT REMOVE **
        private GeoFence() : this(null) { }

        /// <summary>
        /// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
        /// </summary>
        /// <param name="pRowBuilder"></param>
        public GeoFence(DataRowBuilder pRowBuilder)
            : base(pRowBuilder) {
        }

        #endregion

        #region Suggested Customizations

        //    // Use this factory method template to create new instances of this class
        //    public static GeoFence Create(PersistenceManager pManager,
        //      ... your creation parameters here ...) { 
        //
        //      GeoFence aGeoFence = pManager.CreateEntity<GeoFence>();
        //
        //      // if this object type requires a unique id and you have implemented
        //      // the IIdGenerator interface implement the following line
        //      pManager.GenerateId(aGeoFence, // add id column here //);
        //
        //      // Add custom code here
        //
        //      aGeoFence.AddToManager();
        //      return aGeoFence;
        //    }

        //    // Implement this method if you want your object to sort in a specific order
        //    public override int CompareTo(Object pObject) {
        //    }

        //    // Implement this method to customize the null object version of this class
        //    protected override void UpdateNullEntity() {
        //    }

        #endregion

        // Add additional logic to your business object here...
        public static GeoFence Create(Affiliate pAffiliate) {
            PersistenceManager oPM = pAffiliate.PersistenceManager;
            GeoFence rec = null;
            try {
                rec = oPM.CreateEntity<GeoFence>();
                
                // Using the IdeaBlade Id Generation technique
                oPM.GenerateId(rec, GeoFence.GeoFenceIDEntityColumn);
                rec.AffiliateID = pAffiliate.AffiliateID;

                rec.AddToManager();
            } catch (Exception ex) {
                throw ex;
            }
            return rec;
        }

        protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
            if (IsDeserializing) {
                return;
            }
            base.OnColumnChanging(pArgs);
            DoAudit(pArgs);
        }

        private void DoAudit(DataColumnChangeEventArgs pArgs) {
            if (!this.PersistenceManager.IsClient) {
                return;
            }
            if (this.RowState == DataRowState.Added) {
                return;
            }
            if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
                AuditRecord = GeoFenceAudit.Create(this.PersistenceManager, this);
            }
        }

        protected override void AddRules(RuleList pList) {
            PropertyRequiredRule.AddToList(pList, ZoneColumn.ColumnName);
            pList.Add(IsZoneUnique, ZoneEntityColumn.ColumnName);

            base.AddRules(pList);
        }

        private bool IsZoneUnique(object pTarget, RuleArgs e) {
            GeoFence rec = pTarget as GeoFence;

            RdbQuery qry = new RdbQuery(typeof(GeoFence), GeoFence.ZoneEntityColumn, EntityQueryOp.EQ, rec.Zone);
            qry.AddClause(GeoFence.AffiliateIDEntityColumn, EntityQueryOp.EQ, rec.AffiliateID);
            qry.AddClause(GeoFence.GeoFenceIDEntityColumn, EntityQueryOp.NE, rec.GeoFenceID);
            EntityList<GeoFence> recList = this.PersistenceManager.GetEntities<GeoFence>(qry, QueryStrategy.DataSourceThenCache);
            if (recList.Count > 0) {
                e.Description = "GeoFence Zone name must be unique!";
                return false;
            }
            return true;
        }
    }

    #region EntityPropertyDescriptors.GeoFencePropertyDescriptor
    ////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
    ////    For example: 
    ////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
    //public static partial class EntityPropertyDescriptors {
    //	public partial class GeoFencePropertyDescriptor : BaseEntityPropertyDescriptor {
    //    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
    //    public AdaptedPropertyDescriptor Age {
    //      get { return this.Get("Age"); }
    //    }
    //  }
    //} 
    #endregion

}