﻿using Dapper;
using System;
using System.Data.SqlClient;

namespace CabRideEngineDapper {
    public partial class NachaDetail {

        public static NachaDetail GetOrCreate(SqlConnection pConn, string pFileName, string pTransType, string pReferenceNo, DateTime pTransDate) {
            string sql = @"SELECT *
                            FROM   NachaDetail
                            WHERE  TransDate = @TransDate
                                   AND FileName = @FileName
                                   AND TransType = @TransType
                                   AND ReferenceNo = @ReferenceNo";

            NachaDetail rec = pConn.Get<NachaDetail>(sql, new { TransDate = pTransDate, FileName = pFileName, TransType = pTransType, ReferenceNo = pReferenceNo });
            if (rec.IsNullEntity) {
                rec = Create();
            }
            return rec;
        }
    }
}
