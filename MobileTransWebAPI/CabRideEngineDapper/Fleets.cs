﻿using CabRideEngineDapper.Utils;
using Dapper;

namespace CabRideEngineDapper {
    public partial class Fleets {

        public static Fleets GetFleet(long pFleetID) {
            using (var conn = SqlHelper.OpenSqlConnection()) {
                return conn.Get<Fleets>(pFleetID);
            }
        }
    }
}
