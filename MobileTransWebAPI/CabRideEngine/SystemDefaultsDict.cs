using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

using TaxiPassCommon;
using System.Text;

namespace CabRideEngine {
	[Serializable]
	public sealed partial class SystemDefaultsDict : SystemDefaultsDictDataRow {

		#region Constructors -- DO NOT MODIFY

		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new';
		// it is also required if the class is visible from a web service.
		// DO NOT REMOVE **
		private SystemDefaultsDict() : this(null) { }

		/// <summary>
		/// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
		/// </summary>
		/// <param name="pRowBuilder"></param>
		public SystemDefaultsDict(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static SystemDefaultsDict Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      SystemDefaultsDict aSystemDefaultsDict = pManager.CreateEntity<SystemDefaultsDict>();
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aSystemDefaultsDict, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aSystemDefaultsDict.AddToManager();
		//      return aSystemDefaultsDict;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		// Add additional logic to your business object here...
		public static SystemDefaultsDict Create(PersistenceManager pManager) {
			SystemDefaultsDict oSystemDefaultsDict = null;
			try {
				oSystemDefaultsDict = (SystemDefaultsDict)pManager.CreateEntity(typeof(SystemDefaultsDict));
				oSystemDefaultsDict.InActive = false;
				oSystemDefaultsDict.AddToManager();

			} catch (Exception ex) {
				throw ex;
			}
			return oSystemDefaultsDict;
		}

		// ********************************************************************************************
		// Get Or Create record
		// ********************************************************************************************
		public static SystemDefaultsDict GetOrCreate(PersistenceManager pPM, string pKey) {
			SystemDefaultsDict rec = GetByKey(pPM, pKey);

			if (rec.IsNullEntity) {
				rec = Create(pPM);
				rec.ReferenceKey = pKey;

			}

			return rec;
		}


		// ********************************************************************************************
		// Get Smart Phone Update Driver Profile Duration
		// ********************************************************************************************
		public static int SmartPhoneUpdateDriverProfileDuration(PersistenceManager pPM) {
			SystemDefaultsDict rec = GetByKey(pPM, "SmartPhoneUpdateDriverProfileDuration");
			return rec.ValueString.IsNullOrEmpty() ? 0 : Convert.ToInt32(rec.ValueString);
		}

		public static int SmartPhoneUpdateDriverProfileDuration(PersistenceManager pPM, int pDuration) {
			SystemDefaultsDict rec = SystemDefaultsDict.GetOrCreate(pPM, "SmartPhoneUpdateDriverProfileDuration");
			rec.ValueString = pDuration.ToString();
			
			return rec.ValueString.IsNullOrEmpty() ? 0 : Convert.ToInt32(rec.ValueString);
		}

		// ********************************************************************************************
		// Get Black Listed Area Codes
		// ********************************************************************************************
		public static string BlackListedAreaCodes(PersistenceManager pPM) {
			SystemDefaultsDict rec = SystemDefaultsDict.GetByKey(pPM, "BlackListedAreaCodes");
			return rec.ValueString;
		}

		public static string BlackListedAreaCodes(PersistenceManager pPM, string pAreaCodes) {
			SystemDefaultsDict rec = SystemDefaultsDict.GetOrCreate(pPM, "BlackListedAreaCodes");
			rec.ValueString = pAreaCodes;

			return rec.ValueString;
		}



		// ********************************************************************************************
		// Return all rows
		// ********************************************************************************************
		public static EntityList<SystemDefaultsDict> GetAll(PersistenceManager pPM, bool pIncludeInactive) {
			RdbQuery oQry = new RdbQuery(typeof(SystemDefaultsDict));
			if (!pIncludeInactive) {
				oQry.AddClause(SystemDefaultsDict.InActiveEntityColumn, EntityQueryOp.EQ, true);
			}
			oQry.AddOrderBy(SystemDefaultsDict.ReferenceGroupEntityColumn);
			oQry.AddOrderBy(SystemDefaultsDict.ReferenceKeyEntityColumn);
			return pPM.GetEntities<SystemDefaultsDict>(oQry);
		}

		// ********************************************************************************************
		// Return all rows for a Group
		// ********************************************************************************************
		public static EntityList<SystemDefaultsDict> GetGroup(PersistenceManager pPM, string pGroup, bool pIncludeInactive) {
			StringBuilder sql = new StringBuilder();
			sql.AppendFormat("usp_SystemDefaultDictGet @ReferenceGroup='{0}'", pGroup);
			if (!pIncludeInactive) {
				sql.Append(", @Inactive = 0");
			}
			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(SystemDefaultsDict), sql.ToString());
			EntityList<SystemDefaultsDict> oSystemDefaultsDict = pPM.GetEntities<SystemDefaultsDict>(qry);
			return oSystemDefaultsDict;
		}

		// ********************************************************************************************
		// Given a key and active status, return the row
		// ********************************************************************************************
		public static SystemDefaultsDict GetByKey(PersistenceManager pPM, string pGroup, string pKey, bool pInactive) {
			RdbQuery oQry = new RdbQuery(typeof(SystemDefaultsDict), SystemDefaultsDict.ReferenceKeyEntityColumn, EntityQueryOp.EQ, pKey);
			oQry.AddClause(SystemDefaultsDict.ReferenceGroupEntityColumn, EntityQueryOp.EQ, pGroup);
			oQry.AddClause(SystemDefaultsDict.InActiveEntityColumn, EntityQueryOp.EQ, pInactive);
			return pPM.GetEntity<SystemDefaultsDict>(oQry);
		}

		// ********************************************************************************************
		// Given a key, return the row regardless of active status
		// ********************************************************************************************
		public static SystemDefaultsDict GetByKey(PersistenceManager pPM, string pKey) {
			RdbQuery oQry = new RdbQuery(typeof(SystemDefaultsDict), SystemDefaultsDict.ReferenceKeyEntityColumn, EntityQueryOp.EQ, pKey);
			return pPM.GetEntity<SystemDefaultsDict>(oQry);
		}
	}

	#region EntityPropertyDescriptors.SystemDefaultsDictPropertyDescriptor
	////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
	////    For example: 
	////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
	//public static partial class EntityPropertyDescriptors {
	//	public partial class SystemDefaultsDictPropertyDescriptor : BaseEntityPropertyDescriptor {
	//    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
	//    public AdaptedPropertyDescriptor Age {
	//      get { return this.Get("Age"); }
	//    }
	//  }
	//} 
	#endregion

}