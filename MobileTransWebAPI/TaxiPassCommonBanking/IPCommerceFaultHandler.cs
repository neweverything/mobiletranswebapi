﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

namespace TaxiPassCommon.Banking {
	public class IPCommerceFaultHandler {
		public bool handleSvcInfoFault(Exception _ex, out string _strErrorID, out string _strErrorMessage) {//Note : that the boolean indicates if the fault was handled by this class

			_strErrorID = "";
			_strErrorMessage = "";

			#region AuthorizationFault
			try {
				if (((FaultException<TaxiPassCommon.Banking.com.ipcommerce.SvcInfo.AuthorizationFault>)(_ex)).Detail != null) {
					_strErrorID = ((FaultException<TaxiPassCommon.Banking.com.ipcommerce.SvcInfo.AuthorizationFault>)(_ex)).Detail.ErrorID.ToString();
					_strErrorMessage = ((FaultException<TaxiPassCommon.Banking.com.ipcommerce.SvcInfo.AuthorizationFault>)(_ex)).Detail.ProblemType;
					return true;//Positive Match
				}
			} catch { }
			#endregion AuthorizationFault

			#region ClaimNotFoundFault
			try {
				if (((FaultException<TaxiPassCommon.Banking.com.ipcommerce.SvcInfo.ClaimNotFoundFault>)(_ex)).Detail != null) {
					_strErrorID = ((FaultException<TaxiPassCommon.Banking.com.ipcommerce.SvcInfo.ClaimNotFoundFault>)(_ex)).Detail.ErrorID.ToString();
					_strErrorMessage = ((FaultException<TaxiPassCommon.Banking.com.ipcommerce.SvcInfo.ClaimNotFoundFault>)(_ex)).Detail.ProblemType;
					return true;//Positive Match
				}
			} catch { }
			#endregion ClaimNotFoundFault

			#region CWSValidationResultFault
			try {
				if (((FaultException<TaxiPassCommon.Banking.com.ipcommerce.SvcInfo.CWSValidationResultFault>)(_ex)).Detail != null) {
					foreach (TaxiPassCommon.Banking.com.ipcommerce.SvcInfo.CWSValidationErrorFault error in ((FaultException<TaxiPassCommon.Banking.com.ipcommerce.SvcInfo.CWSValidationResultFault>)(_ex)).Detail.Errors) {
						_strErrorMessage = _strErrorMessage + error.RuleKey + " : " + error.RuleMessage + "\r\n\r\n";
					}
					return true;//Positive Match
				}
			} catch { }
			#endregion CWSValidationResultFault

			#region AuthenticationFault
			try {
				if (((FaultException<TaxiPassCommon.Banking.com.ipcommerce.SvcInfo.AuthenticationFault>)(_ex)).Detail != null) {
					_strErrorID = ((FaultException<TaxiPassCommon.Banking.com.ipcommerce.SvcInfo.AuthenticationFault>)(_ex)).Detail.ErrorID.ToString();
					_strErrorMessage = ((FaultException<TaxiPassCommon.Banking.com.ipcommerce.SvcInfo.AuthenticationFault>)(_ex)).Detail.ProblemType;
					return true;//Positive Match
				}
			} catch { }
			#endregion AuthenticationFault

			#region STSUnavailableFault
			try {
				if (((FaultException<TaxiPassCommon.Banking.com.ipcommerce.SvcInfo.STSUnavailableFault>)(_ex)).Detail != null) {
					_strErrorID = ((FaultException<TaxiPassCommon.Banking.com.ipcommerce.SvcInfo.STSUnavailableFault>)(_ex)).Detail.ErrorID.ToString();
					_strErrorMessage = ((FaultException<TaxiPassCommon.Banking.com.ipcommerce.SvcInfo.STSUnavailableFault>)(_ex)).Detail.ProblemType;
					return true;//Positive Match
				}
			} catch { }
			#endregion STSUnavailableFault

			#region ExpiredTokenFault
			try {
				if (((FaultException<TaxiPassCommon.Banking.com.ipcommerce.SvcInfo.ExpiredTokenFault>)(_ex)).Detail != null) {
					_strErrorID = ((FaultException<TaxiPassCommon.Banking.com.ipcommerce.SvcInfo.ExpiredTokenFault>)(_ex)).Detail.ErrorID.ToString();
					_strErrorMessage = ((FaultException<TaxiPassCommon.Banking.com.ipcommerce.SvcInfo.ExpiredTokenFault>)(_ex)).Detail.ProblemType;
					return true;//Positive Match
				}
			} catch { }
			#endregion AuthenticationFault

			#region InvalidTokenFault
			try {
				if (((FaultException<TaxiPassCommon.Banking.com.ipcommerce.SvcInfo.InvalidTokenFault>)(_ex)).Detail != null) {
					_strErrorID = ((FaultException<TaxiPassCommon.Banking.com.ipcommerce.SvcInfo.InvalidTokenFault>)(_ex)).Detail.ErrorID.ToString();
					_strErrorMessage = ((FaultException<TaxiPassCommon.Banking.com.ipcommerce.SvcInfo.InvalidTokenFault>)(_ex)).Detail.ProblemType;
					return true;//Positive Match
				}
			} catch { }
			#endregion InvalidTokenFault

			#region CWSServiceInformationUnavailableFault
			try {
				if (((FaultException<TaxiPassCommon.Banking.com.ipcommerce.SvcInfo.CWSServiceInformationUnavailableFault>)(_ex)).Detail != null) {
					_strErrorID = ((FaultException<TaxiPassCommon.Banking.com.ipcommerce.SvcInfo.CWSServiceInformationUnavailableFault>)(_ex)).Detail.ErrorID.ToString();
					_strErrorMessage = ((FaultException<TaxiPassCommon.Banking.com.ipcommerce.SvcInfo.CWSServiceInformationUnavailableFault>)(_ex)).Detail.ProblemType;
					return true;//Positive Match
				}
			} catch { }
			#endregion CWSServiceInformationUnavailableFault

			//Additional Faults called out in the data contract

			#region CWSValidationErrorFault
			try {
				if (((FaultException<TaxiPassCommon.Banking.com.ipcommerce.SvcInfo.CWSValidationErrorFault>)(_ex)).Detail != null) {
					_strErrorID = ((FaultException<TaxiPassCommon.Banking.com.ipcommerce.SvcInfo.CWSValidationErrorFault>)(_ex)).Detail.RuleKey;
					_strErrorMessage = ((FaultException<TaxiPassCommon.Banking.com.ipcommerce.SvcInfo.CWSValidationErrorFault>)(_ex)).Detail.RuleMessage;
					return true;//Positive Match
				}
			} catch { }
			#endregion CWSValidationErrorFault

			#region Presently Unsupported Faults
			try {
				if (((FaultException<TaxiPassCommon.Banking.com.ipcommerce.SvcInfo.BadAttemptThresholdExceededFault>)(_ex)).Detail != null)
					_strErrorMessage = "BadAttemptThresholdExceededFault thrown however not handled by code";
				if (((FaultException<TaxiPassCommon.Banking.com.ipcommerce.SvcInfo.GeneratePasswordFault>)(_ex)).Detail != null)
					_strErrorMessage = "GeneratePasswordFault thrown however not handled by code";
				if (((FaultException<TaxiPassCommon.Banking.com.ipcommerce.SvcInfo.InvalidEmailFault>)(_ex)).Detail != null)
					_strErrorMessage = "InvalidEmailFault thrown however not handled by code";
				if (((FaultException<TaxiPassCommon.Banking.com.ipcommerce.SvcInfo.LockedByAdminFault>)(_ex)).Detail != null)
					_strErrorMessage = "LockedByAdminFault thrown however not handled by code";
				if (((FaultException<TaxiPassCommon.Banking.com.ipcommerce.SvcInfo.OneTimePasswordFault>)(_ex)).Detail != null)
					_strErrorMessage = "OneTimePasswordFault thrown however not handled by code";
				if (((FaultException<TaxiPassCommon.Banking.com.ipcommerce.SvcInfo.PasswordExpiredFault>)(_ex)).Detail != null)
					_strErrorMessage = "PasswordExpiredFault thrown however not handled by code";
				if (((FaultException<TaxiPassCommon.Banking.com.ipcommerce.SvcInfo.PasswordInvalidFault>)(_ex)).Detail != null)
					_strErrorMessage = "PasswordInvalidFault thrown however not handled by code";
				if (((FaultException<TaxiPassCommon.Banking.com.ipcommerce.SvcInfo.SendEmailFault>)(_ex)).Detail != null)
					_strErrorMessage = "SendEmailFault thrown however not handled by code";
				if (((FaultException<TaxiPassCommon.Banking.com.ipcommerce.SvcInfo.STSUnavailableFault>)(_ex)).Detail != null)
					_strErrorMessage = "STSUnavailableFault thrown however not handled by code";
				if (((FaultException<TaxiPassCommon.Banking.com.ipcommerce.SvcInfo.UserNotFoundFault>)(_ex)).Detail != null)
					_strErrorMessage = "UserNotFoundFault thrown however not handled by code";
			} catch { }
			#endregion Presently Unsupported Faults

			//As one last check look at the GeneralFaults
			if (GeneralFaults(_ex, out _strErrorID, out _strErrorMessage))
				return true;//Positive Match

			_strErrorMessage = "An unhandled fault was thrown \r\nMessage : " + _ex.Message;
			return false;//In this case unable to match fault so return false. 
		}

		public bool handleTxnFault(Exception _ex, out string _strErrorID, out string _strErrorMessage) {//Note : that the boolean indicates if the fault was handled by this class
			_strErrorID = "";
			_strErrorMessage = "";

			#region CWSConnectionFault
			try {
				if (((FaultException<TaxiPassCommon.Banking.com.ipcommerce.Txn.CWSConnectionFault>)(_ex)).Detail != null) {
					_strErrorID = ((FaultException<TaxiPassCommon.Banking.com.ipcommerce.Txn.CWSConnectionFault>)(_ex)).Detail.ErrorID.ToString();
					_strErrorMessage = ((FaultException<TaxiPassCommon.Banking.com.ipcommerce.Txn.CWSConnectionFault>)(_ex)).Detail.ProblemType;
					return true;//Positive Match
				}
			} catch { }
			#endregion CWSConnectionFault

			#region CWSExtendedDataNotSupportedFault
			try {
				if (((FaultException<TaxiPassCommon.Banking.com.ipcommerce.Txn.CWSExtendedDataNotSupportedFault>)(_ex)).Detail != null) {
					_strErrorID = ((FaultException<TaxiPassCommon.Banking.com.ipcommerce.Txn.CWSExtendedDataNotSupportedFault>)(_ex)).Detail.ErrorID.ToString();
					_strErrorMessage = ((FaultException<TaxiPassCommon.Banking.com.ipcommerce.Txn.CWSExtendedDataNotSupportedFault>)(_ex)).Detail.ProblemType;
					return true;//Positive Match
				}
			} catch { }
			#endregion CWSExtendedDataNotSupportedFault

			#region CWSInvalidOperationFault
			try {
				if (((FaultException<TaxiPassCommon.Banking.com.ipcommerce.Txn.CWSInvalidOperationFault>)(_ex)).Detail != null) {
					_strErrorID = ((FaultException<TaxiPassCommon.Banking.com.ipcommerce.Txn.CWSInvalidOperationFault>)(_ex)).Detail.ErrorID.ToString();
					_strErrorMessage = ((FaultException<TaxiPassCommon.Banking.com.ipcommerce.Txn.CWSInvalidOperationFault>)(_ex)).Detail.ProblemType;
					return true;//Positive Match
				}
			} catch { }
			#endregion CWSInvalidOperationFault

			#region CWSOperationNotSupportedFault
			try {
				if (((FaultException<TaxiPassCommon.Banking.com.ipcommerce.Txn.CWSOperationNotSupportedFault>)(_ex)).Detail != null) {
					_strErrorID = ((FaultException<TaxiPassCommon.Banking.com.ipcommerce.Txn.CWSOperationNotSupportedFault>)(_ex)).Detail.ErrorID.ToString();
					_strErrorMessage = ((FaultException<TaxiPassCommon.Banking.com.ipcommerce.Txn.CWSOperationNotSupportedFault>)(_ex)).Detail.ProblemType;
					return true;//Positive Match
				}
			} catch { }
			#endregion CWSOperationNotSupportedFault

			#region CWSValidationResultFault
			try {
				if (((FaultException<TaxiPassCommon.Banking.com.ipcommerce.Txn.CWSValidationResultFault>)(_ex)).Detail != null) {
					foreach (TaxiPassCommon.Banking.com.ipcommerce.Txn.CWSValidationErrorFault error in ((FaultException<TaxiPassCommon.Banking.com.ipcommerce.Txn.CWSValidationResultFault>)(_ex)).Detail.Errors) {
						_strErrorMessage = _strErrorMessage + error.RuleKey + " : " + error.RuleMessage + "\r\n";
					}
					return true;//Positive Match
				}
			} catch { }
			#endregion CWSValidationResultFault

			#region AuthenticationFault

			try {
				if (((FaultException<TaxiPassCommon.Banking.com.ipcommerce.Txn.AuthenticationFault>)(_ex)).Detail != null) {
					_strErrorID = ((FaultException<TaxiPassCommon.Banking.com.ipcommerce.Txn.AuthenticationFault>)(_ex)).Detail.ErrorID.ToString();
					_strErrorMessage = ((FaultException<TaxiPassCommon.Banking.com.ipcommerce.Txn.AuthenticationFault>)(_ex)).Detail.ProblemType;
					return true;//Positive Match
				}
			} catch { }
			#endregion AuthenticationFault

			#region ExpiredTokenFault
			try {
				if (((FaultException<TaxiPassCommon.Banking.com.ipcommerce.Txn.ExpiredTokenFault>)(_ex)).Detail != null) {
					_strErrorID = ((FaultException<TaxiPassCommon.Banking.com.ipcommerce.Txn.ExpiredTokenFault>)(_ex)).Detail.ErrorID.ToString();
					_strErrorMessage = ((FaultException<TaxiPassCommon.Banking.com.ipcommerce.Txn.ExpiredTokenFault>)(_ex)).Detail.ProblemType;
					return true;//Positive Match
				}
			} catch { }
			#endregion ExpiredTokenFault

			#region InvalidTokenFault
			try {
				if (((FaultException<TaxiPassCommon.Banking.com.ipcommerce.Txn.InvalidTokenFault>)(_ex)).Detail != null) {
					_strErrorID = ((FaultException<TaxiPassCommon.Banking.com.ipcommerce.Txn.InvalidTokenFault>)(_ex)).Detail.ErrorID.ToString();
					_strErrorMessage = ((FaultException<TaxiPassCommon.Banking.com.ipcommerce.Txn.InvalidTokenFault>)(_ex)).Detail.ProblemType;
					return true;//Positive Match
				}
			} catch { }
			#endregion InvalidTokenFault

			#region CWSTransactionServiceUnavailableFault
			try {
				if (((FaultException<TaxiPassCommon.Banking.com.ipcommerce.Txn.CWSTransactionServiceUnavailableFault>)(_ex)).Detail != null) {
					_strErrorID = ((FaultException<TaxiPassCommon.Banking.com.ipcommerce.Txn.CWSTransactionServiceUnavailableFault>)(_ex)).Detail.ErrorID.ToString();
					_strErrorMessage = ((FaultException<TaxiPassCommon.Banking.com.ipcommerce.Txn.CWSTransactionServiceUnavailableFault>)(_ex)).Detail.ProblemType;
					return true;//Positive Match
				}
			} catch { }
			#endregion CWSTransactionServiceUnavailableFault

			#region CWSInvalidMessageFormatFault
			try {
				if (((FaultException<TaxiPassCommon.Banking.com.ipcommerce.Txn.CWSInvalidMessageFormatFault>)(_ex)).Detail != null) {
					_strErrorID = ((FaultException<TaxiPassCommon.Banking.com.ipcommerce.Txn.CWSInvalidMessageFormatFault>)(_ex)).Detail.ErrorID.ToString();
					_strErrorMessage = ((FaultException<TaxiPassCommon.Banking.com.ipcommerce.Txn.CWSInvalidMessageFormatFault>)(_ex)).Detail.ProblemType;
					return true;//Positive Match
				}
			} catch { }
			#endregion CWSInvalidMessageFormatFault

			#region CWSInvalidServiceInformationFault
			try {
				if (((FaultException<TaxiPassCommon.Banking.com.ipcommerce.Txn.CWSInvalidServiceInformationFault>)(_ex)).Detail != null) {
					_strErrorID = ((FaultException<TaxiPassCommon.Banking.com.ipcommerce.Txn.CWSInvalidServiceInformationFault>)(_ex)).Detail.ErrorID.ToString();
					_strErrorMessage = ((FaultException<TaxiPassCommon.Banking.com.ipcommerce.Txn.CWSInvalidServiceInformationFault>)(_ex)).Detail.ProblemType;
					return true;//Positive Match
				}
			} catch { }
			#endregion CWSInvalidServiceInformationFault

			#region CWSTransactionAlreadySettledFault
			try {
				if (((FaultException<TaxiPassCommon.Banking.com.ipcommerce.Txn.CWSTransactionAlreadySettledFault>)(_ex)).Detail != null) {
					_strErrorID = ((FaultException<TaxiPassCommon.Banking.com.ipcommerce.Txn.CWSTransactionAlreadySettledFault>)(_ex)).Detail.ErrorID.ToString();
					_strErrorMessage = ((FaultException<TaxiPassCommon.Banking.com.ipcommerce.Txn.CWSTransactionAlreadySettledFault>)(_ex)).Detail.ProblemType;
					return true;//Positive Match
				}
			} catch { }
			#endregion CWSTransactionAlreadySettledFault

			#region CWSTransactionFailedFault
			try {
				if (((FaultException<TaxiPassCommon.Banking.com.ipcommerce.Txn.CWSTransactionFailedFault>)(_ex)).Detail != null) {
					_strErrorID = ((FaultException<TaxiPassCommon.Banking.com.ipcommerce.Txn.CWSTransactionFailedFault>)(_ex)).Detail.ErrorID.ToString();
					_strErrorMessage = ((FaultException<TaxiPassCommon.Banking.com.ipcommerce.Txn.CWSTransactionFailedFault>)(_ex)).Detail.ProblemType;
					return true;//Positive Match
				}
			} catch { }
			#endregion CWSTransactionFailedFault

			//Additional Faults called out in the data contract
			#region CWSValidationErrorFault
			try {
				if (((FaultException<TaxiPassCommon.Banking.com.ipcommerce.Txn.CWSValidationErrorFault>)(_ex)).Detail != null) {
					_strErrorID = ((FaultException<TaxiPassCommon.Banking.com.ipcommerce.Txn.CWSValidationErrorFault>)(_ex)).Detail.RuleKey;
					_strErrorMessage = ((FaultException<TaxiPassCommon.Banking.com.ipcommerce.Txn.CWSValidationErrorFault>)(_ex)).Detail.RuleMessage;
					return true;//Positive Match
				}
			} catch { }
			#endregion CWSValidationErrorFault

			#region CWSDeserializationFault
			try {
				if (((FaultException<TaxiPassCommon.Banking.com.ipcommerce.Txn.CWSDeserializationFault>)(_ex)).Detail != null) {
					_strErrorID = ((FaultException<TaxiPassCommon.Banking.com.ipcommerce.Txn.CWSDeserializationFault>)(_ex)).Detail.ErrorID.ToString();
					_strErrorMessage = ((FaultException<TaxiPassCommon.Banking.com.ipcommerce.Txn.CWSDeserializationFault>)(_ex)).Detail.ProblemType;
					return true;//Positive Match
				}
			} catch { }

			#endregion CWSDeserializationFault

			//As one last check look at the GeneralFaults
			if (GeneralFaults(_ex, out _strErrorID, out _strErrorMessage))
				return true;//Positive Match

			_strErrorMessage = "An unhandled fault was thrown \r\nMessage : " + _ex.Message;
			return false;//In this case unable to match fault so return false. 
		}

		//public bool handleTMSFault(Exception _ex, out string _strErrorID, out string _strErrorMessage) {//Note : that the boolean indicates if the fault was handled by this class
		//    _strErrorID = "";
		//    _strErrorMessage = "";

		//    #region AuthenticationFault
		//    try {
		//        if (((FaultException<TaxiPassCommon.Banking.com.ipcommerce.DataServices.AuthenticationFault>)(_ex)).Detail != null) {
		//            _strErrorID = ((FaultException<TaxiPassCommon.Banking.com.ipcommerce.DataServices.AuthenticationFault>)(_ex)).Detail.ErrorID.ToString();
		//            _strErrorMessage = ((FaultException<TaxiPassCommon.Banking.com.ipcommerce.DataServices.AuthenticationFault>)(_ex)).Detail.ProblemType;
		//            return true;//Positive Match
		//        }
		//    } catch { }
		//    #endregion AuthenticationFault

		//    #region TMSOperationNotSupportedFault
		//    try {
		//        if (((FaultException<TaxiPassCommon.Banking.com.ipcommerce.DataServices.TMSOperationNotSupportedFault>)(_ex)).Detail != null) {
		//            _strErrorID = ((FaultException<TaxiPassCommon.Banking.com.ipcommerce.DataServices.TMSOperationNotSupportedFault>)(_ex)).Detail.ErrorID.ToString();
		//            _strErrorMessage = ((FaultException<TaxiPassCommon.Banking.com.ipcommerce.DataServices.TMSOperationNotSupportedFault>)(_ex)).Detail.ProblemType;
		//            return true;//Positive Match
		//        }
		//    } catch { }
		//    #endregion TMSOperationNotSupportedFault

		//    #region TMSTransactionFailedFault
		//    try {
		//        if (((FaultException<TaxiPassCommon.Banking.com.ipcommerce.DataServices.TMSTransactionFailedFault>)(_ex)).Detail != null) {
		//            _strErrorID = ((FaultException<TaxiPassCommon.Banking.com.ipcommerce.DataServices.TMSTransactionFailedFault>)(_ex)).Detail.ErrorID.ToString();
		//            _strErrorMessage = ((FaultException<TaxiPassCommon.Banking.com.ipcommerce.DataServices.TMSTransactionFailedFault>)(_ex)).Detail.ProblemType;
		//            return true;//Positive Match
		//        }
		//    } catch { }
		//    #endregion TMSTransactionFailedFault

		//    #region TMSUnknownServiceKeyFault
		//    try {
		//        if (((FaultException<TaxiPassCommon.Banking.com.ipcommerce.DataServices.TMSUnknownServiceKeyFault>)(_ex)).Detail != null) {
		//            _strErrorID = ((FaultException<TaxiPassCommon.Banking.com.ipcommerce.DataServices.TMSUnknownServiceKeyFault>)(_ex)).Detail.ErrorID.ToString();
		//            _strErrorMessage = ((FaultException<TaxiPassCommon.Banking.com.ipcommerce.DataServices.TMSUnknownServiceKeyFault>)(_ex)).Detail.ProblemType;
		//            return true;//Positive Match
		//        }
		//    } catch { }
		//    #endregion TMSUnknownServiceKeyFault

		//    #region ExpiredTokenFault
		//    try {
		//        if (((FaultException<TaxiPassCommon.Banking.com.ipcommerce.DataServices.ExpiredTokenFault>)(_ex)).Detail != null) {
		//            _strErrorID = ((FaultException<TaxiPassCommon.Banking.com.ipcommerce.DataServices.ExpiredTokenFault>)(_ex)).Detail.ErrorID.ToString();
		//            _strErrorMessage = ((FaultException<TaxiPassCommon.Banking.com.ipcommerce.DataServices.ExpiredTokenFault>)(_ex)).Detail.ProblemType;
		//            return true;//Positive Match
		//        }
		//    } catch { }
		//    #endregion ExpiredTokenFault

		//    #region InvalidTokenFault
		//    try {
		//        if (((FaultException<TaxiPassCommon.Banking.com.ipcommerce.DataServices.InvalidTokenFault>)(_ex)).Detail != null) {
		//            _strErrorID = ((FaultException<TaxiPassCommon.Banking.com.ipcommerce.DataServices.InvalidTokenFault>)(_ex)).Detail.ErrorID.ToString();
		//            _strErrorMessage = ((FaultException<TaxiPassCommon.Banking.com.ipcommerce.DataServices.InvalidTokenFault>)(_ex)).Detail.ProblemType;
		//            return true;//Positive Match
		//        }
		//    } catch { }
		//    #endregion InvalidTokenFault

		//    #region TMSUnavailableFault
		//    try {
		//        if (((FaultException<TaxiPassCommon.Banking.com.ipcommerce.DataServices.TMSUnavailableFault>)(_ex)).Detail != null) {
		//            _strErrorID = ((FaultException<TaxiPassCommon.Banking.com.ipcommerce.DataServices.TMSUnavailableFault>)(_ex)).Detail.ErrorID.ToString();
		//            _strErrorMessage = ((FaultException<TaxiPassCommon.Banking.com.ipcommerce.DataServices.TMSUnavailableFault>)(_ex)).Detail.ProblemType;
		//            return true;//Positive Match
		//        }
		//    } catch { }
		//    #endregion TMSUnavailableFault


		//    //As one last check look at the GeneralFaults
		//    if (GeneralFaults(_ex, out _strErrorID, out _strErrorMessage))
		//        return true;//Positive Match

		//    _strErrorMessage = "An unhandled fault was thrown \r\nMessage : " + _ex.Message;
		//    return false;//In this case unable to match fault so return false. 
		//}

		public bool GeneralFaults(Exception _ex, out string _strErrorID, out string _strErrorMessage) {//Note : that the boolean indicates if the fault was handled by this class

			_strErrorID = "";
			_strErrorMessage = "";

			try {
				if (((FaultException<TaxiPassCommon.Banking.com.ipcommerce.Txn.CWSFault>)(_ex)).Detail != null) {
					_strErrorID = ((FaultException<TaxiPassCommon.Banking.com.ipcommerce.Txn.CWSFault>)(_ex)).Detail.ErrorID.ToString();
					_strErrorMessage = ((FaultException<TaxiPassCommon.Banking.com.ipcommerce.Txn.CWSFault>)(_ex)).Detail.ProblemType;
					return true;//Positive Match
				}
			} catch { }

			//No General faults found
			return false;
		}
	}

}
