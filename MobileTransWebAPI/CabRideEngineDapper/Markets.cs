﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

using Dapper;
using TaxiPassCommon;

using CabRideEngineDapper.Utils;
using System.Data;

namespace CabRideEngineDapper {
	public partial class Markets {

		public static Markets GetMarket(long pMarketID) {
			using (var conn = SqlHelper.OpenSqlConnection()) {
				return GetMarket(conn, pMarketID);
			}

		}

		public static Markets GetMarket(SqlConnection pConn, long pMarketID) {
			return pConn.Get<Markets>(pMarketID);
		}

		// ******************************************************************************
		// Get a single Market
		// ******************************************************************************
		public static Markets GetMarket(SqlConnection pConn, string pMarket) {
			List<Markets> oMarkets = GetData(pConn, pMarket, "");
			return oMarkets.DefaultIfEmpty(Markets.GetNullEntity()).FirstOrDefault();
		}

		// ******************************************************************************
		// Get all Markets in a Region
		// ******************************************************************************
		public static List<Markets> GetMarketByRegion(SqlConnection pConn, long pRegionID) {
			return GetData(pConn, "", pRegionID.ToString());
			
		}

		// ******************************************************************************
		// Call the proc
		// ******************************************************************************
		private static List<Markets> GetData(SqlConnection pConn, string pMarket, string pRegionID) {
			string sql = "usp_MarketsGet";
			DynamicParameters parms = new DynamicParameters();
			if (!pMarket.IsNullOrEmpty()) {
				parms.Add("Market", pMarket);
			}
			if (!pRegionID.IsNullOrEmpty()) {
				parms.Add("RegionID", pRegionID);
			}

			return pConn.Query<Markets>(sql, parms, commandType: CommandType.StoredProcedure).ToList();
		}

	}
}
