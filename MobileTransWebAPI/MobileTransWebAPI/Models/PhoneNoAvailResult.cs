﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileTransWebAPI.Models {
	public class PhoneNoAvailResult {
		public string ErrorMsg { get; set; }

		public bool IsAvailable { get; set; }
		public bool PhoneNoUpdated { get; set; }
		public long DriverID { get; set; }
	}
}