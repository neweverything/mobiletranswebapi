﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Dapper;
using System.Data;

namespace CabRideEngineDapper {
	public class usp_TokenCreate {
		public string Token { get; set; }

		public static usp_TokenCreate ExecuteProc(SqlConnection pConn
											   , string pTableName
											   , long pID
											   , string pModifiedBy
											   , DateTime pModifiedDate
											   , bool pGloballyUnique
											   , int pTokenLength) {

			DynamicParameters param = new DynamicParameters();
			param.Add("TableName", pTableName);
			param.Add("ID", pID);
			param.Add("ModifiedBy", pModifiedBy);
			param.Add("ModifiedDate", pModifiedDate);
			param.Add("GloballyUnique", pGloballyUnique);
			param.Add("TokenLength", pTokenLength);
			return pConn.Query<usp_TokenCreate>("usp_TokenCreate", param, commandType: CommandType.StoredProcedure).FirstOrDefault();
		}
	}
}
