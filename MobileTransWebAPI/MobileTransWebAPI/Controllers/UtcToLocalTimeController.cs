﻿using CabRideEngineDapper;
using System;
using System.Web.Http;

namespace MobileTransWebAPI.Controllers {

	public class UtcToLocalTimeController : ApiController {

		public DateTime Get(long pAffiliateId, DateTime pUtcTime) {
			Affiliate aff = Affiliate.GetAffiliate(pAffiliateId);

			return aff.UtcToLocalTimeActual(pUtcTime);
		}

	}
}
