﻿using System;
using System.Data;

using System.Collections.Generic;
using System.Linq;

using IdeaBlade.Persistence;
using IdeaBlade.Persistence.Rdb;

using TaxiPassCommon;
using TaxiPassCommon.Banking;


namespace CabRideEngine {
	/// <summary>
	/// The developer class representing the DriverPaymentLossGain business object class.
	/// </summary>
	/// <remarks>
	/// <para>Place all application-specific business logic in this class.</para>
	/// <para>
	/// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
	/// and will not overwrite changes.
	/// </para>
	/// <para>
	/// This class inherits from the generated class, its companion in the pair of classes 
	/// defining a business object. Properties in the generated class may be overridden in 
	/// order to change the behavior or add business logic.
	/// </para>
	/// <para>
	/// This class is the final class in the inheritance chain for this business object. 
	/// It should not be inherited.
	/// </para>
	/// </remarks>
	[Serializable]
	public sealed partial class DriverPaymentLossGain : DriverPaymentLossGainDataRow {

		#region Constructors -- DO NOT MODIFY

		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new';
		// it is also required if the class is visible from a web service.
		// DO NOT REMOVE **
		private DriverPaymentLossGain() : this(null) { }

		/// <summary>
		/// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
		/// </summary>
		/// <param name="pRowBuilder"></param>
		public DriverPaymentLossGain(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static DriverPaymentLossGain Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      DriverPaymentLossGain aDriverPaymentLossGain = pManager.CreateEntity<DriverPaymentLossGain>();
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aDriverPaymentLossGain, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aDriverPaymentLossGain.AddToManager();
		//      return aDriverPaymentLossGain;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		// Add additional logic to your business object here...
		#region Standard Entity logic to create/audit/validate the entity
		public static DriverPaymentLossGain Create(DriverPayments pDriverPayment) {
			DriverPaymentLossGain rec = null;
			try {
				rec = pDriverPayment.PersistenceManager.CreateEntity<DriverPaymentLossGain>();

				// Using the IdeaBlade Id Generation technique
				rec.DriverPaymentID = pDriverPayment.DriverPaymentID;
				rec.AddToManager();

			} catch (Exception ex) {
				throw ex;
			}
			return rec;
		}

		protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
			if (IsDeserializing) {
				return;
			}
			base.OnColumnChanging(pArgs);
			DoAudit(pArgs);
		}

		private void DoAudit(DataColumnChangeEventArgs pArgs) {
			if (!this.PersistenceManager.IsClient) {
				return;
			}
			if (this.RowState == DataRowState.Added) {
				return;
			}
			if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
				AuditRecord = DriverPaymentLossGainAudit.Create(this);
			}
		}
		#endregion

		public static EntityList<DriverPaymentLossGain> GetTransactionsToProcess(PersistenceManager pPM) {
			RdbQuery qry = new RdbQuery(typeof(DriverPaymentLossGain), DriverPaymentLossGain.ProcessLossGainEntityColumn, EntityQueryOp.EQ, true);
			qry.AddSpan(EntityRelations.DriverPayments_DriverPaymentLossGain);
			return pPM.GetEntities<DriverPaymentLossGain>(qry);
		}


		public DriverPaymentResults FixCharge() {

			PersistenceManager oPM = GlobalSettings.PM;
			if (oPM == null) {
				oPM = this.PersistenceManager;
			}
			DriverPaymentResults results = oPM.GetNullEntity<DriverPaymentResults>();

			string cardNo = this.DriverPayments.DecryptCardNumber();

			//Is it a TaxiPass Card
			TaxiPassCards tpCard = TaxiPassCards.GetCardNo(oPM, cardNo);
			if (!tpCard.IsNullEntity) {
				results.Result = "-1";
				results.Message = "Can not adjust TaxiPass Card";
				return results;
			}

			DriverPaymentResults oPayResults = null;

			//Is it a promo payment card
			EntityList<DriverPromotions> promoList = DriverPromotions.GetPromotionsByCardNo(this.PersistenceManager, cardNo);
			if (promoList.Count > 0) {
				results.Result = "-1";
				results.Message = "Can not adjust a promo transaction";
				return results;
			}

			if (RefundTrans()) {
				this.DriverPayments.BypassFraudCheck = true;
				this.DriverPayments.Fare = this.LossGainAmount;
				oPayResults = this.DriverPayments.ChargeCard(true);
			}

			this.DriverPayments.Save();

			return oPayResults;
		}

		private bool RefundTrans() {
			bool refunded = false;

			DriverPayments oPayment = this.DriverPayments;

			if (oPayment.CreditCardProcessor.IsNullOrEmpty() || oPayment.CreditCardProcessor.ToEnum<TaxiPassCommon.Banking.CardProcessors>() == TaxiPassCommon.Banking.CardProcessors.USAePay) {
				VerisignAccounts oAcct = oPayment.Affiliate.MyVerisignAccount;
				TaxiPassCommon.Banking.USAePay oPay = new TaxiPassCommon.Banking.USAePay(Utils.CreditCardUtils.CreateUSAePayCredentials(oAcct));

				string refNo = "";
				decimal refAmount = 0;
				if(!oPayment.DriverPaymentChargeResults.IsNullEntity) {
					refNo = oPayment.DriverPaymentChargeResults.Reference;
					refAmount = oPayment.DriverPaymentChargeResults.Amount;
				} else {
					// will we ever do multiple passes on a transaction, ie we charge $75, fix it to $15 then fix to $25
					// this should work as we pick up the last sale or delayed trans that we processed
					List<DriverPaymentResults> resultList = (from p in oPayment.DriverPaymentResultses
															 where p.Result == "0" && p.Amount == oPayment.TotalCharge &&
															   (p.TransType == VerisignTrxTypes.Sale || p.TransType == VerisignTrxTypes.DelayedCapture)
															 orderby p.TransDate descending
															 select p).ToList();

					if (resultList.Count > 0) {
						refNo = resultList[0].Reference;
						refAmount = resultList[0].Amount;
					}
				}

				if (!refNo.IsNullOrEmpty()) {
					string driverID = oPayment.AffiliateDriverID.ToString();
					if (driverID.IsNullOrEmpty()) {
						driverID = oPayment.DriverID.ToString();
					}
					CCRequestRefNo req = new CCRequestRefNo(refNo, refAmount, CardUtils.IsTestCard(oPayment.CardNumber), driverID, oPayment.TransNo, oPayment.IsGetRideTrans, oPayment.CardNumberDisplay);
					ProcessorResponse resp = oPay.Refund(req);
					
					//store refund in DB
					DriverPaymentResults result = this.DriverPayments.ProcessResponse(resp, VerisignTrxTypes.Credit, refAmount, null);
					if (result.Result == "0") {
						refunded = true;
					}
				}
			} else {
				//Process Verisign
			}
			return refunded;
		}
	}

	#region EntityPropertyDescriptors.DriverPaymentLossGainPropertyDescriptor
	////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
	////    For example: 
	////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
	//public static partial class EntityPropertyDescriptors {
	//	public partial class DriverPaymentLossGainPropertyDescriptor : BaseEntityPropertyDescriptor {
	//    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
	//    public AdaptedPropertyDescriptor Age {
	//      get { return this.Get("Age"); }
	//    }
	//  }
	//} 
	#endregion

}