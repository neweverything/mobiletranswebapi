﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CabRideEngine.VTS {

	public class Tran {
		public string RowID;
		public string IPNumber;
		public DateTime DateTime;
		public string CardNumber;
		public decimal Amount;
		public string CabNumber;
		public string Note;
		public DateTime TripDate;
		public DateTime TripTimeStart;
		public DateTime TripTimeEnd;
		public decimal TripFare;
		public decimal TripTrips;
		public decimal TripExtras;
		public decimal TripTips;
		public decimal TripDist;
		public decimal TripToll;
		public string TripDriverID;
		public string StoreForward;
		public string NumService;
		public string CardType;
		public string BankApproval;
		public string BankActionCode;
		public decimal Tax;
		public string System;
	}
}
