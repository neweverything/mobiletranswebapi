﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

using Dapper;
using TaxiPassCommon;

using CabRideEngineDapper.Utils;
using TaxiPassCommon.Banking;

namespace CabRideEngineDapper {

	public partial class DriverBankInfo {


		private Drivers mDriver;
		public Drivers Drivers {
			get {
				if (mDriver == null) {
					mDriver = Drivers.GetDriver(this.DriverID);
				}
				return mDriver;
			}
		}

		private MerchantInfo mMerchantInfo;
		public MerchantInfo MerchantInfo {
			get {
				if (!this.MerchantID.HasValue) {
					return null;
				}
				if (mMerchantInfo == null) {
					mMerchantInfo = MerchantInfo.GetMerchant(this.MerchantID.Value);
				}
				return mMerchantInfo;
			}
		}

		public static List<DriverBankInfo> GetBankInfo(long pDriverID) {
			
			using (var conn = SqlHelper.OpenSqlConnection()) {
				return conn.GetList<DriverBankInfo>(new { DriverID = pDriverID }).ToList();
			}
		}


		public PayInfo GetPayInfo() {
			PayInfo oPay = new PayInfo();
			oPay.NachaFile = this.Drivers.Affiliate.SendNacha;
			oPay.BankAccountHolder = this.BankAccountHolder;
			oPay.BankAccountNumber = this.BankAccountNumber;
			oPay.BankAccountPersonal = this.BankAccountPersonal;
			oPay.BankAddress = this.BankAddress;
			oPay.BankCity = this.BankCity;
			oPay.BankCode = this.BankCode;
			oPay.BankState = this.BankState;
			oPay.BankSuite = this.BankSuite;
			oPay.BankZIPCode = this.BankZIPCode;
			if (oPay.MyMerchant == null) {
				oPay.MyMerchant = new Merchant();
			}
			oPay.MyMerchant.MerchantNo = this.MerchantInfo.MerchantNo;
			oPay.PaidTo = "Cell: " + this.Drivers.Name;
			return oPay;
		}
	}
}
