﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using IdeaBlade.Util;

namespace CabRideEngine {

	public enum ACHPaymentScheduleEnum {
		NoSchedule,
		MonWed,
		WorkWeek,
		Affiliate,
		WorkWeekMinus1,
		EveryOtherDay,
		Custom,
		PayoutMatrix
	}

	public class ACHPaymentSchedule {
		private int? mCode;
		private string mDescription;

		private bool mIsNullEntity = false;

		private static BindableList<ACHPaymentSchedule> msEntities;
		private static ACHPaymentSchedule msNullEntity;

		

		static ACHPaymentSchedule() {

			// Create the null entity
			msNullEntity = new ACHPaymentSchedule(null, "");
			msNullEntity.mIsNullEntity = true;

			// Populate list.
			msEntities = new BindableList<ACHPaymentSchedule>();
			msEntities.Add(new ACHPaymentSchedule(Convert.ToInt32(ACHPaymentScheduleEnum.NoSchedule), ""));
			msEntities.Add(new ACHPaymentSchedule(Convert.ToInt32(ACHPaymentScheduleEnum.MonWed), "ACH Monday and Wednesday"));
			msEntities.Add(new ACHPaymentSchedule(Convert.ToInt32(ACHPaymentScheduleEnum.WorkWeek), "ACH Every Day"));
			msEntities.Add(new ACHPaymentSchedule(Convert.ToInt32(ACHPaymentScheduleEnum.WorkWeekMinus1), "ACH Every Day - 1"));
			msEntities.Add(new ACHPaymentSchedule(Convert.ToInt32(ACHPaymentScheduleEnum.Affiliate), "Affiliate Notification"));
			msEntities.Add(new ACHPaymentSchedule(Convert.ToInt32(ACHPaymentScheduleEnum.EveryOtherDay), "Every Other Day"));
			msEntities.Add(new ACHPaymentSchedule(Convert.ToInt32(ACHPaymentScheduleEnum.Custom), "Custom"));
			msEntities.Add(new ACHPaymentSchedule(Convert.ToInt32(ACHPaymentScheduleEnum.PayoutMatrix), "Payout Matrix"));

		}

		private ACHPaymentSchedule(int? pCode, string pDescription) {
			mCode = pCode;
			mDescription = pDescription;
		}

		/// <summary>Get the ACHPaymentSchedule null entity.</summary>
		public static ACHPaymentSchedule NullEntity {
			get {
				return msNullEntity;
			}
		}

		/// <summary>Get the list of all ACHPaymentSchedule.</summary>
		public static BindableList<ACHPaymentSchedule> GetAll() {
			return msEntities;
		}

		/// <summary>Get the ACHPaymentSchedule by Code.</summary>
		public static ACHPaymentSchedule GetById(int? pCode) {
			foreach (ACHPaymentSchedule oRec in msEntities) {
				if (oRec.Code == pCode) {
					return oRec;
				}
			}
			return NullEntity;
		}

		/// <summary>Return true if this is the null entity</summary>
		[BindingBrowsable(false)]
		public bool IsNullEntity {
			get {
				return mIsNullEntity;
			}
		}

		/// <summary>Get the Code</summary>
		public int? Code {
			get {
				return mCode;
			}
		}

		public string Description {
			get {
				return mDescription;
			}
		}



	}
}
