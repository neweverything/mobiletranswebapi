﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;


namespace Test {
	class Program {

		public static string Url = "http://cabpay.com/mobiletranswebapi/api/";

		static void Main(string[] args) {

			//GetServerTime();

			GetStates("USA");
		}

		private static double GetServerTime() {
			bool bOk = false;
			HttpWebRequest request = WebRequest.Create(Url + "ServerTime") as HttpWebRequest;
			request.Timeout = 60000;

			using (HttpWebResponse response = (HttpWebResponse)request.GetResponse()) {
				bOk = response.StatusCode == HttpStatusCode.OK;

				if (bOk) {
					System.IO.StreamReader strm = new System.IO.StreamReader(response.GetResponseStream());
					string x = strm.ReadToEnd();
					response.Close();

					return Convert.ToDouble(x);
				}
			}

			return 0;
		}

		private static void GetStates(string p) {
			string Body = "pCountryCode=USA"; // "{\"ld\":{\"UserName\":\"blah\",\"PassWord\":\"blah\",\"AppKey\":\"blah\"}}";
			byte[] byteData = UTF8Encoding.UTF8.GetBytes(Body);
			try {

				

				// Create the web request
				HttpWebRequest request = WebRequest.Create(Url + "states") as HttpWebRequest;
				//HttpWebRequest request = WebRequest.Create(Url + "states/?pCountryCode=USA") as HttpWebRequest;
				//request.ContentLength = Body.Length;

				// Set type to POST
				request.Method = "GET";
				request.ContentType = "text/json";

				// Write the parameters
				//StreamWriter stOut = new StreamWriter(request.GetRequestStream(), System.Text.Encoding.ASCII);
				//stOut.Write(Body);
				//stOut.Close();

				//WebResponse response = request.GetResponse();

				//System.IO.StreamReader strm = new System.IO.StreamReader(response.GetResponseStream());

				//string responseString = strm.ReadToEnd();
				//response.Close();

				request.BeginGetResponse(new AsyncCallback(ProcessHttpResponse), request);
			} catch (WebException we) {
				Console.Error.WriteLine("Exception: " + we.Message);
			} catch (System.Exception sysExc) {
				Console.Error.WriteLine("Exception: " + sysExc.Message);
			}
		}

		private static void ProcessHttpResponse(IAsyncResult iar) {
			HttpWebRequest request = (HttpWebRequest)iar.AsyncState;
			HttpWebResponse response;

			response = (HttpWebResponse)request.EndGetResponse(iar);
			Console.Error.WriteLine("get response.");
			System.IO.StreamReader strm = new System.IO.StreamReader(response.GetResponseStream());

			string responseString = strm.ReadToEnd();
			response.Close();

			Console.Error.WriteLine("response: " + responseString);
		}

	}
}
