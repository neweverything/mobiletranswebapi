﻿using CabRideEngineDapper;
using CabRideEngineDapper.Utils;
using Dapper;
using MobileTransWebAPI.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Http;
using TaxiPassCommon;
using TaxiPassCommon.Banking;

namespace MobileTransWebAPI.Controllers {
    public class FundPayCardSelectedVouchersController : ApiController {

        public FundsTransferResponse Get() {
            string request = "5184B6F882F74AB06884F30CD12DE40E60B9FCC130421FD2770279EC4906E7899124B275447CAC0DC13D78064158032F66D8F5A8E17AA21EDB2620D41AE7A4532C26E363D998FC3527E6F214A4E483037C1E53B8C7798C27C4BE5E7ABB0609B39BEE2215945DAC048B0F53A5CF96DACE1F7B195301380151FA5D766E6445487D8A3AA1DE4A88CFA3E25B8FC3D7C250ACF9CFAD44F50EF431A1141CE1";
            return FundPayCard(request);
        }

        [HttpGet]
        public FundsTransferResponse FundPayCard(string pRequest) {
            FundsTransferResponse fundResults = new FundsTransferResponse();

            WebApiApplication.LogIt("FundPayCardSelectedVouchers:", pRequest, "FundPayCard_{0}");

            try {
                using (var conn = SqlHelper.OpenSqlConnection()) {

                    FundPayCardSelected info = new FundPayCardSelected();
                    string request = pRequest.DecryptIceKey();
                    info = JsonConvert.DeserializeObject<FundPayCardSelected>(request);

                    DateTime requestDate = Convert.ToDateTime(info.RequestTime);

                    if (WebApiApplication.GetUTCServerTime().Subtract(requestDate).TotalMinutes > 1) {
                        fundResults.ReturnMessage = "Invalid Access";
                        return fundResults;
                    }

                    if (!info.Phone.IsNullOrEmpty()) {

                        AffiliateDrivers pPayeeDriver = AffiliateDrivers.GetDriverByCallInCell(conn, info.Phone);

                        if (pPayeeDriver.IsNullEntity || pPayeeDriver.RedeemerStopPayment || pPayeeDriver.TransCardNumber.IsNullOrEmpty()) {
                            fundResults.ReturnCode = "-1";
                            fundResults.ReturnMessage = "You cannot get paid at this time, please contact TaxiPass to resolve";
                            return fundResults;
                        }

                        List<DriverPayments> payList = GetPayList(conn, pPayeeDriver.AffiliateDriverID, info.TransList);
                        if (payList.Count == 0) {
                            fundResults.ReturnCode = "-1";
                            fundResults.ReturnMessage = "No vouchers available for funding";
                            return fundResults;
                        }

                        bool dejavooStopPayment = payList.Where(p => p.CreditCardProcessor.Equals(Platforms.Dejavoo.ToString()) && p.DriverPaymentChargeResults.ProcessorBatchNo.IsNullOrEmpty()).ToList().Count > 0;
                        if (dejavooStopPayment) {
                            fundResults.ReturnCode = "-1";
                            fundResults.ReturnMessage = "Please settle batch or bring credit card machine to trailer for payment";
                            return fundResults;
                        }

                        decimal decAmount = 0;

                        FSV.Credentials cred = new FSV.Credentials();
                        AffiliateDriverRef rec;
                        cred = conn.Query<FSV.Credentials>("usp_SystemDefaultsDict_Flattened", new { ReferenceGroup = "FSV" }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                        rec = AffiliateDriverRef.GetOrCreate(conn, pPayeeDriver.AffiliateDriverID, "FSV", "Registration");

                        FSV fsv = new FSV(cred, cred.UseProductionAsBoolen);
                        FSV.VerifyConnectionResponse verifyResponse = new FSV.VerifyConnectionResponse();
                        verifyResponse = fsv.VerifyConnection();
                        if (verifyResponse.ReturnCode != "1") {
                            fundResults.ReturnCode = "-1";
                            fundResults.ReturnMessage = "Could not verify connection to USBank: " + verifyResponse.ReturnMessage;
                            return fundResults;
                        }

                        FSV.CardRegistrationResponse resp = JsonConvert.DeserializeObject<FSV.CardRegistrationResponse>(rec.ValueString);
                        FSV.FundsTransfer transfer = new FSV.FundsTransfer();
                        transfer.FromCardID = cred.AdjustmentCard.DecryptIceKey();
                        transfer.FromCardPassCode = cred.AdjustmentCode.DecryptIceKey();
                        transfer.ToCardID = resp.CardID;

                        bool splitPayments = false;
                        if (cred.NewTekStartDate.Year > 2010) {
                            splitPayments = true;
                        }

                        List<string> forceNewCellNumberList = new List<string>();
                        if (!cred.NewTekForceByDriverCellNo.IsNullOrEmpty()) {
                            forceNewCellNumberList = cred.NewTekForceByDriverCellNo.Split('|').ToList();
                            for (int i = 0; i < forceNewCellNumberList.Count; i++) {
                                forceNewCellNumberList[i] = forceNewCellNumberList[i].Trim();
                            }
                            splitPayments = true;
                        }

                        List<DriverPayments> newPayList = new List<DriverPayments>();
                        if (splitPayments) {
                            newPayList.AddRange(payList.Where(p => p.ChargeDate < cred.NewTekStartDate && !forceNewCellNumberList.Contains(p.AffiliateDrivers.Cell)));
                        } else {
                            newPayList.AddRange(payList);
                        }

                        if (newPayList.Count > 0) {
                            ProcessPayments(fundResults, conn, info, pPayeeDriver, payList, ref decAmount, fsv, transfer, false);
                        }

                        transfer = new FSV.FundsTransfer();
                        transfer.FromCardID = cred.NewTekAdjustmentCard.DecryptIceKey();
                        transfer.FromCardPassCode = cred.NewTekAdjustmentCode.DecryptIceKey();
                        transfer.ToCardID = resp.CardID;

                        newPayList = new List<DriverPayments>();
                        if (splitPayments) {
                            newPayList.AddRange(payList.Where(p => p.ChargeDate >= cred.NewTekStartDate || forceNewCellNumberList.Contains(p.AffiliateDrivers.Cell)));
                        }
                        if (newPayList.Count > 0) {
                            ProcessPayments(fundResults, conn, info, pPayeeDriver, payList, ref decAmount, fsv, transfer, true);
                        }
                    }

                }
            } catch (Exception ex) {
                fundResults.ReturnMessage = ex.Message;
            }

            return fundResults;
        }

        public static void ProcessPayments(FundsTransferResponse pFundResults, SqlConnection pConn, FundPayCardSelected pInfo, AffiliateDrivers pPayeeDriver, List<DriverPayments> pPayList, ref decimal pDecAmount, FSV pFSV, FSV.FundsTransfer pTransfer, bool pNewTekFunding) {
            List<ACHDetail> achRecList = new List<ACHDetail>();
            DateTime achPaidDate = DateTime.Now;
            string transNo = "";

            foreach (DriverPayments pay in pPayList) {
                if (pay.Test || pay.DoNotPay.GetValueOrDefault(false) || pay.Failed) {
                    continue;
                }
                //ACHDetail achRec = ACHDetail.GetTransactionByDriverPaymentID(mPM, pay.DriverPaymentID, QueryStrategy.DataSourceThenCache);
                ACHDetail achRec = ACHDetail.GetByDriverPaymentID(pay.DriverPaymentID);
                if (!achRec.ACHPaidDate.HasValue) {
                    if (achRec.IsNullEntity) {
                        achRec = ACHDetail.Create(pay.DriverPaymentID);
                    }


                    if (pInfo.TaxiPassRedeemerID < 1 && pay.RedemptionFee == 0) {
                        pay.RedemptionFee = pay.Affiliate.AffiliateDriverDefaults.DriverInitiatedPayCardRedemptionFee;
                    }
                    pDecAmount = pay.DriverTotalPaid; // - pay.Affiliate.AffiliateDriverDefaults.DriverInitiatedPayCardRedemptionFee.GetValueOrDefault(0);

                    if (pDecAmount == 0) {
                        pDecAmount = pay.DriverPaymentKioskAmount.Amount;
                    }

                    if (pDecAmount == 0) {
                        achRec.ACHErrorReason = "No Fare amount found";
                    } else {
                        achRec.AmountPaid = pDecAmount;
                        pTransfer.TransferAmount += pDecAmount;
                    }

                    achRec.ACHErrorReason = "Paid: " + pay.DriverTotalPaid.ToString("C");
                    //}
                    achRec.ACHPaidDate = achPaidDate;
                    achRec.ACHPaidTo = pPayeeDriver.Name;
                    achRec.ACHBankAccountNumber = "PayCard: " + pPayeeDriver.TransCardNumber.DecryptIceKey().Right(5);

                    achRec.TransCardAdminNo = pPayeeDriver.TransCardAdminNo;
                    //achRec.Save(SessionHelper.EMail(Session));
                    if (transNo.IsNullOrEmpty()) {
                        transNo = pay.TransNo;
                    } else {
                        transNo = string.Format("{0}-{1}", transNo, pay.TransNo);
                    }
                    achRecList.Add(achRec);

                    if (!pay.DriverPaid && achRec.AmountPaid.GetValueOrDefault(0) != 0) {
                        pay.DriverPaid = true;
                        pay.DriverPaidDate = achRec.ACHPaidDate;
                        pay.TaxiPassRedeemerID = pInfo.TaxiPassRedeemerID;
                        if (pay.TaxiPassRedeemerID.GetValueOrDefault(0) == 0) {
                            pay.TaxiPassRedeemerID = SystemDefaultsDict.GetByKey(pConn, "SelfRedemption", "RedeemerID", false).ValueString.AsLong();
                        }
                        pay.Save(pInfo.UserID);

                        DriverPaymentRef dpRef = DriverPaymentRefTable.Create();
                        dpRef.DriverPaymentID = pay.DriverPaymentID;
                        dpRef.ReferenceGroup = "ACHToPayCard";
                        dpRef.ReferenceKey = "AdminNo";
                        dpRef.ValueString = pPayeeDriver.TransCardAdminNo;
                        dpRef.Save(pInfo.UserID);
                    }
                }
            }

            pTransfer.TransferAmount = achRecList.Distinct().Sum(p => p.AmountPaid.GetValueOrDefault(0));
            pTransfer.ReferenceFromCard = string.Format("ADriverID: {0}|{1}", pPayeeDriver.AffiliateDriverID, transNo);
            if (pTransfer.ReferenceFromCard.Length > 100) {
                long masterVoucherID = pPayList[0].DriverPaymentID;
                foreach (var achRec in achRecList) {
                    DriverPaymentAux aux = DriverPaymentAux.GetOrCreate(achRec.DriverPaymentID);
                    aux.RowState = DataRowState.Unchanged;
                    aux.MasterVoucherID = masterVoucherID;
                    aux.Save(pInfo.UserID);
                }
                pTransfer.ReferenceFromCard = string.Format("ADriverID: {0}|^{1}", pPayeeDriver.AffiliateDriverID, masterVoucherID);
            }
            pTransfer.ReferenceToCard = "Driver Payment";

            var funded = pFSV.TransferToPayCard(pTransfer);
            if (funded.ReturnCode == "1") {
                pFundResults.ReturnCode = funded.ReturnCode;
                pFundResults.ReturnMessage = funded.ReturnMessage;
                Misc.SendTransCardPaymentSMS(pPayList.ToList(), pPayeeDriver, "");
                //mPM.SaveChanges(achRecList);
                foreach (var achRec in achRecList) {
                    achRec.Save();
                }

                string fundingAccountBalance = "FundingAccountBalance";
                if (pNewTekFunding) {
                    fundingAccountBalance = "NewTekFundingAccountBalance";
                }

                SystemDefaultsDict fundBal = SystemDefaultsDict.GetOrCreate(pConn, "FSV", fundingAccountBalance);
                fundBal.ValueString = funded.FromCardBalance;
                fundBal.Save();

                USBankPayCardPaid usBank = USBankPayCardPaid.Create();
                usBank.AdminNo = pTransfer.ToCardID;
                usBank.AffiliateDriverID = pPayeeDriver.AffiliateDriverID;
                usBank.CardEnding = pPayeeDriver.TransCardCardNumber.Right(5);
                usBank.DebitTransaction = pTransfer.TransferAmount;
                usBank.FundingTransDate = DateTime.Now;
                usBank.LastName = pPayeeDriver.LastName.IsNullOrEmpty() ? pPayeeDriver.Name : pPayeeDriver.LastName;
                //usBank.TaxiPassRedeemerID = info.tax
                usBank.TransactionDetail = pTransfer.ReferenceFromCard;
                usBank.FundingAccountNo = pTransfer.FromCardID;
                usBank.Save();

            } else {
                pFundResults.ReturnCode = funded.ReturnCode;
                //fundResults.ReturnMessage = string.Format("Payment of {0} to be posted later to your card\n{1}", transfer.TransferAmount.ToString("C"), funded.ReturnMessage);
                pFundResults.ReturnMessage = string.Format("Payment of {0} to be posted later to your card\n", pTransfer.TransferAmount.ToString("C"));
                pPayeeDriver.SendDriverMsg(string.Format("Payment of {0} to be posted later to your card", pTransfer.TransferAmount.ToString("C")), false);
            }
        }

        public List<DriverPayments> GetPayList(SqlConnection pConn, long pAffiliateDriverID, List<string> pTransList) {
            string sql = @"DECLARE @LookBackDays INT = (SELECT CONVERT(INT, ValueString)
                                                         FROM   SystemDefaultsDict
                                                         WHERE  ReferenceGroup = 'PayCard'
                                                                AND ReferenceKey = 'PayDriverChargeDateMinusDays'
                                                        )

                            DECLARE @MaxAge INT = (SELECT CONVERT(INT, ValueString)
                                                         FROM   SystemDefaultsDict
                                                         WHERE  ReferenceGroup = 'PayCard'
                                                                AND ReferenceKey = 'MaxAge'
                                                        )


                            SELECT  DISTINCT
                                    DriverPayments.*
                            FROM    dbo.DriverPayments
                            LEFT JOIN ACHDetail ON ACHDetail.DriverPaymentID = DriverPayments.DriverPaymentID
                            LEFT JOIN DriverPaymentAux ON DriverPaymentAux.DriverPaymentID = DriverPayments.DriverPaymentID
                            WHERE   AffiliateDriverID = @AffiliateDriverID
                                    AND ACHPaidDate IS NULL
                                    AND Failed = 0
                                    AND Test = 0
                                    AND DATEADD(DAY, @LookBackDays, ChargeDate) < GETUTCDATE()
		                            AND ChargeDate >= '1/16/2016'
		                            AND ChargeDate > DATEADD(DAY, -@MaxAge, GETUTCDATE())
                                    AND ISNULL(DriverPayments.DoNotPay, 0) = 0
                                    AND ISNULL(DriverPaymentAux.DoNotPay, 0) = 0
                                    AND TaxiPassRedeemerID IS NULL
                            		AND TransNo IN @TransNo";

            return pConn.Query<DriverPayments>(sql, new { AffiliateDriverID = pAffiliateDriverID, TransNo = pTransList }).ToList();
        }

    }
}
