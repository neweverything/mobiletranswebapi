﻿using CabRideEngineDapper.Utils;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace CabRideEngineDapper {
	public partial class GateWayBatchInfo {

		public static GateWayBatchInfo GetBatchInfo(VerisignAccounts pAccount, string pBatchNum, string pSequence, DateTime pOpenedDate, DateTime pClosedDate) {
			string sql = @"SELECT *
                            FROM   GateWayBatchInfo
                            WHERE  BatchNumber = @BatchNum
                                   AND VerisignAccountID = @VerisignAccountID
                                   AND Sequence = @Sequence
                                   AND Opened = @OpenedDate
                                   AND Closed = @ClosedDate";

			using (var conn = SqlHelper.OpenSqlConnection()) {
				return conn.Get<GateWayBatchInfo>(sql, new { BatchNum = pBatchNum, VerisignAccountID = pAccount.VerisignAccountID, Sequence = pSequence, OpenedDate = pOpenedDate, ClosedDate = pClosedDate });
			}
		}

		public static GateWayBatchInfo GetBatchInfo(SqlConnection pConn, string pBatchNum, string pSequence, DateTime pOpenedDate, DateTime pClosedDate) {
			string sql = @"SELECT *
                            FROM   GateWayBatchInfo
                            WHERE  BatchNumber = @BatchNum
                                   AND Sequence = @Sequence
                                   AND Opened = @OpenedDate
                                   AND Closed = @ClosedDate";

			return pConn.Get<GateWayBatchInfo>(sql, new { BatchNum = pBatchNum, Sequence = pSequence, OpenedDate = pOpenedDate, ClosedDate = pClosedDate });
		}

		public static GateWayBatchInfo GetCardConnectBatchInfo(SqlConnection pConn, string pBatchNum, string pSequence, DateTime pClosedDate) {
			string sql = @"SELECT *
                            FROM   GateWayBatchInfo
                            WHERE  BatchNumber = @BatchNum
                                   AND Sequence = @Sequence
                                   AND Closed = @ClosedDate
                                   AND GateWay = 'CardConnect'";

			return pConn.Get<GateWayBatchInfo>(sql, new { BatchNum = pBatchNum, Sequence = pSequence, ClosedDate = pClosedDate });
		}

		public static GateWayBatchInfo GetNewtekBatchInfo(SqlConnection pConn, string pBatchNum, string pSequence, DateTime pOpenedDate, DateTime pClosedDate) {
			string sql = @"SELECT *
                            FROM   GateWayBatchInfo
                            WHERE  BatchNumber = @BatchNum
                                   AND Sequence = @Sequence
								   AND Opened = @OpenedDate
                                   AND Closed = @ClosedDate
                                   AND GateWay = 'Newtek'";

			return pConn.Get<GateWayBatchInfo>(sql, new { BatchNum = pBatchNum, Sequence = pSequence, OpenedDate = pOpenedDate, ClosedDate = pClosedDate });
		}


		public static List<GateWayBatchInfo> GetBatches(SqlConnection pConn, DateTime pStart, DateTime pEnd) {
			string sql = @"SELECT GateWayBatchInfo.*, GroupName FROM GateWayBatchInfo
                            LEFT JOIN CardGatewayGroups ON CardGatewayGroupsID = VerisignAccountID
                            WHERE Closed >= @StartDate AND Closed < @EndDate
                            ORDER BY Closed";

			return pConn.Query<GateWayBatchInfo>(sql, new { StartDate = pStart, EndDate = pEnd }).ToList();
		}

		[Editable(false)]
		public string GroupName { get; set; }

		public static GateWayBatchInfo GetDejavooBatchInfo(SqlConnection pConn, string pBatchId, string pBatchNumber) {
			string sql = $@"SELECT *
                            FROM   GateWayBatchInfo
                            WHERE  BatchNumber = @BatchNum
                                   AND Sequence = @Sequence
                                   AND GateWay = '{Platforms.Dejavoo.ToString()}'";

			return pConn.Get<GateWayBatchInfo>(sql, new { BatchNum = pBatchId, Sequence = pBatchNumber });
		}
	}
}
