﻿using System.Collections.Generic;

namespace MobileTransWebAPI.Models {

	public partial class FundingPayCardInfo {
		public int PayCardLoadsAvailable { get; set; }
		public int VouchersAvailableForPayment { get; set; }
		public decimal TotalVoucherPayout { get; set; }
		public decimal FundingAcctAmt { get; set; }
		public decimal DisplayFundingMsgAt { get; set; }

		public string ErrorMessage { get; set; }
		public bool DelayedFunding { get; set; }

		public List<Vouchers> VoucherList { get; set; }
		public string CardNumber { get; set; }

		public int MissingDejavooBatchCount { get; set; }
		public decimal MissingBatchDriverTotal { get; set; }

		public bool HasDriversLicenseImage { get; set; }
		public string VehicleNo { get; set; }
	}
}