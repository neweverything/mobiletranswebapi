using System;
using System.Collections.Generic;
using System.Collections.Specialized;

using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Net;
using System.Xml;

using System.Data;

using Sgml;

namespace CabRideEngine.Utils {
    public class WebCrawler {

        private StringCollection mLinks = new StringCollection();
        private string mHTMLSource;
        private string mServerURL;
        private string mPathURL;
        private string mScheme;

        public StringCollection LinksFound {
            get {
                return mLinks;
            }
        }

        public void Start(string pURL, bool bExcludeParentURL) {
            try {
                GetWellFormedHTML(pURL);
                GetHRefs(bExcludeParentURL);
            } catch (Exception ex) {
                throw ex;
            }
        }
        public string GetWellFormedHTML(string pURI) {
            StreamReader oReader = null;
            StringWriter oSW = null;
            SgmlReader sgmlReader = null;
            XmlTextWriter oWriter = null;

            try {
                if (pURI == String.Empty) {
                    return "";
                }
                HttpWebRequest oRequest = (HttpWebRequest)WebRequest.Create(pURI);
                HttpWebResponse oResponse = (HttpWebResponse)oRequest.GetResponse();

                mServerURL = oRequest.Address.Host;
                mScheme = oRequest.Address.Scheme + "://";
                mPathURL = pURI;

                oReader = new StreamReader(oResponse.GetResponseStream());
                sgmlReader = new SgmlReader();
                sgmlReader.DocType = "HTML";
                sgmlReader.InputStream = new StringReader(oReader.ReadToEnd());
                oSW = new StringWriter();
                oWriter = new XmlTextWriter(oSW);
                oWriter.Formatting = Formatting.Indented;
                //writer.WriteStartElement("Test");
                while (sgmlReader.Read()) {
                    if (sgmlReader.NodeType != XmlNodeType.Whitespace) {
                        oWriter.WriteNode(sgmlReader, true);
                    }
                }
                mHTMLSource = oSW.ToString();

                return oSW.ToString();
            } catch (Exception exp) {
                if (oWriter != null) {
                    oWriter.Close();
                }
                if (sgmlReader != null) {
                    sgmlReader.Close();
                }
                if (oSW != null) {
                    oSW.Close();
                }
                if (oReader != null) {
                    oReader.Close();
                }
                throw exp;
            }
        }

        private StringCollection GetHRefs(bool bExcludeParent) {
            if (mHTMLSource == null) {
                return null;
            }
            MatchCollection oMatch = new Regex("<a[^>]*href\\s*=\\s*\"?(?<HRef>[^\">\\s]*)\"?[^>]*>", RegexOptions.Singleline | RegexOptions.IgnoreCase).Matches(mHTMLSource);
            StringCollection UrlList = new StringCollection();
            foreach (Match match1 in oMatch) {
                string sURL = match1.Groups["HRef"].Value;
                sURL.Trim();
                if ((sURL != "") && (sURL.Substring(0, 1) != "#")) {
                    if (sURL.Substring(0, 1) == "/") {
                        sURL = mScheme + mServerURL + sURL;
                    } else if (string.Compare(sURL.Substring(0, 7), "http://", true) != 0) {
                        if (string.Compare(sURL.Substring(0, 3), "www", true) == 0) {
                            sURL = mScheme + sURL;
                        } else {
                            sURL = mPathURL + sURL;
                        }
                    }
                }
                if (bExcludeParent) {
                    if (sURL != (mScheme + mServerURL + "/")) {
                        UrlList.Add(sURL);
                    }
                } else {
                    UrlList.Add(sURL);
                }
            }
            mLinks = UrlList;

            return UrlList;
        }

        private string StripComments(string pSource) {
            Regex regex1 = new Regex(this.GetExpressionForTagContents("!"));
            return regex1.Replace(pSource, "");
        }

        private string GetExpressionForTagContents(string pTagName) {
            if (pTagName == "!") {
                return "<!.*?-->";
            }
            if (string.Compare(pTagName, "!doctype", true) == 0) {
                return "<!doctype.*?>";
            }
            if (string.Compare(pTagName, "br", true) == 0) {
                return @"<br\s*/?\s*>";
            }
            return ("<(" + pTagName + @")(>|\s+[^>]*>).*?</\1\s*>");
        }

        public string GetTextData(string pURI) {
            StreamReader oReader = null;
            try {
                if (pURI == String.Empty) {
                    return "";
                }
                HttpWebRequest oRequest = (HttpWebRequest)WebRequest.Create(pURI);
                HttpWebResponse oResponse = (HttpWebResponse)oRequest.GetResponse();

                if (oResponse.StatusCode != HttpStatusCode.OK) {
                    throw new Exception("Error getting file: " + pURI + " -> " + oResponse.StatusDescription);
                }
                oReader = new StreamReader(oResponse.GetResponseStream());
                string sData = oReader.ReadToEnd();

                return sData;
            } catch (Exception ex) {
                oReader.Close();
                throw ex;
            }
        }

        public  DataSet GetDataSet(string pURI) {
            
            try {
                if (pURI == String.Empty) {
                    return null;
                }
                HttpWebRequest oRequest = (HttpWebRequest)WebRequest.Create(pURI);
                HttpWebResponse oResponse = (HttpWebResponse)oRequest.GetResponse();

                if (oResponse.StatusCode != HttpStatusCode.OK) {
                    throw new Exception("Error getting file: " + pURI + " -> " + oResponse.StatusDescription);
                }
                DataSet oDS = new DataSet();
                oDS.ReadXml(new StreamReader(oResponse.GetResponseStream()));
                
                return oDS;
            } catch (Exception ex) {
                throw ex;
            }
        }

    }
}
