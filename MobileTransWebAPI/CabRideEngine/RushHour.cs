﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Util;

namespace CabRideEngine {
    [Serializable]
    public sealed class RushHour : RushHourDataRow {

        #region Constructors -- DO NOT MODIFY
        // Do not create constructors for this class
        // Developers cannot create instances of this class with the "new" keyword
        // You should write a static Create method instead; see the "Suggested Customization" region
        // Please review the documentation to learn about entity creation and inheritance

        // This private constructor prevents accidental instance creation via 'new'
        private RushHour() : this(null) { }

        // Typed DataSet constructor (needed by framework) - DO NOT REMOVE
        public RushHour(DataRowBuilder pRowBuilder)
            : base(pRowBuilder) {
        }

        #endregion

        #region Suggested Customizations

        //    // Use this factory method template to create new instances of this class
        //    public static RushHour Create(PersistenceManager pManager,
        //      ... your creation parameters here ...) { 
        //
        //      RushHour aRushHour = (RushHour) pManager.CreateEntity(typeof(RushHour));
        //
        //      // if this object type requires a unique id and you have implemented
        //      // the IIdGenerator interface implement the following line
        //      pManager.GenerateId(aRushHour, // add id column here //);
        //
        //      // Add custom code here
        //
        //      aRushHour.AddToManager();
        //      return aRushHour;
        //    }

        //    // Implement this method if you want your object to sort in a specific order
        //    public override int CompareTo(Object pObject) {
        //    }

        //    // Implement this method to customize the null object version of this class
        //    protected override void UpdateNullEntity() {
        //    }

        #endregion

        // Add additional logic to your business object here...

        protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
            if (IsDeserializing)
                return;
            base.OnColumnChanging(pArgs);
            //DoAudit(pArgs);
        }

        private void DoAudit(DataColumnChangeEventArgs pArgs) {
            if (!this.PersistenceManager.IsClient) {
                return;
            }
            if (this.RowState == DataRowState.Added) {
                return;
            }
            if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
                AuditRecord = RushHourAudit.Create(this.PersistenceManager, this);
            }

        }

        public override long RushHourID {
            get {
                if (base.RushHourID == 0) {
                    long lKey = SystemDefaults.GetNextKey(SystemDefaults.RushHourEntityColumn.ColumnName);
                    base.RushHourID = lKey;
                }
                return base.RushHourID;
            }
            set {
                base.RushHourID = value;
            }
        }
    }

}
