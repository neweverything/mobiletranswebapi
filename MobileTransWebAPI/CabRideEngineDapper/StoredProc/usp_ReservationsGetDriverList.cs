﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CabRideEngineDapper {
	public partial class usp_ReservationsGetDriverList {
		public string Fleet { get; set; }
		public string ServiceType { get; set; }
		public long DriverID { get; set; }
		public string DriverName { get; set; }
		public string DriverCell { get; set; }
		public double Latitude { get; set; }
		public double Longitude { get; set; }
		public int MaxPassengers { get; set; }
		public decimal BaseRate { get; set; }
		public decimal MileageRate { get; set; }

		public decimal LikePercent { get; set; }
		public int LikesCount { get; set; }
		public decimal ReplyPercent { get; set; }
		public int ReplyCount { get; set; }
		public bool Referred { get; set; }
		public bool Favorite { get; set; }

		public string ProfileImageURL { get; set; }
		public DateTime ProfileImageModifiedDate { get; set; }

		public string ProfileThumbURL { get; set; }
		public DateTime ThumbModDate { get; set; }

		public int Distance { get; set; }
		public DateTime GPSTime { get; set; }
		public int Tier { get; set; }
		public int AccountDriver { get; set; }
		public bool VIPDriver { get; set; }
		public string GPSAge { get; set; }

		public long AccountID { get; set; }
		public decimal SearchRadius { get; set; }
	}
}
