﻿using CabRideEngineDapper.Utils;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CabRideEngineDapper.StoredProc {
    public class usp_CardGatewaysGet {
        public Int32 CardGatewayGroupsID { get; set; }
        public string GroupName { get; set; }
        public bool DefaultGroup { get; set; }
        public string Processor { get; set; }
        public Int32 CardGatewayDetailsID { get; set; }
        public string ReferenceKey { get; set; }
        public string ValueString { get; set; }
        public int ProcessorOrder { get; set; }
        public string Salt { get; set; }
        public bool Validate { get; set; }


        public static List<usp_CardGatewaysGet> GetGateWays(SqlConnection pConn, long pID) {
            return pConn.Query<usp_CardGatewaysGet>("usp_CardGatewaysGet", new { CardGatewayGroupsID = pID }, commandType: CommandType.StoredProcedure).ToList();
        }
    }
}
