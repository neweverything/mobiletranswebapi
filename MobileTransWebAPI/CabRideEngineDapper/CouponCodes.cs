﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

using Dapper;
using TaxiPassCommon;

using CabRideEngineDapper.Utils;

namespace CabRideEngineDapper {
	public partial class CouponCodes {

		public static CouponCodes GetCoupon(SqlConnection pConn, string pCode) {
			return pConn.GetList<CouponCodes>(new { CouponCode = pCode }).FirstOrDefault();
		}


	}
}
