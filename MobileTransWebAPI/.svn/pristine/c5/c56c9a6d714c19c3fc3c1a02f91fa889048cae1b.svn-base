﻿using CabRideEngineDapper;
using CabRideEngineDapper.Utils;
using MobileTransWebAPI.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using TaxiPassCommon;
using TaxiPassCommon.Banking.Enums;

namespace MobileTransWebAPI.Controllers {
    public class FundingPayCardInfoDelayedController : ApiController {

        public static readonly DateTime DefaultDate = DateTime.Parse("1/1/1900");

        public FundingPayCardInfo Get() {
            AffDriverLoginInfo info = new AffDriverLoginInfo() {
                Phone = "7328109300"
            };

            return GetFundingInfoDelayed("31B86712A18309A88813706BEC11335233BC997A7C80A994D8FB386217A89714E5A7636AE8A0F47A5CE766F760E8755645132F6E448C41C370AB0988E8AF3F888ACBA44495D0DA07B66255393F8F80DFEAE289037B1448441180B09A3BB2A313065C55F988A786B8B02FC018E88194FA10D232C5178ABE6942559CC345606178A22436B34D6FDCB6491E3FC1AFFD5A8D1E7D93B7D83251582042148A527014C6535609AB0A582F90E24E5C16B44F30FF258F435325A6D58448D7D67752DB14308ABEFB6E430007B12EEF94F427FFABA3ACBC12543F168F7A35544839B95A8B56CE4B809AC4ACF885596DB454464A419056280FF1F5F92830FBF31D10BDD02815988F5EA3C931238C4261CEF94D59F91C7D8143ACB880BFFF82C2ABDE7F00E8FB3A46414149EBABCE43B5F62844BA365ABB8E0411DD4000ABFCCF7390DB9358652889446D4ED576011B1350CECC591D6BC674BBB81D5D4F1A98FDC73A86141E9A3BE9B692BB878A0F9C008017B80610F96C3AE137987ACD5BF727625D111339641EAA7EDCEACA2A2F515B7606DFEB7990E45051EB1AB2B48DCE23583512D1B8E194BE3044C0E5B7E8FC017BC8E49B5C4202AEA5DFAC0D9A0D43D70B86FAF41BBE3962EE8D96901548FFFC289136DF56366ED85926A870EB2F26BDF683"); // JsonConvert.SerializeObject(info).EncryptIceKey());
        }

        [HttpGet]
        public FundingPayCardInfo GetFundingInfoDelayed(string pRequest) {
            FundingPayCardInfo result = new FundingPayCardInfo();

            AffDriverLoginInfo info = new AffDriverLoginInfo();
            try {
                string request = pRequest.DecryptIceKey();
                info = JsonConvert.DeserializeObject<AffDriverLoginInfo>(request);



                if (!info.Phone.IsNullOrEmpty()) {
                    using (var conn = SqlHelper.OpenSqlConnection()) {
                        List<DriverPayments> payList = new List<DriverPayments>();

                        AffiliateDrivers driver = AffiliateDrivers.GetDriverByCallInCell(conn, info.Phone);
                        if (!driver.IsNullEntity) {
                            DateTime now = WebApiApplication.MyTZConverter.FromUniversalTime(driver.Affiliate.TimeZone, DateTime.UtcNow);
                            if (now.DayOfWeek == DayOfWeek.Saturday || now.DayOfWeek == DayOfWeek.Sunday) {
                                result.ErrorMessage = "Funding not available on the weekend";
                                return result;
                            }
                            if (!driver.TransCardAdminNo.IsNullOrEmpty()) {
                                var achPaidCnt = usp_FundPayCardInfo.GetVoucherCountForPayCard(conn, driver.TransCardAdminNo);
                                result.PayCardLoadsAvailable = achPaidCnt.PayCardLoadsPerMonth - achPaidCnt.AchPaidCount;
                                result.TotalVoucherPayout = achPaidCnt.DriverTotal;
                                result.VouchersAvailableForPayment = achPaidCnt.VoucherCount;
                                result.DisplayFundingMsgAt = achPaidCnt.DisplayFundingMsgAt;
                                result.FundingAcctAmt = achPaidCnt.FundingAcctAmt;
								if (driver.PayCardType.IsNullOrEmpty()) {
									driver.PayCardType = PayCardTypes.USBank.ToString();
									driver.Save();
								}
								if (driver.PayCardType != PayCardTypes.USBank.ToString()) {
									result.FundingAcctAmt = driver.GetPayCardFundingBalance(conn);
								}
							}
                        }




                        string error = "";
                        DateTime dtEnd = CalcAffiliateEndDate(driver.Affiliate, out error);
                        result.ErrorMessage = error;

                        if (result.ErrorMessage.IsNullOrEmpty()) {
                            string holidayName = BankHoliday.GetByDate(conn, DateTime.Today).Name;
                            if (!holidayName.IsNullOrEmpty()) {
                                result.ErrorMessage = "No funding today due to bank holiday: " + holidayName;
                            } else if (dtEnd == DefaultDate) {
                                result.ErrorMessage = "Funding not available today";
                            }
                        }

                        if (result.ErrorMessage.IsNullOrEmpty()) {
                            DateTime start = dtEnd.AddDays(-(double)SystemDefaultsDict.GetByKey("DriverPay", "Do Not Pay Age", false).ValueAsDecimal);
                            payList = DriverPayments.GetPaymentsToAchForAffiliateDriver(conn, driver, start, dtEnd);
                            result.VouchersAvailableForPayment = payList.Count;
                            result.TotalVoucherPayout = payList.Sum(p => p.DriverTotalPaid);
                        }


                    }
                }
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
            return result;

        }


        public static DateTime CalcAffiliateEndDate(Affiliate pAffiliate, out string pError) {

            DateTime now = WebApiApplication.MyTZConverter.ConvertTime(DateTime.Now, TimeZone.CurrentTimeZone.StandardName, pAffiliate.TimeZone);
            switch (now.DayOfWeek) {
                case DayOfWeek.Saturday:
                case DayOfWeek.Sunday:
                    pError = "Cannot fund during the weekend";
                    now = DefaultDate;
                    break;

                default:
                    now = now.AddHours(-pAffiliate.FundPayCardDelayedMinusHours);
                    pError = "";
                    break;

            }


            return now;
        }
    }
}
