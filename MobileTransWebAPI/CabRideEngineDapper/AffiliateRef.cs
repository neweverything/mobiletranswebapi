﻿using CabRideEngineDapper.Utils;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace CabRideEngineDapper {
    public partial class AffiliateRef {

        public static AffiliateRef CreateUpdate(SqlConnection pConn, long pAffiliateID, long? pAffiliateRefID, string pGroup, string pReference, string pValue, string pModifiedBy) {
            string sql = "usp_AffiliateRefUpdate";
            DynamicParameters parms = new DynamicParameters();
            parms.Add("AffiliateID", pAffiliateID);
            if (pAffiliateRefID.GetValueOrDefault(0) > 0) {
                parms.Add("AffiliateRefID", pAffiliateRefID);
            }
            parms.Add("ReferenceGroup", pGroup);
            parms.Add("ReferenceKey", pReference);
            parms.Add("ValueString", pValue);
            parms.Add("ModifiedBy", pModifiedBy);

            AffiliateRef rec = null;
            try {
                rec = pConn.Query<AffiliateRef>(sql, parms, commandType: CommandType.StoredProcedure).FirstOrDefault();
            } catch (Exception ex) {
                throw ex;
            }
            return rec;
        }


        public static AffiliateRef GetCreate(long pAffiliateID, string pGroup, string pReference, string pDefaultValue) {
            using (var conn = SqlHelper.OpenSqlConnection()) {
                return GetCreate(conn, pAffiliateID, pGroup, pReference, pDefaultValue);
            }
        }

        public static AffiliateRef GetCreate(SqlConnection pConn, long pAffiliateID, string pGroup, string pReference, string pDefaultValue) {

            if (pAffiliateID == 0) {
                return AffiliateRef.GetNullEntity();
            }

            AffiliateRef rec = GetByGroupReference(pConn, pAffiliateID, pGroup, pReference);
            if (rec == null || rec.IsNullEntity) {
                try {
                    rec = CreateUpdate(pConn, pAffiliateID, null, pGroup, pReference, pDefaultValue, CabRideDapperSettings.LoggedInUser);
                    // done this way, as edit was not working correctly
                    if (rec.IsNullEntity) {
                        rec = GetByGroupReference(pConn, pAffiliateID, pGroup, pReference);
                    }
                } catch (Exception ex) {
                    throw ex;
                }
            }
            return rec;
        }


        public static AffiliateRef GetByGroupReference(SqlConnection pConn, long pAffiliateID, string pGroup, string pKey) {
            string sql = @"SELECT  *
                            FROM    dbo.AffiliateRef
                            WHERE   AffiliateID = @AffiliateID
                                    AND ReferenceGroup = @ReferenceGroup
                                    AND ReferenceKey = @ReferenceKey";

            return pConn.Get<AffiliateRef>(sql, new { AffiliateID = pAffiliateID, ReferenceGroup = pGroup, ReferenceKey = pKey });
        }

        public static List<AffiliateRef> GetByGroup(long pAffiliateID, string pGroup) {
            string sql = @"SELECT  *
                            FROM    dbo.AffiliateRef
                            WHERE   AffiliateID = @AffiliateID
                                    AND ReferenceGroup = @ReferenceGroup";

            using (var conn = SqlHelper.OpenSqlConnection()) {
                return conn.Query<AffiliateRef>(sql, new { AffiliateID = pAffiliateID, ReferenceGroup = pGroup }).ToList();
            }
        }


    }
}
