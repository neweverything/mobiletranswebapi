﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using System.Collections.Generic;
using System.Linq;
using System.Text;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

using TaxiPassCommon;
using TaxiPassCommon.Banking;


namespace CabRideEngine {
	/// <summary>
	/// The developer class representing the TransCardDefaults business object class.
	/// </summary>
	/// <remarks>
	/// <para>Place all application-specific business logic in this class.</para>
	/// <para>
	/// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
	/// and will not overwrite changes.
	/// </para>
	/// <para>
	/// This class inherits from the generated class, its companion in the pair of classes 
	/// defining a business object. Properties in the generated class may be overridden in 
	/// order to change the behavior or add business logic.
	/// </para>
	/// <para>
	/// This class is the final class in the inheritance chain for this business object. 
	/// It should not be inherited.
	/// </para>
	/// </remarks>
	[Serializable]
	public sealed partial class TransCardDefaults : TransCardDefaultsDataRow {

		#region Constructors -- DO NOT MODIFY

		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new';
		// it is also required if the class is visible from a web service.
		// DO NOT REMOVE **
		private TransCardDefaults() : this(null) { }

		/// <summary>
		/// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
		/// </summary>
		/// <param name="pRowBuilder"></param>
		public TransCardDefaults(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static TransCardDefaults Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      TransCardDefaults aTransCardDefaults = pManager.CreateEntity<TransCardDefaults>();
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aTransCardDefaults, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aTransCardDefaults.AddToManager();
		//      return aTransCardDefaults;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		// Add additional logic to your business object here...
		public static TransCardDefaults Create(PersistenceManager pPM) {
			TransCardDefaults rec = null;

			try {
				rec = pPM.CreateEntity<TransCardDefaults>();

				// Using the IdeaBlade Id Generation technique
				//pPM.GenerateId(rec, TransCardDefaults.TransCardDefaultIDEntityColumn);
				rec.AddToManager();

			} catch (Exception ex) {
				throw ex;
			}
			return rec;
		}

		protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
			if (IsDeserializing)
				return;
			base.OnColumnChanging(pArgs);
			//DoAudit(pArgs);
		}

		private void DoAudit(DataColumnChangeEventArgs pArgs) {
			if (!this.PersistenceManager.IsClient) {
				return;
			}
			if (this.RowState == DataRowState.Added) {
				return;
			}
			if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
				AuditRecord = TransCardDefaultsAudit.Create(this.PersistenceManager, this);
			}
		}


		public override string ClientID {
			get {
				return base.ClientID.DecryptIceKey();
			}
			set {
				base.ClientID = value.EncryptIceKey();
			}
		}

		public override string ClientPass {
			get {
				return base.ClientPass.DecryptIceKey();
			}
			set {
				base.ClientPass = value.EncryptIceKey();
			}
		}

		public override string LocationID {
			get {
				return base.LocationID.DecryptIceKey();
			}
			set {
				base.LocationID = value.EncryptIceKey();
			}
		}

		public override string LocationCity {
			get {
				return base.LocationCity.DecryptIceKey();
			}
			set {
				base.LocationCity = value.EncryptIceKey();
			}
		}

		public override string EmpPIN {
			get {
				return base.EmpPIN.DecryptIceKey();
			}
			set {
				base.EmpPIN = value.EncryptIceKey();
			}
		}

		public override string AcctNo {
			get {
				return base.AcctNo.DecryptIceKey();
			}
			set {
				base.AcctNo = value.EncryptIceKey();
			}
		}

		public override string FundingAccount {
			get {
				return base.FundingAccount.DecryptIceKey();
			}
			set {
				base.FundingAccount = value.EncryptIceKey();
			}
		}

		private static TransCardDefaults GetDefaults(PersistenceManager pPM) {
			RdbQuery qry = new RdbQuery(typeof(TransCardDefaults), TransCardDefaults.ActiveEntityColumn, EntityQueryOp.EQ, true);
			return pPM.GetEntities<TransCardDefaults>(qry)[0];
		}

		public static TransCardDefaults GetDefaults(PersistenceManager pPM, string pCardNumber) {
			List<TransCardDefaults> list = new List<TransCardDefaults>();
			TransCardDefaults oTCDefault = null;

			// comdata admin #'s are in the format "COMDATA nnnn" this will lose the "COMDATA"
			pCardNumber = TaxiPassCommon.Banking.TransCard.AdminNoCleanUp(pCardNumber);

			//Get Card by Card Prefix
			list = (from p in pPM.GetEntities<TransCardDefaults>()
					where !p.CardPrefix.IsNullOrEmpty() && pCardNumber.StartsWith(p.CardPrefix)
					select p).ToList();
			
			if (list.Count > 0) {
				oTCDefault = list[0];
			} else {
				// 4/2 we have TransCards with prefixes not in the system
				// send email letting us know users need to set up an account
				TaxiPassCommon.Mail.EMailMessage email = new TaxiPassCommon.Mail.EMailMessage("email@taxipass.com"
																								, "TransCardDefaults missing card prefix " + pCardNumber.Left(6)
																								, "CabRideEngine.TransCardDefaults.GetDefaults could not find a prefix starting with " + pCardNumber.Left(6));
				email.UseWebServiceToSendMail = Properties.Settings.Default.EMailViaWebService;
				//email.SendMail("hani@taxipass.com;cadkins@taxipass.com");

				list = (from p in pPM.GetEntities<TransCardDefaults>()
						where p.ComData == false && p.Active == true
						select p).ToList();
				oTCDefault = list[0];
			}
			return oTCDefault;
		}


		// **************************************************************************************************************
		// Get default TransCard as prefix may not be in the database
		// **************************************************************************************************************
		public static TransCardDefaults GetDefaultTransCard(PersistenceManager pPM) {
			RdbQuery qry = new RdbQuery(typeof(TransCardDefaults), TransCardDefaults.ActiveEntityColumn, EntityQueryOp.EQ, true);
			qry.AddClause(TransCardDefaults.ComDataEntityColumn, EntityQueryOp.EQ, false);
			return pPM.GetEntities<TransCardDefaults>(qry)[0];
		}

		// **************************************************************************************************************
		// pass in the id, return the row
		// **************************************************************************************************************
		public static TransCardDefaults GetTransCardDefaultsByID(PersistenceManager pPM, long pID){
			RdbQuery qry = new RdbQuery(typeof(TransCardDefaults), TransCardDefaultIDEntityColumn, EntityQueryOp.EQ, pID);
			return pPM.GetEntity<TransCardDefaults>(qry);
		}

		public static TransCardDefaults GetComData(PersistenceManager pPM) {
			RdbQuery qry = new RdbQuery(typeof(TransCardDefaults), ComDataEntityColumn, EntityQueryOp.EQ, true);
			return pPM.GetEntity<TransCardDefaults>(qry);
		}

		public static EntityList<TransCardDefaults> GetComDataList(PersistenceManager pPM) {
			RdbQuery qry = new RdbQuery(typeof(TransCardDefaults), ComDataEntityColumn, EntityQueryOp.EQ, true);
			return pPM.GetEntities<TransCardDefaults>(qry);
		}


		//public TransCardAccess GetTransCardAccess() {
		//    //TransCardDefaults defaults = TransCardDefaults.GetDefaults(pPM);

		//    TransCardAccess access = new TransCardAccess();
		//    access.AcctNo = this.AcctNo;
		//    access.ClientID = this.ClientID;
		//    access.ClientPass = this.ClientPass;
		//    access.CurrencyCode = this.CurrencyCode;
		//    access.EmpPIN = this.EmpPIN;
		//    access.LocationCity = this.LocationCity;
		//    access.LocationCountry = this.LocationCountry;
		//    access.LocationID = this.LocationID;
		//    access.LocationState = this.LocationState;
		//    access.TransCode = this.TransCode;
		//    access.WebServiceURL = this.WebServiceURL;
		//    access.IsComData = this.ComData;
		//    access.FundingAccount = this.FundingAccount;

		//    return access;
		//}
	}

	#region EntityPropertyDescriptors.TransCardDefaultsPropertyDescriptor
	////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
	////    For example: 
	////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
	//public static partial class EntityPropertyDescriptors {
	//	public partial class TransCardDefaultsPropertyDescriptor : BaseEntityPropertyDescriptor {
	//    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
	//    public AdaptedPropertyDescriptor Age {
	//      get { return this.Get("Age"); }
	//    }
	//  }
	//} 
	#endregion

}