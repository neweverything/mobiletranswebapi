﻿using CabRideEngine;
using IdeaBlade.Persistence;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Web.Http;
using System.Web.Script.Serialization;
using TaxiPassCommon;

namespace MobileTransWebAPI.Controllers {
    public class DriverSignatureController : ApiController {

        private PersistenceManager mPM = WebApiApplication.UserPM;


        // ***********************************************************************************
        // Login using a Redeemer id/pw
        // ***********************************************************************************
        [HttpPost]
        public Models.DriverSignDevice DeviceLogin(Models.DriverSignDevice pDevice) {
            JavaScriptSerializer js = new JavaScriptSerializer();
            WebApiApplication.LogIt("  DeviceLogin", js.Serialize(pDevice));

            try {
                pDevice.DeviceLoginID = -1;
                pDevice.DeviceID = -1;
                string sMsg = SetPM();




                if (!sMsg.IsNullOrEmpty()) {
                    pDevice.ErrorMsg = sMsg;
                    return pDevice;
                }

                CabRideEngine.TaxiPassRedeemers oRedeemer = GetRedeemer(pDevice.TPRedeemerEMail);
                if (oRedeemer.IsNullEntity) {
                    pDevice.ErrorMsg = "Redeemer not found";
                    return pDevice;
                }


                if (oRedeemer.PasswordDecrypted.CompareTo(pDevice.TPRedeemerPW) == 0 && oRedeemer.Active) {

                    pDevice.TPRedeemerID = oRedeemer.TaxiPassRedeemerID;

                    StringBuilder sql = new StringBuilder();
                    sql.AppendFormat("usp_DeviceLogin @SerialNo = '{0}'", pDevice.SerialNo);
                    sql.AppendFormat(", @PushToken = '{0}'", pDevice.PushToken);
                    sql.AppendFormat(", @LoginDate = '{0}'", pDevice.LoginDate);

                    if (!pDevice.LogoutDate.IsNullOrEmpty()) {
                        sql.AppendFormat(", @LogoutDate = '{0}'", pDevice.LogoutDate);
                    }
                    sql.AppendFormat(", @TaxiPassRedeemerID = {0}", oRedeemer.TaxiPassRedeemerID);
                    sql.Append(", @ModifedBy = 'MobileTransWebAPI.DeviceLogin'");
                    Entity[] oLogin = DynamicEntities.DynamicDBCall(mPM, sql.ToString());

                    if (oLogin == null) {
                        pDevice.ErrorMsg = "There was a problem logging in";

                    } else if (oLogin.Length == 0) {
                        pDevice.ErrorMsg = "Error logging in";

                    } else {

                        pDevice.ErrorMsg = oLogin[0]["Message"].ToString();
                        if (pDevice.ErrorMsg.IsNullOrEmpty()) {

                            if (!oLogin[0]["DeviceLoginID"].ToString().IsNullOrEmpty()) {
                                pDevice.DeviceLoginID = long.Parse(oLogin[0]["DeviceLoginID"].ToString());
                            }

                            if (!oLogin[0]["DeviceID"].ToString().IsNullOrEmpty()) {
                                pDevice.DeviceID = long.Parse(oLogin[0]["DeviceID"].ToString());
                            }
                            pDevice.SigRequired = false;

                            StringBuilder sInput = new StringBuilder();
                            sInput.AppendFormat("D={0}|R={1}", pDevice.DeviceID, pDevice.TPRedeemerEMail);
                            Models.DriverSignDevice oVouchers = GetVouchers(sInput.ToString().EncryptIceKey());
                            if (oVouchers != null) {
                                pDevice.SigSaved = oVouchers.SigSaved;
                                pDevice.Vouchers = oVouchers.Vouchers;
                                pDevice.Signature = oVouchers.Signature;
                                pDevice.SigRequired = oVouchers.SigRequired;
                                pDevice.SignatureAgreement = oVouchers.SignatureAgreement;
                                pDevice.RedeemerMasterVoucherID = oVouchers.RedeemerMasterVoucherID;
                            }
                        }
                    }
                } else {
                    pDevice.ErrorMsg = "Unable to log in";
                }

            } catch (Exception ex) {
                pDevice.ErrorMsg = "Login Error: " + ex.Message;
            }
            return pDevice;
        }


        // ***********************************************************************************
        // Given a device ID and RedeemerID, 
        // load any vouchers in the Redeemer's queue (Redeemer Master Voucher)
        // ***********************************************************************************
        [HttpGet]
        public Models.DriverSignDevice GetVouchers(string pInput) {
            //WebApiApplication.LogIt("  GetVouchers", js.Serialize(pInput));
            JavaScriptSerializer js = new JavaScriptSerializer();
            string[] arInput = pInput.DecryptIceKey().Split('|');

            Models.DriverSignDevice oDevice = new Models.DriverSignDevice();
            oDevice.SigRequired = false;

            Models.DriverSignVouchers oDriverVoucher;

            #region Validations
            string sMsg = SetPM();
            int iPos = 0;
            if (sMsg.IsNullOrEmpty() && arInput.Length != 2) {
                sMsg = "Invalid input";
            } else {
                try {
                    oDevice.DeviceID = int.Parse(arInput[iPos].Substring(arInput[iPos].IndexOf('=') + 1));
                    iPos++;
                    CabRideEngine.TaxiPassRedeemers oRedeemer = GetRedeemer(arInput[iPos].Substring(arInput[iPos].IndexOf('=') + 1));
                    oDevice.TPRedeemerEMail = arInput[iPos].Substring(arInput[iPos].IndexOf('=') + 1);
                } catch (Exception) {
                    sMsg = "Unable to parse arg #" + iPos.ToString();
                }
            }
            if (sMsg.IsNullOrEmpty() && oDevice.DeviceID == 0) {
                sMsg = "Invalid Device identifier";
            } else if (sMsg.IsNullOrEmpty() && oDevice.TPRedeemerEMail.IsNullOrEmpty()) {
                sMsg = "Invalid Redeemer identifier";
            }

            if (!sMsg.IsNullOrEmpty()) {
                oDevice.ErrorMsg = sMsg;
                return oDevice;
            }
            #endregion Validations

            oDevice.Vouchers = new List<Models.DriverSignVouchers>();

            StringBuilder sql = new StringBuilder();
            sql.AppendFormat("usp_DeviceGetVouchers @DeviceID = {0}", oDevice.DeviceID);
            sql.AppendFormat(", @RedeemerEmail = '{0}'", oDevice.TPRedeemerEMail);
            Entity[] oVouchers = DynamicEntities.DynamicDBCall(mPM, sql.ToString());
            if (oVouchers != null) {
                foreach (Entity oVoucher in oVouchers) {
                    oDevice.Signature = oVoucher["DriverSignature"].ToString();
                    oDevice.SigRequired = bool.Parse(oVoucher["SignatureRequired"].ToString());
                    oDevice.SigSaved = bool.Parse(oVoucher["SigSaved"].ToString());
                    oDriverVoucher = new Models.DriverSignVouchers();
                    oDevice.RedeemerMasterVoucherID = long.Parse(oVoucher["RedeemerMasterVoucherID"].ToString());
                    oDevice.SignatureAgreement = oVoucher["SignatureAgreement"].ToString();

                    oDriverVoucher.DriverPaymentID = long.Parse(oVoucher["DriverPaymentID"].ToString());
                    oDriverVoucher.TransNo = oVoucher["TransNo"].ToString();
                    oDriverVoucher.Amount = decimal.Parse(oVoucher["DriverTotal"].ToString());
                    oDriverVoucher.DriverFee = decimal.Parse(oVoucher["DriverFee"].ToString());
                    oDriverVoucher.RedemptionFee = decimal.Parse(oVoucher["RedemptionFee"].ToString());

                    oDevice.Vouchers.Add(oDriverVoucher);
                }
            }

            return oDevice;
        }


        // ***********************************************************************************
        // Save the Driver's signature to disk
        // If pSigDevice.Signature is empty/null this will clear the db entry
        // ***********************************************************************************
        [HttpPost]
        public Models.DriverSignDevice SetSignature(Models.DriverSignDevice pSigDevice) {
            string Step = "";
            string sDBPath = "";
            string sModifiedBy = "pSigDevice.Signature Cleared";
            try {
                #region SaveImage
                if (!pSigDevice.Signature.IsNullOrEmpty()) {
                    // Base path
                    string sFullPath = Properties.Settings.Default.DriverSignaturePath;
                    if (!System.IO.Directory.Exists(sFullPath)) {
                        Directory.CreateDirectory(sFullPath);
                    }

                    // Base path + today's date
                    sDBPath = DateTime.Now.ToString(@"yyyy\\MM\\dd");
                    sFullPath = Path.Combine(sFullPath, sDBPath);
                    if (!Directory.Exists(sFullPath)) {
                        Directory.CreateDirectory(sFullPath);
                    }

                    Step = "4";

                    string sFileName = "RedeemerMasterVoucherID_" + pSigDevice.RedeemerMasterVoucherID.ToString() + ".png";
                    string sFullFileName = sFullPath + @"\" + sFileName;

                    sDBPath = Path.Combine(sDBPath, sFileName);

                    // remove previous uploads
                    if (File.Exists(sFullFileName)) {
                        File.Delete(sFullFileName);
                    }


                    // Create the image
                    TaxiPassCommon.Images.ImageHelper oImgHelper = new TaxiPassCommon.Images.ImageHelper();
                    System.Drawing.Image oImg = oImgHelper.Base64ToImage(pSigDevice.Signature);

                    // Save the file to disk
                    Step = sFullFileName;
                    oImg.Save(sFullFileName, System.Drawing.Imaging.ImageFormat.Png);

                    sModifiedBy = "pSigDevice.Signature";
                }
                #endregion SaveImage


                // Save the location to DB
                StringBuilder sql = new StringBuilder();
                sql.AppendFormat("usp_RedeemerMasterVoucherUpdate @RedeemerMasterVoucherID ={0}", pSigDevice.RedeemerMasterVoucherID.ToString());
                sql.AppendFormat(", @DriverSignature = '{0}'", sDBPath);
                sql.AppendFormat(", @ModifiedBy = '{0}'", sModifiedBy);
                sql.AppendFormat(", @ModifiedDate = '{0}'", DateTime.UtcNow.ToString());
                DynamicEntities.DynamicDBCall(mPM, sql.ToString());

                // clear out the signature image to avoid sending it back
                pSigDevice.Signature = "";
                pSigDevice.ErrorMsg = Step;
                pSigDevice.SigSaved = true;

            } catch (Exception ex) {
                pSigDevice.SigSaved = false;
                pSigDevice.ErrorMsg = "Error during upload: [" + Step + "] " + ex.Message;
            }
            return pSigDevice;

        }

        // ***********************************************************************************
        // Get a TPRedeemer
        // ***********************************************************************************
        private CabRideEngine.TaxiPassRedeemers GetRedeemer(string pEmail) {
            return CabRideEngine.TaxiPassRedeemers.GetRedeemer(mPM, pEmail);
        }


        // ***********************************************************************************
        // Save the driver's signature
        // ***********************************************************************************
        private void SaveSignature() {
        }



        // **************************************************************************
        // Init the PersistenceManager
        // **************************************************************************
        private string SetPM() {
            string sMsg = "";

            mPM = WebApiApplication.UserPM;
            if (mPM == null) {
                sMsg = "Could not connect to database";
            }
            return sMsg;
        }
    }
}

