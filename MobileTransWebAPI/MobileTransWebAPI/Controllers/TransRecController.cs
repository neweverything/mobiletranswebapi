﻿using CabRideEngineDapper;
using CabRideEngineDapper.Utils;
using Dapper;
using MobileTransWebAPI.Models;
using Newtonsoft.Json;
using System;
using System.Web.Http;
using TaxiPassCommon;

namespace MobileTransWebAPI.Controllers {
    public class TransRecController : ApiController {


        public ChargeInfo Get(string pRequest) {
            //ChargeInfo result = new ChargeInfo();

            //try {
            TransactionRequest req = JsonConvert.DeserializeObject<TransactionRequest>(pRequest.Decrypt());
            //    if (req.AffiliateID == 0 || req.AffiliateDriverID == 0 || req.DriverPaymentID == 0) {
            //        result.ErrorMsg = "Invalid Request";
            //        return result;
            //    }

            //    string sql = @"SELECT  *
            //                FROM    dbo.DriverPayments
            //                WHERE   DriverPaymentID = @DriverPaymentID
            //                        AND AffiliateID = @AffiliateID
            //                        AND AffiliateDriverID = @AffiliateDriverID";
            //    using (var conn = SqlHelper.OpenSqlConnection()) {
            //        //var list = conn.Query<DriverPayments>(sql, new { DriverPaymentID = req.DriverPaymentID, AffiliateID = req.AffiliateID, AffiliateDriverID = req.AffiliateDriverID });

            //        DriverPayments rec = conn.Get<DriverPayments>(sql, new { DriverPaymentID = req.DriverPaymentID, AffiliateID = req.AffiliateID, AffiliateDriverID = req.AffiliateDriverID });

            //        if (rec.IsNullEntity) {
            //            result.ErrorMsg = "Could not find receipt. Check receipt number and ensure correct login used for this transaction";
            //            return result;
            //        }

            //        result.AffDriverID = rec.AffiliateDriverID.GetValueOrDefault(0);
            //        result.AffiliateID = rec.AffiliateID.GetValueOrDefault(0);
            //        result.CardHolder = rec.CardHolder.Decrypt();
            //        result.CardNumberDisplay = rec.CardNumberDisplay;
            //        result.CardType = rec.CardType;
            //        if (rec.ChargeDate.HasValue) {
            //            result.ChargeDate = rec.ChargeDate.Value.ToString();
            //        }
            //        result.DriverID = rec.DriverID;
            //        result.Fare = rec.Fare;
            //        result.Gratuity = rec.Gratuity;
            //        result.Platform = rec.Platform;
            //        result.ReferenceNo = rec.ReferenceNo;
            //        result.TPFee = rec.TaxiPassFee;
            //        result.VoucherNo = rec.TransNo;
            //    }
            //} catch (Exception ex) {
            //    result.ErrorMsg = ex.Message;
            //}

            return Post(req);
        }

        public ChargeInfo Post([FromBody]TransactionRequest pRequest) {

            ChargeInfo result = new ChargeInfo();

            try {
                if (pRequest.AffiliateID == 0 || pRequest.AffiliateDriverID == 0 || pRequest.DriverPaymentID == 0) {
                    result.ErrorMsg = "Invalid Request";
                    return result;
                }

                string sql = @"SELECT  *
                            FROM    dbo.DriverPayments
                            WHERE   DriverPaymentID = @DriverPaymentID
                                    AND AffiliateID = @AffiliateID
                                    AND AffiliateDriverID = @AffiliateDriverID";
                using (var conn = SqlHelper.OpenSqlConnection()) {
                    //var list = conn.Query<DriverPayments>(sql, new { DriverPaymentID = req.DriverPaymentID, AffiliateID = req.AffiliateID, AffiliateDriverID = req.AffiliateDriverID });

                    DriverPayments rec = conn.Get<DriverPayments>(sql, new { DriverPaymentID = pRequest.DriverPaymentID, AffiliateID = pRequest.AffiliateID, AffiliateDriverID = pRequest.AffiliateDriverID });

                    if (rec.IsNullEntity) {
                        result.ErrorMsg = "Could not find receipt. Check receipt number and ensure correct login used for this transaction";
                        return result;
                    }

                    result.AffDriverID = rec.AffiliateDriverID.GetValueOrDefault(0);
                    result.AffiliateID = rec.AffiliateID.GetValueOrDefault(0);
                    result.CardHolder = rec.CardHolder.Decrypt();
                    result.CardNumberDisplay = rec.CardNumberDisplay;
                    result.CardType = rec.CardType;
                    if (rec.ChargeDate.HasValue) {
                        result.ChargeDate = rec.ChargeDate.Value.ToString();
                    }
                    result.DriverID = rec.DriverID;
                    result.Fare = rec.Fare;
                    result.Gratuity = rec.Gratuity;
                    result.Platform = rec.Platform;
                    result.ReferenceNo = rec.ReferenceNo;
                    result.TPFee = rec.TaxiPassFee;
                    result.VoucherNo = rec.TransNo;
                }
            } catch (Exception ex) {
                result.ErrorMsg = ex.Message;
            }

            return result;

        }
    }
}