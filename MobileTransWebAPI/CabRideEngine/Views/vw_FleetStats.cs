using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

namespace CabRideEngine {
  /// <summary>
  /// The developer class representing the vw_FleetStats business object class.
  /// </summary>
  /// <remarks>
  /// <para>Place all application-specific business logic in this class.</para>
  /// <para>
  /// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
  /// and will not overwrite changes.
  /// </para>
  /// <para>
  /// This class inherits from the generated class, its companion in the pair of classes 
  /// defining a business object. Properties in the generated class may be overridden in 
  /// order to change the behavior or add business logic.
  /// </para>
  /// <para>
  /// This class is the final class in the inheritance chain for this business object. 
  /// It should not be inherited.
  /// </para>
  /// </remarks>
  [Serializable]
  public sealed partial class vw_FleetStats : vw_FleetStatsDataRow {
  
    #region Constructors -- DO NOT MODIFY
    
    // Do not create constructors for this class
    // Developers cannot create instances of this class with the "new" keyword
    // You should write a static Create method instead; see the "Suggested Customization" region
    // Please review the documentation to learn about entity creation and inheritance

    // This private constructor prevents accidental instance creation via 'new';
    // it is also required if the class is visible from a web service.
    // DO NOT REMOVE **
    private vw_FleetStats() : this(null) {}

    /// <summary>
    /// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
    /// </summary>
    /// <param name="pRowBuilder"></param>
    public vw_FleetStats(DataRowBuilder pRowBuilder) 
      : base(pRowBuilder) {
    }

    #endregion
    
    #region Suggested Customizations
  
//    // Use this factory method template to create new instances of this class
//    public static vw_FleetStats Create(PersistenceManager pManager,
//      ... your creation parameters here ...) { 
//
//      vw_FleetStats avw_FleetStats = pManager.CreateEntity<vw_FleetStats>();
//
//      // if this object type requires a unique id and you have implemented
//      // the IIdGenerator interface implement the following line
//      pManager.GenerateId(avw_FleetStats, // add id column here //);
//
//      // Add custom code here
//
//      avw_FleetStats.AddToManager();
//      return avw_FleetStats;
//    }

//    // Implement this method if you want your object to sort in a specific order
//    public override int CompareTo(Object pObject) {
//    }

//    // Implement this method to customize the null object version of this class
//    protected override void UpdateNullEntity() {
//    }

    #endregion
    
    // Add additional logic to your business object here...
	// ******************************************************************
	// Get all Fleets' stats
	// ******************************************************************
	public static EntityList<vw_FleetStats> GetFleetStats(PersistenceManager mPM) {
		EntityList<vw_FleetStats> oFleets = mPM.GetEntities<vw_FleetStats>();
		oFleets.ApplySort(vw_FleetStats.FleetEntityColumn.ColumnName, System.ComponentModel.ListSortDirection.Ascending, false);
		return oFleets;
	}
  }
  
  #region EntityPropertyDescriptors.vw_FleetStatsPropertyDescriptor
  ////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
  ////    For example: 
  ////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
  //public static partial class EntityPropertyDescriptors {
	//	public partial class vw_FleetStatsPropertyDescriptor : AdaptedPropertyDescriptor {
  //    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
  //    public AdaptedPropertyDescriptor Age {
  //      get { return this.Get("Age"); }
  //    }
  //  }
  //} 
  #endregion
  
}