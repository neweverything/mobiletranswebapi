using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

namespace CabRideEngine {
	/// <summary>
	/// The developer class representing the GateWayIssue business object class.
	/// </summary>
	/// <remarks>
	/// <para>Place all application-specific business logic in this class.</para>
	/// <para>
	/// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
	/// and will not overwrite changes.
	/// </para>
	/// <para>
	/// This class inherits from the generated class, its companion in the pair of classes 
	/// defining a business object. Properties in the generated class may be overridden in 
	/// order to change the behavior or add business logic.
	/// </para>
	/// <para>
	/// This class is the final class in the inheritance chain for this business object. 
	/// It should not be inherited.
	/// </para>
	/// </remarks>
	[Serializable]
	public sealed partial class GateWayIssue : GateWayIssueDataRow {

		#region Constructors -- DO NOT MODIFY

		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new';
		// it is also required if the class is visible from a web service.
		// DO NOT REMOVE **
		private GateWayIssue() : this(null) { }

		/// <summary>
		/// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
		/// </summary>
		/// <param name="pRowBuilder"></param>
		public GateWayIssue(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static GateWayIssue Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      GateWayIssue aGateWayIssue = pManager.CreateEntity<GateWayIssue>();
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aGateWayIssue, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aGateWayIssue.AddToManager();
		//      return aGateWayIssue;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		// Add additional logic to your business object here...
		private static GateWayIssue Create(GateWayBatchInfo pInfo) {
			GateWayIssue rec = null;

			try {
				rec = pInfo.PersistenceManager.CreateEntity<GateWayIssue>();
				rec.GateWayBatchInfoID = pInfo.GateWayBatchInfoID;
				rec.AddToManager();

			} catch (Exception ex) {
				throw ex;
			}
			return rec;
		}

		public static GateWayIssue GetOrCreate(GateWayBatchInfo pInfo, TaxiPassCommon.Banking.com.USAePay.TransactionObject pTrans) {
			RdbQuery qry = new RdbQuery(typeof(GateWayIssue), TransNoEntityColumn, EntityQueryOp.EQ, pTrans.Details.OrderID);
			qry.AddClause(ReferenceNoEntityColumn, EntityQueryOp.EQ, pTrans.Response.RefNum);
			qry.AddClause(GateWayBatchInfoIDEntityColumn, EntityQueryOp.EQ, pInfo.GateWayBatchInfoID);
			GateWayIssue rec = pInfo.PersistenceManager.GetEntity<GateWayIssue>(qry);
			if (rec.IsNullEntity) {
				rec = Create(pInfo);
			}
			return rec;
		}

		public static GateWayIssue GetOrCreate(GateWayTransaction pTrans) {
			RdbQuery qry = new RdbQuery(typeof(GateWayIssue), TransNoEntityColumn, EntityQueryOp.EQ, pTrans.OrderID);
			qry.AddClause(ReferenceNoEntityColumn, EntityQueryOp.EQ, pTrans.RefNum);
			qry.AddClause(GateWayBatchInfoIDEntityColumn, EntityQueryOp.EQ, pTrans.GateWayBatchInfoID);
			GateWayIssue rec = pTrans.PersistenceManager.GetEntity<GateWayIssue>(qry);
			if (rec.IsNullEntity) {
				rec = Create(pTrans.GateWayBatchInfo);
			}
			rec.GateWayTransactionID = pTrans.GateWayTransactionID;
			return rec;
		}

		protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
			if (IsDeserializing)
				return;
			base.OnColumnChanging(pArgs);
			DoAudit(pArgs);
		}

		private void DoAudit(DataColumnChangeEventArgs pArgs) {
			if (!this.PersistenceManager.IsClient) {
				return;
			}
			if (this.RowState == DataRowState.Added) {
				return;
			}
			if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
				AuditRecord = GateWayIssueAudit.Create(this);
			}
		}

	}

	#region EntityPropertyDescriptors.GateWayIssuePropertyDescriptor
	////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
	////    For example: 
	////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
	//public static partial class EntityPropertyDescriptors {
	//	public partial class GateWayIssuePropertyDescriptor : BaseEntityPropertyDescriptor {
	//    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
	//    public AdaptedPropertyDescriptor Age {
	//      get { return this.Get("Age"); }
	//    }
	//  }
	//} 
	#endregion

}