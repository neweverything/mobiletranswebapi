﻿using System;
using System.Collections.Generic;
using System.Linq;
using TaxiPassCommon.Banking.Models;

namespace TaxiPassCommon.Banking {
	public class Gateway {

		private List<IGatewayCredentials> mProcessorList;
		private CardProcessors mProcessor = CardProcessors.All;

		public Gateway(List<IGatewayCredentials> pProcessorList) {
			mProcessorList = pProcessorList;
		}

		public Gateway(IGatewayCredentials pGateWayCredential) {
			mProcessorList = new List<IGatewayCredentials>();
			mProcessorList.Add(pGateWayCredential);
		}

		public Gateway(List<IGatewayCredentials> pProcessorList, CardProcessors pProcessor) {
			mProcessorList = pProcessorList;
			SetProcessor(pProcessor);
		}

		public Gateway(IGatewayCredentials pGateWayCredential, CardProcessors pProcessor) {
			mProcessorList = new List<IGatewayCredentials>();
			mProcessorList.Add(pGateWayCredential);
			SetProcessor(pProcessor);
		}

		public void SetProcessor(CardProcessors pProcessor) {
			mProcessor = pProcessor;
		}

		public List<CardProcessors> GetMyGateway() {
			return mProcessorList.Select(p => p.MyGateway()).ToList();
		}

		public string GetTransactionHistory(DateTime pStart, DateTime pEnd) {
			string result = "";
			foreach (IGatewayCredentials cred in mProcessorList) {
				if (mProcessor == CardProcessors.All || cred.MyGateway() == mProcessor) {
					IProcessor gateway = GetGateway(cred);
					try {
						result = gateway.GetTransactionHistory(pStart, pEnd);
					} catch (Exception ex) {
						result = ex.Message;
					}
				}
			}
			return result;
		}

		public List<CheckStatus> GetCheckTransactionHistory(DateTime pStart, DateTime pEnd) {
			List<CheckStatus> result = new List<CheckStatus>();
			foreach (IGatewayCredentials cred in mProcessorList) {
				if (mProcessor == CardProcessors.All || cred.MyGateway() == mProcessor) {
					IProcessor gateway = GetGateway(cred);
					try {
						result = gateway.GetCheckTransactionHistory(pStart, pEnd);
					} catch (Exception ex) {
						result.Add(new CheckStatus { StatusMessage = ex.Message });
					}
				}
			}
			return result;
		}

		/// <summary>
		/// Preauthorizes the card.
		/// </summary>
		/// <param name="pCardData">The card data.</param>
		/// <param name="pAuthAmount">The auth amount.</param>
		/// <param name="pComments">The comments.</param>
		/// <param name="pVoucherNo">The voucher no.</param>
		/// <param name="pService">The service type.</param>
		/// <returns></returns>
		public List<ProcessorResponse> PreAuthCard(CCRequest pRequest) {
			List<ProcessorResponse> resultList = new List<ProcessorResponse>();
			ProcessorResponse result = new ProcessorResponse();

			foreach (IGatewayCredentials cred in mProcessorList) {
				if (mProcessor == CardProcessors.All || cred.MyGateway() == mProcessor) {
					IProcessor gateway = GetGateway(cred);
					try {
						result = gateway.PreAuthCard(pRequest);
					} catch (Exception ex) {
						result.Result = "-1";
						result.Message = ex.Message;
						result.CreditCardProcessor = cred.MyGateway();
					}
					resultList.Add(result);

					if (result != null && result.Result == "0") {
						break;
					}
				}
			}

			return resultList;
		}

		/// <summary>
		/// Charges the card.
		/// </summary>
		/// <param name="pCardData">The card data.</param>
		/// <param name="pChargeAmount">The charge amount.</param>
		/// <param name="pComments">The comments.</param>
		/// <param name="pVoucherNo">The voucher no.</param>
		/// <param name="pService">The service type.</param>
		/// <param name="pDriverIDs">The driver Id or affiliate driver id.</param>
		/// <returns></returns>
		public List<ProcessorResponse> ChargeCard(CCRequest pRequest) {
			List<ProcessorResponse> resultList = new List<ProcessorResponse>();
			ProcessorResponse result = new ProcessorResponse();

			foreach (IGatewayCredentials cred in mProcessorList) {
				if (mProcessor == CardProcessors.All || cred.MyGateway() == mProcessor) {
					IProcessor gateway = GetGateway(cred);
					try {
						if (pRequest.CardData.ReferenceNo.IsNullOrEmpty()) {
							result = gateway.ChargeCard(pRequest);
						} else {
							result = gateway.ChargeAuth(new CCRequestRefNo(pRequest));
						}
					} catch (Exception ex) {
						result.Result = "-1";
						result.Message = ex.Message;
						result.CreditCardProcessor = cred.MyGateway();
					}
					resultList.Add(result);

					if (result != null && result.Result == "0") {
						break;
					}
				}
			}

			return resultList;
		}


		/// <summary>
		/// Voids the specified transaction by ReferenceNo.
		/// </summary>
		/// <param name="pReferenceNo">The reference no.</param>
		/// <param name="pTestCard">if set to <c>true</c> [test card].</param>
		/// <returns></returns>
		public ProcessorResponse Void(CCRequestRefNo pRequest) {
			ProcessorResponse result = new ProcessorResponse();
			result.TransType = TrxTypes.Void;

			if (mProcessorList.Count > 1 && mProcessor == CardProcessors.All) {
				throw new Exception("Can you process 1 gateway for void");
			}
			if (mProcessorList.Count == 0) {
				throw new Exception("No processor defined");
			}

			IGatewayCredentials cred = mProcessorList.FirstOrDefault(p => p.MyGateway() == mProcessor || mProcessor == CardProcessors.All);
			IProcessor gateway = GetGateway(cred);


			try {
				bool voided = gateway.Void(pRequest);
				if (voided) {
					result.Result = "0";
					result.Message = "Approved";
				} else {
					result.Result = "1";
					result.Message = "Error voiding transaction";
				}
				result.CreditCardProcessor = cred.MyGateway();
				result.Reference = pRequest.RefNum;
			} catch (Exception ex) {
				result.Result = "-1";
				result.Message = ex.Message;
				result.CreditCardProcessor = cred.MyGateway();
			}

			return result;
		}


		public List<ProcessorResponse> OpenCredit(CCRequest pRequest) {
			List<ProcessorResponse> resultList = new List<ProcessorResponse>();
			ProcessorResponse result = new ProcessorResponse();

			foreach (IGatewayCredentials cred in mProcessorList) {
				if (mProcessor == CardProcessors.All || cred.MyGateway() == mProcessor) {
					IProcessor gateway = GetGateway(cred);
					try {
						result = gateway.OpenCredit(pRequest);
					} catch (Exception ex) {
						result.Result = "-1";
						result.Message = ex.Message;
						result.CreditCardProcessor = cred.MyGateway();
					}
					resultList.Add(result);

					if (result != null && result.Result == "0") {
						break;
					}
				}
			}

			return resultList;
		}



		/// <summary>
		/// Gets the gateway.
		/// </summary>
		/// <param name="processor">The processor.</param>
		/// <returns></returns>
		/// <exception cref="System.Exception"></exception>
		private IProcessor GetGateway(IGatewayCredentials processor) {
			IProcessor gateway;
			switch (processor.MyGateway()) {
				case CardProcessors.Litle:
				case CardProcessors.LitleManual:
				case CardProcessors.LitleSwipe:
				case CardProcessors.LitleECommerce:
					gateway = new Litle(processor as LitleCredentials);
					break;

				case CardProcessors.USAePay:
				case CardProcessors.USAePayManual:
				case CardProcessors.USAePaySwipe:
				case CardProcessors.USAePayECommerce:
					gateway = new USAePay(processor as USAePayCredentials);
					break;

				case CardProcessors.Verisign:
					gateway = new Verisign(processor as VerisignCredentials);
					break;

				case CardProcessors.AuthorizeNet:
					gateway = new AuthorizNet(processor as AuthorizeNetCredentials);
					break;


				//case CardProcessors.Adyen:
				//    gateway = new Adyen(processor as AdyenCredentials);
				//    break;


				case CardProcessors.Stripe:
					gateway = new StripeFP(processor as StripeCredentials);
					break;

				case CardProcessors.CardConnect:
				case CardProcessors.CardConnectManual:
				case CardProcessors.CardConnectSwipe:
					gateway = new CardConnect(processor as CardConnectCredentials);
					break;


				case CardProcessors.ProPay:
				case CardProcessors.ProPayManual:
				case CardProcessors.ProPaySwipe:
					gateway = new ProPay(processor as ProPayCredentials);
					break;


				case CardProcessors.Newtek:
				case CardProcessors.NewtekManual:
				case CardProcessors.NewtekSwipe:
					gateway = new Newtek(processor as NewtekCredentials);
					break;

				default:
					throw new Exception(processor.MyGateway() + " has not defined GetGateway action");
			}
			return gateway;
		}


		/// <summary>
		/// Charges the exisitng authorization based on returned reference number from the gateway.
		/// </summary>
		/// <param name="pRefNum">The reference number.</param>
		/// <param name="pChargeAmount">The charge amount.</param>
		/// <param name="pTestCard">if set to <c>true</c> [test card].</param>
		/// <param name="pDriverID">The driver ID to identify the transaction on the gateway.</param>
		/// <returns></returns>
		/// <exception cref="System.Exception">Can you process 1 gateway for void that originally preauthorized the card</exception>
		public ProcessorResponse ChargeAuth(CCRequestRefNo pRequest) {
			if (mProcessorList.Count > 1 && mProcessor == CardProcessors.All) {
				throw new Exception("Can you process 1 gateway for void");
			}
			if (mProcessorList.Count == 0) {
				throw new Exception("No processor defined");
			}

			IGatewayCredentials cred = mProcessorList.FirstOrDefault(p => p.MyGateway() == mProcessor || mProcessor == CardProcessors.All);
			IProcessor gateway = GetGateway(cred);
			ProcessorResponse result = new ProcessorResponse();
			try {
				result = gateway.ChargeAuth(pRequest);
			} catch (Exception ex) {
				result.Result = "-1";
				result.Message = ex.Message;
				result.CreditCardProcessor = cred.MyGateway();
			}
			return result;
		}


		/// <summary>
		/// Refunds the specified amount from a previously charged trans using the Reference number.
		/// </summary>
		/// <param name="pRefNum">The Ref Num.</param>
		/// <param name="pRefundAmount">The refund amount.</param>
		/// <param name="pTestCard">if set to <c>true</c> [test card].</param>
		/// <returns></returns>
		/// <exception cref="System.Exception">Can you process 1 gateway that originally charged the card for refunds</exception>
		public ProcessorResponse Refund(CCRequestRefNo pRequest) {
			if (mProcessorList.Count > 1 && mProcessor == CardProcessors.All) {
				throw new Exception("Can you process 1 gateway for void");
			}
			if (mProcessorList.Count == 0) {
				throw new Exception("No processor defined");
			}

			IGatewayCredentials cred = mProcessorList.FirstOrDefault(p => p.MyGateway() == mProcessor || mProcessor == CardProcessors.All);
			IProcessor gateway = GetGateway(cred);
			ProcessorResponse result = new ProcessorResponse();
			try {
				result = gateway.Refund(pRequest);
			} catch (Exception ex) {
				result.Result = "-1";
				result.Message = ex.Message;
				result.CreditCardProcessor = cred.MyGateway();
			}
			return result;
		}

		/// <summary>
		/// Preauthorizes the card.
		/// </summary>
		/// <param name="pCardData">The card data.</param>
		/// <param name="pAuthAmount">The auth amount.</param>
		/// <param name="pComments">The comments.</param>
		/// <param name="pVoucherNo">The voucher no.</param>
		/// <param name="pService">The service type.</param>
		/// <returns></returns>
		public List<ProcessorResponse> AvsCheck(CCRequest pRequest) {
			List<ProcessorResponse> resultList = new List<ProcessorResponse>();
			ProcessorResponse result = new ProcessorResponse();

			foreach (IGatewayCredentials cred in mProcessorList) {
				if (mProcessor == CardProcessors.All || cred.MyGateway() == mProcessor) {
					IProcessor gateway = GetGateway(cred);
					try {
						result = gateway.AvsCheck(pRequest);
					} catch (Exception ex) {
						result.Result = "-1";
						result.Message = ex.Message;
						result.CreditCardProcessor = cred.MyGateway();
					}
					resultList.Add(result);
					if (result != null && result.Result == "0") {
						break;
					}
				}
			}

			return resultList;
		}

		public string GetMID() {
			string mid = "";
			IGatewayCredentials cred = mProcessorList.FirstOrDefault(p => p.MyGateway() == mProcessor || mProcessor == CardProcessors.All);
			try {
				var temp = cred.GetType().GetProperty("MerchantID");
				mid = temp.GetValue(cred).ToString();
			} catch (Exception ex) {
				Console.WriteLine(ex.Message);
			}

			return mid;
		}

		public List<IGatewayCredentials> GetCredentials() {
			return mProcessorList;
		}

		public List<ProcessorResponse> ProcessCheck(CheckRequest pRequest) {
			List<ProcessorResponse> resultList = new List<ProcessorResponse>();
			ProcessorResponse result = new ProcessorResponse();

			foreach (IGatewayCredentials cred in mProcessorList) {
				if (mProcessor == CardProcessors.All || cred.MyGateway() == mProcessor) {
					IProcessor gateway = GetGateway(cred);
					try {
						result = gateway.ProcessCheck(pRequest);
					} catch (Exception ex) {
						result.Result = "-1";
						result.Message = ex.Message;
						result.CreditCardProcessor = cred.MyGateway();
					}
					resultList.Add(result);

					result.Result += ""; // to prevent crash
					if (result.Result.IsNullOrEmpty() || result.Result == "0") {
						break;
					}
				}
			}

			return resultList;
		}

		public List<ProcessorResponse> RefundCheck(CheckRequest pRequest) {
			List<ProcessorResponse> resultList = new List<ProcessorResponse>();
			ProcessorResponse result = new ProcessorResponse();

			foreach (IGatewayCredentials cred in mProcessorList) {
				if (mProcessor == CardProcessors.All || cred.MyGateway() == mProcessor) {
					IProcessor gateway = GetGateway(cred);
					try {
						result = gateway.RefundCheck(pRequest);
					} catch (Exception ex) {
						result.Result = "-1";
						result.Message = ex.Message;
						result.CreditCardProcessor = cred.MyGateway();
					}
					resultList.Add(result);

					if (result != null && result.Result == "0") {
						break;
					}
				}
			}

			return resultList;
		}

	}
}
