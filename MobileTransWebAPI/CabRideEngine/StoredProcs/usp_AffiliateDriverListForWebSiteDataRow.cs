using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Runtime.Serialization;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

namespace CabRideEngine {
 
  #region usp_AffiliateDriverListForWebSiteDataTable
  /// <summary>
  /// The generated <see cref="T:IdeaBlade.Persistence.EntityTable"/> class  
  /// containing all <see cref="usp_AffiliateDriverListForWebSite"/> objects.
  /// </summary>
  /// <remarks>
  /// ****************************************************
  /// <para>** DO NOT MODIFY THIS CLASS - AutoGenerated Code</para>
  /// <para>** Any changes made to this code will be lost</para>
  /// <para>** the next time this code is regenerated.</para>
  /// <para>**</para>
  /// <para>** Generated from:   EntityBaseclass.template</para>
  /// <para>** Last modified:    2012-06-18T11:17:03.1351419-07:00</para>
  /// <para>** Template version: 3.110127</para>
  /// ****************************************************
  /// </remarks>
  [BindingBrowsable(false)]
  [Serializable]
  public partial class usp_AffiliateDriverListForWebSiteDataTable : IdeaBlade.Persistence.EntityTable, 
     IdeaBlade.Persistence.Rdb.IRdbTable {
  
    /// <summary>
    /// Initializes a new instance of the usp_AffiliateDriverListForWebSiteDataTable class with no arguments.
    /// </summary>
    public usp_AffiliateDriverListForWebSiteDataTable() {}

    /// <summary>
    /// Initializes a new instance of the usp_AffiliateDriverListForWebSiteDataTable class 
    /// with the <see cref="System.Runtime.Serialization.SerializationInfo"/> 
    /// and the <see cref="System.Runtime.Serialization.StreamingContext"/>.
    /// </summary>
    /// <param name="pInfo">The data needed to serialize or deserialize an object.</param>
    /// <param name="pContext">The source and destination of a given serialized stream.</param>
    protected usp_AffiliateDriverListForWebSiteDataTable(SerializationInfo pInfo, StreamingContext pContext) :
      base(pInfo, pContext) {}

    /// <summary>
    /// Creates a new instance of the <see cref="usp_AffiliateDriverListForWebSiteDataRow"/>.
    /// Constructs a row from the builder.
    /// <para>** For internal use only. **</para>
    /// </summary>
    /// <param name="pRowBuilder"></param>
    /// <returns>The <see cref="usp_AffiliateDriverListForWebSiteDataRow"/>.</returns>
    protected override DataRow NewRowFromBuilder(DataRowBuilder pRowBuilder) {
      return new usp_AffiliateDriverListForWebSite(pRowBuilder);
    }

    /// <summary>Returns the <see cref="System.Type"/> of <see cref="usp_AffiliateDriverListForWebSite"/>.</summary>
    protected override Type GetRowType() {
      return typeof(usp_AffiliateDriverListForWebSite);
    }
    
    /// <summary>
    /// Gets the name of the <see cref="T:IdeaBlade.Rdb.RdbKey"/> or 
    /// <see cref="T:IdeaBlade.Persistence.WS.WsKey"/> defined in <b>IdeaBlade.ibconfig</b>. 
    /// </summary>
    /// <remarks>For this <see cref="usp_AffiliateDriverListForWebSite"/>, the DataSourceKeyName="default".</remarks>
    public override String DataSourceKeyName {
      get { return @"default"; }
    }

    #region Column properties
    //**************************************
    //* Column properties
    //**************************************
    #endregion    

    /// <summary>Creates the schema mapping information for this usp_AffiliateDriverListForWebSiteDataTable.</summary>
    /// <returns>
    /// The <see cref="T:IdeaBlade.Persistence.TableMappingInfo"/> for this usp_AffiliateDriverListForWebSiteDataTable.
    /// </returns>
    protected override TableMappingInfo CreateTableMappingInfo() {
      RdbTableMappingInfo mappingInfo = new RdbTableMappingInfo(this);
      mappingInfo.SourceOwnerName = "dbo"; 
      mappingInfo.SourceTableName = "usp_AffiliateDriverListForWebSite"; 
   
      DataColumnMappingCollection columnMappings = mappingInfo.TableMapping.ColumnMappings;
      columnMappings.Clear();
      return mappingInfo;
    }
  
    /// <summary>** For internal use only. **</summary>
    protected override void UpdateTableMappingInfo() {
      base.UpdateTableMappingInfo();
    }

    /// <summary>
    /// Initializes usp_AffiliateDriverListForWebSite <see cref="System.Data.DataColumn"/> properties.
    /// </summary>
    public override void InitColumnProperties() {
      base.InitColumnProperties();

    }
  }
  #endregion
  
  #region usp_AffiliateDriverListForWebSiteDataRow
  /// <summary>
  /// The generated base class for <see cref="usp_AffiliateDriverListForWebSite"/>.
  /// </summary>
  /// <remarks>
  /// <para>
  /// It represents a row of persistable data in the <see cref="usp_AffiliateDriverListForWebSiteDataTable"/>
  /// and derives from <see cref="T:IdeaBlade.Persistence.Entity"/>.
	/// </para>
  /// ****************************************************
  /// <para>** DO NOT MODIFY THIS CLASS - AutoGenerated Code</para>
  /// <para>** Any changes made to this code will be lost</para>
  /// <para>** the next time this code is regenerated.</para>
  /// <para>**</para>
  /// <para>** Generated from:   EntityBaseclass.template</para>
  /// <para>** Last modified:    2012-06-18T11:17:03.1351419-07:00</para>
  /// <para>** Template version: 3.110127</para>
  /// ****************************************************
  /// </remarks>  
  [Serializable]
  public abstract partial class usp_AffiliateDriverListForWebSiteDataRow : IdeaBlade.Persistence.Entity, 
     IdeaBlade.Persistence.Rdb.IRdbEntity {
  
    /// <summary>
    /// Initializes a new instance of the usp_AffiliateDriverListForWebSiteDataRow.
    /// Constructs a row from the builder.
    /// <para>** For internal use only. **</para>
    /// </summary>
    /// <param name="pRowBuilder"></param>
    protected usp_AffiliateDriverListForWebSiteDataRow(DataRowBuilder pRowBuilder)
      : base(pRowBuilder) {
    }

    /// <summary>Gets the <see cref="usp_AffiliateDriverListForWebSiteDataTable"/>.</summary>
    [BindingBrowsable(false)]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public new usp_AffiliateDriverListForWebSiteDataTable TypedTable {
      get { return (usp_AffiliateDriverListForWebSiteDataTable) base.Table; }
    }

    /// <summary>Returns the <see cref="System.Type"/> of <see cref="usp_AffiliateDriverListForWebSiteDataTable"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public static Type GetTableType() {
      return typeof(usp_AffiliateDriverListForWebSiteDataTable);
    }

    
    /// <summary>
    /// Gets the SQL 'where' clause used to restrict which 
    /// database records are accessible via this entity. 
    /// </summary>
    /// <remarks>
    /// The default property value is defined in the Current Class 
    /// Properties tab in the <b>IdeaBlade DevForce Object Mapping Tool</b>.
    /// <para>
    /// The 'where' clause will be used, in addition to any other 
    /// query-specific criteria, in every retrieval of entities of this type.
    /// </para>
    /// </remarks>
    [BindingBrowsable(false)]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public string SqlWhereClause {
      get { return ""; }
    }
    
    /// <summary>
    /// Determines whether to use the 'distinct' keyword when 
    /// generating the SQL select clause for objects of this type.
    /// </summary>
    /// <remarks>Defaults to false, but may be overridden in subclasses.</remarks>
    [BindingBrowsable(false)]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public bool ForceSqlDistinct {
      get { return false; }
    }

    #region Enums
    //**************************************
    //* Enums
    //**************************************
    #endregion
	
    #region Relation properties
    //**************************************
    //* Relation properties
    //**************************************
    #endregion
    
    #region EntityColumn definitions
    //**************************************
    //* Static EntityColumn definitions
    //**************************************
    #endregion
    
    #region Properties
    #endregion

    #region usp_AffiliateDriverListForWebSite.StoredProcRdbQuery
    //**************************************
    //* usp_AffiliateDriverListForWebSite.StoredProcRdbQuery
    //**************************************    
    /// <summary>
    /// Subclassed <see cref="T:IdeaBlade.Persistence.Rdb.StoredProcRdbQuery"/> which retrieves usp_AffiliateDriverListForWebSite entities.
    /// </summary>
    [Serializable]
    public class StoredProcRdbQuery : IdeaBlade.Persistence.Rdb.StoredProcRdbQuery {
      /// <summary>Initializes a new instance of the StoredProcRdbQuery class.</summary>
      public StoredProcRdbQuery(

        )
        : base("dbo", "usp_AffiliateDriverListForWebSite", typeof(usp_AffiliateDriverListForWebSite)) {
        RdbParameter aParameter;

      }
    }
    #endregion
  }
  #endregion

  #region EntityPropertyDescriptors.usp_AffiliateDriverListForWebSitePropertyDescriptor
  public static partial class EntityPropertyDescriptors {

    /// <summary>
    /// Gets the usp_AffiliateDriverListForWebSitePropertyDescriptor for <see cref="usp_AffiliateDriverListForWebSite"/>.
    /// </summary>
    public static usp_AffiliateDriverListForWebSitePropertyDescriptor usp_AffiliateDriverListForWebSite {
      get { return usp_AffiliateDriverListForWebSitePropertyDescriptor.Instance; }
    }
    
    /// <summary>
    /// The generated class containing <see cref="usp_AffiliateDriverListForWebSite"/> PropertyDescriptors.
    /// </summary>
    public partial class usp_AffiliateDriverListForWebSitePropertyDescriptor : AdaptedPropertyDescriptor {
	
			/// <summary>
      /// Initializes a new instance of the usp_AffiliateDriverListForWebSitePropertyDescriptor class. 
      /// </summary>
      /// <param name="pParentDescriptor">The parent <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/></param>
      /// <param name="pPropertyPath">The type of the property value</param>
			public usp_AffiliateDriverListForWebSitePropertyDescriptor(AdaptedPropertyDescriptor pParentDescriptor, String pPropertyPath)
        : base(pParentDescriptor, pPropertyPath) {}
		
      /// <summary>
      /// Initializes a new instance of the usp_AffiliateDriverListForWebSitePropertyDescriptor class. 
      /// </summary>
      /// <param name="pComponentType">The type of object to which the property is bound</param>
      protected usp_AffiliateDriverListForWebSitePropertyDescriptor(Type pComponentType)
        : base(pComponentType) {}

     internal static usp_AffiliateDriverListForWebSitePropertyDescriptor Instance = new usp_AffiliateDriverListForWebSitePropertyDescriptor(typeof(usp_AffiliateDriverListForWebSite));
    }
  }
  #endregion
  
}
