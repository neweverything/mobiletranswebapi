﻿using CabRideEngineDapper.Utils;
using Dapper;

namespace CabRideEngineDapper {
	public partial class FareTransaction {

		private string mTransactionStatus;
		[Editable(false)]
		public string TransactionStatus {
			get {
				return mTransactionStatus;
			}
			set {
				mTransactionStatus = value;
			}
		}

		public static FareTransaction GetTransByToken(string pToken) {
			string sql = @"SELECT FareTransaction.*
								 , Status TransactionStatus
							FROM FareTransaction
							LEFT JOIN TransactionStatus ON StatusID = TransactionStatusID
							WHERE Token = @Token";

			using (var conn = SqlHelper.OpenSqlConnection()) {
				return conn.Get<FareTransaction>(sql, new { Token = pToken });
			}
		}
	}
}
