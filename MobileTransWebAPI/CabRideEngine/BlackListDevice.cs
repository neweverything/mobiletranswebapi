using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;
using Csla.Validation;

namespace CabRideEngine {
	/// <summary>
	/// The developer class representing the BlackListDevice business object class.
	/// </summary>
	/// <remarks>
	/// <para>Place all application-specific business logic in this class.</para>
	/// <para>
	/// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
	/// and will not overwrite changes.
	/// </para>
	/// <para>
	/// This class inherits from the generated class, its companion in the pair of classes 
	/// defining a business object. Properties in the generated class may be overridden in 
	/// order to change the behavior or add business logic.
	/// </para>
	/// <para>
	/// This class is the final class in the inheritance chain for this business object. 
	/// It should not be inherited.
	/// </para>
	/// </remarks>
	[Serializable]
	public sealed partial class BlackListDevice : BlackListDeviceDataRow {

		#region Constructors -- DO NOT MODIFY

		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new';
		// it is also required if the class is visible from a web service.
		// DO NOT REMOVE **
		private BlackListDevice() : this(null) { }

		/// <summary>
		/// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
		/// </summary>
		/// <param name="pRowBuilder"></param>
		public BlackListDevice(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static BlackListDevice Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      BlackListDevice aBlackListDevice = pManager.CreateEntity<BlackListDevice>();
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aBlackListDevice, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aBlackListDevice.AddToManager();
		//      return aBlackListDevice;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		// Add additional logic to your business object here...
		#region Standard Entity logic to create/audit/validate the entity
		public static BlackListDevice Create(PersistenceManager pPM) {
			BlackListDevice rec = null;

			try {
				rec = pPM.CreateEntity<BlackListDevice>();
				rec.AddToManager();

			} catch (Exception ex) {
				throw ex;
			}
			return rec;
		}


		[BindingBrowsable(false)]
		public override string EntityInstanceDisplayName {
			get {
				return this.SerialNo;
			}
		}

		protected override void AddRules(RuleList pList) {
			PropertyRequiredRule.AddToList(pList, SerialNoEntityColumn.ColumnName);
			pList.Add(IsSerialNoUnique, SerialNoEntityColumn.ColumnName);
		}

		private bool IsSerialNoUnique(object pTarget, RuleArgs e) {
			BlackListDevice rec = pTarget as BlackListDevice;
			if (string.IsNullOrEmpty(rec.SerialNo)) {
				return false;
			}
			RdbQuery qry = new RdbQuery(typeof(BlackListDevice), SerialNoEntityColumn, EntityQueryOp.EQ, rec.SerialNo);
			qry.AddClause(BlackListDeviceIDEntityColumn, EntityQueryOp.NE, rec.BlackListDeviceID);
			EntityList<BlackListDevice> oList = this.PersistenceManager.GetEntities<BlackListDevice>(qry);
			if (oList.Count > 0) {
				e.Description = "Serial Number already exists";
				return false;
			}
			return true;
		}


		public static BlackListDevice GetDevice(PersistenceManager pPM, string pSerialNo) {
			RdbQuery qry = new RdbQuery(typeof(BlackListDevice), SerialNoEntityColumn, EntityQueryOp.EQ, pSerialNo);
			return pPM.GetEntity<BlackListDevice>(qry, QueryStrategy.DataSourceThenCache);
		}

		#endregion

		public static EntityList<BlackListDevice> GetAll(PersistenceManager pPM) {
			
			
			EntityList<BlackListDevice> oList = pPM.GetEntities<BlackListDevice>();
			//oList.ApplySort(BlackListDevice.CardNoEntityColumn.ColumnName, System.ComponentModel.ListSortDirection.Ascending, false);
			return oList;
		
		}
	}

	#region EntityPropertyDescriptors.BlackListDevicePropertyDescriptor
	////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
	////    For example: 
	////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
	//public static partial class EntityPropertyDescriptors {
	//	public partial class BlackListDevicePropertyDescriptor : BaseEntityPropertyDescriptor {
	//    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
	//    public AdaptedPropertyDescriptor Age {
	//      get { return this.Get("Age"); }
	//    }
	//  }
	//} 
	#endregion

}