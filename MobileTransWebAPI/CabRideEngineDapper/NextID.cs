﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

using Dapper;
using CabRideEngineDapper.Utils;

using TaxiPassCommon;

namespace CabRideEngineDapper {

	public partial class NextID {

		internal static long GetNextKey(string pTable) {
			pTable = pTable.Replace("[", "").Replace("]", "");
			string sql = "SELECT * FROM NextID Where Name = @Name";
			using (var conn = SqlHelper.OpenSqlConnection()) {

				while (true) {
					NextID rec = conn.Query<CabRideEngineDapper.NextID>(sql, new { Name = pTable }).DefaultIfEmpty(CabRideEngineDapper.NextID.Create()).FirstOrDefault();
					if (rec.NextID == 0) {
						rec.NextID++;
					}
					long lastID = rec.NextID;
					rec.NextID++;
					if (rec.Save(conn, lastID)) {
						return lastID;
					}
				}
			}
		}


		public bool Save(SqlConnection pConn, long pLastID) {
			bool result = false;
			CabRideEngineDapper.NextID rec = this;
			if (IsNewRecord) {
				result = pConn.Execute("insert NextID(val) values(@val)", new { rec }) > 0;
			} else {
				string sql = string.Format("UPDATE NextID SET NextID = {0} WHERE NextID = {1} AND Name = '{2}'", NextID, pLastID, Name);
                try {
                    int recs = pConn.Execute(sql) ;
                    result = recs > 0;
                } catch (Exception ex) {
                    Console.WriteLine(ex.Message);
                }
			}
			return result;
		}

	}
}
