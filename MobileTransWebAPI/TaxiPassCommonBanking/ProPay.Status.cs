﻿namespace TaxiPassCommon.Banking {
    public partial class ProPay {

        private StatusInfo GetStatus(string pStatusCode) {
            string status = "";
            string desc = "";
            switch (pStatusCode) {
                case "00":
                    status = "Success";
                    desc = "The API request was successful";
                    break;

                case "20":
                    status = "Invalid username";
                    desc = "The value <userid /> is already in use in ProPay’s system";
                    break;

                case "21":
                    status = "Invalid transType";
                    desc = "Requested < transType /> method is not permitted for account";
                    break;

                case "22":
                    status = "InvalidCurrency Code";
                    desc = "Verify that the value for < currencyCode /> is a valid ISO 3 character currency code";
                    break;

                case "23":
                    status = "Invalid accountType";
                    desc = "Verify the value for < accountType /> element is valid";
                    break;

                case "24":
                    status = "Invalid sourceEmail";
                    desc = "Verify that the value for < sourceEmail /> is valid email format.Value < sourceEmail /> must be unique in ProPay system.";
                    break;

                case "25":
                    status = "Invalid firstName";
                    desc = "The value < firstName /> cannot exceed 20 characters";
                    break;

                case "26":
                    status = "Invalid mInitial";
                    desc = "The value < mInitial /> cannot exceed 2 characters";
                    break;

                case "27":
                    status = "Invalid lastName";
                    desc = "The value < lastName /> cannot exceed 25 characters";
                    break;

                case "28":
                    status = "Invalid billAddr";
                    desc = "Verify the value < billAddr /> is valid.Value cannot exceed 100 characters.P.O.Boxes are not allowed.";
                    break;

                case "29":
                    status = "Invalid aptNum";
                    desc = "Verify the value < aptNum /> is valid.Value cannot exceed 100 characters.";
                    break;

                case "30":
                    status = "Invalid city";
                    desc = "The value < city /> cannot exceed 30 characters";
                    break;

                case "31":
                    status = "Invalid state";
                    desc = "The value < state /> cannot exceed 3 characters.U.S.state abbreviation values should contain only 2 characters.";
                    break;

                case "32":
                    status = "Invalid billZip";
                    desc = "Verify the value for < billZip /> is valid";
                    break;

                case "33":
                    status = "Invalid mailAddr";
                    desc = "Verify the value < mailAddr /> is valid.Value cannot exceed 100 characters.";
                    break;

                case "34":
                    status = "Invalid mailApt";
                    desc = "Verify the value < mailApt /> is valid.Value cannot exceed 100 characters";
                    break;

                case "35":
                    status = "Invalid mailCity";
                    desc = "The value < mailCity /> cannot exceed 30 characters";
                    break;

                case "36":
                    status = "Invalid mailState";
                    desc = "The value < state /> cannot exceed 3 characters.Note: U.S.state abbreviation values should contain only 2 characters.";
                    break;

                case "37":
                    status = "Invalid mailZip";
                    desc = "Verify the value for < mailZip /> is valid";
                    break;

                case "38":
                    status = "Invalid dayPhone";
                    desc = "Verify the value < DayPhone /> is valid.For USA and CAN, value must be 10 characters with no dashes.";
                    break;

                case "39":
                    status = "Invalid evenPhone";
                    desc = "Verify the value < EveningPhone /> is valid.For USA and CAN, value must be 10 characters with no dashes";
                    break;

                case "40":
                    status = "Invalid ssn";
                    desc = "Verify that the value for < ssn /> is valid.Must be 9 numerical characters without dashes.";
                    break;

                case "41":
                    status = "Invalid dob";
                    desc = "Verify that value < dob /> is valid and follows ‘mm - ddyyyy’ format";
                    break;

                case "42":
                    status = "Invalid recEmail";
                    desc = "Verify that the value for < recEmail /> is valid email format";
                    break;

                case "43":
                    status = "Invalid knownAccount";
                    desc = "Value for < knownAccount /> should be “Yes” or “No”";
                    break;

                case "44":
                    status = "Invalid amount";
                    desc = "Verify the value < amount /> is being passed without decimals or other non - numerical characters";
                    break;

                case "45":
                    status = "Invalid invNum";
                    desc = "The value<invNum /> cannot exceed 50 characters";
                    break;

                case "46":
                    status = "Invalid rtNum";
                    desc = "Verify that the value<RoutingNumber /> is valid.  Cannot exceed 9 characters.";
                    break;

                case "47":
                    status = "Invalid accntNum";
                    break;

                case "48":
                    status = "Invalid ccNum";
                    desc = "Verify that the value for < ccNum /> element is valid.  Card numbers must pass Luhn check.";
                    break;

                case "49":
                    status = "Invalid expDate";
                    desc = "Verify that the value for < expDate /> element is valid and follows ‘mmdd’ format.";
                    break;

                case "50":
                    status = "Invalid cvv2";
                    desc = "Verify the value < CVV2 /> is valid.Value should be 3 or 4 characters in length.";
                    break;

                case "51":
                    status = "Invalid transNum AND/OR Unable to act perform actions on transNum due to funding";
                    desc = "Verify that the value < transNum /> is a valid numerical value, OR the requested action cannot be performed on the transaction.";
                    break;

                case "52":
                    status = "Invalid splitNum";
                    break;

                case "53":
                    status = "A ProPay account with this email address already exists AND/OR User has no account number";
                    break;

                case "54":
                    status = "A ProPay account with this social security number already exists";
                    break;

                case "55":
                    status = "The email address provided does not correspond to a ProPay account.";
                    break;

                case "56":
                    status = "Recipient’s email address shouldn’t have a ProPay account and does";
                    break;

                case "57":
                    status = "Cannot settle transaction because it already expired";
                    break;

                case "58":
                    status = "Credit card declined";
                    desc = "The value of the responseCode element will provide info on the decline reason";
                    break;

                case "59":
                    status = "Invalid Credential or IP address not allowed";
                    desc = "Verify that the IP address that you are using has been whitelisted by ProPay.Verify that<certStr /> value is valid";
                    break;

                case "60":
                    status = "Credit card authorization timed out; retry at a later time";
                    desc = "Try again later";
                    break;

                case "61":
                    status = "Amount exceeds single transaction limit";
                    desc = "Value < amount /> exceeds the amount allowed for a single transaction.Contact your relationship manager if you need this increased";
                    break;

                case "62":
                    status = "Amount exceeds monthly volume limit";
                    desc = "The ProPay account has processed the maximum amount allowed in a single month.Contact your relationship manager if you need this limit increased.";
                    break;

                case "63":
                    status = "Insufficient funds in account";
                    desc = "There are not enough funds";
                    break;

                case "64":
                    status = "Over credit card use limit";
                    break;

                case "65":
                    status = "Miscellaneous error";
                    desc = "General error; report the issue to ProPay";
                    break;

                case "66":
                    status = "Denied a ProPay account";
                    desc = "Developer should display a descriptive message that guides a new user to fill out ProPay exceptions form and submit it. See notes below.";
                    break;

                case "67":
                    status = "Unauthorized service requested";
                    desc = "The acting entity or account is not configured to perform the requested action.";
                    break;

                case "68":
                    status = "Account not affiliated";
                    desc = "The action requested requires that the account share the same affiliation as the requesting credential";
                    break;

                case "69":
                    status = "Duplicate invoice number(The same card was charged for the same amount with the same invoice number(including blank invoices) in a 1 minute period.  Details about the original transaction are included whenever a 69 response is returned.These details include a repeat of the auth code, the original AVS response, and the original CVV response.)";
                    desc = "Duplicate < invNum /> element value. (The same card was charged for the same amount with the same invoice number (including blank invoices) in a defined period.Details about the original transaction are included whenever a 69 response is returned.These details include a repeat of the auth code, the original AVS response, and the original CVV response.)";
                    break;

                case "70":
                    status = "Duplicate external ID";
                    desc = "< ExternalId /> values must be unique per affiliation";
                    break;

                case "71":
                    status = "Account previously set up, but problem affiliating it with partner";
                    break;

                case "72":
                    status = "The ProPay Account has already been upgraded to a Premium Account";
                    break;

                case "73":
                    status = "Invalid Destination Account";
                    desc = "Verify that the value<RecAccntNum /> is a valid ProPay account.";
                    break;

                case "74":
                    status = "Account or Trans Error";
                    break;

                case "75":
                    status = "Money already pulled";
                    desc = "The funds have already been pulled";
                    break;

                case "76":
                    status = "Not Premium(used only for push / pull transactions)";
                    break;

                case "77":
                    status = "Empty results";
                    break;

                case "78":
                    status = "Invalid Authentication";
                    desc = "Authentication credentials are not valid";
                    break;

                case "79":
                    status = "Generic account status error";
                    desc = "Contact ProPay Customer Service";
                    break;

                case "80":
                    status = "Invalid Password";
                    desc = "Password is not valid";
                    break;

                case "81":
                    status = "Account Expired";
                    desc = "The ProPay account has expired.Contact your relationship manager";
                    break;

                case "82":
                    status = "Invalid UserID";
                    desc = "The value < userId /> is not a valid ProPay userId";
                    break;

                case "83":
                    status = "Batch Trans Count Error";
                    break;

                case "84":
                    status = "Invalid Begin Date";
                    desc = "Verify the value <beginDate />. Must follow format ‘mmdd - yyyy’. BeginDate cannot be greater than EndDate.";
                    break;

                case "85":
                    status = "InvalidEndDate";
                    desc = "Verify the value <endDate />.  Must follow format ‘mm - ddyyyy’. EndDate cannot be greater than BeginDate.";
                    break;

                case "86":
                    status = "InvalidExternalID";
                    break;

                case "87":
                    status = "DuplicateUserID";
                    desc = "Developer should display descriptive response that the email is already in use. See notes below.";
                    break;

                case "88":
                    status = "Invalid track 1";
                    break;

                case "89":
                    status = "Invalid track 2";
                    break;

                case "90":
                    status = "Transaction already refunded";
                    desc = "The transaction has already been refunded and cannot be refunded again";
                    break;

                case "91":
                    status = "Duplicate Batch ID";
                    break;

                case "92":
                    status = "Duplicate Batch Transaction";
                    break;

                case "93":
                    status = "Batch Transaction amount error";
                    break;

                case "94":
                    status = "Unavailable Tier";
                    desc = "The value<tier /> is not available for signup.Verify the previously assigned tiers.";
                    break;

                case "95":
                    status = "Invalid Country Code";
                    desc = "The value < country /> is not valid. Value should be ISO 3166 standard 3 character country codes.";
                    break;

                case "96":
                    status = "Invalid PIN";
                    break;

                case "97":
                    status = "Account created in documentary status";
                    desc = "The account must be validated to be activated";
                    break;

                case "98":
                    status = "Account created in documentary status, and must be paid for";
                    desc = "The account must be validated and paid for to be activated";
                    break;

                case "99":
                    status = "Account created successfully, but must be paid for.";
                    desc = "The account must be paid for to be activated";
                    break;

                case "100":
                    status = "Transaction Already Refunded";
                    desc = "The transaction has already been refunded and cannot be refunded again";
                    break;

                case "101":
                    status = "Refund Exceeds Original Transaction";
                    desc = "The refund amount is greater than the original transaction amount.";
                    break;

                case "102":
                    status = "Invalid Payer Name";
                    desc = "The value < payerName /> is not valid.";
                    break;

                case "103":
                    status = "Transaction does not meet date criteria";
                    break;

                case "104":
                    status = "Transaction could not be refunded due to current transaction state.";
                    break;

                case "105":
                    status = "Direct deposit account not specified";
                    break;

                case "106":
                    status = "Invalid SEC code";
                    desc = "Verify that the value < StandardEntryClassCode /> is valid.Cannot exceed 3 characters.Valid values are: CCD, PPD.";
                    break;

                case "107":
                    status = "Invalid Account Name (ACH account)";
                    desc = "Verify that the value < accountName /> is valid.  Cannot exceed 32 characters.";
                    break;

                case "108":
                    status = "Invalid x509 certificate";
                    break;

                case "109":
                    status = "Invalid value for require CC refund";
                    desc = "The value for < requireCCRefund /> is not valid. Value should be Y or N";
                    break;

                case "110":
                    status = "Required field is missing";
                    desc = "Returned for edit ProPay account.  See Response element for field name Returned if account edit was attempted on an account not belonging to the affiliation.";
                    break;

                case "111":
                    status = "Invalid EIN";
                    desc = "Verify the <EIN /> element value is valid";
                    break;

                case "112":
                    status = "Invalid business legal name(DBA)";
                    desc = "< BusinessLegalName /> is required for business account type signups.";
                    break;

                case "113":
                    status = "One of the business legal address fields is invalid";
                    break;

                case "114":
                    status = "Business(legal) city is invalid";
                    desc = "The value < BusinessCity /> cannot exceed 30 characters";
                    break;

                case "115":
                    status = "Business(legal) state is invalid";
                    desc = "The value < BusinessState /> cannot exceed 3 characters.  For a U.S.state, follow standard 2 character abbreviation.";
                    break;

                case "116":
                    status = "Business(legal) zip is invalid";
                    desc = "Verify that the value < BusinessZip /> is valid.Cannot exceed 9 characters.";
                    break;

                case "117":
                    status = "Business(legal) country is invalid";
                    desc = "The value < BusinessCountry /> cannot exceed 3 characters.  Should follow standard 3 character country code format.";
                    break;

                case "118":
                    status = "Mailing address invalid";
                    break;

                case "119":
                    status = "Business(legal) address is invalid";
                    break;

                case "120":
                    status = "Incomplete business address";
                    break;

                case "121":
                    status = "Amount Encumbered by enhanced Spendback";
                    break;

                case "122":
                    status = "Invalid encrypting device type";
                    desc = "Verify that the value < encryptingDeviceType /> is valid";
                    break;

                case "123":
                    status = "Invalid key serial number";
                    desc = "Verify that the value < keySerialNumber /> is valid.  Value is obtained from the hardware device.";
                    break;

                case "124":
                    status = "Invalid encrypted track data";
                    desc = "Verify that the value < encryptedTrackData /> is valid";
                    break;

                case "125":
                    status = "You may not transfer money between these two accounts.";
                    break;

                case "126":
                    status = "Currency code not allowed for this transaction";
                    desc = "Value < currencyCode /> must be an allowed currency for the merchant account";
                    break;

                case "127":
                    status = "Currency code not permitted for this account";
                    desc = "Value < currencyCode /> must be an allowed currency for the merchant account";
                    break;

                case "128":
                    status = "Requires Additional Validation";
                    break;

                case "129":
                    status = "Multicurrency processing is not allowed for the account";
                    desc = "This account cannot process additional currencies";
                    break;

                case "130":
                    status = "Multicurrency processing is not supported for this bank processor";
                    break;

                case "131":
                    status = "Capture amount exceeds allowed amount";
                    desc = "Capture < amount /> value cannot be more than the initial authorization amount";
                    break;

                case "132":
                    status = "Account setup does not allow capture for amount greater than authorization";
                    desc = "Capture < amount /> value cannot be more than the initial authorization amount";
                    break;

                case "133":
                    status = "Threat Metrix risk denied(no responseCode is returned)";
                    break;

                case "134":
                    status = "Threat Metrix Invalid SessionId";
                    break;

                case "135":
                    status = "Threat Metrix Invalid Account configuration";
                    desc = "Contact ProPay Risk Department";
                    break;

                case "136":
                    status = "External Payment Method Not Provided";
                    break;

                case "137":
                    status = "External Payment Provider not provided";
                    break;

                case "138":
                    status = "External Payment Identifier not provided";
                    break;

                case "139":
                    status = "External Payment Provider not valid";
                    break;

                case "140":
                    status = "External Payment Method Provided";
                    break;

                case "141":
                    status = "Inactive or blocked MCC Code";
                    desc = "Unable to use passed value < MCCCode /> due to the MCC Code being blocked or inactive with the card brands";
                    break;

                case "142":
                    status = "Invalid MCC Code";
                    desc = "Non - numeric or not in ProPay® database Verify that the value < MCCCode /> is valid.  Value cannot exceed 4 characters.  Value must be numerical.";
                    break;

                case "143":
                    status = "Gross settle: invalid credit card information";
                    desc = "Verify the value < GrossSettleCreditCardNumber /> is valid.  Cannot exceed 16 characters and must pass Luhn check.";
                    break;

                case "144":
                    status = "Gross settle: invalid billing information";
                    break;

                case "145":
                    status = "Gross settle: no billing information was included with the payment info";
                    break;


                case "146":
                    status = "Gross settle: error setting up billing information";
                    break;

                case "147":
                    status = "Gross settle: Tier does not support gross settlement";
                    desc = "The tier provided as < tier /> element has not been configured to support gross settlement";
                    break;

                case "148":
                    status = "ExternalPaymentMethodIdentifier Invalid";
                    desc = "Verify that the value<externalPaymentMethodIdentifier/> is valid.  Must be a valid Visa Checkout payment identifier.";
                    break;

                case "149":
                    status = "InvalidDoingBusinessAs";
                    desc = "Verify <DoingBusinessAs /> element value is valid.";
                    break;

                case "150":
                    status = "Invalid Service Setting";
                    desc = "One or more request tags contain invalid data.";
                    break;

                case "151":
                    status = "Amex Enhanced Account Not Configured";
                    break;
            }

            return new StatusInfo() {
                Status = status,
                Desc = desc
            };

        }

        public class StatusInfo {
            public string Status { get; set; }
            public string Desc { get; set; }
        }
    }
}
