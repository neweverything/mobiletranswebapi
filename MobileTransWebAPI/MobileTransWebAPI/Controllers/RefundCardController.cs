﻿using CabRideEngine;
using MobileTransWebAPI.Models;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Web.Http;
using TaxiPassCommon;

namespace MobileTransWebAPI.Controllers {

	public class RefundCardController : ApiController {
		// GET api/refundcard
		public RefundResult Get() {
			RefundResult result = new RefundResult();


			RefundInfo info = JsonConvert.DeserializeObject<RefundInfo>(File.ReadAllText(@"d:\temp\refund.json"));
			result = Post(info);

			return result;
		}


		public RefundResult Post([FromBody]RefundInfo refundInfo) {
			RefundResult result = new RefundResult();


			if (Properties.Settings.Default.UseChargeCardDapper) {
				return new RefundCardDapperController().Post(refundInfo);
			}

			CabRideEngineDapper.Accounts oAcct;
			if (refundInfo.AccountNo.IsNullOrEmpty()) {
				result.ErrorMsg = "Account No is required";
				return result;
			} else {
				if (refundInfo.AccountNo != "SKIP ACCT NO CHECK") {
					oAcct = CabRideEngineDapper.Accounts.GetAccount(refundInfo.AccountNo);
					if (oAcct.IsNullEntity) {
						result.ErrorMsg = "Unknown account";
						return result;
					}
				}
			}

			//LogIt("Refund Data", JsonConvert.SerializeObject(pInfo));

			try {
				// change amount to be 2 decimal places
				refundInfo.RefundAmount = Convert.ToDecimal(refundInfo.RefundAmount.ToString("F2"));

				var oPM = WebApiApplication.UserPM;
				if (oPM == null) {
					result.ErrorMsg = "Could not load database manager";
					return result;
				}

				if (refundInfo.OrigVoucherNo.IsNullOrEmpty()) {
					result.ErrorMsg = "Please provide the original voucher number";
					return result;
				}


				DriverPayments oOrigTrans = DriverPayments.GetPayment(oPM, refundInfo.OrigVoucherNo);
				if (oOrigTrans.IsNullEntity) {
					result.ErrorMsg = "Could not find " + refundInfo.OrigVoucherNo;
					return result;
				}

				if (refundInfo.RefundReason.IsNullOrEmpty()) {
					result.ErrorMsg = "Please provide a reason for the refund";
					return result;
				}

				if (oOrigTrans.ChargeBack || oOrigTrans.Fare < 0) {
					result.ErrorMsg = "Cannot refund a refunded transaction";
					return result;
				}


				decimal refundedAmount = Math.Abs(CabRideEngineDapper.DriverPaymentRef.GetRefundedAmount(refundInfo.OrigVoucherNo));
				if (oOrigTrans.DriverTotal - refundedAmount < refundInfo.RefundAmount) {
					if (refundedAmount > 0) {
						result.ErrorMsg = string.Format("Voucher has already been refunded for {0}, max refund: {1}", refundedAmount.ToString("C"), (oOrigTrans.DriverTotal - refundedAmount).ToString("C"));
					} else {
						result.ErrorMsg = string.Format("Max refund allowed: {1}", refundedAmount.ToString("C"), (oOrigTrans.DriverTotal - refundedAmount).ToString("C"));
					}
					return result;
				}



				var oAffiliate = Affiliate.GetAffiliate(oPM, oOrigTrans.AffiliateID.Value);

				var oRefundTrans = DriverPayments.Create(oOrigTrans.Drivers);
				oRefundTrans.FareNoTaxiPassFee = -Math.Abs(refundInfo.RefundAmount);

				//need to calc TPFee 
				if (!refundInfo.DoNotRefundServiceFee) {
					decimal nTotal = oOrigTrans.DriverTotal + oRefundTrans.Fare;
					decimal nTPFee = DriverPayments.CalcFee(oAffiliate, nTotal);
					if (oOrigTrans.TaxiPassFee == 0) {
						oRefundTrans.TaxiPassFee = 0;
					} else if (oOrigTrans.DriverTotal == Math.Abs(oRefundTrans.DriverTotal)) {
						oRefundTrans.TaxiPassFee = -oOrigTrans.TaxiPassFee;
					} else {
						oRefundTrans.TaxiPassFee = -(oOrigTrans.TaxiPassFee - nTPFee);
					}
				}

				oRefundTrans.CardNumber = oOrigTrans.DecryptCardNumber();
				oRefundTrans.Expiration = oOrigTrans.DecryptCardExpiration();
				oRefundTrans.AffiliateDriverID = oOrigTrans.AffiliateDriverID;
				oRefundTrans.AffiliateID = oOrigTrans.AffiliateID;
				oRefundTrans.CardSwiped = oOrigTrans.CardSwiped;
				if (oRefundTrans.DriverPaymentNotes.IsNullEntity) {
					DriverPaymentNotes.Create(oRefundTrans);
				}


				oRefundTrans.DriverPaymentNotes.Reason = refundInfo.RefundReason;
				oRefundTrans.VehicleID = oOrigTrans.VehicleID;
				oRefundTrans.ChargeBack = true;

				//if (!oRefundTrans.DriverPaymentNotes.Reason.IsNullOrEmpty()) {
				//    oRefundTrans.ChargeBackInfo = oRefundTrans.DriverPaymentNotes.Reason;
				//    if (oRefundTrans.ChargeBackInfo.Length > 200) {
				//        oRefundTrans.ChargeBackInfo = StringUtils.Left(oRefundTrans.ChargeBackInfo, 200);
				//    }
				//}
				oRefundTrans.ChargeBackInfo = string.Format("Refund of Voucher No: {0}  Refund Amount: {1} - {2} ", refundInfo.OrigVoucherNo, refundInfo.RefundAmount.ToString("C"), refundInfo.RefundReason).PadRight(200).Left(200).Trim();

				oRefundTrans.CorporateName = oOrigTrans.CorporateName;
				oRefundTrans.ReferenceNo = oOrigTrans.ReferenceNo;

				TimeZoneConverter oTZ = new TimeZoneConverter();
				oRefundTrans.ChargeDate = oTZ.ConvertTime(DateTime.Now, TimeZone.CurrentTimeZone.StandardName, oOrigTrans.Affiliate.TimeZone);
				oRefundTrans.DriverPaid = true;
				oRefundTrans.DriverPaidDate = oRefundTrans.ChargeDate;

				oRefundTrans.CreditCardProcessor = oOrigTrans.CreditCardProcessor;

				DriverPaymentResults oResult = null;
				try {
					//lock (thisLock) {

					oResult = oRefundTrans.RefundCard(oOrigTrans.DriverPaymentChargeResults.Reference, oOrigTrans.TransNo);
					if (oResult.Result == "0") {
						oRefundTrans.Save("MobileTrans.RefundCard");
						result.NewVoucherNo = oRefundTrans.TransNo;
						result.TPFee = oRefundTrans.TaxiPassFee;
						result.VoucherNo = oOrigTrans.TransNo;
						result.RefundDate = oRefundTrans.ChargeDate.Value;
						result.DriverPaymentID = oRefundTrans.DriverPaymentID;
						result.CardNo = oRefundTrans.CardNumberDisplay;
						result.DriverTotal = oRefundTrans.DriverTotal;
						result.IsTestCard = oRefundTrans.Test;
						result.SoftDescriptor = oRefundTrans.SoftDescriptor;
					} else {
						result.ErrorMsg = "Problem creating refund: " + oResult.Message;
					}

				} catch (Exception ex) {
					string sErrorEmailRecipient = System.Configuration.ConfigurationManager.AppSettings["ErrorEmailRecipient"];
					if (!String.IsNullOrEmpty(sErrorEmailRecipient)) {
						String sHtml = "";
						sHtml += "<hr noshade color=#000000 size=1>";
						sHtml += DateTime.Now.ToString() + " Card Refund Exception error<br>";
						sHtml += "<br>** Error Info **<br>";
						sHtml += "Page: MobileTransWebApi.RefundCard<br>";
						sHtml += "Message: " + ex.Message + "<br>";
						sHtml += "Error Affiliate: " + oAffiliate.Name + "<br>";

						try { // error may happen on page where Request object doesn't exist contextually
							sHtml += "<br>** Request **<br>" + ObjectDisplayer.ObjectToString(refundInfo) + "<br>";
						} catch {
						}

						sHtml += "<br>Stack Trace: " + ex.StackTrace + "<br>";


						// send email if setting is configured in web.config
						try {
							string subject = "MobileTransactions Refund Card Error";
							var cred = CabRideEngineDapper.SystemDefaultsDict.GetEmailCreds(CabRideEngineDapper.Enums.EMailReportFrom.Report);
							TaxiPassCommon.Mail.EMailMessage oMail = new TaxiPassCommon.Mail.EMailMessage(cred, subject, sHtml);
							oMail.UseWebServiceToSendMail = Properties.Settings.Default.SendEmailViaWebService;
							oMail.SendMail(sErrorEmailRecipient);
						} catch {
						}
					}
					result.ErrorMsg = "Problem refunding: " + ex.Message;
					return result;
				}
				if (oResult.Result != "0") {
					result.ErrorMsg = oResult.Message;
					return result;
				}


				oOrigTrans.DriverPaymentNotes.Reason = "See Refund: Trans No: " + oRefundTrans.TransNo + " Date: " + oRefundTrans.MyChargeDateTime.Value.ToString("MM/dd/yyyy") + "\n\n" + oOrigTrans.DriverPaymentNotes.Reason;
				try {
					oOrigTrans.Platform = Platforms.WebTerminal.ToString();
					oOrigTrans.Save("MobileTrans.RefundCard");

				} catch {
				}


			} catch (Exception exUnknown) {
				result.ErrorMsg = exUnknown.Message;
				Utils.EmailErrors.SendErrorEmail("MobileTransWebApi Error: RefundCard", exUnknown.Message + Environment.NewLine + "Input: " + JsonConvert.SerializeObject(refundInfo));
			}

			return result;
		}


	}
}
