﻿using Dapper;
using System.Data.SqlClient;

namespace CabRideEngineDapper {
    public partial class RedeemerShiftVouchers {

        public static RedeemerShiftVouchers GetShiftVoucher(SqlConnection pConn, long pDriverPaymentID) {
            string sql = @"SELECT * FROM RedeemerShiftVouchers
                            WHERE DriverPaymentID = @DriverPaymentID";

            return pConn.Get<RedeemerShiftVouchers>(sql, new { DriverPaymentID = pDriverPaymentID });
        }
    }
}
