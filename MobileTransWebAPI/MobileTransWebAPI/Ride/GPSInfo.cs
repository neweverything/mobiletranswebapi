﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace RideService {

#if !WINDOWS_PHONE
	[System.Reflection.Obfuscation(Exclude = true, ApplyToMembers = true)]
#endif
	public partial class GPSInfo {
		public String Key;
		public long DriverID { get; set; }
		public long AffiliateDriverID { get; set; }
		public long AffiliateID { get; set; }
		public decimal Latitude { get; set; }
		public decimal Longitude { get; set; }
		public string Address { get; set; }
		public string ZipCode { get; set; }
		public float Speed { get; set; }
		public string GpsTime { get; set; }
		public decimal UTCOffset { get; set; }

		public int LatLonAccuracy { get; set; }

		public string MeterStatus { get; set; }

		public String Method { get; set; }

		public string PassengerToken { get; set; }
		public long CustomerDeviceID { get; set; }
		public string Message { get; set; }

		//public Dictionary<string, string> Settings { get; set; }
		private List<string> mSettings = new List<string>();
		public List<string> Settings {
			get {
				return mSettings;
			}
			set {
				mSettings = value;
			}
		}

		// Error info
		public int ErrorNo { get; set; }
		public string ErrorMsg { get; set; }

		public string DriverStatus { get; set; }
		public bool JobInProgress { get; set; }

		//public GPSInfo() {
		//	Settings = new List<string>();
		//}

		// *******************************************************************************************
		// Used in DriverService.LoadDriverDetails
		// *******************************************************************************************
		public static GPSInfo NewGPSInfo(decimal pLat, decimal pLong, string pGPSTime, int pTier) {
			GPSInfo oGPSInfo = new GPSInfo();
			oGPSInfo.Latitude = pLat;
			oGPSInfo.Longitude = pLong;
			oGPSInfo.GpsTime = pGPSTime;
			oGPSInfo.Settings = new List<string>();
			oGPSInfo.Settings.Add("Tier|" + pTier);
			return oGPSInfo;
		}
	}


}