﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CabRideEngineDapper {
	public class HandHeldPrinterTypes {
		private string mCode;
		private string mDescription;

		private bool mIsNullEntity = false;

		private static List<HandHeldPrinterTypes> msEntities;
		private static HandHeldPrinterTypes msNullEntity;

		public enum HandHeldPrinterTypesEnum {
			AMTech,
			Centrodyne,
			BlueBamboo,
			Woosim,
			NoPrinter,
			HomeATM,
			IDTech
		}

		static HandHeldPrinterTypes() {

			// Create the null entity
			msNullEntity = new HandHeldPrinterTypes("", "");
			msNullEntity.mIsNullEntity = true;

			// Populate list of Routing Types.
			msEntities = new List<HandHeldPrinterTypes>();
			msEntities.Add(new HandHeldPrinterTypes(Convert.ToInt16(HandHeldPrinterTypesEnum.AMTech).ToString(), "AM Tech Basic"));
			msEntities.Add(new HandHeldPrinterTypes(Convert.ToInt16(HandHeldPrinterTypesEnum.BlueBamboo).ToString(), "Blue Bamboo"));
			msEntities.Add(new HandHeldPrinterTypes(Convert.ToInt16(HandHeldPrinterTypesEnum.Centrodyne).ToString(), "Centrodyne Meter"));
			msEntities.Add(new HandHeldPrinterTypes(Convert.ToInt16(HandHeldPrinterTypesEnum.HomeATM).ToString(), "HomeATM"));
			msEntities.Add(new HandHeldPrinterTypes(Convert.ToInt16(HandHeldPrinterTypesEnum.IDTech).ToString(), "IDTech"));
			msEntities.Add(new HandHeldPrinterTypes(Convert.ToInt16(HandHeldPrinterTypesEnum.Woosim).ToString(), "Woosim"));
			msEntities.Add(new HandHeldPrinterTypes(Convert.ToInt16(HandHeldPrinterTypesEnum.NoPrinter).ToString(), "No Printer/Swipe"));
		}

		private HandHeldPrinterTypes(string pCode, string pDescription) {
			mCode = pCode;
			mDescription = pDescription;
		}

		/// <summary>Get the CellPhoneDataEntry null entity.</summary>
		public static HandHeldPrinterTypes NullEntity {
			get {
				return msNullEntity;
			}
		}

		/// <summary>Get the list of all hand Held Printer Types.</summary>
		public static List<HandHeldPrinterTypes> GetAll() {
			return msEntities;
		}

		/// <summary>Get the HandHeldPrinterType by Code.</summary>
		public static HandHeldPrinterTypes GetById(string pCode) {
			foreach (HandHeldPrinterTypes oRec in msEntities) {
				if (oRec.Code == pCode) {
					return oRec;
				}
			}
			return NullEntity;
		}

		/// <summary>Return true if this is the null entity</summary>
		//[BindingBrowsable(false)]
		public bool IsNullEntity {
			get {
				return mIsNullEntity;
			}
		}

		/// <summary>Get the Code</summary>
		public string Code {
			get {
				return mCode;
			}
		}

		public string Description {
			get {
				return mDescription;
			}
		}

	}
}
