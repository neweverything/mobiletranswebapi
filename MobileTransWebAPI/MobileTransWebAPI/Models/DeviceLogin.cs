﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using TaxiPassCommon;

namespace MobileTransWebAPI.Controllers {
	public class DeviceLogin {

		public String SerialNo = "";
		public String AppVersion = "";
		public String Phone = "";

		public String Manufacturer = "";
		public String Model = "";
		public String OSVersion = "";
		public String CarrierName = "";

		public String CurrentTime = "";

		public DateTime GetTime() {
			return CurrentTime.ToDateTime(); 
		}

		public string PushToken { get; set; }		// Push messaging identifier
		public string PhoneType { get; set; }		// Android, iPhone, etc.
		public string CertLocation { get; set; }	// iPhone certificate location (AppStore, Enterprise, Sandbox)
		public long AffiliateID { get; set; }		// allow app to set the AffiliateID 

		public string AppName { get; set; }			// App Name, used by Version to determine which version number should be returned

		public string UserID { get; set; }			// Used for new login process
		public string UserPwd { get; set; }

		[Newtonsoft.Json.JsonIgnore]
		public double AppVersionAsDouble {
			get {
				List<string> temp = AppVersion.Split('.').ToList();
				while (temp.Count > 2) {
					temp.RemoveAt(temp.Count - 1);
				}
				while (!temp[0].Left(1).IsNumeric()) {
					temp[0] = temp[0].Substring(1);
				}
				return Convert.ToDouble(string.Join(".", temp.ToArray()), System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
			}
		}
	}
}