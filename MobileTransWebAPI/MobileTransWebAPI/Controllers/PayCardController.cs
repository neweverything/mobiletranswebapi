﻿using CabRideEngineDapper;
using CabRideEngineDapper.Utils;
using MobileTransWebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Script.Serialization;
using TaxiPassCommon;

namespace MobileTransWebAPI.Controllers {

    public class PayCardController : ApiController {


        //public PayCardResult Get() {
        //    PayCardInfo info = new PayCardInfo() {
        //        AffiliateDriverID = 105664,
        //        DriverID = 76281,
        //        TransferAmount = 0,
        //        RequestKey = DateTime.UtcNow.ToString()
        //    };

        //    var result = GetPayCardInfo(JsonConvert.SerializeObject(info));
        //    return result;
        //}


        public PayCardResult GetPayCardInfo(string pRequest) {
            JavaScriptSerializer js = new JavaScriptSerializer();
            PayCardResult result = new PayCardResult();

            Models.PayCardInfo info = js.Deserialize<Models.PayCardInfo>(pRequest.DecryptIceKey());
            WebApiApplication.LogIt("GetPayCardInfo", pRequest);

            DateTime requestDate = Convert.ToDateTime(info.RequestKey.DecryptIceKey());
            if (DateTime.Now.ToUniversalTime().Subtract(requestDate).TotalMinutes > 1) {
                result.ErrorMsg = "Invalid Access";
                return result;
            }

            using (var conn = SqlHelper.OpenSqlConnection()) {

                Drivers driver = Drivers.GetDriver(conn, info.DriverID);
                AffiliateDrivers affDriver = AffiliateDrivers.GetDriver(conn, info.AffiliateDriverID);

                if (driver.Cell != affDriver.Cell && driver.Cell != affDriver.CallInCell) {
                    result.ErrorMsg = "Access denied";
                    return result;
                }

                if (affDriver.TransCardCardNumber.IsNullOrEmpty()) {
                    result.ErrorMsg = "Driver does not have a Pay Card";
                    return result;
                }

                var achPaidCnt = usp_FundPayCardInfo.GetVoucherCountForPayCard(conn, affDriver.TransCardAdminNo);
                result.AvailableTransfers = achPaidCnt.PayCardLoadsPerMonth - achPaidCnt.AchPaidCount;
                result.VoucherTotal = achPaidCnt.DriverTotal;
                result.VoucherCount = achPaidCnt.VoucherCount;
                //result.DisplayFundingMsgAt = achPaidCnt.DisplayFundingMsgAt;
                //result.FundingAcctAmt = achPaidCnt.FundingAcctAmt;

                //List<ACHDetail> batchList = ACHDetail.GetTransCardsWaitingToBatch(conn, affDriver.TransCardAdminNo);
                //List<DriverPayments> canPayList = DriverPayments.GetPaymentsForDriverInitiatedProcess(affDriver);

                //foreach (ACHDetail rec in batchList) {
                //    if (rec.DriverPayment.RedemptionFee == 0) {
                //        rec.DriverPayment.RedemptionFee = rec.DriverPayment.Affiliate.AffiliateDriverDefaults.DriverInitiatedRedemptionFee;
                //    }
                //}
                //foreach (DriverPayments rec in canPayList) {
                //    if (rec.RedemptionFee == 0) {
                //        rec.RedemptionFee = rec.Affiliate.AffiliateDriverDefaults.DriverInitiatedRedemptionFee;
                //    }
                //}
                //result.VoucherTotal = batchList.Sum(p => p.DriverPayment.DriverTotalWithDriverFee) + canPayList.Sum(p => p.DriverTotalWithDriverFee);
                //result.BalanceToTransfer = batchList.Sum(p => p.DriverPayment.DriverTotalPaid) + canPayList.Sum(p => p.DriverTotalPaid);
                result.CardNumber = affDriver.TransCardCardNumber;
                result.CurrentCardBalance = affDriver.GetPayCardBalance(conn);

                //result.VoucherCount = batchList.Count + canPayList.Count;
                result.TransferFee = affDriver.Affiliate.AffiliateDriverDefaults.DriverInitiatedRedemptionFee;

                //always return number of transfers available
                //if (result.BalanceToTransfer > 0) {
                //check number of times ach was processed for the week
                DateTime startOfWeek = DateUtils.GetStartOfCurrentWeek();
                DateTime endOfWeek = DateUtils.GetEndOfCurrentWeek();
                TimeZoneConverter tzConvert = new TimeZoneConverter();
                DateTime endDate = tzConvert.ConvertTime(DateTime.Now, TimeZone.CurrentTimeZone.StandardName, affDriver.Affiliate.TimeZone);

                List<long> paidBatchList = ACHDetail.GetDistinctBatchNumbersForTransCardAlreadyBatched(affDriver, startOfWeek, endDate);
                int batchCount = paidBatchList.Count;
                int batchMaxCount = SystemDefaults.GetDefaults(conn).DriverInitiatedMaxPayCardTransfersPerWeek;
                //check if any holidays and reduce count
                batchMaxCount -= BankHoliday.GetByDateRange(conn, startOfWeek, endOfWeek).Count;

                result.AvailableTransfers = batchMaxCount - batchCount;
                if (result.AvailableTransfers < 0) {
                    result.AvailableTransfers = 0;
                }
                //}
            }

            return result;
        }


        public PayCardResult PostPayCardTransfer(string pRequest) {
            JavaScriptSerializer js = new JavaScriptSerializer();
            PayCardResult result = new PayCardResult();

            Models.PayCardInfo info = js.Deserialize<Models.PayCardInfo>(pRequest.DecryptIceKey());
            WebApiApplication.LogIt("DoPayCardTransfer", pRequest);

            DateTime requestDate = DateTime.Today.AddDays(-1);
            if (info != null) {
                requestDate = Convert.ToDateTime(info.RequestKey.DecryptIceKey());
            }

            if (DateTime.Now.ToUniversalTime().Subtract(requestDate).TotalMinutes > 1) {
                result.ErrorMsg = "Invalid Access";
                return result;
            }

            using (var conn = SqlHelper.OpenSqlConnection()) {

                Drivers driver = Drivers.GetDriver(conn, info.DriverID);
                AffiliateDrivers affDriver = AffiliateDrivers.GetDriver(conn, info.AffiliateDriverID);

                if (driver.Cell != affDriver.Cell && driver.Cell != affDriver.CallInCell) {
                    result.ErrorMsg = "Access denied";
                    return result;
                }

                if (affDriver.TransCardCardNumber.IsNullOrEmpty()) {
                    result.ErrorMsg = "Driver does not have a Pay Card";
                    return result;
                }

                if (affDriver.RedeemerStopPayment) {
                    result.ErrorMsg = "Please call 1-888-TAXI-PAY and press 0# to get paid for these vouchers";
                    return result;
                }

                List<ACHDetail> batchList = ACHDetail.GetTransCardsWaitingToBatch(conn, affDriver.TransCardAdminNo);
                List<DriverPayments> canPayList = DriverPayments.GetPaymentsForDriverInitiatedProcess(affDriver);
                foreach (ACHDetail rec in batchList) {
                    if (rec.DriverPayment.RedemptionFee.GetValueOrDefault(0) == 0) {
                        rec.DriverPayment.RedemptionFee = rec.DriverPayment.Affiliate.AffiliateDriverDefaults.DriverInitiatedRedemptionFee;
                    }
                }
                foreach (DriverPayments rec in canPayList) {
                    if (rec.RedemptionFee.GetValueOrDefault(0) == 0) {
                        rec.RedemptionFee = rec.Affiliate.AffiliateDriverDefaults.DriverInitiatedRedemptionFee;
                    }
                }


                result.BalanceToTransfer = batchList.Sum(p => p.DriverPayment.DriverTotalPaid) + canPayList.Sum(p => p.DriverTotalPaid);
                result.CardNumber = affDriver.TransCardCardNumber;

                if (result.BalanceToTransfer != info.TransferAmount) {
                    result.ErrorMsg = "Invalid transfer amount";
                    return result;
                }

                if (result.BalanceToTransfer > 0) {
                    //check number of times ach was processed for the week
                    DateTime startOfWeek = DateUtils.GetStartOfCurrentWeek();
                    DateTime endOfWeek = DateUtils.GetEndOfCurrentWeek();
                    TimeZoneConverter tzConvert = new TimeZoneConverter();
                    DateTime endDate = tzConvert.ConvertTime(DateTime.Now, TimeZone.CurrentTimeZone.StandardName, affDriver.Affiliate.TimeZone);

                    List<long> paidBatchList = ACHDetail.GetDistinctBatchNumbersForTransCardAlreadyBatched(affDriver, startOfWeek, endDate);
                    int batchCount = paidBatchList.Count;
                    int batchMaxCount = SystemDefaults.GetDefaults(conn).DriverInitiatedMaxPayCardTransfersPerWeek;
                    //check if any holidays and reduce count
                    batchMaxCount -= BankHoliday.GetByDateRange(conn, startOfWeek, endOfWeek).Count;

                    result.AvailableTransfers = batchMaxCount - batchCount;
                    if (result.AvailableTransfers < 0) {
                        result.AvailableTransfers = 0;
                    }
                }
                if (result.AvailableTransfers == 0) {
                    result.ErrorMsg = "You do not have any transfers available this week";
                    return result;
                }


                //TaxiPassCommon.Banking.TransCard.LOAD_CARD_FUNDED_RET fundResult = affDriver.TransferWaitingToBatchToPayCard(false);
                //if (fundResult.ERROR_FOUND.StartsWith("N", StringComparison.CurrentCultureIgnoreCase)) {
                //    result.ErrorMsg = "";
                //    result.AvailableTransfers--;
                //    result.BalanceToTransfer = 0;
                //    result.AmountTransferred = info.TransferAmount;
                //    result.CurrentCardBalance = Convert.ToDecimal(fundResult.CURRENT_CARD_BALANCE.Replace("$", ""));
                //} else {
                //    result.ErrorMsg = fundResult.ERROR_MESSAGE;
                //}
            }

            string retVal = js.Serialize(result);
            WebApiApplication.LogIt("DoPayCardTransferResult", retVal);

            return result;
        }
    }
}
