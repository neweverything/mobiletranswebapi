﻿using CabRideEngineDapper.Utils;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using TaxiPassCommon;
using TaxiPassCommon.Banking;
using TaxiPassCommon.Banking.Models;

namespace CabRideEngineDapper {
	public partial class Affiliate {


		public override int Save() {
			return Save(CabRideDapperSettings.LoggedInUser);
		}

		public override int Save(string pModifiedBy) {
			using (var conn = SqlHelper.OpenSqlConnection()) {
				return Save(conn, pModifiedBy);
			}
		}

		public override int Save(SqlConnection pConn, string pModifiedBy) {

			int saved = base.Save(pConn, pModifiedBy);
			saved += SaveRelatedTables(this, CabRideDapperSettings.LoggedInUser);

			return saved;
		}


		private int SaveRelatedTables(Affiliate pAffiliate, string pModifiedBy) {
			var listProperties = pAffiliate.GetType().GetFields(System.Reflection.BindingFlags.FlattenHierarchy | System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic).ToList();
			int saved = 0;
			foreach (var prop in listProperties) {
				try {
					string propType = prop.DeclaringType.FullName;
					if (prop.Name.Contains("Email")) {
						Console.Write("");
					}
					if (propType.StartsWith("CabRideEngineDapper.", StringComparison.CurrentCultureIgnoreCase)) {
						object propVal = prop.GetValue(pAffiliate);
						BaseEntity entity = propVal as BaseEntity;

						if (propVal != null) {
							object affId = entity.GetType().GetProperty("AffiliateID").GetValue(entity);
							long affID = Convert.ToInt64(affId);
							if (affID == 0) {
								entity.GetType().GetProperty("AffiliateID").SetValue(entity, this.AffiliateID);
							}
							System.Reflection.MethodInfo methodInfo = propVal.GetType().GetMethod("Save", new Type[] { }, new System.Reflection.ParameterModifier[] { });
							try {
								methodInfo.Invoke(propVal, new object[] { });
								saved++;
							} catch (Exception ex1) {
								Console.WriteLine(ex1.Message);
							}
						}
					}
				} catch (Exception ex) {
					Console.WriteLine(ex.Message);
				}
			}
			return saved;
		}

		public static List<Affiliate> GetForDailyBillPayReport(SqlConnection pConn) {
			string sql = @"SELECT *
									FROM Affiliate
									WHERE AffiliateID IN ( SELECT AffiliateID FROM AffiliateContacts WHERE BillPaySiteDailyReport = 1 )
									ORDER BY Name";
			return pConn.Query<Affiliate>(sql).ToList();
		}


		public List<CheckStatus> GetCheckStatusReport(DateTime pStartDate, DateTime pEndDate) {

			using (var conn = SqlHelper.OpenSqlConnection()) {
				//Affiliate aff = SessionHelper.AffiliateContact(pSession).Affiliate;
				long verisignAccountID = this.VerisignAccountID.GetValueOrDefault(0);
				AffiliateRef affRec = AffiliateRef.GetByGroupReference(conn, this.AffiliateID, "Gateways", "eCheck");
				if (!affRec.ValueString.IsNullOrEmpty() && affRec.ValueString.IsNumeric() && affRec.ValueString.ToInt() > 0) {
					verisignAccountID = affRec.ValueString.ToInt();
				}


				var creds = CreditCardUtils.LoadGateWays(conn, verisignAccountID, CabRideEngineDapper.Enums.CreditCardChargeType.Any, "eCheck");

				Gateway gway = new Gateway(creds[0]);
				return gway.GetCheckTransactionHistory(pStartDate, pEndDate);
			}
		}

		public static Affiliate GetAffiliateByAbbreviatedName(SqlConnection pConn, string pName) {
			string sql = @"SELECT * FROM dbo.Affiliate 
							WHERE AbbreviatedName = @AbbreviatedName";

			return pConn.Get<Affiliate>(sql, new { AbbreviatedName = pName });
		}
	}
}
