﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileTransWebAPI.Models {
	public class VehicleInfoRequest {
		public long AffiliateID { get; set; }
		public long DriverID { get; set; }

		public string VehicleNo { get; set; }

		public string RequestKey { get; set; }
	}
}