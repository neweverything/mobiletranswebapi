﻿using System;
using System.Collections.Generic;
using TaxiPassCommon.Banking.Models;

namespace TaxiPassCommon.Banking {
	public interface IProcessor {

		ProcessorResponse PreAuthCard(CCRequest pRequest);
		ProcessorResponse ChargeCard(CCRequest pRequest);
		ProcessorResponse ChargeAuth(CCRequestRefNo pRequest);
		ProcessorResponse Refund(CCRequestRefNo pRequest);
		ProcessorResponse OpenCredit(CCRequest pRequest);
		ProcessorResponse AvsCheck(CCRequest pRequest);
		ProcessorResponse ProcessCheck(CheckRequest pRequest);
		bool Void(CCRequestRefNo pRequest);
		ProcessorResponse RefundCheck(CheckRequest pRequest);


		string GetTransactionHistory(DateTime pStart, DateTime pEnd);
		List<CheckStatus> GetCheckTransactionHistory(DateTime pStart, DateTime pEnd);
	}
}
