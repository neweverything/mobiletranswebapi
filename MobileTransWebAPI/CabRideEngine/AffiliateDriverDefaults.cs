﻿using IdeaBlade.Persistence;
using IdeaBlade.Persistence.Rdb;
using System;
using System.Data;
using System.Text;
using TaxiPassCommon;

namespace CabRideEngine {
	/// <summary>
	/// The developer class representing the AffiliateDriverDefaults business object class.
	/// </summary>
	/// <remarks>
	/// <para>Place all application-specific business logic in this class.</para>
	/// <para>
	/// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
	/// and will not overwrite changes.
	/// </para>
	/// <para>
	/// This class inherits from the generated class, its companion in the pair of classes 
	/// defining a business object. Properties in the generated class may be overridden in 
	/// order to change the behavior or add business logic.
	/// </para>
	/// <para>
	/// This class is the final class in the inheritance chain for this business object. 
	/// It should not be inherited.
	/// </para>
	/// </remarks>
	[Serializable]
	public sealed partial class AffiliateDriverDefaults : AffiliateDriverDefaultsDataRow {

		#region Constructors -- DO NOT MODIFY

		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new';
		// it is also required if the class is visible from a web service.
		// DO NOT REMOVE **
		private AffiliateDriverDefaults()
			: this(null) {
		}

		/// <summary>
		/// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
		/// </summary>
		/// <param name="pRowBuilder"></param>
		public AffiliateDriverDefaults(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static AffiliateDriverDefaults Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      AffiliateDriverDefaults aAffiliateDriverDefaults = pManager.CreateEntity<AffiliateDriverDefaults>();
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aAffiliateDriverDefaults, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aAffiliateDriverDefaults.AddToManager();
		//      return aAffiliateDriverDefaults;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		// Add additional logic to your business object here...
		public static AffiliateDriverDefaults Create(Affiliate pAffiliate) {
			AffiliateDriverDefaults oDefault = null;
			SystemDefaults sysDefault = SystemDefaults.GetDefaults(pAffiliate.PersistenceManager);
			try {
				// Creates the State but it is not yet accessible to the application
				oDefault = pAffiliate.PersistenceManager.CreateEntity<AffiliateDriverDefaults>();
				//pAffiliate.PersistenceManager.GenerateId(oDefault, AffiliateDriverDefaultIDEntityColumn);
				oDefault.AffiliateID = pAffiliate.AffiliateID;
				oDefault.TaxiPayCVV = true;
				oDefault.ValidateCardImprintAmount = 75;
				oDefault.IVRSignUp = true;
				oDefault.DriverPaysSwipeFee = 1;
				oDefault.DriverPaysSwipeFeePerAmount = 25;

				oDefault.CardChargedMaxNumber = sysDefault.DriverCardChargedMaxNumber;
				oDefault.MaxTransactions = sysDefault.DriverMaxTransactions;
				oDefault.ChargeMaxAmount = sysDefault.DriverChargeMaxAmount;
				oDefault.ChargeMaxSwipeAmount = sysDefault.DriverChargeMaxSwipeAmount;
				oDefault.MaxTransPerCardPerMaxSwipe = sysDefault.MaxTransPerCardPerMaxSwipe;
				oDefault.MaxTransPerWeek = sysDefault.DriverMaxTransPerWeek;
				oDefault.MaxTransPerWeekPerCard = sysDefault.DriverMaxTransPerWeekPerCard;
				oDefault.MaxTransPerMonth = sysDefault.DriverMaxTransPerMonth;
				oDefault.MaxTransPerMonthPerCard = sysDefault.DriverMaxTransPerMonthPerCard;
				oDefault.MaxDailyAmount = sysDefault.DriverMaxDailyAmount;
				oDefault.MaxWeeklyAmount = sysDefault.DriverMaxWeeklyAmount;
				oDefault.MaxMonthlyAmount = sysDefault.DriverMaxMonthlyAmount;
				oDefault.ChargeMaxSwipeAmount = 250M;

				oDefault.StartingFare = 5;
				oDefault.DriverCanSubmitCheck = true;

				oDefault.DriverJobNotificationAlertTime = Convert.ToInt32("0" + SystemDefaultsDict.GetByKey(pAffiliate.PersistenceManager, "Ride", "DriverJobNotificationAlertTime", false).ValueString);
				oDefault.AddToManager();
			} catch (Exception ex) {
				throw ex;
			}
			return oDefault;
		}

		//protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
		//	if (IsDeserializing)
		//		return;
		//	base.OnColumnChanging(pArgs);
		//	DoAudit(pArgs);
		//}

		//private void DoAudit(DataColumnChangeEventArgs pArgs) {
		//	if (!this.PersistenceManager.IsClient) {
		//		return;
		//	}
		//	if (this.RowState == DataRowState.Added) {
		//		return;
		//	}
		//	if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
		//		AuditRecord = AffiliateDriverDefaultsAudit.Create(this.PersistenceManager, this);
		//	}
		//}

		public override bool? ChargePassRequireCustInfo {
			get {
				return base.ChargePassRequireCustInfo.GetValueOrDefault(false);
			}
			set {
				base.ChargePassRequireCustInfo = value;
			}
		}

		public override bool? ExcludeFromRedeemerStopPayment {
			get {
				return base.ExcludeFromRedeemerStopPayment.GetValueOrDefault(false);
			}
			set {
				base.ExcludeFromRedeemerStopPayment = value;
			}
		}

		public override bool?  DebitFeeProcessing {
			get {
				return base.DebitFeeProcessing.GetValueOrDefault(false);
			}
			set {
				base.DebitFeeProcessing = value;
			}
		}

        public override bool? CheckFeeProcessing {
            get {
                return base.CheckFeeProcessing.GetValueOrDefault(false);
            }
            set {
                base.CheckFeeProcessing = value;
            }
        }

        public static AffiliateDriverDefaults GetDriverDefaults(Affiliate pAffiliate) {
			RdbQuery qry = new RdbQuery(typeof(AffiliateDriverDefaults), AffiliateDriverDefaults.AffiliateIDEntityColumn, EntityQueryOp.EQ, pAffiliate.AffiliateID);
			return pAffiliate.PersistenceManager.GetEntity<AffiliateDriverDefaults>(qry);
		}

		public static AffiliateDriverDefaults GetDriverDefaults(PersistenceManager pPM, long pAffiliateID) {
			RdbQuery qry = new RdbQuery(typeof(AffiliateDriverDefaults), AffiliateDriverDefaults.AffiliateIDEntityColumn, EntityQueryOp.EQ, pAffiliateID);
			return pPM.GetEntity<AffiliateDriverDefaults>(qry);
		}


		// ************************************************************************************************************
		// Return the data
		// ************************************************************************************************************
		private static EntityList<AffiliateDriverDefaults> GetData(PersistenceManager pPM, string pAffiliateid) {

			StringBuilder sql = new StringBuilder();
			sql.Append("usp_AffiliateDriverGet ");
			if (!pAffiliateid.IsNullOrEmpty()) {
				sql.AppendFormat(" @AffiliateID = {0}", pAffiliateid);
			}
			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(AffiliateDriverDefaults), sql.ToString());
			EntityList<AffiliateDriverDefaults> oData = pPM.GetEntities<AffiliateDriverDefaults>(qry);
			if (oData.Count == 0) {
				oData.Add(pPM.GetNullEntity<AffiliateDriverDefaults>());
			}
			return oData;
		}


		public static EntityList<AffiliateDriverDefaults> GetEmailInactiveDriversDuration(PersistenceManager pPM) {
			RdbQuery qry = new RdbQuery(typeof(AffiliateDriverDefaults), AffiliateDriverDefaults.EmailInactiveDriversDurationEntityColumn, EntityQueryOp.GT, 0);
			return pPM.GetEntities<AffiliateDriverDefaults>(qry);
		}

		public static AffiliateDriverDefaults GetDriverDefaultsByGPSReaderAccount(PersistenceManager pPM, string pGPSReaderAccount) {
			RdbQuery qry = new RdbQuery(typeof(AffiliateDriverDefaults), TaxiTronicGPSReaderAccountEntityColumn, EntityQueryOp.EQ, pGPSReaderAccount);
			return pPM.GetEntity<AffiliateDriverDefaults>(qry);
		}

		public int GetAuthLimit1() {
			int amount = 75;
			if (AuthAmount1.HasValue && AuthAmount1 > 0) {
				amount = AuthAmount1.Value;
			}
			return amount;
		}

		public int GetAuthLimit2() {
			int amount = 100;
			if (AuthAmount2.HasValue && AuthAmount2 > 0) {
				amount = AuthAmount2.Value;
			}
			return amount;
		}

		public int GetAuthLimit3() {
			int amount = Convert.ToInt32(this.ChargeMaxAmount);
			if (amount < 1) {
				amount = Convert.ToInt32(SystemDefaults.GetDefaults(this.PersistenceManager).DriverChargeMaxAmount);
			}
			if (amount < 0) {
				amount = 150;
			}
			return amount;
		}

		public decimal CalcSwipeFee(decimal pDriverTotal) {
			decimal driverFee = 0;
			if (AffiliateID != 161 && DriverPaysSwipeFee > 0) {
				if (DriverPaysSwipeFeePerAmount > 0) {
					decimal dFeeAmt = DriverPaysSwipeFeePerAmount;
					int multiplier = Convert.ToInt32(pDriverTotal / dFeeAmt);

					if (pDriverTotal - (dFeeAmt * multiplier) > 0) {
						multiplier++;
					}
					driverFee = DriverPaysSwipeFee * multiplier;
				} else {
					driverFee += DriverPaysSwipeFee;
				}

			}

			return driverFee;
		}
	}

	#region EntityPropertyDescriptors.AffiliateDriverDefaultsPropertyDescriptor
	////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
	////    For example: 
	////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
	//public static partial class EntityPropertyDescriptors {
	//	public partial class AffiliateDriverDefaultsPropertyDescriptor : BaseEntityPropertyDescriptor {
	//    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
	//    public AdaptedPropertyDescriptor Age {
	//      get { return this.Get("Age"); }
	//    }
	//  }
	//} 
	#endregion

}
