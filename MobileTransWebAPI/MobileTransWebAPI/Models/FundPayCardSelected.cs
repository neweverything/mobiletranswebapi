﻿using System.Collections.Generic;

namespace MobileTransWebAPI.Models {
	public partial class FundPayCardSelected {

		public string UserID { get; set; }
		public string RequestTime { get; set; }
		public string Phone { get; set; }
		public long TaxiPassRedeemerID { get; set; }
		public string VehicleNo { get; set; }

		public List<string> TransList { get; set; }
	}
}