﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Dapper;
using TaxiPassCommon;

using CabRideEngineDapper.Utils;

namespace CabRideEngineDapper {
	public partial class CustomerAddress {

		public static List<CustomerAddress> GetCustomerAddresses(long pCustomerID) {
			using (var conn = SqlHelper.OpenSqlConnection()) {
				string sql = "SELECT * FROM CustomerAddress WITH (NOLOCK) WHERE CustomerID = @CustomerID";
				return conn.Query<CustomerAddress>(sql, new { CustomerID = pCustomerID }).ToList();
			}
		}
	}
}
