﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using TaxiPassCommon;

using IdeaBlade.Persistence;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

using Csla.Validation;

namespace CabRideEngine {
    [Serializable]
    public sealed class Airlines : AirlinesDataRow {

        #region Constructors -- DO NOT MODIFY
        // Do not create constructors for this class
        // Developers cannot create instances of this class with the "new" keyword
        // You should write a static Create method instead; see the "Suggested Customization" region
        // Please review the documentation to learn about entity creation and inheritance

        // This private constructor prevents accidental instance creation via 'new'
        private Airlines() : this(null) {
        }

        // Typed DataSet constructor (needed by framework) - DO NOT REMOVE
        public Airlines(DataRowBuilder pRowBuilder)
            : base(pRowBuilder) {
        }

        #endregion

        #region Suggested Customizations

        //    // Use this factory method template to create new instances of this class
        //    public static Airlines Create(PersistenceManager pManager,
        //      ... your creation parameters here ...) { 
        //
        //      Airlines aAirlines = pManager.CreateEntity<Airlines>();
        //
        //      // if this object type requires a unique id and you have implemented
        //      // the IIdGenerator interface implement the following line
        //      pManager.GenerateId(aAirlines, // add id column here //);
        //
        //      // Add custom code here
        //
        //      aAirlines.AddToManager();
        //      return aAirlines;
        //    }

        //    // Implement this method if you want your object to sort in a specific order
        //    public override int CompareTo(Object pObject) {
        //    }

        //    // Implement this method to customize the null object version of this class
        //    protected override void UpdateNullEntity() {
        //    }

        #endregion

        // Add additional logic to your business object here...
        public static Airlines Create(PersistenceManager pPM) {
            Airlines oAirline = null;

            try {
                // Creates the State but it is not yet accessible to the application
                oAirline = (Airlines)pPM.CreateEntity(typeof(Airlines));

				//pPM.GenerateId(oAirline, Airlines.AirlineIDEntityColumn);

                oAirline.AddToManager();

            } catch (Exception ex) {
                throw ex;
            }
            return oAirline;
        }

		//protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
		//	if (IsDeserializing)
		//		return;
		//	base.OnColumnChanging(pArgs);
		//	DoAudit(pArgs);
		//}

		//private void DoAudit(DataColumnChangeEventArgs pArgs) {
		//	if (!this.PersistenceManager.IsClient) {
		//		return;
		//	}
		//	if (this.RowState == DataRowState.Added) {
		//		return;
		//	}
		//	if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
		//		AuditRecord = AirlinesAudit.Create(this.PersistenceManager, this);
		//	}
		//	//AuditRecord[pArgs.Column.ColumnName] = pArgs.ProposedValue;
		//}

        protected override void AddRules(RuleList pList) {
            PropertyRequiredRule.AddToList(pList, Airlines.CodeEntityColumn.ColumnName);
            PropertyRequiredRule.AddToList(pList, Airlines.NameEntityColumn.ColumnName);
            pList.Add(IsAirlineCodeUnique, Airlines.CodeEntityColumn.ColumnName);
        }

        private bool IsAirlineCodeUnique(object pTarget, RuleArgs e) {
            Airlines oAirline = (Airlines)pTarget;

            RdbQuery oQry = new RdbQuery(typeof(Airlines), Airlines.CodeEntityColumn, EntityQueryOp.EQ, oAirline.Code);
            oQry.AddClause(Airlines.AirlineIDEntityColumn, EntityQueryOp.NE, oAirline.AirlineID);
            EntityList<Airlines> oAirlines = this.PersistenceManager.GetEntities<Airlines>(oQry);
            if (oAirlines.Count > 0) {
                e.Description = "Airline code must be unique!";
                return false;
            }
            return true;
        }

        [BindingBrowsable(false)]
        public override string EntityInstanceDisplayName {
            get {
                return Code;
            }
        }

        public string FormattedPhone {
            get {
                string sPhone = "";
                if (Phone != null) {
					sPhone = Phone.FormattedPhoneNumber();
                }
                return sPhone;
            }

            set {
                Phone = value;
            }
        }

        public override string Phone {
            get {
                return base.Phone;
            }
            set {
                base.Phone = StringUtils.CleanPhoneNumbers(value);
            }
        }

        public static Airlines GetAirline(PersistenceManager pPM, string pAirline) {
            RdbQuery oQry = new RdbQuery(typeof(Airlines), Airlines.CodeEntityColumn, EntityQueryOp.EQ, pAirline);
            return pPM.GetEntity<Airlines>(oQry);
        }

        public static EntityList<Airlines> GetAllAirlines(PersistenceManager pPM, string pSortColumn) {
            EntityList<Airlines> oAirlines = pPM.GetEntities<Airlines>();
            if (pSortColumn.Length > 0) {
                oAirlines.ApplySort(pSortColumn, System.ComponentModel.ListSortDirection.Ascending, false);
            }
            return oAirlines;
        }
                 
    }

}
