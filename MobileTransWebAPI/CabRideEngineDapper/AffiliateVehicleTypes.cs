﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

using Dapper;
using TaxiPassCommon;

using CabRideEngineDapper.Utils;

namespace CabRideEngineDapper {
	public partial class AffiliateVehicleTypes {

		public static AffiliateVehicleTypes Create(long pAffiliateID) {
			AffiliateVehicleTypes rec = AffiliateVehicleTypes.Create();
			rec.AffiliateID = pAffiliateID;
			return rec;
		}

		public static List<AffiliateVehicleTypes> GetByAffiliate(SqlConnection pConn, long pAffiliateID) {
			return pConn.GetList<AffiliateVehicleTypes>(new { AffiliateID = pAffiliateID }).ToList();
		}

		public static List<string> ListForGetRide(SqlConnection pConn, long pAffiliateID) {
			string sql = @"SELECT VehicleType FROM dbo.VehicleTypes
							WHERE VehicleTypeID IN (SELECT VehicleTypeID FROM dbo.AffiliateVehicleTypes WHERE AffiliateID = @AffiliateID)
							ORDER BY VehicleType";

			return pConn.Query<string>(sql, new { AffiliateID = pAffiliateID }).ToList();
		}
	}
}
