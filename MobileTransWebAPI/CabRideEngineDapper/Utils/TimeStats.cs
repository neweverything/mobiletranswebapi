﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CabRideEngineDapper.Utils {
    public class TimeStats : MyTimeStats {
        public TimeStats() {
            Info = new List<ElapsedInfo>();
            Start = DateTime.Now;
        }
    }

    public abstract class MyTimeStats {
        public string Server { get; set; }
        public string Process { get; set; }
        public string Method { get; set; }
        public string Key { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }

        public int GetUniqueTransNoCount { get; set; }

        //public ObjectId Id { get; set; }
        //public double GetUniqueTransNo { get; set; }
        //public double USAePay { get; set; }
        //public double Verisign { get; set; }
        //public double DriverLookUp { get; set; }
        //public double AffiliateDriverLookUp { get; set; }
        //public double GetCreateDriverPayment { get; set; }
        //public double PreAuthCard { get; set; }
        //public double PreAuthCard { get; set; }

        public double ElapsedTime {
            get {
                return End.Subtract(Start).TotalSeconds;
            }
        }

        public string ErrorMessage { get; set; }
        public List<ElapsedInfo> Info { get; set; }

        public class ElapsedInfo {
            public string Type { get; set; }
            public double Elapsed { get; set; }
        }
    }
}
