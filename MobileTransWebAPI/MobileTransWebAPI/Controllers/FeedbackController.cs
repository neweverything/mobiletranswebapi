﻿using CabRideEngine;
using IdeaBlade.Persistence;
using MobileTransWebAPI.Models;
using MobileTransWebAPI.Utils;
using System;
using System.Web.Http;
using System.Web.Script.Serialization;
using TaxiPassCommon;
using TaxiPassCommon.Mail;

namespace MobileTransWebAPI.Controllers {
	public class FeedbackController : ApiController {

		private PersistenceManager mPM = WebApiApplication.UserPM;

		[HttpPost]
		public SupportFeedbackResult SaveFeedback(SupportFeedback pRequest) {
			SupportFeedbackResult result = new SupportFeedbackResult();

			JavaScriptSerializer js = new JavaScriptSerializer();
			WebApiApplication.LogIt("Feedback", js.Serialize(pRequest));

			DateTime requestDate = DateTime.Today.AddDays(-1);
			if (pRequest != null) {
				requestDate = pRequest.Key.DecryptIceKey().ToDateTime();
			}

			if (WebApiApplication.GetUTCServerTime().Subtract(requestDate).TotalMinutes > 1) {
				result.ErrorMsg = "Invalid Access";
				return result;
			}

			string sMsg = Common.SetPM(mPM);

			Drivers driver = Drivers.GetDriver(mPM, pRequest.DriverID);
			AffiliateDrivers affDriver = AffiliateDrivers.GetDriver(mPM, pRequest.AffiliateDriverID);

			if (driver.IsNullEntity) {
				result.ErrorMsg = "Invalid Access";
				return result;
			}

			Feedback feedback = Feedback.Create(mPM);
			feedback.DriverID = pRequest.DriverID;
			feedback.AffiliateID = driver.AffiliateID;
			if (pRequest.AffiliateDriverID > 0) {
				feedback.AffiliateDriverID = pRequest.AffiliateDriverID;
			}


			feedback.Notes = pRequest.Feedback;
			feedback.FeedBackType = "NewTaxiPass Driver";

			try {
				feedback.Save();
				result.Sent = true;
				string subject = "Driver Support/Feedback";
				string msg = string.Format("Feedback ID: {0}\nDriver: {1}\nCell: {2}\nFeedback/Support message:\n{3}", feedback.FeedBackID, driver.Name, driver.Cell, feedback.Notes);

				var cred = CabRideEngineDapper.SystemDefaultsDict.GetEmailCreds(CabRideEngineDapper.Enums.EMailReportFrom.Report);
				EMailMessage email = new EMailMessage(cred, subject, msg);
				email.SendMail(SystemDefaultsDict.GetByKey(mPM, "NewTaxiPass Feedback Mail To").ValueString);

			} catch (Exception ex) {
				var cred = CabRideEngineDapper.SystemDefaultsDict.GetEmailCreds(CabRideEngineDapper.Enums.EMailReportFrom.Report);
				EMailMessage email = new EMailMessage(cred, Properties.Settings.Default.SystemName + " Feedback Error", string.Format("{0}\n{1}", ex.Message, ex.StackTrace));
				email.SendMail(Properties.Settings.Default.EMailErrorsTo);
			}

			return result;

		}


	}
}
