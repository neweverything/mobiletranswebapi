﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;
using IdeaBlade.Rdb;

using Csla.Validation;

namespace CabRideEngine {
    /// <summary>
    /// The developer class representing the VIPList business object class.
    /// </summary>
    /// <remarks>
    /// <para>Place all application-specific business logic in this class.</para>
    /// <para>
    /// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
    /// and will not overwrite changes.
    /// </para>
    /// <para>
    /// This class inherits from the generated class, its companion in the pair of classes 
    /// defining a business object. Properties in the generated class may be overridden in 
    /// order to change the behavior or add business logic.
    /// </para>
    /// <para>
    /// This class is the final class in the inheritance chain for this business object. 
    /// It should not be inherited.
    /// </para>
    /// </remarks>
    [Serializable]
    public sealed partial class VIPList : VIPListDataRow {

        #region Constructors -- DO NOT MODIFY

        // Do not create constructors for this class
        // Developers cannot create instances of this class with the "new" keyword
        // You should write a static Create method instead; see the "Suggested Customization" region
        // Please review the documentation to learn about entity creation and inheritance

        // This private constructor prevents accidental instance creation via 'new';
        // it is also required if the class is visible from a web service.
        // DO NOT REMOVE **
        private VIPList() : this(null) {
        }

        /// <summary>
        /// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
        /// </summary>
        /// <param name="pRowBuilder"></param>
        public VIPList(DataRowBuilder pRowBuilder)
            : base(pRowBuilder) {
        }

        #endregion

        #region Suggested Customizations

        //    // Use this factory method template to create new instances of this class
        //    public static VIPList Create(PersistenceManager pManager,
        //      ... your creation parameters here ...) { 
        //
        //      VIPList aVIPList = pManager.CreateEntity<VIPList>();
        //
        //      // if this object type requires a unique id and you have implemented
        //      // the IIdGenerator interface implement the following line
        //      pManager.GenerateId(aVIPList, // add id column here //);
        //
        //      // Add custom code here
        //
        //      aVIPList.AddToManager();
        //      return aVIPList;
        //    }

        //    // Implement this method if you want your object to sort in a specific order
        //    public override int CompareTo(Object pObject) {
        //    }

        //    // Implement this method to customize the null object version of this class
        //    protected override void UpdateNullEntity() {
        //    }

        #endregion

        // Add additional logic to your business object here...
        public static VIPList Create(PersistenceManager pManager) {

            VIPList oVIP = (VIPList)pManager.CreateEntity(typeof(VIPList));

            pManager.GenerateId(oVIP, VIPList.VIPListIDEntityColumn);
            oVIP.AddToManager();
            return oVIP;
        }

        protected override void AddRules(RuleList pList) {
            PropertyRequiredRule.AddToList(pList, VIPList.SearchFieldEntityColumn.ColumnName);
            PropertyRequiredRule.AddToList(pList, VIPList.ValueEntityColumn.ColumnName);
            pList.Add(IsVIPUnique, VIPList.ValueEntityColumn.ColumnName);
        }

        private bool IsVIPUnique(object pTarget, RuleArgs e) {
            VIPList oVIP = (VIPList)pTarget;

            RdbQuery oQry = new RdbQuery(typeof(VIPList), VIPList.ValueEntityColumn, EntityQueryOp.EQ, oVIP.Value);
            oQry.AddClause(SearchFieldEntityColumn, EntityQueryOp.EQ, oVIP.SearchField);
            oQry.AddClause(VIPList.VIPListIDEntityColumn, EntityQueryOp.NE, oVIP.VIPListID);
            EntityList<VIPList> oVIPList = this.PersistenceManager.GetEntities<VIPList>(oQry);
            if (oVIPList.Count > 0) {
                e.Description = "SearchField + VIP must be unique!";
                return false;
            }
            return true;
        }

        [BindingBrowsable(false)]
        public override string EntityInstanceDisplayName {
            get {
                return SearchField + " - " + Value;
            }
        }

        protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
            if (IsDeserializing) {
                return;
            }
            base.OnColumnChanging(pArgs);
            DoAudit(pArgs);
        }

        private void DoAudit(DataColumnChangeEventArgs pArgs) {
            if (!this.PersistenceManager.IsClient) {
                return;
            }
            if (this.RowState == DataRowState.Added) {
                return;
            }
            if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
                AuditRecord = VIPListAudit.Create(this.PersistenceManager, this);
            }
        }

        public static EntityList<VIPList> GetAllVIPList(PersistenceManager pPM) {
            EntityList<VIPList> oVIPList = pPM.GetEntities<VIPList>();
            oVIPList.ApplySort(VIPList.SearchFieldEntityColumn.ColumnName, System.ComponentModel.ListSortDirection.Ascending, true);
            return oVIPList;
        }


    }

    public class VIPSearchField {
        private string mSearchField;

        public VIPSearchField(string pSearchField, string pDisplayText) {
            mSearchField = pSearchField;
            mDisplayText = pDisplayText;
        }

        public string SearchField {
            get {
                return mSearchField;
            }
            set {
                mSearchField = value;
            }
        }

        private string mDisplayText;

        public string DisplayText{
            get {
                return mDisplayText;
            }
            set {
                mDisplayText = value;
            }
        }
	
    }
    #region EntityPropertyDescriptors.VIPListPropertyDescriptor
    ////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
    ////    For example: 
    ////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
    //public static partial class EntityPropertyDescriptors {
    //	public partial class VIPListPropertyDescriptor : BaseEntityPropertyDescriptor {
    //    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
    //    public AdaptedPropertyDescriptor Age {
    //      get { return this.Get("Age"); }
    //    }
    //  }
    //} 
    #endregion

}
