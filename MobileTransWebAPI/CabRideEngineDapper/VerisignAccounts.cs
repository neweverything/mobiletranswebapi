﻿using Dapper;
using System.Data.SqlClient;
using TaxiPassCommon;

namespace CabRideEngineDapper {
    public partial class VerisignAccounts {

        public readonly string ENCRYPT_KEY = "VeRiSiGn";

        [Editable(false)]
        public string GetSetVerisignPassword {
            get {
                Encryption oDecrypt = new Encryption();
                string sPassword = "";
                if (!VerisignPassword.IsNullOrEmpty()) {
                    sPassword = oDecrypt.Decrypt(VerisignPassword, ENCRYPT_KEY);
                }
                return sPassword;
            }
            set {
                Encryption oEncrypt = new Encryption();
                string sValue = oEncrypt.Encrypt(value, ENCRYPT_KEY);
                base.VerisignPassword = sValue;
            }
        }

        [Editable(false)]
        public string GetSetEPayPin {
            get {
                Encryption oDecrypt = new Encryption();
                string sPassword = "";
                if (!base.EPayPin.IsNullOrEmpty()) {
                    sPassword = oDecrypt.Decrypt(base.EPayPin, ENCRYPT_KEY);
                }
                return sPassword;
            }
            set {
                Encryption oEncrypt = new Encryption();
                string sValue = oEncrypt.Encrypt(value, ENCRYPT_KEY);
                base.EPayPin = sValue;
            }
        }

        public static VerisignAccounts GetDefaultAccount(SqlConnection pConn) {
            string sql = @"SELECT * FROM VerisignAccounts
							WHERE DefaultAccount = @DefaultAccount";

            return pConn.Get<VerisignAccounts>(sql, new { DefaultAccount = true });
        }

        public static VerisignAccounts GetAccount(SqlConnection pConn, long pVerisignAccountID) {
            string sql = @"SELECT * FROM VerisignAccounts
                            WHERE VerisignAccountID = @VerisignAccountID";

            return pConn.Get<VerisignAccounts>(sql, new { VerisignAccountID = pVerisignAccountID });
        }
    }
}
