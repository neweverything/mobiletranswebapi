﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;
using Csla.Validation;

namespace CabRideEngine {
	/// <summary>
	/// The developer class representing the BankHoliday business object class.
	/// </summary>
	/// <remarks>
	/// <para>Place all application-specific business logic in this class.</para>
	/// <para>
	/// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
	/// and will not overwrite changes.
	/// </para>
	/// <para>
	/// This class inherits from the generated class, its companion in the pair of classes 
	/// defining a business object. Properties in the generated class may be overridden in 
	/// order to change the behavior or add business logic.
	/// </para>
	/// <para>
	/// This class is the final class in the inheritance chain for this business object. 
	/// It should not be inherited.
	/// </para>
	/// </remarks>
	[Serializable]
	public sealed partial class BankHoliday : BankHolidayDataRow {

		#region Constructors -- DO NOT MODIFY

		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new';
		// it is also required if the class is visible from a web service.
		// DO NOT REMOVE **
		private BankHoliday() : this(null) { }

		/// <summary>
		/// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
		/// </summary>
		/// <param name="pRowBuilder"></param>
		public BankHoliday(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static BankHoliday Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      BankHoliday aBankHoliday = pManager.CreateEntity<BankHoliday>();
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aBankHoliday, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aBankHoliday.AddToManager();
		//      return aBankHoliday;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		// Add additional logic to your business object here...
		public static BankHoliday Create(PersistenceManager pPM) {
			BankHoliday rec = null;

			try {
				rec = pPM.CreateEntity<BankHoliday>();
				pPM.GenerateId(rec, BankHoliday.BankHolidayIDEntityColumn);
				rec.AddToManager();
			} catch (Exception ex) {
				throw ex;
			}
			return rec;
		}

		protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
			if (IsDeserializing)
				return;
			base.OnColumnChanging(pArgs);
			DoAudit(pArgs);
		}

		private void DoAudit(DataColumnChangeEventArgs pArgs) {
			if (!this.PersistenceManager.IsClient) {
				return;
			}
			if (this.RowState == DataRowState.Added) {
				return;
			}
			if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
				AuditRecord = BankHolidayAudit.Create(this);
			}
			//AuditRecord[pArgs.Column.ColumnName] = pArgs.ProposedValue;
		}

		protected override void AddRules(RuleList pList) {
			PropertyRequiredRule.AddToList(pList, BankHoliday.NameEntityColumn.ColumnName);
			PropertyRequiredRule.AddToList(pList, BankHoliday.DateEntityColumn.ColumnName);
		}

		public static EntityList<BankHoliday> GetAll(PersistenceManager pPM, QueryStrategy pQueryStrategy) {
			RdbQuery qry = new RdbQuery(typeof(BankHoliday));
			qry.AddOrderBy(BankHoliday.DateEntityColumn);
			return pPM.GetEntities<BankHoliday>(qry, pQueryStrategy);
		}

		public static BankHoliday GetByDate(PersistenceManager pPM, DateTime pDate) {
			RdbQuery qry = new RdbQuery(typeof(BankHoliday), DateEntityColumn, EntityQueryOp.EQ, pDate);
			return pPM.GetEntity<BankHoliday>(qry);
		}

		public static EntityList<BankHoliday> GetByDateRange(PersistenceManager pPM, DateTime pStartDate, DateTime pEndDate) {
			RdbQuery qry = new RdbQuery(typeof(BankHoliday), DateEntityColumn, EntityQueryOp.GE, pStartDate);
			qry.AddClause(DateEntityColumn, EntityQueryOp.LT, pEndDate.AddDays(1).AddMilliseconds(-1));
			return pPM.GetEntities<BankHoliday>(qry);
		}

		public override string EntityInstanceDisplayName {
			get {
				return this.Name;
			}
		}
	}

	#region EntityPropertyDescriptors.BankHolidayPropertyDescriptor
	////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
	////    For example: 
	////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
	//public static partial class EntityPropertyDescriptors {
	//	public partial class BankHolidayPropertyDescriptor : BaseEntityPropertyDescriptor {
	//    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
	//    public AdaptedPropertyDescriptor Age {
	//      get { return this.Get("Age"); }
	//    }
	//  }
	//} 
	#endregion

}