﻿using System;
using System.Text;

namespace MobileTransWebAPI.Utils {
	public class EmailErrors {
		// ************************************************************
		// Generic email launcher to IT
		// ************************************************************
		public static void SendErrorEmail(string pTitle, string pMsg, bool pIncludeStack = true) {

			StringBuilder sMsg = new StringBuilder();
			sMsg.AppendFormat("{0}<br><br>", pMsg);
			if (pIncludeStack) {
				sMsg.Append(string.Join("<br>", Environment.StackTrace.Split(new string[] { "\r\n" }, StringSplitOptions.None)));
			}


			// Create a title that includes the machine name
			StringBuilder sTitle = new StringBuilder();
			sTitle.AppendFormat("{0} [{1} - {2}]", pTitle, System.Environment.MachineName, DateTime.Now.ToString());

			SendEMail(sTitle.ToString(), sMsg.ToString(), Properties.Settings.Default.EMailFrom, Properties.Settings.Default.EMailErrorsTo);
		}


		// ***************************************************************************************************
		// Generic emailer
		// ***************************************************************************************************
		public static void SendEMail(string pTitle, string pMsg, string pFrom, string pTo) {
			var cred = CabRideEngineDapper.SystemDefaultsDict.GetEmailCreds(CabRideEngineDapper.Enums.EMailReportFrom.Report);
			TaxiPassCommon.Mail.EMailMessage oMail = new TaxiPassCommon.Mail.EMailMessage(cred, pTitle, pMsg);
			oMail.UseWebServiceToSendMail = Properties.Settings.Default.SendEmailViaWebService;
			oMail.SendMail(Properties.Settings.Default.EMailErrorsTo);
		}

	}
}