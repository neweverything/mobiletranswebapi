﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

namespace CabRideEngine {
    [Serializable]
    public sealed class RoutingAudit : RoutingAuditDataRow {

        #region Constructors -- DO NOT MODIFY
        // Do not create constructors for this class
        // Developers cannot create instances of this class with the "new" keyword
        // You should write a static Create method instead; see the "Suggested Customization" region
        // Please review the documentation to learn about entity creation and inheritance

        // This private constructor prevents accidental instance creation via 'new'
        private RoutingAudit()
            : this(null) {
        }

        // Typed DataSet constructor (needed by framework) - DO NOT REMOVE
        public RoutingAudit(DataRowBuilder pRowBuilder)
            : base(pRowBuilder) {
        }

        #endregion

        #region Suggested Customizations

        //    // Use this factory method template to create new instances of this class
        //    public static RoutingAudit Create(PersistenceManager pManager,
        //      ... your creation parameters here ...) { 
        //
        //      RoutingAudit aRoutingAudit = pManager.CreateEntity<RoutingAudit>();
        //
        //      // if this object type requires a unique id and you have implemented
        //      // the IIdGenerator interface implement the following line
        //      pManager.GenerateId(aRoutingAudit, // add id column here //);
        //
        //      // Add custom code here
        //
        //      aRoutingAudit.AddToManager();
        //      return aRoutingAudit;
        //    }

        //    // Implement this method if you want your object to sort in a specific order
        //    public override int CompareTo(Object pObject) {
        //    }

        //    // Implement this method to customize the null object version of this class
        //    protected override void UpdateNullEntity() {
        //    }

        #endregion

        // Add additional logic to your business object here...
        public static RoutingAudit Create(PersistenceManager pManager, Routing pRouting) {
            RoutingAudit oAudit = (RoutingAudit)pManager.CreateEntity(typeof(RoutingAudit));
            //pManager.GenerateId(oAudit, RoutingAudit.RoutingHistoryIDEntityColumn);

            for (int i = 0; i < pRouting.Table.Columns.Count; i++) {
                if (oAudit.Table.Columns.Contains(pRouting.Table.Columns[i].ColumnName)) {
                    oAudit[pRouting.Table.Columns[i].ColumnName] = pRouting[i];
                }
            }
            oAudit.AddToManager();

            return oAudit;
        }

        public static RoutingAudit GetLastPickUpRouting(PersistenceManager pPM, long pReserveID) {
            RdbQuery qry = new RdbQuery(typeof(RoutingAudit), RoutingAudit.ReservationIDEntityColumn, EntityQueryOp.EQ, pReserveID);
            qry.AddClause(RoutingAudit.TypeEntityColumn, EntityQueryOp.EQ, Routing_Types.PickUp);
            qry.AddOrderBy(RoutingAudit.RoutingHistoryIDEntityColumn, System.ComponentModel.ListSortDirection.Descending);
            return pPM.GetEntity<RoutingAudit>(qry);
        }

        public string FormattedAddressShort {
            get {
                string sAddr = "";
                if (RoutingType == RoutingTypes.Airport) {
                    sAddr = FullAirlineName;
                    if (!string.IsNullOrEmpty(FlightNo)) {
                        sAddr += " Flight # " + FlightNo;
                    }
                    if (!string.IsNullOrEmpty(sAddr)) {
                        if (Convert.ToBoolean(Arrival)) {
                            sAddr += " arriving at ";
                        } else {
                            sAddr += " departing from ";
                        }
                    }
                    sAddr += FullAirportName;
                } else {
                    sAddr = Street;
                    if (!(Suite == null || Suite.Trim() == "")) {
                        sAddr += ", " + Suite;
                    }
                    sAddr += ", " + City;
                    sAddr += ", " + State;
                    sAddr += ", " + Country;
                }
                return sAddr;
            }
        }

        private string FullAirportName {
            get {
                return Airports.GetAirport(this.PersistenceManager, Airport).Name;
            }
        }

        private string FullAirlineName {
            get {
                return Airlines.GetAirline(this.PersistenceManager, Airline).Name;
            }
        }


    }

}
