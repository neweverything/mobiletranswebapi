﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Util;

namespace CabRideEngine {
    [Serializable]
    public sealed class ZIPCodeAudit : ZIPCodeAuditDataRow {

        #region Constructors -- DO NOT MODIFY
        // Do not create constructors for this class
        // Developers cannot create instances of this class with the "new" keyword
        // You should write a static Create method instead; see the "Suggested Customization" region
        // Please review the documentation to learn about entity creation and inheritance

        // This private constructor prevents accidental instance creation via 'new'
        private ZIPCodeAudit() : this(null) { }

        // Typed DataSet constructor (needed by framework) - DO NOT REMOVE
        public ZIPCodeAudit(DataRowBuilder pRowBuilder)
            : base(pRowBuilder) {
        }

        #endregion

        #region Suggested Customizations

        //    // Use this factory method template to create new instances of this class
        //    public static ZIPCodeAudit Create(PersistenceManager pManager,
        //      ... your creation parameters here ...) { 
        //
        //      ZIPCodeAudit aZIPCodeAudit = pManager.CreateEntity<ZIPCodeAudit>();
        //
        //      // if this object type requires a unique id and you have implemented
        //      // the IIdGenerator interface implement the following line
        //      pManager.GenerateId(aZIPCodeAudit, // add id column here //);
        //
        //      // Add custom code here
        //
        //      aZIPCodeAudit.AddToManager();
        //      return aZIPCodeAudit;
        //    }

        //    // Implement this method if you want your object to sort in a specific order
        //    public override int CompareTo(Object pObject) {
        //    }

        //    // Implement this method to customize the null object version of this class
        //    protected override void UpdateNullEntity() {
        //    }

        #endregion

        // Add additional logic to your business object here...
        public static ZIPCodeAudit Create(PersistenceManager pManager, ZIPCode pZipCode) {
            ZIPCodeAudit zipCodeAudit = (ZIPCodeAudit)pManager.CreateEntity(typeof(ZIPCodeAudit));
            pManager.GenerateId(zipCodeAudit, ZIPCodeAudit.ZIPCodeHistoryIdEntityColumn);

            for (int i = 0; i < pZipCode.Table.Columns.Count; i++) {
                if (zipCodeAudit.Table.Columns.Contains(pZipCode.Table.Columns[i].ColumnName)) {
                    zipCodeAudit[pZipCode.Table.Columns[i].ColumnName] = pZipCode[i];
                }
            }
            zipCodeAudit.AddToManager();

            return zipCodeAudit;
        }


    }

}
