using System;
using System.Collections.Generic;
using System.Text;

namespace CabRideEngine {
	public enum CardProcessingFailureTypes {
		OKToProcess,
		Authorized,
		AuthorizedByAnotherDriver,
		MaxChargeAmountExceeded,
		MaxCardTransactions,
		MaxDriverTransactions,
		CardUsedTooSoon,
		MaxTransPerWeek,
		MaxTransPerMonth,
		MaxTransPerWeekPerCard,
		MaxTransPerMonthPerCard,
		MaxDailyAmount,
		MaxWeeklyAmount,
		MaxMonthlyAmount,
		MissingCardNumber,
		MaxDayAmountPerCardExceeded,
		MaxWeekAmountPerCardExceeded,
		MaxMonthAmountPerCardExceeded
	}
}
