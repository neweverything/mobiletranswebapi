﻿using CabRideEngineDapper.Enums;
using Dapper;
using Newtonsoft.Json;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using TaxiPassCommon;
using TaxiPassCommon.Banking;
using TaxiPassCommon.Banking.Enums;
using TaxiPassCommon.Banking.Interfaces;
using TaxiPassCommon.Banking.Models.GlobalCashCard;

namespace CabRideEngineDapper {
	public partial class AffiliateDrivers {

		public decimal GetPayCardBalanceUSBank(SqlConnection pConn) {
			decimal balance = 0;

			FSV.Credentials cred = new FSV.Credentials();

			//CabRideEngineDapper.AffiliateDriverRef rec;
			cred = pConn.Query<FSV.Credentials>("usp_SystemDefaultsDict_Flattened", new { ReferenceGroup = "FSV" }, commandType: CommandType.StoredProcedure).FirstOrDefault();

			AffiliateDriverRef recRegistration = AffiliateDriverRef.GetOrCreate(pConn, AffiliateDriverID, "FSV", "Registration");
			AffiliateDriverRef recPassCode = AffiliateDriverRef.GetOrCreate(pConn, AffiliateDriverID, "PayCard", "PassCode");

			FSV fsv = new FSV(cred, cred.UseProductionAsBoolen);
			var result = fsv.VerifyConnection();
			Console.WriteLine(result);

			SystemDefaultsDict balanceCheckCardAdminNo = SystemDefaultsDict.GetByKey(pConn, "FSV", "BalanceCheckCardAdminNo", false);

			FSV.FundsTransfer transfer = new FSV.FundsTransfer(cred);
			transfer.FromCardID = TransCardAdminNo;
			transfer.FromCardPassCode = recPassCode.ValueString;
			transfer.ToCardID = balanceCheckCardAdminNo.ValueString;
			transfer.TransferAmount = 0.01M;

			transfer.ReferenceFromCard = "Balance Check";
			transfer.ReferenceToCard = "Balance Check";

			var funded = fsv.TransferToPayCard(transfer);
			if (funded.ReturnCode == "1") {
				balance = Convert.ToDecimal(funded.FromCardBalance);
			} else {
				var emailCred = SystemDefaultsDict.GetEmailCreds(EMailReportFrom.Report);
				TaxiPassCommon.Mail.EMailMessage mail = new TaxiPassCommon.Mail.EMailMessage(emailCred.From, "PayCard Balance Error", JsonConvert.SerializeObject(funded), emailCred.Password);
				mail.SendMail(Properties.Settings.Default.EMailErrorsTo);
			}

			return balance;
		}

		public decimal GetPayCardBalance(SqlConnection pConn) {
			decimal balance = 0;

			IPayCardCredentials cred = new FSV.Credentials();
			AffiliateDriverRef rec;

			if (PayCardType.IsNullOrEmpty()) {
				PayCardType = PayCardTypes.USBank.ToString();
			}

			PayCardInfo fsvInfo = PayCardInfo.GetByType(pConn, PayCardType);
			if (fsvInfo.PayCardType.Equals(PayCardTypes.TransCard.ToString(), StringComparison.CurrentCultureIgnoreCase)) {
				TransCardDefaults transCardDefaults = TransCardDefaults.GetDefaults(fsvInfo.UseProduction ? fsvInfo.PassCode.ToLong() : fsvInfo.TestPassCode.ToLong());
				cred = JsonConvert.DeserializeObject<TransCard.Credentials>(JsonConvert.SerializeObject(transCardDefaults));
			} else {
				cred = JsonConvert.DeserializeObject<FSV.Credentials>(JsonConvert.SerializeObject(fsvInfo)); //  conn.Query<FSV.Credentials>("usp_SystemDefaultsDict_Flattened", new { ReferenceGroup = "FSV" }, commandType: CommandType.StoredProcedure).FirstOrDefault();
			}
			rec = AffiliateDriverRef.GetOrCreate(pConn, AffiliateDriverID, "FSV", "Registration");


			PayCardGateway payCardGateWay = new PayCardGateway(cred);
			FSV.VerifyConnectionResponse verifyResponse = payCardGateWay.VerifyConnection();

			if (verifyResponse.ReturnCode != "1") {
				return balance;
			}

			FSV.CardRegistrationResponse resp = JsonConvert.DeserializeObject<FSV.CardRegistrationResponse>(rec.ValueString);
			balance = payCardGateWay.GetFundingBalance();

			string driverId = AffiliateDriverID.ToString();
			GlobalCashCard gcc = new GlobalCashCard(cred);
			var history = gcc.GetHistory(driverId);
			//balance = gcc.GetBalance(driverId);

			balance = history.RESULTS.Sum(p => p.NET);
			return balance;
		}

		public decimal GetPayCardFundingBalance(SqlConnection pConn) {
			decimal balance = 0;

			IPayCardCredentials cred = new FSV.Credentials();
			AffiliateDriverRef rec;

			if (PayCardType.IsNullOrEmpty()) {
				PayCardType = PayCardTypes.USBank.ToString();
			}

			PayCardInfo fsvInfo = PayCardInfo.GetByType(pConn, PayCardType);
			if (fsvInfo.PayCardType.Equals(PayCardTypes.TransCard.ToString(), StringComparison.CurrentCultureIgnoreCase)) {
				TransCardDefaults transCardDefaults = TransCardDefaults.GetDefaults(fsvInfo.UseProduction ? fsvInfo.PassCode.ToLong() : fsvInfo.TestPassCode.ToLong());
				cred = JsonConvert.DeserializeObject<TransCard.Credentials>(JsonConvert.SerializeObject(transCardDefaults));
			} else {
				cred = JsonConvert.DeserializeObject<FSV.Credentials>(JsonConvert.SerializeObject(fsvInfo)); //  conn.Query<FSV.Credentials>("usp_SystemDefaultsDict_Flattened", new { ReferenceGroup = "FSV" }, commandType: CommandType.StoredProcedure).FirstOrDefault();
			}
			rec = AffiliateDriverRef.GetOrCreate(pConn, AffiliateDriverID, "FSV", "Registration");


			PayCardGateway payCardGateWay = new PayCardGateway(cred);
			FSV.VerifyConnectionResponse verifyResponse = payCardGateWay.VerifyConnection();

			if (verifyResponse.ReturnCode != "1") {
				return balance;
			}

			FSV.CardRegistrationResponse resp = JsonConvert.DeserializeObject<FSV.CardRegistrationResponse>(rec.ValueString);
			balance = payCardGateWay.GetFundingBalance();

			return balance;
		}

		public TransHistory GetPayCardTransHistory(SqlConnection pConn) {
			IPayCardCredentials cred = new FSV.Credentials();
			AffiliateDriverRef rec;

			PayCardInfo fsvInfo = PayCardInfo.GetByType(pConn, PayCardType);
			if (fsvInfo.PayCardType.Equals(PayCardTypes.TransCard.ToString(), StringComparison.CurrentCultureIgnoreCase)) {
				TransCardDefaults transCardDefaults = TransCardDefaults.GetDefaults(fsvInfo.UseProduction ? fsvInfo.PassCode.ToLong() : fsvInfo.TestPassCode.ToLong());
				cred = JsonConvert.DeserializeObject<TransCard.Credentials>(JsonConvert.SerializeObject(transCardDefaults));
			} else {
				cred = JsonConvert.DeserializeObject<FSV.Credentials>(JsonConvert.SerializeObject(fsvInfo)); //  conn.Query<FSV.Credentials>("usp_SystemDefaultsDict_Flattened", new { ReferenceGroup = "FSV" }, commandType: CommandType.StoredProcedure).FirstOrDefault();
			}
			rec = AffiliateDriverRef.GetOrCreate(pConn, AffiliateDriverID, "FSV", "Registration");


			PayCardGateway payCardGateWay = new PayCardGateway(cred);
			FSV.VerifyConnectionResponse verifyResponse = payCardGateWay.VerifyConnection();

			if (verifyResponse.ReturnCode != "1") {
				return new TransHistory();
			}

			try {
				FSV.CardRegistrationResponse resp = JsonConvert.DeserializeObject<FSV.CardRegistrationResponse>(rec.ValueString);

				string driverId = AffiliateDriverID.ToString();
				GlobalCashCard gcc = new GlobalCashCard(cred);
				var history = gcc.GetHistory(driverId);

				return history;
			} catch (Exception) {
				return new TransHistory();
			}
		}


	}
}
