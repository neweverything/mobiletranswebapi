﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileTransWebAPI.Models {
	public class PhoneNoAvailRequest {
		public long DriverID { get; set; }
		public string CurrentNumber { get; set; }
		public string NewNumber { get; set; }
	}
}