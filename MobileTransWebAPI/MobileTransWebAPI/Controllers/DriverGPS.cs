﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using Newtonsoft.Json;
using RideService;

using IdeaBlade.Persistence;
using CabRideEngine;

using TaxiPassCommon;
using System.Text;

namespace MobileTransWebAPI.Controllers {
	public class DriverGPS : ApiController {
		// GET api/<controller>

		public GPSInfo Get() {
			GPSInfo temp = JsonConvert.DeserializeObject<GPSInfo>("1F9F3AC454495E7EFAF6954CACD59FAEDBBED9878388EF19FADD77E4D0F5734D4810C7E1BF4E9CBE7302268B21D4BDB4CE1CF04BDC54AB3C22D337EB9A235AB65DC0C82531BF395A5EF0EF6EA8CDB59DB89F1F62C6593A8B91849BD93826205F427B2CFE4421986447EFCB5C2473107AD04E6C61934D776456CCE6255D4E02927374E1D669615320ED7E225463CDABBBFB6014C502EFBD050D99B4DC28846EC68F4AABB740FECCB6214D836E96C377D201812E015AFE92C94D451AF5098180A7501B51A040E6E7EEB0EF4690ACFB3F01E6013B39D14DC786A2522B06836859B00A25A2E55C489A01F405D616AAC3CCC7DC64CD0CB865A48D03CAFC5D7CE627A747636E5ED85BF0BB1D1E5DD0C325B954806731BDA9DB014E6AC6274748903018AF5A4BDF2811EB423C9374CE68D42E5CFB85F62212E13DDD3BB0BE5B99FDA95FE6883CE09A7378DE8C5FC9C6".DecryptIceKey());

			return GetGPSPulse(temp);
		}

		// ********************************************************
		// Update the Driver's gps location
		// ********************************************************
		public static GPSInfo GetGPSPulse(GPSInfo pGPSInfo) {

			//mAPITitle = "RideService.DriverService.SetGPSPulse";
			PersistenceManager oPM = WebApiApplication.UserPM;
			pGPSInfo.ErrorMsg = "";
			string sMsg = "";
			//if (Properties.Settings.Default.DriverLogging) {
			//    Global.LogIt("DriverService.SetGPSPulse", JsonConvert.SerializeObject(pGPSInfo));
			//}

			try {
				if (oPM == null) {
					sMsg = "Database connectivity error";
				} else {

					Drivers oDriver = Drivers.GetDriver(oPM, pGPSInfo.DriverID);
					if (oDriver.IsNullEntity) {
						sMsg = "Driver not found";
					} else if (!oDriver.Active) {
						sMsg = "Device is inactive";
					} else if (pGPSInfo.Latitude == 0 || pGPSInfo.Longitude == 0) {
						sMsg = "Invalid Lat/Long";
					}
				}
				if (!sMsg.IsNullOrEmpty()) {
					//ErrorCodes.DriverError.ErrorDic.TryGetValue(sMsg, out mError);
					//pGPSInfo.ErrorMsg = sMsg;
					//pGPSInfo.ErrorNo = mError;
					return pGPSInfo;
				}

				StringBuilder sql = new StringBuilder();
				sql.AppendFormat("usp_DriverSetGPS @DriverID ={0}", pGPSInfo.DriverID);
				sql.AppendFormat(", @Latitude ={0}", pGPSInfo.Latitude);
				sql.AppendFormat(", @Longitude ={0}", pGPSInfo.Longitude);
				sql.AppendFormat(", @Address ='{0}'", pGPSInfo.Address);
				sql.AppendFormat(", @LocationMethod ='{0}'", pGPSInfo.Method);
				sql.AppendFormat(", @AffiliateDriverID ={0}", pGPSInfo.AffiliateDriverID);
				if (pGPSInfo.MeterStatus.IsNullOrEmpty()) {
					pGPSInfo.MeterStatus = "N/A";
				}
				if (!pGPSInfo.MeterStatus.IsNullOrEmpty()) {
					sql.AppendFormat(", @MeterStatus ='{0}'", pGPSInfo.MeterStatus);
				}
				sql.Append(", @Message ='RideQ'");
				if (!pGPSInfo.GpsTime.IsNullOrEmpty()) {
					sql.AppendFormat(", @GPSAcquiredTime ='{0}'", pGPSInfo.GpsTime);
				}
				sql.Append(", @ModifiedBy ='RideService.SetGPSPulse'");
				Entity[] oAlertCode = DynamicEntities.DynamicDBCall(oPM, sql.ToString());
				pGPSInfo.Message = "";
				if (oAlertCode != null) {
					if (oAlertCode.Length > 0) {
						if (!oAlertCode[0]["AlertCode"].ToString().IsNullOrEmpty()) {
							pGPSInfo.Message = "AlertCode=" + oAlertCode[0]["AlertCode"].ToString();
						}
					}
				}

			} catch (Exception exUnknown) {
				pGPSInfo.ErrorNo = 3001;
				pGPSInfo.ErrorMsg = exUnknown.Message;

				//JavaScriptSerializer js = new JavaScriptSerializer();
				StringBuilder sInput = new StringBuilder();
				//sInput.AppendFormat("{0}<br>{1}", mAPITitle, js.Serialize(pGPSInfo));
				//Utils.EmailErrors.SendErrorEmail(mAPITitle, "Input: " + sInput.ToString() + Environment.NewLine + Environment.NewLine + exUnknown.Message);
			}

			return pGPSInfo;
		}
	}
}