﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

using Csla.Validation;


namespace CabRideEngine {
	/// <summary>
	/// The developer class representing the FraudDriverLicenses business object class.
	/// </summary>
	/// <remarks>
	/// <para>Place all application-specific business logic in this class.</para>
	/// <para>
	/// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
	/// and will not overwrite changes.
	/// </para>
	/// <para>
	/// This class inherits from the generated class, its companion in the pair of classes 
	/// defining a business object. Properties in the generated class may be overridden in 
	/// order to change the behavior or add business logic.
	/// </para>
	/// <para>
	/// This class is the final class in the inheritance chain for this business object. 
	/// It should not be inherited.
	/// </para>
	/// </remarks>
	[Serializable]
	public sealed partial class FraudDriverLicenses : FraudDriverLicensesDataRow {

		#region Constructors -- DO NOT MODIFY

		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new';
		// it is also required if the class is visible from a web service.
		// DO NOT REMOVE **
		private FraudDriverLicenses() : this(null) { }

		/// <summary>
		/// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
		/// </summary>
		/// <param name="pRowBuilder"></param>
		public FraudDriverLicenses(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static FraudDriverLicenses Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      FraudDriverLicenses aFraudDriverLicenses = pManager.CreateEntity<FraudDriverLicenses>();
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aFraudDriverLicenses, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aFraudDriverLicenses.AddToManager();
		//      return aFraudDriverLicenses;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		// Add additional logic to your business object here...
		public static FraudDriverLicenses Create(PersistenceManager pPM) {
			FraudDriverLicenses rec = null;
			try {
				rec = pPM.CreateEntity<FraudDriverLicenses>();

				// Using the IdeaBlade Id Generation technique
				//pPM.GenerateId(rec, FraudDriverLicenses.FraudDriverLicenseIDEntityColumn);

				rec.AddToManager();

			} catch (Exception ex) {
				throw ex;
			}
			return rec;
		}


		protected override void AddRules(RuleList pList) {
			PropertyRequiredRule.AddToList(pList, FraudDriverLicenses.LicenseStateEntityColumn.ColumnName);
			PropertyRequiredRule.AddToList(pList, FraudDriverLicenses.LicenseNoEntityColumn.ColumnName);
			pList.Add(IsLicenseNoUnique, LicenseNoEntityColumn.ColumnName);

			base.AddRules(pList);
		}


		private bool IsLicenseNoUnique(object pTarget, RuleArgs e) {
			FraudDriverLicenses rec = pTarget as FraudDriverLicenses;

			RdbQuery qry = new RdbQuery(typeof(FraudDriverLicenses), FraudDriverLicenses.LicenseNoEntityColumn, EntityQueryOp.EQ, rec.LicenseNo);
			qry.AddClause(FraudDriverLicenses.LicenseStateEntityColumn, EntityQueryOp.EQ, rec.LicenseState);
			qry.AddClause(FraudDriverLicenses.FraudDriverLicenseIDEntityColumn, EntityQueryOp.NE, rec.FraudDriverLicenseID);
			EntityList<FraudDriverLicenses> recList = this.PersistenceManager.GetEntities<FraudDriverLicenses>(qry, QueryStrategy.DataSourceThenCache);
			if (recList.Count > 0) {
				e.Description = "License Number already exists!";
				return false;
			}
			return true;
		}


		public static EntityList<FraudDriverLicenses> GetAll(PersistenceManager pPM, QueryStrategy pQueryStrategy) {
			RdbQuery qry = new RdbQuery(typeof(FraudDriverLicenses));
			qry.AddOrderBy(FraudDriverLicenses.LicenseStateEntityColumn);
			return pPM.GetEntities<FraudDriverLicenses>(qry, pQueryStrategy);
		}

		public static FraudDriverLicenses GetLicense(PersistenceManager pPM, string pLicenseNo) {
			RdbQuery qry = new RdbQuery(typeof(FraudDriverLicenses), FraudDriverLicenses.LicenseNoEntityColumn, EntityQueryOp.EQ, pLicenseNo);
			return pPM.GetEntity<FraudDriverLicenses>(qry);
		}

		public static FraudDriverLicenses GetLicense(PersistenceManager pPM, string pLicenseNo, string pState) {
			RdbQuery qry = new RdbQuery(typeof(FraudDriverLicenses), FraudDriverLicenses.LicenseNoEntityColumn, EntityQueryOp.EQ, pLicenseNo);
			qry.AddClause(FraudDriverLicenses.LicenseStateEntityColumn, EntityQueryOp.EQ, pState);

			return pPM.GetEntity<FraudDriverLicenses>(qry);
		}
	}

	#region EntityPropertyDescriptors.FraudDriverLicensesPropertyDescriptor
	////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
	////    For example: 
	////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
	//public static partial class EntityPropertyDescriptors {
	//	public partial class FraudDriverLicensesPropertyDescriptor : BaseEntityPropertyDescriptor {
	//    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
	//    public AdaptedPropertyDescriptor Age {
	//      get { return this.Get("Age"); }
	//    }
	//  }
	//} 
	#endregion

}