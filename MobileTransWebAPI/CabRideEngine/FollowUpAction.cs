﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

using Csla.Validation;

namespace CabRideEngine {
    /// <summary>
    /// The developer class representing the FollowUpAction business object class.
    /// </summary>
    /// <remarks>
    /// <para>Place all application-specific business logic in this class.</para>
    /// <para>
    /// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
    /// and will not overwrite changes.
    /// </para>
    /// <para>
    /// This class inherits from the generated class, its companion in the pair of classes 
    /// defining a business object. Properties in the generated class may be overridden in 
    /// order to change the behavior or add business logic.
    /// </para>
    /// <para>
    /// This class is the final class in the inheritance chain for this business object. 
    /// It should not be inherited.
    /// </para>
    /// </remarks>
    [Serializable]
    public sealed partial class FollowUpAction : FollowUpActionDataRow {

        #region Constructors -- DO NOT MODIFY

        // Do not create constructors for this class
        // Developers cannot create instances of this class with the "new" keyword
        // You should write a static Create method instead; see the "Suggested Customization" region
        // Please review the documentation to learn about entity creation and inheritance

        // This private constructor prevents accidental instance creation via 'new';
        // it is also required if the class is visible from a web service.
        // DO NOT REMOVE **
        private FollowUpAction() : this(null) {
        }

        /// <summary>
        /// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
        /// </summary>
        /// <param name="pRowBuilder"></param>
        public FollowUpAction(DataRowBuilder pRowBuilder)
            : base(pRowBuilder) {
        }

        #endregion

        #region Suggested Customizations

        //    // Use this factory method template to create new instances of this class
        //    public static FollowUpAction Create(PersistenceManager pManager,
        //      ... your creation parameters here ...) { 
        //
        //      FollowUpAction aFollowUpAction = pManager.CreateEntity<FollowUpAction>();
        //
        //      // if this object type requires a unique id and you have implemented
        //      // the IIdGenerator interface implement the following line
        //      pManager.GenerateId(aFollowUpAction, // add id column here //);
        //
        //      // Add custom code here
        //
        //      aFollowUpAction.AddToManager();
        //      return aFollowUpAction;
        //    }

        //    // Implement this method if you want your object to sort in a specific order
        //    public override int CompareTo(Object pObject) {
        //    }

        //    // Implement this method to customize the null object version of this class
        //    protected override void UpdateNullEntity() {
        //    }

        #endregion

        // Add additional logic to your business object here...
        public static FollowUpAction Create(PersistenceManager pManager) {

            FollowUpAction oAction = (FollowUpAction)pManager.CreateEntity(typeof(FollowUpAction));

            //pManager.GenerateId(oAction, FollowUpAction.FollowUpActionIDEntityColumn);
            oAction.AddToManager();
            return oAction;
        }


        protected override void AddRules(RuleList pList) {
            PropertyRequiredRule.AddToList(pList, FollowUpAction.FollowUpEntityColumn.ColumnName);
            pList.Add(IsActionUnique, FollowUpAction.FollowUpEntityColumn.ColumnName);
        }

        private bool IsActionUnique(object pTarget, RuleArgs e) {
            FollowUpAction oAction = (FollowUpAction)pTarget;

            RdbQuery qry = new RdbQuery(typeof(FollowUpAction), FollowUpAction.FollowUpEntityColumn, EntityQueryOp.EQ, oAction.FollowUp);
            qry.AddClause(FollowUpAction.FollowUpActionIDEntityColumn, EntityQueryOp.NE, oAction.FollowUpActionID);
            EntityList<FollowUpAction> oFollowUpAction = this.PersistenceManager.GetEntities<FollowUpAction>(qry);
            if (oFollowUpAction.Count > 0) {
                e.Description = "Follow Up must be unique!";
                return false;
            }
            return true;
        }

        [BindingBrowsable(false)]
        public override string EntityInstanceDisplayName {
            get {
                return this.FollowUp;
            }
        }

        protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
            if (IsDeserializing) {
                return;
            }
            base.OnColumnChanging(pArgs);
            //DoAudit(pArgs);
        }

        private void DoAudit(DataColumnChangeEventArgs pArgs) {
            if (!this.PersistenceManager.IsClient) {
                return;
            }
            if (this.RowState == DataRowState.Added) {
                return;
            }
            if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
                AuditRecord = FollowUpActionAudit.Create(this.PersistenceManager, this);
            }
        }

        public static EntityList<FollowUpAction> GetAll(PersistenceManager pPM, bool pKeepListSorted) {
            EntityList<FollowUpAction> oList = pPM.GetEntities<FollowUpAction>();
            oList.ApplySort(FollowUpAction.FollowUpEntityColumn.ColumnName, System.ComponentModel.ListSortDirection.Ascending, pKeepListSorted);
            return oList;
        }

    }

    #region EntityPropertyDescriptors.FollowUpActionPropertyDescriptor
    ////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
    ////    For example: 
    ////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
    //public static partial class EntityPropertyDescriptors {
    //	public partial class FollowUpActionPropertyDescriptor : BaseEntityPropertyDescriptor {
    //    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
    //    public AdaptedPropertyDescriptor Age {
    //      get { return this.Get("Age"); }
    //    }
    //  }
    //} 
    #endregion

}
