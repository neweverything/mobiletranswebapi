﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using System.Web.Script.Serialization;

using IdeaBlade.Persistence;

using TaxiPassCommon;

using CabRideEngine;
using CabRideEngine.Utils;

using MobileTransWebAPI.Models;
using MobileTransWebAPI.Utils;

namespace MobileTransWebAPI.Controllers {

	public class VehicleNoController : ApiController {

		public VehicleInfoResponse GetAssignVehicleNo(string pRequest) {
			VehicleInfoResponse result = new VehicleInfoResponse();

			JavaScriptSerializer js = new JavaScriptSerializer();

			VehicleInfoRequest info = js.Deserialize<VehicleInfoRequest>(pRequest.DecryptIceKey());
			WebApiApplication.LogIt("VehicleInfo", pRequest);

			DateTime requestDate = DateTime.Today.AddDays(-1);
			if (info != null) {
				requestDate = info.RequestKey.DecryptIceKey().ToDateTime();
			}

            if (WebApiApplication.GetUTCServerTime().Subtract(requestDate).TotalMinutes > 1) {
				result.ErrorMsg = "Invalid Access";
				return result;
			}

			PersistenceManager oPM = WebApiApplication.UserPM;
			if (oPM == null) {
				result.ErrorMsg = "Could not connect to database";
				return result;

			}

			Drivers driver = Drivers.GetDriver(oPM, info.DriverID);
			driver.ByPassValidation = true;		// bypassing validation as we are only changing the vehicle no
			if (driver.AffiliateID != info.AffiliateID) {
				result.ErrorMsg = "Account Error";
				return result;
			}

			driver.VehicleNo = info.VehicleNo;
			driver.Save();

			result.VehicleNo = driver.VehicleNo;
			if (!result.VehicleNo.IsNullOrEmpty()) {
				Vehicles vehiRec = Vehicles.GetVehicle(driver.MyAffiliate, driver.VehicleNo);
				if (!vehiRec.IsNullEntity) {
					if (vehiRec.Affiliate.AffiliateDriverDefaults.CCSiDispatch && vehiRec.Rfid.IsNullOrEmpty()) {
						//UpdateCCSi(vehiRec, driver.Cell);
					}
					result.VehicleID = vehiRec.VehicleID;
					result.VehicleKeyCode = vehiRec.KeyCode;
					result.VehicleRFID = vehiRec.Rfid;
				}
			}

			return result;

		}
	}
}
