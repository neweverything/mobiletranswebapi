﻿using System;

namespace MobileTransWebAPI.Models {
	public class DriverProfile {
		public long DriverID { get; set; }
		public long AffiliateID { get; set; }
		public long? AffiliateDriverID { get; set; }

		public string DriverName { get; set; }
		public string DriverLastName { get; set; }
		public string FleetName { get; set; }

		public string PhoneNo { get; set; }

		public string Street { get; set; }
		public string City { get; set; }
		public string State { get; set; }


		public string VehicleNo { get; set; }
		public string PIN { get; set; }

		public string VoidedCheck { get; set; }
		public string BankRouting { get; set; }
		public string BankAcctNo { get; set; }
		public string BankHolder { get; set; }

		public string DriversLicense { get; set; }

		public HackLicense DriverLicense { get; set; }

		public string Email { get; set; }
		public int MaxPassengers { get; set; }
		public string PickUpLocations { get; set; }
		public string TA_Number { get; set; }

		public HackLicense HackLicense { get; set; }
		public CabRideEngineDapper.DriverPricing Pricing { get; set; }

		public string AppName { get; set; }

		public string RequestKey { get; set; }

		public string SSN { get; set; }

		public string FirstName { get; set; }
		public string LastName { get; set; }
		public DateTime BirthDate { get; set; }
	}
}