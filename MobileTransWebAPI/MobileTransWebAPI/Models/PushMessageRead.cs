﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileTransWebAPI.Models {
	public class PushMessageRead {
		public long DriverID { get; set; }
		public int PushMessageID { get; set; }
		public string Key { get; set; }
	}
}