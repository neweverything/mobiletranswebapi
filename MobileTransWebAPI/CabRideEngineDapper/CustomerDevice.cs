﻿using Dapper;
using System.Data.SqlClient;

namespace CabRideEngineDapper {
    public partial class CustomerDevice {

        public static CustomerDevice GetDeviceByCellPhone(SqlConnection pConn, string pCell) {
            string sql = @"SELECT  *
                            FROM    dbo.CustomerDevice
                            WHERE   PassengerCell = @CellPhone";

            return pConn.Get<CustomerDevice>(sql, new { CellPhone = pCell });
        }

    }
}
