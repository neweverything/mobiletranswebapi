﻿using CabRideEngineDapper.Utils;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using TaxiPassCommon;

namespace CabRideEngineDapper {
    public partial class DriverPaymentVoucherImages {


        public static DriverPaymentVoucherImages Create(DriverPayments pDriverPayments) {
            DriverPaymentVoucherImages rec = pDriverPayments.DriverPaymentVoucherImages;
            if (rec.IsNullEntity || rec.DriverPaymentID == 0) {
                rec = DriverPaymentVoucherImagesTable.Create();
                rec.DriverPaymentID = pDriverPayments.DriverPaymentID;

                // Set the scan date for VoucherImages
                if (!pDriverPayments.ScanDate.HasValue) {
                    pDriverPayments.ScanDate = DateTime.Now;
                    pDriverPayments.Save();
                }
                //rec.Save();
            }
            return rec;
        }

        public override int Save() {
            using (var conn = SqlHelper.OpenSqlConnection()) {
                return Save(conn, CabRideDapperSettings.LoggedInUser);
            }
        }

        public static DriverPaymentVoucherImages GetRecord(long pDriverPaymentID) {
            string sql = "Select * FROM DriverPaymentVoucherImages WHERE DriverPaymentID = @DriverPaymentID";
            using (var conn = SqlHelper.OpenSqlConnection()) {
                return conn.Get<DriverPaymentVoucherImages>(sql, new { DriverPaymentID = pDriverPaymentID });
            }
        }


        [Editable(false)]
        public string MyImageURL {
            get {
                if (this.ImageURL.IsNullOrEmpty()) {
                    return "";
                }

                SystemDefaults def = SystemDefaults.GetDefaults();
                string url = def.VoucherImageURL;


                // We are no longer storing images at cckiosk.com - don't execute this code
                //if (this.DriverPayments.Platform == Platforms.CCKiosk.ToString() ) {
                //if (("" + this.ImageURL).Substring(4, 1) != @"/") {
                //    url = def.CCKioskVoucherImageURL;
                //}
                string imageURL = this.ImageURL.Replace('\\', '/');

                if (!url.EndsWith("/") && !this.ImageURL.StartsWith("/")) {
                    url += "/";
                }
                if (imageURL.StartsWith("http", StringComparison.CurrentCultureIgnoreCase)) {
                    url = imageURL;
                } else {
                    url += imageURL;
                }
                return url;
            }
        }

        //public override int? Save(string pModifiedBy) {
        //    using (var conn = SqlHelper.OpenSqlConnection()) {
        //        return Save(conn, pModifiedBy);
        //    }
        //}

        public override int Save(System.Data.SqlClient.SqlConnection pConn, string pModifiedBy) {
            if (IsNewRecord) {
                this.DriverPaymentVoucherImageID = NextID.GetNextKey(DriverPaymentVoucherImages.TableDef.TABLE_NAME);
            } else {
                RowVersion++;
            }
            return base.Save(pConn, pModifiedBy);
        }


        public static List<DriverPaymentVoucherImages> GetAllValidUnMatched(SqlConnection pConn) {
            string sql = @"SELECT  *
                            FROM    DriverPaymentVoucherImages WITH (NOLOCK)
                            INNER JOIN DriverPayments ON DriverPaymentVoucherImages.DriverPaymentID = DriverPayments.DriverPaymentID
                            WHERE   Matched = 0
                                    AND Invalid = 0
                                    AND DriverPayments.Platform NOT IN ('CCKiosk', 'NexStep')
                                    AND DriverPaymentVoucherImageID NOT IN (SELECT  DriverPaymentVoucherImageID
                                                                            FROM    dbo.MatcherRowLock WITH (NOLOCK)
                                                                            WHERE   (DateLocked >= CASE WHEN MatcherRowLock.ExternallyProcessed = 1 THEN DATEADD(HOUR, -2, GETDATE())
                                                                                                        ELSE DATEADD(mi, -15, GETDATE())
                                                                                                   END)) ";

            return pConn.Query<DriverPaymentVoucherImages>(sql).ToList();
        }

        public static List<DriverPaymentVoucherImages> GetAllInValid(SqlConnection pConn) {
            string sql = @"SELECT  *
                            FROM    dbo.DriverPaymentVoucherImages
                            WHERE   Matched = 0
                                    AND Invalid = 1";
            return pConn.Query<DriverPaymentVoucherImages>(sql).ToList();
        }
    }
}
