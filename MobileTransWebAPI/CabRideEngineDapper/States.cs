﻿using CabRideEngineDapper.Utils;
//using System.ComponentModel.DataAnnotations;

using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using TaxiPassCommon;

namespace CabRideEngineDapper {
	public partial class States {

		#region Save
		public override int Save(string pModifiedBy) {
			//using (var conn = SqlHelper.OpenSqlConnection()) {
			//	if (IsNewRecord) {
			//		ModifiedBy = pModifiedBy;
			//		ModifiedDate = DateTime.UtcNow;

			//		return conn.Insert(this);
			//	} else {
			//		ModifiedBy = pModifiedBy;
			//		ModifiedDate = DateTime.UtcNow;
			//		return conn.Update(this);
			//	}
			//}
			return base.Save(pModifiedBy);
		}

		#endregion

		[Key]
		public override string State {
			get {
				return base.State;
			}
			set {
				base.State = value;
			}
		}

		public static States GetByStateCode(SqlConnection pConn, string pState) {
			//where Name = @Name", new {Name = new DbString { Value = "abcde", IsFixedLength = true, Length = 10, IsAnsi = true });
			string sql = "SELECT * FROM States WHERE State = @State";
			DynamicParameters param = new DynamicParameters();
			param.Add("State", pState);

			//using (var conn = SqlHelper.OpenSqlConnection()) {
			return pConn.Query<States>(sql, param).DefaultIfEmpty(States.GetNullEntity()).FirstOrDefault();
			//}
		}


		public static States GetByStateName(string pStateName) {
			using (var conn = SqlHelper.OpenSqlConnection()) {
				return GetByStateName(conn, pStateName);
			}
		}

		public static States GetByStateName(SqlConnection pConn, string pStateName) {
			string sql = @"SELECT * FROM States
							WHERE StateName = @StateName";

			return pConn.Get<States>(sql, new { StateName = pStateName });
		}


		// ****************************************************
		// Get all states
		// ****************************************************
		public static List<States> GetStatesByCountry(SqlConnection pConn, string pCountryCode) {
			if (pCountryCode.IsNullOrEmpty()) {
				pCountryCode = Countries.GetDefaultCountry().Country;
			}

			List<States> oStates = GetData(pConn, pCountryCode, "");
			return oStates;
		}

		// ****************************************************
		// Call the acutal "Get" proc
		// ****************************************************
		private static List<States> GetData(SqlConnection pConn, string pCountryCode, string pStateCodeOrName) {
			String sql = "usp_StatesGetByCountry ";
			DynamicParameters param = new DynamicParameters();
			if (!pCountryCode.IsNullOrEmpty()) {
				param.Add("Country", pCountryCode);
			}
			if (!pStateCodeOrName.IsNullOrEmpty()) {
				param.Add("StateName", pStateCodeOrName);
			}
			return pConn.Query<States>(sql, param, commandType: CommandType.StoredProcedure).ToList();
		}


		internal static States GetStateForCountry(SqlConnection pConn, string pCountryCode, string pState) {
			return GetData(pConn, pCountryCode, pState).DefaultIfEmpty(States.GetNullEntity()).FirstOrDefault();
		}


		public static List<States> GetStatesForDropDown(SqlConnection pConn, string pCountryCode) {
			if (pCountryCode.IsNullOrEmpty()) {
				pCountryCode = Countries.GetDefaultCountry().Country;
			}

			string sql = @"SELECT State, StateName FROM dbo.States
                            WHERE Country = @Country
                            ORDER BY StateName";

			return pConn.Query<States>(sql, new { Country = pCountryCode }).ToList();
		}

		public static List<States> GetStatesForDropDown(string pCountryCode = "") {
			if (pCountryCode.IsNullOrEmpty()) {
				pCountryCode = Countries.GetDefaultCountry().Country;
			}

			using (var conn = SqlHelper.OpenSqlConnection()) {
				return GetStatesForDropDown(conn, pCountryCode);
			}
		}
	}
}
