﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Dapper;
using System.Data.SqlClient;
using CabRideEngineDapper.Utils;

namespace CabRideEngineDapper {
	public partial class SystemDefaults {

		// cache this data as info doesn't change that often
		private static SystemDefaults mSystemDefaults;
		private static DateTime mLastUpdated;

		public static SystemDefaults GetDefaults() {
			using (var conn = SqlHelper.OpenSqlConnection()) {
				return GetDefaults(conn);
			}
		}

		public static SystemDefaults GetDefaults(SqlConnection pConn) {
			if (mSystemDefaults == null || DateTime.Now.Subtract(mLastUpdated).TotalMinutes > 15) {
				mSystemDefaults = pConn.Query<SystemDefaults>("Select * FROM SystemDefaults").FirstOrDefault();
				mLastUpdated = DateTime.Now;
			}
			return mSystemDefaults;
		}
	}
}
