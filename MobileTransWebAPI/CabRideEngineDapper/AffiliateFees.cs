﻿using CabRideEngineDapper.Utils;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;


namespace CabRideEngineDapper {
    public partial class AffiliateFees {

        public static AffiliateFees Create(Affiliate pAffiliate) {
            AffiliateFees oFee = AffiliateFeesTable.Create();

            //pAffiliate.PersistenceManager.GenerateId(oFee, AffiliateFeesIDEntityColumn);
            oFee.AffiliateID = pAffiliate.AffiliateID;

            return oFee;
        }


        public override int Save(string pModifiedBy) {
            ModifiedDate = DateTime.UtcNow;
            ModifiedBy = string.Format("{0} | {1}", pModifiedBy, CabRideDapperSettings.GetAppName());
            if (IsNewRecord) {
                this.AffiliateFeesID = NextID.GetNextKey(AffiliateFees.TableDef.TABLE_NAME);
            } else {
                RowVersion++;
            }

            return base.Save(pModifiedBy);
        }


        public static List<AffiliateFees> GetFees(long pAffiliateID) {
            using (var conn = SqlHelper.OpenSqlConnection()) {
                return conn.GetList<AffiliateFees>(new { AffiliateID = pAffiliateID }).ToList();
            }

        }
    }
}
