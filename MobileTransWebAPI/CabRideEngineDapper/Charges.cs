﻿using CabRideEngineDapper.Utils;
using Dapper;
using System;
using System.Data.SqlClient;

namespace CabRideEngineDapper {
    public partial class Charges {

        public static Charges Create(long pReservationID) {

            Charges oCharge = ChargesTable.Create();

            oCharge.ReservationID = pReservationID;
            oCharge.DisplayOrder = 0;
            return oCharge;
        }

        #region Save
        public new bool Save() {
            using (var conn = SqlHelper.OpenSqlConnection()) {
                return Save(conn);
            }
        }

        public bool Save(SqlConnection pConn) {
            return Save(pConn, CabRideDapperSettings.GetAppName());
        }

        public new bool Save(SqlConnection pConn, string pModifiedBy) {
            bool result = false;
            if (IsNewRecord) {
                ModifiedDate = DateTime.UtcNow;
                ModifiedBy = pModifiedBy;
                this.ChargeID = NextID.GetNextKey(Charges.TableDef.TABLE_NAME);

                int? id = pConn.Insert(this);
                result = true;
            } else {
                ModifiedBy = pModifiedBy;
                ModifiedDate = DateTime.UtcNow;
                result = pConn.Update(this) > 0;
            }
            return result;
        }
        #endregion


        public static Charges GetCharge(Reservations pReserve, string pDescription) {
            using (var conn = SqlHelper.OpenSqlConnection()) {
                string sql = @"SELECT  *
                                FROM    Charges
                                WHERE   ReservationID = @ReservationID
                                        AND Description = @Description";
                DynamicParameters parms = new DynamicParameters();
                parms.Add("ReservationID", pReserve.ReservationID);
                parms.Add("Description", pDescription);
                return conn.Get<Charges>(sql, parms);
            }
        }

    }
}
