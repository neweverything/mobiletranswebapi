﻿using AuthorizeNet.Api.Contracts.V1;
using AuthorizeNet.Api.Controllers;
using AuthorizeNet.Api.Controllers.Bases;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Net;
using TaxiPassCommon.Banking.Models;

namespace TaxiPassCommon.Banking {
	// named like this to prevent issues with Authorize.Net library
	public class AuthorizNet : IProcessor {

		private AuthorizeNetCredentials mCredentials;

		public AuthorizNet(AuthorizeNetCredentials pCredentials) {
			ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls;
			if (pCredentials.Timeout == 0) {
				pCredentials.Timeout = 30;
			}
			mCredentials = pCredentials;

		}

		public ProcessorResponse ChargeAuth(CCRequestRefNo pRequest) {
			SetLoginInfo(pRequest.TestCard);

			var transactionRequest = new transactionRequestType {
				transactionType = transactionTypeEnum.priorAuthCaptureTransaction.ToString(),    // capture prior only
				amount = pRequest.Amount,
				refTransId = pRequest.RefNum,
				//employeeId = string.Format("D:{0}, AD{1}", pRequest.DriverID, pRequest.AffiliateDriverID),
				poNumber = pRequest.VoucherNo,
				//order = new orderType() {
				//    invoiceNumber = pRequest.VoucherNo
				//}
			};

			var request = new createTransactionRequest { transactionRequest = transactionRequest };

			// instantiate the contoller that will call the service
			var controller = new createTransactionController(request);
			controller.Execute();

			// get the response from the service (errors contained if any)
			var response = controller.GetApiResponse();
			return ProcessResponse(response, pRequest.Amount, pRequest.TestCard, TrxTypes.DelayedCapture, "");
		}

		public ProcessorResponse ChargeCard(CCRequest pRequest) {
			createTransactionResponse response = new createTransactionResponse();
			if (pRequest.CardData.CardType.Equals("GooglePay")) {
				string url = mCredentials.ProdURL;
				if (pRequest.CardData.TestCard) {
					url = mCredentials.TestURL;
				}
				RestClient service = new RestClient(url);
				RestRequest request = new RestRequest(Method.POST);

				ChargeRequest chgRequest = new ChargeRequest();
				chgRequest.createTransactionRequest = new CreateTransactionRequest();
				chgRequest.createTransactionRequest.merchantAuthentication = new MerchantAuthentication();
				chgRequest.createTransactionRequest.transactionRequest = new TransactionRequest();
				chgRequest.createTransactionRequest.transactionRequest.payment = new Payment();
				chgRequest.createTransactionRequest.transactionRequest.payment.opaqueData = new OpaqueData();


				if (pRequest.CardData.TestCard) {
					chgRequest.createTransactionRequest.merchantAuthentication.name = mCredentials.TestUserName; // "9b443yDKk";
					chgRequest.createTransactionRequest.merchantAuthentication.transactionKey = mCredentials.TestPassword; // "4D6nmH22dvwuC55C";
				} else {
					chgRequest.createTransactionRequest.merchantAuthentication.name = mCredentials.UserName;
					chgRequest.createTransactionRequest.merchantAuthentication.transactionKey = mCredentials.Password;
				}
				chgRequest.createTransactionRequest.transactionRequest.transactionType = "authCaptureTransaction";
				chgRequest.createTransactionRequest.transactionRequest.amount = pRequest.Amount.ToString();
				chgRequest.createTransactionRequest.transactionRequest.payment.opaqueData.dataDescriptor = "COMMON.GOOGLE.INAPP.PAYMENT";
				chgRequest.createTransactionRequest.transactionRequest.payment.opaqueData.dataValue = pRequest.CardData.Track2;
				//string js = Newtonsoft.Json.JsonConvert.SerializeObject(chgRequest);
				//request.AddJsonBody(js);
				request.AddJsonBody(chgRequest);
				request.Timeout = 10000;
				var temp = service.Execute<AuthNetTransResponse>(request);
				int start = temp.Content.IndexOf("{");
				string content = temp.Content.Substring(start);
				AuthNetTransResponse resp = JsonConvert.DeserializeObject<AuthNetTransResponse>(content);
				response.transactionResponse = JsonConvert.DeserializeObject<transactionResponse>(JsonConvert.SerializeObject(resp.transactionResponse));
				response.messages = JsonConvert.DeserializeObject<messagesType>(JsonConvert.SerializeObject(resp.messages));
				//response.refId = temp.Data.transactionResponse.refTransID;
				pRequest.CardData.CardNo = $"{response.transactionResponse.accountType} {response.transactionResponse.accountNumber}";
			} else {
				paymentType paymentType = InitService(pRequest);

				var transactionRequest = new transactionRequestType {
					transactionType = transactionTypeEnum.authCaptureTransaction.ToString(),   // charge the card
					amount = pRequest.Amount,
					payment = paymentType,
					employeeId = string.Format("D:{0}, AD{1}", pRequest.CardData.DriverID, pRequest.CardData.AffiliateDriverID),
					poNumber = pRequest.VoucherNo,
					order = new orderType() {
						invoiceNumber = pRequest.VoucherNo,
						//description = pRequest.Comments
					}
				};

				var request = new createTransactionRequest { transactionRequest = transactionRequest };

				// instantiate the contoller that will call the service
				var controller = new createTransactionController(request);
				controller.Execute();

				// get the response from the service (errors contained if any)
				response = controller.GetApiResponse();
			}
			return ProcessResponse(response, pRequest.Amount, pRequest.CardData.TestCard, TrxTypes.Sale, "");

		}



		public string GetTransactionHistory(DateTime pStart, DateTime pEnd) {
			throw new NotImplementedException();
		}

		public ProcessorResponse OpenCredit(CCRequest pRequest) {
			throw new NotImplementedException();
		}

		public ProcessorResponse PreAuthCard(CCRequest pRequest) {
			paymentType paymentType = InitService(pRequest);

			var transactionRequest = new transactionRequestType {
				transactionType = transactionTypeEnum.authOnlyTransaction.ToString(),   // preauth the card
				amount = pRequest.Amount,
				payment = paymentType,
				employeeId = string.Format("D:{0}, AD{1}", pRequest.CardData.DriverID, pRequest.CardData.AffiliateDriverID),
				poNumber = pRequest.VoucherNo,
				order = new orderType() {
					invoiceNumber = pRequest.VoucherNo,
					//description = pRequest.Comments
				}
			};

			var request = new createTransactionRequest { transactionRequest = transactionRequest };

			// instantiate the contoller that will call the service
			createTransactionController controller = new createTransactionController(request);
			controller.Execute();
			//Console.WriteLine(controller);

			// get the response from the service (errors contained if any)
			var response = controller.GetApiResponse();

			return ProcessResponse(response, pRequest.Amount, pRequest.CardData.TestCard, TrxTypes.Authorization, "");
		}

		public ProcessorResponse Refund(CCRequestRefNo pRequest) {
			SetLoginInfo(pRequest.TestCard);

			var creditCard = new creditCardType {
				cardNumber = pRequest.CardNoLast4.Right(4),
				expirationDate = "XXXX"
			};

			//standard api call to retrieve response
			var paymentType = new paymentType { Item = creditCard };

			var transactionRequest = new transactionRequestType {
				transactionType = transactionTypeEnum.refundTransaction.ToString(),
				amount = Math.Abs(pRequest.Amount),
				refTransId = pRequest.RefNum,
				poNumber = pRequest.VoucherNo,
				payment = paymentType,
				order = new orderType() {
					invoiceNumber = pRequest.VoucherNo
				}
			};

			var request = new createTransactionRequest { transactionRequest = transactionRequest };

			// instantiate the contoller that will call the service
			var controller = new createTransactionController(request);
			controller.Execute();

			// get the response from the service (errors contained if any)
			var response = controller.GetApiResponse();
			return ProcessResponse(response, pRequest.Amount, pRequest.TestCard, TrxTypes.Credit, "");
		}

		public bool Void(CCRequestRefNo pRequest) {
			SetLoginInfo(pRequest.TestCard);

			var creditCard = new creditCardType {
				cardNumber = pRequest.CardNoLast4.Right(4),
				expirationDate = "XXXX"
			};

			//standard api call to retrieve response
			var paymentType = new paymentType { Item = creditCard };

			var transactionRequest = new transactionRequestType {
				transactionType = transactionTypeEnum.voidTransaction.ToString(),
				amount = Math.Abs(pRequest.Amount),
				refTransId = pRequest.RefNum,
				poNumber = pRequest.VoucherNo,
				payment = paymentType
			};

			var request = new createTransactionRequest { transactionRequest = transactionRequest };

			// instantiate the contoller that will call the service
			var controller = new createTransactionController(request);
			controller.Execute();

			// get the response from the service (errors contained if any)
			var response = controller.GetApiResponse();
			//Console.WriteLine(response);
			return response.messages.resultCode == messageTypeEnum.Ok;
		}

		private paymentType InitService(CCRequest pRequest) {
			SetLoginInfo(pRequest.CardData.TestCard);

			creditCardTrackType swipe = new creditCardTrackType();
			if (!pRequest.CardData.Track2.IsNullOrEmpty()) {
				swipe.Item = pRequest.CardData.Track2;
			}


			creditCardType creditCard = new creditCardType {
				cardNumber = pRequest.CardData.CardNo,
				expirationDate = pRequest.CardData.CardExpiryAsMMYY,
			};

			if (pRequest.CardData.CardType.Equals("GooglePay")) {
				creditCard = new creditCardType {
					isPaymentToken = true,
					cryptogram = pRequest.CardData.Track2,
					tokenRequestorName = "COMMON.GOOGLE.INAPP.PAYMENT",
				};
				pRequest.CardData.Track2 = "";
			}


			if (!pRequest.CardData.CVV.IsNullOrEmpty()) {
				creditCard.cardCode = pRequest.CardData.CVV;
			}


			//standard api call to retrieve response
			var paymentType = new paymentType();
			if (pRequest.CardData.Track2.IsNullOrEmpty()) {
				paymentType.Item = creditCard;
			} else {
				paymentType.Item = swipe;
			}

			return paymentType;
		}

		private bankAccountType InitService(CheckRequest pRequest) {
			SetLoginInfo(pRequest.CheckData.TestAccount);


			//standard api call to retrieve response
			var bankAccount = new bankAccountType() {
				accountNumber = pRequest.CheckData.AccountNo,
				accountType = pRequest.CheckData.AccountType.ToString().ToEnum<bankAccountTypeEnum>(),
				routingNumber = pRequest.CheckData.RoutingNo,
				nameOnAccount = pRequest.CheckData.CustomerName
			};


			return bankAccount;
		}

		private void SetLoginInfo(bool pTestCard) {
			ApiOperationBase<ANetApiRequest, ANetApiResponse>.RunEnvironment = pTestCard ? AuthorizeNet.Environment.SANDBOX : AuthorizeNet.Environment.PRODUCTION;


			// define the merchant information (authentication / transaction id)
			ApiOperationBase<ANetApiRequest, ANetApiResponse>.MerchantAuthentication = new merchantAuthenticationType() {
				name = pTestCard ? mCredentials.TestUserName : mCredentials.UserName,
				ItemElementName = ItemChoiceType.transactionKey,
				Item = pTestCard ? mCredentials.TestPassword : mCredentials.Password,
			};
		}

		private ProcessorResponse ProcessResponse(createTransactionResponse pResponse, decimal pAmount, bool pTestCard, string pTransType, string pSoftDescriptor) {
			ProcessorResponse oResult = new ProcessorResponse();

			//oResult.GatewayResponse = JSON.JsonHelper.JsonSerialize(pResponse);

			oResult.TestCard = pTestCard;
			oResult.VerisignAccountID = mCredentials.VerisignAccountID;
			oResult.MerchantID = mCredentials.MerchantID;
			oResult.CreditCardProcessor = mCredentials.MyGateway();

			oResult.Result = ((int)pResponse.messages.resultCode).ToString();
			oResult.Reference = pResponse.transactionResponse.transId;
			if (pResponse.transactionResponse.messages != null) {
				oResult.Message = pResponse.transactionResponse.messages[0].description.ToLower().Contains("approved") ? "Approved" : pResponse.transactionResponse.messages[0].description;
			} else {
				if (pResponse.transactionResponse.errors == null) {
					if (pResponse.messages.message.Length > 0) {
						oResult.Message = pResponse.messages.message[0].text;
						if (pResponse.messages.message[0].code.Length < 6 && !pResponse.messages.message[0].code.Equals("0")) {
							oResult.Result = pResponse.messages.message[0].code;
						}
					}
				} else {
					oResult.Message = pResponse.transactionResponse.errors[0].errorText;
					oResult.Result = pResponse.transactionResponse.errors[0].errorCode;
				}
			}
			oResult.Amount = pAmount;
			if (oResult.Result.IsNullOrEmpty()) {
				oResult.Result = "0";
			}
			//fixed for Chase results
			if (oResult.Result != "0" && oResult.Message.StartsWith("Approve", StringComparison.CurrentCultureIgnoreCase)) {
				oResult.Result = "0";
			}
			if (oResult.Result == "346") {
				oResult.Message = string.Format("{0} ({1})", oResult.Message, pSoftDescriptor);
			}

			oResult.AuthCode = pResponse.transactionResponse.authCode;
			oResult.AVS = pResponse.transactionResponse.avsResultCode;
			oResult.AVSZip = pResponse.transactionResponse.cavvResultCode;
			oResult.CVV2Match = pResponse.transactionResponse.cvvResultCode;


			oResult.TransType = pTransType;
			oResult.TransDate = DateTime.Now;

			oResult.BillingDescriptor = pSoftDescriptor;
			return oResult;
		}

		public ProcessorResponse AvsCheck(CCRequest pRequest) {
			throw new NotImplementedException();
		}

		public ProcessorResponse ProcessCheck(CheckRequest pRequest) {
			var bankAccount = InitService(pRequest);

			var transactionRequest = new transactionRequestType {
				transactionType = transactionTypeEnum.authCaptureTransaction.ToString(),   // charge the card
				amount = pRequest.Amount,
				payment = new paymentType(),
				employeeId = string.Format("D:{0}, AD{1}", pRequest.CheckData.DriverID, pRequest.CheckData.AffiliateDriverID),
				poNumber = pRequest.VoucherNo,

				order = new orderType() {
					invoiceNumber = pRequest.VoucherNo,
					//description = pRequest.Comments
				}
			};

			var request = new createTransactionRequest { transactionRequest = transactionRequest };

			// instantiate the contoller that will call the service
			var controller = new createTransactionController(request);
			controller.Execute();

			// get the response from the service (errors contained if any)
			var response = controller.GetApiResponse();

			return ProcessResponse(response, pRequest.Amount, pRequest.CheckData.TestAccount, TrxTypes.Sale, "");

		}

		public ProcessorResponse RefundCheck(CheckRequest pRequest) {
			throw new NotImplementedException();
		}

		public List<CheckStatus> GetCheckTransactionHistory(DateTime pStart, DateTime pEnd) {
			throw new NotImplementedException();
		}


		public class ChargeRequest {
			public CreateTransactionRequest createTransactionRequest { get; set; }
		}
		public class CreateTransactionRequest {
			public MerchantAuthentication merchantAuthentication { get; set; }
			public TransactionRequest transactionRequest { get; set; }
		}
		public class MerchantAuthentication {
			public string name { get; set; }
			public string transactionKey { get; set; }
		}
		public class TransactionRequest {
			public string transactionType { get; set; }
			public string amount { get; set; }
			public Payment payment { get; set; }
		}
		public class Payment {
			public OpaqueData opaqueData { get; set; }
		}
		public class OpaqueData {
			public string dataDescriptor { get; set; }
			public string dataValue { get; set; }
		}
	}
}
