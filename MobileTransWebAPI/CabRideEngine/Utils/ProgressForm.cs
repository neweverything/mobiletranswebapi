using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CabRideEngine.Utils {
    public partial class ProgressForm : Form {

        public bool Cancel = false;

        public ProgressForm() {
            InitializeComponent();
        }

        private void buttonCancel_Click(object sender, EventArgs e) {
            Cancel = true;
        }

        public string Message {
            get {
                return labelMessage.Text;
            }
            set {
                labelMessage.Text = value;
            }
        }

        public int Min {
            get {
                return progressBar1.Minimum;
            }
            set {
                progressBar1.Minimum = value;
            }
        }

        public int Max {
            get {
                return progressBar1.Maximum;
            }
            set {
                progressBar1.Maximum = value;
            }
        }

        public int Value {
            get {
                return progressBar1.Value;
            }
            set {
                progressBar1.Value = value;
                this.Refresh();
            }
        }
    }
}