﻿using CabRideEngineDapper.Utils;
using Dapper;
using System;
using System.Data.SqlClient;
using TaxiPassCommon;

namespace CabRideEngineDapper {
	public partial class AffiliateDriverLicenses {

		public static AffiliateDriverLicenses Create(SqlConnection pConn, AffiliateDrivers pAffiliateDrivers) {


			AffiliateDriverLicenses rec = pConn.Get<AffiliateDriverLicenses>(pAffiliateDrivers.AffiliateDriverID);
			if (rec.IsNullEntity) {
				rec = AffiliateDriverLicensesTable.Create();
				rec.AffiliateDriverID = pAffiliateDrivers.AffiliateDriverID;
			}
			return rec;
		}

		public static AffiliateDriverLicenses GetOrCreate(SqlConnection pConn, long pAffiliateDriverID) {


			AffiliateDriverLicenses rec = GetLicense(pConn, pAffiliateDriverID);
			if (rec.IsNullEntity) {
				rec = AffiliateDriverLicensesTable.Create();
				rec.AffiliateDriverID = pAffiliateDriverID;
			}
			return rec;
		}


		public override int Save() {
			return Save(CabRideDapperSettings.LoggedInUser);
		}


		public override int Save(string pModifiedBy) {
			ModifiedDate = DateTime.UtcNow;
			ModifiedBy = string.Format("{0} | {1}", pModifiedBy, CabRideDapperSettings.GetAppName());
			if (IsNewRecord) {
				//this.AffiliateDriverLicenseID = NextID.GetNextKey(AffiliateDriverLicenses.TableDef.TABLE_NAME);
			} else {
				RowVersion++;
			}

			return base.Save(pModifiedBy);
		}



		public static AffiliateDriverLicenses GetLicense(SqlConnection pConn, long pAffiliateDriverID) {
			string sql = @"SELECT * FROM AffiliateDriverLicenses
                            WHERE AffiliateDriverID = @AffiliateDriverID";

			return pConn.Get<AffiliateDriverLicenses>(sql, new { AffiliateDriverID = pAffiliateDriverID });
		}

		[Editable(false)]
		public string MyImageURL {
			get {
				using (var conn = SqlHelper.OpenSqlConnection()) {
					SystemDefaults def = SystemDefaults.GetDefaults();
					string url = def.LicenseImageURL;
					if (this.LicenseURL.IsNullOrEmpty()) {
						return "";
					}
					if (LicenseURL.StartsWith("http", StringComparison.CurrentCultureIgnoreCase)) {
						return LicenseURL;
					}
					string imageURL = this.LicenseURL.Replace('\\', '/');
					if (!url.EndsWith("/") && !imageURL.StartsWith("/")) {
						url += "/";
					}
					url += imageURL;
					return url;
				}
			}
		}
	}
}
