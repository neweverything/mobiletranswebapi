﻿using CabRideEngineDapper.Utils;
using Dapper;
using System.Collections.Generic;
using System.Linq;

namespace CabRideEngineDapper {
	public partial class AccountCustomField {

		public static AccountCustomField Create(Accounts pAccount) {
			AccountCustomField rec = Create();
			rec.AccountID = pAccount.AccountID;
			rec.FieldType = "String";
			rec.Visible = true;
			return rec;
		}

		public static AccountCustomField Create(long pAccountID) {
			AccountCustomField rec = Create();
			rec.AccountID = pAccountID;
			rec.FieldType = "String";
			rec.Visible = true;
			return rec;
		}

		public static List<AccountCustomField> GetList(long pAccountID) {
			string sql = @"SELECT   *
							FROM     AccountCustomField
							WHERE    AccountID = @AccountID
							ORDER BY DisplayOrder";

			using (var conn = SqlHelper.OpenSqlConnection()) {
				return conn.Query<AccountCustomField>(sql, new { AccountID = pAccountID }).ToList();
			}

		}

		public static List<AccountCustomField> GetSearchableFields(long pAccountID) {
			string sql = @"SELECT *
							FROM AccountCustomField
							WHERE AccountID = @AccountID
								  AND Searchable = 1
							ORDER BY DisplayOrder";

			using (var conn = SqlHelper.OpenSqlConnection()) {
				return conn.Query<AccountCustomField>(sql, new { AccountID = pAccountID }).ToList();
			}

		}
	}
}
