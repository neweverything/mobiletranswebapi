﻿using CabRideEngineDapper.Enums;
using CabRideEngineDapper.Utils;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using TaxiPassCommon;
using TaxiPassCommon.Banking;

namespace CabRideEngineDapper {
	public partial class TaxiPassRedeemerAccounts {


		#region Related Tables

		private Markets mMarkets;
		public Markets Markets {
			get {
				if (mMarkets == null) {
					mMarkets = Markets.GetMarket(this.MarketID);
				}
				return mMarkets;
			}
		}

		private IEnumerable<TaxiPassRedeemers> mTaxiPassRedeemerses;
		public IEnumerable<TaxiPassRedeemers> TaxiPassRedeemerses {
			get {
				if (mTaxiPassRedeemerses == null) {
					mTaxiPassRedeemerses = TaxiPassRedeemers.GetRedeemers(this);
				}
				return mTaxiPassRedeemerses;
			}
		}

		private IEnumerable<RedeemerFees> mRedeemerFeeses;
		public IEnumerable<RedeemerFees> RedeemerFeeses {
			get {
				if (mRedeemerFeeses == null) {
					mRedeemerFeeses = RedeemerFees.GetFees(this);
				}
				return mRedeemerFeeses;
			}
		}

		private MerchantInfo mMerchantInfo;
		public MerchantInfo MerchantInfo {
			get {
				if (!this.MerchantID.HasValue) {
					return null;
				}
				if (mMerchantInfo == null) {
					mMerchantInfo = MerchantInfo.GetMerchant(this.MerchantID.Value);
				}
				return mMerchantInfo;
			}
		}

		private List<TaxiPassRedeemerLocations> mRedeemerLocations;
		public List<TaxiPassRedeemerLocations> TaxiPassRedeemerLocationses {
			get {
				if (mRedeemerLocations == null || mRedeemerLocations.Count == 0) {
					mRedeemerLocations = TaxiPassRedeemerLocations.GetLocations(this);
				}
				return mRedeemerLocations;
			}
		}

		#endregion

		public static TaxiPassRedeemerAccounts GetBySubDomain(string pSubDomain) {
			using (var conn = SqlHelper.OpenSqlConnection()) {
				return GetBySubDomain(conn, pSubDomain);
			}
		}

		public static IEnumerable<TaxiPassRedeemerAccounts> GetAll() {
			string sql = @"SELECT * FROM TaxiPassRedeemerAccounts WITH (nolock) 
                            ORDER BY CompanyName";

			using (var conn = SqlHelper.OpenSqlConnection()) {
				return conn.Query<TaxiPassRedeemerAccounts>(sql);
			}

		}

		public static TaxiPassRedeemerAccounts GetBySubDomain(SqlConnection pConn, string pSubDomain) {
			string sql = "SELECT * FROM TaxiPassRedeemerAccounts WITH (nolock) WHERE Subdomain = @Subdomain";
			using (var conn = SqlHelper.OpenSqlConnection()) {
				return conn.Query<TaxiPassRedeemerAccounts>(sql, new { Subdomain = pSubDomain }).DefaultIfEmpty(TaxiPassRedeemerAccounts.GetNullEntity()).FirstOrDefault();
			}
		}


		public static TaxiPassRedeemerAccounts GetAccount(long pAccountID) {
			//string sql = "SELECT * FROM TaxiPassRedeemerAccounts WITH (nolock) WHERE Subdomain = @Subdomain";
			using (var conn = SqlHelper.OpenSqlConnection()) {
				return conn.Get<TaxiPassRedeemerAccounts>(pAccountID);
			}
		}

		[Editable(false)]
		public decimal GetCardNoVerificationAmt {
			get {
				decimal amt = this.CardNoVerificationAmount;

				if (amt < 1) {
					amt = SystemDefaults.GetDefaults().CardNoVerificationAmount;
					CardNoVerificationAmount = amt;
				}
				return amt;
			}
		}




		public static List<TaxiPassRedeemerAccounts> GetForDropDown(SqlConnection pConn, bool pInsertNullRecord = true) {
			string sql = @"SELECT TaxiPassRedeemerAccountID, CompanyName FROM dbo.TaxiPassRedeemerAccounts
							ORDER BY CompanyName";
			List<TaxiPassRedeemerAccounts> recList = pConn.Query<TaxiPassRedeemerAccounts>(sql).ToList();
			if (pInsertNullRecord) {
				recList.Insert(0, TaxiPassRedeemerAccounts.GetNullEntity());
			}
			return recList;
		}


		public PayInfo GetPayInfo() {
			if (this.BankAccountNumber.IsNullOrEmpty()) {
				return null;
			}
			PayInfo oPay = new PayInfo();
			oPay.NachaFile = this.SendNacha;
			oPay.BankAccountHolder = this.BankAccountHolder;
			oPay.BankAccountNumber = this.BankAccountNumber;
			oPay.BankAccountPersonal = this.BankAccountPersonal;
			oPay.BankAddress = this.BankAddress;
			oPay.BankCity = this.BankCity;
			oPay.BankCode = this.BankCode;
			oPay.BankState = this.BankState;
			oPay.BankSuite = this.BankSuite;
			oPay.BankZIPCode = this.BankZIPCode;
			if (this.MerchantInfo != null) {
				oPay.MyMerchant = new Merchant();
				oPay.MyMerchant.MerchantNo = this.MerchantInfo.MerchantNo;
			}
			oPay.PaidTo = "Redeemer: " + this.CompanyName;

			return oPay;
		}

		public Twilio.SMSMessage SendMsg(string pMsg) {
			using (var conn = SqlHelper.OpenSqlConnection()) {
				SystemDefaults oDef = SystemDefaults.GetDefaults(conn);

				string cell = this.Phone;

				string acctSid = oDef.TwilioAccountSID;
				string acctToken = oDef.TwilioAccountToken;
				string apiVersion = oDef.TwilioAPIVersion;

				if (this.EMailVoucherRedemption) {
					try {
						string mailTo = "";
						var cred = SystemDefaultsDict.GetEmailCreds(EMailReportFrom.Receipt);
						TaxiPassCommon.Mail.EMailMessage mail = new TaxiPassCommon.Mail.EMailMessage(cred.From, "", pMsg, cred.Password);
						foreach (TaxiPassRedeemers rec in this.TaxiPassRedeemerses) {
							if (rec.Admin) {
								mailTo += rec.email + ";";
							}
						}
						if (!mailTo.IsNullOrEmpty()) {
							mailTo = mailTo.Left(mailTo.Length - 1);
							mail.UseWebServiceToSendMail = Properties.Settings.Default.EMailViaWebService;
							mail.SendMail(mailTo);
						}
					} catch (Exception) {
					}
					return new Twilio.SMSMessage();
				}
				TaxiPassCommon.Mail.TwilioTexting twilio = new TaxiPassCommon.Mail.TwilioTexting(acctSid, acctToken, apiVersion, oDef.TwilioSMSNumber);
				return twilio.TwilioTextMessage(pMsg, cell);
			}
		}

		private List<long> mOnlyAffiliateList = null;
		[Editable(false)]
		public List<long> OnlyAffiliateList {
			get {
				if (mOnlyAffiliateList == null) {
					mOnlyAffiliateList = new List<long>();
					var affList = OnlyVouchersFromAffiliates.Split(',');
					foreach (string affId in affList) {
						mOnlyAffiliateList.Add(Convert.ToInt64(affId));
					}
				}
				return mOnlyAffiliateList;
			}
		}


		public static TaxiPassRedeemerAccounts GetByID(long pAcctID) {
			using (var conn = SqlHelper.OpenSqlConnection()) {
				return GetByID(conn, pAcctID);
			}
		}

		public static TaxiPassRedeemerAccounts GetByID(SqlConnection pConn, long pAcctID) {
			string sql = "SELECT * FROM TaxiPassRedeemerAccounts WHERE TaxiPassRedeemerAccountID = @AcctID";
			return pConn.Get<TaxiPassRedeemerAccounts>(sql, new { AcctID = pAcctID });
		}

		public static TaxiPassRedeemerAccounts GetByPhone(SqlConnection pConn, string pPhone) {
			string sql = "SELECT * FROM TaxiPassRedeemerAccounts WHERE Phone = @Phone";
			return pConn.Get<TaxiPassRedeemerAccounts>(sql, new { Phone = pPhone });
		}
	}
}
