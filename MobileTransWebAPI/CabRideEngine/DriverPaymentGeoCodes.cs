﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

using TaxiPassCommon;

namespace CabRideEngine {
	/// <summary>
	/// The developer class representing the DriverPaymentGeoCodes business object class.
	/// </summary>
	/// <remarks>
	/// <para>Place all application-specific business logic in this class.</para>
	/// <para>
	/// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
	/// and will not overwrite changes.
	/// </para>
	/// <para>
	/// This class inherits from the generated class, its companion in the pair of classes 
	/// defining a business object. Properties in the generated class may be overridden in 
	/// order to change the behavior or add business logic.
	/// </para>
	/// <para>
	/// This class is the final class in the inheritance chain for this business object. 
	/// It should not be inherited.
	/// </para>
	/// </remarks>
	[Serializable]
	public sealed partial class DriverPaymentGeoCodes : DriverPaymentGeoCodesDataRow {

		#region Constructors -- DO NOT MODIFY

		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new';
		// it is also required if the class is visible from a web service.
		// DO NOT REMOVE **
		private DriverPaymentGeoCodes() : this(null) { }

		/// <summary>
		/// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
		/// </summary>
		/// <param name="pRowBuilder"></param>
		public DriverPaymentGeoCodes(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static DriverPaymentGeoCodes Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      DriverPaymentGeoCodes aDriverPaymentGeoCodes = pManager.CreateEntity<DriverPaymentGeoCodes>();
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aDriverPaymentGeoCodes, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aDriverPaymentGeoCodes.AddToManager();
		//      return aDriverPaymentGeoCodes;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		// Add additional logic to your business object here...
		public static DriverPaymentGeoCodes Create(DriverPayments pDriverPayment) {
			DriverPaymentGeoCodes rec = null;
			try {
				rec = pDriverPayment.PersistenceManager.CreateEntity<DriverPaymentGeoCodes>();
				rec.DriverPaymentID = pDriverPayment.DriverPaymentID;
				rec.AddToManager();

			} catch (Exception ex) {
				throw ex;
			}
			return rec;
		}

		public void FillAddress() {

			// Fill Address is causing a slow down, will bail out if we do not have a passenger requesting a receipt.  Moving Address look to phone apps

			bool bailOut = SendReceiptTo.IsNullOrEmpty();

			// everything is 0 - Nothing to do - bail out
			if (bailOut || (StartLatitude.GetValueOrDefault(0) == 0 && StartLongitude.GetValueOrDefault(0) == 0 && Latitude.GetValueOrDefault(0) == 0 && Longitude.GetValueOrDefault(0) == 0)) {
				return;
			}

			SystemDefaultsDict dict = SystemDefaultsDict.GetByKey(this.PersistenceManager, "GeoCodes", "PopulateAddress", false);
			if (!Convert.ToBoolean(dict.ValueString)) {
				return;
			}

			// only fill address if we don't already have an address
			com.TaxiPass.GeoCodeWS.GeoCodeService oService;
			try {
				oService = new CabRideEngine.com.TaxiPass.GeoCodeWS.GeoCodeService();
			} catch {
				return;
			}
			// starting address of trip
			if (this.StartLatitude.GetValueOrDefault(0) != 0 && this.StartLongitude.GetValueOrDefault(0) != 0 && this.StartAddress.IsNullOrEmpty()) {
				try {
					com.TaxiPass.GeoCodeWS.Address oAddr = oService.GetAddressByGeoCode(this.StartLatitude.ToString(), this.StartLongitude.ToString());
					this.StartAddress = oAddr.FormattedAddress;
				} catch {
				}
			}
			// ending address of trip
			if (this.Latitude.GetValueOrDefault(0) != 0 && this.Longitude.GetValueOrDefault(0) != 0 && this.Address.IsNullOrEmpty()) {
				try {
					if (StartLatitude == Latitude && StartLongitude == Longitude) {
						Address = StartAddress;
					} else {
						com.TaxiPass.GeoCodeWS.Address oAddr = oService.GetAddressByGeoCode(this.Latitude.ToString(), this.Longitude.ToString());
						this.Address = oAddr.FormattedAddress;
					}
				} catch {
				}
			}
		}


		// *************************************************************************************************
		// return a list of DriverPaymentGeoCodes for a pSendReceiptTo between (optional) pStart and pEnd
		// *************************************************************************************************
		public static EntityList<DriverPaymentGeoCodes> GetSendReceiptTo(PersistenceManager pPM, string pSendReceiptTo, DateTime? pStart, DateTime? pEnd) {
			RdbQuery qry = new RdbQuery(typeof(DriverPaymentGeoCodes), SendReceiptToEntityColumn, EntityQueryOp.GE, pSendReceiptTo);
			qry.AddSpan(EntityRelations.DriverPayments_DriverPaymentGeoCodes);


			RdbSubquery subQry = new RdbSubquery(EntityRelationLink.From(EntityRelations.DriverPayments_DriverPaymentGeoCodes, QueryDirection.ParentQuery));

			if (pStart.HasValue) {
				subQry.AddClause(DriverPayments.ChargeDateEntityColumn, EntityQueryOp.GE, pStart.Value);
			}
			if (pEnd.HasValue) {
				subQry.AddClause(DriverPayments.ChargeDateEntityColumn, EntityQueryOp.LE, pEnd.Value);
			}
			qry.AddSubquery(subQry);

			return pPM.GetEntities<DriverPaymentGeoCodes>(qry);
		}

	}

	#region EntityPropertyDescriptors.DriverPaymentGeoCodesPropertyDescriptor
	////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
	////    For example: 
	////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
	//public static partial class EntityPropertyDescriptors {
	//	public partial class DriverPaymentGeoCodesPropertyDescriptor : BaseEntityPropertyDescriptor {
	//    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
	//    public AdaptedPropertyDescriptor Age {
	//      get { return this.Get("Age"); }
	//    }
	//  }
	//} 
	#endregion

}