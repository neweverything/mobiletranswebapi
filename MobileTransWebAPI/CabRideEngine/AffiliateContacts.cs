﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

using TaxiPassCommon;
using Csla.Validation;

namespace CabRideEngine {
	[Serializable]
	public sealed partial class AffiliateContacts : AffiliateContactsDataRow {

		#region Constructors -- DO NOT MODIFY
		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new'
		private AffiliateContacts()
			: this(null) {
		}

		// Typed DataSet constructor (needed by framework) - DO NOT REMOVE
		public AffiliateContacts(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static AffiliateContacts Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      AffiliateContacts aAffiliateContacts = (AffiliateContacts) pManager.CreateEntity(typeof(AffiliateContacts));
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aAffiliateContacts, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aAffiliateContacts.AddToManager();
		//      return aAffiliateContacts;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		// Add additional logic to your business object here...
		public static AffiliateContacts Create(PersistenceManager pPM, long pAffiliateID, string pEmployee) {
			AffiliateContacts oContact = null;

			try {
				// Creates the State but it is not yet accessible to the application
				oContact = (AffiliateContacts)pPM.CreateEntity(typeof(AffiliateContacts));

				// Using the IdeaBlade Id Generation technique
				//pPM.GenerateId(oContact, AffiliateContactIDEntityColumn);

				// CRITICAL: must tell emp to add itself to its PM
				oContact["AffiliateID"] = pAffiliateID;
				oContact["Employee"] = pEmployee;

				oContact.AddToManager();


			} catch (Exception ex) {
				throw ex;
			}
			return oContact;
		}

		public override string Employee {
			get {
				return base.Employee;
			}
			set {
				if (value.Trim().ToLower() == "<new>") {
					throw new Exception("Value can not be <new>");
				}
				base.Employee = StringUtils.ProperCase(value);
			}
		}

		public override string Title {
			get {
				return base.Title;
			}
			set {
				base.Title = StringUtils.ProperCase(value);
			}
		}

		public string FormattedPhone {
			get {
				return this.Phone.FormattedPhoneNumber();
			}
		}

		public string AdminGridView {
			get {
				string sAdmin = "";
				if (this.Admin) {
					sAdmin = "Yes";
				}
				return sAdmin;
			}
		}

		//protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
		//	if (IsDeserializing)
		//		return;
		//	base.OnColumnChanging(pArgs);
		//	DoAudit(pArgs);
		//}

		//private void DoAudit(DataColumnChangeEventArgs pArgs) {
		//	if (!this.PersistenceManager.IsClient) {
		//		return;
		//	}
		//	if (this.RowState == DataRowState.Added) {
		//		return;
		//	}
		//	if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
		//		AuditRecord = AffiliateContactsAudit.Create(this.PersistenceManager, this);
		//	}
		//	//AuditRecord[pArgs.Column.ColumnName] = pArgs.ProposedValue;
		//}

		//public override EntityValidationInfo Validate(EntityValidationContext pContext) {
		//    //validate fields to see if we can proceed
		//    //Employee = Employee;

		//    return base.Validate(pContext);
		//}

		public override string Password {
			get {

				Encryption oEncrypt = new Encryption();
				string sValue = oEncrypt.Decrypt(base.Password, this.Affiliate.Salt);

				return sValue;
			}
			set {
				if (this.Affiliate.Salt == null || this.Affiliate.Salt == "") {
					this.Affiliate.Salt = StringUtils.GenerateSalt();
				}
				Encryption oEncrypt = new Encryption();
				string sValue = oEncrypt.Encrypt(value, this.Affiliate.Salt);
				base.Password = sValue;
			}
		}

		protected override void AddRules(RuleList pList) {
			PropertyRequiredRule.AddToList(pList, EmployeeEntityColumn.ColumnName);
			PropertyRequiredRule.AddToList(pList, EmailEntityColumn.ColumnName);
			PropertyRequiredRule.AddToList(pList, PasswordEntityColumn.ColumnName);

			pList.Add(IsEMailUnique, EmailEntityColumn.ColumnName);
		}

		private bool IsEMailUnique(object pTarget, RuleArgs e) {
			AffiliateContacts oContact = (AffiliateContacts)pTarget;

			RdbQuery oQry = new RdbQuery(typeof(AffiliateContacts), EmailEntityColumn, EntityQueryOp.EQ, oContact.Email);
			oQry.AddClause(AffiliateContacts.AffiliateContactIDEntityColumn, EntityQueryOp.NE, oContact.AffiliateContactID);
			EntityList<AffiliateContacts> oContacts = this.PersistenceManager.GetEntities<AffiliateContacts>(oQry);
			if (oContacts.Count > 0) {
				e.Description = "An Affiliate Contact already exists with this email address!";
				return false;
			}
			return true;
		}

		// ************************************************************************************
		// Return a list of contacts who are set to receive the ACHTransRpt from CAbRideAutoACH
		// ************************************************************************************
		public static EntityList<AffiliateContacts> GetACHTransReportRecipients(PersistenceManager pPM, long pAffiliateID) {

			RdbQuery qry = new RdbQuery(typeof(AffiliateContacts), AffiliateContacts.AffiliateIDEntityColumn, EntityQueryOp.EQ, pAffiliateID);
			qry.AddClause(AffiliateContacts.ACHTransReportRecipientEntityColumn, EntityQueryOp.GE, true);

			return pPM.GetEntities<AffiliateContacts>(qry);
		}

		public override bool? VehicleManagement {
			get {
				return base.VehicleManagement.GetValueOrDefault(false);
			}
			set {
				base.VehicleManagement = value.GetValueOrDefault(false);
			}
		}

		public override bool? CustomerManagement {
			get {
				return base.CustomerManagement.GetValueOrDefault(false);
			}
			set {
				base.CustomerManagement = value.GetValueOrDefault(false);
			}
		}

		public override bool? ReportReservationHistory {
			get {
				return base.ReportReservationHistory.GetValueOrDefault(false);
			}
			set {
				base.ReportReservationHistory = value.GetValueOrDefault(false);
			}
		}

		public override bool? ReportDriver {
			get {
				return base.ReportDriver.GetValueOrDefault(false);
			}
			set {
				base.ReportDriver = value.GetValueOrDefault(false);
			}
		}

		public override bool? ReportFleet {
			get {
				return base.ReportFleet.GetValueOrDefault(false);
			}
			set {
				base.ReportFleet = value.GetValueOrDefault(false);
			}
		}

		public override bool? ReportRevenue {
			get {
				return base.ReportRevenue.GetValueOrDefault(false);
			}
			set {
				base.ReportRevenue = value.GetValueOrDefault(false);
			}
		}

		public override bool? ReportCustomerHistory {
			get {
				return base.ReportCustomerHistory.GetValueOrDefault(false);
			}
			set {
				base.ReportCustomerHistory = value.GetValueOrDefault(false);
			}
		}

		public override bool? RateTables {
			get {
				return base.RateTables.GetValueOrDefault(false);
			}
			set {
				base.RateTables = value.GetValueOrDefault(false);
			}
		}

		public override bool? HandleChargeBacks {
			get {
				return base.HandleChargeBacks.GetValueOrDefault(false);
			}
			set {
				base.HandleChargeBacks = value.GetValueOrDefault(false);
			}
		}

		public override bool? ACHReport {
			get {
				return base.ACHReport.GetValueOrDefault(false);
			}
			set {
				base.ACHReport = value.GetValueOrDefault(false);
			}
		}

		#region Retrieve Records

		public static AffiliateContacts GetContact(PersistenceManager pPM, string pEMail) {
			RdbQuery qry = new RdbQuery(typeof(AffiliateContacts), AffiliateContacts.EmailEntityColumn, EntityQueryOp.EQ, pEMail);
			return pPM.GetEntity<AffiliateContacts>(qry);
		}

		public static AffiliateContacts GetContact(PersistenceManager pPM, long pContactID) {
			RdbQuery qry = new RdbQuery(typeof(AffiliateContacts), AffiliateContacts.AffiliateContactIDEntityColumn, EntityQueryOp.EQ, pContactID);
			return pPM.GetEntity<AffiliateContacts>(qry);
		}


		public static EntityList<AffiliateContacts> GetContacts(Affiliate pAffiliate) {
			return GetContacts(pAffiliate, false);
		}

		public static EntityList<AffiliateContacts> GetContacts(Affiliate pAffiliate, bool pIncludeInvestors) {
			RdbQuery qry = new RdbQuery(typeof(AffiliateContacts), AffiliateIDEntityColumn, EntityQueryOp.EQ, pAffiliate.AffiliateID);
			qry.AddClause(AffiliateContacts.InvestorEntityColumn, EntityQueryOp.EQ, pIncludeInvestors);
			qry.AddOrderBy(AffiliateContacts.EmployeeEntityColumn, System.ComponentModel.ListSortDirection.Ascending);
			return pAffiliate.PersistenceManager.GetEntities<AffiliateContacts>(qry);
		}

		public static EntityList<AffiliateContacts> GetContacts(PersistenceManager pPM, long pAffiliateID) {
			return GetContacts(pPM, pAffiliateID, false);
		}

		public static EntityList<AffiliateContacts> GetContacts(PersistenceManager pPM, long pAffiliateID, bool pIncludeInvestors) {
			RdbQuery qry = new RdbQuery(typeof(AffiliateContacts), AffiliateIDEntityColumn, EntityQueryOp.EQ, pAffiliateID);
			qry.AddClause(AffiliateContacts.InvestorEntityColumn, EntityQueryOp.EQ, pIncludeInvestors);
			qry.AddOrderBy(AffiliateContacts.EmployeeEntityColumn, System.ComponentModel.ListSortDirection.Ascending);
			return pPM.GetEntities<AffiliateContacts>(qry);
		}
		#endregion

	}

}
