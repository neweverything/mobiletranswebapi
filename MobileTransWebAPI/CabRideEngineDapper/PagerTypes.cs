﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

using Dapper;
using TaxiPassCommon;


namespace CabRideEngineDapper {
	public partial class PagerTypes {

		public static PagerTypes GetPagerType(SqlConnection pConn, long pPagerTypeID) {
			return pConn.Get<PagerTypes>(pPagerTypeID);
		}

		public static PagerTypes GetPagerType(SqlConnection pConn, string pPagerType) {
			return pConn.GetList<PagerTypes>(new { PagerType = pPagerType }).FirstOrDefault();
		}
	}
}
