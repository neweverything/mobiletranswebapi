﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Http;
using TaxiPassCommon;

namespace MobileTransWebAPI.Controllers {
    public class TestController : ApiController {
        // GET api/test
        public IEnumerable<string> Get() {

            try {
                string[] tempList = File.ReadAllLines(@"c:\temp\MobileLog_20161116.txt");
                foreach (string line in tempList) {
                    if (line.Contains(" ChargeCard ")) {
                        string rec = line.Split(new string[] { " ChargeCard " }, StringSplitOptions.RemoveEmptyEntries).Last();
                        rec = rec.Decrypt();

                        if (rec.Contains("JobDetail")) {
                            Models.ChargeInfo chargeInfo = Newtonsoft.Json.JsonConvert.DeserializeObject<Models.ChargeInfo>(rec);
                            if (("" + chargeInfo.ReferenceNo) == "9100288" || ("" + chargeInfo.ReferenceNo) == "9043784" || (chargeInfo.JobDetail != null && (chargeInfo.JobDetail.JobNo == "9100288" || chargeInfo.JobDetail.JobNo == "9043784"))) {
                                //if (rec.Contains("EmailAddress") && rec.Contains("@")) {
                                Console.WriteLine(rec);
                            }
                        }
                    }

                }

            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }

            return new string[] { "value1", "value2" };
        }

        // GET api/test/5
        public string Get(int id) {
            return "value";
        }

        // POST api/test
        public void Post([FromBody]string value) {
        }

        // PUT api/test/5
        public void Put(int id, [FromBody]string value) {
        }

        // DELETE api/test/5
        public void Delete(int id) {
        }
    }
}
