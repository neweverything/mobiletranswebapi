﻿using TaxiPassCommon.Enums;

namespace TaxiPassCommon.Banking {
    public class BankCheck {

        public long AffiliateDriverID { get; set; }
        public long DriverID { get; set; }

        public string ErrorMsg;

        public string CustomerName { get; set; }
        public string RoutingNo { get; set; }
        public string AccountNo { get; set; }
        public CheckAccountTypes AccountType { get; set; }

        public bool TestAccount { get; set; }

        public string CheckNo { get; set; }

        public string DriverLicenseNo { get; set; }
        public string DriverLicenseState { get; set; }

        public string SSN { get; set; }

        public string SalesPerson {
            get {
                string value = string.Format("AD: {0} D:{1}", AffiliateDriverID, DriverID);
                if (AffiliateDriverID == 0 && DriverID == 0) {
                    value = "";
                }
                return value;
            }
        }
    }
}
