﻿using CabRideEngineDapper;
using MobileTransWebAPI.Models;
using System;
using System.Web.Http;
using TaxiPassCommon;

namespace MobileTransWebAPI.Controllers {
	public class CheckStatusReportController : ApiController {

		//public CheckStatusResponse Get() {

		//	new MultiPayeeTransHistoryController().Get();

		//	CheckStatusRequest req = new CheckStatusRequest {
		//		AffiliateID = 2653,
		//		AffiliateCode = "JDSA",
		//		StartDate = new DateTime(2019, 10, 1),
		//		EndDate = new DateTime(2019, 12, 31)
		//	};
		//	//File.WriteAllText(@"c:\TEMP\CheckStatusReq.json", JsonConvert.SerializeObject(req));
		//	return Index(req);
		//}

		public CheckStatusResponse Index(CheckStatusRequest pRequest) {
			CheckStatusResponse result = new CheckStatusResponse();
			//List<CheckStatus> result = new List<CheckStatus>();

			if (pRequest == null) {
				result.ErrorCode = 1;
				result.ErrorMsg = "Request cannot be null";
				return result;
			}

			if (pRequest.AffiliateID == 0 || pRequest.AffiliateCode.IsNullOrEmpty()) {
				result.ErrorCode = 2;
				result.ErrorMsg = "Request object is not valid";
				return result;
			}

			Affiliate aff = Affiliate.GetAffiliate(pRequest.AffiliateID);
			if (aff.IsNullEntity || !aff.AffiliateCode.Equals(pRequest.AffiliateCode)) {
				result.ErrorCode = 3;
				result.ErrorMsg = "Invalid Request";
				return result;
			}

			if (pRequest.StartDate.Year < 2018) {
				pRequest.StartDate = DateTime.Today;
			}
			if (pRequest.EndDate.Year < 2018) {
				pRequest.EndDate = DateTime.Today;
			}

			try {
				result.CheckStatusList = aff.GetCheckStatusReport(pRequest.StartDate, pRequest.EndDate);
				result.ErrorMsg = "OK";
			} catch (Exception ex) {
				result.ErrorMsg = ex.Message;
				result.ErrorCode = ex.HResult;
			}
			return result;
		}
	}
}