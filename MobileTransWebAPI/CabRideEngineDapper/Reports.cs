﻿using CabRideEngineDapper.Utils;
using Dapper;
using System.Data.SqlClient;

namespace CabRideEngineDapper {
    public partial class Reports {

        public static Reports GetReport(string pReportName) {
            using (var conn = SqlHelper.OpenSqlConnection()) {
                return GetReport(conn, pReportName);
            }
        }

        public static Reports GetReport(SqlConnection pConn, string pReportName) {
            string sql = "SELECT * FROM Reports WHERE ReportName = @ReportName";
            return pConn.Get<Reports>(sql, new { ReportName = pReportName });
        }
    }
}
