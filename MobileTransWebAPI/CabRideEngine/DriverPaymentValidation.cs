﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

using Csla.Validation;

namespace CabRideEngine {
	/// <summary>
	/// The developer class representing the DriverPaymentValidation business object class.
	/// </summary>
	/// <remarks>
	/// <para>Place all application-specific business logic in this class.</para>
	/// <para>
	/// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
	/// and will not overwrite changes.
	/// </para>
	/// <para>
	/// This class inherits from the generated class, its companion in the pair of classes 
	/// defining a business object. Properties in the generated class may be overridden in 
	/// order to change the behavior or add business logic.
	/// </para>
	/// <para>
	/// This class is the final class in the inheritance chain for this business object. 
	/// It should not be inherited.
	/// </para>
	/// </remarks>
	[Serializable]
	public sealed partial class DriverPaymentValidation : DriverPaymentValidationDataRow {

		#region Constructors -- DO NOT MODIFY

		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new';
		// it is also required if the class is visible from a web service.
		// DO NOT REMOVE **
		private DriverPaymentValidation() : this(null) { }

		/// <summary>
		/// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
		/// </summary>
		/// <param name="pRowBuilder"></param>
		public DriverPaymentValidation(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static DriverPaymentValidation Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      DriverPaymentValidation aDriverPaymentValidation = pManager.CreateEntity<DriverPaymentValidation>();
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aDriverPaymentValidation, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aDriverPaymentValidation.AddToManager();
		//      return aDriverPaymentValidation;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		// Add additional logic to your business object here...

		public static DriverPaymentValidation Create(DriverPayments pDriverPayment) {
			DriverPaymentValidation oRec = null;
			try {
				oRec = pDriverPayment.PersistenceManager.CreateEntity<DriverPaymentValidation>();

				// Using the IdeaBlade Id Generation technique
				pDriverPayment.PersistenceManager.GenerateId(oRec, DriverPaymentValidation.DriverPaymentValidationIDEntityColumn);
				oRec.DriverPaymentID = pDriverPayment.DriverPaymentID;

				oRec.AddToManager();

			} catch (Exception ex) {
				throw ex;
			}
			return oRec;
		}

		protected override void AddRules(RuleList pList) {
			PropertyRequiredRule.AddToList(pList, DriverPaymentValidation.LicenseNoEntityColumn.ColumnName);
			PropertyRequiredRule.AddToList(pList, DriverPaymentValidation.NameEntityColumn.ColumnName);
			/*
			PropertyRequiredRule.AddToList(pList, DriverPaymentValidation.StreetEntityColumn.ColumnName);
			PropertyRequiredRule.AddToList(pList, DriverPaymentValidation.CityEntityColumn.ColumnName);
			PropertyRequiredRule.AddToList(pList, DriverPaymentValidation.StateEntityColumn.ColumnName);
			*/
		}

		public static DriverPaymentValidation GetDriverInfo(PersistenceManager pPM, string pDriverLicense) {
			RdbQuery qry = new RdbQuery(typeof(DriverPaymentValidation), DriverPaymentValidation.LicenseNoEntityColumn, EntityQueryOp.EQ, pDriverLicense);
			qry.AddOrderBy(DriverPaymentValidation.DriverPaymentValidationIDEntityColumn, System.ComponentModel.ListSortDirection.Descending);
			qry.Top = 1;
			return pPM.GetEntity<DriverPaymentValidation>(qry);
		}

		public static DriverPaymentValidation GetDriverInfo(PersistenceManager pPM, string pDriverLicense, string pState) {
			RdbQuery qry = new RdbQuery(typeof(DriverPaymentValidation), DriverPaymentValidation.LicenseNoEntityColumn, EntityQueryOp.EQ, pDriverLicense);
			qry.AddClause(DriverPaymentValidation.StateEntityColumn, EntityQueryOp.EQ, pState);
			qry.AddOrderBy(DriverPaymentValidation.DriverPaymentValidationIDEntityColumn, System.ComponentModel.ListSortDirection.Descending);
			qry.Top = 1;
			return pPM.GetEntity<DriverPaymentValidation>(qry);
		}

		public static DriverPaymentValidation LastPaymentWithValidation(AffiliateDrivers pAffiliateDriver) {
			string sSQL = "SELECT TOP(1) * FROM DriverPaymentValidation" +
							" INNER JOIN DriverPayments ON DriverPaymentValidation.DriverPaymentID = DriverPayments.DriverPaymentID" +
							" WHERE DriverPayments.AffiliateDriverID = " + pAffiliateDriver.AffiliateDriverID +
							" ORDER BY DriverPaymentValidation.ModifiedDate Desc";

			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(DriverPaymentValidation), sSQL);
			return pAffiliateDriver.PersistenceManager.GetEntity<DriverPaymentValidation>(qry);
		}

	}

	#region EntityPropertyDescriptors.DriverPaymentValidationPropertyDescriptor
	////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
	////    For example: 
	////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
	//public static partial class EntityPropertyDescriptors {
	//	public partial class DriverPaymentValidationPropertyDescriptor : BaseEntityPropertyDescriptor {
	//    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
	//    public AdaptedPropertyDescriptor Age {
	//      get { return this.Get("Age"); }
	//    }
	//  }
	//} 
	#endregion

}