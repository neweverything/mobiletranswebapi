﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CabRideEngineDapper {
	public partial class DriverPaymentCharges {

		public static DriverPaymentCharges Create(long pDriverPaymentID, string pChargeType, decimal pAmount) {
			DriverPaymentCharges rec = DriverPaymentChargesTable.Create();
			rec.DriverPaymentID = pDriverPaymentID;
			rec.ChargeType = pChargeType;
			rec.Amount = pAmount;

			return rec;
		}

	}
}
