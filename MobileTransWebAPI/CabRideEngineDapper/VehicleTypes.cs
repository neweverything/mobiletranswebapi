﻿using CabRideEngineDapper.Utils;
using Dapper;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace CabRideEngineDapper {
    public partial class VehicleTypes {

        [Editable(false)]
        public string DisplayMember {
            get {
                return this.VehicleType;
            }
        }

        public static List<VehicleTypes> GetVehicleTypes(SqlConnection pConn) {
            string sql = "Select * FROM VehicleTypes WITH (NoLock) Order By VehicleType";

            return pConn.Query<VehicleTypes>(sql).ToList();
        }


        public static VehicleTypes GetVehicleType(long pVehicleTypeID) {
            using (var conn = SqlHelper.OpenSqlConnection()) {
                return GetVehicleType(conn, pVehicleTypeID);
            }
        }

        public static VehicleTypes GetVehicleType(SqlConnection pConn, long pVehicleTypeID) {
            return pConn.Get<VehicleTypes>(pVehicleTypeID);
        }

        public static List<VehicleTypes> GetLikeVehicleTypes(SqlConnection pConn, string pVehicleType) {
            string sql = @"SELECT * FROM dbo.VehicleTypes
							WHERE VehicleType LIKE @VehicleType
							ORDER BY VehicleType";

            return pConn.Query<VehicleTypes>(sql, new { VehicleType = pVehicleType + "%" }).ToList();
        }


        public static List<VehicleTypes> GetForDropDown(long pAffiliateID) {
            using (var conn = SqlHelper.OpenSqlConnection()) {
                return GetForDropDown(conn, pAffiliateID);
            }
        }

        public static List<VehicleTypes> GetForDropDown(SqlConnection pConn, long pAffiliateID) {
            string sql = @"SELECT  VehicleTypes.VehicleTypeID
								  , VehicleType
                            FROM    dbo.VehicleTypes
                            WHERE   AffiliateID = 0
                                    OR AffiliateID = @AffiliateID
                            ORDER BY VehicleType";

            return pConn.Query<VehicleTypes>(sql, new { AffiliateID = pAffiliateID }).ToList();
        }

        public static List<VehicleTypes> GetByAffiliateForDropDown(SqlConnection pConn, long pAffiliateID) {
            string sql = @"SELECT DISTINCT
									VehicleTypes.VehicleTypeID
								  , VehicleType
							FROM    dbo.VehicleTypes
							LEFT JOIN dbo.Vehicles ON Vehicles.VehicleTypeID = VehicleTypes.VehicleTypeID
							WHERE   AffiliateID = @AffiliateID";

            return pConn.Query<VehicleTypes>(sql, new { AffiliateID = pAffiliateID }).ToList();
        }

        //        public static VehicleTypes GetVehicleTypeByName(string pVehicleType) {
        //            using (var conn = SqlHelper.OpenSqlConnection()) {
        //                return GetVehicleTypeByName(conn, pVehicleType);
        //            }
        //        }

        //        public static VehicleTypes GetVehicleTypeByName(SqlConnection pConn, string pVehicleType) {
        //            string sql = @"SELECT * FROM dbo.VehicleTypes
        //							WHERE VehicleType = @VehicleType and AffiliateID = 0
        //							ORDER BY VehicleType";

        //            return pConn.Query<VehicleTypes>(sql, new { VehicleType = pVehicleType }).FirstOrDefault();
        //        }


        public static VehicleTypes GetVehicleTypeByName(string pVehicleType, long pAffiliateID = 0) {
            using (var conn = SqlHelper.OpenSqlConnection()) {
                return GetVehicleTypeByName(conn, pVehicleType, pAffiliateID);
            }
        }

        public static VehicleTypes GetVehicleTypeByName(SqlConnection pConn, string pVehicleType, long pAffiliateID) {
            string sql = @"SELECT  *
                            FROM    dbo.VehicleTypes
                            WHERE   (VehicleType = @VehicleType
                                        AND AffiliateID = @AffiliateID
                                    )
                                    OR (VehicleType = @VehicleType
                                        AND AffiliateID = 0
                                        ) ";

            return pConn.Query<VehicleTypes>(sql, new { VehicleType = pVehicleType, AffiliateID = pAffiliateID }).FirstOrDefault();
        }


        public static List<VehicleTypes> GetForDropDown(SqlConnection pConn) {
            string sql = @"SELECT VehicleTypeID
								  , VehicleType
							FROM    dbo.VehicleTypes
							ORDER BY VehicleType";

            return pConn.Query<VehicleTypes>(sql).ToList();
        }

        public static VehicleTypes GetTaxi(SqlConnection pConn) {
            string sql = "SELECT * FROM VehicleTypes WHERE VehicleType = 'Taxi'";
            VehicleTypes rec = pConn.Get<VehicleTypes>(sql);
            if (rec.IsNullEntity) {
                rec = Create();
                rec.VehicleType = "Taxi";
                rec.Save();
            }
            return rec;
        }

        public static List<VehicleTypes> GetAll(SqlConnection pConn) {
            string sql = @"SELECT * FROM VehicleTypes
                            ORDER BY VehicleType";

            return pConn.Query<VehicleTypes>(sql).ToList();
        }

    }
}
