﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

using Csla.Validation;

namespace CabRideEngine {
	[Serializable]
	public sealed class PaymentTypes : PaymentTypesDataRow {

		#region Constructors -- DO NOT MODIFY
		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new'
		private PaymentTypes() : this(null) {
		}

		// Typed DataSet constructor (needed by framework) - DO NOT REMOVE
		public PaymentTypes(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static PaymentTypes Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      PaymentTypes aPaymentTypes = pManager.CreateEntity<PaymentTypes>();
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aPaymentTypes, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aPaymentTypes.AddToManager();
		//      return aPaymentTypes;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		// Add additional logic to your business object here...
		public static PaymentTypes Create(PersistenceManager pPM) {
			PaymentTypes oType = null;

			try {
				// Creates the Country but it is not yet accessible to the application
				oType = (PaymentTypes)pPM.CreateEntity(typeof(PaymentTypes));
				//pPM.GenerateId(oType, PaymentTypes.PaymentTypeIDEntityColumn);
				oType.AddToManager();

			} catch (Exception ex) {
				throw ex;
			}
			return oType;
		}

		protected override void AddRules(RuleList pList) {
			PropertyRequiredRule.AddToList(pList, PaymentTypeEntityColumn.ColumnName);
			pList.Add(IsPaymentTypeUnique, PaymentTypeEntityColumn.ColumnName);
		}

		private bool IsPaymentTypeUnique(object pTarget, RuleArgs e) {
			PaymentTypes oType = (PaymentTypes)pTarget;

			RdbQuery oQry = new RdbQuery(typeof(PaymentTypes), PaymentTypes.PaymentTypeEntityColumn, EntityQueryOp.EQ, oType.PaymentType);
			oQry.AddClause(PaymentTypes.PaymentTypeIDEntityColumn, EntityQueryOp.NE, oType.PaymentTypeID);
			EntityList<PaymentTypes> oPaymentTypes = this.PersistenceManager.GetEntities<PaymentTypes>(oQry);
			if (oPaymentTypes.Count > 0) {
				e.Description = "Payment Type must be unique!";
				return false;
			}
			return true;
		}

		[BindingBrowsable(false)]
		public override string EntityInstanceDisplayName {
			get {
				return PaymentType;
			}
		}

		protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
			if (IsDeserializing)
				return;
			base.OnColumnChanging(pArgs);
			//DoAudit(pArgs);
		}

		private void DoAudit(DataColumnChangeEventArgs pArgs) {
			if (!this.PersistenceManager.IsClient) {
				return;
			}
			if (this.RowState == DataRowState.Added) {
				return;
			}
			if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
				AuditRecord = PaymentTypesAudit.Create(this.PersistenceManager, this);
			}
			//AuditRecord[pArgs.Column.ColumnName] = pArgs.ProposedValue;
		}


	}

}
