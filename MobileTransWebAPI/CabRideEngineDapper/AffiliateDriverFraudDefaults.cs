﻿using CabRideEngineDapper.Utils;
using Dapper;

namespace CabRideEngineDapper {
    public partial class AffiliateDriverFraudDefaults {
        public static AffiliateDriverFraudDefaults GetFraudDefaults(long pAffiliateDriverID) {
            string sql = @"SELECT * FROM AffiliateDriverFraudDefaults
                            WHERE AffiliateDriverID = @AffiliateDriverID";

            using (var conn = SqlHelper.OpenSqlConnection()) {
                return conn.Get<AffiliateDriverFraudDefaults>(sql, new { AffiliateDriverID = pAffiliateDriverID });
            }
        }
    }
}
