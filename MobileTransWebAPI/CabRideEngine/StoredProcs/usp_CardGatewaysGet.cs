﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

using IdeaBlade.Persistence;
using Dapper;

namespace CabRideEngine.StoredProcs {
	public class usp_CardGatewaysGet {
		public Int32 CardGatewayGroupsID { get; set; }
		public string GroupName { get; set; }
		public bool DefaultGroup { get; set; }
		public string Processor { get; set; }
		public Int32 CardGatewayDetailsID { get; set; }
		public string ReferenceKey { get; set; }
		public string ValueString { get; set; }
		public int ProcessorOrder { get; set; }
		public string Salt { get; set; }
		public bool Validate { get; set; }


		public static List<usp_CardGatewaysGet> GetGateWays(PersistenceManager pPM, long pID) {
			List<usp_CardGatewaysGet> dataList = new List<usp_CardGatewaysGet>();

			using (var oConn = Utils.SQLHelper.OpenSqlConnection(pPM)) {
				dataList = oConn.Query<usp_CardGatewaysGet>("usp_CardGatewaysGet", new { CardGatewayGroupsID = pID }, commandType: CommandType.StoredProcedure).ToList();
			}

			return dataList;
		}
	}
}
