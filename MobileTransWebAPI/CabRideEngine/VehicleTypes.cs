﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

using Csla.Validation;

namespace CabRideEngine {
	[Serializable]
	public sealed class VehicleTypes : VehicleTypesDataRow {

		#region Constructors -- DO NOT MODIFY
		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new'
		private VehicleTypes() : this(null) {
		}

		// Typed DataSet constructor (needed by framework) - DO NOT REMOVE
		public VehicleTypes(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static VehicleTypes Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      VehicleTypes aVehicleTypes = pManager.CreateEntity<VehicleTypes>();
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aVehicleTypes, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aVehicleTypes.AddToManager();
		//      return aVehicleTypes;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		// Add additional logic to your business object here...
		public static VehicleTypes Create(PersistenceManager pManager) {

			VehicleTypes oVehicleType = (VehicleTypes)pManager.CreateEntity(typeof(VehicleTypes));

			//pManager.GenerateId(oVehicleType, VehicleTypes.VehicleTypeIDEntityColumn);
			oVehicleType.AddToManager();
			return oVehicleType;
		}

		protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
			if (IsDeserializing)
				return;
			base.OnColumnChanging(pArgs);
			//DoAudit(pArgs);
		}

		private void DoAudit(DataColumnChangeEventArgs pArgs) {
			if (!this.PersistenceManager.IsClient) {
				return;
            }
            if (this.RowState == DataRowState.Added) {
                return;
            }
			if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
				AuditRecord = VehicleTypesAudit.Create(this.PersistenceManager, this);
			}
			//AuditRecord[pArgs.Column.ColumnName] = pArgs.ProposedValue;
		}


		protected override void AddRules(RuleList pList) {
			PropertyRequiredRule.AddToList(pList, VehicleTypes.VehicleTypeEntityColumn.ColumnName);
			pList.Add(IsVehicleTypeUnique, VehicleTypeEntityColumn.ColumnName);
		}

		private bool IsVehicleTypeUnique(object pTarget, RuleArgs e) {
			VehicleTypes oType = (VehicleTypes)pTarget;

			RdbQuery oQry = new RdbQuery(typeof(VehicleTypes), VehicleTypes.VehicleTypeEntityColumn, EntityQueryOp.EQ, oType.VehicleType);
			oQry.AddClause(VehicleTypes.VehicleTypeIDEntityColumn, EntityQueryOp.NE, oType.VehicleTypeID);
			EntityList<VehicleTypes> oVehicleTypes = this.PersistenceManager.GetEntities<VehicleTypes>(oQry);
			if (oVehicleTypes.Count > 0) {
				e.Description = "Vehicle Type must be unique!";
				return false;
			}
			return true;
		}

		[BindingBrowsable(false)]
		public override string EntityInstanceDisplayName {
			get {
				return VehicleType;
			}
		}

        public static EntityList<VehicleTypes> GetAll(PersistenceManager pPM) {
            EntityList<VehicleTypes> oList = pPM.GetEntities<VehicleTypes>();
            oList.ApplySort(VehicleTypes.VehicleTypeEntityColumn.ColumnName, System.ComponentModel.ListSortDirection.Ascending, false);
            return oList;
        }

        public static VehicleTypes GetTaxi(PersistenceManager pPM) {
            RdbQuery qry = new RdbQuery(typeof(VehicleTypes), VehicleTypes.VehicleTypeEntityColumn, EntityQueryOp.EQ, "Taxi");
            VehicleTypes rec = pPM.GetEntity<VehicleTypes>(qry);
            if (rec.IsNullEntity) {
                rec = VehicleTypes.Create(pPM);
                rec.VehicleType = "Taxi";
                rec.Save();
            }
            return rec;
        }

		public static VehicleTypes GetVehicleType(PersistenceManager pPM, string pVehicleType) {
			RdbQuery qry = new RdbQuery(typeof(VehicleTypes), VehicleTypes.VehicleTypeEntityColumn, EntityQueryOp.EQ, pVehicleType);
			VehicleTypes rec = pPM.GetEntity<VehicleTypes>(qry);
			if (rec.IsNullEntity) {
				rec = VehicleTypes.Create(pPM);
				rec.VehicleType = pVehicleType;
				rec.Save();
			}
			return rec;
		}

	}

}
