using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using System.Linq;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;
using System.Collections.Generic;
using System.Text;

namespace CabRideEngine {
	/// <summary>
	/// The developer class representing the usp_AffiliateDriverTotals business object class.
	/// </summary>
	/// <remarks>
	/// <para>Place all application-specific business logic in this class.</para>
	/// <para>
	/// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
	/// and will not overwrite changes.
	/// </para>
	/// <para>
	/// This class inherits from the generated class, its companion in the pair of classes 
	/// defining a business object. Properties in the generated class may be overridden in 
	/// order to change the behavior or add business logic.
	/// </para>
	/// <para>
	/// This class is the final class in the inheritance chain for this business object. 
	/// It should not be inherited.
	/// </para>
	/// </remarks>
	[Serializable]
	public sealed partial class usp_AffiliateDriverTotals : usp_AffiliateDriverTotalsDataRow {

		#region Constructors -- DO NOT MODIFY

		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new';
		// it is also required if the class is visible from a web service.
		// DO NOT REMOVE **
		private usp_AffiliateDriverTotals() : this(null) { }

		/// <summary>
		/// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
		/// </summary>
		/// <param name="pRowBuilder"></param>
		public usp_AffiliateDriverTotals(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static usp_AffiliateDriverTotals Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      usp_AffiliateDriverTotals ausp_AffiliateDriverTotals = pManager.CreateEntity<usp_AffiliateDriverTotals>();
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(ausp_AffiliateDriverTotals, // add id column here //);
		//
		//      // Add custom code here
		//
		//      ausp_AffiliateDriverTotals.AddToManager();
		//      return ausp_AffiliateDriverTotals;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		// Add additional logic to your business object here...
		public static EntityList<usp_AffiliateDriverTotals> ExecuteProc(PersistenceManager pPM, DateTime pStartDate, DateTime pEndDate, bool pGroupByPlatform) {
			StringBuilder sql = new StringBuilder();
			sql.Append("usp_AffiliateDriverTotals @StartDate = '");
			sql.Append(pStartDate.ToShortDateString());
			sql.Append("', @EndDate = '");
			sql.Append(pEndDate.ToShortDateString());
			sql.Append("'");

			if (pGroupByPlatform) {
				sql.Append(", @GroupByPlatform = 1");
			}

			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(usp_AffiliateDriverTotals), sql.ToString());
			qry.CommandTimeout = 300;
			return pPM.GetEntities<usp_AffiliateDriverTotals>(qry, QueryStrategy.DataSourceThenCache);

		}
	}

	#region EntityPropertyDescriptors.usp_AffiliateDriverTotalsPropertyDescriptor
	////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
	////    For example: 
	////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
	//public static partial class EntityPropertyDescriptors {
	//	public partial class usp_AffiliateDriverTotalsPropertyDescriptor : AdaptedPropertyDescriptor {
	//    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
	//    public AdaptedPropertyDescriptor Age {
	//      get { return this.Get("Age"); }
	//    }
	//  }
	//} 
	#endregion

}