﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileTransWebAPI.Models {
	public class NewSignUpInfo {
		public String FleetCode { get; set; }
		public String CabNo { get; set; }
		public String Cell { get; set; }
		public long DriverID { get; set; }

		public String RequestKey { get; set; }
	}
}