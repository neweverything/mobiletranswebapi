using System;
using System.Collections.Generic;
using System.IO;

using System.Linq;

//using IdeaBlade.Util;

namespace TaxiPassCommon.Banking {


    public class TTechGenerator {
        private List<TTechTransaction> mTransactions;

        Dictionary<string, List<TTechTransaction>> mDic = new Dictionary<string, List<TTechTransaction>>();
        public List<PayOut> PayOutList = new List<PayOut>();

        public string ACHFTPSite = "";
        public string ACHUserName = "";
        public string ACHPassword = "";

        public string TTechTransactionLogsPath = "";

        private string mTTechAccountNo;
        public short LaneID;


        public TTechGenerator(List<TTechTransaction> pTransactions, string pTTechAccountNo) {
            mTransactions = pTransactions;
            mTTechAccountNo = pTTechAccountNo;
            FillData();
        }

        public List<TTechTransaction> GetUpdatedTransactions() {
            return mTransactions;
        }

        public string CreateAndFTPTransactions(string pFilePath, System.Windows.Forms.ToolStripProgressBar pProgressBar) {

            string sFile = "";
            try {
                sFile = CreateCSV(pFilePath);
                string achFileName = new System.IO.FileInfo(sFile).Name;
                TaxiPassCommon.Web.FTP oFTP = new TaxiPassCommon.Web.FTP(ACHFTPSite, ACHUserName, ACHPassword);
                oFTP.ChangeDir(TTechTransactionLogsPath);
                oFTP.OpenUpload(sFile, achFileName);

                if (pProgressBar != null) {
                    pProgressBar.Value = 0;
                    pProgressBar.Visible = true;
                    pProgressBar.Maximum = (int)oFTP.FileSize;
                }
                while (oFTP.DoUpload() > 0) {
                    if (pProgressBar != null) {
                        pProgressBar.Value = (int)(((oFTP.BytesTotal) * 100) / oFTP.FileSize);
                    }
                }
                if (pProgressBar != null) {
                    pProgressBar.Visible = false;
                }
                oFTP.Disconnect();
            } catch (Exception ex) {
                throw ex;
            }
            return sFile;
        }

        public string CreateCSV(string pPath) {

            if (pPath.IsNullOrEmpty()) {
                throw new Exception("Path is required");
            }
            if (mTTechAccountNo.IsNullOrEmpty()) {
                throw new Exception("TTech Account No is required");
            }

            if (!Directory.Exists(pPath)) {
                Directory.CreateDirectory(pPath);
            }
            string sFile = "";
            string sBatchNo = "";
            try {
                short i = 0;
                foreach (string sKey in mDic.Keys) {
                    if (!sKey.IsNullOrEmpty()) {
                        i = mDic[sKey][0].LaneID;
                        break;
                    }
                }

                while (true) {
                    sBatchNo = DateTime.Today.ToString("yyyyMMdd") + i.ToString().PadLeft(2, '0') + mTTechAccountNo;
                    sFile = sBatchNo + ".csv";
                    if (!File.Exists(pPath + sFile)) {
                        LaneID = i;
                        break;
                    }
                    i++;
                }
            } catch (Exception ex) {
                throw ex;
            }

            string sCSV = "";

            //process transactions
            decimal total = 0;
            long tranCount = 0;
            string sUseKey = "";

            foreach (string sKey in mDic.Keys) {
                if (sUseKey.IsNullOrEmpty()) {
                    sUseKey = sKey;
                }
                //bool bFirst = true;
                TTechTransaction newTran = null;

                newTran = mDic[sKey][0];
                newTran.Amount = mDic[sKey].Sum(p => p.Amount);
                tranCount++;
                total += newTran.Amount;

                foreach (TTechTransaction oTran in mDic[sKey]) {
                    //if (bFirst) {
                    //    bFirst = false;
                    //    newTran = oTran;
                    //} else {
                    //    newTran.Amount += oTran.Amount;
                    //}
                    oTran.BatchNo = sBatchNo;
                    oTran.LaneID = LaneID;
                }
                sCSV += newTran.CSVRecord;

                PayOut pay = new PayOut();
                pay.Affiliate = newTran.Affiliate;
                pay.Amount = newTran.Amount;
                pay.Name = newTran.CustomerName;
                pay.TransType = newTran.TranType;
                PayOutList.Add(pay);
            }
            //create the batch out records


            //foreach (string sKey in mDic.Keys) {
            //    total += mDic[sKey][0].Amount;
            //    tranCount++;
            //    if (sUseKey == "") {
            //        sUseKey = sKey;
            //    }
            //}

            //foreach (string sKey in mDic.Keys) {
            {
                TTechTransaction oTran = mDic[sUseKey][0];

                TTechTransaction batchTran = new TTechTransaction();
                batchTran.Action = TTechAction.BatchOut;
                batchTran.Site = oTran.Site;  // mDic[sKey][0].Site;
                batchTran.MerchantID = oTran.MerchantID; // mDic[sKey][0].MerchantID;
                batchTran.LaneID = LaneID;
                batchTran.TranID = Convert.ToInt64("9" + oTran.TranID.ToString()); // Convert.ToInt64("9" + mDic[sKey][0].TranID.ToString());
                batchTran.Amount = total; //oTran.Amount; // mDic[sKey][0].Amount;
                batchTran.CheckNumber = tranCount; // batchTran.TransactionCount;
                batchTran.TranDate = DateTime.Now;
                batchTran.ClassCode = "CCD";
                if (oTran.TranType.Equals("credit", StringComparison.CurrentCultureIgnoreCase)) {
                    batchTran.TranType = "Debit";
                } else {
                    batchTran.TranType = "Credit";
                }
                sCSV += batchTran.CSVRecord;
                PayOut pay = new PayOut();
                pay.Amount = batchTran.Amount;
                pay.Name = "Total of Payouts";
                pay.TransType = batchTran.TranType;
                PayOutList.Add(pay);
            }

            if (!Directory.Exists(pPath)) {
                Directory.CreateDirectory(pPath);
            }

            try {
                sFile = Path.Combine(pPath, sFile);
                StreamWriter oWrite = File.CreateText(sFile);
                oWrite.Write(sCSV);
                oWrite.Close();

            } catch (Exception ex) {
                throw ex;
            }
            return sFile;
        }

        private bool FillData() {

            foreach (TTechTransaction oTran in mTransactions) {
                string sKey = oTran.TransactionKey;
                if (!mDic.ContainsKey(sKey)) {
                    mDic.Add(sKey, new List<TTechTransaction>());
                }
                if (mDic[sKey].FirstOrDefault(p => p.TranID == oTran.TranID) == null) {
                    mDic[sKey].Add(oTran);
                }
            }

            return true;
        }

        public class PayOut {
            public string Affiliate { get; set; }
            public string Name { get; set; }
            public decimal Amount { get; set; }
            public string TransType { get; set; }

            public string BatchNo { get; set; }
            public long? AffiliateID { get; set; }
            public long? TaxiPassRedeemerID { get; set; }
            public string BankAccountNo { get; set; }
            public string BankRouting { get; set; }
            public string BankHolderName { get; set; }
            public string BankAccountType { get; set; }

            public short LaneID { get; set; }
            public string PaidTo { get; set; }
            public string PaymentType { get; set; }
            public bool WireTransfer { get; set; }

            public string Phone { get; set; }
        }

    }

}

