﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

using TaxiPassCommon;

namespace CabRideEngine {
	/// <summary>
	/// The developer class representing the KioskStatus business object class.
	/// </summary>
	/// <remarks>
	/// <para>Place all application-specific business logic in this class.</para>
	/// <para>
	/// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
	/// and will not overwrite changes.
	/// </para>
	/// <para>
	/// This class inherits from the generated class, its companion in the pair of classes 
	/// defining a business object. Properties in the generated class may be overridden in 
	/// order to change the behavior or add business logic.
	/// </para>
	/// <para>
	/// This class is the final class in the inheritance chain for this business object. 
	/// It should not be inherited.
	/// </para>
	/// </remarks>
	[Serializable]
	public sealed partial class KioskStatus : KioskStatusDataRow {

		#region Constructors -- DO NOT MODIFY

		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new';
		// it is also required if the class is visible from a web service.
		// DO NOT REMOVE **
		private KioskStatus() : this(null) { }

		/// <summary>
		/// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
		/// </summary>
		/// <param name="pRowBuilder"></param>
		public KioskStatus(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static KioskStatus Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      KioskStatus aKioskStatus = pManager.CreateEntity<KioskStatus>();
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aKioskStatus, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aKioskStatus.AddToManager();
		//      return aKioskStatus;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		// Add additional logic to your business object here...
		public static KioskStatus GetOrCreate(Drivers pDriver) {

			KioskStatus rec = GetStatus(pDriver);
			if (rec.IsNullEntity) {
				rec = pDriver.PersistenceManager.CreateEntity<KioskStatus>();

				rec.DriverID = pDriver.DriverID;
				rec.AddToManager();
				rec.Save();
			}

			return rec;
		}

		protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
			if (IsDeserializing)
				return;
			base.OnColumnChanging(pArgs);
			//DoAudit(pArgs);
		}

		private void DoAudit(DataColumnChangeEventArgs pArgs) {
			if (!this.PersistenceManager.IsClient) {
				return;
			}
			if (this.RowState == DataRowState.Added) {
				return;
			}
			if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
				AuditRecord = KioskStatusAudit.Create(this);
			}
		}

		public override string Status {
			get {
				return base.Status;
			}
			set {
				if (value.IsNumeric()) {
					try {
						base.Status = value.ToEnum<StatusCodes>().ToString();
					} catch (Exception) {
						base.Status = value;
					}
				} else {
					base.Status = value;
				}
			}
		}

		public static KioskStatus GetStatus(Drivers pDriver) {
			RdbQuery qry = new RdbQuery(typeof(KioskStatus), DriverIDEntityColumn, EntityQueryOp.EQ, pDriver.DriverID);
			return pDriver.PersistenceManager.GetEntity<KioskStatus>(qry);
		}


		public enum StatusCodes {
			POWER_UP = 1,
			POWER_DOWN = 2,
			PING_FAIL = 3,
			INTF_RESET = 4,
			BAD_CARD_READ = 5,
			CARD_FULL = 6,
			PRIMARY_SERVER = 7,
			SECONDARY_SERVER = 8,
			MAN_POWER_DOWN = 9,
			POWER_FAIL = 10,
			INTF_DOWN = 11,
			RESET = 12,
			DOWNLOAD_FAILURE = 13,
			UPDATE_CONFIG = 14,
			NEW_APP_DETECTED = 15,
			DIAG_MENU = 16,
			TIME_RESET = 17,
			ID_RESET = 18,
			INTF_UP = 19,
			TIME_DIFF_BIG = 20,
			EVENTLOG_FULL = 21,
			SYSTEM_RESET = 22,
			COMMS_ERROR_PRESENTED = 23,
			BANK_ERROR_PRESENTED = 24,
			PAPER_LOW = 25,
			PRINTER_ERROR = 26,
			OUTOFORDER = 27,
			PRINTER_OK = 28,
			PARTIALCASH = 29,
			RESET_NETWORK = 30,
			RESETAT = 31,
			RESET_UPTIME = 32,
			PAPERJAM = 33,
			PAPER_OUT = 34,
			STATUS_FAILED = 35,
			MANUAL_CLEAR = 36,
			AUTO_CLEAR = 37,
			RECEIPTPRINTED = 38,
			SWITCHTOBACKUP = 40,
			NOPRNSTATUS = 41,
			PRINTERSTATUS = 42,
			DNSFAILURE = 43,
			SOCKETCONNECT = 44,
			WRITESOCKERROR = 45,
			SOCKETNOTCLOSED = 46,
			HTTPBUFFEROVERFLOW = 47,
			NOACTIVEIFACE = 48,
			NOHTTPBUFFERS = 49,
			FUNCTIONBLOCKING = 50,
			CD_LOST = 51,
			USER_MENU = 52
		}
	}

	#region EntityPropertyDescriptors.KioskStatusPropertyDescriptor
	////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
	////    For example: 
	////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
	//public static partial class EntityPropertyDescriptors {
	//	public partial class KioskStatusPropertyDescriptor : BaseEntityPropertyDescriptor {
	//    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
	//    public AdaptedPropertyDescriptor Age {
	//      get { return this.Get("Age"); }
	//    }
	//  }
	//} 
	#endregion

}