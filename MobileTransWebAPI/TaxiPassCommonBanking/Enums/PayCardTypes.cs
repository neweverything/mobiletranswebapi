﻿namespace TaxiPassCommon.Banking.Enums {

	public enum PayCardTypes {
		GlobalCashCard,
		GlobalCashCardMM,
		RapidPay,
		//RapidPayNewTek,
		TransCard,
		USBank,
		//USBankNewTek,
	}

}
