﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;
using TaxiPassCommon;

namespace CabRideEngine {
	/// <summary>
	/// The developer class representing the AffiliateDriverReferrals business object class.
	/// </summary>
	/// <remarks>
	/// <para>Place all application-specific business logic in this class.</para>
	/// <para>
	/// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
	/// and will not overwrite changes.
	/// </para>
	/// <para>
	/// This class inherits from the generated class, its companion in the pair of classes 
	/// defining a business object. Properties in the generated class may be overridden in 
	/// order to change the behavior or add business logic.
	/// </para>
	/// <para>
	/// This class is the final class in the inheritance chain for this business object. 
	/// It should not be inherited.
	/// </para>
	/// </remarks>
	[Serializable]
	public sealed partial class AffiliateDriverReferrals : AffiliateDriverReferralsDataRow {

		#region Constructors -- DO NOT MODIFY

		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new';
		// it is also required if the class is visible from a web service.
		// DO NOT REMOVE **
		private AffiliateDriverReferrals() : this(null) { }

		/// <summary>
		/// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
		/// </summary>
		/// <param name="pRowBuilder"></param>
		public AffiliateDriverReferrals(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static AffiliateDriverReferrals Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      AffiliateDriverReferrals aAffiliateDriverReferrals = pManager.CreateEntity<AffiliateDriverReferrals>();
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aAffiliateDriverReferrals, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aAffiliateDriverReferrals.AddToManager();
		//      return aAffiliateDriverReferrals;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		// Add additional logic to your business object here...
		public static AffiliateDriverReferrals Create(AffiliateDrivers pDriver, long pReferredDriverID) {
			return Create(pDriver, pReferredDriverID, Platforms.TaxiPay.ToString());
		}

		public static AffiliateDriverReferrals Create(AffiliateDrivers pDriver, long pReferredDriverID, string pPlatform) {
			AffiliateDriverReferrals rec = GetRecord(pDriver.PersistenceManager, pDriver.AffiliateDriverID, pReferredDriverID);

			try {
				if (rec.IsNullEntity) {
					PersistenceManager oPM = pDriver.PersistenceManager;
					rec = oPM.CreateEntity<AffiliateDriverReferrals>();
					// Using the IdeaBlade Id Generation technique
					oPM.GenerateId(rec, AffiliateDriverReferrals.AffiliateDriverReferralIDEntityColumn);
					rec.AffiliateDriverID = pDriver.AffiliateDriverID;
					rec.ReferredDriverID = pReferredDriverID;
					rec.Platform = Platforms.TaxiPay.ToString();
					rec.AddToManager();

					rec.ReferralDate = DateTime.Now;

					rec.Save();
				}
			} catch (Exception ex) {
				throw ex;
			}
			return rec;
		}

		protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
			if (IsDeserializing) {
				return;
			}
			base.OnColumnChanging(pArgs);
			DoAudit(pArgs);
		}

		private void DoAudit(DataColumnChangeEventArgs pArgs) {
			if (!this.PersistenceManager.IsClient) {
				return;
			}
			if (this.RowState == DataRowState.Added) {
				return;
			}
			if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
				AuditRecord = AffiliateDriverReferralAudit.Create(this);
			}
		}

		public override System.DateTime ReferralDate {
			get {
				return base.ReferralDate;
			}
			set {
				try {
					TimeZoneConverter convert = new TimeZoneConverter();
					base.ReferralDate = convert.ConvertTime(DateTime.Now, TimeZone.CurrentTimeZone.StandardName, this.AffiliateDrivers.Affiliate.TimeZone);
				} catch (Exception) {
					base.ReferralDate = DateTime.Now;
				}
				//base.ReferralDate = value;
			}
		}
		public static AffiliateDriverReferrals GetRecord(PersistenceManager pPM, long pDriver, long pReferredDriverID) {
			RdbQuery qry = new RdbQuery(typeof(AffiliateDriverReferrals), AffiliateDriverReferrals.AffiliateDriverIDEntityColumn, EntityQueryOp.EQ, pDriver);
			qry.AddClause(AffiliateDriverReferrals.ReferredDriverIDEntityColumn, EntityQueryOp.EQ, pReferredDriverID);
			return pPM.GetEntity<AffiliateDriverReferrals>(qry);
		}

		public static EntityList<AffiliateDriverReferrals> GetReferrals(PersistenceManager pPM, long pAffiliateDriverID) {
			RdbQuery qry = new RdbQuery(typeof(AffiliateDriverReferrals), AffiliateDriverReferrals.AffiliateDriverIDEntityColumn, EntityQueryOp.EQ, pAffiliateDriverID);
			return pPM.GetEntities<AffiliateDriverReferrals>(qry);
		}

		public static EntityList<AffiliateDriverReferrals> GetDriverReferrals(Affiliate pAffiliate) {
			string sSQL = "SELECT AffiliateDriverReferrals.* FROM AffiliateDriverReferrals INNER JOIN AffiliateDrivers ON " +
							"AffiliateDriverReferrals.AffiliateDriverID = AffiliateDrivers.AffiliateDriverID " +
							"WHERE AffiliateDrivers.AffiliateID = " + pAffiliate.AffiliateID;
			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(AffiliateDriverReferrals), sSQL);
			return pAffiliate.PersistenceManager.GetEntities<AffiliateDriverReferrals>(qry);
		}

		public static AffiliateDriverReferrals GetByReferredDriver(PersistenceManager pPM, long pDriverID) {
			RdbQuery qry = new RdbQuery(typeof(AffiliateDriverReferrals), AffiliateDriverReferrals.ReferredDriverIDEntityColumn, EntityQueryOp.EQ, pDriverID);
			return pPM.GetEntity<AffiliateDriverReferrals>(qry);
		}

	}

	#region EntityPropertyDescriptors.AffiliateDriverReferralsPropertyDescriptor
	////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
	////    For example: 
	////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
	//public static partial class EntityPropertyDescriptors {
	//	public partial class AffiliateDriverReferralsPropertyDescriptor : AdaptedPropertyDescriptor {
	//    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
	//    public AdaptedPropertyDescriptor Age {
	//      get { return this.Get("Age"); }
	//    }
	//  }
	//} 
	#endregion

}