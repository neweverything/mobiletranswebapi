﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Util;
using IdeaBlade.Persistence.Rdb;

using CabRideEngine.Utils;
using TaxiPassCommon;

using Csla.Validation;

namespace CabRideEngine {
    /// <summary>
    /// The developer class representing the InvoiceIssues business object class.
    /// </summary>
    /// <remarks>
    /// <para>Place all application-specific business logic in this class.</para>
    /// <para>
    /// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
    /// and will not overwrite changes.
    /// </para>
    /// <para>
    /// This class inherits from the generated class, its companion in the pair of classes 
    /// defining a business object. Properties in the generated class may be overridden in 
    /// order to change the behavior or add business logic.
    /// </para>
    /// <para>
    /// This class is the final class in the inheritance chain for this business object. 
    /// It should not be inherited.
    /// </para>
    /// </remarks>
    [Serializable]
    public sealed partial class InvoiceIssues : InvoiceIssuesDataRow {

        #region Constructors -- DO NOT MODIFY

        // Do not create constructors for this class
        // Developers cannot create instances of this class with the "new" keyword
        // You should write a static Create method instead; see the "Suggested Customization" region
        // Please review the documentation to learn about entity creation and inheritance

        // This private constructor prevents accidental instance creation via 'new';
        // it is also required if the class is visible from a web service.
        // DO NOT REMOVE **
        private InvoiceIssues() : this(null) {
        }

        /// <summary>
        /// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
        /// </summary>
        /// <param name="pRowBuilder"></param>
        public InvoiceIssues(DataRowBuilder pRowBuilder)
            : base(pRowBuilder) {
        }

        #endregion

        #region Suggested Customizations

        //    // Use this factory method template to create new instances of this class
        //    public static InvoiceIssues Create(PersistenceManager pManager,
        //      ... your creation parameters here ...) { 
        //
        //      InvoiceIssues aInvoiceIssues = pManager.CreateEntity<InvoiceIssues>();
        //
        //      // if this object type requires a unique id and you have implemented
        //      // the IIdGenerator interface implement the following line
        //      pManager.GenerateId(aInvoiceIssues, // add id column here //);
        //
        //      // Add custom code here
        //
        //      aInvoiceIssues.AddToManager();
        //      return aInvoiceIssues;
        //    }

        //    // Implement this method if you want your object to sort in a specific order
        //    public override int CompareTo(Object pObject) {
        //    }

        //    // Implement this method to customize the null object version of this class
        //    protected override void UpdateNullEntity() {
        //    }

        #endregion

        // Add additional logic to your business object here...
        public static InvoiceIssues Create(Invoices pInvoice) {
            InvoiceIssues oIssue = null;
            PersistenceManager oPM = pInvoice.PersistenceManager;
            try {
                oIssue = oPM.CreateEntity<InvoiceIssues>();

                // Using the IdeaBlade Id Generation technique
                //oPM.GenerateId(oIssue, InvoiceIssues.InvoiceIssueIDEntityColumn);
                oIssue.InvoiceID = pInvoice.InvoiceID;
                oIssue.TransDate = pInvoice.InvoiceDate;
                oIssue.AddToManager();

            } catch (Exception ex) {
                throw ex;
            }
            return oIssue;
        }

        protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
            if (IsDeserializing) {
                return;
            }
            base.OnColumnChanging(pArgs);
            //DoAudit(pArgs);
        }

        private void DoAudit(DataColumnChangeEventArgs pArgs) {
            if (!this.PersistenceManager.IsClient) {
                return;
            }
            if (this.RowState == DataRowState.Added) {
                return;
            }
            if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
                AuditRecord = InvoiceIssuesAudit.Create(this.PersistenceManager, this);
            }
        }

        protected override void AddRules(RuleList pList) {
            pList.Add(IsVoucherNoUnique, VoucherNoEntityColumn.ColumnName);
        }

        private bool IsVoucherNoUnique(object pTarget, RuleArgs e) {
            InvoiceIssues oIssue = pTarget as InvoiceIssues;
            if (string.IsNullOrEmpty(oIssue.VoucherNo)) {
                return true;
            }
            RdbQuery qry = new RdbQuery(typeof(InvoiceIssues), InvoiceIssues.VoucherNoEntityColumn, EntityQueryOp.EQ, oIssue.VoucherNo);
            qry.AddClause(InvoiceIssues.InvoiceIssueIDEntityColumn, EntityQueryOp.NE, oIssue.InvoiceIssueID);
            EntityList<InvoiceIssues> oList = this.PersistenceManager.GetEntities<InvoiceIssues>(qry);
            if (oList.Count > 0) {
                return TaxiPassCommon.WindowUtils.Verify("Voucher No exists in " + oList.Count.ToString() + " other invoices.  Continue?");
            }
            return true;
        }


    }

    #region EntityPropertyDescriptors.InvoiceIssuesPropertyDescriptor
    ////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
    ////    For example: 
    ////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
    //public static partial class EntityPropertyDescriptors {
    //	public partial class InvoiceIssuesPropertyDescriptor : BaseEntityPropertyDescriptor {
    //    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
    //    public AdaptedPropertyDescriptor Age {
    //      get { return this.Get("Age"); }
    //    }
    //  }
    //} 
    #endregion

}
