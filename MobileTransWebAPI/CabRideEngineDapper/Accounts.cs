﻿using CabRideEngineDapper.Utils;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using TaxiPassCommon;

namespace CabRideEngineDapper {
	public partial class Accounts {

		#region Save
		public new bool Save() {
			using (var conn = SqlHelper.OpenSqlConnection()) {
				return Save(conn);
			}
		}

		public bool Save(SqlConnection pConn) {
			return Save(pConn, CabRideDapperSettings.GetAppName());
		}

		public new bool Save(SqlConnection pConn, string pModifiedBy) {
			bool result = false;
			if (IsNewRecord) {
				ModifiedDate = DateTime.UtcNow;
				ModifiedBy = pModifiedBy;
				this.AccountID = NextID.GetNextKey(Accounts.TableDef.TABLE_NAME);

				int? id = pConn.Insert(this);
				this.AccountID = id.Value;
				result = true;
			} else {
				ModifiedBy = pModifiedBy;
				ModifiedDate = DateTime.UtcNow;
				result = pConn.Update(this) > 0;
			}
			return result;
		}
		#endregion


		#region Relationships
		private Affiliate mAffiliate;
		public Affiliate Affiliate {
			get {
				if (mAffiliate == null) {
					if (IsNullEntity || !this.AffiliateID.HasValue) {
						mAffiliate = Affiliate.GetNullEntity();
					} else {
						mAffiliate = Affiliate.GetAffiliate(this.AffiliateID.Value);
					}
				}
				return mAffiliate;
			}
		}

		private usp_AccountRef_Flattened mAccountRef_Flattened;
		public usp_AccountRef_Flattened AccountRef_Flattened {
			get {
				//if (mAccountRef_Flattened == null || mAccountRef_Flattened.) {
				using (var conn = SqlHelper.OpenSqlConnection()) {
					mAccountRef_Flattened = usp_AccountRef_Flattened.Execute(conn, this.AccountID);
				}
				//}
				return mAccountRef_Flattened;
			}
		}
		#endregion

		public static Accounts GetAccountBySubDomain(SqlConnection pConn, string pSubDomain) {
			if (pSubDomain.IsNullOrEmpty()) {
				return Accounts.GetNullEntity();
			}
			string sql = string.Format("Select * FROM Accounts WITH (NoLock) WHERE {0} = @{0}", Accounts.TableDef.SubDomain);
			DynamicParameters param = new DynamicParameters();
			param.Add(Accounts.TableDef.SubDomain, pSubDomain);
			return pConn.Query<Accounts>(sql, param).DefaultIfEmpty(GetNullEntity()).FirstOrDefault();
		}

		public static Accounts GetAccount(string pAccountNo) {
			using (var conn = SqlHelper.OpenSqlConnection()) {
				return GetAccount(conn, pAccountNo);
			}
		}

		public static Accounts GetAccount(SqlConnection pConn, string pAccountNo) {
			Accounts rec = Accounts.GetNullEntity();

			if (pAccountNo.IsNullOrEmpty()) {
				rec = pConn.Query<Accounts>("usp_AccountsGet", commandType: CommandType.StoredProcedure).DefaultIfEmpty(rec).FirstOrDefault();
			} else {
				rec = pConn.Query<Accounts>("usp_AccountsGet", new { AccountNo = pAccountNo }, commandType: CommandType.StoredProcedure).DefaultIfEmpty(rec).FirstOrDefault();
				if (rec.IsNullEntity) {
					Affiliate aff = Affiliate.GetByAccountCode(pConn, pAccountNo);
					if (!aff.IsNullEntity) {
						rec = Create();
						rec.AccountNo = pAccountNo;
						rec.Name = aff.Name;
						rec.AffiliateID = aff.AffiliateID;
						rec.SubDomain = "";
						rec.Save();
					}
				}
			}


			return rec;
		}



		public static List<Accounts> GetAccounts(SqlConnection pConn) {
			string sql = string.Format("Select * FROM Accounts WITH (NoLock) Order By {0}", Accounts.TableDef.Name);

			return pConn.Query<Accounts>(sql).ToList();
		}

		public static Accounts GetAccountIncludeAffiliate(SqlConnection pConn, string pAccountNo) {
			string sql = @"SELECT * FROM dbo.Accounts WITH (NOLOCK)
							LEFT JOIN dbo.Affiliate ON Affiliate.AffiliateID = Accounts.AffiliateID
							WHERE Accounts.AccountNo = @AccountNo";

			return pConn.Query<Accounts, Affiliate, Accounts>(sql,
				(acct, aff) => { acct.mAffiliate = aff; return acct; },
				new { AccountNo = pAccountNo },
				splitOn: "AffiliateID").DefaultIfEmpty(GetNullEntity()).FirstOrDefault();
		}


		public static Accounts GetAccountByAffiliate(SqlConnection pConn, long pAffiliateID) {
			string sql = @"SELECT  *
                            FROM    dbo.Accounts WITH (NOLOCK)
                            WHERE   AffiliateID = @AffiliateID";

			return pConn.Get<Accounts>(sql, new { AffiliateID = pAffiliateID });
		}


		public static Accounts GetAccountByCodeOrName(SqlConnection pConn, string pAccount) {
			string sql = @"SELECT  *
							FROM    Accounts WITH (NOLOCK)
							WHERE   AccountNo = @Account
									OR Name = @Account";

			return pConn.Get<Accounts>(sql, new { Account = pAccount });
		}

		private AccountRef mISOLogo;
		[Editable(false)]
		public string ISOLogo {
			get {
				CreateISOLogo();
				return mISOLogo.ValueString;
			}
			set {
				CreateISOLogo();
				mISOLogo.ValueString = value;
			}
		}

		private void CreateISOLogo() {
			if (mISOLogo == null) {
				using (var conn = SqlHelper.OpenSqlConnection()) {
					mISOLogo = AccountRef.GetCreate(conn, this.AccountID, "Account", "ISOLogo", "");
				}
			}
		}

	}
}
