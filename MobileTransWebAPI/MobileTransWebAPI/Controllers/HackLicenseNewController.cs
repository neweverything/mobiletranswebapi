﻿using CabRideEngineDapper;
using CabRideEngineDapper.Utils;
using MobileTransWebAPI.Models;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Text;
using System.Web.Http;
using TaxiPassCommon;

namespace MobileTransWebAPI.Controllers {
    public class HackLicenseNewController : ApiController {

        // **************************************************************************
        // Upload a Driver's HackLicense image
        // **************************************************************************
        public HackLicense AddHackLicense(HackLicense pHackLicense) {
            HackLicense result = new HackLicense();

            //pRequest = pRequest.DecryptIceKey();
            //DriverProfile info = js.Deserialize<DriverProfile>(pRequest);
            WebApiApplication.LogIt("AddHackLicenseNew", JsonConvert.SerializeObject(pHackLicense));


            DateTime requestDate = DateTime.Today.AddDays(-1);
            if (pHackLicense != null) {
                requestDate = pHackLicense.RequestKey.DecryptIceKey().ToDateTime();
            }

            if (WebApiApplication.GetUTCServerTime().Subtract(requestDate).TotalMinutes > 1) {
                result.ErrorMsg = "Invalid Access";
                return result;
            }

            if (pHackLicense.AffiliateDriverID < 1) {
                result.ErrorMsg = "Invalid Request";
                return result;
            }

            try {
                using (var conn = SqlHelper.OpenSqlConnection()) {
                    AffiliateDrivers affDriver = AffiliateDrivers.GetDriver(conn, pHackLicense.AffiliateDriverID);
                    if (affDriver.IsNullEntity) {
                        result.ErrorMsg = "Invalid Driver";
                        return result;
                    }
                    if (affDriver.AffiliateID != pHackLicense.AffiliateID) {
                        result.ErrorMsg = "Request configured incorrectly";
                        return result;
                    }


                    DriverHackLicense rec = DriverHackLicense.GetCreateByAffDriverID(conn, affDriver.AffiliateDriverID);

                    string id = pHackLicense.AffiliateDriverID == 0 ? pHackLicense.DriverID.ToString() : pHackLicense.AffiliateDriverID.ToString();

                    if (!pHackLicense.LicenseImageBase64.IsNullOrEmpty()) {
                        TaxiPassCommon.Images.ImageHelper oImgHelper = new TaxiPassCommon.Images.ImageHelper();
                        System.Drawing.Image oImg = oImgHelper.Base64ToImage(pHackLicense.LicenseImageBase64);

                        if (!Directory.Exists(Properties.Settings.Default.HackLicenseImagePath)) {
                            Directory.CreateDirectory(Properties.Settings.Default.HackLicenseImagePath);
                        }
                        string path = Path.Combine(Properties.Settings.Default.HackLicenseImagePath, id);
                        if (!Directory.Exists(path)) {
                            Directory.CreateDirectory(path);
                        }

                        string fileName = string.Format("{0}.png", DateTime.Now.ToString("yyyyMMddhhmmss"));
                        path = Path.Combine(path, fileName);
                        oImg.Save(path, System.Drawing.Imaging.ImageFormat.Png);

                        SystemDefaults oDefaults = SystemDefaults.GetDefaults(conn);
                        //string sURL = oDefaults.VoucherImageURL.Replace("/vouchers/", "/DriverProfileImage/");
                        string temp = "";
                        if (fileName.Contains("/temp/")) {
                            temp = "temp";
                        }
                        pHackLicense.LicenseImageURL = string.Format($"/HackLicense/{temp}/{id}/{fileName}").Replace("//", "/");
                        //rec.LicenseValidated = false;
                    }


                    //CabRideEngineEF.CabDriverRef rec = CabRideEngineEF.CabDriverRef.GetOrCreate(ctx, "HackLicenseInfo", pHackLicense.DriverID);
                    //HackLicenseInfo info = JsonConvert.DeserializeObject<HackLicenseInfo>(rec.ValueString);

                    // allows admin user to upload image and validate on a single post
                    if (pHackLicense.AdminOverride) {
                        rec.LicenseValidated = pHackLicense.LicenseValidated;
                        if (rec.LicenseExpiry.GetValueOrDefault("1/1/1900".ToDateTime()).Year < 2000) {
                            rec.LicenseExpiry = new DateTime(DateTime.Now.Year + 5, 1, 1);
                        }
                    } else {
                        //if (rec.LicenseExpiry != null && rec.LicenseExpiry != pHackLicense.LicenseExpiry) {
                        //	rec.LicenseValidated = false;
                        //}
                        //if (rec.LicenseNo != pHackLicense.LicenseNo) {
                        //	rec.LicenseValidated = false;
                        //}
                    }
                    rec.LicenseClass = pHackLicense.LicenseClass;
                    if (pHackLicense.LicenseExpiry.Year > 2000) {
                        rec.LicenseExpiry = pHackLicense.LicenseExpiry;
                    }
                    if (!pHackLicense.LicenseImageURL.IsNullOrEmpty()) {
                        rec.LicenseImageURL = pHackLicense.LicenseImageURL;
                    }
                    rec.LicenseNo = pHackLicense.LicenseNo;

                    //if (info.LicenseExpiryMonth != pHackLicense.HackLicenseRec.LicenseExpiryMonth || info.LicenseExpiryYear != pHackLicense.HackLicenseRec.LicenseExpiryYear) {
                    //	pHackLicense.HackLicenseRec.LicenseValidated = false;
                    //}
                    //rec.ValueString = JsonConvert.SerializeObject(pHackLicense.HackLicenseRec);

                    rec.Save(Properties.Settings.Default.DefaultUserLogin);

                }


            } catch (Exception exUnknown) {
                pHackLicense.ErrorNo = 3001;
                pHackLicense.ErrorMsg = exUnknown.Message;

                StringBuilder sInput = new StringBuilder();
                sInput.AppendFormat("{0}<br>{1}", this.GetType().Name, JsonConvert.SerializeObject(pHackLicense));
                Utils.EmailErrors.SendErrorEmail(this.GetType().Name, "Input: " + sInput.ToString() + Environment.NewLine + Environment.NewLine + exUnknown.Message);
            }
            return pHackLicense;
        }


    }
}