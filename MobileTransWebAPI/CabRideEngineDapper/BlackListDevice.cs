﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

using Dapper;
using TaxiPassCommon;

namespace CabRideEngineDapper {
	public partial class BlackListDevice {


		public static BlackListDevice GetDevice(SqlConnection pConn, string pSerialNo) {
			string sql = @"SELECT  *
							FROM    BlackListDevice
							WHERE   SerialNo = @SerialNo";

			return pConn.Get<BlackListDevice>(sql, new { SerialNo = pSerialNo });
		}
	}
}
