﻿using CabRideEngineDapper.Utils;
using Dapper;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace CabRideEngineDapper {
	public partial class BlackListBIN {

		public static List<BlackListBIN> GetAll() {
			using (var conn = SqlHelper.OpenSqlConnection()) {
				string sql = @"SELECT * FROM BlackListBIN
							   ORDER BY Bin";
				return conn.Query<BlackListBIN>(sql).ToList();
			}
		}

		public void Undo() {
			using (var conn = SqlHelper.OpenSqlConnection()) {
				string sql = @"SELECT * FROM BlackListBIN
							   WHERE BlackListBinID = @ID";
				var rec = conn.Get<BlackListBIN>(sql, new { ID = this.BlackListBinID });
				this.Bin = rec.Bin;
				this.ModifiedBy = rec.ModifiedBy;
				this.ModifiedDate = rec.ModifiedDate;
				this.Reason = rec.Reason;
				this.RowState = rec.RowState;
				this.RowVersion = rec.RowVersion;

			}
		}

		public int Delete() {
			using (var conn = SqlHelper.OpenSqlConnection()) {
				return conn.Delete(this);
			}
		}

		public static BlackListBIN GetByBIN(SqlConnection conn, string pBIN) {
			string sql = @"SELECT * FROM BlackListBIN
							WHERE Bin = @Bin";

			return conn.Get<BlackListBIN>(sql, new { Bin = pBIN });
		}
	}
}
