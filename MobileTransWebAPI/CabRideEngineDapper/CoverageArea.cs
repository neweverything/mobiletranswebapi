﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Dapper;
using TaxiPassCommon;

using CabRideEngineDapper.Utils;

namespace CabRideEngineDapper {
	public partial class CoverageArea {

		private ZIPCode mZIPCode_CoverageArea;
		public ZIPCode ZIPCode_CoverageArea {
			get {
				if (mZIPCode_CoverageArea == null) {
					using (var conn = SqlHelper.OpenSqlConnection()) {
						string sql = "SELECT * FROM ZiPCode WHERE ZipCode = @zipCode";
						mZIPCode_CoverageArea = conn.Query<CabRideEngineDapper.ZIPCode>(sql, new { zipCode = ZIPCode }).DefaultIfEmpty(CabRideEngineDapper.ZIPCode.GetNullEntity()).FirstOrDefault();
					}
				}
				return mZIPCode_CoverageArea;
			}
		}
	}
}
