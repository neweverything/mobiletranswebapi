﻿using CabRideEngine;
using IdeaBlade.Persistence;
using MobileTransWebAPI.Models;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Text;
using System.Web.Http;
using TaxiPassCommon;

namespace MobileTransWebAPI.Controllers {

    public class HackLicenseController : ApiController {

        public HackLicense Get() {
            string req = "4E7427F059FB4182A5125BC1008EDDBA488B3D165BF995E9B19054D93494F8D641CD408EA523E70F02C20B00D05035AB93AFE4FDFE05633384E4F6A440C2C2D8999BF50FDCA2274716A0498F9C2C60483EACF215026BA360785D214284D996E7A1CF3519574B1B0B6B8CF9DC55E521103983321FAD007DE1C9109847D28B7AC74931BE7D9D3493D750C0C8F61802C26044AC1D05CEF90195CBAF2597B262AD477E792C6913EA75BDE23156B4A8191A363F3EFF99EE0B10C97284D42CA96B33683983321FAD007DE180862E4322FEAAED6112F9C6A3E1F032961CF35F6F88A4671F74FC076B37D00FB2055209890874CC2E063D7C242136599256A267";
            HackLicense rec = null;
            rec = JsonConvert.DeserializeObject<HackLicense>(req.DecryptIceKey());
            rec.RequestKey = DateTime.UtcNow.ToString();

            return AddHackLicense(rec);
        }



        //public const string mAPITitle = "HackLicense";

        // **************************************************************************
        // Upload a Driver's HackLicense image
        // **************************************************************************
        public HackLicense AddHackLicense(HackLicense pHackLicense) {
            HackLicense result = new HackLicense();

            //pRequest = pRequest.DecryptIceKey();
            //DriverProfile info = js.Deserialize<DriverProfile>(pRequest);
            WebApiApplication.LogIt("AddHackLicense", JsonConvert.SerializeObject(pHackLicense));


            DateTime requestDate = DateTime.Today.AddDays(-1);
            if (pHackLicense != null) {
                requestDate = pHackLicense.RequestKey.DecryptIceKey().ToDateTime();
            }

            if (WebApiApplication.GetUTCServerTime().Subtract(requestDate).TotalMinutes > 1) {
                result.ErrorMsg = "Invalid Access";
                return result;
            }

            PersistenceManager oPM = WebApiApplication.UserPM;
            if (oPM == null) {
                result.ErrorMsg = "Could not connect to database";
                return result;
            }

            Drivers driver = Drivers.GetDriver(oPM, pHackLicense.DriverID);
            if (driver.IsNullEntity) {
                result.ErrorMsg = "Invalid Driver Access";
                return result;
            }

            //if (pHackLicense.LicenseImageBase64.IsNullOrEmpty()) {
            //	result.ErrorMsg = "Missing License Image data";
            //}


            try {
                using (var conn = CabRideEngineDapper.Utils.SqlHelper.OpenSqlConnection()) {
                    CabRideEngineDapper.AffiliateDrivers affDriver = CabRideEngineDapper.AffiliateDrivers.GetDriverByCallInCell(conn, driver.Cell);
                    CabRideEngineDapper.DriverHackLicense rec = CabRideEngineDapper.DriverHackLicense.GetCreateByAffDriverID(conn, affDriver.AffiliateDriverID);

                    if (!pHackLicense.LicenseImageBase64.IsNullOrEmpty()) {
                        TaxiPassCommon.Images.ImageHelper oImgHelper = new TaxiPassCommon.Images.ImageHelper();
                        System.Drawing.Image oImg = oImgHelper.Base64ToImage(pHackLicense.LicenseImageBase64);

                        if (!Directory.Exists(Properties.Settings.Default.HackLicenseImagePath)) {
                            Directory.CreateDirectory(Properties.Settings.Default.HackLicenseImagePath);
                        }
                        string path = Path.Combine(Properties.Settings.Default.HackLicenseImagePath, pHackLicense.DriverID.ToString());
                        if (!Directory.Exists(path)) {
                            Directory.CreateDirectory(path);
                        }

                        string fileName = string.Format("{0}.png", DateTime.Now.ToString("yyyyMMddhhmmss"));
                        path = Path.Combine(path, fileName);
                        oImg.Save(path, System.Drawing.Imaging.ImageFormat.Png);

                        SystemDefaults oDefaults = SystemDefaults.GetDefaults(oPM);
                        //string sURL = oDefaults.VoucherImageURL.Replace("/vouchers/", "/DriverProfileImage/");
                        string temp = "";
                        if (fileName.Contains("/temp/")) {
                            temp = "temp";
                        }
                        pHackLicense.LicenseImageURL = string.Format("/HackLicense/{0}/{1}/{2}", temp, pHackLicense.DriverID, fileName);
                        //rec.LicenseValidated = false;
                    }


                    //CabRideEngineEF.CabDriverRef rec = CabRideEngineEF.CabDriverRef.GetOrCreate(ctx, "HackLicenseInfo", pHackLicense.DriverID);
                    //HackLicenseInfo info = JsonConvert.DeserializeObject<HackLicenseInfo>(rec.ValueString);

                    // allows admin user to upload image and validate on a single post
                    if (pHackLicense.AdminOverride) {
                        rec.LicenseValidated = pHackLicense.LicenseValidated;
                        if (rec.LicenseExpiry.GetValueOrDefault("1/1/1900".ToDateTime()).Year < 2000) {
                            rec.LicenseExpiry = new DateTime(DateTime.Now.Year + 5, 1, 1);
                        }
                    } else {
                        //if (rec.LicenseExpiry != null && rec.LicenseExpiry != pHackLicense.LicenseExpiry) {
                        //	rec.LicenseValidated = false;
                        //}
                        //if (rec.LicenseNo != pHackLicense.LicenseNo) {
                        //	rec.LicenseValidated = false;
                        //}
                    }
                    rec.LicenseClass = pHackLicense.LicenseClass;
                    if (pHackLicense.LicenseExpiry.Year > 2000) {
                        rec.LicenseExpiry = pHackLicense.LicenseExpiry;
                    }
                    if (!pHackLicense.LicenseImageURL.IsNullOrEmpty()) {
                        rec.LicenseImageURL = pHackLicense.LicenseImageURL;
                    }
                    rec.LicenseNo = pHackLicense.LicenseNo;

                    //if (info.LicenseExpiryMonth != pHackLicense.HackLicenseRec.LicenseExpiryMonth || info.LicenseExpiryYear != pHackLicense.HackLicenseRec.LicenseExpiryYear) {
                    //	pHackLicense.HackLicenseRec.LicenseValidated = false;
                    //}
                    //rec.ValueString = JsonConvert.SerializeObject(pHackLicense.HackLicenseRec);

                    rec.Save(Properties.Settings.Default.DefaultUserLogin);
                }


            } catch (Exception exUnknown) {
                pHackLicense.ErrorNo = 3001;
                pHackLicense.ErrorMsg = exUnknown.Message;


                StringBuilder sInput = new StringBuilder();
                sInput.AppendFormat("{0}<br>{1}", this.GetType().Name, JsonConvert.SerializeObject(pHackLicense));
                Utils.EmailErrors.SendErrorEmail(this.GetType().Name, "Input: " + sInput.ToString() + Environment.NewLine + Environment.NewLine + exUnknown.Message);
            }
            oPM.Disconnect();
            return pHackLicense;
        }



    }
}
