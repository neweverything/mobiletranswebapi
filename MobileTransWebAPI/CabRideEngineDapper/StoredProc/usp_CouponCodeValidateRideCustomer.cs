﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Dapper;
using System.Data;
using CabRideEngineDapper.Utils;

namespace CabRideEngineDapper {
	public class usp_CouponCodeValidateRideCustomer {
		public decimal Amount { get; set; }
		public decimal MinCharge { get; set; }
		public string Message { get; set; }

		public static usp_CouponCodeValidateRideCustomer Validate(string pCouponCode, string pCustomerToken) {
			using (var conn = SqlHelper.OpenSqlConnection()) {
				return Validate(conn, pCouponCode, pCustomerToken);
			}
		}

		public static usp_CouponCodeValidateRideCustomer Validate(SqlConnection pConn, string pCouponCode, string pCustomerToken) {
			DynamicParameters param = new DynamicParameters();
			param.Add("CouponCode", pCouponCode);
			param.Add("CustomerToken", pCustomerToken);
			return pConn.Query<usp_CouponCodeValidateRideCustomer>("usp_CouponCodeValidateRideCustomer", param, commandType: CommandType.StoredProcedure).FirstOrDefault();
		}
	}
}
