﻿using CabRideEngineDapper.Utils;
using Dapper;
using MobileTransWebAPI.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Http;
using TaxiPassCommon;

namespace MobileTransWebAPI.Controllers {

    public class AffDriverLoginSimpleController : ApiController {
        //
        // GET: /AffDriverLoginSimple/

        public DriverLoginResult Get() {
            string js = "8E1803A0D37D7554AB4E377502C041286B966E2A9A0D1BF5A6AE1DB617EF3280A06F57A0F3EEBBA611FDA672A22809E23825DEA8225D5E49929C273A98BE63056880F658AF0CF34D23E9AF8B958F7838F379A7CC583D4DDA4601DA963EC9952FBB4174132583EF620583A1F37D832F8BF95D30C436250A824EB38CECB96F367FAAC080D110AE408FDDB53CB784661CD4DB983BEC9296C42BA0C99D4850E89078CAA08DFAE561FFB3C89889191B80A03F666E7C461DC05DA78DA12664C78BEF167979E4FE26A94E58512FB5271BC0486C1196D973F6465434F495DBA6984402C389FC86EA900C470844661CCFA30F68BFB16332286D35D3800FF4603965D7C7C0E53F69DD8D1C8099898A95381B890BACF55C29E06A30331A490EFBEEFC04A403DE40667E145222C0744A25CE13A11DFCBAFF90DDC0A8ACDA375F979973ABA155E7F29F8C0E082D5622A36875B51120E51D4A4E998DD8C83E1C0769F8068BCCE2C13151D7ECEA348E936CE888DD1763A50F81CE3E548C3067E10F99353A40E2D00A95937A0671EAE590A00D1BF16D54BBB882E2FB0739588218BAD17C";
            return Login(js);
        }

        [HttpGet]
        public DriverLoginResult Login(string pRequest) {
            DriverLoginResult result = new DriverLoginResult();

            string request = pRequest.DecryptIceKey();
            WebApiApplication.LogIt("AffiliateDriverLoginSimple", request);

            AffDriverLoginInfo info = new AffDriverLoginInfo();
            try {
                info = JsonConvert.DeserializeObject<AffDriverLoginInfo>(request);
            } catch (Exception ex) {
                result.ErrorMsg = ex.Message;
                return result;
            }

            DateTime requestDate = Convert.ToDateTime(info.RequestTime);

            if (WebApiApplication.GetUTCServerTime().Subtract(requestDate).TotalMinutes > 1) {
                result.ErrorMsg = "Invalid Access";
                return result;
            }


            using (var conn = SqlHelper.OpenSqlConnection()) {
                string sql = @"DECLARE @AffiliateID BIGINT = (SELECT   AffiliateID
                                                                FROM     dbo.AffiliateDrivers
                                                                WHERE    Cell = @Cell
                                                                );

                                DECLARE @DriverID BIGINT = (SELECT  DriverID
                                                            FROM    Drivers
                                                            WHERE   AffiliateID = @AffiliateID
                                                                    AND Name = 'Charge Card'
                                                            );

                                SELECT  AffiliateDrivers.AffiliateID
                                        , AffiliateDriverID
                                        , @DriverID DriverID
                                        , ISNULL(ServiceType, 'Taxi') ServiceType
                                        , AffiliateDrivers.Name DriverName
                                        , Affiliate.Name AffiliateName
                                        , AffiliateDrivers.PaidByRedeemer
                                        , StartingFare
                                        , CASE WHEN Driver.ChargeMaxAmount < 1 THEN CASE WHEN AffiliateDriverDefaults.ChargeMaxAmount < 1 THEN SysDef.DriverChargeMaxAmount
                                                                                        ELSE AffiliateDriverDefaults.ChargeMaxAmount
                                                                                    END
                                                ELSE Driver.ChargeMaxAmount
                                          END ChargeMaxAmount
                                        , CASE WHEN Driver.ChargeMaxSwipeAmount < 1 THEN CASE WHEN AffiliateDriverDefaults.ChargeMaxSwipeAmount < 1 THEN SysDef.DriverChargeMaxSwipeAmount
                                                                                            ELSE AffiliateDriverDefaults.ChargeMaxSwipeAmount
                                                                                        END
                                                ELSE Driver.ChargeMaxSwipeAmount
                                          END ChargeMaxSwipeAmount
                                        , ISNULL(CallInCell, AffiliateDrivers.Cell) TaxiPayCell
                                        , AffiliateDrivers.PIN TaxiPayPin
                                        , AffiliateDrivers.Active
                                        , SysDef.CellAdminCode AdminCode
                                        , CASE WHEN HHChargeKioskSlip = 1
                                                    AND (ISNULL(BankAccountNumber, '') != ''
                                                        OR (ISNULL(TransCardAdminNo, '') != ''
                                                            AND TransCardAsDriverIDOnly = 0
                                                            )
                                                        ) THEN CONVERT(BIT, 1)
                                                ELSE 0
                                          END ChargeKioskSlip
                                        , AffiliateDrivers.FraudSuspected
                                        , CASE WHEN ISNULL(BankAccountNumber, '') = '' THEN CONVERT(BIT, 0) ELSE CONVERT(BIT, 1) END HasBankInfo
                                        , StoreForward
                                        , ISNULL(SelfRedemptionMaxVoucherAge.Age, 0) SelfRedemptionMaxVoucherAge
                                        , TimeZone
                                FROM    dbo.AffiliateDrivers
                                LEFT JOIN dbo.Affiliate ON Affiliate.AffiliateID = AffiliateDrivers.AffiliateID
                                LEFT JOIN dbo.AffiliateDriverDefaults ON AffiliateDriverDefaults.AffiliateID = Affiliate.AffiliateID
                                OUTER APPLY (SELECT *
                                                FROM   dbo.Drivers
                                                WHERE  DriverID = @DriverID
                                            ) Driver
                                OUTER APPLY (SELECT TOP 1
                                                    *
                                                FROM   dbo.SystemDefaults
                                            ) SysDef
                                OUTER APPLY (SELECT CONVERT(INT, ValueString) Age
                                             FROM   dbo.SystemDefaultsDict
                                             WHERE  ReferenceKey = 'SelfRedemptionMaxVoucherAge'
                                                    AND ReferenceGroup = 'DriverPay'
                                            ) SelfRedemptionMaxVoucherAge
                                WHERE   AffiliateDrivers.Cell = @Cell
                                        OR CallInCell = @Cell;

                                SELECT  StartAmount
                                        , EndAmount
                                        , Fee
                                FROM    dbo.AffiliateFees
                                WHERE   AffiliateID = @AffiliateID
                                ORDER BY StartAmount;";

                var reader = conn.QueryMultiple(sql, new { Cell = info.Phone });
                result = reader.Read<DriverLoginResult>().FirstOrDefault();

                result.TaxiPayPin = result.TaxiPayPin.DecryptIceKey();
                result.AdminCode = result.AdminCode.DecryptIceKey();

                if (result == null || result.TaxiPayPin != info.PIN) {
                    result = new DriverLoginResult();
                    result.ErrorMsg = "Invalid Phone or PIN";
                    return result;
                }

                if (!result.Active || result.FraudSuspected) {
                    result = new DriverLoginResult();
                    result.ErrorMsg = "Error: Your account is currently disabled.";
                    return result;
                }

                try {
                    result.TPFeesList = reader.Read<AffFees>().ToList();
                } catch (Exception) {
                    result.TPFeesList = new List<AffFees>();
                }
                if (result.TPFeesList.Count == 0) {
                    result.TPFeesList = new List<AffFees>() { new AffFees() { StartAmount = 0.01M, EndAmount = 0, Fee = 5.50M } };
                }

                try {
                    if (!info.SimSerialNumber.IsNullOrEmpty() || !info.SubscriberId.IsNullOrEmpty() || !info.DevicePhoneNumber.IsNullOrEmpty()) {
                        sql = @"SELECT  *
                            FROM    dbo.Devices
                            WHERE   SimSerialNumber = @SimSerialNumber
                                    OR PhoneNumber = @PhoneNumber
                                    OR SubscriberId = @SubscriberId";

                        DynamicParameters parms = new DynamicParameters();
                        parms.Add("SimSerialNumber", info.SimSerialNumber.IsNullOrEmpty() ? "x" : info.SimSerialNumber);
                        parms.Add("SubscriberId", info.SubscriberId.IsNullOrEmpty() ? "x" : info.SubscriberId);
                        parms.Add("PhoneNumber", info.DevicePhoneNumber.IsNullOrEmpty() ? "x" : info.DevicePhoneNumber);
                        CabRideEngineDapper.Devices device = conn.Get<CabRideEngineDapper.Devices>(sql, parms);
                        if (device == null || device.IsNullEntity) {
                            device = CabRideEngineDapper.Devices.Create();
                            device.SimSerialNumber = info.SimSerialNumber;
                            device.SubscriberId = info.SubscriberId;
                            device.PhoneNumber = info.DevicePhoneNumber;
                        }
                        device.Carrier = info.Carrier;
                        device.HardwareManufacturer = info.HardwareManufacturer;
                        device.HardwareModel = info.HardwareModel;
                        device.HardwareOS = info.HardwareOS;
                        device.Save();

                        result.DeviceID = device.DeviceID;

                        sql = @"SELECT  *
                                FROM    dbo.DeviceLogin
                                WHERE   DeviceID = @DeviceID
                                        AND AppName = @AppName";
                        CabRideEngineDapper.DeviceLogin login = conn.Get<CabRideEngineDapper.DeviceLogin>(sql, new { DeviceID = device.DeviceID, AppName = info.AppName });
                        if (login.IsNullEntity) {
                            login = CabRideEngineDapper.DeviceLogin.Create();
                            login.DeviceID = device.DeviceID;
                            login.AppName = info.AppName;
                            login.LoginDate = WebApiApplication.MyTZConverter.FromUniversalTime(result.TimeZone, DateTime.UtcNow);
                        }
                        if (info.InfoUpdate) {
                            login.LastCommunication = WebApiApplication.MyTZConverter.FromUniversalTime(result.TimeZone, DateTime.UtcNow);
                        } else {
                            login.LoginDate = WebApiApplication.MyTZConverter.FromUniversalTime(result.TimeZone, DateTime.UtcNow);
                            login.AffiliateDriverID = result.AffiliateDriverID;
                        }
                        login.VehicleNo = info.Vehicle;
                        login.Save();

                    }
                } catch (Exception ex) {
                    string sErrorEmailRecipient = System.Configuration.ConfigurationManager.AppSettings["ErrorEmailRecipient"];
                    if (!sErrorEmailRecipient.IsNullOrEmpty()) {
                        StringBuilder sHtml = new StringBuilder();
                        sHtml.AppendLine("<hr noshade color=#000000 size=1>");
                        sHtml.AppendLine(DateTime.Now.ToString() + " AffDriverSimpleLogin Error<br>");
                        sHtml.AppendLine("<br>** Error Info **<br>");
                        sHtml.AppendLine("Page: AffDriverSimpleLogin<br>");
                        sHtml.AppendLine("Message: " + ex.Message + "<br>");
                        sHtml.AppendLine("<br>Stack Trace: " + ex.StackTrace + "<br>");

                        // send email if setting is configured in web.config
                        try {
                            string subject = string.Format("{0} Login Error", Properties.Settings.Default.SystemName);
                            TaxiPassCommon.Mail.EMailMessage oMail = new TaxiPassCommon.Mail.EMailMessage(Properties.Settings.Default.EMailFrom, subject, sHtml.ToString());
                            oMail.UseWebServiceToSendMail = Properties.Settings.Default.SendEmailViaWebService;
                            oMail.SendMail(sErrorEmailRecipient);
                        } catch {
                        }
                    }
                }

            }
            return result;
        }
    }
}
