﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

using Dapper;
using TaxiPassCommon;

using CabRideEngineDapper.Utils;

namespace CabRideEngineDapper {
    public partial class DriverPromotions {

        private List<DriverPromotionAffiliates> mDriverPromotionAffiliates;
        public List<DriverPromotionAffiliates> DriverPromotionAffiliateses {
            get {
                if (mDriverPromotionAffiliates == null || mDriverPromotionAffiliates.Count == 0) {
                    using (var conn = SqlHelper.OpenSqlConnection()) {
                        mDriverPromotionAffiliates = DriverPromotionAffiliates.GetAffiliates(conn, DriverPromotionID);
                    }
                }
                return mDriverPromotionAffiliates;
            }
        }

        public static DriverPromotions GetRecord(long pPromoID) {
            using (var conn = SqlHelper.OpenSqlConnection()) {
                return GetRecord(conn, pPromoID);
            }
        }

        public static DriverPromotions GetRecord(SqlConnection pConn, long pPromoID) {
            string sql = "select * from dbo.DriverPromotions with (nolock) WHERE DriverPromotionID = @DriverPromotionID";
            return pConn.Query<DriverPromotions>(sql, new { DriverPromotionID = pPromoID }).DefaultIfEmpty(DriverPromotions.GetNullEntity()).FirstOrDefault();
        }

        public static bool IsPromoCard(SqlConnection pConn, string pCardNo) {
            string sql = "Select count(*) FROM DriverPromotions WITH (NoLock) WHERE CardNumber = @CardNumber";

            int promoListCount = pConn.Query<int>(sql, new {CardNumber = pCardNo }).FirstOrDefault();
            return promoListCount > 0;
        }

        public static List<DriverPromotions> GetPromotionsByCardNo(SqlConnection pConn, string pCardNo) {
            string sql = @"SELECT * FROM  DriverPromotions
                           WHERE CardNumber = @CardNumber";
            return pConn.Query<DriverPromotions>(sql, new { CardNumber = pCardNo}).ToList();
        }

        public static List<DriverPromotions> GetPromotionsForAffiliate(SqlConnection pConn, long pAffiliateID) {
            //string sDate = "'" + DateTime.Today.ToString("MM/dd/yyyy") + "'";
            string sSQL = @"SELECT  DriverPromotions.*
                            FROM    DriverPromotions WITH (NOLOCK)
                            WHERE   (AllAffiliates = 1
                                     AND EndDate >= @Date
                                     AND StartDate <= @Date
                                    )
                                    OR (DriverPromotionID IN (SELECT DISTINCT
                                                                        DriverPromotionID
                                                              FROM      DriverPromotionAffiliates
                                                              WHERE     AffiliateID = @AffiliateID)
                                        AND EndDate >= @Date
                                        AND StartDate <= @Date
                                       )";

            List<DriverPromotions> list = new List<DriverPromotions>();
            try {
                list = pConn.Query<DriverPromotions>(sSQL, new { Date = DateTime.Today, AffiliateID = pAffiliateID}).ToList();

            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }


            return list;

        }

        public static List<DriverPromotions> GetPromotionsForAffiliate(Affiliate pAffiliate) {
            using (var conn = SqlHelper.OpenSqlConnection()) {
                return GetPromotionsForAffiliate(conn, pAffiliate.AffiliateID);
            }
        }

    }
}
