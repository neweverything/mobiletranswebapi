﻿using CabRideEngineDapper;
using CabRideEngineDapper.Utils;
using Dapper;
using MobileTransWebAPI.Models;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.Http;
using TaxiPassCommon;

namespace MobileTransWebAPI.Controllers {

	public class AffDriverLoginSimpleController : ApiController {
		//
		// GET: /AffDriverLoginSimple/

		public DriverLoginResult Get() {

			//using (var conn = SqlHelper.OpenSqlConnection()) {
			//    int[] affIds = new int[] {741, 745 ,749 ,754, 739, 734, 744, 1258, 743, 1088,781,755,796,1269,748,863,777,1293,774,804,759,756,762,736,930,1083,843,799,802,764,770,760,773,1024,1291,795,1292};

			//    foreach (int affId in affIds) {
			//        Affiliate aff = Affiliate.GetAffiliate(conn, affId);

			//        List<AffiliateFees> fees = AffiliateFees.GetFees(affId);
			//        if (fees.Count == 0 || (fees.Count == 1 && fees[0].AffiliateID == 0)) {
			//            AffiliateFees fee = AffiliateFees.Create(aff);
			//            fee.StartAmount = 0.01M;
			//            fee.EndAmount = 20.00M;
			//            fee.Fee = 2;
			//            try {
			//                fee.Save("Hani");
			//            } catch (Exception ex) {
			//                Console.WriteLine(ex.Message);
			//            }

			//            fee = CabRideEngineDapper.AffiliateFees.Create(aff);
			//            fee.StartAmount = 20.01M;
			//            fee.EndAmount = 40.00M;
			//            fee.Fee = 3;
			//            fee.Save("Hani");

			//            fee = CabRideEngineDapper.AffiliateFees.Create(aff);
			//            fee.StartAmount = 40.01M;
			//            fee.EndAmount = 100.00M;
			//            fee.Fee = 4.5M;
			//            fee.Save("Hani");

			//            fee = CabRideEngineDapper.AffiliateFees.Create(aff);
			//            fee.StartAmount = 100.01M;
			//            fee.EndAmount = 200.00M;
			//            fee.Fee = 9;
			//            fee.Save("Hani");

			//            fee = CabRideEngineDapper.AffiliateFees.Create(aff);
			//            fee.StartAmount = 200.01M;
			//            fee.EndAmount = 0.00M;
			//            fee.Fee = 13.5M;
			//            fee.Save("Hani");

			//        }
			//    }

			//}

			string js = "";// 
			js = System.IO.File.ReadAllText(@"c:\temp\test.json");
			//js = "3141C2DD5FE4D21D6ED00F8F1C924A3F6ADF8E907AB2513FF2DD2546A8B35DE2A707DCBE40D214DC76B904940C7F9A1C93D38F54F5049CFB50E7944E8F92E552EEA9CA8BFC3092FE92FBC22BBDFC632D2815A2CD4EADFAE55DA61A9EF169DEB4FA0EF007A5789F71DC372CC9C730378BA765C564A60104E3104D4F43CF97B7D02A6BDCF5D436897C8751E84BD32C446669F136CC4D254344FE780FC9D71BD52C215A6848802E29320CF4CCC98BF70394356D4C39E6E3715CB322C296202EDB75E1BC4CDAB0009F7F6ECAF82F98E8ED8C6A830A81A64C8FA5B44EC43CE58F1745E4D269117254695AF5EA0B0875B06EF4017D2BFC267119AD62480935F6ADC148CFF0CC583F6E84C5AC6839254FF277A8003D68A0BCEAB91EDD2D4930BFEC3510C0584F8E42D7F0C73C816836961C102AA27C27FCF88CA42D2DCC5D92957AADF0B21B417E51DD232EAC6D0AA2FB213AFFE07E2473494D15A02D520084838733060765604ED19B592FD3DF0C6A0D23CDD7CE1946C0DD5E3F428B6D15A75B6549FDFABABA0080D1E2B8343ADD1EA891781C7783743BF1307718B64989798D0F4AC74A8C0CC4CF31AD441394F14432CDA042B5211C136A6491C6FEF325A2BF7FE2A7F159485A013D8E002139D92629E66A514FE1C1D85334B16B442CF91AFA4E0F4DDE12A52262A22B2299EB6FC8".DecryptIceKey();
			var info = JsonConvert.DeserializeObject<AffDriverLoginInfo>(js);
			info.RequestTime = DateTime.UtcNow.ToString("s");
			js = JsonConvert.SerializeObject(info);
			bool sendToProd = false;
			if (sendToProd) {
				ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11;
				RestClient service = WebApiApplication.GetWebService();
				RestRequest request = new RestRequest("AffDriverLoginSimple", Method.GET);
				request.RequestFormat = DataFormat.Json;

				request.AddParameter("pRequest", js);

				var resp = service.Execute<DriverLoginResult>(request);
				Console.WriteLine(resp.Data.AffiliateDriverID);
				return resp.Data;
			} else {
				return Login(js.Encrypt());
			}

		}

		[HttpGet]
		public DriverLoginResult Login(string pRequest) {
			DriverLoginResult result = new DriverLoginResult();

			string request = pRequest.DecryptIceKey();

			WebApiApplication.LogIt("AffiliateDriverLoginSimple", request);

			AffDriverLoginInfo info = new AffDriverLoginInfo();
			try {
				info = JsonConvert.DeserializeObject<AffDriverLoginInfo>(request);
			} catch (Exception ex) {
				result.ErrorMsg = ex.Message;
				return result;
			}

			DateTime requestDate = Convert.ToDateTime(info.RequestTime);

			if (WebApiApplication.GetUTCServerTime().Subtract(requestDate).TotalMinutes > 1) {
				result.ErrorMsg = "Invalid Access";
				return result;
			}

			if (!info.WidgetKey.IsNullOrEmpty()) {
				List<string> valList = info.WidgetKey.DecryptIceKey().Split('|').ToList();
				if (valList.Count == 3) {
					AffiliateDrivers driver = AffiliateDrivers.GetDriver(Convert.ToInt64(valList[1]));
					info.Phone = driver.Cell;
					info.PIN = valList[2];
				} else {
					result.ErrorMsg = "Invalid request";
					return result;
				}
			}
			if (info.Phone.IsNullOrEmpty() || info.PIN.IsNullOrEmpty()) {
				result.ErrorMsg = "Unauthorized";
				return result;
			}


			using (var conn = SqlHelper.OpenSqlConnection()) {

				try {
					var reader = conn.QueryMultiple("usp_AffDriverLoginSimple", new { Cell = info.Phone }, commandType: System.Data.CommandType.StoredProcedure);
					result = reader.Read<DriverLoginResult>().FirstOrDefault();

					if (!info.PIN.IsNullOrEmpty() && info.PIN.Decrypt() != result.TaxiPayPin.Decrypt()) {
						result = new DriverLoginResult();
						result.ErrorMsg = "Invalid PIN";
						return result;
					}

					if (result != null) {
						result.TaxiPayPin = ("" + result.TaxiPayPin).DecryptIceKey();
						result.AdminCode = ("" + result.AdminCode).DecryptIceKey();
					}

					DateTime startOfMonth = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
					DateTime endDate = startOfMonth.AddMonths(1).AddMilliseconds(-1);

					//List<long> paidBatchList = ACHDetail.GetDistinctBatchNumbersForTransCardAlreadyBatched(result.TransCardAdminNo, startOfMonth, endDate);

					if (result == null || result.TaxiPayPin != info.PIN) {
						result = new DriverLoginResult();
						result.ErrorMsg = "Invalid Phone or PIN";
						return result;
					}

					if (!result.Active || result.FraudSuspected) {
						result = new DriverLoginResult();
						result.ErrorMsg = "Error: Your account is currently disabled.";
						return result;
					}

					if (result.DriverID < 1 && result.AffiliateID > 0) {
						reader.Dispose();
						conn.Close();
						Drivers.GetChargeCard(conn, result.AffiliateID);
						return Login(pRequest);
					}


					result.TPFeesList = reader.Read<AffFees>().ToList();
					result.DriverLicense = reader.Read<HackLicense>().FirstOrDefault();

					using (var conn2 = SqlHelper.OpenSqlConnection()) {
						DriverHackLicense hackLicense = DriverHackLicense.GetCreateByAffDriverID(conn2, result.AffiliateDriverID);
						if (!hackLicense.IsNullEntity) {
							result.HackLicense = new HackLicense {
								LicenseClass = hackLicense.LicenseClass,
								LicenseExpiry = hackLicense.LicenseExpiry.GetValueOrDefault(),
								LicenseNo = hackLicense.LicenseNo,
								LicenseImageURL = hackLicense.LicenseImageURL,
								LicenseValidated = hackLicense.LicenseValidated.GetValueOrDefault(false),
								State = hackLicense.State
							};
							if (!result.HackLicense.LicenseImageURL.StartsWith("http")) {
								if (!result.HackLicense.LicenseImageURL.StartsWith("/")) {
									result.HackLicense.LicenseImageURL = $"/{result.HackLicense.LicenseImageURL}";
								}
								result.HackLicense.LicenseImageURL = $"{SystemDefaultsDict.GetByKey("URLS", "HackLicensePath", false).ValueString}{result.HackLicense.LicenseImageURL}";
							}
						}
					}

				} catch (Exception ex) {
					Console.WriteLine(ex.Message);
					result.TPFeesList = new List<AffFees>();
				}
				if (result.TPFeesList.Count == 0 && result.AdditionalTPPercentFee == 0) {
					result.TPFeesList = new List<AffFees>() { new AffFees() { StartAmount = 0.01M, EndAmount = 0, Fee = 5.50M } };
				}


				string[] valList = SystemDefaultsDict.GetByKey("EWRDriverPay", "SkipAskWorkingCityForAffiliates", false).ValueString.Split('|');
				List<long> skipAffIDList = new List<long>();
				foreach (string id in valList) {
					skipAffIDList.Add(Convert.ToInt64(id));
				}

				if (info.AppName.Equals("TaxiPass EWR")) {
					if (result.AffiliateID == 1998) {
						result.AskWorkingCity = true;
					} else if (skipAffIDList.Contains(result.AffiliateID)) {
						// All good do nothing for now
					} else {
						string sql1 = @"UPDATE dbo.AffiliateDrivers
                                       SET AffiliateID = @AffiliateID
                                       WHERE Cell = @Cell

                                       UPDATE dbo.Drivers
                                       SET AffiliateID = @AffiliateID
                                       WHERE Cell = @Cell";

						conn.Execute(sql1, new { Cell = info.Phone, AffiliateID = 1998 });
						result.AskWorkingCity = true;
					}
				}


				try {
					if (!info.SimSerialNumber.IsNullOrEmpty() || !info.SubscriberId.IsNullOrEmpty() || !info.DevicePhoneNumber.IsNullOrEmpty()) {
						string sql = @"SELECT  *
                                        FROM    dbo.Devices
                                        WHERE   SimSerialNumber = @SimSerialNumber
                                                OR PhoneNumber = @PhoneNumber
                                                OR SubscriberId = @SubscriberId";

						DynamicParameters parms = new DynamicParameters();
						parms.Add("SimSerialNumber", info.SimSerialNumber.IsNullOrEmpty() ? "x" : info.SimSerialNumber);
						parms.Add("SubscriberId", info.SubscriberId.IsNullOrEmpty() ? "x" : info.SubscriberId);
						parms.Add("PhoneNumber", info.DevicePhoneNumber.IsNullOrEmpty() ? "x" : info.DevicePhoneNumber);
						CabRideEngineDapper.Devices device = conn.Get<CabRideEngineDapper.Devices>(sql, parms);
						if (device == null || device.IsNullEntity) {
							device = CabRideEngineDapper.Devices.Create();
							device.SimSerialNumber = info.SimSerialNumber;
							device.SubscriberId = info.SubscriberId;
							device.PhoneNumber = info.DevicePhoneNumber;
						}
						device.Carrier = info.Carrier;
						device.HardwareManufacturer = info.HardwareManufacturer;
						device.HardwareModel = info.HardwareModel;
						device.HardwareOS = info.HardwareOS;
						if (!info.DevicePhoneNumber.IsNullOrEmpty()) {
							device.PhoneNumber = info.DevicePhoneNumber;
						}
						device.VersionNo = info.VersionNo;
						device.AppName = info.AppName;
						device.Save(conn, CabRideDapperSettings.LoggedInUser);

						result.DeviceID = device.DeviceID;

						sql = @"SELECT  *
                                FROM    dbo.DeviceLogin
                                WHERE   DeviceID = @DeviceID
                                        AND AppName = @AppName";
						string appName = string.Format("{0} {1}", info.AppName, info.VersionNo);
						CabRideEngineDapper.DeviceLogin login = conn.Get<CabRideEngineDapper.DeviceLogin>(sql, new { DeviceID = device.DeviceID, AppName = appName });
						if (login.IsNullEntity) {
							login = CabRideEngineDapper.DeviceLogin.Create();
							login.DeviceID = device.DeviceID;
							login.AppName = appName;
							login.LoginDate = WebApiApplication.MyTZConverter.FromUniversalTime(result.TimeZone, DateTime.UtcNow);
						}
						if (info.InfoUpdate) {
							login.LastCommunication = WebApiApplication.MyTZConverter.FromUniversalTime(result.TimeZone, DateTime.UtcNow);
						} else {
							login.LoginDate = WebApiApplication.MyTZConverter.FromUniversalTime(result.TimeZone, DateTime.UtcNow);
							login.AffiliateDriverID = result.AffiliateDriverID;
						}
						login.VehicleNo = info.Vehicle;
						login.Save(conn, CabRideDapperSettings.LoggedInUser);

					}
				} catch (Exception ex) {
					string sErrorEmailRecipient = System.Configuration.ConfigurationManager.AppSettings["ErrorEmailRecipient"];
					if (!sErrorEmailRecipient.IsNullOrEmpty()) {
						StringBuilder sHtml = new StringBuilder();
						sHtml.AppendLine("<hr noshade color=#000000 size=1>");
						sHtml.AppendLine(DateTime.Now.ToString() + " AffDriverSimpleLogin Error<br>");
						sHtml.AppendLine("<br>** Error Info **<br>");
						sHtml.AppendLine("Page: AffDriverSimpleLogin<br>");
						sHtml.AppendLine("Message: " + ex.Message + "<br>");
						sHtml.AppendLine("<br>Stack Trace: " + ex.StackTrace + "<br>");

						// send email if setting is configured in web.config
						try {
							string subject = string.Format("{0} Login Error", Properties.Settings.Default.SystemName);
							var cred = SystemDefaultsDict.GetEmailCreds(CabRideEngineDapper.Enums.EMailReportFrom.Report);
							TaxiPassCommon.Mail.EMailMessage oMail = new TaxiPassCommon.Mail.EMailMessage(cred, subject, sHtml.ToString());
							oMail.UseWebServiceToSendMail = Properties.Settings.Default.SendEmailViaWebService;
							oMail.SendMail(sErrorEmailRecipient);
						} catch {
						}
					}
				}

			}

			//if (result.FundPayCardDelayed) {
			//    DateTime now = WebApiApplication.MyTZConverter.FromUniversalTime(result.TimeZone, DateTime.UtcNow);
			//    if (now.DayOfWeek == DayOfWeek.Saturday || now.DayOfWeek == DayOfWeek.Sunday) {
			//        result.FundPayCardDelayed = false;
			//    } else {
			//        using (var conn = SqlHelper.OpenSqlConnection()) {
			//            if (!BankHoliday.GetByDate(conn, now.Date).IsNullEntity) {
			//                result.FundPayCardDelayed = false;
			//            }
			//        }
			//    }
			//}

			//result.EMail = affd

			return result;
		}
	}
}
