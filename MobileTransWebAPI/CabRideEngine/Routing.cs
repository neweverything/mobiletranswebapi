﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

using Csla.Validation;

using TaxiPassCommon;
using TaxiPassCommon.GeoCoding;

namespace CabRideEngine {
	[Serializable]
	public sealed class Routing : RoutingDataRow {

		private bool mValidateAddress = true;

		#region Constructors -- DO NOT MODIFY
		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new'
		private Routing()
			: this(null) {
		}

		// Typed DataSet constructor (needed by framework) - DO NOT REMOVE
		public Routing(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static Routing Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      Routing aRouting = pManager.CreateEntity<Routing>();
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aRouting, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aRouting.AddToManager();
		//      return aRouting;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		// Add additional logic to your business object here...
		public static Routing Create(PersistenceManager pPM, long pReservationID) {
			Routing oRouting = null;

			try {
				oRouting = (Routing)pPM.CreateEntity(typeof(Routing));

				// Using the IdeaBlade Id Generation technique
				//pPM.GenerateId(oRouting, Routing.RoutingIDEntityColumn);

				if (!oRouting.IsValidReservationID(pReservationID)) {
					throw new Exception("Unknown Reservation ID");
				}

				oRouting.ReservationID = pReservationID;
				oRouting.AddToManager();

			} catch (Exception ex) {
				throw ex;
			}
			return oRouting;
		}

		private bool IsValidReservationID(long pReservationID) {
			RdbQuery oQry = new RdbQuery(typeof(Reservations), Reservations.ReservationIDEntityColumn, EntityQueryOp.EQ, pReservationID);
			Reservations oRes = this.PersistenceManager.GetEntity<Reservations>(oQry);
			return !oRes.IsNullEntity;
		}

		protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
			if (IsDeserializing)
				return;
			base.OnColumnChanging(pArgs);
			//DoAudit(pArgs);
		}

		private void DoAudit(DataColumnChangeEventArgs pArgs) {
			if (!this.PersistenceManager.IsClient) {
				return;
			}
			if (this.RowState == DataRowState.Added) {
				return;
			}
			if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
				AuditRecord = RoutingAudit.Create(this.PersistenceManager, this);
			}
			//AuditRecord[pArgs.Column.ColumnName] = pArgs.ProposedValue;
		}


		public override EntityValidationInfo Validate(EntityValidationContext pContext) {
			if (mValidateAddress) {
				if (Convert.ToInt16(Latitude) == 0 && Convert.ToInt16(Longitude) == 0) {
					if (this.RoutingType == CabRideEngine.RoutingTypes.Airport) {
						if (Airport == "") {
							return new EntityValidationInfo(false, "Airport is required");
						}
						RdbQuery oQry = new RdbQuery(typeof(Airports), Airports.AirportEntityColumn, EntityQueryOp.EQ, Airport);
						Airports oAirport = this.PersistenceManager.GetEntity<Airports>(oQry);
						Latitude = oAirport.Latitude;
						Longitude = oAirport.Longitude;
					} else {
						if (Convert.ToInt16(Latitude) == 0 || Convert.ToInt16(Longitude) == 0) {
							SystemDefaults oDefaults = SystemDefaults.GetDefaults(this.PersistenceManager);
							MapPointService oService = new MapPointService(oDefaults.MapPointUserName, oDefaults.MapPointPassword);
							Address oAddress = new Address();
							oAddress.Street = Street;
							oAddress.City = City;
							oAddress.State = State;
							oAddress.Country = Country;
							oAddress.ZIP = ZIPCode;
							if (oService.FindAddress(oAddress)) {
								Latitude = oAddress.Latitude;
								Longitude = oAddress.Longitude;
							}
						}
					}
				}
			}
			return base.Validate(pContext);
		}

		public string FormattedAddress {
			get {
				string sAddr = "";
				if (RoutingType == RoutingTypes.Airport) {
					if (Convert.ToBoolean(Arrival)) {
						sAddr = FullAirlineName + " Flight # " + FlightNo + " arriving at " + FullAirportName;
						if (FlightTime != null) {
							sAddr += " at " + String.Format("{0:t}", FlightTime);
						}
					} else {
						sAddr = FullAirlineName;
						if (!string.IsNullOrEmpty(FlightNo)) {
							sAddr += " Flight # " + FlightNo;
						}
						if (string.IsNullOrEmpty(sAddr)) {
							sAddr = "Departing from ";
						} else {
							sAddr += " departing from ";
						}
						sAddr += FullAirportName;
						if (FlightTime != null) {
							sAddr += " at " + String.Format("{0:t}", FlightTime);
						}
					}
				} else {
					sAddr = Street;
					if (!(Suite == null || Suite.Trim() == "")) {
						sAddr += ", " + Suite;
					}
					sAddr += ", " + City;
					sAddr += ", " + State;
					sAddr += ", " + Country;
				}
				return sAddr;
			}
		}

		public string FormattedAddressShort { // doesn't contain airport info
			get {
				string sAddr = "";
				if (RoutingType == RoutingTypes.Airport) {
					sAddr = FullAirlineName;
					if (!string.IsNullOrEmpty(FlightNo)) {
						sAddr += " Flight # " + FlightNo;
					}
					if (!string.IsNullOrEmpty(sAddr)) {
						if (Convert.ToBoolean(Arrival)) {
							sAddr += " arriving at ";
						} else {
							sAddr += " departing from ";
						}
					}
					sAddr += FullAirportName;
				} else {
					sAddr = Street;
					if (!(Suite == null || Suite.Trim() == "")) {
						sAddr += ", " + Suite;
					}
					sAddr += ", " + City;
					sAddr += ", " + State;
					sAddr += ", " + Country;
				}
				return sAddr;
			}
		}

		public string FormattedType {
			get {
				return Routing_Types.GetById(this.Type).Description;
			}
		}

		private string FullAirportName {
			get {
				return Airports.GetAirport(this.PersistenceManager, Airport).Name;
			}
		}

		private string FullAirlineName {
			get {
				return Airlines.GetAirline(this.PersistenceManager, Airline).Name;
			}
		}

		public Address myAddress {
			get {
				Address oAddress = new Address();
				if (RoutingType == RoutingTypes.Airport) {
					oAddress.IsAirport = true;
					oAddress.Airport = Airport;
					oAddress.AirportName = FullAirportName;
					oAddress.Airline = Airline;
					oAddress.AirlineName = FullAirlineName;
					oAddress.FlightNumber = FlightNo;
					oAddress.AirportFromToCity = this.DepartingCity;
					if (this.Terminal != null) {
						oAddress.Terminal = this.Terminal;
					}
					if (this.FlightTime != null) {
						oAddress.FlightTime = this.FlightTime.Value;
					}
					oAddress.FlightNumber = FlightNo;

				} else {
					oAddress.Street = Street;
					oAddress.Suite = Suite;
				}
				oAddress.City = City;
				oAddress.State = State;
				oAddress.Country = Country;
				oAddress.ZIP = ZIPCode;
				if (Latitude != null) {
					oAddress.Latitude = (double)Latitude;
				}
				if (Longitude != null) {
					oAddress.Longitude = (double)Longitude;
				}

				return oAddress;
			}
		}

		public bool ValidateAddress {
			get {
				return mValidateAddress;
			}
			set {
				mValidateAddress = value;
			}
		}

		public string TextMsgAddress { // doesn't contain airport info
			get {
				string sAddr = "";
				if (RoutingType == RoutingTypes.Airport) {
					sAddr = Airport;
				} else {
					sAddr = Street;
				}
				return sAddr;
			}
		}


		public static Routing GetRouting(Reservations pReserve, short pRoutingOrder) {
			PersistenceManager oPM = pReserve.PersistenceManager;

			RdbQuery qry = new RdbQuery(typeof(Routing), Routing.ReservationIDEntityColumn, EntityQueryOp.EQ, pReserve.ReservationID);
			qry.AddClause(Routing.RoutingOrderEntityColumn, EntityQueryOp.EQ, pRoutingOrder);

			return oPM.GetEntity<Routing>(qry);
		}


		// ********************************************************
		// Return a Reservation's routing entries
		// ********************************************************
		public static EntityList<Routing> GetRouting(Reservations pReserve) {
			PersistenceManager oPM = pReserve.PersistenceManager;

			RdbQuery qry = new RdbQuery(typeof(Routing), Routing.ReservationIDEntityColumn, EntityQueryOp.EQ, pReserve.ReservationID);
			qry.AddOrderBy(Routing.ReservationIDEntityColumn);
			qry.AddOrderBy(Routing.RoutingOrderEntityColumn);

			return oPM.GetEntities<Routing>(qry);
		}

		public static Routing GetRouting(PersistenceManager pPM, long pRoutingID) {
			RdbQuery qry = new RdbQuery(typeof(Routing), Routing.RoutingIDEntityColumn, EntityQueryOp.EQ, pRoutingID);
			return pPM.GetEntity<Routing>(qry);
		}


		public Address ToAddress() {
			Address addr = new Address();
			addr.Street = Street;
			addr.City = City;
			addr.State = State;
			addr.Country = Country;
			addr.ZIP = ZIPCode;
			addr.Latitude = Latitude.GetValueOrDefault(0);
			addr.Longitude = Longitude.GetValueOrDefault(0);
			addr.Airport = Airport;
			addr.Airline = Airline;
			addr.Terminal = Terminal;
			addr.FlightTime = FlightTime;
			addr.FlightNumber = FlightNo;
			return addr;
		}
	}

}
