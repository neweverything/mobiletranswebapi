﻿using CabRideEngineDapper.Utils;
using Dapper;
using System;
using System.Data.SqlClient;
using System.Linq;

namespace CabRideEngineDapper {
    public partial class DriverPaymentNotes {

        public static DriverPaymentNotes Create(DriverPayments pDriverPayment) {
            DriverPaymentNotes rec = GetNullEntity();
            if (pDriverPayment.DriverPaymentID == 0) {
                pDriverPayment.Save();
            }
            rec = GetNote(pDriverPayment.DriverPaymentID);
            if (rec.IsNullEntity) {
                rec = DriverPaymentNotes.Create();
            }
            rec.DriverPaymentID = pDriverPayment.DriverPaymentID;
            pDriverPayment.DriverPaymentNotes = rec;
            return rec;
        }

        public override int Save() {
            if (IsNewRecord) {
                this.DriverPaymentNoteID = NextID.GetNextKey(DriverPaymentNotes.TableDef.TABLE_NAME);
            }
            try {
                return base.Save();
            } catch (Exception) {
            }
            return 0;
        }

        public override int Save(SqlConnection pConn, string pModifiedBy) {
            if (IsNewRecord) {
                this.DriverPaymentNoteID = NextID.GetNextKey(DriverPaymentNotes.TableDef.TABLE_NAME);
            }
            try {
                return base.Save(pConn, pModifiedBy);
            } catch (Exception) {
            }
            return 0;
        }


        private DriverPayments mDriverPayment;
        public DriverPayments DriverPayment {
            get {
                if (mDriverPayment == null || mDriverPayment.IsNullEntity) {
                    using (var conn = SqlHelper.OpenSqlConnection()) {
                        mDriverPayment = conn.Get<DriverPayments>(this.DriverPaymentID);
                    }
                }
                return mDriverPayment;
            }
        }



        internal static DriverPaymentNotes GetNote(long pDriverPaymentID) {
            string sql = "SELECT * FROM DriverPaymentNotes WHERE DriverPaymentID = @DriverPaymentID";
            using (var conn = SqlHelper.OpenSqlConnection()) {
                return conn.Query<DriverPaymentNotes>(sql, new { DriverPaymentID = pDriverPaymentID }).DefaultIfEmpty(DriverPaymentNotes.GetNullEntity()).FirstOrDefault();
            }
        }
    }
}
