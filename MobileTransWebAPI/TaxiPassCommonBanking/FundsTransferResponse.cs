﻿namespace TaxiPassCommon.Banking {

    public class FundsTransferResponse {
        public string ReturnCode { get; set; }
        public string ReturnMessage { get; set; }
        public string FromCardRecordNumber { get; set; }
        public string ToCardRecordNumber { get; set; }
        public string Unused1 { get; set; }
        public string Unused2 { get; set; }
        public string FromCardBalance { get; set; }
        public string ToCardBalance { get; set; }
    }
}
