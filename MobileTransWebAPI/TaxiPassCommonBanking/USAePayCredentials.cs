﻿
namespace TaxiPassCommon.Banking {
    public class USAePayCredentials : IGatewayCredentials {

        public USAePayCredentials() {
            CreditCardProcessor = CardProcessors.USAePay;
        }

        public string SourceKey { get; set; }
        public string Pin { get; set; }
        public string ProdURL { get; set; }

        public string TestURL { get; set; }
        public string SourceKeyTest { get; set; }
        public string PinTest { get; set; }

        public int Timeout { get; set; }

        public long VerisignAccountID { get; set; }
        public string MerchantID { get; set; }

        public bool Validate { get; set; }

        public int ProcessingOrder { get; set; }

        public CardProcessors CreditCardProcessor { get; set; }

        public string TerminalType { get; set; }
        public string CardType { get; set; }


        public USAePayCredentials(string pSourceKey, string pPin, string pURL, int pTimeOut) {
            SourceKey = pSourceKey;
            Pin = pPin;
            ProdURL = pURL;
            Timeout = pTimeOut;
        }

        public CardProcessors MyGateway() {
            return CreditCardProcessor;
        }

    }
}