﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using TaxiPassCommon;


namespace CabRideEngineDapper {
    public partial class vw_VouchersRedeemed {

        public static List<vw_VouchersRedeemed> GetByRedeemerAccount(SqlConnection pConn, long pRedeemerAcctID, DateTime pStart, DateTime pEnd, string pTransNo = "") {
            string sql = @"SELECT distinct * FROM vw_VouchersRedeemed
							WHERE   ( DriverPaymentsRedeemerID IN ( SELECT  TaxiPassRedeemerID
																	FROM    TaxiPassRedeemers
																	WHERE   TaxiPassRedeemerAccountID = @TaxiPassRedeemerAccountID )
									  OR PendingMatchesRedeemerID IN (
									  SELECT    TaxiPassRedeemerID
									  FROM      TaxiPassRedeemers
									  WHERE     TaxiPassRedeemerAccountID = @TaxiPassRedeemerAccountID )
									)
									AND {0}
							ORDER BY TransNo";

            if (pTransNo.IsNullOrEmpty()) {
                sql = string.Format(sql, "DriverPaidDate >= @StartDate AND DriverPaidDate <= @EndDate");
            } else {
                sql = string.Format(sql, "TransNo = @TransNo");
            }
            DynamicParameters parms = new DynamicParameters();
            parms.Add("TaxiPassRedeemerAccountID", pRedeemerAcctID);
            if (pTransNo.IsNullOrEmpty()) {
                parms.Add("StartDate", pStart);
                parms.Add("EndDate", pEnd);
            } else {
                parms.Add("TransNo", pTransNo);
            }
            return pConn.Query<vw_VouchersRedeemed>(sql, parms, commandTimeout: 600).ToList();
        }

        public static List<vw_VouchersRedeemed> GetByRedeemer(SqlConnection pConn, long pRedeemerID, DateTime pStart, DateTime pEnd, string pTransNo = "") {
            string sql = @"SELECT  * FROM vw_VouchersRedeemed
							WHERE   ( DriverPaymentsRedeemerID = @RedeemerID 
									  OR PendingMatchesRedeemerID = @RedeemerID 
									)
									AND {0}
							ORDER BY TransNo";

            if (pTransNo.IsNullOrEmpty()) {
                sql = string.Format(sql, "DriverPaidDate >= @StartDate AND DriverPaidDate <= @EndDate");
            } else {
                sql = string.Format(sql, "TransNo = @TransNo");
            }

            DynamicParameters parms = new DynamicParameters();
            parms.Add("RedeemerID", pRedeemerID);
            if (pTransNo.IsNullOrEmpty()) {
                parms.Add("StartDate", pStart);
                parms.Add("EndDate", pEnd);
            } else {
                parms.Add("TransNo", pTransNo);
            }
            return pConn.Query<vw_VouchersRedeemed>(sql, parms).ToList();
        }

    }
}
