﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

using Csla.Validation;

namespace CabRideEngine {
    [Serializable]
    public sealed class StreetTypes : StreetTypesDataRow {

        #region Constructors -- DO NOT MODIFY
        // Do not create constructors for this class
        // Developers cannot create instances of this class with the "new" keyword
        // You should write a static Create method instead; see the "Suggested Customization" region
        // Please review the documentation to learn about entity creation and inheritance

        // This private constructor prevents accidental instance creation via 'new'
        private StreetTypes() : this(null) {
        }

        // Typed DataSet constructor (needed by framework) - DO NOT REMOVE
        public StreetTypes(DataRowBuilder pRowBuilder)
            : base(pRowBuilder) {
        }

        #endregion

        #region Suggested Customizations

        //    // Use this factory method template to create new instances of this class
        //    public static StreetTypes Create(PersistenceManager pManager,
        //      ... your creation parameters here ...) { 
        //
        //      StreetTypes aStreetTypes = pManager.CreateEntity<StreetTypes>();
        //
        //      // if this object type requires a unique id and you have implemented
        //      // the IIdGenerator interface implement the following line
        //      pManager.GenerateId(aStreetTypes, // add id column here //);
        //
        //      // Add custom code here
        //
        //      aStreetTypes.AddToManager();
        //      return aStreetTypes;
        //    }

        //    // Implement this method if you want your object to sort in a specific order
        //    public override int CompareTo(Object pObject) {
        //    }

        //    // Implement this method to customize the null object version of this class
        //    protected override void UpdateNullEntity() {
        //    }

        #endregion

        // Add additional logic to your business object here...
        public static StreetTypes Create(PersistenceManager pManager) {

            StreetTypes oType = (StreetTypes)pManager.CreateEntity(typeof(StreetTypes));

            //pManager.GenerateId(oType, CabRideEngine.StreetTypes.StreetTypeIDEntityColumn);
            oType.AddToManager();
            return oType;
        }

        protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
            if (IsDeserializing)
                return;
            base.OnColumnChanging(pArgs);
            //DoAudit(pArgs);
        }

        private void DoAudit(DataColumnChangeEventArgs pArgs) {
            if (!this.PersistenceManager.IsClient) {
                return;
            }
            if (this.RowState == DataRowState.Added) {
                return;
            }
            if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
                AuditRecord = StreetTypesAudit.Create(this.PersistenceManager, this);
            }
            //AuditRecord[pArgs.Column.ColumnName] = pArgs.ProposedValue;
        }

        protected override void AddRules(RuleList pList) {
            PropertyRequiredRule.AddToList(pList, StreetTypes.StreetTypeEntityColumn.ColumnName);
            pList.Add(IsStreetTypeUnique, StreetTypeEntityColumn.ColumnName);
        }

        private bool IsStreetTypeUnique(object pTarget, RuleArgs e) {
            StreetTypes oType = (StreetTypes)pTarget;

            RdbQuery oQry = new RdbQuery(typeof(StreetTypes),StreetTypes.StreetTypeEntityColumn, EntityQueryOp.EQ, oType.StreetType);
            oQry.AddClause(StreetTypes.StreetTypeIDEntityColumn, EntityQueryOp.NE, oType.StreetTypeID);
            EntityList<StreetTypes> oStreetTypes = this.PersistenceManager.GetEntities<StreetTypes>(oQry);
            if (oStreetTypes.Count > 0) {
                e.Description = "Street Type must be unique!";
                return false;
            }
            return true;
        }

        [BindingBrowsable(false)]
        public override string EntityInstanceDisplayName {
            get {
                return StreetType;
            }
        }

        public static EntityList<StreetTypes> GetAllStreetTypes(PersistenceManager pPM) {
            EntityList<StreetTypes> oTypes = pPM.GetEntities<StreetTypes>();
            oTypes.ApplySort(StreetTypes.StreetTypeEntityColumn.ColumnName, System.ComponentModel.ListSortDirection.Ascending, false);
            return oTypes;
        }

    }

}
