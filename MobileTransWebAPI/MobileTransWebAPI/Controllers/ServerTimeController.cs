﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using Dapper;

using TaxiPassCommon;

namespace MobileTransWebAPI.Controllers {
	public class ServerTimeController : ApiController {
		public double GetServerTime() {
            string sql = @"SELECT GETUTCDATE() UTCDate";
            using (var conn = CabRideEngineDapper.Utils.SqlHelper.OpenSqlConnection()) {
                return conn.Query<DateTime>(sql).DefaultIfEmpty(DateTime.UtcNow).FirstOrDefault().ToEpochTime("");
            }

			//return DateTime.UtcNow.ToEpochTime("");
		}

	}
}
