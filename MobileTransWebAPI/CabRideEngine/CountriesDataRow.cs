using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Runtime.Serialization;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

namespace CabRideEngine {
 
  #region CountriesDataTable
  /// <summary>
  /// The generated <see cref="T:IdeaBlade.Persistence.EntityTable"/> class  
  /// containing all <see cref="Countries"/> objects.
  /// </summary>
  /// <remarks>
  /// ****************************************************
  /// <para>** DO NOT MODIFY THIS CLASS - AutoGenerated Code</para>
  /// <para>** Any changes made to this code will be lost</para>
  /// <para>** the next time this code is regenerated.</para>
  /// <para>**</para>
  /// <para>** Generated from:   EntityBaseclass.template</para>
  /// <para>** Last modified:    2011-06-29T13:00:28.3777579-07:00</para>
  /// <para>** Template version: 3.110127</para>
  /// ****************************************************
  /// </remarks>
  [BindingBrowsable(false)]
  [Serializable]
  public partial class CountriesDataTable : BaseEntityDataTable, 
     IdeaBlade.Persistence.Rdb.IRdbTable {
  
    /// <summary>
    /// Initializes a new instance of the CountriesDataTable class with no arguments.
    /// </summary>
    public CountriesDataTable() {}

    /// <summary>
    /// Initializes a new instance of the CountriesDataTable class 
    /// with the <see cref="System.Runtime.Serialization.SerializationInfo"/> 
    /// and the <see cref="System.Runtime.Serialization.StreamingContext"/>.
    /// </summary>
    /// <param name="pInfo">The data needed to serialize or deserialize an object.</param>
    /// <param name="pContext">The source and destination of a given serialized stream.</param>
    protected CountriesDataTable(SerializationInfo pInfo, StreamingContext pContext) :
      base(pInfo, pContext) {}

    /// <summary>
    /// Creates a new instance of the <see cref="CountriesDataRow"/>.
    /// Constructs a row from the builder.
    /// <para>** For internal use only. **</para>
    /// </summary>
    /// <param name="pRowBuilder"></param>
    /// <returns>The <see cref="CountriesDataRow"/>.</returns>
    protected override DataRow NewRowFromBuilder(DataRowBuilder pRowBuilder) {
      return new Countries(pRowBuilder);
    }

    /// <summary>Returns the <see cref="System.Type"/> of <see cref="Countries"/>.</summary>
    protected override Type GetRowType() {
      return typeof(Countries);
    }
    
    /// <summary>
    /// Gets the name of the <see cref="T:IdeaBlade.Rdb.RdbKey"/> or 
    /// <see cref="T:IdeaBlade.Persistence.WS.WsKey"/> defined in <b>IdeaBlade.ibconfig</b>. 
    /// </summary>
    /// <remarks>For this <see cref="Countries"/>, the DataSourceKeyName="default".</remarks>
    public override String DataSourceKeyName {
      get { return @"default"; }
    }

    #region Column properties
    //**************************************
    //* Column properties
    //**************************************
    /// <summary>Gets the Country <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn CountryColumn {
      get {
        if (mCountryColumn!=null) return mCountryColumn;
        mCountryColumn = GetColumn("Country", true);
        return mCountryColumn;
      }
    }
    private DataColumn mCountryColumn;
    
    /// <summary>Gets the Name <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn NameColumn {
      get {
        if (mNameColumn!=null) return mNameColumn;
        mNameColumn = GetColumn("Name", true);
        return mNameColumn;
      }
    }
    private DataColumn mNameColumn;
    
    /// <summary>Gets the Default <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn DefaultColumn {
      get {
        if (mDefaultColumn!=null) return mDefaultColumn;
        mDefaultColumn = GetColumn("Default", true);
        return mDefaultColumn;
      }
    }
    private DataColumn mDefaultColumn;
    
    /// <summary>Gets the RowVersion <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn RowVersionColumn {
      get {
        if (mRowVersionColumn!=null) return mRowVersionColumn;
        mRowVersionColumn = GetColumn("RowVersion", true);
        return mRowVersionColumn;
      }
    }
    private DataColumn mRowVersionColumn;
    
    /// <summary>Gets the ModifiedBy <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn ModifiedByColumn {
      get {
        if (mModifiedByColumn!=null) return mModifiedByColumn;
        mModifiedByColumn = GetColumn("ModifiedBy", true);
        return mModifiedByColumn;
      }
    }
    private DataColumn mModifiedByColumn;
    
    /// <summary>Gets the ModifiedDate <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn ModifiedDateColumn {
      get {
        if (mModifiedDateColumn!=null) return mModifiedDateColumn;
        mModifiedDateColumn = GetColumn("ModifiedDate", true);
        return mModifiedDateColumn;
      }
    }
    private DataColumn mModifiedDateColumn;
    
    #endregion    

    /// <summary>Creates the schema mapping information for this CountriesDataTable.</summary>
    /// <returns>
    /// The <see cref="T:IdeaBlade.Persistence.TableMappingInfo"/> for this CountriesDataTable.
    /// </returns>
    protected override TableMappingInfo CreateTableMappingInfo() {
      RdbTableMappingInfo mappingInfo = new RdbTableMappingInfo(this);
      mappingInfo.SourceOwnerName = "dbo"; 
      mappingInfo.SourceTableName = "Countries"; 
      mappingInfo.ConcurrencyColumnName = "RowVersion"; 
      // ColumnSelectionOption: selectAllColumns
      mappingInfo.SourceColumnNames = "*"; 
   
      DataColumnMappingCollection columnMappings = mappingInfo.TableMapping.ColumnMappings;
      columnMappings.Clear();
      columnMappings.Add("Country", "Country");
      columnMappings.Add("Name", "Name");
      columnMappings.Add("Default", "Default");
      columnMappings.Add("RowVersion", "RowVersion");
      columnMappings.Add("ModifiedBy", "ModifiedBy");
      columnMappings.Add("ModifiedDate", "ModifiedDate");
      return mappingInfo;
    }
  
    /// <summary>** For internal use only. **</summary>
    protected override void UpdateTableMappingInfo() {
      base.UpdateTableMappingInfo();
      this.TableMappingInfo.ColumnUpdateMap["RowVersion"] = "RowVersion + 1";
    }

    /// <summary>
    /// Initializes Countries <see cref="System.Data.DataColumn"/> properties.
    /// </summary>
    public override void InitColumnProperties() {
      base.InitColumnProperties();

      CountryColumn.Caption = "Country";
      NameColumn.Caption = "Name";
      DefaultColumn.Caption = "Default";
      RowVersionColumn.Caption = "RowVersion";
      ModifiedByColumn.Caption = "ModifiedBy";
      ModifiedDateColumn.Caption = "ModifiedDate";
    }
  }
  #endregion
  
  #region CountriesDataRow
  /// <summary>
  /// The generated base class for <see cref="Countries"/>.
  /// </summary>
  /// <remarks>
  /// <para>
  /// It represents a row of persistable data in the <see cref="CountriesDataTable"/>
  /// and derives from <see cref="T:IdeaBlade.Persistence.Entity"/>.
	/// </para>
  /// ****************************************************
  /// <para>** DO NOT MODIFY THIS CLASS - AutoGenerated Code</para>
  /// <para>** Any changes made to this code will be lost</para>
  /// <para>** the next time this code is regenerated.</para>
  /// <para>**</para>
  /// <para>** Generated from:   EntityBaseclass.template</para>
  /// <para>** Last modified:    2011-06-29T13:00:28.3777579-07:00</para>
  /// <para>** Template version: 3.110127</para>
  /// ****************************************************
  /// </remarks>  
  [Serializable]
  public abstract partial class CountriesDataRow : BaseEntity, 
     IdeaBlade.Persistence.Rdb.IRdbEntity {
  
    /// <summary>
    /// Initializes a new instance of the CountriesDataRow.
    /// Constructs a row from the builder.
    /// <para>** For internal use only. **</para>
    /// </summary>
    /// <param name="pRowBuilder"></param>
    protected CountriesDataRow(DataRowBuilder pRowBuilder)
      : base(pRowBuilder) {
    }

    /// <summary>Gets the <see cref="CountriesDataTable"/>.</summary>
    [BindingBrowsable(false)]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public new CountriesDataTable TypedTable {
      get { return (CountriesDataTable) base.Table; }
    }

    /// <summary>Returns the <see cref="System.Type"/> of <see cref="CountriesDataTable"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public static Type GetTableType() {
      return typeof(CountriesDataTable);
    }

    /// <summary>
    /// Gets the SQL 'where' clause used to restrict which 
    /// database records are accessible via this entity. 
    /// </summary>
    /// <remarks>
    /// The default property value is defined in the Current Class 
    /// Properties tab in the <b>IdeaBlade DevForce Object Mapping Tool</b>.
    /// <para>
    /// The 'where' clause will be used, in addition to any other 
    /// query-specific criteria, in every retrieval of entities of this type.
    /// </para>
    /// </remarks>
    [BindingBrowsable(false)]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public virtual string SqlWhereClause {
      get { return ""; }
    }
    
    /// <summary>
    /// Determines whether to use the 'distinct' keyword when 
    /// generating the SQL select clause for objects of this type.
    /// </summary>
    /// <remarks>Defaults to false, but may be overridden in subclasses.</remarks>
    [BindingBrowsable(false)]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public virtual bool ForceSqlDistinct {
      get { return false; }
    }
    

    #region Enums
    //**************************************
    //* Enums
    //**************************************
    #endregion
	
    #region Relation properties
    //**************************************
    //* Relation properties
    //**************************************
	  /// <summary>Gets the Stateses relation property.</summary>
    [RelationProperty("Countries_States", 
    QueryDirection.ChildQuery)]
    public virtual ReadOnlyEntityList<States> Stateses {
      get { 
        ReadOnlyEntityList<States> result_;
        if (GetInterceptor<ReadOnlyEntityList<States>>("Stateses", GetStatesesImpl, out result_)) return result_;
        return GetStatesesImpl();
      }
    }
    private ReadOnlyEntityList<States> GetStatesesImpl() {
      return this.GetManagedChildren<States>(EntityRelations.Countries_States);
    } 
    
    #endregion
    
    #region EntityColumn definitions
    //**************************************
    //* Static EntityColumn definitions
    //**************************************
    /// <summary>The Country <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn CountryEntityColumn =
      new EntityColumn(typeof(Countries), "Country", typeof(System.String), false, true, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The Name <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn NameEntityColumn =
      new EntityColumn(typeof(Countries), "Name", typeof(System.String), true, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The Default <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn DefaultEntityColumn =
      new EntityColumn(typeof(Countries), "Default", typeof(System.Boolean), false, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The RowVersion <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn RowVersionEntityColumn =
      new EntityColumn(typeof(Countries), "RowVersion", typeof(System.Int32), false, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The ModifiedBy <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn ModifiedByEntityColumn =
      new EntityColumn(typeof(Countries), "ModifiedBy", typeof(System.String), true, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The ModifiedDate <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn ModifiedDateEntityColumn =
      new EntityColumn(typeof(Countries), "ModifiedDate", typeof(System.DateTime), true, false, false, DataSourceAccessType.ReadWrite);      
    #endregion
    
    #region Properties
    //**************************************
    //* Country methods
    //**************************************
    /// <summary>Gets the Country <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn CountryColumn {
      get { return TypedTable.CountryColumn; }
    }

    /// <summary>Gets or sets the Country.</summary>
    [MaxTextLength(10)]
    [DBDataType(typeof(System.String))]
    public virtual System.String Country {
      get { 
        System.String result_;
        if (GetInterceptor<System.String>("Country", GetCountryImpl, out result_)) return result_;
        return GetCountryImpl();
      }
      set { 
          if (!SetInterceptor<System.String>("Country", value, SetCountryImpl)) {
            SetCountryImpl(value);
          }  
      }    
    }
    private System.String GetCountryImpl() {
      return (System.String) GetColumnValue(CountryColumn, typeof(System.String), false); 
    }
    private void SetCountryImpl(System.String value) {
      SetColumnValue(CountryColumn, value);
    }
    
    //**************************************
    //* Name methods
    //**************************************
    /// <summary>Gets the Name <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn NameColumn {
      get { return TypedTable.NameColumn; }
    }

    /// <summary>Gets or sets the Name.</summary>
    [MaxTextLength(50)]
    [DBDataType(typeof(System.String))]
    public virtual System.String Name {
      get { 
        System.String result_;
        if (GetInterceptor<System.String>("Name", GetNameImpl, out result_)) return result_;
        return GetNameImpl();
      }
      set { 
          if (!SetInterceptor<System.String>("Name", value, SetNameImpl)) {
            SetNameImpl(value);
          }  
      }    
    }
    private System.String GetNameImpl() {
      return (System.String) GetColumnValue(NameColumn, typeof(System.String), true); 
    }
    private void SetNameImpl(System.String value) {
      SetColumnValue(NameColumn, value);
    }
    
    //**************************************
    //* Default methods
    //**************************************
    /// <summary>Gets the Default <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn DefaultColumn {
      get { return TypedTable.DefaultColumn; }
    }

    /// <summary>Gets or sets the Default.</summary>
    [DBDataType(typeof(System.Boolean))]
    public virtual System.Boolean Default {
      get { 
        System.Boolean result_;
        if (GetInterceptor<System.Boolean>("Default", GetDefaultImpl, out result_)) return result_;
        return GetDefaultImpl();
      }
      set { 
          if (!SetInterceptor<System.Boolean>("Default", value, SetDefaultImpl)) {
            SetDefaultImpl(value);
          }  
      }    
    }
    private System.Boolean GetDefaultImpl() {
      return (System.Boolean) GetColumnValue(DefaultColumn, typeof(System.Boolean), false); 
    }
    private void SetDefaultImpl(System.Boolean value) {
      SetColumnValue(DefaultColumn, value);
    }
    
    //**************************************
    //* RowVersion methods
    //**************************************
    /// <summary>Gets the RowVersion <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn RowVersionColumn {
      get { return TypedTable.RowVersionColumn; }
    }

    /// <summary>Gets or sets the RowVersion.</summary>
    [DBDataType(typeof(System.Int32))]
    public virtual System.Int32 RowVersion {
      get { 
        System.Int32 result_;
        if (GetInterceptor<System.Int32>("RowVersion", GetRowVersionImpl, out result_)) return result_;
        return GetRowVersionImpl();
      }
      set { 
          if (!SetInterceptor<System.Int32>("RowVersion", value, SetRowVersionImpl)) {
            SetRowVersionImpl(value);
          }  
      }    
    }
    private System.Int32 GetRowVersionImpl() {
      return (System.Int32) GetColumnValue(RowVersionColumn, typeof(System.Int32), false); 
    }
    private void SetRowVersionImpl(System.Int32 value) {
      SetColumnValue(RowVersionColumn, value);
    }
    
    //**************************************
    //* ModifiedBy methods
    //**************************************
    /// <summary>Gets the ModifiedBy <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn ModifiedByColumn {
      get { return TypedTable.ModifiedByColumn; }
    }

    /// <summary>Gets or sets the ModifiedBy.</summary>
    [MaxTextLength(100)]
    [DBDataType(typeof(System.String))]
    public virtual System.String ModifiedBy {
      get { 
        System.String result_;
        if (GetInterceptor<System.String>("ModifiedBy", GetModifiedByImpl, out result_)) return result_;
        return GetModifiedByImpl();
      }
      set { 
          if (!SetInterceptor<System.String>("ModifiedBy", value, SetModifiedByImpl)) {
            SetModifiedByImpl(value);
          }  
      }    
    }
    private System.String GetModifiedByImpl() {
      return (System.String) GetColumnValue(ModifiedByColumn, typeof(System.String), true); 
    }
    private void SetModifiedByImpl(System.String value) {
      SetColumnValue(ModifiedByColumn, value);
    }
    
    //**************************************
    //* ModifiedDate methods
    //**************************************
    /// <summary>Gets the ModifiedDate <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn ModifiedDateColumn {
      get { return TypedTable.ModifiedDateColumn; }
    }

    /// <summary>Gets or sets the ModifiedDate.</summary>
    [DBDataType(typeof(System.DateTime))]
    public virtual Nullable<System.DateTime> ModifiedDate {
      get { 
        Nullable<System.DateTime> result_;
        if (GetInterceptor<Nullable<System.DateTime>>("ModifiedDate", GetModifiedDateImpl, out result_)) return result_;
        return GetModifiedDateImpl();
      }
      set { 
          if (!SetInterceptor<Nullable<System.DateTime>>("ModifiedDate", value, SetModifiedDateImpl)) {
            SetModifiedDateImpl(value);
          }
      }    
    }
    private Nullable<System.DateTime> GetModifiedDateImpl() {
      return (Nullable<System.DateTime>) GetColumnValue(ModifiedDateColumn, typeof(System.DateTime), true); 
    }
    private void SetModifiedDateImpl(Nullable<System.DateTime> value) {
      SetColumnValue(ModifiedDateColumn, value);
    }
    
    #endregion

  }
  #endregion

  #region EntityPropertyDescriptors.CountriesPropertyDescriptor
  public static partial class EntityPropertyDescriptors {

    /// <summary>
    /// Gets the CountriesPropertyDescriptor for <see cref="Countries"/>.
    /// </summary>
    public static CountriesPropertyDescriptor Countries {
      get { return CountriesPropertyDescriptor.Instance; }
    }
    
    /// <summary>
    /// The generated class containing <see cref="Countries"/> PropertyDescriptors.
    /// </summary>
    public partial class CountriesPropertyDescriptor : BaseEntityPropertyDescriptor {
	
			/// <summary>
      /// Initializes a new instance of the CountriesPropertyDescriptor class. 
      /// </summary>
      /// <param name="pParentDescriptor">The parent <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/></param>
      /// <param name="pPropertyPath">The type of the property value</param>
			public CountriesPropertyDescriptor(AdaptedPropertyDescriptor pParentDescriptor, String pPropertyPath)
        : base(pParentDescriptor, pPropertyPath) {}
		
      /// <summary>
      /// Initializes a new instance of the CountriesPropertyDescriptor class. 
      /// </summary>
      /// <param name="pComponentType">The type of object to which the property is bound</param>
      protected CountriesPropertyDescriptor(Type pComponentType)
        : base(pComponentType) {}

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Country.</summary>
      public AdaptedPropertyDescriptor Country {
        get { return Get("Country"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Name. Aliased with an '_' because Name is reserved.</summary>
      public AdaptedPropertyDescriptor Name_ {
        get { return Get("Name"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Default.</summary>
      public AdaptedPropertyDescriptor Default {
        get { return Get("Default"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for RowVersion.</summary>
      public AdaptedPropertyDescriptor RowVersion {
        get { return Get("RowVersion"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for ModifiedBy.</summary>
      public AdaptedPropertyDescriptor ModifiedBy {
        get { return Get("ModifiedBy"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for ModifiedDate.</summary>
      public AdaptedPropertyDescriptor ModifiedDate {
        get { return Get("ModifiedDate"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Stateses.</summary>
      public AdaptedPropertyDescriptor Stateses {
        get { return Get("Stateses"); }
      }
						
     internal new static CountriesPropertyDescriptor Instance = new CountriesPropertyDescriptor(typeof(Countries));
    }
  }
  #endregion
  
}
