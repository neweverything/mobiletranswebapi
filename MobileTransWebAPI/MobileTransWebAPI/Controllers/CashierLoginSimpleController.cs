﻿using MobileTransWebAPI.Models;
using System.Web.Mvc;

namespace MobileTransWebAPI.Controllers {
	public class CashierLoginSimpleController : Controller {

		// GET: CashierLoginSimple
		public DriverLoginResult Login(string pRequest) {
			return new AffDriverLoginSimpleController().Login(pRequest);
		}
	}
}