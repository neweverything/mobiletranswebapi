﻿using CabRideEngineDapper.Utils;
using Dapper;
using System;
using System.Data.SqlClient;
using System.Linq;
using TaxiPassCommon;

namespace CabRideEngineDapper {
	public partial class Invoices {

		[Editable(false)]
		public string PhoneNoFormatted {
			get {
				return PhoneNo.FormattedPhoneNumber();
			}
		}


		public override decimal? AmountPaid {
			get {
				//string sql = @"SELECT Sum(t.TotalPaid) TotalPaid
				//				FROM (	 SELECT Sum(Fare) TotalPaid
				//						 FROM dbo.DriverPayments
				//						 LEFT JOIN DriverPaymentRef ON DriverPaymentRef.DriverPaymentID = DriverPayments.DriverPaymentID
				//													   AND ReferenceGroup = 'Invoice'
				//													   AND ReferenceKey = 'ID'
				//						 WHERE AffiliateID = @AffiliateID
				//							   AND (   ReferenceNo = 'Invoice: ' + Convert(VARCHAR(20), @InvoiceID)
				//									   OR ValueString = Convert(VARCHAR(20), @InvoiceID))
				//							   AND Failed = 0
				//							   AND Test = 0
				//						 UNION
				//						 SELECT Sum(Fare) TotalPaid
				//						 FROM dbo.CashTrans
				//						 WHERE AffiliateID = @AffiliateID
				//							   AND InvoiceID = @InvoiceID) t";

				using (var conn = SqlHelper.OpenSqlConnection()) {
					try {
						using (SqlCommand comm = new SqlCommand("SET ARITHABORT ON", conn)) {
							comm.ExecuteNonQuery();
						}
						return conn.Query<decimal?>("usp_InvoiceAmountPaid", new { AffiliateID = this.AffiliateID, InvoiceID = this.InvoiceID }, commandType: System.Data.CommandType.StoredProcedure, commandTimeout: 3).FirstOrDefault();
					} catch (Exception ex) {
						return 0;
					}
				}
			}
		}

		[Editable(false)]
		public decimal AmountDue {
			get {
				return InvoiceTotal - AmountPaid.GetValueOrDefault(0);
			}
		}

		[Editable(false)]
		public string CreatedByName {
			get {
				return CreatedBy.Split('|').First();
			}
		}

		[Editable(false)]
		public string ModifiedByName {
			get {
				return ModifiedBy.Split('|').First();
			}
		}

	}
}