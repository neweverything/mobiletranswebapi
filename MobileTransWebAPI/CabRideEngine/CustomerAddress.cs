﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

using Csla.Validation;

using TaxiPassCommon;
using TaxiPassCommon.GeoCoding;

namespace CabRideEngine {
    [Serializable]
    public sealed class CustomerAddress : CustomerAddressDataRow {

        #region Constructors -- DO NOT MODIFY
        // Do not create constructors for this class
        // Developers cannot create instances of this class with the "new" keyword
        // You should write a static Create method instead; see the "Suggested Customization" region
        // Please review the documentation to learn about entity creation and inheritance

        // This private constructor prevents accidental instance creation via 'new'
        private CustomerAddress()
            : this(null) {
        }

        // Typed DataSet constructor (needed by framework) - DO NOT REMOVE
        public CustomerAddress(DataRowBuilder pRowBuilder)
            : base(pRowBuilder) {
        }

        #endregion

        #region Suggested Customizations

        //    // Use this factory method template to create new instances of this class
        //    public static CustomerAddress Create(PersistenceManager pManager,
        //      ... your creation parameters here ...) { 
        //
        //      CustomerAddress aCustomerAddress = pManager.CreateEntity<CustomerAddress>();
        //
        //      // if this object type requires a unique id and you have implemented
        //      // the IIdGenerator interface implement the following line
        //      pManager.GenerateId(aCustomerAddress, // add id column here //);
        //
        //      // Add custom code here
        //
        //      aCustomerAddress.AddToManager();
        //      return aCustomerAddress;
        //    }

        //    // Implement this method if you want your object to sort in a specific order
        //    public override int CompareTo(Object pObject) {
        //    }

        //    // Implement this method to customize the null object version of this class
        //    protected override void UpdateNullEntity() {
        //    }

        #endregion

        // Add additional logic to your business object here...
        public static CustomerAddress Create(PersistenceManager pManager, long pCustomerID) {
            CustomerAddress oAddress = null;
            try {
                oAddress = (CustomerAddress)pManager.CreateEntity(typeof(CustomerAddress));
                oAddress.CustomerID = pCustomerID;

                // Default the country in the combo box
                RdbQuery oQry = new RdbQuery(typeof(Countries), Countries.DefaultEntityColumn, EntityQueryOp.EQ, true);
                Countries oCountry = (Countries)pManager.GetEntity(oQry);
                oAddress.Country = oCountry.Country;

                oAddress.AddToManager();

            } catch (Exception ex) {
                throw ex;
            }
            return oAddress;
        }

        public static CustomerAddress Create(Customer pCustomer) {
            CustomerAddress oAddress = null;
            try {
                oAddress = (CustomerAddress)pCustomer.PersistenceManager.CreateEntity(typeof(CustomerAddress));

                // Using the IdeaBlade Id Generation technique
                pCustomer.PersistenceManager.GenerateId(oAddress, CustomerAddress.CustomerAddressIDEntityColumn);
                oAddress.CustomerID = pCustomer.CustomerID;

                // Default the country in the combo box
                RdbQuery oQry = new RdbQuery(typeof(Countries), Countries.DefaultEntityColumn, EntityQueryOp.EQ, true);
                Countries oCountry = (Countries)pCustomer.PersistenceManager.GetEntity(oQry);
                oAddress.Country = oCountry.Country;

                oAddress.AddToManager();

            } catch (Exception ex) {
                throw ex;
            }
            return oAddress;
        }


        protected override void AddRules(RuleList pList) {
            PropertyRequiredRule.AddToList(pList, StreetColumn.ColumnName);
            PropertyRequiredRule.AddToList(pList, CityColumn.ColumnName);
            PropertyRequiredRule.AddToList(pList, StateColumn.ColumnName);
            PropertyRequiredRule.AddToList(pList, CountryColumn.ColumnName);
            pList.Add(SetGeoCodes, "");
        }

        private bool SetGeoCodes(object pTarget, RuleArgs e) {
            CustomerAddress oAddress = (CustomerAddress)pTarget;

            //validate address with Mappoint
            try {
                Address myAddress = new Address();
                myAddress.Street = oAddress.Street;
                myAddress.City = oAddress.City;
                myAddress.State = oAddress.State;
                myAddress.Country = oAddress.Country;
                myAddress.ZIP = oAddress.ZIPCode;

                SystemDefaults oDefaults = SystemDefaults.GetDefaults(this.PersistenceManager);
                MapPointService oService = new MapPointService(oDefaults.MapPointUserName, oDefaults.MapPointPassword);
                if (oService.FindAddress(myAddress)) {
                    Latitude = myAddress.Latitude;
                    Longitude = myAddress.Longitude;
                }
            } catch (Exception ex) {
                throw ex;
            }
            return true;
        }

        public override string City {
            get {
                return base.City;
            }
            set {
                // TODO hg -> skip logic for now, need to figure out a better way to do this when a country is not in
                //if (value != "") {
                //    RdbQuery oQry = new RdbQuery(typeof(ZIPCode), CabRideEngine.ZIPCode.CityEntityColumn, EntityQueryOp.EQ, value);
                //    if (State != null && State != "") {
                //        oQry.AddClause(CabRideEngine.ZIPCode.StateEntityColumn, EntityQueryOp.EQ, State);
                //    }
                //    if (Country != null && Country != "") {
                //        oQry.AddClause(CabRideEngine.ZIPCode.CountryEntityColumn, EntityQueryOp.EQ, Country);
                //    }
                //    EntityList<ZIPCode> oZips = this.PersistenceManager.GetEntities<CabRideEngine.ZIPCode>(oQry);
                //    if (oZips.Count == 0) {
                //        throw new Exception("Unknown City");
                //    }
                //    base.City = value;
                //    if (ZIPCode == null || ZIPCode == "") {
                //        ZIPCode = oZips[0].ZIPCode;
                //    }
                //}
                base.City = value;
            }
        }

        public override string State {
            get {
                return base.State;
            }
            set {
                // TODO hg -> skip checks for now
                //if (value != "") {
                //    RdbQuery oQry = new RdbQuery(typeof(States), States.StateEntityColumn, EntityQueryOp.EQ, value);
                //    oQry.AddClause(States.CountryEntityColumn, EntityQueryOp.EQ, Country);
                //    EntityList<States> oStates = this.PersistenceManager.GetEntities<States>(oQry);
                //    if (oStates.Count == 0) {
                //        throw new Exception("Unknown State");
                //    }
                //    // validate the zip codes
                //    if (ZIPCode != null && ZIPCode.Trim() != "") {
                //        oQry = new RdbQuery(typeof(ZIPCode), CabRideEngine.ZIPCode.StateEntityColumn, EntityQueryOp.EQ, value);
                //        oQry.AddClause(CabRideEngine.ZIPCode.CountryEntityColumn, EntityQueryOp.EQ, Country);
                //        EntityList<ZIPCode> oZips = this.PersistenceManager.GetEntities<ZIPCode>(oQry);
                //        bool bFound = false;
                //        foreach (ZIPCode oZip in oZips) {
                //            if (oZip.ZIPCode == ZIPCode) {
                //                bFound = true;
                //                break;
                //            }
                //        }
                //        if (!bFound) {
                //            ZIPCode = "";
                //        }
                //    }
                //}
                base.State = value;
            }
        }

        public override string ZIPCode {
            get {
                return base.ZIPCode;
            }
            set {
                // TODO hg -> figure out a better way
                //if (value != "") {
                //    RdbQuery oQry = new RdbQuery(typeof(ZIPCode), CabRideEngine.ZIPCode.ZIPCodeEntityColumn, EntityQueryOp.EQ, value);
                //    oQry.AddClause(CabRideEngine.ZIPCode.CountryEntityColumn, EntityQueryOp.EQ, Country);
                //    EntityList<ZIPCode> oZips = this.PersistenceManager.GetEntities<ZIPCode>(oQry);
                //    if (oZips.Count == 0) {
                //        throw new Exception("Unknown ZIP Code");
                //    }
                //    // get the states
                //    bool bFound = false;
                //    for (int i = 0; i < oZips.Count; i++) {
                //        if (State == oZips[i].State) {
                //            bFound = true;
                //            break;
                //        }
                //    }
                //    if (!bFound) {
                //        State = oZips[0].State;
                //    }

                //    // get the cities
                //    bFound = false;
                //    for (int i = 0; i < oZips.Count; i++) {
                //        if (State == oZips[i].State && City == oZips[i].City) {
                //            bFound = true;
                //            County = oZips[i].County;
                //            break;
                //        }
                //    }
                //    if (!bFound) {
                //        for (int i = 0; i < oZips.Count; i++) {
                //            if (State == oZips[i].State) {
                //                City = oZips[i].City;
                //                County = oZips[i].County;
                //                break;
                //            }
                //        }
                //    } 
                //} 
                base.ZIPCode = value;
            }
        }

        public override EntityValidationInfo Validate(EntityValidationContext pContext) {
            //validate address with Mappoint
            try {
                if (base.RowState != DataRowState.Unchanged) {
                    Address myAddress = new Address();
                    myAddress.Street = Street;
                    myAddress.City = City;
                    myAddress.State = State;
                    myAddress.Country = Country;
                    myAddress.ZIP = ZIPCode;

                    SystemDefaults oDefaults = SystemDefaults.GetDefaults(this.PersistenceManager);
                    MapPointService oService = new MapPointService(oDefaults.MapPointUserName, oDefaults.MapPointPassword);
                    if (oService.FindAddress(myAddress)) {
                        Latitude = myAddress.Latitude;
                        Longitude = myAddress.Longitude;
                    }
                }
            } catch (Exception ex) {
                throw ex;
            }

            return base.Validate(pContext);
        }

        public static CustomerAddress GetAddress(PersistenceManager pPM, long pCustomerAddressID) {
            RdbQuery oQry = new RdbQuery(typeof(CustomerAddress), CustomerAddress.CustomerAddressIDEntityColumn, EntityQueryOp.EQ, pCustomerAddressID);
            return pPM.GetEntity<CustomerAddress>(oQry);
        }

        public static CustomerAddress GetAddress(PersistenceManager pPM, string pStreet, string pCity, string pState, string pCountry) {
            RdbQuery qry = new RdbQuery(typeof(CustomerAddress), CustomerAddress.StreetEntityColumn, EntityQueryOp.EQ, pStreet);
            qry.AddClause(CustomerAddress.CityEntityColumn, EntityQueryOp.EQ, pCity);
            qry.AddClause(CustomerAddress.StateEntityColumn, EntityQueryOp.EQ, pState);
            qry.AddClause(CustomerAddress.CountryEntityColumn, EntityQueryOp.EQ, pCountry);
            return pPM.GetEntity<CustomerAddress>(qry);
        }

        public static CustomerAddress GetAddress(PersistenceManager pPM, long pCustomerAddressID, string pAddressType) {
            RdbQuery oQry = new RdbQuery(typeof(CustomerAddress), CustomerAddress.CustomerAddressIDEntityColumn, EntityQueryOp.EQ, pCustomerAddressID);
            oQry.AddClause(CustomerAddress.AddressTypeEntityColumn, EntityQueryOp.EQ, pAddressType);
            return pPM.GetEntity<CustomerAddress>(oQry);
        }

        public string FormattedAddress {
            get {
                string sAddr = Street;
                if (!(Suite == null || Suite.Trim() == "")) {
                    sAddr += ", " + Suite;
                }
                sAddr += ", " + City;
                sAddr += ", " + State;
                sAddr += ", " + Country;

                return sAddr;
            }
        }

        public void SetAddress(Address pAddress) {
            this.Street = pAddress.Street;
            this.Suite = pAddress.Suite;
            this.City = pAddress.City;
            this.State = pAddress.State;
            this.Country = pAddress.Country;
            this.IsMailingAddress = pAddress.IsMailingAddress;
            this.IsDefault = pAddress.IsDefault;
            this.AddressType = pAddress.AddressType;
            if (string.IsNullOrEmpty(AddressType)) {
                AddressType = "Other";
            }
            this.ZIPCode = pAddress.ZIP;
            if (this.Longitude == 0 || this.Longitude == 0 || this.RowState != DataRowState.Unchanged) {
                try {
                    SystemDefaults oDefaults = SystemDefaults.GetDefaults(this.PersistenceManager);
                    pAddress.Validate(oDefaults.MapPointUserName, oDefaults.MapPointPassword);
                    this.Latitude = pAddress.Latitude;
                    this.Longitude = pAddress.Longitude;
                } catch {
                }
            }
        }

        public Address GetAddress() {
            Address address = new Address();
            address.Street = this.Street;
            address.Suite = this.Suite;
            address.City = this.City;
            address.State = this.State;
            address.Country = this.Country;
            address.Latitude = this.Latitude;
            address.Longitude = this.Longitude;
            address.IsMailingAddress = Convert.ToBoolean(this.IsMailingAddress);
            address.IsDefault = Convert.ToBoolean(this.IsDefault);
            address.AddressType = this.AddressType;
            address.ZIP = this.ZIPCode;
            return address;
        }
    }

}
