﻿using CabRideEngineDapper.Utils;
using Dapper;
using System.Collections.Generic;
using System.Linq;

namespace CabRideEngineDapper {
    public partial class DriverRecurringFees {

        public static List<DriverRecurringFees> GetByDriverID(long pDriverID) {
            string sql = @"SELECT  *
                            FROM    dbo.DriverRecurringFees
                            WHERE   DriverID = @DriverID";
            using (var conn = SqlHelper.OpenSqlConnection()) {
                return conn.Query<DriverRecurringFees>(sql, new { DriverID = pDriverID }).ToList();
            }
        }
    }
}
