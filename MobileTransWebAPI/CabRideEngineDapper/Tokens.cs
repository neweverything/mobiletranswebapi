﻿using CabRideEngineDapper.Utils;
using Dapper;

namespace CabRideEngineDapper {
	public partial class Tokens {

		public static Tokens GetToken(string pToken) {
			string sql = @"SELECT * FROM Tokens
							WHERE Token = @Token";

			using (var conn = SqlHelper.OpenSqlConnection()) {
				return conn.Get<Tokens>(sql, new { Token = pToken });
			}

		}
	}
}