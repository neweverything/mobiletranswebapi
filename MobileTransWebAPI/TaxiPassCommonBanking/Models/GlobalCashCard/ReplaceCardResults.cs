﻿using System.Collections.Generic;

namespace TaxiPassCommon.Banking.Models.GlobalCashCard {
	public class ReplaceCardResults {

		public string RESPONSECODE { get; set; }
		public List<Results> RESULTS { get; set; }
		public string STATUS { get; set; }

		public class Results {
			public string DESCRIPTION { get; set; }
			public long OLDCARDNUMBER { get; set; }
			public long NEWCARDNUMBER { get; set; }
		}


	}
}
