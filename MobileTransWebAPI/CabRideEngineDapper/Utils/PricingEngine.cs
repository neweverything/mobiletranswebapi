﻿using CabRideEngineDapper.Enums;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using TaxiPassCommon;
using TaxiPassCommon.GeoCoding;

namespace CabRideEngineDapper.Utils {
    public class PricingEngine {

        public DateTime RideStartTime { get; set; }
        public DateTime? RideEndTime { get; set; }

        public long AccountID { get; set; }
        public long AffiliateID { get; set; }

        public Address pickUpAddress;
        public Address dropOffAddress;

        public bool TestPricing = false;

        public PricingEngine() {

        }

        public PricingEngine(long pAccountID, long pAffiliateID) {
            AccountID = pAccountID;
            AffiliateID = pAffiliateID;
        }

        public PricedRide CalcRate(string pVehicleType = "Taxi") {
            PricedRide oFare = new PricedRide();

            if (pickUpAddress == null) {
                oFare.ErrorMessage = "mising pickUpAddress";
                return oFare;
            }
            if (dropOffAddress == null) {
                oFare.ErrorMessage = "missing dropOffAddress";
                return oFare;
            }

            // Validate and get the GeoCodes
            if (!ValidateAddress(pickUpAddress)) {
                oFare.ErrorMessage = "Could not get GeoCodes for the given PickUp Address";
                return oFare;
            }
            if (!ValidateAddress(dropOffAddress)) {
                oFare.ErrorMessage = "Could not get GeoCodes for the given DropOff Address";
                return oFare;
            }


            DrivingDistance dist = MapHelper.DrivingDistance(pickUpAddress.ToCoordinate(), dropOffAddress.ToCoordinate());

            if (RideEndTime.HasValue) {
                oFare.Duration = (int)Math.Round(RideEndTime.Value.Subtract(RideStartTime).TotalMinutes, 0, MidpointRounding.AwayFromZero);
            } else {
                oFare.Duration = (int)Math.Round(dist.TravelTime, 0, MidpointRounding.AwayFromZero);
            }
            oFare.Mileage = (int)Math.Round(dist.Distance, 0, MidpointRounding.AwayFromZero);


            // Do new pricing 1st, if not found then do old pricing model
            List<Rates> rateList = new List<Rates>();
            AffiliatePricing affPrice = null;
            if (AffiliateID != 0) {
                using (var conn = SqlHelper.OpenSqlConnection()) {
                    affPrice = new AffiliatePricing(conn, AffiliateID, pickUpAddress, dropOffAddress, pVehicleType);
                    if (affPrice.rateList.Count == 0) {
                        //no rates use old method
                        rateList = Rates.RatesForPricing(AccountID, pickUpAddress.ZIP, dropOffAddress.ZIP, pVehicleType);
                    } else if (affPrice.rateList.Count == 1) {
                        // we have a rate
                        rateList.Add(Rates.GetRate(conn, affPrice.rateList[0].RateID));
                    } else {
                        //determine rate to use, by setting a score to each record
                        affPrice.rateList = (from p in affPrice.rateList
                                             orderby p.Score
                                             select p).ToList();
                        rateList.Add(Rates.GetRate(conn, affPrice.rateList[0].RateID));
                    }
                }
            } else {
                rateList = Rates.RatesForPricing(AccountID, pickUpAddress.ZIP, dropOffAddress.ZIP, pVehicleType);
            }
            try {
                if (rateList.Count == 0) {
                    // need to check if there is an OtherAreas Record or auto pricing not available
                    return oFare;
                }

                // we should only have 1 rate if everything is setup correctly, if more than 1 use 1st record
                Rates rate = null;
                if (rateList.Count > 1) {
                    // do we have a flat rate
                    rate = rateList.FirstOrDefault(p => p.RateType.Equals("Flat", StringComparison.CurrentCultureIgnoreCase));
                }
                if (rate == null) {
                    rate = rateList[0];
                }
                if (rate.SurgeDropMultiplier < 1) {
                    rate.SurgeDropMultiplier += 1;
                }
                if (rate.SurgeMeterMultiplier < 1) {
                    rate.SurgeMeterMultiplier += 1;
                }
                if (rate.SurgeTimeMultiplier < 1) {
                    rate.SurgeTimeMultiplier += 1;
                }
                if (rate.SurgeMinimumMultiplier < 1) {
                    rate.SurgeMinimumMultiplier += 1;
                }

                if (rate.RateType.Trim().Equals("Metered", StringComparison.CurrentCultureIgnoreCase)) {
                    oFare.MeteredFare = (rate.Rate * oFare.Mileage) * rate.SurgeMeterMultiplier;
                    oFare.TimeFare = (rate.TimeMeter * oFare.Duration) * rate.SurgeTimeMultiplier;
                } else {
                    oFare.FlatFare = rate.Rate * rate.SurgeDropMultiplier;
                }
                oFare.DropFee = rate.DropFee;
                if (!pickUpAddress.Airport.IsNullOrEmpty()) {
                    oFare.AirportFee = rate.Airport;
                    oFare.IsAirport = true;
                }
                oFare.TollsFee = rate.Tolls;
                oFare.BookingFee = rate.BookingFee;
                oFare.DropFee = rate.DropFee;
                oFare.MinimumCharge = rate.MinBaseCharge * rate.SurgeMinimumMultiplier;

                if (rate.Fee1.GetValueOrDefault(0) != 0) {
                    oFare.Fee1 = rate.Fee1.Value;
                    if (affPrice.FeeType.Fee1Type == (int)CustomFeeRateType.PerMile) {
                        oFare.Fee1 = rate.Fee1.Value * oFare.Mileage;
                    } else if (affPrice.FeeType.Fee1Type == (int)CustomFeeRateType.PerMinute) {
                        oFare.Fee1 = rate.Fee1.Value * oFare.Duration;
                    }
                    oFare.FeeName1 = affPrice.FeeType.Fee1;
                }

                if (rate.Fee2.GetValueOrDefault(0) != 0) {
                    oFare.Fee2 = rate.Fee2.Value;
                    if (affPrice.FeeType.Fee2Type == (int)CustomFeeRateType.PerMile) {
                        oFare.Fee2 = rate.Fee2.Value * oFare.Mileage;
                    } else if (affPrice.FeeType.Fee2Type == (int)CustomFeeRateType.PerMinute) {
                        oFare.Fee2 = rate.Fee2.Value * oFare.Duration;
                    }
                    oFare.FeeName2 = affPrice.FeeType.Fee2;
                }

                if (rate.Fee3.GetValueOrDefault(0) != 0) {
                    oFare.Fee3 = rate.Fee3.Value;
                    if (affPrice.FeeType.Fee3Type == (int)CustomFeeRateType.PerMile) {
                        oFare.Fee3 = rate.Fee3.Value * oFare.Mileage;
                    } else if (affPrice.FeeType.Fee2Type == (int)CustomFeeRateType.PerMinute) {
                        oFare.Fee3 = rate.Fee3.Value * oFare.Duration;
                    }
                    oFare.FeeName3 = affPrice.FeeType.Fee3;
                }

                if (rate.Fee4.GetValueOrDefault(0) != 0) {
                    oFare.Fee4 = rate.Fee4.Value;
                    if (affPrice.FeeType.Fee4Type == (int)CustomFeeRateType.PerMile) {
                        oFare.Fee4 = rate.Fee4.Value * oFare.Mileage;
                    } else if (affPrice.FeeType.Fee4Type == (int)CustomFeeRateType.PerMinute) {
                        oFare.Fee4 = rate.Fee4.Value * oFare.Duration;
                    }
                    oFare.FeeName4 = affPrice.FeeType.Fee4;
                }

                return oFare;
            } catch (Exception ex) {
                throw ex;
            }

        }



        public bool ValidateAddress(Address oAddress) {

            try {
                if (oAddress.Airport != null && oAddress.Airport.Trim() != "" && (oAddress.ZIP == null || oAddress.ZIP.Trim() == "")) {
                    // ZIPCode missing find and add ZIP Code to the airport table
                    CoverageZones oZone = CoverageZones.GetAirport("SJC");
                    if (!oZone.IsNullEntity) {
                        Airports oAirport = Airports.GetAirport(oAddress.Airport);
                        if ((oAirport.ZIPCode == "" || oAirport.Country == "") && oZone.CoverageAreas.Count > 0) {
                            oAirport.ZIPCode = oZone.CoverageAreas[0].ZIPCode;
                            oAirport.Country = oZone.CoverageAreas[0].Country;
                            oAirport.Save(CabRideDapperSettings.LoggedInUser);
                        }
                        oAddress.Country = oAirport.Country;
                        oAddress.ZIP = oAirport.ZIPCode;
                        oAddress.Latitude = oAirport.Latitude;
                        oAddress.Longitude = oAirport.Longitude;
                    }
                }
                if (oAddress.Latitude == 0 || oAddress.Longitude == 0) {
                    var x = TaxiPassCommon.GeoCoding.MapHelper.GeoCodeAddress(oAddress);
                    if (x.status.Equals("OK")) {
                        if (oAddress.Latitude == 0) {
                            oAddress.Latitude = x.results[0].geometry.location.lat;
                        }
                        if (oAddress.Longitude == 0) {
                            oAddress.Longitude = x.results[0].geometry.location.lng;
                        }
                        if (oAddress.ZIP.IsNullOrEmpty()) {
                            try {
                                var addr = x.results[0].address_components.FirstOrDefault(p => p.types.FirstOrDefault(q => q.Equals("postal_code")) != null);
                                if (addr != null) {
                                    oAddress.ZIP = addr.short_name;
                                }
                            } catch (Exception) {
                                // zip not found
                            }
                        }
                    }
                }
                if (oAddress.ZIP.IsNullOrEmpty()) {
                    // reverse geo code to get zip
                    var geoCode = TaxiPassCommon.GeoCoding.MapHelper.ReverseGeoCode((decimal)oAddress.Latitude, (decimal)oAddress.Longitude);
                    if (geoCode.status.Equals("OK")) {
                        oAddress.ZIP = geoCode.ZipCode;
                    }
                }
            } catch (Exception ex) {
                throw ex;
            }

            return (oAddress.Latitude != 0 && oAddress.Longitude != 0);
        }



        //private class Rates

    }
    [Serializable]
    public class PricedRide {
        public string ErrorMessage = "";
        public decimal DropFee = 0;
        public decimal AirportFee = 0;
        public decimal TollsFee = 0;
        public decimal BookingFee = 0;
        public int Mileage = 0;
        public int Duration = 0;
        public bool IsAirport = false;

        public decimal FlatFare { get; set; }
        public decimal MeteredFare { get; set; }
        public decimal TimeFare { get; set; }

        public decimal Fee1 { get; set; }
        public decimal Fee2 { get; set; }
        public decimal Fee3 { get; set; }
        public decimal Fee4 { get; set; }

        public string FeeName1 { get; set; }
        public string FeeName2 { get; set; }
        public string FeeName3 { get; set; }
        public string FeeName4 { get; set; }

        public decimal MinimumCharge { get; set; }


    }


    public class RideTimes {

        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }

        public static RideTimes GetTimes(SqlConnection pConn, long pReservationID) {

            string sql = @"DECLARE @StartTime AS DATETIME
							, @EndTime  AS DATETIME

					SET @StartTime = (SELECT  MAX(StatusTime) StartTime
					FROM dbo.ReservationStatus
					WHERE STATUS = 'Ride Started' AND ReservationID = @ReservationID)

					SET @EndTime = (SELECT  max(StatusTime) EndTime
					FROM dbo.ReservationStatus
					WHERE STATUS = 'Ride Ended' AND ReservationID = @ReservationID)

					SELECT @StartTime StartTime, @EndTime EndTime";

            return pConn.Query<RideTimes>(sql, new { ReservationID = pReservationID }).FirstOrDefault();
        }
    }
}
