using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Runtime.Serialization;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

namespace CabRideEngine {
 
  #region usp_RedeemerAccountingTypeBalance_GetBalancesDataTable
  /// <summary>
  /// The generated <see cref="T:IdeaBlade.Persistence.EntityTable"/> class  
  /// containing all <see cref="usp_RedeemerAccountingTypeBalance_GetBalances"/> objects.
  /// </summary>
  /// <remarks>
  /// ****************************************************
  /// <para>** DO NOT MODIFY THIS CLASS - AutoGenerated Code</para>
  /// <para>** Any changes made to this code will be lost</para>
  /// <para>** the next time this code is regenerated.</para>
  /// <para>**</para>
  /// <para>** Generated from:   EntityBaseclass.template</para>
  /// <para>** Last modified:    2011-07-29T17:03:57.839181-07:00</para>
  /// <para>** Template version: 3.110127</para>
  /// ****************************************************
  /// </remarks>
  [BindingBrowsable(false)]
  [Serializable]
  public partial class usp_RedeemerAccountingTypeBalance_GetBalancesDataTable : IdeaBlade.Persistence.EntityTable, 
     IdeaBlade.Persistence.Rdb.IRdbTable {
  
    /// <summary>
    /// Initializes a new instance of the usp_RedeemerAccountingTypeBalance_GetBalancesDataTable class with no arguments.
    /// </summary>
    public usp_RedeemerAccountingTypeBalance_GetBalancesDataTable() {}

    /// <summary>
    /// Initializes a new instance of the usp_RedeemerAccountingTypeBalance_GetBalancesDataTable class 
    /// with the <see cref="System.Runtime.Serialization.SerializationInfo"/> 
    /// and the <see cref="System.Runtime.Serialization.StreamingContext"/>.
    /// </summary>
    /// <param name="pInfo">The data needed to serialize or deserialize an object.</param>
    /// <param name="pContext">The source and destination of a given serialized stream.</param>
    protected usp_RedeemerAccountingTypeBalance_GetBalancesDataTable(SerializationInfo pInfo, StreamingContext pContext) :
      base(pInfo, pContext) {}

    /// <summary>
    /// Creates a new instance of the <see cref="usp_RedeemerAccountingTypeBalance_GetBalancesDataRow"/>.
    /// Constructs a row from the builder.
    /// <para>** For internal use only. **</para>
    /// </summary>
    /// <param name="pRowBuilder"></param>
    /// <returns>The <see cref="usp_RedeemerAccountingTypeBalance_GetBalancesDataRow"/>.</returns>
    protected override DataRow NewRowFromBuilder(DataRowBuilder pRowBuilder) {
      return new usp_RedeemerAccountingTypeBalance_GetBalances(pRowBuilder);
    }

    /// <summary>Returns the <see cref="System.Type"/> of <see cref="usp_RedeemerAccountingTypeBalance_GetBalances"/>.</summary>
    protected override Type GetRowType() {
      return typeof(usp_RedeemerAccountingTypeBalance_GetBalances);
    }
    
    /// <summary>
    /// Gets the name of the <see cref="T:IdeaBlade.Rdb.RdbKey"/> or 
    /// <see cref="T:IdeaBlade.Persistence.WS.WsKey"/> defined in <b>IdeaBlade.ibconfig</b>. 
    /// </summary>
    /// <remarks>For this <see cref="usp_RedeemerAccountingTypeBalance_GetBalances"/>, the DataSourceKeyName="default".</remarks>
    public override String DataSourceKeyName {
      get { return @"default"; }
    }

    #region Column properties
    //**************************************
    //* Column properties
    //**************************************
    /// <summary>Gets the CashAccountTypeID <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn CashAccountTypeIDColumn {
      get {
        if (mCashAccountTypeIDColumn!=null) return mCashAccountTypeIDColumn;
        mCashAccountTypeIDColumn = GetColumn("CashAccountTypeID", true);
        return mCashAccountTypeIDColumn;
      }
    }
    private DataColumn mCashAccountTypeIDColumn;
    
    /// <summary>Gets the Type <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn TypeColumn {
      get {
        if (mTypeColumn!=null) return mTypeColumn;
        mTypeColumn = GetColumn("Type", true);
        return mTypeColumn;
      }
    }
    private DataColumn mTypeColumn;
    
    /// <summary>Gets the Balance <see cref="System.Data.DataColumn"/>.</summary>
    public DataColumn BalanceColumn {
      get {
        if (mBalanceColumn!=null) return mBalanceColumn;
        mBalanceColumn = GetColumn("Balance", true);
        return mBalanceColumn;
      }
    }
    private DataColumn mBalanceColumn;
    
    #endregion    

    /// <summary>Creates the schema mapping information for this usp_RedeemerAccountingTypeBalance_GetBalancesDataTable.</summary>
    /// <returns>
    /// The <see cref="T:IdeaBlade.Persistence.TableMappingInfo"/> for this usp_RedeemerAccountingTypeBalance_GetBalancesDataTable.
    /// </returns>
    protected override TableMappingInfo CreateTableMappingInfo() {
      RdbTableMappingInfo mappingInfo = new RdbTableMappingInfo(this);
      mappingInfo.SourceOwnerName = "dbo"; 
      mappingInfo.SourceTableName = "usp_RedeemerAccountingTypeBalance_GetBalances"; 
   
      DataColumnMappingCollection columnMappings = mappingInfo.TableMapping.ColumnMappings;
      columnMappings.Clear();
      columnMappings.Add("CashAccountTypeID", "CashAccountTypeID");
      columnMappings.Add("Type", "Type");
      columnMappings.Add("Balance", "Balance");
      return mappingInfo;
    }
  
    /// <summary>** For internal use only. **</summary>
    protected override void UpdateTableMappingInfo() {
      base.UpdateTableMappingInfo();
    }

    /// <summary>
    /// Initializes usp_RedeemerAccountingTypeBalance_GetBalances <see cref="System.Data.DataColumn"/> properties.
    /// </summary>
    public override void InitColumnProperties() {
      base.InitColumnProperties();

      CashAccountTypeIDColumn.Caption = "CashAccountTypeID";
      TypeColumn.Caption = "Type";
      BalanceColumn.Caption = "Balance";
    }
  }
  #endregion
  
  #region usp_RedeemerAccountingTypeBalance_GetBalancesDataRow
  /// <summary>
  /// The generated base class for <see cref="usp_RedeemerAccountingTypeBalance_GetBalances"/>.
  /// </summary>
  /// <remarks>
  /// <para>
  /// It represents a row of persistable data in the <see cref="usp_RedeemerAccountingTypeBalance_GetBalancesDataTable"/>
  /// and derives from <see cref="T:IdeaBlade.Persistence.Entity"/>.
	/// </para>
  /// ****************************************************
  /// <para>** DO NOT MODIFY THIS CLASS - AutoGenerated Code</para>
  /// <para>** Any changes made to this code will be lost</para>
  /// <para>** the next time this code is regenerated.</para>
  /// <para>**</para>
  /// <para>** Generated from:   EntityBaseclass.template</para>
  /// <para>** Last modified:    2011-07-29T17:03:57.839181-07:00</para>
  /// <para>** Template version: 3.110127</para>
  /// ****************************************************
  /// </remarks>  
  [Serializable]
  public abstract partial class usp_RedeemerAccountingTypeBalance_GetBalancesDataRow : IdeaBlade.Persistence.Entity, 
     IdeaBlade.Persistence.Rdb.IRdbEntity {
  
    /// <summary>
    /// Initializes a new instance of the usp_RedeemerAccountingTypeBalance_GetBalancesDataRow.
    /// Constructs a row from the builder.
    /// <para>** For internal use only. **</para>
    /// </summary>
    /// <param name="pRowBuilder"></param>
    protected usp_RedeemerAccountingTypeBalance_GetBalancesDataRow(DataRowBuilder pRowBuilder)
      : base(pRowBuilder) {
    }

    /// <summary>Gets the <see cref="usp_RedeemerAccountingTypeBalance_GetBalancesDataTable"/>.</summary>
    [BindingBrowsable(false)]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public new usp_RedeemerAccountingTypeBalance_GetBalancesDataTable TypedTable {
      get { return (usp_RedeemerAccountingTypeBalance_GetBalancesDataTable) base.Table; }
    }

    /// <summary>Returns the <see cref="System.Type"/> of <see cref="usp_RedeemerAccountingTypeBalance_GetBalancesDataTable"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public static Type GetTableType() {
      return typeof(usp_RedeemerAccountingTypeBalance_GetBalancesDataTable);
    }

    
    /// <summary>
    /// Gets the SQL 'where' clause used to restrict which 
    /// database records are accessible via this entity. 
    /// </summary>
    /// <remarks>
    /// The default property value is defined in the Current Class 
    /// Properties tab in the <b>IdeaBlade DevForce Object Mapping Tool</b>.
    /// <para>
    /// The 'where' clause will be used, in addition to any other 
    /// query-specific criteria, in every retrieval of entities of this type.
    /// </para>
    /// </remarks>
    [BindingBrowsable(false)]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public string SqlWhereClause {
      get { return ""; }
    }
    
    /// <summary>
    /// Determines whether to use the 'distinct' keyword when 
    /// generating the SQL select clause for objects of this type.
    /// </summary>
    /// <remarks>Defaults to false, but may be overridden in subclasses.</remarks>
    [BindingBrowsable(false)]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public bool ForceSqlDistinct {
      get { return false; }
    }

    #region Enums
    //**************************************
    //* Enums
    //**************************************
    #endregion
	
    #region Relation properties
    //**************************************
    //* Relation properties
    //**************************************
    #endregion
    
    #region EntityColumn definitions
    //**************************************
    //* Static EntityColumn definitions
    //**************************************
    /// <summary>The CashAccountTypeID <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn CashAccountTypeIDEntityColumn =
      new EntityColumn(typeof(usp_RedeemerAccountingTypeBalance_GetBalances), "CashAccountTypeID", typeof(System.Int64), false, true, true, DataSourceAccessType.ReadWrite);      
    /// <summary>The Type <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn TypeEntityColumn =
      new EntityColumn(typeof(usp_RedeemerAccountingTypeBalance_GetBalances), "Type", typeof(System.String), false, false, false, DataSourceAccessType.ReadWrite);      
    /// <summary>The Balance <see cref="T:IdeaBlade.Persistence.EntityColumn"/>.</summary>
    public static readonly EntityColumn BalanceEntityColumn =
      new EntityColumn(typeof(usp_RedeemerAccountingTypeBalance_GetBalances), "Balance", typeof(System.Decimal), false, false, false, DataSourceAccessType.ReadWrite);      
    #endregion
    
    #region Properties
    //**************************************
    //* CashAccountTypeID methods
    //**************************************
    /// <summary>Gets the CashAccountTypeID <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn CashAccountTypeIDColumn {
      get { return TypedTable.CashAccountTypeIDColumn; }
    }

    /// <summary>Gets or sets the CashAccountTypeID.</summary>
    [DBDataType(typeof(System.Int64))]
    public virtual System.Int64 CashAccountTypeID {
      get { 
        System.Int64 result_;
        if (GetInterceptor<System.Int64>("CashAccountTypeID", GetCashAccountTypeIDImpl, out result_)) return result_;
        return GetCashAccountTypeIDImpl();
      }
      set { 
          if (!SetInterceptor<System.Int64>("CashAccountTypeID", value, SetCashAccountTypeIDImpl)) {
            SetCashAccountTypeIDImpl(value);
          }  
      }    
    }
    private System.Int64 GetCashAccountTypeIDImpl() {
      return (System.Int64) GetColumnValue(CashAccountTypeIDColumn, typeof(System.Int64), false); 
    }
    private void SetCashAccountTypeIDImpl(System.Int64 value) {
      SetColumnValue(CashAccountTypeIDColumn, value);
    }
    
    //**************************************
    //* Type methods
    //**************************************
    /// <summary>Gets the Type <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn TypeColumn {
      get { return TypedTable.TypeColumn; }
    }

    /// <summary>Gets or sets the Type.</summary>
    [MaxTextLength(50)]
    [DBDataType(typeof(System.String))]
    public virtual System.String Type {
      get { 
        System.String result_;
        if (GetInterceptor<System.String>("Type", GetTypeImpl, out result_)) return result_;
        return GetTypeImpl();
      }
      set { 
          if (!SetInterceptor<System.String>("Type", value, SetTypeImpl)) {
            SetTypeImpl(value);
          }  
      }    
    }
    private System.String GetTypeImpl() {
      return (System.String) GetColumnValue(TypeColumn, typeof(System.String), false); 
    }
    private void SetTypeImpl(System.String value) {
      SetColumnValue(TypeColumn, value);
    }
    
    //**************************************
    //* Balance methods
    //**************************************
    /// <summary>Gets the Balance <see cref="System.Data.DataColumn"/>.</summary>
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    [BindingBrowsable(false)]
    public virtual DataColumn BalanceColumn {
      get { return TypedTable.BalanceColumn; }
    }

    /// <summary>Gets or sets the Balance.</summary>
    [DBDataType(typeof(System.Decimal))]
    public virtual System.Decimal Balance {
      get { 
        System.Decimal result_;
        if (GetInterceptor<System.Decimal>("Balance", GetBalanceImpl, out result_)) return result_;
        return GetBalanceImpl();
      }
      set { 
          if (!SetInterceptor<System.Decimal>("Balance", value, SetBalanceImpl)) {
            SetBalanceImpl(value);
          }  
      }    
    }
    private System.Decimal GetBalanceImpl() {
      return (System.Decimal) GetColumnValue(BalanceColumn, typeof(System.Decimal), false); 
    }
    private void SetBalanceImpl(System.Decimal value) {
      SetColumnValue(BalanceColumn, value);
    }
    
    #endregion

    #region usp_RedeemerAccountingTypeBalance_GetBalances.StoredProcRdbQuery
    //**************************************
    //* usp_RedeemerAccountingTypeBalance_GetBalances.StoredProcRdbQuery
    //**************************************    
    /// <summary>
    /// Subclassed <see cref="T:IdeaBlade.Persistence.Rdb.StoredProcRdbQuery"/> which retrieves usp_RedeemerAccountingTypeBalance_GetBalances entities.
    /// </summary>
    [Serializable]
    public class StoredProcRdbQuery : IdeaBlade.Persistence.Rdb.StoredProcRdbQuery {
      /// <summary>Initializes a new instance of the StoredProcRdbQuery class.</summary>
      public StoredProcRdbQuery(
        Object pDate
        )
        : base("dbo", "usp_RedeemerAccountingTypeBalance_GetBalances", typeof(usp_RedeemerAccountingTypeBalance_GetBalances)) {
        RdbParameter aParameter;

        aParameter = new RdbParameter("@RETURN_VALUE", DbType.Int32);
        aParameter.Direction = ParameterDirection.ReturnValue;
        aParameter.Size = 0;
        this.Parameters.Add(aParameter);

        aParameter = new RdbParameter("@Date", DbType.DateTime);
        aParameter.Direction = ParameterDirection.Input;
        aParameter.Size = 0;
        aParameter.Value = pDate;
        this.Parameters.Add(aParameter);

      }
    }
    #endregion
  }
  #endregion

  #region EntityPropertyDescriptors.usp_RedeemerAccountingTypeBalance_GetBalancesPropertyDescriptor
  public static partial class EntityPropertyDescriptors {

    /// <summary>
    /// Gets the usp_RedeemerAccountingTypeBalance_GetBalancesPropertyDescriptor for <see cref="usp_RedeemerAccountingTypeBalance_GetBalances"/>.
    /// </summary>
    public static usp_RedeemerAccountingTypeBalance_GetBalancesPropertyDescriptor usp_RedeemerAccountingTypeBalance_GetBalances {
      get { return usp_RedeemerAccountingTypeBalance_GetBalancesPropertyDescriptor.Instance; }
    }
    
    /// <summary>
    /// The generated class containing <see cref="usp_RedeemerAccountingTypeBalance_GetBalances"/> PropertyDescriptors.
    /// </summary>
    public partial class usp_RedeemerAccountingTypeBalance_GetBalancesPropertyDescriptor : AdaptedPropertyDescriptor {
	
			/// <summary>
      /// Initializes a new instance of the usp_RedeemerAccountingTypeBalance_GetBalancesPropertyDescriptor class. 
      /// </summary>
      /// <param name="pParentDescriptor">The parent <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/></param>
      /// <param name="pPropertyPath">The type of the property value</param>
			public usp_RedeemerAccountingTypeBalance_GetBalancesPropertyDescriptor(AdaptedPropertyDescriptor pParentDescriptor, String pPropertyPath)
        : base(pParentDescriptor, pPropertyPath) {}
		
      /// <summary>
      /// Initializes a new instance of the usp_RedeemerAccountingTypeBalance_GetBalancesPropertyDescriptor class. 
      /// </summary>
      /// <param name="pComponentType">The type of object to which the property is bound</param>
      protected usp_RedeemerAccountingTypeBalance_GetBalancesPropertyDescriptor(Type pComponentType)
        : base(pComponentType) {}

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for CashAccountTypeID.</summary>
      public AdaptedPropertyDescriptor CashAccountTypeID {
        get { return Get("CashAccountTypeID"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Type.</summary>
      public AdaptedPropertyDescriptor Type {
        get { return Get("Type"); }
      }

      /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Balance.</summary>
      public AdaptedPropertyDescriptor Balance {
        get { return Get("Balance"); }
      }

     internal static usp_RedeemerAccountingTypeBalance_GetBalancesPropertyDescriptor Instance = new usp_RedeemerAccountingTypeBalance_GetBalancesPropertyDescriptor(typeof(usp_RedeemerAccountingTypeBalance_GetBalances));
    }
  }
  #endregion
  
}
