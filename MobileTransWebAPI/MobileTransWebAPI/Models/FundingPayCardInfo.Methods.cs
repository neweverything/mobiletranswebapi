﻿using CabRideEngineDapper.Utils;
using Dapper;
using System.Data;
using System.Linq;

namespace MobileTransWebAPI.Models {
    public partial class FundingPayCardInfo {

        public static int GetMissedDejavooBatchCount(long pAffiliateDriverID) {

            using (var conn = SqlHelper.OpenSqlConnection()) {
                return conn.Query<int?>("usp_Dejavoo_NotBatchedCount", new { AffiliateDriverID = pAffiliateDriverID }, commandType: CommandType.StoredProcedure).FirstOrDefault().GetValueOrDefault(0);
            }
        }
    }
}