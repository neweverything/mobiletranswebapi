﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace CabRideEngineDapper {
    public partial class usp_GetRideCreateDriverQueue {

        public long ReservationID { get; set; }
        public string ReserveNo { get; set; }
        public DateTime PickUpDate { get; set; }
        public DateTime? PickUpTime { get; set; }
        public long DriverID { get; set; }
        public string Status { get; set; }
        public string AccountNo { get; set; }
        public long AccountID { get; set; }
        public bool ExclusiveDispatch { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZIPCode { get; set; }
        public string CustomerPhone { get; set; }
        public long AffiliateID { get; set; }
        public string SMSText { get; set; }
        public bool DoNotSendNewRideTxtMsg { get; set; }
        public int DriverSearchWait { get; set; }
        public int DriverSearchWait2 { get; set; }
        public int DriverSearchWait3 { get; set; }
        public int DriverSearchAttempts { get; set; }
        public long TestAffiliateDriverID { get; set; }
        public string DriverSearchWaitTimes { get; set; }
        public string FutureJobDriverSearchWaitTimes { get; set; }
        public string RunType { get; set; }

        public static List<usp_GetRideCreateDriverQueue> Execute(SqlConnection pConn) {
            return pConn.Query<usp_GetRideCreateDriverQueue>("usp_GetRideCreateDriverQueue", commandType: CommandType.StoredProcedure).ToList();
        }

    }
}
