﻿using CabRideEngine.Enums;
using Csla.Validation;
using IdeaBlade.Persistence;
using IdeaBlade.Persistence.Rdb;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using TaxiPassCommon;
using TaxiPassCommon.Banking;

namespace CabRideEngine {
	[Serializable]
	[JsonObject(IsReference = true)]
	public sealed partial class Affiliate : AffiliateDataRow {

		#region Constructors -- DO NOT MODIFY
		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new'
		private Affiliate()
			: this(null) {
		}

		// Typed DataSet constructor (needed by framework) - DO NOT REMOVE
		public Affiliate(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion


		public static Affiliate Create(PersistenceManager pManager, bool pAddFees = true) {
			Affiliate oAffiliate = null;
			try {
				oAffiliate = (Affiliate)pManager.CreateEntity(typeof(Affiliate));
				oAffiliate.FleetTypeID = 1;
				oAffiliate.ServiceType = "Taxi";
				oAffiliate.PaySchedule = "0110110";
				oAffiliate.ACHPaymentSchedule = (int)ACHPaymentScheduleEnum.Custom;
				// Using the IdeaBlade Id Generation technique
				//pManager.GenerateId(oAffiliate, Affiliate.AffiliateIDEntityColumn);


				oAffiliate.AddToManager();
				AffiliateDriverDefaults.Create(oAffiliate);

				//DriverFee fee = DriverFee.Create(oAffiliate);
				//fee.StartAmount = 100;
				//fee.EndAmount = 250;
				//fee.Fee = 5;

				//Affiliate
				if (pAddFees) {
					AffiliateFees fee = AffiliateFees.Create(oAffiliate);
					fee.StartAmount = 0.01M;
					fee.EndAmount = 20;
					fee.Fee = 2;

					AffiliateFees fee1 = AffiliateFees.Create(oAffiliate);
					fee1.StartAmount = 20.01M;
					fee1.EndAmount = 40;
					fee1.Fee = 3;

					AffiliateFees fee2 = AffiliateFees.Create(oAffiliate);
					fee2.StartAmount = 40.01M;
					fee2.EndAmount = 100;
					fee2.Fee = 4.50M;

					AffiliateFees fee3 = AffiliateFees.Create(oAffiliate);
					fee3.StartAmount = 100.01M;
					fee3.EndAmount = 200;
					fee3.Fee = 9;

					AffiliateFees fee4 = AffiliateFees.Create(oAffiliate);
					fee4.StartAmount = 200.01M;
					fee4.EndAmount = 300;
					fee4.Fee = 13.50M;

					AffiliateFees fee5 = AffiliateFees.Create(oAffiliate);
					fee5.StartAmount = 300.01M;
					fee5.EndAmount = 400;
					fee5.Fee = 18.00M;

					AffiliateFees fee6 = AffiliateFees.Create(oAffiliate);
					fee6.StartAmount = 400.01M;
					fee6.EndAmount = 500;
					fee6.Fee = 22.50M;

					AffiliateFees fee7 = AffiliateFees.Create(oAffiliate);
					fee7.StartAmount = 500.01M;
					fee7.EndAmount = 600;
					fee7.Fee = 27.00M;

					AffiliateFees fee8 = AffiliateFees.Create(oAffiliate);
					fee8.StartAmount = 600.01M;
					fee8.EndAmount = 700;
					fee8.Fee = 31.50M;

					AffiliateFees fee9 = AffiliateFees.Create(oAffiliate);
					fee9.StartAmount = 700.01M;
					fee9.EndAmount = 800;
					fee9.Fee = 36.00M;

					AffiliateFees fee10 = AffiliateFees.Create(oAffiliate);
					fee10.StartAmount = 800.01M;
					fee10.EndAmount = 900;
					fee10.Fee = 40.50M;

					AffiliateFees fee11 = AffiliateFees.Create(oAffiliate);
					fee11.StartAmount = 900.01M;
					fee11.EndAmount = 1000;
					fee11.Fee = 45.00M;

					AffiliateFees fee12 = AffiliateFees.Create(oAffiliate);
					fee12.StartAmount = 1000.01M;
					fee12.EndAmount = 0;
					fee12.Fee = 50.00M;
				}

				// get default country
				RdbQuery oQry = new RdbQuery(typeof(Countries), Countries.DefaultEntityColumn, EntityQueryOp.EQ, true);
				Countries oCountry = pManager.GetEntity<Countries>(oQry);
				if (!oCountry.IsNullEntity) {
					oAffiliate.Country = oCountry.Country;
				}

			} catch (Exception ex) {
				throw ex;
			}
			return oAffiliate;
		}

		#region Suggested Customizations

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion



		protected override void AddRules(RuleList pList) {
			PropertyRequiredRule.AddToList(pList, NameColumn.ColumnName);
			PropertyRequiredRule.AddToList(pList, TimeZoneColumn.ColumnName);
			pList.Add(SetGeoCodes, "");
		}

		public bool SetGeoCodes(object pTarget, RuleArgs e) {
			Affiliate oAffiliate = (Affiliate)pTarget;
			bool bOk = false;

			//validate address with Mappoint
			try {
				Address oAddress = new Address();
				if (!oAffiliate.Line1.IsNullOrEmpty()) {
					oAddress.Street = oAffiliate.Line1;
				}
				if (!oAffiliate.City.IsNullOrEmpty()) {
					oAddress.City = oAffiliate.City;
				}
				if (!oAffiliate.State.IsNullOrEmpty()) {
					oAddress.State = oAffiliate.State;
				}
				TaxiPassCommon.GeoCoding.Google.GeoResponse oGeoResponse = TaxiPassCommon.GeoCoding.Google.GetGeoCode(oAddress);
				if (oGeoResponse.Status == "OK") {
					Latitude = oGeoResponse.Results[0].Geometry.Location.Lat;
					Longitude = oGeoResponse.Results[0].Geometry.Location.Lng;
					bOk = true;
				} else if (oGeoResponse.Status == "ZERO_RESULTS") {
					bOk = true;
				}

				/* 12/19/2012 MapPoint doesn't work anymore
				Address myAddress = new Address();
				myAddress.Street = oAffiliate.Line1;
				myAddress.City = oAffiliate.City;
				myAddress.State = oAffiliate.State;
				myAddress.Country = oAffiliate.Country;
				myAddress.ZIP = oAffiliate.ZIPCode;

				SystemDefaults oDefaults = SystemDefaults.GetDefaults(this.PersistenceManager);
				MapPointService oService = new MapPointService(oDefaults.MapPointUserName, oDefaults.MapPointPassword);
				if (oService.FindAddress(myAddress)) {
					Latitude = myAddress.Latitude;
					Longitude = myAddress.Longitude;
				}

			} catch (MapPointServiceAddressNotFoundException ex) {
				string sMsg = "";
				sMsg = ex.Message;
				bOk = true; // TaxiPassCommon.WindowUtils.Verify("Address not found: " + ex.Message + "\n\n" + "Save record?");
			} catch (MapPointServiceAddressException ex) {
				if (ex.NumberOfAddresses > 1) {
					e.Description = "Found too many addresses in Mappoint";
				}
			*/
			} catch (Exception ex) {
				if (ex.Message.Contains("The FindAddress method cannot find Post Office Box addresses")) {
					bOk = true;
				} else {
					// hg Dec 7, 2011 address changed need to resign up
					e.Description = ex.Message;
					bOk = true; // false;
				}
			}
			return bOk;
		}

		public override string TimeZone {
			get {
				string myTimeZone = base.TimeZone;
				if (TimeZones.GetAll()[0].DisplayName.Contains("GMT")) {
					if (!myTimeZone.IsNullOrEmpty()) {
						myTimeZone = myTimeZone.Replace("UTC", "GMT");
					}
				} else {
					if (!myTimeZone.IsNullOrEmpty()) {
						myTimeZone = myTimeZone.Replace("GMT", "UTC");
					}
				}
				return myTimeZone;
			}
			set {
				base.TimeZone = value;
			}
		}

		public override string Line1 {
			get {
				return base.Line1;
			}
			set {
				if (value != base.Line1) {
					Latitude = 0;
					Longitude = 0;
				}
				base.Line1 = value;
			}
		}

		public override string ServiceType {
			get {
				if (base.ServiceType.IsNullOrEmpty()) {
					return "";
				}
				return base.ServiceType;
			}
			set {
				base.ServiceType = value;
			}
		}

		public string ServiceSubType {
			get {
				return AffiliateRef.GetByGroupReference(this, "Account", "SubType").ValueString;
			}
			set {
				AffiliateRef rec = AffiliateRef.GetCreate(this, "Account", "SubType", "");
				rec.ValueString = value;
			}
		}


		public override string City {
			get {
				return base.City;
			}
			set {
				if (value != "") {
					RdbQuery oQry = new RdbQuery(typeof(ZIPCode), CabRideEngine.ZIPCode.CityEntityColumn, EntityQueryOp.EQ, value);
					if (State != null && State != "") {
						oQry.AddClause(CabRideEngine.ZIPCode.StateEntityColumn, EntityQueryOp.EQ, State);
					}
					if (Country != null && Country != "") {
						oQry.AddClause(CabRideEngine.ZIPCode.CountryEntityColumn, EntityQueryOp.EQ, Country);
					}
					EntityList<ZIPCode> oZips = this.PersistenceManager.GetEntities<CabRideEngine.ZIPCode>(oQry);
					if (oZips.Count == 0) {
						throw new Exception("Unknown City");
					}
					base.City = value;
					if (ZIPCode == "") {
						ZIPCode = oZips[0].ZIPCode;
					}
				}
				base.City = value;
			}
		}

		public override string State {
			get {
				return base.State;
			}
			set {
				if (value != "") {
					RdbQuery oQry = new RdbQuery(typeof(States), States.StateEntityColumn, EntityQueryOp.EQ, value);
					oQry.AddClause(States.CountryEntityColumn, EntityQueryOp.EQ, Country);
					EntityList<States> oStates = this.PersistenceManager.GetEntities<States>(oQry);
					if (oStates.Count == 0) {
						//throw new Exception("Unknown State");
					}
					// validate the zip codes
					if (ZIPCode.Trim() != "") {
						oQry = new RdbQuery(typeof(ZIPCode), CabRideEngine.ZIPCode.StateEntityColumn, EntityQueryOp.EQ, value);
						oQry.AddClause(CabRideEngine.ZIPCode.CountryEntityColumn, EntityQueryOp.EQ, Country);
						EntityList<ZIPCode> oZips = this.PersistenceManager.GetEntities<ZIPCode>(oQry);
						bool bFound = false;
						foreach (ZIPCode oZip in oZips) {
							if (oZip.ZIPCode == ZIPCode) {
								bFound = true;
								break;
							}
						}
						if (!bFound) {
							ZIPCode = "";
						}
					}


				}
				base.State = value;
			}
		}


		public override string ZIPCode {
			get {
				return base.ZIPCode;
			}
			set {
				if (value != "") {
					RdbQuery oQry = new RdbQuery(typeof(ZIPCode), CabRideEngine.ZIPCode.ZIPCodeEntityColumn, EntityQueryOp.EQ, value);
					oQry.AddClause(CabRideEngine.ZIPCode.CountryEntityColumn, EntityQueryOp.EQ, Country);
					EntityList<ZIPCode> oZips = this.PersistenceManager.GetEntities<ZIPCode>(oQry);
					if (oZips.Count == 0) {
						throw new Exception("Unknown ZIP Code");
					}
					// get the states
					bool bFound = false;
					for (int i = 0; i < oZips.Count; i++) {
						if (State == oZips[i].State) {
							bFound = true;
							break;
						}
					}
					if (!bFound) {
						State = oZips[0].State;
					}

					// get the cities
					bFound = false;
					for (int i = 0; i < oZips.Count; i++) {
						if (State == oZips[i].State && City == oZips[i].City) {
							bFound = true;
							break;
						}
					}
					if (!bFound) {
						for (int i = 0; i < oZips.Count; i++) {
							if (State == oZips[i].State) {
								City = oZips[i].City;
								break;
							}
						}
					}
				}
				base.ZIPCode = value;
			}
		}

		public override string AffiliateCode {
			get {
				return base.AffiliateCode;
			}
			set {
				if (value == "") {
					throw new Exception("Affiliate Code must contain a value");
				}
				value = value.ToUpper();
				RdbQuery oQry = new RdbQuery(typeof(Affiliate), Affiliate.AffiliateCodeEntityColumn, EntityQueryOp.EQ, value);
				oQry.AddClause(Affiliate.AffiliateIDEntityColumn, EntityQueryOp.NE, AffiliateID);
				if (!this.PersistenceManager.GetEntity<Affiliate>(oQry).IsNullEntity) {
					throw new Exception("Value must be unique!");
				}
				base.AffiliateCode = value;
			}
		}

		protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
			if (IsDeserializing)
				return;
			base.OnColumnChanging(pArgs);
		}


		public override string LicenseNumber {
			get {
				return base.LicenseNumber;
			}
			set {
				base.LicenseNumber = value;
				if (this.MarketID.HasValue && !string.IsNullOrEmpty(value)) {
					MasterFleets oFleet = MasterFleets.GetFleetByLicenseNo(this.PersistenceManager, value, this.MarketID.Value);
					if (!oFleet.IsNullEntity) {
						this.Name = StringUtils.ProperCase(oFleet.NameOfLicensee.ToLower());
						this.Line1 = StringUtils.ProperCase(oFleet.StreetAddress.ToLower());
						this.ZIPCode = oFleet.ZipCode;
						this.Phone = oFleet.Telephone;
					}
				}
			}
		}

		public bool PaySunday {
			get {
				return PaySchedule.Left(1) == "1";
			}
			set {
				string temp = "";
				temp = value ? "1" : "0";
				temp += PaySchedule.Substring(1);
				PaySchedule = temp;
			}
		}

		public bool PayMonday {
			get {
				return PaySchedule.Substring(1, 1) == "1";
			}
			//set {
			//    string temp = "";
			//    temp = value ? "1" : "0";
			//    PaySchedule = PaySchedule.Remove(1, 1);
			//    PaySchedule = PaySchedule.Insert(1, temp);
			//}
		}

		public bool PayTuesday {
			get {
				return PaySchedule.Substring(2, 1) == "1";
			}
			set {
				string temp = "";
				temp = value ? "1" : "0";
				PaySchedule = PaySchedule.Remove(2, 1);
				PaySchedule = PaySchedule.Insert(2, temp);
			}
		}

		public bool PayWednesday {
			get {
				return PaySchedule.Substring(3, 1) == "1";
			}
			set {
				string temp = "";
				temp = value ? "1" : "0";
				PaySchedule = PaySchedule.Remove(3, 1);
				PaySchedule = PaySchedule.Insert(3, temp);
			}
		}

		public bool PayThursday {
			get {
				return PaySchedule.Substring(4, 1) == "1";
			}
			set {
				string temp = "";
				temp = value ? "1" : "0";
				PaySchedule = PaySchedule.Remove(4, 1);
				PaySchedule = PaySchedule.Insert(4, temp);
			}
		}

		public bool PayFriday {
			get {
				return PaySchedule.Substring(5, 1) == "1";
			}
			set {
				string temp = "";
				temp = value ? "1" : "0";
				PaySchedule = PaySchedule.Remove(5, 1);
				PaySchedule = PaySchedule.Insert(5, temp);
			}
		}

		public bool PaySaturday {
			get {
				return PaySchedule.Right(1) == "1";
			}
			set {
				string temp = "";
				temp = value ? "1" : "0";
				PaySchedule = PaySchedule.Left(6);
				PaySchedule += temp;
			}
		}

		public override string DefaultPlatform {
			get => base.DefaultPlatform;
			set {
				bool valueChanged = base.DefaultPlatform != value;
				base.DefaultPlatform = value;
				if (valueChanged && value == Platforms.ISO_Licensing.ToString()) {
					DoNotPay = true;
					SoftwareLicensing = true;
					AutoPayFleet = false;
					DoNotTextDrivers = true;
					EnableAutoACH = false;
					VerisignAccountID = null;
				}
			}
		}


		public string GetFirstAffiliateLogin { // snelson 8/17/2006; added for FAX to affiliate; needed ability to display login / password on fax; take first AffiliateContact record
			get {
				RdbQuery oQry = new RdbQuery(typeof(AffiliateContacts), AffiliateContacts.AffiliateIDEntityColumn, EntityQueryOp.EQ, this.AffiliateID);
				oQry.AddClause(AffiliateContacts.EmailEntityColumn, EntityQueryOp.NE, "");
				oQry.AddClause(AffiliateContacts.EmailEntityColumn, EntityQueryOp.NE, null);
				oQry.AddClause(AffiliateContacts.PasswordEntityColumn, EntityQueryOp.NE, "");
				oQry.AddClause(AffiliateContacts.PasswordEntityColumn, EntityQueryOp.NE, null);
				EntityList<AffiliateContacts> oAffiliateContact = this.PersistenceManager.GetEntities<AffiliateContacts>(oQry);
				if (oAffiliateContact.Count == 0) {
					return "** NO LOGIN ON FILE, PLEASE CALL 1-800-CABRIDE ext. 998 **";  // for now display this message
				} else {
					return oAffiliateContact[0].Email;
				}
			}
		}

		public string GetFirstAffiliatePassword { // snelson 8/17/2006; added for FAX to affiliate; needed ability to display login / password on fax; take first AffiliateContact record
			get {
				RdbQuery oQry = new RdbQuery(typeof(AffiliateContacts), AffiliateContacts.AffiliateIDEntityColumn, EntityQueryOp.EQ, this.AffiliateID);
				oQry.AddClause(AffiliateContacts.EmailEntityColumn, EntityQueryOp.NE, "");
				oQry.AddClause(AffiliateContacts.EmailEntityColumn, EntityQueryOp.NE, null);
				oQry.AddClause(AffiliateContacts.PasswordEntityColumn, EntityQueryOp.NE, "");
				oQry.AddClause(AffiliateContacts.PasswordEntityColumn, EntityQueryOp.NE, null);
				EntityList<AffiliateContacts> oAffiliateContact = this.PersistenceManager.GetEntities<AffiliateContacts>(oQry);
				if (oAffiliateContact.Count == 0) {
					return "** NO PASSWORD ON FILE, PLEASE CALL 1-800-CABRIDE ext. 998 **"; // for now display this message
				} else {
					//try {
					//    Utils.Encryption oEncrypt = new CabRideEngine.Utils.Encryption();
					//    string sValue = oEncrypt.Decrypt(oAffiliateContact[0].Password, this.Salt);
					//    return sValue;
					//} catch (Exception ex) {
					//    return "** PLEASE CALL 1-800-CABRIDE ext. 998 **"; // password couldn't decrypt; probably needs to be resaved in CabRide
					//}
					return oAffiliateContact[0].Password;
				}
			}
		}

		public string FormattedReservationPhone {
			get {
				return ReservePhone.FormattedPhoneNumber();
			}
		}


		public static Affiliate GetDefaultAffiliateForState(PersistenceManager pPM, string pState) {
			States state = pPM.GetNullEntity<States>();
			state = States.GetStateForCountry(pPM, "USA", pState);

			if (!state.StateName.IsNullOrEmpty()) {
				pState = state.StateName;
			}

			string affName = string.Format("{0} General", pState);
			RdbQuery qry = new RdbQuery(typeof(Affiliate), Affiliate.StateEntityColumn, EntityQueryOp.EQ, state.State);
			qry.AddClause(Affiliate.NameEntityColumn, EntityQueryOp.EQ, affName);

			Affiliate rec = pPM.GetEntity<Affiliate>(qry);
			if (rec.IsNullEntity) {
				rec = Affiliate.Create(pPM);
				rec.Name = affName;
				rec.State = state.State;
				if (!state.IsNullEntity) {
					Markets market = Markets.GetMarket(pPM, pState);
					if (market.IsNullEntity) {
						market = Markets.Create(pPM);
						market.Market = pState;
						market.State = state.State;
						market.Country = state.Country;
						market.TimeZone = state.TimeZone;
						market.MatchingPriority = 999;
						market.AndroidEnabled = true;
						market.Save();
					}
					rec.MarketID = market.MarketID;
				}
				string affCode = string.Format("{0}GE", state.State);
				if (affCode.Trim().Length < 4) {
					affCode = StringUtils.GetUniqueKey(4, true);
				}

				while (true) {
					qry = new RdbQuery(typeof(Affiliate), AffiliateCodeEntityColumn, EntityQueryOp.EQ, affCode);
					Affiliate code = pPM.GetEntity<Affiliate>(qry);
					if (code.IsNullEntity) {
						break;
					}
					affCode = StringUtils.GetUniqueKey(4);
				}

				rec.AffiliateCode = affCode;

				rec.TimeZone = state.TimeZone;
				if (rec.TimeZone.IsNullOrEmpty()) {
					rec.TimeZone = "(GMT-05:00) Eastern Time (US & Canada)";
				}
				rec.VerisignAccountID = VerisignAccounts.GetDefaultAccount(pPM).VerisignAccountID;
				rec.DefaultPlatform = Platforms.TaxiPay.ToString();

				rec.AffiliateDriverDefaults.TrackSwipeCounts = true;
				rec.AffiliateDriverDefaults.HHFareFirst = true;
				rec.Save();
				pPM.SaveChanges(rec.AffiliateFeeses);
				rec.AffiliateDriverDefaults.Save();
			}

			return rec;
		}


		public static Affiliate GetAffiliateForJob(PersistenceManager pPM, TaxiPassCommon.Address pPUAddress, TaxiPassCommon.Address pDOAddress) {
			return GetAffiliateForJob(pPM, pPUAddress, pDOAddress, false);
		}

		public static Affiliate GetAffiliateForJob(PersistenceManager pPM, TaxiPassCommon.Address pPUAddress, TaxiPassCommon.Address pDOAddress, bool pUse1stAddressFound) {

			EntityList<CoverageArea> oPUAreas = CoverageArea.GetCoverageAreas(pPM, pPUAddress, pUse1stAddressFound);
			EntityList<CoverageArea> oDOAreas = CoverageArea.GetCoverageAreas(pPM, pDOAddress, pUse1stAddressFound);

			long puZone = 0;
			long doZone = 0;

			bool bFoundRate = false;

			if (oDOAreas != null) {
				foreach (CoverageArea puArea in oPUAreas) {
					puZone = puArea.CoverageZoneID;

					foreach (CoverageArea doArea in oDOAreas) {
						doZone = doArea.CoverageZoneID;
						EntityList<Rates> oRates = Rates.GetRates(pPM, puZone, doZone);
						if (oRates.Count > 0) {
							bFoundRate = true;
							break;
						}
					}

					if (bFoundRate) {
						break;
					}
				}
			}

			if (!bFoundRate) {
				doZone = CoverageZones.GetOtherArea(pPM).CoverageZoneID;
				foreach (CoverageArea puArea in oPUAreas) {
					puZone = puArea.CoverageZoneID;
					EntityList<Rates> oRates = Rates.GetRates(pPM, puZone, doZone);
					if (oRates.Count > 0) {
						bFoundRate = true;
						break;
					}
				}
			}

			EntityList<AffiliateCoverageZones> oAffiliatesZones = AffiliateCoverageZones.GetAffiliatesZones(pPM, puZone, doZone);
			List<Affiliate> oAffiliates = new List<Affiliate>();

			//loop through to see how many Affiliatges can do this run
			foreach (AffiliateCoverageZones oZone in oAffiliatesZones) {
				if (oZone.CoverageZoneID == puZone) {
					oAffiliates.Add(oZone.Affiliate);
				}
			}
			if (oAffiliates.Count == 0) {
				//no one can do this run, do we have an affilate in dropoff zone that can pick this person up?
				foreach (AffiliateCoverageZones oZone in oAffiliatesZones) {
					foreach (CoverageArea oArea in oDOAreas) {
						if (oZone.CoverageZoneID == oArea.CoverageZoneID && (bool)oZone.Affiliate.PickUpAnywhere) {
							oAffiliates.Add(oZone.Affiliate);
						}
					}
				}
			}

			if (oAffiliates.Count == 0) {
				return pPM.GetNullEntity<Affiliate>();
			}

			return oAffiliates[0];
		}

		// ************************************************************************
		// Get a single Affiliate
		// ************************************************************************
		public static Affiliate GetAffiliate(PersistenceManager pPM, long pAffiliateID) {
			return GetAffiliate(pPM, pAffiliateID, QueryStrategy.Normal);
		}


		// ************************************************************************
		// Get a single Affiliate
		// ************************************************************************
		public static Affiliate GetAffiliate(PersistenceManager pPM, long pAffiliateID, QueryStrategy pQueryStrategy) {
			EntityList<Affiliate> oAffiliates = GetData(pPM, pAffiliateID.ToString(), "", "");
			Affiliate oAffiliate;
			if (oAffiliates.Count == 0) {
				oAffiliate = pPM.GetNullEntity<Affiliate>();
			} else {
				oAffiliate = oAffiliates[0];
			}
			return oAffiliate;
		}


		// ************************************************************************
		// Get Hey, Taxi
		// ************************************************************************
		public static Affiliate GetTestAffiliate(PersistenceManager pPM) {
			EntityList<Affiliate> oAffiliates = GetData(pPM, "", "Hey, Taxi", "");
			Affiliate oAffiliate;
			if (oAffiliates.Count == 0) {
				oAffiliate = pPM.GetNullEntity<Affiliate>();
			} else {
				oAffiliate = oAffiliates[0];
			}
			return oAffiliate;
		}

		// ************************************************************************
		// Get all the Affiliates
		// ************************************************************************
		public static EntityList<Affiliate> GetAffiliates(PersistenceManager pPM) {
			return GetData(pPM, "", "", "");
		}

		// ************************************************************************
		// Get all the Affiliates
		// ************************************************************************
		public static EntityList<Affiliate> GetAffiliates(PersistenceManager pPM, QueryStrategy pQueryStrategy) {
			return GetData(pPM, "", "", "");
		}

		// ************************************************************************
		// Get all the Affiliates by Market
		// ************************************************************************
		public static EntityList<Affiliate> GetAffiliatesByMarket(PersistenceManager pPM, long pMarketID, QueryStrategy pQueryStrategy) {
			return GetData(pPM, "", "", pMarketID.ToString());
		}

		// ******************************************************************************
		// Call the proc
		// ******************************************************************************
		private static EntityList<Affiliate> GetData(PersistenceManager pPM, string pAffiliateID, string pName, string pMarketID) {
			string sDelimiter = "";
			int iArgCount = 0;
			StringBuilder sql = new StringBuilder();
			sql.Append("usp_AffiliatesGet ");
			if (!pAffiliateID.IsNullOrEmpty()) {
				sql.AppendFormat(" @AffiliateID = {0}", pAffiliateID);
				iArgCount++;
			}
			if (!pName.IsNullOrEmpty()) {
				if (iArgCount > 0) {
					sDelimiter = ", ";
				}
				sql.AppendFormat("{0} @Name = '{1}'", sDelimiter, pName);
				iArgCount++;
			}
			if (!pMarketID.IsNullOrEmpty()) {
				if (iArgCount > 0) {
					sDelimiter = ", ";
				}
				sql.AppendFormat("{0} @MarketID = {1}", sDelimiter, pMarketID);
				iArgCount++;
			}


			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(Affiliate), sql.ToString());
			EntityList<Affiliate> oAffiliates = pPM.GetEntities<Affiliate>(qry);
			return oAffiliates;
		}



		public static EntityList<Affiliate> GetTaxiTronicAffiliates(PersistenceManager pPM, QueryStrategy pQueryStrategy) {
			RdbQuery qry = new RdbQuery(typeof(AffiliateDriverDefaults), AffiliateDriverDefaults.TaxiTronicGPSEntityColumn, EntityQueryOp.EQ, true);
			EntityList<AffiliateDriverDefaults> oList = pPM.GetEntities<AffiliateDriverDefaults>(qry, pQueryStrategy);

			EntityList<Affiliate> affList = new EntityList<Affiliate>();
			foreach (AffiliateDriverDefaults oRec in oList) {
				if (!string.IsNullOrEmpty(oRec.TaxiTronicUserName) && !string.IsNullOrEmpty(oRec.TaxiTronicPassword)) {
					affList.Add(oRec.Affiliate);
				}
			}
			return affList;
		}



		public static EntityList<Affiliate> GetAffiliatesWithDrivers(PersistenceManager pPM) {
			StringBuilder sql = new StringBuilder();
			sql.Append("SELECT Affiliate.* FROM Affiliate with (nolock) WHERE ");
			sql.Append("(AffiliateID IN (SELECT DISTINCT AffiliateID FROM AffiliateDrivers)) ");
			sql.Append("Order By Name");
			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(Affiliate), sql.ToString());

			EntityList<Affiliate> list = new EntityList<Affiliate>();
			try {
				list = pPM.GetEntities<Affiliate>(qry);

			} catch (Exception ex) {
				System.Console.WriteLine(ex.Message);
			}


			return list;
		}

		public static Affiliate GetTaxiTronicAffiliate(PersistenceManager pPM, string pMerchant) {
			RdbQuery qry = new RdbQuery(typeof(AffiliateDriverDefaults), AffiliateDriverDefaults.TaxiTronicMerchantEntityColumn, EntityQueryOp.EQ, pMerchant);
			return pPM.GetEntity<AffiliateDriverDefaults>(qry, QueryStrategy.Normal).Affiliate;
		}

		public static EntityList<Affiliate> GetVTSDataImportFleets(PersistenceManager pPM) {
			RdbQuery qry = new RdbQuery(typeof(Affiliate), VTSDataImportEntityColumn, EntityQueryOp.EQ, true);
			return pPM.GetEntities<Affiliate>(qry);
		}

		public static EntityList<Affiliate> GetAffiliatesWithLicense(PersistenceManager pPM) {
			RdbQuery qry = new RdbQuery(typeof(Affiliate), Affiliate.LicenseNumberEntityColumn, EntityQueryOp.IsNotNull, true);
			qry.AddOrderBy(Affiliate.NameEntityColumn);
			return pPM.GetEntities<Affiliate>(qry);
		}

		public static EntityList<Affiliate> GetAutoACHEnabled(PersistenceManager pPM) {
			RdbQuery qry = new RdbQuery(typeof(Affiliate), Affiliate.EnableAutoACHEntityColumn, EntityQueryOp.EQ, true);
			//qry.AddClause(Affiliate.SendNachaEntityColumn, EntityQueryOp.EQ, pNachaFile);
			qry.AddOrderBy(Affiliate.NameEntityColumn);
			return pPM.GetEntities<Affiliate>(qry);
		}

		public static EntityList<Affiliate> GetSubFleets(Affiliate pAffiliate) {
			RdbQuery qry = new RdbQuery(typeof(Affiliate), Affiliate.ParentAffiliateIDEntityColumn, EntityQueryOp.EQ, pAffiliate.AffiliateID);
			return pAffiliate.PersistenceManager.GetEntities<Affiliate>(qry);
		}

		public static EntityList<Affiliate> GetDispatchEnabled(PersistenceManager pPM) {
			StringBuilder sql = new StringBuilder();
			sql.Append("SELECT Affiliate.* FROM Affiliate ");
			sql.Append("LEFT JOIN AffiliateDriverDefaults ON AffiliateDriverDefaults.affiliateid = affiliate.affiliateid ");
			sql.Append("WHERE dispatchjobs = 1");

			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(Affiliate), sql.ToString());
			return pPM.GetEntities<Affiliate>(qry);
		}


		public DateTime LastAccessed {
			get {
				DateTime lastDate = new DateTime();
				foreach (AffiliateContacts oContact in this.AffiliateContactses) {
					if (oContact.LastAccess.HasValue && oContact.LastAccess.Value > lastDate) {
						lastDate = oContact.LastAccess.Value;
					}
				}
				return lastDate;
			}
		}

		public string Market {
			get {
				string sMarket = "";
				foreach (AffiliateCoverageZones oZone in this.AffiliateCoverageZoneses) {
					if (!sMarket.Contains(oZone.CoverageZones.Markets.Market)) {
						sMarket = oZone.CoverageZones.Markets.Market + ", ";
					}
				}
				if (string.IsNullOrEmpty(sMarket)) {
					return "";
				}
				return sMarket.Substring(0, sMarket.Length - 2);
			}
		}

		public static EntityList<Affiliate> GetAffiliatesByLastAccess(PersistenceManager pPM, bool pWebEnabledOnly) {
			EntityList<Affiliate> oAffiliates = new EntityList<Affiliate>();
			if (pWebEnabledOnly) {
				RdbQuery qry = new RdbQuery(typeof(Affiliate), Affiliate.WebEnabledEntityColumn, EntityQueryOp.EQ, true);
				oAffiliates = pPM.GetEntities<Affiliate>(qry, QueryStrategy.DataSourceThenCache);
			} else {
				oAffiliates = pPM.GetEntities<Affiliate>(QueryStrategy.DataSourceThenCache);
			}
			oAffiliates.ApplySort("LastAccessed", System.ComponentModel.ListSortDirection.Ascending, false);
			return oAffiliates;
		}


		public VerisignAccounts MyVerisignAccount {
			get {
				if (this.VerisignAccountID.HasValue) {
					VerisignAccounts acct = VerisignAccounts.GetAccount(this.PersistenceManager, this.VerisignAccountID.Value);
					if (acct.IsNullEntity) {
						return VerisignAccounts.GetDefaultAccount(this.PersistenceManager);
					}
					return VerisignAccounts.GetAccount(this.PersistenceManager, this.VerisignAccountID.Value);
				}
				return VerisignAccounts.GetDefaultAccount(this.PersistenceManager);
			}
		}


		public CabRideEngineDapper.CardGatewayGroups MyCardGatewayGroup {
			get {
				if (this.VerisignAccountID.HasValue) {
					CabRideEngineDapper.CardGatewayGroups acct = CabRideEngineDapper.CardGatewayGroups.GetGroup(this.VerisignAccountID.Value);
					if (acct.IsNullEntity) {
						return CabRideEngineDapper.CardGatewayGroups.GetDefaultAccount();
					}
					return acct;
				}
				return CabRideEngineDapper.CardGatewayGroups.GetDefaultAccount();
			}
		}

		public string LimoTaxiPassText {
			get {
				string sLimoTaxiPassText = "TaxiPass";
				if (!DefaultPlatform.IsNullOrEmpty()) {
					sLimoTaxiPassText = Platforms.ChargePass.ToString();
				} else {
					if (IsLimo()) {
						sLimoTaxiPassText = "LimoPass";
					}
				}
				return sLimoTaxiPassText;
			}
		}

		public bool IsLimo() {
			if (string.IsNullOrEmpty(this.ServiceType)) {
				return false;
			}
			return this.ServiceType.Equals("limo", StringComparison.CurrentCultureIgnoreCase);
		}

		public bool IsTaxiTronicGPS() {
			bool bTaxiTronicGPS = false;
			if (!this.AffiliateDriverDefaults.IsNullEntity) {
				bTaxiTronicGPS = this.AffiliateDriverDefaults.TaxiTronicGPS;
			}
			return bTaxiTronicGPS;
		}

		public bool IsTaxiTronicAffiliate() {
			bool bTaxiTronic = false;
			if (!this.AffiliateDriverDefaults.IsNullEntity) {
				bTaxiTronic = (!string.IsNullOrEmpty(this.AffiliateDriverDefaults.TaxiTronicMerchant) || this.TaxiTronicAffiliateID.HasValue);
			}
			return bTaxiTronic;
		}


		public Gateway GetGateway(TaxiPassCommon.Banking.CardProcessors pProcessor, CreditCardChargeType pType, string pCardType) {
			List<IGatewayCredentials> acctList = Utils.CreditCardUtils.LoadGateWays(PersistenceManager, VerisignAccountID.Value, pType, pCardType);
			if (acctList.Count == 1) {
				return new Gateway(acctList[0]);
			}
			return new Gateway(acctList.FirstOrDefault(p => p.MyGateway() == pProcessor));
		}

		public List<Gateway> GetGateways(CreditCardChargeType pType, string pCardType) {
			List<IGatewayCredentials> acctList = Utils.CreditCardUtils.LoadGateWays(PersistenceManager, VerisignAccountID.Value, pType, pCardType);
			List<Gateway> gateWayList = new List<Gateway>();

			foreach (IGatewayCredentials rec in acctList) {
				gateWayList.Add(new Gateway(rec));
			}

			return gateWayList;
		}


		private AffiliateRef mNewDriverVoucherCount;
		public int NewDriverVoucherCount {
			get {
				CreateNewDriverVoucherCount();
				return Convert.ToInt32(mNewDriverVoucherCount.ValueString);
			}
			set {
				CreateNewDriverVoucherCount();
				mNewDriverVoucherCount.ValueString = value.ToString();
			}
		}

		private void CreateNewDriverVoucherCount() {
			if (mNewDriverVoucherCount == null) {
				mNewDriverVoucherCount = AffiliateRef.GetCreate(this, "FreezeDriver", "NewDriverVoucherCount", "0");
			}
		}

		private AffiliateRef mDriverFreezeVoucherCount;
		public int DriverFreezeVoucherCount {
			get {
				CreateDriverFreezeVoucherCount();
				return Convert.ToInt32(mDriverFreezeVoucherCount.ValueString);
			}
			set {
				CreateDriverFreezeVoucherCount();
				mDriverFreezeVoucherCount.ValueString = value.ToString();
			}
		}

		private void CreateDriverFreezeVoucherCount() {
			if (mDriverFreezeVoucherCount == null) {
				mDriverFreezeVoucherCount = AffiliateRef.GetCreate(this, "FreezeDriver", "VoucherCount", "0");
			}
		}

		private AffiliateRef mDriverFreezeAverageVoucherAmount;
		public decimal DriverFreezeAverageVoucherAmount {
			get {
				CreateDriverFreezeAverageVoucherAmount();
				return Convert.ToDecimal(mDriverFreezeAverageVoucherAmount.ValueString);
			}
			set {
				CreateDriverFreezeAverageVoucherAmount();
				mDriverFreezeAverageVoucherAmount.ValueString = value.ToString();
			}
		}

		private void CreateDriverFreezeAverageVoucherAmount() {
			if (mDriverFreezeAverageVoucherAmount == null) {
				mDriverFreezeAverageVoucherAmount = AffiliateRef.GetCreate(this, "FreezeDriver", "AverageVoucherAmount", "0");
			}
		}


		//
		private AffiliateRef mEnablePayCardInfo;
		public bool EnablePayCardInfo {
			get {
				CreateEnablePayCardInfo();
				return Convert.ToBoolean(mEnablePayCardInfo.ValueString);
			}
			set {
				CreateEnablePayCardInfo();
				mEnablePayCardInfo.ValueString = value.ToString();
			}
		}

		private void CreateEnablePayCardInfo() {
			if (mEnablePayCardInfo == null) {
				mEnablePayCardInfo = AffiliateRef.GetCreate(this, "WebSite", "EnablePayCardInfo", "False");
			}
		}
		//

		private AffiliateRef mGetRideSignaturePromo;
		public bool GetRideSignaturePromo {
			get {
				CreateGetRideSignaturePromo();
				return Convert.ToBoolean(mGetRideSignaturePromo.ValueString);
			}
			set {
				CreateGetRideSignaturePromo();
				mGetRideSignaturePromo.ValueString = value.ToString();
			}
		}

		private void CreateGetRideSignaturePromo() {
			if (mGetRideSignaturePromo == null) {
				mGetRideSignaturePromo = AffiliateRef.GetCreate(this, "GetRide", "SignaturePromo", false.ToString());
			}
		}


		private AffiliateRef mGetRideReceiptPromo;
		public bool GetRideReceiptPromo {
			get {
				CreateGetRideReceiptPromo();
				return Convert.ToBoolean(mGetRideReceiptPromo.ValueString);
			}
			set {
				CreateGetRideReceiptPromo();
				mGetRideReceiptPromo.ValueString = value.ToString();
			}
		}

		private void CreateGetRideReceiptPromo() {
			if (mGetRideReceiptPromo == null) {
				mGetRideReceiptPromo = AffiliateRef.GetCreate(this, "GetRide", "ReceiptPromo", false.ToString());
			}
		}

		private AffiliateRef mGetRideExtraFees;
		public bool GetRideExtraFees {
			get {
				CreateGetRideExtraFees();
				return Convert.ToBoolean(mGetRideExtraFees.ValueString);
			}
			set {
				CreateGetRideExtraFees();
				mGetRideExtraFees.ValueString = value.ToString();
			}
		}

		private void CreateGetRideExtraFees() {
			if (mGetRideExtraFees == null) {
				mGetRideExtraFees = AffiliateRef.GetCreate(this, "GetRide", "ExtraFees", false.ToString());
			}
		}

		private AffiliateRef mSmartPhoneFeeAmt;
		public decimal SmartPhoneFeeAmt {
			get {
				CreateSmartPhoneFeeAmt();
				return Convert.ToDecimal(mSmartPhoneFeeAmt.ValueString);
			}
			set {
				CreateSmartPhoneFeeAmt();
				mSmartPhoneFeeAmt.ValueString = value.ToString();
			}
		}

		private void CreateSmartPhoneFeeAmt() {
			if (mSmartPhoneFeeAmt == null) {
				mSmartPhoneFeeAmt = AffiliateRef.GetCreate(this, "GetRide", "SmartPhoneFeeAmt", "0");
			}
		}

		private AffiliateRef mSmartPhoneFeeAmtPerRate;
		public decimal SmartPhoneFeeAmtPerRate {
			get {
				CreateSmartPhoneFeeAmtPerRate();
				return Convert.ToDecimal(mSmartPhoneFeeAmtPerRate.ValueString);
			}
			set {
				CreateSmartPhoneFeeAmtPerRate();
				mSmartPhoneFeeAmtPerRate.ValueString = value.ToString();
			}
		}

		private void CreateSmartPhoneFeeAmtPerRate() {
			if (mSmartPhoneFeeAmtPerRate == null) {
				mSmartPhoneFeeAmtPerRate = AffiliateRef.GetCreate(this, "GetRide", "SmartPhoneFeeAmtPerRate", "0");
			}
		}

		private AffiliateRef mSmartPhoneFeeMaxAmt;
		public decimal SmartPhoneFeeMaxAmt {
			get {
				CreateSmartPhoneFeeMaxAmt();
				return Convert.ToDecimal(mSmartPhoneFeeMaxAmt.ValueString);
			}
			set {
				CreateSmartPhoneFeeMaxAmt();
				mSmartPhoneFeeMaxAmt.ValueString = value.ToString();
			}
		}

		private void CreateSmartPhoneFeeMaxAmt() {
			if (mSmartPhoneFeeMaxAmt == null) {
				mSmartPhoneFeeMaxAmt = AffiliateRef.GetCreate(this, "GetRide", "SmartPhoneFeeMaxAmt", "0");
			}
		}


		private AffiliateRef mBankInfoOnWebSite;
		public bool BankInfoOnWebSite {
			get {
				CreateBankInfoOnWebSite();
				return Convert.ToBoolean(mBankInfoOnWebSite.ValueString);
			}
			set {
				CreateBankInfoOnWebSite();
				mBankInfoOnWebSite.ValueString = value.ToString();
			}
		}

		private void CreateBankInfoOnWebSite() {
			if (mBankInfoOnWebSite == null) {
				mBankInfoOnWebSite = AffiliateRef.GetCreate(this, "Affiliate", "BankInfoOnWebSite", false.ToString());
			}
		}

		private AffiliateRef mBillingDescriptor;
		public string BillingDescriptor {
			get {
				CreateBillingDescriptor();
				return mBillingDescriptor.ValueString;
			}
			set {
				CreateBillingDescriptor();
				mBillingDescriptor.ValueString = value;
			}
		}

		private void CreateBillingDescriptor() {
			if (mBillingDescriptor == null) {
				mBillingDescriptor = AffiliateRef.GetCreate(this, "Affiliate", "BillingDescriptor", "");
			}
		}


		private AffiliateRef mDisplayReadOnlyHideDontPayRefundFlags;
		public bool DisplayReadOnlyHideDontPayRefundFlags {
			get {
				CreateDisplayReadOnlyHideDontPayRefundFlags();
				return Convert.ToBoolean(mDisplayReadOnlyHideDontPayRefundFlags.ValueString);
			}
			set {
				CreateDisplayReadOnlyHideDontPayRefundFlags();
				mDisplayReadOnlyHideDontPayRefundFlags.ValueString = value.ToString();
			}
		}

		private void CreateDisplayReadOnlyHideDontPayRefundFlags() {
			if (mDisplayReadOnlyHideDontPayRefundFlags == null) {
				mDisplayReadOnlyHideDontPayRefundFlags = AffiliateRef.GetCreate(this, "Affiliate", "DisplayReadOnlyHideDontPayRefundFlags", false.ToString());
			}
		}

		private AffiliateRef mFundPayCardDelayed;
		public bool FundPayCardDelayed {
			get {
				CreateFundPayCardDelayed();
				return Convert.ToBoolean(mFundPayCardDelayed.ValueString);
			}
			set {
				CreateFundPayCardDelayed();
				mFundPayCardDelayed.ValueString = value.ToString();
			}
		}

		private void CreateFundPayCardDelayed() {
			if (mFundPayCardDelayed == null) {
				mFundPayCardDelayed = AffiliateRef.GetCreate(this, "Affiliate", "FundPayCardDelayed", false.ToString());
			}
		}



		private AffiliateRef mFundPayCardDelayedMinusHours;
		public int FundPayCardDelayedMinusHours {
			get {
				CreateFundPayCardDelayedMinusHours();
				return Convert.ToInt32(mFundPayCardDelayedMinusHours.ValueString);
			}
			set {
				CreateFundPayCardDelayedMinusHours();
				mFundPayCardDelayedMinusHours.ValueString = value.ToString();
			}
		}

		private void CreateFundPayCardDelayedMinusHours() {
			if (mFundPayCardDelayedMinusHours == null) {
				mFundPayCardDelayedMinusHours = AffiliateRef.GetCreate(this, "Affiliate", "FundPayCardDelayedMinusHours", 0.ToString());
			}
		}


		private AffiliateRef mDoNotTextDrivers;
		public bool DoNotTextDrivers {
			get {
				CreateDoNotTextDrivers();
				return Convert.ToBoolean(mDoNotTextDrivers.ValueString);
			}
			set {
				CreateDoNotTextDrivers();
				mDoNotTextDrivers.ValueString = value.ToString();
			}
		}

		private void CreateDoNotTextDrivers() {
			if (mDoNotTextDrivers == null) {
				mDoNotTextDrivers = AffiliateRef.GetCreate(this, "Affiliate", "DoNotTextDrivers", false.ToString());
			}
		}

		private AffiliateRef mAffiliateLogo;
		public string AffiliateLogo {
			get {
				CreateAffiliateLogo();
				return mAffiliateLogo.ValueString;
			}
			set {
				CreateAffiliateLogo();
				mAffiliateLogo.ValueString = value;
			}
		}

		private void CreateAffiliateLogo() {
			if (mAffiliateLogo == null) {
				mAffiliateLogo = AffiliateRef.GetCreate(this, "Affiliate", "Logo", "");
			}
		}


		private AffiliateRef mDejavooMerchantID;
		public string DejavooMerchantID {
			get {
				CreateDejavooMerchantID();
				return mDejavooMerchantID.ValueString;
			}
			set {
				CreateDejavooMerchantID();
				mDejavooMerchantID.ValueString = value;
			}
		}

		private void CreateDejavooMerchantID() {
			if (mDejavooMerchantID == null) {
				mDejavooMerchantID = AffiliateRef.GetCreate(this, "Dejavoo", "MerchantID", "");
			}
		}

		private AffiliateRef mDejavooAmexMerchantID;
		public string DejavooAmexMerchantID {
			get {
				CreateDejavooAmexMerchantID();
				return mDejavooAmexMerchantID.ValueString;
			}
			set {
				CreateDejavooAmexMerchantID();
				mDejavooAmexMerchantID.ValueString = value;
			}
		}

		private void CreateDejavooAmexMerchantID() {
			if (mDejavooAmexMerchantID == null) {
				mDejavooAmexMerchantID = AffiliateRef.GetCreate(this, "Dejavoo", "AmexMerchantID", "");
			}
		}

		private AffiliateRef mDejavooMerchantInfo;
		public string DejavooMerchantInfo {
			get {
				CreateDejavooMerchantInfo();
				return mDejavooMerchantInfo.ValueString;
			}
			set {
				CreateDejavooMerchantInfo();
				mDejavooMerchantInfo.ValueString = value;
			}
		}

		private void CreateDejavooMerchantInfo() {
			if (mDejavooMerchantInfo == null) {
				mDejavooMerchantInfo = AffiliateRef.GetCreate(this, "Dejavoo", "MerchantInfo", "");
			}
		}

		private AffiliateRef mReceiptTemplate;
		public string ReceiptTemplate {
			get {
				CreateReceiptTemplate();
				return mReceiptTemplate.ValueString;
			}
			set {
				CreateReceiptTemplate();
				mReceiptTemplate.ValueString = value;
			}
		}

		private void CreateReceiptTemplate() {
			if (mReceiptTemplate == null) {
				mReceiptTemplate = AffiliateRef.GetCreate(this, "ChargePass", "ReceiptTemplate", "");
			}
		}


		private AffiliateRef mDoNotPay;
		public bool DoNotPay {
			get {
				CreateDoNotPay();
				return mDoNotPay.ValueString.AsBool();
			}
			set {
				CreateDoNotPay();
				mDoNotPay.ValueString = value.ToString();
			}
		}

		private void CreateDoNotPay() {
			if (mDoNotPay == null) {
				mDoNotPay = AffiliateRef.GetCreate(this, "Affiliate", "DoNotPay", false.ToString());
			}
		}

		private AffiliateRef mSoftwareLicensing;
		public bool SoftwareLicensing {
			get {
				CreateSoftwareLicensing();
				return mSoftwareLicensing.ValueString.AsBool();
			}
			set {
				CreateSoftwareLicensing();
				mSoftwareLicensing.ValueString = value.ToString();
			}
		}

		private void CreateSoftwareLicensing() {
			if (mSoftwareLicensing == null) {
				mSoftwareLicensing = AffiliateRef.GetCreate(this, "Affiliate", "SoftwareLicensing", false.ToString());
			}
		}


		private AffiliateRef mReceiptPrinterName;
		public string ReceiptPrinterName {
			get {
				CreateReceiptPrinterName();
				return mReceiptPrinterName.ValueString;
			}
			set {
				CreateReceiptPrinterName();
				mReceiptPrinterName.ValueString = value;
			}
		}

		private void CreateReceiptPrinterName() {
			if (mReceiptPrinterName == null) {
				mReceiptPrinterName = AffiliateRef.GetCreate(this, "Affiliate", "ReceiptPrinterName", "");
			}
		}

		private AffiliateRef mReceiptFooterText;
		public string ReceiptFooterText {
			get {
				CreateReceiptFooterText();
				return mReceiptFooterText.ValueString;
			}
			set {
				CreateReceiptFooterText();
				mReceiptFooterText.ValueString = value;
			}
		}

		private void CreateReceiptFooterText() {
			if (mReceiptFooterText == null) {
				mReceiptFooterText = AffiliateRef.GetCreate(this, "Affiliate", "ReceiptFooterText", "");
			}
		}

		private AffiliateRef mECheckReceiptFooterText;
		public string ECheckReceiptFooterText {
			get {
				CreateECheckReceiptFooterText();
				return mECheckReceiptFooterText.ValueString;
			}
			set {
				CreateECheckReceiptFooterText();
				mECheckReceiptFooterText.ValueString = value;
			}
		}

		private void CreateECheckReceiptFooterText() {
			if (mECheckReceiptFooterText == null) {
				mECheckReceiptFooterText = AffiliateRef.GetCreate(this, "Affiliate", "ECheckReceiptFooterText", "");
			}
		}

		private AffiliateRef mRecurringBilling;
		public bool RecurringBilling {
			get {
				CreateRecurringBilling();
				return Convert.ToBoolean(mRecurringBilling.ValueString);
			}
			set {
				CreateRecurringBilling();
				mRecurringBilling.ValueString = value.ToString();
			}
		}

		private void CreateRecurringBilling() {
			if (mRecurringBilling == null) {
				mRecurringBilling = AffiliateRef.GetCreate(this, "Affiliate", "RecurringBilling", false.ToString());
			}
		}

		private AffiliateRef mManualTransFee;
		public decimal ManualTransFee {
			get {
				CreateManualTransFee();
				return mManualTransFee.ValueString.AsDecimal();
			}
			set {
				CreateManualTransFee();
				mManualTransFee.ValueString = value.ToString();
			}
		}

		private void CreateManualTransFee() {
			if (mManualTransFee == null) {
				mManualTransFee = AffiliateRef.GetCreate(this, "Affiliate", "ManualTransFee", "0.00");

			}
		}

		private AffiliateRef mChargeBackFee;
		public decimal ChargeBackFee {
			get {
				CreateChargeBackFee();
				return mChargeBackFee.ValueString.AsDecimal();
			}
			set {
				CreateChargeBackFee();
				mChargeBackFee.ValueString = value.ToString();
			}
		}

		private void CreateChargeBackFee() {
			if (mChargeBackFee == null) {
				mChargeBackFee = AffiliateRef.GetCreate(this, "Affiliate", "ChargeBackFee", "0");
			}
		}

		private AffiliateRef mHHShowTolls;
		public bool HHShowTolls {
			get {
				CreateHHShowTolls();
				return mHHShowTolls.ValueString.AsBool();
			}
			set {
				CreateHHShowTolls();
				mHHShowTolls.ValueString = value.ToString();
			}
		}

		private void CreateHHShowTolls() {
			if (mHHShowTolls == null) {
				mHHShowTolls = AffiliateRef.GetCreate(this, "AffiliateDriverDefaults", "HHShowTolls", false.ToString());
			}
		}

		private AffiliateRef mDiscountRate;
		public decimal DiscountRate {
			get {
				CreateDiscountRate();
				return mDiscountRate.ValueString.AsDecimal();
			}
			set {
				CreateDiscountRate();
				mDiscountRate.ValueString = value.ToString();
			}
		}

		private void CreateDiscountRate() {
			if (mDiscountRate == null) {
				mDiscountRate = AffiliateRef.GetCreate(this, "Affiliate", "DiscountRate", "0");
			}
		}

		private AffiliateRef mCustomServiceFeeText;
		public string CustomServiceFeeText {
			get {
				CreateCustomServiceFeeText();
				return mCustomServiceFeeText.ValueString;
			}
			set {
				CreateCustomServiceFeeText();
				mCustomServiceFeeText.ValueString = value;
			}
		}

		private void CreateCustomServiceFeeText() {
			if (mCustomServiceFeeText == null) {
				mCustomServiceFeeText = AffiliateRef.GetCreate(this, "Affiliate", "CustomServiceFeeText", "");
			}
		}


		private AffiliateRef mInvoicing;
		public bool Invoicing {
			get {
				CreateInvoicing();
				return mInvoicing.ValueString.ToBool();
			}
			set {
				CreateInvoicing();
				mInvoicing.ValueString = value.ToString();
			}
		}

		private void CreateInvoicing() {
			if (mInvoicing == null) {
				mInvoicing = AffiliateRef.GetCreate(this, "Affiliate", "Invoicing", false.ToString());
			}
		}


		private AffiliateRef mInvoiceAllowPartialPayments;
		public bool InvoiceAllowPartialPayments {
			get {
				CreateInvoiceAllowPartialPayments();
				return mInvoiceAllowPartialPayments.ValueString.ToBool();
			}
			set {
				CreateInvoiceAllowPartialPayments();
				mInvoiceAllowPartialPayments.ValueString = value.ToString();
			}
		}

		private void CreateInvoiceAllowPartialPayments() {
			if (mInvoiceAllowPartialPayments == null) {
				mInvoiceAllowPartialPayments = AffiliateRef.GetCreate(this, "Affiliate", "InvoiceAllowPartialPayments", false.ToString());
			}
		}


		private AffiliateRef mInvoiceAllowRecurringPayments;
		public bool InvoiceAllowRecurringPayments {
			get {
				CreateInvoiceAllowRecurringPayments();
				return mInvoiceAllowRecurringPayments.ValueString.ToBool();
			}
			set {
				CreateInvoiceAllowRecurringPayments();
				mInvoiceAllowRecurringPayments.ValueString = value.ToString();
			}
		}

		private void CreateInvoiceAllowRecurringPayments() {
			if (mInvoiceAllowRecurringPayments == null) {
				mInvoiceAllowRecurringPayments = AffiliateRef.GetCreate(this, "Affiliate", "InvoiceAllowRecurringPayments", false.ToString());
			}
		}


		// Bill page title block 
		private AffiliateRef mBillPaymentPageTitle;
		public string BillPaymentPageTitle {
			get {
				CreateBillPaymentPageTitle();
				return mBillPaymentPageTitle.ValueString;
			}
			set {
				CreateBillPaymentPageTitle();
				mBillPaymentPageTitle.ValueString = value;
			}
		}

		private void CreateBillPaymentPageTitle() {
			if (mBillPaymentPageTitle == null) {
				mBillPaymentPageTitle = AffiliateRef.GetCreate(this, "Affiliate", "BillPaymentPageTitle", " ");
			}
		}

		// Bill page sub title block 
		private AffiliateRef mBillPaymentPageSubTitle;
		public string BillPaymentPageSubTitle {
			get {
				CreateBillPaymentPageSubTitle();
				return mBillPaymentPageSubTitle.ValueString;
			}
			set {
				CreateBillPaymentPageSubTitle();
				mBillPaymentPageSubTitle.ValueString = value;
			}
		}

		private void CreateBillPaymentPageSubTitle() {
			if (mBillPaymentPageSubTitle == null) {
				mBillPaymentPageSubTitle = AffiliateRef.GetCreate(this, "Affiliate", "BillPaymentPageSubTitle", " ");
			}
		}


		private AffiliateRef mEChecksOnly;
		public bool EChecksOnly {
			get {
				CreateEChecksOnly();
				return mEChecksOnly.ValueString.AsBool();
			}
			set {
				CreateEChecksOnly();
				mEChecksOnly.ValueString = value.ToString();
			}
		}

		private void CreateEChecksOnly() {
			if (mEChecksOnly == null) {
				mEChecksOnly = AffiliateRef.GetCreate(this, "Affiliate", "EChecksOnly", false.ToString());
			}
		}

		private AffiliateRef mBillPayRequireApproval;
		public bool BillPayRequirePaymentApproval {
			get {
				CreateBillPayRequireApproval();
				return mBillPayRequireApproval.ValueString.AsBool();
			}
			set {
				CreateBillPayRequireApproval();
				mBillPayRequireApproval.ValueString = value.ToString();
			}
		}

		private void CreateBillPayRequireApproval() {
			if (mBillPayRequireApproval == null) {
				mBillPayRequireApproval = AffiliateRef.GetCreate(this, "Affiliate", "BillPayRequireApproval", false.ToString());
			}
		}

		//private AffiliateRef mEnableECheck;
		//public bool EnableECheck {
		//    get {
		//        CreateEnableECheck();
		//        return mEnableECheck.ValueString.AsBool();
		//    }
		//    set {
		//        CreateEnableECheck();
		//        mEnableECheck.ValueString = value.ToString();
		//    }
		//}

		//private void CreateEnableECheck() {
		//    if (mEnableECheck == null) {
		//        mEnableECheck = AffiliateRef.GetCreate(this, "Affiliate", "EnableECheck", false.ToString());
		//    }
		//}

		//private AffiliateRef mCardGatewayGroupsID;
		//public long CardGatewayGroupsID {
		//    get {
		//        CreateCardGatewayGroupsID();
		//        return mCardGatewayGroupsID.ValueString.AsLong();
		//    }
		//    set {
		//        CreateCardGatewayGroupsID();
		//        mCardGatewayGroupsID.ValueString = value.ToString();
		//    }
		//}

		//private void CreateCardGatewayGroupsID() {
		//    if (mCardGatewayGroupsID == null) {
		//        mCardGatewayGroupsID = AffiliateRef.GetCreate(this, "Affiliate", "CardGatewayGroupsID", "0");
		//    }
		//}

		//private AffiliateRef mSmartPhonePreAuthOnly;
		//public bool SmartPhonePreAuthOnly {
		//	get {
		//		CreateSmartPhonePreAuthOnly();
		//		return Convert.ToBoolean(mSmartPhonePreAuthOnly.ValueString);
		//	}
		//	set {
		//		CreateSmartPhonePreAuthOnly();
		//		mSmartPhoneFeeAmt.ValueString = value.ToString();
		//	}
		//}

		//private void CreateSmartPhonePreAuthOnly() {
		//	if (mSmartPhonePreAuthOnly == null) {
		//		mSmartPhonePreAuthOnly = AffiliateRef.GetCreate(this, "GetRide", "SmartPhonePreAuthOnly", false.ToString());
		//	}
		//}


		//private AffiliateRef mKioskVoucherStopPaymentAge;
		//public int KioskVoucherStopPaymentAge {
		//	get {
		//		if (mKioskVoucherStopPaymentAge == null) {
		//			mKioskVoucherStopPaymentAge = AffiliateRef.GetCreate(this, "KioskVoucher", "StopPaymentAge", "0");
		//		}
		//		return Convert.ToInt32(mKioskVoucherStopPaymentAge.ValueString);
		//	}
		//	set {
		//		if (mKioskVoucherStopPaymentAge == null) {
		//			mKioskVoucherStopPaymentAge = AffiliateRef.GetCreate(this, "KioskVoucher", "StopPaymentAge", "0");
		//		}
		//		mKioskVoucherStopPaymentAge.ValueString = value.ToString();
		//	}
		//}

		//private AffiliateRef mKioskVoucherManagementApprovalAge;
		//public int KioskVoucherManagementApprovalAge {
		//	get {
		//		if (mKioskVoucherManagementApprovalAge == null) {
		//			mKioskVoucherManagementApprovalAge = AffiliateRef.GetCreate(this, "KioskVoucher", "ManagementApprovalAge", "0");
		//		}
		//		return Convert.ToInt32(mKioskVoucherManagementApprovalAge.ValueString);
		//	}
		//	set {
		//		if (mKioskVoucherManagementApprovalAge == null) {
		//			mKioskVoucherManagementApprovalAge = AffiliateRef.GetCreate(this, "KioskVoucher", "ManagementApprovalAge", "0");
		//		}
		//		mKioskVoucherManagementApprovalAge.ValueString = value.ToString();
		//	}
		//}


	}

}
