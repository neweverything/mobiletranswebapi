﻿using CabRideEngineDapper.Utils;
using Dapper;
using System;

namespace CabRideEngineDapper {
    public partial class ReservationRef {


        public static ReservationRef Create(Reservations pReserve, string pTableName) {
            ReservationRef rec = Create();
            rec.ReservationID = pReserve.ReservationID;
            rec.TableName = pTableName;
            return rec;
        }

        // **********************************************************************************************************
        // Get or Create a record
        // **********************************************************************************************************
        public static ReservationRef GetCreate(Reservations pReserve, string pTableName) {
            ReservationRef rec = GetByTableName(pReserve, pTableName);
            if (rec.IsNullEntity) {
                try {
                    rec = Create(pReserve, pTableName);
                } catch (Exception ex) {
                    throw ex;
                }
            }
            return rec;
        }

        public static ReservationRef GetByTableName(Reservations pReserve, string pTableName) {
            string sql = @"SELECT  *
                            FROM    ReservationRef
                            WHERE   TableName = 'DriverPayments'
                                    AND ReservationID = @ReservationID";

            using (var conn = SqlHelper.OpenSqlConnection()) {
                return conn.Get<ReservationRef>(sql, new { ReservationID = pReserve.ReservationID });
            }
        }

        public static ReservationRef GetResByDriverPaymentID(long pDriverPaymentID) {
            string sql = @"SELECT  *
                            FROM    ReservationRef
                            WHERE   TableName = 'DriverPayments'
                                    AND TableID = @TableID";

            using (var conn = SqlHelper.OpenSqlConnection()) {
                return conn.Get<ReservationRef>(sql, new { TableID = pDriverPaymentID });
            }
        }


    }
}