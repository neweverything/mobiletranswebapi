﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileTransWebAPI.Models {
	public class TwilioResponse {
		public string ErrorMsg { get; set; }

		public string CapabilityToken { get; set; }

	}
}