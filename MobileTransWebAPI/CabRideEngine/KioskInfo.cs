﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

namespace CabRideEngine {
	/// <summary>
	/// The developer class representing the KioskInfo business object class.
	/// </summary>
	/// <remarks>
	/// <para>Place all application-specific business logic in this class.</para>
	/// <para>
	/// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
	/// and will not overwrite changes.
	/// </para>
	/// <para>
	/// This class inherits from the generated class, its companion in the pair of classes 
	/// defining a business object. Properties in the generated class may be overridden in 
	/// order to change the behavior or add business logic.
	/// </para>
	/// <para>
	/// This class is the final class in the inheritance chain for this business object. 
	/// It should not be inherited.
	/// </para>
	/// </remarks>
	[Serializable]
	public sealed partial class KioskInfo : KioskInfoDataRow {

		#region Constructors -- DO NOT MODIFY

		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new';
		// it is also required if the class is visible from a web service.
		// DO NOT REMOVE **
		private KioskInfo() : this(null) { }

		/// <summary>
		/// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
		/// </summary>
		/// <param name="pRowBuilder"></param>
		public KioskInfo(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static KioskInfo Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      KioskInfo aKioskInfo = pManager.CreateEntity<KioskInfo>();
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aKioskInfo, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aKioskInfo.AddToManager();
		//      return aKioskInfo;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		// Add additional logic to your business object here...
		public static KioskInfo GetOrCreate(PersistenceManager pPM, string pComputerName) {

			KioskInfo rec = GetKiosk(pPM, pComputerName);
			if (rec.IsNullEntity) {
				rec = pPM.CreateEntity<KioskInfo>();
				pPM.GenerateId(rec, KioskInfo.KioskInfoIDEntityColumn);

				rec.ComputerName = pComputerName;
				rec.AffiliateDriverID = -1;
				rec.AffiliateID = -1;
				rec.DriverID = -1;
				rec.WindowControls = true;
				rec.InsertCardDuration = 15;
				rec.MSRUsesSerialPort = true;
				rec.CancelledDuration = 5;
				rec.FeeDuration = 15;
				rec.KeyBoardEmulator = false;
				rec.MSRSerialPort = "COM1";
				rec.MSRVendorID = 2049;
				rec.MSRProductID = 3;
				rec.PrinterPort = "COM2";
				rec.IgnorePrinter = false;
				rec.MSRSerialPortDTREnable = false;
				rec.ThermalPrinter = false;
				rec.TestCard = false;
				rec.RestartApp = false;
				rec.AddToManager();
				rec.Save();
			}

			return rec;
		}

		public static KioskInfo GetKiosk(PersistenceManager pPM, string pComputerName) {
			RdbQuery qry = new RdbQuery(typeof(KioskInfo), ComputerNameEntityColumn, EntityQueryOp.EQ, pComputerName);
			return pPM.GetEntity<KioskInfo>(qry);
		}

		public static EntityList<KioskInfo> GetAll(PersistenceManager pPM) {
			return GetAll(pPM, pPM.DefaultQueryStrategy);
		}

		public static EntityList<KioskInfo> GetAll(PersistenceManager pPM, QueryStrategy pQueryStrategy) {
			RdbQuery qry = new RdbQuery(typeof(KioskInfo));
			qry.AddOrderBy(ComputerNameEntityColumn, System.ComponentModel.ListSortDirection.Ascending);
			return pPM.GetEntities<KioskInfo>(qry, pQueryStrategy);
		}

	}

	#region EntityPropertyDescriptors.KioskInfoPropertyDescriptor
	////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
	////    For example: 
	////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
	//public static partial class EntityPropertyDescriptors {
	//	public partial class KioskInfoPropertyDescriptor : BaseEntityPropertyDescriptor {
	//    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
	//    public AdaptedPropertyDescriptor Age {
	//      get { return this.Get("Age"); }
	//    }
	//  }
	//} 
	#endregion

}