using System;
using System.Collections.Generic;
using System.Text;

namespace CabRideEngine {
    public static class RideStatusTypes {
        public const string New = "New";
        public const string Cancelled = "Cancelled";
        public const string DateChanged = "Date Changed";
        public const string AffiliateChange = "Affiliate Change";
        public const string RouteChange = "Route Change";
        public const string PassengerChange = "Passenger Change";
        public const string MajorChange = "Major Change";
        public const string Closed = "Closed";
    }

    [Serializable]
    public static class BillingStatusTypes {
        public const string Nothing = "";
        public const string Authorized = "Authorized";
        public const string Charged = "Charged";
        public const string ChargedNoEmail = "Charged no email invoice";
        public const string Invoiced = "Invoiced";
        public const string Billed = "Direct billed";
        public const string FailedCharge = "Failed Charge";
    }

    [Serializable]
    public class ReserveEditStatus {
        public bool DateChanged = false;
        public bool AffiliateChange = false;
        public bool RouteChange = false;
        public bool PassengerChange = false;

        public string GetStatus() {
            int iCount = 0;
            string sStatus = RideStatusTypes.New;

            if (DateChanged) {
                iCount++;
                sStatus = RideStatusTypes.DateChanged;
            }
            if (AffiliateChange) {
                iCount++;
                sStatus = RideStatusTypes.AffiliateChange;
            }
            if (RouteChange) {
                iCount++;
                sStatus = RideStatusTypes.RouteChange;
            }
            if (PassengerChange) {
                iCount++;
                sStatus = RideStatusTypes.PassengerChange;
            }
            if (iCount > 1) {
                sStatus = RideStatusTypes.MajorChange;
            }
            return sStatus;
        }

    }

}
