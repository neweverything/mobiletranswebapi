using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Runtime.Serialization;

using IdeaBlade.Persistence;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

using CabRideEngine.Utils;

using TaxiPassCommon;


namespace CabRideEngine {
    public partial class BlackOutDatesDataTable : IRdbTable {

        private string msError;

        public IList<BlackOutDates> GetChangedList() {
            DataSet oDataSet = PersistenceManager.DataSet;
            List<BlackOutDates> oList = new List<BlackOutDates>();

            try {
                DataTable oTable = oDataSet.Tables[TableName].GetChanges();

                if (oTable != null) {
                    DataRowCollection oRows = oTable.Rows;
                    for (int i = 0; i < oRows.Count; i++) {
                        BlackOutDates oEntity = (BlackOutDates)oRows[i];
                        oList.Add(oEntity);
                    }
                }
            } catch (Exception ex) {
                throw ex;
            }
            return oList;
        }

        public new void RejectChanges() {
            PersistenceManager.DataSet.Tables[TableName].RejectChanges();
        }

        public new void AcceptChanges() {
            PersistenceManager.DataSet.Tables[TableName].AcceptChanges();
        }


        public bool SaveChanges() {
            SaveOptions oOptions = new SaveOptions();
            oOptions.IsTransactional = false;
            return SaveChanges(oOptions);
        }

        public bool SaveChanges(SaveOptions oSaveOptions) {
            IList<BlackOutDates> oList = GetChangedList();

            msError = "";
            SaveResult oResult = null;

            if (oList.Count > 0) {
                try {
                    oResult = PersistenceManager.SaveChanges(oList, oSaveOptions);
                    if (!oResult.Ok) {
                        if (oResult.Exception != null) {
                            Type eType = oResult.Exception.GetType();
                            if (eType == typeof(System.Data.DBConcurrencyException)) {
                                msError = "A concurrency error occurred while saving your data.\n\n Someone probably saved their changes while you were making your changes.\n\n";
                            } else {
								
                                TaxiPassCommon.ErrorLog.ErrorRoutine(oResult.Exception);
                                msError = "Errors saving changes to database: " + oResult.Exception.ToString();
                            }
                        } else {
                            msError = "Errors saving changes to database: Unknown reason";
                        }
                        Exception ex;
                        for (int i = 0; i < oResult.EntitiesWithErrors.Count; i++) {
                            ex = new Exception(oResult.EntitiesWithErrors[i].RowError);
                            ex.Source = TableName;
                            TaxiPassCommon.ErrorLog.ErrorRoutine(ex);
                            oResult.EntitiesWithErrors[i].RemoveFromManager();
                        }
                    }
                } catch (Exception ex) {
                    msError = ex.Message;
                    TaxiPassCommon.ErrorLog.ErrorRoutine(ex);
                    oResult = new SaveResult((new List<Entity>()), ex);
                }
                if (oResult.Ok) {
                    AcceptChanges();
                } else {
                    RejectChanges();
                }
                return oResult.Ok;
            }
            return true;
        }

        public bool HasChanges() {
            return (GetChangedList().Count > 0);
        }

        public string ErrorMessage {
            get {
                return msError;
            }
        }

    }
}
