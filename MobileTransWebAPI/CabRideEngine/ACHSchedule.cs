using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

using TaxiPassCommon;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

namespace CabRideEngine {
  /// <summary>
  /// The developer class representing the ACHSchedule business object class.
  /// </summary>
  /// <remarks>
  /// <para>Place all application-specific business logic in this class.</para>
  /// <para>
  /// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
  /// and will not overwrite changes.
  /// </para>
  /// <para>
  /// This class inherits from the generated class, its companion in the pair of classes 
  /// defining a business object. Properties in the generated class may be overridden in 
  /// order to change the behavior or add business logic.
  /// </para>
  /// <para>
  /// This class is the final class in the inheritance chain for this business object. 
  /// It should not be inherited.
  /// </para>
  /// </remarks>
  [Serializable]
  public sealed partial class ACHSchedule : ACHScheduleDataRow {
  
    #region Constructors -- DO NOT MODIFY
    
    // Do not create constructors for this class
    // Developers cannot create instances of this class with the "new" keyword
    // You should write a static Create method instead; see the "Suggested Customization" region
    // Please review the documentation to learn about entity creation and inheritance

    // This private constructor prevents accidental instance creation via 'new';
    // it is also required if the class is visible from a web service.
    // DO NOT REMOVE **
    private ACHSchedule() : this(null) {}

    /// <summary>
    /// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
    /// </summary>
    /// <param name="pRowBuilder"></param>
    public ACHSchedule(DataRowBuilder pRowBuilder) 
      : base(pRowBuilder) {
    }

    #endregion
    
    #region Suggested Customizations
  
//    // Use this factory method template to create new instances of this class
//    public static ACHSchedule Create(PersistenceManager pManager,
//      ... your creation parameters here ...) { 
//
//      ACHSchedule aACHSchedule = pManager.CreateEntity<ACHSchedule>();
//
//      // if this object type requires a unique id and you have implemented
//      // the IIdGenerator interface implement the following line
//      pManager.GenerateId(aACHSchedule, // add id column here //);
//
//      // Add custom code here
//
//      aACHSchedule.AddToManager();
//      return aACHSchedule;
//    }

//    // Implement this method if you want your object to sort in a specific order
//    public override int CompareTo(Object pObject) {
//    }

//    // Implement this method to customize the null object version of this class
//    protected override void UpdateNullEntity() {
//    }

    #endregion
    
    // Add additional logic to your business object here...

	// **************************************************************
	// Create a new row
	// **************************************************************
	public static ACHSchedule Create(PersistenceManager pPM) {
		ACHSchedule oACHSchedule = null;

		try {
			// Creates the State but it is not yet accessible to the application
			oACHSchedule = (ACHSchedule)pPM.CreateEntity(typeof(ACHSchedule));

			// CRITICAL: must tell emp to add itself to its PM
			oACHSchedule.AddToManager();


		} catch (Exception ex) {
			throw ex;
		}
		return oACHSchedule;
	}


	// *************************************************************
	// Return a list of (optional) active schedules
	// pPayType can be 'Affiliate' or 'Redeemer Account'
	// *************************************************************
	public static EntityList<ACHSchedule> GetActiveSchedules(PersistenceManager pPM, string pPayeeType) {

		return GetData(pPM, "", "", pPayeeType, "1");
	}

	
	// *********************************************************************************
	// retrieve a single ach schedule record
	// *********************************************************************************
	public static ACHSchedule GetACHSchedule(PersistenceManager pPM, long pACHSCheduleID) {
		return GetData(pPM, pACHSCheduleID.ToString(), "", "", "")[0];
	}


	// *********************************************************************************
	// Get an Affiliate's schedule
	// *********************************************************************************
	public static ACHSchedule GetACHScheduleForAffiliate(PersistenceManager pPM, long pAffiliateID) {
		RdbQuery qry = new RdbQuery(typeof(ACHSchedule), ACHSchedule.PayeeIDEntityColumn, EntityQueryOp.EQ, pAffiliateID);
		qry.AddClause(ACHSchedule.PayeeTypeEntityColumn, EntityQueryOp.EQ, "Affiliate");
		return GetData(pPM, "", pAffiliateID.ToString(), "Affiliate","")[0];
	}


	// ************************************************************************************************************
	// Get the data
	// ************************************************************************************************************
	private static EntityList<ACHSchedule> GetData(PersistenceManager pPM, string pACHSCheduleID, string pAffiliateID, string pPayeeType, string pInactive) {
		string sDelimiter = "";
		int iArgCount = 0;

		StringBuilder sql = new StringBuilder();
		sql.Append("usp_ACHScheduleGet ");
		if (!pACHSCheduleID.IsNullOrEmpty()) {
			sql.AppendFormat(" @ACHSCheduleID = {0}", pACHSCheduleID);
			iArgCount++;
		}
		if (!pAffiliateID.IsNullOrEmpty()) {
			if (iArgCount > 0) {
				sDelimiter = ", ";
			}
			sql.AppendFormat("{0}@PayeeID = {1}", sDelimiter, pAffiliateID);
			iArgCount++;
		}
		if (!pPayeeType.IsNullOrEmpty()) {
			if (iArgCount > 0) {
				sDelimiter = ", ";
			}
			sql.AppendFormat("{0}@PayeeType= '{1}'", sDelimiter, pPayeeType);
			iArgCount++;
		}
		if (!pInactive.IsNullOrEmpty()) {
			if (iArgCount > 0) {
				sDelimiter = ", ";
			}
			sql.AppendFormat("{0}@Inactive= {1}", sDelimiter, pInactive);
			iArgCount++;
		}
		PassthruRdbQuery qry = new PassthruRdbQuery(typeof(ACHSchedule), sql.ToString());
		EntityList<ACHSchedule> oData = pPM.GetEntities<ACHSchedule>(qry);
		if (oData.Count == 0) {
			oData.Add(pPM.GetNullEntity<ACHSchedule>());
		}
		return oData;
	}

	// *************************************************************
	// Determine if we need an audit record
	// *************************************************************
	protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
		if (IsDeserializing)
			return;
		base.OnColumnChanging(pArgs);
		DoAudit(pArgs);
	}


	// *************************************************************
	// Create the audit record on changes
	// *************************************************************
	private void DoAudit(DataColumnChangeEventArgs pArgs) {
		if (!this.PersistenceManager.IsClient) {
			return;
		}
		if (this.RowState == DataRowState.Added) {
			return;
		}
		if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
			AuditRecord = ACHScheduleAudit.Create(this);
		}
	}
  }
  
  #region EntityPropertyDescriptors.ACHSchedulePropertyDescriptor
  ////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
  ////    For example: 
  ////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
  //public static partial class EntityPropertyDescriptors {
	//	public partial class ACHSchedulePropertyDescriptor : BaseEntityPropertyDescriptor {
  //    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
  //    public AdaptedPropertyDescriptor Age {
  //      get { return this.Get("Age"); }
  //    }
  //  }
  //} 
  #endregion
  
}