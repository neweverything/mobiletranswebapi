﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

using Dapper;

using TaxiPassCommon;
//using System.ComponentModel.DataAnnotations;


namespace CabRideEngineDapper {
	public partial class POIUsers {

		#region Save
		public new void Save(SqlConnection pConn, string pModifiedBy) {
			if (Password == PasswordDecrypted) {
				Password = Password.EncryptIceKey();
			}

			ModifiedDate = DateTime.UtcNow;
			ModifiedBy = pModifiedBy;

			if (IsNewRecord) {
				CreatedBy = pModifiedBy;
				CreatedDate = ModifiedDate;

				int? id = pConn.Insert(this);
				this.POIUserID = id.Value;
			} else {
				pConn.Update(this);
			}
		}
		#endregion

		[Editable(false)]
		public string PasswordDecrypted {
			get {
				return Password.DecryptIceKey();
			}
		}


		public static List<POIUsers> GetUsersForMaster(long pMasterID) {
			List<POIUsers> userList = new List<POIUsers>();
			string sql = string.Format("SELECT * FROM {0} WITH (NoLock) WHERE {1} = @{1}", POIUsers.TableDef.TABLE_NAME, POIUsers.TableDef.POIMasterID);
			DynamicParameters param = new DynamicParameters();
			param.Add(POIUsers.TableDef.POIMasterID, pMasterID);
			using (var conn = Utils.SqlHelper.OpenSqlConnection()) {
				userList = conn.Query<POIUsers>(sql, param).ToList();
			}
			return userList;
		}




		public static POIUsers GetUserByAccount(string pUserName, long pAccountID, long pPOIMasterID) {
			StringBuilder sql = new StringBuilder("SELECT * FROM POIUsers WITH (NOLOCK) ");
			sql.Append("LEFT JOIN Accounts ON Accounts.POIMasterID = POIUsers.POIMasterID ");
			sql.Append("WHERE UserName = @UserName ");
			if (pPOIMasterID == 0) {
				if (pAccountID == 0) {
					sql.Append("AND Accounts.AccountID is null");
				} else {
					sql.Append("AND Accounts.AccountID = @AccountID");
				}
			} else {
				sql.Append("AND POIUsers.POIMasterID = @POIMasterID");
			}
			DynamicParameters param = new DynamicParameters();
			param.Add("UserName", pUserName);
			if (pPOIMasterID == 0) {
				if (pAccountID != 0) {
					param.Add("AccountID", pAccountID);
				}
			} else {
				param.Add("POIMasterID", pPOIMasterID);
			}
			POIUsers user = POIUsers.GetNullEntity();
			using (var conn = Utils.SqlHelper.OpenSqlConnection()) {
				user = conn.Query<POIUsers>(sql.ToString(), param).DefaultIfEmpty(POIUsers.GetNullEntity()).FirstOrDefault();
			}
			return user;
		}
	}
}
