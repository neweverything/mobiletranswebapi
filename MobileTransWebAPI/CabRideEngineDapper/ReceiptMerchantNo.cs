﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace CabRideEngineDapper {
	public partial class ReceiptMerchantNo {

		public ReceiptMerchantNo() {
			Description = "";
		}

		public static ReceiptMerchantNo GetMerchant(SqlConnection pConn, string pMerchantNo) {
			string sql = @"SELECT  *
							FROM    dbo.ReceiptMerchantNo
							WHERE   Description LIKE @MerchantNo
									AND Active = 1";

			return pConn.Get<ReceiptMerchantNo>(sql, new { MerchantNo = string.Format("%{0}%", pMerchantNo) });
		}

		public static List<ReceiptMerchantNo> GetAll(SqlConnection pConn, bool pActiveOnly = true) {
			string sql = @"SELECT  *
							FROM    dbo.ReceiptMerchantNo
							WHERE   Active = 1
									OR Active = @Active
							ORDER BY Description";

			return pConn.Query<ReceiptMerchantNo>(sql, new { Active = pActiveOnly }).ToList();
		}
	}
}
