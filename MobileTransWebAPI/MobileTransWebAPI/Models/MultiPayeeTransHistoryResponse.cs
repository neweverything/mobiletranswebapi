﻿namespace MobileTransWebAPI.Models {
	public class MultiPayeeTransHistoryResponse {
		public int ErrorCode { get; set; }
		public string ErrorMsg { get; set; }
		public string TransList { get; set; }
	}
}