﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

using Dapper;

using TaxiPassCommon;

using CabRideEngineDapper.Utils;

namespace CabRideEngineDapper {
	public partial class Countries {

		public static Countries GetDefaultCountry() {
			using (var conn = SqlHelper.OpenSqlConnection()) {
				return GetDefaultCountry(conn);
			}
		}

		public static Countries GetDefaultCountry(SqlConnection pConn) {
			
			List<Countries> list = GetData(pConn, "", true);

			if(list.Count>0) {
				return list[0];
			}
			return CabRideEngineDapper.Countries.GetNullEntity();
		}

		// ******************************************************************************
		// Call the proc
		// ******************************************************************************
		private static List<Countries> GetData(SqlConnection pConn, string pName, bool pDefault) {
			string sql = "usp_CountriesGet ";
			DynamicParameters param = new DynamicParameters();
			
			if (!pName.IsNullOrEmpty()) {
				param.Add("@Name", pName);
			}
			if (pDefault) {
				param.Add("@Default", 1);
			}

			return pConn.Query<Countries>(sql, param, commandType: CommandType.StoredProcedure).ToList();

		}


		public static IEnumerable<Countries> GetAllCountriesForDropDown(SqlConnection pConn) {
			string sql = @"SELECT Country, NAME FROM dbo.Countries WITH (NOLOCK)
							ORDER BY NAME";

			return pConn.Query<Countries>(sql);
		}
	}
}
