﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using System.Collections.Generic;
using System.Linq;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

using Csla.Validation;

using TaxiPassCommon;
using System.Text;


namespace CabRideEngine {
	/// <summary>
	/// The developer class representing the RedeemerCashAccounting business object class.
	/// </summary>
	/// <remarks>
	/// <para>Place all application-specific business logic in this class.</para>
	/// <para>
	/// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
	/// and will not overwrite changes.
	/// </para>
	/// <para>
	/// This class inherits from the generated class, its companion in the pair of classes 
	/// defining a business object. Properties in the generated class may be overridden in 
	/// order to change the behavior or add business logic.
	/// </para>
	/// <para>
	/// This class is the final class in the inheritance chain for this business object. 
	/// It should not be inherited.
	/// </para>
	/// </remarks>
	[Serializable]
	public sealed partial class RedeemerCashAccounting : RedeemerCashAccountingDataRow {

		#region Constructors -- DO NOT MODIFY

		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new';
		// it is also required if the class is visible from a web service.
		// DO NOT REMOVE **
		private RedeemerCashAccounting() : this(null) { }

		/// <summary>
		/// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
		/// </summary>
		/// <param name="pRowBuilder"></param>
		public RedeemerCashAccounting(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static RedeemerCashAccounting Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      RedeemerCashAccounting aRedeemerCashAccounting = pManager.CreateEntity<RedeemerCashAccounting>();
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aRedeemerCashAccounting, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aRedeemerCashAccounting.AddToManager();
		//      return aRedeemerCashAccounting;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		// Add additional logic to your business object here...

		public static RedeemerCashAccounting Create(TaxiPassRedeemers pRedeemer) {
			RedeemerCashAccounting rec = null;
			try {
				rec = pRedeemer.PersistenceManager.CreateEntity<RedeemerCashAccounting>();
				rec.TaxiPassRedeemerID = pRedeemer.TaxiPassRedeemerID;
				rec.MyTimeZone = pRedeemer.TaxiPassRedeemerAccounts.Markets.TimeZone;
				rec.Date = DateTime.Now;
				rec.AddToManager();
			} catch (Exception ex) {
				throw ex;
			}
			return rec;
		}

		private string mTimeZone = "";
		private string MyTimeZone {
			get {
				if (mTimeZone.IsNullOrEmpty()) {
					mTimeZone = this.TaxiPassRedeemers.TaxiPassRedeemerAccounts.Markets.TimeZone;
				}
				return mTimeZone;
			}

			set {
				mTimeZone = value;
			}
		}

		public override DateTime Date {
			get {
				return base.Date;
			}
			set {
				TimeZoneConverter tzConvert = new TimeZoneConverter();
				if (MyTimeZone.IsNullOrEmpty()) {
					base.Date = value.ToUniversalTime();
				} else {
					base.Date = tzConvert.ConvertTime(value, TimeZone.CurrentTimeZone.StandardName, MyTimeZone);
				}
			}
		}

		public string FromAccount {
			get {
				if (FromCashier) {
					return TaxiPassRedeemers.GetRedeemer(this.PersistenceManager, this.FromAcctID).Name;
				}
				return CashAccountType.GetByID(this.PersistenceManager, this.FromAcctID).Type;
			}
		}

		public string ToAccount {
			get {
				if (ToCashier) {
					return TaxiPassRedeemers.GetRedeemer(this.PersistenceManager, this.ToAcctID).Name;
				}
				return CashAccountType.GetByID(this.PersistenceManager, this.ToAcctID).Type;
			}
		}

		public decimal? AccountDeposit {
			get {
				if (!ToCashier) {
					return Amount;
				}
				return null;
			}
		}

		public decimal? AccountWithdrawal {
			get {
				if (!FromCashier) {
					return Amount;
				}
				return null;
			}
		}


		public string ProcessedBy {
			get {
				return this.TaxiPassRedeemers.Name;
			}
		}

		public static EntityList<RedeemerCashAccounting> GetByDate(TaxiPassRedeemers pRedeemer, DateTime pDate) {
			return GetByDate(pRedeemer, pDate, pDate);
		}

		public static EntityList<RedeemerCashAccounting> GetByDate(TaxiPassRedeemers pRedeemer, DateTime pStartDate, DateTime pEndDate) {
			RdbQuery qry = new RdbQuery(typeof(RedeemerCashAccounting), RedeemerCashAccounting.DateEntityColumn, EntityQueryOp.GE, pStartDate.Date);
			qry.AddClause(RedeemerCashAccounting.DateEntityColumn, EntityQueryOp.LT, pEndDate.Date.AddDays(1).Date);

			if (pRedeemer.Admin) {
				List<long> redeemerList = (from p in pRedeemer.TaxiPassRedeemerAccounts.TaxiPassRedeemerses
										   select p.TaxiPassRedeemerID).ToList();
				qry.AddClause(RedeemerCashAccounting.TaxiPassRedeemerIDEntityColumn, EntityQueryOp.In, redeemerList);
			} else {
				qry.AddClause(RedeemerCashAccounting.TaxiPassRedeemerIDEntityColumn, EntityQueryOp.EQ, pRedeemer.TaxiPassRedeemerID);
			}
			qry.AddOrderBy(RedeemerCashAccounting.DateEntityColumn, System.ComponentModel.ListSortDirection.Descending);
			return pRedeemer.PersistenceManager.GetEntities<RedeemerCashAccounting>(qry);
		}

		public static EntityList<RedeemerCashAccounting> GetForCashier(PersistenceManager pPM, long pAccountID, DateTime pStartDate, DateTime pEndDate, bool pIncludeAccounts) {
			StringBuilder sql = new StringBuilder();

			sql.Append("SELECT  RedeemerCashAccounting.* FROM RedeemerCashAccounting with(nolock) ");
			sql.Append("WHERE Date >= '");
			sql.Append(pStartDate.ToShortDateString());
			sql.Append("' AND Date < '");
			sql.Append(@pEndDate.AddDays(1).ToShortDateString());
			sql.Append("' AND ((FromCashier = 1 and FromAcctID = ");
			sql.Append(pAccountID);
			sql.Append(") or (ToCashier=1 and ToAcctID = ");
			sql.Append(pAccountID);
			sql.Append(") ");
			if (pIncludeAccounts) {
				sql.Append(" OR (FromCashier = 0 AND ToCashier = 0) ");
			}
			sql.Append(") ORDER BY [Date] DESC");
			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(RedeemerCashAccounting), sql.ToString());
			return pPM.GetEntities<RedeemerCashAccounting>(qry);
		}

		public static EntityList<RedeemerCashAccounting> GetForTrackedAccounts(TaxiPassRedeemerAccounts pRedeemerAccount, DateTime pStartDate, DateTime pEndDate) {
			StringBuilder sql = new StringBuilder();
			sql.Append("EXEC usp_RedeemerCashAccounting_TrackedAccounts ");
			sql.Append("@RedeemerAccountID = ");
			sql.Append(pRedeemerAccount.TaxiPassRedeemerAccountID);
			sql.Append(", ");
			sql.Append("@StartDate = N'");
			sql.Append(pStartDate.ToString());
			sql.Append("', ");
			sql.Append("@EndDate = N'");
			sql.Append(pEndDate.ToString());
			sql.Append("'");

			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(RedeemerCashAccounting), sql.ToString());
			return pRedeemerAccount.PersistenceManager.GetEntities<RedeemerCashAccounting>(qry);
		}
	}

	#region EntityPropertyDescriptors.RedeemerCashAccountingPropertyDescriptor
	////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
	////    For example: 
	////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
	//public static partial class EntityPropertyDescriptors {
	//	public partial class RedeemerCashAccountingPropertyDescriptor : BaseEntityPropertyDescriptor {
	//    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
	//    public AdaptedPropertyDescriptor Age {
	//      get { return this.Get("Age"); }
	//    }
	//  }
	//} 
	#endregion

}