﻿using Dapper;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using TaxiPassCommon;

namespace CabRideEngineDapper.Utils {



    public class AffiliatePricing {


        public CustomFeeType FeeType { get; set; }
        public List<MyRates> rateList { get; set; }

        public class MyRates {
            public long RateID { get; set; }
            public string RateType { get; set; }
            public string VehicleType { get; set; }

            public bool PickUpAllAreas { get; set; }
            public string PickUpLocation { get; set; }
            public string PickUpZip { get; set; }
            public string PickUpCity { get; set; }

            public bool DropOffAllAreas { get; set; }
            public string DropOffLocation { get; set; }
            public string DropOffZip { get; set; }
            public string DropOffCity { get; set; }

            public decimal Rate { get; set; }
            public decimal Tolls { get; set; }
            public decimal PerMinCharge { get; set; }
            public decimal Airport { get; set; }
            public decimal Fee1 { get; set; }
            public decimal Fee2 { get; set; }
            public decimal Fee3 { get; set; }
            public decimal Fee4 { get; set; }

            public int Score {
                get {
                    int value = 10;
                    if (!PickUpZip.IsNullOrEmpty() && !DropOffZip.IsNullOrEmpty()) {
                        value = 1;
                    } else if (!PickUpZip.IsNullOrEmpty() && !DropOffCity.IsNullOrEmpty()) {
                        value = 2;
                    } else if (!PickUpZip.IsNullOrEmpty() && DropOffAllAreas) {
                        value = 3;
                    } else if (!PickUpCity.IsNullOrEmpty() && !DropOffZip.IsNullOrEmpty()) {
                        value = 4;
                    } else if (!PickUpCity.IsNullOrEmpty() && !DropOffCity.IsNullOrEmpty()) {
                        value = 5;
                    } else if (!PickUpCity.IsNullOrEmpty() && DropOffAllAreas) {
                        value = 6;
                    } else if (PickUpAllAreas && !DropOffZip.IsNullOrEmpty()) {
                        value = 7;
                    } else if (PickUpAllAreas && !DropOffCity.IsNullOrEmpty()) {
                        value = 8;
                    }
                    return value;
                }
            }
        }

        public AffiliatePricing(SqlConnection pConn, long pAffiliateID, Address pPickUpAddress, Address pDropOffAddress, string pVehicleType = "Taxi") {
            //public static List<AffiliatePricing> GetPricing(SqlConnection pConn, long pAffiliateID, Address pPickUpAddress, Address pDropOffAddress, string pVehicleType = "Taxi") {
            string sql = @"SELECT  RateID
                                  , LTRIM(RTRIM(RateType)) RateType
                                  , PickUpLocation
	                              , CASE WHEN PickUpLocation = 'All Zip Codes' THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END PickUpAllAreas
	                              , CASE WHEN LEN(PickUpLocation) = 5 AND ISNUMERIC(PickUpLocation) = 1 THEN PickUpLocation ELSE '' END PickUpZip
	                              , CASE WHEN (LEN(PickUpLocation) != 5 OR (LEN(PickUpLocation) = 5 AND ISNUMERIC(PickUpLocation) = 0)) AND PickUpLocation != 'All Zip Codes' THEN PickUpLocation ELSE '' END PickUpCity
                                  , DropOffLocation
	                              , CASE WHEN DropOffLocation = 'All Zip Codes' THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END DropOffAllAreas
	                              , CASE WHEN LEN(DropOffLocation) = 5 AND ISNUMERIC(DropOffLocation) = 1 THEN DropOffLocation ELSE '' END DropOffZip
	                              , CASE WHEN (LEN(DropOffLocation) != 5 OR (LEN(DropOffLocation) = 5 AND ISNUMERIC(DropOffLocation) = 0)) AND DropOffLocation != 'All Zip Codes' THEN DropOffLocation ELSE '' END DropOffCity
                                  , Rate
                                  , Tolls
                                  , Airport
                                  , ISNULL(PerMinCharge, 0) PerMinCharge
                                  , ISNULL(Rates.Fee1, 0) Fee1
                                  , ISNULL(Rates.Fee2, 0) Fee2
                                  , ISNULL(Rates.Fee3, 0) Fee3
                                  , ISNULL(Rates.Fee4, 0) Fee4
                            FROM    Rates
                            OUTER APPLY (SELECT TOP 1
                                                *
                                         FROM   dbo.CustomFeeType
                                         WHERE  AffiliateID = @AffiliateID
                                        ) MyFeeType
                            WHERE   Rates.AffiliateID = @AffiliateID
                                    AND VehicleTypeID = (SELECT VehicleTypeID FROM dbo.VehicleTypes WHERE VehicleType = @VehicleType)
                                    AND (PickUpLocation = @PUZipCode
                                         OR PickUpLocation = @PUCity
                                         OR PickUpLocation = 'All Zip Codes'
                                        )
                                    AND (DropOffLocation = @DOZipCode
                                         OR DropOffLocation = @DOCity
                                         OR DropOffLocation = 'All Zip Codes'
                                        ) 

                        SELECT  *
                        FROM    dbo.CustomFeeType
                        WHERE   AffiliateID = @AffiliateID";

            DynamicParameters parms = new DynamicParameters();
            parms.Add("AffiliateID", pAffiliateID);
            parms.Add("VehicleType", pVehicleType);
            parms.Add("PUZipCode", pPickUpAddress.ZIP);
            parms.Add("PUCity", pPickUpAddress.City);
            parms.Add("DOZipCode", pDropOffAddress.ZIP);
            parms.Add("DOCity", pDropOffAddress.City);

            var multi = pConn.QueryMultiple(sql, parms);

            rateList = multi.Read<MyRates>().ToList();
            FeeType = multi.Read<CustomFeeType>().FirstOrDefault();
        }

    }
}
