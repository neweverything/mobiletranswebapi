﻿namespace MobileTransWebAPI.Models {

	public class ServiceFeeResult {
		public string ErrorMsg { get; set; }
		public decimal ServiceFee { get; set; }
		public decimal DriverFee { get; set; }
		public string CardType { get; set; }
		public decimal CashierFee { get; set; }
		public string FeeDesc { get; set; }
	}
}