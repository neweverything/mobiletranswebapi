﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Text;
using System.Linq;

using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Persistence;
using IdeaBlade.Util;

using Csla.Validation;

using TaxiPassCommon;

namespace CabRideEngine {
	[Serializable]
	public sealed class CustomerCards : CustomerCardsDataRow {

		#region Constructors -- DO NOT MODIFY
		// Do not create constructors for this class
		// Developers cannot create instances of this class with the "new" keyword
		// You should write a static Create method instead; see the "Suggested Customization" region
		// Please review the documentation to learn about entity creation and inheritance

		// This private constructor prevents accidental instance creation via 'new'
		private CustomerCards()
			: this(null) {
		}

		// Typed DataSet constructor (needed by framework) - DO NOT REMOVE
		public CustomerCards(DataRowBuilder pRowBuilder)
			: base(pRowBuilder) {
		}

		#endregion

		#region Suggested Customizations

		//    // Use this factory method template to create new instances of this class
		//    public static CustomerCards Create(PersistenceManager pManager,
		//      ... your creation parameters here ...) { 
		//
		//      CustomerCards aCustomerCards = pManager.CreateEntity<CustomerCards>();
		//
		//      // if this object type requires a unique id and you have implemented
		//      // the IIdGenerator interface implement the following line
		//      pManager.GenerateId(aCustomerCards, // add id column here //);
		//
		//      // Add custom code here
		//
		//      aCustomerCards.AddToManager();
		//      return aCustomerCards;
		//    }

		//    // Implement this method if you want your object to sort in a specific order
		//    public override int CompareTo(Object pObject) {
		//    }

		//    // Implement this method to customize the null object version of this class
		//    protected override void UpdateNullEntity() {
		//    }

		#endregion

		// Add additional logic to your business object here...
		public static CustomerCards Create(PersistenceManager pManager, long pCustomerID) {
			CustomerCards oCards = null;
			try {
				oCards = (CustomerCards)pManager.CreateEntity(typeof(CustomerCards));
				oCards.CustomerID = pCustomerID;
				//oCards.Number = "";

				oCards.AddToManager();

			} catch (Exception ex) {
				throw ex;
			}
			return oCards;
		}

		
		public static CustomerCards Create(Customer pCustomer, string pCardType, string pCardNo) {
			CustomerCards oCard = null;
			try {

				EntityList<CustomerCards> oCards = GetCards(pCustomer.PersistenceManager, pCustomer.CustomerID);
				oCard = oCards.FirstOrDefault(p => p.DecryptCardNumber() == pCardNo || p.Number.DecryptIceKey() == pCardNo);

				if(oCard == null) {
					oCard = Create(pCustomer.PersistenceManager, pCustomer.CustomerID);
					oCard.CardType = Utils.CreditCardUtils.DetermineCardType(pCustomer.PersistenceManager, pCardNo);
					oCard.Number = pCardNo;
					oCard.CardNumberDisplay = pCardNo.CardNumberDisplay();
				}

			} catch (Exception ex) {
				throw ex;
			}
			return oCard;
		}


		protected override void AddRules(RuleList pList) {
			PropertyRequiredRule.AddToList(pList, CardTypeColumn.ColumnName);
			PropertyRequiredRule.AddToList(pList, NumberColumn.ColumnName);
			pList.Add(IsValidCardNumber, NumberColumn.ColumnName);
		}


		public bool IsValidCardNumber(object pTarget, RuleArgs e) {
			bool bOk = false;
			try {
				string sCardNo = base.Number; // Number;
				bOk = Utils.CreditCardUtils.IsValidCardNumber(this.Customer, this.CardType, sCardNo);
			} catch (Exception ex) {
				e.Description = ex.Message;
				return false;
			}
			return bOk;
			//return true;
		}

		// ***********************************************
		// Tokens used by GetRide for obfuscation
		// ***********************************************
		private string mToken;
		public string Token {
			get {
				return mToken;
			}
			set {
				mToken = value;
			}
		}

		public override string Number {
			get {
				return Utils.CreditCardUtils.DecryptCardNo(this.Customer, base.Number);
			}

			set {
				try {
					base.Number = Utils.CreditCardUtils.EncryptCardNo(this.Customer, this.CardType, value);
				} catch (Exception ex) {
					base.Number = value;
					throw ex;
				}
			}
		}


		public override string Name {
			get {
				return base.Name.DecryptIceKey();
			}
			set {
				base.Name = value.EncryptIceKey();
			}
		}


		public override string Verification {
			get {
				return base.Verification.DecryptIceKey();
			}
			set {
				base.Verification = value.EncryptIceKey();
			}
		}


		public string DecryptCardNumber() {
			return Utils.CreditCardUtils.DecryptCardNo(this.Customer, base.Number, false);
		}

		//public string GetCardNumber() {
		//    return CryptoFns.SimpleDESDecrypt(this["Number"].ToString(), this.Customer.Salt);
		//}

		// ****************************************************************************************
		// Get a customer's cards
		// ****************************************************************************************
		public static EntityList<CustomerCards> GetCards(PersistenceManager pPM, long pCustomerID) {
			StringBuilder sql = new StringBuilder();
			sql.AppendFormat("usp_CustomerCardsGet @CustomerID = {0}", pCustomerID);
			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(CustomerCards), sql.ToString());
			EntityList<CustomerCards> oCustomerCards = pPM.GetEntities<CustomerCards>(qry);
			return oCustomerCards;
		}

		public static CustomerCards GetCard(PersistenceManager pPM, long pCardID) {
			RdbQuery qry = new RdbQuery(typeof(CustomerCards), CustomerCards.CustCardIDEntityColumn, EntityQueryOp.EQ, pCardID);
			return pPM.GetEntity<CustomerCards>(qry);
		}

		public static CustomerCards GetCard(PersistenceManager pPM, long pCustomerID, string pCardNo) {
			RdbQuery qry = new RdbQuery(typeof(CustomerCards), CustomerCards.NumberEntityColumn, EntityQueryOp.EQ, pCardNo);
			qry.AddClause(CustomerCards.CustomerIDEntityColumn, EntityQueryOp.EQ, pCustomerID);
			return pPM.GetEntity<CustomerCards>(qry);
		}


		// *********************************************************************************
		// lookup a CustomerCard by Token
		// We were seeing null entity's returned so make sure the CustomerID is set
		// *********************************************************************************
		public static CustomerCards GetCardByToken(PersistenceManager pPM, string pToken, long pCustomerID) {
			StringBuilder sql = new StringBuilder();
			sql.AppendFormat("usp_CustomerCardsGetTokens @CustomerID ={0}", pCustomerID);
			sql.AppendFormat(", @Token = '{0}'", pToken);

			PassthruRdbQuery qry = new PassthruRdbQuery(typeof(CustomerCards), sql.ToString());
			CustomerCards oCard = pPM.GetEntity<CustomerCards>(qry);
			if (oCard.IsNullEntity) {
				oCard = CustomerCards.Create(pPM, pCustomerID);
			}
			oCard.Token = pToken;
			return oCard;
		}

		// *********************************************************************************
		// Return the card tokens for a Customer
		// *********************************************************************************
		public static Entity[] GetCustomerCardTokens(PersistenceManager pPM, long pCustomerID) {
			StringBuilder sql = new StringBuilder();
			sql.AppendFormat("usp_CustomerCardsGetTokens @CustomerID ={0}", pCustomerID);
			return DynamicEntities.DynamicDBCall(pPM, sql.ToString());
		}

		// *********************************************************************************
		// Return the Customer's default card
		// *********************************************************************************
		public static CustomerCards GetCustomerDefaultCard(PersistenceManager pPM, long pCustomerID) {
			RdbQuery qry = new RdbQuery(typeof(CustomerCards), CustomerCards.CustomerIDEntityColumn, EntityQueryOp.EQ, pCustomerID);
			qry.AddClause(CustomerCards.IsDefaultEntityColumn, EntityQueryOp.EQ, true);
			return pPM.GetEntity<CustomerCards>(qry);
			
		}



	}
}
