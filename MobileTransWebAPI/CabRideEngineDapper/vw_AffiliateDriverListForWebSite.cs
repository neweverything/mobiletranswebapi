﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Dapper;
using TaxiPassCommon;
using CabRideEngineDapper.Utils;

namespace CabRideEngineDapper {
	public partial class vw_AffiliateDriverListForWebSite {

		//public static List<vw_AffiliateDriverListForWebSite> GetDrivers(Affiliate pAffiliate, bool? pInActive) {
		//	RdbQuery qry = new RdbQuery(typeof(vw_AffiliateDriverListForWebSite), vw_AffiliateDriverListForWebSite.AffiliateIDEntityColumn, EntityQueryOp.EQ, pAffiliate.AffiliateID);
		//	if (pInActive.HasValue) {
		//		qry.AddClause(ActiveEntityColumn, EntityQueryOp.EQ, !pInActive);
		//	}
		//	qry.AddOrderBy(vw_AffiliateDriverListForWebSite.AffiliateNameEntityColumn);
		//	qry.AddOrderBy(vw_AffiliateDriverListForWebSite.NameEntityColumn);
		//	qry.AddOrderBy(vw_AffiliateDriverListForWebSite.CellEntityColumn);
		//	return pAffiliate.PersistenceManager.GetEntities<vw_AffiliateDriverListForWebSite>(qry);
		//}

		// ******************************************************************************************************************
		// Given a list of affiliates, return the drivers
		// ******************************************************************************************************************
		public static List<vw_AffiliateDriverListForWebSite> GetDrivers(List<Affiliate> pAffiliateList, bool? pInActive) {
			List<long> idList = (from p in pAffiliateList
								 select p.AffiliateID).ToList();
			string sql = "SELECT * FROM vw_AffiliateDriverListForWebSite WHERE AffiliateID in @AffiliateIDs ";
			DynamicParameters param = new DynamicParameters();
			param.Add("AffiliateIDs", idList);

			if (pInActive.HasValue) {
				sql += "AND Active = @Active ";
				param.Add("Active", !pInActive);
			}
			sql += "ORDER BY Name";
			using (var conn = SqlHelper.OpenSqlConnection()) {
				return conn.Query<vw_AffiliateDriverListForWebSite>(sql, param).ToList();
			}
		}

		public static List<vw_AffiliateDriverListForWebSite> GetDrivers(Affiliate pAffiliate, bool? pInActive) {
			StringBuilder sql = new StringBuilder("SELECT * FROM vw_AffiliateDriverListForWebSite WHERE AffiliateID = @AffiliateID ");
			DynamicParameters param = new DynamicParameters();
			param.Add("AffiliateID", pAffiliate.AffiliateID);
			if (pInActive.HasValue) {
				sql.Append("AND Active = @Active ");
				param.Add("Active", !pInActive);
			}
			sql.Append("ORDER BY AffiliateName, Name, Cell");
			using (var conn = SqlHelper.OpenSqlConnection()) {
				return conn.Query<vw_AffiliateDriverListForWebSite>(sql.ToString(), param).ToList();
			}
		}
	}
}
