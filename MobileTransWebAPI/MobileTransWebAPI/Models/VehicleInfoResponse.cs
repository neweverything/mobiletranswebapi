﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileTransWebAPI.Models {
	public class VehicleInfoResponse {
		public string ErrorMsg { get; set; }

		public string VehicleNo { get; set; }
		public long VehicleID { get; set; }
		public string VehicleKeyCode { get; set; }
		public string VehicleRFID { get; set; }


	}
}