﻿using CabRideEngineDapper.Utils;
using Dapper;

namespace CabRideEngineDapper {
    public partial class ReserveMTAUserVerified {

        public static ReserveMTAUserVerified GetRecord(long pReservationID) {
            string sql = @"SELECT * FROM ReserveMTAUserVerified
                            WHERE ReservationID = @ReservationID";

            using (var conn = SqlHelper.OpenSqlConnection()) {
                return conn.Get<ReserveMTAUserVerified>(sql, new { ReservationID = pReservationID });
            }
        }
    }
}