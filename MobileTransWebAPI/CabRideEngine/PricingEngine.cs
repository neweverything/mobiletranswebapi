using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

using CabRideEngine.Utils;

using IdeaBlade.Persistence;
using IdeaBlade.Persistence.Rdb;

using TaxiPassCommon;
using TaxiPassCommon.GeoCoding;

namespace CabRideEngine {
	public class PricingEngine {

		private PersistenceManager mPM;
		private EntityList<Countries> mCountries;
		private static EntityList<Pricing> mPricing = new EntityList<Pricing>();

		private EntityList<CoverageZones> mZones;
		private EntityList<RushHour> mRushHours;
		private EntityList<AffiliateCoverageZones> mAffiliatesZones;

		//static string DefaultCountry = "";
		private long OtherAreas = 0;

		public Address pickUpAddress;
		public Address dropOffAddress;
		public DateTime? RideDateTime = null;
		public bool TestPricing = false;

		public enum MappingService {
			MapPoint,
			MapQuest
		}

		private MappingService mMappingService;
		//private MapQuestService mMapQuestService;
		private MapPointService mMapPointService;

		public PricingEngine(PersistenceManager pPM) {
			InitializeClass(pPM, MappingService.MapPoint);
		}

		public PricingEngine(PersistenceManager pPM, MappingService pMappingService) {
			InitializeClass(pPM, pMappingService);
		}


		private void InitializeClass(PersistenceManager oPM, MappingService pMappingService) {
			mPM = oPM;
			mMappingService = pMappingService;

			mCountries = mPM.GetEntities<Countries>();
			if (mPricing == null) {
				//mPricing = mPM.GetEntities<Pricing>();
			}

			mZones = mPM.GetEntities<CoverageZones>();
			mRushHours = mPM.GetEntities<RushHour>();
			if (OtherAreas == 0) {
				CoverageZones oZone = mZones.Find(CoverageZones.GetOtherAreas);
				if (oZone != null) {
					OtherAreas = oZone.CoverageZoneID;
				}
			}
		}



		public PricedRide CalcRate() {
			return CalcRate(false);
		}

		public PricedRide CalcRate(bool forceRefetch) {
			if (pickUpAddress == null) {
				throw new ArgumentNullException("pickUpAddress");
			}
			if (dropOffAddress == null) {
				throw new ArgumentNullException("dropOffAddress");
			}

			// Validate and get the GeoCodes
			if (!ValidateAddress(pickUpAddress)) {
				throw new Exception("Could not get GeoCodes for the given PickUp Address");
			}
			if (!ValidateAddress(dropOffAddress)) {
				throw new Exception("Could not get GeoCodes for the given DropOff Address");
			}

			EntityList<Pricing> oPricing;
			Pricing oPrice;

			try {
				// Check if we have the rates in the DB
				RdbQuery oQry = new RdbQuery(typeof(Pricing), Pricing.PULatitudeEntityColumn, EntityQueryOp.EQ, pickUpAddress.Latitude);
				oQry.AddClause(Pricing.PULongitudeEntityColumn, EntityQueryOp.EQ, pickUpAddress.Longitude);
				oQry.AddClause(Pricing.DOLatitudeEntityColumn, EntityQueryOp.EQ, dropOffAddress.Latitude);
				oQry.AddClause(Pricing.DOLongitudeEntityColumn, EntityQueryOp.EQ, dropOffAddress.Longitude);

				oPricing = mPM.GetEntities<Pricing>(oQry);
				if (oPricing.Count == 0) {
					forceRefetch = true;
					oPrice = NewPrice();
					oPricing.Add(oPrice);
				} else {
					oPrice = oPricing[0];
				}

				if (forceRefetch) {
					CalcFee(oPrice);
				}

				bool isRushHour = false;
				decimal rushHourRate = 0;
				if (RideDateTime != null && oPrice.Rates.RateType.ToLower() != "flat") {
					// need to calc rush hour fee if any
					bool isWeekDay = DateUtils.IsWeekDay((DateTime)RideDateTime);

					DateTime oRideDate = RideDateTime.Value.Date;
					foreach (RushHour oHour in mRushHours) {
						if (oHour.RushHourType.Trim() == "Date") {
							if (oHour.Day.ToString() != "") {
								//DateTime oRushDate = (DateTime)oHour.Day;
								DateTime oRushStart = (DateTime)oHour.Day.Value.Date + oHour.StartTime.Value.TimeOfDay;
								DateTime oRushEnd = (DateTime)oHour.Day.Value.Date + oHour.EndTime.Value.TimeOfDay;
								if (oRushEnd < oRushStart) {
									oRushEnd = oRushEnd.AddDays(1);
								}

								if (oRushStart.Date == RideDateTime.Value.Date || oRushEnd.Date == RideDateTime.Value.Date) {
									if (oPrice.Rates.RushHour >= 1) {
										rushHourRate = oPrice.Rates.RushHour - 1;
									} else {
										rushHourRate = oPrice.Rates.RushHour;
									}

									if (oHour.StartTime == null && oHour.EndTime == null) {
										isRushHour = true;
										break;
									} else {
										if (DateUtils.IsBetween(oRushStart, oRushEnd, RideDateTime.Value)) {
											if (RideDateTime.Value.AddMinutes(oPrice.Duration) > oRushEnd) {
												// find the number of minutes we are over by
												TimeSpan oTime = RideDateTime.Value.AddMinutes(oPrice.Duration).Subtract(oRushEnd);
												isRushHour = true;
												rushHourRate = rushHourRate * (decimal)((oPrice.Duration - oTime.TotalMinutes) / oPrice.Duration);
												break;
											} else {
												isRushHour = true;
												break;
											}
										}
									}

								}
							}
						}

					}
				}

				PricedRide oFare = new PricedRide();
				oQry = new RdbQuery(typeof(CoverageZones), CoverageZones.CoverageZoneIDEntityColumn, EntityQueryOp.EQ, oPrice.PUCoverageZoneID);
				CoverageZones oPriceZone = mPM.GetEntity<CoverageZones>(oQry);
				oFare.PUZone = oPriceZone.CoverageZone;
				oQry = new RdbQuery(typeof(CoverageZones), CoverageZones.CoverageZoneIDEntityColumn, EntityQueryOp.EQ, oPrice.DOCoverageZoneID);
				oPriceZone = mPM.GetEntity<CoverageZones>(oQry);
				oFare.DOZone = oPriceZone.CoverageZone;
				oFare.Duration = oPrice.Duration;
				oFare.Mileage = oPrice.Mileage;
				oFare.BasePrice = oPrice.Fee + oPrice.Rates.DropFee;
				oFare.DropFee = oPrice.Rates.DropFee;
				if (isRushHour) {
					oFare.IsRushHour = true;
					decimal rushHourFee = oPrice.Fee * rushHourRate;
					oFare.BasePrice += rushHourFee;
				}

				if (pickUpAddress.Airport != null && pickUpAddress.Airport.Trim() != "") {
					oFare.BasePrice += oPrice.Rates.Airport;
					oFare.IsAirport = true;
					oFare.AirportFee = oPrice.Rates.Airport;
				}

				decimal OtherFee = oFare.BasePrice * (oPrice.Rates.OtherPct / 100) + oPrice.Rates.Other;
				oFare.BasePrice += OtherFee;
				oFare.OtherFee = OtherFee;

				if (oFare.BasePrice < oPrice.Rates.MinBaseCharge) {
					oFare.BasePrice = oPrice.Rates.MinBaseCharge;
				}
				oFare.BasePrice += oPrice.Rates.TaxiPass;

				// HG 08/24/06 commented out for now
				//decimal Tip = oFare.BasePrice * (oPrice.Rates.TipPct / 100) + oPrice.Rates.Tip;
				//oFare.FullPrice = oFare.BasePrice + Tip + oPrice.Rates.Tolls;
				//oFare.TollsFee = oPrice.Rates.Tolls;
				//oFare.Gratuity = Tip;

				//if (oPrice.Rates.VoucherPct == 0) {
				//    oFare.FullPrice += oPrice.Rates.Voucher;
				//    oFare.VoucherFee = oPrice.Rates.Voucher;
				//} else {
				//    oFare.VoucherFee = (oFare.BasePrice * (oPrice.Rates.VoucherPct / 100));
				//    oFare.FullPrice += oFare.VoucherFee;
				//}

				if (oPrice.Rates.VoucherPct == 0) {
					oFare.BasePrice += oPrice.Rates.Voucher;
					oFare.VoucherFee = oPrice.Rates.Voucher;
				} else {
					oFare.VoucherFee = (oFare.BasePrice * (oPrice.Rates.VoucherPct / 100));
					oFare.BasePrice += oFare.VoucherFee;
				}

				//// hg moved up to base price calc
				////decimal OtherFee = oFare.BasePrice * (oPrice.Rates.OtherPct / 100) + oPrice.Rates.Other;
				////oFare.FullPrice += OtherFee;
				////oFare.OtherFee = OtherFee;

				//decimal TaxiPass = (oFare.FullPrice * oPrice.Rates.TaxiPassFactor) - oFare.FullPrice;
				//TaxiPass += oPrice.Rates.TaxiPass;


				//oFare.FullPrice += TaxiPass;
				oFare.TaxiPassFee = oPrice.Rates.TaxiPass;

				//hg 11/09/06 round up the base price/booking fee/service fee
				oFare.BasePrice = Math.Round(oFare.BasePrice + 0.49M, MidpointRounding.AwayFromZero);

				oFare.ServiceFee = Math.Round(oFare.BasePrice * (oPrice.Rates.ServiceFeePct / 100) + 0.49M);
				oFare.BookingFee = Math.Round(oPrice.Rates.BookingFee + 0.49M);
				//oFare.BookingFee = 0; // snelson 9/12/2006; currently doing a special where we cover the booking fee
				//oFare.BookingFee = Math.Round(oPrice.Rates.BookingFee + 0.49M);
				oFare.FullPrice = oFare.BasePrice + oFare.ServiceFee + oFare.BookingFee;

				mAffiliatesZones = GetAffiliatesZones(oPrice.PUCoverageZoneID, oPrice.DOCoverageZoneID);
				int iRows = 0;
				string affiliateName = "";
				long affiliateID = 0;
				//loop through to see how many Affiliates can do this run
				foreach (AffiliateCoverageZones oZone in mAffiliatesZones) {
					if (this.TestPricing) {
						if (oZone.CoverageZoneID == oPrice.PUCoverageZoneID && oZone.Affiliate.TestFleet) {
							iRows++;
							affiliateName = oZone.Affiliate.Name;
							affiliateID = oZone.AffiliateID;
						}
					} else {
						if (oZone.CoverageZoneID == oPrice.PUCoverageZoneID && !oZone.Affiliate.DisablePricing && !oZone.Affiliate.TestFleet) {
							iRows++;
							affiliateName = oZone.Affiliate.Name;
							affiliateID = oZone.AffiliateID;
						}
					}
				}
				if (iRows == 0) {
					//no one can do this run, do we have an affilate in dropoff zone that can pick this person up?
					foreach (AffiliateCoverageZones oZone in mAffiliatesZones) {
						if (this.TestPricing) {
							if (oZone.CoverageZoneID == oPrice.DOCoverageZoneID && oZone.Affiliate.PickUpAnywhere.GetValueOrDefault(false) && oZone.Affiliate.TestFleet) {
								iRows++;
								affiliateName = oZone.Affiliate.Name;
								affiliateID = oZone.AffiliateID;
							}
						} else {
							if (oZone.CoverageZoneID == oPrice.DOCoverageZoneID && oZone.Affiliate.PickUpAnywhere.GetValueOrDefault(false) && !oZone.Affiliate.DisablePricing && !oZone.Affiliate.TestFleet) {
								iRows++;
								affiliateName = oZone.Affiliate.Name;
								affiliateID = oZone.AffiliateID;
							}
						}
					}
					if (iRows == 1) {
						oFare.TaxiCabName = affiliateName;
						oFare.TaxiCabID = affiliateID;
						oFare.IsBlackOut = CheckBlackOuts(mAffiliatesZones, affiliateID, RideDateTime.Value.Date);

					} else if (iRows > 1) {
						throw new Exception("More than 1 record found, need to figure out rules and Implement");
					} else {
						throw new Exception("No availability");
					}
				} else if (iRows == 1) {
					oFare.TaxiCabID = affiliateID;
					oFare.TaxiCabName = affiliateName;
					oFare.IsBlackOut = CheckBlackOuts(mAffiliatesZones, affiliateID, RideDateTime.Value.Date);
				} else {
					throw new Exception("More than 1 record found, need to figure out rules and Implement");
				}

				oFare.RideDateTime = (DateTime)RideDateTime;
				oFare.PriceKey = StringUtils.GetUniqueKey(12);
				return oFare;
			} catch (Exception ex) {
				throw ex;
			}

		}

		private bool CheckBlackOuts(EntityList<AffiliateCoverageZones> mAffiliatesZones, long affiliateID, DateTime rideDate) {
			bool IsBlackOut = false;

			foreach (AffiliateCoverageZones oZone in mAffiliatesZones) {
				if (affiliateID == oZone.AffiliateID) {
					ReadOnlyEntityList<BlackOutDates> oDates = oZone.Affiliate.BlackOutDateses;
					foreach (BlackOutDates oDate in oDates) {
						if (rideDate.Date == oDate.Day) {
							IsBlackOut = true;
						}
					}
				}
			}
			return IsBlackOut;
		}

		private Pricing NewPrice() {
			Pricing oPrice = mPM.CreateEntity<Pricing>();
			oPrice.PULatitude = pickUpAddress.Latitude.ToString();
			oPrice.PULongitude = pickUpAddress.Longitude.ToString();
			oPrice.DOLatitude = dropOffAddress.Latitude.ToString();
			oPrice.DOLongitude = dropOffAddress.Longitude.ToString();
			oPrice.AddToManager();
			mPricing.Add(oPrice);
			return oPrice;
		}

		private void CalcFee(Pricing oPrice) {

			try {
				// Use GeoCode to get the Routing
				if (oPrice.Mileage < 1) {
					DrivingDistance oRoute;
					//if (mMappingService == MappingService.MapPoint) {
						if (mMapPointService == null) {
							SystemDefaults oDefaults = SystemDefaults.GetDefaults(oPrice.PersistenceManager);
							mMapPointService = new MapPointService(oDefaults.MapPointUserName, oDefaults.MapPointPassword);
						}
						oRoute = mMapPointService.GetDistance(pickUpAddress, dropOffAddress);
					//} else {
						//if (mMapQuestService == null) {
						//	mMapQuestService = new MapQuestService();
						//}
						//oRoute = mMapQuestService.GetDistance(pickUpAddress, dropOffAddress);
					//}

					oPrice.Duration = Convert.ToInt32(oRoute.TravelTime);
					oPrice.Mileage = Convert.ToInt32(oRoute.Distance);
					if (oPrice.Mileage == 0) {
						pickUpAddress.Latitude = 0;
						pickUpAddress.Longitude = 0;
						ValidateAddress(pickUpAddress);
						dropOffAddress.Latitude = 0;
						dropOffAddress.Longitude = 0;
						ValidateAddress(dropOffAddress);
						if (pickUpAddress.FormattedAddress == dropOffAddress.FormattedAddress) {
							throw new Exception("Pickup and Drop off address cannot be the same");
						}
						CalcFee(oPrice);
					}
				}

				// get coverage zones
				EntityList<CoverageArea> oPUAreas = GetCoverageAreas(pickUpAddress);
				EntityList<CoverageArea> oDOAreas = null;
				try {
					oDOAreas = GetCoverageAreas(dropOffAddress);
				} catch {
					// need to use other areas
				}

				long puZone = 0;
				long doZone = 0;
				bool bFoundRate = false;
				EntityList<Rates> oRates = null;
				if (oDOAreas != null) {
					foreach (CoverageArea puArea in oPUAreas) {
						puZone = puArea.CoverageZoneID;

						foreach (CoverageArea doArea in oDOAreas) {
							doZone = doArea.CoverageZoneID;
							oRates = GetRates(puZone, doZone);
							if (oRates.Count > 0) {
								bFoundRate = true;
								break;
							}
						}

						if (bFoundRate) {
							break;
						}
					}
				}

				if (!bFoundRate) {
					foreach (CoverageArea puArea in oPUAreas) {
						puZone = puArea.CoverageZoneID;
						doZone = OtherAreas;
						oRates = GetRates(puZone, doZone);
						if (oRates.Count > 0) {
							bFoundRate = true;
							break;
						}
					}
				}

				if (oRates.Count == 0) {
					throw new Exception("No rates defined for coverage zones: " + oPUAreas[0].CoverageZones_CoverageArea.CoverageZone + " -> " + oDOAreas[0].CoverageZones_CoverageArea.CoverageZone);
				}

				// Get the rates to calculate the base price
				//long puZone = oPUAreas[0].CoverageZoneID;
				//long doZone = OtherAreas;
				//if (oDOAreas != null) {
				//    doZone = oDOAreas[0].CoverageZoneID;
				//}
				//EntityList<Rates> oRates = GetRates(puZone, doZone);

				//if (oRates.Count == 0) {
				//    doZone = OtherAreas;
				//    oRates = GetRates(puZone, doZone);
				//    if (oRates.Count == 0) {
				//        throw new Exception("No rates defined for coverage zones: " + oPUAreas[0].CoverageZones_CoverageArea.CoverageZone + " -> " + oDOAreas[0].CoverageZones_CoverageArea.CoverageZone);
				//    }
				//}
				oPrice.PUCoverageZoneID = puZone;
				oPrice.DOCoverageZoneID = doZone;

				// now to do the calc
				Rates oRate = oRates[0];
				//decimal basePrice;
				decimal fee;
				if (oRate.RateType.ToLower().Trim() == "flat") {
					fee = oRate.Rate;
					if (oPrice.Mileage > oRate.MinShortRide) {
						if (oRate.LongRide != 0) {
							fee = fee * oRate.LongRide;
						}
					} else {
						if (oRate.ShortRide != 0) {
							fee = fee * oRate.ShortRide;
						}
					}

					//basePrice = oRate.DropFee + fee;
					//if (pickUpAddress.Airport != "") {
					//    basePrice += oRate.Airport;
					//}
				} else {
					fee = oPrice.Mileage * oRate.Rate;
					if (oPrice.Mileage > oRate.MinShortRide) {
						if (oRate.LongRide != 0) {
							fee *= oRate.LongRide;
						}
					} else {
						if (oRate.ShortRide != 0) {
							fee *= oRate.ShortRide;
						}
					}
				}
				oPrice.Fee = fee;
				mPM.SaveChanges(mPricing);

			} catch (Exception ex) {
				switch (oPrice.RowState) {
					case System.Data.DataRowState.Added:
						oPrice.Delete();
						break;
					case System.Data.DataRowState.Modified:
						oPrice.Undo();
						break;
				}
				throw ex;
			}

		}

		private EntityList<Rates> GetRates(long pPUZone, long pDOZone) {
			RdbQuery oQry = new RdbQuery(typeof(Rates), Rates.PUCoverageZoneIDEntityColumn, EntityQueryOp.EQ, pPUZone);
			oQry.AddClause(Rates.DOCoverageZoneIDEntityColumn, EntityQueryOp.EQ, pDOZone);

			return mPM.GetEntities<Rates>(oQry);
		}

		private EntityList<AffiliateCoverageZones> GetAffiliatesZones(long pPUZone, long pDOZone) {
			RdbQuery oQry = new RdbQuery(typeof(AffiliateCoverageZones), AffiliateCoverageZones.CoverageZoneIDEntityColumn, EntityQueryOp.EQ, pPUZone);
			EntityList<AffiliateCoverageZones> oZones = mPM.GetEntities<AffiliateCoverageZones>(oQry);

			if (oZones.Count == 0) {
				oQry = new RdbQuery(typeof(AffiliateCoverageZones), AffiliateCoverageZones.CoverageZoneIDEntityColumn, EntityQueryOp.EQ, pDOZone);
				oZones = mPM.GetEntities<AffiliateCoverageZones>(oQry);
			}

			return oZones;
		}

		private POI GetPOI(string sPOI_ID) {
			try {
				RdbQuery oQry = new RdbQuery(typeof(POI), POI.PoiidEntityColumn, EntityQueryOp.EQ, sPOI_ID);
				EntityList<POI> oPOIs = mPM.GetEntities<POI>(oQry);
				if (oPOIs.Count == 0)
					return null;
				else
					return oPOIs[0];

			} catch (Exception ex) {
				throw ex;
			}
		}

		private ReadOnlyEntityList<CoverageArea> GetCoverageAreas_Old(Address oAddress) {
			RdbQuery oQry = new RdbQuery(typeof(ZIPCode), ZIPCode.ZIPCodeEntityColumn, EntityQueryOp.EQ, oAddress.ZIP.Trim());
			oQry.AddClause(ZIPCode.CountryEntityColumn, EntityQueryOp.EQ, oAddress.Country);
			ZIPCode oZIP = mPM.GetEntity<ZIPCode>(oQry);
			if (oZIP == null) {
				throw new Exception("Could not calc missing Country, ZIP Code: " + oAddress.Country + ", " + oAddress.ZIP);
			}
			if (oZIP.CoverageAreas.Count == 0) {
				throw new Exception("No coverage zones defined for pickup of " + oAddress.ZIP + ", " + oAddress.Country);
			}
			return oZIP.CoverageAreas;
		}

		private EntityList<CoverageArea> GetCoverageAreas(Address oAddress) {
			RdbQuery oQry = new RdbQuery(typeof(CoverageArea), CoverageArea.ZIPCodeEntityColumn, EntityQueryOp.EQ, oAddress.ZIP.Trim());
			oQry.AddClause(CoverageArea.CountryEntityColumn, EntityQueryOp.EQ, oAddress.Country.Trim());
			EntityList<CoverageArea> oAreas = mPM.GetEntities<CoverageArea>(oQry, QueryStrategy.DataSourceThenCache);
			//if (oZIP == null) {
			//    throw new Exception("Could not calc missing Country, ZIP Code: " + oAddress.Country + ", " + oAddress.ZIP);
			//}
			if (oAreas.Count == 0) {
				throw new Exception("No coverage zones defined for " + oAddress.ZIP.Trim() + ", " + oAddress.Country);
			}
			return oAreas;
		}

		private bool ValidateAddress(Address oAddress) {

			try {
				////get the data from the POI table if it exists
				//if (oAddress.ID != null) {
				//    POI oPOI = GetPOI(oAddress.ID);
				//    if (oPOI != null) {
				//        oAddress.Street = oPOI.Line1;
				//        oAddress.Country = oPOI.Country;
				//        oAddress.ZIP = oPOI.ZIPCode;
				//        oAddress.Latitude = Convert.ToDouble(oPOI.Latitude);
				//        oAddress.Longitude = Convert.ToDouble(oPOI.Longitude);
				//    }
				//}
				if (oAddress.Airport != null && oAddress.Airport.Trim() != "" && (oAddress.ZIP == null || oAddress.ZIP.Trim() == "")) {
					// ZIPCode missing find and add ZIP Code to the airport table
					RdbQuery oQry = new RdbQuery(typeof(CoverageZones), CoverageZones.CoverageZoneEntityColumn, EntityQueryOp.EQ, oAddress.Airport);
					CoverageZones oZone = mPM.GetEntity<CoverageZones>(oQry);
					if (!oZone.IsNullEntity) {
						oQry = new RdbQuery(typeof(Airports), Airports.AirportEntityColumn, EntityQueryOp.EQ, oAddress.Airport);
						Airports oAirport = mPM.GetEntity<Airports>(oQry);
						if ((oAirport.ZIPCode == "" || oAirport.Country == "") && oZone.CoverageAreas.Count > 0) {
							oAirport.ZIPCode = oZone.CoverageAreas[0].ZIPCode;
							oAirport.Country = oZone.CoverageAreas[0].Country;
							mPM.SaveChanges(mPM.GetEntities<Airports>(System.Data.DataRowState.Modified));
						}
						oAddress.Country = oAirport.Country;
						oAddress.ZIP = oAirport.ZIPCode;
						oAddress.Latitude = oAirport.Latitude;
						oAddress.Longitude = oAirport.Longitude;
					}
				}
				if (oAddress.Latitude == 0 || oAddress.Longitude == 0) {
					if (mMappingService == MappingService.MapPoint) {
						if (mMapPointService == null) {
							SystemDefaults oDefaults = SystemDefaults.GetDefaults(mPM);
							mMapPointService = new MapPointService(oDefaults.MapPointUserName, oDefaults.MapPointPassword);
						}
						mMapPointService.FindAddress(oAddress);
					} else {
						//if (mMapQuestService == null) {
						//	mMapQuestService = new MapQuestService();
						//}
						//mMapQuestService.FindAddress(oAddress);
					}
				}
			} catch (Exception ex) {
				throw ex;
			}

			return (oAddress.Latitude != 0 && oAddress.Longitude != 0);
		}



		//private class Rates

		//getting a result from USPS API
		//http://testing.shippingapis.com/ShippingAPITest.dll?API=Verify&XML=<AddressValidateRequest%20USERID="349TAXIP6139"><Address%20ID="0"><Address1></Address1><Address2>6406%20Ivy%20Lane</Address2><City>Greenbelt</City><State>MD</State><Zip5></Zip5><Zip4></Zip4></Address></AddressValidateRequest>
	}
	[Serializable]
	public class PricedRide {
		public string PriceKey = "";
		public decimal BasePrice = 0;
		public decimal FullPrice = 0;
		public decimal TaxiPassFee = 0;
		public decimal DropFee = 0;
		public decimal AirportFee = 0;
		public decimal VoucherFee = 0;
		public decimal TollsFee = 0;
		public decimal Gratuity = 0;
		public decimal OtherFee = 0;
		public decimal DiscountFee = 0;
		public decimal ServiceFee = 0;
		public decimal BookingFee = 0;
		public int Mileage = 0;
		public int Duration = 0;
		public bool IsAirport = false;
		public bool IsRushHour = false;
		public decimal Discount = 0;
		public string PUZone = "";
		public string DOZone = "";
		public bool IsBlackOut = false;
		public string TaxiCabName = "";
		public long TaxiCabID = 0;
		public DateTime RideDateTime;
		public string ErrorMessage = "";
		public int ServiceFeePct {
			get {
				int fee = 0;
				if (BasePrice != 0) { // avoid divide by zero error
					fee = (int)(Decimal.Round((ServiceFee / BasePrice * 100), 0));
				}
				return fee;
			}
		}
	}
}
