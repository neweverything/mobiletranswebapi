using System;
using System.Collections.Generic;
using System.Text;

namespace TaxiPassCommon.Banking {
    public class PayInfo {
        
        public string BankAccountHolder;
        public string BankAddress;
        public string BankSuite;
        public string BankCity;
        public string BankState;
        public string BankZIPCode;
        public bool BankAccountPersonal;
        public string BankCode;
        public string BankAccountNumber;
        //public string MerchantNo;
        public Merchant MyMerchant;
        public string PaidTo;

        public bool NachaFile { get; set; }

		public bool WireTransfer { get; set; }

        public string TransactionKey {
            get {
                return this.MyMerchant.MerchantNo + "_" + this.BankCode + "_" + this.BankAccountNumber;
            }
        }

        public string FirstName {
            get {
                if (BankAccountHolder.Contains(" ")) {
                    return BankAccountHolder.Split(' ')[0];
                }
                return BankAccountHolder;
            }
        }

        public string LastName {
            get {
                if (BankAccountHolder.Contains(" ")) {
                    return BankAccountHolder.Split(' ')[1];
                }
                return BankAccountHolder;
            }
        }

    }
}
