﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace MobileTransWebAPI.Controllers {

#if !WINDOWS_PHONE
	[Serializable]
	[ObfuscationAttribute(Exclude = true, ApplyToMembers = true)]
#endif
	public partial class DriverInfo {
		public double AndroidAppVersion { get; set; }
		public double MinAppVersion { get; set; }

		public string AndroidUpdatedTime { get; set; }
		public int AndroidUpdateDelay { get; set; }

		public int UpdateDriverProfileTimer { get; set; }

		public string ErrorMsg { get; set; }
		public bool DeviceFrozen { get; set; }

		public string DriverName { get; set; }
		public string City { get; set; }
		public string State { get; set; }
		public string Cell { get; set; }

		public long DriverID { get; set; }
		public string DriverPin { get; set; }
		public string HackNo { get; set; }


		public long AffiliateDriverID { get; set; }
		public long AffiliateID { get; set; }
		public string CardDataEntryType { get; set; }
		public bool ForceLogin { get; set; }
		public string AffiliateName { get; set; }
		public string ServiceType { get; set; }
		public int VehiclesCount { get; set; }
		public int DriversCount { get; set; }
		public string TaxiMeterUsageType { get; set; }
		public bool Active { get; set; }
		public bool PaidByRedeemer { get; set; }
		public string FraudMessage { get; set; }
		public bool DispatchJobs { get; set; }
		public bool VivoTech { get; set; }
		public string StatusList { get; set; }
		public string TaxiPayPin { get; set; }
		public string TaxiPayCell { get; set; }
		public bool TPFeeOnFareOnly { get; set; }
		public decimal ChargeMaxAmount { get; set; }
		public decimal ChargeMaxSwipeAmount { get; set; }
		public string TPFeeMsg { get; set; }
		public string WeakSignalMessage { get; set; }
		public string PrinterType { get; set; }
		public decimal AutoPreAuthPct { get; set; }
		public decimal ReceiptMaxAmt { get; set; }
		public bool ReceiptDisplayMaxAmt { get; set; }
		public string AdminCode { get; set; }
		public decimal StartingFare { get; set; }

		public int GPSUpdateTime { get; set; }
		public int GPSMaxUpdateTime { get; set; }

		public decimal PassengerVerificationAmt { get; set; }
		public string PassengerVerificationMsg { get; set; }
		public int GPSWarningRepeatTime { get; set; }
		public int GPSWarningTime { get; set; }
		public string GPSWarningMsg { get; set; }
		public decimal MaxTip { get; set; }
		public string ReceiptTitle { get; set; }
		public bool NoPassengerFee { get; set; }
		public bool AutoTipDriverFee { get; set; }
		public decimal AutoTipPercent { get; set; }
		public string WAPUrl { get; set; }
		public bool DisableCharging { get; set; }
		public bool ChargingJob { get; set; }
		public string DriverNo { get; set; }
		public string ReceiptPhoneNo { get; set; }
		public bool InBackSeat { get; set; }

		public bool TrackTripLog { get; set; }
		public bool FareFirst { get; set; }

		public object TPFeesList { get; set; }
		public object CardTypeList { get; set; }
		public object BlackListedCards { get; set; }

		public double DomainNamesLastUpdated { get; set; }
		public double DomainTopLevelLastUpdated { get; set; }
		public decimal SignatureRequiredAmt { get; set; }

		public bool TrackSwipeCounts { get; set; }

		public string VehicleNo { get; set; }
		public long VehicleID { get; set; }
		public string VehicleKeyCode { get; set; }
		public string VehicleRFID { get; set; }

		public bool HasPayCard { get; set; }
		public bool DriverInitiatedPayment { get; set; }

		public bool AskWhoPaysTPFee { get; set; }
		public decimal DisplayDriverFeeMsgAmount { get; set; }
		public string DriverFeeMsg { get; set; }

		public string NewSignUpMsg { get; set; }
		public string NewDriverWelcomeMsg { get; set; }

		public bool AskForCabNo { get; set; }
		public string NewSignUpLiveCall { get; set; }

		public bool IsRedeemer { get; set; }

		public bool CreateMasterVoucher { get; set; }
		public bool DispatchCarmel { get; set; }
		public bool DispatchCCSi { get; set; }

		public decimal DriverPaySwipeFee { get; set; }
		public decimal DriverPaySwipeFeePerAmount { get; set; }

		public string FleetName { get; set; }

		public bool OnBoardingRequired { get; set; }
		public bool HasBankInfo { get; set; }

		// Ride availablility and time to become available
		public bool DispatchAvailable { get; set; }
		public string DispatchAvailableTime { get; set; }



		public List<string> OnBoardingMessages = new List<string>();
		public List<string> PushMessages = new List<string>();

		public bool ChargeKioskSlip { get; set; }

		//public bool AvailableForDispatch { get; set; }
		//public string AvailableDateTime { get; set; }

		public bool CanSubmitCheck { get; set; }

		public Dictionary<string, string> Settings { get; set; }

		public string Email { get; set; }
		public int MaxPassengers { get; set; }
		public string PickUpLocations { get; set; }

		public decimal AuthAmount { get; set; }

		public int StoreForward { get; set; }
//		public bool AllowAirplaneMode { get; set; }

		public List<string> vehicleNoList = new List<string>();
		public bool LoginFailed { get; set; }

        public bool AskWorkingCity { get; set; }
    }


}