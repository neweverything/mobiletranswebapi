﻿using CabRideEngineDapper.Utils;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using TaxiPassCommon;

namespace CabRideEngineDapper {
    public partial class ReservationStatus {

        [Editable(false)]
        public DateTime StatusTimeLocal {
            get {
                return StatusTime.AddHours(-(double)UTCOffset);
            }
        }

        [Editable(false)]
        public DateTime GPSTimeLocal {
            get {
                if (GPSTime.HasValue) {
                    return GPSTime.Value.AddHours(-(double)UTCOffset);
                } else {
                    return StatusTimeLocal;
                }
            }
        }


        public static ReservationStatus Create(Reservations pReservation) {
            ReservationStatus rec = ReservationStatusTable.Create();

            try {
                rec.ReservationID = pReservation.ReservationID;
                rec.StatusTime = DateTime.Now.ToUniversalTime();
            } catch (Exception ex) {
                throw ex;
            }
            return rec;
        }


        public override int Save(string sModifiedBy) {

            try {
                if (this.Address.IsNullOrEmpty()) {
                    FillAddress();
                }
            } catch {
            }
            return base.Save(sModifiedBy);
        }

        public void FillAddress() {

            if (this.Latitude != 0 && this.Longitude != 0) {
                try {
                    com.TaxiPass.GeoCodeWS.GeoCodeService oService = new com.TaxiPass.GeoCodeWS.GeoCodeService();
                    com.TaxiPass.GeoCodeWS.Address oAddr = oService.GetAddressByGeoCode(this.Latitude.ToString(), this.Longitude.ToString());
                    this.Address = oAddr.Street;
                    if (oAddr.Distance > 0) {
                        this.Address += " within " + oAddr.Distance + " feet";
                    }
                } catch {
                }
            }

        }


        public static List<ReservationStatus> GetStatus(SqlConnection pConn, long pReservationID) {
            string sql = @"SELECT * FROM  dbo.ReservationStatus
							WHERE ReservationID = @ReservationID";

            return pConn.Query<ReservationStatus>(sql, new { ReservationID = pReservationID }).ToList();
        }


        public static ReservationStatus GetStatus(SqlConnection pConn, long pReservationID, string pStatus) {
            string sql = @"SELECT * FROM dbo.ReservationStatus
							WHERE ReservationID = @ReservationID  AND Status = @Status";

            return pConn.Query<ReservationStatus>(sql, new { ReservationID = pReservationID, Status = pStatus }).DefaultIfEmpty(ReservationStatus.GetNullEntity()).FirstOrDefault();
        }

        public static List<ReservationStatus> GetStatusList(Reservations pReservation, string pStatusType) {
            string sql = @"SELECT  *
                            FROM    ReservationStatus
                            WHERE   ReservationID = @ReservationID";

            DynamicParameters parms = new DynamicParameters();
            parms.Add("ReservationID", pReservation.ReservationID);
            if (!pStatusType.IsNullOrEmpty()) {
                sql += " AND Status LIKE @StatusType";
                if (!pStatusType.EndsWith("%")) {
                    pStatusType += "%";
                }
                parms.Add("Status", pStatusType);
            }
            using (var conn = SqlHelper.OpenSqlConnection()) {
                return conn.Query<ReservationStatus>(sql, parms).ToList();
            }
        }
    }
}
