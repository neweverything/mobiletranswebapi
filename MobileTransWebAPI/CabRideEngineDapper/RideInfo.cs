﻿using CabRideEngineDapper.Utils;
using Dapper;
using System;

namespace CabRideEngineDapper {
    public partial class RideInfo {

        private DriverPayments mDriverPayments;
        public DriverPayments DriverPayments {
            get {
                if (mDriverPayments == null) {
                    using (var conn = SqlHelper.OpenSqlConnection()) {
                        mDriverPayments = DriverPayments.GetPayment(conn, this.DriverPaymentID);
                    }
                }
                return mDriverPayments;
            }
        }

        public static RideInfo Create(DriverPayments pPayment) {
            RideInfo rec = null;

            try {
                rec = RideInfoTable.Create();
                rec.DriverPaymentID = pPayment.DriverPaymentID;
                pPayment.RideInfo = rec;
            } catch (Exception ex) {
                throw ex;
            }
            return rec;
        }

        public override int Save() {
            return Save(CabRideDapperSettings.LoggedInUser);
        }

        public override int Save(string pModifiedBy) {
            int result = 0;
            using (var conn = SqlHelper.OpenSqlConnection()) {
                ModifiedDate = DateTime.UtcNow;
                ModifiedBy = pModifiedBy;
                if (IsNewRecord) {
                    this.RideInfoID = NextID.GetNextKey(RideInfo.TableDef.TABLE_NAME);
                    int? id = conn.Insert(this);

                    result = id.Value;
                } else {
                    RowVersion++;
                    result = conn.Update(this, additionalWhere: "RowVersion = " + (RowVersion - 1));
                }
            }
            return result;
        }

        public static RideInfo GetByDriverPaymentID(long pDriverPaymentID) {
            string sql = @"SELECT * FROM  dbo.RideInfo
                            WHERE DriverPaymentID = @DriverPaymentID";

            using (var conn = SqlHelper.OpenSqlConnection()) {
                return conn.Get<RideInfo>(sql, new { DriverPaymentID = pDriverPaymentID });
            }
        }

    }
}
