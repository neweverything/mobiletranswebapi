﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Util;
using IdeaBlade.Persistence.Rdb;

using CabRideEngine.Utils;
using TaxiPassCommon;
using TaxiPassCommon.Banking;

using Csla.Validation;

namespace CabRideEngine {
    /// <summary>
    /// The developer class representing the CouponCodes business object class.
    /// </summary>
    /// <remarks>
    /// <para>Place all application-specific business logic in this class.</para>
    /// <para>
    /// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
    /// and will not overwrite changes.
    /// </para>
    /// <para>
    /// This class inherits from the generated class, its companion in the pair of classes 
    /// defining a business object. Properties in the generated class may be overridden in 
    /// order to change the behavior or add business logic.
    /// </para>
    /// <para>
    /// This class is the final class in the inheritance chain for this business object. 
    /// It should not be inherited.
    /// </para>
    /// </remarks>
    [Serializable]
    public sealed partial class CouponCodes : CouponCodesDataRow {

        #region Constructors -- DO NOT MODIFY

        // Do not create constructors for this class
        // Developers cannot create instances of this class with the "new" keyword
        // You should write a static Create method instead; see the "Suggested Customization" region
        // Please review the documentation to learn about entity creation and inheritance

        // This private constructor prevents accidental instance creation via 'new';
        // it is also required if the class is visible from a web service.
        // DO NOT REMOVE **
        private CouponCodes()
            : this(null) {
        }

        /// <summary>
        /// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
        /// </summary>
        /// <param name="pRowBuilder"></param>
        public CouponCodes(DataRowBuilder pRowBuilder)
            : base(pRowBuilder) {
        }

        #endregion

        #region Suggested Customizations

        //    // Use this factory method template to create new instances of this class
        //    public static CouponCodes Create(PersistenceManager pManager,
        //      ... your creation parameters here ...) { 
        //
        //      CouponCodes aCouponCodes = pManager.CreateEntity<CouponCodes>();
        //
        //      // if this object type requires a unique id and you have implemented
        //      // the IIdGenerator interface implement the following line
        //      pManager.GenerateId(aCouponCodes, // add id column here //);
        //
        //      // Add custom code here
        //
        //      aCouponCodes.AddToManager();
        //      return aCouponCodes;
        //    }

        //    // Implement this method if you want your object to sort in a specific order
        //    public override int CompareTo(Object pObject) {
        //    }

        //    // Implement this method to customize the null object version of this class
        //    protected override void UpdateNullEntity() {
        //    }

        #endregion

        #region Standard Entity logic to create/audit/validate the entity
        public static CouponCodes Create(PersistenceManager pPM) {
            CouponCodes oCoupon = null;

            try {
                oCoupon = pPM.CreateEntity<CouponCodes>();

                // Using the IdeaBlade Id Generation technique
                pPM.GenerateId(oCoupon, CouponCodes.CouponCodeIDEntityColumn);
                oCoupon.AddToManager();

            } catch (Exception ex) {
                throw ex;
            }
            return oCoupon;
        }

        protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
            if (IsDeserializing) {
                return;
            }
            base.OnColumnChanging(pArgs);
            DoAudit(pArgs);
        }

        private void DoAudit(DataColumnChangeEventArgs pArgs) {
            if (!this.PersistenceManager.IsClient) {
                return;
            }
            if (this.RowState == DataRowState.Added) {
                return;
            }
            if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
                AuditRecord = CouponCodesAudit.Create(this.PersistenceManager, this);
            }
        }

        [BindingBrowsable(false)]
        public override string EntityInstanceDisplayName {
            get {
                return this.CouponCode;
            }
        }

        protected override void AddRules(RuleList pList) {
            PropertyRequiredRule.AddToList(pList, CouponCodeEntityColumn.ColumnName);
            pList.Add(IsCouponCodeUnique, CouponCodeEntityColumn.ColumnName);
        }

        private bool IsCouponCodeUnique(object pTarget, RuleArgs e) {
            CouponCodes oCoupon = pTarget as CouponCodes;
            if (string.IsNullOrEmpty(oCoupon.CouponCode)) {
                return false;
            }
            RdbQuery qry = new RdbQuery(typeof(CouponCodes), CouponCodes.CouponCodeEntityColumn, EntityQueryOp.EQ, oCoupon.CouponCode);
            qry.AddClause(CouponCodes.CouponCodeIDEntityColumn, EntityQueryOp.NE, oCoupon.CouponCodeID);
            EntityList<CouponCodes> oList = this.PersistenceManager.GetEntities<CouponCodes>(qry);
            if (oList.Count > 0) {
                e.Description = "A coupon already exists with this code: " + oCoupon.CouponCode;
                return false;
            }
            return true;
        }

        #endregion

        #region Retrieve Records

        public static EntityList<CouponCodes> GetAll(PersistenceManager pPM) {
            EntityList<CouponCodes> oList = pPM.GetEntities<CouponCodes>();
            oList.ApplySort(CouponCodes.CouponCodeEntityColumn.ColumnName, System.ComponentModel.ListSortDirection.Ascending, false);
            return oList;
        }

        public static CouponCodes GetCoupon(PersistenceManager pPM, string pCode) {
            RdbQuery qry = new RdbQuery(typeof(CouponCodes), CouponCodeEntityColumn, EntityQueryOp.EQ, pCode);
            return pPM.GetEntity<CouponCodes>(qry);
        }

        #endregion


    }

    #region EntityPropertyDescriptors.CouponCodesPropertyDescriptor
    ////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
    ////    For example: 
    ////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
    //public static partial class EntityPropertyDescriptors {
    //	public partial class CouponCodesPropertyDescriptor : BaseEntityPropertyDescriptor {
    //    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
    //    public AdaptedPropertyDescriptor Age {
    //      get { return this.Get("Age"); }
    //    }
    //  }
    //} 
    #endregion

}
