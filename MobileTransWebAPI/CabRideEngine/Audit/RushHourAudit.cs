﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Util;

namespace CabRideEngine {
    [Serializable]
    public sealed class RushHourAudit : RushHourAuditDataRow {

        #region Constructors -- DO NOT MODIFY
        // Do not create constructors for this class
        // Developers cannot create instances of this class with the "new" keyword
        // You should write a static Create method instead; see the "Suggested Customization" region
        // Please review the documentation to learn about entity creation and inheritance

        // This private constructor prevents accidental instance creation via 'new'
        private RushHourAudit() : this(null) { }

        // Typed DataSet constructor (needed by framework) - DO NOT REMOVE
        public RushHourAudit(DataRowBuilder pRowBuilder)
            : base(pRowBuilder) {
        }

        #endregion

        #region Suggested Customizations

        //    // Use this factory method template to create new instances of this class
        //    public static RushHourAudit Create(PersistenceManager pManager,
        //      ... your creation parameters here ...) { 
        //
        //      RushHourAudit aRushHourAudit = pManager.CreateEntity<RushHourAudit>();
        //
        //      // if this object type requires a unique id and you have implemented
        //      // the IIdGenerator interface implement the following line
        //      pManager.GenerateId(aRushHourAudit, // add id column here //);
        //
        //      // Add custom code here
        //
        //      aRushHourAudit.AddToManager();
        //      return aRushHourAudit;
        //    }

        //    // Implement this method if you want your object to sort in a specific order
        //    public override int CompareTo(Object pObject) {
        //    }

        //    // Implement this method to customize the null object version of this class
        //    protected override void UpdateNullEntity() {
        //    }

        #endregion

        // Add additional logic to your business object here...

        public static RushHourAudit Create(PersistenceManager pManager, RushHour pRushHour) {
            RushHourAudit oAudit = (RushHourAudit)pManager.CreateEntity(typeof(RushHourAudit));
            //pManager.GenerateId(oAudit, RushHourAudit.RushHourHistoryIdEntityColumn);

            for (int i = 0; i < pRushHour.Table.Columns.Count; i++) {
                if (oAudit.Table.Columns.Contains(pRushHour.Table.Columns[i].ColumnName)) {
                    oAudit[pRushHour.Table.Columns[i].ColumnName] = pRushHour[i];
                }
            }
            oAudit.AddToManager();

            return oAudit;
        }

    }

}
