﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using IdeaBlade.Persistence;
using IdeaBlade.Rdb;
using IdeaBlade.Persistence.Rdb;
using IdeaBlade.Util;

using Csla.Validation;

using TaxiPassCommon;

namespace CabRideEngine {
    /// <summary>
    /// The developer class representing the RedeemerFees business object class.
    /// </summary>
    /// <remarks>
    /// <para>Place all application-specific business logic in this class.</para>
    /// <para>
    /// The <b>IdeaBlade DevForce Object Mapping Tool</b> generates this class once 
    /// and will not overwrite changes.
    /// </para>
    /// <para>
    /// This class inherits from the generated class, its companion in the pair of classes 
    /// defining a business object. Properties in the generated class may be overridden in 
    /// order to change the behavior or add business logic.
    /// </para>
    /// <para>
    /// This class is the final class in the inheritance chain for this business object. 
    /// It should not be inherited.
    /// </para>
    /// </remarks>
    [Serializable]
    public sealed partial class RedeemerFees : RedeemerFeesDataRow {

        #region Constructors -- DO NOT MODIFY

        // Do not create constructors for this class
        // Developers cannot create instances of this class with the "new" keyword
        // You should write a static Create method instead; see the "Suggested Customization" region
        // Please review the documentation to learn about entity creation and inheritance

        // This private constructor prevents accidental instance creation via 'new';
        // it is also required if the class is visible from a web service.
        // DO NOT REMOVE **
        private RedeemerFees() : this(null) { }

        /// <summary>
        /// ** Typed DataSet constructor (needed by framework) - DO NOT REMOVE **
        /// </summary>
        /// <param name="pRowBuilder"></param>
        public RedeemerFees(DataRowBuilder pRowBuilder)
            : base(pRowBuilder) {
        }

        #endregion

        #region Suggested Customizations

        //    // Use this factory method template to create new instances of this class
        //    public static RedeemerFees Create(PersistenceManager pManager,
        //      ... your creation parameters here ...) { 
        //
        //      RedeemerFees aRedeemerFees = pManager.CreateEntity<RedeemerFees>();
        //
        //      // if this object type requires a unique id and you have implemented
        //      // the IIdGenerator interface implement the following line
        //      pManager.GenerateId(aRedeemerFees, // add id column here //);
        //
        //      // Add custom code here
        //
        //      aRedeemerFees.AddToManager();
        //      return aRedeemerFees;
        //    }

        //    // Implement this method if you want your object to sort in a specific order
        //    public override int CompareTo(Object pObject) {
        //    }

        //    // Implement this method to customize the null object version of this class
        //    protected override void UpdateNullEntity() {
        //    }

        #endregion

        // Add additional logic to your business object here...
        public static RedeemerFees Create(TaxiPassRedeemerAccounts pRedeemerAccount) {
            RedeemerFees rec = null;
            try {
                rec = pRedeemerAccount.PersistenceManager.CreateEntity<RedeemerFees>();

                // Using the IdeaBlade Id Generation technique
                pRedeemerAccount.PersistenceManager.GenerateId(rec, RedeemerFees.RedeemerFeeIDEntityColumn);
                rec.TaxiPassRedeemerAccountID = pRedeemerAccount.TaxiPassRedeemerAccountID;
                //rec.Platform = Enum.GetNames(typeof(Platforms))[0];
                rec.AddToManager();

            } catch (Exception ex) {
                throw ex;
            }
            return rec;
        }

        protected override void OnColumnChanging(DataColumnChangeEventArgs pArgs) {
            if (IsDeserializing) {
                return;
            }
            base.OnColumnChanging(pArgs);
            DoAudit(pArgs);
        }

        private void DoAudit(DataColumnChangeEventArgs pArgs) {
            if (!this.PersistenceManager.IsClient) {
                return;
            }
            if (this.RowState == DataRowState.Added) {
                return;
            }
            if ((AuditRecord == null) || (AuditRecord.RowState == DataRowState.Unchanged)) {
                AuditRecord = RedeemerFeesAudit.Create(this.PersistenceManager, this);
            }
        }

        protected override void AddRules(RuleList pList) {
            PropertyRequiredRule.AddToList(pList, RedeemerFees.PlatformEntityColumn.ColumnName);
            pList.Add(IsPlatformUnique, PlatformEntityColumn.ColumnName);
        }

        private bool IsPlatformUnique(object pTarget, RuleArgs e) {
            RedeemerFees rec = pTarget as RedeemerFees;

            if (rec.Platform.IsNullOrEmpty()) {
                return false;
            }

            RdbQuery qry = new RdbQuery(typeof(RedeemerFees), RedeemerFees.PlatformEntityColumn, EntityQueryOp.EQ, rec.Platform);
            qry.AddClause(RedeemerFees.TaxiPassRedeemerAccountIDEntityColumn, EntityQueryOp.EQ, rec.TaxiPassRedeemerAccountID);
            qry.AddClause(RedeemerFees.RedeemerFeeIDEntityColumn, EntityQueryOp.NE, rec.RedeemerFeeID);
            
            EntityList<RedeemerFees> oList = this.PersistenceManager.GetEntities<RedeemerFees>(qry);
            if (oList.Count > 0) {
                e.Description = "A fee already exists for platform: " + rec.Platform;
                return false;
            }
            return true;
        }


        public static RedeemerFees GetRedemeerFee(TaxiPassRedeemerAccounts pRedeemerAccounts, string pPlatform) {
            RdbQuery qry = new RdbQuery(typeof(RedeemerFees), RedeemerFees.TaxiPassRedeemerAccountIDEntityColumn, EntityQueryOp.EQ, pRedeemerAccounts.TaxiPassRedeemerAccountID);
            qry.AddClause(RedeemerFees.PlatformEntityColumn, EntityQueryOp.EQ, pPlatform);
            return pRedeemerAccounts.PersistenceManager.GetEntity<RedeemerFees>(qry, QueryStrategy.DataSourceThenCache);
        }


    }

    #region EntityPropertyDescriptors.RedeemerFeesPropertyDescriptor
    ////*** Use this template to enable strongly-typed identification of custom properties for UI databinding.
    ////    For example: 
    ////      "Employee.Age" can be identified as EntityPropertyDescriptors.Employee.Age  
    //public static partial class EntityPropertyDescriptors {
    //	public partial class RedeemerFeesPropertyDescriptor : BaseEntityPropertyDescriptor {
    //    /// <summary>Gets the <see cref="T:IdeaBlade.Util.AdaptedPropertyDescriptor"/> for Age.</summary>
    //    public AdaptedPropertyDescriptor Age {
    //      get { return this.Get("Age"); }
    //    }
    //  }
    //} 
    #endregion

}