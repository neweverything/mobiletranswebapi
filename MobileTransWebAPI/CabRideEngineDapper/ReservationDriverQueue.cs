﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Dapper;
using System.Data.SqlClient;

namespace CabRideEngineDapper {

    public partial class ReservationDriverQueue {

        public static List<ReservationDriverQueue> GetByReservationID(SqlConnection pConn, long pReservationID) {
            string sql = @"SELECT  *
                            FROM    dbo.ReservationDriverQueue
                            WHERE   ReservationID = @ReservationID
                            ORDER BY DistanceToPU";

            return pConn.Query<ReservationDriverQueue>(sql, new { ReservationID = pReservationID }).ToList();
        }

        public static ReservationDriverQueue GetByReservationIDAndDriver(SqlConnection pConn, long pReservationID, long? pDriverID) {
            string sql = @"SELECT  *
                            FROM    dbo.ReservationDriverQueue
                            WHERE   ReservationID = @ReservationID AND DriverID = @DriverID
                            ORDER BY DistanceToPU";

            return pConn.Query<ReservationDriverQueue>(sql, new { ReservationID = pReservationID, DriverID = pDriverID }).FirstOrDefault();
        }
    }
}
