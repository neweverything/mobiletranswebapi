﻿using System;

namespace MobileTransWebAPI.Models {
	public class CheckStatusRequest {
		public long AffiliateID { get; set; }
		public string AffiliateCode { get; set; }
		public DateTime StartDate { get; set; }
		public DateTime EndDate { get; set; }

	}
}