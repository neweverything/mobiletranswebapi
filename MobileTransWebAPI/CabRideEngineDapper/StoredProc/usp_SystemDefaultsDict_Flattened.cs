﻿using CabRideEngineDapper.Utils;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace CabRideEngineDapper {
    public class usp_SystemDefaultsDict_Flattened {

        public string RedeemerManagers { get; set; }
        public string MaxVoucherAge { get; set; }
        public string ApprovalRequiredAccounts { get; set; }
        public string HideNoImprint { get; set; }
        public string DoNotPayAge { get; set; }
        public string AccountLimits { get; set; }

        public static usp_SystemDefaultsDict_Flattened Execute(string pGroup) {
            using (var conn = SqlHelper.OpenSqlConnection()) {
                return Execute(conn, pGroup);
            }
        }

        public static usp_SystemDefaultsDict_Flattened Execute(SqlConnection pConn, string pGroup) {
            //var temp = pConn.Query("usp_SystemDefaultsDict_Flattened", new { ReferenceGroup = pGroup }, commandType: CommandType.StoredProcedure).DefaultIfEmpty(new usp_SystemDefaultsDict_Flattened()).FirstOrDefault();
            //Console.WriteLine(temp);
            return pConn.Query<usp_SystemDefaultsDict_Flattened>("usp_SystemDefaultsDict_Flattened", new { ReferenceGroup = pGroup }, commandType: CommandType.StoredProcedure).DefaultIfEmpty(new usp_SystemDefaultsDict_Flattened()).FirstOrDefault();
        }
    }
}
