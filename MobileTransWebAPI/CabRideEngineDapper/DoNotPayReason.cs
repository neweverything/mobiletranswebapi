﻿using CabRideEngineDapper.Utils;
using Dapper;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace CabRideEngineDapper {
    public partial class DoNotPayReason {

        public static List<DoNotPayReason> GetForDropDowns(SqlConnection pConn, bool pAddNullRec = true) {
            string sql = @"SELECT DoNotPayReasonID, Description FROM DoNotPayReason WITH (NOLOCK)
							ORDER BY Description";

            List<DoNotPayReason> recList = pConn.Query<DoNotPayReason>(sql).ToList();
            if (pAddNullRec) {
                recList.Insert(0, DoNotPayReason.GetNullEntity());
            }
            return recList;
        }

        public static DoNotPayReason GetReason(string pReason) {
            string sql = @"SELECT * FROM DoNotPayReason
							WHERE Description = @Description";

            using (var conn = SqlHelper.OpenSqlConnection()) {
                return conn.Get<DoNotPayReason>(sql, new { Description = pReason });
            }

        }

        public static DoNotPayReason GetCreateReason(string pReason) {
            string sql = @"SELECT * FROM DoNotPayReason
							WHERE Description = @Description";

            using (var conn = SqlHelper.OpenSqlConnection()) {
                var rec = conn.Get<DoNotPayReason>(sql, new { Description = pReason });

                if (rec.IsNullEntity) {
                    rec = Create();
                    rec.Description = pReason;
                    rec.Save(CabRideDapperSettings.LoggedInUser);
                }
                return rec;
            }

        }
    }
}
