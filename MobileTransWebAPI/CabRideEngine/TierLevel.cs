using System;
using System.Collections.Generic;
using System.Text;

using IdeaBlade.Util;

namespace CabRideEngine {
    public class TierLevel {

        private short mLevel;
        private string mDescription;

        private bool mIsNullEntity = false;

        private static BindableList<TierLevel> msEntities;
        private static TierLevel msNullEntity;

        static TierLevel() {

            // Create the null entity
            msNullEntity = new TierLevel(0, "");
            msNullEntity.mIsNullEntity = true;

            // Populate list of Routing Types.
            msEntities = new BindableList<TierLevel>();
            msEntities.Add(new TierLevel(0, ""));
            msEntities.Add(new TierLevel(1, "Call Center"));
            msEntities.Add(new TierLevel(2, "TaxiPass"));
        }

        private TierLevel(short pLevel, string pDescription) {
            mLevel = pLevel;
            mDescription = pDescription;
        }

        /// <summary>Get the TierLevel null entity.</summary>
        public static TierLevel NullEntity {
            get {
                return msNullEntity;
            }
        }

        /// <summary>Get the list of all TierLevel.</summary>
        public static BindableList<TierLevel> GetAll() {
            return msEntities;
        }

        /// <summary>Get the TierLevel by ID.</summary>
        public static TierLevel GetById(short pLevel) {
            foreach (TierLevel oLevel in msEntities) {
                if (oLevel.Level == pLevel) {
                    return oLevel;
                }
            }
            return NullEntity;
        }

        /// <summary>Return true if this is the null entity</summary>
        [BindingBrowsable(false)]
        public bool IsNullEntity {
            get {
                return mIsNullEntity;
            }
        }

        /// <summary>Get the Level</summary>
        public short Level {
            get {
                return mLevel;
            }
        }

        public string Description {
            get {
                return mDescription;
            }
        }

    }
}
