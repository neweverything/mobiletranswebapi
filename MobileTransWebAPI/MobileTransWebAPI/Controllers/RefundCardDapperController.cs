﻿using CabRideEngineDapper;
using CabRideEngineDapper.Utils;
using MobileTransWebAPI.Models;
using Newtonsoft.Json;
using System;
using System.Web.Http;
using TaxiPassCommon;

namespace MobileTransWebAPI.Controllers {

	public class RefundCardDapperController : ApiController {
		// GET api/refundcard
		public RefundResult Get() {
			RefundResult result = new RefundResult();

			string data = "AD602CF1BB4B41A629B3848A497C513BCBBD31EDE6D6CC393B5E42789C152537BF58261A2CB6CBE96FCB53A1529889A783C66E823569FEC9AA3EF27868D3B0883B5E42789C15253712329E6B6F1660D75292D9093CEAB0FA6ACE16820FB6D3DFAEC814042DBF6D03B259E154B88918BFB1458B26CAE8B6F6907542D3D4889103DA436FA8CDA7F9A9D1B6386FA64C474B5E9A134BEFB0805C671D8C3BE4C35CC5277E607B8810039FE592F601D918A24FED5F26093E3A934649C4550114B9BCFB05D024B3EB94B331825A36B3D4604866DA5D39177CCE10BEF1B43CA17453C530F21F6A1814E4418156F0451FA48215DF2D1E5282C5FC312A91779A2DA6D9A7C04A2383240155E85565E15593".Decrypt();
			RefundInfo refund = JsonConvert.DeserializeObject<RefundInfo>(data);



			result = Post(refund);

			return result;
		}


		public RefundResult Post([FromBody]RefundInfo refundInfo) {
			RefundResult result = new RefundResult();

			Accounts oAcct;
			if (refundInfo.AccountNo.IsNullOrEmpty()) {
				result.ErrorMsg = "Account No is required";
				return result;
			} else {
				if (refundInfo.AccountNo != "SKIP ACCT NO CHECK") {
					oAcct = Accounts.GetAccount(refundInfo.AccountNo);
					if (oAcct.IsNullEntity) {
						result.ErrorMsg = "Unknown account";
						return result;
					}
				}
			}

			WebApiApplication.LogIt(string.Format("{0} MobileTrans Web Service Data", refundInfo.Platform), JsonConvert.SerializeObject(refundInfo), "refund_" + refundInfo.Platform + "_{0}.txt");

			try {
				// change amount to be 2 decimal places
				refundInfo.RefundAmount = Convert.ToDecimal(refundInfo.RefundAmount.ToString("F2"));


				if (refundInfo.OrigVoucherNo.IsNullOrEmpty()) {
					result.ErrorMsg = "Please provide the original voucher number";
					return result;
				}

				using (var conn = SqlHelper.OpenSqlConnection()) {

					DriverPayments oOrigTrans = DriverPayments.GetPayment(conn, refundInfo.OrigVoucherNo);
					if (oOrigTrans.IsNullEntity) {
						result.ErrorMsg = "Could not find " + refundInfo.OrigVoucherNo;
						return result;
					}

					if (refundInfo.RefundReason.IsNullOrEmpty()) {
						result.ErrorMsg = "Please provide a reason for the refund";
						return result;
					}

					if (oOrigTrans.ChargeBack || oOrigTrans.Fare < 0) {
						result.ErrorMsg = "Cannot refund a refunded transaction";
						return result;
					}


					decimal refundedAmount = Math.Abs(DriverPaymentRef.GetRefundedAmount(refundInfo.OrigVoucherNo));
					if (oOrigTrans.DriverTotal - refundedAmount < refundInfo.RefundAmount) {
						if (refundedAmount > 0) {
							result.ErrorMsg = string.Format("Voucher has already been refunded for {0}, max refund: {1}", refundedAmount.ToString("C"), (oOrigTrans.DriverTotal - refundedAmount).ToString("C"));
						} else {
							result.ErrorMsg = string.Format("Max refund allowed: {1}", refundedAmount.ToString("C"), (oOrigTrans.DriverTotal - refundedAmount).ToString("C"));
						}
						return result;
					}



					var oAffiliate = Affiliate.GetAffiliate(conn, oOrigTrans.AffiliateID.Value);

					var oRefundTrans = DriverPayments.Create(oOrigTrans.Drivers);
					oRefundTrans.OriginalTransNo = oOrigTrans.TransNo;

					if (refundInfo.DoNotRefundServiceFee) {
						oRefundTrans.SetFareNoTaxiPassFee = -Math.Abs(refundInfo.RefundAmount);
					} else {
						oRefundTrans.SetFare = -Math.Abs(refundInfo.RefundAmount);
					}
					oRefundTrans.Platform = oOrigTrans.Platform;
					if (!refundInfo.Platform.IsNullOrEmpty()) {
						oRefundTrans.Platform = refundInfo.Platform;
					}

					//need to calc TPFee 
					if (!refundInfo.DoNotRefundServiceFee) {
						decimal nTotal = oOrigTrans.DriverTotal + oRefundTrans.Fare;
						decimal nTPFee = DriverPayments.CalcFee(oAffiliate, nTotal);
						if (oOrigTrans.TaxiPassFee == 0) {
							oRefundTrans.TaxiPassFee = 0;
						} else if (oOrigTrans.DriverTotal == Math.Abs(oRefundTrans.DriverTotal)) {
							oRefundTrans.TaxiPassFee = -oOrigTrans.TaxiPassFee;
						} else {
							oRefundTrans.TaxiPassFee = -(oOrigTrans.TaxiPassFee - nTPFee);
						}
					}

					oRefundTrans.GetSetCardNumber = oOrigTrans.DecryptCardNumber();
					oRefundTrans.GetSetExpiration = oOrigTrans.DecryptCardExpiration();
					oRefundTrans.AffiliateDriverID = oOrigTrans.AffiliateDriverID;
					oRefundTrans.AffiliateID = oOrigTrans.AffiliateID;
					oRefundTrans.CardSwiped = oOrigTrans.CardSwiped;
					if (oRefundTrans.DriverPaymentNotes.IsNullEntity) {
						DriverPaymentNotes.Create(oRefundTrans);
					}


					oRefundTrans.DriverPaymentNotes.Reason = refundInfo.RefundReason;
					oRefundTrans.VehicleID = oOrigTrans.VehicleID;
					oRefundTrans.ChargeBack = true;

					//if (!oRefundTrans.DriverPaymentNotes.Reason.IsNullOrEmpty()) {
					//    oRefundTrans.ChargeBackInfo = oRefundTrans.DriverPaymentNotes.Reason;
					//    if (oRefundTrans.ChargeBackInfo.Length > 200) {
					//        oRefundTrans.ChargeBackInfo = StringUtils.Left(oRefundTrans.ChargeBackInfo, 200);
					//    }
					//}
					oRefundTrans.ChargeBackInfo = string.Format("Refund of Voucher No: {0}  Refund Amount: {1} - {2} ", refundInfo.OrigVoucherNo, refundInfo.RefundAmount.ToString("C"), refundInfo.RefundReason).PadRight(200).Left(200).Trim();

					oRefundTrans.CorporateName = oOrigTrans.CorporateName;
					oRefundTrans.ReferenceNo = oOrigTrans.ReferenceNo;

					oRefundTrans.ChargeDate = oAffiliate.LocalTime;
					oRefundTrans.DriverPaid = true;
					oRefundTrans.DriverPaidDate = oRefundTrans.ChargeDate;

					oRefundTrans.CreditCardProcessor = oOrigTrans.CreditCardProcessor;

					DriverPaymentResults oResult = null;
					try {
						//lock (thisLock) {
						//oRefundTrans.VoidTrans(conn);
						oResult = oRefundTrans.RefundCard(oOrigTrans.DriverPaymentChargeResults.Reference, oOrigTrans.TransNo);
						if (oResult.Result == "0") {
							oRefundTrans.Save("MobileTrans.RefundCard");
							result.NewVoucherNo = oRefundTrans.TransNo;
							result.TPFee = oRefundTrans.TaxiPassFee;
							result.VoucherNo = oOrigTrans.TransNo;
							result.RefundDate = oRefundTrans.ChargeDate.Value;
							result.DriverPaymentID = oRefundTrans.DriverPaymentID;
							result.CardNo = oRefundTrans.CardNumberDisplay;
							result.DriverTotal = oRefundTrans.DriverTotal;
							result.IsTestCard = oRefundTrans.Test;
							result.SoftDescriptor = oRefundTrans.SoftDescriptor;
							result.TransType = oResult.TransType;
						} else {
							result.ErrorMsg = "Problem creating refund: " + oResult.Message;
						}

					} catch (Exception ex) {
						string sErrorEmailRecipient = System.Configuration.ConfigurationManager.AppSettings["ErrorEmailRecipient"];
						if (!String.IsNullOrEmpty(sErrorEmailRecipient)) {
							String sHtml = "";
							sHtml += "<hr noshade color=#000000 size=1>";
							sHtml += DateTime.Now.ToString() + " Card Refund Exception error<br>";
							sHtml += "<br>** Error Info **<br>";
							sHtml += "Page: MobileTransWebApi.RefundCard<br>";
							sHtml += "Message: " + ex.Message + "<br>";
							sHtml += "Error Affiliate: " + oAffiliate.Name + "<br>";

							try { // error may happen on page where Request object doesn't exist contextually
								sHtml += "<br>** Request **<br>" + ObjectDisplayer.ObjectToString(refundInfo) + "<br>";
							} catch {
							}

							sHtml += "<br>Stack Trace: " + ex.StackTrace + "<br>";


							// send email if setting is configured in web.config
							try {
								string subject = "MobileTransactions Refund Card Error";
								var cred = SystemDefaultsDict.GetEmailCreds(CabRideEngineDapper.Enums.EMailReportFrom.Report);
								TaxiPassCommon.Mail.EMailMessage oMail = new TaxiPassCommon.Mail.EMailMessage(cred, subject, sHtml);
								oMail.UseWebServiceToSendMail = Properties.Settings.Default.SendEmailViaWebService;
								oMail.SendMail(sErrorEmailRecipient);
							} catch {
							}
						}
						result.ErrorMsg = "Problem refunding: " + ex.Message;
						return result;
					}
					if (oResult.Result != "0") {
						result.ErrorMsg = oResult.Message;
						return result;
					}


					oOrigTrans.DriverPaymentNotes.Reason = "See Refund: Trans No: " + oRefundTrans.TransNo + " Date: " + oRefundTrans.ChargeDate.Value.ToString("MM/dd/yyyy") + "\n\n" + oOrigTrans.DriverPaymentNotes.Reason;
					try {
						//oOrigTrans.Platform = Platforms.WebTerminal.ToString();
						oOrigTrans.Save("MobileTrans.RefundCard");

					} catch {
					}
				}

			} catch (Exception exUnknown) {
				result.ErrorMsg = exUnknown.Message;
				Utils.EmailErrors.SendErrorEmail("MobileTransWebApi Error: RefundCard", exUnknown.Message + Environment.NewLine + "Input: " + JsonConvert.SerializeObject(refundInfo));
			}

			return result;
		}


	}
}
