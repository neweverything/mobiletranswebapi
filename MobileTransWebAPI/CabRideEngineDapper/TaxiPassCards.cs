﻿using Dapper;
using System.Data.SqlClient;
using TaxiPassCommon.Cryptography;

namespace CabRideEngineDapper {
    public partial class TaxiPassCards {

        [Editable(false)]
        public string SetTaxiPassCardNo {
            set {
                TaxiPassCardNo = value;
                CardNumber = CryptoFns.MD5HashUTF16ToString(base.TaxiPassCardNo);
            }
        }

        public static TaxiPassCards GetCard(SqlConnection pConn, long pTaxiPassCardID) {
            return pConn.Get<TaxiPassCards>(pTaxiPassCardID);
        }

        public static TaxiPassCards GetCardNo(SqlConnection pConn, string pCardNo) {
            string sql = @"SELECT * FROM TaxiPassCards
                            WHERE TaxiPassCardNo = @CardNo";

            return pConn.Get<TaxiPassCards>(sql, new { CardNo = pCardNo });
        }

    }
}

