﻿using CabRideEngineDapper.Utils;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using TaxiPassCommon;

namespace CabRideEngineDapper {
	public partial class CabDriverRef {

		public static CabDriverRef GetRecord(string pTableName, long pSourceID, string pFieldName) {
			using (var conn = SqlHelper.OpenSqlConnection()) {
				return GetRecord(conn, pTableName, pSourceID, pFieldName);
			}
		}


		public static CabDriverRef GetRecord(SqlConnection pConn, string pTableName, long pSourceID, string pFieldName) {
			string sql = "SELECT * FROM CabDriverRef WHERE SourceTable=@SourceTable AND SourceID=@SourceID AND ReferenceKey=@ReferenceKey";
			return pConn.Query<CabDriverRef>(sql, new { SourceTable = pTableName, SourceID = pSourceID, ReferenceKey = pFieldName }).DefaultIfEmpty(CabDriverRef.GetNullEntity()).FirstOrDefault();
		}



		public static CabDriverRef GetOrCreate(SqlConnection pConn, string pTableName, long pSourceID, string pFieldName, string pValue = "") {
			string sql = $@"select * from CabDriverRef 
							WHERE SourceTable = @TableName
									AND SourceID = @SourceID
									AND ReferenceKey = @FieldName";

			DynamicParameters param = new DynamicParameters();
			param.Add("TableName", pTableName);
			param.Add("SourceID", pSourceID);
			param.Add("FieldName", pFieldName);
			CabDriverRef rec = pConn.Query<CabDriverRef>(sql, param).DefaultIfEmpty(CabDriverRef.GetNullEntity()).FirstOrDefault();

			if (rec.IsNullEntity) {
				rec = Create(pConn, pTableName, pSourceID, pFieldName, pValue);
			}
			return rec;
		}

		public static CabDriverRef GetOrCreateWithValue(SqlConnection pConn, string pTableName, long pSourceID, string pFieldName, string pValue) {
			string sql = $@"SELECT * FROM CabDriverRef 
							WHERE SourceTable = @TableName
									AND SourceID = @SourceID
									AND ReferenceKey = @FieldName
									{(pValue.IsNullOrEmpty() ? "" : "AND ValueString = @ValueString")}";

			DynamicParameters param = new DynamicParameters();
			param.Add("TableName", pTableName);
			param.Add("SourceID", pSourceID);
			param.Add("FieldName", pFieldName);
			if (!pValue.IsNullOrEmpty()) {
				param.Add("ValueString", pValue);
			}
			CabDriverRef rec = pConn.Query<CabDriverRef>(sql, param).DefaultIfEmpty(CabDriverRef.GetNullEntity()).FirstOrDefault();

			if (rec.IsNullEntity) {
				rec = Create(pConn, pTableName, pSourceID, pFieldName, pValue);
			}
			return rec;
		}

		private static CabDriverRef Create(SqlConnection pConn, string pTableName, long pSourceID, string pFieldName, string pValue = "") {

			string sql = @"usp_CabDriverRefInsert";// SourceID = SourceID
												   //, SourceTable = @SourceTable
												   //, ReferenceKey = @ReferenceKey
												   //, ValueString = @ValueString";

			DynamicParameters param = new DynamicParameters();
			param.Add("SourceTable", pTableName);
			param.Add("SourceID", pSourceID);
			param.Add("ReferenceKey", pFieldName);
			param.Add("ValueString", pValue);

			return pConn.Query<CabDriverRef>(sql, param, commandType: CommandType.StoredProcedure).FirstOrDefault();

		}

	}
}
