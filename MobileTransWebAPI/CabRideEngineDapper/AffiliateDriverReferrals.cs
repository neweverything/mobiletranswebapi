﻿using CabRideEngineDapper.Utils;
using Dapper;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace CabRideEngineDapper {
    public partial class AffiliateDriverReferrals {

        public static AffiliateDriverReferrals Create(AffiliateDrivers pDriver, long pReferredDriverID) {
            return Create(pDriver, pReferredDriverID, Platforms.TaxiPay.ToString());
        }

        public static AffiliateDriverReferrals Create(AffiliateDrivers pDriver, long pReferredDriverID, string pPlatform) {
            using (var conn = SqlHelper.OpenSqlConnection()) {
                AffiliateDriverReferrals rec = GetRecord(conn, pDriver.AffiliateDriverID, pReferredDriverID);

                try {
                    if (rec.IsNullEntity) {
                        rec = Create();
                        // Using the IdeaBlade Id Generation technique
                        //oPM.GenerateId(rec, AffiliateDriverReferrals.AffiliateDriverReferralIDEntityColumn);
                        rec.AffiliateDriverID = pDriver.AffiliateDriverID;
                        rec.ReferredDriverID = pReferredDriverID;
                        rec.Platform = Platforms.TaxiPay.ToString();
                        rec.ReferralDate = DateTime.Now;

                        rec.Save();
                    }
                } catch (Exception ex) {
                    throw ex;
                }
                return rec;
            }
        }


        public override int Save(string pModifiedBy) {
            if (RowState == DataRowState.Modified || RowState == DataRowState.Added) {
                ModifiedDate = DateTime.UtcNow;
                ModifiedBy = string.Format("{0} | {1}", pModifiedBy, CabRideDapperSettings.GetAppName());
            }
            if (IsNewRecord) {
                this.AffiliateDriverReferralID = NextID.GetNextKey(AffiliateDriverReferrals.TableDef.TABLE_NAME);
            } else {
                RowVersion++;
            }

            return base.Save(pModifiedBy);

        }

        public override int Save() {
            return Save(CabRideDapperSettings.LoggedInUser);
        }


        private AffiliateDrivers mAffiliateDrivers;
        public AffiliateDrivers AffiliateDrivers {
            get {
                if (mAffiliateDrivers == null) {
                    mAffiliateDrivers = AffiliateDrivers.GetDriver(this.AffiliateDriverID);
                }
                return mAffiliateDrivers;
            }
        }


        public static AffiliateDriverReferrals GetRecord(SqlConnection pConn, long pAffiliateDriverID, long pReferredDriverID) {
            string sql = @"SELECT  *
                            FROM    AffiliateDriverReferrals
                            WHERE   AffiliateDriverID = @AffiliateDriverID
                                    AND ReferredDriverID = @ReferredDriverID";
            return pConn.Get<AffiliateDriverReferrals>(sql, new { AffiliateDriverID = pAffiliateDriverID, ReferredDriverID = pReferredDriverID });
        }

        public static AffiliateDriverReferrals GetReferredDriver(long pAffiliateDriverID) {
            using (var conn = SqlHelper.OpenSqlConnection()) {
                string sql = "SELECT * FROM AffiliateDriverReferrals WHERE ReferredDriverID = @AffiliateDriverID";
                return conn.Query<AffiliateDriverReferrals>(sql, new { AffiliateDriverID = pAffiliateDriverID }).DefaultIfEmpty(AffiliateDriverReferrals.GetNullEntity()).FirstOrDefault();
            }
        }

        public static AffiliateDriverReferrals GetByReferredDriver(SqlConnection pConn, long pDriverID) {
            string sql = "SELECT * FROM AffiliateDriverReferrals WHERE ReferredDriverID = @ReferredDriverID";
            return pConn.Get<AffiliateDriverReferrals>(sql, new { ReferredDriverID = pDriverID });
        }
    }
}
