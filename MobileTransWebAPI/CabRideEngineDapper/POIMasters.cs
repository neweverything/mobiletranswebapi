﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

using Dapper;

namespace CabRideEngineDapper {
    public partial class POIMasters {

        public static POIMasters Create(string pPOIName) {
            POIMasters rec = POIMastersTable.Create();
            rec.POIName = pPOIName;
            return rec;
        }

        public static POIMasters GetOrCreate(SqlConnection pConn, string pPOIName, string pCreatedBy) {
            POIMasters rec = GetMaster(pConn, pPOIName);

            if (rec.IsNullEntity) {
                rec = POIMasters.Create(pPOIName);
                rec.Save(pConn, pCreatedBy);
            }
            return rec;
        }

        public static POIMasters GetMaster(SqlConnection pConn, string pName) {
            string sql = string.Format("Select * FROM {0} WITH (NoLock) WHERE {1} = @{1}", POIMasters.TableDef.TABLE_NAME, POIMasters.TableDef.POIName);
            DynamicParameters param = new DynamicParameters();
            param.Add(POIMasters.TableDef.POIName, pName);
            return pConn.Query<POIMasters>(sql, param).DefaultIfEmpty(POIMasters.GetNullEntity()).FirstOrDefault();
        }

        public new void Save(SqlConnection pConn, string pModifiedBy) {
            if (IsNewRecord) {
                CreatedBy = pModifiedBy;
                CreatedDate = DateTime.UtcNow;
                ModifiedDate = CreatedDate;
                ModifiedBy = CreatedBy;

                int? id = pConn.Insert(this);
                this.POIMasterID = id.Value;
            } else {
                ModifiedBy = pModifiedBy;
                ModifiedDate = DateTime.UtcNow;
                pConn.Update(this);
            }
        }
    }
}
