﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace TaxiPassCommon.Banking {
	public class NachaGenerator {

		public List<TaxiPassCommon.Banking.TTechGenerator.PayOut> PayOutList = new List<TaxiPassCommon.Banking.TTechGenerator.PayOut>();


		private List<oBankDefaults> mNachaDefaults;
		private string mDestination = "";
		private string mDestinationName = "";
		private string mOrigin = "";


		private string mOriginName = "";
		private string mRoutingNumber = "";
		private int mBatchNo = 0;
		private string mBankName = "";
		private string mTaxID = "";

		private string mReferenceCode = "";


		private int mBatchCount = 0;

		//private List<TTechTransaction> mTransList;

		private decimal mDebitTotal = 0;
		private decimal mCreditTotal = 0;
		private decimal mDebitGroupTotal = 0;
		private decimal mCreditGrouptTotal = 0;

		private DateTime mFundingDate = DateTime.Today;


		// hash totals
		private Int64 mBatchABATotal;
		private Int64 mFileABATotal;
		private int mDetailLineCount = 0;


		public NachaGenerator(string pBank
							, string pDestination
							, string pDestinationName
							, string pOrigin
							, string pOriginName
							, string pRoutingNumber
							, List<oBankDefaults> pNachaDefaults
							, int pBatchNo
							, string pReferenceCode
							, DateTime pFundingDate) {

			mDestination = pDestination;
			mDestinationName = pDestinationName;
			mOrigin = pOrigin;
			mOriginName = pOriginName;

			mNachaDefaults = pNachaDefaults;

			mRoutingNumber = pRoutingNumber;
			mBatchNo = pBatchNo;
			mBankName = pBank;
			mReferenceCode = pReferenceCode;
			mFundingDate = pFundingDate;
			mTaxID = mOrigin;
		}


		// ********************************************************************************************
		// Overload for CreateFile to alway create the file
		// ********************************************************************************************
		public string CreateFile(string pFilePath, List<TTechTransaction> pTransList, eBankNames pBankName) {
			return CreateFile(pFilePath, pTransList, pBankName, true);
		}


		// ***********************************************************************
		// Main processing method for creating the file
		// ***********************************************************************
		public string CreateFile(string pFilePath, List<TTechTransaction> pTransList, eBankNames pBankName, bool pWriteFile) {
			//mTransList = pTransList;

			if (pFilePath.IsNullOrEmpty()) {
				throw new Exception("Path is required");
			}

			if (!System.IO.Directory.Exists(pFilePath)) {
				System.IO.Directory.CreateDirectory(pFilePath);
			}

			StringBuilder sFileName = new StringBuilder();
			sFileName.Append(mBankName);
			sFileName.Append("_");
			sFileName.Append(DateTime.Today.ToString("yyyyMMdd"));
			sFileName.Append("_");
			sFileName.Append(mBatchNo);
			sFileName.Append(".nacha");
			while (true) {
				if (!File.Exists(Path.Combine(pFilePath, sFileName.ToString()))) {
					break;
				}
				mBatchNo++;
				sFileName.Remove(0, sFileName.Length);
				sFileName.Append(mBankName);
				sFileName.Append("_");
				sFileName.Append(DateTime.Today.ToString("yyyyMMdd"));
				sFileName.Append("_");
				sFileName.Append(mBatchNo);
				sFileName.Append(".nacha");

			}


			StringBuilder sData = new StringBuilder();
			sData.Append(HeaderFile(pBankName));

			var x = (from p in pTransList
					 where p.ClassCode == StandardEntryTypes.PPD.ToString() && p.TranType.Equals("CREDIT", StringComparison.CurrentCultureIgnoreCase) && !p.WireTransfer
					 select p).ToList();

			if (x.Count > 0) {
				mBatchCount++;
				sData.Append(CreateBatchRecords(x, StandardEntryTypes.PPD, pBankName));
			}

			x = (from p in pTransList
				 where p.ClassCode == StandardEntryTypes.PPD.ToString() && p.TranType.Equals("DEBIT", StringComparison.CurrentCultureIgnoreCase)
				 select p).ToList();

			if (x.Count > 0) {
				mBatchCount++;
				sData.Append(CreateBatchRecords(x, StandardEntryTypes.PPD, pBankName));
			}

			x = (from p in pTransList
				 where p.ClassCode == StandardEntryTypes.CCD.ToString() && p.TranType.Equals("CREDIT", StringComparison.CurrentCultureIgnoreCase) && !p.WireTransfer
				 select p).ToList();

			if (x.Count > 0) {
				mBatchCount++;
				sData.Append(CreateBatchRecords(x, StandardEntryTypes.CCD, pBankName));
			}

			x = (from p in pTransList
				 where p.ClassCode == StandardEntryTypes.CCD.ToString() && p.TranType.Equals("DEBIT", StringComparison.CurrentCultureIgnoreCase)
				 select p).ToList();

			if (x.Count > 0) {
				mBatchCount++;
				sData.Append(CreateBatchRecords(x, StandardEntryTypes.CCD, pBankName));
			}

			if (mBatchCount == 0) {
				return "";
			}

			sData.Append(FileControl(pTransList, pBankName));

			if (pWriteFile) {
				File.WriteAllText(Path.Combine(pFilePath, sFileName.ToString()), sData.ToString());
				return sFileName.ToString();
			}

			return "";
		}



		// *************************************************************************************
		// Build out the transaction detail
		// *************************************************************************************
		private string CreateBatchRecords(List<TTechTransaction> pTransList
										, StandardEntryTypes pStandardEntryType
										, eBankNames pBankName) {

			StringBuilder sData = new StringBuilder();
			sData.Append(BatchHeader(pStandardEntryType, pBankName));

			string sUseKey = "";
			Dictionary<string, List<TTechTransaction>> oDic = FillData(pTransList);
			foreach (string sKey in oDic.Keys) {
				if (sUseKey.IsNullOrEmpty()) {
					sUseKey = sKey;
				}
				TTechTransaction newTran = null;
				try {
					newTran = oDic[sKey][0];
					newTran.Amount = oDic[sKey].Sum(p => p.Amount);

					foreach (TTechTransaction oTran in oDic[sKey]) {
						oTran.BatchNo = DateTime.Today.ToString("yyyyMMdd") + mBatchNo.ToString().PadLeft(2, '0') + mOrigin;
					}
					sData.Append(EntryDetail(newTran, pBankName));
				} catch (Exception ex) {
					try {
						Mail.EMailMessage mail = new Mail.EMailMessage("email@taxipass.com", "NachGenerator Error", string.Format("{0}\n Data: {1}", ex.Message, JSON.JsonHelper.JsonSerialize(oDic[sKey])), "");
						mail.UseWebServiceToSendMail = true;
						mail.SendMail(Properties.Settings.Default.EMailErrorsTo);
					} catch (Exception) {
					}
					newTran.PaidTo += " Error: " + ex.Message;
					Console.WriteLine(string.Format("Error Processing:  Voucher: {0}, Paid To: {1}, Error: {2}", newTran.TranID, newTran.PaidTo, ex.Message));

				}
			}

			sData.Append(BatchControl(pTransList, oDic.Count, pBankName, pStandardEntryType.ToString()));
			return sData.ToString();
		}


		// ********************************************************************************************
		// Create dictionary of data for CreateBatchRecords
		// ********************************************************************************************
		private Dictionary<string, List<TTechTransaction>> FillData(List<TTechTransaction> pTransList) {
			Dictionary<string, List<TTechTransaction>> oDic = new Dictionary<string, List<TTechTransaction>>();

			mCreditGrouptTotal = 0;
			mDebitGroupTotal = 0;

			foreach (TTechTransaction oTran in pTransList) {
				string sKey = oTran.TransactionKey;
				if (!oDic.ContainsKey(sKey)) {
					oDic.Add(sKey, new List<TTechTransaction>());
				}
				if (oDic[sKey].FirstOrDefault(p => p.TranID == oTran.TranID) == null) {
					oDic[sKey].Add(oTran);
				}
			}

			return oDic;
		}


		// ***********************************************************************
		// Define the header row (1st row in the file
		// ***********************************************************************
		private string HeaderFile(eBankNames pBankName) {
			StringBuilder sHeader = new StringBuilder();
			sHeader.Append("1");                              //1 01-01 record type code
			sHeader.Append("01");                             //2 02-03 priority code
			sHeader.Append(mDestination.SizeLeft(10, " "));   //3 04-13 destination
			sHeader.Append(SetOrigin(pBankName));             //4 14-23 Origin
			sHeader.Append(DateTime.Now.ToString("yyMMdd"));  //5 24-29 File Date
			sHeader.Append(DateTime.Now.ToString("HHmm"));    //6 30-33 File Time

			string fileId = mBatchNo.ToString();

			if (mBatchNo > 9) {
				//int batchNo = 48 + mBatchNo;
				//fileId = "" + Convert.ToChar(batchNo);
				while (fileId.IsNumeric()) {
					fileId = StringUtils.GenerateSalt().Left(1);
				}
			}
			sHeader.Append(fileId.Left(1).ToUpper());         //7 34-34 File ID
			sHeader.Append("094");                            //8 35-37 Record Size
			sHeader.Append("10");                             //9 38-39 Blocking Factor
			sHeader.Append("1");                              //10 40-40 format code
			if (pBankName == eBankNames.TD || pBankName == eBankNames.BankUnited) {
				sHeader.Append(mDestinationName.ToUpper().Replace(",", "").SizeRight(23));   //11 41-63 destination name   
			} else {
				sHeader.Append(mDestinationName.SizeRight(23));   //11 41-63 destination name   
			}

			sHeader.Append(mOriginName.SizeRight(23));        //12 64-86 origin name
			sHeader.Append(mReferenceCode.SizeRight(8));      //13 87-94 reference code
			sHeader.Append("\r\n");

			return sHeader.ToString();
		}




		// ****************************************************************************
		// create a header for the subset of data in CreateBatchRecords
		// ****************************************************************************
		private string BatchHeader(StandardEntryTypes pStandardEntryType, eBankNames pBankName) {
			StringBuilder sBatchHeader = new StringBuilder();
			sBatchHeader.Append("5");                                        //1 01-01		record type
			if (pBankName == eBankNames.BankUnited) {
				sBatchHeader.Append("220");                                  //2 02-04		service class
			} else {
				sBatchHeader.Append("200");                                  //2 02-04		service class
			}
			sBatchHeader.Append(mOriginName.SizeRight(16));                  //3 05-20		company name
			sBatchHeader.Append("".PadRight(20));                            //4 21-40		company discretionary data

			sBatchHeader.Append(SetCompanyID(pBankName, pStandardEntryType.ToString()));    //5 41-50		Origin/Company Identification

			sBatchHeader.Append(pStandardEntryType.ToString());              //6 51-53		standard entry
			sBatchHeader.Append("PaymentACH");                               //7 54-63		company entry description
			sBatchHeader.Append("".PadRight(6));                             //8 64-69		company descriptive date


			sBatchHeader.Append(mFundingDate.ToString("yyMMdd"));            //9 70-75		effective entry date
			sBatchHeader.Append("   ");                                      //10 76-78		Blank fill
			sBatchHeader.Append("1");                                        //11 79-79		originator status code
			sBatchHeader.Append(mRoutingNumber.SizeRight(8));                //12 80-87		originating DFI
			sBatchHeader.Append(mBatchCount.ToString().SizeLeft(7, "0"));    //13 88-94		batch number

			sBatchHeader.Append("\r\n");
			return sBatchHeader.ToString();
		}


		// ***************************************************************************************
		// Create the actual data row from CreateBatchRecords
		// ***************************************************************************************
		private long mBankUnitedHash = 0;
		private long mBankUnitedHashCCD = 0;
		private long mBankUnitedHashAll = 0;

		private string EntryDetail(TTechTransaction pTrans, eBankNames pBankName) {
			mDetailLineCount++;
			StringBuilder sDetail = new StringBuilder();

			sDetail.Append("6");                                                    //1 01-01 record type code
																					//2 02-03 trans code
			if (pTrans.TranType.Equals("DEBIT", StringComparison.CurrentCultureIgnoreCase)) {
				sDetail.Append("27");
				mDebitGroupTotal += pTrans.Amount;
				mDebitTotal += pTrans.Amount;
			} else {
				sDetail.Append("22");
				mCreditTotal += pTrans.Amount;
				mCreditGrouptTotal += pTrans.Amount;
			}
			sDetail.Append(pTrans.ABANumber.SizeLeft(9, " "));                      // 3,4 04-12 routing number;
			mBankUnitedHashAll += pTrans.ABANumber.Left(8).ToInt();
			if (pTrans.ClassCode == StandardEntryTypes.CCD.ToString()) {
				mBankUnitedHashCCD += pTrans.ABANumber.Left(8).ToInt();
			} else {
				mBankUnitedHash += pTrans.ABANumber.Left(8).ToInt();
			}
			// mBatchABATotal: used in the Batch control footer (type 8)
			// mFileABATotal: used in File control footer (type 9)
			if (pTrans.ABANumber.IsNumeric()) {
				mBatchABATotal += Int64.Parse(pTrans.ABANumber.Right(8));
				mFileABATotal += Int64.Parse(pTrans.ABANumber.Right(8));
			}

			sDetail.Append(SetAccountNo(pBankName, pTrans.Account));                //5 13-29 account number

			//sDetail.Append(pTrans.Account.SizeLeft(17, " "));                       //5 13-29 account number

			sDetail.Append(Math.Abs(pTrans.Amount * 100).ToString("0000000000"));   //6 30-39 amount 
			if (pTrans.Phone1.Length > 10) {
				pTrans.Phone1 = pTrans.Phone1.Right(10); // reduced size due to recon issues with CitiBank
			}
			sDetail.Append(pTrans.Phone1.SizeRight(15));                            //7 40-54 individual id number
			sDetail.Append(pTrans.CustomerName.SizeRight(22));                      //8 55-76 individual name
			sDetail.Append("  ");                                                   //9 77-78 discretionary data
			sDetail.Append("0");                                                    //10 79-79 addenda record
			if (pBankName == eBankNames.BankUnited) {
				sDetail.Append($"{mRoutingNumber.Left(8)}{mDetailLineCount.ToString().PadLeft(7, '0').SizeRight(7)}");
			} else {
				sDetail.Append(pTrans.TranReference.SizeLeft(15, "0"));                 //11 80-94 trace number
			}
			sDetail.Append("\r\n");

			TTechGenerator.PayOut pay = new TTechGenerator.PayOut();
			pay.Affiliate = pTrans.Affiliate;
			pay.Amount = pTrans.Amount;
			pay.Name = pTrans.CustomerName;
			pay.TransType = pTrans.TranType;

			pay.BatchNo = pTrans.BatchNo;
			pay.BankRouting = pTrans.ABANumber;
			pay.BankAccountNo = pTrans.Account;
			pay.BankAccountType = pTrans.CheckType;
			pay.LaneID = pTrans.LaneID;
			pay.PaidTo = pTrans.PaidTo;
			pay.PaymentType = pTrans.PaymentType;
			pay.WireTransfer = pTrans.WireTransfer;
			pay.Phone = pTrans.Phone1;
			//pay.AffiliateID = pTrans.MerchantID;

			PayOutList.Add(pay);
			return sDetail.ToString();
		}


		// ****************************************************************************
		// create a footer for the subset of data in CreateBatchRecords
		// ****************************************************************************
		private string BatchControl(List<TTechTransaction> batchList, int batchCount, eBankNames pBankName, string pClassCode) {
			//decimal credits = Math.Abs((from p in batchList where p.Amount > 0 select p.Amount).ToList().Sum() * 100);
			//decimal debits = Math.Abs((from p in batchList where p.Amount < 0 select p.Amount).ToList().Sum() * 100);
			string sWork;

			StringBuilder sBatch = new StringBuilder();
			sBatch.Append("8");                                                             //1 01-01 record type code
			if (pBankName == eBankNames.BankUnited) {
				sBatch.Append("220");                                                       //2 02-03 service class code
			} else {
				sBatch.Append("200");                                                       //2 02-03 service class code
			}
			sBatch.Append(batchCount.ToString("F0").SizeLeft(6, "0"));                      //3 05-10 entry count
			switch (pBankName) {
				case eBankNames.BofA:
					sWork = mBatchABATotal.ToString();
					if (sWork.Length > 10) {
						sWork = sWork.Right(10);
					}
					sWork = sWork.PadLeft(10, '0');
					sBatch.Append(sWork);                                                   //4 11-20 entry hash
					break;
				case eBankNames.BankUnited:
					string tmp = "";
					if (pClassCode == StandardEntryTypes.CCD.ToString()) {
						tmp = mBankUnitedHashCCD.ToString().PadLeft(10, '0').Right(10);
					} else {
						tmp = mBankUnitedHash.ToString().PadLeft(10, '0').Right(10);
					}
					sBatch.Append(tmp);                                   //4 11-20 entry hash
					break;
				default:
					sBatch.Append("0".PadRight(10, '0'));                                   //4 11-20 entry hash
					break;
			}

			mBatchABATotal = 0;

			sBatch.Append((mDebitGroupTotal * 100).ToString("F0").SizeLeft(12, "0"));       //5 21-32 Total Debits
			sBatch.Append((mCreditGrouptTotal * 100).ToString("F0").SizeLeft(12, "0"));     //6 33-44 Total Credits

			sBatch.Append(SetCompanyID(pBankName, pClassCode));                             //7 45-54 Company Identification

			sBatch.Append(new string(' ', 19));                                             //8 55-73 message authentication
			sBatch.Append(new string(' ', 6));                                              //9 74-79 reserved
			sBatch.Append(mRoutingNumber.SizeLeft(8, "0"));                                 //10 80-87 originating DFI
			sBatch.Append(mBatchCount.ToString().SizeLeft(7, "0"));                         //11 88-94 batch number
			sBatch.Append("\r\n");

			return sBatch.ToString();
		}


		// *************************************************************************************
		// Final row in the file
		// *************************************************************************************
		private string FileControl(List<TTechTransaction> pTransList, eBankNames pBankName) {

			//decimal credits = Math.Abs((from p in pTransList where p.Amount > 0 select p.Amount).ToList().Sum() * 100);
			//decimal debits = Math.Abs((from p in pTransList where p.Amount < 0 select p.Amount).ToList().Sum() * 100);
			string sWork;

			StringBuilder sFileControl = new StringBuilder();
			sFileControl.Append("9");                                                       //1 01-01 record type code
			sFileControl.Append(mBatchCount.ToString("F0").SizeLeft(6, "0"));               //2 02-07 batch count

			//sFileControl.Append(new string('0', 6));										//3 08-13 block count
			int iBlockCount = 0;

			switch (pBankName) {
				case eBankNames.BofA:
					iBlockCount = mDetailLineCount / 10;
					if (mDetailLineCount % 10 != 0) {
						iBlockCount++;
					}

					break;

				case eBankNames.TD:
					iBlockCount = (mDetailLineCount + 4) * 94;
					iBlockCount = (iBlockCount / 940) + 1;
					break;

				case eBankNames.BankUnited:
					int extra = 9;
					if (mBankUnitedHash == 0 || mBankUnitedHashCCD == 0) {
						extra = 6;
					}
					iBlockCount = (mDetailLineCount + extra) * 94;
					iBlockCount = (iBlockCount / 940) + 1;
					if (iBlockCount == 3 && pTransList.Count < 13) {
						iBlockCount--;
					}
					break;

				default:
					break;
			}


			sFileControl.Append(iBlockCount.ToString().PadLeft(6, '0'));                    //3 08-13 block count


			int count = (from p in pTransList
						 where !p.WireTransfer
						 select p).Count();
			if (pBankName == eBankNames.BankUnited) {
				count = mDetailLineCount;
			}
			sFileControl.Append(count.ToString("F0").SizeLeft(8, "0"));                     //4 14-21 entry count

			switch (pBankName) {
				case eBankNames.BofA:
					sWork = mFileABATotal.ToString();
					if (sWork.Length > 10) {
						sWork = sWork.Right(10);
					}
					sWork = sWork.PadLeft(10, '0');
					sFileControl.Append(sWork);                                             //5 22-31 entry hash
					break;
				case eBankNames.BankUnited:
					string tmp = mBankUnitedHashAll.ToString().PadLeft(10, '0').Right(10);
					sFileControl.Append(tmp);                                                      //5 22-31 entry hash
					break;
				default:
					sFileControl.Append(new string('0', 10));                               //5 22-31 entry hash
					break;
			}
			//


			sFileControl.Append((mDebitTotal * 100).ToString("F0").SizeLeft(12, "0"));      //6 32-43 total debit
			sFileControl.Append((mCreditTotal * 100).ToString("F0").SizeLeft(12, "0"));     //7 44-55 total credit
			sFileControl.Append(new string(' ', 39));                                       //8 56-94 reserved
			sFileControl.Append("\r\n");

			sFileControl.Append(new string('9', 94));
			sFileControl.Append("\r\n");
			sFileControl.Append(new string('9', 94));
			sFileControl.Append("\r\n");
			if (pBankName == eBankNames.BankUnited) {
				sFileControl.Append(new string('9', 94));
				sFileControl.Append("\r\n");
			}

			TaxiPassCommon.Banking.TTechGenerator.PayOut pay = new TaxiPassCommon.Banking.TTechGenerator.PayOut();
			pay.Amount = mDebitTotal;
			pay.Name = "Total of Payouts";
			pay.TransType = "Debit";
			PayOutList.Add(pay);

			pay = new TaxiPassCommon.Banking.TTechGenerator.PayOut();
			pay.Amount = mCreditTotal;
			pay.Name = "Total of Payouts";
			pay.TransType = "Credit";
			PayOutList.Add(pay);

			pay = new TaxiPassCommon.Banking.TTechGenerator.PayOut();
			decimal wireTotal = (from p in pTransList
								 where p.WireTransfer
								 select p.Amount).Sum();
			pay.Amount = wireTotal;
			pay.Name = "Total of Payouts";
			pay.TransType = "Wire Transfer";
			PayOutList.Add(pay);

			//pay = new TaxiPassCommon.Banking.TTechGenerator.PayOut();
			//PayOutList.Add(pay);
			var wireTransList = (from p in pTransList
								 where p.WireTransfer
								 select p).ToList();

			var nameList = (from p in wireTransList
							orderby p.Affiliate
							select p.Affiliate).Distinct().ToList();

			foreach (string affName in nameList) {
				pay = new TaxiPassCommon.Banking.TTechGenerator.PayOut();
				pay.Affiliate = affName;
				wireTotal = (from p in pTransList
							 where p.WireTransfer && p.Affiliate.Equals(affName)
							 select p.Amount).Sum();
				pay.Amount = wireTotal;
				pay.TransType = "Wire Transfer";
				PayOutList.Add(pay);
			}
			return sFileControl.ToString();
		}


		// ***************************************************************************
		// Set the correct Company Identification
		// ***************************************************************************
		private string SetCompanyID(eBankNames pBankName, string pClassCode) {
			string sCompanyID = "";
			switch (pBankName) {
				case eBankNames.Chase:
					sCompanyID = mDestination.PadLeft(10);
					break;
				case eBankNames.Citi:
					sCompanyID = ("6" + mOrigin).PadLeft(10);
					break;
				case eBankNames.BofA:
					try {
						sCompanyID = (from p in mNachaDefaults
									  where p.ClassCode == pClassCode
									  select p.CompanyID).Single();
					} catch (Exception ex) {
						Console.WriteLine(ex.Message);
					}
					break;
				case eBankNames.TD:
					sCompanyID = ("1" + mOrigin).PadLeft(10);
					break;

				case eBankNames.BankUnited:
					sCompanyID = ("1" + mOrigin).PadLeft(10);

					break;
			}
			return sCompanyID;
		}

		// ********************************************************************
		// Set the origin based on bank
		// ********************************************************************
		private string SetOrigin(eBankNames pBankName) {
			string sOrigin;
			switch (pBankName) {
				case eBankNames.BofA:
					sOrigin = mOrigin.SizeLeft(10);
					break;

				case eBankNames.TD:
					sOrigin = ("1" + mTaxID).SizeLeft(10);
					break;

				case eBankNames.BankUnited:
					sOrigin = (" " + mOrigin).SizeLeft(10);
					break;

				default:
					sOrigin = ("6" + mOrigin).SizeLeft(10);
					break;
			}
			return sOrigin;
		}


		// *******************************************************************
		// Either left or right justify the detail account number
		// *******************************************************************
		private string SetAccountNo(eBankNames pBankName, string pAccountNo) {
			switch (pBankName) {
				case eBankNames.BofA:
				case eBankNames.TD:
				case eBankNames.BankUnited:
					pAccountNo = pAccountNo.Trim().SizeRight(17, " ");
					break;
				default:
					pAccountNo = pAccountNo.SizeLeft(17, " ");
					break;
			}
			return pAccountNo;
		}


		public enum StandardEntryTypes {
			PPD,
			CCD
		}

		public enum eBankNames {
			BankUnited,
			BofA,
			Chase,
			Citi,
			TD
		}


		public class oBankDefaults {
			public string ClassCode { get; set; }
			public string CompanyID { get; set; }
		}


	}


}
