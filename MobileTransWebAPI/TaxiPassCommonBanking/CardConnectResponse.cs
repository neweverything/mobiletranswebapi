﻿namespace TaxiPassCommon.Banking {
    public class CardConnectResponse {
        public string amount { get; set; }
        public string resptext { get; set; }
        public string commcard { get; set; }
        public string cvvresp { get; set; }
        public string batchid { get; set; }
        public string avsresp { get; set; }
        public string respcode { get; set; }
        public string merchid { get; set; }
        public string token { get; set; }
        public string authcode { get; set; }
        public string respproc { get; set; }
        public string retref { get; set; }
        public string respstat { get; set; }
        public string account { get; set; }
        public string setlstat { get; set; }

        public string TransactionType {
            get {
                return amount.AsDecimal() > 0 ? "S" : "C";
            }
        }
    }
}
