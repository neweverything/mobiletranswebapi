﻿using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace CabRideEngineDapper {
    public class usp_ReservationGetByResNo {
        public long ReservationID { get; set; }
        public string EMail { get; set; }
        public string PhoneNumber { get; set; }
        public string TransNo { get; set; }
        public string ReceiptTextMSg { get; set; }

        public static List<usp_ReservationGetByResNo> Execute(SqlConnection pConn, string pReserveNo) {
            return pConn.Query<usp_ReservationGetByResNo>("usp_ReservationGetByResNo", new { ReserveNo = pReserveNo }, commandType: CommandType.StoredProcedure).ToList();
        }


    }
}
