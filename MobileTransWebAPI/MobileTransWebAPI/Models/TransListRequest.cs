﻿
namespace MobileTransWebAPI.Models {
    public class TransListRequest {
        public long DriverID { get; set; }
        public long AffDriverID { get; set; }
        public bool CurrentMonth { get; set; }

        public bool GetAllForMasterVoucher { get; set; }

        public string VoucherNo { get; set; }

        public string RequestKey { get; set; }

        public bool FullDate { get; set; }
    }
}